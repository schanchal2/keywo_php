//variable to pass in loadHomePostTimeline with value 0
var lastPostTime = 0;
var searchKeywo, searchText;

$(document).ready(function() {

    //tooltip
    // $('[data-toggle="tooltip"]').tooltip({
    //     trigger:'hover'
    // });

    /*for kepping side panel cards fixed while scrolling the middle panel*/
    // var leftSidePanel = $('.left-panel').offset().top;
    // var leftPanelWidth = $(".left-panel").width();
    // var rightSidePanel = $(".right-panel").offset().top;
    // var rightPanelWidth = $(".left-panel").width();

    // $(document).scroll(function(){

    /*
     for keeping side panel cards stationary
     */
    // var leftSideTop = leftSidePanel - ($(window).scrollTop() + 50);
    // var rightSideTop = rightSidePanel - ($(window).scrollTop() + 50);
    // if(leftSideTop <= 0){
    //   $('.left-panel').css("position","fixed");
    //   $('.left-panel').css("top","40px");
    //   $('.left-panel').css("width",leftPanelWidth);
    // }else{
    //   $('.left-panel').css("position","relative");
    //   $('.left-panel').css("top","");
    // }
    //
    // if(rightSideTop <= 0){
    //   $('.right-panel').css("position","fixed");
    //   $('.right-panel').css("top","60px");
    //   $('.right-panel').css("width",rightPanelWidth);
    // }else{
    //   $('.right-panel').css("position","relative");
    //   $('.right-panel').css("top","");
    // }


    // });

    //initialization of JS used while setting post
    previewInitialization();
    // alert(socialFileUrl);


    if(socialFileUrl == "social/content_upload") {
        loadAnanyticPage('all');
    } else if(socialFileUrl == "social/content_consumption") {
        loadConsumptionPage('all');
    }

    /*
     @ To initialize comments data on social post details page reload
     */
    if (socialFileUrl == "social/socialPostDetails" || socialFileUrl == "social/social_blog") {
        commentAppendData('onReload', '', '30', '', '');
    }

    // function videoPause(){
    //   console.log ('this');
    // }
    // $(".YouVid").on('click', '#document html body #player .html5-video-player .html5-video-container video', function(){
    //     console.log ('this');
    //   });

    // $('video').click(function(event){
    //     alert('niks');
    // });


    //to pause and play one video at a time
    /// Inject YouTube API script
    // var tag = document.createElement('script');
    // tag.src = "//www.youtube.com/player_api";
    // var firstScriptTag = document.getElementsByTagName('script')[0];
    // firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // // global variable for the player
    // var player;

    // // this function gets called when API is ready to use
    // function onYouTubePlayerAPIReady() {
    //   // create the global player from the specific iframe (#video)
    //   player = new YT.Player('YouVid', {
    //     events: {
    //       // call this function when player is ready to use
    //       'onReady': onPlayerReady
    //     }
    //   });
    // }
    // function onPlayerReady(events){
    //   alert(event);
    // }


    // to Remove modal data when enten modal is removed.
    $("#editPostDetails").on('hide.bs.modal', function() {
        $('#editPostDetails .modal-dialog .card .modal-body').html('');
    });



    // //To redirect on default mode.
    // var systemModeRedirect = $('#modSwitchId').attr('value');
    // // alert ("System Mode : "+systemModeRedirect);
    // if (systemModeRedirect == 1 ) {
    //   // alert('redirect Needed');
    //   var sysMode = $('#modSwitchId').text();
    //   // alert(sysMode);
    //   if (sysMode != "") {
    //     window.location.href = sysMode;
    //   } else {
    //     alert('Error In Page Load of Dashboard');
    //   }
    // }

    /*
     * Calling Function to load Posts at page load time it will load all posts
     * according to url for private or home timeline
     */
    if (socialFileUrl == "social/index") {
        loadHomePostTimeline(lastPostTime, 'all', searchKeywo, searchText);
        //get post count on every 15sec (15000millisecond) interval from getUserInfo api
        setInterval(getUserNewPostCount, 15000);
    } else if (socialFileUrl == "social/privateTimeline") {
        loadPostTimeline('all');
    } else if (socialFileUrl == "social/otherTimeline") {
        //variable for otherTimeline page
        var url = window.location.search.substr(1);
        if (url.indexOf('&') == -1) {
            var otherEmail = url.substring(url.indexOf('=') + 1);
        } else {
            var otherEmailAddr = url.substring(url.indexOf('=') + 1);
            var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
        }
        var otherType = $("#private-next-post-data").attr("data-type");
        // alert(otherType);
        loadOtherTimeline(otherType, otherEmail);
    } else if (socialFileUrl == 'social/userActivity') {
        loadFollowedPeople();
    } else if(socialFileUrl == "social/viewKeywoTimeline") {
        var url     = window.location.search.substring(1);
        var value   = url.lastIndexOf("=");
        if (value == 7) {
            searchKeywo = url.slice(value + 1);
        } else if (value == 10) {
            searchText = url.slice(value + 1);
        }
        loadHomePostTimeline(lastPostTime, 'all', searchKeywo, searchText);
    }





    //Desktop Notification Show toast call on 20 sec interval
    // setInterval(getDesktopNotificationTost, 100000);

    //on click of post count hide div, scroll up, call api to update count & call memcache
    $('#post-count').on('click', function() {
        //hiding div
        $("#post-count").addClass("hide");
        $(".social-status-tabs").each(function() {
            $(this).removeClass("active");
        });
        $("#all").addClass("active");
        //show loader
        $('#ajaxLoader1').show();
        //go to all tab
        window.location.hash = "#all";
        //$("#all").trigger("click");
        //scroll top

        //call api to update count
        var count = $("#post-count").attr("data-post-count");
        // alert(count)
        updateNewPostCount(count);
        //call memcache
        $("#home-next-post-data").attr('data-count', '0');
        $('#home-next-post-data').attr('data-post-type', 'all');
        $('#home-next-post-data').attr('data-scroll-allow', 'true');
        $('#home-next-post-data').attr('data-status-empty', '');
        loadHomePostTimeline(lastPostTime, 'all', searchKeywo, searchText);
    });

    //For "Add keywords" tags
    // ::: TAGS BOX
    $(".tag-text input").on({
        keyup: function(ev) {
            // if: comma|enter (delimit more keyCodes with | pipe)
            if (/(13|32)/.test(ev.which)) {
                convertToTag(this, "keywords-for-tags");
            }
        },
        focusout:function (ev) {
            convertToTag(ev.target,"keywords-for-tags");
        },
        paste: function() {
            //alert("hellqo");
        }
    });
    $('.tag-text').on('click', 'span', function() {

        var dataID = $(this).parent().attr("data-id");
        $(this).remove(); //    if(confirm("Remove "+ $(this).text() +"?"))
        var keywordLength = $("#tags[data-id=" + $(this).attr("data-id") + "]").find("span").length;
        $("#" + dataID).html("");
        var countKeyword = 1;
        $("#tags[data-id=" + dataID + "]").find("span").each(function() {
            if (countKeyword <= 3) {
                $("#" + dataID).append("<a>" + $(this).text() + "</a>&nbsp&nbsp");
            } else {
                $("#" + dataID).append("<a style='color:red;'>" + $(this).text() + "</a>&nbsp&nbsp");
            }
            countKeyword++;
        });

        if ($("#tags").find("span").length > 0 && $("#tags").find("span").length < 4) {
            $("#tags").css("border", "1px solid #ccc");
        }

    });

    //hide all preview forms preview button click
    $(".hide-preview").click(function() {
        $(".audio-preview-form").slideUp(600);
        $(".image-preview-form").slideUp(600);
        $(".video-preview-form").slideUp(600);
        $(".blog-preview-form").slideUp(600);
        $(".loader-audio-form").show();
        $("#audio-iframe").hide();
    });

    // for upload image in blog form
    $('#blog-image-upload').change(function() {

        //var filename = $('#blog-image-upload').val();
        var filename = $('#blog-image-upload').val().replace(/C:\\fakepath\\/i, '');
        $('#blog-img-path').val(filename);
        // console.log("hi");
    });

    // for upload image in image form
    $('#image-form-upload').change(function() {
        readURL(this);
        var filename = $('#image-form-upload').val().replace(/C:\\fakepath\\/i, '');
        //var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
        $('#image-input').val(filename);
        if (imagePreviewExtensionCheck("image-post")) {
            $(".img-preview").css("display", "inline-block");
        } else {
            $(".img-preview").css("display", "none");
        }

    });

    /*
     For previewing image to user in preview form
     */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
                var img = $(".img-preview");
                //  console.log("preview : "+img);
                if (img.width() >= 460 && img.height() >= 230) {} else {
                    //   alert("Image resolution is too small to upload!!!");
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('.share-something').focus(function() {
        $(".share-something-textarea").slideDown().show();
    })

    $(".social-search-box").click(function() {
        $(".share-something-textarea").slideDown(600);
        $(".share-search-top-box").hide();
        //$(".share-post-box").focus();
    });

    //main post button on index page
    $(".share-post-box").focus(function() {
        $(".share-something-compressed").removeClass("share-small");
        $(".post-bottom-controls").show(50);
        $(".keywords-for-tags").show(50);
        $(".share-post-box").removeClass("compressed-div");
        $(".share-something-compressed").addClass("extended-post");
        $(".share-something-textarea").addClass("innerAll");
        $(".post-edit-icon").hide(50);
    });


    //on body click share something box should be closed
    $("body").click(function(e) {
        if (e.target.getAttribute("class") == "tagged-keyword-span") {
            e.preventDefault();
        } else {
            if (e.target.id == "social-micro-blog-box" || $(e.target).parents("#social-micro-blog-box").size() || $(".tagged-keyword-span").length > 0) {} else {
                if (!$(".share-post-box").val() || $.trim($(".share-post-box").val()) == "") {
                    $(".share-post-box").val("");
                    $(".share-something-compressed").addClass("share-small");
                    $(".post-bottom-controls").hide(50);
                    $(".keywords-for-tags").hide(50);
                    $(".share-post-box").addClass("compressed-div");
                    $(".share-something-compressed").removeClass("extended-post");
                    $(".share-something-textarea").removeClass("innerAll");
                    $(".post-edit-icon").show(50);
                }
            }
        }
    });

    //edit short description
    $('#my-info-name-edit').editable({
        url: '../../controllers/social/updateUser.php',
        success: function(data) {
            /* actions on success */
            var result = JSON.parse(data);
            //console.log(result.errCode);
            if(result.errCode == 100){
                window.location.href = rootUrl;
            } else if(result.errCode == 5){
                showToast("failed", 'Error in updation, please try again.');
            }
        },
        toogle: 'manual',
        type: 'text',
        params: function(params) {
            params.srsSelected = params.pk
            return params;
        },
        title: 'Enter comments',
        emptytext: "Hi tell us something about yourself...",
        validate: function(value) {
            if (value.length > 60) {
                return "Character limit exceed(60) ";
            }
        }
    });
    //JS for x-editable on changing short description
    $(".my-info-name-edit").click(function(e) {
        e.stopPropagation();
        $('#my-info-name-edit').editable('toggle');
    })

    // jQuery(".share-something").focus(function() {
    //    jQuery('.share-something-textarea').slideDown(300);
    // }).blur(function() {
    //    jQuery('.share-something-textarea').slideUp(300);
    // });



    // post display JS
    $('#blog').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "blog");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'blog');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'blog', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('blog');
        } else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('blog');
        } else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('blog', otherEmail);
        }
    });
    $('#video').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "video");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'video');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'video', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('video');
        } else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('video');
        } else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('video', otherEmail);
        }
    });
    $('#image').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "image");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'image');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'image', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('image');
        }  else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('image');
        }else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('image', otherEmail);
        }
    });
    $('#audio').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "audio");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'audio');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'audio', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('audio');
        }  else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('audio');
        }else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('audio', otherEmail);
        }
    });
    $('#status').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "status");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'status');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'status', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('status');
        }  else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('status');
        }else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('status', otherEmail);
        }
    });
    $('#all').on('click', function(e) {
        $("#private-next-post-data").attr("data-type", "all");
        e.preventDefault();
        if (socialFileUrl == "social/index" || socialFileUrl == "social/viewKeywoTimeline") {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'all');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, 'all', searchKeywo, searchText);
        } else if (socialFileUrl == "social/privateTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            loadPostTimeline('all');
        } else if (socialFileUrl == "social/content_upload") {
            $("#load-analytic-next-post-data").attr("data-create-time", '');
            loadAnanyticPage('all');
        }else if (socialFileUrl == "social/otherTimeline") {
            $("#private-next-post-data").attr("data-create-time", '');
            //variable for otherTimeline page
            var url = window.location.search.substr(1);
            if (url.indexOf('&') == -1) {
                var otherEmail = url.substring(url.indexOf('=') + 1);
            } else {
                var otherEmailAddr = url.substring(url.indexOf('=') + 1);
                var otherEmail     = otherEmailAddr.substring(0, otherEmailAddr.indexOf('&'));
            }
            loadOtherTimeline('all', otherEmail);
        }
    });
    $('#people').on('click', function(e) {
        e.preventDefault();
        $("#private-next-post-data").attr("data-create-time", '');
        if (socialFileUrl == "social/viewKeywoTimeline") {
            $('#home-post-data').html("");
            if ($("#social-search-text-right-side").val()!='') {
                searchText = $('#social-search-text-right-side').val();
                loadSearchPeopleResult(searchText);
            } else {
                $('#home-post-data').append('<h4 class="modal-header text-blue">End of Search Result/No Search Found</h4>');
            }
        }
    });
    $('#consumption_blog').on('click', function(e) {
        if (socialFileUrl == "social/content_consumption") {
            $("#load-consumption-next-post-data").attr("data-create-time", '');
            loadConsumptionPage('blog');
        }
    });

    $('#consumption_video').on('click', function(e) {
        if (socialFileUrl == "social/content_consumption") {
            $("#load-consumption-next-post-data").attr("data-create-time", '');
            loadConsumptionPage('video');
        }
    });
    $('#consumption_audio').on('click', function(e) {
        if (socialFileUrl == "social/content_consumption") {
            $("#load-consumption-next-post-data").attr("data-create-time", '');
            loadConsumptionPage('audio');
        }
    });
    $('#consumption_image').on('click', function(e) {
        if (socialFileUrl == "social/content_consumption") {
            $("#load-consumption-next-post-data").attr("data-create-time", '');
            loadConsumptionPage('image');
        }
    });
    $('#consumption_status').on('click', function(e) {
        if (socialFileUrl == "social/content_consumption") {
            $("#load-consumption-next-post-data").attr("data-create-time", '');
            loadConsumptionPage('status');
        }
    });

    /*
     Social tabs click
     */
    $(".social-status-tabs").click(function() {
        $(".social-status-tabs").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
    //my earning tabs
    $(".content-tabs").click(function() {
        $(".content-tabs").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
    //notification tabs click
    $(".settings-preference-module a").click(function() {
        $(".settings-preference-module a").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
    //activity tabs click
    $(".acivity-tab-display").click(function() {
        $(".acivity-tab-display").each(function() {
            $(this).removeClass("active");
        });
        $(this).addClass("active");
    });
    //for active tabs
    var link = window.location.href;
    link = link.split("#");
    $("#" + link[1]).trigger("click");

    //social search
    $('#social-search-text-right-side').on("keyup", function(event){
        searchText = $('#social-search-text-right-side').val();
        if (searchText == ' ') {
            $('#social-search-text-right-side').val("").parent(".input-group").addClass('has-error');
        }
        if (event.which == 13) {
            searchText = $('#social-search-text-right-side').val();
            if (searchText != '') {
                if (socialFileUrl == "social/index" || socialFileUrl == "social/viewallFollowedKeyword") {
                    window.location = rootUrl + 'views/social/viewKeywoTimeline.php?searchText=' + searchText;
                }
                if (socialFileUrl == "social/viewKeywoTimeline") {
                    window.history.pushState(searchText, "searchText", rootUrl + 'views/social/viewKeywoTimeline.php?searchText=' + searchText);
                }
            }
            socialSearchResult();
            $("#social-search-text").addClass("input-group social-right-panel-search__input-group");
        }
    });

    $('.social-right-panel-search__search-btn').on("click", function() {
        searchText = $('#social-search-text-right-side').val();
        if (searchText != '') {
            if (socialFileUrl == "social/index" || socialFileUrl == "social/viewallFollowedKeyword") {
                window.location = rootUrl + 'views/social/viewKeywoTimeline.php?searchText=' + searchText;
            }
            if (socialFileUrl == "social/viewKeywoTimeline") {
                window.history.pushState(searchText, "searchText", rootUrl + 'views/social/viewKeywoTimeline.php?searchText=' + searchText);
            }
        }
        socialSearchResult();
        $("#social-search-text").addClass("input-group social-right-panel-search__input-group");
    });
    //append search value
    if (searchText != undefined) {
        $('#social-search-text-right-side').val(decodeURIComponent(searchText));
    }

    //notify on click
    $("#append-data-notify").load("notificationData.php?category=notify-all");



}); // end of document ready
$('.convertCurrencyPrice').click(function() {
    var currencyType = $('#login-header-start').attr('user-curr-prefer');
    alert(currencyType);
    // convertPrice(currencyType);
});

//function to view all notification
$(".notify-view-data").click(function(e){
    var selectedId = $(this).attr('id');
    switch(selectedId) {
        case 'notify-all':
            $("#append-data-notify").load("notificationData.php?category=notify-all");
            break;
        case 'notify-comment':
            $("#append-data-notify").load("notificationData.php?category=notify-comment");
            break;
        case 'notify-following':
            $("#append-data-notify").load("notificationData.php?category=notify-following");
            break;
        case 'notify-like':
            $("#append-data-notify").load("notificationData.php?category=notify-like");
            break;
        case 'notify-mention':
            $("#append-data-notify").load("notificationData.php?category=notify-mention");
            break;
        case 'notify-reply':
            $("#append-data-notify").load("notificationData.php?category=notify-reply");
            break;
        case 'notify-ask':
            $("#append-data-notify").load("notificationData.php?category=notify-ask");
            break;
        case 'notify-bid':
            $("#append-data-notify").load("notificationData.php?category=notify-bid");
            break;
        case 'notify-buy':
            $("#append-data-notify").load("notificationData.php?category=notify-buy");
            break;
        case 'notify-deposit':
            $("#append-data-notify").load("notificationData.php?category=notify-deposit");
            break;
        case 'notify-withdrawal':
            $("#append-data-notify").load("notificationData.php?category=notify-withdrawal");
            break;
        case 'notify-wallet':
            $("#append-data-notify").load("notificationData.php?category=notify-wallet");
            break;
    }
});

//to get search result for social
function socialSearchResult() {
    $('.keyword-feed-detail').addClass('hide');
    if (isNotEmpty("social-search-text")) {
        var type   = $('.social-status-tabs.active').attr("id");
        searchText = $('#social-search-text-right-side').val();
        if (type == "people") {
            loadSearchPeopleResult(searchText);
        } else {
            $("#home-next-post-data").attr('data-count', '0');
            $('#home-next-post-data').attr('data-post-type', 'all');
            $('#home-next-post-data').attr('data-scroll-allow', 'true');
            $('#home-next-post-data').attr('data-status-empty', '');
            loadHomePostTimeline(lastPostTime, type, searchKeywo, searchText);
        }
    } else {
        $('.follower-container.innerT').html("");
        $('#search-error-msg').parent(".modal-header").remove();
        $('#home-post-data h4 ').addClass("hidden");
        $('#home-post-data').append('<h4 class="modal-header text-blue">Enter text to get search result </h4>');
    }
}


/*------------mention users------------------*/
var tags           = [];
var suggestionData = [];
$(function() {
    $(".can-mention-user").on("keyup", function(e) {
        if ($(this).val().length >= 1) {
            mentionOnKeyup($(this).val());
        }
    });
});

function mentionOnKeyup(mentionInput) {
    var atkeyword = mentionInput.split(' ');
    if ((atkeyword[atkeyword.length - 1]).charAt(0) == '@') {
        var getSuggestion = (atkeyword[atkeyword.length - 1]).substring(1);
        if (getSuggestion != "") {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: { lastChar: getSuggestion },
                url: '../../controllers/social/getMentionUser.php',
                async: false,
                success: function(data) {
                    if (data.errCode == -1) {
                        tags = [];
                        for (var i = 0; i < data.errMsg.length; i++) {
                            tags.push('{"name":"' + data.errMsg[i].first_name + ' ' + data.errMsg[i].last_name + '","img":"' + data.errMsg[i].imgSrc + '","job":"' + data.errMsg[i].account_handle + '","email":"' + data.errMsg[i].email + '"}');
                        }
                        suggestionData = $.map(tags, function(values) {
                            var value = JSON.parse(values);
                            return { 'name': value.name, 'img': value.img, 'job': value.job, 'email': value.email };
                        });
                    } else if (data.errCode == 4) {
                        suggestionData = "No Result"
                    }
                },
                error: function() {
                    return false;
                }
            });
            if (getSuggestion.length == 1) {
                $('.inputor').atwho({
                    at: "@" + getSuggestion,
                    data: suggestionData,
                    limit: 10,
                    displayTpl: "<li class='sugg-wrapper'><img class='sugg-img' src='${img}' height='32' width='32'/><div class='sugg-name'> ${name}<div class='keyhandle'>${job}</div> </div><div class='hidden selectedEmail'>${email}</div></li>",
                    insertTpl: '@${job}',
                });
                $('.inputor').on("inserted.atwho", function(event, query) {
                    var htmlText   = query.html(query.html());
                    //console.log(htmlText.find(".selectedEmail").html());
                });
            }
        }
    }
}
/*------------end of mention users------------------*/

/*------------get timeline data & related task on post---------------------*/
// JS to update post count in DB using api
function updateNewPostCount(postcount) {
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        data: { count: postcount },
        url: '../../controllers/social/updateNewPostCount.php',
        success: function(postcount) {
            return true;
        },
        error: function() {
            return false;
        }
    });
}


// JS to get user new post count from getUserInfo api
function getUserNewPostCount() {
    $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '../../controllers/social/getNewPostCount.php',
        success: function(postcount) {
            $(".my-info-posts-count").text(postcount['postCount']);
            if ((postcount['value'] != 0) && (postcount['value'] != "true") && (postcount['value'] != "false")) {
                $("#post-count").attr("data-post-count", postcount['value']);
                $("#post-count").attr("data-post-count-msg", postcount['errMsg']);
                $("#post-count a").text(postcount['errMsg']);
                $("#post-count").removeClass("hide");
            } else if (postcount['value'] == "true") {
                $("#post-count").addClass("hide");
                updateNewPostCount('0');
                $("#home-next-post-data").attr('data-count', '0');
                $('#home-next-post-data').attr('data-post-type', 'all');
                $('#home-next-post-data').attr('data-scroll-allow', 'true');
                $('#home-next-post-data').attr('data-status-empty', '');
                loadHomePostTimeline(lastPostTime, "all", searchKeywo, searchText);
            } else {
                $("#post-count").attr("data-post-count", 0);
            }
        },
        error: function(event, jqxhr, status, thrownError) {
            $("#post-count").attr("data-post-count", 0);
        }
    });
}


// JS Function to get all user's search on people search.
function loadSearchPeopleResult(searchUserName) {
    $('.keyword-feed-detail').addClass('hide');
    var dataCount          = $("#private-next-post-data").attr("data-count");
    var lastDataCreateTime = $("#private-next-post-data").attr("data-create-time");
    $.ajax({
        type: 'POST',
        dataType: 'html',
        data: ({ 'searchUserName': searchUserName, 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime }),
        url: '../../views/social/searchUserTimelineContent.php',
        success: function(postdata) {
            $('#show-error-message').hide();
            $('#home-post-data').html("");
            if (lastDataCreateTime == "") {
                $('#home-post-data').html(postdata);
            } else {
                $('#home-post-data').append(postdata);
            }
            $("#private-next-post-data").attr("data-create-time", $("#private-last-data-time-" + dataCount).attr('lastDataTime'));
            $("#private-next-post-data").attr("data-status-empty", $("#private-last-data-time-" + dataCount).attr('data-empty'));
            $("#private-next-post-data").attr("data-count", parseInt(dataCount) + 1);
        },
        beforeSend: function() {
            $("#private-next-post-data").attr("data-scroll-allow", "false");
        },
        complete: function() {
            $('#ajaxLoader1').hide();
            $("#private-next-post-data").attr("data-scroll-allow", "true");
        },
        error: function() {
            // alert('error on Loading Timeline');
            $('#loading-content-div').hide();
            $('#show-error-message').text('No Result Found!!');
        }
    });
}


// JS Function to get all Post from api to Show on other user's timeline.
function loadOtherTimeline(type, otherEmail) {
    var dataCount = $("#private-next-post-data").attr("data-count");
    var lastDataCreateTime = $("#private-next-post-data").attr("data-create-time");
    $.ajax({
        type: 'POST',
        dataType: 'html',
        data: ({ 'type': type, 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime, 'otherEmail': otherEmail }),
        url: '../../views/social/otherTimelineContent.php',
        success: function(postdata) {
            $('#show-error-message').hide();
            if (lastDataCreateTime == "") {
                $('#private-post-data').html(postdata);
            } else {
                $('#private-post-data').append(postdata);
            }
            $("#private-next-post-data").attr("data-create-time", $("#private-last-data-time-" + dataCount).attr('lastDataTime'));
            $("#private-next-post-data").attr("data-status-empty", $("#private-last-data-time-" + dataCount).attr('data-empty'));
            $("#private-next-post-data").attr("data-count", parseInt(dataCount) + 1);
            $('.social-status-tabs').removeClass('active');
            $('#' + type).addClass('active');
        },
        beforeSend: function() {
            $("#private-next-post-data").attr("data-scroll-allow", "false");
        },
        complete: function() {
            $('#ajaxLoader1').hide();
            $("#private-next-post-data").attr("data-scroll-allow", "true");
        },
        error: function() {
            // alert('error on Loading Timeline');
            $('#loading-content-div').hide();
            $('#show-error-message').text('No Post Yet!!');
        }
    });
}


// JS Function to get all Post from api to Show on private timeline.
function loadPostTimeline(type) {
    var dataCount = $("#private-next-post-data").attr("data-count");
    var lastDataCreateTime = $("#private-next-post-data").attr("data-create-time");
    $.ajax({
        type: 'POST',
        dataType: 'html',
        data: ({ 'type': type, 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime }),
        url: '../../views/social/privateTimelineContent.php',
        success: function(postdata) {
            $('#show-error-message').hide();
            if (lastDataCreateTime == "") {
                $('#private-post-data').html(postdata);
            } else {
                $('#private-post-data').append(postdata);
            }
            $("#private-next-post-data").attr("data-create-time", $("#private-last-data-time-" + dataCount).attr('lastDataTime'));
            $("#private-next-post-data").attr("data-status-empty", $("#private-last-data-time-" + dataCount).attr('data-empty'));
            $("#private-next-post-data").attr("data-count", parseInt(dataCount) + 1);
        },
        beforeSend: function() {
            $("#private-next-post-data").attr("data-scroll-allow", "false");
        },
        complete: function() {
            $('#ajaxLoader1').hide();
            $("#private-next-post-data").attr("data-scroll-allow", "true");
        },
        error: function() {
            // alert('error on Loading Timeline');
            $('#loading-content-div').hide();
            $('#show-error-message').text('No Post Yet!!');
        }
    });
}


// JS Function to get all Post from Memcache store into Session & Show on home timeline.
function loadHomePostTimeline(lastPostTime, type, keywo, searchText) {
    var count = $('#home-next-post-data').attr('data-count');
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        data: { lastPostTime: lastPostTime, type: type, keywo:keywo, searchText:searchText },
        url: '../../controllers/social/getPostTimeline.php',
        // beforeSend: function(){
        //     $('#loading-content-div').show();
        // },
        success: function(postdata) {
            $('#show-error-message').hide();
            if (keywo || searchText) {
                $.ajax({
                    type: 'POST',
                    dataType: 'html',
                    data: { type: type, n: count, postdata:postdata },
                    url: '../../views/social/searchTimelineContent.php',
                    success: function(posthomedata) {
                        $('#show-error-message').hide();
                        $('#ajaxLoader1').hide();
                        // $('#loading-content-div').hide();
                        if (searchText) {
                            count = 0;
                        }
                        if (count == 0) {
                            $('#home-post-data').html("");
                            $('#home-post-data').html(posthomedata);
                            $('#home-next-post-data').attr('data-count', parseInt(count) + 10);
                            var checkScrollStatus = $('#checkScrollStatus-' + count).attr('data-scroll-allow');
                            $('#home-next-post-data').attr('data-status-empty', checkScrollStatus);
                        }
                    },
                    error: function() {
                        $('#loading-content-div').hide();
                        $('#show-error-message').text('No Search Found!!');
                    }
                });
            } else {
                $.ajax({
                    type: 'GET',
                    dataType: 'html',
                    data: { type: type, n: count },
                    url: '../../views/social/homeTimelineContent.php',
                    success: function(posthomedata) {
                        $('#show-error-message').hide();
                        $('#ajaxLoader1').hide();
                        $('#tags input').val('');
                        // $('#loading-content-div').hide();
                        if (count == 0) {
                            $('#home-post-data').html(posthomedata);
                            $('#home-next-post-data').attr('data-count', parseInt(count) + 10);
                            var checkScrollStatus = $('#checkScrollStatus-' + count).attr('data-scroll-allow');
                            $('#home-next-post-data').attr('data-status-empty', checkScrollStatus);
                        }
                    },
                    error: function() {
                        $('#loading-content-div').hide();
                        $('#show-error-message').text('No Post Yet!!');
                    }
                });
            }
        }
    });
}


//to remove post by owner
function removePersonalPost(postId, type, postTime) {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: { postId: postId, type: type, postTime: postTime },
        url: '../../controllers/social/removePost.php',
        success: function(data) {
            $(".yes-remove-post-btn").removeAttr("disabled");
            if (data.errCode == -1) {
                if (socialFileUrl == "social/index" || socialFileUrl == "social/privateTimeline") {
                    //update post count
                    var postCount = $('.my-info-posts-count').text();
                    var newPostCount = postCount - 1;
                    $('.my-info-posts-count').text(newPostCount);
                    $('#showModalPost .close-dialog').click();
                    $("#post_" + postId).slideUp(800);
                    $("#" + postId).slideUp(800);
                } else {
                    window.location = rootUrl + 'views/social/index.php';
                }
            } else if (data.errCode == 100) {
                window.location = rootUrl;
            } else if (data.errCode == 2) {
                postDeletedModal('This Post No longer Exist');
                postLoadAsPerUrl();
            }
        },
        error: function() {
            $(".yes-remove-post-btn").removeAttr("disabled");
            showToast("failed", 'Post not removed. Please try again.');
        }
    });
}


//to hide post by owner/viewer
function hidePost(postId, type, postTime, postSharedType,email,action) {
    if (action == "hide") {
        var blockCheck = getBlockedUser(email);
        if (blockCheck) {
            var removeCheck = removePostOfUser(postId, type, postTime, email);
            if (removeCheck) {
                var hideCheck = hidePostOfUser(postId, type, postTime, email);
                if (hideCheck) {
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        data: {postId: postId, type: type, postTime: postTime, postSharedType: postSharedType, action:action},
                        url: '../../controllers/social/hidePost.php',
                        success: function (data) {
                            if (data.errCode == -1) {
                                if (socialFileUrl == "social/index" || socialFileUrl == "social/privateTimeline" || socialFileUrl == "social/otherTimeline" || socialFileUrl == "social/userActivity" || socialFileUrl == "social/viewKeywoTimeline") {
                                    $('#showModalPost .close-dialog').click();
                                    $("#post_" + postId).slideUp(800);
                                    $("#" + postId).slideUp(800);
                                    return true;
                                } else {
                                    window.location = rootUrl + 'views/social/index.php';
                                }
                            } else if (data.errCode == 100) {
                                window.location = rootUrl;
                            } else if (data.errCode == 4) {
                                postDeletedModal('This Post is already hidden');
                                postLoadAsPerUrl();
                            }
                        },
                        error: function () {
                            alert('Post not removed. Please try again.');
                        }
                    });
                }
            }
        }
    } else if (action == "unhide") {
        var blockCheck = getBlockedUser(email);
        if (blockCheck) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                data: {postId: postId, type: type, postTime: postTime, postSharedType: postSharedType, action:action},
                url: '../../../controllers/social/hidePost.php',
                success: function (data) {
                    if (data.errCode == -1) {
                        $("#post_" + postId).slideUp(800);
                        showToast("Suceess", 'Post unhidden successfully!!');
                        return true;
                    } else if (data.errCode == 2) {
                        showToast("failed", 'Post already unhidden/deleted!!');
                        //$("#post_" + postId).hide();
                    }
                },
                error: function () {
                    showToast("failed", 'Something Went Wrong!!');
                }
            });
        }
    }
}



//js to store post
function createNewPost(postType, formname, method) {

    var validBlog = false;
    var keywordString = "";
    var controllerUrl = "../../controllers/social/setPostController.php";
    if (method == 'create') {
        if ($("#tags input").val() != "") {
            var txt = $("#tags input").val(); // allowed all characters
            var sameKeywordFlag = 0;

            $("#tags input").parent().find("span").each(function() {
                if ($(this).text() == "#" + txt) {
                    sameKeywordFlag = 1;
                }
            });
            if (sameKeywordFlag == 0) {
                if (txt) {
                    $("<span/>", { class: "tagged-keyword-span", text: '#' + txt.toLowerCase(), insertBefore: $("#tags input[type='text']") });
                }
                displayPreview($("#tags input").parent());
            }
            $("#tags input").val("");
        }

        $("#tags span").each(function() {
            keywordString = keywordString + "," + $(this).text().substring(1);
        });
        $("input[name=modalKeywords]").val(keywordString.substring(1));
    }


    // alert(controllerUrl);
    // var modalCommentText = $("#"+formname).find('#modalComment').val();
    // var modalCommentLength = modalCommentText.length;
    // alert(modalCommentLength);

    var form = $("#" + formname)[0];
    var formData = new FormData(form);
    // console.log(formData);
    // alert(postType);
    if (postType == 'audio') {
        if (isNotEmpty("audio-post") & hasValidLink('audio') & descriptionBoxCheck("audio-post")) {
            validBlog = true;
        }
    } else if (postType == 'video') {
        console.log("postType __ video");
        if (isNotEmpty("video-post") & hasValidLink('video') & descriptionBoxCheck("video-post")) {
            validBlog = true;
        }
    } else if (postType == 'image') {
        if (isNotEmpty("image-post") & imageExtensionCheck("image-post") & descriptionBoxCheck("image-post")) {
            validBlog = true;
        }
    } else if (postType == 'blog') {
        var imageCheckFlag = true;
        // $("#blog-post .imageCheck").each(function(){
        var image_file = $("#blog-post .imageCheck").val();
        if (image_file != '') {
            if (imageExtensionCheck("blog-post")) {
                imageCheckFlag = true;
            } else {
                imageCheckFlag = false;
            }
        }
        // });
        if (isNotEmpty("blog-post") && imageCheckFlag == true) {
            validBlog = true;
        }
        var editorValue = $('#editor').html();

        formData.append("blog_content", editorValue);
    } else if (postType == 'status') {
        if (isNotEmpty("status-post") & descriptionBoxCheck("status-post")) {
            validBlog = true;
        }
    } else if (postType == 'status-modal') {
        if (isNotEmpty("status-post-modal") & descriptionBoxCheck("status-post-modal")) {
            validBlog = true;
        }
        postType = 'status';
    }
    var ftue = '';
    var ftue = '';
    if (validBlog) {
        if (postType == 'blog') {
            $.ajax({
                type: 'POST',
                url: controllerUrl,
                data: formData,
                dataType: 'JSON',
                async: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".form-post-btn").prop("disabled", true);
                    $(".form-post-btn").val("Posting...");
                },
                complete: function() {
                    $(".form-post-btn").prop("disabled", false);
                    $(".form-post-btn").val("Post");
                },
                success: function(data) {

                    if (data.errCode == -1) {
                        $('.close-button').click();
                        //clear data on form
                        $("#" + formname + " input").each(function() {
                            if ($(this).attr("type") != "hidden" && $(this).attr("type") != "button") {
                                $(this).val("");
                                $("#tags span").each(function() { $(this).remove() })
                            }
                        });
                        $("#" + formname + " textarea").each(function() {
                            $(this).val("");
                        });
                        //refresh post data
                        postLoadAsPerUrl();

                        // showToast("success",data.errMsg);
                        //update post count
                        if (method == 'create') {
                            var postCount = $('.my-info-posts-count').text();
                            var newPostCount = ++postCount;
                            $('.my-info-posts-count').text(newPostCount);
                        }
                        $('#editPostDetails .modal-dialog .card .modal-body').html('');
                    }else if(data.errCode == 9){
                        postDeletedModal('Post description / Blog content should not exceeds limit.');
                        postLoadAsPerUrl();
                    } else if (data.errCode == 4) {
                        postDeletedModal('This Post No longer Exist');
                        postLoadAsPerUrl();
                    } else {
                        showToast("failed", data.errMsg);
                    }
                },
                error: function(data) {
                    showToast("failed", 'Failed To Add/Update');
                }
            });

        } else {
            $.ajax({
                type: 'POST',
                url: controllerUrl,
                data: formData,
                dataType: 'JSON',
                async: false,
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $(".form-post-btn").prop("disabled", true);
                    $(".form-post-btn").val("Posting...");
                },
                complete: function() {
                    $(".form-post-btn").prop("disabled", false);
                    $(".form-post-btn").val("Post");
                },
                success: function(data) {
                    if (data.errCode == -1) {

                        $('.close-button').click();
                        //clear data on form
                        $("#" + formname + " input").each(function() {
                            if ($(this).attr("type") != "hidden" && $(this).attr("type") != "button") {
                                $(this).val("");
                                $("#tags span").each(function() { $(this).remove() })
                            }
                        });
                        $("#" + formname + " textarea").each(function() {
                            $(this).val("");
                        });
                        //refresh post data
                        postLoadAsPerUrl();

                        if (method == 'create') {
                            var postCount = $('.my-info-posts-count').text();
                            var newPostCount = ++postCount;
                            $('.my-info-posts-count').text(newPostCount);
                        }
                        $('#editPostDetails .modal-dialog .card .modal-body').html('');

                        showToast("Success", "Post created Successfully.");
                        ftue = data;
                    }else if (data.errCode == 4) {
                        postDeletedModal('This Post No longer Exist');
                        postLoadAsPerUrl();
                        ftue = false;
                    }
                    else if(data.errCode == 51){
                        $(".video-card").parent().css("border", "1px solid #a94442");
                        $("#video-input").html("<h1 class='text-center text-red'>Invalid Link</h1>");
                        ftue = false;
                    }
                    else {
                        showToast("failed", data.errMsg);
                        ftue = false;
                    }
                },
                error: function(data) {
                    showToast("failed", 'Failed To Add/update');
                }
            });
        }
    } else {}

    return ftue;
}


//Insert in preview form for all
$(".preview").on("keyup", function(e) {
    displayPreview($(this)); //for displaying preview forms
});
// $(".preview-tag-change").keyup(function(e){
//   if(e.which == 188 || e.which == 13 || e.which == 32){
//     displayPreview($(this).parent());
//   }
// });

/**
 * [convertToTag description]
 * @param  {[object]} targetElement [it will be input tag]
 * @param  {[sting]} classToApply  [redudent parameter as two variations of same code found with just different classname ]
 */
function convertToTag(targetElement, classToApply) {
    var txt = targetElement.value;
    txt = txt.replace(" ", "");
    txt = $.trim(txt);

    var sameKeywordFlag = 0;
    $(targetElement).parent().find("span").each(function() {
        if ($(this).text().toLowerCase() == "#" + txt.toLowerCase()) {
            sameKeywordFlag = 1;
        }
    });
    if (sameKeywordFlag == 0) {
        if (txt) $("<span/>", { class: classToApply, text: '#' + txt.toLowerCase(), insertBefore: targetElement });
        displayPreview($(targetElement).parent());
    }
    targetElement.value = "";
}

//Check inputs are empty or not before post
function isNotEmpty(formID) {
    var emptyFlag = 0;
    // alert(formID);
    // console.log($(""+formID+" .checkEmpty"));
    $("#" + formID + " .checkEmpty").each(function() {
        // console.log('ech : '+$(this).val());
        if ($.trim($(this).val()) == "" && !$(this).hasClass("tag-input") && !$(this).hasClass("hero-unit")) {
            $(this).parent().attr("class", "has-error has-feedback");
            emptyFlag = 1;
        } else if ($(this).hasClass("hero-unit")) {
            if ($.trim($("#editor").html()) == "") {
                emptyFlag = 1;
                $(this).css("border", "1px solid #a94442");
            } else {
                $(this).css("border", "1px solid #ccc");
            }
        } else {
            $(this).parent().attr("class", "");
        }
        if ($(this).hasClass("tag-input")) {
            if ($("#" + formID + " #tags").find("span").length == 0 || $("#" + formID + " #tags").find("span").length > 3) {
                emptyFlag = 1;
                $("#" + formID + " #tags").css("border", "1px solid #a94442");
            } else {
                $("#" + formID + " #tags").css("border", "1px solid #ccc");
            }
        }
    });
    //  alert(emptyFlag);
    if (emptyFlag == 0) {
        return true;
    } else {

        return false;
    }
}

//check Comment for report post
function descriptionReportBoxCheck(formID) {
    emptyFlag = 0;
    var modalCommentText = $("#" + formID).find('#modalComment').val();
    var modalCommentLength = modalCommentText.length;
    if (modalCommentLength > maxCharReportCount) {
        emptyFlag = 1;
    } else if (formID == "status-post" && modalCommentLength == 0) {
        emptyFlag = 1;
    } else if (formID == "status-post-modal" && modalCommentLength == 0) {
        emptyFlag = 1;
    }
    if (emptyFlag == 0) {
        $("#" + formID).find('#modalComment').parent().attr("class", "");
        return true;
    } else {
        $("#" + formID).find('#modalComment').parent().attr("class", "has-error has-feedback");
        return false;
    }
}

//Check inputs are empty or not before post
function modalIsNotEmpty(formID) {
    var emptyFlag = 0;
    // alert(formID);
    // console.log($(""+formID+" .checkEmpty"));
    $(formID + " .checkEmpty").each(function() {
        // console.log('ech MOdal : '+$(this).val());
        if ($.trim($(this).val()) == "" && !$(this).hasClass("tag-input") && !$(this).hasClass("hero-unit")) {
            $(this).parent().attr("class", "has-error has-feedback");
            emptyFlag = 1;
        } else if ($(this).hasClass("hero-unit")) {
            if ($.trim($("#editor").html()) == "") {
                emptyFlag = 1;
                $(this).css("border", "1px solid #ff0000");
            } else {
                $(this).css("border", "1px solid #ccc");
            }
        } else {
            $(this).parent().attr("class", "");
        }
        if ($(this).hasClass("tag-input")) {
            if ($(formID + " #tags").find("span").length == 0 || $(formID + " #tags").find("span").length > 3) {
                emptyFlag = 1;
                $(formID + " #tags").css("border", "1px solid #a94442");
            } else {
                $(formID + " #tags").css("border", "1px solid #ccc");
            }
        }

    });
    // alert(emptyFlag);
    if (emptyFlag == 0) {
        return true;
    } else {

        return false;
    }
}



//check Comment Section
function descriptionBoxCheck(formID) {
    emptyFlag = 0;
    var modalCommentText = $("#" + formID).find('#modalComment').val();
    var modalCommentLength = modalCommentText.length;
    if (modalCommentLength > maxCharCount) {
        emptyFlag = 1;
    } else if (formID == "status-post" && modalCommentLength == 0) {
        emptyFlag = 1;
    } else if (formID == "status-post-modal" && modalCommentLength == 0) {
        emptyFlag = 1;
    }
    if (emptyFlag == 0) {
        $("#" + formID).find('#modalComment').parent().attr("class", "");
        return true;
    } else {
        $("#" + formID).find('#modalComment').parent().attr("class", "has-error has-feedback");
        return false;
    }
}


//check Image Extension
function imageExtensionCheck(formID) {
    var emptyFlag = 1;
    // alert(formID);
    // console.log($(""+formID+" .checkEmpty"));
    var extensions = new Array("jpg", "jpeg", "gif", "png", "bmp");
    $("#" + formID + " .imageCheck").each(function() {
        var image_file = $(this).val();
        var image_length = $(this).val().length;
        var pos = image_file.lastIndexOf('.') + 1;
        var ext = image_file.substring(pos, image_length);
        var final_ext = ext.toLowerCase();
        var flagForImage = false;
        for (i = 0; i < extensions.length; i++) {
            if (extensions[i] == final_ext) {
                emptyFlag = 0;
                flagForImage = true;
            }
        }
        // alert(flagForImage);
        if (flagForImage) {
            $(this).parent().attr("class", "");
        } else {
            $(this).parent().attr("class", "has-error has-feedback");
        }
    });

    if (emptyFlag == 0) {

        return true;
    } else {

        return false;
    }
}

//check sound cloud and youtube links
function hasValidLink(mediaType) {
    if (mediaType == "audio") {
        var hasValidAudioFlag = 0;
        $(".checkSoundCloudLink").each(function() {
            var soundLink = $(this).val();
            if (soundLink.startsWith("https://soundcloud.com/") || soundLink.search("https://w.soundcloud.com/") || soundLink.startsWith("https://w.soundcloud.com/")) {
                $(this).parent().attr("class", "");
            } else {
                hasValidAudioFlag = 1;
                $(this).parent().attr("class", "has-error has-feedback");
            }
        });
        if (hasValidAudioFlag == 0) {
            return true;
        } else {
            return false;
        }
    } else if (mediaType == "video") {
        var hasValidVideoFlag = 0;
        $(".checkVideoLink").each(function() {
            var videoLink = $(this).val();
            if ((videoLink.startsWith("https://www.youtube.com/") || videoLink.startsWith("https://youtu.be/") || videoLink.startsWith("https://www.youtube.com/embed/") || videoLink.startsWith("https://player.vimeo.com/") || videoLink.startsWith("https://vimeo.com/")) && (!hasMultipleHttps(videoLink))) {
                $(this).parent().attr("class", "");
            } else {
                hasValidVideoFlag = 1;
                $(this).parent().attr("class", "has-error has-feedback");
            }
        });
        if (hasValidVideoFlag == 0) {
            return true;
        } else {
            return false;
        }
    }
}

function checkYoutubeLink(url) {
    var p = /^(?:https?:\/\/)?(?:www\.)?youtube\.com\/watch\?(?=.*v=((\w|-){11}))(?:\S+)?$/;
    return (url.match(p)) ? RegExp.$1 : false;
}

function checkVimeoLink(url) {
    if (/https:\/\/vimeo.com\/\d{8}(?=\b|\/)/.test(url) ) {
        return true; //alert("matches");
    } else {
        return false; //alert("doesn't match")
    };
}

function hasMultipleHttps(sUrl) {
    return (sUrl.lastIndexOf("https:") > 0) ? true : false;
}

function displayPreview(srcElem) {
    var api = $(srcElem).data("api");
    var dataID = $(srcElem).data("id");
    var dataTagID = "";
    if (dataID == "audio-selected-keywords" || dataID == "image-selected-keywords" || dataID == "video-selected-keywords" || dataID == "blog-selected-keywords") {
        dataTagID = "tags";
    }
    if (api == "videoAPI") {
        //ajax call for video

        var url = $(srcElem).val();
        if (url.indexOf("https://www.youtube.com/") != -1) {
            if (checkYoutubeLink(url) !== false) {
                var urlLink = $(srcElem).val();
                urlLink = urlLink.replace("watch?v=", "embed/");
                $("#" + dataID).html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + urlLink + '"></iframe>');
            } else {
                $("#" + dataID).html("<span style='color:red;'>Invalid Link</span>");
            }
        } else if (url.indexOf("https://vimeo.com/") != -1) {
            if (checkVimeoLink($(srcElem).val()) !== false) {
                var urlLink = $(srcElem).val();
                console.log(urlLink)
                urlLink = urlLink.replace("vimeo.com/", "player.vimeo.com/video/");
                $("#" + dataID).html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + urlLink + '"></iframe>');
            } else {
                $("#" + dataID).html("<span style='color:red;'>Invalid Link</span>");
            }
        } else {
            $("#" + dataID).html("<span style='color:red;'>Invalid Link</span>");
        }

    } else if (api == "soundcloud") {
        //ajax call for audio sound cloud
    } else if (dataTagID != "" && $("#" + dataTagID + "[data-id=" + dataID + "]").find("span").length > 0) {
        var keywordLength = $("#" + dataTagID + "[data-id=" + dataID + "]").find("span").length;
        $("#" + dataID).html("");
        var countKeyword = 1;
        $("#" + dataTagID + "[data-id=" + dataID + "]").find("span").each(function() {
            if (countKeyword <= 3) {
                $("#" + dataID).append("<a data-toggle='tooltip' data-placement='top' title='" + $(this).text() + "'>" + $(this).text() + "</a>&nbsp&nbsp");
            } else {
                $("#" + dataID).append("<a style='color:red;' data-toggle='tooltip' data-placement='top' title='" + $(this).text() + "'>" + $(this).text() + "</a>&nbsp&nbsp");
            }
            countKeyword++;
        });
    } else if (dataID == "blog-preview-description") {
        $("#" + dataID).html($(srcElem).html());
    } else {
        $("#" + dataID).html($(srcElem).val());
    }

}

/*Blog form rich text editor js*/
$(function() {
    function initToolbarBootstrapBindings() {
        var fonts = ['Serif', 'Sans', 'Arial', 'Arial Black', 'Courier',
                'Courier New', 'Comic Sans MS', 'Helvetica', 'Impact', 'Lucida Grande', 'Lucida Sans', 'Tahoma', 'Times',
                'Times New Roman', 'Verdana'
            ],
            fontTarget = $('[title=Font]').siblings('.dropdown-menu');
        $.each(fonts, function(idx, fontName) {
            fontTarget.append($('<li><a data-edit="fontName ' + fontName + '" style="font-family:\'' + fontName + '\'">' + fontName + '</a></li>'));
        });
        $('a[title]').tooltip({ container: 'body' });
        $('.dropdown-menu input').click(function() {
            return false; })
            .change(function() { $(this).parent('.dropdown-menu').siblings('.dropdown-toggle').dropdown('toggle'); })
            .keydown('esc', function() { this.value = '';
                $(this).change(); });

        $('[data-role=magic-overlay]').each(function() {
            var overlay = $(this),
                target = $(overlay.data('target'));
            overlay.css('opacity', 0).css('position', 'absolute').offset(target.offset()).width(target.outerWidth()).height(target.outerHeight());
        });
        if ("onwebkitspeechchange" in document.createElement("input")) {
            var editorOffset = $('#editor').offset();
            $('#voiceBtn').css('position', 'absolute').offset({ top: editorOffset.top, left: editorOffset.left + $('#editor').innerWidth() - 35 });
        } else {
            $('#voiceBtn').hide();
        }
    };


    function showErrorAlert(reason, detail) {
        var msg = '';
        if (reason === 'unsupported-file-type') { msg = "Unsupported format " + detail; } else {
            console.log("error uploading file", reason, detail);
        }
        $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>' +
            '<strong>File upload error</strong> ' + msg + ' </div>').prependTo('#alerts');
    };
    initToolbarBootstrapBindings();
    $('#editor').wysiwyg({ fileUploadError: showErrorAlert });
    window.prettyPrint && prettyPrint();
});

// (function(i, s, o, g, r, a, m) {
//     i['GoogleAnalyticsObject'] = r;
//     i[r] = i[r] || function() {
//         (i[r].q = i[r].q || []).push(arguments)
//     }, i[r].l = 1 * new Date();
//     a = s.createElement(o),
//         m = s.getElementsByTagName(o)[0];
//     a.async = 1;
//     a.src = g;
//     m.parentNode.insertBefore(a, m)
// })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
// ga('create', 'UA-37452180-6', 'github.io');
// ga('send', 'pageview');


! function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (!d.getElementById(id)) { js = d.createElement(s);
        js.id = id;
        js.src = "https://platform.twitter.com/widgets.js";
        fjs.parentNode.insertBefore(js, fjs); } }(document, "script", "twitter-wjs");

/*------------------------share post modal---------------------------------*/
//to show share modal
function sharePostModal(postId, postTime, type, userAccHandle, postMethod,email) {
    if(socialFileUrl == 'social/ft_share'){
        var blockCheck = true;
    }else{
        var blockCheck = getBlockedUser(email);
    }
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, type, postTime, email);
        // alert(removeCheck);
        if (removeCheck) {
            // alert(socialFileUrl);
            if (socialFileUrl == "social/index" || socialFileUrl == "social/privateTimeline" || socialFileUrl == "social/socialPostDetails" || socialFileUrl == 'social/otherTimeline' || socialFileUrl == "social/userActivity" || socialFileUrl == "social/viewKeywoTimeline" || socialFileUrl == "social/ft_share") {

                var hideCheck = hidePostOfUser(postId, type, postTime, email);
                if (hideCheck) {
                    $.ajax({
                        type: 'POST',
                        url: '../../views/social/sharePostModal.php',
                        data: {
                            postId: postId,
                            postTime: postTime,
                            email: email,
                            postMethod: postMethod
                        },
                        dataType: 'html',
                        success: function(data) {
                            if (postMethod == 'create') {
                                $('#showModalPost .modal-dialog .card .modal-body-social-user-image').html(data);
                                previewInitialization();
                                $('#showModalPost').modal('show');
                            } else if (postMethod == 'update') {
                                $('#showModalPost .modal-dialog .card .modal-body-social-user-image').html(data);
                                previewInitialization();
                                $('#showModalPost').modal('show');
                            }
                        },
                        error: function() {
                            if (postMethod == 'create') {
                                showToast("failed", 'Error in post share');
                            } else if (postMethod == 'update') {
                                showToast("failed", 'Error in post update');
                            }
                        }
                    });
                }
            }
        }
    }
}

//setter to share already shared post
function shareUserPost(postId, createdAt, postParentId, postCreatedAt, postMethod, postParentType, postParentEmail, keywords) {
    var postScope = $('#share_post_scope').val();
    var ftueShareParam = '';
    if (postMethod == "update") {
        var postShortDesc = $('#editPostDetails #modalComment').val();
    } else {
        var postShortDesc = $('#showModalPost #modalComment').val();
    }
    // console.log(isNotEmpty("share-post-desc"));
    // console.log(descriptionBoxCheck("share-post-desc"));
    if (isNotEmpty("share-post-desc") && descriptionBoxCheck("share-post-desc")) {
        $.ajax({
            type: 'POST',
            url: '../../controllers/social/setSharePost.php',
            data: {
                postId: postId,
                postTime: createdAt,
                postScope: postScope,
                postShortDesc: postShortDesc,
                postCreatedAt: postCreatedAt,
                postParentId: postParentId,
                postMethod: postMethod,
                postParentType: postParentType,
                postParentEmail: postParentEmail,
                keywords:keywords
            },
            dataType: 'json',
            beforeSend: function() {
                $(".form-post-btn").prop("disabled", true);
                $(".form-post-btn").val("Posting...");
            },
            complete: function() {
                $(".form-post-btn").prop("disabled", false);
                $(".form-post-btn").val("Post");
            },
            success: function(data) {
                $('.close-button').click();
                if (data.errCode == -1) {
                    //update post-share count on share
                    $('.my-share-post-count').text(data.sharePostCount);
                    //refresh post data
                    // alert("hellow");
                    postLoadAsPerUrl();
                    //tab active
                    $(".social-status-tabs").each(function() {
                        $(this).removeClass("active");
                    });
                    $("#all").addClass("active");
                    //show toast
                    showToast("success", data.errMsg);
                    //update count
                    var postCount = $('.my-info-posts-count').text();
                    var newPostCount = ++postCount;
                    $('.my-info-posts-count').text(newPostCount);

                    if(socialFileUrl == "social/ft_share"){
                        ftueSharePost(data);
                    }
                }
            },
            error: function() {
                showToast("failed", 'Failed to share post');
            }
        });
    }
}
/*------------end of shared post--------------------------------*/

// To open Model for edit post
function showModalPost(post_id, createdAt, search_count, $ipAddr, emailId, userSearchType) {
    var blockCheck = getBlockedUser(emailId);
    if (blockCheck) {
        var privacyFlag = removePostOfUser(post_id, userSearchType, createdAt, emailId);
        if (privacyFlag) {
            var hideCheck = hidePostOfUser(post_id, userSearchType, createdAt, emailId);
            if (hideCheck) {
                $.ajax({
                    type: 'POST',
                    url: '../../views/social/showPostModal.php',
                    data: {
                        postId: post_id,
                        postTime: createdAt,
                        searchCount: search_count,
                        ipAddress: $ipAddr,
                        email: emailId,
                        userSearchType: userSearchType
                    },
                    dataType: 'html',
                    success: function (data) {
                        // alert(data);
                        $('#showModalPost .modal-dialog .card .modal-body-social-user-image').html('');
                        $('#showModalPost #comment-form' + post_id).html('');
                        $('#showModalPost .modal-dialog .card .modal-body-social-user-image').html(data);
                        previewInitialization();
                        commentAppendData('function', '', '3', '', post_id);

                        var callEnterEvent = $('#comment-hidden-enter-event-' + post_id).attr('execute-enetr-event');
                        // alert(callEnterEvent);
                        // if (callEnterEvent == 'true'){
                        // alert('true');
                        $('#showModalPost #comment-form' + post_id + ' #user-comment-text' + post_id).keypress(function (e) {
                            if ($(this).val().length >= 1) {
                                mentionOnKeyup($(this).val());
                            }
                            if (e.which == 13) {
                                createCommentOnPostFromModal(post_id);
                            }
                        });
                        $('#comment-hidden-enter-event-' + post_id).attr('execute-enetr-event', 'false');
                        // }
                        if ($('#social-post-delete-div' + post_id).attr('modal-show') != 'none') {
                            $('#showModalPost').modal('show');
                        }
                    },
                    error: function (data) {
                        alert('Error in post Update');
                    }
                });
            }
        }
    }
}

$("#showModalPost").on("hidden.bs.modal", function() {
    $('#showModalPost .modal-dialog .card .modal-body-social-user-image').html('');
    $('#showModalPost .comment-text-empty').html('');
});

/******************** END CDP JS **************************/


function editPersonalPost(postId, postCreatedAt, postType, acHandler,email,method) {
    // $('#showModalPost .close-dialog').click();
    if(method=="update"){
        var privacyFlag = removePostOfUser(postId, postType, postCreatedAt, email);
        if(privacyFlag) {
            editPost(postId, postCreatedAt, postType, acHandler,email);
        }
    }else{
        editPost(postId, postCreatedAt, postType, acHandler,email);
    }

}

function editPost(postId, postCreatedAt, postType, acHandler,email){
    $.ajax({
        type: 'POST',
        url: '../../views/social/oldPostEdit.php',
        data: {
            postId: postId,
            postType: postType,
            acHandler: acHandler,
            email: email,
            postTime: postCreatedAt
        },
        dataType: 'html',
        success: function (data) {
            $('#editPostDetails .modal-dialog .card .modal-body').html(data);
            previewInitialization(postId);
            if ($('#showModalPost').hasClass('in')) {
                $('#showModalPost').modal('hide').on('hidden.bs.modal', function (event) {
                    event.preventDefault();
                    /* Act on the event */
                    $('#editPostDetails').modal('show');
                });
            } else {
                $('#editPostDetails').modal('show');
            }
        },
        error: function (data) {
            alert('Error in post Update');
        }
    });
}
//for preview checking image extension
function imagePreviewExtensionCheck(formID) {
    var emptyFlag = 1;
    // alert(formID);
    // console.log($(""+formID+" .checkEmpty"));
    var extensions = new Array("jpg", "jpeg", "gif", "png", "bmp");
    $("#" + formID + " .imageCheck").each(function() {
        var image_file = $(this).val();
        var image_length = $(this).val().length;
        var pos = image_file.lastIndexOf('.') + 1;
        var ext = image_file.substring(pos, image_length);
        var final_ext = ext.toLowerCase();
        var flagForImage = false;
        for (i = 0; i < extensions.length; i++) {
            if (extensions[i] == final_ext) {
                emptyFlag = 0;
                flagForImage = true;
            }
        }
    });
    if (emptyFlag == 0) {
        return true;
    } else {

        return false;
    }
}

function previewInitialization(postId) {
    $(".tag-input").focus(function() {
        $(".tag-text").css({ border: "1px solid #719ECE", boxShadow: "0 0 10px #719ECE" });
    });
    $(".tag-input").blur(function() {
        $(".tag-text").css({ border: "1px solid #ccc", boxShadow: "none" });
    });
    //on paste event for tagged keywords
    $(".tag-input").bind("paste", function(e) {
        // access the clipboard using the api
        var pastedData = e.originalEvent.clipboardData.getData('text');
        e.preventDefault();
        var keywords = pastedData.split(/[\s\n]/);
        // var keywords = pastedData.Split('\n');

        for (var i = 0; i < keywords.length; i++) {
            var txt = keywords[i].substring(0, 50); // allowed all characters
            var sameKeywordFlag = 0;
            $(this).parent().find("span").each(function() {
                if ($(this).text().toLowerCase() == "#" + txt.toLowerCase()) {
                    sameKeywordFlag = 1;
                }
            });
            if (sameKeywordFlag == 0) {
                if (txt) $("<span/>", { class: "tagged-keyword-span", text: '#' + txt.toLowerCase(), insertBefore: this });
                displayPreview($(this).parent());
            }

        }
    });

    $('.selectpicker').selectpicker({});
    $('#image-form-upload').trigger("change");
    // for upload image in image form
    $('#image-form-upload').change(function() {
        readURL(this);
        var filename = $('#image-form-upload').val().replace(/C:\\fakepath\\/i, '');
        //  var filename = $('#image-form-upload').val();
        $('#image-input').val(filename);
        if (imagePreviewExtensionCheck("image-post")) {
            $(".img-preview").css("display", "inline-block");
        } else {
            $(".img-preview").css("display", "none");
        }
    });
    $(".preview").on("keyup", function(e) {
        displayPreview($(this));
    });
    //audio preview button click
    $("#audioPreview").click(function() {
        $(".audio-preview-form").slideDown(600);
        setTimeout(function() {
            $(".loader-audio-form").hide();
            $("#audio-iframe").show();
        }, 2000);
        $("#audio-selected-keywords").html($("#audioKeywords").val()).addClass("text-blue");
        $("#audio-preview-description").html($("[data-id='audio-preview-description']").val());
        if (postId != "") {
            $(".tag-text").css("pointer-events", "none");
            $("#audio-preview-wrapper").html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + $("[data-id='audio-input']").val() + '"></iframe>');
        } else {
            var urlLink = $("[data-id='audio-input']").val();
            if (urlLink.startsWith("https://soundcloud.com/")) {
                $("#audio-preview-wrapper").html(urlLink).addClass("text-blue")
            } else {
                $("#audio-preview-wrapper").html(urlLink);
            }
        }
        $(".audio-preview-form").slideDown(600);
    });
    //image preview button click
    $("#imagePreview").click(function() {
        $("#image-selected-keywords").html($("#imageKeywords").val()).addClass("text-blue");
        $("#image-preview-description").html($("[data-id='image-preview-description']").val());
        $(".img-preview").show();
        $(".img-preview").attr("src", $("#image-feature-preview").attr("src"));
        if (postId != "") {
            $(".tag-text").css("pointer-events", "none");
        }
        $(".image-preview-form").slideDown(600);
    });
    //video preview button click
    $("#videoPreview").click(function() {
        $("#video-selected-keywords").html($("#videoKeywords").val()).addClass("text-blue");
        $("#video-preview-description").html($("[data-id='video-preview-description']").val());
        if (postId != "") {
            $(".tag-text").css("pointer-events", "none");
            $("#video-input").html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + $("[data-id='video-input']").val() + '"></iframe>');
        }
        else {
            var urlLink = $("[data-id='video-input']").val();
            if (urlLink.indexOf("https://www.youtube.com/") != -1) {
                urlLink = urlLink.replace("watch?v=", "embed/");
                $("#video-input").html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + urlLink + '"></iframe>');
                $("#video_link").css("border", "1px solid #ccc");

            }
            else if (urlLink.indexOf("https://vimeo.com/") != -1) {
                urlLink = urlLink.replace("vimeo.com/", "player.vimeo.com/video/");
                $("#video-input").html('<iframe width="100%" height="200" scrolling="no" frameborder="no" style="pointer-events:auto;" src="' + urlLink + '"></iframe>');
                $("#video_link").css("border", "1px solid #ccc");

            }
            else {
                $("#video-input").html("<span style='color:red;'>Invalid Link</span>");
                $("#video_link").css('border','1px solid #a94442');
            }
        }
        $(".video-preview-form").slideDown(600);
    });
    //blog preview button click
    $("#blogPreview").click(function() {
        $("#blog-selected-keywords").html($("#blogKeywords").val()).addClass("text-blue");
        $("#blog-title").html($("[data-id='blog-title']").val());
        $("#blog-preview-description").html($("#editor").html());
        $(".img-preview").show();
        $(".img-preview").attr("src", $("#image-feature-preview").attr("src"));
        if (postId != "") {
            $(".tag-text").css("pointer-events", "none");
        }
        $(".blog-preview-form").slideDown(600);
    });
    //hide all preview forms preview button click
    $(".hide-preview").click(function() {
        $("#video-input").html('');
        $(".audio-preview-form").slideUp(600);
        $(".image-preview-form").slideUp(600);
        $(".video-preview-form").slideUp(600);
        $(".loader-audio-form").show();
        $("#audio-iframe").hide();
    });

    /*
     For previewing image to user in preview form
     */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {

                if ($("#image-feature-preview").width() > 50 && $("#image-feature-preview").height() > 50) {
                    $('.img-preview').attr('src', e.target.result);
                } else {
                    // $(".img-preview").attr("src", "null");
                    $(".img-preview").attr("src", "fsd");
                    $(".img-preview").attr("alt", "Invalid Image");
                }
                var img = $(".img-preview");
                //console.log("preview : " + e.target.result);
                if (img.width() >= 460 && img.height() >= 230) {} else {
                    //   alert("Image resolution is too small to upload!!!");
                }
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    //For "Add keywords" tags
    // ::: TAGS BOX
    $(".tag-text input").on({
        keyup: function(ev) {
            // if: comma|enter (delimit more keyCodes with | pipe)
            if (/(13|32)/.test(ev.which)) {
                convertToTag(this, "tagged-keyword-span");
            }
        },
        focusout:function (ev) {
            convertToTag(ev.target, "tagged-keyword-span");
        },
        paste: function() {
            //alert("hellqo");
        }
    });
    $('.tag-text').on('click', 'span', function() {

        var dataID = $(this).parent().attr("data-id");
        $(this).remove(); //    if(confirm("Remove "+ $(this).text() +"?"))
        var keywordLength = $("#tags[data-id=" + $(this).attr("data-id") + "]").find("span").length;
        $("#" + dataID).html("");
        var countKeyword = 1;
        $("#tags[data-id=" + dataID + "]").find("span").each(function() {
            if (countKeyword <= 3) {
                $("#" + dataID).append("<a>" + $(this).text() + "</a>&nbsp&nbsp");
            } else {
                $("#" + dataID).append("<a style='color:red;'>" + $(this).text() + "</a>&nbsp&nbsp");
            }
            countKeyword++;
        });

        if ($("#tags").find("span").length > 0 && $("#tags").find("span").length < 4) {
            $("#tags").css("border", "1px solid #ccc");
        }
    });
}

/* To show preview of uploaded Image */
function imagePostPreview(id, pre_id, nextSlider, uploadDir) {
    var files = $(id)[0].files;
    var img = new Image();

    if (!files.length || !window.FileReader) return;
    if (/^image/.test(files[0].type)) {
        var reader = new FileReader(); // instance of the FileReader
        reader.readAsDataURL(files[0]); // read the local file
        var fileType = files[0].type;
        var fileName = files[0].name;
        $(pre_id).show();
        // $('#slider-input-'+nextSlider).show();
        reader.onloadend = function() {
            img.onload = function() {
                if (id == "#feature-image") {
                    if ((img.height < 460 || img.height > 480) && (img.width < 1440 || img.width > 1980)) {
                        $(id).val('');
                        $(".modal-err").text('Image should be greater than 1440 * 460 pixels!!!');
                        $('#my_image').show();
                        $(pre_id).hide();
                    } else {
                        $('#my_image').hide();
                        $(".modal-err").text('');
                    }
                } else {
                    if ((img.height < 40 || img.height > 100) && (img.width < 40 || img.width > 100)) {
                        $(id).val('');
                        $(".modal-err").text('Image should be greater than 40 * 40 and smaller than 100*100 pixels!!!');
                        $('#my_icon').show();
                        $(pre_id).hide();
                    } else {
                        $(".modal-err").text('');
                        $('#my_icon').hide();
                    }
                }
            }
            $(pre_id).attr('src', this.result);
            $('.img-preview').attr('src', this.result);
        }

    }
}

//--------------------------------------Followers.php Js Start-------------------------------------


// //to follow and unfollow User and create Json
function ajaxAppFollowEvent(handler, email) {
    var buttonText = $("#" + handler).val();
    var followingCount = $('#followingCount').text();
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        $.ajax({
            url: '../../controllers/social/setJsonController.php',
            type: 'POST',
            dataType: 'json',
            data: ({ user_email: email }),
            success: function(data) {
                if (data.errCode == -1) {
                    if (buttonText == 'Unfollow') {
                        $("#" + handler).attr('value', "Follow");
                        $(".otherFollowDropDown").html("<i class='fa fa-user'></i>&nbspFollow User");
                        $('#followingCount').html(followingCount - 1);
                        $("#" + handler).removeClass('btn-social-dark');
                        $("#" + handler).addClass('btn-social');
                    } else {
                        $("#" + handler).attr('value', "Unfollow");
                        $(".otherFollowDropDown").html("<i class='fa fa-user-times'></i>&nbspUnfollow User");
                        $('#followingCount').html(Number(followingCount) + 1);
                        $("#" + handler).addClass('btn-social-dark');
                        $("#" + handler).removeClass('btn-social');
                    }
                    showToast("success", 'User ' + buttonText + 'ed' + ' Successfully');
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else {
                    showToast("failed", 'User ' + buttonText + ' Failed');
                }
            },
            beforeSend: function() {
                $(".follow-unfollow-button").prop("disabled", true);
            },
            complete: function() {
                $(".follow-unfollow-button").prop("disabled", false);
            },
            error: function(data) {
                showToast("failed", 'Error in Connection');
            }
        });

    }

}


// to load followings list
$('#followings').on('click', function() {
    $('#followersType').attr('type', 'followings');
    $('#followingDataAppend').attr('pageScrollEndFlag', 'true');
    $('#followingDataAppend').attr('followingDataAppendCount', '');
    $('#followingDataAppend').attr('followingDataLoopCount', '');
    $('#followingdivAppendId').text('1');
    $('#followerdivAppendId').text('1');
    $('#followers').removeClass('active');
    $('#block').removeClass('active');
    $('#followings').addClass('active');
    $('#suggestedUserList').removeClass('active');
    loadFollowings('followings');
});

// To load Followers List
$('#followers').on('click', function() {
    $('#followersType').attr('type', 'followers');
    $('#followerDataAppend').attr('pageScrollEndFlag', 'true');
    $('#followerDataAppend').attr('followingDataAppendCount', '');
    $('#followerDataAppend').attr('followingDataLoopCount', '');
    $('#followingdivAppendId').text('1');
    $('#followerdivAppendId').text('1');
    $('#followers').addClass('active');
    $('#block').removeClass('active');
    $('#followings').removeClass('active');
    $('#suggestedUserList').removeClass('active');
    loadFollowers('followers');
});


// To load Followers List
$('#block').on('click', function() {
    $('#followersType').attr('type', 'blocked');
    $('#followerDataAppend').attr('pageScrollEndFlag', 'true');
    $('#followerDataAppend').attr('followingDataAppendCount', '');
    $('#followerDataAppend').attr('followingDataLoopCount', '');
    $('#followingdivAppendId').text('1');
    $('#followerdivAppendId').text('1');
    $('#block').addClass('active');
    $('#followers').removeClass('active');
    $('#followings').removeClass('active');
    $('#suggestedUserList').removeClass('active');
    loadBlocked('blocked');
});

// To load suggested List
$('#suggestedUserList').on('click', function() {
    $('#followersType').attr('type', 'suggested');
    $('#followerDataAppend').attr('pageScrollEndFlag', 'true');
    $('#followerDataAppend').attr('followingDataAppendCount', '');
    $('#followerDataAppend').attr('followingDataLoopCount', '');
    $('#followingdivAppendId').text('1');
    $('#followerdivAppendId').text('1');
    $('#suggestedUserList').addClass('active');
    $('#followers').removeClass('active');
    $('#followings').removeClass('active');
    $('#block').removeClass('active');
    loadSuggestedList("suggested");
});


// To append Suggested's Data
function loadSuggestedList(type) {
    var jsonFileNo = $('#blockDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followerDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followerDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#blockdivAppendId').text();
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/suggestedList.php',
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }
                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followerDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followerDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followerDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followerdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
                $('#blocked-data').hide();
                $('#suggested-user-details').hide();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}

// To append Blocked's Data
function loadBlocked(type) {
    var jsonFileNo = $('#followerDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followerDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followerDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followerdivAppendId').text();
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/blockList.php',
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }
                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followerDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followerDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followerDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followerdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
                $('#blocked-data').hide();
                $('#suggested-user-details').show();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}
// To append follower's Data
function loadFollowers(type) {
    var jsonFileNo = $('#followerDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followerDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followerDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followerdivAppendId').text();
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/followingsList.php',
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }
                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followerDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followerDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followerDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followerdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
                $('#blocked-data').hide();
                $('#suggested-user-details').show();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}

// To Append Followings Data
function loadFollowings(type) {
    var jsonFileNo = $('#followingDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followingDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followingDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followingdivAppendId').text();
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/followingsList.php',
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }
                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followingDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followingDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followingDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followingdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
                $('#blocked-data').hide();
                $('#suggested-user-details').show();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}

// To Append Followings Data
function loadFollowingsOfOther(type, email) {

    var jsonFileNo = $('#followingDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followingDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followingDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followingdivAppendId').text();
    // if (scrollAllow == 'false') {
    //     scrollAllow = 'true';
    // }
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/followingsListOfOther.php?email=' + email + '&type=' + type,
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }
                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followingDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followingDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followingDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followingdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            complete: function(postdata) {
                $('#followingAjaxCall').attr('ajxCallFlag','true');
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}

//--------------------------------------Followers.php Js End-------------------------------------

//----------------------------Block User Account From TimeLine ------------------------------------
function blockUserAcc(elem, emailId,postId,type,postTime) {
    $.ajax({
        type: 'POST',
        url: '../../controllers/social/hidePostOfUser.php',
        dataType: 'json',
        data: { 'postId': postId, 'type': type, 'postTime': postTime, 'email': emailId },
        success: function() {
            $.ajax({
                url: '../../controllers/social/setBlockController.php',
                type: 'POST',
                dataType: 'json',
                data: ({ user_email: emailId }),
                success: function(data) {
                    if (data.errCode == -1) {
                        window.location = rootUrl;
                    } else if (data.errCode == 100) {
                        window.location.href = rootUrl;
                    } else {
                        showToast("failed", 'User action Failed');
                    }
                },
                error: function() {
                    showToast("failed", 'Error in Connection');
                }
            });
        },error: function() {
            showToast("failed", 'Error in Connection');
        }
    });
}

//----------------------------UnBlock User Account From Blocked Listings  ------------------------------------
function ajaxAppBlockedEvent(handler, email) {
    var buttonText = $("#" + handler).val();
    $.ajax({
        url: '../../controllers/social/setBlockController.php',
        type: 'POST',
        dataType: 'json',
        data: ({ user_email: email }),
        success: function(data) {
            if (data.errCode == -1) {
                if (buttonText == 'Unblock') {
                    $("#" + handler).attr('value', "Block");
                    $("#" + handler).removeClass('btn-social-dark');
                    $("#" + handler).addClass('btn-social');
                } else {
                    $("#" + handler).attr('value', "Unblock");
                    $("#" + handler).addClass('btn-social-dark');
                    $("#" + handler).removeClass('btn-social');
                }
                showToast("success", 'User ' + buttonText + ' Successfully');
            } else {
                showToast("failed", 'User ' + buttonText + ' Failed');
            }
        },
        beforeSend: function() {
            $(".follow-unfollow-button").prop("disabled", true);
        },
        complete: function() {
            $(".follow-unfollow-button").prop("disabled", false);
        },
        error: function(data) {
            showToast("failed", 'Error in Connection');
        }
    });
}

//----------------------------Follow User Account ------------------------------------
function followUserAcc(elem, emailId) {
    $.ajax({
        url: '../../controllers/social/setJsonController.php',
        type: 'POST',
        dataType: 'json',
        data: ({ user_email: emailId }),
        success: function(data) {
            if (data.errCode == -1) {
                console.log($.trim($(elem).text()));
                if ($.trim($(elem).text()) == "Follow User") {
                    $(elem).html("<i class='fa fa-user-times'></i>&nbspUnfollow User");
                    showToast("success", 'User Followed Successfully');
                    $(".otherFollowBtn").val("Unfollow");
                    $(".otherFollowBtn").addClass("btn-social-dark").removeClass("btn-social");
                } else {
                    $(elem).html("<i class='fa fa-user'></i>&nbspFollow User");
                    showToast("success", 'User Unfollowed Successfully');
                    $(".otherFollowBtn").val("Follow");
                    $(".otherFollowBtn").addClass("btn-social").removeClass("btn-social-dark");
                }

            } else if (data.errCode == 100) {
                window.location.href = rootUrl;
            } else {
                showToast("failed", 'User action Failed');
            }
        },
        error: function(data) {
            showToast("failed", 'Error in Connection');
        }
    });
}


//To get proper time or date of created at
function dataCreatedAt(createdAt) {
    var returnText;
    $.ajax({
        type: 'POST',
        dataType: 'text',
        async: false,
        data: {
            createdAt: createdAt
        },
        url: rootUrl + 'views/social/getDateFormate.php',
        success: function(data) {
            returnText = data;
        },
        error: function(data) {
            console.log('error in created at');
            returnText = "ERROR";
        }
    });
    return returnText;
}



function LikeCountModal(postId, createdAt, likelowerLimit, likeupperLimit) {
    $.ajax({
        type: 'POST',
        dataType: 'html',
        async: false,
        data: {
            post_id: postId,
            created_at: createdAt,
            likelowerLimit: likelowerLimit,
            likeupperLimit: likeupperLimit
        },
        url: '../../views/social/showLikeModal.php',
        success: function(data) {
            if (likelowerLimit == 0) {
                $('#myModal_like #likeUserList').html(data);
            } else {
                $('#myModal_like #likeUserList').append(data);
            }
            var limitVar = parseInt(likelowerLimit) - (likeupperLimit);
            $('#myModal_like #likeLoadMore-' + limitVar).hide();
            $('#myModal_like ').modal('show');
        },
        error: function(ts) {
            alert(ts.responseText);
        }
    });
}



//--------------------------      Start BookMark Post JS    -------------------------------//
function bookmarkAccountOfUser(post_type, post_id, postTime,email) {
    if ($('#bookmark' + post_id).hasClass("fa-star-o")) {
        $('#bookmark' + post_id).removeClass("fa-star-o").addClass("fa-star text-yellow");
    } else {
        $('#bookmark' + post_id).removeClass("fa-star text-yellow").addClass("fa-star-o");
    }
    if ($('.bookmarkpop' + post_id).hasClass("fa-star-o")) {
        $('.bookmarkpop' + post_id).removeClass("fa-star-o").addClass("fa-star text-yellow");
    } else {
        $('.bookmarkpop' + post_id).removeClass("fa-star text-yellow").addClass("fa-star-o");
    }
    $.ajax({
        type: 'POST',
        url: rootUrl + 'controllers/social/setBookmarkController.php',
        dataType: 'json',
        data: { 'post_id': post_id, 'type': post_type, 'postTime': postTime },
        success: function(data) {
            if (data.errCode == -1) {
                $message = 'Successfully bookmark post';
                //console.log($('#'+id+''+post_id).hasClass("fa-star-o"));

            } else {
                if ($('#bookmark' + post_id).hasClass("fa-star-o")) {
                    $('#bookmark' + post_id).removeClass("fa-star-o").addClass("fa-star text-yellow");
                } else {
                    $('#bookmark' + post_id).removeClass("fa-star text-yellow").addClass("fa-star-o");
                }
                if ($('#bookmarkpop' + post_id).hasClass("fa-star-o")) {
                    $('#bookmarkpop' + post_id).removeClass("fa-star-o").addClass("fa-star text-yellow");
                } else {
                    $('#bookmarkpop' + post_id).removeClass("fa-star text-yellow").addClass("fa-star-o");
                }
            }
        }
    });
}

//--------------------------      END Like Post JS      -------------------------------//

/* --------------------------  Comment JQuery start here -------------------------- */

// to Set comment on post in node.
function createCommentOnPost(postId) {
    var commentText = $('#user-comment-text' + postId).val();
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var createrEmail  = $('#comment-basic-details' + postId).attr('post-creater-email');
    var callAjaxFlag = $('#comment-basic-details' + postId).attr('call-ajax-flag');
    var commentValid = "false";
    var createdDataTime;
    var commentData;

    var blockCheck = getBlockedUser(createrEmail);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, "", postCreatedAt, createrEmail);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, "", postCreatedAt, createrEmail);
            if (hideCheck) {
                if (isNotEmpty("comment-form" + postId)) {
                    commentValid = "true";
                }
                if (callAjaxFlag == "true" & commentValid == 'true') {

                    var createrEmail2 = $('#comment-basic-details' + postId).attr('post-creater-email');
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        async: 'false',
                        data: {
                            commentText: encodeURIComponent(commentText),
                            postId: encodeURI(postId),
                            postCreatedAt: encodeURI(postCreatedAt),
                            PostCreatorEmail: encodeURI(createrEmail2)
                        },
                        url: '../../controllers/social/setCommentOnPost.php',
                        success: function (data) {
                            if (data.errCode == -1) {
                                commentData = decodeHTMLEntities(data.errMsg.comment);
                                createdDataTime = dataCreatedAt(data.errMsg.created_at);
                                $('#user-comment-text' + postId).val('');
                                $('.comment-count' + postId).text(data.errMsg.comment_count);
                                $('#user-comment-title' + postId).text(commentData);
                                $('#comment-created-time' + postId).text(createdDataTime);
                                commentAppendData('function', '', '3', '', postId);
                            } else if (data.errCode == 100) {
                                window.location.href = rootUrl;
                            } else if (data.errCode == 4) {
                                if (!$('#postDeletedModal').hasClass('in')) {
                                    postDeletedModal("This Post No longer Exit");
                                }
                                postLoadAsPerUrl();
                            } else {
                                showToast("failed", 'Failed To Add Comment');
                            }
                        },
                        beforeSend: function (data) {
                            $('#comment-basic-details' + postId).attr('call-ajax-flag', 'false');
                        },
                        complete: function (data) {
                            $('#comment-basic-details' + postId).attr('call-ajax-flag', 'true');
                        },
                        error: function (data) {
                            showToast("failed", 'Error In Comment Connection');
                        }
                    });
                }
            }
        }
    }

}



// to sec comment clicked from modal popup
function createCommentOnPostFromModal(postId) {
    var commentText = $('#showModalPost #comment-form' + postId + ' #user-comment-text' + postId).val();
    var postCreatedAt = $('#showModalPost #comment-basic-details' + postId).attr('post-created-at');
    var createrEmail  = $('#comment-basic-details' + postId).attr('post-creater-email');
    var callAjaxFlag = $('#comment-basic-details' + postId).attr('call-ajax-flag');
    var callAjaxFlag = $('#showModalPost #comment-basic-details' + postId).attr('call-ajax-flag');
    var commentValid = "false";
    var createdDataTime;
    var commentData;

    var blockCheck = getBlockedUser(createrEmail);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, "", postCreatedAt, createrEmail);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, "", postCreatedAt, createrEmail);
            if (hideCheck) {
                if (modalIsNotEmpty("#showModalPost #comment-form" + postId)) {
                    commentValid = "true";
                }
                if (callAjaxFlag == "true" & commentValid == 'true') {
                    var createrEmail2 = $('#showModalPost #comment-basic-details' + postId).attr('post-creater-email');
                    $.ajax({
                        type: 'POST',
                        dataType: 'JSON',
                        async: 'false',
                        data: {
                            commentText: encodeURIComponent(commentText),
                            postId: encodeURI(postId),
                            postCreatedAt: encodeURI(postCreatedAt),
                            PostCreatorEmail: encodeURI(createrEmail2)
                        },
                        url: '../../controllers/social/setCommentOnPost.php',
                        success: function (data) {
                            if (data.errCode == -1) {
                                commentData = decodeHTMLEntities(data.errMsg.comment);
                                createdDataTime = dataCreatedAt(data.errMsg.created_at);
                                $('#showModalPost #comment-form' + postId + ' #user-comment-text' + postId).val('');
                                $('#showModalPost .comment-count' + postId).text(data.errMsg.comment_count);
                                $('.comment-count' + postId).text(data.errMsg.comment_count);
                                // $('#showModalPost #comment-form'+postId+' #user-comment-title'+postId).text(commentData);
                                // $('#showModalPost #comment-form'+postId+' #comment-created-time'+postId).text(createdDataTime);
                                // $('.myComment1').show();
                                commentAppendData('function', '', '3', '', postId);
                                // showToast("success",'Successfully Added Comment');
                            } else if (data.errCode == 100) {
                                window.location.href = rootUrl;
                            } else if (data.errCode == 4) {
                                if (!$('#postDeletedModal').hasClass('in')) {
                                    postDeletedModal("This Post No longer Exit");
                                }
                                postLoadAsPerUrl();
                            } else {
                                showToast("failed", 'Failed To Add Comment');
                            }
                        },
                        beforeSend: function (data) {
                            $('#showModalPost #comment-basic-details' + postId).attr('call-ajax-flag', 'false');
                        },
                        complete: function (data) {
                            $('#showModalPost #comment-basic-details' + postId).attr('call-ajax-flag', 'true');
                        },
                        error: function (data) {
                            showToast("failed", 'Error In Comment Connection');
                        }
                    });
                }
            }
        }
    }else {
        showToast("failed", 'Something Went Wrong!!');
        $('#showModalPost').modal('hide');
        postLoadAsPerUrl();
    }
}

// To convert encoded comment messege into normal comment messege.
function decodeHTMLEntities(text) {
    var entities = [
        ['amp', '&'],
        ['apos', '\''],
        ['#x27', '\''],
        ['#x2F', '/'],
        ['#39', '\''],
        ['#47', '/'],
        ['lt', '<'],
        ['gt', '>'],
        ['nbsp', ' '],
        ['quot', '"'],
        ['#039', "'"]
    ];
    for (var i = 0, max = entities.length; i < max; ++i)
        text = text.replace(new RegExp('&' + entities[i][0] + ';', 'g'), entities[i][1]);
    return text;
}
// to get Comment list and append to post
function commentAppendData(callType, lastCommentTime, limit, commentId, postId) {
    $('#comment-text-add' + postId).show();
    $('#showModalPost #comment-text-add' + postId).show();
    var resultdata;
    if (postId == '') {
        var postId = $('.comment-special-class').attr('post-id');
    }
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callAjaxFlag = $('#comment-basic-details' + postId).attr('call-ajax-get-flag');
    var commentmoreLoad;
    if (callAjaxFlag == 'true') {
        $.ajax({
            type: 'POST',
            dataType: 'text',
            async: false,
            data: {
                postId: encodeURI(postId),
                postCreatedAt: encodeURI(postCreatedAt),
                lastCommentTime: encodeURI(lastCommentTime),
                limit: encodeURI(limit),
                commentId: encodeURI(commentId)
            },
            url: '../../views/social/getCommentData.php',
            success: function(data) {
                resultdata = data;
                if (callType == 'onReload' || callType == 'function') {
                    $('#comment-append' + postId).html(resultdata);
                    $('#showModalPost .modal-dialog .card .modal-body-social-user-image #comment-append' + postId).html(resultdata);
                } else {
                    $('#comment-append' + postId).append(resultdata);
                    $('#showModalPost .modal-dialog .card .modal-body-social-user-image #comment-append' + postId).append(resultdata);
                }
                commentmoreLoad = $('#get-more-comment' + lastCommentTime + postId).text();
                commentLoadFlag = $('#get-more-comment' + lastCommentTime + postId).attr('commentLoadClick');
                $('#loadMoreCommentPostPage' + postId).attr('onclick', $('#get-more-comment' + lastCommentTime + postId).text());
                $('#showModalPost #loadMoreCommentPostPage' + postId).attr('onclick', $('#get-more-comment' + lastCommentTime + postId).text());
                if (commentLoadFlag == 'true') {
                    $('#commentLoadMoreButton' + postId).show();
                    $('#showModalPost #commentLoadMoreButton' + postId).show();
                } else {
                    $('#commentLoadMoreButton' + postId).hide();
                    $('#showModalPost #commentLoadMoreButton' + postId).hide();
                }
                var commentCountAppend = $('#get-more-comment' + lastCommentTime + postId).attr('commentTotalCount');
                if (commentCountAppend != '') {
                    $('.comment-count' + postId).text(commentCountAppend);
                    $('#showModalPost .comment-count' + postId).text(commentCountAppend);
                }

                $('#remainingLoadCount' + postId).text($('#get-more-comment' + lastCommentTime + postId).attr('remainingCount'));
                $('#showModalPost #remainingLoadCount' + postId).text($('#get-more-comment' + lastCommentTime + postId).attr('remainingCount'));
                var callEnterEvent = $('#comment-hidden-enter-event-' + postId).attr('execute-enter-event-main-page');
                if (callEnterEvent == 'true') {
                    $('#user-comment-text' + postId).keypress(function(e) {
                        if ($(this).val().length >= 1) {
                            mentionOnKeyup($(this).val());
                        }
                        if (e.which == 13) {
                            createCommentOnPost(postId);
                        }
                    });
                    $('#comment-hidden-enter-event-' + postId).attr('execute-enter-event-main-page', 'false');
                    if (socialFileUrl != 'social/social_blog' || socialFileUrl != 'social/socialPostDetails') {
                        $('#user-comment-text' + postId).focus();
                        $('#showModalPost #user-comment-text' + postId).focus();
                    }
                }
            },
            beforeSend: function(data) {
                $('#comment-basic-details' + postId).attr('call-ajax-get-flag', 'false');
            },
            complete: function(data) {
                $('#comment-basic-details' + postId).attr('call-ajax-get-flag', 'true');
            },
            error: function(data) {
                showToast("failed", 'Error In Comment Connection');
            }
        });
    }
    if (callType == 'function') {
        return;
    }
}


// function to call update and edit comment
function editDeleteComment(type, id, postId) {
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callEditDeletFlag = $('#comment-basic-details' + postId).attr('call-ajax-edit-flag');
    if (type == 'update') {
        $("#comment-append" + postId).each(function() {
            $('.comment-editing-post-' + postId).show();
            $('.comment-editing-each-post-' + postId).hide();
            $('.comment-editing-action-post-' + postId).show();

            $('#showModalPost .comment-editing-post-' + postId).show();
            $('#showModalPost .comment-editing-each-post-' + postId).hide();
            $('#showModalPost .comment-editing-action-post-' + postId).show();
        });
        var EditingComment = $('#Comment-og-text' + id).text();

        // #showModalPost
        $('#comment-edit-display-' + id).hide();
        $('#comment-edit-display-action-' + id).hide();
        $('#comment-text-add' + postId).hide();

        $('#showModalPost #comment-edit-display-' + id).hide();
        $('#showModalPost #comment-edit-display-action-' + id).hide();
        $('#showModalPost #comment-text-add' + postId).hide();
        var newEditHtml = '<div id = "comment-edit-form' + id + '" class="form-group"><input type = "text" id = "user-comment-text-edit' + id + '" class = "form-control checkEmpty can-mention-user inputor"  value = "' + EditingComment + '" comment-id="' + id + '"  maxlength = "600"></div>';
        $('#comment-edit-' + id).html(newEditHtml);
        $('#comment-edit-' + id).show();

        $('#showModalPost #comment-edit-' + id).html(newEditHtml);
        $('#showModalPost #comment-edit-' + id).show();

        // to get new comment text for editing
        $('.comment-editing').on('keyup', '#user-comment-text-edit' + id, function(event) {
            if (event.which == 13) {
                submitEditComment(postId, id);
            }
        });

        $('#showModalPost .comment-editing').on('keyup', '#user-comment-text-edit' + id, function(event) {
            if (event.which == 13) {
                var elem = this;
                submitEditCommentModal(postId, id, elem);
            }
        });
    }

    //to delete comment
    if (type == 'delete') {
        var UserEmail = $('#commented-list-'+id).attr('comment-user-email');
        // console.log(UserEmail);
        if (socialFileUrl == 'social/myprofile') {
            UserEmail = $('.commented-list-'+id).attr('comment-user-email');
        }
        // console.log(UserEmail);
        if (UserEmail) {
            var blockCheck = getBlockedUser(UserEmail);
            if (blockCheck) {
                var commentDeleteflag = commentEditDeleteajaxFunc('', id, 'delete', postId, postCreatedAt);
                // console.log(commentDeleteflag);
                if (commentDeleteflag.errCode == -1) {
                    $('#commented-list-' + id).slideUp(200);
                    $('#showModalPost #commented-list-' + id).slideUp(200);
                    // var deleteCommentNo = $('.comment-count'+postId).text();
                    // if (parseInt(deleteCommentNo) != 0) {
                    $('.comment-count' + postId).text(commentDeleteflag.errMsg.comment_count);
                    commentAppendData('function', '', '3', '', postId);

                    // }
                    if (socialFileUrl == 'social/myprofile') {
                        $('#comment-data-delete-'+postId).slideUp(200);
                        $("#commentActivity").click();
                    }
                    // showToast("success",'Successfully Deleted Comment');
                } else if (commentDeleteflag.errCode == 4) {
                    postDeletedModal('The link you followed may be broken, or the page may have been removed');
                    postLoadAsPerUrl();
                    commentDivShow(postId);
                }
                // else if(commentDeleteflag.errCode == 4) {
                //   postDeletedModal('This Post No longer Exist');
                //   postLoadAsPerUrl();
                // }
                else {
                    showToast("failed", 'failed to Delete Comment');
                }
            } else {
                showToast("failed", 'You Have Blocked This User');
                commentAppendData('function', '', '3', '', postId);
            }
        } else {
            showToast("failed", 'Something Went Wrong...!');
        }
        //$('.modal').modal('hide');
    }
    return;
}

// function to edit comment
function submitEditComment(postId, comid) {
    var commentText = $('#user-comment-text-edit' + comid).val();
    // console.log(commentText);
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var commentValid = "false";
    var id = $('#user-comment-text-edit' + comid).attr('comment-id');
    if (isNotEmpty("comment-edit-form" + comid)) {
        commentValid = "true";
    }
    if (commentValid == 'true') {
        var commentEditflag = commentEditDeleteajaxFunc(commentText, id, 'update', postId, postCreatedAt);
        if (commentEditflag.errCode == -1) {
            commentAppendData('function', '', '3', '', postId);
            $('#comment-text-add' + postId).show();
        }
        // else if(commentEditflag.errCode == 14) {
        //   postDeletedModal('This Comment No longer Exist');
        //   commentAppendData('function','','3' , '', postId);
        // }
        else if (commentEditflag.errCode == 4) {
            postDeletedModal('The link you followed may be broken, or the page may have been removed');
            postLoadAsPerUrl();
            commentDivShow(postId);
        } else {
            showToast("failed", 'failed to Edit Comment');
        }
    }
    return true;
}

// function to edit comment from Post Modal
function submitEditCommentModal(postId, comid, elem) {
    var commentText = $(elem).val();
    var postCreatedAt = $('#showModalPost #comment-basic-details' + postId).attr('post-created-at');
    var commentValid = "false";
    var id = $(elem).attr('comment-id');
    var createdDataTime;
    var commentData;
    if (modalIsNotEmpty("#showModalPost #comment-edit-form" + comid)) {
        commentValid = "true";
    }
    if (commentValid == 'true') {
        var commentEditflag = commentEditDeleteajaxFunc(commentText, id, 'update', postId, postCreatedAt);
        if (commentEditflag.errCode == -1) {
            commentAppendData('function', '', '3', '', postId);
            $('#showModalPost #comment-text-add' + postId).show();
        }
        // else if(commentEditflag.errCode == 14) {
        //   postDeletedModal('This Comment No longer Exist');
        //   commentAppendData('function','','3' , '', postId);
        // }
        else if (commentEditflag.errCode == 4) {
            postDeletedModal('The link you followed may be broken, or the page may have been removed');
            postLoadAsPerUrl();
            commentDivShow(postId);
        } else {
            showToast("failed", 'failed to Edit Comment');
        }
    }
    return true;
}

//function to call ajax function to edit comment
function commentEditDeleteajaxFunc(commentText, id, type, postId, postCreatedAt) {
    var editAjaxFlag = false;
    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: {
            commentText: encodeURIComponent(commentText),
            commentId: encodeURI(id),
            type: encodeURI(type),
            postId: encodeURI(postId),
            postCreatedAt: encodeURI(postCreatedAt),
        },
        url: rootUrl + 'controllers/social/editDeleteComment.php',
        success: function(data) {
            editAjaxFlag = data;
        },
        error: function(data) {
            editAjaxFlag = data;
        }
    });
    return editAjaxFlag;
}

// to Create reply pn comment
function createReplyOnReply(id) {
    // var replyHtml = '<div class="col-xs-11"><div id="comment-form" class="form-group"><input type="text" id="user-comment-reply-text" class="form-control checkEmpty reply-input-box" placeholder="Write Reply Here"></div></div>';
    // $('#comment-reply-div-'+id).append(replyHtml);
    // $('#comment-reply-div-'+id).show();
    // $('.comment-reply-create').on('keyup','#user-comment-reply-text',function(event){
    //   // alert('enter');
    //   if (event.which == 13) {
    //     // alert('123');
    //     setReplyOnComment(id,'reply', postId);
    //   }
    // });
    $('#user-comment-reply-text-' + id).focus();
}

function setReplyOnComment(id, replycall, postId) {
    var commentText = $('#user-comment-reply-text-' + id).val();
    // var postId        = $('#comment-basic-details').attr('post-id');
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callAjaxFlag = $('#comment-basic-details' + postId).attr('call-reply-ajax-flag');
    var commentValid = "false";
    var createdDataTime;
    var commentData;
    if (isNotEmpty("comment-reply-div-" + id)) {
        commentValid = "true";
    }
    if (replycall == 'reply') {
        commentId = id;
        replyId = '';
    } else {
        commentId = '';
        replyId = id;
    }
    if (callAjaxFlag == "true" & commentValid == 'true') {
        var createrEmail2  = $('#comment-basic-details' + postId).attr('post-creater-email');
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            async: 'false',
            data: {
                commentText: encodeURIComponent(commentText),
                postId: encodeURI(postId),
                postCreatedAt: encodeURI(postCreatedAt),
                commentId: encodeURI(commentId),
                replyId: encodeURI(replyId),
                PostCreatorEmail: encodeURI(createrEmail2)
            },
            url: '../../controllers/social/setCommentOnPost.php',
            success: function(data) {
                // console.log(data);
                if (data.errCode == -1) {
                    // console.log("Rplydata : "+data);
                    // commentData     = decodeHTMLEntities(data.errMsg.comment);
                    // createdDataTime = dataCreatedAt(data.errMsg.created_at);
                    // $('#user-comment-text').val('');
                    // $('.comment-count').text(data.errMsg.comment_count);
                    // $('#user-comment-title').text(commentData);
                    // $('#comment-created-time').text(createdDataTime);
                    // // $('.myComment1').show();
                    // commentAppendData('function','','3', '');
                    $('.comment-count' + postId).text(data.errMsg.comment_count);
                    $('#reply-count-' + commentId).text(" (" + data.errMsg.replyCount + ") ");
                    // console.log(parseInt(commentReplyCount) + 1);
                    getCommentReplies('function', '', '3', id, '2', postId);
                    $('#user-comment-reply-text-' + id).val('');
                    // showToast("success",'Successfully Added Comment');
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else if (data.errCode == 14) {
                    if (!$('#postDeletedModal').hasClass('in')) {
                        postDeletedModal('This Comment No longer Exist');
                    }
                    commentAppendData('function', '', '3', '', postId);
                    getCommentReplies('function', '', '3', id, '2', postId);
                } else if (data.errCode == 4) {
                    if (!$('#postDeletedModal').hasClass('in')) {
                        postDeletedModal("This Post No longer Exit");
                    }
                    postLoadAsPerUrl();
                } else {
                    showToast("failed", 'failed to Add Reply On Comment');
                }
            },
            beforeSend: function(data) {
                $('#comment-basic-details').attr('call-reply-ajax-flag', 'false');
            },
            complete: function(data) {
                $('#comment-basic-details').attr('call-reply-ajax-flag', 'true');
            },
            error: function(data) {
                showToast("failed", 'Error In Connection');
            }
        });
    }
}

//To set reply on comment from post Modal
function setReplyOnCommentModal(id, replycall, postId, elements) {
    var commentText = $(elements).val();
    var postCreatedAt = $('#showModalPost #comment-basic-details' + postId).attr('post-created-at');
    var callAjaxFlag = $('#showModalPost #comment-basic-details' + postId).attr('call-reply-ajax-flag');
    var commentValid = "false";
    var createdDataTime;
    var commentData;
    if (modalIsNotEmpty("#showModalPost #comment-reply-div-" + id)) {
        commentValid = "true";
    }
    if (replycall == 'reply') {
        commentId = id;
        replyId = '';
    } else {
        commentId = '';
        replyId = id;
    }
    if (callAjaxFlag == "true" & commentValid == 'true') {
        var createrEmail2  = $('#showModalPost #comment-basic-details' + postId).attr('post-creater-email');
        $.ajax({
            type: 'POST',
            dataType: 'JSON',
            async: 'false',
            data: {
                commentText: encodeURIComponent(commentText),
                postId: encodeURI(postId),
                postCreatedAt: encodeURI(postCreatedAt),
                commentId: encodeURI(commentId),
                replyId: encodeURI(replyId),
                PostCreatorEmail: encodeURI(createrEmail2)
            },
            url: '../../controllers/social/setCommentOnPost.php',
            success: function(data) {
                console.log(data);
                if (data.errCode == -1) {
                    // console.log("Rplydata : "+data);
                    // commentData     = decodeHTMLEntities(data.errMsg.comment);
                    // createdDataTime = dataCreatedAt(data.errMsg.created_at);
                    // $('#user-comment-text').val('');
                    // $('.comment-count').text(data.errMsg.comment_count);
                    // $('#user-comment-title').text(commentData);
                    // $('#comment-created-time').text(createdDataTime);
                    // // $('.myComment1').show();
                    // commentAppendData('function','','3', '');
                    $('.comment-count' + postId).text(data.errMsg.comment_count);
                    $('#showModalPost .comment-count' + postId).text(data.errMsg.comment_count);
                    $('#reply-count-' + commentId).text(" (" + data.errMsg.replyCount + ") ");
                    $('#showModalPost #reply-count-' + commentId).text(" (" + data.errMsg.replyCount + ") ");
                    getCommentReplies('function', '', '3', id, '2', postId);
                    $(elements).val('');
                    // showToast("success",'Successfully Added Comment');
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else if (data.errCode == 14) {
                    if (!$('#postDeletedModal').hasClass('in')) {
                        postDeletedModal('This Comment No longer Exist');
                    }
                    commentAppendData('function', '', '3', '', postId);
                    getCommentReplies('function', '', '3', id, '2', postId);
                } else if (data.errCode == 4) {
                    if (!$('#postDeletedModal').hasClass('in')) {
                        postDeletedModal("This Post No longer Exit");
                    }
                    postLoadAsPerUrl();
                } else {
                    showToast("failed", 'failed to Add Reply On Comment');
                }
            },
            beforeSend: function(data) {
                $('#comment-basic-details').attr('call-reply-ajax-flag', 'false');
            },
            complete: function(data) {
                $('#comment-basic-details').attr('call-reply-ajax-flag', 'true');
            },
            error: function(data) {
                showToast("failed", 'Error In Connection');
            }
        });
    }
}

// to get replies on perticular comment
function getCommentReplies(callType, lastCommentReplyTime, limit, commentId, rplyCallType, postId) {
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callAjaxFlag = $('#comment-indivisual-div-' + commentId).attr('call-ajax-reply-get-flag');
    var commentmoreLoad;
    if (callAjaxFlag == 'true') {
        $.ajax({
            type: 'POST',
            dataType: 'html',
            async: false,
            data: {
                postId: encodeURI(postId),
                postCreatedAt: encodeURI(postCreatedAt),
                lastCommentTime: encodeURI(lastCommentReplyTime),
                limit: encodeURI(limit),
                commentId: encodeURI(commentId),
                rplyCallType: encodeURI(rplyCallType)
            },
            url: '../../views/social/getCommentReplyData.php',
            success: function(data) {
                $('#comment-reply-click-' + commentId).css('pointer-events', 'none');
                $('#showModalPost #comment-reply-click-' + commentId).css('pointer-events', 'none');
                if (callType == 'function') {
                    $('#comment-reply-append-' + commentId).html(data);
                    $('#comment-reply-append-' + commentId).slideDown(600);

                    $('#showModalPost #comment-reply-append-' + commentId).html(data);
                    $('#showModalPost #comment-reply-append-' + commentId).slideDown(600);
                } else {
                    $('#comment-reply-append-' + commentId).append(data);

                    $('#showModalPost #comment-reply-append-' + commentId).append(data);
                }
                commentmoreLoad = $('#get-more-comment-reply-' + lastCommentReplyTime + commentId).text();
                commentLoadFlag = $('#get-more-comment-reply-' + lastCommentReplyTime + commentId).attr('commentLoadClick');
                $('#loadMoreCommentReplyPostPage' + commentId).attr('onclick', $('#get-more-comment-reply-' + lastCommentReplyTime + commentId).text());
                $('#showModalPost #loadMoreCommentReplyPostPage' + commentId).attr('onclick', $('#get-more-comment-reply-' + lastCommentReplyTime + commentId).text());
                if (commentLoadFlag == 'true') {
                    $('#commentReplyLoadMoreButton' + commentId).show();
                    $('#showModalPost #commentReplyLoadMoreButton' + commentId).show();
                } else {
                    $('#commentReplyLoadMoreButton' + commentId).hide();
                    $('#showModalPost #commentReplyLoadMoreButton' + commentId).hide();
                }
                if (rplyCallType == 1) {
                    var replyHtml = '<div class="col-xs-11"><div id="comment-form' + commentId + '" class="form-group"><input type="text" id="user-comment-reply-text-' + commentId + '" class="form-control checkEmpty reply-input-box can-mention-user inputor" placeholder="Write Reply Here"  maxlength = "600"></div></div>';
                    $('#comment-reply-div-' + commentId).append(replyHtml);
                    $('#comment-reply-div-' + commentId).show(500);

                    $('#showModalPost #comment-reply-div-' + commentId).append(replyHtml);
                    $('#showModalPost #comment-reply-div-' + commentId).show(500);
                    $('.comment-reply-create').on('keypress', '#user-comment-reply-text-' + commentId, function(ev) {
                        if ($(this).val().length >= 1) {
                            mentionOnKeyup($(this).val());
                        }
                        if (ev.which == 13) {
                            setReplyOnComment(commentId, 'reply', postId);
                        }
                    });

                    $('#showModalPost .comment-reply-create').on('keypress', '#user-comment-reply-text-' + commentId, function(ev) {
                        if ($(this).val().length >= 1) {
                            mentionOnKeyup($(this).val());
                        }
                        if (ev.which == 13) {
                            var elements = this;
                            setReplyOnCommentModal(commentId, 'reply', postId, elements);
                        }
                    });
                }
                $('#remainingReplyLoadCount' + commentId).text($('#get-more-comment-reply-' + lastCommentReplyTime + commentId).attr('remainingCount'));

                $('#showModalPost #remainingReplyLoadCount' + commentId).text($('#get-more-comment-reply-' + lastCommentReplyTime + commentId).attr('remainingCount'));

                $('#user-comment-reply-text-' + commentId).focus();

                $('#showModalPost #user-comment-reply-text-' + commentId).focus();
            },
            beforeSend: function(data) {
                $('#comment-indivisual-div-' + commentId).attr('call-ajax-reply-get-flag', 'false');
            },
            complete: function(data) {
                $('#comment-indivisual-div-' + commentId).attr('call-ajax-reply-get-flag', 'true');
            },
            error: function(data) {
                console.log('error in getting Comment');
                returnText = "ERROR";
            }
        });
    }
    if (callType == 'function') {
        return;
    }
}



// function to call update and edit comment
function editDeleteReply(type, id, cId, postId) {
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callEditDeletFlag = $('#comment-indivisual-div-' + cId).attr('call-ajax-reply-get-flag');
    if (type == 'update') {
        // #showModalPost
        // getCommentReplies('function', '', '3', cId, '1', postId);
        $("#comment-reply-append-" + cId).each(function() {
            $('.comment-data-each' + cId).show();
            $('.comment-editing-each' + cId).hide();
            $('.comment-action-each' + cId).show();

            $('#showModalPost .comment-data-each' + cId).show();
            $('#showModalPost .comment-editing-each' + cId).hide();
            $('#showModalPost .comment-action-each' + cId).show();
        });
        var EditingComment = $('#Comment-reply-og-text' + id).text();
        $('#comment-edit-display-' + id).hide();
        $('#comment-edit-display-action-' + id).hide();
        $('#comment-reply-div-' + cId).hide();

        $('#showModalPost #comment-edit-display-' + id).hide();
        $('#showModalPost #comment-edit-display-action-' + id).hide();
        $('#showModalPost #comment-reply-div-' + cId).hide();
        var newEditHtml = '<div id = "reply-edit-form' + id + '" class="form-group"><input type = "text" id = "user-comment-reply-text' + id + '" class = "form-control checkEmpty reply-input-box can-mention-user inputor"  value = "' + EditingComment + '" comment-id="' + id + '" parent-comment-id = "' + cId + '"  maxlength = "600"></div>';
        $('#comment-edit-' + id).html(newEditHtml);
        $('#comment-edit-' + id).show();

        $('#showModalPost #comment-edit-' + id).html(newEditHtml);
        $('#showModalPost #comment-edit-' + id).show();
        $('.comment-editing').on('keyup', '#user-comment-reply-text' + id, function(event) {
            if (event.which == 13) {
                submitEditReply(postId, id);
            }
        });

        $('#showModalPost .comment-editing').on('keyup', '#user-comment-reply-text' + id, function(event) {
            if (event.which == 13) {
                var eve = this;
                submitEditReplyModal(postId, id, eve);
            }
        });
    }
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');
    var callEditDeletFlag = $('#comment-indivisual-div-' + cId).attr('call-ajax-reply-get-flag');
    if (type == 'delete') {
        var UserEmail = $('#commented-list-'+id).attr('comment-user-email');
        if (UserEmail) {
            var blockCheck = getBlockedUser(UserEmail);
            if (blockCheck) {
                var commentDeleteflag = commentEditDeleteajaxFunc('', id, 'delete', postId, postCreatedAt);
                // console.log(commentDeleteflag);
                if (commentDeleteflag.errCode == -1) {
                    $('#commented-list-' + id).slideUp(200);
                    $('#showModalPost #commented-list-' + id).slideUp(200);
                    $('.comment-count' + postId).text(commentDeleteflag.errMsg.comment_count);
                    $('#reply-count-' + cId).text(' (' + commentDeleteflag.errMsg.replyCount + ') ');

                    $('#showModalPost .comment-count' + postId).text(commentDeleteflag.errMsg.comment_count);
                    $('#showModalPost #reply-count-' + cId).text(' (' + commentDeleteflag.errMsg.replyCount + ') ');
                    getCommentReplies('function', '', '3', cId, '2  ', postId);
                    if (socialFileUrl == 'social/userActivity') {
                        $('#comment-data-delete-'+postId).slideUp(200);
                    }
                    // showToast("success",'Successfully Deleted Comment');
                } else if (commentDeleteflag.errCode == 14) {
                    postDeletedModal('This Comment / Reply No longer Exist');
                    commentAppendData('function', '', '3', '', postId);
                    getCommentReplies('function', '', '3', cId, '2  ', postId);
                } else if (commentDeleteflag.errCode == 4) {
                    postDeletedModal('This Post No longer Exist');
                    postLoadAsPerUrl();
                } else {
                    showToast("failed", 'failed to Delete Reply');
                }
            } else {
                showToast("failed", 'You Have Blocked This User');
                getCommentReplies('function', '', '3', cId, '2  ', postId);
            }
        } else {
            showToast("failed", 'Something Went Wrong...!');
        }
    }
    return;
}


// function to edit comment
function submitEditReply(postId, cid) {
    // alert('1');
    var commentText = $('#user-comment-reply-text' + cid).val();
    // var postId        = $('#comment-basic-details').attr('post-id');
    var postCreatedAt = $('#comment-basic-details' + postId).attr('post-created-at');

    var commentValid = "false";
    var id = $('#user-comment-reply-text' + cid).attr('comment-id');
    var parentId = $('#user-comment-reply-text' + cid).attr('parent-comment-id');
    if (isNotEmpty("reply-edit-form" + cid)) {
        commentValid = "true";
    }
    if (commentValid == 'true') {
        var commentEditflag = commentEditDeleteajaxFunc(commentText, id, 'update', postId, postCreatedAt);
        if (commentEditflag.errCode == -1) {
            commentAppendData('function', '', '3', '', postId);
            getCommentReplies('function', '', '3', parentId, '1', postId);
            $('#comment-text-add' + postId).show();

            // showToast("success",'Successfully Updated Comment');
        } else if (commentEditflag.errCode == 14) {
            postDeletedModal('This Comment / Reply No longer Exist');
            commentAppendData('function', '', '3', '', postId);
            getCommentReplies('function', '', '3', parentId, '1', postId);
        } else if (commentEditflag.errCode == 4) {
            postDeletedModal('This Post No longer Exist');
            postLoadAsPerUrl();
        }
    } else {
        showToast("failed", 'failed to Edit Reply On Comment');
    }
    return true;
}


// function to edit comment from post Modal
function submitEditReplyModal(postId, cid, eve) {
    var commentText = $(eve).val();
    // var postId        = $('#comment-basic-details').attr('post-id');
    var postCreatedAt = $('#showModalPost #comment-basic-details' + postId).attr('post-created-at');

    var commentValid = "false";
    var id = $(eve).attr('comment-id');
    var parentId = $(eve).attr('parent-comment-id');
    if (modalIsNotEmpty("#showModalPost #reply-edit-form" + cid)) {
        commentValid = "true";
    }
    if (commentValid == 'true') {
        var commentEditflag = commentEditDeleteajaxFunc(commentText, id, 'update', postId, postCreatedAt);
        if (commentEditflag.errCode == -1) {
            commentAppendData('function', '', '3', '', postId);
            getCommentReplies('function', '', '3', parentId, '1', postId);
            $('#showModalPost #comment-text-add' + postId).show();
            // showToast("success",'Successfully Updated Comment');
        } else if (commentEditflag.errCode == 14) {
            postDeletedModal('This Comment / Reply No longer Exist');
            commentAppendData('function', '', '3', '', postId);
            getCommentReplies('function', '', '3', parentId, '1', postId);
        } else if (commentEditflag.errCode == 4) {
            postDeletedModal('This Post No longer Exist');
            postLoadAsPerUrl();
        }
    } else {
        showToast("failed", 'failed to Edit Reply On Comment');
    }
    return true;
}

/* To load other commented data and show total data*/
function seeMoreComment(id) {
    $('#seeMore-load' + id).hide();
    $('#comment-text-2' + id).show();
}

/* To show Comment Div on click of comment url*/
function commentDivShow(id) {
    // alert(id);
    commentAppendData('function', '', '3', '', id);
    $('#comment-include-' + id).slideDown(600);
    var callEnterEvent = $('#comment-hidden-enter-event-' + id).attr('execute-enter-event-main-page');
    return true;
}

// function to call edit delete function to delete comment
function callCommentDeleteEditFunction(id, postId, callType, commentReplyId) {
    $('#deleteCommentModal').modal('hide');
    if (callType == 'reply') {
        editDeleteReply('delete', id, commentReplyId, postId);
    } else if (callType == 'comment') {
        editDeleteComment('delete', id, postId);
    }
}

// function to display delete confirmation modal
function deleteConfirmation(postId, id, callType, commentReplyId) {
    // alert("del "+type);
    $.ajax({
        type: 'POST',
        dataType: 'HTML',
        async: false,
        data: {
            commentId: encodeURI(id),
            postId: encodeURI(postId),
            callType: encodeURI(callType),
            CommentReplyId: encodeURI(commentReplyId)
        },
        url: rootUrl + 'views/social/deleteConfirmation.php',
        success: function(data) {
            $('#deleteCommentModal .modal-dialog .card .modal-body-social-user-image').html(data);
            $('#deleteCommentModal').modal('show');
        },
        error: function(data) {
            showToast("failed", 'Error In Connection');
        }
    });
}

/* -------------------------- Comment JQuery End Here -------------------------- */
/// Show report pop up
function showReportDetails(acHandler, type, postId, createdAt) {
    $('#showModalPost').modal('hide');
    $.ajax({
        type: 'POST',
        url: '../../views/social/reportDetails.php',
        data: ({ postId: postId, acHandler: acHandler, created_at: createdAt, postType: type }),
        dataType: 'html',
        success: function(data) {
            $('#showReportDetails .modal-dialog .card .modal-body-report-user').html(data);
            previewInitialization();
            $('#showReportDetails').modal('show');
        },
        error: function(data) {
            alert('Error in post Update');
        }
    });

}


/******************** Start CDP JS **************************/

function socialCDPForPost(elem,post_id, created_at, post_type, account_handle, posted_by, shared_by, search_count, ip_address, keyword,email) {
    if ($('#thumbs-up' + post_id).hasClass("fa-thumbs-o-up") || $('#showModalPost').find('#thumbs-up' + post_id).hasClass("fa-thumbs-o-up")) {

        $('#thumbs-up' + post_id).css({ "cursor": "not-allowed", "opacity": "0.6" });
        $('#thumbs-up' + post_id).removeClass("fa-thumbs-o-up").addClass("fa-coin-hand");

        $('#showModalPost').find('#thumbs-up' + post_id).css({ "cursor": "not-allowed", "opacity": "0.6" });
        $('#showModalPost').find('#thumbs-up' + post_id).removeClass("fa-thumbs-o-up").addClass("fa-coin-hand");

        explode(event,post_id);
    }

    if(socialFileUrl == "social/ft_like"){
       var  blockCheck = true;
        $(elem).css('pointer-events','none');
    }else{
        var blockCheck = getBlockedUser(email);
        $(elem).css('pointer-events','none');
    }
    if (blockCheck) {
        var removeCheck = removePostOfUser(post_id, post_type, created_at, email);
        if (removeCheck) {
            // alert(socialFileUrl);
            if (socialFileUrl == "social/index" || socialFileUrl == "social/privateTimeline" || socialFileUrl == "social/socialPostDetails" || socialFileUrl == 'social/otherTimeline' || socialFileUrl == "social/userActivity" || socialFileUrl == "social/viewKeywoTimeline" || socialFileUrl == "social/ft_like" || socialFileUrl == "social/ft_create" || socialFileUrl == "social/ft_share") {
                var hideCheck = hidePostOfUser(post_id, post_type, created_at, email);
                if (hideCheck) {
                    var post_collection = $('#post_collection' + post_id).val();
                    $.ajax({
                        url: '../../controllers/social/checkCDPValidations.php',
                        type: "POST",
                        dataType: 'JSON',
                        async: true,
                        data: {
                            content_id: post_id,
                            posted_by: posted_by,
                            shared_by: shared_by,
                            mode_type: 'social',
                            searchCountFromIP: search_count,
                            origin_ip: ip_address,
                            created_at: created_at,
                            account_handle: account_handle,
                            q: keyword,
                            post_type: post_type,
                            email: email,
                            socialCDPType: "view_" + post_type
                        },
                        success: function (data) {
                            //console.log(data);
                            //Number of interaction count and user available balance on header
                            if (data.errCode == -1) {
                                message = 'success';
                                var trans_id = data.transaction_id;
                                var trans_type = data.transaction_type;
                                var current_payout = data.current_payout;

                                if (data.search_type == 'Qualified') {
                                    // if qualified then interaction count should be green
                                    $('#pending-interaction').css("color", "rgb(9, 250, 9)");
                                    $('#pending-interaction').html(data.no_of_pending_interaction);
                                    $('#available-balance').html(number_format_sort(parseFloat(data.total_available_balance),4)+" "+keywoDefaultCurrency);
                                    $("#available-balance").attr('origprice',parseFloat(data.total_available_balance)+" "+keywoDefaultCurrency);
                                    //update like post count and create like post json file
                                    updateLikePostCount(post_id, created_at, post_type, account_handle, posted_by, shared_by, search_count, ip_address, keyword, email);

                                    //To create CDP Json file of successfull CDP transaction
                                    createCDPLikeJsonFile(post_id, created_at, posted_by, shared_by, post_collection, trans_id, trans_type, post_type, current_payout);
                                    if(socialFileUrl == "social/ft_like" || socialFileUrl == "social/ft_create" || socialFileUrl == "social/ft_share"){
                                        updateLikePostDetails();
                                    }
                                } else {
                                    // if unqualified then interaction count should be red
                                    $('#pending-interaction').css("color", "red");
                                    $('#pending-interaction').html(data.no_of_pending_interaction);
                                    $('#thumbs-up' + post_id).css({"cursor": "not-allowed", "opacity": "0.6"});
                                    $('#showModalPost').find('#thumbs-up' + post_id).css({
                                        "cursor": "not-allowed",
                                        "opacity": "0.6"
                                    });
                                    //change status of post after CDP call
                                    $('#cdp_status' + post_id + ' i').removeClass('cdp-red').addClass('text-color-Gray');
                                    $('#showModalPost').find('#cdp_status' + post_id + ' i').removeClass('cdp-red').addClass('text-color-Gray');
                                }
                            } else {
                                message = 'failed';
                            }
                        },
                        error: function (xhr, status, error) {
                        }
                    });

                }
            }
        }
    }
}

//--------------------------        END CDP JS     -------------------------------//


//--------------------------      Start Like Post JS    -------------------------------//
function updateLikePostCount(post_id, created_at, post_type, account_handle, posted_by, shared_by, search_count, ip_address, keyword, account_email) {
    $.ajax({
        type: 'POST',
        url: '../../controllers/social/likePostController.php',
        dataType: 'json',
        async: false,
        data: { 'post_id': post_id, 'created_at': created_at, 'type': post_type, 'account_handle': account_handle, 'email': account_email },
        success: function(data) {
            if (data.errCode == -1) {

                $message = 'Successfully like post';
                var likeCount = data.activity.like_count;
                // if ($('#showModalPost').find('#thumbs-up' + post_id).hasClass("fa-thumbs-o-up") || $('#thumbs-up' + post_id).hasClass("fa-thumbs-o-up")) {
                //
                //     $('#showModalPost').find('#thumbs-up' + post_id).removeClass("fa-thumbs-o-up").addClass("fa-coin-hand");
                //     $('#showModalPost').find('#thumbs-up' + post_id).css({ "cursor": "not-allowed", "opacity": "0.6" });
                //     $('#thumbs-up' + post_id).css({ "cursor": "not-allowed", "opacity": "0.6" });

                $('.like-count' + post_id).html(likeCount);
                $('#showModalPost').find('.like-count' + post_id).html(likeCount);

                //     $('#thumbs-up' + post_id).removeClass("fa-thumbs-o-up").addClass("fa-coin-hand");
                //     explode(event,post_id);
                // }
            } else {
                $message = 'Failed like post';
            }
        }
    });
}

//like hover
// $(".like-area").hover(function() {
//     if ($(this).find("i").hasClass("fa-thumbs-o-up")) {
//         $(".likes-click").css("cursor", "not-allowed");
//         $(".likes-click").css("opacity", "0.6");
//     } else {
//         $(".likes-click").css("cursor", "pointer");
//         $(".likes-click").css("opacity", "1");
//     }
// });

function explode(e,id) {
    var x = e.clientX;
    var y = e.clientY;
    var c = document.createElement('canvas');
    var ctx = c.getContext('2d');
    var ratio = window.devicePixelRatio;
    var particles = [];
    var cardDormer = $("#post_"+id).find(".social-likes-dislikes");
    $(c).appendTo(cardDormer);

    c.style.position = 'absolute';
    c.style.left = (-25) + 'px';
    c.style.top = (-25) + 'px';
    c.style.pointerEvents = 'none';
    c.style.width = 50 + 'px';
    c.style.height = 50 + 'px';
    c.width = 100 * ratio;
    c.height = 100 * ratio;

    function Particle() {
        return {
            x: c.width / 2,
            y: c.height / 2,
            radius: 10,
            color:'rgb(0,0,0)',
            rotation: r(0,360, true),
            speed:4,
            friction: 0.9,
            opacity: r(0,0.5, true),
            yVel: 0,
            gravity: 0
        }
    }

    for(var i=0; ++i<5;) {
        particles.push(Particle());
    }

    function render() {
        ctx.clearRect(0, 0, c.width, c.height)

        particles.forEach(function(p, i) {

            angleTools.moveOnAngle(p, p.speed)

            p.opacity -= 0.01
            p.speed *= p.friction
            p.radius *= p.friction

            p.yVel += p.gravity
            p.y += p.yVel

            if(p.opacity < 0) return
            if(p.radius < 0) return

            ctx.beginPath()
            ctx.globalAlpha = p.opacity
            ctx.fillStyle = p.color
            ctx.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false)
            ctx.fill()
        })
    }

    ;(function renderLoop(){
        requestAnimationFrame(renderLoop)
        render()
    })()

    setTimeout(function() {
        $(c).remove();
    }, 3000)
}

var angleTools={getAngle:function(t,n){var a=n.x-t.x,e=n.y-t.y;return Math.atan2(e,a)/Math.PI*180},getDistance:function(t,n){var a=t.x-n.x,e=t.y-n.y;return Math.sqrt(a*a+e*e)},moveOnAngle:function(t,n){var a=this.getOneFrameDistance(t,n);t.x+=a.x,t.y+=a.y},getOneFrameDistance:function(t,n){return{x:n*Math.cos(t.rotation*Math.PI/180),y:n*Math.sin(t.rotation*Math.PI/180)}}};
function r(a,b,c){ return parseFloat((Math.random()*((a?a:1)-(b?b:0))+(b?b:0)).toFixed(c?c:0)); }


//--------------------------      END Like Post JS      -------------------------------//

//--------------------------      Start Create CDP Json File JS -----------------------------//
function createCDPLikeJsonFile(post_id, created_at, posted_by, shared_by, post_collection, trans_id, trans_type, post_type, current_payout) {
    $.ajax({
        type: "POST",
        url: '../../controllers/social/setPostJsonController.php',
        dataType: "html",
        async: false,
        data: {
            content_id: post_id,
            posted_by: posted_by,
            shared_by: shared_by,
            created_at: created_at,
            post_collection_name: post_collection,
            transaction_id: trans_id,
            transaction_type: trans_type,
            post_type: post_type,
            current_payout: current_payout
        },
        success: function(data) {
            //change status of post after CDP call
            $('#cdp_status' + post_id + ' i').removeClass('cdp-red').addClass('cdp-green');
        }
    });
}
//--------------------------      END Create CDP Json File JS -----------------------------//


/* To SHow Modal with dynanamic message */
function postDeletedModal(message) {
    console.log("postDeletedModal called");
    $('.modal').modal('hide');
    var ModalDeletionHtml = '<div class="modal-body">' + message + '</div><div class="modal-footer"><!--<input type="button" class="form-post-btn btn-default " data-dismiss = "modal" value = "No">   --><input type="button" class="form-post-btn" data-dismiss = "modal" onclick="" value = "OK"></div>';

    $('#postDeletedModal .modal-dialog .card .modal-body-post-deleted').html(ModalDeletionHtml);
    $('#postDeletedModal').modal('show');
    return true;
}

/* to reload current page on clic of OK button of modal */
function socialRedirectToPage() {
    $('#postDeletedModal').modal('hide');
    window.location = rootUrl + '/views/' + socialFileUrl + '.php';
}


//To load Page data as per Url
function postLoadAsPerUrl() {
    if (socialFileUrl == "social/index") {
        $("#home-next-post-data").attr('data-count', '0');
        $('#home-next-post-data').attr('data-post-type', 'all');
        $('#home-next-post-data').attr('data-scroll-allow', 'true');
        $('#home-next-post-data').attr('data-status-empty', '');
        loadHomePostTimeline(lastPostTime, "all", searchKeywo, searchText);
        $(".social-status-tabs").each(function() {
            $(this).removeClass("active");
        });
        $("#all").addClass("active");
    } else if (socialFileUrl == "social/privateTimeline") {
        $("#private-next-post-data").attr("data-create-time", '');
        loadPostTimeline("all");
    } else if (socialFileUrl == 'social/otherTimeline') {
        location.reload();
    } else if (socialFileUrl == 'social/socialPostDetails') {
        location.reload();
    }
    return true;
}

//Check Deleted Post Blocked User
function removePersonalPostForBlockedUser(elem, email, postId, type, postTime) {
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, type, postTime, email);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, type, postTime, email);
            if (hideCheck) {
                blockUserAcc(elem, email,postId,type,postTime);
            }
        }
    }
}

//Check Deleted Post follow and unfollow  User
function followAccountOfUser(elem, email, postId, type, postTime) {
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, type, postTime, email);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, type, postTime, email);
            if (hideCheck) {
                followUserAcc(elem, email);
            }
        }
    }
}

//Check bookmark Post for Deleted Post User
function setBookmarkAccountOfUser(postId, type, postTime, email) {
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, type, postTime, email);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, type, postTime, email);
            if (hideCheck) {
                bookmarkAccountOfUser(type, postId, postTime,email);
            }
        }
    }
}

//Check Report Post for Deleted Post User
function setReportAccountOfUser(postId, type, postTime, email) {
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        var removeCheck = removePostOfUser(postId, type, postTime, email);
        if (removeCheck) {
            var hideCheck = hidePostOfUser(postId, type, postTime, email);
            if (hideCheck) {
                showReportDetails(email, type, postId, postTime);
            }
        }
    }
}



/********************************************other Popular , Other & Related post **********************************************/

function reloadOtherPosts(userId, postId, postCreationTime, postType, postSharedType, keywords, accountHandler) {
    //console.log(userId);
    $.ajax({
        type: 'POST',
        dataType: 'HTML',
        data: { 'userId': userId, 'postId': postId, 'postCreationTime': postCreationTime, 'postType': postType, 'postSharedType': postSharedType, 'keywords': keywords, 'accountHandler': accountHandler },
        url: '../../views/social/widgetsOtherPost.php',
        success: function(data) {
            $('#popularaPostWidget').html(data);
        },
        beforeSend: function(data) {
            $('#other-post-refresh-btn').css('pointer-events', 'none');
        },
        complete: function(data) {
            $('#other-post-refresh-btn').css('pointer-events', 'auto');
        },
        error: function(data) {
            console.log(data);
            //showToast("failed", 'failed to Load Popular Post.');
        },
    });
}

function getPopularPost(postType) {
    $('.popular-active-indicate').removeClass('active');
    $('#popular-post-type-' + postType).addClass('active');
    $.ajax({
        type: 'POST',
        dataType: 'HTML',
        data: { 'postType': postType },
        url: '../../views/social/widgetPopularPost.php',
        success: function(data) {
            $('#popular-post-details').html(data);
        },
        beforeSend: function(data) {
            $('.popular-active-indicate').css('pointer-events', 'none');
        },
        complete: function(data) {
            $('.popular-active-indicate').css('pointer-events', 'auto');
        },
        error: function(data) {
            console.log(data);
            //showToast("failed", 'failed to Load Popular Post.');
        },
    });
}

//trending keywords according to script
$('.trending-content li a').on('click', function() {
    var j = $('.trending-content li a');
    $('.trending-content li').each(function() {
        var k = $(this).find('a').data('id');
        $(k).hide();
        $(j).removeClass('active');
    });
    var i = $(this).data('id');
    $(i).show();
    $(this).addClass('active');
    getTrendingKeywords($(this).data("id"));
});
function getTrendingKeywords(postType) {
    $.ajax({
        type: 'POST',
        dataType: 'HTML',
        data: { 'postType': postType },
        url: '../../views/social/trendingKeywords.php',
        success: function(data) {
            $('#trending-keywords-data').html(data);
        },
        beforeSend: function() {

        },
        complete: function() {

        },
        error: function() {
            showToast("failed", 'Failed to load trending keywords.');
        },
    });
}


/********************************************End other Popular , Other, Related post *******************************************/

/********************************************Mutual list **********************************************/
function loadMutual(type, email) {
    var jsonFileNo = $('#followingDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followingDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followingDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followingdivAppendId').text();
    if (scrollAllow == 'false') {
        scrollAllow = 'true';
    }
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId':divAppendId }),
            url: '../../views/social/mutualList.php?email=' + email + '&type=' + type,
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }

                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followingDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followingDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followingDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followingdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}
/********************************************End Mutual list  *******************************************/



/******************************************** ACTIVITY LOG ***********************************************/

//Initialization likePost
function likeInitialize() {
    $('#like-post-data-append').attr('like-array', '');
    $('#like-post-data-append').attr('like-start', '');
    $('#like-post-data-append').attr('pageScrollEndFlag', '');
    $('#like-post-data-append').attr('like-last-date', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    return true;
}

//Initialization Comment On Post
function commentInitialize() {
    $('#comment-post-data-append').attr('comment-start', '');
    $('#comment-post-data-append').attr('comment-last-date', '');
    $('#comment-post-data-append').attr('ajaxCallFlag', 'true');
    $('#comment-post-data-append').attr('pageScrollEndFlag', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    return true;
}

//Initialization Like On Post
function hiddenPostInitialize() {
    $('#hidden-post-data-append').attr('hidden-start', '');
    $('#hidden-post-data-append').attr('hidden-last-date', '');
    $('#hidden-post-data-append').attr('ajaxCallFlag', 'true');
    $('#hidden-post-data-append').attr('pageScrollEndFlag', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    $('#hidden-post-data-append').attr('l-value', 0);
    return true;
}


//Initialization Bookmark On Post
function bookmarkPostInitialize() {
    $('#bookmark-post-data-append').attr('bookmark-array', '');
    $('#bookmark-post-data-append').attr('bookmark-start', '');
    $('#bookmark-post-data-append').attr('pageScrollEndFlag', '');
    $('#bookmark-post-data-append').attr('bookmark-last-date', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    return true;
}

//Initialization Bookmark On Post
function followPeopleInitialize() {
    $('#followingDataAppend').attr('followingDataAppendCount', '');
    $('#followingDataAppend').attr('followingDataLoopCount', '');
    $('#followingDataAppend').attr('pageScrollEndFlag', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    $('#followingDataAppend').attr('ajxCallFlag', 'true');
    $('#followingDataAppend').text('1');
    return true;
}

//Initialization Bookmark On Post
function followKeywordInitialize() {
    $('#keyword-follow-user-data-append').attr('keyword-follow-json', '');
    $('#keyword-follow-user-data-append').attr('keyword-follow-start', '');
    $('#keyword-follow-user-data-append').attr('keyword-follow-last-keyword', '');
    $('#keyword-follow-user-data-append').attr('ajaxCallFlag' , 'true');
    $('#keyword-follow-user-data-append').attr('pageScrollEndFlag', '');
    $('#followingDataAppend').attr('followingDataExistCount', 0);
    return true;
}

//Click Of Like tab in Activity page
$('#likeDislikeActivity').on('click',function(){
    likeInitialize();

    loadLikeSuccess = getActivityLike();
    if (loadLikeSuccess) {

        $('#keywordSortingType').hide();
        $('#activityType').text('Likes');
        $('.acivity-tab-display').removeClass('active');
        $('#likeDislikeActivityDiv').addClass('active');
        followPeopleInitialize();
        bookmarkPostInitialize();
        commentInitialize();
        hiddenPostInitialize();
        followKeywordInitialize();
        $("html, body").animate({ scrollTop: 0 }, 300);
        $('#activity-log-details').attr('activity-type-to-load','postLike');


    }
});

//Click Of Comment tab in Activity page
$('#commentActivity').on('click',function(){
    commentInitialize();
    // $("#commentActivityDivLoader").show();
    loadCommentSuccess = getActivityComment();
    if (loadCommentSuccess) {

        likeInitialize();
        followPeopleInitialize();
        bookmarkPostInitialize();
        hiddenPostInitialize();
        followKeywordInitialize();
        $('#activity-log-details').attr('activity-type-to-load','postComment');
        $('#keywordSortingType').hide();
        $('#activityType').text('Comments');
        $('.acivity-tab-display').removeClass('active');
        $('#commentActivityDiv').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 300);


    }
    // $("#commentActivityDivLoader").hide();
});

//Click Of Hidden Post tab in Activity page
$('#hiddenPostActivity').on('click',function(){
    hiddenPostInitialize();


    loadCommentSuccess = getActivityHiddenPost();
    if (loadCommentSuccess) {

        commentInitialize();
        likeInitialize();
        followPeopleInitialize();
        bookmarkPostInitialize();
        followKeywordInitialize();
        $('#activity-log-details').attr('activity-type-to-load','hiddenPost');
        $('#keywordSortingType').hide();
        $('#activityType').text('Hidden Posts');
        $('.acivity-tab-display').removeClass('active');
        $('#hiddenPostActivityDiv').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 300);


    }
});

//Click Of Bookmark Post tab in Activity page
$('#bookmarkPostActivity').on('click',function(){
    bookmarkPostInitialize();

    loadBookmarkSuccess = getActivityBookmarkPost();
    if (loadBookmarkSuccess) {

        $('#keywordSortingType').hide();
        commentInitialize();
        likeInitialize();
        followPeopleInitialize();
        hiddenPostInitialize();
        followKeywordInitialize();
        $('#activity-log-details').attr('activity-type-to-load','postBookmark');
        $('#activityType').text('Post Bookmarks');
        $('.acivity-tab-display').removeClass('active');
        $('#bookmarkPostActivityDiv').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 300);


    }
});

//Click Of FOllowed People tab in Activity page
$('#followedPeopleActivity').on('click',function(){
    followPeopleInitialize();

    $("#followedKeywordActivityDivLoder").show();
    var loadFollowedSuccess = loadFollowedPeople();
    if (loadFollowedSuccess) {

        $('#keywordSortingType').hide();
        commentInitialize();
        likeInitialize();
        bookmarkPostInitialize();
        hiddenPostInitialize();
        followKeywordInitialize();
        $('#activity-log-details').attr('activity-type-to-load','followPeople');
        $('#activityType').text('Followed People');
        $('.acivity-tab-display').removeClass('active');
        $('#followedPeopleActivityDiv').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 300);


    }
    $("#followedKeywordActivityDivLoder").hide();
});

//Click Of Followed Keyword tab in Activity page
$('#followedKeywordActivity').on('click',function(){
    followKeywordInitialize();

    var loadFollowedKeywordSuccess = getActivityfollowKeywordPost();
    if (loadFollowedKeywordSuccess) {

        $('#keywordSortingType').show();
        followPeopleInitialize();
        commentInitialize();
        likeInitialize();
        bookmarkPostInitialize();
        hiddenPostInitialize();

        $('#activity-log-details').attr('activity-type-to-load','followKeyword');
        $('#activityType').text('Followed Keywords');
        $('.acivity-tab-display').removeClass('active');
        $('#followedKeywordActivityDiv').addClass('active');
        $("html, body").animate({ scrollTop: 0 }, 300);


    }
});


//Click Of changePassword
$('#changePassword').on('click',function(){

    $('#keywordSortingType').show();
    followPeopleInitialize();
    commentInitialize();
    likeInitialize();
    bookmarkPostInitialize();
    hiddenPostInitialize();

    // $('.acivity-tab-display').removeClass('active');
    // $('#followedKeywordActivityDiv').addClass('active');
    // $("html, body").animate({ scrollTop: 0 }, 300);

    $.ajax({
        type: 'POST',
        dataType: 'HTML',
        async: false,
        url: rootUrl + 'views/social/user_social/change_password.php',
        success: function (data) {
            loadSuccess = true;
            $(".profile-details-div").hide();
            $('#activity-log-details').html("");
            $('#activity-title-div').hide();
            // activity-title-div
            $('#change-password-details').html(data);


        }
    });
});


// To Append Followings Data
function loadFollowedPeople() {
    var type               = 'followings';
    var jsonFileNo         = $('#followingDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo    = $('#followingDataAppend').attr('followingDataLoopCount');
    var scrollAllow        = $('#followingDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag        = $('#followingDataAppend').attr('ajxCallFlag');
    var DataExistLoopCount = parseInt($('#followingDataAppend').attr('followingDataExistCount'));
    var divAppendId        = $('#followingDataAppend').text();
    var loadSuccess        = false;
    if (scrollAllow       != 'false' & ajxCallFlag != 'false') {
        $('#followingDataAppend').attr('ajxCallFlag', 'false');
        $.ajax({
            type:     'POST',
            dataType: 'html',
            data:     ({'type':type, 'jsonFileNo':jsonFileNo, 'jsonFileInnerNo':jsonFileInnerNo, 'divAppendId': divAppendId, 'DataExistLoopCount': DataExistLoopCount}),
            url:      rootUrl+'views/social/getActivityFollowingsList.php',
            async:    false,
            success:  function(postdata) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (jsonFileNo == ''){
                    $('#activity-log-details').html(postdata);
                } else {
                    $('#activity-log-details').append(postdata);
                }
                $('#followingDataAppend').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend'+divAppendId).attr('jsonFileNo');
                if(jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend'+divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend'+divAppendId).attr('pageScrollEnd');
                var DataExistCount = $('#followingAppend'+divAppendId).attr('DataExistCount');
                $('#followingDataAppend').attr('followingDataAppendCount',jsonValue);
                $('#followingDataAppend').attr('followingDataLoopCount',jsonValueReadNo);
                $('#followingDataAppend').attr('pageScrollEndFlag',pageScrollEnd);
                $('#followingDataAppend').attr('followingDataExistCount',DataExistCount);
                $('#followingDataAppend').text(parseInt(divAppendId)+1);


                $('#activity-log-details').show();
                getUserActivityCount();

            },
            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
                $("#followedPeopleActivityDivLoder").show();
            },
            complete:   function(postdata){
                $('#ajaxLoader').hide();
                $("#followedPeopleActivityDivLoder").hide();
            },
            error:      function(postdata) {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }

    return loadSuccess;
}


//To Get All like data from Node
function getActivityLike() {

    $(".activity-log-details-div").show();
    var likArray   = $('#like-post-data-append').attr('like-array');
    var likStart   = $('#like-post-data-append').attr('like-start');
    var likLastDate = $('#like-post-data-append').attr('like-last-date');
    var ajaxCall    = $('#like-post-data-append').attr('ajaxCallFlag');
    var dataEnd     = $('#like-post-data-append').attr('pageScrollEndFlag');
    // alert(dataEnd);
    var loadSuccess = false;
    if (ajaxCall == "true" && dataEnd != "true") {
        $.ajax({
            type:     'POST',
            dataType: 'HTML',
            data:     {
                likeArray: likArray, likeStart: likStart ,likLastDate: likLastDate
            },
            async:      false,
            url:        rootUrl+'views/social/getActivtyLikeData.php',
            success:    function(data) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (likStart == ''){
                    $('#activity-log-details').html(data);
                } else {
                    $('#activity-log-details').append(data);
                }
                if (likStart == '') {
                    likStart = 0;
                }
                var newLikeArray      = $('#like-frontendArray-'+likStart).attr('like-started-array');
                var newLikeStart      = $('#like-frontendArray-'+likStart).attr('like-start');
                var newLikeDataEnded  = $('#like-frontendArray-'+likStart).attr('likeDataEnd');
                var newLikeEndingDate = $('#like-frontendArray-'+likStart).attr('like-prev-date');
                $('#like-post-data-append').attr('like-array', newLikeArray);
                $('#like-post-data-append').attr('like-start', newLikeStart);
                $('#like-post-data-append').attr('pageScrollEndFlag', newLikeDataEnded);
                $('#like-post-data-append').attr('like-last-date', newLikeEndingDate);
                $('#activity-log-details').show();
                getUserActivityCount();
            },
            beforeSend: function() {
                $('#like-post-data-append').attr('ajaxCallFlag','false');
                if (likStart != ''){
                    $('#ajaxLoader').show();
                }
                $("#likeDislikeActivityDivLoader").show();
            },
            complete:   function() {
                $('#like-post-data-append').attr('ajaxCallFlag','true');
                $('#ajaxLoader').hide();
                $("#likeDislikeActivityDivLoader").hide();
            },
            error:      function(data) {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }
    return loadSuccess;
}


//To Get All Comment data from Node
function getActivityComment() {

    $(".activity-log-details-div").show();
    var commStart    = $('#comment-post-data-append').attr('comment-start');
    var commLastDate = $('#comment-post-data-append').attr('comment-last-date');
    var ajaxCall     = $('#comment-post-data-append').attr('ajaxCallFlag');
    var dataEnd      = $('#comment-post-data-append').attr('pageScrollEndFlag');
    var loadSuccess  = false;
    //console.log(commStart+' & '+commLastDate+' & '+ajaxCall+' & '+dataEnd+' & '+loadSuccess);
    if (ajaxCall     == "true" && dataEnd != "true") {
        $.ajax({
            type:     'POST',
            dataType: 'HTML',
            data:     {
                lastCommTime: commStart, commLastDate: commLastDate
            },
            async:      false,
            url:        rootUrl+'views/social/getActivtyCommentsData.php',
            success:    function(data) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (commStart == ''){
                    $('#activity-log-details').html(data);
                } else {
                    $('#activity-log-details').append(data);
                }
                // if (commStart == '') {
                //     commStart = 0;
                //   }
                var newCommentStart      = $('#comment-frontendArray-'+commStart).attr('comment-start');
                var newCommentDataEnded  = $('#comment-frontendArray-'+commStart).attr('commentDataEnd');
                var newCommentEndingDate = $('#comment-frontendArray-'+commStart).attr('comment-prev-date');
                console.log('append : '+newCommentStart+' & '+newCommentDataEnded+' & '+newCommentEndingDate);
                $('#comment-post-data-append').attr('comment-start', newCommentStart);
                $('#comment-post-data-append').attr('pageScrollEndFlag', newCommentDataEnded);
                $('#comment-post-data-append').attr('comment-last-date', newCommentEndingDate);
                $('#activity-log-details').show();
                getUserActivityCount();

            },
            beforeSend: function(data) {
                $("#commentActivityDivLoader").show();
                $('#comment-post-data-append').attr('ajaxCallFlag','false');
                if (commStart != ''){
                    $('#ajaxLoader').show();
                }

            },
            complete:   function(data) {
                $('#comment-post-data-append').attr('ajaxCallFlag','true');
                $('#ajaxLoader').hide();
                $("#commentActivityDivLoader").hide();
            },
            error:      function(data) {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }
    return loadSuccess;
}


//To Get All Hidden Post data from Node
function getActivityHiddenPost() {
    $(".activity-log-details-div").show();
    var hiddStart    = $('#hidden-post-data-append').attr('hidden-start');
    var hiddLastDate = $('#hidden-post-data-append').attr('hidden-last-date');
    var ajaxCall     = $('#hidden-post-data-append').attr('ajaxCallFlag');
    var dataEnd      = $('#hidden-post-data-append').attr('pageScrollEndFlag');
    var lValue       = parseInt($('#hidden-post-data-append').attr('l-value'));
    var loadSuccess  = false;
    //console.log(hiddStart+' & '+hiddLastDate+' & '+ajaxCall+' & '+dataEnd+' & '+loadSuccess+' & '+lValue);
    //console.log(ajaxCall+' & '+dataEnd);
    if (ajaxCall == "true" && dataEnd != "true") {
        $.ajax({
            type:     'POST',
            dataType: 'HTML',
            data:     {
                lastHiddTime: hiddStart, hiddLastDate: hiddLastDate, lValue: lValue
            },
            async:      false,
            url:        rootUrl+'views/social/getActivtyHiddenPostData.php',
            success:    function(data) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (hiddStart == ''){
                    $('#activity-log-details').html(data);
                } else {
                    $('#activity-log-details').append(data);
                }
                var newHiddenStart      = $('#hidden-frontendArray-'+hiddStart).attr('hidden-start');
                var newHiddenDataEnded  = $('#hidden-frontendArray-'+hiddStart).attr('hiddenDataEnd');
                var newHiddenEndingDate = $('#hidden-frontendArray-'+hiddStart).attr('hidden-prev-date');
                var newlValue           = $('#hidden-frontendArray-'+hiddStart).attr('l-value');
                //console.log('append : '+newHiddenDataEnded+"     & "+hiddStart+"    &&     "+newHiddenStart);
                $('#hidden-post-data-append').attr('hidden-start', newHiddenStart);
                $('#hidden-post-data-append').attr('pageScrollEndFlag', newHiddenDataEnded);
                $('#hidden-post-data-append').attr('hidden-last-date', newHiddenEndingDate);
                $('#hidden-post-data-append').attr('l-value', newlValue);
                $('#activity-log-details').show();
                getUserActivityCount();

            },
            beforeSend: function() {
                $('#comment-post-data-append').attr('ajaxCallFlag','false');
                if (hiddStart != ''){
                    $('#ajaxLoader').show();
                }
                $("#hiddenPostActivityDivLoader").show();
            },
            complete:   function() {
                $('#comment-post-data-append').attr('ajaxCallFlag','true');
                $('#ajaxLoader').hide();
                $("#hiddenPostActivityDivLoader").hide();
            },
            error:      function() {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }
    return loadSuccess;
}

//To Get All Bookmark Post data from Node
function getActivityBookmarkPost() {

    $(".activity-log-details-div").show();
    var bookArray    = $('#bookmark-post-data-append').attr('bookmark-array');
    var bookStart    = $('#bookmark-post-data-append').attr('bookmark-start');
    var bookLastDate = $('#bookmark-post-data-append').attr('bookmark-last-date');
    var ajaxCall     = $('#bookmark-post-data-append').attr('ajaxCallFlag');
    var dataEnd      = $('#bookmark-post-data-append').attr('pageScrollEndFlag');
    var loadSuccess  = false;
    if (ajaxCall == "true" && dataEnd != "true") {
        $.ajax({
            type:     'POST',
            dataType: 'HTML',
            data:     {
                bookmarkArray: bookArray, bookmarkStart: bookStart, bookLastDate: bookLastDate
            },
            async:      false,
            url:        rootUrl+'views/social/getActivtyBookmarkPostData.php',
            success:    function(data) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (bookStart == ''){
                    $('#activity-log-details').html(data);
                } else {
                    $('#activity-log-details').append(data);
                }
                if (bookStart == '') {
                    bookStart = 0;
                }
                var newBookArray     = $('#bookmark-frontendArray-'+bookStart).attr('bookmark-started-array');
                var newBookStart     = $('#bookmark-frontendArray-'+bookStart).attr('book-start');
                var newBookDataEnded = $('#bookmark-frontendArray-'+bookStart).attr('bookDataEnd');
                var newBookEndingDate = $('#bookmark-frontendArray-'+bookStart).attr('bookmark-prev-date');
                $('#bookmark-post-data-append').attr('bookmark-array', newBookArray);
                $('#bookmark-post-data-append').attr('bookmark-start', newBookStart);
                $('#bookmark-post-data-append').attr('pageScrollEndFlag', newBookDataEnded);
                $('#bookmark-post-data-append').attr('bookmark-last-date', newBookEndingDate);
                $('#activity-log-details').show();
                getUserActivityCount();
            },
            beforeSend: function() {
                $('#bookmark-post-data-append').attr('ajaxCallFlag','false');
                if (bookStart){
                    $('#ajaxLoader').show();
                }
                $("#bookmarkPostActivityDivLoder").show();
            },
            complete:   function() {
                $('#bookmark-post-data-append').attr('ajaxCallFlag','true');
                $('#ajaxLoader').hide();
                $("#bookmarkPostActivityDivLoder").hide();
            },
            error:      function(data) {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }
    return loadSuccess;
}

//To Get All followed Keyword data from Node
function getActivityfollowKeywordPost() {

    $(".activity-log-details-div").show();
    var keywordjsonNo  = $('#keyword-follow-user-data-append').attr('keyword-follow-json');
    var keywordStart   = $('#keyword-follow-user-data-append').attr('keyword-follow-start');
    var keywordLastKey = $('#keyword-follow-user-data-append').attr('keyword-follow-last-keyword');
    var ajaxCall       = $('#keyword-follow-user-data-append').attr('ajaxCallFlag');
    var dataEnd        = $('#keyword-follow-user-data-append').attr('pageScrollEndFlag');
    var DataExistLoopCount = parseInt($('#keyword-follow-user-data-append').attr('followingDataExistCount'));
    var loadSuccess    = false;
    if (ajaxCall       == "true" && dataEnd != "true") {
        $.ajax({
            type:     'POST',
            dataType: 'HTML',
            data:     {
                jsonFileNo: keywordjsonNo, jsonFileInnerNo: keywordStart, lastKeyword: keywordLastKey, DataExistLoopCount: DataExistLoopCount
            },
            async:      false,
            url:        rootUrl+'views/social/getActivtyfollowKeywordData.php',
            success:    function(data) {
                loadSuccess = true;
                $('#change-password-details').html('');
                $(".profile-details-div").hide();
                if (keywordLastKey == ''){
                    $('#activity-log-details').html(data);
                } else {
                    $('#activity-log-details').append(data);
                }
                var newKeywordJson          = $('#keyword-follow-frontendArray-'+keywordLastKey).attr('keyword-follow-started-json');
                var newKeywordStart         = $('#keyword-follow-frontendArray-'+keywordLastKey).attr('keyfollow-start');
                var newKeywordDataEnded     = $('#keyword-follow-frontendArray-'+keywordLastKey).attr('keywoFollowDataEnd');
                var newKeywordEndingKeyword = $('#keyword-follow-frontendArray-'+keywordLastKey).attr('keyword-follow-prev-keyword');
                var DataExistCount          = $('#keyword-follow-frontendArray-'+keywordLastKey).attr('DataExistCount');
                console.log(newKeywordJson+' & '+newKeywordStart+' & '+newKeywordDataEnded+' & '+newKeywordEndingKeyword);
                $('#keyword-follow-user-data-append').attr('keyword-follow-json', newKeywordJson);
                $('#keyword-follow-user-data-append').attr('keyword-follow-start', newKeywordStart);
                $('#keyword-follow-user-data-append').attr('keyword-follow-last-keyword', newKeywordEndingKeyword);
                $('#keyword-follow-user-data-append').attr('pageScrollEndFlag', newKeywordDataEnded);
                $('#keyword-follow-user-data-append').attr('followingDataExistCount',DataExistCount);
                $('#activity-log-details').show();
                getUserActivityCount();
            },
            beforeSend: function(data) {
                $('#keyword-follow-user-data-append').attr('ajaxCallFlag','false');
                if (keywordLastKey){
                    $('#ajaxLoader').show();
                }
                $("#followedKeywordActivityDivLoder").show();
            },
            complete:   function(data) {
                $('#keyword-follow-user-data-append').attr('ajaxCallFlag','true');
                $('#ajaxLoader').hide();
                $("#followedKeywordActivityDivLoder").hide();
            },
            error:      function(data) {
                loadSuccess = false;
                showToast("failed", 'Error in Connection');
            }
        });
    }
    return loadSuccess;
}

function getUserActivityCount() {
    var loadSuccess = false;
    $.ajax({
        type:       'POST',
        dataType:   'JSON',
        async:      false,
        url:        rootUrl+'controllers/social/getActivityCount.php',
        success:    function(data) {
            loadSuccess = true;
            if (data.errCode == -1) {
                $('#likeDislikeActivityCount').text('('+data.errMsg.likeCount+')');
                $('#commentActivityCount').text('('+data.errMsg.CommentPost+')');
                $('#hiddenPostActivityCount').text('('+data.errMsg.hiddenPost+')');
                $('#bookmarkPostActivityCount').text('('+data.errMsg.bookmarkCount+')');
                $('#followedPeopleActivityCount').text('('+data.errMsg.followPeople+')');
                $('#followedKeywordActivityCount').text('('+data.errMsg.followKeywords+')');
            } else if (data.errCode == 100) {
                window.location = rootUrl;
            }
        },
        beforeSend: function() {

        },
        complete:   function() {

        },
        error:      function(data) {
            loadSuccess = false;
            showToast("failed", 'Error in Activity Connection');
        }
    });
    return loadSuccess;

}

/****************************************** ACTIVITY LOG END *********************************************/

/****************************************** ajaxSuggestedFollow *********************************************/
function ajaxSuggestedFollow(elem,accHandle,email) {
    console.log(elem);
    var blockCheck = getBlockedUser(email);
    if (blockCheck) {
        $(elem).css('pointer-events', 'none');
        var followingCount = $('#followingCount').text();
        $.ajax({
            url: '../../controllers/social/setJsonController.php',
            type: 'POST',
            dataType: 'json',
            data: ({user_email: email}),
            success: function (data) {
                if (data.errCode == -1) {
                    $('#followingCount').html(Number(followingCount) + 1);
                    showToast("success", 'User Followed Successfully');
                    getSuggestedUserData();
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else if (data.errCode == 120) {
                    showToast("failed", 'Something Went Wrong!!');
                    window.location.href = rootUrl + "views/user/ft_follow.php";
                } else {
                    showToast("failed", 'User ' + buttonText + ' Failed');
                }
            }
        });
    }
}

function getSuggestedUserData() {
    $.ajax({
        type:     'POST',
        dataType: 'HTML',
        url:      '../../views/social/suggestedUser.php',
        success:  function(data) {
            $('#suggested-user-details').html(data);
        },
        error: function(data) {
            console.log(data);
            //showToast("failed", 'failed to Load Popular Post.');
        },
    });
}

// to remove userdiv from suggested data
function clearSuggestedUser(id) {
    if(id) {
        // $('#suggested'+id).hide('slide', {direction: 'left'}, 1000);
        $( '#suggested'+id ).slideUp('slow', function(){
            $( '#suggested'+id ).remove();
            var suggestHtml = $('.suggested-module').html();
            suggestHtml = suggestHtml.trim();
            if (suggestHtml == '') {
                var suggestAppendHtml = '<div id = "suggestedAppendedSlider" style = "display:none;"><center>Users Not Available</center></div>';
                $('.suggested-module').html(suggestAppendHtml);
                $('#suggestedAppendedSlider').slideDown(200);

            }
        });
        // $( '#suggested'+id ).remove();

    }
}

/********************************************Mutual Keywordlist **********************************************/
function loadMutualKeyword(type, email) {
    var jsonFileNo = $('#followingDataAppend').attr('followingDataAppendCount');
    var jsonFileInnerNo = $('#followingDataAppend').attr('followingDataLoopCount');
    var scrollAllow = $('#followingDataAppend').attr('pageScrollEndFlag');
    var ajxCallFlag = $('#followingAjaxCall').attr('ajxCallFlag');
    var divAppendId = $('#followingdivAppendId').text();
    if (scrollAllow == 'false') {
        scrollAllow = 'true';
    }
    if (scrollAllow != 'false' & ajxCallFlag != 'false') {
        $('#followingAjaxCall').attr('ajxCallFlag', 'false');
        $.ajax({
            type: 'POST',
            dataType: 'html',
            data: ({ 'type': type, 'jsonFileNo': jsonFileNo, 'jsonFileInnerNo': jsonFileInnerNo, 'divAppendId': divAppendId }),
            url: '../../views/social/keywordMutualList.php?email=' + email + '&type=' + type,
            success: function(postdata) {
                $('#ajaxLoader').hide();
                if (jsonFileNo == '') {
                    $('#followings-data').html(postdata);
                } else {
                    $('#followings-data').append(postdata);
                }

                $('#followingAjaxCall').attr('ajxCallFlag', 'true');
                var jsonValue = $('#followingAppend' + divAppendId).attr('jsonFileNo');
                if (jsonValue == 0) {
                    jsonValue = '';
                }
                var jsonValueReadNo = $('#followingAppend' + divAppendId).attr('fileReadNo');
                if (jsonValueReadNo == 0) {
                    jsonValueReadNo = "";
                }
                var pageScrollEnd = $('#followingAppend' + divAppendId).attr('pageScrollEnd');
                $('#followingDataAppend').attr('followingDataAppendCount', jsonValue);
                $('#followingDataAppend').attr('followingDataLoopCount', jsonValueReadNo);
                $('#followingDataAppend').attr('pageScrollEndFlag', pageScrollEnd);
                $('#followingdivAppendId').text(parseInt(divAppendId) + 1);
                $('#followings-data').show();
                $('#follow-data').hide();
            },

            beforeSend: function(postdata) {
                if (jsonFileNo != '') {
                    $('#ajaxLoader').show();
                }
            },
            error: function(postdata) {
                alert('error on Loading Timeline');
            }
        });
    }
}
/********************************************End Mutual list  *******************************************/

/*************************************** Desktop Show Notification************************************/

function getDesktopNotificationTost() {

    $.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: '../../views/notification/desktopNotification.php',
        success: function(data) {
            if (data.errCode == -1) {
                for(var i in data.errMsg) {
                    var rawText = data.errMsg[i].notification_body;
                    var notifyMsg = rawText.replace(/<\/?a[^>]*>/g, "");
                    console.log(notifyMsg);
                    showToast("Suceess", notifyMsg, 20000);
                }
            } else if (data.errCode == 100) {
                window.location = rootUrl;
            } else if (data.errCode == 2 || data.errCode == 1) {
                showToast("Failed", 'Failed To Load Notifications');
            }
        },
        beforeSend: function(data) {

        },
        error: function(data) {
            showToast("Failed", 'Error In Connection');
        }
    });
}

/*************************************** Desktop Show Notification************************************/


/*=================================================
 =            Stacked bootstrap modal             =
 =================================================*/
// $('.modal').on('shown.bs.modal', function(e) {
//     var date = new Date();
//     console.log("shown.bs.modal", date.getTime() );
// });
$('.modal').on('shown.bs.modal', function(e) {
    var modalzIndex=1050;
    var zIndex =1040;
    $.each($('.modal-backdrop'), function(index, val) {
        zIndex += index * 10;
        // console.log("modal-backdrop =========",$(this), zIndex);
        $(this).css('z-index', zIndex);
    });
    $.each($('.modal.fade.in'), function(index, val) {
        // modalzIndex = parseInt($(this).css('z-index'));
        modalzIndex += index * 10;
        // console.log( ".modal.fade.in =========",$(this), modalzIndex);
        $(this).css('z-index', modalzIndex);
    });
})
/*=====  End of Stacked bootstrap modal   ======*/



function loadPopularPostDetailPage(redirectUrl){
    window.open(redirectUrl, '_blank');
}

function loadOtherPostDetailPage(redirectUrl){
    window.open(redirectUrl, '_blank');
}

function loadRelatedPostDetailPage(redirectUrl){
    window.open(redirectUrl, '_blank');
}


function getBlockedUser(email){
    var blockFlag = false;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {emailId: email},
        url: rootUrl + 'controllers/social/getBlockedUser.php',
        async: false,
        success: function (data) {
            if (data.errCode == 50) {
                blockFlag = true;
            } else if(data.errCode == 120){
                showToast("failed", 'Something Went Wrong!!');
                $('#showModalPost').modal('hide');
                window.location.href = rootUrl + "views/user/ft_follow.php";
            }else {
                showToast("failed", 'Something Went Wrong!!');
                $('#showModalPost').modal('hide');
                postLoadAsPerUrl();
                blockFlag = false;
            }
        },
        error: function() {
            blockFlag = false;
        }
    });
    return blockFlag;
}

function removePostOfUser(postId, type, postTime, email){
    var removeFlag = false;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: { postId: postId, type: type, postTime: postTime,email: email },
        url: '../../controllers/social/removePostForAllUser.php',
        async: false,
        success: function(data) {
            $(".yes-remove-block-btn").removeAttr("disabled");
            if (data.errCode == -1) {
                removeFlag = true;
            } else if (data.errCode == 100) {
                window.location = rootUrl;
                removeFlag = false;
            } else if (data.errCode == 4) {
                postDeletedModal('This Post No longer Exist');
                postLoadAsPerUrl();
                removeFlag = false;
            }
        },
        error: function() {
            $(".yes-remove-post-btn").removeAttr("disabled");
            showToast("failed", 'Post not removed. Please try again.');
            removeFlag = false;
        }
    });

    return removeFlag;
}

function hidePostOfUser(postId, type, postTime, email){
    var hideFlag = false;
    $.ajax({
        type: 'POST',
        url: '../../controllers/social/hidePostOfUser.php',
        dataType: 'json',
        async: false,
        data: { 'postId': postId, 'type': type, 'postTime': postTime, 'email': email },
        success: function(data) {
            if (data.errCode == -1) {
                hideFlag = true;
            }else{
                postDeletedModal('This Post No longer Exist');
                postLoadAsPerUrl();
                hideFlag = false;
            }
        },
        error: function() {
            hideFlag = false;
        }
    });

    return hideFlag;
}



/// load anaytic page

// JS Function to get all Post from api to Show on private timeline.
function loadAnanyticPage(type) {
    var dataCount = $("#load-analytic-next-post-data").attr("data-count");
    var lastDataCreateTime = $("#load-analytic-next-post-data").attr("data-create-time");
    $.ajax({
        type: 'POST',
        dataType: 'html',
        data: ({ 'type': type, 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime }),
        url: '../../keywords/user_dashboard/loadUploadContent.php',
        success: function(postdata) {
            $('#show-error-message').hide();
            if (lastDataCreateTime == "") {
                $('#load-analytic-post-data').html(postdata);
            } else {
                $('#load-analytic-post-data').append(postdata);
            }
            $("#load-analytic-next-post-data").attr("data-create-time", $("#load-analytic-last-data-time-" + dataCount).attr('lastDataTime'));

            $("#load-analytic-next-post-data").attr("data-type", $("#load-analytic-last-data-time-" + dataCount).attr('datatype'));

            $("#load-analytic-next-post-data").attr("data-status-empty", $("#load-analytic-last-data-time-" + dataCount).attr('data-empty'));
            $("#load-analytic-next-post-data").attr("data-count", parseInt(dataCount) + 1);
        },
        beforeSend: function() {
            $("#load-analytic-next-post-data").attr("data-scroll-allow", "false");
        },
        complete: function() {
            $('#ajaxLoader1').hide();
            $("#load-analytic-next-post-data").attr("data-scroll-allow", "true");
        },
        error: function() {
            // alert('error on Loading Timeline');
            $('#loading-content-div').hide();
            $('#show-error-message').text('No Post Yet!!');
        }
    });
}



// JS Function to get all Post from api to Show on private timeline.
function loadConsumptionPage(type) {
    var dataCount = $("#load-consumption-next-post-data").attr("data-count");
    var lastDataCreateTime = $("#load-consumption-next-post-data").attr("data-create-time");
    $.ajax({
        type: 'POST',
        dataType: 'html',
        data: ({ 'type': type, 'dataCount': dataCount, 'lastDataCreateTime': lastDataCreateTime }),
        url: '../../keywords/user_dashboard/load_consumption.php',
        success: function(postdata) {
            $('#show-error-message').hide();
            if (lastDataCreateTime == "") {
                $('#load-consumption-post-data').html(postdata);
            } else {
                $('#load-consumption-post-data').append(postdata);
            }
            $("#load-consumption-next-post-data").attr("data-create-time", $("#load-consumption-last-data-time-" + dataCount).attr('lastDataTime'));

            $("#load-consumption-next-post-data").attr("data-type", $("#load-consumption-last-data-time-" + dataCount).attr('datatype'));

            $("#load-consumption-next-post-data").attr("data-status-empty", $("#load-consumption-last-data-time-" + dataCount).attr('data-empty'));
            $("#load-consumption-next-post-data").attr("data-count", parseInt(dataCount) + 1);
        },
        beforeSend: function() {
            $("#load-consumption-next-post-data").attr("data-scroll-allow", "false");
        },
        complete: function() {
            $('#ajaxLoader1').hide();
            $("#load-consumption-next-post-data").attr("data-scroll-allow", "true");
        },
        error: function() {
            // alert('error on Loading Timeline');
            $('#loading-content-div').hide();
            $('#show-error-message').text('No Post Yet!!');
        }
    });
}


// //to follow and unfollow User and create Json
function ajaxAppFollowEventForFTU(handler, email) {
    var buttonText = $("#" + handler).val();
    var followingCount = $('#followingCount').text();
    var followedCheck = getFollowedUser(email);
    if (followedCheck) {
        $.ajax({
            url: '../../controllers/social/setJsonController.php',
            type: 'POST',
            dataType: 'json',
            data: ({ user_email: email }),
            success: function(data) {
                if (data.errCode == -1) {
                    if (buttonText == 'Unfollow') {
                        $("#" + handler).attr('value', "Follow");
                        $(".otherFollowDropDown").html("<i class='fa fa-user'></i>&nbspFollow User");
                        $('#followingCount').html(followingCount - 1);
                        $("#" + handler).removeClass('btn-social-dark');
                        $("#" + handler).addClass('btn-social');
                    } else {
                        $("#" + handler).attr('value', "Unfollow");
                        $(".otherFollowDropDown").html("<i class='fa fa-user-times'></i>&nbspUnfollow User");
                        $('#followingCount').html(Number(followingCount) + 1);
                        $("#" + handler).addClass('btn-social-dark');
                        $("#" + handler).removeClass('btn-social');
                    }
                    showToast("success", 'User ' + buttonText + 'ed' + ' Successfully');
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else {
                    showToast("failed", 'User ' + buttonText + ' Failed');
                }
            },
            beforeSend: function() {
                $(".follow-unfollow-button").prop("disabled", true);
            },
            complete: function() {
                $(".follow-unfollow-button").prop("disabled", false);
            },
            error: function(data) {
                showToast("failed", 'Error in Connection');
            }
        });

    } else {
        showToast("failed", 'User is already Followed!!');
        $("#" + handler).attr('value', "Unfollow");
        $(".otherFollowDropDown").html("<i class='fa fa-user-times'></i>&nbspUnfollow User");
        $('#followingCount').html(Number(followingCount) + 1);
        $("#" + handler).addClass('btn-social-dark');
        $("#" + handler).removeClass('btn-social');
        postLoadAsPerUrl();
    }

}


function getFollowedUser(email){
    var blockFlag = false;
    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {emailId: email},
        url: rootUrl + 'controllers/social/getFollowedUser.php',
        async: false,
        success: function (data) {
            
            if (data.errCode == 50) {
                blockFlag = true;
            } else {
                blockFlag = false;
            }
        },
        error: function() {
            blockFlag = false;
        }
    });
    return blockFlag;
}
