$(document).ready(function() {
    //display Apps first
    $('#app').tab('show');
    $("#errorBlank").hide();
    $("#errorSearch").hide();
});

//to activate selectpicker dropdown
$(function() {
    $('.selectpicker').selectpicker({
        //options will be here
    });
});


//js function to load all app on fav app modal
function suggestList()
{
  $('.userFavAppIdInput').autocomplete({
      source: '../../controllers/app/searchController.php'
  });

  var userFavAppIdInput     = document.getElementsByClassName("userFavAppIdInput");
  userFavAppIdInput.onkeyup = function (e) {
    if (e.keyCode === 13) {
      var changeAppID = $('.userFavAppIdInput').data("changeappid");
      changeAppID     = changeAppID.toString();
      getAppIdByAppName(changeAppID, $('.userFavAppIdInput').data('action'), $('.userFavAppIdInput').data("favappid"), $('.userFavAppIdInput').data("btnchange"));
    }
  }
}


//js to load modal whether for add fav app or change fav app
function favActionModal(changeAppId, action, favAppId, btnChange) {
    $(".modal-content").load("favAppModal.php?changeAppId=" + changeAppId + "&action=" + action + "&favAppId=" + favAppId + "&btnChange=" + btnChange);
    $(".modal-backdrop").show();
    $("#favAppsModal").modal("show");
}

$("#favAppSearchForm").submit(function(e) {
    e.preventDefault();
});

//js to get app id by name
function getAppIdByAppName(changeAppId, action, favAppIds, btnChange) {
    var appName = $('#favAppsModal #userFavAppId').val();
    $.ajax({
        type: 'POST',
        url: '../../controllers/app/getAppIdByName.php',
        dataType: 'json',
        data: { 'appName': appName },
        success: function(data) {
            $(".apperror").html("");
            if (data.errCode == -1) {
                var appId = data.appId;
                favBtnAction(appId, action, favAppIds, btnChange, changeAppId);
            } else {
                $(".apperror").html(data.errMsg);
            }
        }
    });
}


// js to add/change/remove favourite user app/s
function favBtnAction(appId, action, favAppId, btnChange, changeAppId) {
    //add fav app
    if (action == 'add') {
        if (favAppId != "") {
            var userFavAppIds = favAppId + ',' + appId;
        } else {
            var userFavAppIds = appId;
        }

        //check fav count
        var countStatus = favCountCheck(userFavAppIds, action);
        if (countStatus == 'true') {
            //btn change to remove if btnChange true
            if (btnChange == 'true') {
                $(".changeText").text('Remove');
                $(".changeText").attr('onclick', "favBtnAction('" + appId + "', 'remove', '" + favAppId + "', 'true');");
            }
        }
    }

    //change fav app
    if (action == 'change') {
        var countStatus = 'true'
        var arrReplaceFav = changeAppId.split(',');
        var arrOldFav = favAppId.split(',');
        var arrOldFavStr = arrOldFav.toString();
        var replaceAppId = arrOldFavStr.replace(arrReplaceFav, appId);
        var userFavAppIds = replaceAppId;
    }
    //remove fav app
    if (action == 'remove') {
        var countStatus = 'true'
        var arrReplaceFav = appId.split(',');
        var arrOldFav = favAppId.split(',');
        var arrNewFav = arrOldFav.filter(function(obj) {
            return arrReplaceFav.indexOf(obj) == -1;
        });
        var userFavAppIds = arrNewFav.toString();
        //btn change to add if btnChange true
        if (btnChange == 'true') {
            $(".changeText").text('Add To Fav');
            $(".changeText").attr('onclick', "favBtnAction('" + appId + "', 'add', '" + favAppId + "', 'true');");
        }
    }
    //if count status is tru than execute
    if (countStatus == 'true') {
        $.ajax({
            type: 'POST',
            url: '../../controllers/app/setFavApp.php',
            dataType: 'html',
            data: { 'appId': appId, 'action': action, 'actionType': "setApp", 'changeAppId': changeAppId },
            success: function(data) {
              var email = $('#login-header-start').attr('user-email');
                loadSearchApp(email, rootUrl)
                if (btnChange != 'true') {
                    $('.modal-backdrop, #favAppsModal').hide();
                    var classId = "appfavclass";
                    activeApp(classId);
                }
            }
        });
    }
}


// function to check fav app count & check for max count to 6
function favCountCheck(userFavAppIds, action) {
    var checktype = "";
    $.ajax({
        type: 'POST',
        url: '../../controllers/app/favAppCountCheck.php',
        dataType: 'html',
        async: false,
        data: { 'userFavAppIds': userFavAppIds, 'action': action, 'actionType': "checkCount" },
        success: function(data) {
            var jsondata = JSON.parse(data);
            if (jsondata["errCode"] == 5) {
                checktype = "false";
                $(".apperror").html('You can not choose more than six favorite apps.');
            } else {
                checktype = "true";
            }
        }
    });
    return checktype;
}





function defaultBtnAction(appId) {
    var login_statusfeatures = '<?php echo $login_status; ?>';
    var email = '<?php echo $email; ?>';
    if (login_statusfeatures == 0) {
        //  console.log("status 0");
    } else {
        var defaultVal = $("#default-div").text();
        if (defaultVal == appId) {} else {
            $.ajax({
                type: 'POST',
                url: '../../controllers/app/setDefaultAppController.php',
                dataType: 'html',
                data: { 'appId': appId },
                success: function(data) {
                    console.log(defaultVal);
                    $("#default-div").html(defaultVal);
                    $("#default-div").attr('id', 'set-default-div-' + defaultVal);
                    $("#set-default-div-" + appId).html(appId);
                    $("#set-default-div-" + appId).attr('id', 'default-div');

                    $("#defaultBtn-" + appId).addClass('bg-color-Red');

                    $("#defaultBtn-" + appId).html("Default");
                    $("#defaultAppBtn-" + defaultVal).css('background-color', '#009eeb');
                    $("#defaultAppBtn-" + defaultVal).html("Set Default");
                    $("#defaultAppBtn-" + defaultVal).attr('id', 'defaultBtn-' + defaultVal);
                    $("#defaultBtn-" + appId).attr('id', 'defaultAppBtn-' + appId);
                }
            });
        }
    }
}


//to call fav app content on click
$("#appfavclass").on('shown.bs.tab', function() {
    var classId = "appfavclass";
    activeApp(classId);
});


//to call on app click
$("#appdefclass").on('shown.bs.tab', function() {
    var classId = "appdefclass";
    activeApp(classId);
});


//function for activeApp class
function activeApp(classId) {
    $.ajax({
        url: 'loadAppList.php',
        type: 'POST',
        dataType: 'html',
        data: ({ 'reloadId': classId }),
        success: function(data) {
            if (classId == "appdefclass") {
                $("#upadteApp").html("");
                $("#upadteApp").html(data);
            } else if (classId == "appfavclass") {
                $("#updateFavApp").html("");
                $("#updateFavApp").html(data);
                changeColorplus();
            }
            $("html, body").animate({ scrollTop: 0 }, 400);
        }
    });
}

/*
Showing and hiding the plus button and
changing the color of count
For hiding the plus button and
applying red color to the count
*/
function changeColorplus() {
    //For the ajax in function Active app
    var divCount = $(".favApp").length;
    if (divCount === 6) {
        $("#hideFav").hide();
        $("span.favCount").css('color', 'red');
    } else {
        $("#hideFav").show();
        //$( "span.favCount" ).css('color', '#09c344');
    }
}
//Autocomplete to search App
$(function() {
    $("#appSuggest").autocomplete({
        source: '../../controllers/app/searchController.php'
    });
});

//apps searches
function ajaxAppInfo(page) {
    var searchData = page.toLowerCase();
    $.ajax({
        url: 'appinfo.php',
        type: 'POST',
        dataType: 'html',
        data: ({ appInfoAjaxPageid: searchData }),
        success: function(data) {
            $("#upadteApp").html(data);
            $("#errorSearch").hide();
            $("html, body").animate({ scrollTop: 0 }, 400);
            window.location.hash = "#app";
        }
    });
}


//appends hash for reloading
$(function() {
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function(e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
});

//load widget
$("#appclass").click(function() {
    $.ajax({
        url: 'loadWidgetList.php',
        type: 'POST',
        dataType: 'html',
        data: ({ 'reloadId': "appdefclass" }),
        success: function(data) {
            $("#widget").html(data);
            $("html, body").animate({ scrollTop: 0 }, 400);
        }
    });
});
