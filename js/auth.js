/*
*-----------------------------------------------------------------------------------------------------------
*       Authentication javascript
*-----------------------------------------------------------------------------------------------------------
*   This authentication javascript is used to make an ajax request by accessing the methods.
*   This includes :
*
*   signupSubmit()        :   The signupSubmit() method is used to create new account on request
*                             of registerController.php controller using ajax request.
*
*   login()               :   The login method is used to authenticate the valid user on request of
*                             loginController.php controller using ajax request by taking parameter
*                             as email and password.
*
*   forgetPassword()      :   The forgetPassword() method is used to restore the user's forgot
*                             password on request of forgetPasswordController.php controller using ajax
*                             ajax request by taking parameter as email address.
*
*   resendActivation()    :   The resendActivation() method is used to activate the user's account on
*                             on request of resendActivationController.php controller using ajax request
*                             taking parameter as email address.
*/

/*
*-----------------------------------------------------------------------------------------------------------
*   Function signupSubmit:
*-----------------------------------------------------------------------------------------------------------
*   Fucntion Name :   signupSubmit()
*   Purpose       :   To create new account.
*   Controller    :   registerController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Mongo Database.
*/

function signupSubmit(){

    var validator = $('#registerForm').data('bootstrapValidator');
    validator.validate();
    console.log(validator)
    if(validator.isValid() && $("#email").parent().hasClass('has-success'))
    {
        console.log("valid")
        var name=$.trim($("#firstName").val());
        var name = name.toString();
        var lastname=$.trim($("#lastName").val());
        var lastname = lastname.toString();
        var referral=$("#referal").val();
        var email=$("#email").val();
        var password=$("#password").val();
        var confPwd=$("#confirmPassword").val();
        var handle=$("#accountHandle").val();
        var country = '';
        var city = '';
        var agree = $("#agree").is(":checked");
        var gender = '';

        // var name=$.trim($("#firstName").val());
        // var name = name.toString();
        // var lastname=$.trim($("#lastName").val());
        // var lastname = lastname.toString();
        // var referral=$("#referal").val();
        // var email=$("#email").val();
        // var password=$("#password").val();
        // var confPwd=$("#confirmPassword").val();
        // var handle=$("#accountHandle").val();
        // var country = $("#country option:selected").val();
        // var city = $("#city option:selected").val();
        // var agree = $("#agree").is(":checked");
        // var gender = $('input[name=gender]:checked').val();

        $.ajax({
            type: "POST",
            dataType:"json",
            url: "../../controllers/user/registerController.php",
            beforeSend: function() {
              $('#registerbutton').html('Signing Up...').prop('disabled', false);
                $('#registerbutton').prop('disabled', true);
                $('.loading-icon').html("<i class='fa fa-spinner fa-pulse fa-fw'></i>");
              $('#signup').html('Signing Up...').prop('disabled', false);
                $('#signup').prop('disabled', true);
                $('.loading-icon').html("<i class='fa fa-spinner fa-pulse fa-fw'></i>");

            } ,
            complete: function(){
                $('#registerbutton').html('Sign Up').prop('disabled', false);
                $('.loading-icon').html("")
                $('#signup').html('Sign Up').prop('disabled', false);
                $('.loading-icon').html("")
            },
            data: {
                email: encodeURIComponent(email),first_name: name,last_name: lastname, gender: gender, password: password,confirm_password: confPwd,referral: encodeURIComponent(referral), agree: agree, country: country, city: city, requestType: "signup", handle: handle, grecaptcharesponse: encodeURIComponent(grecaptcha.getResponse())
                //email: encodeURIComponent(email),first_name: name,last_name: lastname,password: password,confirm_password: confPwd,mobile_number: txtMobile,referral: encodeURIComponent(referral),country: country,client_IP: client_ip
            },
            async: true,
            success:function(data){
                if(data.errCode == -1){
                	$("#registerForm").data('bootstrapValidator').resetForm();
                    customResetForm("registerForm");
                    showToast("success",data.errMsg);
                }else{
                    $('input[type="password"]').val("");
                    $('#password').parent().attr('class','has-error has-feedback form-group');
                    $('#password').parent().find("span").attr('class','glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross');
                    $('#confirmPassword').parent().attr('class','has-error has-feedback form-group');
                    $('#confirmPassword').parent().find("span").attr('class','glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross');
                    showToast("failed",data.errMsg);
                }
            },
            error: function (xhr, status, error) {
                // showToast("failed",data.errMsg);
            }
        });
      }
    else{
      /*alert("not valid");*/
      return;
    }
}

$("#email").keyup(function(e){

  //ajax call for checking register email address
  if (e.which != 32) {
    if (e.which == 8 && $.trim($(this).val()) == "") {
      $(this).parent().attr("class", "has-error has-feedback form-group");
      $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
      $(this).removeClass("has-loader");
    }else if(checkEmailAdd($(this).val())){
      if($("#email").val().length <= 40){
        var elem = this;
          $.ajax({
              type: "POST",
              dataType: "json",
              url: "../../controllers/user/registerController.php",
              data: {
                  requestType: "email",
                  requestValue: $(this).val()
              },
              beforeSend : function(){
                  $(elem).parent().tooltip("hide");
                  $(elem).addClass("has-loader");
                  $(elem).parent().attr("class", "has-error has-feedback form-group");
                  $(elem).siblings().attr("class", "fa fa-spinner fa-pulse fa-1x fa-fw form-control-feedback email-fa-error");
             },
              complete: function(){

              },
              async: true,
              success: function(data) {
                  var handle = $("#accountHandle").val();
                  checkHandleData(handle);
                  
                  if (data.errCode == -1) {
                    if(data.errMsg == "BLOCKED"){
                      toolTipShow(elem, 'Domain is blocked');
                      $(elem).removeClass("has-loader");
                      $(elem).parent().attr("class", "has-error has-feedback form-group");
                      $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                    }else{
                      toolTipShow(elem, '');
                      $(elem).removeClass("has-loader");
                      $(elem).parent().attr("class", "has-success has-feedback form-group");
                      $(elem).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
                    }
                  } else {
                    toolTipShow(elem, 'Email address is already exist');
                    $(elem).removeClass("has-loader");
                    $(elem).parent().attr("class", "has-error has-feedback form-group");
                    $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                  }
              },
              error: function(xhr, status, error) {
                $(elem).removeClass("has-loader");
                $(elem).parent().attr("class", "has-error has-feedback form-group");
                $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
              }
          });
      }else{
        toolTipShow(this, 'Email address length is too long');
        $(this).removeClass("has-loader");
        $(this).parent().attr("class", "has-error has-feedback form-group");
        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
      }
    }else{
        toolTipShow(this, 'Email address is invalid');
        $(this).removeClass("has-loader");
        $(this).parent().attr("class", "has-error has-feedback form-group");
        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
      }
    } else {
        $(this).removeClass("has-loader");
        $(this).val($(this).val().replace(/\s+/g, ''));
    }
});

function blockSpecialChar() {
	var str =  String(e).replace(/&/g, '')
						.replace(/</g, '')
						.replace(/>/g, '')
						.replace(/"/g, '')
						.replace(/'/g, '')
						.replace(/!/g, '')
						.replace(/%/g, '');
  $("#accountHandle").val(str);
  return str;

//	var regex = /^[0-9a-zA-Z\_\.]+$/
}
function isDoubleByte(str) {
    for (var i = 0, n = str.length; i < n; i++) {
        if (str.charCodeAt( i ) > 255) { $("#accountHandle").val(""); }
    }
}

//account handle check availability and validations
$('#accountHandle').blur(function(){
    var handle = $("#accountHandle").val();
    checkHandleData(handle);
});


function checkHandleData(handle)
{
    var accountHandleString = handle;
    if(accountHandleString != "" && accountHandleString.length >= 6 && accountHandleString.length <= 16){
        if(!$.isNumeric(accountHandleString)){
            var pattern = new RegExp(/^([a-zA-Z0-9\.\_]+)$/);
            if(pattern.test(accountHandleString) && (accountHandleString.match(/[\.\_]/g) || []).length < 2){
                var handle = $("#accountHandle").val();
                handle = $.trim(handle);
                var elem = "#accountHandle";
                $(elem).parent().tooltip("hide");

                var emailUser=$("#email").val();

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../controllers/user/registerController.php",
                    data: {
                        requestType: "account_handle", requestValue: handle,emailUser:emailUser
                    },
                    beforeSend: function () {
                        $(elem).parent().tooltip("hide");
                        $(elem).addClass("has-loader");
                        $(elem).parent().attr("class", "has-error has-feedback form-group");
                        $(elem).siblings().attr("class", "fa fa-spinner fa-pulse fa-1x fa-fw form-control-feedback email-fa-error");
                    },
                    complete: function () {

                    },
                    async: true,
                    success: function (data) {

                        if (data.errCode == -1) {
                            toolTipShow(elem, '');
                            $(elem).removeClass("has-loader");
                            $(elem).parent().attr("class", "has-success has-feedback form-group");
                            $(elem).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
                        } else {
                            //if(data.errMsg.failed !="")
                            toolTipShow(elem, 'Sorry! this Handle Id is already in use. Kindly sign-up using a different Handle Id.');


                            $(elem).removeClass("has-loader");
                            $(elem).parent().attr("class", "has-error has-feedback form-group");
                            $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                        }
                    },
                    error: function (xhr, status, error) {
                        console.log(error)
                        $(elem).removeClass("has-loader");
                        $(elem).parent().attr("class", "has-error has-feedback form-group");
                        $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                    }
                });
            }else{
                toolTipShow('#accountHandle', 'Only one special characer allowed either "." or "_"');
                $('#accountHandle').parent().attr("class", "has-error has-feedback");
                $('#accountHandle').siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            }
        }else{
            toolTipShow('#accountHandle', 'At least one alphabet is required. You may enter numbers and special characters( . _ )');
            $('#accountHandle').parent().attr("class", "has-error has-feedback");
            $('#accountHandle').siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
        }

    }else{
        toolTipShow('#accountHandle', 'Accout handle should be between 6 to 16 characters(inclusive)');
        $('#accountHandle').parent().attr("class", "has-error has-feedback");
        $('#accountHandle').siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }
}
//Check for account handle name existence
$('#accountHandle').keyup(function(e) {
//	blockSpecialChar($("#accountHandle").val());
  isDoubleByte($("#accountHandle").val());// FOR HANDLING UTF CHARACTERS

  if (e.which != 32) {
    if (e.which == 8 && $(this).val() == "") {
      $(this).parent().attr("class", "has-error has-feedback form-group");
      $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }else if($(this).val().length >= 6 && $(this).val().length <= 16){
      $(this).parent().tooltip('hide');
    }else{
      toolTipShow(this, 'Accout handle should be between 6 to 16 characters(inclusive)');
      $(this).parent().attr("class", "has-error has-feedback form-group");
      $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }
  }else {
      $(this).val($(this).val().replace(/\s+/g, ''));
  }

});

//check referal code on focus out
$("#referal").blur(function(e){
  var referal = $("#referal").val();
  referal = $.trim(referal);
  var elem = this;
  if($.trim($(this).val()) != ""){
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controllers/user/registerController.php",
        data: {
            requestType: "referal", requestValue: referal
        },
        beforeSend: function () {
          $(elem).parent().tooltip("hide");
          $(elem).addClass("has-loader");
          $(elem).parent().attr("class", "has-error has-feedback form-group");
          $(elem).siblings().attr("class", "fa fa-spinner fa-pulse fa-1x fa-fw form-control-feedback email-fa-error");
        },
        complete: function () {
        },
        async: true,
        success: function (data) {
            if (data.errCode == -1) {
              //toolTipShow(elem, '');
                $(elem).removeClass("has-loader");
                $(elem).parent().attr("class", "has-success has-feedback form-group");
                $(elem).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
            } else {
              toolTipShow(elem, 'Invalid Referral code');
              $(elem).removeClass("has-loader");
              $(elem).parent().attr("class", "has-error has-feedback form-group");
              $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            }
        },
        error: function (xhr, status, error) {
          $(elem).removeClass("has-loader");
          $(elem).parent().attr("class", "has-error has-feedback form-group");
          $(elem).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
        }
    });
  }else{
    toolTipShow(elem, '');
    $(elem).removeClass("has-loader");
    $(elem).parent().attr("class", "form-group");
    $(elem).siblings().attr("class", "");

  }

})
// Check for account referal id existence
// $('#referal').keyup(function(e) {
//     //console.log(e.which);
//     if (e.which != 32) {
//         if (e.which == 8 && $.trim($(this).val()) == "") {
//             $(this).parent().attr("class", "");
//             $(this).siblings().attr("class", "");
//         }else{
//             toolTipShow(this, 'Invalid Referral code');
//             $(this).removeClass("has-loader");
//             $(this).parent().attr("class", "has-error has-feedback");
//             $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
//         }
//     }else {
//         $(this).val($(this).val().replace(/\s+/g, ''));
//     }
//
// });

/*
*-----------------------------------------------------------------------------------------------------------
*   Event : keypress()
*-----------------------------------------------------------------------------------------------------------
*   Event Name    :   $('#accountHandle').keypress()
*   Purpose       :   To check whether email or account handle is available or not.
*   Controller    :   registerController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Node Database.
*/

/*$('#accountHandle').keypress(function() {
    console.log('focusout accountHandle');
    var handle=$("#accountHandle").val();
    handle = $.trim(handle);

    if(handle && handle.length > 2){
        $.ajax({
            type: "POST",
            dataType:"json",
            url: "../../controllers/user/registerController.php",
            data: {
                requestType: "accountHandle", requestValue: handle
            },
            async: true,
            success:function(data){
                console.log(data);

                if(data.errCode == -1){
                    console.log("success");
                }else{
                    console.log("bamboo");
                }
            },
            error: function (xhr, status, error) {
                alert(error);
            }
        });
    }


});*/

/*
*-----------------------------------------------------------------------------------------------------------
*   Event : click()
*-----------------------------------------------------------------------------------------------------------
*   Event Name    :   $("#loginLink").on('click', function(e) {});
*   Purpose       :   To authenticate the valid user name (email) and password.
*   Controller    :   loginController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Node Database.
*/

$("#loginLink").on("click",function(e){//Header login form submit button on enter
  e.preventDefault();
  userLogin(1);
});
$("#internalLoginButton").on("click",function(e){//Inrnal login form submit button on enter
  e.preventDefault();
  userLogin(2);
});
function userLogin(formId){

      var emailHandle, password, errMsgId, resendLinkVisibility;

      if(formId == 1){
            emailHandle = $("#form_prelogin_login_username").val();
            password    = $("#form_prelogin_login_password").val();
            errMsgId    =  'errMsg';
            resendLinkVisibility = 'resend-main-div';

            emailHandleDivId = 'form_prelogin_login_username';
            passwordDivId = 'form_prelogin_login_password';
      }
      if(formId == 2){
            emailHandle = $("#form_login_username").val();
            password    = $("#form_login_password").val();
            errMsgId    =   'loginErrMsg';
            resendLinkVisibility = 'resend-link';

            emailHandleDivId = 'form_login_username';
            passwordDivId = 'form_login_password';
      }

      if(emailHandle == '' && password == ''   ){
            $("#"+errMsgId).html('All fields are mandatory');
            $("#"+emailHandleDivId).focus();
      }else if(emailHandle == ''){
            $("#"+errMsgId).html('All fields are mandatory');
            $("#"+emailHandleDivId).focus();
      }else if(password == ''){
            $("#"+errMsgId).html('All fields are mandatory');
            $("#"+passwordDivId).focus();
      } else{

          var tz = jstz.determine(); // Determines the time zone of the browser client
          var timeZone=tz.name(); // Returns the name of the time zone eg "Europe/Berlin"

          $.ajax({
                  type: "POST",
                  dataType:"json",
                  url: "../../controllers/user/loginController.php",
                  data: {
                      email: encodeURIComponent(emailHandle), password: encodeURIComponent(password),timeZone:timeZone
                  },
                  async: true,
                  beforeSend: function() {
                      $('#loginLink').css('background','url(../../images/login_loader.gif)');
                  } ,
                  complete: function(){
                      $('#loginLink').css('background','url(../../images/right-arrow.png)');
                  },
                  success:function(data){
                      if(data.errCode == -1){

                          //alert('success');
                          var redirectURL = '../../views/dashboard/index.php';
                          /**** Update set user timezone start**********/
                          updateUserTimezone("../../views/time/tzUpdate.php");
                          /**** Update set user timezone end**********/
                          window.location = redirectURL;
                      }else if(data.errCode == 3){
                          $('#'+errMsgId).html('Account is not active.');
                          $('#'+resendLinkVisibility).css('visibility','visible');
                      }else if(data.errCode == 4){
                          $("#"+errMsgId).html('Email / Handle does not exist.');
                      }else if(data.errCode == 6){
                          $("#"+errMsgId).html(data.errMsg);
                      }else if(data.errCode == 15){
                              $("#"+errMsgId).html('User account is blocked');
                      }
                  },
                  error: function (xhr, status, error) {
                      alert(error);
                  }
            });
      }
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Event : click()
*-----------------------------------------------------------------------------------------------------------
*   Event Name    :   $('#resendActivationEmail').click()
*   Purpose       :   To resend activation link on link expired or inactive account.
*   Controller    :   resendActivationController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Mongo Database.
*/

$("#resendActivationEmail").on('click', function(e) {

        e.preventDefault();
        var  email = $('#activateEmail').val();

        if(email != ''){
            if(checkEmailAdd(email)){
              $.ajax({
                    type: "POST",
                    dataType:"json",
                    beforeSend: function() {
                        $('#resendActivationEmail').prop('disabled', true).html("<i class='fa fa-spinner fa-pulse fa-fw'></i>");
                    } ,
                    complete: function(){
                        $('#resendActivationEmail').html('Submit').prop('disabled', false);
                    },
                    url: "../../controllers/user/resendverificationController.php",
                    data: {
                        email: encodeURIComponent(email), flag: 1
                    },
                    async: true,
                    success:function(data){

                        if(data.errCode == -1){
                            showToast("Success","Email Verification link has been sent, Please Verify");
                            $('#erroremailshow').html("");
                            $("#activateEmail").val("");
                        }else if(data.errCode == 15){
                            $("#erroremailshow").html('Your email has blocked due to some security reason. <br />Please contact to customer support.');
                        }else{
                            $("#erroremailshow").html(data.errMsg);
                        }
                    },
                    error: function (xhr, status, error) {
                        alert(error);
                    }
              });
            }else{
               // alert('Not a valid email address');
                $('#erroremailshow').html("Please enter your valid email address");
                $('#activateEmail').focus();
            }
        }else{
            //alert('Mandatory field not found');
            $('#erroremailshow').html("Fields marked with an asterisk (*) are required.");
            $('#activateEmail').focus();
        }
});

/*
*-----------------------------------------------------------------------------------------------------------
*   Event : click()
*-----------------------------------------------------------------------------------------------------------
*   Event Name    :   $("#forgetPassword").on('click', function(e) {});
*   Purpose       :   To send reset password link.
*   Controller    :   forgotPasswordController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Node Database.
*/
$("#forgetPassword").on('click', function(e) {
    e.preventDefault();

    var email = $("#forget_password_email").val();
    var flag=1;

    if(email==''){
      $("#erroremailshow").html('Fields marked with an asterisk (*) are required.');
          $("#forget_password_email").focus();
    }
    else if(!checkEmailAdd(email)){
      $("#erroremailshow").html('Please enter a valid email.');
         $("#forget_password_email").focus();
     }
     else{
          $.ajax({
                type: "POST",
                dataType:"json",
                url: "../../controllers/user/forgotPasswordController.php",
                beforeSend: function() {
                    $('#forgetPassword').prop('disabled', true).html("<i class='fa fa-spinner fa-pulse fa-fw'></i>");
                } ,
                complete: function(){
                    $('#forgetPassword').html('Submit').prop('disabled', false);
                },
                data: {
                    email: encodeURIComponent(email),flag:encodeURIComponent(flag)
                },
                async: true,
                success:function(data){

                    if(data.errCode == -1){
                        showToast("Sucess","A Reset password link has been sent to your email ID, Please verify");
                        $("#forget_password_email").val("");
                    }else{
                        $("#erroremailshow").html(data.errMsg);
                    }
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
          });
    }

});

/*
*-----------------------------------------------------------------------------------------------------------
*   Event : click()
*-----------------------------------------------------------------------------------------------------------
*   Event Name    :   $('#resetPassword').click()
*   Purpose       :   To reset new password on click of password reset email link.
*   Controller    :   resetPasswordController.php
*   Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                     (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*   Source        :   Mongo Database.
*/

$('#resetPassword').on('click', function(e){

    e.preventDefault;

    var newPassword = $('#newPassword').val();
    var confirmPassword = $('#newConfirmPassword').val();
    var email = $('#forgetEmail').val();
    var auth  = $('#forgetAuth').val();
    var time  = $('#currentTime').val();

    if(newPassword == '' && confirmPassword == ''){
      $('#erroremailshow').html('Fields marked with an asterisk (*) are required');
    }else if(newPassword != confirmPassword){
      $('#erroremailshow').html('Fields marked with an asterisk (*) are required');
    } else if(newPassword.length < 6 && confirmPassword.length< 6){
      $('#erroremailshow').html('Your password should be of minimum six characters');
    }else{
      $.ajax({
            type: "POST",
            dataType:"json",
            url: "../../controllers/user/resetPasswordController.php",
            data: {
                email: encodeURIComponent(email), newPassword: newPassword, confirmPassword : confirmPassword, auth : auth, time : time
            },
            beforeSend: function() {
              $('#resetPassword').val('Resetting...');
            } ,
            complete: function(){
              $('#resetPassword').val('Reset Password');
            },
            async: true,
            success:function(data){
                if(data.errCode == -1){
                    showToast("Sucess",data.errMsg);
                    $('#newPassword').val('');
                    $('#newPassword').parent().attr("class", "form-group");
                    $('#newPassword').siblings().attr("class", "");
                    $('#newConfirmPassword').val('');
                    $('#newConfirmPassword').parent().attr("class", "form-group");
                    $('#newConfirmPassword').siblings().attr("class", "");
                }else{
                    $("#erroremailshow").html(data.errMsg);
                }
            },
            error: function (xhr, status, error) {
                alert(error);
            }
      });
    }
});

///------------------------------------to Change Password------------------------------------------------
function passwordSubmit(elem){
    // $(elem).css('pointer-events','none');
    currentPassword = $("#currentPassword").val();
    newPassword     = $("#newPassword").val();
    confirmPassword     = $("#confirmPassword").val();
    if(newPassword == '' && confirmPassword == ''){
        $('#erroremailshow').html('Fields marked with an asterisk (*) are required');
    }else if(newPassword != confirmPassword){
        $('#erroremailshow').html('Password Did Not Matched!!!');
        $("#confirmPassword").val("");
        $("#erroremailshow").show().delay(3000).fadeOut();
    } else if(newPassword.length < 6 && confirmPassword.length< 6){
        $('#erroremailshow').html('Your password should be of minimum six characters');
    }else if(currentPassword == '') {
        $('#errorPassword').html('Fields marked with an asterisk (*) are required');
        $("#newPassword").prop("disabled", true);
        $("#confirmPassword").prop("disabled", true);
      }
      else {
        $.ajax({
            url: '../../../controllers/user/checkPasswordController.php',
            type: 'POST',
            dataType: 'json',
            data: ({password: currentPassword}),
            success: function (data) {

                if (data.errCode == -1) {
                    $.ajax({
                        url:'../../../controllers/user/changePasswordController.php',
                        type: 'POST',
                        dataType:'json',
                        data: ({currentPassword:currentPassword, newPassword:newPassword, confirmPassword:confirmPassword}),
                        beforeSend : function(data){
                            $(elem).css('pointer-events','none');
                        },
                        success: function(data){
                            if (data.errCode == -1) {
                                $("#newPassword").prop( "disabled", false );
                                $("#confirmPassword").prop( "disabled", false );
                                showToast("success",'Password Changed');
                                setTimeout(function(){
                                    window.location.href = rootUrl + 'views/user/logout.php';
                                }, 5000);
                            }  else if(data.errCode == -2){
                                window.location.href = "../../two_factor_auth/index.php";
                            }else {
                                showToast("failed",'User action Failed');
                            }
                        },
                        error: function(data) {
                            showToast("failed",'Error in Connection');
                        }
                    });
                } else if (data.errCode == 3) {
                    $('#errorPassword').html('Your Entered Wrong Password!!!!');
                    $("#newPassword").prop("disabled", true);
                    $("#confirmPassword").prop("disabled", true);
                } else {
                    showToast("failed", 'User action Failed');
                }
            },
            error: function (data) {
                showToast("failed", 'Error in Connection');
            }
        });
    }
}

function resetForm(elem) {
    $("#confirmPassword").val("");
    $("#newPassword").val("");
    $("#currentPassword").val("");
    $('#errorPassword').html("");

}