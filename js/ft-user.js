/**
 * Created by trilok on 29/5/17.
 */

$("#ft_userForm").submit(function() {
    return false;
});
$('#ft_shortDesc').on('keyup keydown keypress change paste', function() {
  if ($(this).val() == '') {
                $("#hideglyp_onempty [class*=glyphicon ]").hide();
    
  }
});
$("#ft_next").on('click', function () {

    var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');
    var pagedata='';
    var processPage='';

    var current_path = window.location.pathname.split('/').pop();

    if(current_path == 'ft_profile.php'){
        var validator = $('#ft_userUpdateForm').data('bootstrapValidator');
        validator.validate();
        console.log(validator.isValid())
        if(validator.isValid())
        {
            if(curr_page == 'profile') {
                var ftShortDesc = $("#ft_shortDesc").val();
                var date = $('#datepicker_dob').val();
                var country = $('#ft_country').find(":selected").text();
                var state = $('#ft_state').find(":selected").text();
                var city = $('#ft_city').find(":selected").text();
                var gen = $('input[name=gender]:checked').val();
                var profile_pic = $('#fileUpload').val();
                var interest = $("#ft_interest").val();
                var acct_handle = $("#accountHandle").val();

                if (ftShortDesc === '' || ftShortDesc == null ) {
                    // Hide the success icon
                    $("#hideglyp_onempty [class*=glyphicon ]").hide();
                    // $('hideglyp_onempty.glyphicon').hide();
                }

                var calcAge = date;
                var dobYear = '';

                if (calcAge != '') {
                    calcAge = calcAge.split('/');
                }

                // get date of birth year
                dobYear = parseInt(calcAge[2]);

                // date of birth calculation
                var now = new Date();
                var nowYear = now.getFullYear();
                var age = nowYear - dobYear;

                if (age < 13) {
                    showToast('failed', 'Age should be above 13 years.');
                    return false;
                }

                /*Getting profile image*/
                var file_data = $("#fileUpload").prop("files")[0];
                //console.log(file_data); return;
                if(typeof(file_data) != "undefined"){
                    var img_name = file_data["name"];
                    var img_type = file_data["type"];
                    var img_size = file_data["size"];
                }

                //$('#ft_next').prop('disabled', false);
                pagedata = {img_name:img_name,short_desc:ftShortDesc,country:country,state:state,city:city,acct_handle:acct_handle,interest:interest,gender:gen,date_of_birth:date,profile_info:curr_page,next:next_page,type:userType};
                processPage = 'updateProfileController.php';
            }
        }
        else{
            $(this).attr('disabled',false);
            return false;
        }
    }else if(curr_page == 'follow'){
        pagedata = {page:curr_page,next:next_page,sid:session_id};
        processPage = 'ftUserProcessController.php';
        var followingCount = $('#followingCount').text();
        if(followingCount < 5){
            showToast("Error","Please follow 5 accounts to proceed.");
            return false;
        }
    }else if(curr_page == 'claim'){
        pagedata = {page:curr_page,next:next_page};
        processPage = 'ftUserProcessController.php';
    }else if(curr_page == 'like'){
        pagedata = {page:curr_page,next:next_page};
        processPage = 'ftUserProcessController.php';
    }else if(curr_page == 'create'){
        pagedata = {page:curr_page,next:next_page};
        processPage = 'ftUserProcessController.php';
    }else if(curr_page == 'share'){
        pagedata = {page:curr_page,next:next_page};
        processPage = 'ftUserProcessController.php';
    }

    pagedata = JSON.stringify(pagedata);
    /*Creating object of FormData class*/
    var form_data = new FormData();
    form_data.append("fileToUpload", file_data);
    form_data.append('data', pagedata);

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/user/"+processPage,
        beforeSend: function() {
            /*$('#ft_next').prop('disabled', false);
            $('#ft_next').prop('disabled', true);*/
            $('#ft_next').append( "&nbsp;<i class='fa fa-spinner fa-pulse fa-fw'></i>");
        } ,
        complete: function(){
            $('#ft_next').html("Next");
        },
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        success: function(data){
            if(data.errCode == -1){
                showToast("Success",data.errMsg);
                window.setTimeout(function(){

                    if(data.nextPage == 'finish'){
                        next_page = 'dashboard/index.php';
                    }else{
                        next_page = 'user/ft_'+data.nextPage+'.php';
                    }
                    window.location.href = rootUrl+"views/"+next_page;
                },2000);

            }else if(data.errCode == 101){
                /* Display error when user already update profile */
                showToast("Succes",data.errMsg);
                window.setTimeout(function(){
                    window.location.href = rootUrl+"views/"+next_page;
                },1000);
            }else{
                showToast("Error",data.errMsg);
                window.setTimeout(function(){
                    location.reload();
                },1000);
            }

        }
    });
});

// $("input.checkName").keyup(function() {

//     if ($(this).val().length > 0 && $.trim($(this).val()) != "") {
//         $(this).parent().tooltip("hide");
//         $(this).parent().attr("data-original-title", "");
//         $(this).parent().attr("class", "has-success has-feedback");
//         $(this).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
//     } else {
//         toolTipShow(this, "First name/last name cannot be empty");
//         $(this).parent().attr("class", "has-error has-feedback");
//         $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
//     }
// });


function ftUserStatusUpdate(status, curr_page, next_page){
    /*var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');*/

    var returnStatus = '';

    if(status){

        $.ajax({
            type: "POST",
            dataType:"JSON",
            url: rootUrl+"controllers/user/ftUserProcessController.php",
            cache: false,
            async: false,
            data: {page:curr_page,next:next_page,sid:session_id},
            success: function(data){
                if(data.errCode == -1){

                    returnStatus = data;

                }else{

                    returnStatus = data;
                }
            }
        });
    }else{
        showToast("Error",'status failed');
        window.setTimeout(function(){
            location.reload();
        },2000);
    }
return returnStatus;
}

$("#claim").on('click', function () {

    var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/keywords/claimKeywordsController.php",
        beforeSend: function() {
            $('#claim').prop('disabled', false);
            $('#claim').prop('disabled', true);
            $('#unclaim').prop('disabled', true);
            $('#claim').append( "&nbsp;<i class='fa fa-spinner fa-pulse fa-fw'></i>");
        } ,
        complete: function(){
            $('#claim').html($("#claim").val());
        },
        cache: false,
        data: {type:'claim',page:curr_page,next:next_page,sid:session_id},
        success: function(data){

            if(data.errCode == -1){

                showToast("Success",data.errMsg);
                window.setTimeout(function(){
                    window.location.href = rootUrl+"views/dashboard/index.php";
                },2000);

            }else{
                showToast("Error",data.errMsg);
                window.setTimeout(function(){
                    location.reload();
                },2000);
            }

        }
    });
});

$("#unclaim").on('click', function () {

    var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/keywords/claimKeywordsController.php",
        beforeSend: function() {
            $('#unclaim').prop('disabled', false);
            $('#claim').prop('disabled', true);
            $('#unclaim').prop('disabled', true);
            $('#unclaim').append( "&nbsp;<i class='fa fa-spinner fa-pulse fa-fw'></i>");
        } ,
        complete: function(){
            $('#unclaim').html($("#unclaim").val());
        },
        cache: false,
        data: {type:'unclaim',page:curr_page,next:next_page,sid:session_id},
        success: function(data){

            if(data.errCode == -1){

                showToast("Success",data.errMsg);
                window.setTimeout(function(){
                    window.location.href = rootUrl+"views/dashboard/index.php";
                },2000);

            }else{
                 showToast("Error",data.errMsg);
                 window.setTimeout(function(){
                 location.reload();
                 },2000);
            }

        }
    });
});

function populateState() {
    var val = document.getElementById('ft_country').value;
    val = val.split('~~');

    var cid = val[0]; // retrieve country id
    var countryCode = val[1]; // retrieve country code number

    $.ajax({
        type: "POST",
        dataType: "html",
        url: rootUrl+"controllers/widget/getStates.php",
        data:'countryID='+cid,
        success: function(data){

            if($(".form-group").hasClass('hidden')){
                $('.form-group').removeClass('hidden');
                $("#ft_state").html(data);
            }else{
                $("#ft_state").html(data);
                $("#ft_city").html("<option disabled selected = 'selected' value=''>Select City</option>");
            }
        }
    });
}

function populateCity() {
    var val = document.getElementById('ft_state').value;
    $.ajax({
        type: "POST",
        url: rootUrl+"controllers/widget/getCities.php",
        data:'stateID='+val,
        success: function(data){

            if($(".form-group").hasClass('hide')){
                $('.form-group').removeClass('hide');
                $("#ft_city").html(data);
            }else{
                $("#ft_city").html(data);
            }
        }
    });
}

//account handle check availability and validations
$('#accountHandle').blur(function(){
    var handle = $("#accountHandle").val();
    checkHandleData(handle);
});


/* Distribute earning on create post during ftue */
function ftueCreateNewPost(postType, formname, method) {

    var postStatus = createNewPost(postType, formname, method);

    if(postStatus.errCode == -1){
        var post_id = postStatus.errMsg._id;
        var created_at = postStatus.errMsg.created_at;
        var post_type = postStatus.errMsg.post_type;
        var posted_by = postStatus.errMsg.posted_by;
        var ip_address = postStatus.errMsg.meta.user_ip;
        var keyword = postStatus.errMsg.keywords.join(' ');

        var curr_page = $('#page').attr('current');
        var next_page = $('#page').attr('next');

        $.ajax({
            type: "POST",
            dataType:"JSON",
            url: rootUrl+"controllers/cdp/complementaryCDP.php",
            cache: false,
            async: false,
            data: {post_id:post_id,created_at:created_at,post_type:post_type,posted_by:posted_by,shared_by:'',origin_ip:ip_address,sid:session_id,page:curr_page,next:next_page},
            success: function(data){

                if(data.errCode == -1){

                    if(data.nextPage == 'finish'){
                        next_page = 'dashboard/index.php';
                    }else{
                        next_page = 'user/ft_'+data.nextPage+'.php';
                    }
                    var msg = "Congratulations!!!<br>You have successfully created a post and received a complimentary like from Keywo.";
                    loadUserPostDetails(post_id,created_at,msg,data.errCode);
                    var currPref = (userCurrPref == 'USD') ? keywoDefaultCurrency : userCurrPref;
                    $("#available-balance").html(number_format_sort(parseFloat(data.total_available_balance),4)+" "+keywoDefaultCurrency);
                    $("#available-balance").attr('origprice',parseFloat(data.total_available_balance)+" "+keywoDefaultCurrency);
                    showToast("Success",msg);
                    /*window.setTimeout(function(){
                        window.location.href = rootUrl+"views/"+next_page;
                    },2000);*/

                }else if(data.errCode == 404){
                    var msg = $("#notify").text();
                    loadUserPostDetails(post_id,created_at,msg,data.errCode);
                    showToast("Success",data.errMsg);

                }else{
                    showToast("Error","Something went wrong, please try again later.");
                    window.setTimeout(function(){
                        location.reload();
                    },2000);
                }
            }
        });

    }else{
        showToast("Error","Something went wrong, please try again later.");
        window.setTimeout(function(){
            location.reload();
        },2000);
    }
}

/* Distribute earnings on share post */
function ftueSharePost(sharePost){

    if(sharePost.errCode == -1){

        sharePost = sharePost.sharePostDetail;
        var post_id = sharePost._id;
        var created_at = sharePost.created_at;
        var ip_address = sharePost.meta.user_ip;
        var post_type = sharePost.post_type;

        var curr_page = $('#page').attr('current');
        var next_page = $('#page').attr('next');

        $.ajax({
            type: "POST",
            dataType:"JSON",
            url: rootUrl+"controllers/cdp/complementaryCDP.php",
            cache: false,
            data: {post_id:post_id,created_at:created_at,post_type:post_type,origin_ip:ip_address,sid:session_id,page:curr_page},
            success: function(data){

                if(data.errCode == -1){

                    var msg = "Congratulations!!!<br>You have successfully shared a post and received a complimentary like from Keywo.";
                    loadUserPostDetails(post_id,created_at,msg,data.errCode);
                    showToast("Success",msg);
                    /*var currPref = (userCurrPref == 'USD') ? keywoDefaultCurrency : userCurrPref;*/
                    $("#available-balance").html(number_format_sort(parseFloat(data.total_available_balance),4)+" "+keywoDefaultCurrency);
                    $("#available-balance").attr('origprice',parseFloat(data.total_available_balance)+" "+keywoDefaultCurrency);

                }else if(data.errCode == 404){
                    var msg = $("#notify").text();
                    loadUserPostDetails(post_id,created_at,msg,data.errCode);
                    showToast("Success",data.errMsg);

                }else{
                    showToast("Error","failure: Something went wrong, please try again later.");
                    window.setTimeout(function(){
                        location.reload();
                    },2000);
                }
            }
        });

    }else{
        alert("Error: while distribute complimentary like on shared post");
    }
}


function loadUserPostDetails(postId,createdTime,msg,errCode)
{
    var returnResp = '';
    var curr_page = $('#page').attr('current');
    $.ajax({
        type: "POST",
        dataType:"html",
        url: rootUrl+"controllers/social/getPostDetailsAjax.php",
        cache: false,
        async: false,
        data: {postId:postId,postCreatedAt:createdTime,sid:session_id},
        success: function(data){
            //returnResp = data;
            $("#pageContent").html(data);
            $("#notify").html(msg);
            if(curr_page == 'create' && errCode != 404){
                $("#create").addClass('fa fa-check');
                $("#notify").removeClass('text-dark-blue');
                $("#notify").addClass('text-success');
            }else if(curr_page == 'share' && errCode != 404){
                $("#share").addClass('fa fa-check');
                $("#notify").removeClass('text-dark-blue');
                $("#notify").addClass('text-success');
            }
              
        }
    });
    //return returnResp;
}

/* Called after like a post */
function updateLikePostDetails(){

    var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');

    var msg = "Congratulations. You earned "+parseFloat(payout).toFixed(4)+" for post like.";
    var respData = ftUserStatusUpdate(true, curr_page, next_page);
    if(respData.errCode == -1){
        $('.currency').text(parseFloat(payout).toFixed(4)+" "+keywoDefaultCurrency);
        $('.currency').attr("origprice",payout+" "+keywoDefaultCurrency);
        $("#notifyLike").html(msg);
        $("#notifyLike").removeClass('text-dark-blue');
        $("#note").removeClass('hidden');
        $("#notifyLike").addClass('text-success');
        $("#like").addClass('fa fa-check');
        $("#ft_navigator").attr('value','Next');
        $("#ft_navigator").text('Next');
        showToast("Success",msg);
    }else{
        showToast("Error","failure: Something went wrong, please try again later.");
    }
}

$(document).on('click','#ft_navigator', function () {

    var curr_page = $('#page').attr('current');
    var next_page = $('#page').attr('next');
    var btn_value = $('#ft_navigator').val();

    $("#ft_navigator").prop('disabled',false);
    $("#ft_navigator").prop('disabled',true);
    $('#ft_navigator').append("&nbsp;<i class='fa fa-spinner fa-pulse fa-fw'></i>");

    if(next_page == 'finish'){
        if(btn_value != 'Skip'){
            next_page = 'dashboard/index.php';
        }else{
            curr_page = curr_page+'_skip';
            var respData = ftUserStatusUpdate(true, curr_page, next_page);
            if(respData.errCode == -1){
                next_page = 'dashboard/index.php';
            }else{
                return;
            }
        }
    }else{
        next_page = 'user/ft_'+next_page+'.php';
    }

    window.setTimeout(function(){
        window.location.href = rootUrl+"views/"+next_page;
        $('#ft_navigator').html(btn_value);
    },1000);


});
