/* Created by Palak

It is called in prelogin_footer.js for validations, open modal, adding toast

*/


/*postOpenModel function is used for CDP call and json file creation on popeup after 10 sec*/
// function postOpenModel(post_id, posted_by, shared_by){
//     if(shared_by == ''){
//         shared_by = posted_by;
//     }
//     var data = $('#'+post_id).html();
//     openModal($("#"+post_id), data);
//
//     // setTimeout( function(){
//         $.ajax({
//             url: '../../controllers/cdp/coinDistributionController.php',
//             type: "POST",
//             data: {content_id: post_id, posted_by: posted_by, shared_by: shared_by, mode_type : 'social'},
//             success: function (data) {
//                 console.log(data);
//                 $.ajax({
//                     type: "POST",
//                     url: '../../controllers/social/setPostJsonController.php',
//                     dataType : "html",
//                     data: {content_id: post_id, posted_by: posted_by, shared_by: shared_by},
//                     success: function (data) {
//                         console.log(data);
//                     }
//                 });
//             },
//             error: function (xhr, status, error) {
//             }
//         });
//     // }, 10000 );
// }


//Call this function to open modal
// function openModal(elem, content) {
//     console.log(content);
//     // return;
//     $(elem).show();
//   //  if (!$("body .modal").length > 0) {
//         // Modal
//     var html;
//         html = '<div class="modal fade" id="myModal" role="dialog">';
//         html += '<div class="modal-dialog">';
//         //Modal content
//         html += '<div class="card social-card social-form-modal">';
//         html += '<div class="innerAll html-content">';
//         html += content;
//         html += '</div>';
//         html += '</div>';
//         html += '</div>';
//         html += '</div>';
//
//         //$("body").html('');
//         $('body').append(html);//append modal to html body
//
//   //  }
//
//     $(elem).attr("data-toggle", "modal");
//     $(elem).attr("data-target", "#myModal");
//     //$(elem).attr("data-backdrop", "static");// modal will not get close on click on mask
//     //$(elem).attr("data-keyboard", "false");
//
// }


// loader in earning pages
  $(window).load(function(){
        $('#cover').fadeOut(1000);
    });
// loader in earning pages



//check email address
function checkEmailAdd(emailAdd) {
    var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    if (pattern.test(emailAdd)) {
        return true;
    } else {
        return false;
    }
}
//check password length
function checkPasswordLength(password) {
    if (password.length >= 6) {
        return true;
    } else {
        return false;
    }
}
//check password is identical or not
function passwordIdentical(password, confirmPassword) {
    if (password == confirmPassword) {
        return true;
    } else {
        return false;
    }
}
//call this function wherever needed
function showToast(toastTitle, toastMsg, toastTime) {
    //  var toastType = "info";//"error";//"warning";//"success";
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "100000",
        "timeOut": toastTime,
        //  "extendedTimeOut": "1000000",
        "showEasing": "swing",
        "showMethod": "fadeIn"
    }
    toastr["error"]("<div class='col-xs-3 padding-none'><div class='toast-img-div'><img src='" + rootUrlForToast + "images/toast_img.jpg'></div></div><div class='col-xs-8 '><div class=''><p class='margin-bottom-none toastMsg-class'>" + toastMsg + "</p></div></div>");
}

/*
 tootip call
 */
// $(function () {
//    $('[data-toggle="tooltip"]').tooltip()
//  });

function setCity() {
    var cityDropDown = $(".selCity");
    var cityDropDownSignup = $(".selSignupCity");

    $(".selCity").html('');
    cityDropDown.trigger("chosen:updated");
    $(".selCity").parent().addClass("has-error");
    $(".selCity").parent().find("i").addClass("glyphicon-remove");

    $(".selSignupCity").html('');
    cityDropDownSignup.trigger("chosen:updated");
    $(".selSignupCity").parent().parent().addClass("has-error");
    $(".selSignupCity").parent().find("i").addClass("glyphicon-remove");
}
/* Display cities according to country select */
function printCities(countryName) {
    if (countryName == null || countryName == '') {
        setCity();
        return false;
    } else {
        setCity();
        $.ajax({
            type: "POST",
            url: "../../controllers/citiesController.php",
            dataType: "html",
            data: 'country=' + countryName,
            async: true,
            success: function(data) {
                var city_dropdown = $("#city");
                city_dropdown.html('');
                city_dropdown.html(data);
                city_dropdown.trigger("chosen:updated");
            },
            error: function(xhr, status, error) {
                alert(error);
            }
        });
    }

}

/*
Reset form
*/
function customResetForm(id) {

    $('#' + id)[0].reset();
    $('#country').find('option:first-child').prop('selected', true)
        .end().trigger('chosen:updated');
    $('#city').find('option:first-child').prop('selected', true)
        .end().trigger('chosen:updated');
    grecaptcha.reset();
    $('#' + id).find('.has-error').each(function() {
        $(this).removeClass("has-error");
    });
    $('#' + id).find('.has-success').each(function() {
        $(this).removeClass("has-success");
    });
    $('#' + id).find('.form-control-feedback').each(function() {
        $(this).removeClass("glyphicon-ok");
        $(this).removeClass("glyphicon-remove");
    });

}



$("input.restrictPasswordSpace").keyup(function(e) {
    if (e.which != 32) {

    } else {
        $(this).val($(this).val().replace(/\s+/g, ''));
    }
});
$(".registration input").one("click", function() {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: "focus"
    });
    if ($(this).attr("type") == "radio" || $(this).attr("type") == "checkbox" || $(this).attr("id") == "referal") {

    } else {
        toolTipShow(this, "This field is mandatory");
        $(this).parent().attr("class", "has-error has-feedback");
        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }
});
$("#registerForm input").one("click", function() {
    if ($(this).attr("type") == "radio" || $(this).attr("type") == "checkbox" || $(this).attr("id") == "referal") {

    } else {
        toolTipShow(this, "This field is mandatory");
        $(this).parent().attr("class", "has-error has-feedback");
        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }
});
$("#firstName").keydown(function(e) {
    if (e.which == 9) {
        if ($.trim($(this).val()) == "") {
            toolTipShow(this, "This field is mandatory");
            $(this).parent().attr("class", "has-error has-feedback");
            $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
        }
    }

});

function toolTipShow(elem, tooltipMsg) {
    $(elem).parent().attr({
        'style': 'display:block',
        'class': 'glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross',
        'data-toggle': 'tooltip',
        'data-original-title': tooltipMsg,
        'data-placement': 'right',
    });
    $(elem).parent().tooltip({
        show: true,
        trigger: "focus"
    });
}
$("input.checkName").keyup(function() {

    if ($(this).val().length > 0 && $.trim($(this).val()) != "") {
        $(this).parent().tooltip("hide");
        $(this).parent().attr("data-original-title", "");
        $(this).parent().attr("class", "has-success has-feedback");
        $(this).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
    } else {
        toolTipShow(this, "First name/last name cannot be empty");
        $(this).parent().attr("class", "has-error has-feedback");
        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
    }
});



$("input.passwordValidation").on('change keyup',

function(e) {
    if (e.which != 32) {
        if ($(this).val().length >= 6) {
            $(this).parent().tooltip("hide");
            $(this).parent().attr("data-original-title", "");
            $(this).parent().attr("class", "has-success has-feedback form-group ");
            $(this).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
            if ($(".passwordIdenticalValidation").val() != "" && $(".passwordIdenticalValidation").val() != $(this).val()) {
                $(".passwordIdenticalValidation").parent().attr("class", "has-error has-feedback form-group");
                $(".passwordIdenticalValidation").siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            } else if ($(".passwordIdenticalValidation").val() == $(this).val()) {
                $(".passwordIdenticalValidation").parent().tooltip("hide");
                $(".passwordIdenticalValidation").parent().attr("data-original-title", "");
                $(".passwordIdenticalValidation").parent().attr("class", "has-success has-feedback form-group");
                $(".passwordIdenticalValidation").siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color");
            }
        } else if ($(this).val() == "") {
            $(this).parent().attr("class", "has-error has-feedback form-group");
            $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
        } else {
            toolTipShow(this, "Password length should be greater than and equal to 6");
            $(this).parent().attr("class", "has-error has-feedback form-group");
            $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            if ($(".passwordIdenticalValidation").val() != "" && $(".passwordIdenticalValidation").val() != $(this).val()) {
                $(".passwordIdenticalValidation").parent().attr("class", "has-error has-feedback form-group");
                $(".passwordIdenticalValidation").siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross form-group");
            } else if ($(".passwordIdenticalValidation").val() == $(this).val()) {
                $(".passwordIdenticalValidation").parent().tooltip("hide");
                $(".passwordIdenticalValidation").parent().attr("data-original-title", "");
                $(".passwordIdenticalValidation").parent().attr("class", "has-success has-feedback form-group");
                $(".passwordIdenticalValidation").siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color form-group");
            }
        }
    } else {
        $(this).val($(this).val().replace(/\s+/g, ''));
    }


});

$("input.passwordIdenticalValidation").keyup(function(e) {
    if (e.which != 32) {
        if ($(this).val() == $(".passwordValidation").val() && $(this).val() != "") {
            $(this).parent().tooltip("hide");
            $(this).parent().attr("data-original-title", "");
            $(this).parent().attr("class", "has-success has-feedback form-group");
            $(this).siblings().attr("class", "glyphicon glyphicon-ok form-control-feedback glyphicon-success-color form-group");
        } else {
            toolTipShow(this, "Both passwords should be identical");
            $(this).parent().attr("class", "has-error has-feedback form-group");
            $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross form-group");
        }
    } else {
        $(this).val($(this).val().replace(/\s+/g, ''));
    }
});

function clearPassword() {
    $("#newPassword").val('');
    $("#newConfirmPassword").val('');
    // $("#newPassword").parent().attr("class", "");
    // $("#newPassword").siblings().attr("class", "");
    // $("#newConfirmPassword").parent().attr("class", "");
    // $("#newConfirmPassword").siblings().attr("class", "");
}

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});
//scroll to top
$(window).on('beforeunload', function() {
$(window).scrollTop(0);
});

//scroll to top
// notifications js
// $(document).ready(function(){
//     $("#demo").click(function(){
//         $("#notify-drop").slideToggle();
//     });
// });
// notifications js

function updateUserTimezone(rootUrl)
{
    var tz = jstz.determine(); // Determines the time zone of the browser client
    var timeZone=tz.name(); // Returns the name of the time zone eg "Europe/Berlin"

    $.ajax({
        type: "POST",
        dataType: "json",
        url: rootUrl+"views/time/tzUpdate.php",
        data: {
            userTimezone:timeZone
        },
        success: function (data) {
            if(data["errCode"]=="-1")
            {
                console.log(data["errMsg"]);
            }else {
                console.log(data["errMsg"]);
            }

        },
        error: function () {
            console.log("Failed to Update Timezone");
        }
    });
}

/* ------------ tooltip -------------*/
 $('[data-toggle="tooltip"]').tooltip({ trigger: "hover" }); 
