/*
*-----------------------------------------------------------------------------------------------------------
*    Saerch app ajax javascript
*-----------------------------------------------------------------------------------------------------------
*   This search javascript file is used to get function of search landing page which are required to loaded on page load
*/



/*
*-----------------------------------------------------------------------------------------------------------
*   Function searchLandingAjax
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   $(document).ready(function(){});
*   Purpose           :    for scrolling search result page Up
*/
$(document).ready(function(){
  // browser window scroll (in pixels) after which the "back to top" link is shown
  var offset = 300,
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    offset_opacity = 1200,
    //duration of the top scrolling animation (in ms)
    scroll_top_duration = 700,
    //grab the "back to top" link
    $back_to_top = $('.cd-top');

  //hide or show the "back to top" link
  $(window).scroll(function(){
    ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
    if( $(this).scrollTop() > offset_opacity ) {
      $back_to_top.addClass('cd-fade-out');
    }
  });

  //smooth scroll to top
  $back_to_top.on('click', function(event){
    event.preventDefault();
    $('body,html').animate({
      scrollTop: 0 ,
      }, scroll_top_duration
    );
  });

  // //To redirect on default mode.
  // var systemModeRedirect = $('#modSwitchId').attr('value');
  // // alert ("System Mode : "+systemModeRedirect);
  // if (systemModeRedirect == 2 ) {
  //   // alert('redirect Needed');
  //   var sysMode = $('#modSwitchId').text();
  //   // alert(sysMode);
  //   if (sysMode != "") {
  //     window.location.href = sysMode;
  //   } else {
  //     alert('Error In Page Load of Dashboard');
  //   }
  // }
});

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function checkEmptyField
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name     :   checkEmptyField()
 *   Purpose           :   check landing box is empty or not on click event
 *   Arguments         :
 */
function checkEmptyField(){
	var searchQuery = $(".landing-search-box").val();
	if (searchQuery.trim() == ""){
		$(".landing-search-box").val('');
		$('.landing-search-box').focus();
		return false;
	}
	return true;
}

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function searchLandingAjax
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name     :   searchLandingAjax()
 *   Purpose           :   Makes an ajax request to the landing page which loads on search app page.
 *   Arguments         :   (string) loginStatus, (string) searchRootUrl, (string) landing, (int) appId,  (string) rootUrl, (string) appLogo, (string) appName, (string) appURL
 */

function searchLandingAjax(loginStatus, searchRootUrl, landing, appId,  rootUrl, appLogo, appName, appURL) {
    // validate login status if 1 then only load the landing page.
    if (loginStatus == 1) {

        $.ajax({
            type: 'POST',
            url: searchRootUrl + landing,
            dataType: 'html',
            data: {
                appId: appId,
                rootUrl: encodeURIComponent(rootUrl),
                appLogo: encodeURIComponent(appLogo),
                appName: encodeURIComponent(appName),
                appURL: encodeURIComponent(appURL)
            },
            async: true,
            success: function (data) {
                $('.loadLandingPage').html(data);
            }
        });

    } else {
        window.location.href = rootUrl + 'views/prelogin/';
    }
    // end - validate login status if 1 then only load the landing page.

}

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function searchLandingAjax
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name     :   searchLandingAjax()
 *   Purpose           :   Makes an ajax request to the landing page which loads on search app page.
 *   Arguments         :   (string) loginStatus, (string) searchRootUrl, (string) landing, (int) appId,  (string) rootUrl, (string) appLogo, (string) appName, (string) appURL
 */

function loadWidgets(searchQuery, currentCurrPref,  rootUrlSearch) {
		//Get widget result using ajax
		$.ajax({
			type:'GET',
			url: rootUrlSearch+'widget_page.php',
			data: {q:searchQuery, currentCurrPref:currentCurrPref},
			dataType: 'html',
			async:false,
			success: function(data){
				// This will replace all content in the body tag with what has been retrieved
				$("#widget").html();
				$("#widget").show();
				$("#widget").html(data).fadeIn(1000);
				$(".closeBtnDiv").hide();
				//callphpfunction();
			}
		});
}

function coinDistributionProcess(appId, appName, searhcQuery, originIp, rootUrl, logFileName, ipSearchCount){

    var message = '';
	$.ajax({
		type:'POST',
		url: rootUrl+'controllers/cdp/coinDistributionController.php',
		data: {q:encodeURIComponent(searhcQuery),appName: encodeURIComponent(appName), content_id: appId,log: encodeURIComponent(logFileName), searchCountFromIP:ipSearchCount, origin_ip : originIp, mode_type : 'search'},
		dataType: 'JSON',
        async:true,
		success: function(data) {

			console.log(data);
            if(data.errCode == -1){
                message = 'success';
                if(data.search_type == 'Qualified'){
                	// if qualified then interaction count should be green
                  	$('#pending-interaction').css("color", "rgb(9, 250, 9)");
                  	$('#pending-interaction').html(data.no_of_pending_interaction);
  				  	$('#available-balance').html(data.total_available_balance+" "+keywoDefaultCurrency);
  				  	$('#available-balance').attr('origprice', data.total_available_balance+" "+keywoDefaultCurrency);
                }else{
                	// if unqualified then interaction count should be red
                    $('#pending-interaction').css("color", "red");
                    $('#pending-interaction').html(data.no_of_pending_interaction);
                }
            }else{
                message = 'failed';
            }
		}
	});
}


/* --------------------Block explorer ajax calls---------------------------*/
//ajax call to trans
function ajaxBlockSingleTrans(transd_id) {
	$.ajax({
		url: '../blockexplorer/blockSingleTransAjax.php',
		type: 'POST',
		data: ({ blockSingleHash:transd_id }),

		success: function(data){
			$(".blockstran2").empty();
			$(".blockstran2").html(data);
			$("html, body").animate({ scrollTop: 0 }, 400);

		}
	});
}

//ajax call user trans input
function ajaxBlockUserTrans(transUser_id) {
	$.ajax({
		url: '../blockexplorer/blockUserTransAjax.php',
		type: 'POST',
		data: ({ blockUserHash:transUser_id }),

		success: function(data){
			$(".blockstran2").empty();
			$(".blockstran2").html(data);
			$("html, body").animate({ scrollTop: 0 }, 400);
		}
	});
}

//ajax call user trans output
function ajaxBlockUserTransOuT(transUserOut_id) {
	$.ajax({
		url: '../blockexplorer/blockUserTransAjax.php',
		type: 'POST',
		data: ({ blockUserHash:transUserOut_id }),

		success: function(data){
			$(".blockstran2").empty();
			$(".blockstran2").html(data);
			$("html, body").animate({ scrollTop: 0 }, 400);
		}
	});
}
