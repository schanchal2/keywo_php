
var renewKeyword = '';
var cientSessionFrmOwnedKwd = '';
var payment_type = '';
var rootURL = '';

var acceptTermFlag = "false";
$(document).ready(function(){

    $("#btn-kwd-renew-licence").click(function(){
        $(this).css("background-color","#969798");
        $(this).css("box-shadow","1px 1px 1px 1px #969798");
        $(this).prop('disabled', true);
        $(".btn-pay-now").prop('disabled', false);
        $(".btn-pay-now").css('background-color',"#b96b0d");
        acceptTermFlag = true;
    });

});


function renewOwnedKeyword(payment_type_attr, keyword){

    renewKeyword = keyword;
    payment_type = payment_type_attr;
    var totalITDAmount = $('#totalITDAmount').val();
    var renewAmt = $('#renewalAmount').val();

    if (typeof totalITDAmount == "undefined") {
        totalITDAmount = renewAmt;
    }

    var msg = "Are you Sure, You want to renew "+kwdRenewInCart+" keyword for price of "+ finalITDAmt +" "+keywoDefaultCurrency;

    $("#response").text('');
    $("#response").text(msg);
    $("#keyword-popup-confirmation").modal({
        show:true,
        backdrop: 'static',
        keyboard: false
    });
    $('#renew-now-btn').prop("disabled", true);
    $('#renew-now-btn').css("background-color","1px 1px 1px 1px #969798");
    $('#pay-for-renew').val('Yes');
    $('#pay-for-renew').show();
    $('#change_to_ok').val('No');

}

function keywordRenewNow(keyword, clientSessionId, rootUrl){

     var url = rootUrl+'views/keywords/cart/renew_checkout.php';


   /* if(keyword != '' && clientSessionId != '' && rootUrl != ''){
        var form = $('<form action="' + url + '" method="post" id="keywordRenewalConfDialog">' +
            '<input type="hidden" name="keywordToRenew" id="keywordToRenew" value="' + encodeURIComponent(keyword) + '" />' +
            '<input type="hidden" name="clientSessionIdKeywordRenew" id="clientSessionIdKeywordRenew" value="' + encodeURIComponent(clientSessionId) + '" />' +
            '</form>');
        $('body').html(form);
        $(form).submit();
    }else{
        alert('Something went wrong, Please try after sometime');
    }*/

    $.ajax({
        type : "POST",
        dataType : "JSON",
        url : "../../../controllers/keywords/addRenewKwdInPresaleTbl.php",
        data : {
            clientSessionId : encodeURIComponent(clientSessionId),
            keyword : encodeURIComponent(keyword),
        },
        success: function(data){
            if(data.errCode == -1){
                window.setTimeout(function(){
                    window.location.href = url;
                },2000)
            }else{
                showToast("failed",data.errMsg);
            }
        }
    });

}

$('#renew-yes-btn').click(function(e){

    var kwd = renewKeyword;
    var sessionId = cientSessionFrmOwnedKwd;
    var url = rootURL+'views/keywords/cart/renew_checkout.php';

    if(kwd!= ''){

        var form = $('<form action="' + url + '" method="post" id="keywordRenewalConfDialog">' +
            '<input type="text" name="keywordToRenew" id="keywordToRenew" value="' + encodeURIComponent(kwd) + '" />' +
            '<input type="text" name="clientSessionIdKeywordRenew" id="clientSessionIdKeywordRenew" value="' + encodeURIComponent(sessionId) + '" />' +
            '</form>');
        $('body').html(form);
        $(form).submit();
    }else{
        var msg = "Can't process this, Please try after some time.";

        $('#renewKeywordText').text('');
        $('#renewKeywordText').text(msg);
    }
});

//function renewKeywordLicence(payment_type, keyword){
function renewKeywordLicence(){

    var clientSessionId = $('#clientSid').val();
    $('#pay-for-renew').prop("disabled", true);

    $.ajax({
        type : "POST",
        dataType : "JSON",
        url : "../../../controllers/keywords/renewKeywordController.php",
        data : {
            clientSessionId : encodeURIComponent(clientSessionId),
            keyword : encodeURIComponent(kwdRenewInCart),
            termNcondition : encodeURIComponent(acceptTermFlag)
        },
        success: function(data){

            console.log(data);
            if(data.errCode == -2){
                window.location.href = "../../two_factor_auth/index.php";
            } else if(data.errCode == -1){
                showToast("success","You have successfully renew the licence of "+kwdRenewInCart+ " keyword");
                $('#pay-for-renew').prop("disabled", false);
                window.setTimeout(function(){
                    window.location.href = data.redirect_url;
                },2000)
            }else{
                showToast("failed",data.errMsg);
                window.setTimeout(function(){
                    window.location.href = data.redirect_url;
                },2000)
            }
        },
        error: function(xhr, status, error) {
            var errMsg = "Error initiating renew.Please try again after sometime.";
            $("#response").text('');
            $("#response").text(errMsg);
            $('#change_to_ok').val('OK');
            $('#pay-for-renew').css('display', 'none');
            $("#keyword-popup-confirmation").modal("show");
        }
    });
}

$('#renewOwnershipKeyword').click(function(){

    var clientSessionId = $('#clientSid').val();


    $.ajax({
        type: 'POST',
        url: '../../../controllers/keywords/addKeywordForRenew.php',
        dataType: 'html',

        data: {
            clientSessionId : encodeURIComponent(clientSessionId)
        },
        success : function(data){

            $('#loadRenewKeywordWithinDays').html(data);

            if($("#loadRenewKeywordWithinDays input[type='hidden']").val() == 0){
                $('#addMoreKeyword').hide();
            }else{
                $('#addMoreKeyword').show();
            }
        },
        error: function(xhr, status, error){
            alert('failed');
        }

    });

});


$('#addMoreKeyword').click(function(){

    $('#show-error').css('display', 'none');

    var oldKwd = $('#oldRenewKeyword').val();
    var clientSessionId = $('#clientSid').val();

    var val = [];
    $('#loadRenewKeywordWithinDays input[type="checkbox"]:checked').each(function(i){
        val[i] = $(this).val();
    });

    if (val.length === 0) {
        $('#show-error').html('Please select your keyword');
        $('#show-error').css('display', 'block');
        setTimeout('$("#show-error").fadeOut()',2000);
    }else{

        if(jQuery.inArray(oldKwd, val) == -1) {
            val.push(oldKwd);
        }

        $.ajax({
            type: 'POST',
            url: '../../../controllers/keywords/renewMoreKeyword.php',
            dataType: 'html',

            data: {
                renewMoreKeyword : val,
                clientSessionId :  encodeURIComponent(clientSessionId)
            },
            success : function(data){
                // loadRenewalCheckoutPaymentBox(val);
                $("#keyword-popup-newkeyword").modal("hide");
                $('#renewMoreKeyword').html(data);
                var check = $('#flagCheck').val();
                if(check == 1){
                    showToast("Success","Keyword successfully added to cart.");
                }
                loadRenewcheckoutPaymentBox();
            },
            error: function(xhr, status, error){
                alert('failed');
            }
        });
    }
});

function loadRenewalCheckoutPaymentBox(val){
   // var pageurl = rootUrl+"views/keywords/cart/renewalCheckoutPaymentBox.php?r="+Math.random();
    var pageurl = rootUrl+"views/keywords/cart/renewalCheckoutPaymentBox.php";
    $("#paymentBox").load(pageurl, {"keyword" : val});
}


function removeRenewKwd(keyword){

    $.ajax({
        type: 'POST',
        url: '../../../controllers/keywords/removeRenewKwd.php',
        dataType: 'html',

        data: {
            keyword : encodeURIComponent(keyword),
        },
        success : function(data){

            $('#renewMoreKeyword').html(data);
            var check = $('#flagCheck').val();
            var currentRenewKwdCount = $('#currentRenewKwdCount').val();

            // if keyword remove from cart then it show a toast message
            if(check == 0){
                showToast("Success","Keyword successfully removed from cart");
            }

            // if keyword not available in cart to renew then redirect to users owned keyword page.
            if(currentRenewKwdCount == 0){
                showToast("Success","Keyword successfully removed from cart");
                window.setTimeout(function(){
                    window.location.href = "../user_dashboard/owned_keyword.php";
                },1000);
            }


            loadRenewcheckoutPaymentBox();
        },
        error: function(xhr, status, error){
            alert(error);
        }
    });
}

function loadRenewalCartBox(){
    var pageurl = rootUrl+"controllers/keywords/loadUserRenewCart.php?r="+Math.random();
    $('#renewMoreKeyword').load(pageurl);
}

function renewKeywordByOthers(type){

    payment_type = type;
    var client_sid = $("#clientSid").val();
    var userCartKeyword = kwdRenewInCart;
    var finalRenewAmt = finalITDAmt;

    var payment_mode = $('input[name=payment_mode]:checked', '#payment-mode-selection').val();
    payment_mode = payment_mode.toLowerCase();

   if(payment_mode == "bitcoin"){
       renewByBitgo();
   }else if(payment_mode == "paypal"){
       renewByPaypal(payment_mode, client_sid, userCartKeyword, finalRenewAmt);
   }

}

function  renewByPaypal(payment_mode, client_sid, userCartKeyword, finalRenewAmt){
    //alert(payment_type+"    "+ client_sid+"     "+ userCartKeyword +"        "+ finalRenewAmt);

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/renewFromPaypal.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            finalAmount: encodeURIComponent(finalRenewAmt),
            mode: encodeURIComponent(payment_mode),
           // orderId : encodeURIComponent(orderIdForPaypal),
            // keywordprice : encodeURIComponent(keywordprice)

        },
        success: function(data){

            if(data.errCode == -1){
                dataResponse = data;
                var Msg = '';

                Msg = 'You are purchasing '+data.purchasingAmtInITD+ ' '+keywoDefaultCurrency+' @'+data.newCartPriceSGD+ ' SGD using '+ payment_mode+'.';

                $("#response-other").text('');
                $("#response-other").text(Msg);
                $("#keyword-renew-popup-confirmation").modal("show");
               /* $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');*/
            }else if(data.errCode == 201){
                showToast('failed', 'This is not your owned keyword. Please renew your owned keyword');
                window.setTimeout(function(){
                    window.location.href = rootUrl+"views/keywords/user_dashboard/owned_keyword.php";
                },2000)

            }else{
                $("#response-other").text('');
                $("#response-other").text(data.errMsg);
                $("#keyword-renew-popup-confirmation").modal("show");
               /* $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');*/
            }
        },
        error: function(xhr, status, error) {
            var errMsg = "Error initiating payment.Please try again after sometime."
            $("#response-other").text('');
            $("#response-other").text(errMsg);
            $("#keyword-renew-popup-confirmation").modal("show");
        }
    });

}


function cancelRenewDialog(){
    $("#keyword-renew-popup-confirmation").modal("hide");
}


function renewConfirmation() {
    var data = dataResponse;

    if (data.errCode == -1) {
        var keywordBasket = data.keyword_renew;
       // var keywordBasket = keywordBasket.replace('#', '');
       // var keywordBasket = keywordBasket.replace('&comma;', '');

        var client_sid = $("#clientSid").val();
        // $("#keyword-popup-confirmation").modal("hide");
       // $('.yes-btn').prop("disabled", true);
        //$('.yes-btn').css('background-color','rgb(150, 151, 152)');
        if (payment_type == 'other') {
            $("input[name='payment_mode']").each(function () {
                if ($(this).is(':checked')) {
                    payment_type = $(this).val();
                }
            });
        }

        proceedToPayment(payment_type, data, client_sid, keywordBasket);

    }else{
        var errMsg = "Error initiating payment, Please try again after sometime.";
        $("#response-other").text('');
        $("#response-other").text(errMsg);
        $("#keyword-renew-popup-confirmation").modal("show");
    }

}

function proceedToPayment(type, data, client_sid, keywords){
    type = type.toLowerCase();

    if(type == "bitcoin"){
        proceedByBitgo();
    }else if(type == "paypal"){
        proceedByPaypal(type, data, client_sid, keywords);
    }
}

function  proceedByPaypal(type, data, client_sid, keywords){

    var totalAmount = data.newCartPriceSGD;
    var singleAmount = data.singleAmount;
    var kwdBasketPaypal = keywords;
    var custom = data.custom;

    $("#totalPricePaypal").val("");
    $("#totalPricePaypal").val(totalAmount);
    $("#itemPricePaypal").val("");
    $("#itemPricePaypal").val(singleAmount);
    $("#keywordBasketPaypal").val("");
    $("#keywordBasketPaypal").val(kwdBasketPaypal);
    $("#customPaypal").val("");
    $("#customPaypal").val(custom);

    $("#renewByPaypalForm").submit();
}

function loadRenewcheckoutPaymentBox(){
    var pageurl = rootUrl+"views/keywords/cart/renewalCheckoutPaymentBox.php?r="+Math.random();
    $("#paymentBox").load(pageurl);
}

function cancelConfirmation(){
    //alert('hkfhkfkfdsfk');
    $("#keyword-popup-confirmation").modal("hide");
    $('#renew-now-btn').prop('disabled', false);
    $('#renew-now-btn').css('background-color', 'rgb(185, 107, 13)');

}