$(document).ready(function () {
    //CHECK INTRNET CONNECTIVITY
    setInterval(function(){
      if(!navigator.onLine) {
        //internet disconnected
        //console.log("offline");
        showToast("failed", "No internet connectivity");
      }
    },20000);


    //keyword search performance dialog open on load of search result pages only
    var path = $(location).attr('href');
    if(path.includes("search_result_")) {
        $(".keyword-search-container").show();
    }

    //Header Notification click
    $(".header_notification_icon").on('click', function () {
      	$(".cart-menu , .keyword-search-container , #manage-app-menu, #static-user-menu").hide();
    	  var userId = $(this).data("userid");
        var notifycount = $(this).data("notifycount");
        var category = $(this).data("category");
        var rootUrl = $(this).data("rooturltoload");

        var callurl=rootUrl+"views/notification/notification.php";
        if ($('.notification-container').css("display") == "block") {
            $('.notification-container').slideUp();
        } else {
            $('.notification-container').slideDown();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: callurl,
                data: {user_id: userId, category: category, notifyCount: notifycount},
                success: function (data) {
                    $('#notifyDetails').html(data);
                    $('#notifyDetails').attr('class', 'active');
                },
                error: function (data) {
                    alert('error');
                }
            });
        }
      //  $('[data-toggle="tooltip"]').tooltip("hide");
    });

    $(".header_keywordsearch_icon").on('click', function () {
        $(".cart-menu, .cart-content-container, .notification-container, #manage-app-menu, #static-user-menu").hide();
        $('.keyword-search-container').toggle('fast');
      //  $('[data-toggle="tooltip"]').tooltip("hide");
    });
    $(".header_cart_icon").on('click', function () {
        $(".cart-content-container, .notification-container, #manage-app-menu, #static-user-menu, .keyword-search-container").hide();
        $('.cart-menu').toggle('fast');
      //  $('[data-toggle="tooltip"]').tooltip("hide");
    });
    $(".header_manage_apps").on('click', function () {
        $(".cart-menu , .cart-content-container, .notification-container , .keyword-search-container, #static-user-menu").hide();
        $('#manage-app-menu').slideToggle();
    //    $('[data-toggle="tooltip"]').tooltip("hide");
    });
    $(".header_main_menu").on('click', function () {
        $(".cart-menu , .cart-content-container, .notification-container , #manage-app-menu, .keyword-search-container").hide();
        $('#static-user-menu').slideToggle();
        // $('[data-toggle="tooltip"]').tooltip("hide");
    });


    $("body").click(function (e) {
        var classStr = $(e.target).attr("class");
        if (classStr == undefined || (classStr.search("notification-header") == -1 && $('.notification-container').css("display") == "block")) {
            $('.notification-container').slideUp();
        }
    });

    $(".search-btn-header").click(function(){
        $("#my_form").submit();
    });
    // var sysMode = $('#modSwitchId').text();
    //
    // if (sysMode != "") {
    //   var systemSwitchMode = $('#modSwitchId').attr('value');
    //   loadSystemModePage(sysMode, systemSwitchMode);
    // }

    /*
     * Calling Function to load timeline Posts
     * At page load time it will load all posts
    */
    // loadPostTimeline();
    // $(".cart-header").click(function(){
    //
    // });

});
function openCart(email){
  $("#static-user-menu, .notification-container , #manage-app-menu, .keyword-search-container").hide();
  if ($('.cart-menu').css("display") == "block") {
    $('.cart-menu').hide('fast');
  }else{
    showUserCart(rootUrl, email);
  }
}
// JS Function to get all Post from Node Api to Show on timeline using memcache
// function loadPostTimeline(){
//   $.ajax({
//     type: 'GET',
//     dataType: 'html',
//     url: '../../controllers/social/getPostTimeline.php',
//     success: function(postdata) {
//       var newpostdata = postdata;
//       $('.postingTimeline').text('');
//       $('.postingTimeline').html(newpostdata);
//     },
//     error: function() {
//       alert('error on Loading Timeline');
//     }
//   });
// }

function showUserCart(rootUrl, email)
{

  //if ($('.cart-menu').css("display") == "block") {
  //  $('.cart-menu').hide('fast');
//  }else{
    var callurl = rootUrl + "controllers/keywords/loadUserCartController.php";
    $.ajax({
        type: "POST",
        dataType: "html",
        url: callurl,
        data: {user_id: encodeURIComponent(email)},
        success: function (data) {
            $('.cart-menu').html('');
            $('.cart-menu').html(data);
            $('.cart-menu').show('fast');
            $('#cartTotalCount').text($('#basketCount').val())
        },
        error: function (data) {
            alert('error');
        }
    });
//  }
}

//
// if (systemSwitchMode == 1) {
//   var newUrl = rootUrl+"views/search/"+data.errMsg.appName+"/";
//   //redirect to new page
//   window.history.pushState('page2', 'search', newUrl);
// }



/*
 *-----------------------------------------------------------------------------------------------------------
 *       Header javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This header javascript is used to make an ajax request by accessing the methods.
 *   This includes :
 *
 *   loadSearchApp()        :  The loadSearchApp() method is used to create new account on request
 *                             of registerController.php controller using ajax request.
 *
 *
 */

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function loadSearchApp:
 *-----------------------------------------------------------------------------------------------------------
 *   Fucntion Name :   loadSearchApp()
 *   Purpose       :   On click of app icon will retrieve the app details.
 *   Response      :   ()
 */

function loadSearchApp(email, rootUrl) {
    var classType = $('#appMenu').attr('class');
    $('#appMenu').html("");
    $.ajax({
        type: "GET",
        dataType: "html",
        url: rootUrl + 'views/apps/appMenu.php',
        data: {email: email},
        success: function (data) {
            $('#appMenu').html(data);
            $('#appMenu').attr('class', 'active');
        },
        error: function (data) {
            alert('error');
        }
    });

}


/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function ChangeUserMode:
 *-----------------------------------------------------------------------------------------------------------
 *   Fucntion Name :   ChangeUserMode()
 *   Purpose       :   On click of Switch Mode Button will Change Code of System Mode.
 *   Response      :   ()
 */
function changeUserMode(rootUrl) {
    $('.switch').css('pointer-events','none');
    var systemMode = $('#modSwitchId').attr('value');
    // alert(systemMode);
    var analyticSwitchUrl = encodeURIComponent($('#analiticPageUrl').attr('value'));
    // alert(analyticSwitchUrl);
    var systemRedirectionPath = '';
    var callurl = rootUrl + "controllers/header/systemModeController.php";
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: callurl,
        data: {mode_id: systemMode, analyticSwitchUrl: analyticSwitchUrl},
        success: function (data) {
            if (data.errCode == -1) {
                var switchModeId = data.errMsg.system_mode;
                $('#modSwitchId').attr('value', switchModeId);
                if (switchModeId == 1) {
                    systemRedirectionPath = data.errMsg.url;
                } else if (switchModeId == 2) {
                    systemRedirectionPath = rootUrl + 'views/social/index.php';
                }
                // Redirect to specific page as per Url
                window.location.href = systemRedirectionPath;
            } else {
                // Redirect to login Page
                window.location.href = rootUrl;
            }
            $('.switch').css('pointer-events','auto');
        },
        error: function (data) {
            alert('Error In System Mode Controller');
            $('.switch').css('pointer-events','auto');
        }
    });
}

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function loadSystemModePage:
 *-----------------------------------------------------------------------------------------------------------
 *   Fucntion Name :   loadSystemModePage()
 *   Purpose       :   On Load this will append HTML of Desired Mode (Social/Search Landing Page.)
 *   Response      :   ()
 */
function loadSystemModePage(systemModePageUrl, systemModeCode) {
    var rootUrlPath = $('.header_notification_icon').attr('data-rootUrltoload');
    var systemSwitchSession = $('#analiticPageUrl').text();
    if (systemSwitchSession == systemModeCode) {
        $.ajax({
            type:"POST",
            dataType:"JSON",
            data:{systemMode:systemModeCode, pageType:''},
            url: rootUrlPath + "/controllers/header/sessionSet.php",
            success: function(data){
                window.location.href = systemModePageUrl;
            },
            error:function(data){
                alert('Error In Page Re-Load');
            }
        });
    } else {
        alert("Error In Page Load");
    }
}

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function redirectDefaultApp:
 *-----------------------------------------------------------------------------------------------------------
 *   Fucntion Name :   redirectDefaultApp()
 *   Purpose       :   On Load this will append HTML of Desired Mode (Social/Search Landing Page.)
 *   Response      :   ()
 */
function redirectDefaultApp() {
    var rootUrlPath = $('.header_notification_icon').attr('data-rootUrltoload'); //Root URl Path
    var systemSwitchSession = $('#analiticPageUrl').text(); //Session of headerMode
    var systemModeSwitch = $('#modSwitchId').attr('value');
    if (systemSwitchSession != 1 || systemSwitchSession != 2) {
        $.ajax({
            type:"POST",
            dataType:"JSON",
            data:{systemMode:systemSwitchSession, modeCode:systemModeSwitch, pageType:'header'},
            url: rootUrlPath + "/controllers/header/sessionSet.php",
            success: function(data){
                window.location.href = rootUrlPath;
            },
            error:function(data){
                alert('Error In Page Re-Load');
            }
        });
    } else {
        alert("Error In Page Load");
    }
}


/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function validateEmpty
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name     :   validateEmpty()
 *   Purpose           :   check search box is empty or not on click event
 *   Arguments         :
 */
function validateEmpty(element){
    var formId = $(element).attr("id");
    var input = $("#"+formId+" input[id=search_box]").val();
    if (input.trim() == ""){
        $("#search_box").val('');
        return false;
    }
    return true;
}

/*
 *-----------------------------------------------------------------------------------------------------------
 *   Function convertPrice
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name     :   convertPrice()
 *   Purpose           :   Convert price to currency
 *   Arguments         :   User currency preference
 */

function convertPrice(curCode){
    if(curCode == 'USD'){
       /*return;*/
        $("[origprice]").each(function(){
            var displayprice=$(this).text();
            var actualprice=$(this).attr("origPrice");

            if(displayprice.indexOf(keywoDefaultCurrency)>0){
                $(this).text(number_format_sort(parseFloat(actualprice),4)+' '+curCode.toUpperCase());
                actualprice = parseFloat(actualprice.replace(" ",curCode));
                $(this).attr("data-original-title",actualprice+' '+curCode.toUpperCase());
            }else{
                actualprice = parseFloat(actualprice.replace(" ",curCode));
                var amountConverted = $(this).attr("origPrice");
                $(this).html((number_format_sort(actualprice,4))+' '+amountConverted.split(" ")[1]);
                $(this).attr("data-original-title",$(this).attr("origPrice"));
            }
        })

    }else{

        $.ajax({
            global: false,
            type: 'GET',
            dataType:'json',
            url: rootUrl+'controllers/convertCurrency.php?curCode='+curCode,
            beforeSend:function(){
                // this is where we append a loading image
                //$("a[origprice]").html('Loading...');
            },
            success:function(data){

                if(data['errCode'] == -1)
                {
                    exchangeRate = data['currRate'];

                    var curCodeRate = exchangeRate[curCode.toLowerCase()];

                    $("[origprice]").each(function(){
                        var displayprice=$(this).text();
                        var actualprice=$(this).attr("origPrice");
                        var tooltipData = '';

                        if(displayprice.indexOf(keywoDefaultCurrency)>0){
                            actualprice = (actualprice.replace(/,/g , ""));
                            actualprice = parseFloat(actualprice.replace(/.*?(([0-9-]*\.)?[0-9]-).*/g, "$1",keywoDefaultCurrency, ""));

                            if((Math.abs(actualprice))>=1000){
                                tooltipData = (curCodeRate*(actualprice)).toFixed(8)+' '+curCode.toUpperCase()+'' ;
                                var netAmount = (curCodeRate*(actualprice)).toFixed(8);

                                if(curCode.toUpperCase() == 'BTC'){
                                    $(this).text(number_format_sort(netAmount,8)+' '+curCode.toUpperCase());
                                }else{
                                    $(this).text(number_format_sort(netAmount,4)+' '+curCode.toUpperCase());
                                }

                            }else{
                                var datatodisplay = number_format_sort((curCodeRate*actualprice),8);
                                tooltipData = (curCodeRate*actualprice);
                                if(curCode.toUpperCase() == 'BTC'){
                                    $(this).text(number_format_sort(datatodisplay,8)+' '+curCode.toUpperCase());
                                    tooltipData = tooltipData.toFixed(8)+' '+curCode.toUpperCase();
                                }else{
                                    $(this).text(number_format_sort(datatodisplay,4)+' '+curCode.toUpperCase());
                                    tooltipData = tooltipData.toFixed(4)+' '+curCode.toUpperCase();
                                }

                            }
                        }else{
                            actualprice = parseFloat(actualprice.replace(" ",curCode)).toFixed(4);
                            var amountConverted = $(this).attr("origPrice");
                            tooltipData = amountConverted;
                            $(this).html(number_format_sort(actualprice,4)+' '+amountConverted.split(" ")[1]);
                            $(this).attr("data-original-title",$(this).attr("origPrice"));
                        }
                        
                        /* Display converted price in tooltip */
                        $(this).attr("data-original-title",tooltipData);
                    });
                } else {
                    alert("Currency not converted at this time.");
                }
            }
        })
    }
}

function number_format_sort(number, precision){
    var suffix = '';
    var devBy = '';

    /*if(is_float($num)){
     $num = round($num);
     }*/

    if (number <= 999) {
        // 0 - 999
        suffix = 'NA';
    } else if (number < 900000) {
        // 0.9k-850k
        devBy = 1000;
        suffix = 'K';
    } else if (number < 900000000) {
        // 0.9m-850m
        devBy = 1000000;
        suffix = 'M';
    } else if (number < 900000000000) {
        // 0.9b-850b
        devBy = 1000000000;
        suffix = 'B';
    } else {
        // 0.9t+
        devBy = 9999999999999;
        suffix = 'T';
    }
    var num_format;

    if(suffix == "NA"){
        suffix = "";
        num_format = parseFloat(number);
    }else{
        num_format = parseFloat((number/devBy));
    }

    if ( precision > 0 ) {

        num_format = num_format.toFixed(precision);
    }else{
        num_format = Math.round(num_format);
    }

     return num_format+suffix;
}

function htmlAntiEntities(str) {
    return $('<div/>').html(str).text();
}


function decimalAmountValidation(amount)
{
    var patt = /^(\d{0,15})(\.)(\d{1,8})?$|^(\d{0,15}\.?)$/;

    var newInputValue = amount;
    if (patt.test(newInputValue)){
        return true;

    } else {
        return false;

    }
    if (!patt.test(inputValue)
        && e.which != 8   // backspace
        && e.which != 46  // delete
        && (e.which < 37  // arrow keys
        || e.which > 40)) {
        e.preventDefault();
        return false;
    }

}

function setUpdateAsk(keyword, action, elem){
    var amount = parseFloat($('#askAmount').val());
    if(amount != "" && amount >= 0.24 && action != 3){
        $.ajax({
            type:"POST",
            dataType:"json",
            data:{keyword:keyword, amount:amount, type:'ask', action:action},
            url: rootUrl+"controllers/keywords/askController.php",
            beforeSend: function() {
              if($(elem).val() == "Delete"){
                $('.dialog-loading-icon-delete').show();
              }else{
                $('.dialog-loading-icon').show();
              }
              $("#keyword-popup-ask-bid").find("input[type='button']").prop("disabled",true);
            } ,
            complete: function(){
              if($(elem).val() == "Delete"){
                $('.dialog-loading-icon-delete').hide();
              }else{
                $('.dialog-loading-icon').hide();
              }
              $("#keyword-popup-ask-bid").find("input[type='button']").prop("disabled",false);
            },
            success: function(data){

              //  console.log(data);
                if(data.errCode == -2){
                    window.location.href = rootUrl+"views/two_factor_auth/index.php";
                }else if(data.errCode == 1){
                    showToast("failed",data.errMsg);
                }else if(data.errCode == -1){
                    showToast("success",data.errMsg);
                    window.location.href = data.redirect_url;
                }else{
                    showToast("failed",data.errMsg);
                    window.location.href = data.redirect_url;
                }

               /* if(data.errCode == -1){

                    showToast("success",data.errMsg);
                    window.setTimeout(function(){location.reload()},2000)
                }else{
                    showToast("failed",data.errMsg);
                    window.setTimeout(function(){location.reload()},2000)
                }*/
            },
            error:function(data){
                //alert('Error In Page Re-Load');
                showToast("failed",'Error In Page Re-Load');
            }
        });
    }else if(action == 3){

        var msg = "Are you sure you want to delete ask for  #"+keyword+" keyword.";
        $('#deleteaskResponse').text('');
        $('#deleteaskResponse').text(msg);

        $("#keyword-popup-ask-bid").modal("hide");
        $("#keyword-popup-delete-ask").modal("show");

        $('#delete-ask-yes-btn').on('click', function() {
            $('#delete-ask-yes-btn').prop("disabled",true);
                $.ajax({
                    type:"POST",
                    dataType:"json",
                    data:{keyword:keyword, amount:amount, type:'ask', action:action},
                    url: rootUrl+"controllers/keywords/askController.php",
                    success: function(data){

                        if(data.errCode == -2){
                            window.location.href = rootUrl+"views/two_factor_auth/index.php";
                        }else if(data.errCode == 1){
                            showToast("failed",data.errMsg);
                        }else if(data.errCode == -1){
                            showToast("success",data.errMsg);
                            window.location.href = data.redirect_url;
                        }else{
                            showToast("failed",data.errMsg);
                            window.location.href = data.redirect_url;
                        }
                    },
                    error:function(data){
                        //alert('Error In Page Re-Load');
                        showToast("failed",'Error In Page Re-Load');
                    }
                });
            // e.preventDefault();
        });
        $("#delete-ask-no-btn").click(function(){
            $('#delete-ask-yes-btn').unbind("click");
        });

    }else{
        //alert("Minimum ask amount should be 0.24 ITD")
        showToast("failed",'Minimum ask amount should be 0.24 '+keywoDefaultCurrency);
    }

}

function setUpdateBid(keyword, action, elem){

    var amount = parseFloat($('#bidAmount').val());
    if(amount != "" && amount >= 0.24 && action != 3)
    {
        $.ajax({
            type:"POST",
            dataType:"json",
            url: rootUrl+"controllers/keywords/bidController.php",
            data:{keyword:keyword, amount:amount, type:'bid', action:action},
            beforeSend: function() {
              if($(elem).val() == "Delete"){
                $('.dialog-loading-icon-delete').show();
              }else{
                $('.dialog-loading-icon').show();
              }
              $("#keyword-popup-ask-bid").find("input[type='button']").prop("disabled",true);
            } ,
            complete: function(){
              if($(elem).val() == "Delete"){
                $('.dialog-loading-icon-delete').hide();
              }else{
                $('.dialog-loading-icon').hide();
              }
              $("#keyword-popup-ask-bid").find("input[type='button']").prop("disabled",false);
            },
            success: function(data){
                console.log(data);
                if(data.errCode == -2){
                    // success = -2 if redirect from 2FA page
                    window.location.href = rootUrl+"views/two_factor_auth/index.php";
                }else if(data.errCode == 7){
                    showToast("failed",data.errMsg);
                }else if(data.errCode == -1){
                    showToast("success",data.errMsg);
                    window.location.href = data.redirect_url;
                }else{
                    showToast("failed",data.errMsg);
                    window.location.href = data.redirect_url;
                }
            },
            error:function(data){
                showToast("failed",'Error In Page Re-Load');
            }
        });

    }else if(action == 3){

        var msg = "Are you sure you want to delete bid for  #"+keyword+" keyword.";
        $('#deleteResponse').text('');
        $('#deleteResponse').text(msg);

        $("#keyword-popup-ask-bid").modal("hide");
        $("#keyword-popup-delete-bid").modal("show");

        $('#delete-yes-btn').on('click', function() {
            $('#delete-yes-btn').prop("disabled",true);
            $.ajax({
                type:"POST",
                dataType:"json",
                url: rootUrl+"controllers/keywords/bidController.php",
                data:{keyword:keyword, amount:amount, type:'bid', action:action},
                success: function(data){

                    if(data.errCode == -2){
                        window.location.href = rootUrl+"views/two_factor_auth/index.php";
                    }else if(data.errCode == 7){
                        showToast("failed",data.errMsg);
                    }else if(data.errCode == -1){
                        showToast("success",data.errMsg);
                        window.location.href = data.redirect_url;
                    }else{
                        showToast("failed",data.errMsg);
                        window.location.href = data.redirect_url;
                    }
                },
                error:function(data){
                    showToast("failed",'Error In Page Re-Load');
                }
            });
            // e.preventDefault();
        });

        $("#delete-no-btn").click(function(){
            $('#delete-yes-btn').unbind("click");
        });

    }else{
        showToast("failed",'Minimum bid amount should be 0.24 '+keywoDefaultCurrency);
    }
}




function clearUserCart(clientSessionIdFromCart, email, rootUrl){

  $.ajax({
      type:"POST",
      dataType:"json",
      data:{clinetSessionId: encodeURIComponent(clientSessionIdFromCart)},
      url: rootUrl+"controllers/keywords/clearUserCart.php",
      success: function(data){
          console.log(data);

          if(data.errCode == -1){
              var pathname = window.location.pathname;  // get url
              var filename= pathname.split('/').pop();  // get filename

            $(".button-text").val("Add To Cart");
            showUserCart(rootUrl,email);
            $('#cartTotalCount').text('0');

             showToast("success",data.errMsg);

              if(filename == 'checkout.php')
              {
                  //location.reload();
                  window.setTimeout(function(){
                      window.location.href=rootUrl+"views/keywords/marketplace/keyword_search.php";
                  },1000)
              }


          }else{
            //alert(data.errMsg);
             showToast("success",data.errMsg);
          }
      },
      error:function(data){
         alert('Error In Page Re-Load');
      }
  });
}

function openTradeDialog(elem){
    var keyword = $(elem).attr("value");

    $("#keyword-popup-ask-bid").modal();
    $("#keyword-popup-ask-bid .modal-dialog").html('');
    $("#keyword-popup-ask-bid .modal-dialog").load(rootUrl+"views/keywords/ask_bid_dailog.php?r="+Math.random()+"&keyword="+keyword);

    $("#keyword-popup-ask-bid").on('shown.bs.modal', function () {
        if($(elem).text() == "Edit Ask"){ // For triggering currency conversion of input value of edit ask
            var e = jQuery.Event("keyup");
            e.which = '13';
            $("#askAmount").trigger(e);
        }else if($(elem).text() == "Edit Bid"){ // For triggering currency conversion of input value of edit bid
            var e = jQuery.Event("keyup");
            e.which = '13';
            $("#bidAmount").trigger(e);
        }
    });

}


function cancelDelete(){
    $('#delete-yes-btn').unbind('click');
}

$(function(){
    $('.xssValidation').on("keydown", function(e){
        if (e.shiftKey && (e.which == 188 || e.which == 190)) {
            e.preventDefault();
        }
    });
});

// To follow And unfollow Keyword
function followUnfollow(keyword,page) {

    var buttonText = $("#fu_"+keyword).val();
    $.ajax({
        url: rootUrl+'controllers/social/setkeywordJsonController.php',
        type: 'POST',
        dataType:'json',
        data: ({user_email:page,keyword:keyword}),
        success: function(data) {
            if (data.errCode == -1) {
                $.ajax({
                    url: rootUrl + 'views/keywords/analytics/followUnfollow.php',
                    type: 'POST',
                    dataType: 'html',
                    data: ({user_email: page, keyword: keyword}),
                    success: function (data) {
                        if (data.errCode != -1) {
                            if (buttonText == 'Unfollow') {
                                $("#fu_" + keyword).attr('value', "Follow");
                                $("#fu_" + keyword).removeClass('btn-social');
                                $("#fu_" + keyword).addClass('btn-social-dark');
                                $('[data-trending-keyword='+keyword+']').attr('value', "Follow");
                                $('[data-trending-keyword='+keyword+']').addClass('btn-social-dark');
                            } else {
                                $("#fu_" + keyword).attr('value', "Unfollow");
                                $("#fu_" + keyword).addClass('btn-social-dark');
                                $("#fu_" + keyword).removeClass('btn-social');

                                $('[data-trending-keyword='+keyword+']').attr('value', "Unfollow");
                                $('[data-trending-keyword='+keyword+']').addClass('btn-social-dark');

                            }
                            showToast("success", 'keyword ' + buttonText + ' Successfully');
                        } else {
                            showToast("failed", 'keyword ' + buttonText + ' Failed');
                        }
                    },
                    error: function (data) {
                        location.href = rootUrl + "views/prelogin/index.php";
                        showToast("failed", 'Error in Connection');
                    }
                });
            }else if(data.errCode == 120){
                showToast("failed", 'Something Went Wrong!!');
                window.location.href = rootUrl + "views/user/ft_follow.php";
            }else{
                showToast("failed", 'Something Went Wrong!!');
                postLoadAsPerUrl();
            }
        },
    });
}



