/*
*-----------------------------------------------------------------------------------------------------------
*    Landing javascript
*-----------------------------------------------------------------------------------------------------------
*   This landing javascript file is used to get function which are required to loaded on page load and
*   on click event.
*
*   1.  getLandingContent()  :  This is the common function for all event occured on landing page,
*                               whether it be on page load and on click event.
*/

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getLandingContent
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getLandingContent()
*   Purpose           :   Makes an ajax request to the landing page controller file.
*   controller Name   :   landingPageController.php
*   Arguments         :   (string) type
*   Returns           :   1.  keywo feature list in json encode format, if type == feature
*                         2.  keywo default video i.e. introduction video, if type == default
*                         3.  keywo feature video on click of fature list, if type == id
*/

function getLandingContent(type){

}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getLandingSlider
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getLandingSlider()
*   Purpose           :   Makes an ajax request to the landing page controller to retrieve the consumer
*                         details required to load on page load.
*   controller Name   :   landingPageController.php
*   Arguments         :   None
*   Returns           :   CMS Slider details from database that is set by admin, in json encode format.
*/

function getLandingSlider(rootURL){
      $.ajax({
            type : 'POST',
            url : rootURL+'controllers/landingFeatureController.php',
            datatype: 'json',
            data : {
                type : encodeURIComponent('landing_slider')
            },
            async: true,
            success : function(data){
              var cmsData = JSON.parse(data);
               // console.log(cmsData);
                if(cmsData.errCode == '-1'){
                    //called below function to create html design and return the designed data.
                    var design=displayCMSContent(rootURL,cmsData.data,'getLandingSlider');
                    $('#myCarousel .carousel-inner').html(design);

                    }else{
                  alert('failed');
                }
            },
            error: function(jqXHR, error, errorThrown){
                if(jqXHR.status && jqXHR.status == 400){
                      alert(jqXHR.responseText);
                }else{
                      alert("Something went wrong");
                }
            }
      });
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getPageData
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getPageData()
*   Purpose           :   Makes an ajax request to the landing page controller to retrieve the consumer
*                         details required to load on page load.
*   controller Name   :   landingPageController.php
*   Arguments         :   None
*   Returns           :   CMS PageData details from database that is set by admin, in json encode format.
*/

function getPageData(rootURL){
      $.ajax({
            type : 'POST',
            url : rootURL+'controllers/landingFeatureController.php',
            datatype: 'json',
            data : {
                type : encodeURIComponent('landing_page')
            },
            async: true,
            success : function(data){
              //  console.log(data);
                var cmsData = JSON.parse(data);
                
                if(cmsData.errCode == '-1'){
                    $("#bannerTitle").html();
                    $("#bannerTitle").html(cmsData.data[0].banner_image);

                    $("#page_title").html();
                    $("#page_title").html(cmsData.data[0].page_title);
                    }else{
                  alert('failed');
                }
            },
            error: function(jqXHR, error, errorThrown){
                if(jqXHR.status && jqXHR.status == 400){
                      alert(jqXHR.responseText);
                }else{
                      alert("Something went wrong");
                }
            }
      });
}
/*
*-----------------------------------------------------------------------------------------------------------
*   Function getEarnData
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getEarnData()
*   Purpose           :   Makes an ajax request to the landing page controller to retrieve the consumer
*                         details required to load on page load.
*   controller Name   :   landingPageController.php
*   Arguments         :   None
*   Returns           :   CMS HowtoEarn details from database that is set by admin, in json encode format.
*/

function getEarnData(rootURL){
      $.ajax({
            type : 'POST',
            url : rootURL+'controllers/landingFeatureController.php',
            datatype: 'json',
            data : {
                type : encodeURIComponent('landing_earn')
            },
            async: true,
            success : function(data){
              //  console.log(data);
                var cmsData = JSON.parse(data);

                if(cmsData.errCode == '-1'){
                    var earnData = '';
                    $.each(cmsData.data, function (key, value) {
                        earnData += "<li class='earn-li'><a id=earn-"+value.id+" href=" +value.content_video_link+" class='list-item-earn' data-rootURL = '"+rootURL+"'>"+value.content_title+"</a></li>";
                    });
                    $('#keywo-earn').html(earnData);

                    /* Bind click event for feture list*/
                    $(".list-item-earn").click(function(e){
                        e.preventDefault();
                        $(".side-left").parent().children().find(".earn-li").children().removeClass("list-selected");
                        $(this).addClass("list-selected");
                        getLandingVideo($(this).attr("data-rootURL"),this.id);
                    });

                  }else{
                  alert('failed');
                }
            },
            error: function(jqXHR, error, errorThrown){
                if(jqXHR.status && jqXHR.status == 400){
                      alert(jqXHR.responseText);
                }else{
                      alert("Something went wrong");
                }
            }
      });
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getFeatureData
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getFeatureData()
*   Purpose           :   Makes an ajax request to the landing page controller to retrieve the consumer
*                         details required to load on page load.
*   controller Name   :   landingPageController.php
*   Arguments         :   None
*   Returns           :   CMS FeatureData details from database that is set by admin, in json encode format.
*/

function getFeatureData(rootURL){
      $.ajax({
            type : 'POST',
            url : rootURL+'controllers/landingFeatureController.php',
            datatype: 'json',
            data : {
                type : encodeURIComponent('landing_feature')
            },
            async: true,
            success : function(data){
                var cmsData = JSON.parse(data);
                if(cmsData.errCode == '-1'){
                    //console.log(cmsData.data)
                    var featureData = '';
                    $.each(cmsData.data, function (key, value) {
                        featureData += "<li class='earn-li'><a id=feature-"+value.id+" href=" +value.content_video_link+" class='list-item-feature' data-rootURL = '"+rootURL+"'>"+value.content_title+"</a></li>";
                    });
                    $('#keywo-feature').html(featureData);

                    /* Bind click event for feture list*/
                    $(".list-item-feature").click(function(e){
                        e.preventDefault();
                        $(".side-left").parent().children().find(".earn-li").children().removeClass("list-selected");
                        $(this).addClass("list-selected");
                        getLandingVideo($(this).attr("data-rootURL"),this.id);

                    });
                  }else{
                  alert('failed');
                }
            },
            error: function(jqXHR, error, errorThrown){
                if(jqXHR.status && jqXHR.status == 400){
                      alert(jqXHR.responseText);
                }else{
                      alert("Something went wrong");
                }
            }
      });


}




/*
*-----------------------------------------------------------------------------------------------------------
*   Function getLandingVideo
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   getLandingVideo()
*   Purpose           :   Makes an ajax request to the landing page controller to retrieve video [by id]
*                         details required to load on page load.
*   controller Name   :   landingPageController.php
*   Arguments         :   None
*   Returns           :   Landing Video details from database in json encode format.
*/

function getLandingVideo(rootURL,id){
      $.ajax({
            type : 'POST',
            url : rootURL+'controllers/landingFeatureController.php',
            datatype: 'json',
            data : {
                type : encodeURIComponent('landing_video_by_id'), vidId:id
            },
            async: true,
            success : function(data){
              var cmsData = JSON.parse(data);
                if(cmsData.errCode == '-1'){
                    $('#short_desc').html();
                    $('#long_desc').html();
                    $('#short_desc').html(cmsData.data[0].short_description);
                    $('#long_desc').html(cmsData.data[0].long_description);
                    var url=$('#'+id).attr('href');
                    $(".embed-responsive-item").attr('src',url);
                    //called below function to create html design and return the designed data.
                   /* var design=displayCMSContent(rootURL,cmsData.data,'getLandingVideo');
                    $('.carousel-inner').html(design);*/
                    }else{
                  alert('failed');
                }
            },
            error: function(jqXHR, error, errorThrown){
                if(jqXHR.status && jqXHR.status == 400){
                      alert(jqXHR.responseText);
                }else{
                      alert("Something went wrong");
                }
            }
      });
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function displayCMSContent
*-----------------------------------------------------------------------------------------------------------
*   Function Name     :   displayCMSContent()
*   Purpose           :   create the design and returns it so that it can be
                          displayed in html page.
*   Arguments         :   response Data of Ajax and name of function from which it is called.
*   Returns           :   Html content formated to fit in the design to display on the page.
*   Call              :   on successful response of Ajax.
*/

function displayCMSContent(rootURL,data,callfrom){
     if(callfrom =='getLandingSlider'){
      //alert(data);
           var design='';
           var activeclass='';
           for(var i=0;i<data.length;i++){
            if(i==0){design+='<div class="item active">';}
            else{design+='<div class="item">';}
            design+='<div class="row-fluid">'+
                      '<div class="col-xs-6 padding-left-none">'+
                        '<a href="#x" class="thumbnail"> '+
                          '<h4 style="display:none">Consuming Content</h4>'+
                          '<img src="'+data[i].slider_image_consumer+'" alt="Image" style="max-width:100%;" >'+
                        '</a>'+
                      '</div>'+
                      '<div class="col-xs-6 padding-right-none">'+
                        '<a href="#x" class="thumbnail">'+
                           '<h4 style="display:none">  Uploading Content </h4>'+
                           '<img src="'+data[i].slider_image_producer+'" alt="Image" style="max-width:100%;" >'+
                        '</a>'+
                      '</div>'+
                    '</div><!--/row-fluid-->'+
                  '</div><!--/item-->';

            //console.log(':::'+data[i].slider_image_consumer);
            //console.log('::'+data[i].slider_image_producer);
         }
       return design;    

    }
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Event .load()
*-----------------------------------------------------------------------------------------------------------
*   Event Name        :   $('.pool-stats-main-div').load();
*   Purpose           :   To load pool stats details on page load.
*/

//$('#pool-stats-main-div').load('../prelogin/poolStats.php');
