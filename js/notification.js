/*
*-----------------------------------------------------------------------------------------------------------
*    Notification Inbox javascript
*-----------------------------------------------------------------------------------------------------------
*   This landing javascript file is used to get function which are required to DropDown on page load and
*   on click event.
*/

function  notification(forwords,backwords,user_email,user_id){


var backwords_offset;
var forwords_offset;


function getDataPageToken(cursor,offset,flag){


    //var user_email = "<?php echo $email;?>"; //alert(user_email);
   // var user_id = "<?php echo $user_id ?>"; //alert(user_id);
    var catgory = $( ".selectCat option:selected" ).val(); //alert(catgory);

    var offsets ;
    var cursors;
    var value;
    if(flag=="0"){
        offsets = offset;
        value  = cursor;
        cursors = "fwd_cursor";
    }
    else if(flag=="1"){
        offsets = offset;
        value = cursor;
        cursors = "bkwd_cursor";
    }
    $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controllers/notification/getNotificationList.php",
            data: {
                id: user_id, cursor : cursors, value:cursor,offset_from: offsets,category: catgory }
            ,
            success:function(dataupdate){
                //alert('forword cursor'+dataupdate);

//            jQuery(".tableData").html("");
                jQuery(".tableData").html(dataupdate);
                var forwords = $('#fwd_cursor').val()
                var backwords= $('#bkwd_cursor').val()
                //alert('forwords'+forwords+'backwords'+backwords)
                if (forwords == '' || forwords == null){
                    jQuery(".forword").attr('disabled','disabled');
                }else{
                    jQuery(".forword").removeAttr("disabled");
                }
                if (backwords == "" || backwords == null){
                    jQuery(".backword").attr('disabled','disabled');
                }else{
                    jQuery(".backword").removeAttr("disabled");
                }
            }
            ,
            error: function () {
                alert(error);
            }
        }
    );
}



jQuery(document).ready(function(){
    var category = $('.selectCat').val();

    //window.localStorage.setItem("category", category);

  //  var notificationDetails = "<?php echo $arrCount; ?>";
 //   var forwords = "<?php echo $fwd_cursor; ?>";
  //  var backwords= "<?php echo $bkwd_cursor; ?>";
//        alert('forwords'+forwords+'backwords'+backwords)
 //   backwords_offset= "<?php echo $bkwd_cursor_offset_from; ?>";
 //   forwords_offset= "<?php echo $fwd_cursor_offset_from; ?>";
    if(notificationDetails < 0){
        jQuery(".mailinbox").hide();
        jQuery(".msgStatus").html("No Notifications");
        jQuery(".msgStatus").show();
        jQuery(".forword").attr('disabled','disabled');
        jQuery(".backword").attr('disabled','disabled');
    }

    if (forwords == '' || forwords == null){
        jQuery(".forword").attr('disabled','disabled');
    }else{
        jQuery(".forword").removeAttr("disabled");
    }
    if (backwords == "" || backwords == null){
        jQuery(".backword").attr('disabled','disabled');
    }else{
        jQuery(".backword").removeAttr("disabled");
    }

    // check all checkboxes in table
    if(jQuery('.checkall').length > 0) {
        jQuery('.checkall').click(function(){

                var parentTable = jQuery(this).parents('table');
                var ch = parentTable.find('.checkbox');
                if(jQuery(this).is(':checked')) {
                    //check all rows in table
                    ch.each(function(){
                            jQuery(this).prop('checked',true);
                            jQuery(this).parent().addClass('checked');
                            //used for the custom checkbox style
                            jQuery(this).parents('tr').addClass('selected');
                            // to highlight row as selected
                        }
                    );
                }
                else {
                    //uncheck all rows in table
                    ch.each(function(){
                            jQuery(this).prop('checked',false);
                            jQuery(this).parent().removeClass('checked');
                            //used for the custom checkbox style
                            jQuery(this).parents('tr').removeClass('selected');
                        }
                    );
                }
            }
        );
    }
    if(jQuery('.mailinbox').length > 0) {
        // star
        //  jQuery('.msgstar').click(function(){
        //  if(jQuery(this).hasClass('starred'))
        //  jQuery(this).removeClass('starred');
        //  else
        //  jQuery(this).addClass('starred');
        //});
        //add class selected to table row when checked
        jQuery('.mailinbox tbody input:checkbox').click(function(){
                if(jQuery(this).is(':checked'))
                    jQuery(this).parents('tr').addClass('selected');
                else
                    jQuery(this).parents('tr').removeClass('selected');
            }
        );
        // trash
        if(jQuery('.delete').length > 0) {
            //alert(jQuery('.delete').length);
            jQuery('.delete').click(function(ev){
                var chkdata = jQuery(".checkbox");
                var category = $( ".selectCat option:selected" ).val();
                window.localStorage.setItem("category", category);
                if(!$(".checkbox").is(':checked')){
                    alert('No selected message');
                } else{


                    var c = false;
                    var cn = 0;
                    var o = new Array();
                    var _id = "";
                    var jsonObj = [];
                    jQuery('.mailinbox .checkbox').each(function(){
                        if(jQuery(this).is(':checked')) {
                            c = true;
                            o[cn] = jQuery(this);
                            cn++;
                            var col_name = jQuery(this).attr("flag");
                            var statuschk = jQuery(this).attr("status");
                            var status;
                            if(statuschk == "1"){
                                status = true;
                            }
                            else{
                                status = false;
                            }
                            _id = jQuery(this).val();
                            item = {}
                            item ["_id"] = _id;
                            item ["collection_name"] = col_name;
                            item ["status"] = status;
                            jsonObj.push(item);
                        }
                    });

                    var doc_deldata = JSON.stringify(jsonObj);
                    var user_email = "<?php echo $email;?>";
                    var user_id = "<?php echo $id; ?>";
                    var doc_id = doc_deldata;
                    //console.log('docid '+doc_id);
                    //return;
                    //console.log('user id '+user_id);
                    //console.log('user email '+user_email);
                    $.ajax({
                            //alert("Chanchal");
                            type: "POST",
                            url: "../../controllers/notification/deleteNotification.php",
                            data: {
                                uid: (user_id),doc_id: (doc_id),email: (user_email) },
                            success:function(dataupdate){

                                window.location.replace("notification_inbox.php");

                            }
                            ,
                            error: function () {
                                alert(error);
                            }
                        }
                    );
                    if(!c) {
                        alert('No selected message');
                        ev.preventDefault();
                        return false;
                    }
                }
            });
        }
        // mark as read
        jQuery('.mark_read').click(function(){
                var category = $('.selectCat option:selected').val();

                window.localStorage.setItem("category", category);
                if(!$(".checkbox").is(':checked')){
                    alert('No selected message');
                } else{
                    var c = false;
                    var cn = 0;
                    var o = new Array();
                    var _id = "";
                    var jsonObj = [];
                    jQuery('.mailinbox .checkbox').each(function(){
                            if(jQuery(this).is(':checked')) {
                                c = true;
                                o[cn] = jQuery(this);
                                var col_name = jQuery(this).attr("flag");
                                var statuschk = jQuery(this).attr("status");
                                //  alert(statuschk);
                                var status;
                                if(statuschk == 1){
                                    status = true;
                                }
                                else{
                                    status = false;
                                }

                                _id = jQuery(this).val();
                                item = {}
                                item ["_id"] = _id;
                                item ["collection_name"] = col_name;
                                item ["status"] = status;
                                jsonObj.push(item);
                                cn++;
                            }
                        }
                    );

                    var doc_data = JSON.stringify(jsonObj);
                    console.log(doc_data);
                    //  return;
                 //   var user_email = "<?php echo $email;?>";
                   // var user_id = "<?php echo $user_id ?>";
                    var doc_id = doc_data;


                    $.ajax({
                            type: "POST",
                            // url: "keywords/controller/updateNotification.php",
                            data: {
                                uid: user_id,docid: doc_id,email: user_email
                            },
                            success:function(dataupdate){
                                //    console.log(dataupdate);
                                var obj =jQuery.parseJSON(dataupdate);
                                if(obj.errCode [-1]== [-1]){
                                    window.location.replace("notification_inbox.php");
                                }else if(obj.errCode[2] == 5){
                                    window.location.replace("notification_inbox.php");
                                }   else{
                                    alert("error in system");
                                }

                            }  ,
                            error: function () {
                                alert(error);
                            }
                        }
                    );
                    if(!c) {
                        alert('No selected message');

                    }
                }

            }
        );
        // make messages to unread
        jQuery('.mark_unread').click(function(){
                var c = false;
                var cn = 0;
                var o = new Array();
                jQuery('.mailinbox input:checkbox').each(function(){
                        if(jQuery(this).is(':checked')) {
                            c = true;
                            o[cn] = jQuery(this);
                            cn++;
                        }
                    }
                );
                if(!c) {
                    alert('No selected message');
                }
                else {
                    var msg = (o.length > 1)? 'messages' : 'message';
                    if(confirm('Mark '+o.length+' '+msg+' to unread')) {
                        for(var a=0;a<cn;a++) {
                            jQuery(o[a]).parents('tr').addClass('unread');
                        }
                    }
                }
            }
        );
    }



    // for select catgory
    jQuery(".selectCat").click(function(){

        var setCategory = jQuery(this).val();

        //  window.localStorage.setItem("category", setCategory);
        var setcat = setCategory;
        var user_email = "<?php echo $email;?>";
        var user_id = "<?php echo $user_id ?>";
//          alert('setcat'+setcat+''+user_email+''+user_id)
        $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controllers/notification/getNotificationList.php",
                data: {
                    id: user_id,category: setcat,email: user_email},
                success:function(dataGet){
                    // alert(dataGet);

                    jQuery(".tableData").html("");
                    jQuery(".tableData").html(dataGet);

                }
                ,
                error: function () {
                    alert(error);
                }
            }
        );
    });


    jQuery(".title").click(function(){
        var getID = jQuery(this).attr('id');
        var tocken_flag = jQuery(this).attr('tocken_flag');
        var statusRU = jQuery(this).attr('statusRU');

        // alert(getID);
        //alert(tocken_flag);

        jsonObj = [];
        item = {}

        var status;
        if(statusRU == "1"){
            status = true;
        }
        else{
            status = false;
        }
        _id = jQuery(this).val();
        item = {
        }
        item ["_id"] = getID;
        item ["collection_name"] = tocken_flag;
        item ["status"] = status;
        jsonObj.push(item);

        //    console.log(jsonObj);
        var doc_data = JSON.stringify(jsonObj);
        //   console.log(doc_data);
        var user_email = "<?php echo $email;?>";
        var user_id = "<?php echo $user_id ?>";
        var doc_id = doc_data;
        $.ajax({
            type: "POST",
            // url: "keywords/controller/updateNotification.php",
            data: {
                uid: user_id,docid: doc_id,email: user_email},
            success:function(dataupdate){
                //    console.log(dataupdate);
            },
            error: function () {
                alert(error);
            }
        });
    });
});
document.getElementById('download').click();
}
