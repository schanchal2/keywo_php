
$(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
    var that = this;

    //paste event
    if (event.type === "paste") {
        setTimeout(function() {
            $(that).val($(that).val().replace(/[^\d].+/, ""));
        }, 100);
    } else {

        if (event.which < 48 || event.which > 57) {
            event.preventDefault();
        } else {
            $(this).val($(this).val().replace(/[^\d].+/, ""));
        }
    }

});

function ShowHideDiv() {
    var ddlPassport = document.getElementById("ddlPassport");
    var dvPassport = document.getElementById("dvPassport");
    var dv1Passport = document.getElementById("dv1Passport");
    var dv2Passport = document.getElementById("dv2Passport");
    dvPassport.style.display = ddlPassport.value == "Wallet" ? "block" : "none";
    dv1Passport.style.display = ddlPassport.value == "BitGo" ? "block" : "none";
    dv2Passport.style.display = ddlPassport.value == "Paypal" ? "block" : "none";
}


// allow till current date selection.

$(document).ready(function () {
    var today = new Date();
    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy',
        autoclose:true,
        endDate: "today",
        maxDate: today
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });


    $('.datepicker').keyup(function () {
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9^-]/g, '');
        }
    });
});