/**
 * Created by trilok on 28/2/17.
 */

var acceptTermFlag = "false";
$(document).ready(function(){

    $(".btn-accept-licence").click(function(){
        $(this).css("background-color","#969798");
        $(this).css("box-shadow","1px 1px 1px 1px #969798");
        $(this).prop('disabled', true);
        $(".btn-pay-now").prop('disabled', false);
        $(".btn-pay-now").css('background-color',"#b96b0d");
        acceptTermFlag = true;
    });

});

$('#procceedToPayment').on('hidden.bs.modal', function (e) {
    // do something…
    /*cdpause();*/
    stopTimer();
});

function cancel(){
    $("#keyword-popup-confirmation").modal("hide");
}

/* Load user cart keywords on page load */
function cartCheckoutDataLoad(){
    var pageurl = rootUrl+"controllers/keywords/loadUserCheckoutCart.php?r="+Math.random();
    $("#checkoutCartTable").load(pageurl);
}

/* Load user payments box on page load */
function loadCheckoutPaymentBox(){
    var pageurl = rootUrl+"views/keywords/cart/checkoutPaymentBox.php?r="+Math.random();
    $("#paymentBox").load(pageurl);
}

/*function paymentContentCheckoutLoad(){
 $('#tabsCheckoutLoadDiv').load(rootUrl+"views/keywords/paymentContentCheckoutLoad.php?r="+Math.random()+"",function(){
 });
 }*/

function redirectToPayment(type, data, client_sid, keywords){
    type = type.toLowerCase();
    if(type == "wallet"){
        purchaseByWallet(type, data.newCartPriceUSD, client_sid, keywords)
    }else if(type == "paypal"){
        purchaseByPaypal(type, data, client_sid, keywords)
    }else if(type == "bitcoin"){
        purchaseByBitgo(type, data.newCartPriceUSD, data.purchasingAmtInITD, data.userAvailableBalance, data.newCartPriceBTC, client_sid, keywords)
    }
}

var dataResponse = "";
var payment_type = "";
function prePaymentProcess(type){
    payment_type = type;
    var client_sid = $("#clientSid").val();
    var userCartKeyword = keywordBasket;
    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/prePaymentProcess.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            finalamtInUSD: finalamtUSD
        },
        success: function(data){
            console.log(data);
            /*$("#response").text('');
             $("#response").text(data.errMsg);
             $("#keyword-popup-confirmation").modal("show");*/
            dataResponse = data;
            if(data.errCode == -1){

                var Msg = '';
                if(data.errMsg == "Proceed Payment with new Rates"){

                    Msg = "Are you Sure, You want to buy "+htmlAntiEntities(keywordBasketwithTags)+" for price of "+data.newCartPriceUSD+" "+keywoDefaultCurrency;

                    $("#response").text('');
                    $("#response").text(Msg);
                    $("#keyword-popup-confirmation").modal({
                       backdrop: 'static',
                       keyboard: false
                    });
                    $('#pay-now-by-wallet').prop("disabled", true);

                    // make ajax to generate btc and update tables
                }else if(data.errMsg == "Proceed Payment with current Rates"){
                    Msg = "Proceed payment with current rate "+htmlAntiEntities(keywordBasketwithTags)+" price of "+data.newCartPriceUSD+" "+keywoDefaultCurrency;
                    $("#response").text('');
                    $("#response").text(Msg);
                    $("#keyword-popup-confirmation").modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('#pay-now-by-wallet').prop("disabled", true);
                    $('#pay-now-by-wallet').css('background-color','rgb(150, 151, 152)'); //background-color:rgb(150, 151, 152)
                    // make ajax to generate btc and update tables
                }

            }else if(data.errMsg == "User cart is empty"){
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#pay-now-by-wallet').prop("disabled", true);
                $('#pay-now-by-wallet').css('background-color','rgb(150, 151, 152)');

            }else if(data.errMsg == "Slab rate changed"){
                $("#response").text('');
                $("#response").text("Slab rate changed, proceed with current slab rates.");
                $("#keyword-popup-confirmation").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $(".close").hide();
                $("#btnNo").hide();
                $("#btnYes").val("OK");
                $('#pay-now-by-wallet').css('background-color','rgb(150, 151, 152)');

            }else if(data.errMsg == "Cart consists of un-available keywords"){
                var notAvailbleKeywords = data.keyword_not_available;
                if(notAvailbleKeywords){
                    $('.errMsgDiv').hide();
                    $("#unavailableKeywordsspan").html("");
                    $("#unavailableKeywordsspan").html(notAvailbleKeywords);
                    $("#unavailableKeywords").css("display","block");
                }
            }else if(data.errMsg == "Check your available balance"){
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#pay-now-by-wallet').prop("disabled", true);
            }else if(data.errMsg == "Error blocking keywords for 15 minutes"){
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#pay-now-by-wallet').prop("disabled", true);
                $('#pay-now-by-wallet').css('background-color','rgb(150, 151, 152)');

            }else{
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#pay-now-by-wallet').prop("disabled", true);
                $('#pay-now-by-wallet').css('background-color','rgb(150, 151, 152)');

            }

        },
        error: function(xhr, status, error) {
            var errMsg = "Error initiating payment.Please try again after sometime."
            $("#response").text('');
            $("#response").text(errMsg);
            $("#keyword-popup-confirmation").modal({
                backdrop: 'static',
                keyboard: false
            });
        }
    });


}

function paypal(payment_type, client_sid, payment_mode, userCartKeyword, finalamtUSD){

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/paypalPrePaymentProcess.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            finalAmount: encodeURIComponent(finalamtUSD),
            mode: encodeURIComponent(payment_mode),
            orderId : encodeURIComponent(orderIdForPaypal),
            // keywordprice : encodeURIComponent(keywordprice)

        },
        success: function(data){

            dataResponse = data;

            if(data.errCode == -1){
                var Msg = '';

                Msg = 'You are purchasing '+data.purchasingAmtInITD+ ' '+keywoDefaultCurrency+' @'+data.newCartPriceSGD+ ' SGD using '+ payment_mode+'.';

                $("#response").text('');
                $("#response").text(Msg);
                $("#keyword-popup-confirmation").modal("show");
                $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');
            }else if(data.errMsg == "Cart consists of un-available keywords"){
                var notAvailbleKeywords = data.keyword_not_available;
                if(notAvailbleKeywords){
                    $('.errMsgDiv').hide();
                    $("#unavailableKeywordsspan").html("");
                    $("#unavailableKeywordsspan").html(notAvailbleKeywords);
                    $("#unavailableKeywords").css("display","block");
                }
            }else{
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal("show");
                $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');
            }
        },
        error: function(xhr, status, error) {
            var errMsg = "Error initiating payment.Please try again after sometime."
            $("#response").text('');
            $("#response").text(errMsg);
            $("#keyword-popup-confirmation").modal("show");
        }
    });
}

function paymentAfterConfirmation(){

    var data = dataResponse;

    if(data.errCode == -1){
        var keywordBasket = data.keyword_available;
        var keywordBasket = keywordBasket.replace('#','');
        var keywordBasket = keywordBasket.replace('&comma;','');
    }

    var client_sid = $("#clientSid").val();
    if(data.errMsg == "User cart is empty"){
        $("#keyword-popup-confirmation").modal("hide");
    }else if(data.errMsg == "Slab rate changed"){
        $("#keyword-popup-confirmation").modal("hide");
        location.reload();
        //cartCheckoutDataLoad();
    }else if(data.errMsg == "Check your available balance"){
        $("#keyword-popup-confirmation").modal("hide");
        /*location.reload();*/
        cartCheckoutDataLoad();
        loadCheckoutPaymentBox();
    }else if(data.errMsg == "Cart consists of un-available keywords"){
        $("#keyword-popup-confirmation").modal("hide");
    }else{
        // $("#keyword-popup-confirmation").modal("hide");
        $('.yes-btn').prop("disabled", true);
        //$('.yes-btn').css('background-color','rgb(150, 151, 152)');
        if(payment_type == 'other')
        {
            $("input[name='payment_mode']").each(function(){
                if($(this).is(':checked')) {
                    payment_type = $(this).val();
                }
            });
        }

        redirectToPayment(payment_type,data,client_sid,keywordBasket);
    }
}

function purchaseByWallet(paymentMode, totalPrice, client_sid, keywords){
    termsTempFlag = acceptTermFlag;
    acceptTermFlag = false;
    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/keywords/buyKeywordByWallet.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            mode: paymentMode,
            keywordBasket : keywords,
            termsFlag : termsTempFlag
        },
        success: function(data){

            console.log(data);

            if(data.errCode == -2){
                window.location.href = "../../two_factor_auth/index.php";
            }else  if(data.errCode == -1){
                showToast("success",data.errMsg);
                window.setTimeout(function(){
                    window.location.href = data.redirect_url;
                },2000);
            }else{
                showToast("failed",data.errMsg);
            }
            //location.reload();

        }
    });
}

function closeErrImgCheckoutPayment() {
    $('#unavailableKeywords').fadeOut('fast');
}

function purchaseByPaypal(type, data, client_sid, keywords){

    var totalAmount = data.newCartPriceSGD;
    var singleAmount = data.singleAmount;
    var kwdBasketPaypal = keywords;
    var custom = data.custom;


    $("#totalPricePaypal").val("");
    $("#totalPricePaypal").val(totalAmount);
    $("#itemPricePaypal").val("");
    $("#itemPricePaypal").val(singleAmount);
    $("#keywordBasketPaypal").val("");
    $("#keywordBasketPaypal").val(kwdBasketPaypal);
    $("#customPaypal").val("");
    $("#customPaypal").val(custom);

    $("#buyByPaypalForm").submit();

    // alert(keywords+"     "+orderIdForPaypal+"   "+totalPrice+"   "+client_sid); return;

    /* $.ajax({
     type: "POST",
     dataType:"JSON",
     url: rootUrl+"controllers/paypalPrePaymentProcess.php",
     data: {
     client_sid: encodeURIComponent(client_sid),
     finalAmount: encodeURIComponent(totalPrice),
     mode: encodeURIComponent(paymentMode),
     orderId : encodeURIComponent(orderIdForPaypal)

     },
     success: function(data){
     }
     });*/
}

function purchaseByBitgo(paymentMode, totalPrice, amount_due, walletBalance, amountDueInBTC, client_sid, keywords){

    var encryptedCustom = orderIdForBitgo;

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/keywords/getPaymentAddress.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            keywordBasket: keywords,
            encryptedCustom: encryptedCustom,
            finalAmount: totalPrice,
            mode: paymentMode
        },
        success: function(data){
            if(data.errCode == -1){
                window.localStorage.clear();
                btcAddress = data.paymentAddress;
                // hide the payment confirmation dialog box
                $("#keyword-popup-confirmation").modal("hide");
                // load payment dailog
                $('#procceedToPayment').load(rootUrl+"views/keywords/cart/bitgo_payment_dailog.php", {client_id: client_sid, finalBtcAmount:data.amountInBTC, finalUsdAmount:data.amountInUSD, recieverAddress:data.paymentAddress, keywordWithTags: keywords, orderId: encryptedCustom, amount_due:amount_due, wallet_balance:walletBalance, amount_due_in_btc: amountDueInBTC});

                $('#procceedToPayment').modal('show');

            }else{
                window.localStorage.clear();
                alert("Payment process failed");
            }

        }
    });

}

// Check for first confirmation from bitgo
function checkForBitgoConfirmation(){

    var encryptedCustom = orderIdForBitgo;
    var btcAddress = $.trim($("#btcAddress").val());
    var client_sid = $("#clientSid").val();

    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: rootUrl+"controllers/keywords/getPaymentConfirmation.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            orderId: encryptedCustom,
            recieverAddress: btcAddress
        },

        success: function(data){
            if(data.errCode == -1){
                var status = data.errMsg.status;
                stopTimer();
                $('.sucess').css('display','block');
                $('.dailogMess').css('display','none');
                $('#paymentStatus').html('');
                $('#paymentStatus').html('Payment success');
                $('.message').html('');
                if(status == 'paid')
                {
                    $('.message').html("We have recieved your payment amount "+data.errMsg.amount_recieved+" successfully");
                } else if(status == 'mispaid' || status == 'overpaid'){
                    $('.message').html("We have recieved your payment amount "+data.errMsg.amount_recieved+", Please contact searchtrade for refund "+status+" amount");
                } else {
                    $('.message').html("We did not recieved your payment");
                }

                cartCheckoutDataLoad();
                cartLoad();

                //var t = setTimeout(window.location='https://searchtrade.com/dashbord.php', 000);
                //Display message to user for success
            }else{
                console.log('Pending Confirmation')
            }
        }

    });

    //return true;
}

/*function prePaymentOthers(type){

 payment_type = type;
 var client_sid = $("#clientSid").val();
 var userCartKeyword = keywordBasket;
 $.ajax({
 type: "POST",
 dataType:"JSON",
 url: rootUrl+"controllers/prePaymentOthers.php",
 data: {
 client_sid: encodeURIComponent(client_sid),
 finalamtInUSD: finalamtUSD
 },
 success: function(data){
 /!*$("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");*!/
 dataResponse = data;
 if(data.errCode == -1){

 var Msg = '';
 if(data.errMsg == "Proceed Payment with new Rates"){

 Msg = "Are you Sure, You want to buy "+htmlAntiEntities(keywordBasketwithTags)+" for price of "+data.newCartPriceUSD+" "+keywoDefaultCurrency;

 $("#response").text('');
 $("#response").text(Msg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);

 // make ajax to generate btc and update tables
 }else if(data.errMsg == "Proceed Payment with current Rates"){
 Msg = "Proceed payment with current rate "+htmlAntiEntities(keywordBasketwithTags)+" price of "+data.newCartPriceUSD+" "+keywoDefaultCurrency;
 $("#response").text('');
 $("#response").text(Msg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);
 $('#pay-by-others').css('background-color','rgb(150, 151, 152)'); //background-color:rgb(150, 151, 152)
 // make ajax to generate btc and update tables
 }

 }else if(data.errMsg == "User cart is empty"){
 $("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);
 $('#pay-by-others').css('background-color','rgb(150, 151, 152)');

 }else if(data.errMsg == "Slab rate changed"){
 $("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').css('background-color','rgb(150, 151, 152)');

 }else if(data.errMsg == "Cart consists of un-available keywords"){
 var notAvailbleKeywords = data.keyword_not_available;
 if(notAvailbleKeywords){
 $('.errMsgDiv').hide();
 $("#unavailableKeywordsspan").html("");
 $("#unavailableKeywordsspan").html(notAvailbleKeywords);
 $("#unavailableKeywords").css("display","block");
 }
 }else if(data.errMsg == "Check your available balance"){
 $("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);
 }else if(data.errMsg == "Error blocking keywords for 15 minutes"){
 $("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);
 $('#pay-by-others').css('background-color','rgb(150, 151, 152)');

 }else{
 $("#response").text('');
 $("#response").text(data.errMsg);
 $("#keyword-popup-confirmation").modal("show");
 $('#pay-by-others').prop("disabled", true);
 $('#pay-by-others').css('background-color','rgb(150, 151, 152)');

 }

 },
 error: function(xhr, status, error) {
 var errMsg = "Error initiating payment.Please try again after sometime."
 $("#response").text('');
 $("#response").text(errMsg);
 $("#keyword-popup-confirmation").modal("show");
 }
 });

 }*/

function prePaymentOthers(type){
    payment_type = type;
    var client_sid = $("#clientSid").val();
    var userCartKeyword = keywordBasket;

    var payment_mode = $('input[name=payment_mode]:checked', '#payment-mode-selection').val();

    var currency = '';
    var purchasingAmt = 0;

    if(payment_mode == 'Bitcoin'){
        bitgo(payment_type, client_sid, payment_mode, userCartKeyword, finalamtUSD);
    }else if(payment_mode == 'PayPal'){
        paypal(payment_type, client_sid, payment_mode, userCartKeyword, finalamtUSD);
    }
}

function cancelPurchase(){
    $('#pay-by-others').css("background-color", "#b96b0d");
    $('#pay-by-others').prop("disabled", false);
    $('#pay-now-by-wallet').css("background-color", "#b96b0d");
    $('#pay-now-by-wallet').prop("disabled", false);
    cancel();
}


function bitgo(payment_type, client_sid, payment_mode, userCartKeyword, finalamtUSD){

    $.ajax({
        type: "POST",
        dataType:"JSON",
        url: rootUrl+"controllers/prePaymentOthers.php",
        data: {
            client_sid: encodeURIComponent(client_sid),
            finalamtInUSD: finalamtUSD
        },
        success: function(data){
            if(data.errCode == -1){
                dataResponse = data;
                var Msg = '';

               /* if(payment_mode == 'Bitcoin'){
                    currency = 'BTC';
                    purchasingAmt = data.newCartPriceBTC;
                }else{
                    currency = 'SGD';
                    purchasingAmt = data.newCartPriceSGD;
                }*/

                Msg = 'You are purchasing '+data.purchasingAmtInITD+ ' '+keywoDefaultCurrency+' @'+data.newCartPriceBTC+ ' BTC using '+ payment_mode+'.';

                $("#response").text('');
                $("#response").text(Msg);
                $("#keyword-popup-confirmation").modal("show");
                $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');
            }else{
                $("#response").text('');
                $("#response").text(data.errMsg);
                $("#keyword-popup-confirmation").modal("show");
                $('#pay-by-others').prop("disabled", true);
                $('#pay-by-others').css('background-color','rgb(150, 151, 152)');
            }
        },
        error: function(xhr, status, error) {
            var errMsg = "Error initiating payment.Please try again after sometime."
            $("#response").text('');
            $("#response").text(errMsg);
            $("#keyword-popup-confirmation").modal("show");
        }
    });
}

