/**
 * Created by amit on 2/5/17.
 */

$('#downloadReport').click(function(){
    //var months = { "Jan." : 01, "Feb." : 02, "Mar." : 03, "Apr." : 04, "May" : 05, "June" : 06, "July" : 07, "Aug." : 08, "Sep." : 09, "Oct." : 10, "Nov." : 11, "Dec." : 12 };
    var months = { "JAN" : 01, "FEB" : 02, "MAR" : 03, "APR" : 04, "MAY" : 05, "JUNE" : 06, "JULY" : 07, "AUG" : 08, "SEP" : 09, "OCT" : 10, "NOV" : 11, "DEC" : 12 };

    var currentMonth = $('.ui-state-active .ui-button-text').text();
    var monthNum = '';
    // get current year
    var currentYear = (new Date()).getFullYear();
    var monthReport = '';
    var reportName = '';

    var transType = $('#transType').val();

    if(jQuery.inArray(currentMonth, months) == -1) {
        if(months[currentMonth] < 10){
            monthNum = '0'+months[currentMonth];
        }else{
            monthNum = months[currentMonth];
        }
    }

     monthReport = monthNum+'_'+currentYear+'_transaction';
    reportName = currentMonth+'_'+currentYear;

    var walletParameter="<input type='hidden' name='reportformonth' value='"+monthReport+"'><input type='hidden' name='type' value='"+transType+"'><input type='hidden' name='reportName' value='"+reportName+"' >";
    $("#walletDynamicParameter").html("");
    $("#walletDynamicParameter").html(walletParameter);
    $("#downloadWalletData").submit();


});

$('#bidSent').click(function(){
    getBidSentData();
});

$('#bidReceive').click(function(){
    getBidRecieveData();
});


$('#activeTradeAll').click(function(){
    getActiveTradeData();
});


////////////////////////////////////////////

$('#downloadTradeReport').click(function(){
    var months = { "Jan." : 01, "Feb." : 02, "Mar." : 03, "Apr." : 04, "May" : 05, "June" : 06, "July" : 07, "Aug." : 08, "Sep." : 09, "Oct." : 10, "Nov." : 11, "Dec." : 12 };
    var currentMonth = $('.ui-state-active .ui-button-text').text();
    var monthNum = '';
    // get current year
    var currentYear = (new Date()).getFullYear();
    var monthReport = '';
    var reportName = '';

    var tradeType = $('#tradeSelection').val();

    if(jQuery.inArray(currentMonth, months) == -1) {
        if(months[currentMonth] < 10){
            monthNum = '0'+months[currentMonth];
        }else{
            monthNum = months[currentMonth];
        }
    }

    monthReport = monthNum+'_'+currentYear+'_transaction';
    reportName = currentMonth+'_'+currentYear;

    var tradeParameter="<input type='hidden' name='type' value='"+tradeType+"'><input type='hidden' name='reportName' value='"+reportName+"' >";
    $("#tradeDynamicParameter").html("");
    $("#tradeDynamicParameter").html(tradeParameter);
    $("#downloadTradeData").submit();


});


$('#downloadLikeReport').click(function(){
    var months       = { "Jan." : 01, "Feb." : 02, "Mar." : 03, "Apr." : 04, "May" : 05, "June" : 06, "July" : 07, "Aug." : 08, "Sep." : 09, "Oct." : 10, "Nov." : 11, "Dec." : 12 };
    var currentMonth = $('.ui-state-active .ui-button-text').text();
    var monthNum     = '';
    var currentYear  = (new Date()).getFullYear();
    var reportName   = '';
    var postType     = $('#likeSelection').val();
    var file         =  window.location;

    reportName = currentMonth + '-' + currentYear;

    var tradeParameter = "<input type='hidden' name='file' value='"+file+"'><input type='hidden' name='type' value='"+postType+"'><input type='hidden' name='reportName' value='"+reportName+"' >";

    $("#likeDynamicParameter").html("");
    $("#likeDynamicParameter").html(tradeParameter);
    $("#downloadLikeData").submit();
});

$('#downloadUploadReport').click(function(){
    var months       = { "Jan." : 01, "Feb." : 02, "Mar." : 03, "Apr." : 04, "May" : 05, "June" : 06, "July" : 07, "Aug." : 08, "Sep." : 09, "Oct." : 10, "Nov." : 11, "Dec." : 12 };
    var currentMonth = $('.ui-state-active .ui-button-text').text();
    var monthNum     = '';
    var currentYear  = (new Date()).getFullYear();
    var reportName   = '';
    var postType     = $('#uploadSelection').val();
    var file         =  window.location;

    reportName = currentMonth + '-' + currentYear;

    var tradeParameter = "<input type='hidden' name='file' value='"+file+"'><input type='hidden' name='type' value='"+postType+"'><input type='hidden' name='reportName' value='"+reportName+"' >";

    $("#uploadDynamicParameter").html("");
    $("#uploadDynamicParameter").html(tradeParameter);
    $("#downloadUploadData").submit();
});