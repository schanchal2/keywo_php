/*
 *-----------------------------------------------------------------------------------------------------------
 *    Currency Converter ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Currency Converter which are required to load on page load
 */


var cityName = '<?php echo $ipcity; ?>';

function callphpCurrfunction(t) {
    var flag = $("#" + t).attr("flag");
    var isTyping = $("#" + t).val();
    var isTyping1 = $('#currcode').val();
    var fullnameto = $('option:selected', "#currcode").attr('fullname'); //alert(fullnameto);
    var isTyping2 = $('#currcode2').val();
    var fullnameFrom = $('option:selected', "#currcode2").attr('fullname');
    if (t === "currcode" || t === "currcode2") {
        var isTyping = $("#changeCurr").val();
    }
    if (flag == 4) {
        var allset = isTyping + " " + isTyping2 + " " + isTyping1 + " " + flag
    } else {
        var allset = isTyping + " " + isTyping1 + " " + isTyping2 + " " + flag;
    }
    var data = allset;

    $.ajax({
        type: 'POST',
        dataType: 'json',
        data: {q: allset},
        url: '../../../controllers/search/refresh_currencyController.php',
        success: function (data) {

            $("#from_curr").text(fullnameto);
            $("#to_curr").text(fullnameFrom);
            if (data["flag"] == 4) {
                if (isTyping == "") {
                    $("#changeCurr").val('0');
                    $("#amt").text('0');
                } else {
                    $("#changeCurr").val(data["amt"]);
                    $("#d_amt").text(isTyping);
                    $("#amt").text(data["amt"]);
                }
            } else {
                if (isTyping == "") {
                    //aler("testing");
                    $("#newchangeCurr").val('0');
                    $("#d_amt").text('0');
                } else {
                    $("#newchangeCurr").val(data["amt"]);
                    $("#d_amt").text(data["amt"]);
                    $("#amt").text(isTyping);
                }
            }


        }
    });
}


function isNumberKey(evt) {
    var target = evt.target || evt.srcElement;
    var charCode = (evt.which) ? evt.which : event.keyCode
    var parts = target.value.split('.');
    if (charCode > 31 && (charCode < 45 || charCode > 57) || (parts.length > 1 && charCode == 46))
        return false;
    return true;
}

/*

 *-----------------------------------------------------------------------------------------------------------
 *    TimeZone Converter ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of TimeZone Converter which are required to load on page load

 */


function callfunction(v){
    var ch_tm = $('#time').val();
    var isTyping1 = $('#fromtimecode_int').val();
    var isTyping2 = $('#totimecode_int').val();
    var allset = ch_tm+" "+isTyping1+" "+isTyping2;
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{q:allset},
        url: '../../../controllers/search/changetimezoneController.php',
        success: function(data){
            $("#first_tz_int").text(data["select_date"]);
            $("#sec_tz_int").text(data["int_date"]);
        }
    });
}
function callphpTimefunction(t){
    var flag=$("#"+ t).attr("flag");
    var isTyping = $("#"+ t).val();
    var tz_l = isTyping.substring ( 0, isTyping.indexOf ( "," ) );
    var tz_n = isTyping.substring ( isTyping.indexOf ( "," ) + 1 );
    var isTyping1 = $('#fromtimecode_int').val();
    var isTyping2 = $('#totimecode_int').val();
    var tz_isTyping1 = isTyping1.substring ( 0, isTyping1.indexOf ( "," ) );
    var fullnameto = $('option:selected', "#fromtimecode_int").attr('fullname');
    var fullnameFrom = $('option:selected', "#totimecode_int").attr('fullname');
    var tz_isTyping2 = isTyping2.substring ( 0, isTyping2.indexOf ( "," ) );
    if(flag==3){
        var allset = tz_l+" "+tz_n+" "+tz_isTyping2+" "+flag;
    }else{
        var allset = tz_l+" "+tz_n+" "+tz_isTyping1+" "+flag;
    }
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{q:allset},
        url: '../../../controllers/search/refreshtimezoneController.php',
        success: function(data){
            if(data["flag"]==3){
                $("#fromtm").text(data["long"]);
                $("#fromtm_diff").text(data["long"]);
                $("#fromtm1").text(data["short"]);
                $("#fromtm_int").text(data["long"]);
                $("#fromtm1_int").text(data["short"]);
                $("#first_tz").text(data["res_date"]);
                $("#res_diff").text(data["time_diff"]);
                $("#first_tz_int").text(data["int_date"]);
            }else{
                $("#totm").text(data["long"]);
                $("#totm_diff").text(data["long"]);
                $("#totm1").text(data["short"]);
                $("#totm_int").text(data["long"]);
                $("#totm1_int").text(data["short"]);
                $("#sec_tz").text(data["res_date"]);
                $("#res_diff").text(data["time_diff"]);
                $("#sec_tz_int").text(data["int_date"]);
            }
        }
    });
}

/*

 *-----------------------------------------------------------------------------------------------------------
 *    Unit Converter ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Unit Converter which are required to load on page load

 */

function callphpUnitfunction(){
    var isTyping = document.getElementById("unitname").val;
    var to = document.getElementById("to").val;
    var frm = document.getElementById("frm").val;
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{q:isTyping},
        url: '../../../controllers/search/refreshUnitController.php',
        success: function(data){

            $("#unit_set1").empty();
            $("#unit_set2").empty();
            var InvestigationHtml = "";
            var InvestigationHtmll = "";
            if(data["errCode"]==-1){
                console.log(data);

                var x = data["set"];
                var x = x.split(",");

                console.log(x);
                InvestigationHtml += '<select class="unitSelect form-control" onchange="changeunitset(this.id)"  type="text" id="unit_set_to" name="Investigation[]" >';
                var i;
                for (i = 0; i < x.length; i++) {
                    if(to == x[i]){
                        InvestigationHtml += '<option  value="'+x[i]+'" selected>'+x[i]+'</option>';

                    }else{
                        InvestigationHtml += '<option  value="'+x[i]+'" >'+x[i]+'</option>';
                    }
                }
                InvestigationHtml += '</select>';
                InvestigationHtmll += '<select class="unitSelect form-control" onchange="changeunitset(this.id)"  type="text" id="unit_set_from" name="Investigation[]" >';
                var i;
                for (i = 0; i < x.length; i++) {
                    if(frm == x[i]){
                        InvestigationHtmll += '<option  value="'+x[i]+'" selected>'+x[i]+'</option>';
                    }else{
                        InvestigationHtmll += '<option  value="'+x[i]+'" >'+x[i]+'</option>';
                    }
                }
                InvestigationHtmll += '</select>';
            }else{
                InvestigationHtml = "error fetching investigation";
            }
            $("#unit_set1").html(InvestigationHtmll);
            $("#unit_set2").html(InvestigationHtml);
            changeunitset(this.id);
        }
    });
}
function changeunitset(v){
    var action=v;
    var type = $('#unitname').val();
    var q = $('#fromnumber').val();
    var p = $('#tonumber').val();
    var x = $( "#unit_set_from option:selected" ).text();
    var y = $( "#unit_set_to option:selected" ).text();
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{q:q,p:p,x:x,y:y,action:action,type:type},
        url: '../../../controllers/search/unitCalcController.php',
        success: function(data){
            if(data["errCode"] == -1){
                if(data["Msg"] == "fromvalue"){
                    $("#tonumber").val(data["errMsg"]["value"]);
                }
                if(data["Msg"] == "tovalue"){
                    $("#fromnumber").val(data["errMsg"]["value"]);
                }
            }
        }
    });
}
function isNumberKey(evt){
    var target = evt.target || evt.srcElement;
    var charCode = (evt.which) ? evt.which : event.keyCode
    var parts = target.value.split('.');
    if (charCode > 31 && (charCode < 45 || charCode > 57) || (parts.length > 1 && charCode == 46))
        return false;
    return true;
}


/*
 *-----------------------------------------------------------------------------------------------------------
 *    Calculator ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Calculator which are required to load on page load
 */


function calcPerc(Fi,Ei,n) {
    AppendFlag = 1;
    var input =  document.forms["calciForm"].elements[Ei].value;
    document.getElementById("output").value = eval(eval(document.forms["calciForm"].elements[Ei].value) / 100);
    document.getElementById('dot').removeAttribute("disabled");

}
var cnt = 0;
$("#output").keypress(function(event) {
    if (event.which == 37) {
        if(document.getElementById("output").value  != ''){
            calcPerc(0,0,'%');
        }
        event.preventDefault();
        return false;
    }
});

function TextMarquee()
{
    Pos++;
    var TextOut = "";
    if (Pos == TextLength)
        Pos = 1 - Width;
    if (Pos < 0)
    {
        for (var i=1; i<=Math.abs(Pos); i++)
            TextOut = TextOut + " ";
        TextOut = TextOut + Text.substring(0,Width-i+1);
    }
    else
        TextOut = TextOut + Text.substring(Pos,Width+Pos);
    window.status = TextOut;
    window.setTimeout("TextMarquee()",Speed);
}
function Clr(Fi,Ei)
{
    AppendFlag = 1;
    flag2 = 0;
    popen = 0;
    point = 0;
    cnt = 0;
    document.getElementById("output").value = '';
    document.getElementById("output").focus();

    document.getElementById('dot').removeAttribute("disabled");


}
$("#output").click(function(event) {
    if( document.getElementById("output").value  == '0') { document.getElementById("output").value = '';  cnt = 0;}
});

$("#output").keypress(function(event) {

    if(AppendFlag == 1){
        document.getElementById("output").value = '';
        AppendFlag = 0;
    }
    if (event.which == 13) {
        if(document.getElementById("output").value  != ''){

            var st = document.forms["calciForm"].elements[0].value;

            var len = st.length;

            var strChar = st.charAt(len - 1);
            var lastChar = strChar.charCodeAt(0);

            if (lastChar == 42 || lastChar == 43 || lastChar == 45 || lastChar == 46  || lastChar == 47){

                event.preventDefault();
                return false;

            }

            Res(0,0);
        }
        event.preventDefault();
        //return false;
    }
    if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; cnt = 0;}
});

$("#output").keypress(function(event) {
    if(document.getElementById("output").value  == ''){
        cnt = 0;

    }

});
$("#search_box").keypress(function(event) {
    if(event.which == 61){
        Res(0,0);
        event.preventDefault();
        return false;
    }
    if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; }
});

$("#output").keyup(function(event) {

    var st = document.forms["calciForm"].elements[0].value;
    var len = st.length;
    var strChar = st.charAt(st.length - 1);
    var lastChar = strChar.charCodeAt(0);

    // var remaingData = st.slice(0,-1);
    // var keyPressChar = String.fromCharCode(event.charCode);
    if (len==1 && (lastChar == 42 || lastChar == 43 || lastChar == 45 || lastChar == 46  || lastChar == 47)){
        //if (('/*-+.'.indexOf(keyPressChar) !== -1)){

        var result = 0 + strChar;

        document.forms["calciForm"].elements[0].value = result;
        event.preventDefault();
        return false;
        //}
    }

    if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; cnt = 0; }
});

function Res(Fi,Ei)
{
    var str = document.forms["calciForm"].elements[Ei].value;
    if ((flag2 == 0) && (popen == 0))
    {

        for (i = 0; i < str.length; i++)
            if (str.charAt(i) == '.') {

                var length=parseInt(str.length/2);
                var d=Math.pow(10, length);
                var x = eval(document.forms["calciForm"].elements[Ei].value);
                var input = document.getElementById("output").value =Math.round((x)* d)/  d;
                point=1;

                if(point==1){
                    $("#dot").attr("disabled","disabled");

                }
            }
        var display = document.forms["calciForm"].elements[Ei].value;

        var x = eval(document.forms["calciForm"].elements[Ei].value);
        document.getElementById("output").value = parseFloat(x);
        flag = 0;
        point = 0;
        //AppendFlag = 1;

    }

}


function Add(Fi,Ei,n)
{


    if(AppendFlag == 1){
        document.forms["calciForm"].elements[Ei].value = '';
        AppendFlag = 0;
    }
    var st = document.forms["calciForm"].elements[Ei].value;
    var len = st.length;
    if (st.charAt(len-1) == ")")
        return;
    if (n == "PI")
    {
        if (st.charAt(len-1) == ")" || st.charAt(len-1) == "0" || st.charAt(len-1) == "1" || st.charAt(len-1) == "2" || st.charAt(len-1) == "3" || st.charAt(len-1) == "4" || st.charAt(len-1) == "5" || st.charAt(len-1) == "6" || st.charAt(len-1) == "7" || st.charAt(len-1) == "8" || st.charAt(len-1) == "9")
            return;
        AppendFlag = 1;
        document.getElementById("output").value == Math.PI;
        var x = Math.PI;
        n = x;
    }
    if(document.forms["calciForm"].elements[Ei].value == '0')
    {
        if(n === '0'){

        }else{
            document.forms["calciForm"].elements[Ei].value = n;
        }
    }else{
        var secondLastChar = st.charAt(st.length - 2);
        var lastChar = st.charAt(st.length - 1);

        if ( ('/*-+.'.indexOf(secondLastChar) !== -1) && (secondLastChar !='') && (lastChar == '0')){
            var remaingData = st.slice(0,-1);
            document.forms["calciForm"].elements[Ei].value = remaingData + n;
        }else{
            document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
        }
    }

    document.getElementById("output").value =(document.forms["calciForm"].elements[Ei].value);
    flag = 1;
    flag2 = 0;

    document.getElementById('dot').removeAttribute("disabled");

}
function Par(Fi,Ei,n)
{
    var st = document.forms["calciForm"].elements[Ei].value;
    var len = st.length;
    if ( (n == "(") )
    {
        AppendFlag = 0;
        if (st.charAt(len-1) == ")") {
            return;
        }else if (st.charAt(len-1) == "("){
            popen++;
        }else
        {
            if ( (len > 0) && (flag2 == 0) ){
                if(st == '0'){
                    st = document.forms["calciForm"].elements[Ei].value = ''+n;
                    popen++;
                    return;
                }

                return;
            }else{
                popen++;
            }
        }
    }

    else if ( (n == ")") && (popen > 0) )
    {
        if (st.charAt(len-1) == "(")
            return;
        else
            popen--;
    }
    else
        return;
    document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
    document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
    flag = 1;
    point = 0;

}
function Bck(Fi,Ei)
{

    var st = document.forms["calciForm"].elements[Ei].value;

    var len = st.length;
    cnt = 0;
    if (len >= 0)
    {
        if (st.charAt(len-1) == ".")
            point = 0;
        if (st.charAt(len-1) == "+")
            flag2 = 0;
        if (st.charAt(len-1) == "-")
            flag2 = 0;
        if (st.charAt(len-1) == "*")
            flag2 = 0;
        if (st.charAt(len-1) == "/")
            flag2 = 0;
        if (st.charAt(len-1) == ")")
            popen++;
        if (st.charAt(len-1) == "(")
            popen--;
        document.forms["calciForm"].elements[Ei].value = st.substring(0,len-1);
        document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
        flag = 1;
    }
    if (len === 1){
        document.getElementById("output").value = 0;
    }

    document.getElementById('dot').removeAttribute("disabled");




}
function Pnt(Fi,Ei)
{
    AppendFlag = 0;
    if ( (flag2 == 0) && (point == 0) )
    {

        flag2 = 1;
        flag = 1;
        point = 1;
        document.forms["calciForm"].elements[Ei].value =
            document.forms["calciForm"].elements[Ei].value + ".";
        document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
    }


}

function Fnc(Fi,Ei,n)
{
    AppendFlag = 0;
    var st = document.forms["calciForm"].elements[Ei].value;
    var len = st.length;
    if (st.charAt(len-1) == "(" && n == '-')
        document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
    var lastChar = st.charAt(st.length - 1);
    if(('/*-+'.indexOf(lastChar) !== -1)){
        if ( ('/*-+'.indexOf(n) !== -1) ){
            var remaingData = st.slice(0,-1);
            document.forms["calciForm"].elements[Ei].value = remaingData + n;
        }
    }else{
        if (flag2 == 0)
        {
            if(st == 0 && n == '-'){
                document.forms["calciForm"].elements[Ei].value = '';
            }
            flag2 = 1;
            flag = 1;
            point = 0;
            document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
            document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;

        }

    }
    document.getElementById('dot').removeAttribute("disabled");


}



function Inv()
{
    inv = 1;
}

function Spc(Fi,Ei,f)
{
    var y = '';
    if ( (flag2 == 0) && (popen == 0) )
    {
        var st = document.forms["calciForm"].elements[Ei].value;
        var x = '';
        if(st > 0){
            if (st.length == 0)
                x = 0;
            else
                x = eval(st);
            if (f=="sqrt")
            {
                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }
                if (inv == 0)
                    y = (x>0) ? Math.sqrt(x) : "";
                else
                    y = x * x;
            }
            if (f=="pow2")
            {
                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }
                if (inv == 1)
                    y = (x>0) ? Math.sqrt(x) : "";
                else
                    y = x * x;
            }
            if (f=="ln")
            {
                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }
                if (inv == 0)
                    y = (x>0) ? Math.log(x) : "";
                else
                    y = Math.exp(x);
            }
        }else{
            AppendFlag = 1;
            y = "Invalid Input";
        }
        if (f=="exp")
        {
            if (inv == 1)
                y = (x>0) ? Math.log(x) : "";
            else
                y = Math.exp(x);
        }
        if (f=="sin")
        {
            if(AppendFlag == 0){
                y = '';
                AppendFlag = 1;
            }
            if (inv == 0)

            //y = parseFloat(Math.sin(x*fact).toFixed());
                y = parseFloat(Math.sin(x*fact));

            else
                y = 1./fact * Math.asin(x);
        }
        if (f=="cos")
        {


            if(AppendFlag == 0){
                y = '';
                AppendFlag = 1;
            }
            if (inv == 0)
            //y = parseFloat(Math.cos(x*fact).toFixed());
                y = parseFloat(Math.cos(x*fact));
            else
                y = 1./fact * Math.acos(x);
        }
        if (f=="tan")
        {

            if(AppendFlag == 0){
                y = '';
                AppendFlag = 1;
            }


            if (inv == 0){

                //y = parseFloat(Math.tan(x*fact).toFixed());
                y = parseFloat(Math.tan(x*fact));
            }

            else
                y = 1./fact * Math.atan(x);
        }
        if (f=="inv")
        {
            y = (inv==0) ? 1 / x : x;
        }
        document.forms["calciForm"].elements[Ei].value = y;
        document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
        inv = 0;
        point = 0;
    }
}
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 37 || charCode > 57) &&(charCode < 90 || charCode > 94 ))
        return false;
    return true;
}

function IsOneDecimalPoint(evt)
{
    var input = document.getElementById("output").value;

    if (event.which == 46) {
        cnt++;
        //event.preventDefault();
        if(cnt > 1){
            if($.isNumeric(input)){
                return false;
            }

        }
    }
    var target = evt.target || evt.srcElement;
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 37 || charCode > 57) &&(charCode < 90 || charCode > 94 ))
        return false;
    return true;

}





/*

 *-----------------------------------------------------------------------------------------------------------
 *    Weather Forecast ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Weather Forecast which are required to load on page load

 */


$(document).ready(function(){

    $("#widgetHtml").css({"display":"block"});
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{city:cityName},
        url: '../../../controllers/search/weatherHelperController.php',
        cache:false,
        success: function(data){
            if(cityName != ""){
                 document.getElementById('forcast').style.display = "block";
                $("#weatherdesc").html(data["desc"]);
                $("#wtempcel").html(data["temp"]);
                $("#humidity").html(data["hm"]+'%');
                $("#windspeed").html(data["wind_speed"]+' '+'km/hr');
                $("#weatherdt").html(data["datetime"]);

                if(data["w_pic"] === null){
                    $("#weathericon").attr("src", url);
                    $("#weatherdesc").html('Description Not available');
                }else{
                    var url = "https://openweathermap.org/img/w/"+data["w_pic"]+".png";
                    $("#weathericon").attr("src", url);
                }
            }else{
                $("#widgetHtml").css({"display":"block"});
                $(".dialog_apps").css({"padding-bottom":"0px"});
            }
        }
    });
});

function getweather(t){
    document.getElementById('dropdown').style.display = "none";
    document.getElementById('close').style.display = "none";
    document.getElementById('change').style.display = "block";
    var sel_city = $('#city-list :selected').text();
    var sel_con = $('#country-list :selected').text();
    var sel_sta = $('#state-list :selected').text();
    var allset = sel_city+", "+sel_sta+", "+sel_con;
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{city:sel_city},
        url: '../../../controllers/search/weatherHelperController.php',
        cache:false,
        success: function(data){
             document.getElementById('forcast').style.display = "block";
            document.getElementById('weatherloc').style.display = "inline";
            $("#weatherloc").html(allset);
            $("#weatherdesc").html(data["desc"]);
            $("#wtempcel").html(data["temp"]);
            $("#humidity").html(data["hm"]+'%');
            $("#windspeed").html(data["wind_speed"]+' '+'km/hr');
            $("#weatherdt").html(data["datetime"]);
            if(data["w_pic"] === null){
                //var url = "<?php echo $rootUrlImages; ?>fog.png";
                $("#weathericon").attr("src", url);
                $("#weatherdesc").html('Description Not available');
            }else{
                var url = "https://openweathermap.org/img/w/"+data["w_pic"]+".png";
                $("#weathericon").attr("src", url);
            }
        }
    });


}
function getState() {
    var val = document.getElementById('country-list').value;
    $.ajax({
        type: "POST",
        url: "../../../controllers/widget/getStates.php",
        data:'countryID='+val,
        success: function(data){
            $("#state-list").html(data);
        }
    });
}
function getCity() {
    var val = document.getElementById('state-list').value;
    $.ajax({
        type: "POST",
        url: "../../../controllers/widget/getCities.php",
        data:'stateID='+val,
        success: function(data){
            $("#city-list").html(data);
        }
    });
}

function showDiv() {
    document.getElementById('dropdown').style.display = "block";
    document.getElementById('close').style.display = "block";
    document.getElementById('weatherloc').style.display = "none";
    document.getElementById('change').style.display = "none";
}
function hideDiv() {
    document.getElementById('dropdown').style.display = "none";
    document.getElementById('close').style.display = "none";
    document.getElementById('weatherloc').style.display = "block";
    document.getElementById('change').style.display = "block";
}
function myCel(flag) {
    if(flag == 0){
        var num1 = document.getElementById('wtempcel');
        var t_frt1 = num1.innerHTML;
        var t_cel1 = (t_frt1 - 32) / 1.8;
        document.getElementById('wtempcel').innerHTML= Math.round(t_cel1);
        document.getElementById('weatherf').setAttribute("onClick","myCel(1);");
        document.getElementById('weatherc').setAttribute("onClick","");
        document.getElementById("weatherf").style.color = "blue";
        document.getElementById("weatherc").style.color = "black";
    }else{
        var num = document.getElementById('wtempcel');
        var t_cel = num.innerHTML;
        var t_frt = ((t_cel* 9) / 5) + 32;
        document.getElementById('wtempcel').innerHTML= Math.round(t_frt);
        document.getElementById('weatherc').setAttribute("onClick","myCel(0);");
        document.getElementById('weatherf').setAttribute("onClick","");
        document.getElementById("weatherc").style.color = "blue";
        document.getElementById("weatherf").style.color = "black";
    }
}


/*
 *-----------------------------------------------------------------------------------------------------------
 *    Language Translator ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Language Translator which are required to load on page load
 */


function toCount(entrance,exit,text,characters) {

    $('.tw-text-large').css({"font-size":"20px"});
    if(appStatus == "TRUE"){
        $('#textArea').css({"min-height": "40px"});
    }else{
        $('#textArea').css({"min-height": "75px"});
    }

    document.getElementById("textArea").style.overflow = 'hidden';
    document.getElementById("textArea").style.height = 0;
    document.getElementById("textArea").style.height = document.getElementById("textArea").scrollHeight + 'px';
    var entranceObj=document.getElementById(entrance);
    var exitObj=document.getElementById(exit);
    var length=characters - entranceObj.value.length;

    if(length <= 0) {

        length=0;
        text='<span class="disable"> '+text+' <\/span>';
        entranceObj.value=entranceObj.value.substr(0,characters);
    }
    exitObj.innerHTML = text.replace("{CHAR}",length);
    var text_length = $('#textArea').val().length;

    if(length == 256){
        $('#textTranslate').html("Translation");
        if(appStatus == "TRUE"){
            $('#textArea').css({"height": "40px"});
        }else{
            $('#textArea').css({"height": "75px"});
        }
        $('.tw-text-large').css({"font-size":"45px", "line-height":"normal" });
    }
    if(text_length > 256){
        alert("Character Size Limit Exceed");
    }
}
function swap()
{
    //var langList = $('#langList').val(); //alert(langList);
    var one = $("#textArea").val();//alert(one);
    var two = $("#textTranslate").html();//alert(two);
    if(one !== ""){
        var temp = one;
        one = two;
        two = temp;
        document.getElementById('textArea').value = one;
        if(one == "Translation"){
            document.getElementById('textArea').value = "";
        }
        document.getElementById('textTranslate').innerHTML = two;
    }
}
function calltranslatorfunction(t){
    //alert("Insert in js");
    var isTypingTo = $('#langcodeTo').val();
    var isTypingFrom = $('#langcodeFrom').val();
    var isTyping = $("#textArea").val();
    var data = isTypingFrom+" "+isTypingTo;
    var url = "../../../controllers/search/RefreshtranslatorController.php";
    //alert("inserted in variable");
    if(appStatus == "TRUE"){
        url = "../../../controllers/search/RefreshtranslatorController.php";
    }
    if(data !== "" && isTyping !== "")
    {
        $.ajax({
            type: 'POST',
            dataType:'json',
            data:{q:data, isTyping:isTyping},
            url: url,
            beforeSend: function(){
                $(".translate").html("Translating...");
            },
            success: function(data){
                var convertedText = data["textT"];
                $("#textTranslate").html(convertedText);
                $(".translate").html("Translate");
            }
        });
    }
}


/*
 *-----------------------------------------------------------------------------------------------------------
 *    Crypto Currency ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Crypto Currency which are required to load on page load
 */



function adjFont(){
    $(".adjFont").each(function(){
        var value,countVal;
        value = $(this).text();
        var fontcount = value.length;
        if(fontcount>18){
            countVal=24*(18/fontcount);
            var roundfig = Math.round(countVal);
            $(this).css('font-size',''+roundfig+'px');
        }
    });
}

$("body").click(function(event){
    if(appStatus == "TRUE"){
        if($(event.target).attr('class').toString() != "btn dropdown-toggle btn-default" && $(event.target).attr('class').toString() != "filter-option pull-left"){
            $(".dropdown-menu").css("display", "none");
        }else{
            if($(".dropdown-menu").is(":visible")){
                $(".dropdown-menu").css("display", "none");
            }else{
                $(".dropdown-menu").css("display", "block");
            }
        }
    }
});

function ajaxCall() {
    //alert("Inserted in ajax");
    var searchData = document.getElementById("currencySearch").value;
    //alert("Inserted in value");
    var url = "../../../crypto_json/get_crypto_currancy_data.php";
    if(appStatus == "TRUE"){
        url = "../../../crypto_json/get_crypto_currancy_data.php";
    }
    $.ajax({
        type: 'POST',
        dataType:'json',
        data:{searchCurrancy: searchData},
        url: url,
        success: function(data){
            var imagePathAbs = 'http://coinmarketcap.com/static/img/coins/16x16/';
            var red = 'red';
            var green = 'green';
            var id = data['id'];
            var name = data['name'];
            var short_code = data['symbol'];
            var priceUsd = data['price_usd'];
            var percent_change1h = data['percent_change_1h'];
            var percent_change24h = data['percent_change_24h'];
            var percent_change7d = data['percent_change_7d'];
            var market_capUsd = data['market_cap_usd'];
            var one_day_volumeUsd = data['one_day_volume_usd'];
            var availableSupply = data['available_supply'];
            var imageSrc = imagePathAbs + id + '.png';
            if(percent_change1h < 0){
                $('#oneHour').css('color', red);
            }else{
                $('#oneHour').css('color', green);
            }
            if(percent_change24h < 0){
                $('#oneDay').css('color', red);
            }else{
                $('#oneDay').css('color', green);
            }
            if(percent_change7d < 0){
                $('#sevenDay').css('color', red);
            }else{
                $('#sevenDay').css('color', green);
            }
            percent_change1h = percent_change1h + ' %';
            percent_change24h = percent_change24h + ' %';
            percent_change7d = percent_change7d + ' %';
            var init_curr = "'BTC'";
            $('#image').attr('src', imageSrc);
            var cur_price = '<a href="javascript:" onclick="convertPriceBtc('+init_curr+')" origPrice1="'+priceUsd+' USD">'+priceUsd+' USD</a>';
            var market_price = '<a href="javascript:" onclick="convertPriceBtc('+init_curr+')" origPrice1="'+market_capUsd+' USD">'+market_capUsd+' USD</a>';
            var one_day_vol_price = '<a href="javascript:" onclick="convertPriceBtc('+init_curr+')" origPrice1="'+one_day_volumeUsd+' USD">'+one_day_volumeUsd+' USD</a>';

            $('#name_head').text(name);
            $('#shortCode').text(short_code);
            $('#curPrice').html(cur_price);
            $('#oneHour').text(percent_change1h);
            $('#oneDay').text(percent_change24h);
            $('#sevenDay').text(percent_change7d);
            $('#market').html(market_price);
            $('#volume24').html(one_day_vol_price);
            $('#available').text(availableSupply);
            if(appStatus == "TRUE"){
                adjFont();
            }
        }
    });
}
