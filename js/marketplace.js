/*
 *-----------------------------------------------------------------------------------------------------------
 *    Keyword Market ajax javascript
 *-----------------------------------------------------------------------------------------------------------
 *   This javascript file is used to get function of Keyword Market which are required to load on page load
 */

$(document).ready(function(){


    $('#SuggestedKeywordsLoad').load("suggested_keywords.php"); // load suggested html page
    $('#loaderFirstPurchase').load("newowned_keywords.php"); // load NewlyOwned Keywords html page
    $('#bidkeyword').load("bid_keywords.php"); // load latestBid Keywords html page
    $('#askkeyword').load("ask_keywords.php"); // load latestAsk Keywords html page
    $('#latesttradekeyword').load("latest_trade_keywords.php"); // load latestTrade Keywords html page
    $('#interactionAnalytic').load("interactionAnalytics.php"); // load interactionAnalytics html page
    $('#earningAnalytic').load("earningAnalytics.php"); // load earningAnalytics html page
});

function SuggestedKeywords(elem){
    //alert("Inserted");
    $(elem).find("i").attr("class","fa fa-refresh fa-spin fa-1x fa-fw");
    $('.suggested').load("suggested_keywords.php?",function(){
        $(elem).find("i").attr("class","fa fa-refresh fa-1x fa-fw");
    });
}

function SearchedKeywords(keyword) {

    var filter = document.getElementsByName('ownerFilter');

    for (var i = 0, length = filter.length; i < length; i++) {
        if (filter[i].checked) {
            // do whatever you want with the checked radio
            (filter[i].value);

            // only one radio can be logically checked, don't check the rest
            break;
        }
    }

    var filter=(filter[i].id);

    if(filter == "all")
    {
        var url = "allKeyword.php"
    }
    if(filter == "unowned")
    {
        var url = "unOwnedKeyword.php"
    }
    if(filter == "owned")
    {
        var url = "ownedKeyword.php"
    }

    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'html',
        data: {
            keyword : keyword
        },
        async: true,
        success: function (data) {
            $('.search-box').html(data);
        }
    });

}




// This function is use for refreshing the latest trade and getting updated data.

function latestTradeKeywords(elem){

    $(elem).find("i").attr("class","fa fa-refresh fa-spin fa-1x fa-fw");
    $('#latesttradekeyword').load("latest_trade_keywords.php",function(){
        $(elem).find("i").attr("class","fa fa-refresh");
    });

}

// This function is use for refreshing the Ask and getting updated data.
function askKeywords(){

    $('#askkeyword').load("ask_keywords.php");
}

// This function is use for refreshing the Bid and getting updated data.
function bidsKeywords(){

    $('#bidkeyword').load("bid_keywords.php");
}

// This function is use for refreshing the Newly Owned and getting updated data.
function newlyOwnedKeywords(){

    $('#loaderFirstPurchase').load("newowned_keywords.php");
}

function interactionanalytic(){

    $('#interactionAnalytic').load("interactionAnalytics.php");
}

function earninganalytic(){

    $('#earningAnalytic').load("earningAnalytics.php");
}

// This function is used for blank search box at market place, if user removed the search keyword from searchbox then suggested keyword should display.

$(".marketplace").keyup(function(e){
    if(e.which == 8){
        if($.trim($(this).val()) == ""){

            /* Change url after input box is empty */
            var url = window.location.href;
            url = url.split('?')[0];
            history.pushState(null, null, url);

             $.ajax({
                 type: 'GET',
                 url: 'suggested_keywords.php',
                 dataType: 'html',
                 async: true,
                  success: function (data) {
                     $('.suggested').html(data);
                     $('#suggestedlisting').hide();
                     $('#loader').attr('onclick',"SuggestedKeywords(this)");
                 }
             });
        }
    }

});

// this function is used for searching keywords.

function searchResult() {
    // alert("Testing");
    var keyword_search = $.trim($("#keyword_search").val());
    if (keyword_search != "") {
        var keywordsArray = [], cnt = 0;
        var searchSearchInput = keyword_search.split(" ");
        for(var i=0; i<searchSearchInput.length; i++){
            if(searchSearchInput[i] != ""){
                keywordsArray[cnt] = searchSearchInput[i];
                cnt++;
            }
        }
        // console.log(keywordsArray);
        if(keywordsArray.length > 10){
            $(".errMsg").show();
            $(".errMsg").html("Maximum keywords exceeded");
        }else{
            var alpha = /(?!^ +$)^.+$/;
            if (!keyword_search.match(alpha)) {
                $(".errMsg").show();
                $(".errMsg").html('Please enter a valid query');
            }else{
                $('#keyword_search_form').submit();
            }
        }
    }
    else
    {
        $(".errMsg").show();
        $(".errMsg").html('Please enter any keyword');
        return false;
    }
}

// This function is for Performance Search form submit.

$("#keyword_search").keypress(function(e){
    if(e.which == 13) {
        // alert("Testing");
        e.preventDefault();
        searchResult();
        return false;
    }
})

// This function is for Performance Search form submit.

$("#performance_search_form").keypress(function(e){
    if(e.which == 13) {
        // alert("Testing");
        e.preventDefault();
        searchResultPerformance();
        return false;
    }
})


// searchResultPerformance used for onclick search result on performance search keyword page

function searchResultPerformance() {


    var performance_search = $.trim($("#performance_search").val()); //alert(keyword_search);

    $.ajax({
        type: 'GET',
        url: rootUrl+'views/keywords/marketplace/performance_keyword.php',
        dataType: 'html',
        data: {
            q: performance_search
        },
        async: true,
        success: function (data) {
            $('.performance').html(data);
        }
    });

}


function ownedKeyword(keyword){

    $.ajax({
        type: 'POST',
        url: 'ownedKeyword.php',
        dataType: 'html',
        data: {
            keyword : keyword
        },
        async: true,
        success: function (data) {
            $('.search-box').html(data);
            //$('.keywordavailable').hide();
        }
    });
}

function unownedKeyword(keyword){

    $.ajax({
        type: 'POST',
        url: 'unOwnedKeyword.php',
        dataType: 'html',
        data: {
            keyword : keyword
        },
        async: true,
        success: function (data) {
            $('.search-box').html(data);
            //$('.keywordnotavailable').hide();
        }
    });
}

function allKeyword(keyword){

    $.ajax({
        type: 'POST',
        url: 'allKeyword.php',
        dataType: 'html',
        data: {
            keyword : keyword
        },
        async: true,
        success: function (data) {
            $('.search-box').html(data);
        }
    });
}

var viewAllFilter = 'latest';
// This function is used for pagination numner functionality.

function paginationFunction(pageName, pageID, filterType){
    $.ajax({
        type: 'GET',
        url: 'pagination.php',
        dataType: 'html',
        data: {
            pagename : pageName,
            page : pageID,
            filterType : filterType
        },
        async: true,
        success: function (data) {
            $(".pagination-numbers").each(function(){
                $(this).removeClass("active");
            });
            var id = pageID;
            $("#"+id).addClass("active");
            $('.short').html(data);
        }
    });
}

$("#prev").click(function(e){
    e.preventDefault();
    var prevID = 0;
    var pagename = $(".short").attr("id");
    $(".pagination-numbers").each(function(){


        if($(this).hasClass("active")) {
            prevID = parseInt($(this).attr("id"));
        }
    });
    var prevPageNo = 2;
    if(prevID == prevPageNo){
        //   $("li a[name = 'prev']").disable(true);
        $("#prev").attr("disabled", true);
    }else{
        $("#prev").removeAttr('disabled');
    }
    $("#next").removeAttr('disabled');
    prevID--;
    $('#gotovalue').val(prevID);
    paginationFunction(pagename, prevID,viewAllFilter);
});

// This functionality is used for pagination part for Forword cursor.

$("#next").click(function(e){
    e.preventDefault();
    var nextID = 0;
    var pagename = $(".short").attr("id");
    var lastPage = 0;
    $(".pagination-numbers").each(function(){
        if($(this).hasClass("active")){
            nextID = parseInt($(this).attr("id"));
            //alert(nextID);
            // hideshow1(nextID,"next",last_page);
        }
        lastPage = parseInt($(this).attr("id"));
    });
    //console.log(lastPage+"   "+(nextID+1));
    var nextPageNo = (nextID+1);
    if(lastPage == nextPageNo){
        $("#next").attr("disabled", true);
    }else{
        $("#next").removeAttr('disabled');
    }
    $("#prev").removeAttr('disabled');
    nextID++;
    $('#gotovalue').val(nextID);
    paginationFunction(pagename, nextID, viewAllFilter);
});

$("li a[name = 'paginationid']").click(function(e){
    e.preventDefault();
    var id = $(this).attr("id"); //alert(id);
    var pagename = $(".short").attr("id"); //alert(pagename);
    if(id == 1)
    {
        $("#prev").attr("disabled", true);
    }
    else
    {
        $("#prev").removeAttr("disabled");
    }

    var lastPage = 0;
    $(".pagination-numbers").each(function(){
        lastPage = parseInt($(this).attr("id"));
    });
    // console.log(lastPage+"   "+id);
    if(lastPage == id){
        $("#next").attr("disabled", true);
    }else{
        $("#next").removeAttr('disabled');
    }

    $('#gotovalue').val(id);
    paginationFunction(pagename, id, viewAllFilter);


})

// This functionality is used for pagination for GOTO part.


$("#gotovalue").keyup(function () {

    var pagename = $(".short").attr("id");
    var lastPage = 0;
    $(".pagination-numbers").each(function(){
        lastPage = parseInt($(this).attr("id")); //alert(lastPage);

    });
    //alert(lastPage);
    var textinput = $('#gotovalue').val();
    //  alert(textinput);
    if(lastPage == textinput){
        $("#next").attr("disabled", true);
    }else{
        $("#next").removeAttr('disabled');
    }

    if(textinput == 1){
        $("#prev").attr("disabled", true);
    }else{
        $("#prev").removeAttr('disabled');
    }

    if (textinput == '') {
        // alert("Value Not Match");
        var textinput = 1;

    }
    else if(textinput > lastPage || textinput == 0)
    {
        //alert("This Page is NotAvailable");
        var textinput = 1;
        showToast("success","This page is NotAvailable");
    }

    paginationFunction(pagename, textinput, viewAllFilter);


});

function addRemoveFromCart(email, keyword, index,  clientSessionId, rootURL, source){
   // alert(rootURL);

    var action = $(document.getElementById(source+"_cartButton_"+keyword)).val();
    var hdrPerformanceQry = $('#performance_search').val();
    var searchQuery = $('#keyword_search').val();
    var pathname = window.location.pathname;  // get url
    var filename= pathname.split('/').pop();  // get filename

    if(action == "Add To Cart"){
        action = "add";
    }else{
        action = "remove";
    }

    $.ajax({

        type: 'POST',
        dataType : 'json',
        url : rootUrl+'controllers/keywords/addRemoveCartController.php',
        data : { keyword : encodeURIComponent(keyword), index : encodeURIComponent(index), action : encodeURIComponent(action), clientSessionId: encodeURIComponent(clientSessionId)},

        success : function(data){
            if(data.errCode == -1){

                /* For remove keyword from checkout page */
                if(source == 'cht' || source == 'crt'){

                    if(data.cart_total_count == '0'){
                        var redirectUrl = rootURL+"views/keywords/marketplace/keyword_search.php";
                        window.location = redirectUrl;
                    }else{
                        // var pathname = window.location.pathname;  // get url
                        // var filename= pathname.split('/').pop();  // get filename
                        if(filename == 'checkout.php')
                        {
                            //location.reload();
                            cartCheckoutDataLoad();
                            loadCheckoutPaymentBox();

                            // Enable pay now button
                            $(".btn-accept-licence").css("background-color","#376175");
                            $(".btn-accept-licence").css("box-shadow","1px 1px 1px 1px #7393a2");
                            $(".btn-accept-licence").prop('disabled', false);

                            acceptTermFlag = false;
                        }else{
                            if ($('.cart-menu').css("display") != "block") {
                                $('.cart-menu').show('fast');
                            }

                        }
                        $('#cartTotalCount').html(data.cart_total_count);
                        //showUserCart(rootURL, email);
                    }
                }else{
                    if ($('.cart-menu').css("display") != "block") {
                        $('.cart-menu').show('fast');
                    }
                    showUserCart(rootURL, email);
                    if(filename == 'keyword_search.php' && searchQuery.match(keyword))
                    {

                        document.getElementById("km_cartButton_"+keyword).value = data.cart_button_text;
                    }
                    if(hdrPerformanceQry != ''){
                        document.getElementById("hdr_cartButton_"+keyword).value = data.cart_button_text;
                    }

                    if(filename == 'keyword_search.php' || filename == 'viewKeywoTimeline.php' || filename == 'keyword_analytics.php' || filename == 'view_interaction_analytics.php' || filename == 'interactionBlog.php' || filename == 'interactionVideo.php' || filename == 'interactionImage.php' || filename == 'interactionAudio.php' || filename == 'view_earning_analytics.php' || filename == 'earningBlog.php' || filename == 'earningVideo.php' || filename == 'earningImage.php' || filename == 'earningAudio.php' || filename == 'viewallFollowedKeyword.php')
                    {
                        document.getElementById("km_cartButton_"+keyword).value = data.cart_button_text;
                    }

                    var cartCount = parseInt($('#cart-count').val());
                    if(cartCount == 0){
                        cartCount = cartCount + 1;
                        $('.cartCount').append('<span class="badge badge-blue" id="cartTotalCount">'+ cartCount +'</span>');
                        $('#cart-count').val(cartCount);
                    }else{
                        $('#cartTotalCount').text(data.cart_total_count);
                        if(data.cart_total_count == '0'){
                            //$('#cartTotalCount').hide();
                        }
                    }
                }

                // on success
                showToast("success",data.errMsg);
                showUserCart(rootURL, email);
            }else if(data.errMsg == "Keyword already in cart"){
                // if keyword already in cart

                // $('#km_cartButton_'+index).val('Remove');
                document.getElementById("km_cartButton_"+keyword).value = "Remove";
                if(hdrPerformanceQry != ''){
                    document.getElementById("hdr_cartButton_"+keyword).value = "Remove";
                }

                $('#cartTotalCount').text(data.cart_total_count);
                showUserCart(rootURL, email);
                showToast("failed",data.errMsg);

            }else if(data.errMsg == "Keyword already removed from cart"){
                // $('#km_cartButton_'+index).val('Add To Cart');
                document.getElementById("km_cartButton_"+keyword).value = "Add To Cart";
                if(hdrPerformanceQry != ''){
                    document.getElementById("hdr_cartButton_"+keyword).value = "Add To Cart";
                }
                $('#cartTotalCount').text(data.cart_total_count);
                showUserCart(rootURL, email);
                showToast("failed",data.errMsg);
            }else{
                // on failure
                showToast("failed",data.errMsg);
            }
        },
    });
}

function cartOpen(){
    //cartOpenFlag = 1;

    //$cartMenu.stop().slideToggle({direction: 'left'}, 10000);
    //$(".main-menu, .app-menu  , .search-menu, .notificationList").hide();
    $( ".add-to-cart-list" ).slideDown();

}

function buyNowClick(keyword, amount){

    var msg = "Are you Sure, You want to buy #"+keyword+" for price of "+amount+" "+keywoDefaultCurrency;

    $('#buyResponse').text('');
    $('#buyResponse').text(msg);
    $("#keyword-popup-confirm-buy").modal("show");

    $('#buy-yes-btn').click(function(){
      $('#buyNowKeyword').prop("disabled","true");
        $("#keyword-popup-confirm-buy").modal("hide");

        if(keyword != ''){
            $.ajax({
                type: 'POST',
                dataType : 'json',
                url : rootUrl+'controllers/keywords/buyNowController.php',
                data : { buyNowKeyword : encodeURIComponent(keyword)},

                success : function(data){
                    console.log(data);

                    if(data.errCode == -2){
                        window.location.href = rootUrl+"views/two_factor_auth/index.php";
                    } else if(data.errCode == -1){
                        showToast("success",data.errMsg);
                        $('#buyNowKeyword').prop("disabled","false");
                        //window.setTimeout(function(){location.reload()},2000)
                        window.setTimeout(function(){
                            window.location.href = data.redirect_url;
                        },2000)
                    }else if(data.errCode == 23){

                        $('#buyResponse').text('');
                        $('#buyResponse').text(data.errMsg);
                        $('#buy-yes-btn').hide();
                        $('#buy-no-btn').val('OK');
                        $("#keyword-popup-confirm-buy").modal("show");
                        $('#buy-no-btn').click(function(){
                            location.reload();
                        });
                    }else if(data.errCode == 24){

                        $('#buyResponse').text('');
                        $('#buyResponse').text(data.errMsg);
                        $('#buy-yes-btn').hide();
                        $('#buy-no-btn').val('OK');
                        $("#keyword-popup-confirm-buy").modal("show");
                        $('#buy-no-btn').click(function(){
                            location.reload();
                        });
                    }else{
                        showToast("failed",data.errMsg);
                        window.setTimeout(function(){location.reload()},2000)
                    }
                }
            });
        }else{
            showToast("failed","Keyword not found");
        }
    });

}

function acceptBid(){
    cancel();
    $('#acceptKeyword').prop("disabled","true");
    var keyword = $('#acceptKeyword').val();
    // alert(keyword);

    var path = window.location.href;
    //  alert(path);
    if(keyword != ''){
        $.ajax({
            type: 'POST',
            dataType : 'json',
            url : rootUrl+'controllers/keywords/acceptBidController.php',
            data : { acceptBidKeyword : keyword},

            success : function(data){
               // console.log(data);
                if(data.errCode == -2){
                    window.location.href = rootUrl+"views/two_factor_auth/index.php";
                }else if(data.errCode == 1){
                    showToast("failed",data.errMsg);
                }else if(data.errCode == -1){
                    showToast("success",data.errMsg);
                    window.location.href = data.redirect_url;
                }else if(data.errCode == 40){ // to handle if session is exit from one tab and trying to accept from another tab
                    showToast("failed",data.errMsg);
                    window.setTimeout(function(){location.reload()},2000)
                }else{
                    showToast("failed",data.errMsg);
                    window.location.href = data.redirect_url;
                }


                return;
               /* if(data.errCode == -1){
                    $('#acceptKeyword').prop("disabled","false");
                    showToast("success",data.errMsg);
                    // window.setTimeout(function(){location.reload()},2000)
                    window.setTimeout(function(){
                        window.location.href = data.redirect_url;
                    },2000)
                }else{
                    showToast("failed",data.errMsg);
                     window.setTimeout(function(){location.reload()},2000)
                }*/
            },
        });
    }else{
        showToast("failed","Keyword not found");
    }
}

function cancel(){
    $('.close').click();
}
