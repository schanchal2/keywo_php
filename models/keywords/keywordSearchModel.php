<?php
// Getting Values for suggested keywords
    function getSuggestedKeywords($conn){

        $returnArr = array();

        $query = "SELECT DISTINCT keyword FROM suggested_keywords WHERE suggested_keywords.keyword NOT IN (SELECT keyword FROM recent_sold_keyword) ORDER BY id DESC LIMIT 10";

        $result = runQuery($query, $conn);
    //    print_r($result);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=10;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

// Getting Values for searched keywords

    function optimizeSearchQuery($searchQuery) {
        //print_r(func_get_args()); die;
        //optimize search query
        $searchQuery = strtolower($searchQuery);
        $searchQueryArr = explode(" ", $searchQuery);
        $searchQueryArrUnique = array_unique($searchQueryArr);
        $searchQueryFinal = "";
        foreach ($searchQueryArrUnique as $key => $value) {
            if ($value != "") {
                $searchQueryFinal .= $value . " ";
            }
        }

        $searchQueryFinal           = strip_tags(trim($searchQueryFinal));
        $searchQueryFinal = preg_replace("/\s\s([\s]+)?/", " ", $searchQueryFinal);

        return $searchQueryFinal;
    }

// Checking whether keywords is available or not(sold or blocked by admin)

    function checkForKeywordAvailability($keyword,$conn){

        $keyword = cleanQueryParameter($conn, $keyword);

        $retArray = array();
        if(isset($keyword) && !empty($keyword)){
            $result = getKeywordOwnershipDetails($conn, $keyword); //print_r($result);
            $kwdDetails = $result["errMsg"];
            if(noError($result)){
                if(count($kwdDetails) > 0){
                    /* Check keyword claim status, if status 1 then keyword available to all otherwise not available */
                    if($kwdDetails['claim_status'] == 1){
                        $kwdOwnerEmail = $result["errMsg"]["buyer_id"]; //print_r("kwdOwnerEmail").($kwdOwnerEmail);
                        $result= getBlockedKeyword($keyword,$conn);
                        if(noError($result)){
                            $returnArr["errCode"]=-1;
                            $returnArr["keyword_details"]=$kwdDetails;
                            $id = $result["errMsg"]["id"];
                            if(isset($id) && !empty($id)){
                                $returnArr["errMsg"]="keyword_blocked";
                            }else{
                                if(isset($kwdOwnerEmail) && !empty($kwdOwnerEmail)){
                                    $returnArr["errMsg"]="keyword_not_available";
                                }else{
                                    $returnArr["errMsg"]="keyword_available";
                                }
                            }
                        }else{
                            $returnArr["errCode"]=3;
                            $returnArr["errMsg"]="Error getting keyword's blocked status";
                        }
                    }else{
                        $returnArr["errCode"]=4;
                        $returnArr["errMsg"]="Keyword not available";
                    }

                }else{
                    $returnArr["errCode"]=-1;
                    $returnArr["keyword_details"]=$kwdDetails;
                    $returnArr["errMsg"]="keyword_available";
                }

            }else{
                $returnArr["errCode"]=5;
                $returnArr["errMsg"]="Error getting ownership details";
            }
        }else{
            $returnArr["errCode"]=6;
            $returnArr["errMsg"]="Keyword not found";
        }

        return $returnArr;
    }

    function checkKeywordCartStatus($words,$conn){

        $returnArr = array();

        //get keyword ownership table name
        $kwdTableName = getKeywordOwnershipTableName($words);

        $query = "select * from " . $kwdTableName . " where keyword='" . $words . "' ";

        $result = runQuery($query, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res = $row;

            $returnArr["errCode"] =-1;

            $returnArr["errMsg"] = $res;

        } else {

            $returnArr["errCode"] = 5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;
    }

    function getBlockedKeyword($words,$conn){ /*  By Dinesh for Bloking Keywords*/

        //cleaning of parameters for SQL Injection
        $words = cleanQueryParameter($conn, $words); //echo $words;

        $returnArr = array();

        $query = "select * from blocked_keywords where keyword='" . $words . "' ";

        $result = runQuery($query, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;
        //echo "<pre>"; print_r($returnArr); echo "</pre>";die;
    }

    function recentlySoldKeyword($conn){

        $returnArr = array();
        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit; //echo $start_from;

        $query = "SELECT * FROM recent_sold_keyword ORDER BY purchase_timestamp DESC Limit $start_from, $limit;";
     //   print_r($query);die;

        $result = runQuery($query, $conn);
       // echo "<pre>"; print_r($query); echo "</pre>";

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"][5]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function getMostSearchedKeywords($keyword,$conn){

        $returnArr = array();

        $query = "select sum(count)+(social_count) from keyword_count where keyword = $keyword;";

        //print_r($query); die;
        $result = runQuery($query, $conn);

        if(noError($result)){
            $res = array();
            while ($row = mysqli_fetch_assoc($result["dbResource"]))
                $res[] = $row;

            $returnArr["errCode"]=-1;
            $returnArr["errMsg"]=$res;
        } else {
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]=$result["errMsg"];
        }

        return $returnArr;
    }

    function getLatestBid($conn){

        $returnArr = array();

        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit; //echo $start_from;

        $query = "SELECT * FROM latest_bid ORDER BY bid_time DESC Limit $start_from, $limit;";
        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function getLatestAsk($conn){

        $returnArr = array();

        $returnArr = array();

        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit; //echo $start_from;

        $query = "SELECT * FROM latest_ask ORDER BY ask_time DESC Limit $start_from, $limit;";

        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function getLatestTrade($conn){

        $returnArr = array();

        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit; //echo $start_from;

        $query = "SELECT * FROM latest_trades ORDER BY trade_time DESC Limit $start_from, $limit;";

        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function pagination($conn){

        $returnArr = array();

        $query = "SELECT COUNT(id) as totalrecord FROM latest_trades;";
        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function paginationask($conn){



        $returnArr = array();

        $query = "SELECT COUNT(id) as totalrecord FROM latest_ask;";
        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function paginationbid($conn){



        $returnArr = array();

        $query = "SELECT COUNT(id) as totalrecord FROM latest_bid;";
        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function paginationnewowned($conn){



        $returnArr = array();

        $query = "SELECT COUNT(id) as totalrecord FROM recent_sold_keyword;";
       // echo "<pre>"; print_r($query); echo "</pre>";
        //echo $query;
        //die();
        $result = runQuery($query, $conn);


        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

function getKeywordSale($conn){

    $returnArr = array();

    $query = "select sum(keyword_price) as keyword_sale from recent_sold_keyword;";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getKeywordOwner($conn){

        $returnArr = array();

        $query = "select count(*) as count from mykeyword_details";
        $result = runQuery($query, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"][5]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }


function getInteractionDetails($type,$conn){

        $returnArr = array();

        $query = "SELECT `keyword`,{$type} FROM top_1000_keyword_earnings_interaction ORDER BY {$type} DESC ";
        
        $result = runQuery($query, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function getAnalyticsDetails($offset,$limit,$tableName,$type,$conn){

        $returnArr = array();
        $sqlquery = "select keyword, sum({$type}) AS total_count from {$tableName} GROUP BY `keyword` order by total_count desc limit {$offset},{$limit}";
        $result = runQuery($sqlquery, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

     function getAnalyticsDetailsCount($tableName,$conn){

        $returnArr = array();
        $sqlquery = "select count(DISTINCT(keyword)) as count from {$tableName}";
        $result = runQuery($sqlquery, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

    function getSocialSubcatDetail($offset,$limit,$tableName,$type,$postType,$conn){

        $returnArr = array();

        $sqlquery = "select keyword, sum({$type}) AS total_interaction_count from {$tableName} where post_type = '{$postType}' GROUP BY `keyword` order by {$type} desc limit {$offset},{$limit}";
        $result = runQuery($sqlquery, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

     function getSocialSubcatCount($tableName,$postType,$conn){

        $returnArr = array();
        $sqlquery = "select count(DISTINCT(keyword)) as count from {$tableName} where post_type = '{$postType}'";
        $result = runQuery($sqlquery, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

function getRequestCount($conn, $tableName, $type, $orderby,$startDate,$endDate)
{
    $returnArr = array();
    global $blanks;
    if($startDate == '' && $endDate == '')
    {
        $query = "SELECT count(*) as count from $tableName ORDER BY $type $orderby ";
    }
    else{
        $query = "SELECT count(*) as count from $tableName where trade_time BETWEEN '$startDate' AND '$endDate 23:59:59' ORDER BY $type $orderby ";
    }

//    if($timestamp!="" || $srNo!="" || $srType!="" || $userName!="" || $raisedBy!="" || $priority!="" || $holdBy!="" || $type!="" || $requestStatus!="" || $agentIDS!="" || $escalateBy=="")
//    {
//        $query .= " where  1=1 ";
//    }



//     echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getRequestData($offset, $limit, $tableName, $type, $orderby, $startDate,$endDate,$conn)
{

    $returnArr = array();
    global $blanks;
    if($startDate == '' && $endDate == '')
    {
        $query = "SELECT * from $tableName";
    }
    else {
        $query = "SELECT * from $tableName where trade_time BETWEEN '$startDate' AND '$endDate 23:59:59' ";
    }

    $query .= " ORDER by $type $orderby";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

//     echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}
?>