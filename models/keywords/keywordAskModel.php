<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 16/2/17
 * Time: 12:45 PM
 */

/****************** Function related to Ask on keyword ******************/

function addToHighestAsk($conn,$keyword,$kwd_owner_email,$kwd_ask_price,$ask_transaction_id){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn, $keyword); 
    $kwd_owner_email = cleanQueryParameter($conn, $kwd_owner_email);
    $kwd_ask_price = cleanQueryParameter($conn, $kwd_ask_price);
    $ask_transaction_id = cleanQueryParameter($conn, $ask_transaction_id);


    $getLowestAskPriceId = getLowestAskPriceId($conn);
    $getLowestAskPriceId =$getLowestAskPriceId["errMsg"]["id"];
    $ask_price =$getLowestAskPriceId["errMsg"]["ask_price"];


    $query = "insert into highest_ask (kwd_owner_email,ask_price,keyword,ask_transaction_id) values('".$kwd_owner_email."','".$kwd_ask_price."','".$keyword."','".$ask_transaction_id."');";
    $result = runQuery($query, $conn);

    if(noError($result)){
        //$getHighestAskDesc = getHighestAskDesc($conn);

        $getHighestAskCount = getHighestAskCount($conn);
        $getHighestAskCount = $getHighestAskCount["errMsg"]["count"];
        $returnArr["errCode"] = -1;
        if($getHighestAskCount == 1000){

            $query_dlt= "DELETE FROM highest_ask where id=".$getLowestAskPriceId." limit 1;";
            $result_dlt= runQuery($query_dlt, $conn);
            if(noError($result_dlt)){
                $returnArr["errCode"] = -1;

            }else{

                $returnArr["errCode"] = 8;
                $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
            }

        }

    } else {

        $returnArr["errCode"] = 7;
        $returnArr["errMsg"]=" updation failed".mysqli_error($conn);

    }
    return $returnArr;
}

//latest ask
function addToLatestAsk($conn,$keyword,$kwd_owner_email,$kwd_ask_price,$ask_transaction_id){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $kwd_owner_email = cleanQueryParameter($conn,$kwd_owner_email);
    $kwd_ask_price = cleanQueryParameter($conn,$kwd_ask_price);
    $ask_transaction_id = cleanQueryParameter($conn,$ask_transaction_id);

    $returnArr = array();

    $query = "INSERT INTO latest_ask(kwd_owner_email,keyword,ask_price,ask_transaction_id,ask_time) VALUES('".$kwd_owner_email."','".$keyword."','".$kwd_ask_price."','".$ask_transaction_id."',NOW());";
    $result = runQuery($query,$conn);
    if(noError($result)){

        //get lastest ask sorted by date in descending order
        $result = getLatestAskSortedByDate($conn);
        if(noError($result)){
            $ask_records = $result["errMsg"];
            $ask_records_count = COUNT($ask_records);
            $ask_last_index = $ask_records_count -1;
            $ask_last_value = $ask_records[$ask_last_index];
            $ask_last_id = $ask_last_value["ask_transaction_id"];

            if($ask_records_count > 1000){
                $query = "DELETE FROM latest_ask WHERE ask_transaction_id='".$ask_last_id."';";
                $result = runQuery($query,$conn);
            }

            $returnArr["errCode"]=-1;
            $returnArr["errMsg"]= "Success adding ask to latest ask";
        }else{
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]=$result["errMsg"];
        }
    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}

function addToLatestAskAll($conn,$keyword,$kwd_owner_email,$kwd_ask_price,$ask_transaction_id){

    $returnArr = array();

    $query = "INSERT INTO latest_ask_all(kwd_owner_email,keyword,ask_price,ask_transaction_id,ask_time) VALUES('".$kwd_owner_email."','".$keyword."','".$kwd_ask_price."','".$ask_transaction_id."',NOW());";
    $result = runQuery($query,$conn);
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]= "Success adding ask to latest ask all";
    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}


function deleteFromHighestAsk($conn,$ask_transaction_id){

    //cleaning of parameters for SQL Injection
    $ask_transaction_id = cleanQueryParameter($conn, $ask_transaction_id);

    $query= "DELETE FROM highest_ask where ask_transaction_id='".$ask_transaction_id."'";
    $query= runQuery($query, $conn);
    if(noError($query)){
        $returnArr["errCode"] = -1;

    }else{

        $returnArr["errCode"] = 8;
        $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
    }
    return $returnArr;
}

function deleteFromLatestAsk($conn,$ask_transaction_id){

    //cleaning of parameters  for XSS
    $ask_transaction_id = cleanQueryParameter($conn, $ask_transaction_id);

    $returnArr = array();


    $query = "DELETE FROM latest_ask WHERE ask_transaction_id='".$ask_transaction_id."';";
    $result = runQuery($query, $conn);


    if(noError($result)){

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]="Delete ask from lastest ask";

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function deleteFromLatestAskAll($conn,$ask_transaction_id){

    $returnArr = array();
    $query = "DELETE FROM latest_ask_all WHERE ask_transaction_id='".$ask_transaction_id."';";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]="Delete ask from lastest ask all";
    } else {
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}


function getLowestAskPriceId($conn){

    $returnArr = array();
    //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
    $query = "SELECT id FROM highest_ask order by MIN(ask_price);";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getHighestAskCount($conn){

    $returnArr = array();
    //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
    $query = "select count(*) as count from highest_ask where 1=1;";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getLatestAskSortedByDate($conn){



    $returnArr = array();


    $query = "SELECT * FROM latest_ask ORDER BY ask_time DESC;";

    //echo $query;
    //die();
    $result = runQuery($query, $conn);


    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

/****************** Function related to Ask on keyword ******************/

?>