<?php

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getFollowUnfollow()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getFollowUnfollow()
*   Purpose       :   This function update $follow_unfollow(column) into keyword_ownership_<alphabet_first_letter> into keyword db.
*                 :   if user click on follow button in analytics page thn it will store session emailId in the column, and if
*                 :   session email id already exist then after clicking on unfollow it will delete the session emailID
*   Arguments     :   (string) $keyword, (string) $kwdTableName, (string) $email, (integer) $follow_unfollow,
*                 :   (object)$conn
*/

    function getFollowUnfollow($keyword,$kwdTableName,$emailjson,$follow_unfollow,$conn){

        $returnArr = array();

        if (empty($follow_unfollow)) {

           echo $query = "update $kwdTableName set follow_unfollow = '$emailjson' where keyword = '$keyword';";
        }
        else
        {
          //echo  $query = "update $kwdTableName set follow_unfollow = '' where keyword = '$keyword';";
        }

        $result = runQuery($query, $conn);
        //    print_r($result);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=10;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getDailyKeywordEarningAnalytics()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getDailyKeywordEarningAnalytics()
*   Purpose       :   This function gives total sum of social and search earning detail of whole day.(for earning setter is created by admin side)
*   Arguments     :   (string) $date, (string) $intractionType, (string) $keyword, (object)$conn
*/

    function getDailyKeywordEarningAnalytics($date,$intractionType,$keyword,$conn){

        $returnArr = array();
        $extraArg = array();

        $dateYear=explode("-",$date);
        $Year=$dateYear[0];
        $month=$dateYear[1];

        $query = "select sum(total_interaction_count) as total_interaction_count,sum(total_interaction_earning) as total_interaction_earning from daily_keyword_earnings_{$month}_{$Year} where keyword='{$keyword}' and tracking_date='{$date}'";


        if($intractionType=="social")
        {
            $query .=" and interaction_type='{$intractionType}' group by post_type";
        }
        if($intractionType=="search")
        {
            $query .=" and interaction_type='{$intractionType}'";
        }
        echo $query;

        $result = runQuery($query, $conn);

        if (noError($result)) {
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"])) {
                array_push($res, $row);
            }
            $errMsg = $res;
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {

            $returnArr = setErrorStack($returnArr, 12, null, $extraArg);

        }

        return $returnArr;
    }

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getDistinctKeywordUsed()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getDistinctKeywordUsed()
*   Purpose       :   This function gives distinct keywords from table daily_keyword_earnings_{currentDate}_{currentYear}
*   Arguments     :   (string) $tablename, (string) $date, (object)$conn
*/

    function getDistinctKeywordUsed($conn, $tablename, $date)
    {

        $returnArr = array();
        $extraArg = array();

       $query = "select distinct(keyword) from {$tablename} where tracking_date='{$date}'";

        $result = runQuery($query, $conn);

        if (noError($result)) {
            $res = array();
            while ($row = mysqli_fetch_assoc($result["dbResource"]))
                array_push($res, $row["keyword"]);

            $errMsg = $res;
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
        }
        return $returnArr;
    }


function getKeywordLifetimeEarningIntraction($conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select keyword_lifeTime_intractions,keyword_lifeTime_earnings from keywords_stat";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res=$row;
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getKeywordYesterdayEarningIntraction($conn)
{

    $returnArr = array();
    $extraArg = array();

    $userDate = date("Y-m-d", strtotime('-1 day'));

    $query = "select sum(total_interaction_count) as total_interaction_count,sum(total_interaction_earning) as total_interaction_earning from daily_keyword_earnings_".date('m')."_".date('Y')." where type_qualification='qualified' and tracking_date='{$userDate}'";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res=$row;
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}

?>