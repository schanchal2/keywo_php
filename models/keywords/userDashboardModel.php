<?php

function getMyKeywordDetails($conn,$buyer_id){
    //cleaning of parameters for SQL Injection
    //$buyer_id = cleanQueryParameter($conn,$buyer_id);
    $returnArr = array();

    $query = "select * from mykeyword_details where buyer_id='".$buyer_id."'";

    $result = runQuery($query,$conn);

    if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res = $row;

            $returnArr["errCode"] =-1;

            $returnArr["errMsg"] = $res;

        } else {

            $returnArr["errCode"] = 5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

    return $returnArr;
}

function getAllKeywordRequestCount($email,$conn){

        $returnArr = array();
        $tableName = "mykeyword_details";

        $getMyKeywordDetails = getMyKeywordDetails($conn,$email);
        if(noError($getMyKeywordDetails)) {

            $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
            $myKeywords = $getMyKeywordDetails["transaction_details"];
            $myKeywords = json_decode($myKeywords, true);
            $myKeywords = array_reverse($myKeywords);
            $kwdCount = count($myKeywords);
        }

        $sqlquery = "select count($kwdCount) as count from {$tableName}";
        $result = runQuery($sqlquery, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;

    }


function getAllKeywordRequestData($email,$offset, $limit, $conn)
{
    $returnArr = array();
    global $blanks;

     $query = "select * from mykeyword_details where buyer_id='".$email."'";

     $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    
    return $returnArr;
}

function getActiveBids($email,$conn)
{
    $returnArr = array();
    global $blanks;

    $query = "select sum(bids_active) as bidValue from presale_users where user_email = '{$email}';";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


?>

