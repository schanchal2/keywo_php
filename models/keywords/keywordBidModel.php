<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 16/2/17
 * Time: 12:36 PM
 */


/****************** Function related to set Bid on keyword ******************/

/*
Description: This function is used to add bid ID to user's bid details

Input parameters:
$kwdDbConn --> Connection resource to Keywords DB
$buyerId --> Email Id of bidder
$bidId --> Bid Transaction Id

Output parameters:
Array : Related response
*/
function addBidDetailsByBuyerId($kwdDbConn,$buyerId,$bidId){

    //cleaning of parameters for SQL Injection
    $buyerId = cleanQueryParameter($kwdDbConn, $buyerId);
    $bidId = cleanQueryParameter($kwdDbConn, $bidId);

    $returnArr = array();
    $buyerBidDetails = array();
    //get bid details of user
    $result = getBidDetailsByBuyerId($kwdDbConn,$buyerId);

    $buyerEmail = $result["errMsg"]["buyer_id"];
    $buyerBidDetailsJson = $result["errMsg"]["bid_details"];

    if(isset($buyerEmail) && !empty($buyerEmail)){
        //the buyerId exists
        $buyerBidDetails = json_decode($buyerBidDetailsJson,TRUE);

        $buyerBidDetails[] = $bidId;

        $buyerBidDetailsNewJson = json_encode($buyerBidDetails,JSON_UNESCAPED_UNICODE);

        $query = "UPDATE mybid_details SET bid_details='".$buyerBidDetailsNewJson."' WHERE buyer_id='".$buyerId."'";
        $result = runQuery($query,$kwdDbConn);

        if(noError($result)){
            $returnArr["errCode"]=-1;
            $returnArr["errMsg"]="Success updating bid details";
        }else{
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]="Error updating bid details";
        }
    }else{
        //the buyerId does not exists
        $buyerBidDetails[] = $bidId;
        $buyerBidDetailsNewJson = json_encode($buyerBidDetails,JSON_UNESCAPED_UNICODE);
        $query = "INSERT INTO mybid_details(buyer_id,bid_details)VALUES('".$buyerId."','".$buyerBidDetailsNewJson."')";
        $result = runQuery($query,$kwdDbConn);

        if(noError($result)){
            $returnArr["errCode"]=-1;
            $returnArr["errMsg"]="Success updating bid details";
        }else{
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]="Error updating bid details";
        }
    }

    return $returnArr;
}

function addActiveBidsCountOfUser($email,$conn){
    //cleaning of parameters for SQL Injection
    $email = cleanQueryParameter($conn,$email);

    $returnArr = array();

    //get user details
    $result = getUserCartDetails($email, $conn);

    $bids_active_count = $result["errMsg"]["bids_active"];
    if(!isset($bids_active_count) && empty($bids_active_count)){
        $bids_active_count = 0;
    }
    if(noError($result)){
        $bids_active_count = $bids_active_count + 1;
        $query = "UPDATE presale_users SET bids_active='".$bids_active_count."' WHERE user_email='".$email."'";
        $result = runQuery($query,$conn);

        if(noError($result)){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = "Successfully added to active bids count";
        }else{
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"] = $result["errMsg"];
        }
    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function addCancelledBidsCountOfUser($email,$conn){

    //cleaning of parameters for SQL Injection
    $email = cleanQueryParameter($conn,$email);

    $returnArr = array();

    //get user details
    $result = getUserCartDetails($email, $conn);
    $bids_cancelled = $result["errMsg"]["bids_cancelled"];
    if(!isset($bids_cancelled) && empty($bids_cancelled)){
        $bids_cancelled = 0;
    }
    if(noError($result)){
        $bids_cancelled = $bids_cancelled + 1;
        $query = "UPDATE presale_users SET bids_cancelled='".$bids_cancelled."' WHERE user_email='".$email."'";
        $result = runQuery($query,$conn);
        if(noError($result)){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = "Successfully added to active bids count";
        }else{
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"] = $result["errMsg"];
        }
    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function deductActiveBidsCountOfUser($email,$conn){
    //cleaning of parameters for SQL Injection
    $email = cleanQueryParameter($conn, $email);

    $returnArr = array();

    //get user details
    $result = getUserCartDetails($email, $conn);
    $bids_active_count = $result["errMsg"]["bids_active"];
    if(!isset($bids_active_count) && empty($bids_active_count)){
        $bids_active_count = 0;
    }
    if(noError($result)){
        $bids_active_count = $bids_active_count - 1;
        if($bids_active_count < 1){
            $bids_active_count = 0;
        }
       $query = "UPDATE presale_users SET bids_active='".$bids_active_count."' WHERE user_email='".$email."'";
        $result = runQuery($query,$conn);
        if(noError($result)){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = "Successfully added to active bids count";
        }else{
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"] = $result["errMsg"];
        }
    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function addToHighestBid($conn,$keyword,$bidder_email,$bid_price,$bid_transaction_id){

    $returnArr = array();
    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $bidder_email = cleanQueryParameter($conn,$bidder_email);
    $bid_price = cleanQueryParameter($conn,$bid_price);
    $bid_transaction_id = cleanQueryParameter($conn,$bid_transaction_id);

    $getLowestBidPriceId = getLowestBidPriceId($conn);

    $getLowestBidPriceId =$getLowestBidPriceId["errMsg"]["id"];

    $getHighestBidForKeywords = getHighestBidForKeywords($conn,$keyword);
    //printArr($getHighestBidForKeywords);//die();

    $getHighestBidPriceForKeywords = $getHighestBidForKeywords["errMsg"]["bid_price"];
    $getHighestBididForKeywords = $getHighestBidForKeywords["errMsg"]["id"];


    if($bid_price > $getHighestBidPriceForKeywords){

        $query = "insert into highest_bid (bidder_email,bid_price,keyword,bid_transaction_id) values('".$bidder_email."','".$bid_price."','".$keyword."','".$bid_transaction_id."');";
        $result = runQuery($query, $conn);

        if(noError($result)){
            //$getHighestAskDesc = getHighestAskDesc($conn);
            $query_dlt= "DELETE FROM highest_bid where id=".$getHighestBididForKeywords.";";
            $result_dlt= runQuery($query_dlt, $conn);
            $getHighestBidCount = getHighestBidCount($conn);

            $getHighestBidCount = $getHighestBidCount["errMsg"]["count"];
            $returnArr["errCode"] = -1;
            if($getHighestBidCount == 1000){

                $query_dlt= "DELETE FROM highest_bid where id=".$getLowestBidPriceId." limit 1;";
                $result_dlt= runQuery($query_dlt, $conn);

                if(noError($result_dlt)){
                    $returnArr["errCode"] = -1;

                }else{

                    $returnArr["errCode"] = 8;
                    $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
                }

            }

        } else {

            $returnArr["errCode"] = 7;
            $returnArr["errMsg"]=" updation failed".mysqli_error($conn);

        }

    }

    return $returnArr;
}

function addToMostBid($conn,$keyword,$no_of_active_bids,$active_bids){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $no_of_active_bids = cleanQueryParameter($conn,$no_of_active_bids);
    $active_bids = cleanQueryParameter($conn,$active_bids);

    $getLowestMostBidId = getLowestMostBidId($conn);

    $getLowestMostBidId =$getLowestMostBidId["errMsg"]["id"];
    $getLowestNoOfBids =$getLowestMostBidId["errMsg"]["no_of_active_bid"];
    //get most bid id where no_of_active_bid = 0
    $getMostBidId = getMostBidId($conn);

    $mostBidIdCountZero = $getMostBidId["errMsg"]["id"];
    //echo $mostBidIdCountZero;
    //die();
    //if($no_of_active_bids > $getLowestNoOfBids){
    $result = getKeywordFromMostBids($conn,$keyword);

    $kwdID = $result["errMsg"]["id"];
    if(isset($kwdID) && !empty($kwdID)){
        $query = "UPDATE most_bid SET no_of_active_bid='".$no_of_active_bids."',active_bid='".$active_bids."' WHERE keyword ='".$keyword."'";
    }else{
        $query = "insert into most_bid (keyword,no_of_active_bid,active_bid) values('".$keyword."','".$no_of_active_bids."','".$active_bids."');";
    }

    //echo $query;die();
    $result = runQuery($query, $conn);

    if(noError($result)){
        //$getHighestAskDesc = getHighestAskDesc($conn);

        $getMostBidCount = getMostBidCount($conn);
        $getMostBidCount = $getMostBidCount["errMsg"]["count"];

        $query = "DELETE FROM most_bid where id='".$mostBidIdCountZero."'";
        $query= runQuery($query, $conn);
        if(noError($query)){
            $returnArr["errCode"] = -1;
        }else{
            $returnArr["errCode"] = 8;
            $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
        }


        if($getMostBidCount == 1000){

            $query_dlt= "DELETE FROM most_bid where id=".$getLowestMostBidId." limit 1;";
            $result_dlt= runQuery($query_dlt, $conn);
            if(noError($result_dlt)){
                $returnArr["errCode"] = -1;

            }else{

                $returnArr["errCode"] = 8;
                $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
            }

        }

    } else {

        $returnArr["errCode"] = 7;
        $returnArr["errMsg"]=" updation failed".mysqli_error($conn);

    }

    return $returnArr;

}

//latest bids
function addToLatestBid($conn,$keyword,$bidder_email,$kwd_bid_price,$bid_transaction_id){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $bidder_email = cleanQueryParameter($conn,$bidder_email);
    $kwd_bid_price = cleanQueryParameter($conn,$kwd_bid_price);
    $bid_transaction_id = cleanQueryParameter($conn,$bid_transaction_id);

    $returnArr = array();

    $query = "INSERT INTO latest_bid(bidder_email,keyword,bid_price,bid_transaction_id,bid_time) VALUES('".$bidder_email."','".$keyword."','".$kwd_bid_price."','".$bid_transaction_id."',NOW());";
    $result = runQuery($query,$conn);

    if(noError($result)){

        //get lastest bid sorted by date in descending order
        $result = getLatestBidSortedByDate($conn);

        if(noError($result)){
            $bid_records = $result["errMsg"];
            $bid_records_count = COUNT($bid_records);
            $bid_last_index = $bid_records_count -1;
            $bid_last_value = $bid_records[$bid_last_index];
            $bid_last_id = $bid_last_value["bid_transaction_id"];

            if($bid_records_count > 1000){
                $query = "DELETE FROM latest_bid WHERE bid_transaction_id='".$bid_last_id."';";
                $result = runQuery($query,$conn);
            }

            $returnArr["errCode"]=-1;
            $returnArr["errMsg"]= "Success adding bid to latest bid";
        }else{
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]=$result["errMsg"];
        }
    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}

//latest bids
function addToLatestBidAll($conn,$keyword,$bidder_email,$kwd_bid_price,$bid_transaction_id){

    $returnArr = array();

    $query = "INSERT INTO latest_bid_all(bidder_email,keyword,bid_price,bid_transaction_id,bid_time) VALUES('".$bidder_email."','".$keyword."','".$kwd_bid_price."','".$bid_transaction_id."',NOW());";
    $result = runQuery($query,$conn);

    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]= "Success adding bid to latest bid all";
    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}

/**
Description: This function is used to get user's bid details

Input parameters:
$conn --> Connection resource to Keywords DB
$buyerId --> Email Id of bidder

Output parameters:
Array : Array containing bid details and related response
 */
function getBidDetailsByBuyerId($conn,$buyerId){

    //cleaning of parameters for SQL Injection
    $buyerId = cleanQueryParameter($conn,$buyerId);
    $returnArr = array();

    $query = "SELECT * FROM mybid_details WHERE buyer_id='".$buyerId."'";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;

}

function getLowestBidPriceId($conn){

    $returnArr = array();
    //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
    $query = "SELECT * FROM highest_bid order by MIN(bid_price);";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getHighestBidForKeywords($conn,$keyword){


    $returnArr = array();
    //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
    $query = "SELECT * FROM highest_bid WHERE keyword='".$keyword."' order by bid_price DESC LIMIT 1;";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getHighestBidCount($conn){

    $returnArr = array();
    //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
    $query = "select count(*) as count from highest_bid where 1=1;";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getLowestMostBidId($conn){

    $returnArr = array();
    $query = "SELECT * FROM most_bid order by MIN(no_of_active_bid);";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getMostBidId($conn){

    $returnArr = array();
    $query = "select * from most_bid where no_of_active_bid = 0;";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getKeywordFromMostBids($conn,$keyword){

    $returnArr = array();
    $query = "SELECT * FROM most_bid WHERE keyword='".$keyword."';";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getMostBidCount($conn){

    $returnArr = array();
    $query = "select count(*) as count from most_bid where 1=1;";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getLatestBidSortedByDate($conn){



    $returnArr = array();


    $query = "SELECT * FROM latest_bid ORDER BY bid_time DESC;";

    //echo $query;
    //die();
    $result = runQuery($query, $conn);


    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

/****************** Function related to set Bid on keyword end ******************/


/****************** Function related to edit or delete Bid on keyword ******************/


/*
Description: This function is used to remove bid ID from user's bid details

Input parameters:
$conn --> Connection resource to Keywords DB
$buyerId --> Email Id of bidder
$bidId --> Bid Transaction Id

Output parameters:
Array : Related response
*/
function removeBidDetailsByBuyerId($conn,$buyerId,$bidId,$keyword){


    //cleaning of parameters for SQL Injection
    $buyerId = cleanQueryParameter($conn, $buyerId);
    $bidId = cleanQueryParameter($conn, $bidId);
    $keyword = cleanQueryParameter($conn, $keyword);

    $bidArr = explode("~~",$bidId);
    if(!isset($bidArr[6]) && empty($bidArr[6])){
        //get the last character from bid ID
        $lastChar = substr($bidId, -1);
        if ($lastChar == "~~") {
            $bidId = $bidId . $keyword;
        } else {
            $bidId = $bidId . "~~" . $keyword;
        }
    }
    $returnArr = array();
    $result = getBidDetailsByBuyerId($conn,$buyerId);

    $buyerEmail = $result["errMsg"]["buyer_id"];
    $buyerBidDetailsJson = $result["errMsg"]["bid_details"];

    //remove bid id from bid details
    $buyerBidDetails = json_decode($buyerBidDetailsJson,TRUE);

    $buyerBidDetails = array_diff($buyerBidDetails, array($bidId));
    $buyerBidDetailsNewJson = json_encode($buyerBidDetails,JSON_UNESCAPED_UNICODE);


    $query = "UPDATE mybid_details SET bid_details='".$buyerBidDetailsNewJson."' WHERE buyer_id='".$buyerId."'";
    $result = runQuery($query,$conn);
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]="Success removing bid details";
    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]="Error removing bid details";
    }

    return $returnArr;
}

function deleteFromHighestBid($conn,$bid_transaction_id){

    //cleaning of parameters for SQL Injection
    $bid_transaction_id = cleanQueryParameter($conn,$bid_transaction_id);

    $query= "DELETE FROM highest_bid where bid_transaction_id='".$bid_transaction_id."';";
    $query= runQuery($query, $conn);
    if(noError($query)){
        $returnArr["errCode"] = -1;

    }else{

        $returnArr["errCode"] = 8;
        $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
    }
    return $returnArr;
}

function deleteFromLatestBid($conn,$bid_transaction_id){

    //cleaning of parameters for SQL Injection
    $bid_transaction_id = cleanQueryParameter($conn,$bid_transaction_id);

    $returnArr = array();


    $query = "DELETE FROM latest_bid WHERE bid_transaction_id='".$bid_transaction_id."';";
    $result = runQuery($query, $conn);


    if(noError($result)){

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]="Delete bid from lastest bid";

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function rectifyHighestBidTable($conn,$keyword){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $result = getKeywordOwnershipDetails($conn, $keyword);
    if(noError($result)){
        $highest_bid_id = $result["errMsg"]["highest_bid_id"];
        $highestBidArray = explode("~~",$highest_bid_id);
        $higest_bid_email = $highestBidArray[1];
        if(isset($highest_bid_id) && !empty($highest_bid_id )){
            $highest_bid_amount = $result["errMsg"]["highest_bid_amount"];
            $highest_bid_keyword = getHighestBidDetailsByKeyword($conn,$keyword);
            //echo "<br>";
            $highest_bid_price = $highest_bid_keyword["errMsg"]["bid_price"];
            $highest_bid_price_id = $highest_bid_keyword["errMsg"]["id"];
            $returnArr["errCode"]=-1;
            if($highest_bid_amount > $highest_bid_price){

                if(isset($highest_bid_price_id) && !empty($highest_bid_price_id )){
                    $query = "UPDATE highest_bid SET bidder_email='".$higest_bid_email."',bid_price='".$highest_bid_amount."',bid_transaction_id='".$highest_bid_id."' WHERE keyword='".$keyword."' ;";
                }else{
                    $query = "INSERT INTO highest_bid(bidder_email,bid_price,keyword,bid_transaction_id) VALUES('".$higest_bid_email."','".$highest_bid_amount."','".$keyword."','".$highest_bid_id."');";
                }

                //echo $query;

                $result = runQuery($query,$conn);
                if(noError($result)){
                    $returnArr["errCode"]=-1;
                }else{
                    $returnArr["errCode"]=5;
                }
                //printArr($result);

            }
        }else{
            $returnArr["errCode"]=5;
        }
    }else{
        $returnArr["errCode"]=5;
    }
    return $returnArr;
}

function deleteFromLatestBidAll($conn,$bid_transaction_id){

    $returnArr = array();
    $query = "DELETE FROM latest_bid_all WHERE bid_transaction_id='".$bid_transaction_id."';";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]="Delete bid from lastest bid all";
    } else {
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;

}

function deleteFromMostBids($conn,$keyword){

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn,$keyword);
    $result = getKeywordFromMostBids($conn,$keyword);
    $kwdID = $result["errMsg"]["id"];
    $no_of_active_bid = $result["errMsg"]["no_of_active_bid"];
    $new_no_of_active_bid = $no_of_active_bid - 1;
    if($new_no_of_active_bid < 1){
        //$new_no_of_active_bid = 0;
        $query = "DELETE FROM most_bid WHERE keyword ='".$keyword."'";
    }else{
        $query = "UPDATE most_bid SET no_of_active_bid='".$new_no_of_active_bid."' WHERE keyword ='".$keyword."'";
    }
    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]= "success";
    }else{
        $returnArr["errCode"] = 8;
        $returnArr["errMsg"]=" updation failed".mysqli_error($conn);
    }

    return $returnArr;
}

function getHighestBidDetailsByKeyword($conn,$keyword){

    $returnArr = array();

    $query = "SELECT * FROM highest_bid WHERE keyword='".$keyword."';";
    // $query = "SELECT (SUM(price) - (SELECT SUM(price) FROM user_transaction WHERE type='Affiliate Earnings') )as total_pool_income FROM user_transaction WHERE type='keyword_purchase';";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


/* Bid curl request */

/*function deductBlockedForBids($email, $bidAmtGross){
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if(isset($email) && !empty($email)){
        $headers = array();

        //create signature
        $apiText = "email=".$email."&amount=".$bidAmtGross."&publicKey=".$walletPublicKey;
        $postFields = "email=".urlencode($email)."&amount=".urlencode($bidAmtGross)."&publicKey=".urlencode($walletPublicKey);

        $apiName = 'deductBlockedForBids';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText,'requestUrl' => $requestUrl,'curlPostFields' => $postFields,'log' => $paramArr['xml'],'curlType' => $curl_type);

        if($mode == 'production'){
            $nonce_result = generateSignatureWithNonce($apiText);
            if(noError($nonce_result)){
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            }else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if(noError($respArray)){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        }else if($respArray["errCode"] != 73){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    }else{
        $retArray["errCode"] = 1;
        $retArray["errMsg"] = "Error: Email is missing";
    }

    return $retArray;
}*/

/*function addBlockedForBids($email, $bidAmtGross){
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if(isset($email) && !empty($email)){
        $headers = array();

        //create signature
        $apiText = "email=".$email."&amount=".$bidAmtGross."&publicKey=".$walletPublicKey;
        $postFields = "email=".urlencode($email)."&amount=".urlencode($bidAmtGross)."&publicKey=".urlencode($walletPublicKey);

        $apiName = 'addBlockedForBids';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText,'requestUrl' => $requestUrl,'curlPostFields' => $postFields,'log' => $paramArr['xml'],'curlType' => $curl_type);

        if($mode == 'production'){
            $nonce_result = generateSignatureWithNonce($apiText);
            if(noError($nonce_result)){
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            }else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if(noError($respArray)){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        }else if($respArray["errCode"] != 73){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    }else{
        $retArray["errCode"] = 1;
        $retArray["errMsg"] = "Error: Email is missing";
    }

    return $retArray;
}*/

/****************** Function related to edit or delete Bid on keyword end ******************/


/****************** Function related accept bid on keyword starts ******************/
function checkForKeywordOwnershipLock($keyword,$conn){

    $returnArr = array();

    //get keyword ownership table name
    $tableName = getKeywordOwnershipTableName($keyword);

    $result = getKeywordOwnershipDetails($conn,$keyword);

    if(noError(($result))){
        $lockedTime = $result["errMsg"]["locked_time"];

        //get current date in php
        $currentTime = date("Y-m-d H:i:s");

        //$currentTime = time();
        $currDateInStr = strtotime($currentTime);

        $lockedTimeInStr = strtotime($lockedTime);

        if($currDateInStr > $lockedTimeInStr){
            $query = "UPDATE ".$tableName." SET locked_time= DATE_ADD(NOW(), INTERVAL 1 SECOND) WHERE keyword='".$keyword."'";
            $result = runQuery($query,$conn);
            if(noError($result)){
                $errMsg = "Succesfully applied lock to keyword ownership row";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
            }else{
                $errMsg = "Error Applying lock to keyword ownership row";
                $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
            }
        }else{
            $errMsg = "The table is already locked";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = $result["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr; 
}


?>