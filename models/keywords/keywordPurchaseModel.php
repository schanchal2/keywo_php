<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 21/2/17
 * Time: 5:50 PM
 */


function getKeywordOwnershipDetailsInCart($conn, $keyword){

    $returnArr    = array();

    //get keyword ownership table name
    $kwdTableName = getKeywordOwnershipTableName($keyword);

    //cleaning of parameters for SQL Injection
    $kwdTableName = cleanQueryParameter($conn, $kwdTableName);
    $keyword = cleanQueryParameter($conn, $keyword);

    $query  = "SELECT * FROM " . $kwdTableName . " WHERE keyword = '" . html_entity_decode($keyword)  . "' AND status='inCart'";

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}

function addToRecentSold($conn,$keyword,$keyword_price,$buyer_id)
{
    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn, $keyword);
    $keyword_price = cleanQueryParameter($conn, $keyword_price);
    $buyer_id = cleanQueryParameter($conn, $buyer_id);

    $currentSlabRate = getCurrentActiveSlab($conn);
    if (noError($currentSlabRate)) {
        $currentSlabRate = $currentSlabRate["errMsg"];
        $activeSlabRate = $currentSlabRate["ITD_Price"];
    } else {
        print('Error: Unable to retrieve slab rate');
        exit;
    }

   $query = "INSERT INTO recent_sold_keyword(`keyword`,`keyword_price`,`currency`,`purchase_timestamp`,`buyer_id`,`usd_price`) value('".$keyword."','".$keyword_price."','ITD',NOW(),'".$buyer_id."',".$activeSlabRate.");";

    //$query = "INSERT INTO recent_sold_keyword(`keyword`,`keyword_price`,`purchase_timestamp`,`buyer_id`) value('".$keyword."','".$keyword_price."',NOW(),'".$buyer_id."');";
    $result = runQuery($query, $conn);

    if(noError($result)){
        $returnArr["errCode"]=-1;
        $query_slc="SELECT * FROM `recent_sold_keyword` WHERE 1 order by purchase_timestamp DESC";
        $result = runQuery($query_slc, $conn);
        if(noError($result)){
            $returnArr["errCode"]=-1;
            $query_cnt= "select count(*) as count from recent_sold_keyword where 1=1";
            $result = runQuery($query_cnt, $conn);
            if(noError($result)){
                $result = mysqli_fetch_assoc($result['dbResource']);
                $returnArr["errCode"]=-1;
                if($result['count'] == 1001){
                    $query_dlt= "DELETE FROM recent_sold_keyword where 1=1 limit 1";
                    $result = runQuery($query_dlt, $conn);
                    if(noError($result)){
                        $returnArr["errCode"]=-1;
                    }else{
                        $returnArr["errCode"] = 8;
                        $returnArr["errMsg"]=" deletion failed".mysqli_error($conn);
                    }
                }

            }else{
                $returnArr["errCode"]=-1;
            }
        }else{
            $returnArr["errCode"]=-1;
        }

    }else{
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]=" Insertion failed".mysqli_error($conn);
    }
    return $returnArr;

}


function deleteBitgoPaymentRecord($conn, $keyword, $payment_type = 'wallet'){
    $returnArr = array();

    $query = "delete from payments where keyword_cart='".cleanQueryParameter($conn, $keyword)."' and status='unpaid' ";

    /* Delete bitgo entry from payments when keyword buy by paypal */
    if($payment_type == 'paypal'){
        $query .= "and payment_type = 'bitgo'";
    }else if($payment_type == 'bitgo'){
        $query .= "and payment_type = 'paypal'";
    }else{
        $query .= "and payment_type = 'paypal' and payment_type = 'bitgo'";
    }

    $deleteSuggestedKwd = runQuery($query, $conn);
    if(noError($deleteSuggestedKwd)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Records deleted successfully";
    }else{
        $returnArr["errCode"] = 6;
        $returnArr["errMsg"] = "Error : Deleting records";
    }

    return $returnArr;
}

function updateMykeywordDetails($conn,$buyer_id,$purchase_timestamp,$keyword,$kwd_price,$voucher_allocated,$payment_mode,$fund_amount){

    //cleaning of parametrs for XSS
    $buyer_id = cleanQueryParameter($conn,$buyer_id);
    $purchase_timestamp = cleanQueryParameter($conn, $purchase_timestamp);
    $keyword = cleanQueryParameter($conn, $keyword);
    $kwd_price = cleanQueryParameter($conn, $kwd_price);
    $voucher_allocated = cleanQueryParameter($conn, $voucher_allocated);
    $payment_mode = cleanQueryParameter($conn, $payment_mode);
    $fund_amount = cleanQueryParameter($conn, $fund_amount);

    // calculate 1 year timestamp from the current timestamp
    $expiry_timestamp  = date("j F, Y",mktime(0, 0, 0, date("m",strtotime($purchase_timestamp )),   date("d",strtotime($purchase_timestamp )),   date("Y",strtotime($purchase_timestamp ))+1));


    $getmykeyword_details = getMykeywordDetails($conn,$buyer_id);
    $buyerId = $getmykeyword_details['errMsg']['buyer_id'];

    if(empty($buyerId)){

        $data[]=array('keyword'=>$keyword,'kwd_price'=>$kwd_price,'currency'=>'ITD','purchase_timestamp'=>$purchase_timestamp,'voucher_allocated'=>$voucher_allocated,'payment_mode'=>$payment_mode,'fund_amount'=>$fund_amount, 'expiry_timestamp' => $expiry_timestamp);
        //create json of that array
        $transaction=json_encode($data,JSON_UNESCAPED_UNICODE);
        //echo $transaction;
        //printArr($transaction);
        $query_update="insert into mykeyword_details(buyer_id,transaction_details) values('".$buyer_id."','".$transaction."');";
        $result_upd= runQuery($query_update, $conn);
        if(noError($result_upd)){
            $returnArr["errCode"]=-1;

        } else {
            $returnArr["errCode"]=6;
            $returnArr["errMsg"]="Could not update transaction data: ".mysqli_error($conn);
        }

    }
    else{
        $getmykeyword_detailsId = getMyKeywordDetails($conn,$buyer_id);
        $getDelatl = $getmykeyword_detailsId['errMsg']['transaction_details'];
        $xyz= json_decode($getDelatl,true);
        $xyz[]=Array('keyword'=>$keyword,'purchase_timestamp'=>$purchase_timestamp,'kwd_price'=>$kwd_price,'currency'=>'ITD','voucher_allocated'=>$voucher_allocated,'payment_mode'=>$payment_mode,'fund_amount'=>$fund_amount);

        $xyz= json_encode($xyz,JSON_UNESCAPED_UNICODE);

        $query_update="UPDATE mykeyword_details SET transaction_details='".$xyz."' where buyer_id='".$buyer_id."'";
        $result_upd= runQuery($query_update, $conn);

        if(noError($result_upd)){
            $returnArr["errCode"]=-1;

        } else {
            $returnArr["errCode"]=6;
            $returnArr["errMsg"]="Could not update mykeyword_details data: ".mysqli_error($conn);
        }

    }


    //check for accept bid
    return $returnArr;

}

function getMyKeywordDetails($conn, $buyer_id)
{

    $returnArr = array();

    $query = "select * from mykeyword_details where buyer_id='" . $buyer_id . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

function getMyKeywordDetailsForClaim($conn, $limit)
{

    $returnArr = array();

    //$query = "select * from mykeyword_details limit {$limit}";
    $query = "select * from mykeyword_details";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

function updateFirstBuyStatus($buyerId,$status){

    global $walletPublicKey, $mode,  $walletURL;
    $retArray = array();

    if (isset($buyerId) && !empty($buyerId)) {

        $headers = array();

        //create signature
        $apiText = "user_id={$buyerId}&first_buy_flag={$status}&publicKey={$walletPublicKey}";
        $postFields = "first_buy_flag=" . $status . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = "user/{$buyerId}/firstbuystatus";
        $requestUrl = "{$walletURL}";
        $curl_type = 'PUT';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

function deleteUserStatsFromRevenueTable($conn,$keyword){

    $returnArr = array();
    $kwdTableName = getKeywordRevenueTableName($keyword);
    $kwdTableName = $kwdTableName['table_name'];

    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn, $keyword);
    $kwdTableName = cleanQueryParameter($conn, $kwdTableName);

    $query = "UPDATE ".$kwdTableName." SET user_kwd_search_count='',user_kwd_ownership_earnings='' WHERE keyword='".$keyword."';";
    $result = runQuery($query, $conn);

    if(noError($result)){

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]="Updated user stats";

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getVoucherForSaleBySlabId($slabId,$noOfVouchers,$conn){

    $returnArr = array();
    //cleaning of parameters for SQL Injection
    $slabId = cleanQueryParameter($conn, $slabId);
    $noOfVouchers = cleanQueryParameter($conn, $noOfVouchers);


    $query = " SELECT * FROM presale_vouchers WHERE voucher_slab='".$slabId."' AND voucher_status = 'available' ORDER BY voucher_id LIMIT ".$noOfVouchers.";";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function generateNewBtcAddress($userId)
{
    global $walletPublicKey,$walletPrivateKey,$walletURL, $mode;
    $retArray = array();
    $header = array();

    /* create signature */
    $apiText = "user_id={$userId}&publicKey={$walletPublicKey}";
    $postFields = "publicKey=" . urlencode($walletPublicKey);
    $requestUrl    = strtolower($walletURL);
    $apiName = "pocket/{$userId}/purchase/address";
    $curl_type = 'GET';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }

    return $retArray;
}

function initialisePurchase($address, $purchaseUser, $purchaseUserId)
{
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if (!empty($address) && !empty($purchaseUser) && !empty($purchaseUserId)) {

        $headers = array();

        //create signature
        $apiText = "purchase_user={$purchaseUser}&deposit_address={$address}&user_id={$purchaseUserId}&publicKey={$walletPublicKey}";
        $postFields = "deposit_address=" . $address . "&purchase_user=" . $purchaseUser . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = "pocket/{$purchaseUserId}/purchase";
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

function getBitgoConfirmationFromWallet($address, $purchaseUserId)
{
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if (!empty($address) && !empty($purchaseUser)) {

        $headers = array();

        //create signature
        $apiText = "deposit_address={$address}&publicKey={$walletPublicKey}";
        $postFields = "deposit_address=" . $address . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = "pocket/{$purchaseUserId}/purchase/";
        $requestUrl = "{$walletURL}";
        $curl_type = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

function getBitgoTransactionDetails($txtHash,$btcAddress){

    $returnArr = array();

    if (!empty($txtHash)) {

        $api_request_url = "https://blockchain.info/rawtx/{$txtHash}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $api_request_url);
        $result = curl_exec($ch);
        $resultArr1 = json_decode($result,true);
		
		$resultArr = array();
        $response = $resultArr1['out'];

        foreach ($response as $key => $value) {
            if($value['addr'] == $btcAddress){
                $resultArr['out'][0]['value'] = $value['value'];
                $resultArr['out'][0]['addr'] = $value['addr'];
            }
        }
        //echo "withdrawal_address: ".$withdrawalAddress;
        if(count($resultArr) == 0){
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Invalid transaction hash";
        }else{
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $resultArr;
        }
        //echo json_encode($returnArr);

    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "All fields are mandatory";
    }

    return $returnArr;
}

function getBitgoTransactionDetailsTest($txtHash,$btcAddress){

    $returnArr = array();

    if (!empty($txtHash)) {

        $api_request_url = "http://tbtc.blockr.io/api/v1/tx/raw/{$txtHash}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $api_request_url);
        $result = curl_exec($ch);
        $resultArr1 = json_decode($result,true);
        $resultArr = array();
        $response = $resultArr1['data']['tx'];

        foreach ($response['vout'] as $key => $value) {
            if($value['scriptPubKey']['addresses'][0] == $btcAddress){
                $resultArr['out'][0]['value'] = $value['value']*100000000;
                $resultArr['out'][0]['addr'] = $value['scriptPubKey']['addresses'][0];
            }
        }

        //echo "withdrawal_address: ".$withdrawalAddress;
        if(count($resultArr) == 0){
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Invalid transaction hash";
        }else{
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $resultArr;
        }

    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "All fields are mandatory";
    }

    return $returnArr;
}

function getPaymentInfoByBtcAddress($recieverAddress,$kwdConn)
{
    $data = array();
    $query = "SELECT * from payments WHERE payment_address='" . $recieverAddress . "'"; // `no_of_confirmation`,`amount_received`,`status`
    $result = runQuery($query, $kwdConn);

    if (noError($result)) {
        while($row = mysqli_fetch_assoc($result['dbResource'])){
            $data[] = $row;
        }

        $returnArr["errCode"] = -1;
        $returnArr['errMsg'] = $data[0];
    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Error : {$result['errMsg']}";
    }

    return $returnArr;
}

function updatePaymentDetails($kwdConn,$order_id,$transactionId,$status,$recieverAddress,$userIp,$email,$amount_due,$bitgoRecievedAmountBTC,$bitgoRecievedAmountITD,$totalAvailableBal,$noOfConfirmation,$jsonResponse,$keywords){

    if($status === 'cancelled'){
        $returnArr["errCode"] = 3;
        $returnArr["errMsg"]  = "Error The transaction has cancelled";
        $returnArr["status"] = $status;
    }else {
        $paidAmount = 0;
		if($keywords != 'itd_purchase_only'){
			if($totalAvailableBal > -1 && $totalAvailableBal > $amount_due){
				$refundAmount = (float)($totalAvailableBal - $amount_due);
				$paidAmount = number_format(($totalAvailableBal - $refundAmount),8);
				$status = 'overpaid';
			} else if($totalAvailableBal > -1 && $totalAvailableBal < $amount_due) {
				$paidAmount = $bitgoRecievedAmountITD;
				$paidAmount = number_format((float)$paidAmount,8);
				$status = 'mispaid';
			} else if($totalAvailableBal > -1 && $totalAvailableBal == $amount_due){
				$paidAmount = $bitgoRecievedAmountITD;
				$status = 'exact';
			}
		}else{
			if($bitgoRecievedAmountBTC > -1 && $bitgoRecievedAmountBTC > $amount_due){
				$refundAmount = (float)($bitgoRecievedAmountBTC - $amount_due);
				$paidAmount = number_format(($bitgoRecievedAmountBTC - $refundAmount),8);
				$status = 'overpaid';
			} else if($bitgoRecievedAmountBTC > -1 && $bitgoRecievedAmountBTC < $amount_due) {
				$paidAmount = $bitgoRecievedAmountBTC;
				$paidAmount = number_format((float)$paidAmount,8);
				$status = 'mispaid';
			} else if($bitgoRecievedAmountBTC > -1 && $bitgoRecievedAmountBTC == $amount_due){
				$paidAmount = $bitgoRecievedAmountBTC;
				$status = 'exact';
			}
		}
        

        if($noOfConfirmation == 0){
            $query = "UPDATE payments SET amount_recieved = " . $bitgoRecievedAmountBTC . ",paid_amount= " . $paidAmount . ",no_of_confirmation = ".$noOfConfirmation.",status = '".$status."',total_payment_data='" . $jsonResponse . "',payment_time = now(), payment_transaction_id = '".$transactionId."' WHERE order_id='" . $order_id . "' AND username='".$email."' ";
        } else {
            $query = "UPDATE payments SET amount_recieved = " . $bitgoRecievedAmountBTC . ",purchase_itd = ".$bitgoRecievedAmountITD.", paid_amount =" . $paidAmount . ",no_of_confirmation = ".$noOfConfirmation.",status = '".$status."',total_payment_data='" . $jsonResponse . "',payment_time = now(),request_ip_address='" . $userIp . "' WHERE order_id='" . $order_id . "' AND username='".$email."' AND payment_address = '".$recieverAddress."' "; // /*AND payment_transaction_id='".$transactionId."'*/
        }

        $result = runQuery($query, $kwdConn);

        if (noError($result)) {

            //$trans = commitTransaction($conn);
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"]  = "payment address updated successfully";
            $returnArr["status"] = $status;
        } else {

            //$trans = rollbackTransaction($conn);
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"]     = "Error update payment address";
            $returnArr["status"] = $status;

        }
    }

    return $returnArr;
}

function creditUserItdPurchase($email,$userId,$recieverAddr,$recievedAmt,$userIp,$exchangeRates,$meta_data,$keywords)
{
    global $communityPoolUserId,$communityPoolUser;
    $returnArr = array();

    if(!empty($email) && !empty($userId) && !empty($recieverAddr) && !empty($recievedAmt))
    {
       /*  Credit itd purchase amount to user balance */
        $creditEarning   = creditUserEarning($userId, $recievedAmt, 'purchase_itd', 'add');
        if(noError($creditEarning))
        {
            /* Make transaction for itd purchase from ITD */
            $recipientEmail = $email;
            $recipientUserId = $userId;
            $senderEmail = $communityPoolUser;
            $senderUserId = $communityPoolUserId;
            $type = 'purchase_itd';
            $paymentMode = 'ITD';
            $meta_data = json_decode($meta_data, true);

            if($keywords != 'itd_purchase_only'){
                $metaDetails = array("buyer_id" => $email, "raise_on" => "keyword purchase by bitgo");        
            }else{
                $metaDetails = array("buyer_id" => $email, "raise_on" => "ITD purchase by bitgo",'country' => $meta_data['country'],'gender' => $meta_data['gender'],'device' => $meta_data['device'] );
            }    
            
            $transaction  = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $recievedAmt, $type, $paymentMode, $userIp, $exchangeRates, $metaDetails);

            if (noError($transaction)) {
                //success inserting itd purchase transaction
                $errMsg = "Insert user transaction for ITD purchase : success.";
                $returnArr['errCode'] = -1;
                $returnArr['errMsg'] = $errMsg;
            } else {
                //error inserting itd purchase transaction
                $errMsg = "Insert user transaction for ITD purchase : failed.";
                $returnArr['errCode'] = 2;
                $returnArr['errMsg'] = $errMsg;
            }

        }else{
            $errMsg = "Credit purchase itd amount to user : failed.";
            $returnArr['errCode'] = 2;
            $returnArr['errMsg'] = $errMsg;
        }
    }else{
        $errMsg = "All fields are mandatory";
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = $errMsg;
    }

    return $returnArr;
}

function updateExpiryTimestamp($keyword, $kwdDbConn){
    $returnArr = array();

    //get keyword ownership table name
    $kwdTableName = getKeywordOwnershipTableName($keyword);
    $keyword = cleanQueryParameter($kwdDbConn, $keyword);

    $query = "update ".$kwdTableName." set ownership_expiry_time = DATE_ADD(ownership_expiry_time, INTERVAL 1 YEAR) where keyword='".$keyword."'";

    $execQuery = runQuery($query, $kwdDbConn);
    if(noError($execQuery)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Expiry timestamp update successfully";
    }else{
        $returnArr["errCode"] = 78;
        $returnArr["errMsg"] ="Error: Updating keyword expiry timestamp";
    }

    return $returnArr;
}

function getPaymentDetailsByOrderId($orderId,$email,$conn){

    $returnArr = array();

    $query = "SELECT * FROM payments WHERE order_id='".$orderId."' AND username='".$email."';";

    $result = runQuery($query, $conn);


    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}

function deleteSuggestedKeyword($kwdDbConn, $keyword){

    $returnArr = array();

    $query = "delete from suggested_keywords where keyword='".cleanQueryParameter($kwdDbConn, $keyword)."'";
    $deleteSuggestedKwd = runQuery($query, $kwdDbConn);
    if(noError($deleteSuggestedKwd)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Suggested keyword deleted successfully";
    }else{
        $returnArr["errCode"] = 6;
        $returnArr["errMsg"] = "Error : Deleting suggested keyword";
    }

    return $returnArr;
}

?>