<?php

function addRejectedBidsCountOfUser($email, $conn){
	$returnArr = array();

	//get user details
	$result = getUserCartDetails($email,$conn);
	$bids_rejected = $result["errMsg"]["bids_rejected"];
	if(!isset($bids_rejected) && empty($bids_rejected)){
		$bids_rejected = 0;
	}
	if(noError($result)){
		$bids_rejected = $bids_rejected + 1;
		$query = "UPDATE presale_users SET bids_rejected='".$bids_rejected."' WHERE user_email='".$email."'";
		$result = runQuery($query,$conn);
		if(noError($result)){
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = "Successfully added to active bids count";
		}else{
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"] = $result["errMsg"];
		}
	}else{
		$returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

/*function removeBidDetailsByBuyerId($conn,$buyerId,$bidId,$keyword){

	//cleaning of parameters for SQL Injection
	$buyerId = cleanQueryParameter($conn, $buyerId);
	$bidId = cleanQueryParameter($conn, $bidId);
	$keyword = cleanQueryParameter($conn, $keyword);

	$bidArr = explode("~~",$bidId);
	if(!isset($bidArr[6]) && empty($bidArr[6])){
		//get the last character from bid ID
		$lastChar = substr($bidId, -1);
		if ($lastChar == "~~") {
			$bidId = $bidId . $keyword;
		} else {
			$bidId = $bidId . "~~" . $keyword;
		}
	}
	$returnArr = array();
	$result = getBidDetailsByBuyerId($conn,$buyerId);
	$buyerEmail = $result["errMsg"]["buyer_id"];
	$buyerBidDetailsJson = $result["errMsg"]["bid_details"];

	//remove bid id from bid details
	$buyerBidDetails = json_decode($buyerBidDetailsJson,TRUE);
	$buyerBidDetails = array_diff($buyerBidDetails, array($bidId));
	$buyerBidDetailsNewJson = json_encode($buyerBidDetails,JSON_UNESCAPED_UNICODE);

	$query = "UPDATE mybid_details SET bid_details='".$buyerBidDetailsNewJson."' WHERE buyer_id='".$buyerId."'";
	$result = runQuery($query,$conn);
	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Success removing bid details";
	}else{
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = "Error removing bid details";
	}

	return $returnArr;
}
*/
function updateKwdDetailsAfterTrade($conn,$buyerId,$sellerId,$purchase_timestamp,$keyword,$kwd_price,$payment_mode="trade"){

	//update buyer details
	$result = updatemykeyword_details($conn,$buyerId,$purchase_timestamp,$keyword,$kwd_price,'',$payment_mode,'');
	//printArr($result);
	if(noError($result)){

		$newTransactionDetails = array();
		$getmykeyword_detailsId = getmykeyword_detailsId($conn,$sellerId);
		$transaction_details = $getmykeyword_detailsId['errMsg']['transaction_details'];
		$transaction_details = json_decode($transaction_details,TRUE);
		foreach($transaction_details as $value){

			if($value["keyword"] != $keyword){
				$newTransactionDetails[] = $value;
			}

		}

		//updating the new transaction details
		$newTransactionDetails = json_encode($newTransactionDetails,JSON_UNESCAPED_UNICODE);
		$query_update="UPDATE mykeyword_details SET transaction_details='".$newTransactionDetails."' where buyer_id='".$sellerId."'";
		$result= runQuery($query_update, $conn);
		if(noError($result)){
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $result["errMsg"];

		}else{
			$returnArr["errCode"] = 6;
			$returnArr["errMsg"] = $result["errMsg"];
		}


	}else{
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

function updatemykeyword_details($conn,$buyer_id,$purchase_timestamp,$keyword,$kwd_price,$voucher_allocated,$payment_mode,$fund_amount){

	//cleaning of parametrs for XSS
	$buyer_id = cleanQueryParameter($conn, $buyer_id);
	$purchase_timestamp = cleanQueryParameter($conn, $purchase_timestamp);
	$keyword = cleanQueryParameter($conn, $keyword);
	$kwd_price = cleanQueryParameter($conn, $kwd_price);
	$voucher_allocated = cleanQueryParameter($conn, $voucher_allocated);
	$payment_mode = cleanQueryParameter($conn, $payment_mode);
	$fund_amount = cleanQueryParameter($conn, $fund_amount);


		$getmykeyword_detailsId = getmykeyword_detailsId($conn,$buyer_id);
		$buyerId = $getmykeyword_detailsId['errMsg']['buyer_id'];
		if(empty($buyerId)){

			$data[]=array('keyword'=>$keyword,'kwd_price'=>$kwd_price,'purchase_timestamp'=>$purchase_timestamp,'voucher_allocated'=>$voucher_allocated,'payment_mode'=>$payment_mode,'fund_amount'=>$fund_amount);
			//create json of that array
			$transaction=json_encode($data,JSON_UNESCAPED_UNICODE);
			//echo $transaction;
			//printArr($transaction);
			$query_update="insert into mykeyword_details(buyer_id,transaction_details) values('".$buyer_id."','".$transaction."');";
			$result_upd= runQuery($query_update, $conn);
			if(noError($result_upd)){
			$returnArr["errCode"] = -1;

			} else {
				$returnArr["errCode"] = 6;
				$returnArr["errMsg"] = "Could not update transaction data: ".mysqli_error($conn);
			}


		}
		else{
			$getmykeyword_detailsId = getmykeyword_detailsId($conn,$buyer_id);
			$getDelatl = $getmykeyword_detailsId['errMsg']['transaction_details'];
			$xyz= json_decode($getDelatl,true);
			$xyz[]=Array('keyword'=>$keyword,'purchase_timestamp'=>$purchase_timestamp,'kwd_price'=>$kwd_price,'voucher_allocated'=>$voucher_allocated,'payment_mode'=>$payment_mode,'fund_amount'=>$fund_amount);

			$xyz= json_encode($xyz,JSON_UNESCAPED_UNICODE);

			$query_update="UPDATE mykeyword_details SET transaction_details='".$xyz."' where buyer_id='".$buyer_id."'";
			$result_upd= runQuery($query_update, $conn);

			if(noError($result_upd)){
			$returnArr["errCode"] = -1;

			} else {
				$returnArr["errCode"] = 6;
				$returnArr["errMsg"] = "Could not update mykeyword_details data: ".mysqli_error($conn);
			}

			}

			//check for accept bid
			return $returnArr;
}

function getmykeyword_detailsId($conn,$buyer_id){

 $returnArr = array();

 $query = "select * from mykeyword_details where buyer_id='".$buyer_id."'";

 $result = runQuery($query, $conn);

 if(noError($result)){
  $res = array();

  while ($row = mysqli_fetch_assoc($result["dbResource"]))

  $res = $row;

  $returnArr["errCode"] = -1;

  $returnArr["errMsg"] = $res;

 } else {

  $returnArr["errCode"] = 5;

  $returnArr["errMsg"] = $result["errMsg"];

 }

 return $returnArr;

}

function clearKeywordOwnershipLock($keyword,$conn){

	$returnArr = array();
	$tableName = getKeywordOwnershipTableName($keyword);

	$query = "UPDATE ".$tableName." SET locked_time= NULL WHERE keyword='".$keyword."'";
	$result = runQuery($query,$conn);
	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Succesfully cleared lock to keyword ownership row";
	}else{
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = "Error clearing lock to keyword ownership row";
	}

	return $returnArr;
}

/*function deductActiveBidsCountOfUser($email,$conn){



	//cleaning of parameters for SQL Injection
	$email = cleanQueryParameter($conn, $email);

	$returnArr = array();

	//get user details
	$result = getBuyerDetails($email,$conn);
	$bids_active_count = $result["errMsg"]["bids_active"];
	if(!isset($bids_active_count) && empty($bids_active_count)){
		$bids_active_count = 0;
	}
	if(noError($result)){
		$bids_active_count = $bids_active_count - 1;
		if($bids_active_count < 1){
			$bids_active_count = 0;
		}
		$query = "UPDATE presale_users SET bids_active='".$bids_active_count."' WHERE user_email='".$email."'";
		$result = runQuery($query,$conn);
		if(noError($result)){
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = "Successfully added to active bids count";
		}else{
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"] = $result["errMsg"];
		}
	}else{
		$returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}
*/
function addAcceptedBidsCountOfUser($email,$conn){
	$returnArr = array();

	//get user details
	$result = getBuyerDetails($email,$conn);

	$bids_accepted = $result["errMsg"]["bids_accepted"];
	if(!isset($bids_accepted) && empty($bids_accepted)){
		$bids_accepted = 0;
	}
	if(noError($result)){
		$bids_accepted = $bids_accepted + 1;
		$query = "UPDATE presale_users SET bids_accepted='".$bids_accepted."' WHERE user_email='".$email."'";
		$result = runQuery($query,$conn);
		if(noError($result)){
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = "Successfully added to active bids count";
		}else{
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"] = $result["errMsg"];
		}
	}else{
		$returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

function addToLatestTrade($conn,$keyword,$trader_email,$trade_price){

		$LatestTradeId = getLowestLatestTradeId($conn);
		$LatestTradeId =$LatestTradeId["errMsg"]["id"];


		 $query = "insert into latest_trades (keyword,trader_email,trade_price) values('".$keyword."','".$trader_email."','".$trade_price."');";
		//echo $query;
		//die();
		$result = runQuery($query, $conn);

		 if(noError($result)){
			   $HighestTradeCount = getHighestTradeCount($conn);
			  $HighestTradeCount = $HighestTradeCount["errMsg"]["count"];
			  $returnArr["errCode"] = -1;
			 if($HighestTradeCount == 1000){

				$query_dlt= "DELETE FROM latest_trades where id=".$LatestTradeId." limit 1;";
				  $result_dlt= runQuery($query_dlt, $conn);
				 if(noError($result_dlt)){
					 $returnArr["errCode"] = -1;

				 }else{

						$returnArr["errCode"] = 8;
						$returnArr["errMsg"] = " updation failed".mysqli_error($conn);
				 }

			 }

			 } else {

				$returnArr["errCode"] = 7;
				$returnArr["errMsg"]=" updation failed".mysqli_error($conn);

			}
		return $returnArr;
 }

 function getLowestLatestTradeId($conn){

 $returnArr = array();
 //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
 $query = "SELECT * FROM latest_trades order by trade_time desc;";
 //echo $query;
 //die();
 $result = runQuery($query, $conn);

 if(noError($result)){
  $res = array();

  while ($row = mysqli_fetch_assoc($result["dbResource"]))

  $res = $row;

  $returnArr["errCode"]=-1;

  $returnArr["errMsg"]=$res;

 } else {

  $returnArr["errCode"]=5;

  $returnArr["errMsg"]=$result["errMsg"];

 }

 return $returnArr;

}

function getHighestTradeCount($conn){

 $returnArr = array();
 //$query = "SELECT * FROM `highest_ask` WHERE 1=1 order by ask_price DESC;";
 $query = "select count(*) as count from latest_trades where 1=1;";

 $result = runQuery($query, $conn);

 if(noError($result)){
  $res = array();

  while ($row = mysqli_fetch_assoc($result["dbResource"]))

  $res = $row;

  $returnArr["errCode"]=-1;

  $returnArr["errMsg"]=$res;

 } else {

  $returnArr["errCode"]=5;

  $returnArr["errMsg"]=$result["errMsg"];

 }

 return $returnArr;

}

function getBuyerDetails($buyerId,$conn){

 $returnArr = array();

 //cleaning parameters for XSS Attacks
 $buyerId = cleanQueryParameter($conn, $buyerId);

 $query = "SELECT * FROM presale_users WHERE user_email='".$buyerId."' ";

 $result = runQuery($query, $conn);

 if(noError($result)){
  $res = array();

  while ($row = mysqli_fetch_assoc($result["dbResource"]))

  $res = $row;

  $returnArr["errCode"]=-1;

  $returnArr["errMsg"]=$res;

 } else {

  $returnArr["errCode"]=5;

  $returnArr["errMsg"]=$result["errMsg"];

 }

 return $returnArr;

}


  function deleteFromHighestAskByKeyword($conn,$keyword){
	$query= "DELETE FROM highest_ask where keyword='".$keyword."'";
	$query= runQuery($query, $conn);
	if(noError($query)){
		$returnArr["errCode"] = -1;
	}else{
		$returnArr["errCode"] = 8;
		$returnArr["errMsg"]=" updation failed".mysqli_error($conn);
	}

	 return $returnArr;
 }

function deleteFromMostBidByKeyword($conn,$keyword){

 	$query= "DELETE FROM most_bid where keyword='".$keyword."'";
	$query= runQuery($query, $conn);
	if(noError($query)){
	$returnArr["errCode"] = -1;

	}else{

	$returnArr["errCode"] = 8;
	$returnArr["errMsg"] = " updation failed".mysqli_error($conn);
	}

 	return $returnArr;
}

function deleteFromHighestBidByKeyword($conn,$keyword){

 	$query= "DELETE FROM highest_bid where keyword='".$keyword."'";
	$query= runQuery($query, $conn);
	if(noError($query)){
		$returnArr["errCode"] = -1;
	}else{
		$returnArr["errCode"] = 8;
		$returnArr["errMsg"]=" updation failed".mysqli_error($conn);
	}

	return $returnArr;
 }

 function deleteFromLatestAskByKeyword($conn,$keyword){
	$returnArr = array();

	$query = "DELETE FROM latest_ask WHERE keyword='".$keyword."';";
	$result = runQuery($query, $conn);
	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Delete bid from lastest bid";
	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}


function insertTradeProfit($keyword,$previousOwnerEmail,$purchasePrice,$traderEmail,$tradePrice,$userKwdOwnershipEarnings,$conn){
	$retArray = array();

	$tradeProfit = ($tradePrice + $userKwdOwnershipEarnings) - $purchasePrice;
	$query = "INSERT into trade_profit(keyword,previous_owner_email,purchase_price,trader_email,trade_price,trade_profit,kwd_ownership_earnings) VALUES('".$keyword."','".$previousOwnerEmail."','".$purchasePrice."','".$traderEmail."','".$tradePrice."','".$tradeProfit."','".$userKwdOwnershipEarnings."');";

	$result = runQuery($query,$conn);
	if(noError($result)){
		$retArray["errCode"] = -1;
        $retArray["errMsg"]  = "Success inserting total trade profit so far";
	}else{
		$retArray["errCode"] = 2;
        $retArray["errMsg"]  = "Error inserting total trade profit so far";
	}

	return $retArray;
}

function deleteFromLatestBidByKeywordAll($conn,$keyword){

	$returnArr = array();

	$query = "DELETE FROM latest_bid_all WHERE keyword='".$keyword."';";
	$result = runQuery($query, $conn);
	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Delete bid from lastest bid all";
	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

function deleteFromLatestBidByKeyword($conn,$keyword){

	$returnArr = array();

	$query = "DELETE FROM latest_bid WHERE keyword='".$keyword."';";
	$result = runQuery($query, $conn);
	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Delete bid from lastest bid";
	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

function notifyoptions($u_id,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code){

	$ch = curl_init();
	//$retArray = array();
	//global $walletURLIP;
	global $NotificationURL;
	global $walletPublicKey;
	global $walletPrivateKey;

	//192.168.2.221:4101/api/notify/v2/settings/users/:id/notifyoptions
	$walletURL = $NotificationURL."v2/settings/users/".$u_id."/notifyoptions";

	if ($method_name == 'DELETE'){
		curl_setopt($ch, CURLOPT_URL, $walletURL);
		$apiText = "id=".$u_id."&publicKey=".$walletPublicKey;
		$signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".urlencode($u_id)."&publicKey=".urlencode($walletPublicKey)."&signature=".$signature);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	}

	if ($method_name == 'GET'){
		$apiText = "id=".$u_id."&publicKey=".$walletPublicKey;
		$signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
		$api_request_url = $walletURL. '?publicKey=' .$walletPublicKey.'&signature='.$signature;
		curl_setopt($ch, CURLOPT_URL, $api_request_url);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	}

	if ($method_name == 'PUT'){
		curl_setopt($ch, CURLOPT_URL, $walletURL."/".$optionid);
		$apiText = "id=".$u_id."&optionid=".$optionid."&buy_container=".$buy_container."&ask_container=".$ask_container."&bid_container=".$bid_container."&buy_perm_code=".$buy_perm_code."&ask_perm_code=".$ask_perm_code."&bid_perm_code=".$bid_perm_code."&kwd_license_container=".$kwd_license_container."&deposit_container=".$deposit_container."&withdrawal_container=".$withdrawal_container."&kwd_license_perm_code=".$kwd_license_perm_code."&deposit_perm_code=".$deposit_perm_code."&withdrawal_perm_code=".$withdrawal_perm_code."&publicKey=".$walletPublicKey;
		$signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".urlencode($u_id)."&optionid=".$optionid."&buy_container=".urlencode($buy_container)."&ask_container=".urlencode($ask_container)."&bid_container=".urlencode($bid_container)."&buy_perm_code=".urlencode($buy_perm_code)."&ask_perm_code=".urlencode($ask_perm_code)."&bid_perm_code=".urlencode($bid_perm_code)."&kwd_license_container=".urlencode($kwd_license_container)."&deposit_container=".urlencode($deposit_container)."&withdrawal_container=".urlencode($withdrawal_container)."&kwd_license_perm_code=".urlencode($kwd_license_perm_code)."&deposit_perm_code=".urlencode($deposit_perm_code)."&withdrawal_perm_code=".urlencode($withdrawal_perm_code)."&publicKey=".urlencode($walletPublicKey)."&signature=".$signature);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	}
	if ($method_name == 'POST'){
		curl_setopt($ch, CURLOPT_URL, $walletURL);
		$apiText = "id=".$u_id."&buy_container=".$buy_container."&ask_container=".$ask_container."&bid_container=".$bid_container."&buy_perm_code=".$buy_perm_code."&ask_perm_code=".$ask_perm_code."&bid_perm_code=".$bid_perm_code."&kwd_license_container=".$kwd_license_container."&deposit_container=".$deposit_container."&withdrawal_container=".$withdrawal_container."&kwd_license_perm_code=".$kwd_license_perm_code."&deposit_perm_code=".$deposit_perm_code."&withdrawal_perm_code=".$withdrawal_perm_code."&publicKey=".$walletPublicKey;
		$signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "id=".urlencode($u_id)."&buy_container=".urlencode($buy_container)."&ask_container=".urlencode($ask_container)."&bid_container=".urlencode($bid_container)."&buy_perm_code=".urlencode($buy_perm_code)."&ask_perm_code=".urlencode($ask_perm_code)."&bid_perm_code=".urlencode($bid_perm_code)."&kwd_license_container=".urlencode($kwd_license_container)."&deposit_container=".urlencode($deposit_container)."&withdrawal_container=".urlencode($withdrawal_container)."&kwd_license_perm_code=".urlencode($kwd_license_perm_code)."&deposit_perm_code=".urlencode($deposit_perm_code)."&withdrawal_perm_code=".urlencode($withdrawal_perm_code)."&publicKey=".urlencode($walletPublicKey)."&signature=".$signature);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
	}

	$result = curl_exec($ch);

	$resultJson = json_decode($result,true);

	//get curl response
	$errCode = $resultJson["errCode"];
	$errMsg = $resultJson["errMsg"];

	if($errCode == -1){

		$retArray["errCode"] = -1;
		$retArray["errMsg"] = $errMsg;

	}else{
		$retArray["errCode"] = 2;
		$retArray["errMsg"] = $errMsg;
	}


	return $retArray;
}

function sendNotificationBuyPrefrence($to,$mail_subject,$email_body,$first_name,$last_name,$id,$smsText,$mobileNumber,$notification_body,$preference_code,$category,$linkStatus){

	$ch = curl_init();
    $retArray = array();
	global $mailHeader;
	global $mailbasetext;
    global $walletPublicKey;
    global $walletPrivateKey;
    global $newServerRootURL;
    global $walletURLIPnotification;
    $NotificationURL              = $walletURLIPnotification."api/notify/v2/inbox/user";
    // if($linkStatus == 1){
    //     //$message  = '<div style="border:solid thin black; padding:5px"><div style="width:100%; text-align:left; min-height:50px; background-color:#25a2dc; padding: 1%; padding-bottom: 0px;"><img style="max-width:200px;" src="https://searchtrade.com/images/searchtrade_white.png"></div><p>Hello,<br/><br/>Your bid of 0.00800000 BTC for the keyword #INDIA on SearchTrade.com was not the highest bid and so it has been rejected.</p><p>Regards from SearchTrade team.<br><br>Product of SearchTrade.com Pte Ltd, Singapore.<br><br>';
		//
    //     // include file
    //     include_once("dompdf/dompdf_config.inc.php");
		//
		// $html='<html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>';
    //     //save file on server
    //     $dompdf = new DOMPDF();
    //    	$dompdf->load_html($html.$mailHeader.$email_body.$mailbasetext, 'UTF-8');
    //     $dompdf->render();
    //     $output = $dompdf->output();
		//
    //     // generate random code
    //     $length = 20;
    //     $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    //     $charactersLength = strlen($characters);
    //     for ($i = 0; $i < $length; $i++) {
    //         $randomString .= $characters[rand(0, $charactersLength - 1)];
    //     }
    $URL = "";
    //     //$URL    ="../../keywords/keyword_certificate/".$to.$randomString.".pdf";
    //     $URL    =$newServerRootURL."keywords/keyword_certificate/".$to.$randomString.".pdf";
    //     file_put_contents($URL, $output);
		//
    // }

	// Insert script tab in wallet database as &lt; and &gt;
	//$email_body = rawurlencode($email_body);
	//$notification_body = urlencode($notification_body);

	//$email_body = str_replace('<', '&lt;',$email_body);

    curl_setopt($ch, CURLOPT_URL, $NotificationURL);
    $apiText = "mail_to=".$to."&mail_subject=".$mail_subject."&user_id=".$id."&preference_code=".$preference_code."&publicKey=".$walletPublicKey;
    $signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
    //echo $signature;
    curl_setopt($ch, CURLOPT_POSTFIELDS, "mail_to=".$to."&mail_subject=".$mail_subject."&email_body=".rawurlencode($email_body)."&first_name=".rawurlencode($first_name)."&last_name=".rawurlencode($last_name)."&user_id=".$id."&smsText=".$smsText."&mobileNumber=".$mobileNumber."&notification_body=".rawurlencode($notification_body)."&preference_code=".$preference_code."&category=".$category."&link=".$URL."&publicKey=".$walletPublicKey."&signature=".$signature);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);
    //printArr($result);die();

    $resultJson = json_decode($result,true);

    //get curl response
    $errCode = $resultJson["errCode"];
    $errMsg = $resultJson["errMsg"];

    if($errCode == -1){
        $retArray["errCode"] = -1;
        $retArray["errMsg"] = $errMsg;

    }else{
        //error message
        $retArray["errCode"] = $errCode;
        $retArray["errMsg"] = $errMsg;
    }

    return $retArray;
}


function sendKwdOwnershipZipToAdmin($keyword, $conn,$type) {

    global $adminEmail;
    global $docRoot;
    global $mailbasetext;
    global $mailHeader;
    global $ownershipZipMail;
    global $userRequiredFields;
    global $walletURLIP;

    $returnArr = array();
    //Array of file
    $fileArray = array( "keywords_ownership_a","keywords_ownership_b","keywords_ownership_c","keywords_ownership_d","keywords_ownership_e","keywords_ownership_f","keywords_ownership_g","keywords_ownership_h","keywords_ownership_i","keywords_ownership_j","keywords_ownership_k","keywords_ownership_l","keywords_ownership_m","keywords_ownership_n","keywords_ownership_o","keywords_ownership_other","keywords_ownership_p","keywords_ownership_q","keywords_ownership_r","keywords_ownership_s","keywords_ownership_t","keywords_ownership_u","keywords_ownership_v","keywords_ownership_w","keywords_ownership_x","keywords_ownership_y","keywords_ownership_z","keywords_ownership_num");

    //take Db dump
    backup_tables($keyword,$type,$conn);
    //take ownership excel backup
    writeDbToExcel($fileArray,$conn);
    //convert to zip
    convertDbToBackupZip($fileArray);


	$to         = $ownershipZipMail;
	$subject = "Keywo: Keyword Licence ".$type." Ownership Zip";
	$message =  '<p>Hi, <br/> <br/> There has been '.$type.' of #' . $keyword . ' on keywo.com.<br><br>You have been sent the ownership zip for the same.<br><br></p>';
	$path = $docRoot."db_backup/keywords/temporary_zip/";
	// if(!is_dir($path)){
	// 	mkdir($path, 0777, true);
	// }
	$path       = $path."keywords_ownership.zip";
	$keyword    = $keyword;
	
	// send keyword ownership zip mail to admin
	Send_Mail($to, $subject, $message, $path, $keyword,$attachment = "true");
	$ownerDetails = $userRequiredFields .",_id";

	$getOwnerDetails = getUserInfo($to, $walletURLIP . 'api/v3/', $ownerDetails);

	$getOwnerDetails = $getOwnerDetails["errMsg"];
	$id 		= $getOwnerDetails["_id"];
	$first_name = $getOwnerDetails["first_name"];
	$last_name = $getOwnerDetails["last_name"];

	$linkStatus = "";
	$category = "keyword ownership zip for admin";
	$notiSend = sendNotificationBuyPrefrence($to,$subject,$message,$first_name,$last_name,$id,$smsText,$mobileNumber,$message,$permissionCode,$category,$linkStatus);


	if(noError($notiSend)){

		$URL   = $docRoot . 'db_backup/keywords/excel_backups/';
		$URL   = $URL . '*';
		$files = glob($URL); // get all file names
		foreach ($files as $file) { // iterate files
			if (is_file($file)){
				unlink($file); // delete file
			}
		}

		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Successfully send ownership zip to admin";
	}else{
		$returnArr["errCode"] = 4;
		$returnArr["errMsg"] = "Error: sending ownership zip email to admin";
	}

return $returnArr;
}

function backup_tables($keyword,$type,$conn, $tables = '*') {

    global $docRoot;
    //get all of the tables
    if ($tables == '*') {
        $tables = array();
        $result = mysqli_query($conn,'SHOW TABLES');

        while ($row = mysqli_fetch_row($result)) {
            $tables[] = $row[0];
        }
    } else {
        $tables = is_array($tables) ? $tables : explode(',', $tables);
    }

    //cycle through
    foreach ($tables as $table) {

        if ($table != "presale_vouchers")
            $result = mysqli_query($conn,'SELECT * FROM ' . $table);
        $num_fields = mysqli_num_fields($result);

        $return .= 'DROP TABLE ' . $table . ';';
        $row2 = mysqli_fetch_row(mysqli_query($conn,'SHOW CREATE TABLE ' . $table));
        $return .= "\n\n" . $row2[1] . ";\n\n";

        for ($i = 0; $i < $num_fields; $i++) {
            while ($row = mysqli_fetch_row($result)) {
                $return .= 'INSERT INTO ' . $table . ' VALUES(';
                for ($j = 0; $j < $num_fields; $j++) {
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = preg_replace("\n", "\\n", $row[$j]);
                    if (isset($row[$j])) {
                        $return .= '"' . $row[$j] . '"';
                    } else {
                        $return .= '""';
                    }
                    if ($j < ($num_fields - 1)) {
                        $return .= ',';
                    }
                }
                $return .= ");\n";
            }
        }
        $return .= "\n\n\n";


    }

    $URL = $docRoot.'db_backup/keywords/database_backup/';
    if(!is_dir($URL)){
    	mkdir($URL, 0777, true);
	}

    //save file
    $URL    = $URL. '/db-backup-'.$type.'-afterKeyword-' . $keyword . '_' . date("Y-m-d-H-i-s") . '-' . (md5(implode(',', $tables))) . '.sql';
	  $handle = fopen($URL, 'w+');
    fwrite($handle, $return);
    fclose($handle);

    $URL = $docRoot.'db_backup/keywords/excel_backups/';
    if(!is_dir($URL)){
    	mkdir($URL, 0777, true);
	}
    //$URL    = $docRoot.'db_backup/keywords/excel_backups/db-backup.sql';

    $URL    = $URL.'/db-backup.sql';
	$handle = fopen($URL, 'w+');
    fwrite($handle, $return);
    fclose($handle);

    return;
}


function writeDbToExcel($fileArray,$conn) {

    global $docRoot;
    /***** Write All tables to server *****/
    foreach ($fileArray as $DB_TBLName) {



        // Create MySQL connection
        $sql = "Select * from " . $DB_TBLName . ";";

        $result = mysqli_query($conn,$sql);

        //  $URL    = $docRoot.'db_backup/keywords/excel_backups/' . $DB_TBLName . '.xls';

        $URL    = $docRoot.'db_backup/keywords/excel_backups';
        if(!is_dir($URL)){
			mkdir($URL, 0777, true);
		}

        $URL    = $URL.'/' . $DB_TBLName . '.xls';
        $handle = fopen($URL, 'w+');

        $output = null;
        /***** Start of Formatting for Excel *****/
        // Define separator (defines columns in excel &amp; tabs in word)
        $sep    = "\t"; // tabbed character

        // Start of printing column names as names of MySQL fields
		while ($fieldinfo=mysqli_fetch_field($result))
		{
			$output .= "{$fieldinfo ->name}\t";
		}
        //print("\n");
        $output .= "\n";
        // End of printing column names

        // Start while loop to get data
        while ($row = mysqli_fetch_row($result)) {
            $schema_insert = "";
            for ($j = 0; $j < mysqli_num_fields($result); $j++) {
                if (!isset($row[$j])) {
                    $schema_insert .= "NULL" . $sep;
                } elseif ($row[$j] != "") {
                    $schema_insert .= "$row[$j]" . $sep;
                } else {
                    $schema_insert .= "" . $sep;
                }
            }
            $schema_insert = str_replace($sep . "$", "", $schema_insert);
            $schema_insert = preg_replace("/\r\n|\n\r|\n|\r/", " ", $schema_insert);
            $schema_insert .= "\t";
            $output .= trim($schema_insert);
            $output .= "\n";


        }

        fwrite($handle, $output);
        fclose($handle);

    }

	return;
}

function convertDbToBackupZip($fileArray) {

    global $docRoot;
    $zip = new ZipArchive;

    //$URL = $docRoot.'db_backup/keywords/temporary_zip/keywords_ownership.zip';

    $URL = $docRoot.'db_backup/keywords/temporary_zip';
    if(!is_dir($URL)){
    	mkdir($URL, 0777, true);
	}

    $URL = $URL.'/keywords_ownership.zip';

    //if ($zip->open($URL, true ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) === TRUE) {
		if ($zip->open($URL, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE ) === TRUE ) {
        foreach ($fileArray as $filename) {
            $URL = $docRoot.'db_backup/keywords/excel_backups/' . $filename . '.xls';
            $zip->addFile($URL, $filename . '.xls');

        }

        $URL = $docRoot.'db_backup/keywords/excel_backups/db-backup.sql';
        $zip->addFile($URL, 'db-backup.sql');

        $zip->close();

    } else {

    }


   return;
}

function deleteFromLatestAskByKeywordAll($conn,$keyword){
	$returnArr = array();
	$query = "DELETE FROM latest_ask_all WHERE keyword='".$keyword."';";
	$result = runQuery($query, $conn);
	if(noError($result)){
		$returnArr["errCode"][-1]=-1;
		$returnArr["errMsg"]="Delete ask from lastest ask all";
	} else {
		$returnArr["errCode"][5]=5;
		$returnArr["errMsg"]=$result["errMsg"];
	}

	return $returnArr;

}

?>
