<?php

function getKeywordRevenueTableName($keyword) {
    $returnArr = array();
    $extraArgs = array();
    //$keyword = strip_tags($keyword);
    $keyword = strtolower($keyword);

    //Get first char
    $firstchar     = substr($keyword, 0, 1);
    $firstchar_low = strtolower($firstchar);
    if (strcasecmp($firstchar_low, $firstchar) == 0) {
        $firstchar = $firstchar_low;
    }

    //Get correct table-name
    $tableName = "";
    if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
        $tableName = "revenue_keyword_other";
    }else if (ctype_alpha($firstchar)) {
        $tableName = "revenue_keyword_" . $firstchar;
    } elseif (ctype_digit($firstchar)) {
        $tableName = "revenue_keyword_num";
    } elseif (ctype_punct($firstchar)) {
        $tableName = "revenue_keyword_num";
    } elseif (ctype_space($firstchar)) {
        $str = preg_replace('/\s+/', '', $firstchar);
    }else{
        $tableName = "revenue_keyword_num";
    }

    $extraArgs["table_name"] = $tableName;
    $errMsg = "Successfully fetched table name";
    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    return $returnArr;
}

function getKeywordAnalyticsTableName($keyword) {
    $returnArr = array();
    $extraArgs = array();
    //$keyword = strip_tags($keyword);
    $keyword = strtolower($keyword);

    //Get first char
    $firstchar     = substr($keyword, 0, 1);
    $firstchar_low = strtolower($firstchar);
    if (strcasecmp($firstchar_low, $firstchar) == 0) {
        $firstchar = $firstchar_low;
    }

    //Get correct table-name
    $tableName = "";
    if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
        $tableName = "keywords_analytics_other";
    }else if (ctype_alpha($firstchar)) {
        $tableName = "keywords_analytics_" . $firstchar;
    } elseif (ctype_digit($firstchar)) {
        $tableName = "keywords_analytics_num";
    } elseif (ctype_punct($firstchar)) {
        $tableName = "keywords_analytics_num";
    } elseif (ctype_space($firstchar)) {
        $str = preg_replace('/\s+/', '', $firstchar);
    }else{
        $tableName = "keywords_analytics_num";
    }

    $extraArgs["table_name"] = $tableName;
    $errMsg = "Successfully fetched table name";
    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    return $returnArr;
}

function getRevenueDetailsByKeyword($conn, $keyword) {
    $returnArr    = array();
    $extraArgs    = array();
    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn, $keyword);

    //get keyword ownership table name
    $kwdTableName = getKeywordRevenueTableName($keyword);
    if(noError($kwdTableName)){
        $kwdTableName = $kwdTableName["table_name"];

        $query  = "SELECT * FROM " . $kwdTableName . " WHERE keyword = '" . $keyword . "'";
        $result = runQuery($query, $conn);

        if (noError($result)) {
            $res = array();
            while ($row = mysqli_fetch_assoc($result["dbResource"])){
                $res[] = $row;
            }

            $extraArgs["data"] = $res;
            $extraArgs["table_name"] = $kwdTableName;
            $errMsg    =   $result["errMsg"];
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        } else {

            $errMsg   =   $result["errMsg"];
            $errCode  =   $result["errCode"];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Unable to retrieve table name";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function getkeywordAnaliticsDetails($conn, $keyword) {
    //print_r(func_get_args());
    $returnArr    = array();
    $extraArgs    = array();
    //cleaning of parameters for SQL Injection
    $keyword = cleanQueryParameter($conn, $keyword);

    //get keyword ownership table name
    $kwdTableName = getKeywordAnalyticsTableName($keyword);
    if(noError($kwdTableName)){
        $kwdTableName = $kwdTableName["table_name"];

        $query  = "SELECT * FROM " . $kwdTableName . " WHERE keyword = '" . $keyword . "'";
        $result = runQuery($query, $conn);

        if (noError($result)) {
            $res = array();
            while ($row = mysqli_fetch_assoc($result["dbResource"])){
                $res[] = $row;
            }

            $extraArgs["data"] = $res;
            $extraArgs["table_name"] = $kwdTableName;
            $errMsg    =   $result["errMsg"];
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        } else {

            $errMsg   =   $result["errMsg"];
            $errCode  =   $result["errCode"];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Unable to retrieve table name";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function updateRevenueTable($query, $keyword, $kwdDbConn){

    $returnArr = array();
    if(isset($query) && !empty($query) && isset($kwdDbConn) && !empty($kwdDbConn)){

        $execQuery = runQuery($query, $kwdDbConn);
        if(noError($execQuery)){

            $errMsg = "Success: Revenue details update successfully for '". $keyword. "' keyword";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{

            $errMsg = $execQuery["errMsg"];
            $errCode = $execQuery["errCode"];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Missing mandatory field";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function updateAnalyticsTable($query, $keyword, $kwdDbConn){

    $returnArr = array();
    if(isset($query) && !empty($query) && isset($kwdDbConn) && !empty($kwdDbConn)){

        $execQuery = runQuery($query, $kwdDbConn);
        if(noError($execQuery)){

            $errMsg = "Success: Analytics details update successfully for '". $keyword. "' keyword";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{

            $errMsg = $execQuery["errMsg"];
            $errCode = $execQuery["errCode"];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Missing mandatory field";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function updateOwnershipEarningInRevenueTable($query, $kwdDbConn){

    $returnArr = array();

    //  $query = "UPDATE ".$kwdRevenueTableName." SET app_kwd_ownership_earnings='".$appKwdOwnershipEarning."',user_kwd_ownership_earnings='".$usrKwdOwnershipEarning."' WHERE keyword='".$keyword."'";
    $execQuery = runQuery($query, $kwdDbConn);
    if(noError($execQuery)){
        $errMsg = "Successfully update revenue table";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = "Error: Unable to udpate keyword revenue details";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

?>