<?php

function getUserCartDetails($buyerId, $kwdsConn){

    $returnArr = array();

    //cleaning parameters for XSS Attacks
    $buyerId = cleanQueryParameter($kwdsConn, $buyerId);


  $query = "SELECT * FROM presale_users WHERE user_email= '{$buyerId}'";

    $result = runQuery($query, $kwdsConn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;
}

function getPresaleStatus($kwdsConn){

    $returnArr = array();

    $query = "SELECT * FROM presale_status ORDER BY timestamp DESC LIMIT 1;";

    $result = runQuery($query, $kwdsConn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getUserTotalCartPrice($kwdConn, $email){

    $retArray = array();
    $checkEmail = checkEmailExistance($email);

    if (noError($checkEmail)) {
        if (isset($email) && !empty($email)) {

            $userCartDetails    = getUserCartDetails($email, $kwdConn);

            $user_cart = $userCartDetails["errMsg"]["user_cart"];

            $user_cart = json_decode($user_cart, TRUE);

            $userCartCount = COUNT($user_cart);

            if (noError($userCartDetails)) {

                $getTotalKeyword_Sold=getMaxKeywordSold($kwdConn);

                $totalkwdsold = $getTotalKeyword_Sold['errMsg']['total_kw_sold'];


                //code to get individual and total price for the keyword
                $priceChangeFlage    = false;
                $result              = getCurrentActiveSlab($kwdConn);


                $current_start_limit = $result["errMsg"]["start_limit"];
                $current_end_limit   = $result["errMsg"]["end_limit"];
                $nextslab            = getNextActiveSlab($current_end_limit, $kwdConn);

                //$nextSlab_price = $nextslab["errMsg"]["BTC_price"];
                $nextSlab_price = $nextslab["errMsg"]["ITD_Price"];

                /* Calculate total keyword sold */
                $sold_plus_incart = $totalkwdsold + $userCartCount;

                if ($sold_plus_incart <= $current_end_limit) {
                    $highestPrice     = $result["errMsg"]["ITD_Price"];
                    $priceChangeFlage = false;
                } else {
                    $highestPrice     = $result["errMsg"]["ITD_Price"];
                    $priceChangeFlage = true;

                    $low_price     = $nextSlab_price; //price of next slab
                    $lowPricekeywordCount  = $sold_plus_incart - $current_end_limit; // count of low price keyword
                    $highPricekeywordCount = $userCartCount - $lowPricekeywordCount; // count of high price keyword
                }

                $count = 1;
                $finalamt=0;
                foreach ($user_cart as $key => $myKeyword) {

                    if ($count <= $lowPricekeywordCount) {
                        $keywordprice     = $low_price;
                        $priceChangeFlage = false;
                    } else {
                        $keywordprice     = $highestPrice;
                        $priceChangeFlage = true;

                    }

                    $count++;
                    //Check For Keyword Availability
                    $result            = getKeywordOwnershipDetails($kwdConn, $myKeyword);

                    $kwdOwnershipId    = $result["errMsg"]["id"];
                    $kwdOwnershipEmail = $result["errMsg"]["buyer_id"];
                    if (isset($kwdOwnershipId) && ($kwdOwnershipEmail != $email)) {
                        $availabilityFlag = 2;
                    } else {
                        $availabilityFlag = -1;
                    }

                    if ($availabilityFlag == -1) {

                        $finalamt                               = $finalamt + $keywordprice;
                        $retArray["errMsg"]["keywords"][$myKeyword] = $keywordprice;

                    } else {
                        $retArray["errMsg"]["keywords"][$myKeyword] = "NA";
                        //keyword not available
                    }


                }

                $retArray["errMsg"]["keywords"] = array_reverse($retArray["errMsg"]["keywords"],TRUE);
                $retArray["errCode"] = -1;
                $retArray["errMsg"]["total_cart_price"] = $finalamt;
                //end of code to get individual and total price for the keyword

            } else {
                $retArray["errCode"] = 2;
                $retArray["errMsg"]  = "Error getting cart details";
            }

        } else {
            $retArray["errCode"] = 2;
            $retArray["errMsg"]  = "Mandatory field not found";
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "User is not registered";
    }

    return $retArray;
}


function getMaxKeywordSold($kwdConn){

    $returnArr = array();

    //$query = "select max(kwd_sold) as total_kw_sold from presale_slabs;";
    $query = "select sum(kwd_sold) as total_kw_sold from first_purchase_slabs;";

    $result = runQuery($query, $kwdConn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


function getCurrentActiveSlab($kwdConn){

    //getCurrentActiveSlab
    $returnArr = array();

    $query = "select * from first_purchase_slabs where (kwd_Sold != (end_limit - start_limit +1)) order by id asc limit 1";
    //$query = "select * from presale_slabs; ";

    $result = runQuery($query, $kwdConn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


function getNextActiveSlab($endLimit, $kwdConn){

    $returnArr = array();

    //cleaning of parametrs for XSS
    $endLimit = cleanQueryParameter($kwdConn, $endLimit);


    $query = "select * from first_purchase_slabs where start_limit = {$endLimit}+1;";

    $result = runQuery($query, $kwdConn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

/*
 *Function	: 	checkEmailExistance()
 *Purpose	: 	To validate if user exist in wallet database.
 *Arguments	: 	(string)$email.
*/
function checkEmailExistance($email){

    global $walletPublicKey, $mode,$walletURL;
    $retArray  = array();
    $headers  = array();

    $apiText = "email={$email}&publicKey={$walletPublicKey}";
    $postFields = "email=".urlencode($email)."&publicKey=" . urlencode($walletPublicKey);
    $apiName = 'user/checkExistanceEmail';
    $requestUrl = "{$walletURL}";
    $curl_type = 'POST';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }
    return $retArray;
}

function getKeywordAvailability($words,$conn){

    $returnArr = array();

    //get keyword ownership table name
    $tableName = getKeywordOwnershipTableName($words);

    $query = "select * from " . $tableName . " where keyword='" . $words . "' ";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"][5]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}




function getKeywordOwnershipDetails($kwdConn, $keyword) {
    //get keyword ownership table name
    $kwdTableName = getKeywordOwnershipTableName($keyword);

    $returnArr    = array();

    //cleaning of parameters for SQL Injection
    $kwdTableName = cleanQueryParameter($kwdConn, $kwdTableName);
    $keyword = cleanQueryParameter($kwdConn, $keyword);

   $query  = "SELECT *, DATEDIFF(ownership_expiry_time,now()) as days_to_expire FROM " . $kwdTableName . " WHERE keyword = '" . $keyword . "' AND status='sold'";
    $result = runQuery($query, $kwdConn);
//    print_r($result);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }
    return $returnArr;

}

function getKeywordOwnershipTableName($keyword) {

    //$keyword = strip_tags($keyword);
    $keyword = strtolower($keyword);

    //Get first char
    $firstchar     = substr($keyword, 0, 1);
    $firstchar_low = strtolower($firstchar);
    if (strcasecmp($firstchar_low, $firstchar) == 0) {
        $firstchar = $firstchar_low;
    }

    //Get correct table-name
    $tableName = "";

    if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
        /*  if(strlen($keyword) != strlen($keyword, 'utf-8')){*/
        $tableName = "keywords_ownership_other";
    }else if (ctype_alpha($firstchar)) {
        $tableName = "keywords_ownership_" . $firstchar;
    } elseif (ctype_digit($firstchar)) {
        $tableName = "keywords_ownership_num";
    } elseif (ctype_punct($firstchar)) {
        $tableName = "keywords_ownership_num";
    } elseif (ctype_space($firstchar)) {
        $str = preg_replace('/\s+/', '', $firstchar);
    }else{
        $tableName = "keywords_ownership_num";
    }

    return $tableName;

}


function checkUserExistsInPresale($username,$conn){
    $returnArr = array();

    $query = "SELECT COUNT(*) as user_count FROM presale_users WHERE user_email ='".$username."';";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function insertUserInPresale($email, $kwdConn){

    $returnArr = array();
    $sid = session_id();
    $query = "INSERT INTO presale_users(user_email,user_type,session_start_time,session_id) VALUES('" . $email . "','user',NOW(),'".$sid."');";
    $result = runQuery($query, $kwdConn);
    if(noError(($result))){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Successfully insert user in presale";
    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function insertUserCartKeywordForPayment($conn, $keywordBasket, $totaAmount, $email, $type, $purchaseITDAmt)
{
    $returnArr = array();

    $length = "5";
    $strength = "1";
    $nounce = time();
    $randomCode = generateVoucherCode($length, $strength);
    $orderId = $randomCode . $nounce;
    $query = "INSERT INTO payments(order_id,order_time,amount_due_in_usd,username,payment_type,keyword_cart,amount_due) VALUE('" . $orderId . "',now(),'" . $totaAmount . "','" . $email . "','" . $type . "','" . $keywordBasket . "','" . $purchaseITDAmt . "');";
    //set custom attribute(order-id+email)
    $custom = $orderId . "," . $email;

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Success";
        $returnArr["orderId"] = $orderId;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function addKeywordToCart($keyword, $buyerId, $conn, $i, $xml_data) {

    $keyword   = strtolower($keyword);
    $returnArr = array();
    $cart      = array();

    $trans = startTransaction($conn);
    if (noError($trans)) {

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Start transaction: success';

        $result = checkForKeywordAvailability($keyword, $conn);


        if (noError($result)) {
            $keyword_status = $result["errMsg"];

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Checking for keyword availability: success';

            if ($keyword_status == "keyword_available") {

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'. Keyword status: The keyword #'.$keyword." is available";

                //Get User Cart
                $buyerDetails = getUserCartDetails($buyerId, $conn);

                $buyerCart    = $buyerDetails["errMsg"]["user_cart"];

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'. Get '.$buyerId.' cart details';

                $cart         = json_decode($buyerCart, true);
                if (empty($cart) || $cart == NULL) {
                    $cart = array();
                }
                $cart_count = COUNT($cart);

                if (!(in_array($keyword, $cart))) {

                    $cart[]    = strtolower($keyword);
                    $cart_json = json_encode($cart,JSON_UNESCAPED_UNICODE);

                    $query  = "UPDATE presale_users SET user_cart = '" . $cart_json . "' WHERE user_email = '" . $buyerId . "'";
                    $result = runQuery($query, $conn);

                    if (noError($result)) {

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. #'.$keyword.' keyword added in cart: success';

                        $trans  = commitTransaction($conn);

                        mysqli_autocommit($conn, TRUE);

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. Commit transaction: success';

                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"]      = "Keyword successfully added to cart";
                        $returnArr["cartCount"] = $cart_count+1;
                        $returnArr["step_number"] = $i;
                        $returnArr["xml_data"] = $xml_data;
                        mysqli_autocommit($conn, TRUE);
                    } else {

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. #'.$keyword.' keyword added in cart: failed';

                        $trans  = rollbackTransaction($conn);
                        $returnArr["errCode"] = 5;
                        $returnArr["errMsg"]     = "Error adding keyword to cart";
                        $returnArr["step_number"] = $i;
                        $returnArr["xml_data"] = $xml_data;

                    }

                } else {

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. #'.$keyword.' keyword already added in cart';

                    $trans = rollbackTransaction($conn);
                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"]     = "Keyword already in cart";
                    $returnArr["cartCount"] = $cart_count;
                    $returnArr["step_number"] = $i;
                    $returnArr["xml_data"] = $xml_data;
                }
            } else {
                if ($keyword_status == "keyword_blocked") {

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. Keyword status: The keyword #'.$keyword." is blocked";

                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"]     = "The keyword is blocked";
                    $returnArr["step_number"] = $i;
                    $returnArr["xml_data"] = $xml_data;

                } elseif ($keyword_status == "keyword_not_available") {

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. Keyword status: The keyword #'.$keyword." is not available";

                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"]     = "The keyword is not available";
                    $returnArr["step_number"] = $i;
                    $returnArr["xml_data"] = $xml_data;
                }
            }
        } else {

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Checking for keyword availability: failed';

            $returnArr["errCode"] = 5;
            $returnArr["errMsg"]     = "Something went wrong.Please try after some time";
            $returnArr["step_number"] = $i;
            $returnArr["xml_data"] = $xml_data;
        }
    } else {

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Start transaction: failed';

        $returnArr["errCode"] = 5;
        $returnArr["errMsg"]     = "Unable to start transaction";
        $returnArr["step_number"] = $i;
        $returnArr["xml_data"] = $xml_data;


    }

    return $returnArr;
}


//function for generating Voucher
function generateVoucherCode($length, $strength)
{
    $vowels = '123457890';
    $consonants = 'abcdefghijklmnopqrstuvwxyz';
    if ($strength & 1) {
        $consonants .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    if ($strength & 2) {
        $vowels .= "AEUY";
    }
    if ($strength & 4) {
        $consonants .= '23456789';
    }
    $password = '';
    $alt = time() % 2;
    for ($i = 0; $i < $length; $i++) {
        if ($alt == 1) {
            $password .= $consonants[(rand() % strlen($consonants))];
            $alt = 0;
        } else {
            $password .= $vowels[(rand() % strlen($vowels))];
            $alt = 1;
        }
    }
    return $password;
}

function removeKeywordFromCart($keyword, $buyerId, $conn, $i, $xml_data) {

    $returnArr = array();
    $cart      = array();

    //Get User Cart
    $buyerDetails = getUserCartDetails($buyerId, $conn);


    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'. Getting '.$buyerId.' cart details';

    $buyerCart    = $buyerDetails["errMsg"]["user_cart"];
    $cart         = json_decode($buyerCart, true);

    $cart_count   = COUNT($cart);
    $keyword      = strtolower($keyword);
    if (in_array($keyword, $cart)) {

        //Remove Keyword From Cart Array
        $cart = array_diff($cart, array(
            $keyword
        ));


        $cart_json = json_encode($cart,JSON_UNESCAPED_UNICODE);

        $cart_count = $cart_count - 1;
        if ($cart_count == 0) {
            $query = "UPDATE presale_users SET user_cart = '" . $cart_json . "',cart_start_time = NULL,cart_end_time = NULL WHERE user_email = '" . $buyerId . "'";
        } else {
            $query = "UPDATE presale_users SET user_cart = '" . $cart_json . "' WHERE user_email = '" . $buyerId . "'";
        }

        $result = runQuery($query, $conn);

        if (noError($result)) {

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. #'.$keyword.' keyword removed from cart: success';

            $trans = commitTransaction($conn);

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Commit transaction : success';

            $returnArr["errCode"] = -1;
            $returnArr["errMsg"]      = "Keyword successfully removed from cart";
            $returnArr["cartCount"] = $cart_count;
            $returnArr["step_number"] = $i;
            $returnArr["xml_data"] = $xml_data;

        } else {

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. #'.$keyword.' keyword removed from cart: failed';

            //$trans = rollbackTransaction($conn);
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"]     = "Error removing keyword to cart";
            $returnArr["step_number"] = $i;
            $returnArr["xml_data"] = $xml_data;
        }
    } else {

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Keyword already removed from cart';

        //$trans = rollbackTransaction($conn);
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"]     = "Keyword already removed from cart";
        $returnArr["cartCount"] = $cart_count;
        $returnArr["step_number"] = $i;
        $returnArr["xml_data"] = $xml_data;
    }

    return $returnArr;
}


function get_all_slab($conn)
{
    $returnArr = array();

    $query = "SELECT * FROM first_purchase_slabs;";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function blockKeywordsDuringPayment($blockTimeInMinute, $buyerId, $conn) {

    $returnArr = array();
    $cart      = array();

    //Get User Cart
    $buyerDetails = getUserCartDetails($buyerId, $conn);
    /*echo "getBuyerDetails <br />";
    printArr($buyerDetails);*/
    $buyerCart    = $buyerDetails["errMsg"]["user_cart"];
    $cart         = json_decode($buyerCart, true);

    $cart_count = COUNT($cart);
    if ($cart_count > 0) {

        foreach ($cart as $keyword) {


            $trans = startTransaction($conn);
            if (noError($trans)) {
                //Insert Into Keyword-Ownership Database
                // 4.4.1 clean keyword

                $keyword = strip_tags($keyword);
                $keyword = strtolower($keyword);
                // 4.4.2 get first char

                $firstchar     = substr($keyword, 0, 1);
                $firstchar_low = strtolower($firstchar);
                if (strcasecmp($firstchar_low, $firstchar) == 0) {
                    $firstchar = $firstchar_low;
                }

                $tableName = "";
                if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
                    $tableName = "keywords_ownership_other";
                }else if (ctype_alpha($firstchar)) {
                    $tableName = "keywords_ownership_" . $firstchar;
                } elseif (ctype_digit($firstchar)) {
                    $tableName = "keywords_ownership_num";
                } elseif (ctype_punct($firstchar)) {
                    $tableName = "keywords_ownership_num";
                } elseif (ctype_space($firstchar)) {
                    $str = preg_replace('/\s+/', '', $firstchar);
                }else{
                    $tableName = "keywords_ownership_num";
                }

               // echo "$tableName <br />";

                $query = "insert into " . $tableName . " (`buyer_id`,`cart_end_time`,`keyword`) values( '";
                $query .= cleanQueryParameter($conn, $buyerId) . "'";
                $query .= ",DATE_ADD(NOW(), INTERVAL " . $blockTimeInMinute . " MINUTE)";
                $query .= ",'" .  cleanQueryParameter($conn, $keyword) . "')";

                $result = runQuery($query, $conn);

                if (noError($result)) {

                    $query = "UPDATE presale_users SET cart_start_time=NOW(),cart_end_time=DATE_ADD(NOW(), INTERVAL " . $blockTimeInMinute . " MINUTE) WHERE user_email = '" . $buyerId . "'";

                    $result = runQuery($query, $conn);
                    /*echo "runQuery update<br />";
                    printArr($result);*/
                    if (noError($result)) {

                        $trans = commitTransaction($conn);
                        //echo "commitTransaction <br />"; die;
                    } else {

                        $trans = rollbackTransaction($conn);

                    }

                } else {
                    $trans = rollbackTransaction($conn);

                }
            }
        }

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]      = "Keywords Successfully blocked";
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"]     = "Cart is empty";
    }

//die;
    return $returnArr;
}


function getBlockKeywordsDuringPayment($conn){

    $returnArr = array();

    $query = "SELECT * FROM presale_users WHERE cart_end_time !='';";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;
}

function unblockKeywordsDuringPayment($buyerId, $conn) {

    $returnArr = array();
    $cart      = array();

    $trans = startTransaction($conn);
    if (noError($trans)) {

        //Get User Cart
        $buyerDetails = getUserCartDetails($buyerId, $conn);
        $buyerCart    = $buyerDetails["errMsg"]["user_cart"];
        $cart         = json_decode($buyerCart, true);

        foreach ($cart as $value) {
            $keyword = $value;
            $keyword = strip_tags($keyword);
            $keyword = strtolower($keyword);
            // 4.4.2 get first char

            $firstchar     = substr($keyword, 0, 1);
            $firstchar_low = strtolower($firstchar);
            if (strcasecmp($firstchar_low, $firstchar) == 0) {
                $firstchar = $firstchar_low;
            }

            // 4.4.3 get correct tablename

            $tableName = "";

            if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
                $tableName = "keywords_ownership_other";
            }else if (ctype_alpha($firstchar)) {
                $tableName = "keywords_ownership_" . $firstchar;
            } elseif (ctype_digit($firstchar)) {
                $tableName = "keywords_ownership_num";
            } elseif (ctype_punct($firstchar)) {
                $tableName = "keywords_ownership_num";
            } elseif (ctype_space($firstchar)) {
                $str = preg_replace('/\s+/', '', $firstchar);
            }else{
                $tableName = "keywords_ownership_num";
            }

            $query  = "DELETE FROM " . $tableName . " WHERE keyword = '" . cleanQueryParameter($conn, $keyword) . "' AND status = 'inCart' ";
            $result = runQuery($query, $conn);
            if (noError($result)) {
                $query  = "UPDATE presale_users SET cart_start_time = NULL,cart_end_time = NULL WHERE user_email = '" . $buyerId . "'";
                $result = runQuery($query, $conn);
            }
        }

        $trans                    = commitTransaction($conn);
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]      = "Successfully cleared the cart";

    } else {

        $returnArr["errCode"] = 5;
        $returnArr["errMsg"]     = "Unable to start transaction";

    }

    return $returnArr;
}


function writeActiveBidToJson($newActivBidsJsonName, $newActivBidsJson){
    global $docRoot;
    $addRejectBidData = array();

    $returnArr = array();
    $extraArgs = array();

    $jsonDir = $docRoot."json_directory/keywords/reject_bids/";

    if(!is_dir($jsonDir)){
        mkdir($jsonDir, 0777, true);
    }

    if(is_dir($jsonDir)){

        // creating file name with .json extension
        $rejectBids = $jsonDir.$newActivBidsJsonName;
        if(!file_exists($rejectBids)){
            $fp = fopen($rejectBids, 'w');
            fwrite($fp, $newActivBidsJson);
            fclose($fp);
        }else{
            $fp = fopen($rejectBids, "r+");
            $readFile = fread($fp, filesize($rejectBids));
            $readFile = json_decode($readFile, true);

            fclose($fp);

            $newActivBidsJson = json_decode($newActivBidsJson, true);
            foreach($readFile as $key => $value){
                $addRejectBidData[] = $value;
            }

             foreach($newActivBidsJson as $key => $value){
                $addRejectBidData[] = $value;
            }

            $addRejectBidData = json_encode($addRejectBidData);
            $fp1 = fopen($rejectBids, "w");
            fwrite($fp1, $addRejectBidData);
            fclose($fp);
        }
        $errMsg = "Active bid details write Successfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = "json directory not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

  return $returnArr;
}

function clearAllKeywordFromCart($email, $kwdDbConn, $xml_data, $i){

    $returnArr = array();
    $cart      = array();
    $j = 0;

    $startTransaction = startTransaction($kwdDbConn);
    if(noError($startTransaction)){

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Start Transaction : success';

        $buyerDetails = getUserCartDetails($email, $kwdDbConn);

        if(noError($buyerDetails)){
          $buyerDetails = $buyerDetails["errMsg"];
          $buyerCart    = $buyerDetails["user_cart"];
          $cart         = json_decode($buyerCart, true);
          $errCode      = 0;

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Get user cart details: success';

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. User keywords in cart => '.$buyerCart;
            
          foreach($cart as $key => $keyword){
              // get keyword ownership details
              $getKwdOwnershipDetails        = getKeywordOwnershipDetails($kwdDbConn, $keyword);
              if(noError($getKwdOwnershipDetails)){
                  $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                  $kwdOwnerEmail = $getKwdOwnershipDetails["buyer_id"];

                  $i = $i + 1;
                  $j = $j + 1;
                  $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'. Get keyword ownership details of #'.$keyword.' : success';

                  // get keyword table name
                  $kwdTableName  = getKeywordOwnershipTableName($keyword);

                  $j = $j + 1;
                  $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'. Get keyword ownership table name of #'.$keyword.' : success, 1] Table Name: '.$kwdTableName;

                  if($kwdOwnerEmail == $email){
                      $query  = "DELETE FROM " . $kwdTableName . " WHERE keyword = '" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND status = 'inCart' ";
                      $result = runQuery($query, $kwdDbConn);
                      if (noError($result)) {

                          $j = $j + 1;
                          $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'. Delete #'.$keyword.' details from  '.$kwdTableName.' ownership table: success.';
                          $errorCode = -1;
                          $errMsg = ' Delete #'.$keyword.' details from  '.$kwdTableName.' ownership table: success.';
                      } else {

                          $j = $j + 1;
                          $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'. Delete #'.$keyword.' details from  '.$kwdTableName.' ownership table: failed.';
                          $errorCode = 99;
                          $errMsg = 'Delete #'.$keyword.' details from  '.$kwdTableName.' ownership table: failed.';

                          $errCode = $errCode + 1;
                      }

                      $extraArgs["stepCounter"] = $i;
                      $extraArgs["xml_data"] = $xml_data;
                      $errMsg = "Success";
                      $returnArr = setErrorStack($returnArr, $errorCode, $errMsg, $extraArgs);
                  }
              }else{

                  $i = $i + 1;
                  $j = $j + 1;
                  $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'. Get keyword ownership details of #'.$keyword.' : failed';

                  $extraArgs["stepCounter"] = $i;
                  $extraArgs["xml_data"] = $xml_data;

                  $errMsg = "Error: fetching ownership details of #".$keyword. " keyword";
                  $returnArr = setErrorStack($returnArr, $key, $errMsg, $extraArgs);
              }

              $j = 0;
          } // end of foreach loop

          if ($errCode == 0) {
              $query  = "UPDATE presale_users SET cart_start_time = NULL,user_cart = NULL,cart_end_time = NULL WHERE user_email = '" . $email . "'";
              $result = runQuery($query, $kwdDbConn);
              if (noError($result)) {

                  $i = $i + 1;
                  $xml_data["step".$i]["data"] = $i.'. Update presale_user table set cart_start_time=NULL, cart_end_time = NULL of user '.$email.' : success';

                  $trans                    = commitTransaction($kwdDbConn);

                  $extraArgs["stepCounter"] = $i;
                  $extraArgs["xml_data"] = $xml_data;
                  $errMsg = "Successfully cleared the cart";
                  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
              } else {

                  $i = $i + 1;
                  $xml_data["step".$i]["data"] = $i.'. Update presale_user table set cart_start_time=NULL, cart_end_time = NULL of user '.$email.' : failed';

                  rollbackTransaction($kwdDbConn);

                  $extraArgs["stepCounter"] = $i;
                  $extraArgs["xml_data"] = $xml_data;
                  $errMsg = "Error: clearing user cart";
                  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
              }
          } else {

              $i = $i + 1;
              $xml_data["step".$i]["data"] = $i.'. clearing user cart : failed';

              rollbackTransaction($kwdDbConn);

              $extraArgs["stepCounter"] = $i;
              $extraArgs["xml_data"] = $xml_data;

              $errMsg = "Error: clearing user cart";
              $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
          }
        }else{

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Get user cart details: failed';

            $extraArgs["stepCounter"] = $i;
            $extraArgs["xml_data"] = $xml_data;

            $errMsg = "Error: Fetching user cart details";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Start Transaction : failed';

        $extraArgs["stepCounter"] = $i;
        $extraArgs["xml_data"] = $xml_data;

        $errMsg = "Error: start transaction";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;

}


function getAllKwdOwnershipData($smlLetter,$conn){

    $returnArr = array();

    $query = "SELECT *, DATEDIFF(ownership_expiry_time,now()) as no_of_days FROM keywords_ownership_".$smlLetter." where DATEDIFF(ownership_expiry_time,now()) <= 30";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function sendEmailNotification($bulkEmailArr){

    $returnArr = array();
    global $userRequiredFields, $walletURLIP;

    foreach($bulkEmailArr as $key => $renewEmail){

        // get user info to retrieve the first name and last name.
        $getUserReqFields = $userRequiredFields.",_id";
        $getKwdOwnerDetails = getUserInfo($renewEmail["to"], $walletURLIP . 'api/v3/', $getUserReqFields);
        if(noError($getKwdOwnerDetails)){
            $getKwdOwnerDetails = $getKwdOwnerDetails["errMsg"];
            $id = $getKwdOwnerDetails["_id"];
            $firstName = $getKwdOwnerDetails["first_name"];
            $lastName = $getKwdOwnerDetails["last_name"];

            // get notification preference
            $method_name          = "GET";

            $getNotificationPreference = notifyoptions($id, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
            if(noError($getNotificationPreference)){
                $permissionCode  = $getNotificationPreference["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                $to      = $renewEmail["to"];
                $subject = $renewEmail["subject"];
                $message = $renewEmail["body"];
                $notification_message = $renewEmail["notification_msg"];
                $category   = "buy";
                $linkStatus = 1;
                $sendEmailAndNotifiToKwdOwner   = sendNotificationBuyPrefrence($to, $subject, $message, $firstName, $lastName, $id, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                if(noError($sendEmailAndNotifiToKwdOwner)){
                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = 'Successfully send email and notification for renew keyword ownership to '.$renewEmail["to"];
                }else{
                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = 'Error: Unable to send email and notification to '.$renewEmail["to"].' user. ';
                }
            }else{
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = 'Error: Fetching notification preference of '.$renewEmail["to"].' user.';
            }
        }else{

            $returnArr["errCode"] = 1;
            $returnArr["errMsg"] = 'Error: Fetching notification preference of '.$renewEmail["to"].' user.';
        }
    }

    return $returnArr;
}

function revokeKeywordOwnership($email, $keyword, $kwdTableName, $conn){

    $returnArr = array();

    $keyword = cleanQueryParameter($conn, $keyword);

//    //get keyword ownership table name
//    $kwdTableName = getKeywordOwnershipTableName($keyword);

    $query = "Update ".$kwdTableName." set buyer_id='', ownership_expiry_time='' where keyword='".$keyword."'";

    $execQuery = runQuery($query, $conn);

    $execQuery = array("errCode" => -1);
    if(noError($execQuery)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Successfully revoked ownership of ".$email;
    }else{
        $returnArr["errCode"] = 3;
        $returnArr["errMsg"] = "Error: Revoking ownership of ".$email;
    }

    return $returnArr;
}

function deleteOwnedKwdFromMyKwdDetails($kwdBuyerId, $keyword, $kwdTableName, $conn){

    $returnArr = array();

    $query = "Select * from mykeyword_details where buyer_id='".$kwdBuyerId."'";

    $execQuery = runQuery($query, $conn);

    if (noError($execQuery)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($execQuery["dbResource"]))
            $res = $row;

        $myKeyword = $res["transaction_details"];

        $myKeyword = json_decode($myKeyword, true);

        foreach($myKeyword as $key => $ownedKwd){

            if($ownedKwd["keyword"] == $keyword){
                unset($myKeyword[$key]);
            }
        }

        $myKeyword = json_encode($myKeyword);

        $query = "Update mykeyword_details set transaction_details='".$myKeyword."' where buyer_id='".$kwdBuyerId."'";
        $execQuery = runQuery($query, $conn);
        if(noError($execQuery)){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = "Success: Update my owned keyword details and revoked ownership";
        }else{
            $returnArr["errCode"] = 23;
            $returnArr["errMsg"] = "Error: Updating my owned keyword details";
        }
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $execQuery["errMsg"];
    }

    return $returnArr;
}

function addRejectActiveBids($rejectBidJson, $conn){

    $returnArr = array();
    $values = array();

    $rejectBidJson = json_decode($rejectBidJson, true);

    foreach( $rejectBidJson as $key =>  $row) {
        $row = explode('~~', $row);
        $values[] =  "('" . $row[0] . "','" . $row[1] . "'," . $row[2]. "," . $row [3]. "," . $row[4]. ",'" . $row[5]. "','" . $row[6]. "','" . $row[7]. "'," .$row[8]. ")";
    }

    $query = "INSERT INTO reject_bid_queue (bid_table_name, email, random_number, bid_amount, trade_fees, bid_place_on_date, keyword, user_id, renewal_amount) VALUES ". implode(',',$values);

    $execQuery = runQuery($query, $conn);

    if(noError($execQuery)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Successfully insert reject bid user in queue";

    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = "Error: Inserting reject bid user in queue";
    }

    return $returnArr;

}


function updateRenewKwdInPresale( $email, $renewArr, $kwdDbConn){
    $returnArr = array();

    $query = "update presale_users set user_renew_kwd_in_cart='{$renewArr}'  WHERE user_email= '{$email}'";

    $result = runQuery($query, $kwdDbConn);

    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]= "Renew keyword added successfully";
    } else {
        $returnArr["errCode"]=5;
        $returnArr["errMsg"]= "Error: Adding renew keyword";

    }

    return $returnArr;

}

function removeRenewKwdFrmPresale($email, $keyword, $kwdDbConn){

    $returnArr = array();

    $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
    if(noError($getPresaleDetails)){
        $getPresaleDetails = $getPresaleDetails["errMsg"];

        $renewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];
        $renewKwd = json_decode($renewKwd, true);

        if(in_array($keyword, $renewKwd)){
            //Remove Keyword From Cart Array
            $renewKwd = array_diff($renewKwd, array(
                $keyword
            ));

            $renewKwd = json_encode($renewKwd,JSON_UNESCAPED_UNICODE);

            $updateRenewKwd = updateRenewKwdInPresale( $email, $renewKwd, $kwdDbConn);
            if(noError($updateRenewKwd)){
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "successfully remove ".$keyword." keyword";
            }else{
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error: updating the keyword renewal details";
            }

        }

   }else{
       $returnArr["errCode"] = 1;
       $returnArr["errMsg"] = "Error: Fetching user renew keyword details";
   }

    return $returnArr;
}

function getTotalKeyword_used($conn){

    $returnArr = array();

    $query = "SELECT SUM(kwd_sold) AS total_used_kwd from first_purchase_slabs;";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getAllMyKeywordOwners($conn){
    $returnArr = array();

    $query = "SELECT * FROM mykeyword_details;";

    //echo $query;
    $result = runQuery($query,$conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;

    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllMyKeywordOwnersCount($conn){
    $returnArr = array();
    $ownerCount = 0;
    $result = getAllMyKeywordOwners($conn);

    if(noError($result)){
        foreach($result["errMsg"] as $value){
            $transactionDetails = $value["transaction_details"];
            $transactionDetailsArr = json_decode($transactionDetails,TRUE);
            $transactionDetailsCount = COUNT($transactionDetailsArr);
            if($transactionDetailsCount > 0){

                $ownerCount = $ownerCount + 1;
            }

        }

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]["kwd_owner_count"] = $ownerCount;
    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}


function getTotalTradeProfitSoFar($conn){

    $returnArr = array();

    $query = "SELECT SUM(trade_profit) as total_trade_profit FROM trade_profit WHERE trade_profit >0";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

?>
