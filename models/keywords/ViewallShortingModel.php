<?php


function highesttradeshorting($pagename,$type,$conn)
{

    if ($pagename == "latesttrade" && $type == "highest") {
        $limit = 15;
        if (isset($_GET["page"])) {
            $page = $_GET["page"];
        } else {
            $page = 1;
        };
        $start_from = ($page - 1) * $limit; //echo $start_from;
        //  $sql = "SELECT * FROM latest_trades ORDER BY id DESC;";
       // echo   $sql = "SELECT * FROM latest_trades ORDER BY trade_price DESC Limit $start_from, $limit;";
        $sql = "SELECT * FROM latest_trades ORDER BY trade_price DESC Limit $start_from, $limit;";
        $result = runQuery($sql, $conn);

        if (noError($result)) {
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"] = -1;

            $returnArr["errMsg"] = $res;

        } else {

            $returnArr["errCode"] = 5;

            $returnArr["errMsg"] = $result["errMsg"];

        }

        return $returnArr;
    } else if ($pagename == "latesttrade" && $type == "lowest") {
        $limit = 15;
        if (isset($_GET["page"])) {
            $page = $_GET["page"];
        } else {
            $page = 1;
        };
        $start_from = ($page - 1) * $limit;

        $sql = "SELECT * FROM latest_trades ORDER BY trade_price ASC Limit $start_from, $limit;";
        $result = runQuery($sql, $conn);

        if (noError($result)) {
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"] = -1;

            $returnArr["errMsg"] = $res;

        } else {

            $returnArr["errCode"] = 5;

            $returnArr["errMsg"] = $result["errMsg"];

        }

        return $returnArr;


    }

    else if ($pagename == "latesttrade" && $type == "latest") {
        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit;

        //$sql = "SELECT * FROM latest_trades WHERE trade_time > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND trade_time <= NOW() order by id DESC  Limit $start_from, $limit;";
        $sql = "select * from `latest_trades` order by trade_time DESC Limit $start_from, $limit;";
        $result = runQuery($sql, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;


    }
}


function askhighest($conn)
{

    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    $sql = "SELECT * FROM latest_ask ORDER BY ask_price DESC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function asklowest($conn)
{

    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    $sql = "SELECT * FROM latest_ask ORDER BY ask_price ASC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function asklatest($conn)
{
    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    //$sql = "SELECT * FROM latest_ask WHERE ask_time > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND ask_time <= NOW() order by id DESC  Limit $start_from, $limit";
    $sql = "select * from `latest_ask` order by ask_time DESC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function bidhighest($conn)
{
    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    $sql = "SELECT * FROM latest_bid ORDER BY bid_price DESC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function bidlowest($conn)
{
    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    $sql = "SELECT * FROM latest_bid ORDER BY bid_price ASC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function bidlatest($conn)
{
    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    //$sql = "SELECT * FROM latest_bid WHERE bid_time > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND bid_time <= NOW() order by id DESC  Limit $start_from, $limit";
    $sql = "select * from `latest_bid` order by bid_time DESC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

function newownedlatest($conn)
{
    $limit = 15;
    if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
    $start_from = ($page-1) * $limit;

    //$sql = "SELECT * FROM latest_bid WHERE bid_time > DATE_SUB(NOW(), INTERVAL 24 HOUR) AND bid_time <= NOW() order by id DESC  Limit $start_from, $limit";
    $sql = "select * from `recent_sold_keyword` order by purchase_timestamp DESC Limit $start_from, $limit;";
    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}



function datePaginations($type,$startDate,$endDate,$conn)
{

    if($type == "latest")
    {
        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit;

    echo $sql = "select  *  from latest_trades where trade_time BETWEEN '$startDate' AND '$endDate 23:59:59' ORDER BY id DESC Limit $start_from, $limit";

    $result = runQuery($sql, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;


}

    else if($type == "highest")
    {
        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit;

         $sql = "SELECT * FROM latest_trades  where trade_time BETWEEN '$startDate' AND '$endDate 23:59:59' ORDER BY trade_price DESC Limit $start_from, $limit;";
        $result = runQuery($sql, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;


    }

    else if($type == "lowest")
    {
        $limit = 15;
        if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
        $start_from = ($page-1) * $limit;

         $sql = "SELECT * FROM latest_trades where trade_time BETWEEN '$startDate' AND '$endDate 23:59:59' ORDER BY trade_price ASC Limit $start_from, $limit;";
        $result = runQuery($sql, $conn);

        if(noError($result)){
            $res = array();

            while ($row = mysqli_fetch_assoc($result["dbResource"]))

                $res[] = $row;

            $returnArr["errCode"]=-1;

            $returnArr["errMsg"]=$res;

        } else {

            $returnArr["errCode"]=5;

            $returnArr["errMsg"]=$result["errMsg"];

        }

        return $returnArr;


    }

}



?>