<?php

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateQueryList()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateQueryList()
*   Purpose       :   This function will insert/update support_requests table and store queryInfo rasied by User
*                 :   $queryList will bring json format data(requestType,keywordName in array sperated by space, payment,amountITD,hashPayment,BTCaddress,comment )
*   Arguments     :   (json) $queryList, (object)$conn
*/
function checkIsPresentAgent($conn)
{

    $returnArr = array();
    global $agentsGroupCS;
    $query = "select count(*) as count from admin_user where group_id LIKE '%{$agentsGroupCS}%'  and admin_email <> 'admin@keywo.com'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function updateQueryList($ticketID,$regemail,$queryList,$country,$conn)
{
    $creationFlag = false;
    $ticketID_old = getRequestTicketDetails($regemail,$conn)["errMsg"][0]["ticket_id"];
    if($ticketID_old =="") {
        global $agentsGroupCS;
        $DBConnection = new DBConnection();
        $credential = $DBConnection->getDatabaseCredential("acl");
        $dbaccesscontrol = $credential["database"];
        $ticketID = getNextOrderNumber($conn);
        $returnArr = array();
        $tableName = "support_requests";
        $ticketRasiedon = date('Y-m-d h:i:s');

        $getUserQueryDetails = getQueryListInfo($conn, $ticketID);

        if (noError($getUserQueryDetails)) {
            $getUserQueryDetails = $getUserQueryDetails["data"][0];
            $ticket_id = $getUserQueryDetails["ticket_id"];
        }

        if (empty($ticket_id)) {

            $connAcl = createDBConnection("acl");
            $connAcl = $connAcl["connection"];
            $checkCount=checkIsPresentAgent($connAcl)["errMsg"][0]["count"];
            if($checkCount>0)
            {
                $assignQuery = "SELECT
                        COUNT(assigned_agent_id) AS assigned_count,
                        admin_user.admin_id,
                        ticket_id,
                        reg_email,
                        admin_user.admin_email
                    FROM
                        support_requests
                    RIGHT JOIN {$dbaccesscontrol}.admin_user ON support_requests.assigned_agent_id = {$dbaccesscontrol}.admin_user.admin_id
                    WHERE
                        {$dbaccesscontrol}.admin_user.group_id LIKE '%{$agentsGroupCS}%' and  {$dbaccesscontrol}.admin_user.admin_email <> 'admin@keywo.com'
                    GROUP BY
                        assigned_agent_id
                    ORDER BY
                        assigned_count ASC,
                        assigned_agent_id ASC
                    LIMIT 1";
            }else
            {
                $assignQuery = "SELECT
                        COUNT(assigned_agent_id) AS assigned_count,
                        admin_user.admin_id,
                        ticket_id,
                        reg_email,
                        admin_user.admin_email
                    FROM
                        support_requests
                    RIGHT JOIN {$dbaccesscontrol}.admin_user ON support_requests.assigned_agent_id = {$dbaccesscontrol}.admin_user.admin_id
                    WHERE
                        {$dbaccesscontrol}.admin_user.group_id LIKE '%{$agentsGroupCS}%'
                    GROUP BY
                        assigned_agent_id
                    ORDER BY
                        assigned_count ASC,
                        assigned_agent_id ASC
                    LIMIT 1";
            }

            $resultJoin = runQuery($assignQuery, $conn);
           // echo $assignQuery;
            while ($row = mysqli_fetch_assoc($resultJoin["dbResource"])) {
                $assigned_agent["admin_id"] = $row["admin_id"];
                $assigned_agent["admin_email"] = $row["admin_email"];
            }
            
            $query = "INSERT into " . $tableName . "(ticket_id,request_type,reg_email,keyword_purchased,amount_paid,date_time,paypal_address,bitcoin_address,transaction_hash,prevoiuskwd_owner,isrecivedTrade_email,cashout_status,trade_type,wallet_service_used,comments,request_status,requestTime,assigned_on,assigned_to,assigned_by,closed_on,ratings_by_user,user_country,rec_sender_email,assigned_agent_id) VALUES('$ticketID','" . $queryList['requestType'] . "','$regemail','" . $queryList['keywordName'] . "','" . $queryList['amountITD'] . "','" . $queryList['payment'] . "','" . $queryList['paypal'] . "','" . $queryList['BTCaddress'] . "','" . $queryList['hashPayment'] . "','','" . $queryList["recNotification"] . "','','" . $queryList["tradeType"] . "','','" . $queryList["comment"] . "','new','$ticketRasiedon','$ticketRasiedon','{$assigned_agent["admin_email"]}','auto','','','$country','" . $queryList["recEmail"] . "',{$assigned_agent["admin_id"]})";

            $creationFlag = true;


        } else {
            $query = "UPDATE " . $tableName . " SET request_status='' WHERE ticket_id='" . $ticketID . "'";

        }
        // echo $query;
        $result = runQuery($query, $conn);
           // printArr($result);

        if (noError($result)) {
            $res = array();
            if ($creationFlag) {
                $query_Comment = updateCommentText($queryList["comment"], $regemail, $ticketID, 'customer', $conn);
                if (noError($query_Comment)) {
                    $returnArr["errCode"] = -1;

                    $returnArr["errMsg"] = "Query Submitted Success";
                } else {
                    $returnArr["errCode"] = 10;

                    $returnArr["errMsg"] = $query_Comment["errMsg"];
                }
            } else {
                $returnArr["errCode"] = -1;

                $returnArr["errMsg"] ="Query Submitted Success";
            }      
        } else {

            $returnArr["errCode"] = 10;

            $returnArr["errMsg"] = $result["errMsg"];

        }
    }else{
        $returnArr["errCode"] = 10;
        $returnArr["errMsg"] = "Ticket is already active from user.Not allowed to create new tickets until first one closed";
    }

        return $returnArr;
}


function getQueryListInfo($conn, $ticketID) {
    
    $returnArr    = array();
    $extraArgs    = array();

        $query  = "SELECT * FROM `support_requests` WHERE ticket_id = '" . $ticketID . "'";
        $result = runQuery($query, $conn);

        if (noError($result)) {
            $res = array();
            while ($row = mysqli_fetch_assoc($result["dbResource"])){
                $res[] = $row;
            }

            $extraArgs["data"] = $res;
            $errMsg    =   $result["errMsg"];
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        } else {

            $errMsg   =   $result["errMsg"];
            $errCode  =   $result["errCode"];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
        }

    return $returnArr;
}



function selectmaxservicerequest($conn){

    $returnArr = array();

    $query = "SELECT MAX(id) as lastid FROM support_requests;";

//echo $query;
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"][5]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


function getNextOrderNumber($conn)
{
    $result=selectmaxservicerequest($conn);
    $lastOrder=$result["errMsg"]["lastid"];
    $lastOrder = $lastOrder+1;
    return 'KT-' .$lastOrder;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateQueryList()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateQueryList()
*   Purpose       :   This function will insert/update support_requests table and store queryInfo rasied by User
*                 :   $queryList will bring json format data(requestType,keywordName in array sperated by space, payment,amountITD,hashPayment,BTCaddress,comment )
*   Arguments     :   (json) $queryList, (object)$conn
*/

function getSupportHistory($email, $offsetValue, $limit, $conn) {

    $returnArr = array();

    $query = "SELECT id,request_type,requestTime,closed_on,ratings_by_user FROM support_requests WHERE reg_email = '".$email."' AND request_status = 'closed' ";

    if (!empty($offsetValue)) {
        $query .= " AND id > ".$offsetValue;
    } 

    $query .= " ORDER BY closed_on DESC ";

    $query .= " LIMIT ".$limit;
    
    // echo "QUERY : ". $query;
    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"]  = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"]  = $result["errMsg"];

    }

    return $returnArr;
}


function setCloseTicket($ticketID,$conn)
{

    $returnArr = array();

    $query = "UPDATE support_requests SET request_status='closed', closed_on = NOW() WHERE ticket_id='" . $ticketID . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $extraArg = array();

        $result = runQuery($query, $conn);
        if (noError($result)) {
            $errMsg = "Data Updated Successfully.";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
        }
        return $returnArr;
    }
}

function setCustomerReviews($ticketId,$custStar,$conn) {
    $returnArr = array();

    $query = "UPDATE support_requests SET ratings_by_user='" . $custStar . "' WHERE ticket_id='" . $ticketId . "'";
   
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $extraArg = array();

        $result = runQuery($query, $conn);
        if (noError($result)) {
            $errMsg = "Data Updated Successfully.";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
        }
        return $returnArr;
    }
}




function getAllfaqsSupportData($offset, $limit, $conn, $category)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from faqs";

   if($category!="") {
       $query .= " where 1=1 ";
   }

     if ($category!="") {
        $query .= " AND  category='" . $category . "' AND status='1'";
    }

    $query .= " ORDER by created DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllfaqsSupportDataCount($conn,$category)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from faqs";

   if($category!="")
   {
       $query .= " where 1=1 ";
   }

    if ($category!="") {
        $query .= " AND  category='" . $category . "'";
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllUserSupportfaqsData($offset, $limit, $conn, $category)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from keywo_features_faq";

     if ($category!="") {
        $query .= " WHERE category='" . $category . "'";
    }

    $query .= " ORDER by created DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllUserSupportfaqsDataById($id, $conn)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from keywo_features_faq WHERE id='".$id."';";

     

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}