
<?php

//name to id convert apps
function getAppIDbyName($appname, $conn){
  $returnArr = array();
  $query = "SELECT * from sc_app_details where app_name='".$appname."'";
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
          $res[] = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}

//name For Autocomplete
function getAppIDbyNameForAutoComplete($appname, $conn){
  $returnArr = array();
  $query = "SELECT * from sc_app_details where app_name like '%".$appname."%' and status=1";
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
          $res[] = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}

//name For Checking the field is present or not
function getAppIDbyNameValidationComplete($appname, $conn){
  $returnArr = array();
    $query = "SELECT * from sc_app_details where app_name = '".$appname."'";
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
          $res[] = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}

//name to id convert apps
function getAppInfo($conn){
  $returnArr = array();
  $query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 order by TotSearches DESC";

  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
          $res[] = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}


//all app info
function getAppInfoById($id,$conn){
	$returnArr = array();
	$query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where app_id='".$id."' and status=1";

	$result = runQuery($query, $conn);
	if(noError($result)){
		$res = array();
		while ($row = mysqli_fetch_assoc($result["dbResource"]))
			$res[] = $row;
		$returnArr["errCode"]=-1;
		$returnArr["errMsg"]=$res;
	} else {
		$returnArr["errCode"]=5;
		$returnArr["errMsg"]=$result["errMsg"];
	}
	return $returnArr;
}

/*
 *Function	:	getSuggestedApps()
 *Purpose	: 	To get Suggested App With is not in viewed app.
  *Argumnets	: 	(string)$page, (string)$conn.
 */
 function getSuggestedApps($page, $favSearchApp,$conn){
  if(empty($favSearchApp)){
    $favSearchApp = 0;
  }

	$returnArr = array();
	if($page == "apps.php"){
	    $query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 AND app_id NOT IN ($favSearchApp) ORDER BY RAND()";
	}
	else
	{
	 	$query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 AND  app_id NOT IN ($favSearchApp) order by TotSearches DESC limit 5";
	}
	$result = runQuery($query, $conn);
	if(noError($result)){
		$res = array();
		while ($row = mysqli_fetch_assoc($result["dbResource"]))
			$res[] = $row;
		$returnArr["errCode"]=-1;
		$returnArr["errMsg"]=$res;
	} else {
		$returnArr["errCode"]=5;
		$returnArr["errMsg"]=$result["errMsg"];
	}
	return $returnArr;
}


/*
 *Function	: 	getFavSearchCategories();
 *Purpose	: 	To return favourite apps categories details to default search result page.
 *Argumnets	: 	(string)$favSearchApp, (string)$conn.
*/

function getFavSearchCategories($favSearchApp, $conn){

    $returnArr = array();
    $extraArg = array();
    $favSearchApp = empty($favSearchApp) ? "'#'" : $favSearchApp;
    //$query = "select sf.*, (`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from search_favourite_app sf JOIN sc_app_details sc ON sf.app_id = sc.app_id where sf.status=1 and (sf.app_id in ($favSearchApp)) order by TotSearches DESC";
    $query = "select *, (`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 and (app_id in ($favSearchApp)) order by TotSearches DESC";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $extraArg["data"]=$res;
        $returnArr = setErrorStack($returnArr, -1 ,null, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 3 ,null, $extraArg);
    }

    return $returnArr;
}


/*
 *Function	:	getAllApps()
 *Purpose	: 	To get app details from sc_app_details table.
 *
 */

function getAllApps( $page,$conn){
	$returnArr = array();

	if($page == "apps.php" || $page == "appAjax.php"){
		$query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 order by TotSearches DESC";
	}
	else
	{
		$query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 order by TotSearches DESC limit 5";
	}

	$result = runQuery($query, $conn);
	if(noError($result)){
		$res = array();
		while ($row = mysqli_fetch_assoc($result["dbResource"]))
			$res[] = $row;
		$returnArr['errCode']=-1;
		$returnArr['errMsg']=$res;
	} else {
		$returnArr['errCode']=5;
		$returnArr['errMsg']=$result['errMsg'];
	}

	return $returnArr;
}


/*
 *Function	:	getFavLoggedOutApps()
 *Purpose	: 	To get app details from sc_app_details and search_favourite_app table in logged out mode.
 *Arguments	: 	(string)$conn as search database connection (search97_db).
 *Returns	:	Returns all app details in array having status = 1.
				Where 1 states active app on system.
 */

function getFavLoggedOutApps($conn){
    $returnArr = array();

    $query = "SELECT sc.*, (`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches, sf.result_url from sc_app_details as sc join search_favourite_app as sf
	 ON sc.app_id=sf.app_id WHERE sc.status=1 order by TotSearches DESC LIMIT 0,5";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr['errCode'][-1]=-1;
        $returnArr['errMsg']=$res;
    } else {
        $returnArr['errCode'][5]=5;
        $returnArr['errMsg']=$result['errMsg'];
    }

    return $returnArr;
}

//function to call an api to add fav app
function addFavApp($userId, $appId) {
  global  $walletPublicKey, $mode, $NotificationURL;
  $retArray = array();
  $header   = array();

  /* create signature */
  $apiText    = "user_id={$userId}&favappid={$appId}&publicKey={$walletPublicKey}";
  $postFields = "user_id=" . $userId . "&favappid=" . $appId . "&publicKey=" . urlencode($walletPublicKey);
  $apiName    = 'user/' . $userId . '/favapp/add/' . $appId;
  $curl_type  = 'PUT';
  $requestUrl = $NotificationURL."v2/";
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
  // print_r($curlReqParam); die;

  /* Check mode type from config (Nonce active or not)*/
  // if($mode == 'production') {
  //   $nonce_result = generateSignatureWithNonce($apiText);
  //   if(noError($nonce_result)) {
  //     $nonce_result        = $nonce_result['errMsg'];
  //     $retArray['errCode'] = -1;
  //     //$retArray['errMsg']  = $nonce_result['errMsg'];
  //     $headers[]           = "x-ts: {$nonce_result['timestamp']}";
  //     $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
  //   } else {
  //     $retArray['errCode'] = 2;
  //     $retArray['errMsg']  = $nonce_result['errMsg'];
  //   }
  // }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
  //print_r($respArray);
  if (noError($respArray)) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}

//function to call an api to update fav app
function updateFavApp($userId, $appId, $changeAppId) {
  global  $walletPublicKey, $mode, $NotificationURL;
  $retArray = array();
  $header   = array();

  /* create signature */
  $apiText    = "user_id={$userId}&favappid={$changeAppId}&publicKey={$walletPublicKey}";
  $postFields = "user_id=" . $userId . "&favappid=" . $changeAppId . "&publicKey=" . urlencode($walletPublicKey) . "&updateId=" . $appId;
  $apiName    = 'user/' . $userId . '/favapp/update/' . $changeAppId;
  $curl_type  = 'PUT';
  $requestUrl = $NotificationURL."v2/";
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
  //print_r($curlReqParam); die;

  /* Check mode type from config (Nonce active or not)*/
  // if($mode == 'production') {
  //   $nonce_result = generateSignatureWithNonce($apiText);
  //   if(noError($nonce_result)) {
  //     $nonce_result        = $nonce_result['errMsg'];
  //     $retArray['errCode'] = -1;
  //     //$retArray['errMsg']  = $nonce_result['errMsg'];
  //     $headers[]           = "x-ts: {$nonce_result['timestamp']}";
  //     $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
  //   } else {
  //     $retArray['errCode'] = 2;
  //     $retArray['errMsg']  = $nonce_result['errMsg'];
  //   }
  // }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
  //print_r($respArray);
  if (noError($respArray)) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}

//function to call an api to delete/remove fav app
function deleteFavApp($userId, $appId) {
  global  $walletPublicKey, $mode, $NotificationURL;
  $retArray = array();
  $header   = array();

  /* create signature */
  $apiText    = "user_id={$userId}&favappid={$appId}&publicKey={$walletPublicKey}";
  $postFields = "user_id=" . $userId . "&favappid=" . $appId . "&publicKey=" . urlencode($walletPublicKey);
  $apiName    = 'user/' . $userId . '/favapp/delete/' . $appId;
  $curl_type  = 'PUT';
  $requestUrl = $NotificationURL."v2/";
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
  // print_r($curlReqParam); die;

  /* Check mode type from config (Nonce active or not)*/
  // if($mode == 'production') {
  //   $nonce_result = generateSignatureWithNonce($apiText);
  //   if(noError($nonce_result)) {
  //     $nonce_result        = $nonce_result['errMsg'];
  //     $retArray['errCode'] = -1;
  //     //$retArray['errMsg']  = $nonce_result['errMsg'];
  //     $headers[]           = "x-ts: {$nonce_result['timestamp']}";
  //     $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
  //   } else {
  //     $retArray['errCode'] = 2;
  //     $retArray['errMsg']  = $nonce_result['errMsg'];
  //   }
  // }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
  //print_r($respArray);
  if (noError($respArray)) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    // if ($mode == 'production') {
    //   $nonce_counter = incrementVectorCounter();
    // }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}
?>
