<?php
//name to id convert apps
function updateCommentText($commentText, $emailID,$tickectId,$userType, $conn)
{
    $extraArg = array();
    $returnArr = array();
    $res = array();
    $msg = array();
    $rowdata="";
    $queryToCheck = "SELECT * FROM support_requests WHERE ticket_id='" . $tickectId . "'";

    $resultCheck = runQuery($queryToCheck, $conn);
    if (noError($resultCheck)) {
        while ($row = mysqli_fetch_assoc($resultCheck["dbResource"])){

            $rowdata = isset($row['request_status'])?$row['request_status']:"";
            $rowMsg = isset($row['support_msgs'])?$row['support_msgs']:"";
        }

        if ($rowdata != 'closed') {
            $msg = json_decode($rowMsg, true);
            $newMsg = array( 
                "id"               => (count($msg)+1),
                "msg_content"      => "{$commentText}",
                "msg_writer_email" => "{$emailID}",
                "user_type"        => "{$userType}",
                "msg_time"         => date('Y-m-d H:i:s'));
            if (!empty($msg)) {
                array_push( $msg, $newMsg);
            } else {
                $msg = array($newMsg);
            }
            $finalArray = json_encode($msg);                
            
            $query = "UPDATE support_requests SET support_msgs='" . $finalArray . "' WHERE ticket_id='" . $tickectId . "'";
            $result = runQuery($query, $conn);
            if (noError($result)) {
                $errMsg = "Message Added Successfully.";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
            } else {
                $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
            }
        } else {
            $errMsg = "Ticket Is Closed";
            $returnArr = setErrorStack($returnArr, 54, $errMsg, $extraArg);
        }
    }else {
        $errMsg = "Ticket Is Closed";
        $returnArr = setErrorStack($returnArr, 54, $errMsg, $extraArg);
    }
    return $returnArr;
}

function getCommentText($tickectId, $conn, $lastmsgid=""){
    $extraArg        = array();
    $returnArr       = array();
    $res3            = array();
    $res             = array();
    $newMessageArray = array();
    $arrayFound      = false;
    $arrayStart      = false;
    if($lastmsgid    == ""){
        $lastmsgid  = 1;
        $arrayStart = true;
    }
    $query = "SELECT support_msgs FROM support_requests where ticket_id ='".$tickectId ."'";
    $result = runQuery($query, $conn);
    if (noError($result)) {        
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $res2 = json_decode($res[0]['support_msgs'], true);        
        
        foreach ($res2 as $key => $value) {
            if($arrayStart) {
                $arrayFound = true;
            }
            if ($arrayFound == false) {
                if ($value['id'] == $lastmsgid) {
                    $arrayFound = true;
                    continue;
                }  
            }
            if ($arrayFound) {
                array_push($newMessageArray, $res2[$key]);  
            }            
        }
        $res3[] = $newMessageArray;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res3;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}

function getTicketDetails($email, $conn){
  $extraArg = array();
  $returnArr = array();
  $query = "SELECT * FROM support_requests where reg_email = '".$email ."'";
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
      $res[] = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }
  return $returnArr;
}

function getLastCount($email, $conn){
 $extraArg = array();
  $returnArr = array();
  $query = "SELECT max(id) FROM support_requests where reg_email = '".$email ."'  and request_status <> 'closed'" ;
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
      $res = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }
  return $returnArr;

}

function getTicketDetailsByTicketId($id, $conn){
    $extraArg = array();
    $returnArr = array();
    $query = "SELECT * FROM support_requests where id = '".$id ."' " ;
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}

function getRequestTicketDetails($email,$conn){
    $extraArg = array();
    $returnArr = array();
    $query = "select * from support_requests where reg_email = '$email' and  request_status <> 'closed' order by id DESC LIMIT 0, 1" ;
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}

//add Contackl form
function setContactForm($name, $email,$message, $conn){
    $extraArg = array();
    $returnArr = array();
    $query = "INSERT INTO contact(name, email, message) value('$name','$email','$message')";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "Message Added Contact Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }
    return $returnArr;
}

function selectmaxbuglist($conn){

    $returnArr = array();

    $query = "SELECT MAX(id) as lastid FROM bug_reports";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}
function getNextOrderNumbers($conn)
{
    $result=selectmaxbuglist($conn);
    $lastOrder=$result["errMsg"]["lastid"];
    $lastOrder = $lastOrder+1;
    return 'BG-' .$lastOrder;
}
function updateBugData($bug_id,$data,$conn)
{
    $bugID = getNextOrderNumbers($conn);
    $tableName   = "bug_reports";
    $requestDate = date('Y-m-d h:i:s');
    $returnArr=array();
    $extraArg=array();
    $getQueryListByID = getQueryListByID($conn, $bug_id);

    if (noError($getQueryListByID)) {
        if(isset($getQueryListByID["data"][0]))
        {
            $getQueryListByID = $getQueryListByID["data"][0];
            $bugid = $getQueryListByID["bug_id"];
        }
    }

    if(empty($bugid))
    {
        $query = "INSERT into " . $tableName . "(bug_id,bug_createdAt,bug_desc,bug_rasied_by,bug_status,created_by,imagePath) VALUES('$bugID','$requestDate','".$data["bug_desc"]."','".$data["email"]."','open','".$data["reportType"]."','".$data["imagePath"]."')";
    }
    else
    {
        $query = "UPDATE " . $tableName . " SET bug_status='closed' WHERE bug_id='" . $bug_id . "'";
    }

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $returnArr = setErrorStack($returnArr, -1, "Data Updated Success.", $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 56, null, $extraArg);
    }
    return $returnArr;
}

function getQueryListByID($conn, $bug_id) {

    $returnArr    = array();
    $extraArgs    = array();

    $query  = "SELECT * FROM `bug_reports` WHERE bug_id = '" . $bug_id . "'";
    // $query  = "SELECT * FROM `support_requests`";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[] = $row;
        }

        $extraArgs["data"] = $res;
        $errMsg    =   $result["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    } else {

        $errMsg   =   $result["errMsg"];
        $errCode  =   $result["errCode"];
        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }

    return $returnArr;
}
?>
