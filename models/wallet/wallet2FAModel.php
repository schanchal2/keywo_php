<?php

function setSecurityPreference($secretKey,$securityPreference,$email){
	global $walletPublicKey, $mode, $walletURL, $walletURLIPnotification;
  	global $descriptionArray;
  	$userId = $_SESSION['id'];
	$retArray = array();
	$headers = array();

	if (!empty($secretKey) && !empty($securityPreference) && !empty($email)) {

    
    $postFields = array( 
      "user_email" => $email,
      "gauth_key" => $secretKey,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "pref=2&user_email={$email}&user_id={$userId}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "user/{$userId}/securesettings/2";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}

function setbackupCodes($backupCodes,$securityPreference,$email){

    global $walletPublicKey, $mode, $walletURL, $walletURLIPnotification;
    global $descriptionArray;
    $userId = $_SESSION['id'];
    $retArray = array();
    $headers = array();

    if (!empty($backupCodes) && !empty($securityPreference) && !empty($email)) {

    
    $postFields = array( 
      "user_email" => $email,
      "gauth_backup_codes" => $backupCodes,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "pref=2&user_email={$email}&user_id={$userId}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "user/{$userId}/securesettings/2";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}

function getBackupCodes($email,$userId){
    $returnArr = array();
    global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;
    if(!empty($email) && !empty($userId)){
        $postFields = "user_email={$email}&publicKey=".urlencode($walletPublicKey);
        $apiText = "user_id={$userId}&user_email={$email}&publicKey={$walletPublicKey}";
        $apiName = "user/{$userId}/securesettings/2";
        $requestUrl = "{$walletURL}";

        $curl_type = 'GET';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        // nonce implementation
       if($mode == 'production'){
            $nonce_result = generateSignatureWithNonce($apiText);
            if(noError($nonce_result)){
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            }else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }
        // end nonce

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            /*$returnArr['errCode'] = -1;
            $returnArr['errMsg'] = $respArray['errMsg'];
            $returnArr['mode'] = $mode;*/

            $errMsg               =   $respArray["errMsg"];
            $errCode              =   $respArray["errCode"];
            $extraArgs["mode"]    =   $mode;
            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

        } else if ($respArray["errCode"] != 73) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $errMsg               =   $respArray["errMsg"];
              $errCode              =   $respArray["errCode"];
              $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
        } else {
              $errMsg               =   $respArray["errMsg"];
              $errCode              =   $respArray["errCode"];
              $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
        }
    }else{
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = "Mandatory feilds are missing";
    }
    return $returnArr;
}


function updateCodeState($email,$userId,$backup_code_id,$backup_code){
    $returnArr = array();
    global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    if(!empty($backup_code_id) && !empty($backup_code)){
        $postFields = "code_id={$backup_code_id}&bkup_code={$backup_code}&user_email={$email}&publicKey=".urlencode($walletPublicKey);
        $apiText = "user_email={$email}&user_id={$userId}&pref=2&publicKey={$walletPublicKey}";
        $apiName = "user/{$userId}/securesettings/2/verify";
        $requestUrl = "{$walletURL}";

        $curl_type = 'GET';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        // nonce implementation
       if($mode == 'production'){
            $nonce_result = generateSignatureWithNonce($apiText);
            if(noError($nonce_result)){
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            }else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }
        // end nonce

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            /*$returnArr['errCode'] = -1;
            $returnArr['errMsg'] = $respArray['errMsg'];
            $returnArr['mode'] = $mode;*/

            $errMsg               =   $respArray["errMsg"];
            $errCode              =   $respArray["errCode"];
            $extraArgs["mode"]    =   $mode;
            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

        } else if ($respArray["errCode"] != 73) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $errMsg               =   $respArray["errMsg"];
              $errCode              =   $respArray["errCode"];
              $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
        } else {
              $errMsg               =   $respArray["errMsg"];
              $errCode              =   $respArray["errCode"];
              $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
        }
    }else{
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = "Mandatory feilds are missing";
    }
    return $returnArr;
}

function setPasswordSecurityPreferene($email,$userId){
    $retArray = array();
    global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    if(!empty($email) && !empty($userId)){
        $postFields = array( 
          "user_email" => $email,
          "publicKey" =>  urlencode($walletPublicKey)
        );

        //create signature
        $apiText = "pref=1&user_email={$email}&user_id={$userId}&publicKey={$walletPublicKey}";
        $postFields = $postFields;
        $apiName = "user/{$userId}/securesettings/1";
        $requestUrl = "{$walletURL}";
        $curl_type = 'PUT';
        $content_type = "content-type: application/json";
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } 
    }else{
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = "Mandatory feilds are missing";
    }
    return $retArray;
}


function verifyPassword($paramArray)
{
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if (isset($paramArray['emailHandle']) && !empty($paramArray['emailHandle']) && isset($paramArray['password']) && !empty($paramArray['password'])) {

        $headers = array();

        //create signature
        $apiText = "username={$paramArray['emailHandle']}&password={$paramArray['password']}&publicKey={$walletPublicKey}";
        $postFields = "username=" . $paramArray['emailHandle'] . "&password=" . $paramArray['password'] . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = 'user/login';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}
?>