<?php

//to fetch shortforms of all currency codes 
function getAllCurrencies($conn){
	$returnArr = array();
	$currencyArr = array();
	$query = "SELECT id, shortforms FROM currency_value";
	$queryResult = runQuery($query,$conn);
	if(noError($queryResult)){
	    while($row = mysqli_fetch_assoc($queryResult["dbResource"])){
	        $currencyArr[] = $row;
	    }

		$returnArr['errCode'] = -1;
		$returnArr['errMsg'] = $currencyArr;	    

	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Failed to fetch all currencies";
	}

	return $returnArr;
}


/**
*   Function Name :   updateCurrencyPref()
*   Purpose       :   This function updates currency preference of user.
*   Arguments     :   (string)$userId, (string)$currCode 
*/
function updateCurrencyPref($userId,$currCode){

  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if (!empty($userId) && !empty($userId) && !empty($currCode)) {

    
    $postFields = array( 
      "user_id" => $userId,
      "currency_code" => $currCode,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "user_id={$userId}&currency_code={$currCode}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "user/{$userId}/currencypreference";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}
?>