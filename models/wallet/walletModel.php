<?php

/**
*   Function Name :   getAllTransctions()
*   Purpose       :   This function retrieve all transcations details.
*   Arguments     :   (string) $email, (string) $userRequiredFields
*/


function getUsersTransctionsHistory($email,$user_id,$staus,$sortBy,$from,$to,$search,$bkwd_cursor,$fwd_cursor,$offset_from,$deepFilter){
	
    global $communityPoolUser, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();
    $userRequiredFields = "batched_container";

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(isset($userRequiredFields) && !empty($userRequiredFields)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "user_id={$user_id}&user_email={$email}&publicKey={$walletPublicKey}";
                      $transaction_status = (empty($staus) || $staus=='null')?'':"&transaction_status={$staus}";
                      $sort_type = empty($sortBy)?'':"&sort_type={$sortBy}";
                      $deep_filter = "&deep_filter={$deepFilter}";
                      $date_jumpers_gt = empty($from)?'':"&date_jumpers_gt={$from}";
                      $date_jumpers_lt = empty($to)?'':"&date_jumpers_lt={$to}";
                      $bkwd_cursor = empty($bkwd_cursor)?'':"&bkwd_cursor={$bkwd_cursor}";
                      $fwd_cursor = empty($fwd_cursor)?'':"&fwd_cursor={$fwd_cursor}";
                      $offset_from = empty($offset_from)?'':"&offset_from={$offset_from}";
                      $search = urlencode($search);
                      $search = empty($search)?'':"&search={$search}";	

                      $postFields = "user_email=".$email.$transaction_status.$sort_type.$deep_filter.$date_jumpers_gt.$date_jumpers_lt.$search.$bkwd_cursor.$fwd_cursor.$offset_from."&publicKey=".urlencode($walletPublicKey);
                      

                      $apiName = "pocket/{$user_id}/transactions";
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);
                      //printArr($respArray);
                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}

/**
*   Function Name :   getMonthlyTransctionRecords()
*   Purpose       :   This function retrieve all transcations details.
*   Arguments     :   (string) $email, (string) $userRequiredFields
*/


function getMonthlyTransctionRecords($email,$user_id,$month,$year){
  
    global $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();
    $userRequiredFields = "batched_container";

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(!empty($email) && !empty($month) && !empty($year) && !empty($user_id)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                      $reportformonth = "{$month}_{$year}_transaction";

                      $apiText = "user_email={$email}&user_id={$user_id}&reportformonth={$reportformonth}&publicKey={$walletPublicKey}";
                      
                      $reportformonth = (empty($month) || empty($year))?'':"&reportformonth={$reportformonth}";
                      $postFields = "user_email=".$email.$reportformonth."&publicKey=".urlencode($walletPublicKey);

                      $apiName = "pocket/{$user_id}/transactions/report";
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);
                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}

function getUsersAllRequests($email,$user_id,$staus,$sortBy,$from,$to,$search,$bkwd_cursor,$fwd_cursor,$offset_from,$deepFilter){
    global $communityPoolUser, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();
    $userRequiredFields = "batched_container";

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(isset($userRequiredFields) && !empty($userRequiredFields)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "user_id={$user_id}&user_email={$email}&publicKey={$walletPublicKey}";
                      $transaction_status = (empty($staus) || $staus=='null')?'':"&req_status={$staus}";
                      $sort_type = empty($sortBy)?'':"&sort_type={$sortBy}";
                      $deep_filter = empty($deepFilter)?'':"&deep_filter={$deepFilter}";
                      $date_jumpers_gt = empty($from)?'':"&date_jumpers_gt={$from}";
                      $date_jumpers_lt = empty($to)?'':"&date_jumpers_lt={$to}";
                      //$bkwd_cursor = empty($bkwd_cursor)?'':"&bkwd_cursor={$bkwd_cursor}";
                      $fwd_cursor = empty($fwd_cursor)?'':"&fwd_cursor_offset_from={$fwd_cursor}";
                      $offset_from = empty($offset_from)?'':"&bkwd_cursor_offset_from={$offset_from}";
                      $search = urlencode($search);
                      $search = empty($search)?'':"&search={$search}";  

                      $postFields = "user_email=".$email.$transaction_status.$sort_type.$deep_filter.$date_jumpers_gt.$date_jumpers_lt.$search.$fwd_cursor.$offset_from."&publicKey=".urlencode($walletPublicKey);

                      $apiName = "pocket/{$user_id}/payment/request";
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);

                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr; 
}

/**
*   Function Name :   getTransctionsDetails()
*   Purpose       :   This function retrieve transcation details.
*   Arguments     :   (string) $email, (string) $userRequiredFields
*/


function getTransctionsDetails($email,$user_id,$transaction_id,$transaction_boundary){
    global $userRequiredFields, $mode, $walletPublicKey, $walletURL;
    $extraArgs = array();
    $returnArr = array();
    $headers = array();
    $userRequiredFields = "batched_container";

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(isset($userRequiredFields) && !empty($userRequiredFields)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "transaction_boundary={$transaction_boundary}&user_email={$email}&user_id={$user_id}&publicKey={$walletPublicKey}";

                      //$apiText = "user_email={$email}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
                      $transaction_id = empty($transaction_id)?'': $transaction_id ;
                      $transaction_boundary = empty($transaction_boundary)?'':"&transaction_boundary={$transaction_boundary}";
                      

                      $postFields = "user_email=".$email.$transaction_boundary."&fieldnames={$userRequiredFields}&publicKey=".urlencode($walletPublicKey);

                      //$postFields ="user_email=".$email."&transaction_status={$staus}&sort_type={$sortBy}&date_jumpers_gt={$from}&date_jumpers_lt={$to}&fieldnames={$userRequiredFields}&publicKey=".urlencode($walletPublicKey);
                      $apiName = "pocket/{$user_id}/transactions/{$transaction_id}";
                     
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);

                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}

/**
*   Function Name :   getTransctionsDetails()
*   Purpose       :   This function retrieve transcation details.
*   Arguments     :   (string) $email, (string) $userRequiredFields
*/


function getRequestDetails($email,$user_id,$transaction_id,$transaction_boundary){
    global $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $extraArgs = array();
    $headers = array();
    $userRequiredFields = "batched_container";

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(isset($userRequiredFields) && !empty($userRequiredFields)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                     
                      $apiText = "user_id={$user_id}&user_email={$email}&req_id={$transaction_id}&publicKey={$walletPublicKey}";

                      //$apiText = "user_email={$email}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
                      $transaction_id = empty($transaction_id)?'': $transaction_id ;
                      $transaction_boundary = empty($transaction_boundary)?'':"&transaction_boundary={$transaction_boundary}";
                      

                      $postFields = "user_email=".$email.$transaction_boundary."&fieldnames={$userRequiredFields}&publicKey=".urlencode($walletPublicKey);

                      //$postFields ="user_email=".$email."&transaction_status={$staus}&sort_type={$sortBy}&date_jumpers_gt={$from}&date_jumpers_lt={$to}&fieldnames={$userRequiredFields}&publicKey=".urlencode($walletPublicKey);
                      $apiName = "pocket/{$user_id}/payment/request/{$transaction_id}";
                     
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);

                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}


/**
*   Function Name :   sendMoneyByWallet()
*   Purpose       :   This function sent money to reciver.
*   Arguments     :   (array) $senderDetails, (array) $receiverDetails, (string) $clientIP, (number) $amount, (array) $currExchangeRates, (string) $note
*/

function sendMoneyByWallet($senderDetails,$receiverDetails,$clientIP,$amount,$currExchangeRates,$note,$metaData){
  
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if (!empty($senderDetails) && !empty($receiverDetails) && !empty($currExchangeRates) && isset($clientIP) && $amount) {

    $senderEmail = $senderDetails['email'];
    $senderId = $senderDetails['_id'];
    $senderHandle = $senderDetails['account_handle'];

    $receiverEmail = $receiverDetails['email'];
    $receiverId = $receiverDetails['_id'];
    $receiverHandle = $receiverDetails['account_handle'];

    $exchangeRateBTC = $currExchangeRates['btc'];
    $exchangeRateUSD = $currExchangeRates['usd'];
    $exchangeRateSGD = $currExchangeRates['sgd'];

    $senderDesc = sprintf($descriptionArray['sender_desc'], $amount, 'IT$', $receiverDetails['first_name']);
    $receiverDesc = sprintf($descriptionArray['reciver_desc'], $amount, 'IT$', $senderDetails['first_name']);
    $payment_mode = 'bitcoin';
    $noteArr = array();
    $metaData['note'] = utf8_encode($note);
    
    $postFields = array( 
      "sender_email" => $senderEmail,
      "sender_handle" => $senderHandle,
      "recipient_email" => $receiverEmail,
      "recipient_user_id" => $receiverId,
      "recipient_handle" => $receiverHandle,
      "payment_mode" => $payment_mode,
      "origin_ip" => $clientIP,
      "meta_details" => json_encode($metaData),
      "sent_desc" => urlencode(utf8_encode($senderDesc)),
      "usd" => $exchangeRateUSD,
      "sgd" => $exchangeRateSGD,
      "btc" => $exchangeRateBTC,
      "sender_amount" => $amount,
      "received_desc" => urlencode(utf8_encode($receiverDesc)),
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "receiver_email={$receiverEmail}&user_id={$senderId}&sender_email={$senderEmail}&origin_ip={$clientIP}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "pocket/{$senderId}/payment/send";
    $requestUrl = "{$walletURL}";
    $curl_type = 'POST';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}

/**
*   Function Name :   sendITDRequestByWallet()
*   Purpose       :   This function sent ITD request to other keywo user.
*   Arguments     :   (array) $senderDetails, (array) $receiverDetails, (string) $clientIP, (number) $amount, (array) $currExchangeRates, (string) $note
*/
function sendITDRequestByWallet($senderDetails,$receiverDetails,$clientIP,$amount,$currExchangeRates,$note,$metaData){

  
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if (!empty($senderDetails) && !empty($receiverDetails) && !empty($currExchangeRates) && isset($clientIP) && $amount) {

    $senderEmail = $senderDetails['email'];
    $senderId = $senderDetails['_id'];
    $senderHandle = $senderDetails['account_handle'];

    $receiverEmail = $receiverDetails['email'];
    $receiverId = $receiverDetails['_id'];
    $receiverHandle = $receiverDetails['account_handle'];

    $exchangeRateBTC = $currExchangeRates['btc'];
    $exchangeRateUSD = $currExchangeRates['usd'];
    $exchangeRateSGD = $currExchangeRates['sgd'];

    $senderDesc = sprintf($descriptionArray['req_sender_desc'], $amount, 'IT$', $receiverDetails['first_name']);
    $receiverDesc = sprintf($descriptionArray['req_receiver_desc'], $amount, 'IT$', $senderDetails['first_name']);
    $payment_mode = 'bitcoin';
    $metaData['note'] = utf8_encode($note);

    $postFields = array( 
      "sender_email" => $senderEmail,
      "sender_handle" => $senderHandle,
      "recipient_email" => $receiverEmail,
      "recipient_user_id" => $receiverId,
      "recipient_handle" => $receiverHandle,
      "payment_mode" => $payment_mode,
      "origin_ip" => $clientIP,
      "meta_details" => json_encode($metaData),
      "sent_desc" => urlencode(utf8_encode($senderDesc)),
      "usd" => $exchangeRateUSD,
      "sgd" => $exchangeRateSGD,
      "btc" => $exchangeRateBTC,
      "sender_amount" => $amount,
      "received_desc" => urlencode(utf8_encode($receiverDesc)),
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
     $apiText = "receiver_email={$receiverEmail}&user_id={$senderId}&sender_email={$senderEmail}&origin_ip={$clientIP}&publicKey={$walletPublicKey}";

    $postFields = $postFields;
    $apiName = "pocket/{$senderId}/payment/request";
    $requestUrl = "{$walletURL}";
    $curl_type = 'POST';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}

/**
*   Function Name :   acceptITDRequest()
*   Purpose       :   This function accept IT$ request.
*   Arguments     :   (array) $senderDetails, (array) $receiverDetails, (string) $transactionBoundary, (number) $amount, (string) $note
*/

function acceptITDRequest($senderDetails,$receiverDetails,$transactionId,$note,$metaData){

  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if (!empty($senderDetails) && !empty($receiverDetails) && !empty($transactionId)) {

    $senderEmail = $senderDetails['email'];
    $senderId = $senderDetails['_id'];
    $senderHandle = $senderDetails['account_handle'];
    $receiverEmail = $receiverDetails['email'];
    $receiverId = $receiverDetails['_id'];
    $receiverHandle = $receiverDetails['account_handle'];


    $postFields = array( 
      "payment_request_id" => $transactionId,
      "payee_user_id" => $receiverId,
      "payee_email" => $receiverEmail,
      "payee_handle" => $receiverHandle,
      "payer_email" => $senderEmail,
      "payer_handle" => $senderHandle,
      "reason" => utf8_encode($note),
      "meta_details" => json_encode($metaData),
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "payee_email={$receiverEmail}&payee_user_id={$receiverId}&action=accept&user_id={$senderId}&payment_request_id={$transactionId}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "pocket/{$senderId}/payment/request/accept";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}


/**
*   Function Name :   regectITDRequest()
*   Purpose       :   This function reject IT$ request.
*   Arguments     :   (array) $senderDetails, (array) $receiverDetails, (string) $transactionBoundary, (number) $amount, (string) $note
*/

function rejectITDRequest($senderDetails,$receiverDetails,$transactionId,$note){

  
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if (!empty($senderDetails) && !empty($receiverDetails) && !empty($transactionId)) {

    $senderEmail = $senderDetails['email'];
    $senderId = $senderDetails['_id'];
    $senderHandle = $senderDetails['account_handle'];
    $receiverEmail = $receiverDetails['email'];
    $receiverId = $receiverDetails['_id'];
    $receiverHandle = $receiverDetails['account_handle'];


    $postFields = array( 
      "payment_request_id" => $transactionId,
      "payee_user_id" => $receiverId,
      "payee_email" => $receiverEmail,
      "payee_handle" => $receiverHandle,
      "payer_email" => $senderEmail,
      "payer_handle" => $senderHandle,
      "reject_reason" => utf8_encode($note),
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "payee_email={$receiverEmail}&payee_user_id={$receiverId}&action=reject&user_id={$senderId}&payment_request_id={$transactionId}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "pocket/{$senderId}/payment/request/reject";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}



/*
 *Function  : sendIPAuthVrifcationEmail().
 *purpose :   To send bitgo/paypal verification email and to store auth email details in database.
 *Arguments :   (string)$email,(string)$userIp.
*/
function sendVrifcationEmail($email, $userIp, $deviceType, $currentBrowser,$resend){

  global $kwdsServername;
  global $kwdsDBname;
  global $kwdsUsername;
  global $kwdsPassword;
  global $presaleRootURL;
  global $browserAgent;
  if(empty($browserAgent) || $browserAgent ==''){
    $browserAgent = $currentBrowser;
  }

  $kwdsConn = createDbConnection($kwdsServername, $kwdsUsername, $kwdsPassword, $kwdsDBname);
  if(noError($kwdsConn)) {
    $kwdsConn = $kwdsConn["errMsg"];
    $type = "ip_verif_email";
    $mode = "email";
    $payload = $userIp;
    $status = "unverified";
    $currentTime =  date("Y-m-d H:i:s");
    $date = strtotime($currentTime) + 3600;
    $expirytime = date("Y-m-d H:i:s",$date);
    $ipAuthToken = sha1($email."~~".$userIp."~~".$currentTime);
    $checkAuthEmail = checkAuthIPEmail($email,$userIp,$kwdsConn);

    if(noError($checkAuthEmail) || $resend == 1) {
      $emailDetails = insertauthEmailDetails($kwdsConn, $email, $payload, $ipAuthToken, $type, $mode, $status);

      if ($emailDetails["errCode"] == -1) {
        $authURL = $presaleRootURL . "views/verifyUserIP.php";
        $successFlag = 1;
        $getAuthEmailBody = getIPAuthEmailBody($browserAgent, $userIp, $deviceType, $authURL, $currentTime, $ipAuthToken, $email, $successFlag);

        // email sending code here
        $to = $email;
        $subject = "SearchTrade: IP Verification";
        $emailBody = $getAuthEmailBody;
        $permissionCode = 2;
        $category = "admin_support";
        $linkStatus = 1;
        $sendIPAuthoriseMail = sendNotificationBuyPrefrence($to, $subject, $emailBody, $first_name, $last_name, $id, $smsText, $mobileNumber, $message, $permissionCode, $category, $linkStatus);
        if (noError($sendIPAuthoriseMail)) {
          $retArray["errCode"][-1] = -1;
          $retArray["errMsg"] = "Successfully send authorisation email";
        } else {
          $retArray["errCode"][2] = 2;
          $retArray["errMsg"] = "Failed to send authorisation email";
        }
      } else {
        $retArray["errCode"][2] = 2;
        $retArray["errMsg"] = $emailDetails["errMsg"];
      }
    }else{
      $retArray["errCode"][5] = 5;
      $retArray["errMsg"] = $checkAuthEmail["errMsg"];
    }
  }else{
    print("Database Error");
    exit();
  }

  return $retArray;
}


/*
 *Function : checkAuthIPEmail().
 *purpose :  To check email already sent or not.
 *Arguments :  (string)$email,(string)$conn.
*/
function checkAuthEmail($email,$payload,$type,$conn){
  $returnArr = array();
  $query = "SELECT auth_token from verification_notifications WHERE email='".$email."' AND payload='".$payload."' AND type='".$type."' AND send_timestamp >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND status='unverified'";

  $queryResult = runQuery($query,$conn);
  $numRows = mysqli_num_rows($queryResult["dbResource"]);
  if($numRows > 0){
    $returnArr["errMsg"] = "Authorization email already sent.";
    $returnArr["errCode"] = 2;
  }else{
    $returnArr["errMsg"] = "Authorization email not sent.";
    $returnArr["errCode"] = -1;
  }
  return $returnArr;
}

function deactivePreviousMail($email,$payload,$type,$conn){
  $returnArr = array();
  $query = "DELETE FROM verification_notifications WHERE email='".$email."' AND type='".$type."'";

  $queryResult = runQuery($query,$conn);
  if(noError($queryResult)){
	$returnArr["errMsg"] = "Deactivated previous mailes.";
    $returnArr["errCode"] = -1;
  }else{
	$returnArr["errMsg"] = "Error in deactivating previous mailes.";
    $returnArr["errCode"] = 2;
  }
  return $returnArr;	
}

/*
 *Function : insertauthEmailDetails().
 *purpose :  To store auth email details in database.
 *Arguments :  (string)$email,(string)$payload,(string)$ipAuthToken,(timestamp)$currentTime,(timestamp)$expirytime,(string)$type,(string)$mode,(string)$status,(string)$kwdsConn.
*/

function insertauthEmailDetails($conn,$email,$payload,$authToken,$type,$mode,$status){

    $query = "INSERT INTO verification_notifications VALUES('$authToken','$email','$type','$mode','$payload',NOW(),NOW()+INTERVAL 1 HOUR,'$status')";

    $queryResult = runQuery($query,$conn);
    if(noError($queryResult)){
        $retArray["errCode"] = -1;
        $retArray["errMsg"]  = "Successfully inserted verification notification";
    }else{
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "Error in inserting verification notifications";
    }
    return $retArray;
}

/*
 *Function : verifyAuthToken().
 *purpose :  To verify usersAuthtoken which is in verification email.
 *Arguments :  (string)$email,(string)$auth,(string)$payload,(string)$type,(string)$mode,(conn)$kwdsConn.
*/

function verifyAuthToken($conn,$email,$auth,$payload,$type,$mode){ 
    if(!empty($payload)){
      $query = "DELETE FROM verification_notifications WHERE auth_token='".$auth."' AND email='".$email."' AND payload ='".$payload."' AND send_timestamp >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND type='".$type."' AND mode='".$mode."' AND status='unverified'";
    }else{
      $query = "DELETE FROM verification_notifications WHERE auth_token='".$auth."' AND email='".$email."' AND send_timestamp >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND type='".$type."' AND mode='".$mode."' AND status='unverified'";
    }

    $queryResult = runQuery($query,$conn);
    $numRows = mysqli_affected_rows($conn);
    if($numRows > 0){
        $returnArr["errMsg"]= "Success!!!";
        $returnArr["errCode"]= -1;
    }else{
        $returnArr["errMsg"]= "The email verification link has expired";
        $returnArr["errCode"]= 2;
    }
    return $returnArr;
}


/*
 *Function : getPayload().
 *purpose :  To verify usersAuthtoken which is in verification email.
 *Arguments :  (string)$email,(string)$auth,(string)$payload,(string)$type,(string)$mode,(conn)$kwdsConn.
*/

function getPayload($conn,$email,$auth,$payload,$type,$mode){ 
    if(!empty($payload)){
      $query = "SELECT * FROM verification_notifications WHERE auth_token='".$auth."' AND email='".$email."' AND payload ='".$payload."' AND send_timestamp >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND type='".$type."' AND mode='".$mode."' AND status='unverified'";
    }else{
      $query = "SELECT * FROM verification_notifications WHERE auth_token='".$auth."' AND email='".$email."' AND send_timestamp >= DATE_SUB(NOW(), INTERVAL 1 HOUR) AND type='".$type."' AND mode='".$mode."' AND status='unverified'";
    }
    
    $queryResult = runQuery($query,$conn);

    $row = mysqli_fetch_array($queryResult['dbResource']);
    if(!empty($row)){
      $returnArr['payload'] = $row;
      $returnArr["errMsg"]= "Success!!!";
      $returnArr["errCode"]= -1;
    }else{
      $returnArr["errMsg"]= "The email verification link has expired";
      $returnArr["errCode"]= 2;
    }
    /*$numRows = mysqli_affected_rows($conn);
    if($numRows > 0){
        $returnArr["errMsg"]= "Success!!!";
        $returnArr["errCode"]= -1;
    }else{
        $returnArr["errMsg"]= "The email verification link has expired";
        $returnArr["errCode"]= 2;
    }*/
    return $returnArr;
}



function setTransactionPin($trans_pin){
  $email = $_SESSION['email'];
  $user_id = $_SESSION['id'];
  global $walletURLIP; 
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  if(!empty($email) && !empty($user_id) && !empty($trans_pin)){
    $postFields = array( 
      "user_email" => $email,
      "transact_pin" => $trans_pin,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "pref=3&user_email={$email}&user_id={$user_id}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "user/{$user_id}/securesettings/3";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }
  }else{
    $retArray['errMsg'] = "Mandatory feilds not found";
    $retArray['errCode'] = 2;    
  }
  return $retArray;
}

function verifyTransactionPin($trans_pin){

  global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;
  $email = $_SESSION['email'];
  $userId = $_SESSION['id'];
  $returnArr = array();
  if(isset($email) && !empty($email) && isset($trans_pin) && !empty($trans_pin) && isset($userId) && !empty($userId)){
    $postFields = "pin_to_verify={$trans_pin}&user_email={$email}&publicKey=".urlencode($walletPublicKey);
    $apiText = "user_email={$email}&user_id={$userId}&pref=3&publicKey={$walletPublicKey}";
    $apiName = "user/{$userId}/securesettings/3/verify";
    $requestUrl = "{$walletURL}";

    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    // nonce implementation
   if($mode == 'production'){
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)){
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        }else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }
    // end nonce

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        /*$returnArr['errCode'] = -1;
        $returnArr['errMsg'] = $respArray['errMsg'];
        $returnArr['mode'] = $mode;*/

        $errMsg               =   $respArray["errMsg"];
        $errCode              =   $respArray["errCode"];
        $extraArgs["mode"]    =   $mode;
        $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

    } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    } else {
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    }
  }else{
    $returnArr['errMsg'] = "Mandatory feilds not found";
    $returnArr['errCode'] = 2;    
  }

  return $returnArr;
}

/**
* Function: setWalletAddress();
* Purpose: To set wallet bitcoin or paypal email address.
* Arguments: (string)email,(string)payload,(string)addressMode
*/
function setWalletAddress($email,$payload,$addressMode){
  $retArray = array();
  global $walletURLIP; 
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;

  if(!empty($email) && !empty($payload) && !empty($addressMode)){
      $userRequiredFields = $userRequiredFields.",email,account_handle";
        //fetching user details                 
        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
        if(noError($getUserDetails)){
            $getUserDetails = $getUserDetails['errMsg'];
            $accountHandle = $getUserDetails['account_handle'];
            $userId = $getUserDetails['_id'];
            if($addressMode == "paypal"){
              $postFields = array( 
                "user_email" => $email,
                "paypal_address" => $payload,
                "publicKey" =>  urlencode($walletPublicKey)
              );

              //create signature
              $apiText = "user_id={$userId}&address_type=paypal&user_email={$email}&publicKey={$walletPublicKey}";
              $postFields = $postFields;
              $apiName = "pocket/{$userId}/cashout/settings/paypal";
              $requestUrl = "{$walletURL}";
              $curl_type = 'POST';
              $content_type = "content-type: application/json";
              $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

              if ($mode == 'production') {
                  $nonce_result = generateSignatureWithNonce($apiText);
                  if (noError($nonce_result)) {
                      $nonce_result = $nonce_result['errMsg'];
                      $retArray['errCode'] = -1;
                      //$retArray['errMsg'] = $nonce_result['errMsg'];
                      $headers[] = "x-ts: {$nonce_result['timestamp']}";
                      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                  } else {
                      $retArray['errCode'] = 2;
                      $retArray['errMsg'] = $nonce_result['errMsg'];
                  }
              }
            }else{
              $postFields = array( 
                "user_email" => $email,
                "blockchain_address" => $payload,
                "publicKey" =>  urlencode($walletPublicKey)
              );

              //create signature
              $apiText = "user_id={$userId}&address_type=blockchain&user_email={$email}&publicKey={$walletPublicKey}";
              $postFields = $postFields;
              $apiName = "pocket/{$userId}/cashout/settings/blockchain";
              $requestUrl = "{$walletURL}";
              $curl_type = 'POST';
              $content_type = "content-type: application/json";
              $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

              if ($mode == 'production') {
                  $nonce_result = generateSignatureWithNonce($apiText);
                  if (noError($nonce_result)) {
                      $nonce_result = $nonce_result['errMsg'];
                      $retArray['errCode'] = -1;
                      //$retArray['errMsg'] = $nonce_result['errMsg'];
                      $headers[] = "x-ts: {$nonce_result['timestamp']}";
                      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                  } else {
                      $retArray['errCode'] = 2;
                      $retArray['errMsg'] = $nonce_result['errMsg'];
                  }
              }
            }
          $respArray = curlRequest($curlReqParam, $headers);
          if (noError($respArray)) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $retArray['errCode'] = -1;
              $retArray['errMsg'] = $respArray['errMsg'];
              $retArray['mode'] = $mode;

          } else if ($respArray["errCode"] != 73) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $retArray['errCode'] = $respArray['errCode'];
              $retArray['errMsg'] = $respArray['errMsg'];
          } else {
              $retArray['errCode'] = $respArray['errCode'];
              $retArray['errMsg'] = $respArray['errMsg'];
          }
        }else{
          $retArray['errMsg'] = "Failed to setup wallet cashout address.";
          $retArray['errCode'] = 2;  
        }
  }else{
    $retArray['errMsg'] = "Failed to setup wallet cashout address.";
    $retArray['errCode'] = 2;
  }

  return $retArray;
}


/**
* Function: updatedWalletAddress();
* Purpose: To updated wallet bitcoin or paypal email address.
* Arguments: (string)email,(string)payload,(string)addressMode, (string)cashoutId
*/
function updatedWalletAddress($email,$payload,$addressMode,$cashoutId){
  $retArray = array();
  global $walletURLIP; 
  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;

  if(!empty($email) && !empty($payload) && !empty($addressMode)){
      $userRequiredFields = $userRequiredFields.",email,account_handle";
        //fetching user details                 
        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
        if(noError($getUserDetails)){
            $getUserDetails = $getUserDetails['errMsg'];
            $accountHandle = $getUserDetails['account_handle'];
            $userId = $getUserDetails['_id'];
            if($addressMode == "paypal"){
              $postFields = array( 
                "user_email" => $email,
                "paypal_address" => $payload,
                "publicKey" =>  urlencode($walletPublicKey)
              );

              //create signature
              $apiText = "address_type=paypal&user_email={$email}&user_id={$userId}&publicKey={$walletPublicKey}";
              $postFields = $postFields;
              $apiName = "pocket/{$userId}/cashout/{$cashoutId}/settings/paypal";
              $requestUrl = "{$walletURL}";
              $curl_type = 'PUT';
              $content_type = "content-type: application/json";
              $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

              if ($mode == 'production') {
                  $nonce_result = generateSignatureWithNonce($apiText);
                  if (noError($nonce_result)) {
                      $nonce_result = $nonce_result['errMsg'];
                      $retArray['errCode'] = -1;
                      //$retArray['errMsg'] = $nonce_result['errMsg'];
                      $headers[] = "x-ts: {$nonce_result['timestamp']}";
                      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                  } else {
                      $retArray['errCode'] = 2;
                      $retArray['errMsg'] = $nonce_result['errMsg'];
                  }
              }
            }else{
              $postFields = array( 
                "user_email" => $email,
                "blockchain_address" => $payload,
                "publicKey" =>  urlencode($walletPublicKey)
              );

              //create signature
              $apiText = "address_type=blockchain&user_email={$email}&user_id={$userId}&publicKey={$walletPublicKey}";
              $postFields = $postFields;
              $apiName = "pocket/{$userId}/cashout/{$cashoutId}/settings/blockchain";
              $requestUrl = "{$walletURL}";
              $curl_type = 'PUT';
              $content_type = "content-type: application/json";
              $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

              if ($mode == 'production') {
                  $nonce_result = generateSignatureWithNonce($apiText);
                  if (noError($nonce_result)) {
                      $nonce_result = $nonce_result['errMsg'];
                      $retArray['errCode'] = -1;
                      //$retArray['errMsg'] = $nonce_result['errMsg'];
                      $headers[] = "x-ts: {$nonce_result['timestamp']}";
                      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                  } else {
                      $retArray['errCode'] = 2;
                      $retArray['errMsg'] = $nonce_result['errMsg'];
                  }
              }
            }
          $respArray = curlRequest($curlReqParam, $headers);
          if (noError($respArray)) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $retArray['errCode'] = -1;
              $retArray['errMsg'] = $respArray['errMsg'];
              $retArray['mode'] = $mode;

          } else if ($respArray["errCode"] != 73) {
              if ($mode == 'production') {
                  $nonce_counter = incrementVectorCounter();
              }
              $retArray['errCode'] = $respArray['errCode'];
              $retArray['errMsg'] = $respArray['errMsg'];
          } else {
              $retArray['errCode'] = $respArray['errCode'];
              $retArray['errMsg'] = $respArray['errMsg'];
          }
        }else{
          $retArray['errMsg'] = "Failed to setup wallet cashout address.";
          $retArray['errCode'] = 2;  
        }
  }else{
    $retArray['errMsg'] = "Failed to setup wallet cashout address.";
    $retArray['errCode'] = 2;
  }

  return $retArray;
}


/**
* Function: setWalletAddress();
* Purpose: To set wallet bitcoin or paypal email address.
* Arguments: (string)email,(string)payload,(string)addressMode
*/
function getWalletAddress($email){

  global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

  $returnArr = array();
  $headers = array();

  $userRequiredFields = $userRequiredFields.",email,account_handle";
  //fetching user details                 
  $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);

  if(noError($getUserDetails)){
    $getUserDetails = $getUserDetails['errMsg'];
    $userId = $getUserDetails['_id'];
    $postFields = "publicKey=".urlencode($walletPublicKey);
    $apiText = "user_id={$userId}&publicKey={$walletPublicKey}";
    $apiName = "pocket/{$userId}/cashoutsettings";
    $requestUrl = "{$walletURL}";

    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    // nonce implementation
   if($mode == 'production'){
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)){
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        }else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }
    // end nonce

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        /*$returnArr['errCode'] = -1;
        $returnArr['errMsg'] = $respArray['errMsg'];
        $returnArr['mode'] = $mode;*/

        $errMsg               =   $respArray["errMsg"];
        $errCode              =   $respArray["errCode"];
        $extraArgs["mode"]    =   $mode;
        $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

    } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    } else {
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    }

  }else{
    $errMsg               =   "Error in fetching user details";
    $errCode              =   12;
    $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

  } 

  return $returnArr;
  
}


/**
*   Function Name :   getAccountTotalCount()
*   Purpose       :   This function retrieve all transcations details.
*   Arguments     :   (string) $email, (string) $userRequiredFields
*/


function getAccountTotalCount($email,$user_id){
  
    global  $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(!empty($email) && !empty($user_id)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "user_email={$email}&user_id={$user_id}&publicKey={$walletPublicKey}"; 

                      $postFields = "user_email=".$email."&publicKey=".urlencode($walletPublicKey);
                      

                      $apiName = "pocket/{$user_id}/kyc/calls";
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);
                      //printArr($respArray);
                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}

function getDailyCount($email,$user_id,$estimates){
  
    global  $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(!empty($email) && !empty($user_id)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "user_email={$email}&estimates={$estimates}&user_id={$user_id}&publicKey={$walletPublicKey}"; 

                      $postFields = "user_email=".$email."&publicKey=".urlencode($walletPublicKey);
                      

                      $apiName = "pocket/{$user_id}/kyc/{$estimates}";
                      $requestUrl = "{$walletURL}";

                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);
                      //printArr($respArray);
                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
   

    return $returnArr;
}

function checkSendLimit($kycStablDetails,$kyc_current_level,$amountInITD,$email,$user_id){ 
  $returnArr = array();
  $kyc_level = str_replace("kyc_", "", $kyc_current_level);
  global $rootUrl;

  $kycYearlyCount = getDailyCount($email,$user_id,'yearly');
  $kycDailyCount = getDailyCount($email,$user_id,'daily');

  if(noError($kycYearlyCount) && noError($kycDailyCount)){
    $dailySendCount = $kycDailyCount['errMsg']['sends'];
    $yearlySendCount = $kycYearlyCount['errMsg']['sends'];
    $yearlyIncomeCount = $kycYearlyCount['errMsg']['receives'];

    $dailySendLimit = $kycStablDetails[$kyc_level-1]['send_fund_limit'];
    $yearlySendLimit = $kycStablDetails[$kyc_level-1]['yearly_send_fund_limit'];
    $yearlyIncomeLimit = $kycStablDetails[$kyc_level-1]['yearly_income_limit'];

    if(($yearlySendCount+$amountInITD) <= $yearlySendLimit){
        if(($dailySendCount+$amountInITD) <= $dailySendLimit){
          if($yearlyIncomeCount <= $yearlyIncomeLimit){
            $returnArr['errCode'] = -1;
            $returnArr['errMsg'] = "Successfully checked send ITD limits";
          }else{
            $returnArr['errCode'] = 4;
            $returnArr['errMsg'] = 'You have exceeded yearly incoming ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
          }
        }else{
          $returnArr['errCode'] = 3;
          $returnArr['errMsg'] = 'You have exceeded daily send ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
        }
    }else{
      $returnArr['errCode'] = 2;
      $returnArr['errMsg'] = 'You have exceeded yearly send ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
    }


  }else{
    $returnArr['errCode'] = 4;
    $returnArr['errMsg'] = "Failed to check send ITD limits";

  }

  return $returnArr;
}

function checkCashoutLimit($kycStablDetails,$kyc_current_level,$amountInITD,$email,$user_id){
  // printArr(func_get_args());  die;
  $returnArr = array();
  $kyc_level = str_replace("kyc_", "", $kyc_current_level);
  global $rootUrl;

  $kycYearlyCount = getDailyCount($email,$user_id,'yearly');
  $kycDailyCount = getDailyCount($email,$user_id,'daily');

  if(noError($kycYearlyCount) && noError($kycDailyCount)){
    $dailyCashoutCount = $kycDailyCount['errMsg']['cashouts'];
    $yearlyCashoutCount = $kycYearlyCount['errMsg']['cashouts'];
    $yearlyIncomeCount = $kycYearlyCount['errMsg']['receives'];

    $dailyCashoutLimit = $kycStablDetails[$kyc_level-1]['withdrawal_limit'];
    $yearlyCashoutLimit = $kycStablDetails[$kyc_level-1]['yearly_withdrawal_limit'];
    $yearlyIncomeLimit = $kycStablDetails[$kyc_level-1]['yearly_income_limit'];

    if(($yearlyCashoutCount+$amountInITD) <= $yearlyCashoutLimit){
        if(($dailyCashoutCount+$amountInITD) <= $dailyCashoutLimit){
          if($yearlyIncomeCount <= $yearlyIncomeLimit){
            $returnArr['errCode'] = -1;
            $returnArr['errMsg'] = "Successfully checked cashout ITD limits";
          }else{
            $returnArr['errCode'] = 4;
            $returnArr['errMsg'] = 'You have exceeded yearly incoming ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
          }
        }else{
          $returnArr['errCode'] = 3;
          $returnArr['errMsg'] = 'You have exceeded daily cashout ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
        }
    }else{
      $returnArr['errCode'] = 2;
      $returnArr['errMsg'] = 'You have exceeded yearly cashout ITD limit,<a href="'.$rootUrl.'views/wallet/accountLevelSettings.php">Click here</a> to upgrade your KYC level';
    }


  }else{
    $returnArr['errCode'] = 4;
    $returnArr['errMsg'] = "Failed to check send ITD limits";

  }

  return $returnArr;
}

/**
* Function: checkforDuplicateAddress();
* Purpose: To to check duplicate address address.
* Arguments: (string)cashoutMode,(string)wallet_address
*/
function checkforDuplicateAddress($cashoutMode,$wallet_address){

  global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

  $email = $_SESSION['email'];
  $userId = $_SESSION['id'];
  $wallet_address = trim($wallet_address);
  $returnArr = array();
  $headers = array();

  if(!empty($cashoutMode) && !empty($wallet_address)){
    if($cashoutMode == 'paypal_email'){
      $postFields = "paypal_address={$wallet_address}&publicKey=".urlencode($walletPublicKey);
      $apiText = "payment_mode=paypal&user_id={$userId}&publicKey={$walletPublicKey}";
      $apiName = "pocket/{$userId}/cashoutsettings/verify/paypal";
    }else{
      $postFields = "blockchain_address={$wallet_address}&publicKey=".urlencode($walletPublicKey);
      $apiText = "payment_mode=blockchain&user_id={$userId}&publicKey={$walletPublicKey}";
      $apiName = "pocket/{$userId}/cashoutsettings/verify/blockchain";
    }
    
    
    $requestUrl = "{$walletURL}";

    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
    // nonce implementation
   if($mode == 'production'){
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)){
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        }else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }
    // end nonce

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        /*$returnArr['errCode'] = -1;
        $returnArr['errMsg'] = $respArray['errMsg'];
        $returnArr['mode'] = $mode;*/

        $errMsg               =   $respArray["errMsg"];
        $errCode              =   $respArray["errCode"];
        $extraArgs["mode"]    =   $mode;
        $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

    } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    } else {
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    }

  }else{
    $errMsg               =   "Mandatory feilds not found";
    $errCode              =   12;
    $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

  } 

  return $returnArr;
  
}

?>