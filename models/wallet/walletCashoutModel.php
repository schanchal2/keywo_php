<?php

/**
*   Function Name :   processRequest()
*   Purpose       :   This function processes cashout requests.
*   Arguments     :   (object) $conn, (array)$request, (array)$returnArr, (integer)$j
*/
function processRequest($conn,$request,$returnArr,$j,$withdrawalFees,$minCashoutAmt,$minManualCashoutAmt){

	$currentTable = "queue_processor_".date("m_Y",strtotime($request['request_date']));
	$requestId = $request['id'];
	$i = 1;
	global $walletURLIP,$userRequiredFields;
    $extraArgs=array();
	$currencyRates = json_decode($request['currency_rates'],true);
	$coversionRates = $currencyRates['usd'] / $currencyRates['btc'];
	$amountinBTC = ($request['payment_amount'] + $request['cashout_fees'])/ $coversionRates;
	$amountInITD = $request['payment_amount'] + $request['cashout_fees'];
	$currentTime = date("Y-m-d H:i:s");
	$query = "UPDATE {$currentTable} SET status='processing',request_update_date='{$currentTime}',comment='started request processing' WHERE id={$requestId}";
	$queryResult = runQuery($query, $conn);
	if($queryResult){
		$msg = "Start processing cashout request {$j}";
		$xmlChild['requestStep'.$i] = $i.". {$msg}";


		//check whether amount is lower than 0.003 btc
		if($amountInITD > $minCashoutAmt){
			$msg = "Success : Cashout amount is grater than 0.0003 btc.";
			$xmlChild['requestStep'.++$i] = $i.". {$msg}";
	    	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);


    		//fetch user detials
			$userEmail = trim($request['user_email']);
		    $userRequiredFields = $userRequiredFields.",email,account_handle,active,deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,active,purchase_itd";                   
			$getUserDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);

			if(noError($getUserDetails)){
				if($getUserDetails['errMsg']['active'] == 1){
					$msg = "Success : Fetched user details";
					$xmlChild['requestStep'.++$i] = $i.". {$msg}";
			        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

			        //calculate users available balance.
					$getUserBalance = calculateUserBalance($getUserDetails);

			        if (noError($getUserBalance)) {
			        	$msg = "Success : Calculated user balance.";
			        	$xmlChild['requestStep'.++$i] = $i.". {$msg}";
			        	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
			            $userBalance = $getUserBalance['errMsg']['total_available_balance'];
						$userBalance = $userBalance + $request['payment_amount'] + $request['cashout_fees'];

			            //check whether available balace is grater than request amount or not.
			            if($userBalance >= ($request['payment_amount'] + $request['cashout_fees'])){
				        	$msg = "Success : Checked users available balance";
				        	$xmlChild['requestStep'.++$i] = $i.". {$msg}";
				        	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	        		    	//check whether amount is grater thant 0.1 btc
					    	if($amountInITD <= $minManualCashoutAmt ){
					    		$msg = "Success : Cashout amount is lower than 0.1 btc.";
					    		$xmlChild['requestStep'.++$i] = $i.". {$msg}";
					    		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

					        	//users wallet balance transactions 
					        	$userWalletBalance = checkUserWalletBalance($conn,$returnArr);	
					        	if(noError($userWalletBalance)){

					        		$currentTime = date("Y-m-d H:i:s");
									$query = "UPDATE {$currentTable} SET status='approved_tbc',request_update_date='{$currentTime}',comment='Successfully completed auto verification' WHERE id={$requestId}";
									$queryResult = runQuery($query, $conn);
									if($queryResult){
										$errMsg = 'Success: Updated request status to approved TBC';
										$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
									    $returnArr = setErrorStack($userWalletBalance, -1, $errMsg, $extraArgs);
									}else{
										$errMsg = 'Error: Updating request status to approved TBC';		
										$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
									    $returnArr = setErrorStack($userWalletBalance, 10, $errMsg, $extraArgs);	
									}

					        	}else{	

								    $returnArr = $userWalletBalance;
					        	}

				        	}else{
					    		$currentTime = date("Y-m-d H:i:s");
                                //old code--
					    		//$query = "UPDATE {$currentTable} SET status='pending_manual_verification',request_update_date='{$currentTime}',comment='cashout amount grater than 0.1 btc' WHERE id={$requestId}";
					    		/*$queryResult = runQuery($query, $conn);
					    		if($queryResult){
									$errMsg = 'Success: Updated request status to pending for manual veification';
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
								    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
					    		}else{
									$errMsg = 'Error: Updating request status to pending for manual verification';
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
								    $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
					    		}*/

                                /********************************************* for auto assigne cashot to agents ******************************************/
                                    $connAdmin = createDBConnection("acl");
                                    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

                                    $resultAdmin = selectAdminToAssign($connAdmin);
                                    if (noError($resultAdmin)) {
                                        $resultAdmin = $resultAdmin["errMsg"][0];
                                        $admin_email = $resultAdmin["admin_email"];
                                        $updateCashoutQueu = updateTableForCashout($requestId,$currentTime, $admin_email, $currentTable, $conn);
                                        if (noError($updateCashoutQueu)) {
                                            $updateAdminCount = updateAdminAssignedCount($admin_email, $connAdmin);
                                            $errMsg = 'Success: Updated request status to pending for manual veification';
                                            $xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                                        }else{

                                            $errMsg = 'Error: Updating request status to pending for manual verification';
                                            $xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
                                            $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

                                        }
                                    }else{
                                        $errMsg = 'Error: Updating request status to pending for manual verification';
                                        $xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
                                        $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                                    }

                                /********************************************* for auto assigne cashot to agents ******************************************/

					    	}

			            }else{
							$currentTime = date("Y-m-d H:i:s");
							$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='users available balance is low' WHERE id={$requestId}";
							$queryResult = runQuery($query, $conn);
							if($queryResult){
								$errMsg = 'Success: Updated request status to rejected';
								$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
							    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
							    $amount = $request['payment_amount'] + $request['cashout_fees'];
					    		$pendingWithdrawal = addDeductBlockedPendingWithdrawal($amount,$request['user_id'],$request['user_email'],"deduct");
							}else{
								$errMsg = 'Error: Updating request status to rejected';		
								$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
							    $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);	
							}

			            }


			        } else {
			            $errMsg = 'Error: Calculating user balance';
			            $xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			        	$returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);
			        }

				}else{
					$currentTime = date("Y-m-d H:i:s");
					$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='this user is not active' WHERE id={$requestId}";
					$queryResult = runQuery($query, $conn);
					if($queryResult){
						$errMsg = 'Success: Updated request status to rejected';
						$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
					    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
	    			    $amount = $request['payment_amount'] + $request['cashout_fees'];
			    		$pendingWithdrawal = addDeductBlockedPendingWithdrawal($amount,$request['user_id'],$request['user_email'],"deduct");
					}else{
						$errMsg = 'Error: Updating request status to rejected';	
						$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";	
					    $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);	
					}

				}  

			}else{
				$errMsg = 'Error: Fetching user deails';
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";

		        $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
			}

	    	

		}else{

			$currentTime = date("Y-m-d H:i:s");
			$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='cashout amount lower than 0.0003 btc' WHERE id={$requestId}";
			$queryResult = runQuery($query, $conn);
			if($queryResult){
				$errMsg = 'Success: Updated request status to rejected';
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
			    $amount = $request['payment_amount'] + $request['cashout_fees'];
			    $pendingWithdrawal = addDeductBlockedPendingWithdrawal($amount,$request['user_id'],$request['user_email'],"deduct");

			}else{
				$errMsg = 'Error: Updating request status to rejected';		
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			    $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);	
			}
			
		}

	}else{
		$errMsg = 'Error: Updating request status to processing';		
		$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
	    $returnArr = setErrorStack($returnArr, 12, $errMsg, $extraArgs);	
	}


	$request1Attribut["email"] = $request['user_email'];
	$request1Attribut["amount"] = $request['payment_amount'];
	$cashoutRequest = "request{$j}";
	$xml_data[$cashoutRequest]["data"] = '';
	$xml_data[$cashoutRequest]["attribute"] =$request1Attribut;
	$xml_data[$cashoutRequest]["childelement"] =$xmlChild;
	$returnArr['xml_data'] = $xml_data;
	return $returnArr;
			
}



/**
*   Function Name :   checkUserWalletBalance()
*   Purpose       :   This function checkes users wallet balance by fetching various transactions.
*   Arguments     :   (object) $conn
*/
function checkUserWalletBalance($conn, $returnArr){
    $extraArgs=array();
	$errMsg = 'Success: checked users wallet balance';
    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    return $returnArr;
}


/**
*   Function Name :   getCashoutRequests()
*   Purpose       :   This function retrieve pending cashout request for processing.
*   Arguments     :   (object) $conn
*/
function getCashoutRequests($conn){

    $keywordsTableName=getKeywordsDbName("dbkeywords");
    $returnArr = array();
  $dataArr = array();
  $dataArr1 = array();
  $query = "SHOW TABLES WHERE tables_in_{$keywordsTableName} LIKE 'queue_processor%'";
  $result = runQuery($query, $conn);
  if(noError($result)){
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $dataArr[] = $row;
        }

        foreach($dataArr as $key => $value){
          $tableName = $value["Tables_in_{$keywordsTableName}"];
          $query = "SELECT * FROM {$tableName} WHERE status='pending'";
      		$result = runQuery($query, $conn);
      	while($row = mysqli_fetch_assoc($result["dbResource"])){
        if(count($dataArr1) < 10){
            $dataArr1[] = $row; 
        }else{
          break;
        }

          }
          
        }

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $dataArr1;
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    
    return $returnArr;
}


/**
*   Function Name :   getRequestsForErrorHandler()
*   Purpose       :   This function retrieve cashout request which are not procesing from long time.
*   Arguments     :   (object) $conn
*/
function getRequestsForErrorHandler($conn){
    $keywordsTableName=getKeywordsDbName("dbkeywords");
    $returnArr = array();
  $dataArr = array();
  $dataArr1 = array();
  $query = "SHOW TABLES WHERE tables_in_{$keywordsTableName} LIKE 'queue_processor%'";
  $result = runQuery($query, $conn);
  if(noError($result)){
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $dataArr[] = $row;
        }

        foreach($dataArr as $key => $value){
          $tableName = $value["Tables_in_{$keywordsTableName}"];
          $query = "SELECT * FROM {$tableName} WHERE request_date < DATE_SUB(NOW(),INTERVAL 3 day) AND request_update_date < DATE_SUB(NOW(),INTERVAL 3 day) AND (status !='pending' || status !='paid' || status !='auto_rejected' || status !='manual_rejected')";
      		$result = runQuery($query, $conn);
      		while($row = mysqli_fetch_assoc($result["dbResource"])){
        		if(count($dataArr1) < 10){
            		$dataArr1[] = $row; 
        		}else{
          			break;
        		}

          	}
          
        }

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $dataArr1;
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    
    return $returnArr;
}


/**
*   Function Name :   processRequest()
*   Purpose       :   This function processes cashout requests.
*   Arguments     :   (object) $conn, (array)$request, (array)$returnArr, (integer)$j
*/
function processRequestForErrorHandler($conn,$request,$returnArr,$j){

	$currentTable = "queue_processor_".date("m_Y",strtotime($request['request_date']));
	$requestId = $request['id'];
	$i = 1;
	global $walletURLIP,$userRequiredFields;
    $extraArgs=array();
	$currencyRates = json_decode($request['currency_rates'],true);
	$coversionRates = $currencyRates['usd'] / $currencyRates['btc'];
	$amountinBTC = $request['payment_amount'] / $coversionRates;

	

		//check whether amount is lower than 0.003 btc
		if($amountinBTC > 0.0003){
			$msg = "Success : Cashout amount is grater than 0.0003 btc.";
			$xmlChild['requestStep'.++$i] = $i.". {$msg}";
	    	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);


    		//fetch user detials
			$userEmail = trim($request['user_email']);
		    $userRequiredFields = $userRequiredFields.",email,account_handle,active,deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,active,purchase_itd";                   
			$getUserDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
			
			if(noError($getUserDetails)){
				if($getUserDetails['errMsg']['active'] == 1){
					$msg = "Success : Fetched user details";
					$xmlChild['requestStep'.++$i] = $i.". {$msg}";
			        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

			        //calculate users available balance.
					$getUserBalance = calculateUserBalance($getUserDetails);
			        if (noError($getUserBalance)) {
			        	$msg = "Success : Calculated user balance.";
			        	$xmlChild['requestStep'.++$i] = $i.". {$msg}";
			        	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
			            $userBalance = $getUserBalance['errMsg']['total_available_balance'];

			            //check whether available balace is grater than request amount or not.
			            if($userBalance > $request['payment_amount']){
				        	$msg = "Success : Checked users available balance";
				        	$xmlChild['requestStep'.++$i] = $i.". {$msg}";
				        	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	        		    	//check whether amount is grater thant 0.1 btc
					    	if($amountinBTC < 0.1 ){
					    		$msg = "Success : Cashout amount is lower than 0.1 btc.";
					    		$xmlChild['requestStep'.++$i] = $i.". {$msg}";
					    		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

				        		$currentTime = date("Y-m-d H:i:s");
								$query = "UPDATE {$currentTable} SET status='pending_manual_verification',request_update_date='{$currentTime}',comment='This request needs to process by manual' WHERE id={$requestId}";
								$queryResult = runQuery($query, $conn);
								if($queryResult){
									$errMsg = 'Success: Updated request status to pending for manual verification';
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
								    $returnArr = setErrorStack($userWalletBalance, -1, $errMsg, $extraArgs);
								}else{
									$errMsg = 'Error: Updating request status to pending for manual verification';		
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
								    $returnArr = setErrorStack($userWalletBalance, 10, $errMsg, $extraArgs);	
								}

				        	}else{
					    		$currentTime = date("Y-m-d H:i:s");
					    		$query = "UPDATE {$currentTable} SET status='pending_manual_verification',request_update_date='{$currentTime}',comment='cashout amount grater than 0.1 btc' WHERE id={$requestId}";
					    		$queryResult = runQuery($query, $conn);
					    		if($queryResult){
									$errMsg = 'Success: Updated request status to pending for manual veification';
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
								    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
					    		}else{
									$errMsg = 'Error: Updating request status to pending for manual verification';	
									$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";	
								    $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);	
					    		}

					    	}

			            }else{
							$currentTime = date("Y-m-d H:i:s");
							$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='users available balance is low' WHERE id={$requestId}";
							$queryResult = runQuery($query, $conn);
							if($queryResult){
								$errMsg = 'Success: Updated request status to rejected';
								$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
							    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
							}else{
								$errMsg = 'Error: Updating request status to rejected';		
								$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
							    $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);	
							}

			            }


			        } else {
			            $errMsg = 'Error: Calculating user balance';
			            $xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			        	$returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);
			        }

				}else{
					$currentTime = date("Y-m-d H:i:s");
					$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='this user is not active' WHERE id={$requestId}";
					$queryResult = runQuery($query, $conn);
					if($queryResult){
						$errMsg = 'Success: Updated request status to rejected';
						$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
					    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
					}else{
						$errMsg = 'Error: Updating request status to rejected';	
						$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";	
					    $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);	
					}

				}  

			}else{
				$errMsg = 'Error: Fetching user deails';
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";

		        $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
			}

	    	

		}else{

			$currentTime = date("Y-m-d H:i:s");
			$query = "UPDATE {$currentTable} SET status='auto_rejected',request_update_date='{$currentTime}',comment='cashout amount lower than 0.0003 btc' WHERE id={$requestId}";
			$queryResult = runQuery($query, $conn);
			if($queryResult){
				$errMsg = 'Success: Updated request status to rejected';
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
			}else{
				$errMsg = 'Error: Updating request status to rejected';		
				$xmlChild['requestStep'.++$i] = $i.". {$errMsg}";
			    $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);	
			}
			
		}




	$request1Attribut["email"] = $request['user_email'];
	$request1Attribut["amount"] = $request['payment_amount'];
	$cashoutRequest = "request{$j}";
	$xml_data[$cashoutRequest]["data"] = '';
	$xml_data[$cashoutRequest]["attribute"] =$request1Attribut;
	$xml_data[$cashoutRequest]["childelement"] =$xmlChild;
	$returnArr['xml_data'] = $xml_data;
	return $returnArr;
}


/**
*   Function Name :   addBlockedPendingWithdrawal()
*   Purpose       :   This function adds cashout amount in pending withdrawal.
*   Arguments     :   (string) $amountInITD, (string)$userId, (string)$userEmail 
*/
function addDeductBlockedPendingWithdrawal($amountInITD,$userId,$userEmail,$action){

  global $walletPublicKey, $mode, $walletURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();
  $attr = 'blockedpendingwithdrawals';
  if (!empty($amountInITD) && !empty($userId) && !empty($userEmail) && !empty($action)) {

    
    $postFields = array( 
      "user_id" => $userId,
      "amount" => $amountInITD,
      "attr" => $attr,
      "action" => $action,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    //create signature
    $apiText = "user_id={$userId}&amount={$amountInITD}&attr={$attr}&action={$action}&publicKey={$walletPublicKey}";

    $postFields = $postFields;
    $apiName = "user/{$userId}/blockedpendingwithdrawals/{$action}";
    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } 
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "All fields are mandatory";
}

return $retArray; 
}	

/**
*   Function Name :   getAllCashoutRequest()
*   Purpose       :   This function fetches all cashout requests of user.
*   Arguments     :   (object) $conn, (string)$userEmail, (string)$status 
*/
function getAllCashoutRequest($conn,$userEmail,$status,$olderTime,$newerTime){

	$keywordsTableName = getKeywordsDbName("dbkeywords");
	
	$returnArr = array();
	$noOfRecords = 11;
	$dataArr = array();
	$reuestArr = array();
	$reuestArr2 = array();
	$query = "SHOW TABLES WHERE tables_in_{$keywordsTableName} LIKE 'queue_processor%' ";
	$result = runQuery($query, $conn);
	if(noError($result)){
	    while($row = mysqli_fetch_assoc($result["dbResource"])){
	        $dataArr[] = $row;
	    }
	}

	$where = (!empty($olderTime))? "AND request_date < '{$olderTime}'" : '';
	$where .= (!empty($newerTime))? "AND request_date >= '{$newerTime}'" : '';
	$where .= (!empty($status))? "AND status LIKE '%{$status}%'" : '';
	//$where .= (empty($status))? "AND (status = 'approved_tbc' OR status = 'pending_manual_verification' OR status = 'pending' OR status = 'processing' OR status LIKE '%rejected') " : '';


	$allTables = array_reverse($dataArr);

	foreach($allTables as $table){
		$tableName = $table["Tables_in_{$keywordsTableName}"];
		$query = "SELECT * FROM {$tableName} WHERE user_email = '{$userEmail}' {$where} ORDER BY request_date DESC";

		$queryResult = runQuery($query,$conn);
		if(noError($queryResult)){
			

			    while($row = mysqli_fetch_assoc($queryResult["dbResource"])){
			    	if(count($reuestArr2) < $noOfRecords){
			        	$reuestArr2[] = $row;

			        }else{
						break;
					}
			    }
			
		}

	}

	$reuestArr['request_container'] = $reuestArr2;

	if(count($reuestArr2) == $noOfRecords){
		$backwardCursor = $reuestArr2[$noOfRecords-2]['request_date']; 
		array_pop($reuestArr['request_container']);
	}else{
		$backwardCursor = '';
	}
	$reuestArr['backward_cursor'] = $backwardCursor; 

	if(empty($olderTime) && empty($newerTime) || true){

		$firstRecordTime = $reuestArr['request_container'][0]['request_date'];
		$reuestArr1 = array();
		$whereStatus = (!empty($status))? "AND status = '{$status}'" : '';
		$whereStatus .= (empty($status))? "AND (status = 'pending' OR status = 'processing' OR status LIKE '%rejected') " : '';
		foreach($allTables as $table){
			$tableName = $table["Tables_in_{$keywordsTableName}"];
			$query = "SELECT * FROM {$tableName} WHERE user_email = '{$userEmail}' {$whereStatus} AND request_date > '{$firstRecordTime}' LIMIT 1";
			$queryResult = runQuery($query,$conn);
			if(noError($queryResult)){
				if(count($reuestArr1) < 1){
				    while($row = mysqli_fetch_assoc($queryResult["dbResource"])){
				        $reuestArr1[] = $row;
				    }
				}else{
					break;
				}
			}
		}

		if(count($reuestArr1) > 0){
			$forwordCursor = $firstRecordTime;
		}else{
			$forwordCursor = '';
		}
		$reuestArr['forword_cursor'] = $forwordCursor; 		
	}

	return $reuestArr;
} 

/**
*   Function Name :   getCashoutRequestById()
*   Purpose       :   This function fetches perticulercashout requests of user by cashout id.
*   Arguments     :   (object) $conn, (number)$cashout_id, (string)$request_date 
*/
function getCashoutRequestById($conn, $cashout_id, $request_date, $userEmail){
	$reuestArr = array();
	$returnArr = array();
	$tableName = "queue_processor_".date('m_Y',strtotime($request_date));
	$query = "SELECT * FROM {$tableName} WHERE id = {$cashout_id} AND user_email = '{$userEmail}'";
	$queryResult = runQuery($query,$conn);
	if(noError($queryResult)){
	    while($row = mysqli_fetch_assoc($queryResult["dbResource"])){
	        $reuestArr[] = $row;
	    }
	}

	$returnArr['errCode'] = -1;
	$returnArr['errMsg'] = $reuestArr;
	return $returnArr;
}


/************************ admin **************************************************************************/

function checkIsPresentAgent($conn)
{

    $returnArr = array();
    global $coagent;
    $query = "select count(*) as count from admin_user where group_id LIKE '%{$coagent}%'  and admin_email <> 'admin@keywo.com'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function selectAdminToAssign($conn)
{
    $connAcl = createDBConnection("acl");
    $connAcl = $connAcl["connection"];
    $check=checkIsPresentAgent($connAcl);
    if(noError($check))
    {
        $countData=$check["errMsg"][0]["count"];
    $returnArr = array();
    global $coagent;

    if($countData>0) {
        $query = "select admin_id,admin_email from admin_user where group_id LIKE '%{$coagent}%' and cashout_assigned=(select min(cashout_assigned) from admin_user where group_id LIKE '%{$coagent}%') and admin_email <> 'admin@keywo.com' limit 1";
    }else
    {
        $query = "select admin_id,admin_email from admin_user where group_id LIKE '%{$coagent}%' and cashout_assigned=(select min(cashout_assigned) from admin_user where group_id LIKE '%{$coagent}%') limit 1";
    }

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $check["errMsg"];
    }
    return $returnArr;
}


function updateTableForCashout($id,$currentTime,$assignedTo,$tablename,$conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE {$tablename} SET assigned_to='{$assignedTo}',status='pending_for_uam_approval',request_update_date='{$currentTime}',comment='cashout amount greater than Automode Amount set by admin' where id={$id}";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $errMsg = "Data  Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}


function updateAdminAssignedCount($admin_email,$conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET cashout_assigned=cashout_assigned+1 where admin_email='{$admin_email}'";
    $result = runQuery($query, $conn);


    if (noError($result)) {
        $errMsg = "Data  Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}
/************************ admin **************************************************************************/

function getKeywordsDbName($database)
{
    $DBConnection =new DBConnection();
    $credential=$DBConnection->getDatabaseCredential($database);
    return $credential["database"];
}


?>