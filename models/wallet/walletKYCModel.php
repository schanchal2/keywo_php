<?php
function updaetKYCLevelDocs($user_id,$email,$doc1,$doc2,$doc3,$doc4){
	$retArray = array();
	global $walletPublicKey, $mode, $walletURL;

	if(!empty($user_id) && !empty($email)){
		$doc1 = (!empty($doc1))?$doc1:'';
		$doc2 = (!empty($doc1))?$doc2:'';
		$doc3 = (!empty($doc1))?$doc3:'';
		$doc4 = (!empty($doc1))?$doc4:'';

		$postFields = array( 
	      "email" => $email,
	      "doc_1" => $doc1,
	      "doc_2" => $doc2,
	      "doc_3" => $doc3,
	      "doc_4" => $doc4,		
	      "publicKey" =>  urlencode($walletPublicKey)
	    );
	    $apiText = "user_id={$user_id}&level=3&email={$email}&publicKey={$walletPublicKey}";
	    $postFields = $postFields;
	    $apiName = "pocket/{$user_id}/kyc/level/3";
	    $requestUrl = "{$walletURL}";
	    $curl_type = 'PUT';
	    $content_type = "content-type: application/json";
	    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

	    if ($mode == 'production') {
	        $nonce_result = generateSignatureWithNonce($apiText);
	        if (noError($nonce_result)) {
	            $nonce_result = $nonce_result['errMsg'];
	            $retArray['errCode'] = -1;
	            //$retArray['errMsg'] = $nonce_result['errMsg'];
	            $headers[] = "x-ts: {$nonce_result['timestamp']}";
	            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
	        } else {
	            $retArray['errCode'] = 2;
	            $retArray['errMsg'] = $nonce_result['errMsg'];
	        }
	    }

	    $respArray = curlRequest($curlReqParam, $headers);
	    if (noError($respArray)) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = -1;
	        $retArray['errMsg'] = $respArray['errMsg'];
	        $retArray['mode'] = $mode;

	    } else if ($respArray["errCode"] != 73) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    } else {
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    }
	}else{
		$retArray['errCode'] = 2;
		$retArray['errMsg'] = "All feilds are mandatory";
	}

	return $retArray;
}

function verifyOTP($conn,$mobile,$otp){

	$returnArr = array();
	$mobile = urldecode($mobile);
	$query = "SELECT * FROM otp_gen WHERE  opt_number='".$otp."' AND mob='".$mobile."' AND created_on >= DATE_SUB(NOW(),INTERVAL 30 MINUTE) AND status='Active'";
    $check = runQuery($query, $conn);
	$row = mysqli_fetch_array($check['dbResource']);
	if($row){
		$update = "UPDATE otp_gen SET status='Inactive' WHERE mob='".$mobile."'";
		$checkUpdate = runQuery($update, $conn);
		if(noError($checkUpdate)){
			$returnArr['errCode'] = -1;
			$returnArr['errMsg'] = "OTP has been successfully verified";
		}else{
			$returnArr['errCode'] = 3;
			$returnArr['errMsg'] = "Failed to verify OTP";
		}

	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Invalid OTP";
	}
	return $returnArr;
}

function generateOTP($conn,$user_id,$mobile){
	$mobile = urldecode($mobile);
	$returnArr = array();
	$otp = generateOTPnumber(6, false, 'd');

	$update = "UPDATE otp_gen SET status='Inactive' WHERE mob='".$mobile."'";
	$checkUpdate = runQuery($update, $conn);

	$insert_otp = "INSERT INTO otp_gen (user_id,mob,opt_number,status) VALUES('".$user_id."','".cleanQueryParameter($conn,$mobile)."','".$otp."','Active')";
	$insert_succ = runQuery($insert_otp, $conn);

	if(noError($insert_succ)) {

		//send sms	
		$msg = "Dear User, Your OTP for verification is: ".$otp;
		$response = sendSMS($msg,$mobile); 
		$returnArr['errCode'] = -1;
		$returnArr['errMsg'] = "Otp has been sent to your number";
	}else{
		$returnArr['errCode'] = 3;
		$returnArr['errMsg'] = "Failed.Error in generating otp";
	}

	return $returnArr;

}

function updateKYCLevelCount($user_id, $email,$KYClevel){
	$retArray = array();
	global $walletPublicKey, $mode, $walletURL;

	if(!empty($user_id) && !empty($email) && !empty($KYClevel)){
		$postFields = array( 
	      "email" => $email,
	      "publicKey" =>  urlencode($walletPublicKey)
	    );
	    $apiText = "email={$email}&move={$KYClevel}&user_id={$user_id}&publicKey={$walletPublicKey}";
	    $postFields = $postFields;
	    $apiName = "pocket/{$user_id}/kyc/level/shift/{$KYClevel}";
	    $requestUrl = "{$walletURL}";
	    $curl_type = 'PUT';
	    $content_type = "content-type: application/json";
	    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

	    if ($mode == 'production') {
	        $nonce_result = generateSignatureWithNonce($apiText);
	        if (noError($nonce_result)) {
	            $nonce_result = $nonce_result['errMsg'];
	            $retArray['errCode'] = -1;
	            //$retArray['errMsg'] = $nonce_result['errMsg'];
	            $headers[] = "x-ts: {$nonce_result['timestamp']}";
	            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
	        } else {
	            $retArray['errCode'] = 2;
	            $retArray['errMsg'] = $nonce_result['errMsg'];
	        }
	    }

	    $respArray = curlRequest($curlReqParam, $headers);
	    if (noError($respArray)) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = -1;
	        $retArray['errMsg'] = $respArray['errMsg'];
	        $retArray['mode'] = $mode;

	    } else if ($respArray["errCode"] != 73) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    } else {
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    }
	}else{
		$retArray['errCode'] = 2;
		$retArray['errMsg'] = "All feilds are mandatory";
	}

	return $retArray;

}


function updateMobileKYCLevel1($user_id, $email, $mobile){
	$mobile = urldecode($mobile);
	global $walletPublicKey, $mode, $walletURL;
  	global $descriptionArray;
	$retArray = array();

	if(!empty($user_id) && !empty($email) && !empty($mobile)){
		// $mobile = urlencode($mobile);
		$postFields = array( 
	      "email" => $email,
	      "cell_no" => $mobile,
	      "publicKey" =>  urlencode($walletPublicKey)
	    );

	    $apiText = "user_id={$user_id}&level=2&email={$email}&publicKey={$walletPublicKey}";
	    $postFields = $postFields;
	    $apiName = "pocket/{$user_id}/kyc/level/2";
	    $requestUrl = "{$walletURL}";
	    $curl_type = 'PUT';
	    $content_type = "content-type: application/json";
	    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

	    if ($mode == 'production') {
	        $nonce_result = generateSignatureWithNonce($apiText);
	        if (noError($nonce_result)) {
	            $nonce_result = $nonce_result['errMsg'];
	            $retArray['errCode'] = -1;
	            //$retArray['errMsg'] = $nonce_result['errMsg'];
	            $headers[] = "x-ts: {$nonce_result['timestamp']}";
	            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
	        } else {
	            $retArray['errCode'] = 2;
	            $retArray['errMsg'] = $nonce_result['errMsg'];
	        }
	    }
	    $respArray = curlRequest($curlReqParam, $headers);
	    if (noError($respArray)) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = -1;
	        $retArray['errMsg'] = $respArray['errMsg'];
	        $retArray['mode'] = $mode;

	    } else if ($respArray["errCode"] != 73) {
	        if ($mode == 'production') {
	            $nonce_counter = incrementVectorCounter();
	        }
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    } else {
	        $retArray['errCode'] = $respArray['errCode'];
	        $retArray['errMsg'] = $respArray['errMsg'];
	    } 

	}else{
		$retArray['errCode'] = 2;
		$retArray['errMsg'] = "All feilds are mandatory";
	}

	return $retArray;
}

function isAlreadyExistMobileNo($userId, $email,$mobile_to_check){

  global $walletURLIP, $userRequiredFields, $mode, $walletPublicKey, $walletURL;
  $mobile1 = urldecode($mobile_to_check);
  $mobile = urlencode($mobile1);
  $returnArr = array();
  if(isset($email) && !empty($email) && isset($mobile) && !empty($mobile) && isset($userId) && !empty($userId)){
    $postFields = "cell_no={$mobile}&publicKey=".urlencode($walletPublicKey);
    $apiText = "mobile_number={$mobile1}&user_id={$userId}&publicKey={$walletPublicKey}";
    $apiName = "pocket/{$userId}/kyc/cell/exist";
    $requestUrl = "{$walletURL}";

    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
    // nonce implementation
   if($mode == 'production'){
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)){
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        }else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }
    // end nonce

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        /*$returnArr['errCode'] = -1;
        $returnArr['errMsg'] = $respArray['errMsg'];
        $returnArr['mode'] = $mode;*/

        $errMsg               =   $respArray["errMsg"];
        $errCode              =   $respArray["errCode"];
        $extraArgs["mode"]    =   $mode;
        $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

    } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    } else {
          $errMsg               =   $respArray["errMsg"];
          $errCode              =   $respArray["errCode"];
          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
    }
  }else{
    $returnArr['errMsg'] = "Mandatory feilds not found";
    $returnArr['errCode'] = 2;    
  }

  return $returnArr;
}



function getKYCSlabByLevel($connSearch){

    $returnArr = array();
    $extraArg = array();

    $query = "SELECT * FROM kyc_levels where 1=1";
    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 64, null, $extraArg);
    }
    return $returnArr;
}
?>