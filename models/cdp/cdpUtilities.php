<?php


/* Archive expires file */
function archivesFiles($arcieveDir, $file, $folder){
    $returnArr = array();

    $zip = new ZipArchive;  /* Make object of ZipArchive class */
    $zipDir = $zip->open($arcieveDir.$folder,ZipArchive::CREATE);  /* Create new zip folder if not exist */

    if ($zipDir === TRUE) {
        $zip->addFile($file, filemtime($file).'.json');  /* Add  json file in zip directory from path */
        $zip->close();

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "success";
    }else{
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = "Error: Unable to archieve";
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getCurrentPayout()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getCurrentPayout()
*   Purpose       :   This function returns the current payout amount from payout.json file
*                     in IT$ amount i.e. (USD).
*   Arguments     :   none
*   Returns       :   (float) current_payout
*/

function getCurrentPayout(){

    global $docRoot;
    $returnArr = array();
    $extraArgs = array();

    $payoutJson = $docRoot."security/payout/payout.json";
    if(file_exists($payoutJson)){
        $readJson = file_get_contents($payoutJson);
        $readJson = json_decode($readJson, true);
        $payout = $readJson["payout"];

        $errMsg = "Success: Retrieve payout amount";
        $extraArgs["current_payout"] = $payout;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = "Error: File not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateAppSearchCount()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateAppSearchCount()
*   Purpose       :   This function update search count into sc_app_details table where keywords search from
*                 :   corresponding app.
*   Arguments     :   (array) $appUpdateParam, (oject) $searchDbConn
*/

//  updateAppSearchCount($paramArray["app_id"], $paramArray["app_name"], $paramArray["search_query"], $paramArray["search_type"], $searchDbConn);
function updateAppSearchCount($appId, $appName, $searchQuery, $searchType, $searchDbConn){
    $extraArgs = array();
    $returnArr = array();
    $appUpdateParam = array("app_id" => $appId, "app_name" => $appName, "search_query" => $searchQuery, "search_type" => $searchType);

    if(isset($appUpdateParam) && !empty($appUpdateParam)){
        if(isset($searchDbConn) && !empty($searchDbConn)){
            if(count($appUpdateParam) > 0){
                if($appUpdateParam["search_type"] == "Qualified"){
                    $scAppTableFieldName = "totalQualifiedSearches";
                }else{
                    $scAppTableFieldName = "totalUnqualifiedSearches";
                }

                $errMsg = "Success : Get user search type";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $increaseSearches = array("field_name" => $scAppTableFieldName, "app_id" => $appUpdateParam["app_id"]);
                // 1. Update totalUnqualifiedSearches or totalQualifiedSearches field by 1 into sc_app_details table of corresponding appId.
                $updateSearchCountFields = increaseAppSearchCount($increaseSearches, $searchDbConn);

                if(noError($updateSearchCountFields)){

                    $errMsg = "Success : increase search count on app";
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    // 2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
                    $increaseKeywordCountInAppJson = addKeywordInAppJson($appUpdateParam["app_id"], $appUpdateParam["app_name"], $appUpdateParam["search_query"]);

                    if(noError($increaseKeywordCountInAppJson)){
                        $totalKeywordUniqueCount = $increaseKeywordCountInAppJson["errMsg"]["noOfNewKwds"];

                        $errMsg = "Success : Added ". $totalKeywordUniqueCount." unique keyword in ". $appUpdateParam["app_name"] ." app json ";
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                        // 3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
                        $increaseUniqueKwdCount = updateTotalKwdSearchedOnApp($totalKeywordUniqueCount, $appUpdateParam["app_id"], $searchDbConn);
                        if(noError($increaseUniqueKwdCount)){
                            $errMsg = "Success : Increase total unique keyword searched count on app";
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                        }else{
                            $errMsg = $increaseUniqueKwdCount["errMsg"];
                            $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                        }
                    }else{
                        $errMsg = "Error occured while adding unique keyword count in app json";
                        $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                    }
                }else{
                    $errMsg = $updateSearchCountFields["errMsg"];
                    $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                }
            }else{
                $errMsg = "Error: Parameters not found";
                $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
            }
        }else{
            $errMsg = "Error: Missing connection parameters";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Missing required parameters";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function increaseAppSearchCount()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   increaseAppSearchCount()
*   Purpose       :   This function update search count by 1 or requested field into sc_app_details table.
*   Arguments     :   (array) $increaseSearches, (oject) $searchDbConn
*/

function increaseAppSearchCount($increaseSearches, $searchDbConn){
    $returnArr = array();
    $extraArgs = array();

    if(isset($increaseSearches) && !empty($increaseSearches)){
        if(isset($searchDbConn) && !empty($searchDbConn)){
            $query = "update sc_app_details set ".$increaseSearches["field_name"] ." = ". $increaseSearches["field_name"] ." + 1 where app_id = ".$increaseSearches["app_id"];

            $execQuery = runQuery($query, $searchDbConn);
            if(noError($execQuery)){
                $errMsg = "Record update successfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
            }else{
                $errMsg = "Error: ".$execQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
            }
        }else{
            $errMsg = "Error: Missing connection object";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Missing required parameters to update app searches";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function addKeywordInAppJson()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   increaseAppSearchCount()
*   Purpose       :   This function Create / update app json file where unique keyword with count = 1 or
*                 :   increase count of existing keyword.
*   Arguments     :   (integer) $appId, (string) $appName, (string) $keyword
*/

function addKeywordInAppJson($appId, $appName, $keywords){

    global $docRoot;

    $returnArr = array();
    $keywordArr = array();
    $totalKeywordSearched = 0;
    $insertCount=0;

    //convert search query into array of keywords.
    $keywords = strtolower($keywords);
    $searchArr = explode(" ",$keywords);

    // concatenate _ to app name if app name contains more then 1 words.
    $appName = explode(" ", $appName);
    $appName = implode("_", $appName);

    // creating directory to store search keywords details in json format with search app id and name.

    $searchKeywordDir = $docRoot."/json_directory/app_json";
    if(!is_dir($searchKeywordDir)){
        mkdir($searchKeywordDir, 0777, true);
    }

    $appName = strtolower($appName);
    $jsonKwd = $appId."_".$appName."_KeywordSearched.json";
    if(!file_exists($searchKeywordDir."/".$jsonKwd)){
        $fp = fopen($searchKeywordDir."/".$jsonKwd, "w");
        foreach($searchArr as $key => $value){
            $keywordArr[$value] = 1;
        }
        fwrite($fp, json_encode($keywordArr));
        fclose($fp);
        $insertCount++;
    }else{
        $fp1 = fopen($searchKeywordDir."/".$jsonKwd, "r+");
        $json = fread($fp1, filesize($searchKeywordDir."/".$jsonKwd));
        $resultJson = json_decode($json, true);
        foreach($searchArr as $key => $val){
            if(array_key_exists($val, $resultJson)){
                $resultJson[$val]++;
            }else{
                $resultJson[$val] = 1;
                $insertCount++;
            }
        }
        $fp2 = fopen($searchKeywordDir."/".$jsonKwd, "w");
        fwrite($fp2,json_encode($resultJson));
        fclose($fp2);
        //$totalKeywordSearched = count($resultJson);
    }
    $returnArr['errCode'] = -1;
    $returnArr['errMsg']["noOfNewKwds"] = $insertCount;

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateTotalKwdSearchedOnApp()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateTotalKwdSearchedOnApp()
*   Purpose       :   This function update total unique keyword search count in sc_app_details table.
*   Arguments     :   (integer) $totalUniqueCount, (object) $searchDbConn
*/

function updateTotalKwdSearchedOnApp($noOfCount, $appId, $searchDbConn){
    $returnArr = array();
    $extraArgs = array();

    $query = "update sc_app_details set totalKeywordSearched = totalKeywordSearched + ".$noOfCount." where app_id = ".$appId;
    $execQuery =  runQuery($query, $searchDbConn);
    if(noError($execQuery)){
        $errMsg = "Record update successfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = $execQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function addSearchHistory()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   addSearchHistory()
*   Purpose       :   This function insert user search transaction details into week wise user search
*                 :   history table.
*   Arguments     :  (string) $email, (array) $param, (object) $conn
*/
function addSearchHistory($email, $param, $conn){
    $extraArgs = array();
    $tableName = "";
    $retArray = array();
    $dt = explode(":",date("Y:m:j",time()));

    if($dt[2] > 0 && $dt[2] <= 7)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_1wk";
    }
    elseif($dt[2] > 7 && $dt[2] <= 14)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_2wk";
    }
    elseif($dt[2] > 14 && $dt[2] <= 21)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_3wk";
    }
    elseif($dt[2] > 21 && $dt[2] <= 31)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_4wk";
    }

    $query = "INSERT INTO {$tableName} (id,trans_id, sender, receiver_email, type, keyword, discount, commision, origin_ip, credit_amount, payout, usd, sgd, app_id,timestamp, exchange_rate_json) VALUES ('', '".$param['trans_id']."', '".$param['sender']."', '".$email."', '".$param['type']."', '".$param['keyword']."', '".$param['discount']."', '".$param['commision']."', '".$param['origin_ip']."', '".$param['amount']."', '".$param['payout']."', '".$param['usd']."', '".$param['sgd']."', '".$param['app_id']."','".$param["trans_time"]."', '". $param["jsonExchangeRate"]."' )";

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){

        $errMsg = "Success: Insert search history successfully.";
        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

        $servername = null;$username = null;$password = null;$dbname = null;
    } else {
        $errMsg = "Error: ".$execQuery["errMsg"];
        $retArray = setErrorStack($retArray, 1, $errMsg, $extraArgs);
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function insertSearchHistory()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   insertSearchHistory()
*   Purpose       :   This function insert user search history into mysql database.
*/

function insertSearchHistory($email, $searchQuery, $type, $currentPayout, $jsonExchangeRate, $appId, $transactionId, $transactionTime, $clientIp, $searchDbConn){

    $returnArr = array();
    $extraArgs = array();
    $param = array();
    $param["trans_id"] = $transactionId;
    $param["trans_time"] = $transactionTime;
    $param['sender'] = 'communitypool@keywo.com';
    $param['reciever'] = $email;
    $param['type'] = $type;
    $param['keyword'] = urlencode($searchQuery);
    $param['desc'] = '';
    $param['payMode'] = '';
    $param['discount'] = '';
    $param['commission'] = '';
    $param['origin_ip'] = $clientIp;
    $param['amount'] = $currentPayout;
    $param['payout'] = number_format($currentPayout*4, 2);
    // $param['usd'] = $currUSD;
    // $param['sgd'] = $currSGD;
    $param['app_id'] = $appId;
    $param["jsonExchangeRate"] = $jsonExchangeRate;

    // insert into week wise user search history
    $insertHistory = addSearchHistory($email, $param, $searchDbConn);

    if(noError($insertHistory)){
        $errMsg = $insertHistory["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = "Error: ".$insertHistory["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function increaseUserSearches()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   increaseUserSearches()
*   Purpose       :   This function increase logged in users qualified or unqualified searches
*                 :   in user detail @ notification server.
*   Arguments     :   (string)searchtype
*/

function increaseUserSearches( $email, $searchType){

    global $walletPublicKey, $mode, $walletURLIPnotification;
    $retArray = array();

    if($searchType == "Qualified"){
        $type = "qualified";
    }else{
        $type = "unqualified";
    }

    if (isset($searchType) && !empty($searchType)) {

        $headers = array();

        //create signature
        $apiText = "email=".$email."&searchCount=1&publicKey={$walletPublicKey}";
        $postFields = "email={$email}&searchCount=1&publicKey={$walletPublicKey}";
        $apiName = 'api/notify/v2/search/'.$type.'/add';
        $requestUrl = "{$walletURLIPnotification}";
        $curl_type = 'PUT';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function increaseKeywoStatsSearches()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   increaseKeywoStatsSearches()
*   Purpose       :   This function update qualified_interaction or unqualified_interaction into keywo_stats
*                 :   table ( instead of pool user)
*   Arguments     :   (string) $searchType, (object) $kwdDbConn
*/

function increaseKeywoStatsSearches($searchType, $kwdDbConn){
    $extraArgs = array();

    $returnArr = array();

    if(isset($searchType) && !empty($searchType) && isset($kwdDbConn) && !empty($kwdDbConn)){

        if($searchType == "Qualified"){
            $columnName = "qualified_interaction";
        }else{
            $columnName = "unqualified_interaction";
        }

        $query = "Update keywo_stats set ".$columnName." = ". $columnName ." + 1";

        $execQuery = runQuery($query, $kwdDbConn);
        if(noError($execQuery)){
            $errMsg = "Success: Keywo stats record update successfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{
            $errMsg = $execQuery["errMsg"];
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function addKwdCntInRevenueTable()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   addKwdCntInRevenueTable()
*   Purpose       :   This function update app_kwd_search_count, user_kwd_search_count, kwd_total_paid_searches,
*                 :   kwd_total_unpaid_searches into keyword_revenue_<alphabet_first_letter> into keyword db.
*   Arguments     :   (string) $searchQuery, (string) $searchType, (string) $email, (integer) $appId,
*                 :   (string)$clientIp, (object) $kwdDbConn
*/

function addKwdCntInRevenueTable($searchQuery, $searchType, $email, $appId, $post_type, $clientIp, $kwdDbConn){
    $extraArgs = array();
    $currentDate = date("Y/m/d");
    // 7. In keyword revenue table increase below given fileds,
    //  7.1 Increase total_keyword_unpaid count,
    //  7.2 Increae user_kwd_search_count count,
    //  7.3 Increase app_kwd_search_count in json value of correponding app Id

    $returnArr = array();


    if(isset($searchQuery) && !empty($searchQuery) && isset($searchType) && !empty($searchType) && isset($email) && !empty($email) && isset($appId) && !empty($appId) && isset($kwdDbConn) && !empty($kwdDbConn)){

        // convert search query into array useing explode method
        $keywordArr = explode(" ", $searchQuery);

        foreach($keywordArr as $key => $keyword){

            //increase keyword count in geotrends database.
            // status = not completed
            //$addKwdCntInGeoDb = updateSearchTrends($keyword, $appId);

            if(isset($keyword) && !empty($keyword)){

                //get keyword ownership details from keyword_ownership_<first_lette e.g. c > table from keyword database.
                $kwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                if(noError($kwdOwnershipDetails)){
                    $kwdOwnershipDetails = $kwdOwnershipDetails["data"][0];
                    $kwdOwnerEmail       = $kwdOwnershipDetails["buyer_id"];
                    $noOfActiveBids      = $kwdOwnershipDetails["no_of_active_bids"];
                    $highestBidAmount    = $kwdOwnershipDetails["highest_bid_amount"];
                    $kwdAskPrice         = $kwdOwnershipDetails["ask_price"];

                    // if keyword owner email is empty then set keyword owner as admin
                    if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                        $kwdOwnerEmail = "admin";
                    }

                    // get keyword revenue details
                    $kwdRevenueDetails = getRevenueDetailsByKeyword($kwdDbConn, $keyword);

                    if(noError($kwdRevenueDetails)){
                        $kwdRevenueTableName    = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails      = $kwdRevenueDetails["data"][0];
                        $id                     = $kwdRevenueDetails["id"];
                        $interactionCount      = $kwdRevenueDetails["app_kwd_search_count"];
                        $userKwdSearchCount     = $kwdRevenueDetails["user_kwd_search_count"];
                        $kwdTotalPaidSearches   = $kwdRevenueDetails["kwd_total_paid_searches"];
                        $kwdTotalUnpaidSearches = $kwdRevenueDetails["kwd_total_unpaid_searches"];

                        if(!isset($kwdTotalPaidSearches) && empty($kwdTotalPaidSearches)){
                            $kwdTotalPaidSearches = 0;
                        }
                        if(!isset($kwdTotalPaidSearches) && empty($kwdTotalPaidSearches)){
                            $kwdTotalUnpaidSearches = 0;
                        }

                        if($searchType == "Qualified"){
                            $kwdTotalPaidSearches++;
                        }else{
                            $kwdTotalUnpaidSearches++;
                        }

                        if(isset($id) && !empty($id)) {
                            $interactionCount = json_decode($interactionCount, true);
                            $userKwdSearchCount = $userKwdSearchCount + 1;
                            if(array_key_exists("total", $interactionCount)){
                                $interactionCount["total"] = $interactionCount["total"] + 1;
                            }

                            $checkAppExist = validateAppExistence($appId);
                            if(noError($checkAppExist)){
                                $checkAppExist = $checkAppExist["errMsg"]["app_id"];
                            }

                            if(!empty($checkAppExist)){
                                if(isset($appId) && !empty($appId)){
                                    if(array_key_exists("search", $interactionCount)){
                                        if(array_key_exists($appId, $interactionCount["search"])){
                                            $interactionCount["search"][$appId] = $interactionCount["search"][$appId] + 1;
                                        }else{
                                            $interactionCount["search"][$appId] = 1;
                                        }
                                    }else{
                                        $interactionCount["search"][$appId] = 1;
                                    }
                                }else{
                                    $interactionCount["search"][$appId] = 1;
                                }
                            }

                            if(isset($post_type) && !empty($post_type)){
                                if(array_key_exists("social", $interactionCount)){
                                    if(array_key_exists($post_type, $interactionCount["social"])){
                                        $interactionCount["social"][$post_type] = $interactionCount["social"][$post_type] + 1;
                                    }else{
                                        $interactionCount["social"][$post_type] = 1;
                                    }
                                }else{
                                    $interactionCount["social"][$post_type] = 1;
                                }
                            }else{
                                $interactionCount["social"][$post_type] = 1;
                            }

                            // json encode appKwdSearchCount variable to update the app_kwd_search_count value.
                            $interactionCount = json_encode($interactionCount);


                            $query = "UPDATE ".$kwdRevenueTableName." SET app_kwd_search_count='".$interactionCount."',user_kwd_search_count='".$userKwdSearchCount."',kwd_total_paid_searches='".$kwdTotalPaidSearches."',kwd_total_unpaid_searches='".$kwdTotalUnpaidSearches."' WHERE keyword='".$keyword."'";
                        }else{
                            $interactionCount["total"] = 1;

                            $checkAppExist = validateAppExistence($appId);
                            if(noError($checkAppExist)){
                                $checkAppExist = $checkAppExist["errMsg"]["app_id"];
                            }

                            if(!empty($checkAppExist)){
                                if(isset($appId) && !empty($appId)){
                                    $interactionCount["search"][$appId] = 1;
                                }
                            }

                            if(isset($post_type) && !empty($post_type)){
                                $interactionCount["social"][$post_type] = 1;
                            }

                            $appKwdSearchCountJson = json_encode($interactionCount);

                            $query = "INSERT into ".$kwdRevenueTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches,search_startFrom,follow_unfollow) VALUES('".$keyword."','".$kwdOwnerEmail."','".$appKwdSearchCountJson."','','1','".$perKeywordPayout."','".$kwdTotalPaidSearches."','".$kwdTotalUnpaidSearches."','','".$currentDate."','')";
                        }
                         $updateRevenueTable = updateRevenueTable($query, $keyword, $kwdDbConn);
                        if(noError($updateRevenueTable)){
                            $errMsg = $updateRevenueTable["errMsg"];
                            $errCode = $updateRevenueTable["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                        }else{
                            $errMsg = $updateRevenueTable["errMsg"];
                            $errCode = $updateRevenueTable["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                        }

                    }else{
                        $errMsg = $kwdRevenueDetails["errMsg"];
                        $errCode = $kwdRevenueDetails["errCode"];
                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                    }

                }else{
                    $errMsg = $kwdOwnershipDetails["errMsg"];
                    $errCode = $kwdOwnershipDetails["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                }
            }else{
                $errMsg = "Error: ". $key ." value not found";
                $returnArr = setErrorStack($returnArr, $key, $errMsg, $extraArgs);
            }
        } // end for loop
    }else{
        $errMsg = "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function qualifiedCDP()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   qualifiedCDP()
*   Purpose       :   This function update content consumer qualified searches and earning.
*   Arguments     :   (string) $userId, (string) $email, (string) $searchType, (float) $currentPayout,
*                 :   (json)$getExchangeRate, (integer)$appId, (string) $appName,(object) $searchDbConn
*/

function qualifiedCDP($param, $searchDbConn, $xml_data, $i){

    global $walletURLIPnotification, $walletURLIP, $userRequiredFields, $communityPoolUser, $communityPoolUserId ;

    $returnArr = array();
    $extraArgs = array();
    $currentPayout = $param["current_payout"] ;
    $param["mode_type"] = strtolower($param["mode_type"]);

    /*
      2. Check total search count searched on particular IP.
        - Get max ip limit set by admin from admin_settings table.
      3. Check if logged in user has referral user or not (in cdp cron structure).
      4. Credit user interaction earning using API request.
      5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
         with type = "interaction_earning"
      6. Insert transaction into week wise user_search_history table into MYSQL search database.
      7. Increase logged in user's qualified searches in userdetails @notification server
          - 7.1 Deduct qualified search of logged in user from node side ( on success of increase qualified searches).
      8. Insert new record into content_consumer_list into mysql table of keyword database.
      9. Insert/update qualified search count + 1 into keywo_stats table of keyword MYSQL database.
      10. write CDP process into xml logs.
    */


    if(isset($param) && !empty($param) && isset($searchDbConn) && !empty($searchDbConn)){
        if(count($param) > 0){
            // 3. calculate referral payout amount if referral user exist
            /**** start search referral earning ****/
            $userReqField = "ref_email";
            $getReferralEmail = getUserInfo($param["email"], $walletURLIPnotification.'api/notify/v2/', $userReqField);

            if(noError($getReferralEmail)){
                $getReferralEmail = $getReferralEmail["errMsg"]["ref_email"];

                // if referral user exist then only below code execute
                if(isset($getReferralEmail) && !empty($getReferralEmail)){

                    $referralEmailExist = "Referral Email exist - ".$getReferralEmail;

                    $calculateAffiliatePayout = calculateAffPayout($param["current_payout"], $param["mode_type"]);

                    if(noError($calculateAffiliatePayout)){
                        $calculateAffiliatePayout = $calculateAffiliatePayout["data"];

                        $referralPayout = $calculateAffiliatePayout["refPayoutAmnt"];
                        $currentPayout = $calculateAffiliatePayout["userPayoutAmnt"];
                    }else{
                        $errMsg = $calculateAffiliatePayout["errMsg"];
                        $errCode = $calculateAffiliatePayout["errCode"];
                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                    }
                }else{
                    $referralEmailExist = "Referral email does not exist";
                }

                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. '.$referralEmailExist;

                // 4. Credit user interaction earning @wallet server where 'searchearning' is the field arrtibute and
                //  'add' is the action to perform in @wallet server's table.

                // assign attribute to perform earning
                if($param["mode_type"] == 'search'){
                    $earningAttribute = "searchearning";
                    $transType = "search_earning";
                }else if($param["mode_type"] == 'social'){
                    $earningAttribute = "post_view_earnings";
                    $transType = "post_viewer_earning";
                }

                if(isset($param["share_status"]) && !empty($param["share_status"])){
                    $sharedStatus = $param["share_status"];
                }

                $creaditInteractionEarning = creditUserEarning($param["user_id"], $currentPayout, $earningAttribute, "add");
                if(noError($creaditInteractionEarning)){
                    $creaditInteractionEarning = $creaditInteractionEarning["errMsg"];
                    $totalAvailableBalance = $creaditInteractionEarning["search_earning"];
                    $contentConsumerId = $creaditInteractionEarning["_id"];
                    //  5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
                    //   with type = "interaction_earning"

                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. Success: Credit user interaction earning with current payout amount : '.$currentPayout;

                    // parameters required to insert in transaction

                    $recipientEmail = $param["email"];
                    $recipientUserId = $contentConsumerId;
                    $senderEmail = $communityPoolUser;
                    $senderUserId = $communityPoolUserId;
                    $amount = $currentPayout;
                    $type = $transType;
                    $paymentMode = 'ITD';
                    $originIp = $param["origin_ip"];
                    $exchangeRateInJson = $param["exchange_rate_in_json"];

                    //$metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => urlencode(utf8_encode($param["search_query"])), "discount" => 0, "commision" => 0, "post_type" => $param["post_type"],"post_created_at"=>$param["post_created_at"],"post_created_at"=>$param["post_created_at"],"post_type" => $param["post_type"], "share_status" => $sharedStatus);
                    $metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => $param["search_query"], "discount" => 0, "commision" => 0, "post_type" => $param["post_type"],"post_created_at"=>$param["post_created_at"],"post_created_at"=>$param["post_created_at"],"post_type" => $param["post_type"], "share_status" => $sharedStatus);

                    $insertUsrInteractionTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                    // retrieve transaction time and transaction id on success of interaction earning transaction @wallet server
                    if(noError($insertUsrInteractionTrnasaction)){
                        // retrieving receiver_search_earning transaction id and transaction time.
                        $insertUsrInteractionTrnasaction = $insertUsrInteractionTrnasaction["errMsg"]["committed_transaction"];

                        $transactionId = $insertUsrInteractionTrnasaction["transaction_id"]; // transaction time should get from transaction success response
                        $transactionTime = $insertUsrInteractionTrnasaction["timestamp"]; // transaction time should get from transaction success response.
                        $transactionTime = $transactionTime / 1000; // convert millisecond to seconds
                        $transactionTime = date('Y-m-d H:i:s', $transactionTime);

                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. Success: Insert content consumer interaction earning transaction with transaction id: '.$transactionId.' and transaction time: '.$transactionTime ;

                        // 6. Insert transaction into week wise user_search_history table into MYSQL search database.
                        $insertUsrSearchHistory = insertSearchHistory($param["email"], $param["search_query"], $transType, $currentPayout, $param["exchange_rate_in_json"], $param["app_id"], $transactionId, $transactionTime, $param["origin_ip"], $searchDbConn);
                        if(noError($insertUsrSearchHistory)){

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Success:  Insert transaction into search history with type = "qualified_earning"';

                            //  7. Increase logged in user's qualified searches in userdetails @notification server
                            $increaseUsrQualifiedSearch = increaseUserSearches($param["email"], $param["interaction_type"]);

                            if(noError($increaseUsrQualifiedSearch)){

                                $increaseUsrQualifiedSearch = $increaseUsrQualifiedSearch["errMsg"];
                                // retrive user's total number of interaction pending
                                $totalPendingIneraction = $increaseUsrQualifiedSearch["no_of_qualified_interactions_pending"];
                                if($totalPendingIneraction <= 0 ){
                                    $totalPendingIneraction = 0;
                                }

                                $i = $i+1;
                                $xml_data["step".$i]["data"] = $i.'. Success: Increase logged in user qualified searches, available user total number of pending interaction: '.$totalPendingIneraction;

                                // close search database connection
                                mysqli_close($searchDbConn);

                                $i = $i+1;
                                $xml_data["step".$i]["data"] = $i.'. Success: Close search database connection';

                                // create keyword database connection

                                $keywordDbConn = createDBConnection("dbkeywords");
                                if(noError($keywordDbConn)){
                                    $keywordDbConn = $keywordDbConn["connection"];

                                    $i = $i+1;
                                    $xml_data["step".$i]["data"] = $i.'. Success: Create connection to keyword database';

                                    //  8. Insert new record into content_consumer_list into mysql table of keyword database on success
                                    //     of content consumer earning.

                                    // Note : If search user has referral eamil then referral payout amount will be deducted from search user amount only, not from other 3 users.
                                    // So in content consumer lsit table has total current payout amount.

                                    $insertContentConsumerRecord = addContentConsumerList($transactionId, $transactionTime, $param["email"], $param["user_id"], $param["search_query"], $param["current_payout"], $param["origin_ip"], $param["interaction_type"], $param["content_id"], $param["exchange_rate_in_json"], $param["creator_id"], $param["sharer_id"], $param["cdp_status_from_social"], $param["mode_type"], $keywordDbConn, $param["country"], $param["gender"], $param["browser"], $param["device"], $param["username"], $param["post_type"], $param["post_created_at"]);

                                    if(noError($insertContentConsumerRecord)){

                                        $i = $i+1;
                                        $xml_data["step".$i]["data"] = $i.'. Success: Add record into consumer list, where 1) Consumer email: '.$param["email"].', 2) Search Query: '.$param["search_query"].', 3) Current Payout: '.$param["current_payout"]. ', 4) App Id: '.$param["app_id"].', 5) Origin IP: '.$param["origin_ip"]. ', 6) Transaction Id: '.$transactionId.' 7) Transaction Time: '.$transactionTime;



                                        // add keyword interaction in revenue table
                                        $increaseKwdCntInRevenueTable = addKwdCntInRevenueTable($param["search_query"], $param["interaction_type"], $param["email"], $param["content_id"], $param["post_type"], $param["origin_ip"], $keywordDbConn);

                                        if(noError($increaseKwdCntInRevenueTable)){

                                            $i = $i+1;
                                            $xml_data["step".$i]["data"] = $i.'. Success: Update interaction count in revenue table ';

                                            $increaseKwdCntInAnalyticsTable = addKwdCntInAnalyticsTable($param["search_query"], $param["interaction_type"], $param["post_type"], $param["mode_type"], $keywordDbConn);

                                            if(noError($increaseKwdCntInAnalyticsTable)){

                                                $i = $i+1;
                                                $xml_data["step".$i]["data"] = $i.'. Success: Update interaction count in analytics table';


                                                if(isset($getReferralEmail) && !empty($getReferralEmail)){

                                                    // get referral email user id
                                                    $referralUserField = $userRequiredFields.",user_id";

                                                    $getReferralDetails = getUserInfo($getReferralEmail, $walletURLIP.'api/v3/', $referralUserField);
                                                    if(noError($getReferralDetails)){
                                                        $getReferralUserId = $getReferralDetails["errMsg"]["_id"];

                                                        $i = $i+1;
                                                        $xml_data["step".$i]["data"] = $i.'. Success: Get Referral email user id: '.$getReferralUserId;

                                                        if($param["mode_type"] == "search"){
                                                            $refEarningAttribute = "searchaffearning";
                                                            $refTransactionType = "search_referral_earnings";
                                                        }elseif($param["mode_type"] == "social"){
                                                            $refEarningAttribute = "social_affiliate_earning";
                                                            $refTransactionType = "social_referral_earnings";
                                                        }

                                                        /*Credit search earning to user reference email '%' set by admin */
                                                        $creditSrchAffErn = creditUserEarning($getReferralUserId, $referralPayout, $refEarningAttribute, "add");
                                                        if(noError($creditSrchAffErn)){

                                                            // Insert user transaction for referral user.

                                                            $i = $i+1;
                                                            $xml_data["step".$i]["data"] = $i.'. Success: Credit referral payout amount as '.$referralPayout.' to referral user: '.$getReferralEmail;

                                                            // parameters required to insert in transaction

                                                            // $details = array("reference_transaction_id" => $transactionId, "referral email" => $getReferralEmail, "mode" => "search", "amount" => $referralPayout, "type" => $type, "payment_mode" => $paymentMode, "keyword" => $keyword, "message" => "Search referral earning transaction happens due to search user", "timestamp" => date('Y-m-d H:i:s', time()));

                                                            $recipientEmail = $getReferralEmail;
                                                            $recipientUserId = $getReferralUserId;
                                                            $senderEmail = $communityPoolUser;
                                                            $senderUserId = $communityPoolUserId;
                                                            $amount = $referralPayout;
                                                            $type = $refTransactionType;
                                                            $paymentMode = 'ITD';
                                                            $originIp = $param["origin_ip"];
                                                            $exchangeRateInJson = $param["exchange_rate_in_json"];


                                                          //  $metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => urlencode(utf8_encode($param["search_query"])), "discount" => 0, "commision" => 0, "mode_type" => $param["mode_type"], 'referrer_transaction_id' => $transactionId, "referree_email" =>  $param["email"], "post_type" => $param["post_type"], "share_status" => $sharedStatus);
                                                            $metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => $param["search_query"], "discount" => 0, "commision" => 0, "mode_type" => $param["mode_type"], 'referrer_transaction_id' => $transactionId, "referree_email" =>  $param["email"], "post_type" => $param["post_type"], "share_status" => $sharedStatus);

                                                            $insertReferralUserTransaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                            if(noError($insertReferralUserTransaction)){

                                                                // retrieving receiver_search_earning transaction id and transaction time.
                                                                $insertReferralUserTransaction = $insertReferralUserTransaction["errMsg"]["committed_transaction"];

                                                                $transId = $insertReferralUserTransaction["transaction_id"]; // transaction time should get from transaction success response
                                                                $transTime = $insertReferralUserTransaction["timestamp"];
                                                                $transTime = $transTime / 1000; // convert millisecond to seconds
                                                                $transTime = date('Y-m-d H:i:s', $transTime);

                                                                $i = $i+1;
                                                                $xml_data["step".$i]["data"] = $i.'. Success: Insert user transaction for referral user where Transaction Id: '.$transId.' and Transaction Time: '.$transTime;

                                                                // close keyword database connection
                                                                mysqli_close($keywordDbConn);

                                                                $i = $i+1;
                                                                $xml_data["step".$i]["data"] = $i.'. Success: Close kd database connection';

                                                                // create search database connection
                                                                $searchDbConn = createDBConnection("dbsearch");

                                                                if(noError($searchDbConn)){
                                                                    $searchDbConn = $searchDbConn["connection"];

                                                                    $i = $i+1;
                                                                    $xml_data["step".$i]["data"] = $i.'. Success: Create connection to search database';

                                                                    // insert user search history for referral user.
                                                                    $insertReferralUserSearchHistory = insertSearchHistory($getReferralEmail, $param["search_query"], "search_referral_earnings", $referralPayout, $exchangeRateInJson, $appId, $transId, $transTime, $originIp, $searchDbConn);
                                                                    if(noError($insertReferralUserSearchHistory)){

                                                                        $i = $i+1;
                                                                        $xml_data["step".$i]["data"] = $i.'. Success: insert user search history for referral user';

                                                                        $i = $i+1;
                                                                        $xml_data["step".$i]["data"] = $i.'. Successfully search affiliate earning earned by referral user';

                                                                        $extraArgs["xml_data"] = $xml_data;
                                                                        $errMsg = "Successfully search affiliate earning earned by referral user.";
                                                                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                                                                    }else{
                                                                        $errMsg = "Error: Unable to insert search history for referral user.";
                                                                        $errCode = $insertReferralUserSearchHistory["errCode"];
                                                                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                                                    }
                                                                }else{
                                                                    $errMsg = "Error: search database connection";
                                                                    $errCode = $searchDbConn["errCode"];
                                                                    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                                                }
                                                            }else{
                                                                $errMsg = "Error: Unable to insert user transaction for referral user";
                                                                $errCode = $creditSrchAffErn["errCode"];
                                                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                                            }
                                                        }else{
                                                            $errMsg = "Error: Unable to credit search referral earning to referral user";
                                                            $errCode = $creditSrchAffErn["errCode"];
                                                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                                        }
                                                    }else{
                                                        $errMsg = "Error: Unable to fetch referral user details";
                                                        $errCode = $getReferralDetails["errCode"];
                                                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                                    }
                                                }

                                            }else{

                                                $i = $i+1;
                                                $xml_data["step".$i]["data"] = $i.'. Error: Unable to update interaction count in analytics';

                                                $extraArgs["xml_data"] = $xml_data;
                                                $errMsg = "Error: Unable to get user balance details";
                                                $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);

                                            }

                                        }else{
                                            $i = $i+1;
                                            $xml_data["step".$i]["data"] = $i.'. Error: Unable to update interaction count';

                                            $extraArgs["xml_data"] = $xml_data;
                                            $errMsg = "Error: Unable to get user balance details";
                                            $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);
                                        }

                                        $requiredBalance = $userRequiredFields.",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                                        $getUserTotalBalance = getUserInfo($param["email"], $walletURLIP . 'api/v3/', $requiredBalance);
                                        if(noError($getUserTotalBalance)){

                                            $getUserBalance = calculateUserBalance($getUserTotalBalance);

                                            $getUserTotalBalance       =   $getUserBalance["errMsg"]["total_available_balance"];

                                            // assign total number of pending interacation into an extraArgs array.
                                            $extraArgs["no_of_pending_interaction"] = $totalPendingIneraction;
                                            $extraArgs["total_available_balance"] = number_format($getUserTotalBalance, 4,'.','');
                                            $extraArgs["search_type"] = $param["interaction_type"];
                                            $extraArgs["xml_data"] = $xml_data;
                                            $extraArgs["step_number"] = $i;
                                            $extraArgs["transaction_id"] = $transactionId;
                                            $extraArgs["transaction_type"] = $transType;
                                            $errMsg = "Content consumer earning process done successfully";
                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                                        }else{
                                            $i = $i+1;
                                            $xml_data["step".$i]["data"] = $i.'. Error: Unable to get user balance details';

                                            $extraArgs["xml_data"] = $xml_data;
                                            $errMsg = "Error: Unable to get user balance details";
                                            $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);
                                        }
                                    }else{

                                        $i = $i+1;
                                        $xml_data["step".$i]["data"] = $i.'. Error: Insert record into consumer list';

                                        $extraArgs["xml_data"] = $xml_data;
                                        $errMsg = "Error: Unable to insert record into consumer list";
                                        $returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);
                                    }
                                }else{
                                    $i = $i+1;
                                    $xml_data["step".$i]["data"] = $i.'. Error: Unable to create kd database connection';

                                    $extraArgs["xml_data"] = $xml_data;
                                    $errMsg = "Error: Unable to create keyword database connection";
                                    $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);
                                }
                            }else{

                                $i = $i+1;
                                $xml_data["step".$i]["data"] = $i.'. Error: Increase logged in user qualified searches';

                                $extraArgs["xml_data"] = $xml_data;
                                $errMsg = $increaseUsrQualifiedSearch["errMsg"];
                                $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                            }
                        }else{

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Error:  Add transaction into search history with type = "qualified_earning"';

                            $extraArgs["xml_data"] = $xml_data;
                            $errMsg = $insertUsrSearchHistory["errMsg"];
                            $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                        }
                    }else{

                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. Error: Insert content consumer interaction earning transaction server';

                        $extraArgs["xml_data"] = $xml_data;
                        $errMsg = $insertUsrInteractionTrnasaction["errMsg"];
                        $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                    }
                }else{

                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. Error: Credit user interaction earning with current payout amount : '.$currentPayout;

                    $extraArgs["xml_data"] = $xml_data;
                    $errMsg = $creaditInteractionEarning["errMsg"];
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
            }else{

                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. Error: Unable to get referral user email';

                $extraArgs["xml_data"] = $xml_data;
                $errMsg = "Error: Unable to get referral user email";
                $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
            }
        }else{

            $i = $i+1;
            $xml_data["step".$i]["data"] = $i.'. Error: No data found';

            $extraArgs["xml_data"] = $xml_data;
            $errMsg = "Error: No data found";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{

        $i = $i+1;
        $xml_data["step".$i]["data"] = $i.'. Error:  Mandatory field not found';

        $extraArgs["xml_data"] = $xml_data;
        $errMsg = "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function addContentConsumerList()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   addContentConsumerList()
*   Purpose       :   This function insert content consumer records into content_consumer_list table
*                 :   @MySQL keyword database. This records is used for rest 3 users earning transaction.
*   Arguments     :   (string) $senderEmail, (string) $receiverEmail, (float)$amount, (string)$type,
*                 :    (string) $keyword, (string) $payment_mode, (float)$discount, (string)$originIp
*                 :    (json)$exchangeRateInjson
*/

function addContentConsumerList($transactionId, $transactionTime, $consumerEmail, $userId, $searchQuery, $currentPayout, $originIp, $searchType, $contentId, $exchangeRateInJson, $creatorId, $sharerId, $cdpStatusFromSocial, $modeType, $keywordDbConn, $country, $gender, $browser, $device, $username, $postType,$post_created_at){

    $returnArr = array();
    $extraArgs = array();

    // calculate total payout
    $totalPayout = number_format(($currentPayout * 4), 8);

    $query = "Insert into content_consumer_list (consumer_email, consumer_user_id, wallet_transaction_id, keyword, current_payout, total_payout, origin_ip, interaction_type, content_id, exchange_rate_in_json, post_creator, post_sharer, mode, cdp_status_from_social, country, device, browser, gender, username, post_type,post_created_at)";
    $query .= "values ('".$consumerEmail."', '".$userId."', '".$transactionId."', '".$searchQuery."', ".$currentPayout.",".$totalPayout.", '".$originIp."', '".utf8_decode($searchType)."', '".$contentId."', '". $exchangeRateInJson."','".$creatorId."','".$sharerId."','".$modeType."','".$cdpStatusFromSocial."','".$country."','".$device."','".$browser."','".$gender."','".$username."','".$postType."','".$post_created_at."')";

    //echo $query; die;

    $execQuery = runQuery($query, $keywordDbConn);
    if(noError($execQuery)){
        $errMsg = "Successfully insert records into consumer list";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg = $execQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}


function coinDistribution($cdpReqParam, $searchDbConn, $xml_data, $i){
    $extraArgs = array();
    $returnArr = array();

    if(count($cdpReqParam) > 0){
        if(isset($cdpReqParam) && !empty($cdpReqParam)){

            if($cdpReqParam["interaction_type"] == "Qualified"){
                //qualified process
                if($cdpReqParam["no_of_qualified_interactions_pending"] <= $cdpReqParam['user_interaction_limit'] && $cdpReqParam["no_of_qualified_interactions_pending"] > 0){
                    //Convert miliseconds to seconds
                    $second = round($cdpReqParam["last_hour_search_time"] / 1000);
                    //Calculate time difference in seconds
                    $timeDiff = time() - $second;

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. Time difference of last search - '.$timeDiff. ' seconds';

                    // if time diference less then 1 hour
                    if($timeDiff < 3600){

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. User search timing less than 1 hour';

                        // check number of searches in last hour.
                        if($cdpReqParam["no_of_searches_in_last_hour"] < 10){

                            $i = $i + 1;
                            $xml_data["step".$i]["data"] = $i.'.  User last hour total search less than 10';

                            // Eligible for qualified searches
                            $qualifiedProc = qualifiedCDP($cdpReqParam, $searchDbConn, $xml_data, $i);

                            if(noError($qualifiedProc)){
                                // on success
                                $extraArgs["no_of_pending_interaction"] = $qualifiedProc["no_of_pending_interaction"];
                                $extraArgs["total_available_balance"] = $qualifiedProc["total_available_balance"];
                                $extraArgs["search_type"] = $qualifiedProc["search_type"];
                                $extraArgs["transaction_id"] = $qualifiedProc["transaction_id"];
                                $extraArgs["transaction_type"] = $qualifiedProc["transaction_type"];
                                $extraArgs["xml_data"] = $qualifiedProc["xml_data"];
                                $extraArgs["step_number"] = $qualifiedProc["step_number"];
                                $errMsg = $qualifiedProc["errMsg"];
                                $errCode = $qualifiedProc["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }else{
                                // on failure
                                $errMsg = $qualifiedProc["errMsg"];
                                $errCode = $qualifiedProc["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }

                        }else if($cdpReqParam["no_of_searches_in_last_hour"] > 9){
                            //  Unqualified searches
                            $unqualifiedSearches = unqualifiedSearches($cdpReqParam, $searchDbConn, $xml_data, $i);
                            if(noError($unqualifiedSearches)){
                                // on success
                                $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                                $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                                $extraArgs["search_type"] = $unqualifiedSearches["search_type"];
                                $errMsg = $unqualifiedSearches["errMsg"];
                                $errCode = $unqualifiedSearches["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }else{
                                // on failure
                                $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                                $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                                $errMsg = $unqualifiedSearches["errMsg"];
                                $errCode = $unqualifiedSearches["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }
                        }
                    }else if($timeDiff >= 3600){ // if time difference greater then 1 hour

                        // 1. update logged in user's last_hour_search_time in userdetails @notification server.
                        // 2. Eligible user for qualified searches
                        $updateLastSearchTime = updateLastHourSearchTime($cdpReqParam["user_id"]);
                        if(noError($updateLastSearchTime)){
                            $qualifiedProc = qualifiedCDP($cdpReqParam, $searchDbConn, $xml_data, $i);

                            if(noError($qualifiedProc)){
                                // on success
                                $extraArgs["no_of_pending_interaction"] = $qualifiedProc["no_of_pending_interaction"];
                                $extraArgs["total_available_balance"] = $qualifiedProc["total_available_balance"];
                                $extraArgs["search_type"] = $qualifiedProc["search_type"];
                                 $extraArgs["transaction_id"] = $qualifiedProc["transaction_id"];
                                $extraArgs["transaction_type"] = $qualifiedProc["transaction_type"];
                                $extraArgs["xml_data"] = $qualifiedProc["xml_data"];
                                $extraArgs["step_number"] = $qualifiedProc["step_number"];
                                $errMsg = $qualifiedProc["errMsg"];
                                $errCode = $qualifiedProc["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }else{
                                // on failure
                                $errMsg = $qualifiedProc["errMsg"]; 
                                $errCode = $qualifiedProc["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                            }
                        }else{
                            $errMsg = "Error: ".$updateLastSearchTime["errMsg"];
                            $errCode = $updateLastSearchTime["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                        }
                    }

                }else if($cdpReqParam["no_of_qualified_interactions_pending"] == 0){
                    // unqualified process
                    // if cdpReqParam["search_type"] == "unqualified" then execute the unqualified process

                    $unqualifiedSearches = unqualifiedSearches($cdpReqParam, $searchDbConn, $xml_data, $i);
                    if(noError($unqualifiedSearches)){
                        // on success
                        $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                        $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                        $extraArgs["search_type"] = $unqualifiedSearches["search_type"];
                        $errMsg = $unqualifiedSearches["errMsg"];
                        $errCode = $unqualifiedSearches["errCode"];
                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                    }else{
                        // on failure
                        $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                        $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                        $errMsg = $unqualifiedSearches["errMsg"];
                        $errCode = $unqualifiedSearches["errCode"];
                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                    }
                }
            }else{
                // unqualified process
                $unqualifiedSearches = unqualifiedSearches($cdpReqParam, $searchDbConn, $xml_data, $i);

                if(noError($unqualifiedSearches)){
                    // on success
                    $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                    $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                    $extraArgs["search_type"] = $unqualifiedSearches["search_type"];
                    $errMsg = $unqualifiedSearches["errMsg"];
                    $errCode = $unqualifiedSearches["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                }else{
                    // on failure
                    $extraArgs["xml_data"] = $unqualifiedSearches["xml_data"];
                    $extraArgs["step_number"] = $unqualifiedSearches["step_number"];
                    $errMsg = $unqualifiedSearches["errMsg"];
                    $errCode = $unqualifiedSearches["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                }
            }
        }else{
            $errMsg = "Error: Mandatory field not found";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: Missing request parameters";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}


function unqualifiedSearches($paramArray, $searchDbConn, $xml_data, $i){

    $returnArr = array();
    global $walletURLIPnotification;

    // set search_type = unqaulified
    $paramArray["search_type"] = "Unqualified";

    /*
    1. Update totalUnqualifiedSearches field by 1 into sc_app_details table of corresponding appId.
    2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
    3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
    4. Update week wise user search transaction history table with type = "Unqualified" into mysql search database
    5. Increasing logged in user's unqualified search count into "userdetails" using API (@notification server).
    6. Update unqualified_searches count by 1 into keywo_stats table of corresponding appId (instead of updating
       pool unqualified searches in pool user).
    */

    /* step 1 - 3 process are execute in appSearchCountUpdate() method */
    $updateKeywordSerches = updateAppSearchCount($paramArray["content_id"], $paramArray["app_name"], $paramArray["search_query"], $paramArray["search_type"], $searchDbConn);

    if(noError($updateKeywordSerches)){

        $i = $i+1;
        $xml_data["step".$i]["data"] = $i.'. Success: Update app search count details';

        // 4. Update week wise user search transaction history table with type = "Unqualified" into mysql search database
        // get current timestamp
        $transactionTime = date("Y:m:d H:i:s");

        $insertSearchHistoryTrans = insertSearchHistory($paramArray["email"], $paramArray["search_query"], "Unqualified", $paramArray["current_payout"], $paramArray["exchange_rate_in_json"],  $paramArray["app_id"], $transactionId, $transactionTime, $paramArray["origin_ip"], $searchDbConn);

        if(noError($insertSearchHistoryTrans)){

            $i = $i+1;
            $xml_data["step".$i]["data"] = $i.'. Success: Insert search history with type as unqualified';

            // close search database connection
            mysqli_close($searchDbConn);

            //  5. Increasing logged in user's unqualified search count into "userdetails" using API (@notification server).
            $addUserSearches = increaseUserSearches($paramArray["email"], $paramArray["search_type"]);

            if(noError($addUserSearches)){

                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. Success: Increase user unqualified search count';

                //create keyword database conncection
                $kwdDbConn = createDBConnection("dbkeywords");
                if(noError($kwdDbConn)){
                    $kwdDbConn = $kwdDbConn["connection"];
                    // 6. Update unqualified_searches count by 1 into keywo_stats table of corresponding appId (instead of updating
                    //    pool unqualified searches in pool user).

                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. Success:  Create connection to kwd database';

                    $addKeywoStatsSearches = increaseKeywoStatsSearches($paramArray["search_type"], $kwdDbConn);
                    if(noError($addKeywoStatsSearches)){

                        /* 7. In keyword revenue table ( @ keyword database )increase below given fileds,
                            7.1 Increase total_keyword_unpaid count,
                            7.2 Increae user_kwd_search_count count,
                            7.3 Increase app_kwd_search_count in json value of correponding app Id
                        */

                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. Success:  Increase total number of keywo unqualified searches';
                        //addKwdCntInAnalyticsTable

                        $increaseKwdCntInRevenueTable = addKwdCntInRevenueTable($paramArray["search_query"], $paramArray["search_type"], $paramArray["email"], $paramArray["content_id"], $paramArray["post_type"], $paramArray["origin_ip"], $kwdDbConn);

                        if(noError($increaseKwdCntInRevenueTable)){

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Success: Update keyword revenue details';

                            $extraArgs["xml_data"] = $xml_data;
                            $extraArgs["step_number"] = $i;
                            $extraArgs["search_type"] = $paramArray["search_type"];
                            $errMsg =  "Unqualified search done successfully";
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                        }else{
                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Error: Update keyword revenue details';

                            $extraArgs["xml_data"] = $xml_data;
                            $extraArgs["step_number"] = $i;
                            $errMsg =  $increaseKwdCntInRevenueTable["errMsg"];
                            $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                        }

                        $increaseKwdCntInAnalyticsTable = addKwdCntInAnalyticsTable($paramArray["search_query"], $paramArray["search_type"], $paramArray["post_type"], $kwdDbConn);

                        if(noError($increaseKwdCntInAnalyticsTable)){

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Success: Update keyword analytics details';

                            $extraArgs["xml_data"] = $xml_data;
                            $extraArgs["step_number"] = $i;
                            $extraArgs["search_type"] = $paramArray["search_type"];
                            $errMsg =  "Unqualified search done successfully";
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                        }else{
                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Error: Update keyword analytics details';

                            $extraArgs["xml_data"] = $xml_data;
                            $extraArgs["step_number"] = $i;
                            $errMsg =  $increaseKwdCntInAnalyticsTable["errMsg"];
                            $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                        }

                    }else{
                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. Error: Unable to update stats searches';

                        $extraArgs["xml_data"] = $xml_data;
                        $extraArgs["step_number"] = $i;
                        $errMsg =  "Error: Unable to update stats searches";
                        $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                    }
                }else{
                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. Error: Unable to create kwd database connection';

                    $extraArgs["xml_data"] = $xml_data;
                    $extraArgs["step_number"] = $i;
                    $errMsg =  "Error: Unable to create kwd database connection";
                    $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                }
            }else{
                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. '.$addUserSearches["errMsg"];

                $extraArgs["xml_data"] = $xml_data;
                $extraArgs["step_number"] = $i;
                $errMsg =  $addUserSearches["errMsg"];
                $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
            }
        }else{
            $i = $i+1;
            $xml_data["step".$i]["data"] = $i.'. '.$insertSearchHistoryTrans["errMsg"];

            $extraArgs["xml_data"] = $xml_data;
            $extraArgs["step_number"] = $i;
            $errMsg =  $insertSearchHistoryTrans["errMsg"];
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $i = $i+1;
        $xml_data["step".$i]["data"] = $i.'. '.$updateKeywordSerches["errMsg"];

        $extraArgs["xml_data"] = $xml_data;
        $extraArgs["step_number"] = $i;
        $errMsg =  $updateKeywordSerches["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

// function that update last hour search timestamp
function updateLastHourSearchTime($userId){
    global $walletPublicKey, $mode, $walletURLIPnotification;
    $retArray = array();

    if (isset($userId) && !empty($userId)) {

        $headers = array();

        //create signature
        $apiText = "user_id={$userId}&publicKey={$walletPublicKey}";
        $postFields = "publicKey=" . urlencode($walletPublicKey);
        $apiName = "user/{$userId}/lasthoursearchtime";
        $requestUrl = "{$walletURLIPnotification}api/notify/v2/";
        $curl_type = 'PUT';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

function getContentConsumerDetail($kwdDbConn){
    $returnArr = array();
    $result = array();
    $extraArgs = array();

    $query = "select * from content_consumer_list limit 500";
    $execQuery = runQuery($query, $kwdDbConn);
    if(noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $result[] = $row;
        }

        $extraArgs["data"] = $result;
        $errMsg =  "Successfully fetch consumer list";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg =  "Error: ".$execQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function increaseAppDeveloperEarning($appId, $currentPayout, $searchDbConn){
    $returnArr = array();

    $query = "Update sc_app_details set totalAppEarning = totalAppEarning + ".$currentPayout." where app_id = ".$appId;

    $execQuery = runQuery($query, $searchDbConn);
    if(noError($execQuery)){
        $errMsg =  "Successfully update app developer app earning";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg =  "Error: Unable to update app earning";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function updateKwdAndOwnerEarning($searchQuery, $searchType, $consumerEmail, $currentPayout, $appId, $originIp, $exchangeRateInJson, $kwdDbConn, $walletTransactionId, &$xml_data, $recordId, $cdpStepDone, &$xml_atrr, &$stepCounterKwdOwnership,$postType){

    $returnArr = array();
    $xml_atrribute = array();
    $extraArgs = '';
    global $unOwnedKwdOwnerUserId, $communityPoolUserId, $unownedKwdOwnerUser, $communityPoolUser, $userRequiredFields, $walletURLIP;

    if(isset($searchQuery) && !empty($searchQuery) && isset($searchType) && !empty($searchType) && isset($consumerEmail) && !empty($consumerEmail) && isset($currentPayout) && !empty($currentPayout) && isset($appId) && !empty($appId) && isset($kwdDbConn) && !empty($kwdDbConn)){
        // convert search query into array useing explode method
        $keywordArr = explode(" ", $searchQuery);

        // get last keyword from array to udpate keyword_cdp_status=3 in content_consumer_list table.
        $lastKeywordInArr = end($keywordArr);

        // get total keyword count
        $kwdCount = count($keywordArr);
        // calculate per keyword current payout
        $perKeywordPayout = $currentPayout/$kwdCount;

        $perKeywordPayout = number_format($perKeywordPayout, 8);

        $stepCounterKwdOwnershiploop=0;

        foreach($keywordArr as $key => $keyword){


            $keywordbase64=md5($keyword);

            if(isset($keyword) && !empty($keyword)){
                //get keyword ownership details from keyword_ownership_<first_lette e.g. c > table from keyword database.
                $kwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);


                $response = json_encode($kwdOwnershipDetails["errMsg"]);

                if(noError($kwdOwnershipDetails)){



                    $errMsg = "Success: Getting keyword ownership details.";
                    $stepCounterKwdOwnershiploop++; // Step counter
                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";

                    $kwdOwnershipDetails = $kwdOwnershipDetails["errMsg"];

                    $kwdOwnerEmail    = $kwdOwnershipDetails["buyer_id"];
                    $noOfActiveBids   = $kwdOwnershipDetails["no_of_active_bids"];
                    $highestBidAmount = $kwdOwnershipDetails["highest_bid_amount"];
                    $kwdAskPrice      = $kwdOwnershipDetails["ask_price"];
                    $expireWithinDays = $kwdOwnershipDetails["days_to_expire"];

                    // if keyword owner email is empty then set keyword owner as admin
                    /*if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                        $kwdOwnerEmail = "admin";
                    }*/

                    if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                        $kwdOwnerEmail = "admin";
                    }else{
                        // validate the keyword remainder period if keyword serving reminder period
                        if($expireWithinDays <= 30){
                            $kwdOwnerEmail = "admin";
                        }else{
                            $kwdOwnerEmail  = $kwdOwnerEmail;
                        }
                    }

                    // get keyword revenue details
                    $kwdRevenueDetails = getRevenueDetailsByKeyword($kwdDbConn, $keyword);

                    $response = json_encode($kwdRevenueDetails);

                    if(noError($kwdRevenueDetails)){


                        $errMsg = "Success: Getting keyword revenue details.";
                        $stepCounterKwdOwnershiploop++; // Step counter
                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";

                        $kwdRevenueTableName    = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails      = $kwdRevenueDetails["data"][0];

                        $id = $kwdRevenueDetails["id"];
                        $appKwdOwnershipEarning = $kwdRevenueDetails["app_kwd_ownership_earnings"];
                        $usrKwdOwnershipEarning = $kwdRevenueDetails["user_kwd_ownership_earnings"];

                        /* if(isset($id) && !empty($id)){
                             // update revenue details

                             //update app keyword ownership earnings
                             $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                             if(array_key_exists($appId,$appKwdOwnershipEarning)){
                                 $appKwdOwnershipEarning[$appId] = $appKwdOwnershipEarning[$appId] + $perKeywordPayout;
                             }else{
                                 $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                             }
                             $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                             //update user keyword ownership earnings
                             $usrKwdOwnershipEarning = $usrKwdOwnershipEarning + $perKeywordPayout;

                             $query =  "UPDATE ".$kwdRevenueTableName." SET app_kwd_ownership_earnings='".$appKwdOwnershipEarning."',user_kwd_ownership_earnings='".$usrKwdOwnershipEarning."' WHERE keyword='".$keyword."'";

                         }else{
                             // insert revenue details
                             //insert app keyword ownership earnings
                             $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                             $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                             $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                             //insert into revenue table
                             $query = "INSERT into ".$kwdRevenueTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches) VALUES('".$keyword."','".$kwdOwnerEmail."','','".$appKwdOwnershipEarning."','1','".$perKeywordPayout."','','','')";
                         }*/


                        if(isset($id) && !empty($id)){
                            // update revenue details

                            //update app keyword ownership earnings
                            $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                            if(array_key_exists($appId,$appKwdOwnershipEarning)){
                                $appKwdOwnershipEarning[$appId] = $appKwdOwnershipEarning[$appId] + $perKeywordPayout;
                            }else{
                                $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                            }
                            $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                            //update user keyword ownership earnings
                            $usrKwdOwnershipEarning = $usrKwdOwnershipEarning + $perKeywordPayout;


                        }else{
                            // insert revenue details
                            //insert app keyword ownership earnings
                            $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                            $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                            $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                            //insert into revenue table
                            $query = "INSERT into ".$kwdRevenueTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches,search_startFrom,follow_unfollow) VALUES('".$keyword."','".$kwdOwnerEmail."','','','','','','','','','')";
                            $updateOwnershipEarningInRevenueTable = updateOwnershipEarningInRevenueTable($query, $kwdDbConn);
                            if(!noError($updateOwnershipEarningInRevenueTable)){
                                $errMsg =  "Error: Insert revenue details in  revenue table";
                                $returnArr = setErrorStack($returnArr, 45, $errMsg, $extraArgs);
                            }


                        }



                        $xml_atrribute["current_app_kwd_ownership_earning"] = $appKwdOwnershipEarning;
                        $xml_atrribute["current_user_kwd_ownership_earning"] = $usrKwdOwnershipEarning;

                        $errMsg = "Success:  calcualte current user and app keyword earnings.";
                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";






                        /*  if(noError($updateOwnershipEarningInRevenueTable)){*/


                        if($kwdOwnerEmail == "admin"){

                            // get keyword owner user id using getUserInfo method
                            $kwdOwnerInfo = $userRequiredFields.",user_id,gender,first_name,last_name";

                            $kwdOwnerUserInfo =  getUserInfo($kwdOwnerEmail, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                            $kwdOwnergender = $kwdOwnerUserInfo["gender"];
                            $kwdOwnerNames = $kwdOwnerUserInfo["first_name"]." ".$kwdOwnerUserInfo["last_name"];


                            $xml_atrribute["user"] = $unownedKwdOwnerUser;
                            $xml_atrribute["amount_given_to_user"] = $perKeywordPayout;
                            $xml_atrribute["interaction_type"] = "keyword_ownership_earning";
                            $xml_atrribute["kwd_owner_gender"] = $kwdOwnergender;
                            $xml_atrribute["post_n_app_id"] = $appId;
                            $xml_atrribute["keywordName"] = $keywordbase64;
                            if($kwdOwnerNames=="")
                            {
                                $xml_atrribute["username"]="admin";
                            }else{
                                $xml_atrribute["username"] = $kwdOwnerNames;
                            }
                            $xml_atrribute["number_of_days_left_for_renew"]=$expireWithinDays;


                            $errMsg = "Start {$unownedKwdOwnerUser} coin distribution process.";
                            $stepCounterKwdOwnershiploop++; // Step counter

                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";




                            $startTransaction = startTransaction($kwdDbConn);
                            if(noError($startTransaction)){
                                $addKwdUnownedOwnerIncome = creditUserEarning($unOwnedKwdOwnerUserId, $perKeywordPayout, "kwdincome", "add");


                                $response = json_encode($addKwdUnownedOwnerIncome["errMsg"]);

                                if(noError($addKwdUnownedOwnerIncome)){



                                    $xml_atrribute["response"] = $response;

                                    $errMsg = "Success: credit unowned keyword ownership earning.";
                                    $stepCounterKwdOwnershiploop++; // Step counter

                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";



                                    $addKwdUnownedOwnerIncome = $addKwdUnownedOwnerIncome["errMsg"];
                                    $totalUnownedKwdIncome = number_format($addKwdUnownedOwnerIncome["total_kwd_income"], 8);

                                    $cdpStepDone["keyword"][$keyword]["owner"] = $kwdOwnerEmail;
                                    $cdpStepDone["keyword"][$keyword]["update_user_balance"] = "success";

                                    $updateKwdOwnerJson = json_encode($cdpStepDone);

                                    $updateCDPStepForKwdEarning =  udpateContentConsumer($recordId, "cdp_steps_done = '".$updateKwdOwnerJson."'", $kwdDbConn);
                                    if(noError($updateCDPStepForKwdEarning)){
                                        $commitTransaction = commitTransaction($kwdDbConn);
                                        if(noError($commitTransaction)){

                                            $startTransaction = startTransaction($kwdDbConn);
                                            if(noError($startTransaction)){
                                                // insert user transaction for keywor owner transaction
                                                $recipientEmail = $unownedKwdOwnerUser;
                                                $recipientUserId = $unOwnedKwdOwnerUserId;
                                                $senderEmail = $communityPoolUser;
                                                $senderUserId = $communityPoolUserId;
                                                $amount = $perKeywordPayout;
                                                $type = "keyword_earning";
                                                $paymentMode = 'ITD';
                                                $originIp = $originIp;
                                                $exchangeRateInJson = $exchangeRateInJson;

                                                $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $unownedKwdOwnerUser, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0,"post_type" => $postType);

                                                $insertUnownedEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);



                                                $response = json_encode($insertUnownedEarningTrnasaction["errMsg"]);
                                                if(noError($insertUnownedEarningTrnasaction)){



                                                    $errMsg = "Success: Insert user transaction for unowned keyword ownership earning.";
                                                    $stepCounterKwdOwnershiploop++; // Step counter

                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                                    // credit keyword refund to community pool
                                                    $creditRefundAmtToCommunity = creditUserEarning($communityPoolUserId, $perKeywordPayout, "kwdincome", "add");
                                                    $response = json_encode($creditRefundAmtToCommunity["errMsg"]);
                                                    if(noError($creditRefundAmtToCommunity)){



                                                        $errMsg = "Success: Insert user transaction for unowned keyword ownership earning.";
                                                        $stepCounterKwdOwnershiploop++; // Step counter

                                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";



                                                        // insert transaction for keyword refund amount
                                                        $recipientEmail = $communityPoolUser;
                                                        $recipientUserId = $communityPoolUserId;
                                                        $senderEmail = $unownedKwdOwnerUser;
                                                        $senderUserId = $unOwnedKwdOwnerUserId;
                                                        $amount = $perKeywordPayout;
                                                        $type = "keyword_earning_refund";
                                                        $paymentMode = 'ITD';
                                                        $originIp = $originIp;
                                                        $exchangeRateInJson = $exchangeRateInJson;

                                                        $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" =>  $unownedKwdOwnerUser, "receiver" => $communityPoolUser, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0, "desc" => "The ".$unownedKwdOwnerUser." send ".$perKeywordPayout." ITD as keyword refund amount to ".$communityPoolUser." user.", "post_type" => $postType);

                                                        $insertPoolKwdRefundTransaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                        $response = json_encode($insertPoolKwdRefundTransaction["errMsg"]);
                                                        if(noError($insertPoolKwdRefundTransaction)){



                                                            $errMsg = "Success: Insert user transaction for keyword refund earning.";
                                                            $stepCounterKwdOwnershiploop++; // Step counter

                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                                            $cdpStepDone["keyword"][$keyword]["insert_user_transaction"] = "success";

                                                            $updateTransJson = json_encode($cdpStepDone);

                                                            $udpateCDPStepForKwdTrans = udpateContentConsumer($recordId, "cdp_steps_done = '".$updateTransJson."'", $kwdDbConn);

                                                            $response = json_encode($udpateCDPStepForKwdTrans["errMsg"]);

                                                            if(noError($udpateCDPStepForKwdTrans)){



                                                                $errMsg = "Success: Update cdp step done for insert user transaction as {$updateTransJson}. ";
                                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                                                $commitTransaction = commitTransaction($kwdDbConn);
                                                                if(noError($commitTransaction)){




                                                                    $errMsg = "Success: commit transaction ";
                                                                    $stepCounterKwdOwnershiploop++; // Step counter

                                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;



                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                                    $errMsg =  "Successfully done CDP for keyword earning for keyword: ".$keyword;
                                                                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                }else{



                                                                    $errMsg = "Failed: commit transaction ";
                                                                    $stepCounterKwdOwnershiploop++; // Step counter

                                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                                    $errMsg =  "Error: commit transaction to keywor owner transaction";
                                                                    $returnArr = setErrorStack($returnArr, 10, $errMsg, $extraArgs);

                                                                }
                                                            }else{



                                                                $errMsg = "Failed: Update cdp step done for insert user transaction as {$updateTransJson}. ";
                                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                                $errMsg =  "Error: commit transaction to keywor owner transaction";
                                                                $returnArr = setErrorStack($returnArr, 11, $errMsg, $extraArgs);
                                                            }

                                                        }else{


                                                            $errMsg = "Failed: Unabel to insert transaction for keyword refund earning.";
                                                            $stepCounterKwdOwnershiploop++; // Step counter

                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                            $errMsg =  "Error: Unabel to insert transaction for keyword refund earning";
                                                            $returnArr = setErrorStack($returnArr, 23, $errMsg, $extraArgs);
                                                        }

                                                    }else{


                                                        $errMsg = "Failed: Unabel to credit refund amount to community pool as keyword income ";
                                                        $stepCounterKwdOwnershiploop++; // Step counter

                                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                        $errMsg =  "Error: Unabel to credit refund amount to community pool as keyword income";
                                                        $returnArr = setErrorStack($returnArr, 21, $errMsg, $extraArgs);

                                                    }


                                                    if($keyword == $lastKeywordInArr){

                                                        $udpateKeywordCDPStatus = udpateContentConsumer($recordId, "cdp_status = 3", $kwdDbConn);
                                                        if(noError($udpateKeywordCDPStatus)){
                                                            $commitTransaction = commitTransaction($kwdDbConn);
                                                            if(!noError($commitTransaction)){
                                                                $errMsg =  "Error: commit transaction";
                                                                $returnArr = setErrorStack($returnArr, 22 , $errMsg, $extraArgs);
                                                            }
                                                        }else{
                                                            $errMsg =  "Error: Unable to update keyword cdp status";
                                                            $errCode = $udpateKeywordCDPStatus["errCode"];
                                                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                                                        }
                                                    }

                                                } else{



                                                    $errMsg = "Failed: Insert user transaction for unowned keyword ownership earning.";
                                                    $stepCounterKwdOwnershiploop++; // Step counter

                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                    $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                    $rollbackTransaction = rollbackTransaction($kwdDbConn);
                                                    if(!noError($rollbackTransaction)){
                                                        $errMsg =  "Error: rollback keyword owner transaction";
                                                        $returnArr = setErrorStack($returnArr, 12, $errMsg, $extraArgs);

                                                    }


                                                    $errMsg =  "Error: Unable to insert transaction for unownedKwdOwner@keywo.com user";
                                                    $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);
                                                }
                                            }else{
                                                $errMsg = "Error: start transaction";
                                                $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);
                                            }
                                        }else{
                                            $errMsg = "Error: unable to commit keyword owner earning";
                                            $returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);
                                        }
                                    }else{
                                        $errMsg = "Error: updating cdp step for keyword owner earning";
                                        $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);
                                    }

                                }else{




                                    $errMsg = "Failed: credit unowned keyword ownership earning.";
                                    $stepCounterKwdOwnershiploop++; // Step counter

                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                    $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                    $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                    $rollbackTransaction = rollbackTransaction($kwdDbConn);
                                    if(!noError($rollbackTransaction)){
                                        $errMsg =  "Error: Unable to rollback.";
                                        $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                                    }

                                    $errMsg =  "Error: Unable to update keyword income to unowned keyword owner.";
                                    $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                                }
                            }else{
                                $errMsg =  "Error: start transaction";
                                $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
                            }

                        }else{ // if keywordOwnerEmail == "keywordowner@bitstreet.in" i.e keyword owned user
                            /*
                              1. update keyword income to keyword owner
                              2. Add keyword payout amount to communitpol@keywo.com user.
                              3. Insert user transaction where sender = communitypool@keywo.com and receiver = keyord_owner
                            */

                            $startTransaction = startTransaction($kwdDbConn);
                            if(noError($startTransaction)){

                                // get keyword owner user id using getUserInfo method
                                $kwdOwnerInfo = $userRequiredFields.",user_id,gender,first_name,last_name";

                                $kwdOwnerUserInfo =  getUserInfo($kwdOwnerEmail, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                                if(noError($kwdOwnerUserInfo)){
                                    // retrieve user id
                                    $kwdOwnerUserInfo = $kwdOwnerUserInfo["errMsg"];
                                    $kwdOwnerId = $kwdOwnerUserInfo["_id"];
                                    $kwdOwnerGender = $kwdOwnerUserInfo["gender"];
                                    $keywordOwnerusername = $kwdOwnerUserInfo["first_name"]." ".$kwdOwnerUserInfo["last_name"];

                                    // 1. update keyword income to keyword owner
                                    $addKwdOwnerkeywordIncome = creditUserEarning($kwdOwnerId, $perKeywordPayout, "kwdincome", "add");

                                    $response = json_encode($addKwdOwnerkeywordIncome["errMsg"]);

                                    $xml_atrribute["user"] = $kwdOwnerEmail;
                                    $xml_atrribute["amount_given_to_user"] = $perKeywordPayout;
                                    $xml_atrribute["interaction_type"] = "keyword_ownership_earning";
                                    $xml_atrribute["post_n_app_id"] = $appId;
                                    $xml_atrribute["kwd_owner_gender"] = $kwdOwnerGender;
                                    $xml_atrribute["keywordName"] = $keywordbase64;
                                    $xml_atrribute["response"] = $response;
                                    $xml_atrribute["username"] = $keywordOwnerusername;
                                    $xml_atrribute["number_of_days_left_for_renew"]=$expireWithinDays;

                                    if(noError($addKwdOwnerkeywordIncome)){


                                        $errMsg = "Success: credit keyword payout amount to {$kwdOwnerEmail}.";
                                        $stepCounterKwdOwnershiploop++; // Step counter

                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                        $cdpStepDone["keyword"][$keyword]["owner"] = $kwdOwnerEmail;
                                        $cdpStepDone["keyword"][$keyword]["update_user_balance"] = "success";

                                        $json = json_encode($cdpStepDone);

                                        $udpateCDPStepForKwdEarning = udpateContentConsumer($recordId, "cdp_steps_done = '".$json."'", $kwdDbConn);
                                        if(noError($udpateCDPStepForKwdEarning)){



                                            $errMsg = "Success: Update cdp step done for credit keyword payout amount to keyword owner.";
                                            $stepCounterKwdOwnershiploop++; // Step counter

                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";



                                            $commitTransaction = commitTransaction($kwdDbConn);
                                            if(noError($commitTransaction)){



                                                $errMsg = "Success: commit transaction.";
                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                                // 2. Insert user transaction where sender = communitypool@keywo.com and receiver = keyord_owner
                                                // parameters required to insert in transaction
                                                $addKwdOwnerkeywordIncome = $addKwdOwnerkeywordIncome["errMsg"];

                                                $totKwdIncome = number_format($addKwdOwnerkeywordIncome["total_kwd_income"], 8);

                                                $startTransaction = startTransaction($kwdDbConn);
                                                if(noError($startTransaction)){
                                                    // insert user transaction for keywor owner transaction
                                                    $recipientEmail = $kwdOwnerEmail;
                                                    $recipientUserId = $kwdOwnerId;
                                                    $senderEmail = $communityPoolUser;
                                                    $senderUserId = $communityPoolUserId;
                                                    $amount = $perKeywordPayout;
                                                    $type = "keyword_ownership_earning";
                                                    $paymentMode = 'ITD';
                                                    $originIp = $originIp;
                                                    $exchangeRateInJson = $exchangeRateInJson;

                                                    $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $kwdOwnerEmail, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode,"content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0, "post_type" => $postType);

                                                    $insertKwdOwnerEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                    $response = json_encode($insertKwdOwnerEarningTrnasaction["errMsg"]);

                                                    if(noError($insertKwdOwnerEarningTrnasaction)){



                                                        $errMsg = "Success: Insert user transaction for keyword ownership earning.";
                                                        $stepCounterKwdOwnershiploop++; // Step counter

                                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";


                                                        $cdpStepDone["keyword"]["keyword"]["insert_user_transaction"] = "success";

                                                        $json = json_encode($cdpStepDone);

                                                        $udpateCDPStepForKwdTrans = udpateContentConsumer($recordId, "cdp_steps_done = '".$json."'", $kwdDbConn);
                                                        if(noError($udpateCDPStepForKwdTrans)){


                                                            $errMsg = "Success: Update cdp step for insert transaction as {$json}.";
                                                            $stepCounterKwdOwnershiploop++; // Step counter

                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";



                                                            $commitTransaction = commitTransaction($kwdDbConn);
                                                            if(noError($commitTransaction)){



                                                                $errMsg = "Success: commit transaction.";
                                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                                $insertKwdOwnerEarningTrnasaction = $insertKwdOwnerEarningTrnasaction["errMsg"]["committed_transaction"]["receiver_keyword_earning"];
                                                                $transactionId = $insertKwdOwnerEarningTrnasaction["transaction_id"];
                                                                $transTime = $insertKwdOwnerEarningTrnasaction["timestamp"];
                                                                $transTime = $transTime / 1000; // convert millisecond to seconds
                                                                $transTime = date('Y-m-d H:i:s', $transTime);


                                                                $errMsg =  "Successfully done keyword earning and keyword owner earning coin distribution process";

                                                                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                                                            }else{



                                                                $errMsg = "Success: commit transaction.";
                                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                                $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                                $errMsg =  "Error: Unabel to update cdp step for keyword transaction ";
                                                                $extraArgs["xmlData"]=$xml_data;
                                                                $extraArgs["xmlAttribute"]=$xml_atrr;
                                                                $returnArr = setErrorStack($returnArr, 18, $errMsg, $extraArgs);
                                                            }
                                                        }else{



                                                            $errMsg = "Failed: Update cdp step for insert transaction as {$json}.";
                                                            $stepCounterKwdOwnershiploop++; // Step counter

                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                            $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;

                                                            $errMsg =  "Error: Unabel to update cdp step for keyword transaction ";

                                                            $returnArr = setErrorStack($returnArr, 17, $errMsg, $extraArgs);
                                                        }

                                                        if($keyword == $lastKeywordInArr){

                                                            $udpateKeywordCDPStatus = udpateContentConsumer($recordId, "cdp_status = 3", $kwdDbConn);
                                                            if(noError($udpateKeywordCDPStatus)){
                                                                $commitTransaction = commitTransaction($kwdDbConn);
                                                                if(!noError($commitTransaction)){
                                                                    $errMsg =  "Error: commit transaction";
                                                                    $returnArr = setErrorStack($returnArr, 22 , $errMsg, $extraArgs);
                                                                }
                                                            }else{
                                                                $errMsg =  "Error: Unable to update keyword cdp status";
                                                                $errCode = $udpateKeywordCDPStatus["errCode"];
                                                                $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                                                            }
                                                        }

                                                    }else{



                                                        $errMsg = "Failed: Insert user transaction for keyword ownership earning.";
                                                        $stepCounterKwdOwnershiploop++; // Step counter

                                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                        $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;


                                                        $rollbackTransaction = rollbackTransaction($kwdDbConn);
                                                        if(!noError($rollbackTransaction)){
                                                            $errMsg =  "Error: rollback transaction";

                                                            $returnArr = setErrorStack($returnArr,19 , $errMsg, $extraArgs);
                                                        }

                                                        $errMsg =  "Error: Unable to insert user transaction where sender = communitypool@keywo.com and receiver= ".$kwdOwnerEmail;
                                                        $returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);
                                                    }
                                                }else{
                                                    $errMsg =  "Error: start transaction ";
                                                    $returnArr = setErrorStack($returnArr, 15, $errMsg, $extraArgs);
                                                }

                                            }else{


                                                $errMsg = "Failed: commit transaction.";
                                                $stepCounterKwdOwnershiploop++; // Step counter

                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                                $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                                $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;



                                                $errMsg =  "Error: commit transaction";
                                                $returnArr = setErrorStack($returnArr, 14, $errMsg, $extraArgs);
                                            }
                                        }else{

                                            $xml_atrribute["udpateCDPStepForKwdEarningEmail"] = $kwdOwnerEmail;
                                            $xml_atrribute["udpateCDPStepForKwdEarningResponse"] = $response;

                                            $errMsg = "Failed: Update cdp step done for credit keyword payout amount to keyword owner.";
                                            $stepCounterKwdOwnershiploop++; // Step counter

                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                            $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                            $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                            $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;


                                            $errMsg =  "Error: Unable to update cdp step for keyword earning";
                                            $returnArr = setErrorStack($returnArr, 13, $errMsg, $extraArgs);
                                        }

                                    }else{



                                        $errMsg = "Failed: credit keyword payout amount to {$kwdOwnerEmail}.";
                                        $stepCounterKwdOwnershiploop++; // Step counter

                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                        $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                                        $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;


                                        $errMsg =  "Error: Unable to add keyword income of ". $kwdOwnerEmail." user";
                                        $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);
                                    }
                                }else{

                                    $errMsg =  "Error: Unable to get user info of ". $kwdOwnerEmail." user";
                                    $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                                }

                            }else{
                                $errMsg =  "Error: start transaction";
                                $returnArr = setErrorStack($returnArr, 13, $errMsg, $extraArgs);

                            }
                        }
                    }else{



                        $errMsg = "Failed: Getting keyword revenue details.";
                        $stepCounterKwdOwnershiploop++; // Step counter

                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                        $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                        $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                        $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;


                        $errMsg =  "Error: Unable to get keyword ownership details";
                        $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                    }
                }else{



                    $errMsg = "Failed: Getting keyword ownership details.";
                    $stepCounterKwdOwnershiploop++; // Step counter

                    $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";
                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                    $xml_data["kwywordOwnership{$keywordbase64}"]["attribute"] = $xml_atrribute;

                    $xml_data["kwywordOwnership{$keywordbase64}"]["data"] = '';
                    $xml_data["kwywordOwnership{$keywordbase64}"]["childelement"] = $kwywordOwnershipChild;



                    $errMsg =  "Error: Unable to get keyword ownership details";
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
            }else{
                $errMsg =  "Error: keyord not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
            }

            $kwywordOwnershipChild="";
            $xml_atrr="";
            $stepCounterKwdOwnershiploop=0;
            $xml_atrribute="";


        }
    }else{
        $errMsg =  "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }


    return $returnArr;
}

function calculateAffPayout($currentPayout, $mode){
    $extraArgs = array();
    $returnArr = array();
    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];

        $getKwdAdminSettingDetails = getAdminSettingsFromKeywordAdmin($kwdDbConn);
        if(noError($getKwdAdminSettingDetails)){
            $getKwdAdminSettingDetails = $getKwdAdminSettingDetails["data"];


            if($mode == "search"){
                $searchAffiliatePercent = $getKwdAdminSettingDetails["search_affiliate_percent"];
                // calculate referral payout
                $refPayout = number_format(($currentPayout*$searchAffiliatePercent)/100,8);  // Reference payout
                $userPayout = number_format(($currentPayout - $refPayout),8);   // User current payout
            }elseif($mode == "social"){
                // calculate social referral payout
                $socialAffiliatePercent = $getKwdAdminSettingDetails["social_affiliate_percent"];
                $refPayout = number_format(($currentPayout*$socialAffiliatePercent)/100,8); // social referral payout
                $userPayout = number_format(($currentPayout - $refPayout),8);   // User current payout
            }

            $extraArgs["data"]["refPayoutAmnt"] = $refPayout;
            $extraArgs["data"]["userPayoutAmnt"] = $userPayout;

            $errMsg =  "Successfully calculate user payout amount";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{
            $errMsg =  "Error: Fetching keyword admin setting details";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg =  "Error: Creating keyword database connection";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function deleteContentConsumerFromList($recordId, $kwdDbConn){
    $extraArgs = array();
    $returnArr = array();

    $query = "delete from content_consumer_list where id={$recordId}";
    $execQuery = runQuery($query, $kwdDbConn);

    if(noError($execQuery)){
        $errMsg =  "Successfully delete the content consumer from list after users earning";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg =  "Error: Unable to update consumer list";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function updateUserCDPStatus($id, $status, $kwdConn){
    $extraArgs = array();
    $returnArr = array();

    if($status == 1){
        $fieldName = "keywo_user_cdp_status";
        $status = 1;
    }elseif($status == 2){
        $fieldName = "app_developer_user_cdp_status";
        $status = 2;
    }elseif($status == 3){
        $fieldName = "keyword_cdp_status";
        $status = 3;
    }

    $query = "Update content_consumer_list set {$fieldName} = {$status} where id = {$id}";
    $execQuery = runQuery($query, $kwdConn);
    if(noError($execQuery)){
        $errMsg =  "Record update successfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg =  "Error: Unable to update consumer";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function udpateContentConsumer($id, $fields, $kwdConn){
    $extraArgs = array();
    $returnArr = array();

    $query = "Update content_consumer_list set {$fields} where id = {$id}";
    $execQuery = runQuery($query, $kwdConn);

    if(noError($execQuery)){
        $errMsg =  "Record update successfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        $errMsg =  "Error: Unable to update consumer";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function calculateSharerPayout($currentPayout, $kwdDbConn){
    $extraArgs = array();
    $returnArr = array();

    $getKwdAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
    if(noError($getKwdAdminSetting)){
        $getKwdAdminSetting = $getKwdAdminSetting["data"];
        $postSharerPercent =  $getKwdAdminSetting["post_share_percent"];

        // calculate referral payout
        $sharerPayout = number_format(($currentPayout*$postSharerPercent)/100,8);  // Reference payout
        $contentCreatorPayout = number_format(($currentPayout - $sharerPayout),8);   // User current payout

        $extraArgs["data"]["sharerPayoutAmnt"] = $sharerPayout;
        $extraArgs["data"]["contentCreatorPayoutAmnt"] = $contentCreatorPayout;

        $errMsg =  "Successfully calculate user payout amount";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    }else{

        $errMsg =  'Error: Unable to get admin setting';
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function getUserDetailFromUserId($userId, $requestUrl, $userRequiredFields)
{

    global  $walletPublicKey, $mode;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText      = "user_id={$userId}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
    $postFields   = "user_id=" . $userId . "&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
    // $requestUrl    = strtolower($requestUrl);
    $apiName      = 'user/getuserdetails_temp';
    $curl_type    = 'GET';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function addKwdCntInAnalyticsTable()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   addKwdCntInAnalyticsTable()
*   Purpose       :   This function update app_kwd_search_count, user_kwd_search_count, kwd_total_paid_searches(Earning),
*                 :   into keyword_analytics_<alphabet_first_letter> into keyword db.
*   Arguments     :   (string) $searchQuery, (string) $searchType, (string) $email, (integer) $appId,
*                 :   (string)$clientIp, (object) $kwdDbConn
*/

function addKwdCntInAnalyticsTable($searchQuery, $searchType, $post_type, $modeType, $kwdDbConn){
    $extraArgs = array();
    // 7. In keyword revenue table increase below given fileds,
    //  7.1 Increase total_keyword_unpaid count,
    //  7.2 Increae user_kwd_search_count count,
    //  7.3 Increase app_kwd_search_count in json value of correponding app Id

    $returnArr = array();
    $date = date("d-m-y");
    $year = date('Y');
    $month = lcfirst(date('M'));

    if(isset($searchQuery) && !empty($searchQuery) && isset($searchType) && !empty($searchType) && isset($kwdDbConn) && !empty($kwdDbConn)){

        // convert search query into array useing explode method
        $keywordArr = explode(" ", $searchQuery);


        foreach ($keywordArr as $key => $keyword) {

            //increase keyword count in geotrends database.
            // status = not completed
            //$addKwdCntInGeoDb = updateSearchTrends($keyword, $appId);

            if (isset($keyword) && !empty($keyword)) {

                //get keyword ownership details from keyword_ownership_<first_lette e.g. c > table from keyword database.
                $kwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                if (noError($kwdOwnershipDetails)) {
                    $kwdOwnershipDetails = $kwdOwnershipDetails["data"][0];

                    $kwdOwnerEmail = $kwdOwnershipDetails["buyer_id"];
                    $noOfActiveBids = $kwdOwnershipDetails["no_of_active_bids"];
                    $highestBidAmount = $kwdOwnershipDetails["highest_bid_amount"];
                    $kwdAskPrice = $kwdOwnershipDetails["ask_price"];

                    // if keyword owner email is empty then set keyword owner as admin
                    if (!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)) {
                        $kwdOwnerEmail = "admin";
                    }

                    // get keyword revenue details

                    $kwdAnaliticsDetails = getkeywordAnaliticsDetails($kwdDbConn, $keyword); //echo "<pre>"; print_r($kwdAnaliticsDetails); echo "</pre>";

                    if (noError($kwdAnaliticsDetails)) {
                        $kwdAnalyticsTableName = $kwdAnaliticsDetails["table_name"];
                        $kwdAnaliticsDetails = $kwdAnaliticsDetails["data"][0];
                        $kwdAnaliticsColumnName = $month;
                        $kwdAnaliticsColumn = $kwdAnaliticsDetails[$month];
                        $id = $kwdAnaliticsDetails["id"];

                        if(isset($id) && !empty($id)){

                            // update keyword revenue records
                            $search_interaction = json_decode($kwdAnaliticsColumn, true);

                            if(array_key_exists($date,$search_interaction["analytics"])){
                                $social_interaction = $search_interaction["analytics"];
                                $search_interaction1 = $search_interaction["analytics"][$date];
                            }else{
                                $social_interaction = $search_interaction["analytics"];
                                $search_interaction1 = $search_interaction["analytics"];
                            }

                            //$search_interaction1 = $search_interaction["analytics"][$date][$modeType]; //echo "Testing "; printArr($search_interaction1);
                            //$social_interaction  = $search_interaction["analytics"][$date]; //echo "Testing "; printArr($search_interaction1);

                            if($modeType == 'search')
                            {
                                if(array_key_exists("search_Interaction",$search_interaction1)){

                                    //echo "testingSearch_Interaction ".$search_interaction1;
                                    $search_interaction["analytics"][$date][$modeType]["search_Interaction"] = $search_interaction1["search_Interaction"] + 1;

                                }else{
                                    $searchInteraction = 1; //echo "else ".$search_interaction1;
                                    $search_interaction["analytics"][$date] = array('search'=>array('search_Interaction'=>($searchInteraction),'search_Earning'=>0),'social'=>array($post_type => 0),'social_Earning'=>array($post_type => 0));
                                }
                            }

                            elseif($modeType == 'social')
                            {
                                if(isset($post_type) && !empty($post_type)){
                                    if(array_key_exists($date, $search_interaction["analytics"])){
                                        if(array_key_exists($modeType, $search_interaction["analytics"][$date])){
                                            // echo "exist post type";
                                            if(array_key_exists($post_type, $search_interaction["analytics"][$date][$modeType])){
                                                // echo "exist";
                                                $search_interaction['analytics'][$date][$modeType][$post_type] = $search_interaction["analytics"][$date][$modeType][$post_type] + 1;

                                            }else{
                                                // echo "post not exist";
                                                $search_interaction["analytics"][$date][$modeType][$post_type] = 1;
                                                //$search_interaction["analytics"][$date][$modeType][$post_type] = array('search'=>array('search_Interaction'=>($social_interaction["search"]["search_Interaction"]),'search_Earning'=>($social_interaction["search"]["search_Earning"])),'social'=>array($post_type => ($interactionCount[$modeType][$post_type])),'social_Earning'=>array($post_type => 0));
                                            }
                                        }

                                    }else{
                                        $search_interaction["analytics"][$date] = array('search'=>array('search_Interaction'=>0,'search_Earning'=>0),'social'=>array($post_type => 1),'social_Earning'=>array($post_type => 0));
                                    }

                                }
                            }

                            $appKwdSearchCountJson = json_encode($search_interaction);

                            //update into analytics table
                            $query = "UPDATE " . $kwdAnalyticsTableName . " SET $kwdAnaliticsColumnName ='" . $appKwdSearchCountJson . "' where keyword = '" . $keyword . "'";

                        }

                        else {
                            // insert new keyword revenue record;

                            $searchCount = 0;
                            $appKwdSearchCountJson1 = array ('year'=>$year);
                            $appKwdSearchCountJson1['analytics'][$date] = array('search'=>array('search_Interaction'=>($searchCount),'search_Earning'=>0),'social'=>array($post_type => 1),'social_Earning'=>array($post_type => 0));
                            $appKwdSearchCountJson = json_encode($appKwdSearchCountJson1);

                         $query = "INSERT into " . $kwdAnalyticsTableName . "(keyword,".$kwdAnaliticsColumnName.") VALUES('" . $keyword . "','".$appKwdSearchCountJson."')";
                        }
                        // insert / update keyword search details into keyword_revenue_<first_letter> e.g. keyword_revenue_c in keyword database.
                        $updateAnalyticsTable = updateAnalyticsTable($query,$keyword,$kwdDbConn);
                        if (noError($updateAnalyticsTable)) {
                            $errMsg = $updateAnalyticsTable["errMsg"];
                            $errCode = $updateAnalyticsTable["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                        } else {
                            $errMsg = $updateAnalyticsTable["errMsg"];
                            $errCode = $updateAnalyticsTable["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                        }

                    } else {
                        $errMsg = $kwdAnaliticsDetails["errMsg"];
                        $errCode = $kwdAnaliticsDetails["errCode"];
                        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                    }

                } else {
                    $errMsg = $kwdOwnershipDetails["errMsg"];
                    $errCode = $kwdOwnershipDetails["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                }
            } else {
                $errMsg = "Error: " . $key . " value not found";
                $returnArr = setErrorStack($returnArr, $key, $errMsg, $extraArgs);
            }
        } // end for loop
    } else {
        $errMsg = "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}


function validateAppExistence($appId){

    $returnArr = array();

    $searchDbConn = createDBConnection("dbsearch");
    if(noError($searchDbConn)){
        $searchDbConn = $searchDbConn["connection"];

        $query = "Select * from sc_app_details where app_id=".$appId;

        $execQuery = runQuery($query, $searchDbConn);
        if(noError($execQuery)){
                $res = array();

            while ($row = mysqli_fetch_assoc($execQuery["dbResource"]))

                $res = $row;

            $returnArr["errCode"] = -1;

            $returnArr["errMsg"] = $res;

        }else{
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Error: ".$execQuery["errMsg"];

        }

    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error: Database connection";
    }

    return $returnArr;
}


?>