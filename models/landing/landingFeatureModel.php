<?php
/*
*-----------------------------------------------------------------------------------------------------------
*    Landing Page Model
*-----------------------------------------------------------------------------------------------------------
*
*   Description : This model is used to define method required for landing page.
*/

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getSliderData()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getSliderData()
*   Purpose       :    To retrieve the CMS details and display on landing page.
*   Arguments     :    (string)$conn as connection object.
*   Returns       :    (array) errMsg contains all the retrieve details.
*/

function getSliderData($conn){
  $returnArr = array();
  $extraArgs = array();
  $query = "select slider_image_consumer,slider_image_producer from cms_slider ORDER BY `orders` asc";
  $execQuery = runQuery($query, $conn);

  if(noError($execQuery)){
      $cmsRecords = array();
      while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $cmsRecords[] = $row;
      }

      $errMsg = "Successfully fetch CMS Slider details";
      $extraArgs["data"] = $cmsRecords;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

  }else{
      $errMsg = "Error getting CMS Slider details";
      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getPageData()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getPageData()
*   Purpose       :    To retrieve the CMS PageData details and display on landing page.
*   Arguments     :    (string)$conn as connection object.
*   Returns       :    (array) errMsg contains all the retrieve details.
*/
function getPageData($conn){

  $returnArr = array();
  $extraArgs = array();
  $query = "select page_title,banner_image from cms_LandingPage";

  $execQuery = runQuery($query, $conn);

  if(noError($execQuery)){
      $cmsRecords = array();
      while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $cmsRecords[] = $row;
      }

      $errMsg = "Successfully fetch CMS LandingPage details";
      $extraArgs["data"] = $cmsRecords;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

  }else{
      $errMsg = "Error getting CMS LandingPage details";
      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}
/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getHowToEarn()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getHowToEarn()
*   Purpose       :    To retrieve the CMS HowToEarn details and display on landing page.
*   Arguments     :    (string)$conn as connection object.
*   Returns       :    (array) errMsg contains all the retrieve details.
*/
function getHowToEarn($conn){

  $returnArr = array();
  $extraArgs = array();

  $query = "select id,content_title,content_video_link from cms_howToEarn";
  $execQuery = runQuery($query, $conn);

  if(noError($execQuery)){
      $cmsRecords = array();
      while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $cmsRecords[] = $row;
      }

      $errMsg = "Successfully fetch CMS HowToEarn details";
      $extraArgs["data"] = $cmsRecords;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

  }else{
      $errMsg = "Error getting CMS HowToEarn details";
      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}
/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getFeatureData()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getFeatureData()
*   Purpose       :    To retrieve the CMS FeatureData details and display on landing page.
*   Arguments     :    (string)$conn as connection object.
*   Returns       :    (array) errMsg contains all the retrieve details.
*/
function getFeatureData($conn){

  $returnArr = array();
  $extraArgs = array();

  $query = "select id,content_title,content_video_link from cms_keywoFeatures";
  $execQuery = runQuery($query, $conn);

  if(noError($execQuery)){
      $cmsRecords = array();
      while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $cmsRecords[] = $row;
      }

      $errMsg = "Successfully fetch CMS FeatureData details";
      $extraArgs["data"] = $cmsRecords;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

  }else{
      $errMsg = "Error getting CMS FeatureData details";
      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}
/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getVideoById()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getVideoById()
*   Purpose       :    To retrieve the Video details By id and display on landing page.
*   Arguments     :    (string)$conn as connection object and (String)$id contains id of video with tablename .
*   Returns       :    (array) errMsg contains all the retrieve details.
*/
function  getVideoById($conn,$id){
  $returnArr = array();
  $extraArgs = array();

    /* Get video id and video type from */
    $paramArr = explode('-', $id);
    $ftype = $paramArr[0];
    $id = $paramArr[1];

    if(!empty($ftype)){
        if($ftype == 'earn'){
            $query = "select short_description,long_description from cms_howToEarn where id= $id ";
        }else{
            $query = "select short_description,long_description from cms_keywoFeatures where id= $id ";
        }
    }
  $execQuery = runQuery($query, $conn);

  if(noError($execQuery)){
      $cmsRecords = array();
      while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $cmsRecords[] = $row;
      }

      $errMsg = "Successfully fetch VideoById details";
      $extraArgs["data"] = $cmsRecords;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

  }else{
      $errMsg = "Error getting VideoById details";
      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getKeywoStats
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getKeywoStats()
*   Purpose       :   This function returns Value of columns passed through array .
*                     e.g: total registered users
*   Arguments     :   1.(obj)$conn [connection object] to connect database
*   Returns       :   (int) Total count of each column passed in arguments (array)$columns_arr .
*/

function getKeywoStats($conn) {

      $returnArr = array();
      $res = array();
      $extraArgs = array();

      $query = "select * from keywo_stats";

     $execQuery = runQuery($query, $conn);

      if( noError($execQuery)){
           while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
              $res[] = $row;
           }

          $errMsg = "Successfully : fetch keywo-stats details";
          $extraArgs["data"] = $res;
          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

      }else{
          $errMsg = "Error : Fetching keywo-stats details";
          $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
      }

      return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getFirstPurchaseKeywordSold
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getFirstPurchaseKeywordSold()
*   Purpose       :   This function get the total number of keyword sold from first purchase.
*   Arguments     :   1.(obj)$conn [connection object] to connect database
*   Returns       :   (int) Total count of each column passed in arguments (array)$columns_arr .
*/

function getFirstPurchaseKeywordSold($conn) {

      $returnArr = array();
      $res = array();
      $extraArgs = array();

      $query = "select sum(kwd_sold) as total_keyword_sold from first_purchase_slabs";

     $execQuery = runQuery($query, $conn);


      if( noError($execQuery)){
           while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
              $res[] = $row;
           }

          $errMsg = "Successfully : fetched first purchase sold keywords";
          $extraArgs["data"] = $res;
          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

      }else{
          $errMsg = "Error : Fetching first purchase sold keywords";
          $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
      }

      return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getPoolStats
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getPoolStats()
*   Purpose       :   This function retrieve all pool stats details.
*   Arguments     :   (string) $$communityPoolUser, (string) $userRequiredFields
*/

function getPoolStats(){

    global $communityPoolUser, $userRequiredFields, $mode, $walletPublicKey, $walletURL;

    $returnArr = array();
    $headers = array();
    $userRequiredFields .= ",search_earning,purchases,trade_fees,total_unsold_kwd_refund,social_content_view_earnings";

    if(isset($communityPoolUser) && !empty($communityPoolUser)){
          $errMsg = "Success : Pool user found";
          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

          if(isset($walletPublicKey) && !empty($walletPublicKey)){
                $errMsg = "Success: Key found";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                if(isset($userRequiredFields) && !empty($userRequiredFields)){
                      $errMsg = "Success: Required field found";
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                      $apiText = "email={$communityPoolUser}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
                      $postFields ="email=".$communityPoolUser."&fieldnames={$userRequiredFields}&publicKey=".urlencode($walletPublicKey);
                      $apiName = 'user/userdetails';
                      $requestUrl = "{$walletURL}";
                      $curl_type = 'GET';
                      $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

                      // nonce implementation
                     if($mode == 'production'){
                          $nonce_result = generateSignatureWithNonce($apiText);
                          if(noError($nonce_result)){
                              $nonce_result = $nonce_result['errMsg'];
                              $retArray['errCode'] = -1;
                              //$retArray['errMsg'] = $nonce_result['errMsg'];
                              $headers[] = "x-ts: {$nonce_result['timestamp']}";
                              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
                          }else {
                              $retArray['errCode'] = 2;
                              $retArray['errMsg'] = $nonce_result['errMsg'];
                          }
                      }
                      // end nonce

                      $respArray = curlRequest($curlReqParam, $headers);

                      if (noError($respArray)) {
                          if ($mode == 'production') {
                              $nonce_counter = incrementVectorCounter();
                          }
                          /*$returnArr['errCode'] = -1;
                          $returnArr['errMsg'] = $respArray['errMsg'];
                          $returnArr['mode'] = $mode;*/

                          $errMsg               =   $respArray["errMsg"];
                          $errCode              =   $respArray["errCode"];
                          $extraArgs["mode"]    =   $mode;
                          $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);

                      } else if ($respArray["errCode"] != 73) {
                            if ($mode == 'production') {
                                $nonce_counter = incrementVectorCounter();
                            }
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      } else {
                            $errMsg               =   $respArray["errMsg"];
                            $errCode              =   $respArray["errCode"];
                            $returnArr = setErrorStack($returnArr, $errCode , $errMsg, $extraArgs);
                      }
                }else{
                      $errMsg = "Failure: Required field not found";
                      $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
          } else{
                $errMsg = "Failure: Key not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
    }else{
          $errMsg = "Error : Pool user not found";
          $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function calculateTotalPayout
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   calculateTotalPayout()
*   Purpose       :   To calucalte the total payout.
*   Arguments     :   (array) $totalSearchPayout
*/

function calculateTotalPayout($searchPayout){

    $returnArr = array();

    if(count($searchPayout) > 0){
        $searchPayout = $searchPayout["search_earning"] + $searchPayout["social_content_view_earnings"];
        $searchPayout = $searchPayout * 4;
        $searchPayout = number_format($searchPayout, 4);

        $errMsg = "Success : search payout found";
        $extraArgs["total_payout_given"] = $searchPayout;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    }else{
        $errMsg = "Error : Payout is missing";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function calculateTotalIncome
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   calculateTotalIncome()
*   Purpose       :   To calucalte the total income earned in community pool.
*   Arguments     :   (array) $totalSearchPayout
*/

function calculateTotalIncome($incomeArr){
    $returnArr = array();

    $purchase = number_format($incomeArr["puchase"], 4);
    $trade_fees = number_format($incomeArr["trade_fees"], 4);
    $kwd_unsold_refund = number_format($incomeArr["keyword_unsold_refund"], 4);
    if(count($incomeArr) > 0){
          if(isset($purchase) && !empty($purchase)){
              if(isset($trade_fees) && !empty($trade_fees)){
                    if(isset($kwd_unsold_refund) && !empty($kwd_unsold_refund)){
                          $totalIncomeEarned = $purchase + $trade_fees + $kwd_unsold_refund;
                          $totalIncomeEarned = number_format($totalIncomeEarned, 4);

                          $errMsg = "Success : Calculate total income earned";
                          $extraArgs["total_income_earned"] = $totalIncomeEarned;
                          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                    }else{
                          $errMsg = "Error :  Array third key value is missing ";
                          $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                    }
              }else{
                    $errMsg = "Error :  Array second key value is missing ";
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
              }
          }else{
                $errMsg = "Error :  Array first key value is missing ";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
    }else{
          $errMsg = "Error : empty array";
          $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

?>
