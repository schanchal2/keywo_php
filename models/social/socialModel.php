<?php
/*
 *Function :   getPostCdpStatus()
 *Purpose  :     To check CDP status true or false for perticular user for the post
 *Arguments  :   (string)$accountHandle, (integer)$postId
*/
function getPostCdpStatus($accountHandle, $postId) {
  // print_r($postId); die;
  $returnArr = array();
  $targetDir = "../../json_directory/social/post/".$accountHandle."/";

  //get all files from targetDir directory
  $jsonFiles = dirToArray($targetDir);
  foreach($jsonFiles as $key => $fileContent){
      $jsonData = $targetDir.$fileContent;
      $jsonData = file_get_contents($jsonData);
      $data     = json_decode($jsonData, true);
      foreach($data as $key => $value){
          $val = $value['post_id'];
          // if(in_array($val, $postId)){
          if($val == $postId) {
              $response = array(
                  'postId' => $val,
                  'status' => true,
              );
              $returnArr = array_push($returnArr, $response);
          }
      }
  }
  // print_r($returnArr); die;
  return $returnArr;
}


//get notification data
function getNotificationData($userId, $category, $offsetFrom) {

  global $mode, $NotificationURL, $walletPublicKey;
  $retArray     = array();

  if (isset($userId) && !empty($userId) && isset($category) && !empty($category)) {
    $headers      = array();
    $dataNow      = round(microtime(true) * 1000);
    //create signature
    $apiText      = 'user_id=' . $userId . '&publicKey=' . $walletPublicKey;
    $postFields   = 'publicKey=' . $walletPublicKey . '&category=' . $category;
    if ($offsetFrom != 0) {
        $postFields = 'publicKey=' . $walletPublicKey . '&category=' . $category . '&offset_from=' . $offsetFrom;
    }
    $apiName      = 'v2/inbox/user/' . $userId . '/';
    $requestUrl   = "{$NotificationURL}";
    $curlType     = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
    //print_r($curlReqParam);die;
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


//get links on short description or comment if any user is mentioned
function getLinksOnText($text, $mention, $sessionAccHandle) {

  $count["counter"] = 0;
  $data             = explode(" @", $text);

  if (strpos($data[0], '@') !== false) {
    $firstAccHandle  = explode(" ", explode("@", $data[0])[1])[0];
    if ($matchResArr = my_array_search($mention, "account_handle", $firstAccHandle)) {
      $count["counter"] = 90;
      if ($sessionAccHandle == $firstAccHandle) {
        $text = str_replace("@" . $firstAccHandle, "<a href='../../views/social/privateTimeline.php' target='_blank'>@" . $firstAccHandle . "</a>", $text);
      } else {
        $text = str_replace("@" . $firstAccHandle, "<a href='../../views/social/otherTimeline.php?email=" . base64_encode($matchResArr[0]["email"]) . "' target='_blank'>@" . $firstAccHandle . "</a>", $text);
      }
      unset($data[0]);
    }
  }

  foreach($data as $accHandle) {
    $accHandle       = explode(" ", $accHandle)[0];
    if ($matchResArr = my_array_search($mention, "account_handle", $accHandle)) {
        $count["counter"] = $count["counter"] + 90;
      if ($sessionAccHandle == $accHandle) {
        $text = str_replace("@" . $accHandle, "<a href='../../views/social/privateTimeline.php' target='_blank'>@" . $accHandle . "</a>", $text);
      } else {
        $text = str_replace("@" . $accHandle, "<a href='../../views/social/otherTimeline.php?email=" . base64_encode($matchResArr[0]["email"]) . "' target='_blank'>@" . $accHandle . "</a>", $text);
      }
    }
  }

  $returnArr["text"]    = $text;
  $returnArr["counter"] = $count["counter"];

  return $returnArr;
}


//get specail user & add into json followerwonk
function getSpecialUser() {

  global $mode, $socialUrl, $walletPublicKey;
  $retArray     = array();
  $headers      = array();
  //create signature
  $apiText      = 'publicKey=' . $walletPublicKey . '&flag=' . '2' . '&limit=' . '10' . '&skip=' . '0';
  $postFields   = 'publicKey=' . $walletPublicKey . '&flag=' . '2' . '&limit=' . '10' . '&skip=' . '0';
  $apiName      = 'admin/specialfollowing';
  $requestUrl   = "{$socialUrl}";
  $curlType     = 'GET';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
  // print_r($curlReqParam);die;
  if ($mode == 'production') {
      $nonce_result = generateSignatureWithNonce($apiText);
      if (noError($nonce_result)) {
          $nonce_result = $nonce_result['errMsg'];
          $retArray['errCode'] = -1;
          //$retArray['errMsg'] = $nonce_result['errMsg'];
          $headers[] = "x-ts: {$nonce_result['timestamp']}";
          $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
      } else {
          $retArray['errCode'] = 2;
          $retArray['errMsg'] = $nonce_result['errMsg'];
      }
  }

  $respArray = curlRequest($curlReqParam, $headers);

  if (noError($respArray)) {
      if ($mode == 'production') {
          $nonce_counter = incrementVectorCounter();
      }
      $retArray['errCode'] = -1;
      $retArray['errMsg']  = $respArray['errMsg'];
      $retArray['mode']    = $mode;

  } else if ($respArray["errCode"] != 73) {
      if ($mode == 'production') {
          $nonce_counter = incrementVectorCounter();
      }
      $retArray['errCode'] = $respArray['errCode'];
      $retArray['errMsg']  = $respArray['errMsg'];
  } else {
      $retArray['errCode'] = $respArray['errCode'];
      $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}


//get mention user
function getMentionUser($blockedUserList, $search) {

  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($search) && !empty($search)) {

      $headers      = array();
      //create signature
      $apiText      = 'search_query=' . $search . '&publicKey=' . $walletPublicKey;
      $postFields   = 'publicKey=' . $walletPublicKey .  '&block_list=' . $blockedUserList;
      $apiName      = 'user/mention_directory/search_query/' . $search;
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);die;
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


//get user info by passing account handle
function getUserInfoAccHandle($accHandle, $requestUrl, $userRequiredFields)
{

  global  $walletPublicKey, $mode;
  $retArray = array();
  $headers   = array();

  /* create signature */
  $apiText      = "fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
  $postFields   = "account_handle=" . $accHandle . "&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
  // $requestUrl    = strtolower($requestUrl);
  $apiName      = 'user/userdetails';
  $curl_type    = 'GET';


  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

  /* Check mode type from config (Nonce active or not)*/
  if($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if(noError($nonce_result)) {
      $nonce_result        = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg']  = $nonce_result['errMsg'];
      $headers[]           = "x-ts: {$nonce_result['timestamp']}";
      $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg']  = $nonce_result['errMsg'];
    }
  }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}


//get search result on user name
function getSearchUserResult($searchText, $userId, $lastDataCreateTime) {

    global $mode, $socialUrl, $walletPublicKey;
    $retArray = array();

    if (isset($userId) && !empty($userId) && isset($searchText) && !empty($searchText)) {

        $headers      = array();
        //create signature
        $apiText      = "user_id=" . $userId . "&publicKey=" . $walletPublicKey;
        $postFields   = 'user_id=' . $userId . "&search=" . rawurlencode($searchText) . "&time_from=" . $lastDataCreateTime . '&publicKey=' . $walletPublicKey;
        $apiName      = 'user';
        $requestUrl   = "{$socialUrl}";
        $curlType     = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg']  = $respArray['errMsg'];
            $retArray['mode']    = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "All fields are mandatory";
    }

    return $retArray;
}


//get user private timeline post directly from api
function getUserTimelinePost($userId, $type, $lastDataTime, $viewType, $otherAccHandle, $sessionUserId) {

  global $mode, $socialUrl;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($type) && !empty($type)) {

      $headers      = array();
      if (empty($lastDataTime)) {
        $lastDataTime = round(microtime(true) * 1000);
      }

      $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
      $blockUserArray          = getBlockedUser($targetDirAccountHandler);

      //create signature
      $apiText      = "user_id=" . $userId . "&type=" . $type . '&time=' . $lastDataTime . '&view_type=' . $viewType . "&session_id=" . $sessionUserId . "&blockUserArray=" . json_encode($blockUserArray);
      $postFields   = 'user_id=' . $userId . '&type=' . $type .  '&time=' . $lastDataTime . '&view_type=' . $viewType . "&session_id=" . $sessionUserId . "&blockUserArray=" . json_encode($blockUserArray);

      if ($viewType == "other") {
;
        // Get other user is followed by login user or not
				$followerStatus     = "false";
				$getFollowerDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($otherAccHandle[0]).".json";
				if (file_exists($getFollowerDetails)) {
				$jsondataFollower = file_get_contents($getFollowerDetails);
				$dataFollower     = json_decode($jsondataFollower, true);
					if (in_array($userId, $dataFollower["followers"])) {
						$followerStatus = "true";
					}
				}
        $postFields .= '&follower_status=' . $followerStatus;
      }

      $apiName      = 'post';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);die;
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}

//get post deatils by sending "postId" to api
function getPostDetail($postId, $postTime, $userID, $status) {
  global $mode, $socialUrl;
  $retArray = array();

  if (isset($postId) && !empty($postId) && isset($postTime) && !empty($postTime)) {

      $headers      = array();
      //create signature
      $apiText      = "follow=". $status ."&user_id=". $userID ."&post_id=" . $postId . "&time=" . $postTime;
      $postFields   = "follow=". $status ."&user_id=". $userID ."&post_id=" . $postId . "&time=" . $postTime;
      $apiName      = 'post';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}

//if memcache is empty than get data from fallback api
function getUserHomeTimelinePost($userId, $postType, $postTime, $keyword, $searchText, $blockedUser)
{
  global $mode, $socialUrl;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($postType) && !empty($postType)) {

      $headers      = array();
      //create signature
      $apiText      = 'user_id=' . $userId . '&post_type=' . $postType . '&time=' . $postTime;
      $postFields   = 'user_id=' . $userId . '&post_type=' . $postType . '&time=' . $postTime . '&blockUserArray=' . $blockedUser;
      //search according to keyword
      if (isset($keyword) && !empty($keyword)) {
          if (empty($postTime)) {
              $postTime = round(microtime(true) * 1000);
          }
          $postFields   = 'user_id=' . $userId . '&post_type=' . $postType . '&time=' . $postTime . '&keyword=' . rawurlencode($keyword) . '&blockUserArray=' . $blockedUser;
      }
      //search according to searchtext
      if (isset($searchText) && !empty($searchText)) {
        if (empty($postTime)) {
            $postTime = round(microtime(true) * 1000);
        }
        $postFields = 'user_id=' . $userId . '&post_type=' . $postType . '&time=' . $postTime . '&search_text=' . rawurlencode($searchText) . '&blockUserArray=' . $blockedUser;
      }
      $apiName      = 'post/hometimeline';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


//get trending keywords with respect to type of post
function getTrendingKeywords($currentTime, $postType) {

  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($currentTime) && !empty($currentTime) && isset($postType) && !empty($postType)) {

      $headers      = array();
      //create signature
      $apiText      = "instance=" . $currentTime . "&post_type=" . $postType . "&publicKey=" . $walletPublicKey;
      $postFields   = 'instance=' . $currentTime . "&post_type=" . $postType . '&publicKey=' . $walletPublicKey;
      $apiName      = 'post/trendingkeyword';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


//function to remove post by owner
function removePost($postId, $userId, $type, $postTime)
{
  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($type) && !empty($type) && isset($postTime) && !empty($postTime) && isset($postId) && !empty($postId)) {

      $headers      = array();
      //create signature
      $apiText      = "post_id=" . $postId . "&post_type=" . $type . "&post_time=" . $postTime . "&user_id=" . $userId . "&publicKey=" . $walletPublicKey;
      $postFields   = 'user_id=' . $userId . "&post_type=" . $type . '&publicKey=' . $walletPublicKey . '&time=' . $postTime;
      $apiName      = 'post/' . $postId;
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'DELETE';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


//function to hide post by owner/viewer
function hidePost($postId, $userId, $type, $postTime, $postSharedType, $action)
{
  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($type) && !empty($type) && isset($postTime) && !empty($postTime) && isset($postId) && !empty($postId)) {

      $headers      = array();
      //create signature
      if ($action == "hide") {
        $apiText      = 'post_id=' . $postId . '&post_type=' . $type . '&invisible_to=' . $userId . '&publicKey=' . $walletPublicKey . '&time=' . $postTime;
        $postFields   = 'post_id=' . $postId . '&post_type=' . $type . '&invisible_to=' . $userId . '&publicKey=' . $walletPublicKey . '&time=' . $postTime;
        $apiName      = 'post/update/invisible_to';
      } else if ($action == "unhide") {
        $apiText      = 'post_id=' . $postId . '&post_type=' . $type . '&visible_to=' . $userId . '&publicKey=' . $walletPublicKey . '&time=' . $postTime;
        $postFields   = 'post_id=' . $postId . '&post_type=' . $type . '&visible_to=' . $userId . '&publicKey=' . $walletPublicKey . '&time=' . $postTime;
        $apiName      = 'post/update/visible_to';
      }
      if (!empty($postSharedType)) {
        $postFields = 'post_id=' . $postId . '&post_type=' . $type . '&invisible_to=' . $userId . '&publicKey=' . $walletPublicKey . '&time=' . $postTime . '&parent_post_type=' . $postSharedType;
      }
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'PUT';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;

}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function setPostInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setPostInfo()
*   Purpose       :   User can Create Post
*   Arguments     :   all Types of Post Field
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setPostInfo($posted_by,$post_type,$shortdesc,$keywords,$postscope,$blog_title,$blog_content,$img_file_name,$asset_url, $userDeviceIp, $userDeviceCountry, $userDeviceState, $userDeviceCity, $userDeviveBrowser, $userDeviceType, $userGender,$YoutubeVideoId,$videoDefaultTitle) {
    global $mode, $walletPublicKey, $socialUrl;
    $headers      = array();
    $blog_content = str_replace("&nbsp;", " ", $blog_content);
    $apiName      = 'post/create';
    $requestUrl   = "{$socialUrl}";
    $curlType     = 'POST';

    $apiText="posted_by=$posted_by&post_type=$post_type&post_short_desc=$shortdesc&post_scope=$postscope&publicKey=$walletPublicKey";

    //Sending Signature to api with public key
    $postFields="posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&keywords=".rawurlencode($keywords)."&post_scope=$postscope&blog_title=$blog_title&blog_content=".rawurlencode($blog_content)."&img_file_name=$img_file_name&asset_url=$asset_url&video_thumbnail=$YoutubeVideoId&device=$userDeviceType&user_ip=$userDeviceIp&gender=$userGender&country=$userDeviceCountry&state=$userDeviceState&city=$userDeviceCity&browser=$userDeviveBrowser&video_desc=$videoDefaultTitle&publicKey=$walletPublicKey";

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

return $retArray;

}



/*
*-----------------------------------------------------------------------------------------------------------
*    Function setReportInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setReportInfo()
*   Purpose       :   User can Create Post
*   Arguments     :   all Types of Post Field
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setReportInfo($report_by,$postId,$reportType,$reportText,$reportId,$created_at,$post_type)
{
    global $mode, $walletPublicKey, $socialUrl;
    $headers      = array();
    $apiName      = "post/$postId/report";
    $requestUrl   = "{$socialUrl}";
    $curlType     = 'POST';

    $apiText="post_id=$postId&post_time=$created_at&post_type=$post_type&report_by=$report_by&publicKey=$walletPublicKey";

    $postFields="post_time=$created_at&post_type=$post_type&report_by=$report_by&report_text=$reportText&publicKey=$walletPublicKey";

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
//     printArr($curlReqParam);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}




/*
*-----------------------------------------------------------------------------------------------------------
*    Function updatePostInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updatePostInfo()
*   Purpose       :   User can Create Post
*   Arguments     :   all Types of Post Field
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function updatePostInfo($id, $postTime, $posted_by, $post_type, $shortdesc, $postscope, $postParentType, $blog_title, $blog_content, $img_file_name, $asset_url,$YoutubeVideoId,$videoDefaultTitle)
{

      global $walletPublicKey,$walletPrivateKey,$socialUrl,$walletURLIPnotification;
      $ch = curl_init();
      $returnArr = array();

      $api_request_url = $socialUrl . "post/update/post_details";
      //Creating Signature

      $api_text="time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&publicKey=$walletPublicKey";

      $signature = hash_hmac('sha512', $api_text, $walletPrivateKey);

      switch ($post_type) {
        case "status":
        $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&publicKey=$walletPublicKey&signature=$signature";

        break;
        case "image":
      $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&img_file_name=$img_file_name&publicKey=$walletPublicKey&signature=$signature";

        break;
        case "blog":
      $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&blog_title=$blog_title&blog_content=$blog_content&img_file_name=$img_file_name&publicKey=$walletPublicKey&signature=$signature";

        break;
        case "audio":
      $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&asset_url=$asset_url&publicKey=$walletPublicKey&signature=$signature";

        break;
        case "video":
        $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&asset_url=$asset_url&video_thumbnail=$YoutubeVideoId&video_desc=$videoDefaultTitle&publicKey=$walletPublicKey&signature=$signature";

        break;
        case "share":
        $postFields="post_id=$id&time=$postTime&posted_by=$posted_by&post_type=$post_type&parent_post_type=$postParentType&post_short_desc=".rawurlencode($shortdesc)."&post_scope=$postscope&publicKey=$walletPublicKey&signature=$signature";

        break;
      }

      curl_setopt($ch, CURLOPT_URL, $api_request_url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
      $result = curl_exec($ch);
      $resultJson = json_decode($result,true);
      //get curl response
      $errCode = $resultJson["errCode"];
      $errMsg = $resultJson["errMsg"];
      if($errCode == -1){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $errMsg;
      }else{
      //error message
        $returnArr["errCode"] = $errCode;
        $returnArr["errMsg"] = $errMsg;
      }
      // echo "<pre>";
      // print_r($returnArr);
      // echo "</pre>";
      return $returnArr;
}





/*
*-----------------------------------------------------------------------------------------------------------
*    Function getUserInfoForMultiUser
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   *    Function getUserInfoForMultiUser()
*   Purpose       :  Api request to featch multiple user details from user_id
*   Arguments     : User ids
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

function getUserInfoForMultiUser($email, $requestUrl, $userRequiredFields)
{
  global  $walletPublicKey, $mode;
  $retArray = array();
  $headers   = array();
// echo "useriD : ".$email."<br>";
  /* create signature */
  $apiText      = "userids={$email}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
  $postFields   = "userids=" . $email . "&multiIds=true&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
  // $requestUrl    = strtolower($requestUrl);
  $apiName      = 'user/userdetails';
  $curl_type    = 'GET';


  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

  /* Check mode type from config (Nonce active or not)*/
  if($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if(noError($nonce_result)) {
      $nonce_result        = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg']  = $nonce_result['errMsg'];
      $headers[]           = "x-ts: {$nonce_result['timestamp']}";
      $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg']  = $nonce_result['errMsg'];
    }
  }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}



//update post count call api
function updatePostCount($userId, $count)
{
  global $NotificationURL, $walletPublicKey, $walletPrivateKey;
  $apiUrl     = 'v2/user/' . $userId . '/count/newpost';
  $apiText    = "user_id=" . $userId . "&count=" . $count . "&publicKey=" . $walletPublicKey;
  $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
  $postFields = 'count=' . $count . '&publicKey=' . $walletPublicKey . '&signature=' . $signature;
	$retArray   = array();
	$ch         = curl_init();
  // execute curl request
	curl_setopt($ch, CURLOPT_URL, $NotificationURL . $apiUrl);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
  // curl request result
	$result = curl_exec($ch);
  //close curl
	curl_close($ch);
  //json decode result
	$resultJson = json_decode($result,true);

	return $resultJson;
}

// To give All Users Details present on Server to provide Following List.
function getAllUserforFollowingList() {
  global $walletURLIPnotification, $walletPublicKey, $walletPrivateKey;
  $apiText    = "publicKey=" . urlencode($walletPublicKey);
  $postFields = "publicKey=" . urlencode($walletPublicKey);
  $requestUrl = 'api/notify/v2/';
  $apiName    = 'user/suggestion';
  $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
  $apiUrl     = $walletURLIPnotification.$requestUrl. $apiName.'?'.$postFields.'&signature='.$signature;
  $retArray   = array();
  $ch         = curl_init();
  // execute curl request
  curl_setopt($ch, CURLOPT_URL, $apiUrl );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  // curl request result
  $result = curl_exec($ch);
  //close curl
  curl_close($ch);
  //json decode result
  $resultJson = json_decode($result,true);
  return $resultJson;
}

//update post count call api
function updatePostDisplayCount($postDisplayIds)
{
  global $walletURLIPnotification, $walletPublicKey, $walletPrivateKey;
  $apiUrl     = 'social/v1/post/update/displaycount';
  $apiText    = "post_display_Json=".$postDisplayIds."&publicKey=" . $walletPublicKey;
  $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
  $postFields = 'post_display_Json=' . $postDisplayIds . '&publicKey=' . $walletPublicKey . '&signature=' . $signature;
  $retArray   = array();
  $ch         = curl_init();
  // execute curl request
  curl_setopt($ch, CURLOPT_URL, $walletURLIPnotification . $apiUrl);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
  curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
  // curl request result
  $result = curl_exec($ch);
  //close curl
  curl_close($ch);
  //json decode result
  $resultJson = json_decode($result,true);

  return $resultJson;
}


//Post view count api call
function postViewCount($postId, $postTime)
{
    global $walletURLIPnotification, $walletPublicKey, $walletPrivateKey;
    $apiUrl    = 'social/v1/post/update/viewcount';
    $apiText    = "post_id".$postId."&publicKey=" . $walletPublicKey;
    $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
    $postFields = 'post_id=' . $postId . '&time='.$postTime.'&publicKey=' . $walletPublicKey . '&signature=' . $signature;
    $retArray   = array();

    $ch         = curl_init();
    // execute curl request
    curl_setopt($ch, CURLOPT_URL, $walletURLIPnotification . $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    // curl request result
    $result = curl_exec($ch);
    //close curl
    curl_close($ch);
    //json decode result
    $resultJson = json_decode($result,true);
    return $resultJson;


}

  // Curl helper function
function curl_get($url) {
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
      $return = curl_exec($curl);
      curl_close($curl);
      return $return;
}

/*
*   Function Name :  updatePostActivityCount()
*   Purpose       :  Api request to store like/comment/share details in node database
*   Arguments     :  (Integer) $userId, (Integer) $postId, (String) $action, (timestamp) $postTime
*   Returns       :   (array) $retArray User info array according to required fields passed.
*/

function updatePostActivityCount($userId, $postId, $action, $postTime, $postType)
{
    global $socialUrl, $walletPublicKey, $walletPrivateKey;

    $apiUrl     = 'post/count/'.$action;
    $apiText    = 'post_id='. $postId . '&user_id=' . $userId . '&action=' . $action.'&time=' . $postTime . '&publicKey=' . $walletPublicKey;
    $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
    $postFields = 'post_type=' . $postType . '&post_id=' . $postId . '&user_id=' . $userId . '&action=' . $action . '&time=' . $postTime . '&publicKey=' . $walletPublicKey . '&signature=' . $signature;
    $retArray   = array();
    $ch         = curl_init();
    // execute curl request
    curl_setopt($ch, CURLOPT_URL, $socialUrl . $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
    // curl request result
    $result = curl_exec($ch);
    //close curl
    curl_close($ch);
    //json decode result
    $resultJson = json_decode($result,true);

    return $resultJson;
}


/*
*   Function Name :  getPostActivityCount()
*   Purpose       :  Api request to get like/comment/share details from node db
*   Arguments     :  (Integer) $postId, (String) $action, (timestamp) $postTime
*   Returns       :  (array) $retArray User like/share/comment countaccording to required fields passed.
*/

function  getPostActivityCount($postIds, $postCommentTime){

    global $mode, $socialUrl,  $walletPublicKey;
    $retArray = array();

    if (isset($postIds) && !empty($postIds)) {

        $headers      = array();
        //create signature
        $apiText      = 'post_ids='. $postIds . '&publicKey=' . $walletPublicKey;
        $postFields   = 'post_ids=' . $postIds  . '&publicKey=' . $walletPublicKey . '&commentTime=' . $postCommentTime;
        $apiName      = 'post/activity/count';
        $requestUrl   = "{$socialUrl}";
        $curlType     = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
//         print_r($curlReqParam);die;
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg']  = $respArray['errMsg'];
            $retArray['mode']    = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }
    return $retArray;
}


/*
*   Function Name :  getCommentReplyCount()
*   Purpose       :  Api request to get reply count on comment from node db
*   Arguments     :  (Integer) $postId, (String) $action, (timestamp) $postTime
*   Returns       :  (array) $retArray User like/share/comment countaccording to required fields passed.
*/

function  getCommentReplyCount($postIds, $commentId, $lastReplyTime){

  global $socialUrl, $walletPublicKey, $walletPrivateKey;
// echo "<br>".$postIds; echo "<br>".$action; echo "<br>".$postCommentTime;
  $apiUrl     = 'post/activity/count';
  $apiText    = 'post_ids='. $postIds . '&publicKey=' . $walletPublicKey;
  $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey); // if ($action == 'comment') {
    $postFields = 'post_ids=' . $postIds  . '&publicKey=' . $walletPublicKey . '&signature=' . $signature .  '&comment_id=' . $commentId . '&commentTime=' . $lastReplyTime;
  $apiUrl     = $socialUrl. $apiUrl.'?'.$postFields;
  $retArray   = array();
  $ch         = curl_init();
  // execute curl request
  curl_setopt($ch, CURLOPT_URL, $apiUrl );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  // curl request result
  $result = curl_exec($ch);
  //close curl
  curl_close($ch);
  //json decode result
  $retArray = json_decode($result,true);

  return $retArray;
}




/*
*   Function Name :  ()
*   Purpose       :  Api request to get like/comment/share user's details from node db
*   Arguments     :  (Integer) $postId, (Timestamp) $postTime, (String) $action, (Integer) $lowerLimit
*   Returns       :  (array) $retArray User like/share/comment users list according to required fields passed.
*/

function getUserDetailsOnActivity($postId, $postTime, $action, $lowerLimit, $upperLimit){

    global $socialUrl, $walletPublicKey, $walletPrivateKey;

    $apiUrl     = 'post/activity/'.$action;

    $apiText    = 'action=' .$action. '&limit=' .$upperLimit. '&post_id=' .$postId. '&time='. $postTime .'&publicKey='. $walletPublicKey;

    $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
    $postFields = 'post_id=' . $postId . '&time=' . $postTime . '&action=' . $action . '&lowerLimit=' . $lowerLimit . '&limit=' . $upperLimit . '&publicKey=' . $walletPublicKey . '&signature=' . $signature;
    $apiUrl     = $socialUrl. $apiUrl.'?'.$postFields;

    $retArray   = array();
    $ch         = curl_init();
    // execute curl request
    curl_setopt($ch, CURLOPT_URL, $apiUrl );
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    // curl request result
    $result = curl_exec($ch);
    //close curl

    if(curl_errno($ch)){
        echo 'Request Error:' . curl_error($ch);
    }
    curl_close($ch);
    //json decode result
    $retArray = json_decode($result,true);

    return $retArray;
}

/*
 *Function	: 	createLikeJsonFile()
 *Purpose	: 	To create Like Json file to show like count
 *Arguments	: 	(string) $post_id, (string) $type, (string) $account_handle, (string) $data.
*/
function createLikeJsonFile($post_id, $type, $account_handle, $data)
{
    $returnArr = array();
    global $docRoot;
    $extraArg = array();

    // creating directory to store like post result in json format.
    $thisdir = $docRoot . 'json_directory/social/like/' . $account_handle;

    /* To check folder Existance */
    $folderExist = folderPresenceCheck($thisdir);
    if (noError($folderExist)) {
        $jsonFile = $account_handle . '_like_' . getMultipleofPostID($post_id) . '.json';
        $jsonPath = $thisdir . '/' . $jsonFile;

        /*
            1) check whether file exist or not
            2) if file does not exist then create a new file with account handler name.
        */
        if (!file_exists($jsonPath)) {
            // creating json file with account handle name.

            $json_data = json_encode($data);
            file_put_contents($jsonPath, $json_data);
            $errMsg    = 'Success : Like post json file created';
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            //append data to existing json file
            $content   = file_get_contents($jsonPath);
            $tempArray = json_decode($content, true);

            //check if post type is already exist in json file
            //if type exist append post id to existing post type
            //else create new post type with post id

            if(array_key_exists($type, $tempArray)){
                $newData                      = $data[$type][0];

                array_push($tempArray[$type], $newData);
                $jsonData                       = json_encode($tempArray);

                file_put_contents($jsonPath, $jsonData);
                $errMsg    = $type.'is already exist in file';
                $returnArr = setErrorStack($returnArr,  -1, $errMsg, $extraArg);
            }else{
                $tempArray = array_merge($tempArray,  $data);
                $jsonData  = json_encode($tempArray);

                file_put_contents($jsonPath, $jsonData);
                $errMsg    = 'created new type' .$type.'in joson file';
                $returnArr = setErrorStack($returnArr,  5, $errMsg, $extraArg);
            }
        }
    } else {
        $checkArr['error']      = "Directed Folder Mismatched";
    }
    return $returnArr;
}

/*
 *Function	: 	getLikePostStatus()
 *Purpose	: 	 To check post like status true or false for perticular user for the post
 *Arguments	: 	(string)$accountHandle, (integer)$postId
*/
function getLikePostStatus($accountHandle, $postId, $type) {
    $data      = array();
    $returnArr = array();
    global $jsonPath;
    $targetDir = "{$jsonPath['social']}like/".$accountHandle."/";

    //get all files from targetDir directory
    $jsonFiles = dirToArray($targetDir);
    foreach($jsonFiles as $key => $fileContent) {
        $jsonData = $targetDir . $fileContent;
        $jsonData = file_get_contents($jsonData);
        $data = json_decode($jsonData, true);
        if(isset($data[$type]))
        {
           $data = $data[$type];
         }else
         {
           $data =array();
         }
       

        if(count($data)>0)
        {

        foreach ($data as $key => $value) {
            $newPostId = $value['post_id'];
            if($postId == $newPostId){
                $response = array(
                    'postId' => $postId,
                    'status' =>  true,
                );

                $returnArr = array_push($returnArr, $response);
            }
        }
      }
    }
    return $returnArr;
}


// To Edit OR Create new comment
/*
*-----------------------------------------------------------------------------------------------------------
*    Function creatComment
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   creatComment()
*   Purpose       :   User Can Put or Set Comment On Perticular post
*   Arguments     :   $userId(Int), $postId(Int), $postCreationTime(Int), $commentText(String), $commentId(Int), $replyId(Int)
*   Returns       :   (array) $retArray Comment Info As per set comment.
*/
function creatComment($userId, $postId, $postCreationTime, $commentText, $commentId, $replyId){
  global $walletPublicKey, $mode, $socialUrl;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($postId) && !empty($postId) && isset($postCreationTime) && !empty($postCreationTime) && isset($commentText) && !empty($commentText)) {

      $headers      = array();
      if (empty($lastDataTime)) {
        $lastDataTime = round(microtime(true) * 1000);
      }
      //create signature
      $apiText    = "post_id=" . $postId . "&user_id=" . $userId . "&action=comment&time=" . $postCreationTime . "&publicKey=" . $walletPublicKey;
      $postFields = "post_id=" . $postId . "&user_id=" . $userId . "&comment=" . $commentText . "&time=" . $postCreationTime . "&publicKey=" . $walletPublicKey . "&comment_id=" . $commentId . "&reply_id=" . $replyId;
      $apiName      = 'post/count/comment';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'PUT';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }
  return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function setSharerPost
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setSharerPost()
*   Purpose       :   User can share already shared Post
*   Arguments     :   all Types of Post Field
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setSharerPost($parentPostId, $parentCreatedAt, $childPostId, $childCreatedAt, $userId, $postType, $shortDesc, $postScope, $userDeviceIp, $userDeviceCountry, $userDeviceState, $userDeviceCity, $userDeviveBrowser, $userDeviceType, $userGender, $parentBlockedUser, $keywords) {

  global $walletPublicKey, $socialUrl;

  $ch              = curl_init();
  $returnArr       = array();
  $api_request_url = $socialUrl . "post/create";
  $postFields      = "keywords=" . $keywords . "&blockUserArray=" . $parentBlockedUser . "&parent_post_id=" . $parentPostId . "&child_post_id=" . $childPostId . "&parent_post_time=" . $parentCreatedAt . "&child_post_time=" . $childCreatedAt . "&posted_by=" . $userId . "&post_type=" .$postType . "&post_short_desc=" . rawurlencode($shortDesc) . "&post_scope=" . $postScope . "&device=" . $userDeviceType . "&user_ip=" . $userDeviceIp . "&gender=" . $userGender . "&country=" . $userDeviceCountry . "&state=" . $userDeviceState . "&city=" . $userDeviceCity . "&browser=" . $userDeviveBrowser."&publicKey=" . $walletPublicKey;

  curl_setopt($ch, CURLOPT_URL, $api_request_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
  curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);

  $result     = curl_exec($ch);
  $resultJson = json_decode($result,true);
  //get curl response
  $errCode    = $resultJson["errCode"];
  $errMsg     = $resultJson["errMsg"];
  if ($errCode == -1) {
    $returnArr["errCode"] = -1;
    $returnArr["errMsg"]  = $errMsg;
  } else {
  //error message
    $returnArr["errCode"] = $errCode;
    $returnArr["errMsg"]  = $errMsg;
  }

  return $returnArr;
}


//Get comment list of post
function getCommentData($userId, $postId, $postCreationTime, $lastCommentTime, $limit, $commentId) {
  global $walletURLIPnotification, $walletPublicKey, $walletPrivateKey;
  $apiText    = "action=comment" . "&limit=" . $limit . "&post_id=" . $postId . "&time=" . $postCreationTime . "&publicKey="  .  urlencode($walletPublicKey);
  $postFields = "action=comment" . "&limit=" . $limit . "&post_id=" . $postId . "&time=" . $postCreationTime . "&publicKey="  .  urlencode($walletPublicKey) . "&commentTime=" . $lastCommentTime . "&comment_id=" . $commentId;
  $requestUrl = 'social/v1/post/';
  $apiName    = 'activity/comment';
  $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);
  $apiUrl     = $walletURLIPnotification.$requestUrl. $apiName.'?'.$postFields.'&signature='.$signature;
  $retArray   = array();
  $ch         = curl_init();
  // execute curl request
  curl_setopt($ch, CURLOPT_URL, $apiUrl );
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  // curl request result
  $result = curl_exec($ch);
  //close curl
  curl_close($ch);
  //json decode result
  $resultJson = json_decode($result,true);
  // printArr($resultJson);
  return $resultJson;


  // global $walletPublicKey, $mode, $socialUrl;
  // $retArray = array();

  // if (isset($userId) && !empty($userId) && isset($postId) && !empty($postId) && isset($postCreationTime) && !empty($postCreationTime)) {

  //     $headers      = array();
  //     if (empty($lastDataTime)) {
  //       $lastDataTime = round(microtime(true) * 1000);
  //     }
  //     //create signature
  //     $apiText    = "action=comment" . "&limit=" . $limit . "&post_id=" . $postId . "&time=" . $postCreationTime . "&publicKey="  .  urlencode($walletPublicKey);


  //     // $apiText    = "post_id=" . $postId . "&user_id=" . $userId . "&action=comment&time=" . $postCreationTime . "&publicKey=" . $walletPublicKey;

  //      $postFields = "action=comment" . "&limit=" . $limit . "&post_id=" . $postId . "&time=" . $postCreationTime . "&publicKey="  .  urlencode($walletPublicKey) . "&commentTime=" . $lastCommentTime . "&comment_id=" . $commentId;

  //     // $postFields = "post_id=" . $postId . "&user_id=" . $userId . "&comment=" . $commentText . "&time=" . $postCreationTime . "&publicKey=" . $walletPublicKey . "&comment_id=" . $commentId . "&reply_id=" . $replyId;
  //     $apiName      = 'activity/comment';
  //     $requestUrl   = "{$socialUrl}";
  //     $curlType     = 'GET';
  //     $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
  //     // print_r($curlReqParam);
  //     if ($mode == 'production') {
  //         $nonce_result = generateSignatureWithNonce($apiText);
  //         if (noError($nonce_result)) {
  //             $nonce_result = $nonce_result['errMsg'];
  //             $retArray['errCode'] = -1;
  //             //$retArray['errMsg'] = $nonce_result['errMsg'];
  //             $headers[] = "x-ts: {$nonce_result['timestamp']}";
  //             $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
  //         } else {
  //             $retArray['errCode'] = 2;
  //             $retArray['errMsg'] = $nonce_result['errMsg'];
  //         }
  //     }

  //     $respArray = curlRequest($curlReqParam, $headers);

  //     if (noError($respArray)) {
  //         if ($mode == 'production') {
  //             $nonce_counter = incrementVectorCounter();
  //         }
  //         $retArray['errCode'] = -1;
  //         $retArray['errMsg']  = $respArray['errMsg'];
  //         $retArray['mode']    = $mode;

  //     } else if ($respArray["errCode"] != 73) {
  //         if ($mode == 'production') {
  //             $nonce_counter = incrementVectorCounter();
  //         }
  //         $retArray['errCode'] = $respArray['errCode'];
  //         $retArray['errMsg']  = $respArray['errMsg'];
  //     } else {
  //         $retArray['errCode'] = $respArray['errCode'];
  //         $retArray['errMsg']  = $respArray['errMsg'];
  //     }
  // } else {
  //     $retArray["errCode"] = 2;
  //     $retArray["errMsg"]  = "All fields are mandatory";
  // }
  // return $retArray;
}

  /*
  *-----------------------------------------------------------------------------------------------------------
  *    Function editDeleteComment
  *-----------------------------------------------------------------------------------------------------------
  *   Function Name :   editDeleteComment()
  *   Purpose       :   User can Edit Post
  *   Arguments     :   all Types of Post Field
  *   Returns       :   (array) $retArray User info array according to required fileds passed.
  */
  function editDeleteComment($userId, $postId, $postCreationTime, $commentId, $commentText, $actionType) {
    global $walletURLIPnotification, $walletPublicKey, $walletPrivateKey;
    $apiUrl     = 'social/v1/post/'.$postId.'/comment/'.$commentId.'/'.$actionType;

    $apiText    = "post_id=" . $postId . "&comment_id=" . $commentId . "&action=" . $actionType . "&time=" . $postCreationTime . "&user_id=" . $userId . "&publicKey=" . $walletPublicKey;

    $signature  = hash_hmac('sha512', $apiText, $walletPrivateKey);

    $postFields = "post_id=" . $postId . "&user_id=" . $userId . "&comment_id=" . $commentId . "&time=" . $postCreationTime . "&action=" . $actionType . "&publicKey=" . $walletPublicKey . '&signature=' . $signature;

    if ($actionType == 'update') {
      $postFields .= "&comment=" . rawurlencode($commentText);
    }

    $ch         = curl_init();
    // execute curl request
    curl_setopt($ch, CURLOPT_URL, $walletURLIPnotification . $apiUrl);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
    // curl request result
    $result = curl_exec($ch);
    //close curl
    curl_close($ch);
    //json decode result
    // printArr($result);
    $resultJson = json_decode($result,true);
    return $resultJson;
  }

  /*
  *-----------------------------------------------------------------------------------------------------------
  *    Function getReportInfo
  *-----------------------------------------------------------------------------------------------------------
  *   Function Name :   getReportInfo()
  *   Purpose       :   User can Create Post
  *   Arguments     :   all Types of Post Field
  *   Returns       :   (array) $retArray User info array according to required fileds passed.
  */
  function getReportInfo($report_by,$post_id,$created_at)
  {
        global $walletPublicKey,$walletPrivateKey, $socialUrl, $walletURLIPnotification;
        $ch = curl_init();
        $returnArr = array();

        $api_text="post_id=$post_id&report_by=$report_by&publicKey=$walletPublicKey";
        $signature = hash_hmac('sha512', $api_text, $walletPrivateKey);
        $api_request_url=$socialUrl."post/$post_id/report/$report_by?post_time=$created_at&publicKey=$walletPublicKey&signature=$signature";

        //Sending Signature to api with public key
        curl_setopt($ch, CURLOPT_URL, $api_request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

        // curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
        $result = curl_exec($ch);
        $resultJson = json_decode($result,true);

        //get curl response
        $errCode = $resultJson["errCode"];
        $errMsg = $resultJson["errMsg"];
        if($errCode == -1){
          $returnArr["errCode"] = -1;
          $returnArr["errMsg"] = $errMsg;
        }else{
        //error message
          $returnArr["errCode"] = $errCode;
          $returnArr["errMsg"] = $errMsg;
        }
        return $returnArr;
  }

  /*
  *-----------------------------------------------------------------------------------------------------------
  *    Function setReportInfo
  *-----------------------------------------------------------------------------------------------------------
  *   Function Name :   setReportInfo()
  *   Purpose       :   User can Create Post
  *   Arguments     :   all Types of Post Field
  *   Returns       :   (array) $retArray User info array according to required fileds passed.
  */
  function removePostOfBlockedUser($accountHandlerId,$otherAccountHandlerId)
  {
        global $walletPublicKey,$walletPrivateKey, $socialURL, $walletURLIPnotification;
        $ch = curl_init();
        $returnArr = array();
        // $api_request_url="http://192.168.2.169:4001/social/v1/post/$postId/report/$reportType";
        $api_request_url=$walletURLIPnotification."social/v1/user/block";
        //Creating Signature
        $api_text="user_id=$accountHandlerId&block_id=$otherAccountHandlerId&publicKey=$walletPublicKey";
        $signature = hash_hmac('sha512', $api_text, $walletPrivateKey);
        $postFields="user_id=$accountHandlerId&block_id=$otherAccountHandlerId&publicKey=$walletPublicKey&signature=$signature";

        //Sending Signature to api with public key
        curl_setopt($ch, CURLOPT_URL, $api_request_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
        $result = curl_exec($ch);
        $resultJson = json_decode($result,true);

        //get curl response
        $errCode = $resultJson["errCode"];
        $errMsg = $resultJson["errMsg"];
        if($errCode == -1){
          $returnArr["errCode"] = -1;
          $returnArr["errMsg"] = $errMsg;
        }else{
        //error message
          $returnArr["errCode"] = $errCode;
          $returnArr["errMsg"] = $errMsg;
        }
        return $returnArr;
  }

  /*
   *Function	: 	createBookMarkJsonFile()
   *Purpose	: 	To create Like Json file to show like count
   *Arguments	: 	(string) $post_id, (string) $type, (string) $account_handle, (string) $data.
  */
  function createBookMarkJsonFile($post_id, $type, $account_handle, $data)
  {
      $returnArr = array();
      GLobal $docRoot;

      // creating directory to store like post result in json format.
      $thisdir = $docRoot . 'json_directory/social/bookmark/' . $account_handle;

      /* To check folder Existance */
      $folderExist = folderPresenceCheck($thisdir);
      if (noError($folderExist)) {
          $jsonFile = $account_handle . '_bookmark.json';
          $jsonPath = $thisdir . '/' . $jsonFile;

          /*
              1) check whether file exist or not
              2) if file does not exist then create a new file with account handler name.
          */
          if (!file_exists($jsonPath)) {
              // creating json file with account handle name.

              $json_data = json_encode($data,JSON_PRETTY_PRINT);
              file_put_contents($jsonPath, $json_data);
              $errMsg    = 'Success : Bookmark of post json file created';
              $returnArr = setErrorStack($returnArr, -1, $errMsg, Null);
          } else {
              //append data to existing json file
              $content   = file_get_contents($jsonPath);
              $storeArr = $tempArray = json_decode($content, true);

              //check if post type is already exist in json file
              //if type exist append post id to existing post type
              //else create new post type with post id

              if(array_key_exists($type, $tempArray)){
                  $newData                      = $data[$type][0];
                  $flag = 1;
                  foreach ($tempArray[$type] as $key => $value) {
                      if ($value['post_id'] == $newData['post_id'])
                      {
                          $set = array_splice($tempArray[$type], $key, 1);
                          $flag = 0;
                      }
                   }
                  if($flag == 0) {
                    $jsonData                       = json_encode($tempArray,JSON_PRETTY_PRINT);
                    file_put_contents($jsonPath, $jsonData);
                    $errMsg    = $type.$newData['post_id'].' is removed from file';
                    $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
                  }else{
                    array_push($tempArray[$type], $newData);
                    $jsonData                       = json_encode($tempArray,JSON_PRETTY_PRINT);
                    file_put_contents($jsonPath, $jsonData);
                    $errMsg    = $type.' is already exist in file';
                    $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
                  }
              }else{
                  $tempArray = array_merge($tempArray,  $data);
                  $jsonData  = json_encode($tempArray,JSON_PRETTY_PRINT);

                  file_put_contents($jsonPath, $jsonData);
                  $errMsg    = 'created new type' .$type.'in joson file';
                  $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
              }
          }
      } else {
          $checkArr['error']      = "Directed Folder Mismatched";
      }
      return $returnArr;
  }

  /*
   *Function	: 	getBookMarkPost()
   *Purpose	: 	 To check post like status true or false for perticular user for the post
   *Arguments	: 	(string)$accountHandle, (integer)$postId
  */
  function getBookMarkPost($accountHandle, $postId, $type) {
    global $docRoot;
      $returnArr = array();

      $targetDir = $docRoot."json_directory/social/bookmark/".$accountHandle."/".$accountHandle."_bookmark".".json";

     $isExists=file_exists($targetDir);

      if($isExists)
      {
      //get all files from targetDir directory
          $jsonData = file_get_contents($targetDir);

          $data = json_decode($jsonData, true);
          $data = isset($data[$type])?$data[$type]:"";

         if(!empty($data))
         {
          foreach ($data as $key => $value) {
              $newPostId = $value['post_id'];
              if($postId == $newPostId){
                  $response = array(
                      'postId' => $postId,
                      'status' =>  true
                  );
                  $returnArr = array_push($returnArr, $response);
              }

          }
        }
      }
      return $returnArr;
  }

  //get user Special user details
  function getSpecialUserPost()
  {
    global $walletURLIPnotification,$walletPublicKey;
    $apiUrl     = 'social/v1/special_admirers/followings';
  	$retArray   = array();
  	$ch         = curl_init();
    // execute curl request
  	curl_setopt($ch, CURLOPT_URL, $walletURLIPnotification . $apiUrl . '?publicKey='.$walletPublicKey);
  	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    // curl request result
  	$result = curl_exec($ch);
    //close curl
  	curl_close($ch);
    //json decode result
  	$resultJson = json_decode($result,true);
  	return $resultJson;
  }

    /*------------------------------------------------------------------------------------------------------------
    *       Function resetLiveCount()
    *-------------------------------------------------------------------------------------------------------------
    *     Function Name :   resetLiveCount()
    *     Purpose       :   To reset fields for social purpose when user logout's.
    *     Arguments     :   (integer) $userId.
    *     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
    *                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
    */

    function resetLiveCount($userId) {

      global $walletPublicKey, $socialUrl;

      $retArray = array();

      if (!empty($userId)) {

        $headers      = array();
        $apiText      = "user_id={$userId}&publicKey={$walletPublicKey}";
        $postFields   = "user_id=".$userId."&publicKey=" . urlencode($walletPublicKey);
        $apiName      = 'user/resetlivecount';
        $requestUrl   = "{$socialUrl}";
        $curl_type    = 'PUT';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
        $respArray    = curlRequest($curlReqParam, $headers);
        // print_r($curlReqParam); die;
      } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "User not found";
      }

    return $retArray;
    }

/*------------------------------------------------------------------------------------------------------------
*       Function getOtherPostWidget()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getOtherPostWidget()
*     Purpose       :   To Retrive all popular post Details
*     Arguments     :   (integer) $userId.
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function getOtherPostWidget($userId, $postId, $postCreationTime, $type, $keywords, $limit, $userSessionId, $userJsonFile) {

  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($type) && !empty($type) && isset($postId) && !empty($postId) && isset($postCreationTime) && !empty($postCreationTime) && isset($keywords) && !empty($keywords)) {

      $headers      = array();

      $apiText      = "post_id=" . $postId . "&post_owner_id=" . $userId . '&post_type=' . $type . '&post_time=' . $postCreationTime . '&user_id=' . $userSessionId . '&limit=' . $limit . '&publicKey=' . $walletPublicKey;

      $postFields   = 'time=' . $postCreationTime . '&limit=' . $limit .  '&keywords=' . $keywords . "&post_owner_id=" . $userId .  '&user_id=' . $userSessionId .  '&accountHandleFile=' . $userJsonFile . '&publicKey=' . $walletPublicKey;

      $apiName      = 'post/' . $postId . '/posted_by/' . $userId . '/type/' . $type;
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


/*------------------------------------------------------------------------------------------------------------
*       Function getRelatedPostDetails()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getRelatedPostDetails()
*     Purpose       :   To Retrive all Related post Details
*     Arguments     :   (integer) $userId.
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/
function getRelatedPostDetails($handlerName, $type, $postId, $postCreationTime, $keywords, $limit) {
  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($handlerName) && !empty($handlerName) && isset($type) && !empty($type) && isset($postId) && !empty($postId) && isset($postCreationTime) && !empty($postCreationTime) && isset($keywords) && !empty($keywords)) {

      $headers      = array();

      $apiText      = "post_id=" . $postId . "&account_handle=" . $handlerName . '&post_type=' . $type . '&post_time=' . $postCreationTime . '&limit=' . $limit . '&publicKey=' . $walletPublicKey;

      $postFields   = 'time=' . $postCreationTime . '&limit=' . $limit .  '&account_handle=' . $handlerName . "&keywords=" . $keywords . '&publicKey=' . $walletPublicKey;

      $apiName      = 'post/' . $postId . '/account_handle/' . $handlerName . '/type/' . $type;
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getPopularPostDetails()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getPopularPostDetails()
*     Purpose       :   To Retrive all Related post Details
*     Arguments     :   (integer) $userId.
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/
function getPopularPostDetails($type) {
  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($type) && !empty($type)) {

      $headers      = array();

      $apiText      = 'post_type=' . $type . '&publicKey=' . $walletPublicKey;

      $postFields   = 'post_type=' . $type . '&publicKey=' . $walletPublicKey;

      $apiName      = 'post/popularpost';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }
  return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getYoutubeIdFromUrl()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getYoutubeIdFromUrl()
*     Purpose       :   To Retrive Youtube Video Id
*     Arguments     :   (integer) $userId.
*     Response      :   Id(alphanumeric)
*/
function getYoutubeIdFromUrl($url) {
  if (preg_match('/youtube\.com\/watch\?v=([^\&\?\/]+)/', $url, $id)) {
    $values = $id[1];
    $finalImageUrl = "https://img.youtube.com/vi/".$values."/0.jpg";
  } else if (preg_match('/youtube\.com\/embed\/([^\&\?\/]+)/', $url, $id)) {
    $values = $id[1];
    $finalImageUrl = "https://img.youtube.com/vi/".$values."/0.jpg";
  } else if (preg_match('/youtube\.com\/v\/([^\&\?\/]+)/', $url, $id)) {
    $values = $id[1];
    $finalImageUrl = "https://img.youtube.com/vi/".$values."/0.jpg";
  } else if (preg_match('/youtu\.be\/([^\&\?\/]+)/', $url, $id)) {
    $values = $id[1];
    $finalImageUrl = "https://img.youtube.com/vi/".$values."/0.jpg";
  } else if (preg_match('/youtube\.com\/verify_age\?next_url=\/watch%3Fv%3D([^\&\?\/]+)/', $url, $id)) {
      $values = $id[1];
      $finalImageUrl = "https://img.youtube.com/vi/".$values."/0.jpg";
  } else {
  $asset_url = str_replace("https://player.vimeo.com/video/", " ", "$url");
  $finalImageUrl = video_image($asset_url);
  }
  return $finalImageUrl;
}

// Find vimeo thumbnil image
function video_image($url){
  $image_url = parse_url($url);
  $ch = curl_init('http://vimeo.com/api/v2/video/'.substr($image_url['path'], 1).'.php');
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
  $a = curl_exec($ch);
  $hash = unserialize($a);
  return $hash[0]["thumbnail_large"];
}



/*------------------------------------------------------------------------------------------------------------
*       Function getYoutubeIdFromUrl()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getYoutubeIdFromUrl()
*     Purpose       :   To Retrive Youtube Video Id
*     Arguments     :   (integer) $userId.
*     Response      :   Id(alphanumeric)
*/
function getYoutubeTitleFromUrl($url) {
    $returnArray = array();
  $video_id = getYoutubeIdFromUrl($url);
  $youTubeDetails = explode("/",$video_id);
  $videoId = $youTubeDetails[4];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://www.youtube.com/oembed?url=http://www.youtube.com/watch?v='.$videoId.'&format=json');
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $response = curl_exec($ch);
    curl_close($ch);
    if ($response) {
        $result =json_decode($response, true);
        return  $result['title'];
    } else {
        return error_get_last();
    }
  return $result['title'];
}


// Find vimeo thumbnil image
function getVimeoDescription($video_url){
    $oembed_endpoint = 'http://vimeo.com/api/oembed';
// Create the URLs
    $xml_url = $oembed_endpoint . '.xml?url=' . rawurlencode($video_url) . '&width=640';
// Curl helper function
    function getVimeoDescriptionCurl($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }

    $oembed = simplexml_load_string(getVimeoDescriptionCurl($xml_url));
    return $oembed->description;
}


/*------------------------------------------------------------------------------------------------------------
*       Function getActivityCommentData()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getActivityCommentData()
*     Purpose       :   To Retrive User's Liked Post detailes
*     Arguments     :   (integer) $userId.
*     Response      :   returnArray(array)
*/
function getActivityCommentHiddenPostData($userId, $LastCommentTime,$type,$blockUserArray) {

  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($LastCommentTime) && !empty($LastCommentTime)) {

      $headers      = array();
      //create signature  user_id, start_from
      $apiText      = "user_id=" . $userId . "&publicKey=". $walletPublicKey;
      $postFields   = "user_id=" . $userId . "&toDate=" . $LastCommentTime . "&blockUserArray=" . $blockUserArray . "&publicKey=". $walletPublicKey;
      $apiName      = 'user/activity/' . $type;
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      //print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getActivityBookMarkPosts()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getActivityBookMarkPosts()
*     Purpose       :   To Retrive User's Liked Post detailes
*     Arguments     :   (integer) $userId.
*     Response      :   returnArray(array)
*/
function getActivityBookMarkLikePosts($postData, $blockUserArray) {
  // printArr($postData);

  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($postData) && !empty($postData)) {

      $headers      = array();
      //create signature
      $apiText      = "publicKey=". $walletPublicKey;
      $postFields   = "posts=" . $postData . "&blockUserArray=" . $blockUserArray . "&publicKey=". $walletPublicKey;
      $apiName      = 'user/activity';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'GET';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;
}


function getCategoryandLimit()
{
global $walletPublicKey, $mode, $socialUrl;
$requestUrl = $socialUrl;
$retArray = array();
$headers = array();
$apiText = "publicKey={$walletPublicKey}";
$postFields = "publicKey=" . $walletPublicKey;
$apiName = 'admin/report_category';
$curl_type = 'GET';
$curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

if ($mode == 'production') {
$nonce_result = generateSignatureWithNonce($apiText);
if (noError($nonce_result)) {
$nonce_result = $nonce_result['errMsg'];
$retArray['errCode'] = -1;
//$retArray['errMsg'] = $nonce_result['errMsg'];
$headers[] = "x-ts: {$nonce_result['timestamp']}";
$headers[] = "x-cnonce: {$nonce_result['cnonce']}";
} else {
$retArray['errCode'] = 2;
$retArray['errMsg'] = $nonce_result['errMsg'];
}
}

$respArray = curlRequest($curlReqParam, $headers);

if (noError($respArray)) {
if ($mode == 'production') {
$nonce_counter = incrementVectorCounter();
}
$retArray['errCode'] = -1;
$retArray['errMsg'] = $respArray['errMsg'];
$retArray['mode'] = $mode;

} else if ($respArray["errCode"] != 73) {
if ($mode == 'production') {
$nonce_counter = incrementVectorCounter();
}
$retArray['errCode'] = $respArray['errCode'];
$retArray['errMsg'] = $respArray['errMsg'];
} else {
$retArray['errCode'] = $respArray['errCode'];
$retArray['errMsg'] = $respArray['errMsg'];
}


return $retArray;
}

function getBookmarkPostIds($account_handle,$technique) {
  global $rootUrl;
  $thisdir = $rootUrl . 'json_directory/social/bookmark/' . $account_handle;
  $jsonFile = $account_handle . '_bookmark.json';
  $jsonPath = $thisdir . '/' . $jsonFile;
  if (!file_exists($jsonPath)) {
    $content   = file_get_contents($jsonPath);
    $storeArr  = json_decode($content, true);
    $newStoreArr = array();
    foreach ($storeArr as $key => $value) {
      if (count($value) > 0) {
        foreach ($value as $key1 => $value1) {
          $newStoreArr[] = $value1;
          // $a[] =     $value1['action_at'];
        }
      }
    }
    if (!empty($technique)) {
      usort($newStoreArr, "comp");
    }
    // printArr($newStoreArr); die();
    $returnArr['errCode'] = -1;
    $returnArr['errMsg'] = $newStoreArr;
  } else {
    $returnArr['errCode'] = 55;
    $returnArr['errMsg'] = "File Does Not Exist";
  }
  // printArr($returnArr); die();
  return $returnArr;
}

function getLikePostIds($account_handle, $technique) {
    GLobal $docRoot;
    $targetDir = $docRoot ."/json_directory/social/like/".$account_handle."/";
    $filesOfLike = dirToArray($targetDir);
    $newLikeArray = array();
  //$newLikeArray1 = array();
  foreach($filesOfLike as $key => $fileOfLikeDetails)
  {
    $Final= $targetDir.$fileOfLikeDetails;
    $jsonOfLiked = file_get_contents($Final);
    $data = json_decode($jsonOfLiked, true);
    foreach ($data as $key => $value) {
      foreach ($value as $key1 => $value1) {
        $newLikeArray[] = $value1;
        // $newLikeArray1[] = $value1['like_at'];
      }
    }
  }
  if (!empty($technique)) {
    usort($newLikeArray, "comp");
  }
  return $newLikeArray;
}

function comp($a, $b)
{
    return $b['action_at'] - $a['action_at'];
}

function getPostLikeIds($account_handle, $technique, $postType) {
    GLobal $docRoot;
    $targetDir = $docRoot ."/json_directory/social/post/".$account_handle."/";
    $filesOfLike = dirToArray($targetDir);
    $newLikeArray = array();
    foreach($filesOfLike as $key => $fileOfLikeDetails)
    {
        $Final= $targetDir.$fileOfLikeDetails;
        $jsonOfLiked = file_get_contents($Final);
        $data = json_decode($jsonOfLiked, true);

        foreach ($data as $key => $value) {
            if ($postType == "all") {
                $newLikeArray[] = $value;
            } else if ($value['post_type'] == $postType || $value['post_type'] == 'share') {
                $newLikeArray[] = $value;
            }
        }
    }

    if (!empty($technique)) {
        usort($newLikeArray, "cmp");
    }
    return $newLikeArray;
}


function cmp($a, $b)
{
    return $b['created_at'] - $a['created_at'];
}


function getFollowFollowerData($account_handle, $type, $technique) {
  $targetDir = "../../json_directory/social/followerwonk/".$account_handle."/";
  $filesOfFollow = dirToArray($targetDir);
  $newFollowArray = array();
  foreach($filesOfFollow as $key => $fileOfFollowDetails)
  {
    $Final= $targetDir.$fileOfFollowDetails;
    $jsonOfFollow = file_get_contents($Final);
    $data = json_decode($jsonOfFollow, true);
    if (isset($data[$type])) {
        foreach ($data[$type] as $key => $value) {
            $newFollowArray[] = $value;
        }
    }
  }
  if (!empty($technique)) {
    usort($newFollowArray, "comp");
  }
  return $newFollowArray;
}

/*
 *Function	: 	createKeywordJsonFile()
 *Purpose	: 	To create Like Json file to show like count
 *Arguments	: 	(string) $post_id, (string) $type, (string) $account_handle, (string) $data.
*/
function createKeywordJsonFile($post_id, $type, $account_handle, $data,$finalFile)
{
    $returnArr = array();
    GLobal $docRoot;

    // creating directory to store like post result in json format.
    $thisdir = $docRoot . 'json_directory/social/followerwonk/' . $account_handle;

    /* To check folder Existance */
    $folderExist = folderPresenceCheck($thisdir);
    if (noError($folderExist)) {
        // $jsonFile = $account_handle . '_bookmark.json';
        $jsonPath = $thisdir . '/' . $finalFile;

        /*
            1) check whether file exist or not
            2) if file does not exist then create a new file with account handler name.
        */
        if (!file_exists($jsonPath)) {
            // creating json file with account handle name.

            $json_data = json_encode($data,JSON_PRETTY_PRINT);
            file_put_contents($jsonPath, $json_data);
            $errMsg    = 'Success : Bookmark of post json file created';
            $returnArr = setErrorStack($returnArr, -1, $errMsg, Null);
        } else {
            //append data to existing json file
            $content   = file_get_contents($jsonPath);
            $storeArr = $tempArray = json_decode($content, true);

            //check if post type is already exist in json file
            //if type exist append post id to existing post type
            //else create new post type with post id

            if(array_key_exists($type, $tempArray)){
                $newData                      = $data[$type][0];
                $flag = 1;
                foreach ($tempArray[$type] as $key => $value) {
                    if ($value['keyword'] == $newData['keyword'])
                    {
                        $set = array_splice($tempArray[$type], $key, 1);
                        $flag = 0;
                    }
                 }
                if($flag == 0) {
                  $jsonData                       = json_encode($tempArray,JSON_PRETTY_PRINT);
                  file_put_contents($jsonPath, $jsonData);
                  $errMsg    = $type.$newData['keyword'].' is removed from file';
                  $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
                }else{
                  array_push($tempArray[$type], $newData);
                  $jsonData                       = json_encode($tempArray,JSON_PRETTY_PRINT);
                  file_put_contents($jsonPath, $jsonData);
                  $errMsg    = $type.' is already exist in file';
                  $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
                }
            }else{
                $tempArray = array_merge($tempArray,  $data);
                $jsonData  = json_encode($tempArray,JSON_PRETTY_PRINT);

                file_put_contents($jsonPath, $jsonData);
                $errMsg    = 'created new type' .$type.'in joson file';
                $returnArr = setErrorStack($returnArr,  -1, $errMsg, Null);
            }
        }
    } else {
        $checkArr['error']      = "Directed Folder Mismatched";
    }
    return $returnArr;
}


/*
 *Function  :   createNotificationForSocial()
 *Purpose :   To create Notification for all type.
 *Arguments :   (string) $post_id, (string) $type, (Int) $postTime, (array) $sendTo, (array) $sendFrom.
*/
function createNotificationForSocial($postId, $postTime, $type, $sendFrom, $commentText, $postCreatorEmail) {

  global $rootUrl, $cdnSocialUrl, $walletURLIPnotification;
  $returnArr = array();
  if (!empty($postId) && isset($postId) && !empty($postTime) && isset($postTime) && !empty($type) && isset($type) && isset($sendFrom) && count($sendFrom) >= 5 && !empty($postCreatorEmail) && isset($postCreatorEmail)) {


    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$sendFrom["account_handle"]."/";

    $user_id="user_id,account_handle";
    $getUserInfo = getUserInfo($postCreatorEmail, $walletURLIPnotification.'api/notify/v2/', $user_id);
    if(noError($getUserInfo)){
      $getUserInfo = $getUserInfo["errMsg"];
      $user_id     = $getUserInfo["user_id"];
      $accHandle   = $getUserInfo["account_handle"];
    }

    //get json file
    $getFollowed         = getFollowUser($targetDirAccountHandler);

    //Get follow Flag
    $followFlag = getFollowUserValue($getFollowed,$user_id,$sendFrom['id']);


    $postDetails = getPostDetail($postId, $postTime, $sendFrom['id'], $followFlag);
    if (noError($postDetails)) {
      $postDetails = $postDetails['errMsg'][0];
      $postDescription = rawurldecode($postDetails["post_short_desc"]);
        switch ($postDetails['post_type']) {
          case 'image':
            if (!empty($postDescription)) {
              $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDescription);
              $postDescCount = strlen($postDesc);
              if ($postDescCount >= 50) {
                $postTitle = substr($postDesc,0, 50).'...';
              } else {
                $postTitle = $postDesc;
              }
            }
            if (!empty($postDescription)) {
              $mentionedPeople = getMentionedUserFromPost($postDescription, $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          case 'video':
              $videoTitle = $postDetails['post_details']['video_desc'];
            if ($videoTitle != '') {
              $postDesc = $videoTitle;
            } else if (!empty($postDescription)) {
              $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDescription);
            }
            if(!isset($postDesc)){
                $postDesc = "";
            }
            $postDescCount = strlen( $postDesc);
            if ($postDescCount >= 50) {
              $postTitle = substr($postDesc,0, 50).'...';
            } else {
              $postTitle = $postDesc;
            }
            if (!empty($postDescription)) {
              $mentionedPeople = getMentionedUserFromPost($postDescription, $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          case 'blog':
            $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDetails["post_details"]["blog_title"]);
            $postDescCount = strlen($postDesc);
            if ($postDescCount >= 50) {
              $postTitle = substr($postDesc,0, 50).'...';
            } else {
              $postTitle = $postDesc;
            }
            if (!empty(rawurldecode($postDetails["post_details"]["blog_content"]))) {
              $mentionedPeople = getMentionedUserFromPost(rawurldecode($postDetails["post_details"]["blog_content"]), $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          case 'status':
            $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDescription);
            $postDescCount = strlen($postDesc);
            if ($postDescCount >= 50) {
              $postTitle = substr($postDesc,0, 50).'...';
            } else {
              $postTitle = $postDesc;
            }
            if (!empty($postDescription)) {
              $mentionedPeople = getMentionedUserFromPost($postDescription, $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          case 'audio':
            if (!empty($value["post_short_desc"])) {
              $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDescription);
              $postDescCount = strlen($postDesc);
              if ($postDescCount >= 50) {
                $postTitle = substr($postDesc,0, 50).'...';
              } else {
                $postTitle = $postDesc;
              }
            }
            if (!empty($postDescription)) {
              $mentionedPeople = getMentionedUserFromPost($postDescription, $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          case 'share':
            if (!empty($value["post_short_desc"])) {
              $postDesc = preg_replace('/<\/?a[^>]*>/','',$postDescription);
              $postDescCount = strlen($postDesc);
              if ($postDescCount >= 50) {
                $postTitle = substr($postDesc,0, 50).'...';
              } else {
                $postTitle = $postDesc;
              }
            }
            if (!empty($postDescription)) {
              $mentionedPeople = getMentionedUserFromPost($postDescription, $sendFrom['account_handle']);
              $mentionedPeople = $mentionedPeople['errMsg'];
            }
            break;
          default:
            $postTitle = "Please Visit";
            break;
        }


        $mentionedPeople1['first_name'] = $postDetails['user_ref']['first_name'];
        $mentionedPeople1['last_name'] = $postDetails['user_ref']['last_name'];
        $mentionedPeople1['email'] = $postDetails['user_ref']['email'];
        $mentionedPeople1['user_id'] = $postDetails['posted_by'];
        $mentionedPeople1['mentionType'] = 'self';
        $mentionedPeople[] = $mentionedPeople1;

        for ($i = 0; $i< count($mentionedPeople); $i++) { // remove any duplicates from the result array
          $c = 0;
          for($c = 0; $i > $c; $c++) {
            if ($mentionedPeople[$i] == $mentionedPeople[$c]) {
              unset($mentionedPeople[$i]);
            }
          }
        }

        if (!empty($commentText)) {
          $commentText = rawurldecode($commentText);
          $commentMentionedPeople = getMentionedUserFromPost($commentText, $sendFrom['account_handle']);
          $commentMentionedPeople = $commentMentionedPeople['errMsg'];
          foreach ($commentMentionedPeople as $key1 => $value1) {
            $value1['commentMention'] = 'self';
            array_push($mentionedPeople, $value1);
          }
        }

        foreach ($mentionedPeople as $key => $value) {
          if ($sendFrom['email'] != $value['email']) {
          //Switch condition for desiding all content data.
            switch ($type) {
              case 'create':
                $category   = 'New Post';
                $postContent     = "";
                $email_body      = "";
                $preference_code = 1;
                $linkStatus      = 1;
                if (isset($value['mentionType']) && !empty($value['mentionType'])) {
                  // $notification_body = $sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "ed Your Post";
                } else {
                  $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Mentioned You In New ".$postDetails['post_type']." Post</a>";
                }
                break;
              case 'comment':
                $category   = 'comment';
                $postContent     = "";
                $email_body      = "";
                $preference_code = 1;
                $linkStatus      = 1;
                if (isset($value['mentionType']) && !empty($value['mentionType'])) {
                  if (isset($value['commentMention']) && !empty($value['commentMention'])) {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Mentioned You In Comment</a>";
                  } else {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "ed On Your Post</a>";
                  }
                } else {
                  if (isset($value['commentMention']) && !empty($value['commentMention'])) {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Mentioned You In Comment</a>";
                  } else {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "ed On Post Where You Are Mentioned</a>";
                  }
                }
                break;
              case 'reply':
                $category   = 'reply';
                $postContent     = "";
                $email_body      = "";
                $preference_code = 1;
                $linkStatus      = 1;
                if (isset($value['mentionType']) && !empty($value['mentionType'])) {
                  if (isset($value['commentMention']) && !empty($value['commentMention'])) {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Mentioned You In Reply</a>";
                  } else {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Replied On Your Post</a>";
                  }
                } else {
                  if (isset($value['commentMention']) && !empty($value['commentMention'])) {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Mentioned You In Reply</a>";
                  } else {
                    $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails["_id"]) . "&timestamp=" . $postDetails["created_at"] . "&posttype=" . base64_encode($postDetails["post_type"]) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Replied On Post Where You Are Mentioned</a>";
                  }
                }
                break;
              case 'like':
                $category   = 'like';

                if (isset($value['mentionType']) && !empty($value['mentionType'])) {
                  $preference_code   = 2;
                  $linkStatus        = 1;
                  $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "d Your Post</a>";
                  $postContent       = "<div class='col-xs-12'><a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'><div>" . $postTitle ."</div></a></div><br><br><br>";

                  $email_body      = "<tr><td style='padding: 60px 20px 10px; font-size: 20px; color: #0b6796;'>Hi " . $value['first_name'] . ",</td></tr>

                  <tr><td style='padding: 10px 20px;'>" . $sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "d your post.To know more, login to keywo now.</td></tr>  <tr><td style='padding: 10px 20px;'> " . $postContent . "</td></tr>";
                } else {
                  $preference_code   = 1;
                  $linkStatus        = 1;
                  $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "d Post Where You Are Mentioned</a>";
                }
                break;
              case 'share':
                $postContent     = "";
                $email_body      = "";
                $preference_code = 1;
                $linkStatus      = 1;
                $category   = 'share';
                if (isset($value['mentionType']) && !empty($value['mentionType'])) {
                  $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "ed Your Post</a>";
                } else {
                  $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " " . $type . "ed Post Where You Are Mentioned</a>";
                }
                break;
              default:
                $postContent       = "";
                $email_body        = "";
                $preference_code   = 1;
                $linkStatus        = 1;
                $category   = 'social';
                $notification_body = "<a href = '". $rootUrl . "views/social/socialPostDetails.php?post_id=". base64_encode($postDetails['_id']) . "&timestamp=" . $postDetails['created_at'] . "&posttype=" . base64_encode($postDetails['post_type']) . "' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Performed Action On Your Post</a>";
                break;
            }
            $to         = $value['email'];
            $first_name = $value['first_name'];
            $last_name  = $value['last_name'];
            $id         = $value['user_id'];


            $mail_subject = "Your post has been liked!";
            $smsText = '';
            $mobileNumber = '';
            $createNotification = sendNotificationBuyPrefrence($to,$mail_subject,$email_body,$first_name,$last_name,$id,$smsText,$mobileNumber,$notification_body,$preference_code,$category,$linkStatus);
            if (noError($createNotification)) {
              $returnArr['errCode'] = -1;
              $returnArr['errMsg'] = 'Notification Updated Successfully';
            } else {
              $returnArr["errCode"] = 3;
              $returnArr["errMsg"] = 'Error: Unable to send email and notification to '.  $first_name . ' ' . $last_name . ' user. ';
            }
          } else {
            $returnArr['errCode'] = -1;
            $returnArr['errMsg'] = 'User Performing Action On Self Post';
          }
        }
    } else {
      $returnArr['errCode'] = 1;
      $returnArr['errMsg'] = 'Error: Unable to Get Post Details';
    }
  } else {
    $returnArr['errCode'] = 1;
    $returnArr['errMsg'] = 'All Fields Are Mandetory';
  }
  return $returnArr;
}

/*
 *Function  :   createNotificationForFollow()
 *Purpose :   To create Notification for all type.
 *Arguments :   (array) $userDetails, (array) $sendFrom.
*/
function createNotificationForFollow($userDetails, $sendFrom) {
  global $rootUrl;
  if (!empty($userDetails) && isset($userDetails) && isset($sendFrom) && count($sendFrom) >= 5 ) {
    $to                 = $userDetails['email'];
    $first_name         = $userDetails['first_name'];
    $last_name          = $userDetails['last_name'];
    $id                 = $userDetails['id'];
    $notification_body  = "<a href = '". $rootUrl . "views/social/otherTimeline.php?email=". base64_encode($sendFrom['email'])."' target='_blank'>".$sendFrom['first_name'] . " " . $sendFrom['last_name'] . " Is Following You</a>";
    $preference_code    = 1;
    $category           = 'following';
    $linkStatus         = 1;
    $mail_subject       = "Keywo Notification";
    $smsText = '';
    $mobileNumber = '';
    $createNotification = sendNotificationBuyPrefrence($to,$mail_subject,$email_body,$first_name,$last_name,$id,$smsText,$mobileNumber,$notification_body,$preference_code,$category,$linkStatus);
    if (noError($createNotification)) {
      $returnArr['errCode'] = -1;
      $returnArr['errMsg'] = 'Notification Updated Successfully';
    } else {
      $returnArr["errCode"] = 3;
      $returnArr["errMsg"] = 'Error: Unable to send email and notification to '.  $first_name . ' ' . $last_name . ' user. ';
    }
  } else {
    $returnArr['errCode'] = 1;
    $returnArr['errMsg'] = 'All Fields Are Mandetory';
  }
  return $returnArr;
}

/*
 *Function  :   getMentionedUserFromPost()
 *Purpose :   To create Notification for all type.
 *Arguments :   (string) $post_id, (string) $type, (Int) $postTime, (array) $sendTo, (array) $sendFrom.
*/
function getMentionedUserFromPost($textData, $sessonAccountHandler) {
  global $rootUrl, $cdnSocialUrl, $walletURLIPnotification;
  $returnArr = array();
  $returnMentionArray = array();

  if (!empty($textData) && isset($textData)) {
    $textData = preg_replace('/<\/?a[^>]*>/','',$textData);
    $data = explode(" @", $textData);
    if (!empty($data)) {
        if (strpos($data[0], '@') == false) {
            $dataArray = explode("@", $data[0]); //printArr($dataArray);die;
            if (isset($dataArray[1])) {
                $firstAccHandle = explode(" ", $dataArray[1]);
                if (!empty($firstAccHandle)) {
                    $firstAccHandle = $firstAccHandle[0];
                }
                $firstAccHandle = rtrim($firstAccHandle, '</a>');
                if ($sessonAccountHandler != $firstAccHandle) {

                    $otherUserDetails = getUserInfoAccHandle($firstAccHandle, $walletURLIPnotification . 'api/notify/v2/', 'email,first_name,last_name,user_id');
                    if ($otherUserDetails["errCode"] == -1) {
                        $returnMentionArray[] = $otherUserDetails['errMsg'];
                    }
                }
            }
            unset($data[0]);
        }
    }

    foreach($data as $accHandle) {
      $accHandle   = explode(" ", $accHandle)[0];
      $accHandle = rtrim($accHandle, '</a>');
      if ($sessonAccountHandler != $accHandle) {
        $otherUserDetails = getUserInfoAccHandle($accHandle, $walletURLIPnotification . 'api/notify/v2/', 'email,first_name,last_name,user_id');
        if ($otherUserDetails["errCode"] == -1) {
          $returnMentionArray[] = $otherUserDetails['errMsg'];
        }
      }
    }
    $returnArr['errCode'] = -1;
    $returnArr['errMsg']  = $returnMentionArray;
  } else {
    $returnArr['errCode'] = 1;
    $returnArr['errMsg']  = 'All Fields Are Mandetory';
  }
  return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getSuggestedUser
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setReportCountInfo()
*   Purpose       :   get suggested user
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function getSuggestedUser($accountHandlerId)
{
  global $walletPublicKey, $mode, $socialUrl;
  $requestUrl = $socialUrl;
  $retArray = array();
  $headers = array();
  $apiText    = "user_id=".$accountHandlerId."&publicKey={$walletPublicKey}";
  $postFields = "publicKey=" . $walletPublicKey."&user_id=".$accountHandlerId;
  $apiName = 'user/suggestion';
  $curl_type = 'GET';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

  if ($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if (noError($nonce_result)) {
      $nonce_result = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg'] = $nonce_result['errMsg'];
      $headers[] = "x-ts: {$nonce_result['timestamp']}";
      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg'] = $nonce_result['errMsg'];
    }
  }

  $respArray = curlRequest($curlReqParam, $headers);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg'] = $respArray['errMsg'];
    $retArray['mode'] = $mode;

  } else if ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  }


  return $retArray;

}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function setReportCountInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setReportCountInfo()
*   Purpose       :   User get count of reported post
*   Arguments     :   $postId,$created_at,$post_category
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setReportCountInfo($postId,$created_at,$post_category)
{
  global $walletPublicKey, $mode, $socialUrl;
  $requestUrl = $socialUrl;
  $retArray = array();
  $headers = array();
  $apiText="post_id=$postId&post_time=$created_at&category=$post_category&publicKey=$walletPublicKey";
  $postFields = "post_time=$created_at&publicKey=$walletPublicKey";
  $apiName = 'post/'.$postId.'/category/'.$post_category;
  $curl_type = 'GET';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);
  if ($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if (noError($nonce_result)) {
      $nonce_result = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg'] = $nonce_result['errMsg'];
      $headers[] = "x-ts: {$nonce_result['timestamp']}";
      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg'] = $nonce_result['errMsg'];
    }
  }

  $respArray = curlRequest($curlReqParam, $headers);

  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg'] = $respArray['errMsg'];
    $retArray['mode'] = $mode;

  } else if ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  }
  return $retArray;
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function setBlockPostRequest
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setBlockPostRequest()
*   Purpose       :   admin will block the post which is reported too many times Create Post
*   Arguments     :  $postId,$created_at,$post_category
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setBlockPostRequest($postId,$created_at,$post_category)
{
  global $walletPublicKey, $mode, $socialUrl;
  $requestUrl = $socialUrl;
  $retArray = array();
  $headers = array();
  $apiText="post_id=$postId&post_time=$created_at&publicKey=$walletPublicKey";
  $postFields = "post_time=$created_at&publicKey=$walletPublicKey";
  $apiName = 'post/'.$postId.'/block';
  $curl_type = 'PUT';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);
  if ($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if (noError($nonce_result)) {
      $nonce_result = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg'] = $nonce_result['errMsg'];
      $headers[] = "x-ts: {$nonce_result['timestamp']}";
      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg'] = $nonce_result['errMsg'];
    }
  }

  $respArray = curlRequest($curlReqParam, $headers);

  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg'] = $respArray['errMsg'];
    $retArray['mode'] = $mode;

  } else if ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  }
  return $retArray;
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function setUserBlockStatusKeywo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   setUserBlockStatusKeywo()
*   Purpose       :   User will blocked by admin
*   Arguments     :   $id, $reason,$blocked_by,$service_req_ID
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function setUserBlockStatusKeywo($id, $reason,$blocked_by,$service_req_ID)
{
global $walletPublicKey, $mode, $walletURLIPnotification;
$requestUrl = $walletURLIPnotification;
$retArray = array();

$headers = array();
/* create signature */
$apiText = "user_id={$id}&blocked_by={$blocked_by}&service_req_id={$service_req_ID}&publicKey={$walletPublicKey}";
$postFields = $apiText."&reason=" . $reason;

$apiName = "api/notify/v2/admin/block/{$id}";
$curl_type = 'PUT';
$curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

if ($mode == 'production') {
$nonce_result = generateSignatureWithNonce($apiText);
if (noError($nonce_result)) {
$nonce_result = $nonce_result['errMsg'];
$retArray['errCode'] = -1;
//$retArray['errMsg'] = $nonce_result['errMsg'];
$headers[] = "x-ts: {$nonce_result['timestamp']}";
$headers[] = "x-cnonce: {$nonce_result['cnonce']}";
} else {
$retArray['errCode'] = 2;
$retArray['errMsg'] = $nonce_result['errMsg'];
}
}

$respArray = curlRequest($curlReqParam, $headers);

if (noError($respArray)) {
if ($mode == 'production') {
$nonce_counter = incrementVectorCounter();
}
$retArray['errCode'] = -1;
$retArray['errMsg'] = $respArray['errMsg'];
$retArray['mode'] = $mode;

} else if ($respArray["errCode"] != 73) {
if ($mode == 'production') {
$nonce_counter = incrementVectorCounter();
}
$retArray['errCode'] = $respArray['errCode'];
$retArray['errMsg'] = $respArray['errMsg'];
} else {
$retArray['errCode'] = $respArray['errCode'];
$retArray['errMsg'] = $respArray['errMsg'];
}


return $retArray;

}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getAnnouncement
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getAnnouncement()
*   Purpose       :   get Announcement
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

function getAnnouncement()
{
  global $walletPublicKey, $mode, $socialUrl;
  $requestUrl = $socialUrl;
  $retArray = array();
  $headers = array();
  $apiText    = "publicKey={$walletPublicKey}";
  $postFields = $apiText;
  $apiName = 'user/bulletin';
  $curl_type = 'GET';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);
  if ($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if (noError($nonce_result)) {
      $nonce_result = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg'] = $nonce_result['errMsg'];
      $headers[] = "x-ts: {$nonce_result['timestamp']}";
      $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg'] = $nonce_result['errMsg'];
    }
  }

  $respArray = curlRequest($curlReqParam, $headers);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg'] = $respArray['errMsg'];
    $retArray['mode'] = $mode;

  } else if ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg'] = $respArray['errMsg'];
  }


  return $retArray;

}


function setUserStatusWallet($email, $status)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();

    $headers = array();
    /* create signature */
    $apiText = "email={$email}&status={$status}&publicKey={$walletPublicKey}";
    $postFields = "email=" . $email . "&status=" . $status . "&publicKey=" . $walletPublicKey;
    $apiName = 'admin/user/status';
    $curl_type = 'PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function setUserBlockStatusWallet($email, $reason,$blocked_by,$service_req_ID)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "service_req_id={$service_req_ID}&blocked_by={$blocked_by}&reason={$reason}&email={$email}&publicKey={$walletPublicKey}";
    $postFields = $apiText;

    $apiName = "user/blockdetails";
    $curl_type = 'PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;

}


//function to call an api to send following list for user to update memcache data for FTUE
function setFollowingToMemcache($userId, $accountHandle)
{
  global $mode, $socialUrl, $walletPublicKey;
  $retArray = array();

  if (isset($userId) && !empty($userId) && isset($accountHandle) && !empty($accountHandle)) {

      //Get following array
      $targetDirAccountHandler = "../../json_directory/social/followerwonk/" . "$accountHandle" . "/";
      $getFollowingArray       = getFollowUser($targetDirAccountHandler);

      $headers      = array();
      //create signature
      $apiText      = 'user_id=' . $userId . '&followingIds=' . json_encode($getFollowingArray) . '&publicKey=' . $walletPublicKey;
      $postFields   = 'user_id=' . $userId . '&followingIds=' . json_encode($getFollowingArray) . '&publicKey=' . $walletPublicKey;
      $apiName      = 'memcache';
      $requestUrl   = "{$socialUrl}";
      $curlType     = 'PUT';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
      // print_r($curlReqParam);
      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg']  = $respArray['errMsg'];
          $retArray['mode']    = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg']  = $respArray['errMsg'];
      }
  } else {
      $retArray["errCode"] = 2;
      $retArray["errMsg"]  = "All fields are mandatory";
  }

  return $retArray;

}



//get post ananlytics 
function getPostAnanyticsValue($userId) {

    global $mode, $NotificationURL, $walletPublicKey;
    $retArray     = array();

    if (isset($userId) && !empty($userId)){
        $headers      = array();
        $dataNow      = round(microtime(true) * 1000);
        //create signature
        $apiText      = 'user_id=' . $userId . '&publicKey=' . $walletPublicKey;
        $postFields   = 'publicKey=' . $walletPublicKey;
        $apiName      = 'v2/user/' . $userId . '/post_summery/';
        $requestUrl   = "{$NotificationURL}";
        $curlType     = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
        //print_r($curlReqParam);die;
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg']  = $respArray['errMsg'];
            $retArray['mode']    = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "All fields are mandatory";
    }

    return $retArray;
}

// get content consumption data for my earnings graph
function graphConsumptionData($months) {
    $final        = array();
    $months       = json_decode($months);
    $getLikePosts = getPostLikeIds($_SESSION['account_handle'], 'sort', $type = 'all');

    if (!empty($getLikePosts)) {
        foreach ($months as $month) {
            $monthNo = date('m', strtotime($month));
            foreach ($getLikePosts as $post) {
                $likedAt = $post["action_at"];
                $seconds = $likedAt / 1000;
                $likeMonth = date("m", $seconds);
                $earn = 0;
                if ($monthNo == $likeMonth) {
                    //printArr($post);
                    $earn += $post["current_payout"];
                }
            }
            $final[$monthNo] = $earn;
        }
    } else {
        $final = array(0,0,0,0,0,0);
    }
    $value = implode(',', $final);
    //printArr($value);
    return $value;
}

// auto save image form api of both
function save_image($inPath,$outPath)
{ //Download images from remote server
    $in=    fopen($inPath, "rb");
    $out=   fopen($outPath, "wb");
    while ($chunk = fread($in,8192))
    {
        fwrite($out, $chunk, 8192);
    }
    fclose($in);
    fclose($out);
}

//get referral share data for my earnings - share post
function referralShareDetails($userId, $action, $time) {

    global $mode, $socialUrl, $walletPublicKey;
    $retArray     = array();
    $headers      = array();
    if (empty($time)) {
        $time = round(microtime(true) * 1000);
    }
    //create signature
    $apiText      = 'user_id=' . $userId . '&action=' . $action . '&publicKey=' . $walletPublicKey;
    $postFields   = 'publicKey=' . $walletPublicKey . '&time=' . $time;
    $apiName      = 'post/share/user/' . $userId . '/' . $action;
    $requestUrl   = "{$socialUrl}";
    $curlType     = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
    //print_r($curlReqParam);die;
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

//get content earnings
function getContentUpload($userId, $type, $time) {

    global $mode, $socialUrl;
    $retArray = array();

    if (isset($userId) && !empty($userId) && isset($type) && !empty($type) && isset($time) && !empty($time)) {

        $headers      = array();
        //create signature
        $apiText      = "user_id=" . $userId . "&post_type=" . $type . '&time_region=' . $time;
        $postFields   = 'user_id=' . $userId . '&post_type=' . $type .  '&time_region=' . $time;
        $apiName      = 'post';
        $requestUrl   = "{$socialUrl}";
        $curlType     = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
        // print_r($curlReqParam);die;
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg']  = $respArray['errMsg'];
            $retArray['mode']    = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg']  = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "All fields are mandatory";
    }

    return $retArray;
}

?>
