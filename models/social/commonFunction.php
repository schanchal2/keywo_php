<?php

/**
 **********************************************************************************
 *                  postJsonModel.php
 * ********************************************************************************
 *     We used CDP related functions in this model
 */

 /*
  *Function	: 	createJsonFileSpecial()
  *Purpose	: 	To create Json file to add special user
  *Arguments: 	(string)$accountHandle, (array)$specialUser.
 */
function createJsonFileSpecial($accountHandle, $specialUsers) {
  Global $docRoot;
    foreach ($specialUsers["errMsg"] as $specialUser) {

    $targetDirAccountHandler    = $docRoot . "json_directory/social/followerwonk/".$accountHandle."/";
    $folderExist                = folderPresenceCheck($targetDirAccountHandler);
    $accountHandleWithFirstChar = strtolower($specialUser['account_handle'][0]);

    if (has_specchar($accountHandleWithFirstChar)) {
        $accountHandleChar = "0";
    } else {
        $accountHandleChar = $accountHandleWithFirstChar;
    }

    $accountHandlerFileName      = $targetDirAccountHandler.$accountHandle."_info_".$accountHandleChar.".json";

    $data = array(
              'followings'=>[],
              'followers'=> [],
              'blocked'=>[]
            );

    if (!file_exists($accountHandlerFileName)) {
      if(file_put_contents($accountHandlerFileName,json_encode($data,JSON_PRETTY_PRINT))) {
        $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType ,$accountHandlerId ,$otherAccountHandleType, $specialUser['user_id']);
        if (noError($setJson)) {
          $xml_data["step4"]["data"]         = "Json File Creation Of Account Handler";
          $xml_data["userinfo"]["data"]      = "";
          $xml_data["userinfo"]["attribute"] = $setJson;
        } else {
          $returnArr["errCode"] = 50;
          $errMsg               = "Error in Adding Post Details.";
        }
      } else {
        $returnArr["errCode"] = 52;
        $errMsg               = "Error in Creating Json File For Handler.";
      }
    } else {
      if (file_put_contents($accountHandlerFileName, json_encode($data,JSON_PRETTY_PRINT))) {
        $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $specialUser['user_id']);
        if (noError($setJson)) {
          $xml_data["step4"]["data"]         = "Json File Creation Of Account Handler";
          $xml_data["userinfo"]["data"]      = "";
          $xml_data["userinfo"]["attribute"] = $setJson;
        } else {
          $returnArr["errCode"] = 50;
          $errMsg               = "Error in Adding Post Details.";
        }
      } else {
        $returnArr["errCode"] = 52;
        $errMsg               = "Error in Creating Json File For Handler.";
      }
    }
  }
}


/*
 *Function	: 	createJsonFile()
 *Purpose	: 	To create Json file for post viewer
 *Arguments	: 	(string)$email, (string)$post_id, (string)$data.
*/
function createJsonFile($post_id, $account_handle, $data)
{
    $returnArr = array();
    $extraArg = array();
    GLobal $docRoot;

    // creating directory to store social post result in json format.
    $thisdir = $docRoot . 'json_directory/social/post/' . $account_handle;

    /* To check folder Existance */
    $folderExist = folderPresenceCheck($thisdir);
    if (noError($folderExist)) {
        // json file name.
        $jsonFile = $account_handle . '_post_' . getMultipleof500($post_id) . '.json';
        $jsonPath = $thisdir . '/' . $jsonFile;

                /*
          1) check whether file exist or not
          2) if file does not exist then create a new file with account handeler name.
          */
        if (!file_exists($jsonPath)) {
            // creating json file with account handle name.
            $json_data = json_encode($data);
            file_put_contents($jsonPath, $json_data);
            $errMsg = 'Post json file created successfully';
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            //append data to existing json file
            $content = file_get_contents($jsonPath);
            $tempArray = json_decode($content, true);

            $match=search($tempArray, "post_id", $post_id)[0]["post_id"];
            if($match==$post_id)
            {
                $errMsg = 'post_id already exist';
                $returnArr = setErrorStack($returnArr,  3, $errMsg, $extraArg);
            }else{
                array_push($tempArray, $data[0]);
                $jsonData = json_encode($tempArray);
                file_put_contents($jsonPath, $jsonData);
                $errMsg = 'Updated post json file';
                $returnArr = setErrorStack($returnArr,  5, $errMsg, $extraArg);
            }
        }
    } else {
        $checkArr['error']      = "Directed Folder Mismatched";
    }
    return $returnArr;
}

function getMultipleof500($post_id)
{
    $numberList = array();
    for ($counter = 500; $counter <= $post_id; $counter = $counter + 500) {
        array_push($numberList, $counter);
    }
    return end($numberList) + 500;
}


/*
 *Function	: 	dirToArray()
 *Purpose	: 	To get all files from sleected directories
 *Arguments	: 	(string)$dir
*/
function dirToArray($dir) {
$result = array();

     $isExists=file_exists($dir);

      if($isExists)
      {
$cdir = scandir($dir);
foreach ($cdir as $key => $value)
{
  if (!in_array($value,array(".","..")))
  {
     if (is_dir($dir . DIRECTORY_SEPARATOR . $value))
     {
        $result[$value] = dirToArray($dir . DIRECTORY_SEPARATOR . $value);
     }
     else
     {
        $result[] = $value;
     }
  }
}
}
return $result;
}

/*
 *Function	: 	getMultipleofPostID()
 *Purpose	: 	To get benchmarking according to post id
 *Arguments	: 	(string) $post_id
*/
function getMultipleofPostID($post_id)
{
    Global $likePostIdBenchmarking;
    $numberList = array();
    for ($counter = $likePostIdBenchmarking; $counter <= $post_id; $counter = $counter + $likePostIdBenchmarking) {
        array_push($numberList, $counter);
    }
    return end($numberList) + $likePostIdBenchmarking;
}

/*
 *Function	: 	search()
 *Purpose	: 	To search post id in an array
 *Arguments	: 	$array, $key, $value
*/
function search($array, $key, $value)
{
    $results = array();
    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }
        foreach ($array as $subarray) {
            $results = array_merge($results, search($subarray, $key, $value));
        }
    }
    return $results;
}




// It will add or update updationOfJsonBlockFile file For Account Handler
function updationOfJsonBlockFile($accountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId )
{
    $returnArr                  = array();
    $arr_data = array();
    $jsondata = file_get_contents($accountHandlerFileName);
    $data = json_decode($jsondata, true);

    if (in_array($otherAccountHandlerId, $data["followings"]))
    {
      $key = array_search($otherAccountHandlerId, $data["followings"]);
      array_splice($data["followings"], $key, 1);
      //write json data into data.json file
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Updated Json details";
        $returnArr["errCode"] = -1;
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Updated Json Details.";
      }
    }

    if (in_array($otherAccountHandlerId, $data["followers"]))
    {
      $key = array_search($otherAccountHandlerId, $data["followers"]);
      array_splice($data["followers"], $key, 1);
      //write json data into data.json file
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Updated Json details";
        $returnArr["errCode"] = -1;
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Updated Json Details.";
      }
    }

    if (!in_array($otherAccountHandlerId, $data["blocked"]))
    {
      array_push($data["blocked"],$otherAccountHandlerId);
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      //write json data into data.json file
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Added Json details";
        $returnArr["errCode"] = -1;
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Adding Json Details.";
      }
    } else
    {
      $key = array_search($otherAccountHandlerId, $data["blocked"]);
      array_splice($data["blocked"], $key, 1);
      //write json data into data.json file
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Updated Json details";
        $returnArr["errCode"] = -1;
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Updated Json Details.";
      }
    }
    return $returnArr;
}



// It will add or update Json file For  Other Account Handler
function updationOfJsonFileForBlockedOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId )
{
  $returnArr                  = array();
  $arr_data = array();
  $jsondata = file_get_contents($otherAccountHandlerFileName);
  $data = json_decode($jsondata, true);
  if (in_array($accountHandlerId, $data["followers"]))
  {
    $key = array_search($accountHandlerId, $data["followers"]);
    array_splice($data["followers"], $key, 1);
    //write json data into data.json file
    $jsondata = json_encode($data, JSON_PRETTY_PRINT);
    if(file_put_contents($otherAccountHandlerFileName, $jsondata))
    {
      $errMsg = "Successfully Updated Json details";
      $returnArr["errCode"] = -1;
    }
    else
    {
      $returnArr["errCode"] = 50;
      $errMsg = "Error in Updated Json Details.";
    }
  }
  if (in_array($accountHandlerId, $data["followings"]))
  {
    $key = array_search($accountHandlerId, $data["followings"]);
    array_splice($data["followings"], $key, 1);
    //write json data into data.json file
    $jsondata = json_encode($data, JSON_PRETTY_PRINT);
    if(file_put_contents($otherAccountHandlerFileName, $jsondata))
    {
      $errMsg = "Successfully Updated Json details";
      $returnArr["errCode"] = -1;
    }
    else
    {
      $returnArr["errCode"] = 50;
      $errMsg = "Error in Updated Json Details.";
    }
  }
  else {
    $returnArr["errCode"] = -1;
  }
  return $returnArr;
}





// It will add or update Json file For Account Handler
function updationOfJsonFile($accountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId )
{
    $returnArr                  = array();
    $arr_data = array();
    $jsondata = file_get_contents($accountHandlerFileName);
    $data = json_decode($jsondata, true);
    if (!in_array($otherAccountHandlerId, $data["followings"]))
    {
      array_push($data["followings"],$otherAccountHandlerId);
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      //write json data into data.json file
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Added Json details";
        $returnArr["errCode"] = -1;
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Adding Json Details.";
      }
    }
    else
    {
      $key = array_search($otherAccountHandlerId, $data["followings"]);
      array_splice($data["followings"], $key, 1);
      //write json data into data.json file
      $jsondata = json_encode($data, JSON_PRETTY_PRINT);
      if(file_put_contents($accountHandlerFileName, $jsondata))
      {
        $errMsg = "Successfully Updated Json details";
        $returnArr["errCode"] = -1;
        removePostOfBlockedUser($accountHandlerId,$otherAccountHandlerId);
      }
      else
      {
        $returnArr["errCode"] = 50;
        $errMsg = "Error in Updated Json Details.";
      }
    }
    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updationOfKeywordJsonFile()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updationOfKeywordJsonFile()
*   Purpose       :   This function is used for Follow/ Unfollow keywords
*   Arguments     :   (string) $accountHandlerFileName, (string) $keyword
*/

function updationOfKeywordJsonFile($accountHandlerFileName,$keyword)
{
    $returnArr                  = array();
    $email = $_SESSION["email"];
    $arr_data = array();
    $jsondata = file_get_contents($accountHandlerFileName);
    $data = json_decode($jsondata, true);
    if (!in_array($keyword, $data["followedkeyword"]))
    {
        array_push($data["followedkeyword"],$keyword);
        $jsondata = json_encode($data, JSON_PRETTY_PRINT);
        //write json data into data.json file
        if(file_put_contents($accountHandlerFileName, $jsondata))
        {
            $errMsg = "Successfully Added Json details";
            $returnArr["errCode"] = -1;
        }
        else
        {
            $returnArr["errCode"] = 50;
            $errMsg = "Error in Adding Json Details.";
        }
    }
    else
    {
        $key = array_search($keyword, $data["followedkeyword"]);
        array_splice($data["followedkeyword"], $key, 1);
        //write json data into data.json file
        $jsondata = json_encode($data, JSON_PRETTY_PRINT);
        if(file_put_contents($accountHandlerFileName, $jsondata))
        {
            $errMsg = "Successfully Updated Json details";
            $returnArr["errCode"] = -1;
        }
        else
        {
            $returnArr["errCode"] = 50;
            $errMsg = "Error in Updated Json Details.";
        }
    }

    return $returnArr;
}


// It will add or update Json file For  Other Account Handler
function updationOfJsonFileForOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId )
{
  $returnArr                  = array();
  $email = $_SESSION["email"];
  $arr_data = array();
  $jsondata = file_get_contents($otherAccountHandlerFileName);
  $data = json_decode($jsondata, true);
  if (!in_array($accountHandlerId, $data["followers"]))
  {
    array_push($data["followers"],$accountHandlerId);
    $jsondata = json_encode($data, JSON_PRETTY_PRINT);
    //write json data into data.json file
    if(file_put_contents($otherAccountHandlerFileName, $jsondata))
    {
      $errMsg = "Successfully Added Json details";
      $returnArr['errMsg']['type'] = -1;
      $returnArr["errCode"] = -1;
    }
    else
    {
      $returnArr["errCode"] = 50;
      $errMsg = "Error in Adding Json Details.";
    }
  }
  else
  {
    $key = array_search($otherAccountHandlerId, $data["followers"]);
    array_splice($data["followers"], $key, 1);
    //write json data into data.json file
    $jsondata = json_encode($data, JSON_PRETTY_PRINT);
    if(file_put_contents($otherAccountHandlerFileName, $jsondata))
    {
      $errMsg = "Successfully Updated Json details";
      $returnArr['errMsg']['type'] = 1;
      $returnArr["errCode"] = -1;
    }
    else
    {
      $returnArr["errCode"] = 50;
      $errMsg = "Error in Updated Json Details.";
    }
  }

  return $returnArr;
}

// It will Check the account handler name for json file creation

function has_specchar($x)
{
    $excludes = "";
  if (is_array($excludes)&&!empty($excludes)) {
    foreach ($excludes as $exclude) {
    $x=str_replace($exclude,'',$x);
    }
  }
  if (preg_match('/[^a-z]+/i',$x)) {
    return true;
  }
  return false;
}


// Check Valid Url Link
function get_embed($provider, $url, $max_width = '', $max_height = ''){
    $providers = array(
        'youtube' => 'http://www.youtube.com/oembed'
        /* you can add support for more providers here */
    );

    if(!isset($providers[$provider])){
        return 'Invalid provider!';
    }

    $movie_data_json = @file_get_contents(
        $providers[$provider] . '?url=' . urlencode($url) .
        "&maxwidth={$max_width}&maxheight={$max_height}&format=json"
    );

    if(!$movie_data_json){
        $error = error_get_last();
        /* remove the PHP stuff from the error and show only the HTTP error message */
        $error_message = preg_replace('/.*: (.*)/', '$1', $error['message']);
        return $error_message;
    }else{
        $movie_data = json_decode($movie_data_json, true);
        return $movie_data['html'];
    }
}

// Get Conut of Json of Particuler type
function countOfJsonData($targetDirAccountHandler,$type) {
    $countOfBlockedOfAllJson = array();
  $files2 = dirToArray($targetDirAccountHandler);
  foreach($files2 as $key => $files2Details)
  {
    $Final= $targetDirAccountHandler.$files2Details;
    $jsondata = file_get_contents($Final);
    $data = json_decode($jsondata, true);
    $countOfBlockedJson = count($type);
    $countOfBlockedOfAllJson = $countOfBlockedJson + $countOfBlockedOfAllJson;
  }
    return $countOfBlockedOfAllJson;
}

//function to get logged in user blocked users list
function getBlockedUser($targetDirAccountHandler) {
  $userIdsOfAccHandler = array();
  $files = dirToArray($targetDirAccountHandler);
  for ($i = 0; $i < count($files); $i++ ) {
    $finalJsonFile = $targetDirAccountHandler.$files[$i];
    $jsondata      = file_get_contents($finalJsonFile);
    $data          = json_decode($jsondata, true);
    if(isset($data["blocked"])){
        $userIds       = $data["blocked"];
        for($k = 0; $k < count($userIds); $k++) {
            $userIdsOfAccHandler[] = $userIds[$k];
        }
    }
  }
  return $userIdsOfAccHandler;
}

//function to get logged in user following users list
function getFollowUser($targetDirAccountHandler) {
    $userIdsOfAccHandler = array();
  $files = dirToArray($targetDirAccountHandler);

  $blockedData = "";
  for ($i = 0; $i < count($files); $i++ ) {
    $finalJsonFile    = $targetDirAccountHandler.$files[$i];
    $jsondata = file_get_contents($finalJsonFile);
    $data     = json_decode($jsondata, true);
    $userIds  = "";
    if(isset($data["followings"])) {
        $userIds = $data["followings"];
        for ($k = 0; $k < count($userIds); $k++) {
            $userIdsOfAccHandler[] = $userIds[$k];
        }
    }
  }
  return $userIdsOfAccHandler;
}


function getFollowKeyword($accountHandle, $keyword) {
  global $docRoot;
  $returnArr = array();
    if (has_specchar($keyword)) {
        $indexPosition = "0";
    }
    else {
        $indexPosition      = strtolower($keyword[0]);
    }
  $targetDir = $docRoot . "json_directory/social/followerwonk/" . $accountHandle . "/" . $accountHandle . "_info_" . $indexPosition . ".json";
  //get all files from targetDir directory
    if(file_exists($targetDir)){
        $jsonData = file_get_contents($targetDir);
        $data     = json_decode($jsonData, true);
        if(isset($data["followedkeyword"])) {
            $data = $data["followedkeyword"];
            foreach ($data as $key => $value) {
                $newPostId = $value['keyword'];
                if ($keyword == $newPostId) {
                    $response = array(
                        'keyword' => $keyword,
                        'status' => true,
                    );
                    $returnArr = array_push($returnArr, $response);
                }
            }
        }
    }

  return $returnArr;
}

// to check user follow or not
function getFollowUserValue($getFollowingDetails,$user_id,$sessionID){
    if($user_id!=$sessionID){
        if(isset($getFollowingDetails)) {
            if (in_array($user_id, $getFollowingDetails)) {
                $followFlag = 1;
            } else {
                $followFlag = 0;
            }
        }
    }else{
        $followFlag = 2;
    }
    return $followFlag;
}




/**
 **********************************************************************************
 *                  postJsonModel.php
 * ********************************************************************************
 *     We used CDP related functions in this model
 */

/*
 *Function	: 	createJsonFileSpecial()
 *Purpose	: 	To create Json file to add special user
 *Arguments: 	(string)$accountHandle, (array)$specialUser.
*/
/**
 **********************************************************************************
 *                  postJsonModel.php
 * ********************************************************************************
 *     We used CDP related functions in this model
 */

/*
 *Function	: 	followUserAutomatically()
 *Purpose	: 	To create Json file to add special user
 *Arguments: 	(string)$accountHandle, (array)$specialUser.
*/
function followUserAutomatically($accountHandle,$accountHandlerUserId, $specialUsers) {
    Global $docRoot;
    foreach ($specialUsers as $specialUser) {

        $targetDirAccountHandler    = $docRoot . "json_directory/social/followerwonk/".$accountHandle."/";

        $folderExist                = folderPresenceCheck($targetDirAccountHandler);


        $targetDirOtherAccountHandler = $docRoot . "json_directory/social/followerwonk/".$specialUser['account_handle']."/";

        $folderExist                = folderPresenceCheck($targetDirOtherAccountHandler);


        $accountHandleWithFirstChar = strtolower($specialUser['account_handle'][0]);
        $jsonFileCreationOfOtherAccount     = strtolower($accountHandle[0]);

        if (has_specchar($accountHandleWithFirstChar)) {
            $accountHandleChar = "0";
        } else {
            $accountHandleChar = $accountHandleWithFirstChar;
        }


        if (has_specchar($accountHandle)) {
            $accountHandleOfUser = "0";
        } else {
            $accountHandleOfUser = $jsonFileCreationOfOtherAccount;
        }
        $accountHandlerFileName      = $targetDirAccountHandler.$accountHandle."_info_".$accountHandleChar.".json";

     $otherAccountHandlerFileName = $targetDirOtherAccountHandler.$specialUser['account_handle']."_info_".$accountHandleOfUser.".json";

        $data = array(
            'followings'=>[],
            'followers'=> [],
            'blocked'=>[]
        );

        if(!file_exists($otherAccountHandlerFileName))
        {
            if(file_put_contents($otherAccountHandlerFileName, json_encode($data,JSON_PRETTY_PRINT)))
            {
                $setJson = updationOfJsonFileForOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerUserId , $otherAccountHandleType, $otherAccountHandlerId );
            }
        }
        else
        {
            $setJson = updationOfJsonFileForOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerUserId , $otherAccountHandleType, $otherAccountHandlerId );

        }

        if (!file_exists($accountHandlerFileName)) {
            if(file_put_contents($accountHandlerFileName,json_encode($data,JSON_PRETTY_PRINT))) {
                $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType ,$accountHandlerId ,$otherAccountHandleType, $specialUser['user_id']);
                if (noError($setJson)) {
                    $xml_data["step4"]["data"]         = "Json File Creation Of Account Handler";
                    $xml_data["userinfo"]["data"]      = "";
                    $xml_data["userinfo"]["attribute"] = $setJson;
                } else {
                    $returnArr["errCode"] = 50;
                    $errMsg               = "Error in Adding Post Details.";
                }
            } else {
                $returnArr["errCode"] = 52;
                $errMsg               = "Error in Creating Json File For Handler.";
            }
        } else {
            if (file_put_contents($accountHandlerFileName, json_encode($data,JSON_PRETTY_PRINT))) {
                $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $specialUser['user_id']);
                if (noError($setJson)) {
                    $xml_data["step4"]["data"]         = "Json File Creation Of Account Handler";
                    $xml_data["userinfo"]["data"]      = "";
                    $xml_data["userinfo"]["attribute"] = $setJson;
                } else {
                    $returnArr["errCode"] = 50;
                    $errMsg               = "Error in Adding Post Details.";
                }
            } else {
                $returnArr["errCode"] = 52;
                $errMsg               = "Error in Creating Json File For Handler.";
            }
        }



    }
}



?>
