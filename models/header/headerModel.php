<?php
/*
*-----------------------------------------------------------------------------------------------------------
*    Header Model
*-----------------------------------------------------------------------------------------------------------
*
*   Description : This model is used to define method required for header.
*	This includes :
*
*   calculateUserBalance() : The calculateUserBalance() method is used to calculate user’s total available
*							 balance.
*
*
*/

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : calculateUserBalance()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    calculateUserBalance()
*   Purpose       :    To calculate user’s total available balance
*   Arguments     :    (array) $balanceArray.
*   Returns       :    (array) $returnArr User's available balance array.
*/

function calculateUserBalance($balanceArray)
{
	$extraArg 			= array();
    $returnArr 			= array();
    $myAvailableBalance = 0;

	if ($balanceArray['errCode'] == -1) {
		$myAvailableBalance = $balanceArray['errMsg']['deposit'] + $balanceArray['errMsg']['sales'] +
			$balanceArray['errMsg']['cashback'] + $balanceArray['errMsg']['affiliate_earning'] + $balanceArray['errMsg']['purchase_itd'] +
			$balanceArray['errMsg']['total_kwd_income'] + $balanceArray['errMsg']['search_earning'] +
			$balanceArray['errMsg']['social_content_view_earnings'] +
            $balanceArray['errMsg']['search_affiliate_earnings'] +
            $balanceArray['errMsg']['social_affiliate_earnings'] +
            $balanceArray['errMsg']['social_content_sharer_earnings'] + 
            $balanceArray['errMsg']['social_content_creator_earnings'] + 
            $balanceArray['errMsg']['total_app_income'] - $balanceArray['errMsg']['blocked_for_pending_withdrawals'] -
            $balanceArray['errMsg']['blocked_for_bids'] - $balanceArray['errMsg']['approved_withdrawals'] -
            $balanceArray['errMsg']['trade_fees'] -	$balanceArray['errMsg']['purchases'] -
            $balanceArray['errMsg']['renewal_fees'];
	}

	if($myAvailableBalance <= 0){
		$myAvailableBalance =   0.0000000;
	}

	$errMsg    = ['total_available_balance' => $myAvailableBalance];
 	$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

 	if ($balanceArray['errCode'] == 27) {
		$returnArr = $balanceArray;
	}

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getNotificationDetail()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getNotificationDetail()
*   Purpose       :    To get all Notification Details.
*   Arguments     :    (String) $id,(String)$category.
*   Returns       :    (array) $returnArr User's Notification Details array.
*/
function getNotificationDetail($id,$category,$count,$cursor_name, $cursor_value,$offset){

    global $walletPublicKey, $mode,$walletURLIPnotification;
    $retArray = array();

    if (!empty($id) && !empty($id)) {

        $headers = array();

        $apiText = "user_id=".$id."&publicKey=".$walletPublicKey;

        $postFields = "bellcount={$count}&category=".urlencode($category)."&publicKey=".urlencode($walletPublicKey);

        $apiName = "api/notify/v2/inbox/user/{$id}";

        if(isset($offset) && !empty($offset) && isset($cursor_value) && !empty($cursor_value)){
            $postFields = 'offset_from='.$offset.'&'.$cursor_name.'='.$cursor_value.'&publicKey=' . $walletPublicKey .'&category='.$category;
        }
//        echo $api_request_url;
        $requestUrl = $walletURLIPnotification;    //.$api_request_url;
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
       if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}


function deleteNotification($id,$category,$count){

    global $walletPublicKey, $mode,$walletURLIPnotification;
    $retArray = array();

    if (!empty($id) && !empty($id)) {

        $headers = array();

        $apiText = "user_id=".$id."&publicKey=".$walletPublicKey;
        $postFields = "del_container=".$id."publicKey=".urlencode($walletPublicKey);
        $apiName = "api/notify/v2/inbox/user/{$id}";
        $requestUrl = $walletURLIPnotification;
        $curl_type = 'DELETE';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

       if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;




}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function : changeUserSystemMode()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    changeUserSystemMode()
*   Purpose       :    To get all Notification Details.
*   Arguments     :    (String) $id,(String)$category.
*   Returns       :    (array) $returnArr User's Notification Details array.
*/
function changeUserSystemMode($id,$systemMode){
    global $walletPublicKey, $walletPrivateKey, $walletURLIPnotification;
    $ch              = curl_init();
    $returnArr       = array();
    $NotificationURL = $walletURLIPnotification."api/notify/v2/user/";
    $apiText         = "user_id=".$id."&systemmode=".$systemMode."&publicKey=".$walletPublicKey;
    $signature       = hash_hmac('sha512', $apiText, $walletPrivateKey);
    $api_request_url = $NotificationURL.$id.'/usermetadata?publicKey=' . $walletPublicKey . '&signature=' . $signature;
    $postFields      = array('systemmode' => $systemMode);

    curl_setopt($ch, CURLOPT_URL, $api_request_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($postFields));

    $result          = curl_exec($ch);
    $resultJson      = json_decode($result,true);
    $errCode         = $resultJson["errCode"];
    $errMsg          = $resultJson["errMsg"];
    if($errCode == -1){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]  = $errMsg;
        $returnArr            = setErrorStack($returnArr, -1, $errMsg);
    }else{
        /*error message*/
        $returnArr["errCode"] = $errCode;
        $returnArr["errMsg"]  = $errMsg;
        $returnArr            = setErrorStack($returnArr, $errCode, $errMsg);
    }

    return $returnArr;

}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getUserDefaultAppDetails()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getUserDefaultAppDetails()
*   Purpose       :    To get Default app Details.
*   Arguments     :    (String) $email,(String)$connSearch.
*   Returns       :    (array) $returnArr User's Default App Details array.
*/
function getUserDefaultAppDetails($email,$connSearch){
    global $walletURLIPnotification;
    $returnArr = array();

    $searchAppId = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/','default_search_appId');
    if (noError($searchAppId)) {
        $searchAppId = $searchAppId['errMsg']['default_search_appId'];              
        if (!empty($searchAppId)) { 
            $appDetails = getAppDetails($connSearch, $searchAppId);             
            if(noError($appDetails)){   
                $returnArr['errCode']                 = -1;   
                $appDetails                           = $appDetails["errMsg"][0];
                $returnArr['errMsg']['appName']       = $appDetails["app_name"];
                $returnArr['errMsg']['appId']         = $appDetails["app_id"];
                $returnArr['errMsg']['appLogo']       = $appDetails["app_logo"];
                $returnArr['errMsg']['appURL']        = $appDetails["searchresult_url"];
                $returnArr['errMsg']['appLandingUrl'] = $appDetails["app_url"];
                $returnArr['errMsg']['appSearchLogo'] = $appDetails["searchengine_images"];
                $returnArr['errMsg']['landingPage']   = $appDetails["landingPage_url"];                    
            }
        } else if (empty($searchAppId)){
            $appDetails = getDefaultAppId($connSearch);
            if(noError($appDetails)){
                $returnArr['errCode']                 = -1;
                $appDetails                           = $appDetails["errMsg"][0];
                $returnArr['errMsg']['appName']       = $appDetails["app_name"];
                $returnArr['errMsg']['appURL']        = $appDetails["searchresult_url"];
                $returnArr['errMsg']['appLandingUrl'] = $appDetails["app_url"];
                $returnArr['errMsg']['appId']         = $appDetails["app_id"];
                $returnArr['errMsg']['appLogo']       = $appDetails["searchengine_images"];
                $returnArr['errMsg']['appSearchLogo'] = $appDetails["searchengine_images"];
                $returnArr['errMsg']['landingPage']   = $appDetails["landingPage_url"];                
            }
        }
    } else {
        $returnArr['errCode'] = 1;
        $returnArr['errMsg']  = "Error In Fetching Data";
    }    
    $errCode         = $returnArr["errCode"];
    $errMsg          = $returnArr["errMsg"];
    if($errCode == -1){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"]  = $errMsg;
        $returnArr            = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    }else{
        /*error message*/
        $returnArr["errCode"] = $errCode;
        $returnArr["errMsg"]  = $errMsg;
        $returnArr            = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }

    return $returnArr;
}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getTransactionDetail()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getTransactionDetail()
*   Purpose       :    To get all Transaction Details, which will give last 5min transaction record against that keyword in "revenue_keyword_(character)" details.
*   Arguments     :    $id, $date_jumpers_gt (from timing), $date_jumpers_lt (to timing), $type (this feild will ensure type of transaction).
*   Returns       :    (array) $returnArr User's Earning Details array.
*/

//cronForEarningTransaction.php
function getTransactionDetail($date_jumpers_lt, $date_jumpers_gt,$short_type,$transaction_status){

    global $walletPublicKey, $mode,$walletURL,$transaction_status;
    $retArray = array();

    if (!empty($short_type)) {

        $headers = array();
        $apiText = "user_id=".'all'."&publicKey=".urlencode($walletPublicKey);

        $postFields = "date_jumpers_lt=".$date_jumpers_lt."&date_jumpers_gt=".urlencode($date_jumpers_gt)."&short_type=".$short_type."&transaction_status=".$transaction_status."&publicKey=".$walletPublicKey;

        $apiName = "pocket/all/transactions/";

        $requestUrl = $walletURL;    //.$api_request_url;
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}

function getTransactionInfoByEmail($date_jumpers_lt,$date_jumpers_gt,$type,$id){

    global $walletPublicKey, $mode,$walletURL;
    $retArray = array();
    $email = $_SESSION["email"];

    if (isset($type) && !empty($type)) {

        $headers = array();
        $apiText = "user_id=".$id."&user_email=".$email."&publicKey=".urlencode($walletPublicKey);

        $postFields = "user_email=".$email."&date_jumpers_lt=".$date_jumpers_lt."&date_jumpers_gt=".urlencode($date_jumpers_gt)."&type=".$type."&receiver_user_id=".$id."&publicKey=".$walletPublicKey;

        $apiName = "pocket/$id/transactions/";

        $requestUrl = $walletURL;
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getCountReferredUser()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getCountReferredUser()
*   Purpose       :    To get totalcount of Referral User
*   Arguments     :    $referralCode(referralCode of loggedin user)
*   Returns       :    (array) $returnArr User's Referral User Details array.
*/

function getCountReferredUser($referralCode){
    global $walletPublicKey, $mode,$NotificationURL;

    $retArray = array();
    $userId = $_SESSION["id"];
    if (!empty($referralCode)) {

        $headers = array();
        $postFields = $apiText = "user_id=".$userId."&referral_code=".$referralCode."&publicKey=".urlencode($walletPublicKey);

//        $postFields = "my_referral_id=".$referralCode."&publicKey=".$walletPublicKey;

        $apiName = "user/{$userId}/ref_count";

        $requestUrl = $NotificationURL."v2/";
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getTotalLikeCount()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getTotalLikeCount()
*   Purpose       :    To get totalcount of Like of loggedin user
*   Arguments     :    $user_id(loggedin user)
*   Returns       :    (array) $returnArr User's likeCount Details array.
*/


function getTotalLikeCount($postType){
    global $walletPublicKey, $mode,$socialUrl;

    $retArray = array();
    $headers = array();
    $apiText = "action={$postType}&publicKey=".urlencode($walletPublicKey);
    if ($postType == 'share') {
        $postFields = $apiText."&endPoint=count";
    } else {
        $postFields = $apiText;
    }

    $apiName = "{$postType}";

    $requestUrl = $socialUrl;
    $curl_type = 'GET';

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
//$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }
    return $retArray;

}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getCommunityPoolDetails()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getCommunityPoolDetails()
*   Purpose       :    To get all CommunityPool information
*   Arguments     :    (array) $requestUrl.
*/

function getCommunityPoolDetails($requestUrl)
{

    global  $walletPublicKey, $mode;
    $retArray = array();
    $header   = array();

    /* create signature */
    $postFields = $apiText      = "publicKey={$walletPublicKey}";
    $apiName      = 'transaction';
    $curl_type    = 'GET';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : getTotalPost()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    getTotalPost()
*   Purpose       :    Give Total Post Count based on keyword search
*   Arguments     :    $keyword.(take keyword from analytics page and based on keywords fetch post
                       total count)
*/

function getTotalPost($keyword,$requestUrl)
{

    global  $walletPublicKey, $mode;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText    = "keyword=".$keyword."&publicKey=".$walletPublicKey;
    $postFields = "&publicKey=".urlencode($walletPublicKey);
    $apiName    = "post/count/keyword/{$keyword}";
    $curl_type  = 'GET';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}


function getReferralEarninMonthly($email,$id,$month){

    global $walletPublicKey, $mode,$walletURL;
    $retArray = array();

        $headers = array();
        $apiText = "email=".$email."&publicKey=".urlencode($walletPublicKey);

        $postFields = "&email=".$email."&months=".$month."&publicKey=".$walletPublicKey;

        $apiName = "user/$id/analytics/userReferralDetails";

        $requestUrl = $walletURL;
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;

}

function getReferralInteraction($email,$requestUrl)
{

    global  $walletPublicKey, $mode;
    $retArray = array();
    $header   = array();

    /* create signature */
    $postFields = $apiText    = "email=".$email."&publicKey=".$walletPublicKey;
//    $postFields = "&publicKey=".urlencode($walletPublicKey);
    $apiName    = "v2/user/count/referral/shareandinteraction";
    $curl_type  = 'POST';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

?>
