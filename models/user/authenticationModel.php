<?php
/*
* --------------------------------------------------------------------------------------------
*       Authentication Model
*---------------------------------------------------------------------------------------------
*   This authenticaton model is used to authenticate the user activities of following moduels
*   1. Create new account :   user registration
*   2. Login              :   User loggin
*   3. Forget Password    :   Forget password is used to send authentication/verification email
*                             link to the requested email address.
*   4.  Resend Activation :   Resend activation is used to resend the activation link to the
*                             requested email address if user's account is not active or if
*                             verification link is expired.
*/



/*-----------------------------------------------------------------------------------------------------------------
*       Function register
*------------------------------------------------------------------------------------------------------------------
*     Function Name :   register()
*     Purpose       :   Create an account for new user.
*     Arguments     :   (array)$paramArray
*     Returns       :   json
*/

function register($paramArray){

    global $walletPublicKey, $walletURL, $mode, $rootUrl;
    $retArray = array();
    $headers = array();

    /* create signature */
    $apiText = "email={$paramArray['email']}&password={$paramArray['password']}&confirm_password={$paramArray['confirm_password']}&country={$paramArray['country']}&referral={$paramArray['referral']}&flag={$paramArray['flag']}&account_handle={$paramArray['account_handle']}&publicKey={$walletPublicKey}";
    $postFields = "first_name=" . rawurlencode($paramArray['first_name']) . "&last_name=" . rawurlencode($paramArray['last_name']) . "&email=" . urlencode($paramArray['email']) . "&gender=" . urlencode($paramArray['gender']) . "&password=" . utf8_encode($paramArray['password']) . "&confirm_password=" . utf8_encode($paramArray['confirm_password']) . "&system_mode=". $paramArray['system_mode'] . "&country=" . utf8_encode($paramArray['country']) . "&city=" . utf8_encode($paramArray['city']) . "&publicKey=" . urlencode($walletPublicKey) . "&referral=" . urlencode($paramArray['referral']) . "&flag=" . urlencode($paramArray['flag']) ."&account_handle=".rawurlencode(utf8_encode($paramArray['account_handle'])) . "&rootUrl=" . urlencode($rootUrl) . "&client_IP=".$paramArray['client_IP'];
    /*$postFields = "first_name=" . $paramArray['first_name'] . "&last_name=" . $paramArray['last_name'] . "&email=" . $paramArray['email'] . "&gender=" . $paramArray['gender'] . "&password=" . $paramArray['password'] . "&confirm_password=" . $paramArray['confirm_password'] . "&country=" . $paramArray['country'] . "&city=" . $paramArray['city'] . "&publicKey=" . $walletPublicKey . "&referral=" . $paramArray['referral'] . "&flag=" . $paramArray['flag'] ."&account_handle=".$paramArray['account_handle'] . "&rootUrl=" . $rootUrl . "&client_IP=".$paramArray['client_IP'];*/
    $apiName = 'user/signup';
    $requestUrl = "{$walletURL}";
    $curl_type = 'POST';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production'){
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)){
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        }else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    /* Making cur request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }

    return $retArray;
}
/*-----------------------------------------------------------------------------------------------------------------
*       Function getGoogleCaptchaResponse
*------------------------------------------------------------------------------------------------------------------
*     Function Name :   getGoogleCaptchaResponse()
*     Purpose       :   Get google captcha response while user signup.
*     Arguments     :   (string)$gCaptch, (string)$userIp
*     Returns       :   array with google response
*/

function getGoogleCaptchaResponse($gCaptch, $userIp)
{
    global $captchaPrivatekey;
    $retArray = array();

    if (!$gCaptch){
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "Skipped captcha during signup";
    } else {
        $response = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . $captchaPrivatekey . "&response=" . $gCaptch . "&remoteip=" . $userIp), true);

        if($response['success'] == true){
            $retArray["errCode"] = -1;
            $retArray["errMsg"]  = $response['success'];
        }else{
            $retArray["errCode"] = 3;
            $retArray["errMsg"]  = "Invalid Captcha";
        }
    }

    return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function login
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   login()
*     Purpose       :   To authenticate the users.
*     Arguments     :   (array)$paramArray
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function login($paramArray)
{
    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if (isset($paramArray['emailHandle']) && !empty($paramArray['emailHandle']) && isset($paramArray['password']) && !empty($paramArray['password'])) {

        $headers = array();

        //create signature
        $apiText = "username={$paramArray['emailHandle']}&password={$paramArray['password']}&publicKey={$walletPublicKey}";
        $postFields = "username=" . $paramArray['emailHandle'] . "&password=" . $paramArray['password'] . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = 'user/login';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function checkFieldStatus
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   checkFieldStatus()
*     Purpose       :   To check entered user email or account_handle or referal id exist in database or not.
*     Arguments     :   (string) $check_email/account_handle/referal
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function checkFieldStatus($req_type, $req_value)
{
    global $walletPublicKey, $mode,$walletURL;
    $retArray = array();

    if (!empty($req_value) && !empty($req_type)) {

        $headers = array();

        $apiText = "field={$req_type}&value={$req_value}&publicKey={$walletPublicKey}";
        $postFields = "field={$req_type}&value=" . urlencode($req_value) . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = 'user/checkAvailibity';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

       if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function resendVerificationLink
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   resendVerificationLink()
*     Purpose       :   To send link to user if registered user is not active.
*     Arguments     :   (string)$email
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function resendVerificationLink($email,$flag){

    global $walletPublicKey, $mode, $rootUrl, $walletURL;
    $retArray = array();

    if (!empty($email)){

        $headers = array();

        $apiText = "email={$email}&flag=1&publicKey={$walletPublicKey}";
        $postFields ="email=".$email."&flag=1&rootUrl=" . urlencode($rootUrl). "&publicKey=".urlencode($walletPublicKey);
        $apiName = 'user/reverify';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);


       if($mode == 'production'){
            $nonce_result = generateSignatureWithNonce($apiText);

            if(noError($nonce_result)){
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-t
                s: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            }else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if(noError($respArray)){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
        }else if($respArray["errCode"] != 73){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    }else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = "All fields are mandatory";
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function : checkEmailDomainStatus()
*-----------------------------------------------------------------------------------------------------------
*
*   Function Name :    checkEmailDomainStatus()
*   Purpose       :    To check Domain is Blocked or Not.
*   Arguments     :    $email,$conn
*   Returns       :    (array) errMsg contains status message.
*/

function checkEmailDomainStatus($email,$conn){

    $domain =trim(end(explode("@",$email)));
    $returnArr = array();
    $extraArgs = array();
    $query = "select domain_name from blocked_domains where domain_name='".$domain."';";
    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        $cmsRecords = array();
        $row = mysqli_num_rows($execQuery["dbResource"]);
        if($row>0)
        {
            $errMsg = "BLOCKED";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{
            $errMsg = "NOT BLOCKED";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error getting CMS details";
        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
    }

    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function verifyUser
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   verifyUser()
*     Purpose       :   To verify user when clicked on account verification link which was sent on
*                        registed user.
*     Arguments     :   (string)$email
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function verifyUser($email){

    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if(isset($email) && !empty($email)){
        $headers = array();

        //create signature
        $apiText = "email={$email}&publicKey={$walletPublicKey}";
        $postFields = "email=" . urlencode($email) . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = 'user/verify';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName,'apiText' => $apiText,'requestUrl' => $requestUrl,'curlPostFields' => $postFields,'log' => $paramArr['xml'],'curlType' => $curl_type);

         if($mode == 'production'){
              $nonce_result = generateSignatureWithNonce($apiText);
              if(noError($nonce_result)){
                  $nonce_result = $nonce_result['errMsg'];
                  $retArray['errCode'] = -1;
                  //$retArray['errMsg'] = $nonce_result['errMsg'];
                  $headers[] = "x-ts: {$nonce_result['timestamp']}";
                  $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
              }else {
                  $retArray['errCode'] = 2;
                  $retArray['errMsg'] = $nonce_result['errMsg'];
              }
          }

        $respArray = curlRequest($curlReqParam, $headers);

        if(noError($respArray)){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        }else if($respArray["errCode"] != 73){
            if($mode == 'production'){
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    }else{
            $retArray["errCode"] = 1;
            $retArray["errMsg"] = "Error: Email is missing";
     }

     return $retArray;
}



/*------------------------------------------------------------------------------------------------------------
*       Function signupBlocker
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   signupBlocker()
*     Purpose       :   This function prevent user to signup which client IP exceed the signup IP limit.
*     Arguments     :   (string)client_ip
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function signupIpBlocker($user_ip){

    $returnResp = array();
    $setDailyLimitContent = array();
    $setLifetimeLimitContent = array();
    $extraArgs = '';
    $errMsg = '';
    $errCode = '';

    /* Access global variables from config file */
    global $docRoot;

    /* Clean input prameters */
    $user_ip = decodeRequestParameter($user_ip);


    if(!empty($user_ip)){

        /* Initialize xml log */
        /*$xmlArray = initializeXMLLog($user_email);
        $xml_data['request']["data"]='';
        $xml_data['request']["attribute"]=$xmlArray["request"];

        $errMsg = "User signup ip blocker process start.";
        $xml_data['step1']["data"] = "1. {$errMsg}";*/

        /* Set Step1 attributes*/
        /*$xmlStep1Attr["userIP"]=$user_ip;
        $xmlStep1Attr["userEmail"]=$user_email;

        $xml_data['step1']["attribute"]=$xmlStep1Attr;*/

        /* Check required folder exist or not */
        $rootSecurityDir = "{$docRoot}security";

        /* check signup ip blocker directory exist or not */
        $signupIPBlockerDir = "{$rootSecurityDir}/signupIPBlocker";

        /* daily ip limit json file that store daily signup count */
        $dailyIPLimitJson = "{$signupIPBlockerDir}/daily_signup_ip_limit.json";

        /* lifetime ip limit json file that stores lifetime sign up count */
        $lifetimeIPLimitJson = "{$signupIPBlockerDir}/lifetime_signup_ip_limit.json";

        /* Create security directory inside root */
        if(!is_dir($rootSecurityDir)){
            mkdir($rootSecurityDir, 0777, true);
        }

        /* create signup ip blocker directory in security directory if not exist */
        if(!is_dir($signupIPBlockerDir)){
                mkdir($signupIPBlockerDir, 0777, true);
        }

        if(!file_exists($dailyIPLimitJson)){
            $fp = fopen($dailyIPLimitJson, "w");
            if($fp){
                if(file_exists($lifetimeIPLimitJson)){

                    $getLifetimeLimitContent = file_get_contents($lifetimeIPLimitJson);
                    $getLifetimeLimitContent = json_decode($getLifetimeLimitContent, true);

                    $ipExist = array_key_exists($user_ip, $getLifetimeLimitContent);
                    $getLifetimeIpCount = $getLifetimeLimitContent["{$user_ip}"];

                    if($ipExist){

                        if($getLifetimeIpCount < 200){
                            $errMsg = 'Success';
                            $errCode = -1;
                            $setDailyLimitContent["{$user_ip}"] =  1;
                            $setLifetimeLimitContent["{$user_ip}"] = ($getLifetimeLimitContent["{$user_ip}"] + 1);
                        }else{
                            $errMsg = 'Error: Ip Blocked For Signup';
                            $errCode = 2;
                        }
/*
                        $dailyContentJson = json_encode($setDailyLimitContent);
                        $lifetimeContentJson = json_encode($setLifetimeLimitContent);

                        file_put_contents($dailyIPLimitJson, $dailyContentJson);
                        file_put_contents($lifetimeIPLimitJson, $lifetimeContentJson);*/

                    }else{
                        $setDailyLimitContent["{$user_ip}"] =  1;
                        $setLifetimeLimitContent["{$user_ip}"] = 1;

                        $errMsg = 'Success';
                        $errCode = -1;

                    }

                    $dailyContentJson = json_encode($setDailyLimitContent);
                    $lifetimeContentJson = json_encode($setLifetimeLimitContent);

                    file_put_contents($dailyIPLimitJson, $dailyContentJson);
                    file_put_contents($lifetimeIPLimitJson, $lifetimeContentJson);

                }else{
                    $errMsg = 'Error: lifetime json file not found';
                    $errCode = 2;
                }
            }else{
                $errMsg = 'Error: unable to create daily ip limit json file';
                $errCode = 2;
            }
        }else{
            $getDailyLimitContent = file_get_contents($dailyIPLimitJson);
            $getDailyLimitContent = json_decode($getDailyLimitContent, true);

            if(file_exists($lifetimeIPLimitJson)){

                $getLifetimeLimitContent = file_get_contents($lifetimeIPLimitJson);
                $getLifetimeLimitContent = json_decode($getLifetimeLimitContent, true);

                $ipExistDaily = array_key_exists($user_ip, $getDailyLimitContent);
                $ipExistLiftime = array_key_exists($user_ip, $getLifetimeLimitContent);

                $getDailyIpCount = $getDailyLimitContent["{$user_ip}"];
                $getLifetimeIpCount = $getLifetimeLimitContent["{$user_ip}"];

                if($ipExistDaily && $ipExistLiftime){


                    if($getDailyIpCount < 50 && $getLifetimeIpCount < 200){
                        $errMsg = 'Success';
                        $errCode = -1;
                        $setDailyLimitContent["{$user_ip}"] = ($getDailyLimitContent["{$user_ip}"] + 1);
                        $setLifetimeLimitContent["{$user_ip}"] = ($getLifetimeLimitContent["{$user_ip}"] + 1);
                    }else if($getDailyIpCount >= 50 && $getLifetimeIpCount < 200){
                        $errMsg = 'Error: User signup blocked for today';
                        $errCode = 2;
                        $setDailyLimitContent["{$user_ip}"] = $getDailyLimitContent["{$user_ip}"];
                        $setLifetimeLimitContent["{$user_ip}"] = $getLifetimeLimitContent["{$user_ip}"];
                    }else{
                        $errMsg = 'Error: IP Blocked for Signup';
                        $errCode = 2;
                        $setDailyLimitContent["{$user_ip}"] = $getDailyLimitContent["{$user_ip}"];
                        $setLifetimeLimitContent["{$user_ip}"] = $getLifetimeLimitContent["{$user_ip}"];
                    }

                }else if(!$ipExistDaily && !$ipExistLiftime){

                    $errMsg = 'Success';
                    $errCode = -1;
                    $setDailyLimitContent["{$user_ip}"] =  1;
                    $setLifetimeLimitContent["{$user_ip}"] = 1;
                }

                $dailyContentJson = json_encode($setDailyLimitContent);
                $lifetimeContentJson = json_encode($setLifetimeLimitContent);

                file_put_contents($dailyIPLimitJson, $dailyContentJson);
                file_put_contents($lifetimeIPLimitJson, $lifetimeContentJson);

            }else{
                $errMsg = 'Error: lifetime json file not found';
                $errCode = 2;
            }

        }

    }else{
        $errMsg = "Mandatory fields required.";
        $errCode = 2;
        /*$returnResp = setErrorStack($returnResp, 1, $errMsg, $extraArgs);
        $xml_data['step1']["data"] = "1. {$errMsg}";*/
    }

    $returnResp['errCode'] = $errCode;
    $returnResp['errMsg'] = $errMsg;

    return $returnResp;

    /* If not, Then create or proceed next */
    /* Check user ip exist in dailySignupIpCount.json */
    /* If true, Then check user ip exist in lifetimeSignupIpCount.json */
    /* If true, then increase signup Ip count  by 1 in both above json files*/
    /* If false, Then add new ip with ipCount 1 in both above json */
}

/*------------------------------------------------------------------------------------------------------------
*       Function forgotPassword()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   forgotPassword()
*     Purpose       :   To send forget password link to email.
*     Arguments     :   (string)$email.$flag (int) device type 1:desktop,$rooturl
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function forgotPassword($email,$flag){
    global $walletPublicKey, $mode,$rootUrl, $walletURL;

    $retArray = array();
    if (!empty($email)) {

        $headers = array();

        $apiText = "email={$email}&flag={$flag}&publicKey={$walletPublicKey}";
        $postFields = "email=".$email."&flag={$flag}&rootUrl=" . urlencode($rootUrl) . "&publicKey=" . urlencode($walletPublicKey);
        $apiName = 'user/password/forgot';
        $requestUrl = "{$walletURL}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function setResetPassword()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   setResetPassword()
*     Purpose       :   To reset the user password.
*     Arguments     :   (string)email, (string)$password, (string) confirm_password, (string) auth,
*                   :   (timestamp) timestamp
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function setResetPassword($email, $password, $confirmPassword, $auth, $timestamp){

    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();

    if (!empty($email)) {

        $headers = array();

         $apiText = "email={$email}&auth={$auth}&password={$password}&confirm_password={$confirmPassword}&time={$timestamp}&publicKey={$walletPublicKey}";
         $postFields = "email=".$email."&password={$password}&confirm_password={$confirmPassword}&auth=".rawurlencode($auth)."&time=".$timestamp."&publicKey=" . urlencode($walletPublicKey);

        $apiName = 'user/password/reset';
        $requestUrl = "{$walletURL}";
        $curl_type = 'PUT';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function verifyResetPswdAuthLink()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   verifyResetPswdAuthLink()
*     Purpose       :   To validate the the verification link which is sent to user's email link is valid
*                   :   or not.
*     Arguments     :   (string) auth, (string) eamil, (timestamp) timestamp.
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function verifyResetPswdAuthLink($verifyAuthArr){
  global $walletPublicKey, $mode, $walletURL;

  $retArray = array();
  $email = $verifyAuthArr["email"];
  $auth = $verifyAuthArr["auth"];
  $timestamp = $verifyAuthArr["timestamp"];

  if (!empty($email) && !empty($auth) && !empty($timestamp)) {

      $headers = array();

       $apiText = "email={$email}&auth={$auth}&time={$timestamp}&publicKey={$walletPublicKey}";
       $postFields = "email=".$email."&auth=".rawurlencode($auth)."&time=".$timestamp."&publicKey=" . urlencode($walletPublicKey);

      $apiName = 'user/verifyAuthLink';
      $requestUrl = "{$walletURL}";
      $curl_type = 'POST';
      $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

      if ($mode == 'production') {
          $nonce_result = generateSignatureWithNonce($apiText);
          if (noError($nonce_result)) {
              $nonce_result = $nonce_result['errMsg'];
              $retArray['errCode'] = -1;
              //$retArray['errMsg'] = $nonce_result['errMsg'];
              $headers[] = "x-ts: {$nonce_result['timestamp']}";
              $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
          } else {
              $retArray['errCode'] = 2;
              $retArray['errMsg'] = $nonce_result['errMsg'];
          }
      }

      $respArray = curlRequest($curlReqParam, $headers);

      if (noError($respArray)) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = -1;
          $retArray['errMsg'] = $respArray['errMsg'];
          $retArray['mode'] = $mode;

      } else if ($respArray["errCode"] != 73) {
          if ($mode == 'production') {
              $nonce_counter = incrementVectorCounter();
          }
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg'] = $respArray['errMsg'];
      } else {
          $retArray['errCode'] = $respArray['errCode'];
          $retArray['errMsg'] = $respArray['errMsg'];
      }

  } else {
      $retArray["errCode"] = 2;
        $retArray["errMsg"] = "Link is already used/modified.";
  }

  return $retArray;
}

/*------------------------------------------------------------------------------------------------------------
*       Function sendVerificationAuth()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   sendVerificationAuth()
*     Purpose       :   To send an email verification on success of registration.
*     Arguments     :   (array) verifyParam .
*/

function sendVerificationAuth($param){
    global $mode;
    $returnArr = array();
    $headers = array();
    global $rootUrl, $rootUrlImages, $walletPublicKey, $walletURLIPnotification;

    if(isset($param["account_id"]) && !empty($param["account_id"]) && isset($param["email"]) && !empty($param["email"]) && isset($param["first_name"]) && !empty($param["first_name"]) && isset($param["last_name"]) && !empty($param["last_name"]) && isset($param["password"]) && !empty($param["password"]) && isset($param["salt"]) && !empty($param["salt"])){

        // get current time
        $currentTime = time();

        // convert current time from decimal to hexadecimal.
        $decHex = dechex($currentTime);

        // create auth token using sha1 algorithem
        $authToken = sha1($param["account_id"].$currentTime.$param["password"].$param["salt"]).$decHex.dechex($param["account_id"]);

        // create verification link
        $url = $rootUrl."views/user/verifyUser.php?auth=".$authToken."&email=".$param["email"]."&sn_t=".$decHex."&flag=1";

        if($param["email_status"] == 1){
            $body = '<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff" align="center"> 
                        <tbody> 
                            <tr> 
                            <td> <table style="font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif; width:600px;margin:0 auto; color:#444444; " width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" align="center"> <tbody> <tr> <td style="padding: 19px 20px; background:#0b6796;"> <img src="https://lh3.google.com/u/0/d/0B7dA4POfLcDPc0ZaUFVzSmFJU0U=w1556-h772-iv1">
               </td></tr><tr> <td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hello '.$param["first_name"].' ' .$param["last_name"].'</td></tr>
               <tr> <td style="padding: 10px 20px;">
               You just signed up for Keywo.<br> 
                Please click <a href="'.$url.'"> Here </a> to confirm that it is your email address or copy/paste the link below into your browser.<br /><br />
            
             <a href="'. $url.'" >' . $url . ' </a> </br><br />
            
                Thanks!!!<br><br>Note: Please do not reply to this e-mail, this is sent from an unattended mail box. In case you have any queries/responses, please write us at support@keywo.com</td></tr> </tbody> </table> </td></tr></tbody> </table>';
        }else if($param["email_status"] == 2){

            $body = '<table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff" align="center"> <tbody> <tr> <td> <table style="font-family:Helvetica neue,Helvetica,Arial,Verdana,sans-serif; width:600px;margin:0 auto; color:#444444; " width="600" cellpadding="0" cellspacing="0" border="0" bgcolor="#ffffff" align="center"> <tbody> <tr> <td style="padding: 19px 20px; background:#0b6796;"> <img src="https://lh3.google.com/u/0/d/0B7dA4POfLcDPc0ZaUFVzSmFJU0U=w1556-h772-iv1">
               </td></tr><tr> <td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hello '.$param["first_name"].' ' .$param["last_name"].'</td></tr>
               <tr> <td style="padding: 10px 20px;">
               Thanks for signing up for Keywo.<br>
                Please complete the account verification process by clicking on the link below : <br /><br />
            
             <a href="'. $url.'" >' . $url . ' </a> </br><br />
            
                Regards,<br>Team Keywo</td></tr></tbody> </table> </td></tr></tbody> </table>';

        }



        // encode email body because without encode the email body breaks.
        $body =  urlencode($body);

        $apiText = "email={$param["email"]}&emailBody={$body}&publicKey={$walletPublicKey}";
        $postFields = "email=".$param["email"]."&emailBody=".$body."&publicKey=" . urlencode($walletPublicKey);

        $apiName = 'api/notify/v2/account/verify';
        $requestUrl = "{$walletURLIPnotification}";
        $curl_type = 'POST';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $returnArr['errCode'] = 2;
                $returnArr['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);


        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $returnArr['errCode'] = -1;
            $returnArr['errMsg'] = $respArray['errMsg'];
            $returnArr['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $returnArr['errCode'] = $respArray['errCode'];
            $returnArr['errMsg'] = $respArray['errMsg'];
        } else {
            $returnArr['errCode'] = $respArray['errCode'];
            $returnArr['errMsg'] = $respArray['errMsg'];
        }

    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Mandatory fields are missing";
    }

    return $returnArr;
}




/*------------------------------------------------------------------------------------------------------------
*       Function checkHandleReserved()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   checkHandleReserved()
*     Purpose       :   To check handle is reserved or not.
*     Arguments     :  handle,useremail .
*/
function checkHandleReserved($handle,$email)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();
    $headers = array();

    $apiText ="handle=".$handle."&publicKey=".$walletPublicKey;
    $postFields =$apiText."&useremail=".$email;
    $apiName = 'admin/checkhandle';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}



/*------------------------------------------------------------------------------------------------------------
*       Function changeHandleFlagStatus()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   changeHandleFlagStatus()
*     Purpose       :   To change handle flag status 2 so we conside handle is used for signup.
*     Arguments     :  handle,useremail .
*/

function changeHandleFlagStatusAfterSignup($handle,$email)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();
    $headers = array();
    /* create signature */

    $apiText ="handle=".$handle."&useremail=".$email."&publicKey=".$walletPublicKey;
    $postFields =$apiText;
    $apiName = 'admin/changeflag';
    $curl_type = 'PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);


    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


//-----------------------------***********************************------------------------------------------

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getPasswordFlag
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getPasswordFlag()
*   Purpose       :   get password Value user
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function getPasswordFlag($password,$user_id)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();
    $headers = array();
    $apiText    = "user_id=".$user_id."&password=".$password."&publicKey={$walletPublicKey}";
    $postFields = "user_id=".$user_id."&password=".$password."&publicKey=" . $walletPublicKey;
    $apiName = 'user/checkpassword';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;

}

/*------------------------------------------------------------------------------------------------------------
*       Function setPassword()
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   setPassword()
*     Purpose       :   To reset the user password  and send Email.
*     Arguments     :   (string)user_id, (string)old_password, (string) confirm_new_password,
*     Response      :   (json) On success : {"errCode":"-1", "errMsg": "<success messge>"}
*                       (json) On failure : {"errCode":"<failuer error code>", "errMsg":"<failure error msg>"}
*/

function setPassword($user_id, $oldPassword, $new_password, $confirm_new_password, $ipInfo,$browserName,$osInfo){
    $oldPassword          = base64_decode($oldPassword);
    $new_password         = base64_decode($new_password);
    $confirm_new_password = base64_decode($confirm_new_password);

    global $walletPublicKey, $mode, $walletURL;
    $retArray = array();
    $headers = array();

    $apiText = "user_id=".$user_id."&old_password=".$oldPassword."&new_password=".$new_password."&confirm_new_password=".$confirm_new_password."&publicKey=" . $walletPublicKey;

    $postFields = "old_password=".$oldPassword."&new_password=".$new_password."&confirm_new_password=".$confirm_new_password. "&userip=" .$ipInfo . "&browser_name=" .$browserName . "&os_type=" . $osInfo . "&publicKey=" . $walletPublicKey;

    $apiName = "user/".$user_id.'/password/change';

    $requestUrl = "{$walletURL}";
    $curl_type = 'PUT';

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }

    return $retArray;
}
