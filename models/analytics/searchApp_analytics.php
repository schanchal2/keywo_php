<?php


function insert_app_activity($update_field_name,$conn){

    $newTable=createTable_keywo_App_analytics($conn);

    $current_year=date("Y");
    $current_month=date("m");
    $current_date=date("Y-m-d");

    $extraArg=array();
    $returnArr=array();

    if (noError($newTable)) {
        $query="SELECT * FROM daily_app_analytics_{$current_year} WHERE tracking_date = '{$current_date}'";

        $result = runQuery($query, $conn);


        if (noError($result)) {

            if(mysqli_num_rows($result["dbResource"])){


                $query="UPDATE daily_app_analytics_{$current_year} SET  {$update_field_name} = {$update_field_name}+1 WHERE tracking_date='{$current_date}' ";

                $result =  runQuery($query, $conn);

                if (noError($result)) {

                    $errMsg = "App Details update sucessfully";
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


                } else {
                    $returnArr = setErrorStack($returnArr,"A02", null, $extraArg);
                }

            }else{

                $query="INSERT INTO daily_app_analytics_{$current_year} (tracking_date,{$update_field_name})
                                   VALUES('{$current_date}', '1');";

                $result =  runQuery($query, $conn);

                if (noError($result)) {

                    $errMsg = "App Details Insert sucessfully";
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

                } else {
                    $returnArr = setErrorStack($returnArr,"A01", null, $extraArg);
                }
            }
        } else {

            $returnArr = setErrorStack($returnArr,"3", null, $extraArg);
        }

        return $returnArr;
    } else { $returnArr = setErrorStack($returnArr,"A04", null, $extraArg); }
}






function createTable_keywo_App_analytics($conn){


    $current_year=date("Y");
    $current_month=date("m");
    $extraArg=array();
    $returnArr=array();

    $query="SELECT count(*) as existStatus FROM information_schema.TABLES WHERE (TABLE_SCHEMA = 'dbsearch') AND (TABLE_NAME = 'daily_app_analytics_{$current_year}')";

    $result = runQuery($query,$conn);
    while ($row = mysqli_fetch_assoc($result["dbResource"]))
        $res = $row;
    $lastYear= date("Y",strtotime("-1 year"));
    if (!$res["existStatus"]) {

        $query="CREATE TABLE IF NOT EXISTS daily_app_analytics_{$current_year} LIKE daily_app_analytics_{$lastYear}";

        $result = runQuery($query,$conn);

        if (noError($result)) {

            $errMsg = "daily_app_analytics table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr,"A03", null, $extraArg);
        }


    }else{

        $errMsg = "table all ready exist";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    }
    return $returnArr;
}
?>