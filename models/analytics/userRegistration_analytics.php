<?php
function insert_user_statistics($statType, $conn)
{


    $extraArg = array();
    $returnArr = array();

    switch ($statType) {

        case "registered_user":

            $registred_user = "registered_users";
            $unverified_users = "unverified_users";

            $query = "UPDATE keywo_stats SET  {$registred_user} = {$registred_user}+1,{$unverified_users} = {$unverified_users}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "active_users":

            $active_user = "active_users";
            $unverified_users = "unverified_users";

            $query = "UPDATE keywo_stats SET  {$active_user} = {$active_user}+1,{$unverified_users} = {$unverified_users}-1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "users_with_refferal":

            $user_with_referral = "users_with_refferal";
            $registred_user = "registered_users";
            $unverified_users = "unverified_users";

            $query = "UPDATE keywo_stats SET  {$user_with_referral} = {$user_with_referral}+1,{$registred_user} = {$registred_user}+1,{$unverified_users} = {$unverified_users}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "blocked_users_block":

            $update_field_name = "blocked_users";
            $active_user = "active_users";

            $query = "UPDATE keywo_stats SET  {$update_field_name} = {$update_field_name}+1,{$active_user} = {$active_user}-1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "blocked_users_unblock":

            $update_field_name = "blocked_users";
            $active_user = "active_users";

            $query = "UPDATE keywo_stats SET  {$update_field_name} = {$update_field_name}-1,{$active_user} = {$active_user}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "kyc_level_3_users":

            $update_field_name = "kyc_level_3_users";

            $query = "UPDATE keywo_stats SET  {$update_field_name} = {$update_field_name}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;
    }
    return $returnArr;

}

?>