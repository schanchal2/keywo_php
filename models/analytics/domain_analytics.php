<?php

function insert_domain_activity($update_field_name, $domain, $conn)
{


    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM domain_analytics WHERE domain='{$domain}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE domain_analytics SET  {$update_field_name} = {$update_field_name}+1 WHERE domain='{$domain}'";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Domain Data update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "D2", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO domain_analytics(domain,{$update_field_name}) VALUES('{$domain}', '1');";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Domain Data Inserted sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "D3", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}

function deduct_field_count_domain($update_field_name,$domain,$conn)
{

    $extraArg = array();
    $returnArr = array();


    $query = "UPDATE domain_analytics SET  {$update_field_name} = {$update_field_name}-1 WHERE domain='{$domain}'";

    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "Domain Data update sucessfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


    } else {
        $returnArr = setErrorStack($returnArr, "D2", null, $extraArg);
    }


    return $returnArr;

}

function createTable_domain_analytics($conn)
{

    $extraArg = array();
    $returnArr = array();

    $query = "CREATE TABLE `domain_analytics` (
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `domain` varchar(255) DEFAULT NULL,
                  `total_registered_user` bigint(22) DEFAULT '0',
                  `total_verified_user` bigint(22) DEFAULT '0',
                  `total_unverified_user` bigint(22) DEFAULT '0',
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
                    ";
        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "domain_analytics table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "D1", null, $extraArg);
        }



    return $returnArr;
}


?>