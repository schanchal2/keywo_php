<?php 


error_reporting(0);

//All Widgets Functions
//Function For Query Search of any widget type
function query_analyzer($sq,$conn) {	

	//print_r(func_get_args()); die;
	$sq = $sq;	 // echo $sq; 
	$searchkey1 = explode(" ",$sq);
    $detail2=array(); 
    $index=0; 
	$typesArr = array(); 
	$wordsArr = array(); 

//print_r($searchkey1); die;
  
	foreach ($searchkey1 as $key => $value) { 
		if($value!=""){ 
			if (strpos($value,':') !== false) {
				$qexplode = explode(':', $value);
				if(strtolower($qexplode[0])=="coins" || strtolower($qexplode[0])=="Coins" || $qexplode[0]=="weather"||$qexplode[0]=="Weather"||$qexplode[0]=="calculator"||$qexplode[0]=="Calculator"||$qexplode[0]=="currency"||$qexplode[0]=="Length"||$qexplode[0]=="Area"||$qexplode[0]=="Bandwidth"||$qexplode[0]=="timezone"||$qexplode[0]=="Storage"||$qexplode[0]=="Energy"||$qexplode[0]=="Frequency"||$qexplode[0]=="Mileage"||$qexplode[0]=="Mass"||$qexplode[0]=="Angle"||$qexplode[0]=="Pressure"||$qexplode[0]=="Speed"||$qexplode[0]=="Volume"||$qexplode[0]=="Timeunit"||$qexplode[0]=="Currency"||$qexplode[0]=="Timezone"||$qexplode[0]=="length"||$qexplode[0]=="area"||$qexplode[0]=="bandwidth"||$qexplode[0]=="storage"||$qexplode[0]=="energy"||$qexplode[0]=="frequency"||$qexplode[0]=="mileage"||$qexplode[0]=="mass"||$qexplode[0]=="angle"||$qexplode[0]=="pressure"||$qexplode[0]=="speed"||$qexplode[0]=="volume"||$qexplode[0]=="timeunit"||$qexplode[0]=="temperature"||$qexplode[0]=="Temperature"){
					$query = "SELECT * FROM query_type WHERE type like '%".$qexplode[0]."%' LIMIT 0,1";
					$result = runQuery($query, $conn); 
        
					if(noError($result)){
						$res = array();
						$row = mysqli_fetch_assoc($result["dbResource"]);
						$type = $row["type"];
						if(array_key_exists($type, $typesArr))
							$typesArr[$type]++;
						else
							$typesArr[$type]=1;
						    $wordsArr[$value]=$type;
					}
				}
			}
			$query = "SELECT * FROM query_type WHERE innerfield like '%,".$value.",%' LIMIT 0,1";
			$result = runQuery($query, $conn);

			if(noError($result)){
				$res = array();
				$row = mysqli_fetch_assoc($result["dbResource"]);
				$type = $row["type"];
				if(array_key_exists($type, $typesArr))
					$typesArr[$type]++;
				else
					$typesArr[$type]=1;
				    $wordsArr[$value]=$type;
			}
		}
	 }
    arsort($typesArr);
    $returnArr["errCode"]=-1;
    $returnArr["errMsg"] = array("types"=>$typesArr, "words"=>$wordsArr);
    //print_r($returnArr);

    
	return $returnArr;
}

//Function to get date time of given timezone and difference between given timezone
function date_convert($conn,$dt, $tz1, $df1, $tz2, $df2) {  
	$tz1 = $tz1;
	$tz2 = $tz2;
    $output = '';
    $time_result = array(); 
    $list = timezone_identifiers_list();
    if(!in_array($tz1, $list)) { // check source timezone
		trigger_error(__FUNCTION__ . ': Invalid source timezone ' . $tz1, E_USER_ERROR);
    } else if(!in_array($tz2, $list)) { // check destination timezone
        trigger_error(__FUNCTION__ . ': Invalid destination timezone ' . $tz2, E_USER_ERROR);
    } else {
		$d = DateTime::createFromFormat($df1, $dt, new DateTimeZone($tz1));
		// check source datetime
		if($d && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0) {
			// convert timezone
			$d->setTimeZone(new DateTimeZone($tz2));
			// convert dateformat
			$output  = $d->format($df2);
			} else {
			trigger_error(__FUNCTION__ . ': Invalid source datetime ' . $dt . ', ' . $df1, E_USER_ERROR);
		}
		$old_date_timestamp = strtotime($output);
		$new_date = date('l h:i A', $old_date_timestamp);
	}
	$datetime1 = new DateTime();
	$datetime2 = new DateTime($output);
	$interval = $datetime1->diff($datetime2);
	$timediff = $interval->format('%h hours and %i minutes');
	$df3 = date('h:i:s a m/d/Y',time());
	$old_date_timestamp1 = strtotime($df3);
	$new_date1 = date('l h:i A', $old_date_timestamp1);
	$time_result["curr_date"]=$new_date1;
	$time_result["res_date"]=$new_date;
	$time_result["time_diff"]=$timediff;
    
	return $time_result;
}

//Function to get Current datetime of given timezone
function converToTz($conn,$time,$toTz,$fromTz) { 
	$toTz = $toTz;
	$fromTz = $fromTz;
    $date = new DateTime($time, new DateTimeZone($fromTz));
    $date->setTimezone(new DateTimeZone($toTz));
    $time= $date->format('h:i A l');
   
   return $time;
}

//Function to get typelist of unit converter widget
function typeanalyzer($conn){
    $returnArr = array();
    $query = "SELECT type from query_type where id>4 and id<18 or id=22;";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row=mysqli_fetch_assoc($result["dbResource"])){
            $res[$index]=$row["type"];
            $index++;
        }
		$returnArr["errCode"]=-1;
		$returnArr["errMsg"]=$res;
	} else {
		$returnArr["errCode"][5]=5;
		$returnArr["errMsg"]=$result["errMsg"];
	}
	
    return $returnArr;
}

//Function to get unit setlist of particular unit type
function unitset($conn,$unitdata) {
	$unitdata = $unitdata;
    $returnArr = array();
    $query = "SELECT type_set from unit_type where unit_type='".$unitdata."'";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res = $row["type_set"];
        }
    $returnArr["errCode"]=-1;
    $returnArr["errMsg"]=$res;
	} else {
		$returnArr["errCode"][5]=5;
		$returnArr["errMsg"]=$result["errMsg"];
	}
	
	return $returnArr;
}


//Function to get currency convert Amount of given currency code
function convertCurrency( $fromCurrCode, $toCurrCode, $amount, $conn){
	//echo "testing";
//	print_r(func_get_args());die;

	$returnArr = array();
	  $fromCurrCode = $fromCurrCode;
	 $toCurrCode = $toCurrCode; 
	
	//Function to get current price of given currency code
	$result = currency_shortCodeCheck($fromCurrCode,$conn);
	//print_r($result);
	
	if(noError($result)){
		$fromCurrCodeToUSD = $result['errMsg']['current_price'];
		
		//Function to get current price of given currency code
		$result = currency_shortCodeCheck($toCurrCode,$conn);
		if(noError($result)){
			$toCurrCodeToUSD = $result['errMsg']['current_price'];
			$finalamt = $toCurrCodeToUSD/$fromCurrCodeToUSD;
			if($amount === ""){
				$displayAmount = $finalamt * 1;
			}else{
				$displayAmount = $finalamt*$amount;
			}
			if($amount === 0){
				$displayAmount = $finalamt*0;	
			}
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $displayAmount." ".strtoupper($toCurrCode);
		}else{
			$returnArr["errCode"][5] = 5;
			$returnArr["errMsg"] = $result["errMsg"];
		}
	}else{
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get current price of given currency code
function currency_shortCodeCheck($shortforms,$conn){
	$returnArr = array();
	$shortforms = $shortforms;
    $query = "SELECT * from currency_value where shortforms ='$shortforms'";
	$result = runQuery($query, $conn);
	
	if(noError($result)){
		$res = array();
		while ($row = mysqli_fetch_assoc($result["dbResource"]))
			$res = $row;
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get currency longform of given currency code
function currency_longformCheck($CurrCode,$conn){

	$CurrCode = $CurrCode;
	$returnArr = array();
	$query = "SELECT longforms from currency_value where shortforms ='$CurrCode'";
	$result = runQuery($query, $conn);
	if(noError($result)){
		$res = array();
		while ($row = mysqli_fetch_assoc($result["dbResource"]))
		$res = $row;
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get currency longform and shortform of given currencycode
function currencylist($conn){
		$returnArr = array();
		$query = "SELECT longforms,shortforms from currency_value ORDER BY longforms";
		$result = runQuery($query, $conn);
		$index = 0;
		if(noError($result)){
			$res = array();
			while($row = mysqli_fetch_assoc($result["dbResource"])){
				$res[$index] = $row["longforms"].','.$row["shortforms"];
				$index++;
			}
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $res;
		} else {
			$returnArr["errCode"][5] = 5;
			$returnArr["errMsg"] = $result["errMsg"];
		}
		
		return $returnArr;
}

//Function to get unit longform and shortform of given unit name
function unit_long_list($conn){
    $returnArr = array();
    $query = "SELECT u_longforms,u_shortforms from unit_value";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["u_longforms"].','.$row["u_shortforms"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get timezonelist
function timezonelist($conn){
    $returnArr = array();
    $query = "SELECT longforms,shortforms,timezone from timezone_value ORDER BY longforms";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["longforms"].','.$row["shortforms"].','.$row["timezone"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get Shortcode(abbreviation) name of given timezone
function timezone_name($conn){
    $returnArr = array();
    $query = "SELECT shortforms from timezone_value";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["shortforms"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get all cities
function citylist($conn){
    $returnArr = array();
    $query = "SELECT name from cities";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["name"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get All states
function statelist($conn){
    $returnArr = array();
    $query = "SELECT name from states";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["name"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
  return $returnArr;
}

//Function to get All countries
function countrylist($conn){
    $returnArr = array();
    $query = "SELECT name from countries";
    $result = runQuery($query, $conn);
    $index = 0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["name"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get city,state,countryname from given cityname
function countrystatename($sq,$conn){
	$sq = $sq;
	$sq = str_replace(' ', '', $sq);
    $returnArr = array();
    $query = "select s.name as statename,cnt.name as countryname,c.name as cityname from cities as c
			inner join states as s on c.state_id=s.state_id 
			inner join countries as cnt on cnt.country_id=s.country_id 
			where c.name='".$sq."'";
    $result = runQuery($query, $conn);
    $index = 0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["cityname"].','.$row["statename"].','.$row["countryname"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get list of all shortforms of currency
function currencyshortname($conn){
    $returnArr = array();
    $query = "select shortforms from currency_value;";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["shortforms"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get shortcode of given country
function countryshortname($conn){
    $returnArr = array();
    $query = "select name,sortname from countries";
    $result = runQuery($query, $conn);
    $index = 0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["name"].','.$row["sortname"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}

//Function to get language name list
function languageList($conn){
    $returnArr = array();
    $query = "select language,languagecode from language_translator where status = 'Yes' ORDER BY language ";
    $result = runQuery($query, $conn);
    $index=0;
    if(noError($result)){
        $res = array();
        while($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[$index] = $row["language"].','.$row["languagecode"];
            $index++;
        }
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $res;
	} else {
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}
	
	return $returnArr;
}
//Sport Widget Functions
//Function for the team details of given team id
function team_detail($team_id, $conn){
 $team_id = $team_id;
 $sqlTeam = "SELECT * FROM team_master WHERE team_id = $team_id";
 $sqlTeam = runQuery($sqlTeam, $conn);
 $rsTeam = mysqli_fetch_assoc($sqlTeam["dbResource"]);
 
 return array($rsTeam['team_code'],$rsTeam['team_name'],$rsTeam['team_category'],$rsTeam['team_logo']);
}

//Function for Get the tournament details
function tournament_detail($tournament_id, $conn){
 $tournament_id = $tournament_id;
 $sqlTournamanet = "SELECT * FROM tournament_master WHERE tournament_id = $tournament_id";
 $sqlTournamanet = runQuery($sqlTournamanet, $conn);
 $rsTournament = mysqli_fetch_assoc($sqlTournamanet["dbResource"]);
 
 return array($rsTournament['tournament_name'],$rsTournament['tournament_start_date'],$rsTournament['tournament_end_date'],$rsTournament['tournament_level'],$rsTournament['tournament_match_type']);

}

//Function for the convert the Stored time to user time zone
function ConvertGMTToLocalTimezone($gmttime,$timezoneRequired){
 $gmttime = $gmttime;
 $timezoneRequired = $timezoneRequired;
 $date = new DateTime($gmttime, new DateTimeZone($timezoneRequired));
 $autoVar = '';
 $end = '';
 $begin = '';
 $series_data = $date->format('Y-m-d H:i:sP');

 $explodeTime = explode ('+',$series_data);
 if (!empty($explodeTime[1])) {
     $startDateTime = explode ('+',$series_data);
  $autoVar = '+';
  $end = $startDateTime[1];
 }else{
  $startDateTime = explode ('-',$series_data);
  if(count($startDateTime) > 0){
      $end = array_pop($startDateTime); // removes the last element, and returns it

      if(count($startDateTime) > 0){
   $begin = implode('-', $startDateTime); // glue the remaining pieces back together
      }
  }
  $autoVar = '-';
 }
 $time = explode (':',$end);
 $totalMin = ($time[0] * 60)+$time[1];
 $increase = $autoVar.$totalMin.'min';
 $startDate =  date('Y-m-d H:i:s', strtotime($gmttime." $increase"));
 
 return $startDate;
}

//function to get the IP of client
function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
		$ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
		$ip=get_client_ip();
	}
	
	return $ip;
}

//To get Current dateTime of given/selected city
function getdateTimeCity($city, $conn){
	$returnArr = array();	
	$city = $city;
	$timezoneCity = "SELECT offset from cities where name = '".$city."'";
	$resultTime = runQuery($timezoneCity, $conn);

	if(noError($resultTime)){
		$arr = array();
		while($row = mysqli_fetch_assoc($resultTime["dbResource"]))
		{
			$arr[] = $row;
		}
			
		$cityTz = $arr[0]["offset"];
		if(strpos($cityTz,'(') !== false){
			$datanew = explode("(",$cityTz);
			$offset = $datanew[0];
			$offset = trim($offset);
		}else{
			$offset = trim($cityTz);
		}
		if(strpos($offset,'UTC') !== false){
			$offset = str_replace("UTC","",$offset);
		}
		// Calculate seconds from offset
		list($hours, $minutes) = explode(':', $offset);
		$seconds = $hours * 60 * 60 + $minutes * 60;
		
		// Get timezone name from seconds
		$tz = timezone_name_from_abbr('', $seconds, 1);
		if($tz === false){
			$tz = timezone_name_from_abbr('', $seconds, 0);
		} 
		date_default_timezone_set($tz);
		$newdate_city = date('r');
		if(strpos($offset,'+') !== false){
			$newdate_city = explode("+",$newdate_city);
		}
		if(strpos($offset,'-') !== false){
			$newdate_city = explode("-",$newdate_city);
		}
		$dateCity = $newdate_city[0];
		$newdate_city = date("l, Y-m-d h:i:s A", strtotime($dateCity));
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $newdate_city;
			
	}else{
		$returnArr["errCode"][5] = 5;
		$returnArr["errMsg"] = $resultTime["errMsg"];
	}	

	return $returnArr;
}

function btcTousd($conn){

	$currcode 	  = "USD";
	$fromCurrCode = "BTC";
	$amount       = '1';
	$returnArr    = array();
	if(isset($currcode) && !empty($currcode)){
			//get currency convert Amount
			$newamt = convertCurrency($fromCurrCode, $currcode, $amount, $conn);
			// $resamt = explode(" ",$newamt["errMsg"]);
			// $resamt = trim($resamt[0],'"');
			// $resamt = round($resamt,"3");
			// $resultamt["errCode"] = -1;
			// $resultamt["amt"] = $resamt;
			if(noError($newamt)){
				$newamt = $newamt["errMsg"]; 
				$returnArr["errCode"] = -1;
				$returnArr["errMsg"] = $newamt;
			}else{
				$returnArr["errCode"][2] = 2;
				$returnArr["errMsg"] = "Error: Fetching current USD rate";
			}
	}else{
		$returnArr["errCode"][3] = 3;
		$returnArr["errMsg"]  = "Currency code ". $currcode." not found";
	}

	return $returnArr;
}

function getuserAppId($email){
	global $walletPublicKey;
    global $walletPrivateKey;
    global $walletURL;
	$ch        = curl_init();
    $retArray  = array();
    curl_setopt($ch, CURLOPT_URL, $walletURL . "getAppId");
    //curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    //create signature
    $apiText = "email=".$email."&publicKey=".$walletPublicKey;
    $signature = hash_hmac('sha512', $apiText, $walletPrivateKey);

    //curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "email=".urlencode($email)."&publicKey=".urlencode($walletPublicKey)."&signature=".$signature);

    $result     = curl_exec($ch);

	// close cURL resource, and free up system resources
	curl_close($ch);
    $resultJson = json_decode($result, true);
	 //get curl response
    $errCode    = $resultJson['errCode'];
    $errMsg     = $resultJson['errMsg'];

    if ($errCode == -1) {
        //save user information in session variable
        $_SESSION["user_information"] = $errMsg;
        //login success redirect to index.php
        $redirectURL             = $successReturnURL;
        $retArray['errCode'] = -1;
        $retArray['errMsg']      = $errMsg;
    } elseif ($errCode == 3) {
        $redirectURL            = $returnURL . "?errMsg=" . $errMsg . "&email=" . $email;
        $retArray['errCode'][2] = $errCode;
        $retArray['errMsg']     = $errMsg;
    } else {
        //error message
        $redirectURL            = $returnURL . "?errMsg=" . $errMsg;
        $retArray['errCode'][2] = $errCode;
        $retArray['errMsg']     = $errMsg;
    }
    return $retArray;
}







?>