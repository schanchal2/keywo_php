<?php
/*
* --------------------------------------------------------------------------------------------
*       Search Model
*---------------------------------------------------------------------------------------------
*   This search model is used to search apps and widgets related data
*/


/*-----------------------------------------------------------------------------------------------------------------
*       Function getPopularKeyword
*------------------------------------------------------------------------------------------------------------------
 *Function	:	getPopularKeyword()
 *Purpose	: 	To get keyword which has highest count from SearchKeywordsJsonFiles directory from server.
 *Arguments	: 	(string)$newServerRootURL, (int)$appId, (string)$appName.
 *Returns	:	Returns keyword which has highest count.
 */
function getPopularKeyword($newServerRootURL, $appId, $appName){

    $returnArr = array();
    $extraArgs = array();

   $appJson = $newServerRootURL."app_json";

    if(!is_dir($appJson)){
        mkdir($appJson, 0777);
    }
    if(is_dir($appJson)){
        $fileName = $appId."_".strtolower($appName)."_KeywordSearched.json";
        if(!file_exists($appJson."/".$fileName)){
            $fp = fopen($appJson."/".$fileName, "a+");
        }
        if(file_exists($appJson."/".$fileName)){
            $getJsonContent = file_get_contents($appJson."/".$fileName);
            $getJsonContent = json_decode($getJsonContent, true);
            arsort($getJsonContent);

            $getJsonContent = array_slice($getJsonContent, 0, 10, true);
            $keyword = array_keys($getJsonContent);
            $extraArgs["keywords"]=$keyword;

            $returnArr = setErrorStack($returnArr, -1 , null, $extraArgs);
        }else{
            $errMsg = "Error: File ".$fileName." Not Found";
            $returnArr = setErrorStack($returnArr, S02 , $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: ".$appJson." Not Found";
        $returnArr = setErrorStack($returnArr, S03 , $errMsg, $extraArgs);
    }
   return $returnArr;
}

/*
 *Function	:	getRecentSearchQuery()
 *Purpose	:	To retrieve recently search query from corresponding week table from user history.
 *Arguments	: 	(integer)$appId, (string)$conn.
 *Returns	:	Returns recently searched query.
*/

function getRecentSearchQuery($appId, $conn){

    $tableName = "";
    $returnArr = array();
    $extraArgs = array();
    $dt = explode(":",date("Y:m:j",time()));

    if($dt[2] > 0 && $dt[2] <= 7)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_1wk";
    }
    elseif($dt[2] > 7 && $dt[2] <= 14)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_2wk";
    }
    elseif($dt[2] > 14 && $dt[2] <= 21)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_3wk";
    }
    elseif($dt[2] > 21 && $dt[2] <= 31)
    {
        $tableName = "user_search_history_".$dt[0]."_".$dt[1]."_4wk";
    }

    $query = "select keyword from ".$tableName." where app_id=".$appId." order by timestamp desc limit 10";

    $result = runQuery($query, $conn);
    if(noError($result)){
        while($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
            $returnArr = setErrorStack($returnArr, -1 , $res, $extraArgs);
    }else{
        $returnArr = setErrorStack($returnArr, 3, null, $extraArgs);
    }


    return $returnArr;
}

/*
 *Function	:	getLatestJson()
 *Purpose	:	To retrieve recently created json file in corresponding app json directory.
				e.g. AmazonJsonFiles, YoutubeJsonFiles, DailymotionJsonFiles.
 *Arguments	: 	(string)$dir
 *Returns	:	Returns latest json file name i.e. recently created json file in directory.
*/

function getLatestJson($path)
{
    $returnArr = array();
    $extraArgs = array();

//    $dir = opendir($path);
//    $list = array();
//    while($file = readdir($dir))
//    {
//        if($file != '..' && $file != '.')
//        {
//            $mtime = filemtime($path . $file) . ',' . $file;
//            $list[$mtime] = $file;
//        }
//    }
//    closedir($dir);
//    krsort($list);
//
//    foreach($list as $key => $value)
//    {
//        $latestJson = $list[$key];
//        //return $list[$key];
//    }


    //$path = "/path/to/my/dir";

    $latest_ctime = 0;
    $latest_filename = '';

    $d = dir($path);
    while (false !== ($entry = $d->read())) {
        $filepath = "{$path}/{$entry}";
        // could do also other checks than just checking whether the entry is a file
        if (is_file($filepath) && filectime($filepath) > $latest_ctime) {
            $latest_ctime = filectime($filepath);
            $latest_filename = $entry;
        }
    }


    $returnArr = setErrorStack($returnArr, -1, $latest_filename, $extraArgs);
    //printArr($returnArr);
    return $returnArr;

}

/*
 *Function	:	getBitcoinLatestNews()
 *Purpose	:	To retrieve latest 5 bitcoin news based on publish date from search trade database.
 *Arguments	: 	(string)$conn.
 *Returns	:	Returns 5 updated bitcoin latest news.
 */

function getBitcoinLatestNews($conn){
    $returnArr = array();
    $extraArgs = array();

    $query = "SELECT i.item_id ,i.title, i.description,i.link,i.publish_date, i.source,e.url FROM items i LEFT JOIN channels c ON c.channel_id = i.channel_id LEFT JOIN images e ON i.item_id = e.item_id WHERE c.category = 'bitcoin' order by i.publish_date desc limit 5";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
            $returnArr = setErrorStack($returnArr, -1, $res, $extraArgs);
    } else {
        $returnArr = setErrorStack($returnArr, 3, null, $extraArgs);
    }

    return $returnArr;

}

/*
 *Function	:	getBitcoinPopularNews()
 *Purpose	:	To retrieve popular news from search trade database based on most searched keyword that is stored in json file.
				Retrieve top 10 keyword from 13_bitcoin_news_KeywordSearched.json which has maximum count located in SearchkeywordJsonFile directory.
 *Arguments	: 	(string)$keyword, (integer)$limit, (string)$conn.
 *Returns	:	Returns popular 5 results of bitcoin news.
 */

function getBitcoinPopularNews($keyword, $limit, $conn){

    $returnArr = array();
    $extraArgs = array();
    $res = array();


    foreach($keyword as $key => $arrValue){
        // remove special character from keywords.
        $arrValue = preg_replace('/[^A-Za-z0-9\-]/', '', $arrValue);

        $query = "SELECT i.item_id ,i.title,i.description,i.link,i.publish_date, i.source,e.url FROM items i LEFT JOIN channels c ON c.channel_id = i.channel_id LEFT JOIN images e ON i.item_id = e.item_id WHERE i.title like  '%".$arrValue."%' OR c.category = 'bitcoin' order by i.publish_date desc limit ".$limit."";

        $result = runQuery($query, $conn);
        if(noError($result)){

            while($row = mysqli_fetch_assoc($result["dbResource"])) {
                $res[] = $row;
                if (count($res) > 4) {
                    break;
                }
            }
            $extraArgs["data"] = $res;
            $returnArr = setErrorStack($returnArr, -1, null , $extraArgs);
        }else{
            $returnArr = setErrorStack($returnArr, 3, null, $extraArgs);
        }
        if(count($res) >= 4){
            break;
        }
    }

    return $returnArr;
}

/*
 *Function	:	getAppFooter()
 *Purpose	:	To retrieve app footer names from search trade database
 *Arguments	: 	(string)$conn.
 *Returns	:	Returns app details with status = 1
 */
 function getAppFooter($conn)
 {
     $returnArr = array();
     $extraArgs = array();
     $query = "SELECT *, (`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where app_id in (2,9,8,10,6,1,4,7,13) AND status = 1 order by TotSearches DESC";

     $result = runQuery($query, $conn);
     if (noError($result)) {
         $res = array();
         while ($row = mysqli_fetch_assoc($result["dbResource"]))
             $res[] = $row;
         $returnArr = setErrorStack($returnArr, -1, $res , $extraArgs);
     } else {
         $errMsg = $result["errMsg"];
         $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
     }
     return $returnArr;
 }