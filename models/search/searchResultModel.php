<?php
/*
* --------------------------------------------------------------------------------------------
*       Search Model
*---------------------------------------------------------------------------------------------
*   This search model is used to search apps and widgets related data
*/

/*
 *
 *
 *
 */

 function autoSuggest($conn, $term, $appStatus){
     $returnArr = array();
     $extraArg  = array();

     if(trim($term ) !== ""){
         //fetching suggetions from current week table
         $data = array();
         $date = (int)date("d");
         if ($date <= 7) {
             $week = 1;
         } elseif ($date <= 14) {
             $week = 2;
         } elseif ($date <= 21) {
             $week = 3;
         } else {
             $week = 4;
         }

         $tableName = "user_search_history_".date("Y")."_".date("m")."_".$week."wk";

         $query = "SELECT keyword,count(keyword) AS kcount FROM $tableName WHERE keyword like '" .cleanQueryParameter($conn, $term) . "%' GROUP BY keyword ORDER BY kcount DESC LIMIT 0,6";
         if($appStatus == "TRUE"){
              $query = "SELECT keyword,count(keyword) AS kcount FROM $tableName WHERE keyword like '" .cleanQueryParameter($conn, $term) . "%' GROUP BY keyword ORDER BY kcount DESC LIMIT 0,3";
         }

         $result = runQuery($query, $conn);
         if (noError($result)) {
             $rows_returned = mysqli_num_rows($result["dbResource"]);

             while($result1 = mysqli_fetch_assoc($result["dbResource"])){
                 $suggest = str_replace("&#039;", "'", $result1["keyword"]);
                 array_push($data, html_entity_decode($suggest));
             }
             $extraArg["data"]=$data;
             $returnArr = setErrorStack($returnArr, -1 ,"Success!!" , $extraArg);
         } else {
             $returnArr = setErrorStack($returnArr, S01 ,null , $extraArg);
         }
         return $returnArr;
     }
 }

/*
 *Function	: 	getAppDetails()
 *Purpose	: 	To retrieve all app details from sc_app_details.
 *Arguments	: 	(string)$conn, (integer)$appId.
*/
function getAppDetails($conn, $appId){
    $returnArr = array();
    $extraArg = array();

    $query = "select * from sc_app_details where app_id = ".$appId."";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
            $returnArr = setErrorStack($returnArr, -1 ,$res , $extraArg);
    } else {
            $errMsg = $result['errMsg'];
            $returnArr = setErrorStack($returnArr, 5 ,$errMsg , $extraArg);
    }
    return $returnArr;
}

/*
 *Function	:	getBitcoinNews()
 *Purpose	: 	To retrieve bitcoin related news result from database having category = bitcoin.
 *Arguments	: 	(int)$offset, (string)$searchData, (string)$category, (int)$no_of_rows, (string)$conn.
 *Returns	:	Returns bitcoin result based on offset and no_of_rows parameters.
 */

function getBitcoinNews($offset, $searchData, $category, $no_of_rows, $conn){
    global $blanks;
    $returnArr 	= array();
    $extraArg   = array();

    if((!in_array($offset, $blanks)) && (!in_array($no_of_rows, $blanks))){

        $query = 'SELECT i.item_id ,i.title,i.description,i.link,i.publish_date, i.source,e.url FROM items i LEFT JOIN channels c ON c.channel_id = i.channel_id  LEFT JOIN images e ON i.item_id = e.item_id WHERE ('.$searchData.') and c.category = '."'$category'".' order by i.publish_date desc  LIMIT '.$offset.','.$no_of_rows.'';

    }else{

        $query = 'SELECT i.item_id ,i.title,i.description,i.link,i.publish_date, i.source,e.url FROM items i LEFT JOIN channels c ON c.channel_id = i.channel_id LEFT JOIN images e ON i.item_id = e.item_id WHERE ('.$searchData.') and  c.category = '."'$category'".'  order by i.publish_date desc';
    }


    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
            $returnArr = setErrorStack($returnArr, -1 ,$res , $extraArg);
    } else {
            $errMsg = $result['errMsg'];
            $returnArr = setErrorStack($returnArr, 5 ,$errMsg , $extraArg);
    }

    return $returnArr;
}


/*
*Function  :   getAllFavApps()
*Purpose   :   To get app details from sc_app_details table.
*Arguments :   (string)$conn as search database connection (search97_db), (int)$favSearchApp favorite search app id.
*Returns   :   Returns all app details in array having status = 1.
*              Where 1 states active app on system.
*/
function getAllFavApps($favSearchApp, $conn){
    $extraArg  = array();
    $returnArr = array();
    $errMsg    = '';
    $favSearchApp = empty($favSearchApp) ? "'#'" : $favSearchApp;
    $query = "SELECT *, (`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details  where app_id in ($favSearchApp) and status=1 order by TotSearches DESC";
    $result = runQuery($query, $conn);
    
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
            $extraArg['data'] = $res;
            $returnArr = setErrorStack($returnArr,-1,'User favorite app',$extraArg);
    } else {
         $returnArr = setErrorStack($returnArr,FA01,$errMsg,$extraArg);
    }
    return $returnArr;
}


function createImageUrlJson($resArray, $filename, $dirPath, $appId){

    $i=0;
    $imageUrl = array();
    $retArray = array();
    $extraArg = array();
    $imgFolderPath = '';

    switch ($appId) {
        case 4:
            $imgFolderPath = "{$dirPath}/youtubeImages";
            if(!file_exists("{$imgFolderPath}/{$filename}")){
                $resArray = $resArray['items'];
                foreach($resArray as $resItems) {
                    $imageUrl[] = $resItems['snippet']['thumbnails']['medium']['url'];
                }
            }else{
                $resArray = $resArray['items'];
                foreach($resArray as $resItems) {
                    $imageUrl[] = $resItems['snippet']['thumbnails']['medium']['url'];
                }
            }
            break;
        case 9:
            $imgFolderPath = "{$dirPath}/dailymotionImages";
            if(!file_exists("{$imgFolderPath}/{$filename}")){
                while($i < 5)
                {
                    foreach($resArray[$i]['list'] as $res){
                        $imageUrl[] =  $res['thumbnail_url'];
                    }
                    $i++;
                }
            }else{

                while($i < 5) {
                    foreach($resArray[$i]['list'] as $res){
                        $imageUrl[] =  $res['thumbnail_url'];
                    }
                    $i++;
                }
                file_put_contents("{$imgFolderPath}/{$filename}", json_encode($imageUrl));

            }
            break;
    }

    if(count($imageUrl) > 0) {
        if(!is_dir($imgFolderPath)){
            mkdir($imgFolderPath, 0777, true);
        }

        file_put_contents("{$imgFolderPath}/{$filename}", json_encode($imageUrl));

        $errMsg = json_encode($imageUrl);
        $retArray = setErrorStack($retArray,-1, $errMsg, $extraArg);
    }
    else{
        $errMsg = "No Api Results";
        $retArray = setErrorStack($retArray, S04, $errMsg, $extraArg);
    }


    unset($imageUrl);
    unset($i);
    unset($imgFolderPath);
    return $retArray;
}




/*
  *Function	:	getIPSearchCount()
  *Purpose	:	To retrieve search count of corresponsing IP.
  *Arguments: 	(string)$ip.
  *Returns	:	Returns search count searched on particular IP.
*/

function getIPSearchCount($ip){
    global $docRoot;
    $returnArr = array();
    $extraArgs = array();
    $currentIP = $ip;

    $ipBlockerDir 		=   $docRoot."IPBlocker/";
    $ip 				= 	explode(".", $ip);
    $getFirstByteIpVal 	= 	$ip[0];
    $ipCeilVal 			= 	ceil($getFirstByteIpVal/20);
    $ipInnerDir 		= 	"IP_".$ipCeilVal;
    $ipBlockerDir 		= 	$ipBlockerDir.$ipInnerDir;
    $ipJsonFile 		= 	$ip[0].".json";

    if(is_dir($ipBlockerDir)){
        if(file_exists($ipBlockerDir."/".$ipJsonFile)){
            // read json file
            $readJson = file_get_contents($ipBlockerDir."/".$ipJsonFile);
            $readJson = json_decode($readJson, true);
            foreach($readJson as $key => $value){
                if($key == $currentIP){
                    $searchCount = $value["search_count"];
                }
            }
            $extraArgs["ip_search_count"] = $searchCount;
            $errMsg = "Successfully fetch ip search count";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{

            $errMsg =  $ipJsonFile. " not found in ".$ipBlockerDir." directory.";
            $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
        }
    }else{

        $errMsg = $ipBlockerDir." directory not found";
        $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
    }
    return $returnArr;
}

/*
  * Function : getDefaultAppId()
  * Purpose  : To retrieve DEfault app set by Admin.
  * Arguments:   (string)$conn.
  * Returns  : Returns Array of default app..
*/

function getDefaultAppId($conn){
  $returnArr = array();
    $query = "SELECT a.*,app.* from admin_setting as a  inner join sc_app_details as app ON a.default_app_id = app.app_id order by a.timestamp desc limit 1";
  $result = runQuery($query, $conn);

  if(noError($result)){
    $res = array();
    while ($row = mysqli_fetch_assoc($result["dbResource"]))
      $res[] = $row;

    $returnArr['errCode']=-1;
    $returnArr['errMsg']=$res;
  } else {
    $returnArr['errCode'][5]=5;
    $returnArr['errMsg']=$result['errMsg'];
  }

  return $returnArr;

}


?>