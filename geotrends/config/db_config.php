<?php

class DBConnection
{

    private $db_host = "192.168.2.250";  //Database Running Host IP OR Domain Primary DB

    private $db_host_secondary ="192.168.2.250";  // Database Running Host IP OR Domain Secondary Failure DB

    private $port ="3306";  //Database Running on port

    //Geotrends database credential starts
    private $search_trends_user="dbuser";
    private $search_trends_password="dbuser@123";
    private $search_trends_database="dbtrends";
    //Geotrends database credential ends

    public function getDatabaseCredential($database)
    {
        switch($database)
        {
            case "dbkeywords": //if request for keyword database connection
                $db_array["host"]=$this->db_host;
                $db_array["user"]=$this->keyword_user;
                $db_array["password"]=$this->keyword_password;
                $db_array["database"]=$this->keyword_database;
                $db_array["port"]=$this->port;
                return $db_array;
                break;

            case "dbsearch"://if request for search database connection
                $db_array["host"]=$this->db_host;
                $db_array["user"]=$this->search_user;
                $db_array["password"]=$this->search_password;
                $db_array["database"]=$this->search_database;
                $db_array["port"]=$this->port;
                return $db_array;
                break;

            case "acl": //if request for admin acl database connection
                $db_array["host"]=$this->db_host;
                $db_array["user"]=$this->acl_admin_user;
                $db_array["password"]=$this->acl_admin_password;
                $db_array["database"]=$this->acl_admin_database;
                $db_array["port"]=$this->port;
                return $db_array;
                break;

            case "dbtrends"://if request for geo trends database connection
                $db_array["host"]=$this->db_host;
                $db_array["user"]=$this->search_trends_user;
                $db_array["password"]=$this->search_trends_password;
                $db_array["database"]=$this->search_trends_database;
                $db_array["port"]=$this->port;
                return $db_array;
                break;
            default :
                $error="Database Connection Error";
        }
    }
    public function getDBConnection($credential)
    {
        $host= $credential["host"]; //get credential out of array
        $user=$credential["user"];
        $password=$credential["password"];
        $database=$credential["database"];
        $port=$credential["port"];

        $db_conn =mysqli_connect($host, $user, $password, $database,$port);//pass parameters to mysqli
        if(!$db_conn){

            //On Failure of primary DB
            $host=$this->db_host_secondary;
            $db_conn =mysqli_connect($host, $user, $password, $database,$port);
            if(!$db_conn){
                $returnArr["errCode"] = 1;
                $returnArr["errMsg"] = "Unable to connect $database database".$db_conn->connect_error;
            }else{
                if (!mysqli_select_db($db_conn, $database)) {
                    $returnArr["errCode"] = 1;
                    $returnArr["errMsg"] = "Could not select DB:$database" . mysqli_error();
                } else {
                    $returnArr=$db_conn;
                }
            }

        }else{
            //if connection success return connection details
            if (!mysqli_select_db($db_conn, $database)) {
                $returnArr["errCode"] = 1;
                $returnArr["errMsg"] = "Could not select DB:$database" . mysqli_error();
            } else {
                $returnArr=$db_conn;
            }
        }
        return $returnArr;
    }

}

function createDBConnection($database)
{

    $returnArr = array();
    $extraArg = array();
    $DBConnection =new DBConnection();
    $credential=$DBConnection->getDatabaseCredential($database);
    $conn=$DBConnection->getDBConnection($credential);
    $type=gettype($conn);
    if($type=='array')
    {
        $returnArr = setErrorStack($returnArr,1,null,$extraArg);
    }else
    {
        $extraArg['connection'] = $conn;
        $errMsg = 'Database Connection Success!';
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }
    return $returnArr;
}

function closeDBConnection($conn)
{
    mysqli_close($conn);
}
