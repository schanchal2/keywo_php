<?php
/*
*-----------------------------------------------------------------------------------------------------------
*    Function runQuery
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   runQuery()
*   Purpose       :   To execute the sql queries.
*   Arguments     :   (string)$query, (string)$conn
*   Returns       :   Array that contains errCode and errMsg having success or failure message.
*/

function runQuery($query, $conn){
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_query($conn, $query);
    if(!$result){
        $extraArg['query']   = $query;
        $returnArr = setErrorStack($returnArr,3,null,$extraArg);
    }else{
        $extraArg['dbResource'] = $result;
        $errMsg = 'Query Successful';
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function noError
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   noError()
*   Purpose       :   This function valdiate the condition, if array contains errCode = -1 then true
*                      else false.
*   Arguments     :   (array)$resArray
*   Returns       :   boolean (true or false)
*/

function noError($resArray) {
    $noError = false;
    if($resArray["errCode"] == -1){
        $noError = true;
    }
    return $noError;
}

function printArr($arr) {
  	print("<pre>");
  	   print_r($arr);
  	print("</pre>");
}

function cleanQueryParameter($conn,$string) {
	if($string!="") {
        $string = trim($string);
        $string = addslashes($string);
        $string = mysqli_real_escape_string($conn, $string);
    }

    return $string;
}

/*  Function 	:	getKeywordOwnershipTableName().
	purpose		: 	To returns alphabetical ordered table name based on keywords first letter.
	Arguments	: 	(string)$keyword.
*/
function getKeywordOwnershipTableName($keyword) {

    //$keyword = strip_tags($keyword);
    $keyword = strtolower($keyword);

    //Get first char
    $firstchar     = substr($keyword, 0, 1);
    $firstchar_low = strtolower($firstchar);
    if (strcasecmp($firstchar_low, $firstchar) == 0) {
        $firstchar = $firstchar_low;
    }

    //Get correct table-name
    // $tableName = "";
    // if (ctype_alpha($firstchar)) {
    //     $tableName = "interest_" . $firstchar;
    // } elseif (ctype_digit($firstchar)) {
    //     $tableName = "interest_num";
    // } elseif (ctype_punct($firstchar)) {
    //     $tableName = "interest__num";
    // } elseif (ctype_space($firstchar)) {
    //     $str = preg_replace('/\s+/', '', $firstchar);
    // }


     //Get correct table-name
    $tableName = "";
     if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
        $tableName = "interest_other";
    }else if (ctype_alpha($firstchar)) {
        $tableName = "interest_" . $firstchar;
    } elseif (ctype_digit($firstchar)) {
        $tableName = "interest_num";
    } elseif (ctype_punct($firstchar)) {
        $tableName = "interest_num";
    } elseif (ctype_space($firstchar)) {
        $str = preg_replace('/\s+/', '', $firstchar);
    }else{
		$tableName = "interest_num";
	}


    return $tableName;
}

/*  Function 	:	kwd_count().
	purpose		: 	To returns keyword count searched on searchtrade.
	Arguments	: 	(string)$conn, (string)$keyword.
*/

function kwd_count($conn,$keyword){

	$tableName = getKeywordOwnershipTableName($keyword);

	//$query ="select distinct keyword,count(distinct keyword) AS NumberofTimes from interest_".$firstchar." where keyword='".$keyword."'";
	$query ="select count(keyword) AS NumberofTimes from ".$tableName." where keyword='".$keyword."'";
	$result= runQuery($query,$conn);
	if(noError($result)){
		$row= mysqli_fetch_array($result["dbResource"]);
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $row['NumberofTimes'];
	} else {
		$returnArr["errCode"] = 3;
		$returnArr["errMsg"] = "Could not fetch kwd count: ".mysqli_error($conn);
	}

	return $returnArr;
}

/*  Function 	:	addToTop1000().
	purpose		: 	To insert keyword count into top1000 columns.
	Arguments	: 	(string)$conn, (string)$keyword, (integer)$cnt.
*/

function addToTop1000($conn, $keyword, $cnt){
    // adds the keyword and its count to top1000 table and sorts the records
    $query = "insert into top1000 values('";
    $query .= $keyword;
    $query .= "'," . $cnt . ")ON DUPLICATE KEY UPDATE NumberOfTimes=";
    $query .= $cnt;

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $query_slc = "SELECT count(*) AS count FROM `top1000` order by NumberOfTimes DESC";
        $query_slc = runQuery($query_slc, $conn);

        if(noError($query_slc)) {
              $res = array();
			         while ($row = mysqli_fetch_assoc($query_slc["dbResource"]))
                  $res = $row;

                  $returnArr["errCode"] = -1;
			               $row_cnt = $res['count'];

                     if ($row_cnt >= 1001) {

                        $query_dlt  = "DELETE FROM top1000 ORDER BY NumberOfTimes ASC LIMIT 1;";
                        $result_dlt = runQuery($query_dlt, $conn);
                        if (noError($result_dlt)) {
                            $returnArr["errCode"] = -1;
                            $returnArr["errMsg"]  = 'sucess update top1000 table';
                        } else {
                            $returnArr["errCode"] = 8;
                            $returnArr["errMsg"] = " updation failed" . mysqli_error($conn);
                        }
                    }
        } else {
            $returnArr["errCode"] = 5;
            $returnArr["errMsg"] = $query_slc["errMsg"];
        }
    } else {
        $returnArr["errCode"] = 6;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function addToSearchInterestTable($conn,$keyword,$city,$country,$latitude,$longitude,$areaCode,$region,$requestorIP,$appId,$device_type){

    $returnArr = array();

		$query = "insert into search_interest (`keyword`,`city`,`country`,`latitude`,`longitude`,`areaCode`,`region`,`time`,`ip`,`app_id`,`device_type`) values( ";
    $query .= " '" . $keyword . "'";
    $query .= ", '" . $city . "'";
    $query .= ", '" . $country . "'";
    $query .= ", '" . $latitude. "'";
    $query .= ", '" . $longitude . "'";
    $query .= ", '" . $areaCode . "'";
    $query .= ", '" . $region . "'";
    $query .= ", NOW() ";
    $query .= ", '" . $requestorIP . "'";
    $query .= ", '" . $appId . "'";
    $query .= ", '" . $device_type . "')";

    $result = runQuery($query, $conn);
		if(noError($result)){
		    $returnArr["errCode"] = -1;
		    $returnArr["errMsg"] = "Record Insert successfully";
		} else {
		    $returnArr["errCode"] = 5;
		    $returnArr["errMsg"] = $result["errMsg"];
		}

	return $returnArr;
}

/*  Function 	:	addToDailyCount().
	purpose		: 	To insert keywords count.
	Arguments	: 	(string)$conn, (string)$keyword.
*/

function addToDailyCount($conn,$keyword){

	$returnArr = array();

	$query="insert into keyword_count(keyword, count) values('".$keyword."',1) on duplicate key update count = count + 1 ";
	$result = runQuery($query, $conn);

	if(noError($result)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Record Insert successfully";
	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"] = $result["errMsg"];
	}

	return $returnArr;
}

/*  Function 	:	updatecountryDataJson().
	purpose		: 	To update countrywise searched keywords count into country table.
	Arguments	: 	(string)$conn, (string)$country, (string)$keyword, (integer)$cnt, (string)$tableName, (integer)$appId.
*/

function updatecountryDataJson($conn, $country, $keyword, $cnt, $tableName, $appId){

	$kwd_count=$cnt;
	$count=1;

	// fetch unique IP count country wise
	$query="select count(distinct ip) as uni_ip from ".$tableName." where country='".$country."' and keyword='".$keyword."'";

	$result_kwd_count_countrywise=runQuery($query,$conn);
	$result_count=mysqli_fetch_assoc($result_kwd_count_countrywise["dbResource"]);
	$result_count=$result_count['uni_ip'];

	// fetch count of city in selected country
	$query="select count(distinct city) as cities from ".$tableName." where country='".$country."' and keyword='".$keyword."'";

	$result_city_count=runQuery($query,$conn);
	$city_count=mysqli_fetch_assoc($result_city_count["dbResource"]);
	$city_cnt=$city_count['cities'];

	//fetch search count of particulor country
	$query="select SearchCount from kwd_count_countrywise where countryName='".$country."'";

	$result_sel_country=runQuery($query,$conn);
	$row = mysqli_fetch_assoc($result_sel_country["dbResource"]);

	//if new country then add dummy json as well as searched word into the json
	if(empty($row['SearchCount'])){

		$data1[]=array('kwd'=>$keyword,'kwd_count'=>$kwd_count,'unique_ip'=>$result_count,'city_cnt'=>$city_cnt, 'appId'=> $appId);
		//created json is then sorted using user defined sorting function sortByOrder1 at line:94
		usort($data1, 'sortByOrder1');
		//to maintain the length of json last element is popped
		//array_pop($data);
		$countryjson=json_encode($data1);
	}else{

		// if country already exists then get its top100 json
		$query_sel="select top1000 from kwd_count_countrywise where countryName='".$country."'";
		$result_sel= runQuery($query_sel, $conn);

		while($row = mysqli_fetch_assoc($result_sel["dbResource"])){
			//convert json into an array
			$top1000_kwd_countrywise= json_decode($row['top1000'],true);
		}
		$found=false;
		// check if the searched keyword already exists in array
		foreach($top1000_kwd_countrywise as $key=>$val2){
			// if present in array then update its corresponding data
			if ($val2['kwd'] == $keyword){
				$top1000_kwd_countrywise[$key]['kwd_count'] = $kwd_count;
				$top1000_kwd_countrywise[$key]['unique_ip'] = $result_count;
				$top1000_kwd_countrywise[$key]['city_cnt'] = $city_cnt;
				$top1000_kwd_countrywise[$key]['appId'] = $appId;
				// flag variable is set to true
				$found=true;
			}
		}
		// if keyword is new then add it the array
		if(!$found){
			$top1000_kwd_countrywise[]=array('kwd'=>$keyword,'kwd_count'=>$kwd_count,'unique_ip'=>$result_count,'city_cnt'=>$city_cnt, 'appId'=>$appId);
		}
		//sort array
		usort($top1000_kwd_countrywise, 'sortByOrder1');
		$countryjson = array_slice($top1000_kwd_countrywise, 0, 1000);
		//array_pop($xyz1);
		//create json of above array n update in db
		$countryjson = json_encode(($countryjson));
	}

	$query_update="insert into kwd_count_countrywise values('".$country."',".$count.",'".$countryjson."') on duplicate key update  SearchCount=SearchCount+1 ,top1000='".$countryjson."'";

	$result_upd= runQuery($query_update, $conn);
	//error reporting
	if(noError($result_upd)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Record update successfully";
	} else {
		$returnArr["errCode"] = 6;
		$returnArr["errMsg"]="Could not update country data: ".mysqli_error($conn);
	}

	return $returnArr;
}

/*  Function 	:	updatecityDataJson().
	purpose		: 	To update citywise searched keywords count into city table.
	Arguments	: 	(string)$conn, (string)$country, (string)$keyword, (integer)$cnt, (string)$tableName, (integer)$appId.
*/

function updatecityDataJson($conn, $city, $keyword, $cnt, $tableName, $appId){

	$count=2;
	$keyword=$keyword;
	$kwd_count=$cnt;

	//get the data required for the record i.e ip count keyword name and its count
	$sel_ip="select count(distinct ip) as uni_ip from ".$tableName." where city='".$city."' and keyword='".$keyword."'";
	$result_sel_ip=runQuery($sel_ip,$conn);

	$row_ip=mysqli_fetch_assoc($result_sel_ip["dbResource"]);
	$k_ip=$row_ip['uni_ip'];

	$sel_city="select SearchCount from kwd_count_citywise where cityName='".$city."'";
	$result_sel_city=runQuery($sel_city,$conn);
	$row1 = mysqli_fetch_assoc($result_sel_city["dbResource"]);
	//check the request is from existing city or new one
	if(empty($row1['SearchCount'])){
		// if its new city create dummy array and add city data i.e SearchCount=1 and json (keyword ,its count,ip count of that word) to it
		$data[]=array('kwd'=>$keyword,'kwd_count'=>$kwd_count,'unique_ip'=>$k_ip, 'appId'=>$appId);
		usort($data, 'sortByOrder');
		//array_pop($data);
		//create json of that array
		$cityjson=json_encode($data);
	} else {
		// if city already exists then update its data
		//get the json top100 from that city
		$query_sel="select top1000 from kwd_count_citywise where cityName='".$city."'";
		$result_sel= runQuery($query_sel, $conn);

		while($row = mysqli_fetch_assoc($result_sel["dbResource"])){
			//converting json into an array
			$xyz= json_decode($row['top1000'],true);
		}
		$found1=false;
		//search if keyword already exists in array or not
		foreach($xyz as $key=>$val){
			if($val['kwd'] == $keyword){
				//if found update/replace its data with new one
				$xyz[$key]['kwd_count'] = $kwd_count;
				$xyz[$key]['unique_ip'] = $k_ip;
				$xyz[$key]['appId'] = $appId;
				$found1=true;
			}
		}
		//add keyword & its cnt at d end of n array n sort in desc order
		if(!$found1){
			$xyz[]=Array('kwd'=>$keyword,'kwd_count'=>$kwd_count,'unique_ip'=>$k_ip, 'appId'=> $appId);
		}
		//sort array using user defined function named sortByOrder at line no:207
		usort($xyz, 'sortByOrder');
		$cityjson = array_slice($xyz, 0, 1000);
		//array_pop($xyz);
		//create json of above array n update in db
		$cityjson = json_encode($cityjson);
	}
	$query_update="insert into kwd_count_citywise values('".$city."',".$count.",'".$cityjson."') on duplicate key update  SearchCount=SearchCount+1 ,top1000='".$cityjson."'";

	$result_upd= runQuery($query_update, $conn);

	if(noError($result_upd)){
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Successfully inserted keyword count into city table";
	} else {
		$returnArr["errCode"] = 6;
		$returnArr["errMsg"]="Could not update city data: ".mysqli_error($conn);
	}

	return $returnArr;
}

/*  Function 	:	sortByOrder1().
	purpose		: 	sorts array in descending order based on kwd_count
	Arguments	: 	(integer)$c, (integer)$d.
*/

function sortByOrder1($c, $d){
    return $d['kwd_count']-$c['kwd_count'];
}

/*  Function 	:	sortByOrder().
	purpose		: 	sorts array in descending order based on kwd_count
	Arguments	: 	(integer)$a, (integer)$b.
*/
function sortByOrder($a, $b){
    return $b['kwd_count']-$a['kwd_count'];
}

?>
