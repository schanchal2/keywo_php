<?php

  // Include helper files
  require_once('../config/db_config.php');
  require_once('../helper/errorMap.php');
  require_once('utilities.php');

  $returnArr = array();
  $extraArgs = array();
  //2. Accept Request
  $keywords    = $_POST['keyword'];
  $appId       = $_POST['appId'];
  $requestorIP = $_POST['requestorIP'];

  $device_type = "web";

  $blanks = array("", " ", '', ' ');

  $geoDbConn = createDBConnection("dbtrends");


  if(noError($geoDbConn)){
      $geoDbConn = $geoDbConn["connection"];

      $errMsg = 'Successfully connect geo database';
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

        //3. Validate request
      if (is_null($keywords) || in_array($keywords, $blanks)) {
          $returnArr["errCode"] = 2;
          $returnArr["errMsg"]  = "Please Enter Some Keyword";
          print(json_encode($returnArr));
          exit;

      }else{
          //4.2 Getting Location of user via IP through API
          $data      = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=" . $requestorIP));
          $country   = strtolower($data['geoplugin_countryName']);
          $city      = strtolower($data['geoplugin_city']);
          $latitude  = $data['geoplugin_latitude'];
          $longitude = $data['geoplugin_longitude'];
          $areaCode  = $data['geoplugin_areaCode'];
          $region    = strtolower($data['geoplugin_region']);

          $keywords = preg_replace("/\s\s([\s]+)?/", " ", $keywords);
          // convert string into array
          $keywords = explode(' ', $keywords);

          //l4.4 loop through keywords array to insert into correct table and fetch counts
          $errFlag   = 0;
          $errMsg    = "";
          $kwdCounts = array();

          foreach($keywords as $keyword){

            $keyword = utf8_encode($keyword);
            
            //get keyword table name
            $tableName = getKeywordOwnershipTableName($keyword);

              //4.4.4 Prepare Query

              $query = "insert into " . $tableName . " (`keyword`,`city`,`country`,`latitude`,`longitude`,`areaCode`,`region`,`time`,`ip`) values( ";
              $query .= " '" . $keyword . "'";
              $query .= ", '" . $city . "'";
              $query .= ", '" .  $country . "'";
              $query .= ", '" .  $latitude . "'";
              $query .= ", '" .  $longitude . "'";
              $query .= ", '" . $areaCode . "'";
              $query .= ", '" .  $region . "'";
              $query .= ", NOW() ";
              $query .= ", '" .  $requestorIP . "')";

             $execInsertQuery = runQuery($query, $geoDbConn);
             if(noError($execInsertQuery)){

                 $errMsg = 'Successfully insert keyword records of " '.$keyword.'" keyword';
                 $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                  $keywordCount = kwd_count($geoDbConn, $keyword);
                  if(noError($keywordCount)){
                        $keywordCount = $keywordCount["errMsg"];

                        $errMsg = 'Successfully add keyword count records';
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                        // add keyword to top 1000
                        $addToTop1000 = addToTop1000($geoDbConn, $keyword, $keywordCount);
                        if(noError($addToTop1000)){
                            $errMsg = 'Successfully add keyword into top 1000 records';
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                            // add keyord details into search interast table
                            $addToSearchInterestTable = addToSearchInterestTable($geoDbConn, $keyword, $city, $country, $latitude, $longitude, $areaCode, $region, $requestorIP, $appId, $device_type);

                            if(noError($addToSearchInterestTable)){
                                  $errMsg = 'Successfully add keyword search interest table';
                                  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                  // add to daily count
                                  $addToDailyCount = addToDailyCount($geoDbConn, $keyword);

                                  if(noError($addToDailyCount)){

                                        $errMsg = 'Successfully add keyword into daily count';
                                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                        // update records into country table
                                        $updatecountryDataJson = updatecountryDataJson($geoDbConn, $country, $keyword, $keywordCount, $tableName, $appId);

                                        if(noError($updatecountryDataJson)){

                                              $errMsg = 'Successfully add keyword details of particular country';
                                              $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                              // udpate records into city table
                                              $updatecityDataJson = updatecityDataJson($geoDbConn, $city, $keyword, $keywordCount, $tableName, $appId);
                                              if(noError($updatecityDataJson)){
                                                  $extraArgs["count"] = $keywordCount;
                                                  $errMsg = 'Successfully add keyword details of particular city';
                                                  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                                              }else{
                                                  $errMsg = 'Error: '.$updatecityDataJson["errMsg"];
                                                  $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);
                                              }
                                        }else{
                                              $errMsg = 'Error: '.$updatecountryDataJson["errMsg"];
                                              $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                                        }
                                  }else{
                                        $errMsg = 'Error: Unable to add daily count records';
                                        $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                                  }
                            } else{
                                  $errMsg = 'Error: Unable to add search interact records';
                                  $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                            }
                        }else{
                            $errMsg = 'Error: '.$addToTop1000["errMsg"];
                            $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                        }
                  }else{
                      $errMsg = 'Error: '.$keywordCount["errMsg"];
                      $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                  }
             }else{
                  $errMsg = 'Error: Executing insert query';
                  $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
             }
          }
      }
  }else{
      $errMsg = 'Error: Unable to create geo database connection';
      $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
  }

echo json_encode($returnArr);

?>
