<?php

$rootUrl = "http://localhost/keywo_php/"; //Static

$docRoot = $_SERVER['DOCUMENT_ROOT']."/keywo_php/";
if($_SERVER['DOCUMENT_ROOT']==""){$docRoot= $_SERVER["PWD"]."/";}//CMD

$rootUrlImages = "{$rootUrl}images/";
$rootUrlJs     = "{$rootUrl}js/";
$rootUrlCss    = "{$rootUrl}css/";

/*CDN social images url*/
$cdnSocialUrl = "https://alfa-cdn-social-2017.s3.ap-south-1.amazonaws.com/";

/*memcache server url*/
$memcacheServerIP   = null;//'192.168.2.169';
$memcacheServerPort = 11211;
// $memcacheServerIP   = '192.168.2.169';
// $memcacheServerIP   = '192.168.2.180';
// $memcacheServerPort = 11211;
/*characters count for social*/
$validateCharCountOnSocial = "20";

/*validation of Comment box for social*/
$validateCharCountOnCommentBox = "600";
$validateWordCountOnkeyword    = "10";

/*benchmarking according to post Id's to create json file for social*/
$likePostIdBenchmarking = 500;

// Like CDP log path  used $jsonPath  @getLikePostStatus socialModel.php
$jsonPath['social'] = $docRoot.'/json_directory/social/';

//search root url
$rootUrlSearch = "{$rootUrl}views/search/";
$blanks        = array('', ' ', "", " ");

$mode      = "development"; //Static

$errorMode = "production"; // For error reporting log

/* for wallet */
$walletPrivateKey = "24eb6b033f8d23556a79880e05ed99289b71cc35b66f2567e0078da07ea1397051b901b5b93960cd4d24d6e23268ca0472f7fb41c7af1dd319f1f6397ecee088";
$walletPublicKey  = "8b428ac0a0ae1be15a6e75d69fbc15a9129909ed261a1aeb4d1e087592659daa";

/* Captcha credential for localhost*/
$captchaPublickey  = "6Lcr8REUAAAAACmneRvqdnyXRysd90FdbsdafMCV";
$captchaPrivatekey = "6Lcr8REUAAAAALf_Gu9FKe8mYQzLWzlNrXFbQ3Un";
 // $captchaPrivatekey = "6Ld5FCEUAAAAABprFDPu9scPepUzyCr3aCkS2dLm";//dinesh

//External API Keys
//for apilayer currency API - searchtradedeveloper account
$currencyAPI       = "f6b240dde39f7a167822f95abfc34343";
$currency_API_link = "https://www.apilayer.net/api/live?";
$validateCharCountOnReportCommentBox = "100";

$walletURLIP = "http://192.168.2.134:5011/"; //Static
// $walletURLIP = "http://192.168.2.126:5600/"; //Static
 //$walletURLIP = "http://192.168.2.122:5000/"; //Static
//$walletURLIP = "http://192.168.2.169:5000/"; //Static

$walletURLIPnotification = "http://192.168.2.134:4101/"; //Static
// $walletURLIPnotification = "http://192.168.2.126:4600/"; //Static
//$walletURLIPnotification = "http://192.168.2.122:4000/"; //Static

$walletURL       = "{$walletURLIP}api/v3/";
$NotificationURL = $walletURLIPnotification."api/notify/";
$socialUrl       = "{$walletURLIPnotification}social/v1/";
/* end for wallet */

/* for emails */
$paymentEmail     = "payment@keywo.com";
$noReplyEmail     = "noreply@keywo.com";
$adminEmail       = "amitmishra@bitstreet.in";
$ownershipZipMail = "amitmishra@bitstreet.in";
/* end for emails */

$keywoLaunchDate = "25 may 2017";

/* special keywo users email id and user id */
$communityPoolUser    = "communitypool@keywo.com";
$keywoUser            = "keywo@keywo.com";
$appDeveloperUser     = "appdeveloper@keywo.com";
$unownedKwdOwnerUser  = "unownedkwdower@keywo.com";
$maintenanceUser      = "maintenance@keywo.com";
$contactEmailId       = "nikhilkakde@bitstreet.in";
$autoBlockerEmail     = "admin@keywo.com";
$searchtradeDeveloper = "searchtradedeveloper@gmail.com";

$appDeveloperUserId    = "34cf467ee95afa20e4413128aea87dff820100ed";
$communityPoolUserId   = "f0b32a34a0c26c6a79f6b29e9c65b02e3e2fd03e";
$unOwnedKwdOwnerUserId = "05454243fe9e5b82d8545f8b18c28c954cbd60d7";
$keywoUserId           = "cf3318e40085cedec47697729da6e4dc42d770d6";
$maintenanceUserId     = "31c354ed235f44c7624f0a575aca43cbe301b5c3";
/* end */

/* user details required fields */
$userRequiredFields = "first_name,last_name";
/* end */

/*max display price*/
$maxPriceDisplay     = 1000;
$maxPriceDispCompare = 999.99999999;
/*end*/

//Youtube Developer KEY - searchtradedeveloper account
$DEVELOPER_KEY = 'AIzaSyDOkg-u9jnhP-WnzX5WPJyV1sc5QQrtuyc';

/***************Log Path Array *****************/
$logPath["userManagement"]   = $docRoot."logs/UserMgmt/";
$logPath["IPBlocker"]        = $docRoot."logs/IPBlocker/";
$logPath["coinDistribution"] = $docRoot."logs/coinDistribution/";
$logPath["cron"]             = $docRoot."logs/cron_logs/";
$logPath["keywordPurchase"]  = $docRoot."logs/keyword/purchase/";
$logPath["keywordBid"]       = $docRoot."logs/keyword/bid/";
$logPath["social"]           = $docRoot."logs/social/";
$logPath["wallet"]           = $docRoot."logs/wallet/";
$logPath["cart"]             = $docRoot."logs/keyword/cart/";
$logPath["security"]         = $docRoot."logs/security/";
/***************************************/
$templatesPath["templates"]=$docRoot."assets/email_templates/";

/***********kyc doc paths******/
$kycUploadDirectoy   = "{$docRoot}kyc_docs/";
$kycAWSPath          = "https://s3.ap-south-1.amazonaws.com/keywo-kyc-doc-2017/2017/";
$minRequestAmount    = 0.5;
$minManualCashoutAmt = 10;
$minCashoutAmt       = 0.4;
/******************************/


/******* global array of exange rate ************/
$exchageCurrencyCode      = array("USD", "BTC", "SGD");
$keywoDefaultCurrencyName = "Credit";
/******* global array of exange rate ************/

/********** for support admin**************/
$chatRefreshTime   = 10000;
$agentsGroupCS     = "csagent";
$hodGroupCS        = "cshod";
$superAdminGroupCS = "SuperAdmin";
$coagent = "coagent";
/********** for support admin**************/

/****************************************************/
define("MAILGUN_API_KEY", "key-2b8f2419e616db09b1297ba51d7cc770");
define("MAILGUN_DOMAIN", "searchtrade.com");
define("DIR_ASSETS_EMAIL_TEMPLATES", "{$docRoot}views/email_template/");
$contactFormSubject = "Keywo Contact";
/****************************************************/


if ($errorMode != "production") {
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}else
{
    ini_set('display_errors', 0);
    error_reporting(0);
}

if ( date_default_timezone_set( "UTC" ) != TRUE)
{
    $now = new DateTime();
    $now->setTimezone(new DateTimeZone('UTC'));
}


?>



