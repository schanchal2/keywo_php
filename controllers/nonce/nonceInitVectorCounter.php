<?php
require_once('../../config/config.php');



/*$getCounter =  getVectorCounter();
echo "<pre>";
print_r($getCounter);
echo "</pre>";*/

function getVectorCounter(){
      global $docRoot;
      $returnArr = array();

      // nonce directory
      $nonceDir = $docRoot."controllers/nonce";

      if(is_dir($nonceDir)){

        // nonce json file that stores current timestamp and next request vector counter.
        $nonceCounterJson = $nonceDir."/nonceCounter.json";
        if(file_exists($nonceCounterJson)){

            // if json exist then read the current vector counter
            $fp = fopen($nonceCounterJson, "r+");
            $readJson = fread($fp, filesize($nonceCounterJson));
            fclose($fp);
            $readJson = json_decode($readJson, true);

            $currentCounter = $readJson["vector_count"];

            if($currentCounter < 1000000000000000){
          		    $nextCounter =$currentCounter +1;
          		    $nextCounter = sprintf("%'.016d\n", $nextCounter);
                  $nextCounter = trim(str_replace("\n",'', $nextCounter));
          	}else{
          		    $nextCounter = $nextCounter+1;
          	}

            // After getting the current vector counter prepare for the next vector counter and write
            // the nonce json file.
            $fp1 = fopen($nonceCounterJson, "r+");
            $currentTime = time();
            $arr = array("time" => $currentTime, "vector_count" => $nextCounter);
            $arr = json_encode($arr);
            fwrite($fp1, $arr);
            fclose($fp1);

            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $currentCounter;
        }else{
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Error: ".$nonceCounterJson." not exist.";
        }
      }else{
          $returnArr["errCode"] = 1;
          $returnArr["errMsg"] = "Error: ".$nonceDir." not exist.";
      }

      return $returnArr;
}

$archieve = createVectorCounterArchieve();

function createVectorCounterArchieve(){
    global $docRoot;

    $nonceDir = $docRoot."controllers/nonce";
    if(is_dir($nonceDir)){
        $nonceCounterJson = $nonceDir."/nonceCounter.json";
        if(file_exists($nonceCounterJson)){
            // If file exist then create or orverwrite the existing json file and create new json file with current time.
            $zip = new ZipArchive;
            $URL = $nonceDir."/nonceCounter.zip";

            if ($zip->open($URL, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE ) === TRUE ) {
                $URL = $nonceDir."/nonceCounter.json";
                $zip->addFile($URL, 'nonceCounter.json');
                $zip->close();

        
            }

            //echo "url : ".$URL;
        }else{
            echo "file not found";
        }
    }else{
      echo "directory not exist";
    }


}
/*
$fp = fopen($nonceCounterJson, "w");
$time = time();
$counter = "0000000000000000";
$arr = array("time" => $time, "vector_count" => $counter);
$arr = json_encode($arr);

fwrite($fp, $arr);
fclose($fp);
chmod($nonceCounterJson, 0777);
echo "file write successfully";
*/





/*$fp = fopen($nonceCounterJson, "a+");

echo "value of fp ".$fp;

if(!$fp){
    echo "Error: file not created";
}else{

  if(filesize($nonceCounterJson) == 0){
        $time = time();
        $counter = "0000000000000000";
        $arr = array("time" => $time, "vector_count" => $counter);
        $arr = json_encode($arr);

        fwrite($fp, $arr);
        fclose($fp);
        chmod($nonceCounterJson, 0777);
        echo "File write successfully";
  }else{
        $fp1 = fopen($nonceCounterJson, "r");
        $read = fread($fp1, filesize($nonceCounterJson));
        echo "<pre>";
        print_r($read);
        echo "</pre>";

  }
}*/
?>
