<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');

error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $postId   = $_POST["postId"]; //print_r($email, $postId);
    $type     = $_POST["type"];
    $postTime = $_POST["postTime"];
    $email    = $_POST["email"];

  $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

  $user_id="user_id,account_handle";
  $getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
  if(noError($getUserInfo)){
    $getUserInfo = $getUserInfo["errMsg"];
    $user_id     = $getUserInfo["user_id"];
    $accHandle   = $getUserInfo["account_handle"];
  }

  $userIdsOfAccHandler     = getFollowUser($targetDirAccountHandler);

  $followFlag = getFollowUserValue($userIdsOfAccHandler,$user_id,$_SESSION['id']);

	//call funcation to remove post
	$postStatus = getPostDetail($postId, $postTime,$_SESSION['id'],$followFlag);
	if (!empty($postStatus)) {
		$returnArr['errCode'] = $postStatus['errCode'];
		$returnArr['errMsg']  = $postStatus['errMsg'];
	} else {
		$returnArr["errCode"] = $postStatus['errCode'];
		$returnArr["errMsg"]  = $postStatus['errMsg'];
	}
} else {
	$returnArr['errCode'] = 100;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
