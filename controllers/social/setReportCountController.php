<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/analytics/userRegistration_analytics.php";
$extraArg = array();

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "blockUserOrPost.xml";
$xmlArray = initializeXMLLog($_SESSION["email"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
$xml_data['step1']["data"] = 'Block post or block User creation process';
/****** partially end log section ******/

error_reporting(0);
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $xml_data["islogin"]["data"] = 'User Session started';
    $email = $_SESSION["email"];
    if (is_array($_POST) && sizeof($_POST) > 0) {
        $xml_data["postFields"]["data"] = 'Success getting POST Data';

        $postId = cleanXSS(trim(urldecode($_POST["postId"])));
        $created_at = cleanXSS(trim(urldecode($_POST["created_at"])));
        $postType = cleanXSS(trim(urldecode($_POST["postType"])));
        $emailIdOfOther = cleanXSS(trim(urldecode($_POST["email"])));

        if (empty($reportText)) {
            $reportType = "option";
            $flagSetForCommentBox = 1;
        } else {
            $reportType = "other";
            if (isset($reportText) && !empty($reportText)) {
                $flagSetForCommentBox = 1;
            } else {
                $flagSetForCommentBox = 0;
            }
        }

        $user_id = "user_id,account_handle";
        $setReportCountInfo = setReportCountInfo($postId, $created_at, $postType);

        if (noError($setReportCountInfo)) {
            $getCMS = $setReportCountInfo["errMsg"];
            $errMsg = "Successfully Got Co Report details";
            $extraArgs["errCode"] = -1;
            $limitArr = $setReportCountInfo["errMsg"];
            $limitArr = $setReportCountInfo["errMsg"];
            foreach($limitArr as $key => $value)
            {
                $limit = $limitArr["{$key}"];
            }



            $reportedCount = $setReportCountInfo["errMsg"]["reportCount"];

            $xml_data["response"]["data"] = "";
            $xml_data["response"]["attributes"] = $setReportCountInfo;


            if ($limit == $reportedCount) {
                $setBlockPostRequest = setBlockPostRequest($postId, $created_at, $postType);
                if (noError($setBlockPostRequest)) {
                    $getCMS = $setBlockPostRequest["errMsg"];
                    $errMsg = "Successfully Blocked Post Count";
                    $extraArgs["errCode"] = -1;
                    $extraArgs["data"] = $setBlockPostRequest;
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

                    $xml_data["response"]["data"] = "";
                    $xml_data["response"]["attributes"] = $setBlockPostRequest;

                    $limit = $setBlockPostRequest["errMsg"]["block_post"];
                    $userReportedCount = $setBlockPostRequest["errMsg"]["threshold"];

                    if ($limit >= $userReportedCount) {
                        $user_id = "user_id,account_handle";
                        $getUserInfo = getUserInfo($emailIdOfOther, $walletURLIPnotification . 'api/notify/v2/', $user_id);
                        if (noError($getUserInfo)) {
                            $getUserInfo = $getUserInfo["errMsg"];
                            $accountHandlerId = $getUserInfo["user_id"];
                            $accountHandle = $getUserInfo["account_handle"];
                            $service_req_ID = "AutoBlock";
                            global $autoBlockerEmail;
                            $reason = "User have reached maximum report post limit";


                            $xml_data["response"]["data"] = "";
                            $xml_data["response"]["attributes"] = $getUserInfo;

                            $setBlockUserFromNotification = setUserNotifyInfo($accountHandlerId, "status", 2);

                            if (noError($setBlockUserFromNotification)) {
                                $getCMS = $setBlockUserFromNotification["errMsg"];
                                $errMsg = "Successfully Blocked User";
                                $extraArgs["errCode"] = -1;
                                $extraArgs["data"] = $setBlockUserFromNotification;
                                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                                $xml_data["response"]["data"] = "";
                                $xml_data["response"]["attributes"] = $setBlockUserFromNotification;
                                $setBlockUser = setUserBlockStatusKeywo($accountHandlerId, $reason, $autoBlockerEmail, $service_req_ID);

                                if (noError($setBlockUser)) {
                                    $setUserStatus = setUserStatusWallet($emailIdOfOther, "2");

                                    if (noError($setUserStatus)) {
                                        $setBlockStatus = setUserBlockStatusWallet($emailIdOfOther, $reason, $autoBlockerEmail, $service_req_ID);


                                        if (noError($setBlockStatus)) {
                                            $connKeyword = createDBConnection("dbkeywords");
                                            noError($connKeyword) ? $connKeyword = $connKeyword["connection"] : checkMode($connKeyword["errMsg"]);
                                            $returnArr = array();

                                            insert_user_statistics("blocked_users_block", $connKeyword);
                                            $getCMS = $setBlockUser["errMsg"];
                                            $errMsg = "Successfully Blocked User";
                                            $extraArgs["errCode"] = -1;
                                            $extraArgs["data"] = $setBlockUser;
                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                                            $xml_data["response"]["data"] = "";
                                            $xml_data["response"]["attributes"] = $setBlockUser;
                                            $xml_data["blockedId"]["data"] = $emailIdOfOther;
                                        } else {
                                            $returnArr["errCode"] = 50;
                                            $errMsg = "Error in Blocking User.";
                                            $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                                            $xml_data["response"]["data"] = "";
                                            $xml_data["response"]["attributes"] = $setBlockUser;
                                        }
                                    }
                                } else {
                                    $returnArr["errCode"] = 50;
                                    $errMsg = "Error in Blocking User.";
                                    $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                                    $xml_data["response"]["data"] = "";
                                    $xml_data["response"]["attributes"] = $setBlockUserFromNotification;
                                }
                            } else {
                                $returnArr["errCode"] = 51;
                                $errMsg = "Error in Featching User Details.";
                                $returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);
                                $xml_data["step2"]["data"] = "get user info";
                                $xml_data["userinfo"]["data"] = "";
                                $xml_data["userinfo"]["attribute"] = $getUserInfo;
                            }
                        } else {
                            $returnArr["errCode"] = 50;
                            $errMsg = "Error in Blocking User.";
                            $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                            $xml_data["response"]["data"] = "";
                            $xml_data["response"]["attributes"] = $setBlockUser;
                        }
                    } else {
                        $returnArr["errCode"] = 50;
                        $errMsg = "Error in Blocking Post.";
                        $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                        $xml_data["response"]["data"] = "";
                        $xml_data["response"]["attributes"] = $setBlockPostRequest;
                    }
                } else {
                    $returnArr["errCode"] = 53;
                    $errMsg = "Limit is not crosses Yet.";
                    $returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);
                    $xml_data["response"]["data"] = "";
                    $xml_data["response"]["attributes"] = $setReportCountInfo;
                }
            } else {
                $returnArr["errCode"] = 50;
                $errMsg = "Error in count of report Post Details.";
                $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);

                $xml_data["response"]["data"] = "";
                $xml_data["response"]["attributes"] = $errMsg;
            }
        } else {
            $returnArr["errCode"] = 52;
            $errMsg = "Error in Post Field Details.";
            $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
            $xml_data["postFields"]["data"] = '. Error: ' . $errMsg;
        }
    } else {
        $errCode = 100;
        $returnArr["errCode"] = $errCode;
        $errMsg = "Please Login To System.";
        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArg);
        $xml_data["islogin"]["data"] = '. Error: ' . $errMsg;
    }
}
$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
echo json_encode($returnArr);





















?>
