<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once ('../../models/social/commonFunction.php');



error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$email     = $_SESSION["email"];
  $count     = $_GET["count"]; //print_r($email, $count);
	//update user post count
	$postCount = updatePostCount($_SESSION['id'], $count);
	//print_r($postCount); die;
	if (!empty($postCount) && $postCount > 0) {
		$returnArr['errCode'] = -1;
		$returnArr['errMsg']  = $postCount['errMsg'];
	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"]  = 'No new post';
	}
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
