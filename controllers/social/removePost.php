<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once ('../../models/social/commonFunction.php');


error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
	$email    = $_SESSION["email"];
  $userId   = $_SESSION["id"];
  $postId   = $_POST["postId"]; //print_r($email, $postId);
	$type     = $_POST["type"];
	$postTime = $_POST["postTime"];
	//call funcation to remove post
	$postStatus = removePost($postId, $userId, $type, $postTime);
	if (!empty($postStatus)) {
		$returnArr['errCode'] = $postStatus['errCode'];
		$returnArr['errMsg']  = $postStatus['errMsg'];
	} else {
		$returnArr["errCode"] = $postStatus['errCode'];
		$returnArr["errMsg"]  = $postStatus['errMsg'];
	}
} else {
	$returnArr['errCode'] = 100;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
