<?php
	session_start();

	/* Add Required Files */
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/deviceHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../helpers/imageFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once "../../backend_libraries/xmlProcessor/xmlProcessor.php";

    $extraArg = array();
	//Checking Session is present or not
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		// printArr($_POST);
		$userId           = $_SESSION['id'];
		$commentText      = cleanXSS(urldecode($_POST['commentText'])); 
		$postId           = cleanXSS(urldecode($_POST['postId'])); 
		$postCreationTime = cleanXSS(urldecode($_POST['postCreatedAt'])); 
		$returnArray      = array();
		// echo "Comment : ".urlencode($commentText);
		$createCommentCall = createComment($userId, $postId, $postCreationTime, rawurlencode($commentText), '');
		// printArr($createCommentCall);
		if (noError($createCommentCall)) {
			$errCode = $createCommentCall['errCode'];
			$createCommentCall = $createCommentCall['errMsg'];
			$commentTextreturn = $createCommentCall['comment'];
			$createCommentCall['comment'] = $commentTextreturn;
			// printArr($createCommentCall);
			$errMsg = $createCommentCall;
			$returnArray['errCode'] = $errCode;
			$returnArray['errMsg'] = $errMsg;		
			$returnArray = setErrorStack($returnArray, -1, $errMsg, $extraArg);	
		} else {
			$errCode = $createCommentCall['errCode'];
			$createCommentCall = $createCommentCall['errMsg'];
			// printArr($createCommentCall);
			$errMsg = $createCommentCall;
			$returnArray = setErrorStack($returnArray, $errCode, $errMsg, $extraArg);
		}
	} else {
		$returnArray['errCode'] = 100;
		$returnArray['errMsg'] = "Please Login";
		$returnArray = setErrorStack($returnArray, 100, $returnArray['errMsg'], $extraArg);
	}
	// $returnArray = setErrorStack($returnArray, -1, "DEMO Successfull ", NULL);
	echo json_encode($returnArray);





?>