<?php
	session_start();

	/* Add Required Files */
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/deviceHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../helpers/imageFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";
	require_once "../../models/keywords/acceptBidModel.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once "../../backend_libraries/xmlProcessor/xmlProcessor.php";

    $extraArg = array();

	//Checking Session is present or not
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		// printArr($_POST);
		$userId           = $_SESSION['id'];
		$commentText      = cleanXSS(urldecode($_POST['commentText']));
		$data             = explode(" @", $commentText); 
		$postId           = cleanXSS(urldecode($_POST['postId']));
		$postCreationTime = cleanXSS(urldecode($_POST['postCreatedAt']));

		if(isset($_POST['commentId'])){
            $commentId    = cleanXSS(urldecode($_POST['commentId']));
		}

        if(isset($_POST['replyId'])){
            $replyId          = cleanXSS(urldecode($_POST['replyId']));
        }


		$PostCreatorEmail = cleanXSS(urldecode($_POST['PostCreatorEmail']));
		if (empty($commentId) || !isset($commentId)) {
			$commentId 	  = '';
		}
		if (empty($replyId) || !isset($replyId)) {
			$replyId 	  = '';
		}
		$returnArray      = array();
		$commnetTextCount = '';
		$commentText1     = '';

		$commnetTextCount = strlen($commentText);
		if ($commnetTextCount >= 600) {
			$commentText1 = substr($commentText,0, 600);
		} else {
			$commentText1 = $commentText;
		}

		$createCommentCall = creatComment($userId, $postId, $postCreationTime, rawurlencode($commentText1), $commentId, $replyId);
		if (noError($createCommentCall)) {
			// $to = 'nikhilkakde@bitstreet.in';
			// $email_body = "This is testing to perform Notification in Social Mode";
			// $smsText = "This is testing to perform Notification in Social Mode";
			// // $mobileNumber = +918898648243;
			// $notification_body = '12121112 Testing Notification Commenting on Post';
			// $preference_code = 2;
			// $category = 'Social';
			// $linkStatus = 1;
			// $a = sendNotificationBuyPrefrence($to,'Testing',$email_body,$_SESSION['first_name'],$_SESSION['last_name'],$_SESSION['id'],$smsText,$mobileNumber,$notification_body,$preference_code,$category,$linkStatus);
			// printArr($_POST);

			$sendFrom['id']             = $_SESSION['id'];
			$sendFrom['email']          = $_SESSION['email'];
			$sendFrom['account_handle'] = $_SESSION['account_handle'];
			$sendFrom['first_name']     = $_SESSION['first_name'];
			$sendFrom['last_name']      = $_SESSION['last_name'];
			if (empty($_POST['commentId'])) {
				$commentType = 'comment';
			} else {
				$commentType = 'reply';
			}
			$commentId = $createCommentCall['errMsg']['_id'];

			// To send notification to all pot related user
			$setNotification = createNotificationForSocial($postId, $postCreationTime, $commentType, $sendFrom, rawurlencode($commentText1), $PostCreatorEmail);
			// printArr($setNotification);

			$errCode                      = $createCommentCall['errCode'];
			$createCommentCall            = $createCommentCall['errMsg'];
			$commentTextreturn            = $createCommentCall['comment'];
			$createCommentCall['comment'] = $commentTextreturn;
			$errMsg                       = $createCommentCall;
			$returnArray['errCode']       = $errCode;
			$returnArray['errMsg']        = $errMsg;
			$returnArray                  = setErrorStack($returnArray, -1, $errMsg, $extraArg);
		} else {
			$errCode                      = $createCommentCall['errCode'];
			$createCommentCall            = $createCommentCall['errMsg'];
			$errMsg                       = $createCommentCall;
			$returnArray                  = setErrorStack($returnArray, $errCode, $errMsg, $extraArg);
		}
	} else {
		$returnArray['errCode'] = 100;
		$returnArray['errMsg'] = "Please Login";
		$returnArray = setErrorStack($returnArray, 100, $returnArray['errMsg'], $extraArg);
	}
	// $returnArray = setErrorStack($returnArray, -1, "DEMO Successfull ", $extraArg);
	echo json_encode($returnArray);





?>
