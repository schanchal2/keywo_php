<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 13/6/17
 * Time: 3:48 PM
 */

/* Add Session Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";

/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once('../../helpers/deviceHelper.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once("../../models/cdp/cdpUtilities.php");

$extraArg = array();
$retArray = array();

$sid  = cleanXSS(trim(urldecode($_POST['sid'])));
$currSid = session_id();

if(isset($_SESSION) && !empty($_SESSION['email'])){

    if($currSid == $sid){

        $postId   	     = cleanXSS(trim(urldecode($_POST['postId'])));
        $postCreatedAt	 = cleanXSS(trim(urldecode($_POST['postCreatedAt'])));
        $id = $_SESSION["id"];

        $parentPostDetails  = getPostDetail($postId, $postCreatedAt, $id, 2);
        //$parentPostDetails = json_decode($parentPostDetails, true);
        //printArr($parentPostDetails);die;

        if(noError($parentPostDetails)){
            /*$retArray['errCode'] = -1;
            $retArray['errMsg'] = $parentPostDetails['errMsg'];*/
            $postDetail = $parentPostDetails['errMsg'][0];

            /* Post details values */
            $accountHandle = $postDetail['user_ref']['account_handle'];
            $emailId = $postDetail['user_ref']['email'];
            $postType = $postDetail['post_type'];
            $postedBy = $postDetail['posted_by'];
            $postCollectionName = $postDetail['post_collection'][0];
            $filename = $postDetail['post_details']['img_file_name'];
            $createdAt = $postDetail['created_at'];
            $emailId = $postDetail["user_ref"]["email"];
            $mil = $postDetail['created_at'];
            $seconds = $mil / 1000;
//            $created_at = date("d-m-Y H:i:s", $seconds);
            $created_at = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
            $timestamp2 = strtotime($created_at);
            $ext = explode('.', $filename);
            $postViewCountDisplay = $postDetail['post_views_count'];

            /* Shared post details values */
            $postSharedParent = $postDetail["post_details"]["parent_post"]["post_id"];
            $postSharedParentId = $postSharedParent["_id"];
            $postSharedParentFname = $postSharedParent["user_ref"]["first_name"];
            $postSharedParentLname = $postSharedParent["user_ref"]["last_name"];
            $postSharedParentUserId = $postSharedParent["user_ref"]["user_id"];
            $postSharedParentAccHandle = $postSharedParent["user_ref"]["account_handle"];
            $postSharedParentEmail = $postSharedParent["user_ref"]["email"];
            $postSharedParentPostedBy = $postSharedParent["posted_by"];
            $postSharedCreatedAt = $postSharedParent["created_at"];
            $postSharedKeyword = $postSharedParent["keywords"][0];
            $postSharedKeywords = $postSharedParent["keywords"];
            $postSharedShortDesc = $postSharedParent["post_short_desc"];
            $postSharedType = $postSharedParent["post_type"];
            $postSharedAssetUrl = $postSharedParent["post_details"]["asset_url"];
            $postSharedBlogTitle = $postSharedParent["post_details"]["blog_title"];
            $postSharedImgUrl = $postSharedParent["post_details"]["img_file_name"];
            $postSharedBlogContent = $postSharedParent["post_details"]["blog_content"];
            $post_share_earnings_parent      = $postSharedParent['post_earnings'];

            /* Get like/share post details */
            $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);

            $postIddata = array(array(
                'post_id' => $postId,
                'time' => $postCreatedAt
            ));

            $bulkPostId = json_encode($postIddata);
            /* Get like post activity count from node database */
            $activityCount = getPostActivityCount($bulkPostId, '');

            $targetActivityCount = $activityCount['errMsg'][$postId];
            $like_count = $targetActivityCount['like_count'];
            $commentCount = $targetActivityCount['comment_count'];
            $shareCount = $targetActivityCount['share_count'];
            $parentLikeCount = $targetActivityCount['parent_like_count'];
            $parentCommentCount = $targetActivityCount['parent_comment_count'];
            $parentShareCount = $targetActivityCount['parent_share_count'];
            $parentViewCount = $targetActivityCount['parent_view_count'];

            if(isset($parentPostDetails["user_ref"]["profile_pic"]) && !empty($parentPostDetails["user_ref"]["profile_pic"])){
                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($parentPostDetails["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $parentPostDetails["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$parentPostDetails["user_ref"]["profile_pic"];

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                $imgSrc = $rootUrlImages."default_profile.jpg";
            }
            ?>
        <div class="social-card card innerLR">
            <div class="social-timeline-image innerMB">
                <div class="half innerAll" id="social-timeline-image">
                    <div class="">
                        <div class="row half innerTB">
                            <div class="col-xs-8">
                                <div class="social-timeline-profile-pic pull-left">
                                    <img src="<?php echo $imgSrc; ?>" class=" img-responsive" />
                                </div>
                                <div class="social-timeline-profile-details pull-left innerL">
                                    <div class="social-user-link-name">
                                        <a href="#" class="ellipses ellipses-general">
                                            <?php echo $postDetail["user_ref"]["first_name"]; ?> <?php echo $postDetail["user_ref"]["last_name"]; ?>
                                        </a>
                                    </div>
                                    <div class="social-user-link">
                                        <a href="#">
                                            <?php echo '@' . $postDetail["user_ref"]["account_handle"]; ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 social-post-options">
                                <div class="pull-right">
                                    <span class="innerLR half text-color-Gray"><i class="fa fa-star-o"></i></span>
                                    <a>
                                        <span class="fa fa-chevron-down"></span>
                                    </a>
                                </div>
                                <br>
                                <div class="pull-right social-post-time-container">
                                    <span class="social-post-time text-light-grey"><span title="<?php  echo  uDateTime("h:i A",$postDetail['created_at'])." - ".uDateTime("d M Y",$postDetail['created_at']);?>"><?php include("../../views/social/getDateFormate.php"); ?></span></span>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if(isset($postDetail["keywords"]) && !empty($postDetail["keywords"])) {
                        $keywords = $postDetail["keywords"];
                    } elseif (isset($postSharedKeyword) && !empty($postSharedKeyword)) {
                        $keywords = $postSharedKeywords;
                    }
                    ?>

                    <div class="social-timeline-keywords-details">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                foreach ($keywords as $keywo) {
                                    ?>
                                    <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="dynamic keyword name">
                                        #<?php echo $keywo ; ?></a>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                    <div class="social-timeline-content-message">
                        <div class="row border-top border-bottom">
                            <div class="col-xs-12 half innerTB">
                                <span>
                                    <?php
                                    $shortDescriptionText = getLinksOnText(rawurldecode($postDetail["post_short_desc"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
                                    $shortDescription     = $shortDescriptionText["text"];
                                    $counterText          = $shortDescriptionText["counter"];
                                    echo $shortDescription;
                                    ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <?php if ($postType == "share") { ?>
                    <div class="row border-bottom">
                        <?php include("../../views/social/shareParentPostContent.php");?>
                    </div>
                    <?php } ?>
                    <div class="social-timeline-earning-comments-view innerT">
                        <div class="row">
                            <div class="col-xs-4">
                                <div>
                                    <label>Earning : </label>
                                    <?php
                                    $currentPayout = getCurrentPayout();
                                    if(noError($currentPayout)) {
                                        $currentPayout = $currentPayout["current_payout"];
                                        $currentPayout = number_format(($currentPayout / 4), 8);
                                    }else{
                                        $currentPayout = 0.0000;
                                    }
                                    //$currentPayout = number_format($currentPayout, 4,'.','');
                                    echo "<span onclick=convertPrice('".$_SESSION['CurrPreference']."') origPrice='".
                                        $currentPayout.' '.$keywoDefaultCurrencyName."' title = '' data-toggle='tooltip' data-placement='bottom' data-original-title= '".$currentPayout.' '.$keywoDefaultCurrencyName."' >".formatNumberToSort($currentPayout,4)." <span class='currency'>$keywoDefaultCurrencyName</span></span>";
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-8">
                                <div class="">
                                    <ul class="list-inline pull-right margin-bottom-none">
                                        <li class="padding-right-none">
                                            <div>
                                                <?php if (!empty($like_count)) { ?>
                                                    <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="">
                                                        <?php $likeCount = $like_count; ?>
                                                        <span class="half innerR like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$likeCount}", 0); ?></span>
                                                    </a>
                                                <?php } else {?>
                                                    <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="">
                                                        <span class="half innerR like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                                <?php } ?>
                                                <label>Likes</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <span class="half innerR comment-count<?php echo $postId;?>"><?php echo formatNumberToSort("{$commentCount}", 0);?></span>
                                                <label class="pull-right">Comments</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0);?></span>
                                                <label>Shares</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="row innerMT inner-2x">
                <div class="col-xs-12">
                    <button type="submit" class="btn-social-wid-auto btn-xs pull-right innerLR" id="ft_navigator" value="Next">Next</button>
                </div>
            </div>
        </div>
            <script>
                //tooltip
                $('[data-toggle="tooltip"]').tooltip({
                    trigger:'hover'
                });
            </script>
        <!--User post card view html ends-->

        <?php }else{
            $retArray['errCode'] = 10;
            $retArray['errMsg'] = "This Post No longer Exist";
        }
    }else{
        $retArray['errCode'] = 11;
        $retArray['errMsg'] = 'Error : unauthorized access';
    }

}else{
    $retArray['errCode'] = 12;
    $retArray['errMsg'] = 'Error : session expired';
}

?>
