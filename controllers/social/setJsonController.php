<?php
/**
 **********************************************************************************
 *                  setJsonController.php
 * ********************************************************************************
 *      This controller is used for Setting Json File For followers and followings Details to social
 */

/* Add Seesion Management Files */
session_start();
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/keywords/acceptBidModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/social/socialModel.php";

$extraArg = array();

//check whether session is destroyed
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$_POST["accountHandleType"]      = "followings";
$_POST["otherAccountHandleType"] = "followers";
$emailOfOtherPerson              = $_POST["user_email"];
global $docRoot;

$xmlProcessor = new xmlProcessor();
$xmlfilename = "JsonCreation.xml";
$xmlArray = initializeXMLLog($_SESSION["email"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
$xml_data['step1']["data"] = 'Json Creation Process';

error_reporting(0);
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
        $email = $_SESSION["email"];
        $xml_data["islogin"]["data"] = 'User Session started';
        if (is_array($_POST) && sizeof($_POST) > 0) {
            $user_id = "user_id,account_handle,first_name,last_name,email";
            $getUserInfoForOtherPerson = getUserInfo($emailOfOtherPerson, $walletURLIPnotification . 'api/notify/v2/', $user_id);
            if (noError($getUserInfoForOtherPerson)) {

                $getUserInfoForOtherPerson = $getUserInfoForOtherPerson["errMsg"];
                $otherAccountHandlerId = $getUserInfoForOtherPerson["user_id"];
                $anotherAccountHandle = $getUserInfoForOtherPerson["account_handle"];

                $accountHandlerId = $_SESSION["id"];
                $accountHandle = $_SESSION["account_handle"];

                $countOfFollowingOfAllJson = 0;
                $countOfFollowerOfAllJson = 0;

                //printArr($getUserInfoForOtherPerson);

                $accountHandleType = cleanXSS(trim(urldecode($_POST["accountHandleType"])));
                $otherAccountHandleType = cleanXSS(trim(urldecode($_POST["otherAccountHandleType"])));


                $targetDirAccountHandler = $docRoot . "json_directory/social/followerwonk/" . $accountHandle . "/";
                $targetDirOtherAccountHandler = $docRoot . "json_directory/social/followerwonk/" . $anotherAccountHandle . "/";

                $folderExist = folderPresenceCheck($targetDirAccountHandler);
                $folderExist1 = folderPresenceCheck($targetDirOtherAccountHandler);

                $jsonFileCreationOfOtherAccountHandleWithFirstChar = strtolower($anotherAccountHandle[0]);
                $jsonFileCreationOfAccountHandleWithFirstChar = strtolower($accountHandle[0]);

                if (has_specchar($jsonFileCreationOfOtherAccountHandleWithFirstChar)) {
                    $jsonFileCreationOfOtherAccountHandle = "0";
                } else {
                    $jsonFileCreationOfOtherAccountHandle = strtolower($anotherAccountHandle[0]);
                }

                if (has_specchar($jsonFileCreationOfAccountHandleWithFirstChar)) {
                    $jsonFileCreationOfAccountHandle = "0";
                } else {
                    $jsonFileCreationOfAccountHandle = strtolower($accountHandle[0]);
                }


                $returnArr = array();

                function getAccountHandlerData()
                {
                    $formdata = array(
                        'followings' => [],
                        'followers' => [],
                        'blocked' => []
                    );
                    return json_encode($formdata, JSON_PRETTY_PRINT);
                }


                $accountHandlerFileName = $targetDirAccountHandler . $accountHandle . "_info_" . $jsonFileCreationOfOtherAccountHandle . ".json";
                if (!file_exists($accountHandlerFileName)) {
                    if (file_put_contents($accountHandlerFileName, getAccountHandlerData())) {
                        $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $otherAccountHandlerId);
                        if (noError($setJson)) {
                            $getCMS = $setJson["errMsg"];
                            $errMsg = "Successfully Added Post details";
                            // $extraArgs["errCode"] = -1;
                            // $extraArgs["data"] = $setJson;
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


                            $xml_data["step4"]["data"] = "Json File Creation Of Account Handler";
                            $xml_data["userinfo"]["data"] = "";
                            $xml_data["userinfo"]["attribute"] = $setJson;


                            $files2 = dirToArray($targetDirAccountHandler);
                            foreach ($files2 as $key => $files2Details) {
                                $Final = $targetDirAccountHandler . $files2Details;
                                $jsondata = file_get_contents($Final);
                                $data = json_decode($jsondata, true);
                                $countOfFollowingJson = count($data["followings"]);
                                $countOfFollowingOfAllJson = $countOfFollowingJson + $countOfFollowingOfAllJson;
                            }
                            $followingCount = $countOfFollowingOfAllJson;
                            $notifyFieldType = 'followings';
                            $setFollowing = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $followingCount);


                        } else {
                            $returnArr["errCode"] = 50;
                            $errMsg = "Error in Adding Post Details.";
                            $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                        }
                    } else {
                        $returnArr["errCode"] = 52;
                        $errMsg = "Error in Creating Json File For Handler.";
                        $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
                    }
                } else {
                    $setJson = updationOfJsonFile($accountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $otherAccountHandlerId);
                    if (noError($setJson)) {
                        $getCMS = $setJson["errMsg"];
                        $errMsg = "Successfully Added Json details";
                        // $extraArgs["errCode"] = -1;
                        // $extraArgs["data"] = $setJson;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


                        $xml_data["step4"]["data"] = "Json File Updation Of Account Handler";
                        $xml_data["userinfo"]["data"] = "";
                        $xml_data["userinfo"]["attribute"] = $setJson;


                        $files2 = dirToArray($targetDirAccountHandler);
                        foreach ($files2 as $key => $files2Details) {
                            $Final = $targetDirAccountHandler . $files2Details;
                            $jsondata = file_get_contents($Final);
                            $data = json_decode($jsondata, true);
                            $countOfFollowingJson = count($data["followings"]);
                            $countOfFollowingOfAllJson = $countOfFollowingJson + $countOfFollowingOfAllJson;
                        }
                        $followingCount = $countOfFollowingOfAllJson;
                        $notifyFieldType = 'followings';
                        $setFollowing = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $followingCount);


                    } else {
                        $returnArr["errCode"] = 50;
                        $errMsg = "Error in Adding Post Details.";
                        $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }
                }

                $otherAccountHandlerFileName = $targetDirOtherAccountHandler . $anotherAccountHandle . "_info_" . $jsonFileCreationOfAccountHandle . ".json";
                if (!file_exists($otherAccountHandlerFileName)) {
                    if (file_put_contents($otherAccountHandlerFileName, getAccountHandlerData())) {
                        $setJson = updationOfJsonFileForOtherAccountHandle($otherAccountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $otherAccountHandlerId);
                        // echo "<br>1<br>";
                        // printArr($setJson);
                        if (noError($setJson)) {
                            $getCMS = $setJson["errMsg"];

                            if (isset($setJson['errMsg']['type']) && !empty($setJson['errMsg']['type']) && $setJson['errMsg']['type'] == -1) {
                                // echo "1";
                                $sendFrom['id'] = $_SESSION['id'];
                                $sendFrom['email'] = $_SESSION['email'];
                                $sendFrom['account_handle'] = $_SESSION['account_handle'];
                                $sendFrom['first_name'] = $_SESSION['first_name'];
                                $sendFrom['last_name'] = $_SESSION['last_name'];

                                $userDetails['id'] = $getUserInfoForOtherPerson['user_id'];
                                $userDetails['email'] = $getUserInfoForOtherPerson['email'];
                                $userDetails['account_handle'] = $getUserInfoForOtherPerson['account_handle'];
                                $userDetails['first_name'] = $getUserInfoForOtherPerson['first_name'];
                                $userDetails['last_name'] = $getUserInfoForOtherPerson['last_name'];

                                // To send notification to all pot related user
                                $setNotification = createNotificationForFollow($userDetails, $sendFrom);
                                // printArr($setNotification);
                            }
                            $errMsg = "Successfully Added Json details";
                            // $extraArgs["errCode"] = -1;
                            // $extraArgs["data"] = $setJson;
                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


                            $xml_data["step5"]["data"] = "Json File Creation Of Another Account Handler";
                            $xml_data["userinfoOFAnotherAccountHandler"]["data"] = "";
                            $xml_data["userinfoOFAnotherAccountHandler"]["attribute"] = $setJson;


                            $files2 = dirToArray($targetDirOtherAccountHandler);

                            foreach ($files2 as $key => $files2Details) {
                                $Final = $targetDirOtherAccountHandler . $files2Details;
                                $jsondata = file_get_contents($Final);
                                $data = json_decode($jsondata, true);
                                $countOfFollowerJson = count($data["followers"]);
                                $countOfFollowerOfAllJson = $countOfFollowerJson + $countOfFollowerOfAllJson;
                            }
                            $followerCount = $countOfFollowerOfAllJson;
                            $notifyFieldType = 'followers';
                            $setFollowing = setUserNotifyInfo($otherAccountHandlerId, $notifyFieldType, $followerCount);


                        } else {
                            $returnArr["errCode"] = 50;
                            $errMsg = "Error in Adding Json Details.";
                            $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);
                        }
                    } else {
                        $returnArr["errCode"] = 52;
                        $errMsg = "Error in Creating Json File For Other Handler.";
                        $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
                    }
                } else {
                    $setJson = updationOfJsonFileForOtherAccountHandle($otherAccountHandlerFileName, $accountHandleType, $accountHandlerId, $otherAccountHandleType, $otherAccountHandlerId);
                    // echo "<br>2<br>";
                    // printArr($setJson);
                    if (noError($setJson)) {
                        $getCMS = $setJson["errMsg"];
                        if (isset($setJson['errMsg']['type']) && !empty($setJson['errMsg']['type']) && $setJson['errMsg']['type'] == -1) {
                            // echo "2";
                            $sendFrom['id'] = $_SESSION['id'];
                            $sendFrom['email'] = $_SESSION['email'];
                            $sendFrom['account_handle'] = $_SESSION['account_handle'];
                            $sendFrom['first_name'] = $_SESSION['first_name'];
                            $sendFrom['last_name'] = $_SESSION['last_name'];

                            $userDetails['id'] = $getUserInfoForOtherPerson['user_id'];
                            $userDetails['email'] = $getUserInfoForOtherPerson['email'];
                            $userDetails['account_handle'] = $getUserInfoForOtherPerson['account_handle'];
                            $userDetails['first_name'] = $getUserInfoForOtherPerson['first_name'];
                            $userDetails['last_name'] = $getUserInfoForOtherPerson['last_name'];

                            // To send notification to all pot related user
                            $setNotification = createNotificationForFollow($userDetails, $sendFrom);
                            // printArr($setNotification);
                        }
                        $errMsg = "Successfully Added Json details";
                        // $extraArgs["errCode"] = -1;
                        // $extraArgs["data"] = $setJson;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

                        $xml_data["step5"]["data"] = "Json File Creation Of Another Account Handler";
                        $xml_data["userinfoOFAnotherAccountHandler"]["data"] = "";
                        $xml_data["userinfoOFAnotherAccountHandler"]["attribute"] = $setJson;


                        $files2 = dirToArray($targetDirOtherAccountHandler);

                        foreach ($files2 as $key => $files2Details) {
                            $Final = $targetDirOtherAccountHandler . $files2Details;
                            $jsondata = file_get_contents($Final);
                            $data = json_decode($jsondata, true);
                            $countOfFollowerJson = count($data["followers"]);
                            $countOfFollowerOfAllJson = $countOfFollowerJson + $countOfFollowerOfAllJson;
                        }
                        $followerCount = $countOfFollowerOfAllJson;
                        $notifyFieldType = 'followers';
                        $setFollowing = setUserNotifyInfo($otherAccountHandlerId, $notifyFieldType, $followerCount);


                    } else {
                        $returnArr["errCode"] = 50;
                        $errMsg = "Error in Adding Json Details.";
                        $returnArr = setErrorStack($returnArr, 50, $errMsg, $extraArg);

                        $xml_data["RESPONSE"]["data"] = "";
                        $xml_data["RESPONSE"]["attributes"] = $errMsg;


                    }
                }
            } else {
                $returnArr["errCode"] = 51;
                $errMsg = "Error in Featching User Details.";
                $returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);

                $xml_data["step2"]["data"] = "get user info";
                $xml_data["userinfo"]["data"] = "";
                $xml_data["userinfo"]["attribute"] = $getUserInfo;


            }
        } else {
            $returnArr["errCode"] = 52;
            $errMsg = "Error in Post Field Details.";
            $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);

            $xml_data["postFields"]["data"] = '. Error: ' . $errMsg;
        }
}
else{
    $errCode = 100;
    $returnArr["errCode"] = $errCode;
    $errMsg = "Please Login To System.";
    $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArg);
    $xml_data["islogin"]["data"] = '. Error: '.$errMsg;
}

$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
echo json_encode($returnArr);
?>
