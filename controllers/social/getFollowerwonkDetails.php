<?php
// function to get user's followerwonk details from json directory
header("Access-Control-Allow-Origin: *");

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once ("../../models/social/commonFunction.php");

$userId        = $_GET["user_id"];
$userAccHAndle = $_GET["account_handle"];
$target        = $_GET["target"];

$targetDirAccountHandler = "../../json_directory/social/followerwonk/" . $userAccHAndle . "/";
$blockedUser             = getBlockedUser($targetDirAccountHandler);
// print_r(json_encode($blockedUser)); die;
if (!empty($blockedUser)) {
	$returnArr["errCode"] = -1;
	$returnArr["errMsg"]  = json_encode($blockedUser);
} else {
	$returnArr["errCode"] = 4;
	$returnArr["errMsg"]  = "Error on reading json for blocked";
}
echo json_encode($returnArr);

?>
