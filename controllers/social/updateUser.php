<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once ('../../models/social/commonFunction.php');


error_reporting(0);

//check whether session is destroyed
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    $returnArr['errMsg'] = 'Please login again';
    $returnArr['errCode'] = 100;
    echo json_encode($returnArr);
    die;
}

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$id                = $_SESSION['id'];
	$email             = $_SESSION["email"];
	$data              = cleanXSS(trim(urldecode($_POST["value"])));

	//edit user's short description
  	$setDesc = setUserNotifyInfo($id, 'short_desc', rawurlencode($data));
  	if(noError($setDesc)){
  		$returnArr['errMsg']  = 'success';
  		$returnArr['errCode'] = $setApp["errCode"];
  	}else{
  		$returnArr["errCode"] = $setApp["errCode"];
  		$returnArr["errMsg"]  = $setApp["errMsg"];
  	}
}else{
	$returnArr['errMsg'] = 'Please try again';
	$returnArr['errCode'] = 2;
}
echo json_encode($returnArr);
?>
