<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once ('../../models/social/commonFunction.php');
$extraArg = array();


/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "postCreation.xml";
$xmlArray = initializeXMLLog($_SESSION["email"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
$xml_data['step1']["data"] = 'Post Creation Process';

/****** partially end log section ******/

// printArr($_POST); die();
// $_POST["post-id"]="1162";
// echo "<br>".$_POST["postscope"]="public";
// echo "<br>".$_POST["modalComment"]="Kavita Maurya";
// echo "<br>".$_POST["post-type"]="status";
// $_POST["post-method"]="Update";



		error_reporting(0);
		if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
		{
				$xml_data["islogin"]["data"] = 'User Session started';
				$email = $_SESSION["email"];
				if(is_array($_POST) && sizeof($_POST) > 0){
				$xml_data["postFields"]["data"] = 'Success getting POST Data';

				$shortdesc              = cleanXSS(trim(urldecode($_POST["modalComment"])));
			  	$post_type            = cleanXSS(trim(urldecode($_POST["post-type"])));
			 	$postscope              = cleanXSS(trim(urldecode($_POST["postscope"])));
				$keywords               = cleanXSS(trim(urldecode($_POST["modalKeywords"])));
				$id                     = cleanXSS(trim(urldecode($_POST["post-id"])));
				$blog_title             = cleanXSS(trim(urldecode($_POST["modalTitle"])));
				$imageUrl               = cleanXSS(trim(urldecode($_POST["imageUrl"])));
				$postTime               = cleanXSS(trim(urldecode($_POST["post-create-time"])));
				$blog_content           = $_POST["blog_content"];
				$img_file_name          = cleanXSS(trim(urldecode($_FILES["fileToUpload"]['name'])));
        		$ext                    = explode('.', $_FILES['fileToUpload']['name']);


				$asset_url              = cleanXSS(trim(urldecode($_POST["asset_url"])));
				$checkArr1                  = array();
				$returnArr                  = array();


				$xml_data["postAttribute"]["data"]="";
				$xml_data["postAttribute"]["attribute"]=$_POST;

				if($post_type=="video"){
					$asset_url=str_replace("watch?v=","embed/","$asset_url");
				}

				$user_id="user_id,account_handle";
				$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);

				if(noError($getUserInfo)){
						$getUserInfo = $getUserInfo["errMsg"];
						$posted_by = $getUserInfo["user_id"];
						$account_handle = $getUserInfo["account_handle"];



											//create dom object to load HTML result from url
											$doc = new DOMDocument();
											$doc->loadHTML($blog_content);
											//get image tags from blog content text editor
											$arr = $doc-> getElementsByTagName("img");

											//loop through image tags
											$count= 0;
											foreach ($arr as $item) {
												$count=$count+1;
												$src = $item->getAttribute("src");

												$new_data=explode(";", $src);
												$type=$new_data[0];
												$extension = str_replace("data:image/","","$type");
												$data=explode(",", $new_data[1]);
												header("Content-type:".$type);
												$content=base64_decode($data[1]);

												$dir = $docRoot.'images/social/users/blogs/';
												$name = $_SESSION['first_name'];

												$targetDir = "../../images/social/users/".$account_handle."/"."post/blog/content/".$extension."/";
												/* To check folder Existance */
												$folderExist = folderPresenceCheck($targetDir);

												if (noError($folderExist)) {
													//File path to store image on php server
													$filename = $targetDir.''.$account_handle.'_'.$blog_title.'_'.$count.".".$extension;
													//file path to store image on node databse
													$filenameHtml = $cdnSocialUrl.'users/'.$account_handle."/"."post/blog/content/".$extension."/".$account_handle.'_'.$blog_title.'_'.$count.".".$extension.".725x500.".$extension;
													file_put_contents($filename, $content);

												} else {
													$checkArr['error']      = "Directed Folder Mismatched";
												}

												//set proper path to images
												$item->setAttribute('src', $filenameHtml);


											}

												//get updated links and image tags
												$blog_content=$doc->saveHTML();

//					printArr($blog_content);

						$xml_data["postAttribute"]["data"]="";
						$xml_data["postAttribute"]["attribute"]=$_POST;

						$xml_data["step2"]["data"] = "get user info";
						$xml_data["userinfo"]["data"] = "";
						$xml_data["userinfo"]["attribute"] = $getUserInfo;

						$extraArgs["xml_data"] = $xml_data;

						if (!isset($_FILES['fileToUpload']['name']) || empty($_FILES['fileToUpload']['name'])) {
							// echo "imagUrl";
								$img_file_name = $imageUrl;
						} else {
							// echo "New Url";
								$img_file_name = $_FILES['fileToUpload']['name'];
						}


            	$setPost = updatePostInfo($id, $postTime, $posted_by, $post_type, $shortdesc, $keywords, $postscope, $blog_title, $blog_content, $img_file_name, $asset_url);
                             // printArr($setPost);
							if(noError($setPost)){
								$getCMS = $setPost["errMsg"];
								$errMsg = "Successfully Added Post details";
								$extraArgs["errCode"] = -1;

                $post_id= $setPost["errMsg"]["_id"];
                $created_at= $setPost["errMsg"]["created_at"];
                if($post_type=="image")
                {

                    $targetDir = "../../images/social/users/".$account_handle."/"."post/images/".$ext[1]."/";
                }
                elseif($post_type=="blog")
                {
                    $targetDir = "../../images/social/users/".$account_handle."/"."post/blog/featured/".$ext[1]."/";
                }
                if($post_type=="image" || $post_type=="blog" )
                {
                    foreach ($_FILES as $key => $file) {
                        if ($file['error'] == 0) {
                            $checkArr = array();
                            list($width, $height, $type, $attr) = getimagesize($file['tmp_name']);
                            //	echo basename($file['name']);

                            /* To check error in File */
                            if (!isset($checkArr['error'])) {
                                $fileName = $account_handle."_".$post_id."_".$created_at."_".$img_file_name;

                                $targetFile = $targetDir . $fileName;
                                //print_r($fileName);

                                /* To check folder Existance */
                                $folderExist = folderPresenceCheck($targetDir);

                                if (noError($folderExist)) {
                                    /* To Upload Image in Folder */
                                    $uploadStatus = uploadImage($file,$width,$height,$type,$targetFile);
                                    if ( noError($uploadStatus) || $uploadStatus['errCode'] == 8 ) {
                                        $checkArr['success']    = $uploadStatus['errMsg'];
                                        $checkArr['name']       = 'images/social/'.$fileName;
                                        $checkArr['errCode']    = $uploadStatus['errCode'];
                                    } else {
                                        $checkArr['error']      = $uploadStatus['errMsg'];
                                        $checkArr['name']       = $file['name'];

                                    }
                                } else {
                                    $checkArr['error']      = "Directed Folder Mismatched";
                                    $checkArr['name']       = $file['name'];
                                }
                            }
                        }
                    }
                }
                				$errMsg = "Post Updated Successfully";
								$extraArgs["data"] = $setPost;
								$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

								$xml_data["RESPONSE"]["data"] = "";
								$xml_data["RESPONSE"]["attributes"] =$setPost;
							}else{
								$returnArr["errCode"] = 50;
								$errMsg = "Error in Adding Post Details.";
								$returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);

								$xml_data["RESPONSE"]["data"] = "";
								$xml_data["RESPONSE"]["attributes"] =$setPost;
							}
					}else{
						$returnArr["errCode"] = 51;
						$errMsg = "Error in Featching User Details.";
						$returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);
						$xml_data["step2"]["data"] = "get user info";
						$xml_data["userinfo"]["data"] = "";
						$xml_data["userinfo"]["attribute"] = $getUserInfo;
					}
			}
			else{
				$returnArr["errCode"] = 52;
				$errMsg = "Error in Post Field Details.";
				$returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);

				$xml_data["postFields"]["data"] = '. Error: '.$errMsg;
			}
		}else{

			$returnArr["errCode"] = 53;
			$errMsg = "Please Login To System.";
			$returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);

			$xml_data["islogin"]["data"] = '. Error: '.$errMsg;
		}
		$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
		echo json_encode($returnArr);

?>
