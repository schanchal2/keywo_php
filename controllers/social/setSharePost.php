<?php
/*
 *      This controller is used for Sharing existing Post to social
 */

/* Add Session Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";

/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/keywords/acceptBidModel.php";

		$extraArg = array();

		/****** log write section ******/
		//for xml writing essential
		$xmlProcessor                    = new xmlProcessor();
		$xmlfilename                     = "shareExistingPost.xml";
		$xmlArray                        = initializeXMLLog($_SESSION["email"]);
		$xmlData['request']["data"]      = '';
		$xmlData['request']["attribute"] = $xmlArray["request"];
		$xmlData['step1']["data"]        = 'Post Creation Process';
		/****** partially end log section ******/
		error_reporting(0);
		if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
			$xmlData["islogin"]["data"] = 'User Session started';
  		    $email                      = $_SESSION["email"];

			if (is_array($_POST) && sizeof($_POST) > 0) {

			$xmlData["postFields"]["data"] = 'Success getting shared post data';

			$shortDesc       = cleanXSS(trim(urldecode($_POST["postShortDesc"])));
			$postType        = 'share';
			$postScope       = cleanXSS(trim(urldecode($_POST["postScope"])));
			$lenghOfShorDesc = strlen($shortDesc);
			$postId   	     = cleanXSS(trim(urldecode($_POST['postId'])));
			$postTime 		 = cleanXSS(trim(urldecode($_POST['postTime'])));
			$postParentId	 = cleanXSS(trim(urldecode($_POST['postParentId'])));
			$postCreatedAt	 = cleanXSS(trim(urldecode($_POST['postCreatedAt'])));
			$postMethod      = cleanXSS(trim(urldecode($_POST['postMethod'])));
			$postParentType  = cleanXSS(trim(urldecode($_POST['postParentType'])));
			$postParentEmail = cleanXSS(trim(urldecode($_POST['postParentEmail'])));
			$keywords        = cleanXSS(trim(urldecode($_POST['keywords'])));

			//post shared variables
			$parentPostId      = $postParentId;
			$childPostId       = $postId;
			$parentCreatedAt   = $postCreatedAt;
			$childCreatedAt    = $postTime;
			$userIp            = '115.240.90.163';//getClientIP();
			$userDeviceData    = getLocationUserFromIP($userIp);
			$userDeviceIp      = $userDeviceData['userIP'];
			$userDeviceCountry = $userDeviceData['country'];
			$userDeviceState   = $userDeviceData['state'];
			$userDeviceCity    = $userDeviceData['city'];
			$userDeviveBrowser = getBrowserName();
			$userDeviceType    = "web";
			$userGender        = $_SESSION['gender'];

  			// discription validation flag variable
  			if ($lenghOfShorDesc > $validateCharCountOnCommentBox) {
  				exit;
  			}

  			$returnArr                             = array();
  			$emailofSpecialUser                    = array();
  			$xmlData["postAttribute"]["data"]      = "";
  			$xmlData["postAttribute"]["attribute"] = $_POST;

            if(!in_array($email,$emailofSpecialUser)){
                //get Folllowed User
                $getFollowed         = getFollowUser("../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/");
                //check for value
                $userParentInfo    = "user_id,account_handle";
                $getParentUserInfo = getUserInfo($postParentEmail, $walletURLIPnotification.'api/notify/v2/', $userParentInfo);
                $followFlag        = getFollowUserValue($getFollowed,$getParentUserInfo["errMsg"]["user_id"],$_SESSION['id']);
            }else{
                $followFlag = 1;
            }
			$parentPostDetails = getPostDetail($parentPostId, $parentCreatedAt, $_SESSION["id"], $followFlag);
			$userInfo          = "user_id,account_handle";
			$getUserInfo       = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $userInfo);

			if (noError($getUserInfo)) {
				$getUserInfo    = $getUserInfo["errMsg"];
				$userId         = $getUserInfo["user_id"];
				$accountHandle  = $getUserInfo["account_handle"];

				$xmlData["postAttribute"]["data"]      = "";
				$xmlData["postAttribute"]["attribute"] = $_POST;
				$xmlData["step2"]["data"]              = "get user info";
				$xmlData["userinfo"]["data"]           = "";
				$xmlData["userinfo"]["attribute"]      = $getUserInfo;

				$extraArgs["xml_data"] = $xmlData;

				//if set share post than
				if ($postMethod == 'create') {
				$parentBlockedUser       = "";
				$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$parentPostDetails["errMsg"][0]["user_ref"]["account_handle"]."/".$parentPostDetails["errMsg"][0]["user_ref"]["account_handle"]."_info_blockList.json";
				if(file_exists($targetDirAccountHandler)) {
					$jsondataBlock     = file_get_contents($targetDirAccountHandler);
	  				$parentBlockedUser = json_decode($jsondataBlock, true);
				}
				$setPost = setSharerPost($parentPostId, $parentCreatedAt, $childPostId, $childCreatedAt, $userId, $postType, $shortDesc, $postScope, $userDeviceIp, $userDeviceCountry, $userDeviceState, $userDeviceCity, $userDeviveBrowser, $userDeviceType, $userGender, json_encode($parentBlockedUser), $keywords);
            	// To send notification to all pot related user
	            $sendFrom['id']             = $_SESSION['id'];
	            $sendFrom['email']          = $_SESSION['email'];
	            $sendFrom['account_handle'] = $_SESSION['account_handle'];
	            $sendFrom['first_name']     = $_SESSION['first_name'];
	            $sendFrom['last_name']      = $_SESSION['last_name'];
	            if (!empty($postParentEmail)) {
	            	$setNotification = createNotificationForSocial($setPost['errMsg']['_id'], $setPost['errMsg']['created_at'], 'share', $sendFrom, '', $postParentEmail);
	            }

							//call an api to increase share countKeyword
							$setSharerPostCount          = updatePostActivityCount($userId, $postId, 'share', $postCreatedAt, $postType);
							$returnArr["errCode"]        = $setPost["errCode"];
							$extraArg["sharePostCount"]  = $setSharerPostCount["errMsg"]["share_count"];
							$extraArg["sharePostDetail"] = $setPost["errMsg"];
							$errMsg                      = "Successfully shared.";
							$returnArr                   = setErrorStack($returnArr, -1, $errMsg, $extraArg);
						} elseif ($postMethod == 'update') {
							// $targetDirAccountHandlerBlock = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
						    // $blockedUser             = getBlockedUser($targetDirAccountHandlerBlock);
						    // print_r(json_encode($blockedUser)); die;
							$setPost                     = updatePostInfo($postId, $postTime, $userId, $postType, $shortDesc, $postScope, $postParentType, '', '', '', '');
							$returnArr["errCode"]        = $setPost["errCode"];
							$errMsg                      = "Successfully updated.";
							$extraArg["sharePostDetail"] = $setPost["errMsg"];
							$returnArr                   = setErrorStack($returnArr, -1, $errMsg, $extraArg);
						}

					} else {
						$returnArr["errCode"] = 51;
						$errMsg               = "Error in Featching User Details.";
						$returnArr            = setErrorStack($returnArr, 51, $errMsg, $extraArg);

						$xmlData["step3"]["data"]         = "get user info";
						$xmlData["userinfo"]["data"]      = "";
						$xmlData["userinfo"]["attribute"] = $getUserInfo;
					}
			} else {
				$returnArr["errCode"] = 52;
				$errMsg               = "Error in Post Field Details.";
				$returnArr            = setErrorStack($returnArr, 52, $errMsg, $extraArg);

				$xmlData["postFields"]["data"] = '. Error: '.$errMsg;
			}
		} else {

			$returnArr["errCode"] = 53;
			$errMsg               = "Please Login To System.";
			$returnArr            = setErrorStack($returnArr, 53, $errMsg, $extraArg);

			$xmlData["islogin"]["data"] = '. Error: '.$errMsg;
		}

		$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xmlData, $xmlArray["activity"]);

		echo json_encode($returnArr);
?>
