<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');

error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
	$email          = $_SESSION["email"];
  $userId         = $_SESSION["id"];
  $postId         = $_POST["postId"];
	$type           = $_POST["type"]; //print_r($email, $postId, $type);
	$postTime       = $_POST["postTime"];
	$postSharedType = $_POST["postSharedType"];
	$action = $_POST["action"];
	//call funcation to hide post
	$postStatus = hidePost($postId, $userId, $type, $postTime, $postSharedType, $action);

	if (!empty($postStatus)) {
		$returnArr['errCode'] = $postStatus['errCode'];
		$returnArr['errMsg']  = $postCount['errMsg'];
	} else {
		$returnArr["errCode"] = $postStatus['errCode'];
		$returnArr["errMsg"]  = $postCount['errMsg'];
	}
} else {
	$returnArr['errCode'] = 100;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
