<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
$extraArg = array();

		/****** log write section ******/
		//for xml writing essential
		$xmlProcessor = new xmlProcessor();
		$xmlfilename = "postCreation.xml";
		$xmlArray = initializeXMLLog($_SESSION["email"]);
		$xml_data['request']["data"] = '';
		$xml_data['request']["attribute"] = $xmlArray["request"];
		$xml_data['step1']["data"] = 'Post Creation Process';
		/****** partially end log section ******/
		global $cdnSocialUrl;
		error_reporting(0);
		if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
		{
				$xml_data["islogin"]["data"] = 'User Session started';
				$email = $_SESSION["email"];
				if(is_array($_POST) && sizeof($_POST) > 0){
				$xml_data["postFields"]["data"] = 'Success getting POST Data';

			$postId              = cleanXSS(trim(urldecode($_POST["postId"])));
			$created_at          = cleanXSS(trim(urldecode($_POST["created_at"])));
			$postType            = cleanXSS(trim(urldecode($_POST["postType"])));
			$reportText          = cleanXSS(trim(urldecode($_POST["report_text"])));

				if(empty($reportText))
				{
					$reportType="option";
					$flagSetForCommentBox = 1;
				}else{
					$reportType="other";
					if (isset($reportText) && !empty($reportText)) {
						$flagSetForCommentBox = 1;
					}else{
						$flagSetForCommentBox = 0;
					}
				}

				$user_id="user_id,account_handle";
				$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
				if(noError($getUserInfo)){
						$getUserInfo = $getUserInfo["errMsg"];
						$posted_by   = $getUserInfo["user_id"];
						$account_handle = $getUserInfo["account_handle"];
						$setPost = setReportInfo($posted_by,$postId, $reportType, rawurlencode($reportText), $reportId,$created_at,$postType);
								if(noError($setPost)){
									$getCMS = $setPost["errMsg"];
									$errMsg = "Successfully Added Report details";
									$extraArgs["errCode"] = -1;
	                $post_id= $setPost["errMsg"]["_id"];
	                $created_at= $setPost["errMsg"]["report_by"];

									$extraArgs["data"] = $setPost;
									$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
									$xml_data["RESPONSE"]["data"] = "";
									$xml_data["RESPONSE"]["attributes"] =$setPost;
								}
								else{
									$returnArr["errCode"] = 50;
									$errMsg = "Error in Adding Report Details.";
									$returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);

									$xml_data["RESPONSE"]["data"] = "";
									$xml_data["RESPONSE"]["attributes"] =$setPost;
								}
					}else{
						$returnArr["errCode"] = 51;
						$errMsg = "Error in Featching User Details.";
						$returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);
						$xml_data["step3"]["data"] = "get user info";
						$xml_data["userinfo"]["data"] = "";
						$xml_data["userinfo"]["attribute"] = $getUserInfo;
					}
			}
			else{
				$returnArr["errCode"] = 52;
				$errMsg = "Error in Post Field Details.";
				$returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
				$xml_data["postFields"]["data"] = '. Error: '.$errMsg;
			}
		}else{
				$errCode = 100;
				$returnArr["errCode"] = $errCode;
				$errMsg = "Please Login To System.";
				$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArg);
				$xml_data["islogin"]["data"] = '. Error: '.$errMsg;
		}
		$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
		echo json_encode($returnArr);

?>
