<?php

session_start();

/* Add DB Management Files*/
require_once ('../../config/config.php');

/* Add Helpers */
require_once  ('../../helpers/arrayHelper.php');
require_once  ('../../helpers/coreFunctions.php');
require_once  ('../../helpers/errorMap.php');
require_once  ('../../helpers/stringHelper.php');
require_once  ('../../helpers/imageFunctions.php');

/*xml include files*/
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

/*Add Models*/
require_once ('../../models/social/socialModel.php');
require_once ('../../models/social/commonFunction.php');
require_once "../../models/keywords/acceptBidModel.php";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "LikeExistingPost.xml";
$xmlArray = initializeXMLLog($_SESSION["email"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
$xml_data['step1']["data"] = 'Post Json File Creation Process';
/****** partially end log section ******/

error_reporting(0);

$returnArr = array();
$extraArgs = array();
if(isset($_SESSION['email']) && !empty($_SESSION['email'])) {
    $xml_data["islogin"]["data"] = 'User Session started';
    //Post field details
    if (is_array($_POST) && sizeof($_POST) > 0) {
        $xml_data["postFields"]["data"] = 'Success getting POST Data';
        $userId              = $_SESSION['id'];
        $account_handle      = $_SESSION['account_handle'];
//        $likeTime             = date("d-m-Y H:i:s");
        $likeTime             = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s"));
        $likeTimeInMilisecond = strtotime($likeTime) * 1000 ;
        $postId              = cleanXSS(trim(urldecode($_POST["post_id"])));
        $postTime            = cleanXSS(trim(urldecode($_POST["created_at"])));
        $type                = cleanXSS(trim(urldecode($_POST["type"])));
        $postEmailId         = cleanXSS(trim(urldecode($_POST["email"])));

        $xml_data["postAttribute"]["data"]      = "";
        $xml_data["postAttribute"]["attribute"] = $_POST;

        $data = array(
            $type  => array(0 => array(
                'post_id'    => $postId,
                'created_at' => $postTime,
                'action_at'    => $likeTimeInMilisecond
            )),
        );
        //update like post activity count to node database
        $activityCount = updatePostActivityCount($userId, $postId, 'like', $postTime, $type);
        if (noError($activityCount)) {
            // To send notification to all pot related user
            $sendFrom['id']             = $_SESSION['id'];
            $sendFrom['email']          = $_SESSION['email'];
            $sendFrom['account_handle'] = $_SESSION['account_handle'];
            $sendFrom['first_name']     = $_SESSION['first_name'];
            $sendFrom['last_name']      = $_SESSION['last_name'];
            $setNotification = createNotificationForSocial($postId, $postTime, 'like', $sendFrom, '', $postEmailId);
            // printArr($setNotification);
            $errMsg    = 'Success : Like post count updated';
            $extraArgs['activity'] = $activityCount['errMsg'];
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

            $xml_data["RESPONSE"]["data"]       = "";
            $xml_data["RESPONSE"]["attributes"] = $activityCount;

            $likePostJson = createLikeJsonFile($postId, $type, $account_handle, $data);
            if(noError($likePostJson)){
                
                $errMsg                             = 'Success : Like post json file created';
                $returnArr                          = setErrorStack($returnArr,  -1, $errMsg, $extraArgs);

                $xml_data["RESPONSE"]["data"]       = "";
                $xml_data["RESPONSE"]["attributes"] = $likePostJson;
            }else{
                $errMsg                             = 'Updated like post json file content';
                $returnArr                          = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

                $xml_data["RESPONSE"]["data"]       = "";
                $xml_data["RESPONSE"]["attributes"] = $likePostJson;
            }
        } else {
            $errCode   = $activityCount['errCode'];
            $errMsg    = $activityCount['errMsg'];
            $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);

            $xml_data["RESPONSE"]["data"]       = "";
            $xml_data["RESPONSE"]["attributes"] = $activityCount;

        }
    }else{
        $errMsg    = 'Error in fetching post field deatails';
        $returnArr = setErrorStack($returnArr, '52', $errMsg, $extraArgs);
        $xml_data["postFields"]["data"] = '. Error: '.$errMsg;
    }
}else{
    $errMsg    = 'Please login into system';
    $returnArr = setErrorStack($returnArr, '53', $errMsg, $extraArgs);
    $xml_data["islogin"]["data"] = '. Error: '.$errMsg;
}

$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
echo json_encode($returnArr);



?>