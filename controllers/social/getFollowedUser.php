<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');

error_reporting(0);
$returnArr = array();
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    	$emailId = urldecode($_POST["emailId"]);
    	//call funcation to remove post
      $blockFlag = false;
      $blockFlagViewedUser = false;
      $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
      $user_id="user_id,account_handle";
      $getUserInfo = getUserInfo($emailId,$walletURLIPnotification.'api/notify/v2/',$user_id);
      if(noError($getUserInfo)){
        $getUserInfo                              = $getUserInfo["errMsg"];
        $accountHandlerId                         = $getUserInfo["user_id"];
        $accountHandle                            = $getUserInfo["account_handle"];

          $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
          $userIdsOfAccHandler     = getFollowUser($targetDirAccountHandler);
          if (in_array($accountHandlerId, $userIdsOfAccHandler)) {
              $blockFlagViewedUser = true;
          }

      	if (($blockFlagViewedUser==1)) {
          $returnArr['errCode'] = "-1";
      		$returnArr['errMsg']  = "Allredy Follow list.";
      	} else {
          $returnArr['errCode'] = "50";
          $returnArr['errMsg']  = "Not In Followed List list.";
      	}
    }
} else {
	$returnArr['errCode'] = 100;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
