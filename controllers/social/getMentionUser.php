<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once ("../../models/social/commonFunction.php");

error_reporting(0);
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
  $search                  = $_POST["lastChar"];
  $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
  $blockedUser             = getBlockedUser($targetDirAccountHandler); //print_r(json_encode($blockedUser)); die;
  $getMentionUser          = getMentionUser(json_encode($blockedUser), $search); //print_r($getMentionUser["errMsg"]); die;
  if (!empty($getMentionUser)) {
  	foreach ($getMentionUser["errMsg"] as $key => $data) {
      if (isset($data["profile_pic"]) && !empty($data["profile_pic"])) {
          global $cdnSocialUrl;
          global $rootUrlImages;
          $extensionUP  = pathinfo($data["profile_pic"], PATHINFO_EXTENSION);
          //CDN image path
          $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data["account_handle"] . '/profile/' . $data["account_handle"] . '_' . $data["profile_pic"] . '.40x40.' . $extensionUP;

          //server image path
          $imageFileOfLocalUP = $rootUrlImages . 'social/users/' . $data["account_handle"] . '/profile/' . $data["account_handle"] . '_' . $data["profile_pic"];

          // check for image is available on CDN
          $file         = $imageFileOfCDNUP;
          $file_headers = @get_headers($file);
          if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
              $imgSrc = $imageFileOfLocalUP;
          } else {
              $imgSrc = $imageFileOfCDNUP;
          }
      } else {
          $imgSrc = $rootUrlImages."default_profile.jpg";
      }
      array_push($getMentionUser["errMsg"][$key]["imgSrc"]);
      $getMentionUser["errMsg"][$key]["imgSrc"] = $imgSrc;
    }

	  $returnArr["errCode"] = $getMentionUser["errCode"];
	  $returnArr["errMsg"]  = $getMentionUser["errMsg"];
  } else {
	  $returnArr["errCode"] = $getMentionUser["errCode"];
	  $returnArr["errMsg"]  = $getMentionUser["errMsg"];
	}
} else {
	$returnArr['errCode'] = 100;
	$returnArr['errMsg']  = 'Error in getting user';
}
//print_r($returnArr);die;
echo json_encode($returnArr);
?>
