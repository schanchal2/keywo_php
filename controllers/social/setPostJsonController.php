<?php

/**
 **********************************************************************************
 *                  setPostJsonController.php
 * ********************************************************************************
 *      This controller is used for Setting coin distribution process for view post, share post and tag keywords
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
ini_set('display_errors', 1);
header('Content-type: text/html; charset=utf-8');
session_start();


/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";

/* Add Model Files */
require_once "../../models/social/commonFunction.php";
require_once "../../models/social/socialModel.php";

/*xml include files*/
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
/* Add helper files*/
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/imageFunctions.php";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "postJsonFile.xml";
$xmlArray = initializeXMLLog($_SESSION["email"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
$xml_data['step1']["data"] = 'Post Json File Creation Process';

/****** partially end log section ******/
$extraArg = array();

error_reporting(0);

if(isset($_SESSION['email']) && !empty($_SESSION['email'])){
    $xml_data["islogin"]["data"] = 'User Session started';
    $email                       = $_SESSION['email'];
    $user_id                     = $_SESSION['id'];
    $account_handle                     = $_SESSION['account_handle'];

    if(is_array($_POST) && sizeof($_POST) > 0) {
        $xml_data["postFields"]["data"] = 'Success getting POST Data';
        $returnArr                      = array();
        $post_collection_name           ='posts';

            $post_id                  = cleanXSS(trim(urldecode($_POST["content_id"])));
            $posted_by                = cleanXSS(trim(urldecode($_POST["posted_by"])));
            $shared_by                = cleanXSS(trim(urldecode($_POST["shared_by"])));
            $postTime                 = cleanXSS(trim(urldecode($_POST["created_at"])));
            $post_collection_name     = cleanXSS(trim(urldecode($_POST["post_collection_name"])));
            $trans_id                 = cleanXSS(trim(urldecode($_POST["transaction_id"])));
            $trans_type               = cleanXSS(trim(urldecode($_POST["transaction_type"])));
            $post_type                = cleanXSS(trim(urldecode($_POST["post_type"])));
            $current_payout           = cleanXSS(trim(urldecode($_POST["current_payout"])));
//            $likeTimeAfterCDP         = date("d-m-Y H:i:s");
            $likeTimeAfterCDP         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s"));
            $afterCDPTimeInMilisecond = strtotime($likeTimeAfterCDP) * 1000 ;

            $xml_data["postAttribute"]["data"]      = "";
            $xml_data["postAttribute"]["attribute"] = $_POST;

            $data = array(0 => array(
                'user_id' => $user_id,
                'post_id' => $post_id,
                'created_at' => $postTime,
                'action_at' => $afterCDPTimeInMilisecond,
                'post_collection_name' => $post_collection_name,
                'transaction_id' => $trans_id,
                'transaction_type' => $trans_type,
                'post_type' => $post_type,
                'current_payout' => $current_payout)
            );

            //user(viewer) views creator post
            if (isset($user_id) && !empty($user_id) && isset($post_id) && !empty($post_id) && isset($posted_by) && !empty($posted_by)) {
                  //Call Json file creation function
                $postJson = createJsonFile($post_id, $account_handle, $data);
                if(noError($postJson)){
                    $errMsg                             = 'Success : Post json file created successfully';
                    $returnArr                          = setErrorStack($returnArr,  -1, $errMsg, $extraArg);
                    $xml_data["RESPONSE"]["data"]       = "";
                    $xml_data["RESPONSE"]["attributes"] = $postJson;
                }else{
                    $errMsg                             = 'Updated json files content';
                    $returnArr                          = setErrorStack($returnArr, 3, $errMsg, $extraArg);
                    $xml_data["RESPONSE"]["data"]       = "";
                    $xml_data["RESPONSE"]["attributes"] = $postJson;
                }
            }
            /*user (viewer) views sharer post
              Sharer shares previous sharer  post and viewer views shared post */
            else if (isset($user_id) && !empty($user_id) && isset($post_id) && !empty($post_id) && (isset($posted_by) && !empty($posted_by) || isset($shared_by) && !empty($shared_by))) {
                //Call Json file creation function
               $postJson = createJsonFile($post_id, $account_handle, $data);
                if(noError($postJson)){
                    $errMsg                             = 'Success : Post json file created successfully';
                    $returnArr                          = setErrorStack($returnArr,  -1, $errMsg, $extraArg);
                    $xml_data["RESPONSE"]["data"]       = "";
                    $xml_data["RESPONSE"]["attributes"] = $postJson;
                }else{
                    $errMsg                             = 'Updated json files content';
                    $returnArr                          = setErrorStack($returnArr, 3, $errMsg, $extraArg);
                    $xml_data["RESPONSE"]["data"]       = "";
                    $xml_data["RESPONSE"]["attributes"] = $postJson;
                }
            }
  } else{
        $returnArr["errCode"]           = 52;
        $errMsg                         = "Error in Post Field Details.";
        $returnArr                      = setErrorStack($returnArr, 52, $errMsg, $extraArg);
        $xml_data["postFields"]["data"] = '. Error: '.$errMsg;
  }
}else{
    $returnArr["errCode"]        = 53;
    $errMsg                      = "Please Login To System.";
    $returnArr                   = setErrorStack($returnArr, 53, $errMsg, $extraArg);
    $xml_data["islogin"]["data"] = '. Error: '.$errMsg;
}
$xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
echo json_encode($returnArr);

?>
