<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/app/appModel.php');

error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$email = $_SESSION["email"];
	//get user post count & update memcache status
	$postFields = "live_new_post_count,update_memcache,post_count";
	$postCount  = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $postFields);
	//print_r($postCount['errMsg']); //die;
	if (!empty($postCount) && $postCount['errMsg']['live_new_post_count'] > 0 && is_int($postCount['errMsg']['live_new_post_count'])) {
		$returnArr['value']     = $postCount['errMsg']['live_new_post_count'];
		$returnArr['errCode']   = -1;
		$returnArr['errMsg']    = "View " . $postCount['errMsg']['live_new_post_count'] . " new post";
		$returnArr['postCount'] = $postCount['errMsg']['post_count'];
	} elseif (!empty($postCount) && ($postCount['errMsg']['update_memcache'] == 'true')) {
		$returnArr['value']     = $postCount['errMsg']['update_memcache'];
		$returnArr['errCode']   = -1;
		$returnArr['errMsg']    = $postCount['errMsg']['update_memcache'];
		$returnArr['postCount'] = $postCount['errMsg']['post_count'];
	} else {
		$returnArr['value']     = 0;
		$returnArr["errCode"]   = 5;
		$returnArr["errMsg"]    = 'No new post';
		$returnArr['postCount'] = $postCount['errMsg']['post_count'];
	}
}else{
	$returnArr['value']   = 0;
	$returnArr['errCode'] = 2;
	$returnArr['errMsg']  = 'Error in getting user';
}
echo json_encode($returnArr);
?>
