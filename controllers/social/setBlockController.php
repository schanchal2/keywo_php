<?php
    /**
     **********************************************************************************
     *                  setJsonController.php
     * ********************************************************************************
     *      This controller is used for Setting Json File For followers and followings Details to social
     */

    /* Add Seesion Management Files */
      session_start();
    header("Access-Control-Allow-Origin: *");
    ini_set('default_charset','utf-8');
    header('Content-type: text/html; charset=utf-8');
    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../models/user/authenticationModel.php";
    require_once "../../models/analytics/userRegistration_analytics.php";
    require_once('../../helpers/deviceHelper.php');
    require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/coreFunctions.php";
    require_once "../../helpers/imageFunctions.php";
    require_once "../../models/social/commonFunction.php";
    require_once "../../models/social/socialModel.php";

    $extraArg = array();
    $_POST["otherAccountHandleType"]="blocked";
    // $emailOfOtherPerson='c@grr.la';
    $emailOfOtherPerson=$_POST["user_email"];



        $xmlProcessor = new xmlProcessor();
        $xmlfilename = "BlockCreation.xml";
        $xmlArray = initializeXMLLog($_SESSION["email"]);
        $xml_data['request']["data"] = '';
        $xml_data['request']["attribute"] = $xmlArray["request"];
        $xml_data['step1']["data"] = 'Json Creation Process';


        error_reporting(0);

          if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
          {
            $email = $_SESSION["email"];
            $xml_data["islogin"]["data"] = 'User Session started';
            if(is_array($_POST) && sizeof($_POST) > 0)
            {
                $user_id="user_id,account_handle";
                $getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$user_id);
                if(noError($getUserInfo)){
                $getUserInfo                              = $getUserInfo["errMsg"];
                $accountHandlerId                         = $getUserInfo["user_id"];
                $accountHandle                            = $getUserInfo["account_handle"];

                $xml_data["step2"]["data"] = "get user info Of Account Handler";
                $xml_data["userinfo"]["data"] = "";
                $xml_data["userinfo"]["attribute"] = $getUserInfo;

                $getUserInfoForOtherPerson = getUserInfo($emailOfOtherPerson,$walletURLIPnotification.'api/notify/v2/',$user_id);
                if(noError($getUserInfoForOtherPerson)){
                $getUserInfoForOtherPerson                 = $getUserInfoForOtherPerson["errMsg"];
                $otherAccountHandlerId                     = $getUserInfoForOtherPerson["user_id"];
                $anotherAccountHandle                      = $getUserInfoForOtherPerson["account_handle"];
                $xml_data["step3"]["data"] = "get user info Of Another Account Handler";
                $xml_data["userinfoOFAnotherAccountHandler"]["data"] = "";
                $xml_data["userinfoOFAnotherAccountHandler"]["attribute"] = $getUserInfoForOtherPerson;
                }
                //printArr($getUserInfoForOtherPerson);

                $accountHandleType                         = cleanXSS(trim(urldecode($_POST["accountHandleType"])));
                $otherAccountHandleType                    = cleanXSS(trim(urldecode($_POST["otherAccountHandleType"])));


                $targetDirAccountHandler                   = "../../json_directory/social/followerwonk/".$accountHandle."/";
                $targetDirOtherAccountHandler              = "../../json_directory/social/followerwonk/".$anotherAccountHandle."/";

                $folderExist                               = folderPresenceCheck($targetDirAccountHandler);
                $folderExist1                              = folderPresenceCheck($targetDirOtherAccountHandler);

                // $jsonFileCreationOfOtherAccountHandle      = strtolower($anotherAccountHandle[0]);
                // $jsonFileCreationOfAccountHandle           = strtolower($accountHandle[0]);



                $jsonFileCreationOfOtherAccountHandleWithFirstChar      = strtolower($anotherAccountHandle[0]);
                $jsonFileCreationOfAccountHandleWithFirstChar           = strtolower($accountHandle[0]);

                if (has_specchar($jsonFileCreationOfOtherAccountHandleWithFirstChar)) {
                    $jsonFileCreationOfOtherAccountHandle="0";
                }
                else {
                    $jsonFileCreationOfOtherAccountHandle      = strtolower($anotherAccountHandle[0]);
                }

                if (has_specchar($jsonFileCreationOfAccountHandleWithFirstChar)) {
                    $jsonFileCreationOfAccountHandle="0";
                }
                else {
                    $jsonFileCreationOfAccountHandle      = strtolower($accountHandle[0]);
                }



                $returnArr                                 = array();

                function getAccountHandlerData()
                {
                  $formdata = array(
                  'followings'=>[],
                  'followers'=> [],
                  'blocked'=>[]
                  );
                  return json_encode($formdata,JSON_PRETTY_PRINT);
                }

                function getAccountHandlerBlockedData()
                {
                  $formdata = array(
                  'blocked'=>[]
                  );
                  return json_encode($formdata,JSON_PRETTY_PRINT);
                }

                $accountHandlerFileName      = $targetDirAccountHandler.$accountHandle."_info_".$jsonFileCreationOfOtherAccountHandle.".json";
                if(!file_exists($accountHandlerFileName))
                {
                  if(file_put_contents($accountHandlerFileName, getAccountHandlerData()))
                  {
                    $setJson =updationOfJsonBlockFile($accountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId);
                    if(noError($setJson)){
                      $getCMS = $setJson["errMsg"];
                      $errMsg = "Successfully Added Post details";
                      // $extraArgs["errCode"] = -1;
                      // $extraArgs["data"] = $setJson;
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                      $xml_data["step4"]["data"] = "Json File Creation Of Account Handler";
                      $xml_data["userinfo"]["data"] = "";
                      $xml_data["userinfo"]["attribute"] = $setJson;

                      $files2 = dirToArray($targetDirAccountHandler);
                      foreach($files2 as $key => $files2Details)
                      {
                        $Final= $targetDirAccountHandler.$files2Details;
                        $jsondata = file_get_contents($Final);
                        $data = json_decode($jsondata, true);
                        $countOfBlockedJson = count($data["blocked"]);
                        $countOfBlockedOfAllJson = $countOfBlockedJson + $countOfBlockedOfAllJson;
                      }
                      $BlockCount = $countOfBlockedOfAllJson;
                      $notifyFieldType = 'block';
                      $setBlock = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $BlockCount);
                      if(noError($setBlock)){
                        $xml_data["step5"]["data"] = "Block User Count Updation at Api Side";
                        $xml_data["userinfo"]["data"] = "";
                        $xml_data["userinfo"]["attribute"] = $setJson;
                        $removeBlockedUserPost = removePostOfBlockedUser($accountHandlerId, $otherAccountHandlerId);
                          if(noError($removeBlockedUserPost)){
                            $xml_data["step6"]["data"] = "Removed Data From User Time Line";
                            $xml_data["userinfo"]["data"] = "";
                            $xml_data["userinfo"]["attribute"] = $removeBlockedUserPost;
                          }else{
                            $returnArr["errCode"] = 50;
                            $errMsg = "Error In removing Post Details Of Bloked user.";
                            $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                          }
                      }else{
                        $returnArr["errCode"] = 50;
                        $errMsg = "Error in Seeting blocked Count.";
                        $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                      }
                    }else{
                      $returnArr["errCode"] = 50;
                      $errMsg = "Error in Adding Post Details.";
                      $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }
                  }
                  else
                  {
                    $returnArr["errCode"] = 52;
                    $errMsg = "Error in Creating Json File For Handler.";
                    $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
                  }
                }
                else
                {
                    $setJson =updationOfJsonBlockFile($accountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId );
                    if(noError($setJson)){
                      $getCMS = $setJson["errMsg"];
                      $errMsg = "Successfully Added Json details";
                      // $extraArgs["errCode"] = -1;
                      // $extraArgs["data"] = $setJson;
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, NULL);
                      $xml_data["step4"]["data"] = "Json File Updation Of Account Handler";
                      $xml_data["userinfo"]["data"] = "";
                      $xml_data["userinfo"]["attribute"] = $setJson;

                      $files2 = dirToArray($targetDirAccountHandler);
                      foreach($files2 as $key => $files2Details)
                      {
                        $Final= $targetDirAccountHandler.$files2Details;
                        $jsondata = file_get_contents($Final);
                        $data = json_decode($jsondata, true);
                        $countOfBlockedJson = count($data["blocked"]);
                        $countOfBlockedOfAllJson = $countOfBlockedJson + $countOfBlockedOfAllJson;
                      }
                      $BlockCount = $countOfBlockedOfAllJson;
                      $notifyFieldType = 'block';
                      $setBlock = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $BlockCount);
                      if(noError($setBlock)){
                        $xml_data["step5"]["data"] = "Block User Count Updation at Api Side";
                        $xml_data["userinfo"]["data"] = "";
                        $xml_data["userinfo"]["attribute"] = $setJson;
                        $removeBlockedUserPost = removePostOfBlockedUser($accountHandlerId, $otherAccountHandlerId);
                          if(noError($removeBlockedUserPost)){
                            $xml_data["step6"]["data"] = "Removed Data From User Time Line";
                            $xml_data["userinfo"]["data"] = "";
                            $xml_data["userinfo"]["attribute"] = $removeBlockedUserPost;

                            $files2 = dirToArray($targetDirAccountHandler);
                            foreach($files2 as $key => $files2Details)
                            {
                              $Final= $targetDirAccountHandler.$files2Details;
                              $jsondata = file_get_contents($Final);
                              $data = json_decode($jsondata, true);
                              $countOfFollowingJson = count($data["followings"]);
                              $countOfFollowingOfAllJson = $countOfFollowingJson + $countOfFollowingOfAllJson;
                            }
                            $followingCount = $countOfFollowingOfAllJson;
                            $notifyFieldType = 'followings';
                            $setFollowing = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $followingCount);


                            $files2 = dirToArray($targetDirAccountHandler);
                            foreach($files2 as $key => $files2Details)
                            {
                              $Final= $targetDirAccountHandler.$files2Details;
                              $jsondata = file_get_contents($Final);
                              $data = json_decode($jsondata, true);
                              $countOfFollowersJson = count($data["followers"]);
                              $countOfFollowersOfAllJson = $countOfFollowersJson + $countOfFollowersOfAllJson;
                            }
                            $followerCount = $countOfFollowersOfAllJson;
                            $notifyFieldType = 'followers';
                            $setfollowers = setUserNotifyInfo($accountHandlerId, $notifyFieldType, $followerCount);

                          }else{
                            $returnArr["errCode"] = 50;
                            $errMsg = "Error In removing Post Details Of Bloked user.";
                            $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                          }
                      }else{
                        $returnArr["errCode"] = 50;
                        $errMsg = "Error in Seeting blocked Count.";
                        $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                      }
                    }else{
                      $returnArr["errCode"] = 50;
                      $errMsg = "Error in Adding Post Details.";
                      $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }
                }


                ///////////////----------------start ------------------------------
                $otherAccountHandlerFileNameForBlockedUser      = $targetDirOtherAccountHandler.$anotherAccountHandle."_info_blockList.json";

                if(!file_exists($otherAccountHandlerFileNameForBlockedUser))
                {
                  if(file_put_contents($otherAccountHandlerFileNameForBlockedUser, getAccountHandlerBlockedData()))
                  {
                      $setJson = updationOfJsonBlockFile($otherAccountHandlerFileNameForBlockedUser,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $accountHandlerId );
                    if(noError($setJson)){
                      $getCMS = $setJson["errMsg"];
                      $errMsg = "Successfully Added Json details";
                      // $extraArgs["errCode"] = -1;
                      // $extraArgs["data"] = $setJson;
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                    }else{
                      $returnArr["errCode"] = 50;
                      $errMsg = "Error in Adding Block 1 Details.";
                      $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }
                  }
                  else
                  {
                    $returnArr["errCode"] = 52;
                    $errMsg = "Error in Creating Json File For Other Handler.";
                    $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
                  }
                }
                else
                {
                  $setJson = updationOfJsonBlockFile($otherAccountHandlerFileNameForBlockedUser,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $accountHandlerId );
                  if(noError($setJson)){
                    $getCMS = $setJson["errMsg"];
                    $errMsg = "Successfully Added Block2 details";
                    // $extraArgs["errCode"] = -1;
                    // $extraArgs["data"] = $setJson;
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                  }else{
                    $returnArr["errCode"] = 50;
                    $errMsg = "Error in Adding Blocl2 Details.";
                    $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                  }
                }


                //// ------------------list End--------------------

                $otherAccountHandlerFileName = $targetDirOtherAccountHandler.$anotherAccountHandle."_info_".$jsonFileCreationOfAccountHandle.".json";
                if(!file_exists($otherAccountHandlerFileName))
                {
                  if(file_put_contents($otherAccountHandlerFileName, getAccountHandlerData()))
                  {

                    $setJson = updationOfJsonFileForBlockedOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId );
                    if(noError($setJson)){
                      $getCMS = $setJson["errMsg"];
                      $errMsg = "Successfully Added Json details";
                      // $extraArgs["errCode"] = -1;
                      // $extraArgs["data"] = $setJson;
                      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

                    $xml_data["step7"]["data"] = "Json File Creation Of Another Account Handler";
                    $xml_data["userinfoOFAnotherAccountHandler"]["data"] = "";
                    $xml_data["userinfoOFAnotherAccountHandler"]["attribute"] = $setJson;

                    }else{
                      $returnArr["errCode"] = 50;
                      $errMsg = "Error in Adding Block 1 Details.";
                      $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }
                  }
                  else
                  {
                    $returnArr["errCode"] = 52;
                    $errMsg = "Error in Creating Json File For Other Handler.";
                    $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);
                  }
                }
                else
                {
                  $setJson =updationOfJsonFileForBlockedOtherAccountHandle($otherAccountHandlerFileName,$accountHandleType , $accountHandlerId , $otherAccountHandleType, $otherAccountHandlerId );
                  if(noError($setJson)){
                    $getCMS = $setJson["errMsg"];
                    $errMsg = "Successfully Added Block2 details";
                    // $extraArgs["errCode"] = -1;
                    // $extraArgs["data"] = $setJson;
                    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

                    $xml_data["step7"]["data"] = "Json File Creation Of Another Account Handler";
                    $xml_data["userinfoOFAnotherAccountHandler"]["data"] = "";
                    $xml_data["userinfoOFAnotherAccountHandler"]["attribute"] = $setJson;

                    $files2 = dirToArray($targetDirOtherAccountHandler);
                    foreach($files2 as $key => $files2Details)
                    {
                      $Final= $targetDirOtherAccountHandler.$files2Details;
                      $jsondata = file_get_contents($Final);
                      $data = json_decode($jsondata, true);
                      $countOfFollowerJson = count($data["followers"]);
                      $countOfFollowerOfAllJson = $countOfFollowerJson + $countOfFollowerOfAllJson;
                    }
                     $followerCount = $countOfFollowerOfAllJson;
                    $notifyFieldType = 'followers';
                    $setFollowing = setUserNotifyInfo($otherAccountHandlerId, $notifyFieldType, $followerCount);


                    $files2 = dirToArray($targetDirOtherAccountHandler);
                    foreach($files2 as $key => $files2Details)
                    {
                      $Final= $targetDirOtherAccountHandler.$files2Details;
                      $jsondata = file_get_contents($Final);
                      $data = json_decode($jsondata, true);
                      $countOfFollowingsJson = count($data["followings"]);
                      $countOfFollowingsOfAllJson = $countOfFollowingsJson + $countOfFollowingsOfAllJson;
                    }
                     $followingsCount = $countOfFollowingsOfAllJson;
                    $notifyFieldType = 'followings';
                    $setFollowing = setUserNotifyInfo($otherAccountHandlerId, $notifyFieldType, $followingsCount);

                    if(noError($setJson)){
                      $xml_data["step8"]["data"] = "Block User Count Updation at Api Side";
                      $xml_data["userinfo"]["data"] = "";
                      $xml_data["userinfo"]["attribute"] = $setJson;
                      $removeBlockedUserPostOfOther = removePostOfBlockedUser($otherAccountHandlerId, $accountHandlerId);
                        if(noError($removeBlockedUserPostOfOther)){
                          $xml_data["step6"]["data"] = "Removed Data From Other User Time Line";
                          $xml_data["userinfo"]["data"] = "";
                          $xml_data["userinfo"]["attribute"] = $removeBlockedUserPost;
                        }else{
                          $returnArr["errCode"] = 50;
                          $errMsg = "Error In removing Post Details Of Bloked user.";
                          $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                        }
                    }else{
                      $returnArr["errCode"] = 50;
                      $errMsg = "Error in Seeting blocked Count.";
                      $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);
                    }

                  }else{
                    $returnArr["errCode"] = 50;
                    $errMsg = "Error in Adding Blocl2 Details.";
                    $returnArr =  setErrorStack($returnArr, 50, $errMsg, $extraArg);

                    $xml_data["RESPONSE"]["data"] = "";
                    $xml_data["RESPONSE"]["attributes"] =$errMsg;
                  }
                }
              }
              else{
    						$returnArr["errCode"] = 51;
    						$errMsg = "Error in Featching User Details.";
    						$returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);

                $xml_data["step2"]["data"] = "get user info";
                $xml_data["userinfo"]["data"] = "";
                $xml_data["userinfo"]["attribute"] = $getUserInfo;
    					}
            }
            else{
              $returnArr["errCode"] = 52;
              $errMsg = "Error in Post Field Details.";
              $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);

              $xml_data["postFields"]["data"] = '. Error: '.$errMsg;
            }
          }
          else{
              $errCode = 100;
              $returnArr["errCode"] = $errCode;
              $errMsg = "Please Login To System.";
              $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArg);
              $xml_data["islogin"]["data"] = '. Error: '.$errMsg;
          }

          $xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
          echo json_encode($returnArr);
              ?>
