<?php
    /**
     **********************************************************************************
     *                  setBookmarkController.php
     * ********************************************************************************
     *      This controller is used for Setting Json File to store bookmark Details Details to social
     */

    /* Add Seesion Management Files */
      session_start();
    header("Access-Control-Allow-Origin: *");
    ini_set('default_charset','utf-8');
    header('Content-type: text/html; charset=utf-8');
    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../models/user/authenticationModel.php";
    require_once "../../models/analytics/userRegistration_analytics.php";
    require_once('../../helpers/deviceHelper.php');
    require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/coreFunctions.php";
    require_once "../../helpers/imageFunctions.php";
    require_once ('../../models/social/socialModel.php');
    require_once ('../../models/social/commonFunction.php');
    $extraArg = array();

        $xmlProcessor = new xmlProcessor();
        $xmlfilename = "JsonCreationForBookmark.xml";
        $xmlArray = initializeXMLLog($_SESSION["email"]);
        $xml_data['request']["data"] = '';
        $xml_data['request']["attribute"] = $xmlArray["request"];
        $xml_data['step1']["data"] = 'Json Creation Process';
        $_POST["type"]="followedkeyword";
        error_reporting(0);
          if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
              if ($_SESSION["userFteStatusFlag"] != 0) {
                  $email = $_SESSION["email"];
                  $xml_data["islogin"]["data"] = 'User Session started';
                  if (is_array($_POST) && sizeof($_POST) > 0) {
//                $bookmarkTime             = date("d-m-Y H:i:s");
                      $bookmarkTime = uDateTime("d-m-Y H:i:s", date("d-m-Y H:i:s"));
                      $bookmarkTimeInMilisecond = strtotime($bookmarkTime) * 1000;
                      $type = cleanXSS(trim(urldecode($_POST["type"])));
                      $keyword = cleanXSS(trim(urldecode($_POST["keyword"])));

                      $user_id = "user_id,account_handle";
                      $getUserInfo = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $user_id);
                      // pri/ntArr($getUserInfo);
                      if (noError($getUserInfo)) {
                          $getUserInfo = $getUserInfo["errMsg"];
                          $accountHandlerId = $getUserInfo["user_id"];
                          $accountHandle = $getUserInfo["account_handle"];
                          $returnArr = array();
                          $data = array(
                              "followedkeyword" => array(0 => array(
                                  'keyword' => $keyword,
                                  'action_at' => $bookmarkTimeInMilisecond
                              )),
                          );


                          $xml_data["step2"]["data"] = "get user info Of Account Handler";
                          $xml_data["userinfo"]["data"] = "";
                          $xml_data["userinfo"]["attribute"] = $getUserInfo;

                          $targetDirAccountHandler = $docRoot . "json_directory/social/followerwonk/" . $accountHandle . "/";

                          $jsonFileCreationForKeyword = strtolower($keyword[0]);
                          if (has_specchar($jsonFileCreationForKeyword)) {
                              $jsonFileCreationForKeyword = "0";
                          } else {
                              $jsonFileCreationForKeyword = strtolower($keyword[0]);
                          }

                          // echo "<br>".$jsonFileCreationForKeyword;
                          // echo "<br>".$accountHandle;
                          // echo "<br>".$keyword ;
                          $finalAccHandle = $accountHandle . '_info_' . $jsonFileCreationForKeyword . '.json';
                          // die;
                          $bookmarkPostJson = createKeywordJsonFile($keyword, $type, $accountHandle, $data, $finalAccHandle);
                          // printArr($likePostJson);
                          if (noError($bookmarkPostJson)) {

                              $xml_data["step3"]["data"] = "Successfully added bookmark Details";
                              $xml_data["bookmarkPostJson"]["data"] = "";
                              $xml_data["bookmarkPostJson"]["attribute"] = $bookmarkPostJson;

                              $errMsg = 'Success : Bookmark post json file created';
                              $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                          } else {


                              $errMsg = 'Updated Bookmark post json file content';
                              $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

                              $xml_data["bookmarkPostJson"]["data"] = '. Error: ' . $errMsg;

                          }
                      } else {
                          $returnArr["errCode"] = 51;
                          $errMsg = "Error in Featching User Details.";
                          $returnArr = setErrorStack($returnArr, 51, $errMsg, $extraArg);

                          $xml_data["step2"]["data"] = "get user info";
                          $xml_data["userinfo"]["data"] = "";
                          $xml_data["userinfo"]["attribute"] = $getUserInfo;
                      }
                  } else {
                      $returnArr["errCode"] = 52;
                      $errMsg = "Error in Post Field Details.";
                      $returnArr = setErrorStack($returnArr, 52, $errMsg, $extraArg);

                      $xml_data["postFields"]["data"] = '. Error: ' . $errMsg;
                  }
              }else{
                  $returnArr['errCode'] = 120;
                  $returnArr['errMsg'] = 'Please Clear FTU';
              }
          }
          else{
              $errCode = 100;
              $returnArr["errCode"] = $errCode;
              $errMsg = "Please Login To System.";
              $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArg);
              $xml_data["islogin"]["data"] = '. Error: '.$errMsg;
          }

          $xmlProcessor->writeXML($xmlfilename, $logPath["social"], $xml_data, $xmlArray["activity"]);
          echo json_encode($returnArr);
              ?>
