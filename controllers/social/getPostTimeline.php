<?php
// function to get home time-line post for login user from "memcache"
// if memcache data is empty than get data from fallback api
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/social/socialModel.php');
require_once ("../../models/social/commonFunction.php");


error_reporting(0);

if (isset($_SESSION["id"]) && !empty($_SESSION["id"])) {

	$userId      = $_SESSION["id"];
	$postTime    = $_GET["lastPostTime"];
	$postType    = $_GET["type"];
	$keyword     = $_GET["keywo"];
    $searchText  = urldecode($_GET["searchText"]);

    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockedUser             = getBlockedUser($targetDirAccountHandler);
    // print_r(json_encode($blockedUser)); die;

	if (isset($keyword) && !empty($keyword)) {
		$postData = getUserHomeTimelinePost($userId, $postType, $postTime, $keyword, $searchText, json_encode($blockedUser));
		if ($postData["errCode"] == -1) {
			$resPost  = json_encode($postData["errMsg"]);
		}
	} elseif (isset($searchText) && !empty($searchText)) {
		$keyword  = "";
        $postData = getUserHomeTimelinePost($userId, $postType, $postTime, $keyword, $searchText, json_encode($blockedUser));
        if ($postData["errCode"] == -1) {
            $resPost  = json_encode($postData["errMsg"]);
        }
	} else {
		if (class_exists('Memcache')) {
			$memcache = new Memcached();
			$memcache->addServer($memcacheServerIP, $memcacheServerPort);
			$resPost = $memcache->get($userId); //print_r($resPost); die;
		} else {
			$resPost = NULL;
		}
		// if memcache is empty than get data from fallback api
		if ($resPost == NULL) {
			$keyword    = "";
			$searchText = "";
			$postData   = getUserHomeTimelinePost($userId, $postType, $postTime, $keyword, $searchText, json_encode($blockedUser));
			if ($postData["errCode"] == -1) {
				$resPost  = json_encode($postData["errMsg"]); //print_r($resPost); die;
			}
		}
		//set post data into Session
		$_SESSION["userPost"] = $resPost;
	}

	if ($nextPostData && !empty($postTime)) {
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $nextPostData;

	} elseif ($resPost) {
		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = $resPost;

	} else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"]  = 'Error in getting timeline post. Please refresh.';
	}

} else {
	$returnArr['errMsg']  = 'No user found.';
	$returnArr['errCode'] = 2;
}

echo json_encode($returnArr);

?>
