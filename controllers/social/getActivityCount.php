<?php	
	header("Access-Control-Allow-Origin: *");
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');
	// error_reporting(-1);
	session_start();
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once('../../helpers/deviceHelper.php');
	// require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/stringHelper.php";
	// require_once "../../helpers/imageFunctions.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";

	if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	   	$returnArray['errCode'] = 100;
  		$returnArray['errMsg'] = 'Please Login';
	} else {

		$emailId = $_SESSION['email'];
		$countArray = array();
		$getLikeCount = getLikePostIds($_SESSION['account_handle'], '');
		$countArray['likeCount'] = count($getLikeCount);

		$getFollowingCount = getFollowFollowerData($_SESSION['account_handle'],'followings','');
		$countArray['followPeople'] = count($getFollowingCount);

		$getBookmarkCount = getBookmarkPostIds($_SESSION['account_handle'], '');
		if ($getBookmarkCount['errCode'] == -1) {
			$getBookmarkCount = $getBookmarkCount['errMsg'];
			$countArray['bookmarkCount'] = count($getBookmarkCount);
		} else {
			$countArray['bookmarkCount'] = 'Error';
		}

		$getKeywordsCount = getFollowFollowerData($_SESSION['account_handle'],'followedkeyword','');
		$countArray['followKeywords'] = count($getKeywordsCount);

		$postFields         = "hide_post_count,comment_count";
	  	$ActivityDetails   = getUserInfo($emailId, $walletURLIPnotification . 'api/notify/v2/', $postFields);
	  	// printArr($ActivityDetails);
	  	if (noError($ActivityDetails)) {
	  		$ActivityDetails = $ActivityDetails['errMsg'];
	  		$countArray['hiddenPost'] = $ActivityDetails['hide_post_count'];
	  		$countArray['CommentPost'] = $ActivityDetails['comment_count'];
	  	} else {
	  		$countArray['hiddenPost'] = 'Error';
	  		$countArray['CommentPost'] = 'Error';
	  	}

	  	$returnArray['errCode'] = -1;
	  	$returnArray['errMsg'] = $countArray;
	}


	
	echo json_encode($returnArray);

?>