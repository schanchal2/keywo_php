<?php

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');

if(isset($_SESSION['email']) && !empty($_SESSION['email'])){

if(isset($_POST) && !empty($_POST)){
     global $docRoot;
     $postId          = $_POST['content_id'];
     $postType        = $_POST['post_type'];
     $postTime        = $_POST['created_at'];
     $email           = $_POST['email'];
     $postSharedBy    = 0;
     $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

   	$user_id="user_id,account_handle";
   	$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
   	if(noError($getUserInfo)){
   		$getUserInfo = $getUserInfo["errMsg"];
   		$user_id     = $getUserInfo["user_id"];
   		$accHandle   = $getUserInfo["account_handle"];
   	}
   	$getFollowingDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($accHandle[0]).".json";
   	$followFlag = getFollowUserValue($getFollowingDetails,$user_id,$_SESSION['id']);

    $postDetail =  getPostDetail($postId, $postTime,$_SESSION['id'],$followFlag);

    if (noError($postDetail)) {
        $postDetail              = $postDetail['errMsg'][0];
    } else {
        $errorCode         = $postDetail['errCode'];
    }

    if ($postType != "share") {
        $postParentId            = $postDetail['_id'];
        $postParentType          = $postDetail['post_type'];
        $postCreatedAt           = $postDetail['created_at'];
        $postAccHandle           = $postDetail['user_ref']['account_handle'];
        $postPostedBy            = $postDetail['posted_by'];
        $keywords                = $postDetail['keywords'];
        $keyword                 = implode(' ', $keywords);
        $_POST["share_status"]   = "";

    } elseif ($postType == "share") {
        $postParentId            = $postDetail['_id'];
        $postParentType          = $postDetail['post_type'];
        $postCreatedAt           = $postDetail['created_at'];
        $postAccHandle           = $postDetail['user_ref']['account_handle'];
        $postSharedBy            = $postDetail['posted_by'];
        $postShareDetails        = $postDetail['post_details']['parent_post']['post_id'];
        $postPostedBy            = $postShareDetails['posted_by'];
        $keywords                = $postShareDetails['keywords'];
        $keyword                 = implode(' ', $keywords);
        $_POST["share_status"]   = "share";
        $_POST["post_type"]      = $postDetail['post_details']['parent_post']['post_id']['post_type'];
    }


    if($postId == $postParentId && $postType == $postParentType && $postTime == $postCreatedAt  &&
        $_POST['account_handle'] == $postAccHandle && $_POST['posted_by'] == $postPostedBy   && $_POST['q'] == $keyword && $_POST['shared_by'] == $postSharedBy){

        //Check Like post details
        $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);

        if($getLikePostStatus != 1){
            $fieldsArray = $_POST;
            $url = "{$rootUrl}controllers/cdp/coinDistributionController.php";
            internalCurlRequestPost($url,count($fieldsArray),makePostUrlForCurl($fieldsArray));
        }else{
            $returnArr["errCode"]=52;
            $returnArr["errMsg"]="This post already liked by user.";
            echo json_encode($returnArr,true);

        }
    }else{
        echo 'Error : in matching fields';
    }
} else{
    $returnArr["errCode"]           = 52;
    $returnArr["errMsg"]            = "Error in Post Field Details.";
    echo json_encode($returnArr,true);
}
}else{
    $returnArr["errCode"]        = 53;
    $returnArr["errMsg"]         = "Please Login To System.";
    echo json_encode($returnArr,true);
}
?>
