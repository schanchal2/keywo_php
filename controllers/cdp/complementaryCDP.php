<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 14/6/17
 * Time: 11:08 AM
 */

/* Add Session Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";

/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once('../../helpers/deviceHelper.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../helpers/walletHelper.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/header/headerModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/cdp/cdpUtilities.php";
require_once "../../models/keywords/keywordCdpModel.php";
require_once "../../models/keywords/userCartModel.php";
require_once('../../models/landing/landingFeatureModel.php');

$extraArg = array();
$retArray = array();

$sid  = cleanXSS(trim(urldecode($_POST['sid'])));
$currSid = session_id();

$superUserEmail = 'dk@grr.la';

if(isset($_SESSION) && !empty($_SESSION['email'])){

    if($currSid == $sid){

        if(isset($_POST) && !empty($_POST)){

            $email                =   $_SESSION["email"]; // either search user or post viewer
            $userId               =   $_SESSION['id'];
            $content_id           =   cleanXSS(trim(urldecode($_POST["post_id"])));  // get app id if user search from search engine else user post view from social then get post id.
            $post_created_at      =   cleanXSS(trim(urldecode($_POST["created_at"])));
            $post_type            =   cleanXSS(trim(urldecode($_POST["post_type"])));
            $originIp             =   cleanXSS(trim(urldecode($_POST["origin_ip"])));
            $creatorId            =   cleanXSS(trim(urldecode($_POST["posted_by"])));
            $sharedId             =   cleanXSS(trim(urldecode($_POST["shared_by"])));
            $page                 =   cleanXSS(trim(urldecode($_POST["page"])));
            $nextPage             =   cleanXSS(trim(urldecode($_POST["next"])));

            // create keyword database connection
            $kwdDbConn = createDBConnection("dbkeywords");

            if (noError($kwdDbConn)) {
                $kwdDbConn = $kwdDbConn["connection"];

                /* Check user already create and share post */
                $getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$email,$_SESSION['id']);
                if(noError($getFtUserDetail)){
                    $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);

                    $column = '';
                    if($page == 'create'){
                        $column = 'post_status';
                    }else{
                        $column = 'share_post';
                    }

                    if($row[$column] == 0){
                        $postDetail  = getPostDetail($content_id, $post_created_at, $_SESSION['id'], 2);
                        $postDetail = $postDetail['errMsg'][0];

                        $listOfUser[0]['userEmail'] = $superUserEmail;
                        $listOfUser[0]['userId']    = 'eb84d4b6308f895afa0b1532ba0c1c72d0d7e35d';
                        $listOfUser[1]['userEmail'] = $keywoUser;
                        $listOfUser[1]['userId']    = $keywoUserId;

                        if($originIp == ''){
                            $originIp = getClientIP();
                        }

                        if ($post_type != "share") {
                            $postParentId            = $postDetail['_id'];
                            $postParentType          = $postDetail['post_type'];
                            $postCreatedAt           = $postDetail['created_at'];
                            $postAccHandle           = $postDetail['user_ref']['account_handle'];
                            $postPostedBy            = $postDetail['posted_by'];
                            $keywords                = $postDetail['keywords'];
                            $keyword                 = implode(' ', $keywords);
                            $keyword                 = strtolower($keyword);

                            $listOfUser[2]['userEmail'] = $_SESSION['email'];
                            $listOfUser[2]['userId'] = $postPostedBy;

                        } elseif ($post_type == "share") {
                            $postParentId            = $postDetail['_id'];
                            $postParentType          = $postDetail['post_type'];
                            $postCreatedAt           = $postDetail['created_at'];
                            $postAccHandle           = $postDetail['user_ref']['account_handle'];
                            $postSharedBy            = $postDetail['posted_by']; //post sharer user id
                            $postShareDetails        = $postDetail['post_details']['parent_post']['post_id'];
                            $postCreatorEmail        = $postShareDetails['user_ref']['email']; //post creator email
                            $postPostedBy            = $postShareDetails['posted_by'];
                            $keywords                = $postShareDetails['keywords'];
                            $keyword                 = implode(' ', $keywords);
                            $keyword                 = strtolower($keyword);

                            $listOfUser[2]['userEmail'] = $postCreatorEmail; // post creator email
                            $listOfUser[2]['userId'] = $postPostedBy;  // //post creator id
                            $listOfUser[3]['userEmail'] = $_SESSION['email']; //post sharer email
                            $listOfUser[3]['userId'] = $postSharedBy;  //post sharer id

                        }

                        // create search database connection
                        $searchDbConn = createDBConnection("dbsearch");
                        if(noError($searchDbConn)) {
                            $searchDbConn = $searchDbConn["connection"];

                            // get max ip limit from admin setting table
                            $getAdminSetting = getSettingsFromSearchAdmin($searchDbConn);
                            if(noError($getAdminSetting)) {
                                $getAdminSetting = $getAdminSetting["data"];
                                $ipMaxLimit = $getAdminSetting["IPMaxLimit"];

                                // get currency exchange rate of usd, sgd and btc
                                $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
                                if (noError($getExchangeRate)) {
                                    $getExchangeRate = $getExchangeRate["exchange_rate"];
                                    $getExchangeRate = json_encode($getExchangeRate);

                                    // 1. Get current payout amount from payout.json file.
                                    $currentPayout = getCurrentPayout();
                                    if(noError($currentPayout)) {
                                        $currentPayout = $currentPayout["current_payout"];
                                        $currentPayout = number_format(($currentPayout / 4), 8);

                                        for($i = 0; $i<=1; $i++){

                                            if($i == 0){
                                                $transType = 'post_viewer_earning';
                                            }else if($i == 1){
                                                $transType = 'keywo_content_view_earning';
                                            }

                                            // array that contains function parameters
                                            $cdpGlobalParam = array(
                                                "user_id" => $listOfUser[$i]['userId'],
                                                "email" => $listOfUser[$i]['userEmail'],
                                                "search_query" => $keyword,
                                                "current_payout" => $currentPayout,
                                                "origin_ip" => $originIp,
                                                "content_id" => $postParentId,
                                                "interaction_type" => 'Qualified',
                                                "exchange_rate_in_json" => $getExchangeRate,
                                                "mode_type" => 'social',
                                                "creator_id" => $listOfUser[2]['post_creator_id'],
                                                "sharer_id" => '',
                                                "post_type"=>$postParentType,
                                                "post_created_at"=>$post_created_at,
                                                "trans_type"=>$transType

                                            );

                                            $ftueCdp = ftueQualifiedCdp($cdpGlobalParam, $searchDbConn);
                                            //update like post activity count to node database
                                            $activityCount = updatePostActivityCount($userId, $content_id, 'like', $post_created_at, $post_type);
                                            $retArray['errCode'][] = $ftueCdp['errCode'];
                                            $retArray['errMsg'][] = $ftueCdp['errMsg'];
                                        }

                                        if($post_type == 'share'){
                                            $earningAttribute = "post_sharer_earnings";
                                            $transType = "post_sharer_earning";

                                            // close search database connection
                                            //mysqli_close($searchDbConn);

                                            // create keyword database connection
                                            $kwdDbConn = createDBConnection("dbkeywords");

                                            if (noError($kwdDbConn)) {
                                                $kwdDbConn = $kwdDbConn["connection"];

                                                if (isset($postSharedBy) && !empty($postSharedBy)) {
                                                    // get sharer payout
                                                    $calculateSharerPayout = calculateSharerPayout($currentPayout, $kwdDbConn);

                                                    $response = json_encode($calculateSharerPayout["data"]);
                                                    if (noError($calculateSharerPayout)) {

                                                        $calculateSharerPayout = $calculateSharerPayout["data"];
                                                        $sharerPayoutAmnt = $calculateSharerPayout["sharerPayoutAmnt"];
                                                        $currentPayout = $calculateSharerPayout["contentCreatorPayoutAmnt"];

                                                        // close search database connection
                                                        //mysqli_close($kwdDbConn);

                                                        $searchDbConn = createDBConnection("dbsearch");
                                                        if(noError($searchDbConn)) {
                                                            $searchDbConn = $searchDbConn["connection"];

                                                            for($i = 2; $i<=count($listOfUser)-1; $i++){

                                                                if($i == 2){
                                                                    $transType = 'post_creator_earning';
                                                                }else if($i == 3){
                                                                    $transType = 'post_sharer_earning';
                                                                }

                                                                // array that contains function parameters
                                                                $cdpGlobalParam = array(
                                                                    "user_id" => $listOfUser[$i]['userId'],
                                                                    "email" => $listOfUser[$i]['userEmail'],
                                                                    "search_query" => $keyword,
                                                                    "current_payout" => $currentPayout,
                                                                    "sharer_payout" => $sharerPayoutAmnt,
                                                                    "origin_ip" => $originIp,
                                                                    "content_id" => $postParentId,
                                                                    "interaction_type" => 'Qualified',
                                                                    "exchange_rate_in_json" => $getExchangeRate,
                                                                    "mode_type" => 'social',
                                                                    "creator_id" => $listOfUser[2]['userId'],
                                                                    "sharer_id" => $listOfUser[3]['userId'],
                                                                    "post_type"=>$postParentType,
                                                                    "post_created_at"=>$post_created_at,
                                                                    "trans_type"=>$transType

                                                                );

                                                                $ftueCdp = ftueQualifiedCdp($cdpGlobalParam, $searchDbConn);
                                                                //update like post activity count to node database
                                                                $activityCount = updatePostActivityCount($userId, $content_id, 'like', $post_created_at, $post_type);
                                                                $retArray['errCode'][] = $ftueCdp['errCode'];
                                                                $retArray['errMsg'][] = $ftueCdp['errMsg'];

                                                            }

                                                        }else{
                                                            $retArray['errCode'] = 3;
                                                            $retArray['errMsg'] = "Error: Connection search problem";
                                                        }

                                                    }else{
                                                        $retArray['errCode'] = 4;
                                                        $retArray['errMsg'] = "Error: calculating sharer payout";
                                                    }
                                                }
                                            }else{
                                                $retArray['errCode'] = 5;
                                                $retArray['errMsg'] = "Error: Connection problem";
                                            }

                                        }else{

                                            $transType = 'post_creator_earning';

                                            // Post creator earning
                                            $cdpGlobalParam = array(
                                                "user_id" => $listOfUser[2]['userId'],
                                                "email" => $listOfUser[2]['userEmail'],
                                                "search_query" => $keyword,
                                                "current_payout" => $currentPayout,
                                                "origin_ip" => $originIp,
                                                "content_id" => $postParentId,
                                                "interaction_type" => 'Qualified',
                                                "exchange_rate_in_json" => $getExchangeRate,
                                                "mode_type" => 'social',
                                                "creator_id" => $listOfUser[2]['userId'],
                                                "sharer_id" => '',
                                                "post_type"=>$postParentType,
                                                "post_created_at"=>$post_created_at,
                                                "trans_type"=>$transType

                                            );

                                            $ftueCdp = ftueQualifiedCdp($cdpGlobalParam, $searchDbConn);
                                            //update like post activity count to node database
                                            $activityCount = updatePostActivityCount($userId, $content_id, 'like', $post_created_at, $post_type);
                                            $retArray['errCode'][] = $ftueCdp['errCode'];
                                            $retArray['errMsg'][] = $ftueCdp['errMsg'];

                                        }

                                        // close search database connection
                                        //mysqli_close($searchDbConn);

                                        // create keyword database connection
                                        $kwdDbConn = createDBConnection("dbkeywords");

                                        if (noError($kwdDbConn)) {
                                            $kwdDbConn = $kwdDbConn["connection"];

                                            $increaseKwdOwnerEarning = updateKwdOwnershipEarnByFtue($keyword, 'Qualified', $_SESSION['email'], $currentPayout, $postParentId, $originIp, $getExchangeRate, $kwdDbConn);

                                            $retArray['errCode'][] = $increaseKwdOwnerEarning['errCode'];
                                            $retArray['errMsg'][] = $increaseKwdOwnerEarning['errMsg'];
                                        }

                                        $count = count(array_count_values($retArray['errCode']));
                                        if($count == 1){
                                            $retArray['errCode'] = -1;
                                            $retArray['errMsg'] = "success";

                                            $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                                            if(noError($status)){

                                                $requiredBalance = $userRequiredFields.",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                                                $getUserTotalBalance = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredBalance);

                                                if(noError($getUserTotalBalance)){

                                                    $getUserBalance = calculateUserBalance($getUserTotalBalance);

                                                    $getUserTotalBalance       =   $getUserBalance["errMsg"]["total_available_balance"];
                                                    $upc = strtolower($_SESSION['CurrPreference']);
                                                    $getExchangeRate = json_decode($getExchangeRate, true);

                                                    $userTotalBalanceUPC = (float)$getUserTotalBalance * $getExchangeRate["{$upc}"];

                                                    // assign total number of pending interacation into an extraArgs array.
                                                    /*$extraArgs["no_of_pending_interaction"] = $totalPendingIneraction;*/
                                                    /*$extraArgs["search_type"] = $param["interaction_type"];
                                                    $extraArgs["xml_data"] = $xml_data;
                                                    $extraArgs["step_number"] = $i;
                                                    $extraArgs["transaction_id"] = $transactionId;
                                                    $extraArgs["transaction_type"] = $transType;*/

                                                    $retArray['errCode'] = -1;
                                                    $retArray['errMsg'] = "Content consumer earning process done successfully";
                                                    $retArray['nextPage'] = $nextPage;
                                                    $retArray["total_available_balance"] = number_format($getUserTotalBalance, 4,'.','');
                                                    $retArray["total_available_balance_upc"] = number_format($userTotalBalanceUPC, 8,'.','');

                                                }else{

                                                    $retArray['errCode'] = 2;
                                                    $errMsg = "Error: Unable to get user balance";
                                                    $retArray['errMsg'] = $errMsg;

                                                }

                                            }else{

                                                $errMsg = "Error: Update user process failed";
                                                $retArray['errCode'] = 2;
                                                $retArray['errMsg'] = $errMsg;
                                            }

                                        }else{
                                            $retArray['errCode'] = 5;
                                            $retArray['errMsg'] = $retArray['errMsg'];
                                        }

                                    }else{
                                        $retArray['errCode'] = 6;
                                        $retArray['errMsg'] = "Error: Getting payout";
                                    }

                                }else{
                                    $retArray['errCode'] = 7;
                                    $retArray['errMsg'] = "Error: Getting exhange rate";
                                }
                            }else{
                                $retArray['errCode'] = 8;
                                $retArray['errMsg'] = "Error: Getting settings from server";
                            }
                        }else{
                            $retArray['errCode'] = 9;
                            $retArray['errMsg'] = "Error: Connection problem";
                        }

                    }else{
                        $retArray['errCode'] = 404;
                        $retArray['errMsg'] = "you have already get complimentary on $page post.";
                    }
                }else{
                    $retArray['errCode'] = $getFtUserDetail['errCode'];
                    $retArray['errMsg'] = $getFtUserDetail['errMsg'];
                }

            }else{
                $retArray['errCode'] = 6;
                $retArray['errMsg'] = "Error: Connection problem";
            }

        }else{
            $retArray['errCode'] = 11;
            $retArray['errMsg'] = 'Error : Fields does not match';
        }
    }else{
        $retArray['errCode'] = 12;
        $retArray['errMsg'] = 'Error : unauthorized access';
    }

}else{
    $retArray['errCode'] = 13;
    $retArray['errMsg'] = 'Error : session expired';
}

echo json_encode($retArray);

function ftueQualifiedCdp($param, $searchDbConn){

    global $communityPoolUser, $communityPoolUserId, $superUserEmail;

    $returnArr = array();
    $extraArgs = array();
    $currentPayout = $param["current_payout"] ;
    $param["mode_type"] = strtolower($param["mode_type"]);

    /*2. Check total search count searched on particular IP.
      - Get max ip limit set by admin from admin_settings table.
    3. Check if logged in user has referral user or not (in cdp cron structure).
    4. Credit user interaction earning using API request.
    5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
       with type = "interaction_earning"
    6. Insert transaction into week wise user_search_history table into MYSQL search database.
    7. Increase logged in user's qualified searches in userdetails @notification server
        - 7.1 Deduct qualified search of logged in user from node side ( on success of increase qualified searches).
    8. Insert new record into content_consumer_list into mysql table of keyword database.
    9. Insert/update qualified search count + 1 into keywo_stats table of keyword MYSQL database.
    10. write CDP process into xml logs.*/

    if(isset($param) && !empty($param) && isset($searchDbConn) && !empty($searchDbConn)){

        $earningAttribute = '';

        if(count($param) > 0){
            // 4. Credit user interaction earning @wallet server where 'searchearning' is the field arrtibute and
            //  'add' is the action to perform in @wallet server's table.

            if($param['trans_type'] == 'post_viewer_earning'){
                $earningAttribute = "post_view_earnings";
                $transType = "post_viewer_earning";
            }else if($param['trans_type'] == 'keywo_content_view_earning'){
                $earningAttribute = "post_view_earnings";
                $transType = "keywo_content_view_earning";
            }else if($param['trans_type'] == 'post_creator_earning'){
                $earningAttribute = "post_creator_earnings";
                $transType = "post_creator_earning";
            }else if($param['trans_type'] == 'post_sharer_earning'){
                $earningAttribute = "post_sharer_earnings";
                $transType = "post_sharer_earning";
                $currentPayout = $param['sharer_payout'];
            }

            $creditInteractionEarning = creditUserEarning($param["user_id"], $currentPayout, $earningAttribute, "add");
            /*echo "creditUserEarning"."<br>";
            printArr($creditInteractionEarning);*/
            if(noError($creditInteractionEarning)){
                $creaditInteractionEarning = $creditInteractionEarning["errMsg"];
                $totalAvailableBalance = $creditInteractionEarning["search_earning"];
                $contentConsumerId = $creditInteractionEarning["_id"];
                //  5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
                //   with type = "interaction_earning"

                // parameters required to insert in transaction

                $recipientEmail = $param["email"];
                $recipientUserId = $param["user_id"];
                $senderEmail = $communityPoolUser;
                $senderUserId = $communityPoolUserId;
                $amount = $currentPayout;
                $type = $transType;
                $paymentMode = 'ITD';
                $originIp = $param["origin_ip"];
                $exchangeRateInJson = $param["exchange_rate_in_json"];

                $metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => urlencode(utf8_encode($param["search_query"])), "discount" => 0, "commision" => 0, "post_type" => $param["post_type"],"post_created_at"=>$param["post_created_at"]);

                $insertUsrInteractionTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                /*echo "insertUserTransaction"."<br>";
                printArr($insertUsrInteractionTrnasaction);*/
                // retrieve transaction time and transaction id on success of interaction earning transaction @wallet server
                if(noError($insertUsrInteractionTrnasaction)){
                    // retrieving receiver_search_earning transaction id and transaction time.
                    $insertUsrInteractionTrnasaction = $insertUsrInteractionTrnasaction["errMsg"]["committed_transaction"];

                    $transactionId = $insertUsrInteractionTrnasaction["transaction_id"]; // transaction time should get from transaction success response
                    $transactionTime = $insertUsrInteractionTrnasaction["timestamp"]; // transaction time should get from transaction success response.
                    $transactionTime = $transactionTime / 1000; // convert millisecond to seconds
                    $transactionTime = date('Y-m-d H:i:s', $transactionTime);

                    // 6. Insert transaction into week wise user_search_history table into MYSQL search database.
                    $insertUsrSearchHistory = insertSearchHistory($param["email"], $param["search_query"], $transType, $currentPayout, $param["exchange_rate_in_json"], $param["app_id"], $transactionId, $transactionTime, $param["origin_ip"], $searchDbConn);

                    if(noError($insertUsrSearchHistory)){

                        // close search database connection
                        //mysqli_close($searchDbConn);

                        // create keyword database connection

                        $keywordDbConn = createDBConnection("dbkeywords");
                        if(noError($keywordDbConn)){
                            $keywordDbConn = $keywordDbConn["connection"];

                            //  8. Insert new record into content_consumer_list into mysql table of keyword database on success
                            //     of content consumer earning.

                            // Note : If search user has referral eamil then referral payout amount will be deducted from search user amount only, not from other 3 users.
                            // So in content consumer lsit table has total current payout amount.

                            // add keyword interaction in revenue table
                            $increaseKwdCntInRevenueTable = addKwdCntInRevenueTable($param["search_query"], $param["interaction_type"], $param["email"], $param["content_id"], $param["post_type"], $param["origin_ip"], $keywordDbConn);

                            if(noError($increaseKwdCntInRevenueTable)){

                                $increaseKwdCntInAnalyticsTable = addKwdCntInAnalyticsTable($param["search_query"], $param["interaction_type"], $param["post_type"], 'social', $keywordDbConn);
                              if(noError($increaseKwdCntInAnalyticsTable)){

                                    $errMsg = "succes: {$earningAttribute}";
                                    $returnArr['errCode'] =  -1;

                                    /* Increase qualified searches to only super user while giving complementary like */
                                    if($param['email'] == $superUserEmail){
                                        //  7. Increase logged in user's qualified searches in userdetails @notification server
                                        $increaseUsrQualifiedSearch = increaseUserSearches($param["email"], $param["interaction_type"]);

                                        if(noError($increaseUsrQualifiedSearch)){

                                            $increaseUsrQualifiedSearch = $increaseUsrQualifiedSearch["errMsg"];
                                            // retrive user's total number of interaction pending
                                            $totalPendingIneraction = $increaseUsrQualifiedSearch["no_of_qualified_interactions_pending"];
                                            if($totalPendingIneraction <= 0 ){
                                                $totalPendingIneraction = 0;
                                            }

                                            $errMsg = "succes: {$earningAttribute}";
                                            $returnArr['errCode'] =  -1;

                                        }else{
                                            $errMsg = "Failed: Increase super user qualified searches";
                                            $returnArr['errCode'] =  $increaseUsrQualifiedSearch['errCode'];
                                        }
                                    }
                                    $returnArr['errMsg'] = $errMsg;
                                    $returnArr['errCode'] = $returnArr['errCode'];

                                }else{
                                    $returnArr['errMsg'] = "Failed: Update keyword analytics";
                                    $returnArr['errCode'] = $increaseKwdCntInAnalyticsTable['errCode'];
                                }

                            }else{
                                $returnArr['errMsg'] = "Failed: Update keyword count in revenue";
                                $returnArr['errCode'] = $increaseKwdCntInRevenueTable['errCode'];
                            }

                        }else{

                            $returnArr['errMsg'] = "Error: Unable to create keyword database connection";
                            $returnArr['errCode'] = $keywordDbConn['errCode'];

                        }

                    }else{

                        $returnArr['errMsg'] = "Error: Unable to insert {$earningAttribute} user search history";
                        $returnArr['errCode'] = $insertUsrSearchHistory['errCode'];
                    }
                }else{

                    $returnArr['errMsg'] = "Error: Insert {$earningAttribute} transaction server";
                    $returnArr['errCode'] = $insertUsrInteractionTrnasaction['errCode'];
                }
            }else{

                $returnArr['errMsg'] = "Error: Credit user interaction earning with current payout amount : {$currentPayout}";
                $returnArr['errCode'] = $creditInteractionEarning['errCode'];
            }
        }else{

            $returnArr['errMsg'] = "Error: No data found";
            $returnArr['errCode'] = 2;
        }
    }else{
        $returnArr['errMsg'] = "Error: Mandatory field not found";
        $returnArr['errCode'] = 2;
    }


    return $returnArr;
}

function updateKwdOwnershipEarnByFtue($searchQuery, $searchType, $consumerEmail, $currentPayout, $appId, $originIp, $exchangeRateInJson, $kwdDbConn){

    $returnArr = array();
    $extraArgs = array();

    global $unOwnedKwdOwnerUserId, $communityPoolUserId, $unownedKwdOwnerUser, $communityPoolUser, $userRequiredFields, $walletURLIP;

    if(isset($searchQuery) && !empty($searchQuery) && isset($searchType) && !empty($searchType) && isset($consumerEmail) && !empty($consumerEmail) && isset($currentPayout) && !empty($currentPayout) && isset($appId) && !empty($appId) && isset($kwdDbConn) && !empty($kwdDbConn)){

        $keywordArr = explode(" ", $searchQuery);

        // get total keyword count
        $kwdCount = count($keywordArr);
        // calculate per keyword current payout
        $perKeywordPayout = $currentPayout/$kwdCount;

        $perKeywordPayout = number_format($perKeywordPayout, 8);

        $stepCounterKwdOwnershiploop=0;

        foreach($keywordArr as $key => $keyword){

            $keywordbase64=md5($keyword);

            if(isset($keyword) && !empty($keyword)){
                //get keyword ownership details from keyword_ownership_<first_lette e.g. c > table from keyword database.
                $kwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                if(noError($kwdOwnershipDetails)){

                    $kwdOwnershipDetails = $kwdOwnershipDetails["errMsg"];

                    $kwdOwnerEmail    = $kwdOwnershipDetails["buyer_id"];
                    $noOfActiveBids   = $kwdOwnershipDetails["no_of_active_bids"];
                    $highestBidAmount = $kwdOwnershipDetails["highest_bid_amount"];
                    $kwdAskPrice      = $kwdOwnershipDetails["ask_price"];
                    $expireWithinDays = $kwdOwnershipDetails["days_to_expire"];

                    if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                        $kwdOwnerEmail = "admin";
                    }else{
                        // validate the keyword remainder period if keyword serving reminder period
                        if($expireWithinDays <= 30){
                            $kwdOwnerEmail = "admin";
                        }else{
                            $kwdOwnerEmail  = $kwdOwnerEmail;
                        }
                    }

                    // get keyword revenue details
                    $kwdRevenueDetails = getRevenueDetailsByKeyword($kwdDbConn, $keyword);

                    if(noError($kwdRevenueDetails)){
                        $kwdRevenueTableName    = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails      = $kwdRevenueDetails["data"][0];

                        $id = $kwdRevenueDetails["id"];
                        $appKwdOwnershipEarning = $kwdRevenueDetails["app_kwd_ownership_earnings"];
                        $usrKwdOwnershipEarning = $kwdRevenueDetails["user_kwd_ownership_earnings"];

                        /* if(isset($id) && !empty($id)){
                             // update revenue details

                             //update app keyword ownership earnings
                             $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                             if(array_key_exists($appId,$appKwdOwnershipEarning)){
                                 $appKwdOwnershipEarning[$appId] = $appKwdOwnershipEarning[$appId] + $perKeywordPayout;
                             }else{
                                 $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                             }
                             $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                             //update user keyword ownership earnings
                             $usrKwdOwnershipEarning = $usrKwdOwnershipEarning + $perKeywordPayout;

                             $query =  "UPDATE ".$kwdRevenueTableName." SET app_kwd_ownership_earnings='".$appKwdOwnershipEarning."',user_kwd_ownership_earnings='".$usrKwdOwnershipEarning."' WHERE keyword='".$keyword."'";

                         }else{
                             // insert revenue details
                             //insert app keyword ownership earnings
                             $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                             $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                             $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                             //insert into revenue table
                             $query = "INSERT into ".$kwdRevenueTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches) VALUES('".$keyword."','".$kwdOwnerEmail."','','".$appKwdOwnershipEarning."','1','".$perKeywordPayout."','','','')";
                         }*/

                        if(isset($id) && !empty($id)){
                            // update revenue details

                            //update app keyword ownership earnings
                            $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                            if(array_key_exists($appId,$appKwdOwnershipEarning)){
                                $appKwdOwnershipEarning[$appId] = $appKwdOwnershipEarning[$appId] + $perKeywordPayout;
                            }else{
                                $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                            }

                            //update user keyword ownership earnings
                            $usrKwdOwnershipEarning = $usrKwdOwnershipEarning + $perKeywordPayout;

                        }else{
                            // insert revenue details
                            //insert app keyword ownership earnings
                            $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning,TRUE);
                            $appKwdOwnershipEarning[$appId] = $perKeywordPayout;
                            $appKwdOwnershipEarning = json_encode($appKwdOwnershipEarning);

                            //insert into revenue table
                            $query = "INSERT into ".$kwdRevenueTableName."(keyword,kwd_owner_email,app_kwd_search_count,app_kwd_ownership_earnings,user_kwd_search_count,user_kwd_ownership_earnings,kwd_total_paid_searches,kwd_total_unpaid_searches,kwd_total_anonymous_searches,search_startFrom,follow_unfollow) VALUES('".$keyword."','".$kwdOwnerEmail."','','','','','','','','','')";
                            $updateOwnershipEarningInRevenueTable = updateOwnershipEarningInRevenueTable($query, $kwdDbConn);
                            if(!noError($updateOwnershipEarningInRevenueTable)){
                                $errMsg =  "Error: Insert revenue details in  revenue table";
                                $returnArr = setErrorStack($returnArr, 45, $errMsg, $extraArgs);
                            }


                        }

                        /*  if(noError($updateOwnershipEarningInRevenueTable)){*/

                        if($kwdOwnerEmail == "admin"){

                            // get keyword owner user id using getUserInfo method
                            $kwdOwnerInfo = $userRequiredFields.",user_id,gender,first_name,last_name";

                            $kwdOwnerUserInfo =  getUserInfo($unownedKwdOwnerUser, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                            $kwdOwnergender = $kwdOwnerUserInfo["gender"];
                            $kwdOwnerNames = $kwdOwnerUserInfo["first_name"]." ".$kwdOwnerUserInfo["last_name"];

                            $addKwdUnownedOwnerIncome = creditUserEarning($unOwnedKwdOwnerUserId, $perKeywordPayout, "kwdincome", "add");

                            if(noError($addKwdUnownedOwnerIncome)){
                                $addKwdUnownedOwnerIncome = $addKwdUnownedOwnerIncome["errMsg"];
                                $totalUnownedKwdIncome = number_format($addKwdUnownedOwnerIncome["total_kwd_income"], 8);

                                $cdpStepDone["keyword"][$keyword]["owner"] = $kwdOwnerEmail;
                                $cdpStepDone["keyword"][$keyword]["update_user_balance"] = "success";

                                $updateKwdOwnerJson = json_encode($cdpStepDone);

                                // insert user transaction for keywor owner transaction
                                $recipientEmail = $unownedKwdOwnerUser;
                                $recipientUserId = $unOwnedKwdOwnerUserId;
                                $senderEmail = $communityPoolUser;
                                $senderUserId = $communityPoolUserId;
                                $amount = $perKeywordPayout;
                                $type = "keyword_ownership_earning";
                                $paymentMode = 'ITD';
                                $originIp = $originIp;
                                $exchangeRateInJson = $exchangeRateInJson;

                                $metaDetails = array("reference_transaction_id" => '', "sender" => $communityPoolUser, "receiver" => $unownedKwdOwnerUser, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                                $insertUnownedEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                $response = json_encode($insertUnownedEarningTrnasaction["errMsg"]);
                                if(noError($insertUnownedEarningTrnasaction)){

                                    // credit keyword refund to community pool
                                    $creditRefundAmtToCommunity = creditUserEarning($communityPoolUserId, $perKeywordPayout, "kwdincome", "add");
                                    if(noError($creditRefundAmtToCommunity)){

                                        $kwywordOwnershipChild["step{$stepCounterKwdOwnershiploop}_{$keywordbase64}"]= "{$stepCounterKwdOwnershiploop}. {$errMsg}";

                                        // insert transaction for keyword refund amount
                                        $recipientEmail = $communityPoolUser;
                                        $recipientUserId = $communityPoolUserId;
                                        $senderEmail = $unownedKwdOwnerUser;
                                        $senderUserId = $unOwnedKwdOwnerUserId;
                                        $amount = $perKeywordPayout;
                                        $type = "keyword_ownership_refund";
                                        $paymentMode = 'ITD';
                                        $originIp = $originIp;
                                        $exchangeRateInJson = $exchangeRateInJson;

                                        $metaDetails = array("reference_transaction_id" => '', "sender" =>  $unownedKwdOwnerUser, "receiver" => $communityPoolUser, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0, "desc" => "The ".$unownedKwdOwnerUser." send ".$perKeywordPayout." ITD as keyword refund amount to ".$communityPoolUser." user.");

                                        $insertPoolKwdRefundTransaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                        $response = json_encode($insertPoolKwdRefundTransaction["errMsg"]);
                                        if(noError($insertPoolKwdRefundTransaction)){

                                            $errMsg = "Success: Insert user transaction for keyword refund earning.";
                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                        }else{

                                            $errMsg =  "Error: Unabel to insert transaction for keyword refund earning";
                                            $returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);
                                        }

                                    }else{

                                        $errMsg =  "Error: Unable to credit refund amount to community pool as keyword income";
                                        $returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);

                                    }

                                } else{

                                    $errMsg =  "Error: Unable to insert transaction for unownedKwdOwner@keywo.com user";
                                    $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);

                                    /*$rollbackTransaction = rollbackTransaction($kwdDbConn);
                                    if(!noError($rollbackTransaction)){
                                        $errMsg =  "Error: rollback keyword owner transaction";
                                        $returnArr = setErrorStack($returnArr, 12, $errMsg, $extraArgs);

                                    }*/

                                }

                            }else{

                                $errMsg =  "Error: Unable to update keyword income to unowned keyword owner.";
                                $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                            }


                        }else{ // if keywordOwnerEmail == "keywordowner@bitstreet.in" i.e keyword owned user
                            /*
                              1. update keyword income to keyword owner
                              2. Add keyword payout amount to communitpol@keywo.com user.
                              3. Insert user transaction where sender = communitypool@keywo.com and receiver = keyord_owner
                            */

                            // get keyword owner user id using getUserInfo method
                            $kwdOwnerInfo = $userRequiredFields.",user_id,gender,first_name,last_name";

                            $kwdOwnerUserInfo =  getUserInfo($kwdOwnerEmail, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                            if(noError($kwdOwnerUserInfo)){
                                // retrieve user id
                                $kwdOwnerUserInfo = $kwdOwnerUserInfo["errMsg"];
                                $kwdOwnerId = $kwdOwnerUserInfo["_id"];
                                $kwdOwnerGender = $kwdOwnerUserInfo["gender"];
                                $keywordOwnerusername = $kwdOwnerUserInfo["first_name"]." ".$kwdOwnerUserInfo["last_name"];

                                // 1. update keyword income to keyword owner
                                $addKwdOwnerkeywordIncome = creditUserEarning($kwdOwnerId, $perKeywordPayout, "kwdincome", "add");
                                $response = json_encode($addKwdOwnerkeywordIncome["errMsg"]);

                                $xml_atrribute["user"] = $kwdOwnerEmail;
                                $xml_atrribute["amount_given_to_user"] = $perKeywordPayout;
                                $xml_atrribute["interaction_type"] = "keyword_ownership_earning";
                                $xml_atrribute["post_n_app_id"] = $appId;
                                $xml_atrribute["kwd_owner_gender"] = $kwdOwnerGender;
                                $xml_atrribute["keywordName"] = $keywordbase64;
                                $xml_atrribute["response"] = $response;
                                $xml_atrribute["username"] = $keywordOwnerusername;
                                $xml_atrribute["number_of_days_left_for_renew"]=$expireWithinDays;

                                if(noError($addKwdOwnerkeywordIncome)){

                                    // 2. Insert user transaction where sender = communitypool@keywo.com and receiver = keyord_owner
                                    // parameters required to insert in transaction
                                    $addKwdOwnerkeywordIncome = $addKwdOwnerkeywordIncome["errMsg"];

                                    $totKwdIncome = number_format($addKwdOwnerkeywordIncome["total_kwd_income"], 8);

                                    // insert user transaction for keywor owner transaction
                                    $recipientEmail = $kwdOwnerEmail;
                                    $recipientUserId = $kwdOwnerId;
                                    $senderEmail = $communityPoolUser;
                                    $senderUserId = $communityPoolUserId;
                                    $amount = $perKeywordPayout;
                                    $type = "keyword_earning";
                                    $paymentMode = 'ITD';
                                    $originIp = $originIp;
                                    $exchangeRateInJson = $exchangeRateInJson;

                                    $metaDetails = array("reference_transaction_id" => '', "sender" => $communityPoolUser, "receiver" => $kwdOwnerEmail, "current_payout" => $perKeywordPayout, "trans_type" => $type, "exchange_rate"  => $exchangeRateInJson, "payment_mode" => $paymentMode,"content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                                    $insertKwdOwnerEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                    $response = json_encode($insertKwdOwnerEarningTrnasaction["errMsg"]);
                                    if(noError($insertKwdOwnerEarningTrnasaction)){

                                        $errMsg = "Success: Insert user transaction for keyword refund earning.";
                                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                    }else{

                                        $errMsg =  "Error: Unable to insert transaction for keyword refund earning";
                                        $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);
                                    }


                                }else{

                                    $errMsg =  "Error: Unable to add keyword income of ". $kwdOwnerEmail." user";
                                    $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
                                }
                            }else{

                                $errMsg =  "Error: Unable to get user info of ". $kwdOwnerEmail." user";
                                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                            }

                        }
                    }else{

                        $errMsg =  "Error: Unable to get keyword ownership details";
                        $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                    }
                }else{

                    $errMsg =  "Error: Unable to get keyword ownership details";
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
            }else{
                $errMsg =  "Error: keyord not found";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
            }

        }

    }else{
        $errMsg =  "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

return $returnArr;
}

?>


