<?php

session_start();


if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){

    // include require files
    require_once('../../config/config.php');
    require_once('../../config/db_config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/walletHelper.php');
    require_once('../../helpers/deviceHelper.php');
    require_once('../../helpers/arrayHelper.php');
    require_once('../../helpers/stringHelper.php');
    require_once('../../helpers/geoTrendHelper.php');
    require_once('../../helpers/walletHelper.php');
    require_once('../../models/cdp/cdpUtilities.php');
    require_once('../../models/keywords/keywordCdpModel.php');
    require_once('../../models/keywords/userCartModel.php');
    require_once('../../models/header/headerModel.php');
    require_once('../../models/wallet/walletKYCModel.php');
    require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
    require_once('../../models/landing/landingFeatureModel.php');


    $logStorePathUserMgmt = $logPath["coinDistribution"];
    if(isset($_POST) && !empty($_POST)){

        // get post filed

        $email                =   $_SESSION["email"]; // either search user or post viewer
        $content_id           =   $_POST["content_id"];    // get app id if user search from search engine else user post view from social then get post id.
        $appName              =   urldecode($_POST["appName"]); // get app name for searched engine.
        $appName              =   strtolower($appName); // convert into small letters
        $searchQuery          =   urldecode($_POST["q"]); // search query
        $searchQuery          =   strtolower($searchQuery); // convert into small letters
        $logFileName          =   urldecode($_POST["log"]);
        $ipSearchCount        =   $_POST["searchCountFromIP"];
        $originIp             =   $_POST["origin_ip"];
        $modeType             =   $_POST["mode_type"];
        $creatorId            =   $_POST["posted_by"];
        $sharedId             =   $_POST["shared_by"];
        $cdpStatusFromSocial  =   $_POST["post_cdp_status"];
        $socialCategoryType   =   $_POST["post_type"];
        $socialCDPtype        =   $_POST["socialCDPType"];
        $post_created_at      =   $_POST["created_at"];
        $sharedStatus       =   $_POST["share_status"];

        /****** log write section ******/
        //for xml writing essential
        $xmlProcessor = new xmlProcessor();
        $xmlArray = initializeXMLLog($email);

        if($modeType == "search"){
            $xmlfilename = $appName.".xml";
        }else{
            $xmlfilename = $socialCDPtype.".xml";
        }

        $country=$xmlArray["activity"]["country"];
        $gender=$xmlArray["activity"]["gender"];
        $device=$xmlArray["activity"]["device"];
        $browser=$xmlArray["activity"]["browser"];
        $username=$xmlArray["activity"]["username"];


        $xml_data['request']["data"] = '';
        $xml_data['request']["attribute"] = $xmlArray["request"];

        $xml_data['step']["data"] = 'Coin Distribution Process';


        if($modeType=="search")
        {
            $appArray["app_id"]=$content_id;
            $appArray["app_name"]=$appName;
            $transType = "search_earning";
            $appArray["intraction_type"]="search_earning"; //trnx type
        }else
        {
            $appArray["post_id"]=$content_id;
            $appArray["app_name"]="social";
            $appArray["post_type"]=$socialCategoryType;
            $transType = "post_viewer_earning";
            $appArray["intraction_type"]="post_view_earnings"; //trnx type
        }


        $appArray["intractionMode"]=$modeType;
        $xml_data['app']["data"] = '';
        $xml_data['app']["attribute"] = $appArray;




        // initialize log step number variable to increase dynamically.
        $i = 0;

        /****** partially end log section ******/


        // create search database connection
        $searchDbConn = createDBConnection("dbsearch");
        if(noError($searchDbConn)){
            $searchDbConn = $searchDbConn["connection"];

            // log path url
            $logStorePathUserMgmt = $logPath["coinDistribution"];

            $msg = "Success : Search database connection";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $i = $i+1;
            $xml_data["step".$i]["data"] = $i.'. ' . $msg;

            // get max ip limit from admin setting table
            $getAdminSetting = getSettingsFromSearchAdmin($searchDbConn);


            if(noError($getAdminSetting)){
                $getAdminSetting = $getAdminSetting["data"];
                $ipMaxLimit = $getAdminSetting["IPMaxLimit"];

                $msg = "Success : get admin setting.";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. '.$msg;

                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. Parameters are 1) Email: '.$email.' 2) Content Id: '.$content_id.' 3) App Name: '.$appName.' 4) Search Query: '.$searchQuery.' 5) IP Search Count: '.$ipSearchCount.' 6) Origin Ip: '.$originIp;


                $xml_data['search_keyword']["data"] = $searchQuery;
                // get currency exchange rate of usd, sgd and btc
                $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);

                if(noError($getExchangeRate)){
                    $getExchangeRate = $getExchangeRate["exchange_rate"];
                    $getExchangeRate = json_encode($getExchangeRate);

                    $msg = "Success : Currency exchange rate";
                    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. '.$msg . ' - '.$getExchangeRate;

                    // access $userRequiredFields field from config.php file
                    $userInteractionDetails = "user_id,no_of_qualified_interactions_pending,kyc_current_level,no_of_searches_in_last_hour,last_hour_search_time";

                    // 1. get current user search type i.e. qualified or unqualified.
                    $interactionType = getSearchType($email, $userInteractionDetails);

                    if(noError($interactionType)){
                        $interactionType        =   $interactionType["data"];
                        $userId                 =   $interactionType["user_id"];
                        $searchType             =   $interactionType["user_search_type"];
                        $noOfInteractionPending =   $interactionType["no_of_qualified_interactions_pending"];
                        $noOfSearchesInLastHour =   $interactionType["no_of_searches_in_last_hour"];
                        $lastHourSearchTime     =   $interactionType["last_hour_search_time"];
                        $qualifiedInteractionLimit     =   $interactionType["qualified_interaction_limit"];

                        $msg = "Success : User search type - ".$searchType;
                        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. '.$msg;

                        if($searchType == "Qualified" && $ipSearchCount <= $ipMaxLimit){
                            $searchType = "Qualified";
                            $currentPayout = $currentPayout;
                        }else{
                            $searchType = "Unqualified";
                            $currentPayout = number_format(0, 8);
                        }


                        // 1. Get current payout amount from payout.json file.
                        $currentPayout = getCurrentPayout();

                        if(noError($currentPayout)){
                            $currentPayout = $currentPayout["current_payout"];
                            $currentPayout = number_format(($currentPayout/4), 8);



                            $intractionArray["qualificationType"]=$searchType;
                            $intractionArray["intractionPayout"]=$currentPayout;
                            $intractionArray["postType"]=$socialCategoryType;
                            $xml_data['intraction']["data"] = '';
                            $xml_data['intraction']["attribute"] = $intractionArray;

                            $msg = "Success : Current payout - ".$currentPayout;
                            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. '.$msg;

                            /*******************************************************************************************************************
                             ************* Process follow in coin distribution process if search type is qualified is given below:**************
                             *
                             *  2. Check total search count searched on particular IP.
                             *  - Get max ip limit set by admin from admin_settings table.
                             * 3. Check if logged in user has referral user or not (in cdp cron structure).
                             * 4. Credit user interaction earning using API request.
                             * 5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
                             *  with type = "interaction_earning"
                             * 6. Insert transaction into week wise user_search_history table into MYSQL search database.
                             * 7. Increase logged in user's qualified searches in userdetails @notification server
                             *   - 7.1 Deduct qualified search of logged in user from node side ( on success of increase qualified searches).
                             * 8. Insert new record into content_consumer_list into mysql table of keyword database.
                             * 9. Insert/update qualified search count + 1 into keywo_stats table of keyword MYSQL database.
                             * 10. write CDP process into xml logs.
                             *
                             **********************************************************************************************************************/

                            /*******************************************************************************************************************
                             ************* Process follow in coin distribution process if search type is unqualified is given below:**************
                             *
                             *  1. Update totalUnqualifiedSearches field by 1 into sc_app_details table of corresponding appId.
                             *  2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
                             *  3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
                             *  4. Update week wise user search transaction history table with type = "Unqualified" into mysql search database
                             *  5. Increasing logged in user's unqualified search count into "userdetails" using API (@notification server).
                             *  6. Update unqualified_searches count by 1 into keywo_stats table of corresponding appId (instead of updating
                             *   pool unqualified searches in pool user).
                             *
                             **********************************************************************************************************************/


                            // array that contains function parameters
                            $cdpGlobalParam = array(
                                "user_id" => $userId,
                                "email" => $email,
                                "interaction_type" => $searchType,
                                "user_interaction_limit" => $qualifiedInteractionLimit,
                                "search_query" => $searchQuery,
                                "current_payout" => $currentPayout,
                                "origin_ip" => $originIp,
                                "content_id" => $content_id,
                                "app_name" => $appName,
                                "exchange_rate_in_json" => $getExchangeRate,
                                "no_of_qualified_interactions_pending" => $noOfInteractionPending,
                                "no_of_searches_in_last_hour" => $noOfSearchesInLastHour,
                                "last_hour_search_time" => $lastHourSearchTime,
                                "mode_type" => $modeType,
                                "creator_id" => $creatorId,
                                "sharer_id" => $sharedId,
                                "cdp_status_from_social" => $cdpStatusFromSocial,
                                "country" => $country,
                                "device" => $device,
                                "gender" => $gender,
                                "browser" => $browser,
                                "username" => $username,
                                "post_type"=>$socialCategoryType,
                                "post_created_at"=>$post_created_at,
                                "share_status" => $sharedStatus

                            );

                            $coinDistribution = coinDistribution($cdpGlobalParam, $searchDbConn, $xml_data, $i);

                            if(noError($coinDistribution)){

                                // get log data from coinDistribution["xml_data"] to xml_data variable.
                                $xml_data = $coinDistribution["xml_data"];
                                $i = $coinDistribution["step_number"];
                                $i = $i+1;
                                $xml_data["step".$i]["data"] = $i.'. Success: '.$coinDistribution["errMsg"];


                                // update geo trend database with keyword details
                                $updateGeoTrendData = updateGeoTrendData($searchQuery, $content_id, $originIp);
                                if(noError($updateGeoTrendData)){

                                    $i = $i+1;
                                    $xml_data["step".$i]["data"] = $i.'. Success: Increase keyword count into geo trends database';

                                    // get user's current search status whether qualified or unqualified.
                                    $currentInteractionType = getSearchType($email, $userInteractionDetails);

                                    if(noError($currentInteractionType)){

                                        $currentInteractionType = $currentInteractionType["data"];
                                        $currentInteractionType = $currentInteractionType["user_search_type"];

                                        if($currentInteractionType == "Qualified" && $ipSearchCount <= $ipMaxLimit){
                                            $currentInteractionType = "Qualified";
                                        }else{
                                            $currentInteractionType = "Unqualified";
                                        }

                                        $i = $i+1;
                                        $xml_data["step".$i]["data"] = $i.'. Success: Get user current interaction type: '.$currentInteractionType;

                                        // on success
                                        $extraArgs["kwd_count"] = $updateGeoTrendData["count"];
                                        if($currentInteractionType == "Qualified"){
                                            $extraArgs["no_of_pending_interaction"] = $coinDistribution["no_of_pending_interaction"];
                                        }else{
                                            $extraArgs["no_of_pending_interaction"] = $noOfInteractionPending;
                                        }
                                        $extraArgs["current_payout"] = $currentPayout;
                                        $extraArgs["total_available_balance"] = $coinDistribution["total_available_balance"];
                                        // $extraArgs["search_type"] = $coinDistribution["search_type"];
                                        $extraArgs["search_type"] = $currentInteractionType;
                                        $extraArgs['transaction_id'] = $coinDistribution["transaction_id"];
                                        $extraArgs['transaction_type'] =$coinDistribution["transaction_type"];
                                        $errCode    = $coinDistribution["errCode"];
                                        $errMsg     = $coinDistribution["errMsg"];

                                        $returnArr  = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);

                                        /******************** RESPONSE LOG ****************/
                                        $responseArr["errCode"]=$errCode;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** RESPONSE LOG ****************/

                                    }else{
                                        $i = $i+1;
                                        $xml_data["step".$i]["data"] = $i.'. Failed: Get user current interaction type';

                                        $extraArgs["xml_data"] = $xml_data;
                                        $errMsg = $currentInteractionType["errMsg"];
                                        $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);

                                        /******************** RESPONSE LOG ****************/
                                        $responseArr["errCode"]="7";
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** RESPONSE LOG ****************/
                                    }
                                }else{
                                    // get log data from coinDistribution["xml_data"] to xml_data variable.
                                    $xml_data = $coinDistribution["xml_data"];
                                    $i = $coinDistribution["step_number"];

                                    $i = $i+1;
                                    $xml_data["step".$i]["data"] = $i.'. '.$updateGeoTrendData["errMsg"];

                                    $extraArgs["xml_data"] = $xml_data;
                                    $errMsg = $updateGeoTrendData["errMsg"];
                                    $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);

                                    /******************** RESPONSE LOG ****************/
                                    $responseArr["errCode"]="6";
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** RESPONSE LOG ****************/
                                }
                            }else{

                                // get log data from coinDistribution["xml_data"] to xml_data variable.
                                $xml_data = $coinDistribution["xml_data"];
                                $i = $coinDistribution["step_number"];

                                $i = $i+1;
                                $xml_data["step".$i]["data"] = $i.'. '.$coinDistribution["errMsg"];

                                // on failure
                                $extraArgs["xml_data"] = $xml_data;
                                $errCode    = $coinDistribution["errCode"];
                                $errMsg     = $coinDistribution["errMsg"];
                                $returnArr  = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);

                                /******************** RESPONSE LOG ****************/
                                $responseArr["errCode"]=$errCode;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** RESPONSE LOG ****************/
                            }
                        }else{

                            $i = $i+1;
                            $xml_data["step".$i]["data"] = $i.'. Unable to get current payout';

                            $extraArgs["xml_data"] = $xml_data;
                            $errMsg = $currentPayout["errMsg"];
                            $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

                            /******************** RESPONSE LOG ****************/
                            $responseArr["errCode"]="5";
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** RESPONSE LOG ****************/
                        }
                    }else{

                        $i = $i+1;
                        $xml_data["step".$i]["data"] = $i.'. Error: Fetching interaction type';

                        $extraArgs["xml_data"] = $xml_data;
                        $errMsg = 'Error: Fetching interaction type';
                        $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);

                        /******************** RESPONSE LOG ****************/
                        $responseArr["errCode"]="4";
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** RESPONSE LOG ****************/
                    }
                }else{
                    $i = $i+1;
                    $xml_data["step".$i]["data"] = $i.'. Error: Fetching exchange rate';

                    $extraArgs["xml_data"] = $xml_data;
                    $errMsg = 'Error: Fetching exchange rate';
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

                    /******************** RESPONSE LOG ****************/
                    $responseArr["errCode"]="3";
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** RESPONSE LOG ****************/
                }
            }else{
                $i = $i+1;
                $xml_data["step".$i]["data"] = $i.'. Error: Fetching setting details';

                $extraArgs["xml_data"] = $xml_data;
                $errMsg = "Error: Fetching setting details";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);

                /******************** RESPONSE LOG ****************/
                $responseArr["errCode"]="2";
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** RESPONSE LOG ****************/
            }
        }else{
            $i = $i+1;
            $xml_data["step".$i]["data"] = $i.'. Error: unable to connect search database';

            $extraArgs["xml_data"] = $xml_data;
            $errMsg = 'Error: unable to connect search database';
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);

            /******************** RESPONSE LOG ****************/
            $responseArr["errCode"]="2";
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** RESPONSE LOG ****************/
        }
    }else{
        $i = $i+1;
        $xml_data["step".$i]["data"] = $i.'. Error: Mandatory field not found';

        $extraArgs["xml_data"] = $xml_data;
        $errMsg = "Error: Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);

        /******************** RESPONSE LOG ****************/
        $responseArr["errCode"]="1";
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** RESPONSE LOG ****************/
    }

    // create or update xml log Files
    $xmlProcessor->writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);
    $returnArr["xml_data"] = array();
    unset($returnArr["xml_data"]);

    echo json_encode($returnArr);
}else{
    echo "Authorisation failure";
    exit;
}