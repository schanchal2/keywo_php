<?php 
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	
$countryID  = $_POST['countryID'];
$query 		= "SELECT id,name FROM states where country_id =".$countryID." ORDER BY name"; 
$result		= runQuery($query, $conn);
if(noError($result)){
	$res = array();
	while($row = mysqli_fetch_assoc($result["dbResource"]))
		$res[] = $row;
}else{
	print("Error: Fetching state details");
}
print "<option value='' disabled selected = 'selected'>Select State</option>";
foreach($res as $state) {
	print "<option value='".$state["id"]."'>".$state["name"]."</option>";
}
?>