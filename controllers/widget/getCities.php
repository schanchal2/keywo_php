<?php 
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}

$stateID = $_POST['stateID'];
$query 	 = "SELECT id,name FROM cities where state_id =".$stateID;
$result  = runQuery($query, $conn);
if(noError($result)){
	$res = array();
	while($row = mysqli_fetch_assoc($result["dbResource"]))
		$res[] = $row;
}else{
	print("Error: Fetching state details");
}

if((count($res)==0)){
    print "<option value='' disabled selected = 'selected'>Select City</option>";
    print "<option value=''>No City Found </option>";
}else{
    print "<option value='' disabled selected = 'selected'>Select City</option>";
    foreach($res as $state) {
        print "<option value='".$state["id"]."'>".$state["name"]."</option>";
    }
}
?>