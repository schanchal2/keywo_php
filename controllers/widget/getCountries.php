<?php 
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	//print_r($conn); die;
$query 	= "SELECT country_id,name,sortname,currency_name,currency_code,country_code_number  FROM countries";
$result = runQuery($query, $conn);
if(noError($result)){
	$res = array();
	while($row = mysqli_fetch_assoc($result["dbResource"]))
		$res[] = $row;
}else{
	print("Error: Fetching state details");
}
foreach($res as $key => $country) {
	$arr["country"][$key] = $country;
}
//send JSON responce
print_r(json_encode($arr));
?>