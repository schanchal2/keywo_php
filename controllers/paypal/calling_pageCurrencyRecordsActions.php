<?php
$currentCurrrency="usd";

$jsonn= file_get_contents("https://bitpay.com/rates/usd");
$json=json_decode($jsonn,true);
$rate = $json["data"]["rate"];
echo $rate;
die();
session_start();
require_once("controller/config.php");
require_once("controller/utilities.php");

try
{
	//Open database connection
	$conn = createDbConnection($psServername, $psDbUsername, $psDbPassword, $psDbname);
	if(noError($conn)){
		$conn = $conn["errMsg"];
	}else{
		printArr("Database Error");
	}

	if($_GET["action"] == "list"){
		//Get record count
		$result = mysqli_query($conn,"SELECT COUNT(*) AS RecordCount FROM currentRatesRecord;");
		$row = mysqli_fetch_array($result);
		$recordCount = $row['RecordCount'];

		//Get records from database
		$result = mysqli_query($conn,"SELECT * FROM currentRatesRecord ORDER BY id LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
		//$result = "SELECT * FROM industry_verticals ORDER BY name LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";";
		//echo $result;
		//Add all records to an array
		//$rows = array();
		while($row = mysqli_fetch_array($result)){
		    $rows[] = $row;
		}

		//Return result to jTable
		$jTableResult = array();
		$jTableResult['Result'] = "OK";
		$jTableResult['TotalRecordCount'] = $recordCount;
		$jTableResult['Records'] = $rows;
		print json_encode($jTableResult);
	}

	


}
catch(Exception $ex)
{
    //Return error message
	$jTableResult = array();
	$jTableResult['Result'] = "ERROR";
	$jTableResult['Message'] = $ex->getMessage();
	print json_encode($jTableResult);
}

?>
