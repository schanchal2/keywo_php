<?php 

// PayPal settings
$paypal_email = 'vishal@searchtrade.com';
$return_url = 'https://searchtrade.com/staging/presaleindependence/coingame/keywordsale/views/paypal/payment-successful.htm';
$cancel_url = 'https://searchtrade.com/staging/presaleindependence/coingame/keywordsale/views/paypal/payment-cancelled.htm';
$notify_url = 'https://searchtrade.com/staging/presaleindependence/coingame/keywordsale/views/paypal/payments.php';
$_xclick = '_xclick';

$item_name = 'Test Item';
$item_amount = 5.00;


// Firstly Append paypal account to querystring
    $querystring .= "?business=".urlencode($paypal_email)."&";

    // Append amount& currency (£) to quersytring so it cannot be edited in html

    //The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
    $querystring .= "item_name=".urlencode($item_name)."&";
    $querystring .= "amount=".urlencode($item_amount)."&";

    //loop for posted values and append to querystring
    foreach($_POST as $key => $value){
        $value = urlencode(stripslashes($value));
        $querystring .= "$key=$value&";
    }

    // Append paypal return addresses
    $querystring .= "return=".urlencode(stripslashes($return_url))."&";
    $querystring .= "cmd=".urlencode(stripslashes($_xclick))."&";
    $querystring .= "cancel_return=".urlencode(stripslashes($cancel_url))."&";
    $querystring .= "notify_url=".urlencode($notify_url);

    // Append querystring with custom field
    //$querystring .= "&custom=".USERID;

    // Redirect to paypal IPN
    header('location:https://www.sandbox.paypal.com/cgi-bin/webscr'.$querystring);   exit();

?>