<?php
	session_start();

	/* Add Global variables */
	$docrootpath = __DIR__;
	$docrootpath = explode('/controllers', $docrootpath);
	$docrootpath = $docrootpath[0] . "/";
	// /* Add DB Management Files */
	require_once "{$docrootpath}config/config.php";
	require_once "{$docrootpath}config/db_config.php";
	/* Add Model */
	require_once("{$docrootpath}helpers/deviceHelper.php");
	require_once "{$docrootpath}helpers/arrayHelper.php";
	require_once "{$docrootpath}helpers/stringHelper.php";
	require_once "{$docrootpath}helpers/errorMap.php";
	require_once "{$docrootpath}helpers/coreFunctions.php";
	require_once("{$docrootpath}models/support/queryListModel.php");
	require_once("{$docrootpath}models/customer/customerModel.php");

	$ticketId = $_POST['ticketId'];
	$starCount = $_POST['starCount'];
	
	$returnArr = array();



	if (!isset($_SESSION["email"]) && empty($_SESSION["email"])) {
	    $returnArr['errCode'] = 100;
	    $returnArr['errMsg'] = 'Please Login';
	} else {
		$conn = createDBConnection('dbkeywords');

	    if(noError($conn)){
	        $conn = $conn["connection"];
	        $starReview = setCustomerReviews($ticketId,$starCount,$conn);
	        if (noError($starReview)) {
	        	$returnArr['errCode'] = $starReview['errCode'];
	    		$returnArr['errMsg'] = $starReview['errMsg'];
	        } else {
	        	$returnArr['errCode'] = $starReview['errCode'];
	    		$returnArr['errMsg'] = $starReview['errMsg'];
	        }
	    }else{
	        $returnArr['errCode'] = 1;
	    	$returnArr['errMsg'] = 'Database Error';
	    }		
	}

	echo json_encode($returnArr);
?>