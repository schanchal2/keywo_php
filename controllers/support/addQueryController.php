<?php

/**
 **********************************************************************************
 *                  addQueryController.php
 * ********************************************************************************
 *      This controller is used for Raising/Generating Tickets related to user Query.
 */

/* Add Seesion Management Files */
session_start();
header("Access-Control-Allow-Origin: *");

/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once("../../models/support/queryListModel.php");
require_once("../../models/customer/customerModel.php");

//DB connection

    $conn        = createDBConnection('dbkeywords');

    $regemail    = $_SESSION["email"];
    $require     = $userRequiredFields .",country";
    $requestUrl  = $NotificationURL.'v2/';
    $getUserInfo = getUserInfo($regemail, $requestUrl, $require);

        if(noError($getUserInfo)) {
            $getUserInfo   = $getUserInfo['errMsg'];
            $country       = $getUserInfo['country'];
        }

        if(noError($conn)){
            $conn = $conn["connection"];
        }else{
            print_r("Database Error");
        }

    $ticketID        = isset($_POST["ticketID"])?cleanQueryParameter($conn,cleanXSS($_POST["ticketID"])):"";
    $requestTpye     = isset($_POST["requestType"])?cleanQueryParameter($conn,cleanXSS($_POST["requestType"])):"";

    $keywordName     = isset($_POST["keywordName"])?cleanQueryParameter($conn,cleanXSS($_POST["keywordName"])):"";
    $keywordarray    = explode(" ",$keywordName);
    $keywordName     = json_encode($keywordarray);

    $payment         = isset($_POST["payment"])?cleanQueryParameter($conn,cleanXSS($_POST["payment"])):"";
    $amountITD       = isset($_POST["amountITD"])?cleanQueryParameter($conn,cleanXSS($_POST["amountITD"])):"";
    $hashPayment     = isset($_POST["hashPayment"])?cleanQueryParameter($conn,cleanXSS(["hashPayment"])):"";
    $BTCaddress      = isset($_POST["BTCaddress"])?cleanQueryParameter($conn,cleanXSS($_POST["BTCaddress"])):"";
    $paypal          = isset($_POST["paypal"])?cleanQueryParameter($conn,cleanXSS($_POST["paypal"])):"";
    $comment         = isset($_POST["comment"])?cleanQueryParameter($conn,cleanXSS($_POST["comment"])):"";
    $tradeType       = isset($_POST["tradeType"])?cleanQueryParameter($conn,cleanXSS($_POST["tradeType"])):"";
    $recEmail        = isset($_POST["recEmail"])?cleanQueryParameter($conn,cleanXSS($_POST["recEmail"])):"";
    $recNotification = isset($_POST["recNotification"])?cleanQueryParameter($conn,cleanXSS($_POST["recNotification"])):"";


// array of error in purchasing keyword
$queryList   = array(

    "requestType"     => $requestTpye,
    "keywordName"     => $keywordName,
    "payment"         => $payment,
    "amountITD"       => $amountITD,
    "hashPayment"     => $hashPayment,
    "BTCaddress"      => $BTCaddress,
    "paypal"          => $paypal,
    "comment"         => $comment,
    "tradeType"       => $tradeType,
    "recEmail"        => $recEmail,
    "recNotification" => $recNotification

);

    $updateQueryList = updateQueryList($ticketID,$regemail,$queryList,$country,$conn);
   if(noError($updateQueryList))
   {
       $returnArr["errCode"] = -1;
       $returnArr["errMsg"] = "Your ticket is created successfully.";
   }else
   {
       $returnArr["errCode"] = $updateQueryList["errMsg"];
       $returnArr["errMsg"] =$updateQueryList["errMsg"];
   }


echo json_encode($returnArr);

?>