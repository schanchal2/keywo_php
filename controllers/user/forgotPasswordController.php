<?php
    /*
    * --------------------------------------------------------------------------------------------
    *   ForgetPassword Controller
    *---------------------------------------------------------------------------------------------
    *   This controller is used to send email link to reset password
    */

    header("Access-Control-Allow-Origin: *");

    ini_set('default_charset','utf-8');
    header('Content-type: text/html; charset=utf-8');

    require_once('../../config/config.php');
    require_once('../../config/db_config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/arrayHelper.php');
    require_once('../../helpers/stringHelper.php');
    require_once ('../../models/user/authenticationModel.php');
    require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
    require_once "../../helpers/deviceHelper.php";
    require_once "../../helpers/coreFunctions.php";

    error_reporting(0);
    // add globla variales here

    $retArray = array();
    $reqArray = array();
    $extraArgs = array();

    // get request parameters
    $email =  urldecode($_POST['email']);
    $flag =  urldecode($_POST['flag']);
    // clean xss script
    $email = cleanXSS(trim($email));

    //for xml writing essential
    $xmlProcessor =new xmlProcessor();

    $xmlfilename="forget_password.xml";

    $xmlArray = initializeXMLLog($email);

    $xml_data['request']["data"]='';
    $xml_data['request']["attribute"]=$xmlArray["request"];

    $xml_data['step']["data"]='Forget Password Activation';

    $searchDbConn = createDBConnection('dbsearch');

    if(noError($searchDbConn)){
          $searchDbConn = $searchDbConn["connection"];

          $logStorePathUserMgmt=$logPath["userManagement"];

          $msg = "Success : Database connection";
          $retArray = setErrorStack($retArray, -1, $msg, $extraArgs);

          $xml_data["step1"]["data"] = '1. '.$msg;

          if(isset($email) && !empty($email)){
                $msg = "Success : Email field found";
                $retArray = setErrorStack($retArray, -1, $msg, $extraArgs);

                $xml_data['step2']["data"]="2. Get Parameters for Forget Password 1]User Email: " . $email ;

                $activateForget = forgotPassword($email,$flag);
                if(noError($activateForget)){
                      $activateForget = $activateForget["errMsg"];

                      $msg = "A verification email has been sent to registered email address, Please verify.";
                      $extraArgs["data"]["first_name"] = $activateForget["first_name"];
                      $extraArgs["data"]["last_name"]  = $activateForget["last_name"];
                      $errCode = $activateForget["errCode"];
                      $retArray = setErrorStack($retArray, $errCode, $msg, $extraArgs);

                      $xml_data["step3"]["data"] = '3. Success : Send verification link to registed email Id.';


                            $msg = "Success : Forget password analytics update successfully.";
                            $retArray = setErrorStack($retArray, -1, $msg, $extraArgs);

                            $xml_data["step4"]["data"] = '4. '.$msg;

                    /******************** for logs user analytics ****************/
                            $responseArr["errCode"]="-1";
                            $responseArr["errMsg"]=$msg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }else{

                    $msg = $activateForget["errMsg"];
                    $errCode = $activateForget["errCode"];
                    $retArray = setErrorStack($retArray, $errCode, $msg, $extraArgs);

                    $xml_data["step3"]["data"] = '3. '.$msg;
                }
          }else{
                $msg = "Failure : Email field is missing.";
                $retArray = setErrorStack($retArray, 2, $msg, $extraArgs);

                $xml_data["step2"]["data"] = '2. '.$msg;
          }
    }else{
          $msg = "Failure : Database connection.";
          $retArray = setErrorStack($retArray, 1, $msg, $extraArgs);

          $xml_data["step1"]["data"] = '1. '.$msg;
    }

    // create and update log file
    $xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

    echo json_encode($retArray);

?>
