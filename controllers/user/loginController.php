<?php
/* --------------------------------------------------------------------------------------------
*   Login Controller
*---------------------------------------------------------------------------------------------
*   This controller is used to authenticate the valid user using user name and password
*/
// start the session.
session_start();

ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

require_once('../../config/db_config.php');
require_once('../../config/config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../models/user/authenticationModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/coreFunctions.php";


error_reporting(0);
// add globla variales here
$returnArr = array();
$extraArgs = array();
$paramArray = array();

// get request parameters
$emailHandle = urldecode($_POST['email']);
$password = urldecode($_POST['password']);

// clean xss script
$emailHandle = cleanXSS(trim($emailHandle));

//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xmlfilename = "user_login.xml";
$xmlArray = initializeXMLLog($emailHandle);
$xml_data['step']["data"] = 'User Login';

/*************************************** log time temp session ******************************************/
$_SESSION["timezone"] = $_POST["timeZone"];
$xmlArray["activity"]["timestamp"]= uDateTime("Y-m-d h:i:s A",date("Y-m-d h:i:s A"));
unset($_SESSION["timezone"]);
/*************************************** log time temp session ******************************************/
if(!isset($_COOKIE["keywo_ver_1"]) && !isset($_SESSION['email'])){

    $searchDbConn = createDBConnection('dbsearch');
    if (noError($searchDbConn)) {
        $searchDbConn = $searchDbConn["connection"];
        $logStorePathUserMgmt = $logPath["userManagement"];

        $msg = "Success : Search database connection";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

        $xml_data["step1"]["data"] = '1. ' . $msg;

        // Email validation
        if (isset($emailHandle) && !empty($emailHandle)) {
            $msg = "Success : Email field found.";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $xml_data["step2"]["data"] = '2. Success : Email/Handle found i.e. ' . $emailHandle;

            // password validation
            if (isset($password) && !empty($password)) {
                $msg = "Success : Password field found";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                if (!empty($password)) {
                    $pwdStatus = 'Yes';
                } else {
                    $pwdStatus = 'No';
                }

                $xml_data['step3']["data"] = "3. Get Parameters for Login 1]User Email/Handle: " . $emailHandle . " 2]Password-: " . $pwdStatus;

                $paramArray["emailHandle"] = $emailHandle;
                $paramArray["password"] = $password;

                $getLoginResponse = login($paramArray);
                if (noError($getLoginResponse)) {
                    $getLoginResponse = $getLoginResponse["errMsg"];

                    $_SESSION["email_handle"] = $emailHandle;
                    $_SESSION["email"] = $getLoginResponse["email"];
                    $_SESSION["id"] = $getLoginResponse["_id"];
                    $_SESSION["first_name"] = $getLoginResponse["first_name"];
                    $_SESSION["account_handle"] = $getLoginResponse["account_handle"];
                    $_SESSION["last_name"] = $getLoginResponse["last_name"];
                    $_SESSION["system_mode"] = $getLoginResponse["system_mode"];
                    $_SESSION["gender"] = $getLoginResponse["gender"];
                    $_SESSION["pending_qualified_search"] = $getLoginResponse["no_of_qualified_searches_pending"];

                    /********************** set user timezone start *******************************/
                    $result = setUserNotifyInfo($_SESSION["id"], "timezone", $_POST["timeZone"]);
                    if(noError($result))
                    {
                        $_SESSION["timezone"] = $_POST["timeZone"];
                    }else{
                        $_SESSION["timezone"] = $_POST["timeZone"];
                    }
                    /********************** set user timezone end *******************************/

                    // inititalize xml logs activity


                    $xml_data['request']["data"] = '';
                    $xml_data['request']["attribute"] = $xmlArray["request"];

                    $returnArr["errCode"] = -1;
                    $extraArgs = $getLoginResponse;
                    $msg = "Success : User authenticatication";
                    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                    $xml_data["step4"]["data"] = '4. ' . $msg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]="-1";
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/

                } else {
                    //  $msg = "Failed : User authenticatication";
                    $msg = $getLoginResponse["errMsg"];
                    $errCode = $getLoginResponse["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);

                    $xml_data["step4"]["data"] = '4. ' . $msg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=$errCode;
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/

                }
            } else {

                $msg = "Failure : Password field not found";
                $returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

                $xml_data["step2"]["data"] = '2. ' . $msg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="3";
                $responseArr["errMsg"]=$msg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }
        } else {
            $msg = "Failure : Email field not found";
            $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

            $xml_data["step2"]["data"] = '2. ' . $msg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]="2";
            $responseArr["errMsg"]=$msg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }
    } else {
        $msg = "Failure : Unable to create search database connection";
        $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);

        $xml_data["step1"]["data"] = '1. ' . $msg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]="1";
        $responseArr["errMsg"]=$msg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/

    }

}else{
    $xml_data['request']["data"] = '';
    $xml_data['request']["attribute"] = $xmlArray["request"];

    $returnArr["errCode"] = -1;
    /*$extraArgs = $getLoginResponse;*/
    $msg = "Success : Session already exist";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    $xml_data["step4"]["data"] = '1. ' . $msg;

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]="-1";
    $responseArr["errMsg"]=$msg;
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
}


// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);


echo json_encode($returnArr);

?>
