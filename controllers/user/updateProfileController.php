<?php

session_start();

header("Access-Control-Allow-Origin: *");

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/stringHelper.php');

$profileUpdateData = $_POST["data"];

$email = $_SESSION["email"];
$userId = $_SESSION["id"];

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}

$returnArr = array();

if(isset($_POST["flag_status"]) && !empty($_POST["flag_status"]) && $_POST["flag_status"] == 999){
    $profileInfo = $_POST["profile_info"];
}else{
    $profileUpdateData = $_POST["data"];
    $profileUpdateData = json_decode($profileUpdateData, true);

    $profileInfo = $profileUpdateData["profile_info"];
}

$nextPage = $profileUpdateData["next"];

if($profileInfo == "profile"){

    $firstName          =   cleanXSS(trim(urldecode($profileUpdateData["first_name"])));
    $lastName           =   cleanXSS(trim(urldecode($profileUpdateData["last_name"])));
    $short_desc          =   cleanXSS(trim(urldecode($profileUpdateData["short_desc"])));
    $mobileNumber       =   cleanXSS(trim(urldecode($profileUpdateData["mobile_number"])));
    $gender             =   cleanXSS(trim(urldecode($profileUpdateData["gender"])));
    $address1           =   cleanXSS(trim(urldecode($profileUpdateData["address1"])));
    $address2           =   cleanXSS(trim(urldecode($profileUpdateData["address2"])));
    $zip                =   cleanXSS(trim(urldecode($profileUpdateData["zip"])));
    $city               =   cleanXSS(trim(urldecode($profileUpdateData["city"])));
    $country            =   cleanXSS(trim(urldecode($profileUpdateData["country"])));
    $state              =   cleanXSS(trim(urldecode($profileUpdateData["state"])));
    $dateOfBirth        =   cleanXSS(trim(urldecode($profileUpdateData["date_of_birth"])));
    $interest           =   cleanXSS(trim(urldecode($profileUpdateData["interest"])));
    $acct_handle        =   cleanXSS(trim(urldecode($profileUpdateData["acct_handle"])));
    $countryCode        =   cleanXSS(trim(urldecode($profileUpdateData["country_code"])));
    $hobbies            =   cleanXSS(trim(urldecode($profileUpdateData["my_hobbies"])));
    $profilePic         =   cleanXSS(trim(urldecode($profileUpdateData["profile_pic"])));
    $imgName            =   cleanXSS(trim(urldecode($profileUpdateData["img_name"])));
    $imgType            =   cleanXSS(trim(urldecode($profileUpdateData["img_type"])));
    $imgSize            =   cleanXSS(trim(urldecode($profileUpdateData["img_size"])));

    if(empty($mobileNumber)){
        $mobileNumber = 0;
    }
    if(empty($zip)){
        $zip = 0;
    }

    if(isset($imgName) && !empty($imgName)){
        $imgName = uniqid() . '.' . end(explode('.', basename($imgName)));
    }else if(empty($profilePic) && empty($imgName)){
        $imgName = $rootUrlImages."default_profile.jpg";
    }else if(isset($profilePic) && !empty($profilePic)){
        $imgName = $profilePic;
    }

    if(isset($acct_handle) && empty($acct_handle)){
        $acct_handle = $_SESSION["account_handle"];
    }

    // upload image file on server
    $uploadImg = uploadProfileImage($acct_handle, $imgName);

    // send request to update profile API
    $apiTextReq = "user_id={$userId}";
    $requestType = "PUT";
    $apiURLEndPoint = "updateUserDetails";

    if(isset($profileUpdateData['type']) && $profileUpdateData['type'] == 'ftUser_new'){
        $requestParameter = "first_name=".$_SESSION["first_name"]."&last_name=".$_SESSION["last_name"]."&card_userinfo=".$short_desc."&city=".$city."&state=".$state."&country=".$country."&date_of_birth=".$dateOfBirth."&gender=".$gender."&profile_pic=".$imgName."&mobile_number=".$mobileNumber."&zip=".$zip;
    }else if(isset($profileUpdateData['type']) && $profileUpdateData['type'] == 'ftUser_old'){
        $requestParameter = "first_name=".$_SESSION["first_name"]."&last_name=".$_SESSION["last_name"]."&card_userinfo=".$short_desc."&city=".$city."&state=".$state."&country=".$country."&date_of_birth=".$dateOfBirth."&gender=".$gender."&profile_pic=".$imgName."&account_handle=".$acct_handle."&mobile_number=".$mobileNumber."&zip=".$zip;
    }else{
        $requestParameter = "first_name=".$firstName."&last_name=".$lastName."&city=".$city."&zip=".$zip."&state=".$state."&country=".$country."&address2=".$address2."&address1=".$address1."&mobile_number=".$mobileNumber."&date_of_birth=".$dateOfBirth."&countryCode=".$countryCode."&hobbies=".$hobbies."&gender=".$gender."&profile_pic=".$imgName;
    }

}else if($profileInfo == "education"){
    $schoolInfo             =   $_POST["school_info"];
    $universityInfo         =   $_POST["university_info"];
    $universityId           =   $_POST["universityId"];
    $curricularActivities   =   $_POST["curricular_acvities"];

    $schoolInfo = json_encode($schoolInfo);
    $universityInfo = json_encode($universityInfo);

    $apiTextReq = "user_id={$userId}&info=education";
    $requestType = "PUT";
    $apiURLEndPoint = "eduworkinfo/education";
    $requestParameter = "school_info=".$schoolInfo."&university_details=".$universityInfo."&university=".$universityId."&activity=".$curricularActivities;

}else if($profileInfo == "work"){

    $companyInfo    =   $_POST["company_info"];
    $skills         =   $_POST["skills"];

    $companyInfo = json_encode($companyInfo);

    $apiTextReq = "user_id={$userId}&info=work";
    $requestType = "PUT";
    $apiURLEndPoint = "eduworkinfo/work";
    $requestParameter = "companyInfo=".$companyInfo."&skills=".$skills;

}


$updateProfile = updateUserProfile($userId, $apiTextReq, $requestType, $apiURLEndPoint, $requestParameter);
if(noError($updateProfile)){
    $successMsg = '';

    // send request to update profile API to wallet
    $apiTextReq = "user_id={$userId}&gender={$gender}&country={$country}";
    $requestType = "PUT";
    $apiURLEndPoint = "setuserdetails";

    if($profileInfo == "profile"){
        $status = array();

        /* Update user details in walllet */
        $setUserDeatils = setUserDetails($userId, $apiTextReq, $requestType, $apiURLEndPoint, $requestParameter);
        if(noError($setUserDeatils)){
            $status = $setUserDeatils;

            /* Update session first name and last name after profile update */
            if((isset($firstName) && !empty($firstName)) && (isset($lastName) && !empty($lastName))){

                $_SESSION['first_name'] = ucfirst(strtolower($firstName));
                $_SESSION['last_name'] = ucfirst(strtolower($lastName));
            }

            if($profileUpdateData['type'] == 'ftUser_new'){
                $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $profileInfo);
            }else if($profileUpdateData['type'] == 'ftUser_old'){
                $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $profileInfo,$profileUpdateData['type'],$acct_handle);
            }

            if(noError($status)){
                $successMsg = "Profile information updated successfully.";

                $_SESSION["gender"] = $gender;
                if($profileUpdateData['type'] == 'ftUser_old'){
                    $_SESSION["account_handle"] = $acct_handle;
                }

            }else{
                /* Error message for already profile update */
                if($status["errCode"] == 101){
                    $successMsg = "You have already update yor profile";
                }else{

                    $successMsg = "failure: You need to enter information in all the required fields.";
                }
            }
        }else{
            $successMsg = "failure: You need to enter information in all the required fields.";
        }

    }else if($profileInfo == "education"){
        $successMsg = "Education information update successfully";
    }else if($profileInfo == "work"){
        $successMsg = "Work information update successfully";
    }

    $errCode = '';
    if($successMsg == 'failure: You need to enter information in all the required fields.'){
        $errCode = 20;
    }else{
        $errCode = -1;
    }
    $returnArr["errCode"] = $errCode;
    $returnArr["errMsg"] = $successMsg;
    $returnArr["nextPage"] = $nextPage;
}else{
    $returnArr["errCode"] = $errCode;
    $returnArr["errMsg"] = "Error : {$updateProfile['errMsg']}";
}

echo json_encode($returnArr);

?>