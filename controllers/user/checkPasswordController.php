<?php
/**
 **********************************************************************************
 *                  setJsonController.php
 * ********************************************************************************
 *      This controller is used for Setting Json File For followers and followings Details to social
 */

/* Add Seesion Management Files */
session_start();
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/social/socialModel.php";

error_reporting(0);

$returnArr  = array();
$extraArgs  = array();

// get request parameters

// clean xss script
if(isset($_SESSION['email']) && !empty($_SESSION['email'])){
    $password         =   cleanXSS(trim(urldecode($_POST["password"])));
    $getPasswordFlag = getPasswordFlag($password,$_SESSION['id']);
    if(noError($getPasswordFlag)){
        $errMsg                             = 'Success : Password Correct';
        $returnArr                          = setErrorStack($returnArr,  -1, $errMsg, $extraArgs);
    }else{
        $errMsg                             = 'Failure  : Password Incorrected';
        $returnArr                          = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }
}else{
    $msg = "Failure : Email field not found";
    $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

    $xml_data['step2']["data"]="2. " . $msg ;
}
echo json_encode($returnArr);
?>
