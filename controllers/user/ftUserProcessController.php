<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 29/5/17
 * Time: 6:31 PM
 */

header("Access-Control-Allow-Origin: *");
header('Content-Type: text/html; charset=utf-8');
session_start();

$docrootpath=explode("controllers/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];

require_once ("{$docrootpath}config/config.php");
require_once ("{$docrootpath}config/db_config.php");
require_once ("{$docrootpath}helpers/errorMap.php");
require_once ("{$docrootpath}helpers/arrayHelper.php");
require_once ("{$docrootpath}helpers/coreFunctions.php");
require_once ("{$docrootpath}helpers/stringHelper.php");
require_once ("{$docrootpath}models/social/socialModel.php");
require_once ("{$docrootpath}models/social/commonFunction.php");

$page = "";
$nextPage = "";
$pageSkip = '';
$sid = '';

$profileUpdateData = $_POST["data"];
if(isset($_POST["data"]) && !empty($_POST["data"])){
    $profileUpdateData = json_decode($profileUpdateData, true);
    $page = strtolower($profileUpdateData['page']);
    $nextPage = strtolower($profileUpdateData['next']);
    $sid  = cleanXSS(trim(urldecode($profileUpdateData['sid'])));
}else{
    $page = strtolower($_POST['page']);
    $pageSkip = $page;
    if($page == 'share_skip'){
        $page = explode('_',$_POST['page']);
        $page = $page[0]; // get value 'share'
    }
    $nextPage = strtolower($_POST['next']);
    $sid  = cleanXSS(trim(urldecode($_POST['sid'])));
}

$currSid = session_id();

$returnArr = array();

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    if($currSid == $sid){

        if(isset($_POST) && !empty($_POST)){

            $email         = $_SESSION["email"];
            $userId        = $_SESSION["id"];
            $accountHandle = $_SESSION["account_handle"];

            $kwdDbConn = createDBConnection("dbkeywords");
            if(noError($kwdDbConn)){
                $kwdDbConn = $kwdDbConn["connection"];
                $errCode = '';
                $errMsg = '';

                switch ($page) {
                    case "follow":
                        /*Perform follow functionality*/
                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                        /*function to call an api to send following list for user to update memcache data*/
                        setFollowingToMemcache($userId, $accountHandle);
                        if(noError($status)){
                            $errCode = -1;
                            $errMsg = "Success: You are now following 5 accounts.";
                        }
                        break;
                    case "claim":
                        /*Perform follow functionality*/
                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                        if(noError($status)){
                            $errCode = -1;
                            $errMsg = "Success: Update first time user success";
                        }
                        break;
                    case "like":
                        /*Perform like and update status*/
                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                        if(noError($status)){
                            $errCode = -1;
                            $errMsg = "Success: Update first time user success";
                        }
                        break;
                    case "create":
                        /*Create new post*/
                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                        if(noError($status)){
                            $errCode = -1;
                            $errMsg = "Success: Update first time user success";
                        }
                        break;
                    case "share":
                        /*Share post*/
                        $nextPage = 'finish';
                        $page = $pageSkip;
                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);
                        if(noError($status)){
                            $errCode = -1;
                            $errMsg = "Success: Update first time user status";
                        }
                        break;
                }

                $returnArr['errCode'] = $errCode;
                $returnArr['errMsg'] = $errMsg;
                $returnArr["nextPage"] = $nextPage;

            }else{
                $returnArr["errCode"] = 3;
                $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
            }

        }else{
            $returnArr['errCode'] = 2;
            $returnArr['errMsg'] = 'Error : Fields does not match';
        }
    }else{
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = 'Error : unauthorized access';
    }

}else{
    $returnArr['errCode'] = 3;
    $returnArr['errMsg'] = 'Error : unauthorized access';
}

echo json_encode($returnArr);

?>