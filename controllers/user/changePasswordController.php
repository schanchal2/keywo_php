<?php
/**
 **********************************************************************************
 *                  setJsonController.php
 * ********************************************************************************
 *      This controller is used for Setting Json File For followers and followings Details to social
 */

/* Add Seesion Management Files */
session_start();
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/commonFunction.php";
require_once('../../models/user/authenticationModel.php');

error_reporting(0);

$returnArr  = array();
$extraArgs  = array();

$id            = $_SESSION['id'];

if(count($_POST)==0){
    $oldPassword                   =   $_SESSION["currentPassword"];
    $password                      =   $_SESSION["newPassword"];
    $confirmPassword               =   $_SESSION["confirmPassword"];
}else{
    $oldPassword                   =   $_POST["currentPassword"];
    $password                      =   $_POST["newPassword"];
    $confirmPassword               =   $_POST["confirmPassword"];

    $_SESSION["currentPassword"]   = base64_encode($oldPassword);
    $_SESSION["newPassword"]       = base64_encode($password);
    $_SESSION["confirmPassword"]   = base64_encode($confirmPassword);
}

// clean xss script
$email            =   cleanXSS(trim($_SESSION["email"]));


$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$changePasswordFlag = "Change Password";
//get Client Ip
$getRemoteIP = getClientIP();

//get browser Information
$getBrowserName = getBrowserName();

//Get OS
$getOSInformation = getOSInformation();

//for xml writing essential
$xmlProcessor =new xmlProcessor();

$xmlfilename="change_password.xml";

$xmlArray = initializeXMLLog($email);
$logStorePathUserMgmt=$logPath["userManagement"];

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];

$xml_data['step']["data"]='User Reset Password';


$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if(!isset($_SESSION["verify_status"])){
    $_SESSION['redirect_url'] = $returnURl;
    $_SESSION['changePass']   = $changePasswordFlag;
    $errMsg = "Success : 2FA is Not set";
    $xml_data['step'.++$i]["data"] = $i.". Success :  2FA is Not set";
    $returnArr["errCode"] = -2;
    $returnArr["errMsg"] = $errMsg;

    //redirecting to 2FA page
    $msg = "Success : Redirecting to 2FA page";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr["errCode"] = -2;
    $_SESSION['form_data'] = json_encode($_POST);
    $_SESSION['xml_step'] = $i;

}else {
    if (isset($_SESSION['email']) && !empty($_SESSION['email'])) {
        $msg = "Success : Email field found";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

        $xml_data['step2']["data"] = "2. " . $msg;

        if (isset($password) && !empty($password)) {
            $msg = "Success : Password field found";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $logWritePassword = "yes";

            $xml_data['step3']["data"] = "3. " . $msg;

            if (isset($confirmPassword) && !empty($confirmPassword)) {
                $msg = "Success : Confirm password field found";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                $logWriteConfirmPassword = "yes";

                $xml_data['step4']["data"] = "4. " . $msg;
                $setPasswordReset = setPassword($id, $oldPassword, $password, $confirmPassword, $getRemoteIP, $getBrowserName, $getOSInformation['name']);
                if (noError($setPasswordReset)) {
                    $msg = "Password Change successfully";
                    $extraArgs["data"] = $setPasswordReset["errMsg"];
                    $errCode = $setPasswordReset["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);

                    $xml_data['step5']["data"] = "5. Get Parameters for Reset Password 1]User Email: " . $email . " Password: " . $logWritePassword . " Confirm Password: " . $logWriteConfirmPassword;
                    $xml_data['step6']["data"] = "6. " . $msg;

                    //Unset All session
                    unset($_SESSION['security_type']);
                    unset($_SESSION['xml_step']);
                    unset($_SESSION['form_data']);
                    unset($_SESSION['activity_id']);
                    unset($_SESSION['redirect_url']);
                    unset($_SESSION['xml_file']);
                    unset($_SESSION['verify_status']);
                    unset($_SESSION['currentPassword']);
                    unset($_SESSION['newPassword']);
                    unset($_SESSION['confirmPassword']);
                    unset($_SESSION['changePass']);


                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]="-1";
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/

                } else {
                    $msg = $setPasswordReset["errMsg"];
                    $errCode = $setPasswordReset["errCode"];
                    $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);
                    $xml_data['step6']["data"] = "6. " . $msg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=$errCode;
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }

            } else {
                $msg = "Failure : Confirm Password filed is missing";
                $returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                $xml_data['step4']["data"] = "4. " . $msg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=4;
                $responseArr["errMsg"]=$msg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }
        } else {
            $msg = "Failure : Password filed is missing";
            $returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

            $xml_data['step3']["data"] = "3. " . $msg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=3;
            $responseArr["errMsg"]=$msg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }
    } else {
        $msg = "Failure : Email field not found";
        $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

        $xml_data['step2']["data"] = "2. " . $msg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$msg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }
}

// create and update log file
$xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);
?>
