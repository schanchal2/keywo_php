<?php
/*
* --------------------------------------------------------------------------------------------
*   Resend Verification Controller
*---------------------------------------------------------------------------------------------
*   This controller is used to resend activation link to registed users.
*/

// start the session.
//session_start();
header("Access-Control-Allow-Origin: *");

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

require_once ('../../models/user/authenticationModel.php');;
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Helpers Files */
require_once "../../helpers/deviceHelper.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";


error_reporting(0);
// add globla variales here

$returnArr  = array();
$extraArgs  = array();
$paramArray = array();

// get request parameters
$email    =   urldecode($_POST['email']);
$flag     =   urldecode($_POST['flag']);

// clean xss script
$email    =   cleanXSS(trim($email));

//for xml writing essential
$xmlProcessor =new xmlProcessor();

$xmlfilename="resend_activation_link.xml";

$xmlArray = initializeXMLLog($email);

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];

$xml_data['step']["data"]='Resend Activation Link';

$searchDbConn = createDBConnection('dbsearch');

if(noError($searchDbConn)){
      $searchDbConn = $searchDbConn["connection"];
      $logStorePathUserMgmt=$logPath["userManagement"];

      $msg = "Success : Database connection";
      $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

      $xml_data["step1"]["data"] = '1. '.$msg;

      if(isset($email) && !empty($email)){
          $msg = "Success : Email field found.";
          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

          $xml_data['step2']["data"]="2. Get Parameters for Resend Activation 1]User Email: " . $email ;

          // get user_id, password and salt value of user from @wallet server.
          $userRequiredData = $userRequiredFields."user_id,password,salt,first_name,last_name,active";
          $requestURL = $walletURLIP . 'api/v3/';

          $getUserDetails = getUserInfo($email, $requestURL, $userRequiredData);
          if(noError($getUserDetails)){

              $getUserDetails = $getUserDetails["errMsg"];
              $isAccountActive = $getUserDetails["active"];

                if($isAccountActive == 0){

                      // send parameters to sendVerificationAuth() method.
                      $param = array("account_id" => $getUserDetails["_id"], "email" => $email, "first_name" => $getUserDetails["first_name"], "last_name" => $getUserDetails["last_name"], "password" => $getUserDetails["password"], "salt" => $getUserDetails["salt"], "email_status" => 2);

                      $getResendResponse = sendVerificationAuth($param);
                      if(noError($getResendResponse)){
                          $getResendResponse = $getResendResponse["errMsg"];
                          $msg = $getResendResponse;
                          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                          $xml_data["step3"]["data"] = '3. Resend verification email sent successfully';
                          /******************** for logs user analytics ****************/
                          $responseArr["errCode"]="-1";
                          $responseArr["errMsg"]= "Resend verification email sent successfully";
                          $xml_data['response']["data"] = "";
                          $xml_data['response']["attributes"] = $responseArr;
                          /******************** for logs user analytics ****************/
                      }else{
                          $errCode = $getResendResponse["errCode"];
                          $msg = $getResendResponse["errMsg"];
                          $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);

                          $xml_data["step4"]["data"] = '4. Error: '.$msg;
                          /******************** for logs user analytics ****************/
                          $responseArr["errCode"]=$errCode;
                          $responseArr["errMsg"]= $msg;
                          $xml_data['response']["data"] = "";
                          $xml_data['response']["attributes"] = $responseArr;
                          /******************** for logs user analytics ****************/
                      }
                }else{
                    $msg = "Account is already activated";
                    $returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                    $xml_data["step3"]["data"] = '4. '.$msg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"] = 4;
                    $responseArr["errMsg"] = $msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

              if($getUserDetails["errCode"] == 4){
                  $msg = "There is no user exist with this email address";
              }else{
                  $msg = "Server Error!!!";
              }
                $returnArr = setErrorStack($returnArr, $getUserDetails["errCode"], $msg, $extraArgs);

                $xml_data["step3"]["data"] = '3. '.$msg;

              /******************** for logs user analytics ****************/
              $responseArr["errCode"] = $getUserDetails["errCode"];
              $responseArr["errMsg"] = $msg;
              $xml_data['response']["data"] = "";
              $xml_data['response']["attributes"] = $responseArr;
              /******************** for logs user analytics ****************/
            }
      }else{
          $msg = "Failure : Email field not found";
          $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

          $xml_data["step2"]["data"] = '2. '.$msg;
          /******************** for logs user analytics ****************/
          $responseArr["errCode"]=2;
          $responseArr["errMsg"]= $msg;
          $xml_data['response']["data"] = "";
          $xml_data['response']["attributes"] = $responseArr;
          /******************** for logs user analytics ****************/
      }
}else{
      $msg = "Failure : Database connection.";
      $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);

        $xml_data["step1"]["data"] = '1. '.$msg;

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]=1;
    $responseArr["errMsg"]= $msg;
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
    /******************** for logs user analytics ****************/
}

// create and update log file
$xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);
?>
