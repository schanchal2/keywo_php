<?php
/**
 **********************************************************************************
 *                  registerController.php
 * ********************************************************************************
 *      This controller is used for register a new user to keywo
 */

/* Add Seesion Management Files */
//session_start();
header("Access-Control-Allow-Origin: *");

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once "../../models/analytics/domain_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";



/* Mute all syntax and warning error */
error_reporting(0);

$retArray = array();
$reqArray = array();
$logStorePathUserMgmt=$logPath["userManagement"];

/* Get request type for proper action and decode request parameters */
$req_type = urldecode($_POST['requestType']);
$req_value = urldecode($_POST['requestValue']);

/* Clean XSS value from  parameters*/
$req_type = cleanXSS(trim($req_type));
$req_value = cleanXSS(trim($req_value));

//for xml writing essential
$xmlProcessor =new xmlProcessor();
$xmlfilename="UserSignup.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog(trim(urldecode($_POST["email"])));

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];

$errMsg = "User registration process start.";
$xml_data['step1']["data"] = "1. {$errMsg}";

$connSearch = createDBConnection('dbsearch');
if(noError($connSearch)){
    $connSearch = $connSearch["connection"];

    $errMsg = "Database connection success.";
    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
    $xml_data["step2"]["data"] = "2. {$errMsg}";

    if($req_type != 'signup'){

        switch ($req_type) {
            case 'account_handle':
                $acctArray = checkFieldStatus($req_type, $req_value);
                $handle=strtolower($req_value);
                $email=strtolower(cleanXSS(trim(urldecode($_POST["emailUser"]))));
                /* Check when request come from ft_profile page for old user */
                if(empty($email)){
                    $email = $_SESSION['email'];
                }

                if(noError($acctArray)){
                    $result=checkHandleReserved($handle,$email);

                    if(noError($result))
                    {
                        $errMsg = $result['errMsg'];
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    }else{
                        $errMsg = $result['errMsg'];
                        $retArray = setErrorStack($retArray, $result['errCode'], $errMsg, $extraArgs);
                    }
                }else{
                    $errMsg = $acctArray['errMsg'];
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                }

                //echo json_encode($retArray);
                break;

            case 'email':

                $checkDomain = checkEmailDomainStatus($req_value,$connSearch);
                if(noError($checkDomain))
                {
                    if($checkDomain['errMsg'] == 'NOT BLOCKED'){
                        $errMsg = $checkDomain['errMsg'];
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                        $checkArray = checkFieldStatus($req_type, $req_value);
                        if(noError($checkArray)){
                            $errMsg = $checkArray['errMsg'];
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                        }else{

                            $errMsg = $checkArray['errMsg'];
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                        }
                    }else{
                        $errMsg = $checkDomain['errMsg'];
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    }

                }else{
                    $errMsg = $checkDomain['errMsg'];
                    $retArray = setErrorStack($retArray, 43, $errMsg, $extraArgs);
                }

                //echo json_encode($retArray);
                break;

            case 'referal':
                $refArray = checkFieldStatus($req_type, $req_value);
                if(noError($refArray)){
                    $errMsg = $refArray['errMsg'];
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                }else{
                    $errMsg = $refArray['errMsg'];
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                }
                break;

            case 'ipstatus':
                $refArray = signupIpBlocker($req_value);
                if(noError($refArray)){
                    $errMsg = $refArray['errMsg'];
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                }else{
                    $errMsg = $refArray['errMsg'];
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                }
                break;

        }

    } else {


        $user_email    = trim(urldecode($_POST["email"]));
        $referral = '';

        /* Accept and validate request parameters*/
        if(isset($_POST["referral"]) && !empty($_POST["referral"])){
            $referral    = urldecode(urlencode($_POST["referral"]));
        }

        $reqArray['email']            = $user_email;
        $reqArray['referral']         = $referral;
        $reqArray['first_name']       = cleanXSS(trim(urldecode($_POST["first_name"])));
        $reqArray['last_name']        = cleanXSS(trim(urldecode($_POST["last_name"])));
        $reqArray['account_handle']   = cleanXSS(trim(urldecode($_POST["handle"])));
        $reqArray['password']         = decodeRequestParameter($_POST["password"]);
        $reqArray['gender']           = decodeRequestParameter($_POST["gender"]);
        $reqArray['confirm_password'] = decodeRequestParameter($_POST["confirm_password"]);
        /*$reqArray['profile_pic']      = decodeRequestParameter($_POST["profile_pic"]);
        $reqArray['mobile_number']    = decodeRequestParameter($_POST["mobile_number"]);*/
        $reqArray['country']          = decodeRequestParameter($_POST["country"]);
        $reqArray['city']             = decodeRequestParameter($_POST["city"]);
        $reqArray['client_IP']        = decodeRequestParameter($xmlArray["activity"]["userIp"]);
        $reqArray['app_status']       = trim(urldecode($_POST["app_status"]));
        $reqArray['agree']            = trim(urldecode($_POST["agree"]));

        $reqArray['flag'] = 1;
        $appStatus = $reqArray['app_status'];

        if ($appStatus == "TRUE") {
            $reqArray['flag']  = trim(urldecode($_POST["flag"]));
        }

        $errMsg = "Getting request parameters success.";
        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
        $xml_data["step3"]["data"] = "3. {$errMsg}";

        /* Get captcha response during signup */
        $captcha  = $_POST["grecaptcharesponse"];
        $captchaResp = getGoogleCaptchaResponse($captcha, $reqArray['client_IP']);

        if(noError($captchaResp)){

            $errMsg = "Getting captcha success.";
            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
            $xml_data["step4"]["data"] = "4. {$errMsg}";

            if(strlen($reqArray['first_name']) <= 50 && strlen($reqArray['last_name']) <= 50 ){

                $errMsg = "First name/Last name validation success.";
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step5"]["data"] = "5. {$errMsg}";

                if (isset($reqArray['email']) && !empty($reqArray['email']) && isset($reqArray['password']) && !empty($reqArray['password']) && isset($reqArray['confirm_password']) && !empty($reqArray['confirm_password']) && isset($reqArray['agree']) && $reqArray['agree'] == 'true' && $reqArray['flag'] != '') {

                    $errMsg = "Required fields validation success";
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step6"]["data"] = "6. {$errMsg}";

                    /* Get user default mode from admin */
                    $getSystemDefaultMode = getSettingsFromSearchAdmin($connSearch);

                    if(noError($getSystemDefaultMode)){

                        $reqArray['system_mode'] = $getSystemDefaultMode['data']['default_system_mode'];

                        $errMsg = "Getting Settings from search success.";
                        $xml_data["step7"]["data"] = "7. {$errMsg}";
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                        /* register method defined in authentication Model */
                        $regArray = register($reqArray);


                        $ref_email=$regArray["errMsg"]["r_email"];

                        /* Secure log to display user sensitive data */
                        if(!empty($reqArray['password'])){
                            $reqArray['password'] = 'yes';
                            $reqArray['confirm_password'] = 'yes';
                        }
                        $xml_data['parameters']["data"]="";
                        $xml_data['parameters']["attributes"]=$reqArray;

                        if(noError($regArray)){

                            $errMsg = "Success : User Registration";
                            $xml_data["step8"]["data"] = "8. {$errMsg}";
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                            $regArray = $regArray["errMsg"];

                            // send parameters to sendVerificationAuth() method in array format
                            $verifyParam = array("account_id" => $regArray["ac_id"], "email" => $regArray["email"], "first_name" => $regArray["first_name"], "last_name" => $regArray["last_name"], "password" => $regArray["u_code"], "salt" => $regArray["key"], 'email_status' => 1);

                            // send verification email from @notification server.
                            $sendVerification = sendVerificationAuth($verifyParam);

                            if(noError($sendVerification)) {


                                $errMsg = "Success : Email send successfully";
                                $xml_data["step9"]["data"] = "9. {$errMsg}";


                                $changeHandleStatus = changeHandleFlagStatusAfterSignup(strtolower($reqArray['account_handle']), strtolower($regArray["email"]));

                                if(noError($changeHandleStatus)){
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                                    /**************insert domain analytics *******************/
                                    $domain = strtolower(trim(end(explode("@", $reqArray['email']))));
                                    insert_domain_activity("total_registered_user", $domain, $connSearch);
                                    insert_domain_activity("total_unverified_user", $domain, $connSearch);
                                    /**************insert domain analytics *******************/


                                    /* Close search database connection */
                                    mysqli_close($connSearch);

                                    /* Create db connection for keywords */
                                    $connKeyword = createDBConnection('dbkeywords');
                                    if (noError($connKeyword)) {
                                        $connKeyword = $connKeyword["connection"];

                                        $errMsg = "Database connection success.";
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step10"]["data"] = "10. {$errMsg}";

                                        /* Update registered user count in mysql table */

                                        if ($ref_email != "") {
                                            $statType = "users_with_refferal";
                                            $updateUser = insert_user_statistics($statType, $connKeyword);
                                        } else {
                                            $statType = "registered_user";
                                            $updateUser = insert_user_statistics($statType, $connKeyword);
                                        }

                                        if (noError($updateUser)) {

                                            $errMsg = "Success : Update insert user statistics";
                                            $xml_data["step11"]["data"] = "11. {$errMsg}";
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                                            $errMsg = "A verification email has been sent to email address. Click on the verification link to activate your account.";
                                            $xml_data["step11"]["data"] = "11. {$errMsg}";
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);

                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $retArray;

                                        } else {

                                            $errMsg = "Failed : Update insert user statistics";
                                            $xml_data["step11"]["data"] = "11. {$errMsg}";
                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $retArray;
                                        }

                                    } else {
                                        $errMsg = "Database connection failed.";
                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                        $xml_data["step10"]["data"] = "10. {$errMsg}";
                                    }
                                }else {

                                    $errMsg = "Failed : Changing Handle status.";
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

                                }

                            }else{
                                $errMsg = $sendVerification["errMsg"];
                                $errCode = $sendVerification["errCode"];
                                $xml_data["step9"]["data"] = "9. {$errMsg}";
                                $retArray = setErrorStack($retArray, $errCode, $errMsg, $extraArgs);

                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $retArray;
                            }
                        }else{
                            $errMsg = "User Registration Failed";
                            $xml_data["step8"]["data"] = "8. {$errMsg}";
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

                            $xml_data['response']["data"]="";
                            $xml_data['response']["attributes"]=$retArray;
                        }
                    }else{
                        $errMsg = "Error: Getting settings from search";
                        $xml_data["step7"]["data"] = "7. {$errMsg}";
                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    }

                }else{

                    $errMsg = "Required fields validation failed";
                    $xml_data["step6"]["data"] = "6. {$errMsg}";
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

                    $xml_data['response']["data"]="";
                    $xml_data['response']["attributes"]=$retArray;
                }

            }else{
                $errMsg =  "First name/Last name must be within 50 characters long";
                $xml_data["step5"]["data"] = "5. {$errMsg}";
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

                $xml_data['response']["data"]="";
                $xml_data['response']["attributes"]=$retArray;
            }

            $xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

        }else{

            $errMsg = $captchaResp['errMsg'];;
            $xml_data["step3"]["data"] = "3. {$errMsg}";
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);

            $xml_data['response']["data"]="";
            $xml_data['response']["attributes"]=$retArray;
        }


    }
}else{

    $errMsg = "Database connection failed.";
    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
    $xml_data["step2"]["data"] = "2. {$errMsg}";
}

//printArr($retArray);die;


echo json_encode($retArray);
