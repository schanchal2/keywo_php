<?php
/*
* --------------------------------------------------------------------------------------------
*   Reset Forget Password Controller
*---------------------------------------------------------------------------------------------
*   This controller is used to reset forgetten password.
*/

// start the session.
session_start();

header("Access-Control-Allow-Origin: *");

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once ('../../models/user/authenticationModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/deviceHelper.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";

error_reporting(0);

$returnArr  = array();
$extraArgs  = array();

// get request parameters
$email            =   urldecode($_POST['email']);
$password         =   utf8_decode($_POST["newPassword"]);
$confirmPassword  =   utf8_decode($_POST["confirmPassword"]);
$auth             =   $_POST["auth"];
$timestamp        =   $_POST["time"];
// clean xss script
$email            =   cleanXSS(trim($email));

//for xml writing essential
$xmlProcessor =new xmlProcessor();

$xmlfilename="reset_password.xml";

$xmlArray = initializeXMLLog($email);
$logStorePathUserMgmt=$logPath["userManagement"];

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];

$xml_data['step']["data"]='User Reset Password';

$searchDbConn = createDBConnection('dbsearch');

if(noError($searchDbConn)){
      $searchDbConn = $searchDbConn["connection"];

      $msg = "Success : Database connection";
      $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

      $xml_data["step1"]["data"] = '1. '.$msg;

      if(isset($email) && !empty($email)){
            $msg = "Success : Email field found";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $xml_data['step2']["data"]="2. " . $msg ;

            if(isset($password) && !empty($password)){
                $msg = "Success : Password field found";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                $logWritePassword = "yes";

                $xml_data['step3']["data"]="3. " . $msg ;

                if(isset($confirmPassword) && !empty($confirmPassword)){
                    $msg = "Success : Confirm password field found";
                    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                    $logWriteConfirmPassword = "yes";

                    $xml_data['step4']["data"]="4. " . $msg ;

                    if(isset($auth) && !empty($auth)){
                          $msg = "Success : Auth field found";
                          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                          $xml_data['step5']["data"]="5. " . $msg ;

                          // call setPasswordReset model.
                          $setPasswordReset = setResetPassword($email, $password, $confirmPassword, $auth, $timestamp);
                        //  printArr($setPasswordReset);
                          if(noError($setPasswordReset)){
                                $msg = "Password reset successfully";
                                $extraArgs["data"] = $setPasswordReset["errMsg"];
                                $errCode = $setPasswordReset["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);

                                $xml_data['step5.1']["data"]="5.1. Get Parameters for Reset Password 1]User Email: " . $email ." Password: ".$logWritePassword." Confirm Password: ".$logWriteConfirmPassword." Auth: ".$auth;

                                $xml_data['step6']["data"]="6. " . $msg ;
                                // to update field in daily analytics table on success
                                $analyticsField = "user_forget_passSucc_count";

                          }else{
                                $msg =  $setPasswordReset["errMsg"];
                                $errCode = $setPasswordReset["errCode"];
                                $returnArr = setErrorStack($returnArr, $errCode, $msg, $extraArgs);

                                $xml_data['step6']["data"]="6. " . $msg ;

                                // to update field in daily analytics table on error
                                $analyticsField = "user_forget_passFailed_count";
                          }

                                $msg = "Success : Reset password analytics update successfully";
                                $retArray = setErrorStack($retArray, -1, $msg, $extraArgs);

                        /******************** for logs user analytics ****************/
                            $responseArr["errCode"]="-1";
                            $responseArr["errMsg"]=$msg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                          // end analytics
                    }else{
                          $msg = "Failure : Auth filed is missing";
                          $returnArr = setErrorStack($returnArr, 5, $msg, $extraArgs);

                          $xml_data['step5']["data"]="5. " . $msg ;
                    }
                }else{
                    $msg = "Failure : Confirm Password filed is missing";
                    $returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                    $xml_data['step4']["data"]="4. " . $msg ;
                }
            }else{
                $msg = "Failure : Password filed is missing";
                $returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

                $xml_data['step3']["data"]="3. " . $msg ;
            }
      }else{
            $msg = "Failure : Email field not found";
            $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

            $xml_data['step2']["data"]="2. " . $msg ;
      }
}else{
      $msg = "Failure : Database connection";
      $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);

      $xml_data["step1"]["data"] = '1. '.$msg;
}

// create and update log file
$xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);
?>
