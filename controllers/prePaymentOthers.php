<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 20/2/17
 * Time: 2:51 PM
 */


header("Access-Control-Allow-Origin: *");
session_start();
require_once('../config/config.php');
require_once('../config/db_config.php');
require_once('../helpers/errorMap.php');
require_once('../helpers/transactionHelper.php');
require_once('../helpers/coreFunctions.php');
require_once('../helpers/deviceHelper.php');
require_once('../helpers/arrayHelper.php');
require_once('../helpers/stringHelper.php');
require_once('../models/keywords/userCartModel.php');
require_once('../models/header/headerModel.php');
require_once('../models/keywords/keywordSearchModel.php');
error_reporting(0);

//Pass array through Clean Query Parameter
array_walk($_POST,'cleanQueryParameter');

//get input parameters from POST
$email           = $_SESSION["email"];
$clientSessionId = $_POST['client_sid'];
$finalamtInUPC = $_POST['finalamtInUPC'];
$finalamtInUSD = $_POST['finalamtInUSD'];
$finalamtInUSD = number_format((float)$finalamtInUSD,2,'.','');
$paymentMode = $_POST["payment_mode"];

//url decoding
$email           = urldecode($email);
$clientSessionId           = urldecode($clientSessionId);
/*User currency preference*/
$upc = strtolower($_SESSION['CurrPreference']);

//get current session Id and refferer page
$currentSessionId    = session_id();
//initialize the return array
$returnArr = array();

//check if ajax request is authorised or not
if ($clientSessionId == $currentSessionId) {
    //check email exixtence in session
    if (isset($email) && !empty($email)) {
        //establish keywords database connection
        // create keyword database connection
        $kwdDbConn = createDBConnection("dbkeywords");

        if (noError($kwdDbConn)) {
            $conn = $kwdDbConn["connection"];
            //check user existence in database
            $result = checkEmailExistance($email);
            if (noError($result)) {

                $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";

                $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);

                if(noError($getUserDetails)){
                    $firstBuyStatus = $getUserDetails["errMsg"]["first_buy_status"];
                    // calculate available balance
                    $getUserBalance = calculateUserBalance($getUserDetails);
                    if(noError($getUserBalance)){
                        $getUserBalance = number_format($getUserBalance["errMsg"]["total_available_balance"], 8);

                        //initialize strings to empty
                        $keywordAvailable = "";
                        $keywordNotAvailable = "";

                        //get user's cart
                        $getCartDetails = getUserCartDetails($email, $conn);
                        if (noError($getCartDetails)) {
                            $user_cart = $getCartDetails['errMsg'];
                            $user_cart = $user_cart["user_cart"];
                            $user_cart = json_decode($user_cart, true);
                            // Count user total keywords in cart
                            $user_cart_count = count($user_cart);
                            if ($user_cart_count > 0) {

                                //loop through user cart
                                foreach ($user_cart as $keyword) {

                                    if (isset($keyword) && !empty($keyword)) {

                                        $keyword = strtolower($keyword);
                                        /* Get keyword availability */
                                        $keywordStatus = checkKeywordCartStatus($keyword, $conn);
                                        $kwdOwnerEmail = $keywordStatus["errMsg"]["buyer_id"];

                                        // check each keyword is available or not for purchase
                                        if (isset($kwdOwnerEmail) && !empty($kwdOwnerEmail) && ($kwdOwnerEmail != $email)) {
                                            $keywordNotAvailable .= "#" . $keyword . ", "; //keywords those are blocked for 15 minutes or sold
                                        } else {
                                            $keywordAvailable .= "#" . $keyword . ", "; //keywords those are available
                                        }
                                    }
                                }

                                // if $keywordNotAvailable is empty then proceed to payment
                                if (empty($keywordNotAvailable)) {
                                    $cartKeywordsDetails  = getUserTotalCartPrice($conn, $email);

                                    if(noError($cartKeywordsDetails)){

                                        $total_cart_price_new = number_format((float)$cartKeywordsDetails['errMsg']['total_cart_price'],2,'.','');

                                        $amtToPurchase = (float)($total_cart_price_new - $getUserBalance);

                                        $amtToPurchase = number_format($amtToPurchase, 8);

                                        //check for slab changes
                                        if ($total_cart_price_new == $finalamtInUSD) {

                                            // get currency exchange rate of usd, sgd and btc
                                            $getExchangeRate = getExchangeRates($upc);

                                            if (noError($getExchangeRate)) {
                                                $getExchangeRate = $getExchangeRate['currRate'];

                                                $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
                                                if (noError($adminSettings)) {
                                                    $errMsg = "getting admin settings of buyer user success.";
                                                    $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];

                                                    if ($firstBuyStatus) {
                                                        $firstBuyCashBack = (float)(($total_cart_price_new * $firstBuyPercent) / 100);
                                                    } else {
                                                        $firstBuyCashBack = 0.00000000;
                                                    }

                                                    //New cart prices
                                                    $newCartPriceUSD = $total_cart_price_new;
                                                    $amtToPurchase = ((float)$amtToPurchase - $firstBuyCashBack);


                                                    /*$newCartPriceUPC = (float)$total_cart_price_new * $getExchangeRate["{$upc}"];
                                                    $newCartPriceUPC = number_format((float)($newCartPriceUPC),4,'.','');

                                                    $newCartPriceBTC = $total_cart_price_new * $getExchangeRate["btc"];
                                                    $newCartPriceBTC = number_format(floatval($newCartPriceBTC),8,'.','');

                                                    $newCartPriceSGD = $total_cart_price_new * $getExchangeRate["sgd"];
                                                    $newCartPriceSGD = number_format((float)($newCartPriceSGD),2,'.','');*/

                                                    $newCartPriceUPC = (float)$amtToPurchase * $getExchangeRate["{$upc}"];
                                                    $newCartPriceUPC = number_format((float)($newCartPriceUPC),4,'.','');

                                                    $newCartPriceBTC = $amtToPurchase * $getExchangeRate["btc"];
                                                    $newCartPriceBTC = number_format(floatval($newCartPriceBTC),8,'.','');

                                                    $newCartPriceSGD = $amtToPurchase * $getExchangeRate["sgd"];
                                                    $newCartPriceSGD = number_format((float)($newCartPriceSGD),2,'.','');

                                                    $blockTimeInMinute = 15;
                                                    //block keywords for 15 minutes
                                                    $result = blockKeywordsDuringPayment($blockTimeInMinute, $email, $conn);

                                                    if (noError($result)) {
                                                        if ($newCartPriceUSD !== $finalamtInUSD) {
                                                            //When rate is fluctuate
                                                            $returnArr["errCode"] = -1;
                                                            $returnArr["errMsg"] = "Proceed Payment with new Rates";
                                                            $returnArr["newCartPriceUSD"] = $amtToPurchase;
                                                            $returnArr["newCartPriceSGD"] = $newCartPriceSGD;
                                                            $returnArr["newCartPriceBTC"] = $newCartPriceBTC;
                                                            $returnArr["newCartPriceUPC"] = $newCartPriceUPC;
                                                            $returnArr["userAvailableBalance"] = $getUserBalance;
                                                            $returnArr["purchasingAmtInITD"] = number_format($amtToPurchase, 2);
                                                            $returnArr["keyword_available"] = rtrim($keywordAvailable,", ");
                                                        } else {
                                                            $returnArr["errCode"] = -1;
                                                            $returnArr["errMsg"] = "Proceed Payment with current Rates";
                                                            $returnArr["newCartPriceUSD"] = $amtToPurchase;
                                                            $returnArr["newCartPriceSGD"] = $newCartPriceSGD;
                                                            $returnArr["newCartPriceBTC"] = $newCartPriceBTC;
                                                            $returnArr["newCartPriceUPC"] = $newCartPriceUPC;
                                                            $returnArr["userAvailableBalance"] = $getUserBalance;
                                                            $returnArr["purchasingAmtInITD"] = number_format($amtToPurchase, 2);
                                                            $returnArr["keyword_available"] = rtrim($keywordAvailable,", ");

                                                        }
                                                    }else {
                                                        $returnArr["errCode"] = 2;
                                                        $returnArr["errMsg"] = "Error blocking keywords for 15 minutes";
                                                    }

                                                }else{
                                                    //Error getting admin settings
                                                }

                                            }else{
                                                $returnArr["errCode"] = 2;
                                                $returnArr["errMsg"]  = "Error: Fetching current exchange rate.";
                                            }

                                        } else {
                                            $returnArr["errCode"] = 2;
                                            $returnArr["errMsg"]  = "Slab rate changed";
                                        }
                                    }else{
                                        $returnArr["errCode"] = 2;
                                        $returnArr["errMsg"] = "User cart is empty";
                                    }

                                } else {
                                    $returnArr["errCode"] = 2;
                                    $returnArr["errMsg"] = "Cart consists of un-available keywords";
                                    $returnArr["keyword_not_available"] = trim($keywordNotAvailable,", ");
                                    $returnArr["keyword_available"] = rtrim($keywordAvailable,", ");
                                }
                            } else {
                                $returnArr["errCode"] = 2;
                                $returnArr["errMsg"] = "User cart is empty";

                            }
                        } else{
                            $returnArr["errCode"] = 2;
                            $returnArr["errMsg"]  = "Sorry buyer details not found";
                        }

                   }else{
                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"]  = "Error: Unable to get available balance.";
                    }
                }else{
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"]  = "Error: Fetching balance details.";
                }

            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = "User is not registered with us";
            }
        } else {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = "Error establishing database connection";

        }

    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = "Mandatory field not found";
    }

} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
}
echo json_encode($returnArr);

?>