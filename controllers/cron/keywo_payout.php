<?php

$docRootnew=explode("controllers/", dirname(__FILE__))[0];
$docrootpath=$docRootnew;


      // Include require files
      require_once("{$docrootpath}config/config.php");
      require_once("{$docrootpath}config/db_config.php");
      require_once("{$docrootpath}helpers/coreFunctions.php");
      require_once("{$docrootpath}helpers/errorMap.php");
      require_once("{$docrootpath}helpers/arrayHelper.php");
      require_once("{$docrootpath}models/cdp/cdpUtilities.php");
      require_once("{$docrootpath}helpers/stringHelper.php");
      require_once "{$docrootpath}models/wallet/walletKYCModel.php";

      // create payout.json file into payout directory.
      $payoutDir = $docrootpath."security/payout/";
      if(!is_dir($payoutDir)){
            mkdir($payoutDir, 0777, true);
      }

      $kwdDbConn = createDBConnection("dbkeywords");

      if(noError($kwdDbConn)){
            $kwdDbConn = $kwdDbConn["connection"];

            $poolUserDistAmt = $userRequiredFields .",purchases,trade_fees,total_unsold_kwd_refund";
            // get community pool user amount
            $getPoolDetails = getUserInfo($communityPoolUser, $walletURL, $poolUserDistAmt);

            if(noError($getPoolDetails)){
                $getPoolDetails = $getPoolDetails["errMsg"];
                $communityPoolBalance = ($getPoolDetails["purchases"]/2) + $getPoolDetails["trade_fees"] + $getPoolDetails["total_unsold_kwd_refund"];
                echo $communityPoolBalance = number_format($communityPoolBalance, 2);

                $getKeywoStats = getKeywoStats($kwdDbConn);

                if(noError($getKeywoStats)){
                      $getKeywoStats                  =   $getKeywoStats["data"][0];
                      // get total registered user.
                      $totalRegisteredUser            =   $getKeywoStats["registered_users"];

                      // close keyword db connection
                      mysqli_close($kwdDbConn);

                      // create search db connection
                      $searchDbConn = createDBConnection("dbsearch");
                      if(noError($searchDbConn)){
                          $searchDbConn = $searchDbConn["connection"];

                          /* Calculate user interaction according to KYC-Level */
                          $getKycLevels = getKYCSlabByLevel($searchDbConn);
                          if(noError($getKycLevels))
                          {

                              $getKycLevels = $getKycLevels['errMsg'];
                              $kycLevelsInteraction  = 0;

                              for($i = 0; $i < count($getKycLevels)-1; $i++)
                              {
                                  $kycLevelsInteraction += $getKycLevels[$i]['intraction'];
                              }

                              // total number of days
                              $days         =   90;
                              // per day user qualified interaction
                              /*$interaction  =   40;*/
                              $avgKycLevelsInteraction = ($kycLevelsInteraction / 3);
                              // calculate total searches
                              /*$totalSearches = $totalRegisteredUser * $interaction;*/
                              $totalSearches = $totalRegisteredUser * $avgKycLevelsInteraction;
                              /*$totalSearches = 3000;*/
                              // calculate total per day payout amount
                              $perDayPayout = number_format((float)($communityPoolBalance / $days), 8, '.', '');
                              $currentPayout = number_format((float)($perDayPayout / $totalSearches), 8, '.', '');

                              //  $currentPayout = 300;

                              /* current 0.00000300 btc equals too 0.01 usd */
                                  if($currentPayout >= 0.0300){
                                  // get current max payout set by admin in admin setting table.
                                  $currentPayout = getSettingsFromSearchAdmin($searchDbConn);
                                  if(noError($currentPayout)){
                                      // get current payout
                                      $currentPayout = $currentPayout["data"]["current_max_payout"];
                                  }else{
                                      print($currentPayout["errMsg"]);
                                      exit;
                                  }
                              }

                              $payoutArray = array("community_pool_balance" => $communityPoolBalance, "days" => $days, "registered_users" => $totalRegisteredUser, "interaction" => $avgKycLevelsInteraction, "total_searches" => $totalSearches, "per_day_payout" => $perDayPayout, "payout" => $currentPayout);
                              $payoutJson = json_encode($payoutArray);

                              $payoutFileName = "payout.json";

                              // create payout.json file into payout directory.
                              $payoutFile = $payoutDir.$payoutFileName;

                              /* Please uncomment below code at the time of keywo launch */

                              /*if(file_exists($payoutFile)){
                                  // write into current payout amount
                                  if(time() - filemtime($payoutFile) >= 300){

                                      $payoutArchieveDir = $payoutDir."payout_archieve/";
                                      if(!is_dir($payoutArchieveDir)){
                                          mkdir($payoutArchieveDir, 0777, true);
                                      }
                                      // archieve payout files
                                      $archievePayoutFile = archivesFiles($payoutArchieveDir ,$payoutFile, "poolBalance.zip");
                                      if(noError($archievePayoutFile)){
                                          // delete the existing payout.json files
                                          unlink($payoutFile);

                                          // create new payout.json file with current balance from community pool
                                          file_put_contents($payoutFile, $payoutJson);
                                          print('Successfully create json file');
                                      }else{
                                          print('Error: '.$archievePayoutFile["errMsg"]);
                                          exit;
                                      }

                                  }else{
                                      // write log file
                                  }
                              }else{
                                  // create new file and write into it.
                                  file_put_contents($payoutFile, $payoutJson);
                              }*/

                          }else{
                              print('Error: Unable to get interaction according KYC-Level');
                              exit;
                          }

                      }else{
                          print('Error: Unable to create search database connection');
                          exit;
                      }
                }else{
                  print($getKeywoStats["errMsg"]);
                  exit;
                }
            }else{
              print('Error: Unable to get pool user amount');
              exit;
            }
      }else{
        print('Error: Unable to create database connection');
        exit;
      }

      echo "<pre>";
      print_r($payoutArray);
      echo "</pre>";
 ?>
