<?php

session_start();
ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath=explode("controllers/", dirname(__FILE__))[0];



// Include dependent files
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once "{$docrootpath}helpers/deviceHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/email_helpers.php";
require_once "{$docrootpath}helpers/stringHelper.php";

require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");



$email  = $_SESSION["email"];
$stepCounter = 0;
$retArray = array();

/* Initialize XML logs */
$xmlProcessor = new xmlProcessor();
$xmlArray = initializeXMLLog(trim(urldecode($email)));
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

/* XML log file path */
$cronLogs = $docrootpath."logs/cron_logs/";
$xmlFilename   = "update_currency.xml";

/* XML title attribute */
$xml_atrr = array();
$xml_atrr["title"] = "update currency cron";


$errMsg = "Start update currency cron."; // Message for log
$stepCounter++; // Step counter
$xml_data["step{$stepCounter}"]["data"] ="";
$xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr; // Set attribute in step
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}"; // Set message for step

$conn = createDBConnection("dbsearch");
if (noError($conn)) {

    $errMsg = "Database connection : success.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $conn = $conn["connection"];

    $result="https://www.apilayer.net/api/live?access_key=f6b240dde39f7a167822f95abfc34343&format=1";
    $response = file_get_contents($currency_API_link."access_key=".$currencyAPI."&format=1");

    $response = json_decode($response, true);
    $currencyList = $response['quotes'];

    if(count($currencyList) > 0)
	{
        $errMsg = "Getting update currency list from api : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        foreach($currencyList as $key => $value){
            $currency_code = str_split($key, 3);
            $currency_update_price = currencyDailyUpdates($currency_code[1],$value,$conn);
        }

        if(noError($currency_update_price)){
            $errMsg = "Update currency table on ". date('Y-m-d H:i:s'). " : success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $retArray["errCode"] = -1;
            $retArray["errMsg"] = $errMsg;

        }else{
            $errMsg = "Error update currency table on ". date('Y-m-d H:i:s'). ".";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $retArray["errCode"] = 2;
            $retArray["errMsg"] = $errMsg;

            $txt.="Operation Message : Function for expire cron By BTC Failed\n";
            $emailContent = "<span> Cron Job for expire has been failed at ".date('Y-m-d H:i:s')." </span><br/><span>Please fix the issue as soon as possible</span> ";
            $emailSubject="Update currency cron";
            $parameters["name"]="Developer";
            $parameters["content"]=$emailContent;
            sendEmail($searchtradeDeveloper, $noReplyEmail, $emailSubject,"currencyCron.html",$parameters);
        }

	}else{
        $errMsg = "Error getting update currency list from api on ". date('Y-m-d H:i:s'). ".";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $retArray["errCode"] = 2;
        $retArray["errMsg"] = $errMsg;
	}

} else {

    $errMsg = "Error in database connection.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $retArray["errCode"] = 2;
    $retArray["errMsg"] = $errMsg;
    exit;
}

$xmlProcessor-> writeXML($xmlFilename, $cronLogs, $xml_data, $xmlArray["activity"]);

printArr($retArray);

function currencyDailyUpdates($currencyCode,$value,$conn){

	$returnArr = array();
	$query = "UPDATE currency_value SET `current_price` = '$value' where `shortforms` = '".$currencyCode."'";
	$result = runQuery($query, $conn);

	if (noError($result)) {

		$returnArr["errCode"] = -1;
		$returnArr["errMsg"] = "Query Transaction successfully committed.";

	}else {
		$returnArr["errCode"] = 5;
		$returnArr["errMsg"]="Transaction Failure";
	}

	return $returnArr;

}

?>
