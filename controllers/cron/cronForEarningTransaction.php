<?php

ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath = explode("controllers/",  dirname(__FILE__))[0];

// Include dependent files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

// create keyword database connection
$conn = createDBConnection("dbkeywords");
if(noError($conn)){

    $conn = $conn["connection"];

    $short_type = "keyword_earning";
    $transaction_status = "confirmed";
    $currenttime        = date("d-m-Y H:i:s");
    $date_jumpers_lt    = strtotime($currenttime) * 1000 ;
    $date_jumpers_gt = $date_jumpers_lt - 300000;
    $date = date("d-m-y");
    $year = date('Y');
    $month = lcfirst(date('M'));

    // get transaction details as per time interval
    $getTransactionDetail  = getTransactionDetail($date_jumpers_lt, $date_jumpers_gt,$short_type,$transaction_status);
    if(noError($getTransactionDetail)){
        $getTransactionDetail = $getTransactionDetail["errMsg"]["batched_container"];

        if(count($getTransactionDetail) > 0){

            foreach($getTransactionDetail as $key => $transDet){

                $metaDetails = $transDet["meta_details"];
                $keyword = $metaDetails["keyword"];
                $amount = $transDet["amount"];
                $post_type = $metaDetails["post_type"];

                // getting data from revenue table
                $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $keyword);
                if(noError($kwdRevenueDetails)){
                    $revenueData = $kwdRevenueDetails["data"];
                    $tableName = $kwdRevenueDetails["table_name"];

                    if(!empty($revenueData)){
                        $revenueData = $revenueData[0];

                        $userKwdOwnershipEarning = $revenueData["user_kwd_ownership_earnings"];
                        $appKwdOwnershipEarning  = $revenueData["app_kwd_ownership_earnings"];

                        if(isset($userKwdOwnershipEarning) && !empty($userKwdOwnershipEarning) && isset($appKwdOwnershipEarning) && !empty($appKwdOwnershipEarning)){

                            $userKwdOwnershipEarning = $userKwdOwnershipEarning + $amount;

                            $appKwdOwnershipEarning = json_decode($appKwdOwnershipEarning, true);

                            if(isset($appKwdOwnershipEarning) && !empty($appKwdOwnershipEarning)) {
                                if(array_key_exists('total',$appKwdOwnershipEarning)) {
                                    $appKwdOwnershipEarning["total"] = $appKwdOwnershipEarning["total"] + $amount;
                                } else {
                                    $appKwdOwnershipEarning["total"] = $amount;
                                }
                            }

                            if (isset($post_type) && !empty($post_type)) {
                                if (array_key_exists($post_type, $appKwdOwnershipEarning["social"])) {
                                    $appKwdOwnershipEarning["social"][$post_type] = $appKwdOwnershipEarning["social"][$post_type] + $amount;
                                } else {
                                    $appKwdOwnershipEarning["social"][$post_type] = $amount;
                                }
                            }
                        }else{
                            $appKwdOwnershipEarning = array('total' => $amount, 'social' => array($post_type => $amount));
                            $userKwdOwnershipEarning = $amount;
                        }
                        $earningJson = json_encode($appKwdOwnershipEarning);

                        // update revenue table.
                        $updateEarningRec = addEarningInRevenue($conn, $keyword, $earningJson, $userKwdOwnershipEarning, $tableName, '', 2);

                        if(noError($updateEarningRec)){
                            // get keyword analytics data
                            $kwdAnalyticsDetails = getkeywordAnaliticsDetails($conn,$keyword);

                            if(noError($kwdAnalyticsDetails)){
                                $kwdAnalyticsTableName = $kwdAnalyticsDetails["table_name"];
                                $kwdAnalyticsDetails = $kwdAnalyticsDetails["data"];

                                if(!empty($kwdAnalyticsDetails)){
                                    $kwdAnalyticsDetails = $kwdAnalyticsDetails[0];
                                    $kwdAnaliticsColumn = $kwdAnalyticsDetails[$month];
                                    $id = $kwdAnalyticsDetails["id"];

                                    $kwdAnalyticsColumnData = json_decode($kwdAnaliticsColumn, true);
                                    $kwdAnalyticsEarning = $kwdAnalyticsColumnData["analytics"][$date]["social"];

                                    if (isset($post_type) && !empty($post_type)) {
                                        if (array_key_exists($date, $kwdAnalyticsColumnData["analytics"])) {
                                            if (array_key_exists("social_Earning", $kwdAnalyticsColumnData["analytics"][$date])) {
                                                if (array_key_exists($post_type, $kwdAnalyticsColumnData["analytics"][$date]["social_Earning"])) {
                                                    $kwdAnalyticsColumnData['analytics'][$date]["social_Earning"][$post_type] = $kwdAnalyticsColumnData["analytics"][$date]["social_Earning"][$post_type] + $amount;
                                                } else {
                                                    $kwdAnalyticsColumnData["analytics"][$date]["social_Earning"][$post_type] = $amount;
                                                }
                                            }
                                        } else {
                                            $kwdAnalyticsColumnData["analytics"][$date] = array('search' => array('search_Interaction' => 0, 'search_Earning' => 0), 'social' => $kwdAnalyticsEarning, 'social_Earning' => array($post_type => $amount));
                                        }
                                    }

                                    $kwdAnalyticsColumnData = json_encode($kwdAnalyticsColumnData);

                                    // update analytics table.
                                    $updateAnalyticsRec = addEarningInRevenue($conn, $keyword, $kwdAnalyticsColumnData, '', $kwdAnalyticsTableName, $month, 3);

                                    if(noError($updateAnalyticsRec)){
                                        print("\n".$updateAnalyticsRec["errMsg"]);
                                    }else{
                                        print("\n".$updateAnalyticsRec["errMsg"]);
                                    }
                                }
                            }else{
                                print("\n".$kwdAnalyticsDetails["errMsg"]);
                            }
                        }else{
                            print("\n".$updateEarningRec["errMsg"]);
                        }
                    }else{
                        $earningArr = array('total' => $amount, 'social' => array($post_type => $amount));
                        $earningJson = json_encode($earningArr);

                        // insert record into revenue table.
                        $insertEarningRec = addEarningInRevenue($conn, $keyword, $earningJson, $amount, $tableName, '', 1);
                        if(noError($insertEarningRec)){
                            print("\n".$insertEarningRec["errMsg"]);
                        }else{
                            print("\n".$insertEarningRec["errMsg"]);
                        }
                    }
                }else{
                    print("\n".'Error : Unable to get revenue details of #'.$keyword);
                }
            }
        }else{
            print('No transaction found');
        }
    }else{
        print('Error: Unable to get transaction details');
    }
}else{
    print("Database Error");
}

/*
 * Function     :   addEarningInRevenue
 * Arguments    :   (string) $conn, (string) $keyword, (json) $earning, (double) $amount, (string) $tableName, (string) $month, (int) $statusType
 * Purpose      :   Inserting and updating records in revenue and analytics table.
 * Return       :   (array) $returnArr, Where errCode = -1 on success and errCode = 1 on failure with error message (errmsg).
 */

function addEarningInRevenue($conn, $keyword, $earningJson, $amount, $tableName, $month, $statusType){

    $returnArr = array();
    $query = '';
    $msg = '';

    if($statusType == 1){
        // insert query
        $query = "insert into ".$tableName." (keyword, app_kwd_ownership_earnings, user_kwd_ownership_earnings) values ('".$keyword."', '".$earningJson."',".$amount.")";
        $msg = "Record insert successfully";

    }else if($statusType == 2){
        // update query
        $query = "update ".$tableName." set app_kwd_ownership_earnings='".$earningJson."', user_kwd_ownership_earnings = ".$amount." where keyword = '".$keyword."'";
        $msg = "Record update successfully";
    }else if($statusType == 3){
        $query = "update ".$tableName." set ".$month." = '".$earningJson."' where keyword='".$keyword."'";
        $msg = "Analytics record update successfully";
    }

    $execQuery = runQuery($query, $conn);

    if(noError($execQuery)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $msg;

    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $execQuery["errMsg"];
    }

    return $returnArr;
}
?>