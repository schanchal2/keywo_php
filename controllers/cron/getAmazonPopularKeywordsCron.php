<?php
// include required files

$docRootnew=explode("controllers/",dirname(__FILE__))[0];
$docrootpath=$docRootnew;

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}views/search/amazon/amazonHelper.php");
require_once("{$docrootpath}models/search/searchLandingModel.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");


error_reporting(0);

// getting cron log directory where log messages are stored
$crondir	=	$docrootpath."logs/cronLogs";
$newServerRootUrl = $docrootpath.'json_directory/';
$dir 		= 	$newServerRootUrl."AmazonJsonFiles";
$time 		= 	time();
$flag		=	true;
$msg		=	'';

$connSearch = createDBConnection("dbsearch");

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];

    if(is_dir($dir)){
        $flag = true;
        print "1. Successfully Found Directory: ".$dir." on ".date("d-m-Y h:i:s A")."<br/>";

        $getKeyword = getPopularKeyword($newServerRootUrl, 8, 'amazon');

        if(noError($getKeyword)){
            $flag = true;
            print  "2. Successfully get most popular keyword from 8_amazon_KeywordSearched.json file on ".date("d-m-Y h:i:s A").". \n";

            $getKeyword = $getKeyword['keywords'];
            // $getAPIResult = displayApiResult($getKeyword, $connSearch); die;
            foreach($getKeyword as $prdKey => $prd){
                // validating whether keyword is UTF-8 character encoded, if found then amazon api should not call.
                $prd = mb_detect_encoding($prd);

                if($prd != "UTF-8"){
                    $prd = $prd.".json";
                    if(file_exists($dir."/".$prd)){
                        if(($time - filemtime($dir."/".$prd)) >= 12){  // 24*60*60
                            unlink($dir."/".$prd);
                        }
                    }
                    if(!file_exists($dir."/".$prd)){
                        // calling amazon API
                        $getAPIResult = displayApiResult($getKeyword[$prdKey], $connSearch);
                        print "3. Successfully getting most popular keyword product from amazon API on ".date("d-m-Y h:i:s A").".\n";

                        echo "Fetch Result successfully";
                    }
                }
            }
        }else{
            $flag = false;
            print "Error: Getting most popular keyword from 8_amazon_KeywordSearched.json file on ".date("d-m-Y h:i:s A").".\n";
            print("Error: ".$getKeyword["errMsg"]);
        }
    }else{
        $flag = false;
        print 'Error: Directory Not Found '.$newServerRootURL."AmazonJsonFIles on ".date("d-m-Y h:i:s A").".\n";
        print("Error: Directory Not Found");
    }

}else{
    print('Error : Database connection');
}



if($flag == true){
    if(!is_dir($crondir)){
        mkdir($crondir, 0777);
    }
    $msg .= "--------------------------------------------------------------------------------------------------------------------\n\n";
    $fp = fopen($crondir."/amazonPopularSearchQueryResultCron.txt", "a+");
    fwrite($fp,$msg);
    fclose($fp);
}else{
    print "Error: Unable to fetch most popular keyword from 8_amazon_KeywordSearched.json on ".date("d-m-Y h:i:s A")."\n ";
}


?>
