<?php

ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath = explode("controllers/", dirname(__FILE__))[0];

// Include dependent files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}/models/analytics/social_analytics.php");

$conn = createDBConnection("dbkeywords");
if (noError($conn)) {
    $conn = $conn["connection"];
} else {
    print_r("Database Error");
}

/* Get Transaction Details*/

$short_type_array = array("post_creator_earning", "post_sharer_earning");
$transaction_status = "confirmed";
$currenttime = date("d-m-Y H:i:s");
$date_jumpers_lt    = strtotime($currenttime) * 1000 ;
$date_jumpers_gt = $date_jumpers_lt - 300000;

foreach ($short_type_array as $short_type) {

    $getTransactionDetail = getTransactionDetail($date_jumpers_lt, $date_jumpers_gt, $short_type, $transaction_status);
    if (noError($getTransactionDetail)) {

        $getTransactionDetail2 = $getTransactionDetail["errMsg"]["batched_container"];

        if (count($getTransactionDetail2) > 0) {
            foreach ($getTransactionDetail2 as $tranraction) {

                $metaDetails = $tranraction["meta_details"];
                $amount = (float)$tranraction["amount"];
                $content_id = $metaDetails["content_id"];
                $post_created_at = $metaDetails["post_created_at"];

                $updatedEarnings = UpdatePostEarnings($content_id, $amount, $post_created_at);
                if (noError($updatedEarnings)) {

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = "Post Earning Detail successfully Updated.";

                } else {
                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"] = "Error in Updating Post Earning Detail";
                }


                printArr($returnArr);

            }

        }else{
            printArr("no relevant data found");
        }
    }
}
