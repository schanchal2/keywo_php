<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

 $docRootnew=explode("controllers/", dirname(__FILE__))[0];

require_once("{$docRootnew}config/config.php");
require_once("{$docRootnew}config/db_config.php");
require_once("{$docRootnew}helpers/coreFunctions.php");
require_once ("{$docRootnew}helpers/errorMap.php");
require_once ("{$docRootnew}helpers/stringHelper.php");
require_once ("{$docRootnew}models/keywords/userCartModel.php");
require_once ("{$docRootnew}helpers/arrayHelper.php");
require_once ("{$docRootnew}models/keywords/acceptBidModel.php");


error_reporting(0);

$reminderPeriodArr = array();
$noticePeriodArr = array();
$finishRemainderPeriod = array();
$releaseOwnershipArr = array();
$i = 0;

$alphabets = range("a", "z");
$alphabets[26] = "num";
$alphabets[27] = "other";

// create keyword database connection
$kwdDbConn = createDBConnection("dbkeywords");

if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    foreach($alphabets as $key => $letters){
        $ownedKeyword = getAllKwdOwnershipData($letters, $kwdDbConn);
        //printArr($ownedKeyword);

        if(noError($ownedKeyword)){
            $ownedKeyword = $ownedKeyword["errMsg"];
            foreach($ownedKeyword as $keyNum => $renewKwd){
                $kwdBuyerId           =   $renewKwd["buyer_id"];
                $keyword            =   $renewKwd["keyword"];
                $dayDiff            =   (int)$renewKwd["no_of_days"];
                $expiryTimestamp    =   $renewKwd["ownership_expiry_time"];

                if($dayDiff == 30 || $dayDiff == 15 || $dayDiff == 7 || $dayDiff == 6 || $dayDiff == 5 || $dayDiff == 4 || $dayDiff == 3 || $dayDiff == 2 || $dayDiff == 1){

                    $reminderPeriodArr[$keyNum]["to"] = $kwdBuyerId;
                    $reminderPeriodArr[$keyNum]["subject"] = "Keywo: Renew Keyword Ownership";
                    $reminderPeriodArr[$keyNum]["body"] = "Attention: We wish to inform you that the keyword #".$keyword." owned by you will be expiring in ".$dayDiff." days.<br /> Please <a href='".$rootUrl."views/keyword/renew_keyword.php'> click here </a> to renew now and continue earning on the keyword.<br />";
                    $reminderPeriodArr[$keyNum]["notification_msg"] = "Attention: We wish to inform you that the keyword #".$keyword." owned by you will be expiring in ".$dayDiff." days.";

                }elseif($dayDiff == 0){
                    $finishRemainderPeriod[$keyNum]["to"] = $kwdBuyerId;
                    $finishRemainderPeriod[$keyNum]["subject"] = "Keywo: Renew Keyword Ownership";
                    $finishRemainderPeriod[$keyNum]["body"] = "This is to inform you that the keyword #".$keyword." owned by you has expired on (".date('j F, Y', strtotime($expiryTimestamp))."). However you can renew it by clicking on this  <a href='".$rootUrl."views/keyword/renew_keyword.php'> link  </a> . If you wish to discontinue ownership, you can release it back to the pool by <a href='".$rootUrl."/controllers/release_keyword_from_ownership.php'> clicking </a> here. For any questions, please contact <a href='".$rootUrl."support/'> support@keywo.com </a>";
                    $finishRemainderPeriod[$keyNum]["notification_msg"] = "This is to inform you that the keyword #".$keyword." owned by you has expired on (".date('j F, Y', strtotime($expiryTimestamp)).").";

                }elseif($dayDiff < 0 && $dayDiff > -15){
                    $noticeDay = 15;
                    $dayDiff = abs($dayDiff);
                    $reminingDays = $noticeDay - $dayDiff;

                    $noticePeriodArr[$keyNum]["to"] = $kwdBuyerId;
                    $noticePeriodArr[$keyNum]["subject"] = "Keywo: Renew Keyword Ownership";
                    $noticePeriodArr[$keyNum]["body"] = "This is to inform you that the keyword #".$keyword." has expired on (". date('j F, Y', strtotime($expiryTimestamp))."). You can still renew it within ".$reminingDays." days to continue using it and start earning again. Please <a href='".$rootUrl."views/keyword/renew_keyword.php'> click here </a> to renew. If you wish to discontinue ownership, you can release it back to the pool by <a href='".$rootUrl."/controllers/release_keyword_from_ownership.php'> clicking </a> here. For any questions, please contact <a href='".$rootUrl."support/'> support@keywo.com  </a>.";
                    $noticePeriodArr[$keyNum]["notification_msg"] =  "This is to inform you that the keyword #".$keyword." has expired on (". date('j F, Y', strtotime($expiryTimestamp))."). You can still renew it within ".$reminingDays." days to continue using it and start earning again.";
                }elseif($dayDiff < -15){

                    $releaseOwnershipArr[$keyNum]["to"] = $kwdBuyerId;
                    $releaseOwnershipArr[$keyNum]["subject"] = "Keywo: Keyword Ownership Revoked";
                    $releaseOwnershipArr[$keyNum]["body"] =  "This is to inform you that the keyword #".$keyword." has ownership expired on ".date('j F, Y', strtotime($expiryTimestamp))." and ownership has been revoked. <br /> Hence you will not be able to trade on this keyword anymore. <br /> For more information please contact to <a href='".$rootUrl."support/'> keywo.com </a>";
                    $releaseOwnershipArr[$keyNum]["notification_msg"] =  "This is to inform you that the keyword #".$keyword." has ownership expired on ".date('j F, Y', strtotime($expiryTimestamp))." and ownership has been revoked. <br /> Hence you will not be able to trade on this keyword anymore.";

                    //get keyword ownership table name
                    $kwdTableName = getKeywordOwnershipTableName($keyword);

                    // revoked ownership
                    $revokeKwdOwnership = revokeKeywordOwnership($kwdBuyerId, $keyword, $kwdTableName, $kwdDbConn);
                    if(noError($revokeKwdOwnership)){

                        // remove keyword from mykeyword_details.
                        $removeOwnedKeyword = deleteOwnedKwdFromMyKwdDetails($kwdBuyerId, $keyword, $kwdTableName, $kwdDbConn);
                        if(noError($removeOwnedKeyword)){
                            print($removeOwnedKeyword["errMsg"]);
                        }else{
                            print($removeOwnedKeyword["errMsg"]);
                        }
                    }else{
                        print($revokeKwdOwnership["errMsg"]);
                    }
                }
            }
        }else{
            print('Error: Fetching records from keywords_ownership_'.$letters);
        }
    }


    if(count($reminderPeriodArr) > 0){
        $sendEmailForRemainder =  sendEmailNotification($reminderPeriodArr);
        if(noError($sendEmailForRemainder)){
            print($sendEmailForRemainder["errMsg"]);
        }else{
            print($sendEmailForRemainder["errMsg"]);
        }
    }
    if(count($finishRemainderPeriod) > 0){
        $sendEmailForFinishRemainder = sendEmailNotification($finishRemainderPeriod);
        if(noError($sendEmailForFinishRemainder)){
            print($sendEmailForFinishRemainder["errMsg"]);
        }else{
            print($sendEmailForFinishRemainder["errMsg"]);
        }
    }

    if(count($noticePeriodArr) > 0){
        $sendEmailForNotice = sendEmailNotification($noticePeriodArr);
        if(noError($sendEmailForNotice)){
            print($sendEmailForNotice["errMsg"]);
        }else{
            print($sendEmailForNotice["errMsg"]);
        }
    }

    if(count($releaseOwnershipArr) > 0){
        $sendEmailForOwnershipRevoke = sendEmailNotification($releaseOwnershipArr);
        if(noError($sendEmailForOwnershipRevoke)){
            print($sendEmailForOwnershipRevoke["errMsg"]);
        }else{
            print($sendEmailForOwnershipRevoke["errMsg"]);
        }
    }

}else{
    print('Error: Keyword database connection');
}



?>