<?php
ini_set('default_charset', 'utf-8');

$docrootpath = explode("controllers/", dirname(__FILE__))[0];

// Include require files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/walletHelper.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/walletHelper.php");
require_once("{$docrootpath}helpers/transactionHelper.php");
require_once("{$docrootpath}models/cdp/cdpUtilities.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/search/searchResultModel.php");
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");
require_once("{$docrootpath}models/landing/landingFeatureModel.php");

// create an object
$xmlProcessor = new xmlProcessor();

$xmlfilename = "three_user_cdp.xml";

/* XML title attribute */
$xml_atrr = array();


// initialize log step number
$stepCounter = 0;
$stepCounterkeywoUser = 0;
$stepCounterAppDev = 0;
$stepCounterContentCreator = 0;
$stepCounterKwdOwnership = 0;

$cdpStepDone = array();
$logAttribute = array();

$kwdDbConn = createDBConnection("dbkeywords");

if (noError($kwdDbConn)) {
    $kwdDbConn = $kwdDbConn["connection"];

    // log path
    $docRoot = $docrootpath;
    $logStorePathUserMgmt = $docrootpath . "logs/coinDistribution/";

    $getConsumerDetails = getContentConsumerDetail($kwdDbConn);
//    printArr($getConsumerDetails);

    if (noError($getConsumerDetails)) {
        $getConsumerDetails = $getConsumerDetails["data"];

        if (count($getConsumerDetails) > 0) {
            foreach ($getConsumerDetails as $key => $contentData) {

                // log activity start
                // inititalize xml logs activity
                $xmlArray = initializeXMLLog('');


                $recordId = $contentData["id"];
                $consumerEmail = $contentData["consumer_email"];
                $consumerId = $contentData["consumer_user_id"];
                $walletTransactionId = $contentData["wallet_transaction_id"];
                $keyword = $contentData["keyword"];
                $currentPayout = $contentData["current_payout"];
                $currentPayout = number_format($currentPayout, 8);
                $totalPayout = $contentData["total_payout"];
                $originIp = $contentData["origin_ip"];
                $interactionType = $contentData["interaction_type"];
                $contentId = $contentData["content_id"];
                $exchangeRateInJson = $contentData["exchange_rate_in_json"];
                $cdpStatus = $contentData["cdp_status"];
                $creatorId = $contentData["post_creator"];
                $sharerId = $contentData["post_sharer"];
                $modeType = $contentData["mode"];
                $cdpStatusFromSocial = $contentData["cdp_status_from_social"];
                $country = $contentData["country"];
                $device = $contentData["device"];
                $gender = $contentData["gender"];
                $browser = $contentData["browser"];
                $username = $contentData["username"];
                $postType = $contentData["post_type"];
                $post_created_at = $contentData["post_created_at"];


                // log data
                $xml_data['request']["data"] = '';
                $xml_data['request']["attribute"] = $contentData;

                $xml_data['step']["data"] = 'Coin Distribution Process';
                // log data


                if ($cdpStatus == 0) {

                    // start transaction to commit the keywo@keywo.com earning
                    $startTransaction = startTransaction($kwdDbConn);

                    if (noError($startTransaction)) {

                        if ($modeType == "search") {
                            $earningAttribute = "searchearning";
                            $earningAttributelog = "search_earning";
                            $transType = "keywo_earning";
                        } else if ($modeType == "social") {
                            $earningAttribute = "post_view_earnings";
                            $earningAttributelog = "post_view_earnings";
                            $transType = "keywo_content_view_earning";
                        }


                        // get keywo user details
                        $keywoReqField = $userRequiredFields . ",gender";

                        $getkeywoDetails = getUserInfo($keywoUser, $walletURLIP . 'api/v3/', $keywoReqField);
                        if (noError($getkeywoDetails)) {
                            $getkeywoDetails = $getkeywoDetails["errMsg"];
                        }
                        $keywoGender = isset($getkeywoDetails["gender"]) ? $getkeywoDetails["gender"] : "other";


                        $xml_atrr["user"] = $keywoUser;
                        $xml_atrr["trans_type"] = $transType;
                        $xml_atrr["interaction_type"] = $earningAttributelog;
                        $xml_atrr["amount_given_to_user"] = $currentPayout;
                        $xml_atrr["keywo_gender"] = $keywoGender;

                        $errMsg = "Start {$keywoUser} user coin distribution process."; // Message for log
                        $stepCounterkeywoUser++; // Step counter

                        $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}"; // Set message for step

                        // 1. credit current payout amount to keywoUser@keywo.com
                        $creditPayoutToKeywoUser = creditUserEarning($keywoUserId, $currentPayout, $earningAttribute, "add");

                        $response = json_encode($creditPayoutToKeywoUser["errMsg"]);


                        if (noError($creditPayoutToKeywoUser)) {

                            $xml_atrr["kwuser"] = $keywoUser;
                            $xml_atrr["kwresponse"] = $response;

                            $errMsg = "Start {$keywoUser} user coin distribution process.";
                            $stepCounterkeywoUser++; // Step counter

                            $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";


                            // add keywo user balance update success into array to update cdp_step_done field into content_consumer_list table.
                            $cdpStepDone["keywo"]["update_user_balance"] = "Success";


                            // convert into json
                            $creditStepDone = json_encode($cdpStepDone);

                            // update into content consumer list table
                            $udpateCDPStep = udpateContentConsumer($recordId, "cdp_steps_done = '" . $creditStepDone . "'", $kwdDbConn);

                            if (noError($udpateCDPStep)) {

                                $errMsg = "Success : Update cdp_step_for: " . $creditStepDone;
                                $stepCounterkeywoUser++; // Step counter

                                $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                // commit transaction for keywo credit earning
                                $commitTransaction = commitTransaction($kwdDbConn);
                                if (noError($commitTransaction)) {

                                    $errMsg = "Success: commit transaction ";
                                    $stepCounterkeywoUser++; // Step counter

                                    $keywoUserChild["step{$stepCounterkeywoUser}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    // get earnings of search earning as well as content view earning
                                    $requiredBalance = $userRequiredFields . ",search_earning,social_content_view_earnings";
                                    $getkeywoBalance = getUserInfo($keywoUser, $walletURLIP . 'api/v3/', $requiredBalance);

                                    $response = json_encode($getkeywoBalance["errMsg"]);

                                    if (noError($getkeywoBalance)) {
                                        $getkeywoBalance = $getkeywoBalance["errMsg"];
                                        $keywoSearchEarning = $getkeywoBalance["search_earning"];
                                        $keywoContentViewEarning = $getkeywoBalance["social_content_view_earnings"];

                                        $totalKeywoBalance = $keywoSearchEarning + $keywoContentViewEarning;


                                        $errMsg = "Success : get {$keywoUser} balance details";
                                        $stepCounterkeywoUser++; // Step counter

                                        $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";


                                        // start transaction for keywon credit earning transaction
                                        $startKeywoEarningTrans = startTransaction($kwdDbConn);
                                        if (noError($startKeywoEarningTrans)) {

                                            // insert keywo user earning transaction @wallet server

                                            // parameters required to insert in transaction
                                            $recipientEmail = $keywoUser;
                                            $recipientUserId = $keywoUserId;
                                            $senderEmail = $communityPoolUser;
                                            $senderUserId = $communityPoolUserId;
                                            $amount = $currentPayout;
                                            $type = $transType;
                                            $paymentMode = 'ITD';
                                            $remoteIp = $originIp;
                                            $exchangeJson = $exchangeRateInJson;

                                            $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $keywoUser, "current_payout" => $currentPayout, "trans_type" => $type, "exchange_rate" => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => $contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0,"post_type"=>$postType);

                                            $insertKeywoEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $remoteIp, $exchangeJson, $metaDetails);

                                            $response = json_encode($insertKeywoEarningTrnasaction["errMsg"]);
                                            if (noError($insertKeywoEarningTrnasaction)) {


                                                $errMsg = "Success : Insert user transaction for {$keywoUser}";
                                                $stepCounterkeywoUser++; // Step counter

                                                $keywoUserChild["step{$stepCounterkeywoUser}"]["data"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                $insertKeywoEarningTrnasaction = $insertKeywoEarningTrnasaction["errMsg"]["committed_transaction"];
                                                $transactionId = $insertKeywoEarningTrnasaction["transaction_id"];
                                                $transactionTime = $insertKeywoEarningTrnasaction["timestamp"];
                                                $transactionTime = $transactionTime / 1000; // convert millisecond to seconds
                                                $transactionTime = date('Y-m-d H:i:s', $transactionTime);

                                                // add keywo user earning transaction success into array to update cdp_step_done field into content_consumer_list table.
                                                $cdpStepDone["keywo"]["insert_user_transaction"] = "Success";

                                                // convert array into json
                                                $keywoEarningTrans = json_encode($cdpStepDone);

                                                // update $keywoEarningTrans into cdp_steps_done field
                                                $updateKeywoTrans = udpateContentConsumer($recordId, "cdp_steps_done = '" . $keywoEarningTrans . "'", $kwdDbConn);
                                                if (noError($updateKeywoTrans)) {


                                                    $errMsg = "Success : update cdp step for insert user transaction: " . $keywoEarningTrans;
                                                    $stepCounterkeywoUser++; // Step counter

                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                    // commit transaction for keywo earning transaction
                                                    $commitKeywoTrans = commitTransaction($kwdDbConn);
                                                    if (noError($commitKeywoTrans)) {


                                                        $errMsg = "Success : Commit transaction";
                                                        $stepCounterkeywoUser++; // Step counter

                                                        $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                        // start transaction
                                                        $startTransaction = startTransaction($kwdDbConn);
                                                        if (noError($startTransaction)) {

                                                            // close keyword database connection
                                                            mysqli_close($kwdDbConn);

                                                            // create search dataabse connection
                                                            $searchDbConn = createDBConnection("dbsearch");
                                                            if (noError($searchDbConn)) {
                                                                $searchDbConn = $searchDbConn["connection"];

                                                                //4.  insert keywo user transaction details into week wise insert search history @mysql database
                                                                $insertSearchHistoryTrans = insertSearchHistory($keywoUser, $keyword, $transType, $currentPayout, $exchangeRateInJson, $contentId, $transactionId, $transactionTime, $originIp, $searchDbConn);


                                                                if (noError($insertSearchHistoryTrans)) {


                                                                    $errMsg = "Success : Insert search history for {$keywoUser}";
                                                                    $stepCounterkeywoUser++; // Step counter

                                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                    // close search database connection
                                                                    mysqli_close($searchDbConn);

                                                                    // create keyword database connection
                                                                    $keywordDbConn = createDBConnection("dbkeywords");
                                                                    if (noError($keywordDbConn)) {
                                                                        $keywordDbConn = $keywordDbConn["connection"];

                                                                        // add keywo user transaction search history into array to update cdp_steps_done field into content_consumer_list table.
                                                                        $cdpStepDone["keywo"]["insert_keywo_transaction_search_history"] = "Success";

                                                                        // convert into json
                                                                        $keywoTransHistory = json_encode($cdpStepDone);

                                                                        // update $keywoEarningTrans into cdp_steps_done field
                                                                        $updateTransHistory = udpateContentConsumer($recordId, "cdp_steps_done = '" . $keywoTransHistory . "'", $keywordDbConn);
                                                                        if (noError($updateTransHistory)) {


                                                                            $errMsg = "Success : update cdp step for insert search history of {$keywoUser}";
                                                                            $stepCounterkeywoUser++; // Step counter

                                                                            $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                            // commit insert keywo transaction into search history
                                                                            $commitKeywoTransHistory = commitTransaction($keywordDbConn);
                                                                            if (noError($commitKeywoTransHistory)) {


                                                                                $errMsg = "Success : commit transaction";
                                                                                $stepCounterkeywoUser++; // Step counter

                                                                                $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                                // update cpd_status = 1 in content_consumer_list table as keywo@keywo.com process done successfully
                                                                                $udpateKeywoCDPStatus = udpateContentConsumer($recordId, "cdp_status = 1", $keywordDbConn);

                                                                                if (noError($udpateKeywoCDPStatus)) {


                                                                                    $errMsg = "Success : update cdp status for {$keywoUser}: cdp_status = 1";
                                                                                    $stepCounterkeywoUser++; // Step counter

                                                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                                    $xml_data['keywoUser ']["data"] = '';
                                                                                    $xml_data['keywoUser']["attribute"] = $xml_atrr;

                                                                                    $xml_data['keywoUser ']["data"] = '';
                                                                                    $xml_data['keywoUser']["childelement"] = $keywoUserChild;


                                                                                    /************************* Done coin distribution process for keywo@keywo.com user ***********************/
                                                                                    // close keyword database connection
                                                                                    mysqli_close($keywordDbConn);

                                                                                    /************************** Start appdeveloper@keywo.com earning or content creator earning process ********************************/

                                                                                    if ($modeType == "search") {


                                                                                        // app developer earning process

                                                                                        // create search database connection
                                                                                        $searchDbConn = createDBConnection("dbsearch");
                                                                                        if (noError($searchDbConn)) {
                                                                                            $searchDbConn = $searchDbConn["connection"];


                                                                                            // get appdeveloper user details
                                                                                            $appDevReqField = $userRequiredFields . ",gender,first_name,last_name";

                                                                                            $getAppDevDetails = getUserInfo($appDeveloperUser, $walletURLIP . 'api/v3/', $appDevReqField);

                                                                                            if (noError($getAppDevDetails)) {
                                                                                                $getAppDevDetails = $getAppDevDetails["errMsg"];
                                                                                                $appDevGender = $getAppDevDetails["gender"];
                                                                                                $usernameAppDev = $getAppDevDetails["first_name"] . " " . $getAppDevDetails["last_name"];
                                                                                            }

                                                                                            // Retrieve app name  or corresponding app id.
                                                                                            $getAppDetails = getAppDetails($searchDbConn, $contentId);


                                                                                            $response = json_encode($getAppDetails["errMsg"]);

                                                                                            if (noError($getAppDetails)) {
                                                                                                $getAppDetails = $getAppDetails["errMsg"][0];
                                                                                                $appName = $getAppDetails["app_name"];
                                                                                                // convert into lower case
                                                                                                $appName = strtolower($appName);

                                                                                                $xml_atrr = '';
                                                                                                $xml_atrr["user"] = $appDeveloperUser;
                                                                                                $xml_atrr["amount_given_to_user"] = $currentPayout;
                                                                                                $xml_atrr["interaction_type"] = "app_earning";
                                                                                                $xml_atrr["app_id"] = $contentId;
                                                                                                $xml_atrr["app_dev_gender"] = $appDevGender;
                                                                                                $xml_atrr["response"] = $response;
                                                                                                $xml_atrr["username"] = $usernameAppDev;

                                                                                                $errMsg = "Success : get app details of {$appName}";
                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";


                                                                                                // log data

                                                                                                /*  1. Update totalQualifiedSearches field by 1 into sc_app_details table of corresponding appId.
                                                                                                2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
                                                                                                3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
                                                                                                */

                                                                                                $updateKeywordSerches = updateAppSearchCount($contentId, $appName, $keyword, $interactionType, $searchDbConn);

                                                                                                $response = json_encode($updateKeywordSerches["errMsg"]);

                                                                                                if (noError($updateKeywordSerches)) {


                                                                                                    $errMsg = "Success : update {$appName} app search details.";
                                                                                                    $stepCounterAppDev++; // Step counter

                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                    // close search database connection
                                                                                                    mysqli_close($searchDbConn);

                                                                                                    // create keyword database connection
                                                                                                    $kwdDbConn = createDBConnection("dbkeywords");
                                                                                                    if (noError($kwdDbConn)) {

                                                                                                        $kwdDbConn = $kwdDbConn["connection"];

                                                                                                        // start transaction
                                                                                                        $startTransaction = startTransaction($kwdDbConn);
                                                                                                        if (noError($startTransaction)) {

                                                                                                            // close keyword database connection
                                                                                                            mysqli_close($kwdDbConn);

                                                                                                            // create search database connection
                                                                                                            $searchDbConn = createDBConnection("dbsearch");
                                                                                                            if (noError($searchDbConn)) {
                                                                                                                $searchDbConn = $searchDbConn["connection"];

                                                                                                                // increase app developer earning in sc_app_details table @mysql database
                                                                                                                $increaseAppDevEarning = increaseAppDeveloperEarning($contentId, $currentPayout, $searchDbConn);

                                                                                                                $response = json_encode($increaseAppDevEarning["errMsg"]);

                                                                                                                if (noError($increaseAppDevEarning)) {


                                                                                                                    $errMsg = "Success : Increase {$appName} app developer earning.";
                                                                                                                    $stepCounterAppDev++; // Step counter

                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                    // close search database connection
                                                                                                                    mysqli_close($searchDbConn);

                                                                                                                    // create keyword database connection
                                                                                                                    $kwdDbConn = createDBConnection("dbkeywords");
                                                                                                                    if (noError($kwdDbConn)) {
                                                                                                                        $kwdDbConn = $kwdDbConn["connection"];

                                                                                                                        $cdpStepDone["content_creator"]["app_developer_earning_in_mysql"] = "success";

                                                                                                                        // convert into json json_encode
                                                                                                                        $udpateAppEarning = json_encode($cdpStepDone);

                                                                                                                        $udpateCDPStepForAppEarning = udpateContentConsumer($recordId, "cdp_steps_done = '" . $udpateAppEarning . "'", $kwdDbConn);

                                                                                                                        $response = json_encode($udpateCDPStepForAppEarning["errMsg"]);

                                                                                                                        if (noError($udpateCDPStepForAppEarning)) {


                                                                                                                            $errMsg = "Success : update cdp step for {$appName} app developer earning: {$response}.";
                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                            $commitAppEarning = commitTransaction($kwdDbConn);

                                                                                                                            if (noError($commitAppEarning)) {


                                                                                                                                $errMsg = "Success : commit transaction.";
                                                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";


                                                                                                                                // increase total app payout amount of appDeveloper@keywo.com user.
                                                                                                                                $increaseAppEarningToAppDeveloper = creditUserEarning($appDeveloperUserId, $currentPayout, "appincome", "add");

                                                                                                                                $response = json_encode($increaseAppEarningToAppDeveloper["errMsg"]);

                                                                                                                                if (noError($increaseAppEarningToAppDeveloper)) {


                                                                                                                                    $errMsg = "Success : credit current payout amount to {$appDeveloperUser}.";
                                                                                                                                    $stepCounterAppDev++; // Step counter

                                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                    $increaseAppEarningToAppDeveloper = $increaseAppEarningToAppDeveloper["errMsg"];
                                                                                                                                    $totalAppEarning = $increaseAppEarningToAppDeveloper["total_app_income"];

                                                                                                                                    // start transaction
                                                                                                                                    $startTransaction = startTransaction($kwdDbConn);
                                                                                                                                    if (noError($startTransaction)) {
                                                                                                                                        $cdpStepDone["content_creator"]["update_user_balance"] = "success";

                                                                                                                                        $udpateAppIncome = json_encode($cdpStepDone);

                                                                                                                                        $udpateCDPStepDoneForAppIncome = udpateContentConsumer($recordId, "cdp_steps_done = '" . $udpateAppIncome . "'", $kwdDbConn);

                                                                                                                                        $response = json_encode($udpateCDPStepDoneForAppIncome["errMsg"]);

                                                                                                                                        if (noError($udpateCDPStepDoneForAppIncome)) {


                                                                                                                                            $errMsg = "Success : update cdp step done for app income: {$udpateAppIncome}.";
                                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";


                                                                                                                                            // commit transaction
                                                                                                                                            $commitAppIncome = commitTransaction($kwdDbConn);
                                                                                                                                            if (noError($commitAppIncome)) {


                                                                                                                                                $xml_atrr["commitAppIncomeuser"] = $appDeveloperUser;

                                                                                                                                                $errMsg = "Success : commit transaction.";
                                                                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                $startTransaction = startTransaction($kwdDbConn);
                                                                                                                                                if (noError($startTransaction)) {

                                                                                                                                                    // insert transaction for app income to appdeveloper @wallet server
                                                                                                                                                    // parameters required to insert in transaction
                                                                                                                                                    $recipientEmail = $appDeveloperUser;
                                                                                                                                                    $recipientUserId = $appDeveloperUserId;
                                                                                                                                                    $senderEmail = $communityPoolUser;
                                                                                                                                                    $senderUserId = $communityPoolUserId;
                                                                                                                                                    $amount = $currentPayout;
                                                                                                                                                    $type = "app_earning";
                                                                                                                                                    $paymentMode = 'ITD';
                                                                                                                                                    $originIp = $originIp;
                                                                                                                                                    $exchangeRateInJson = $exchangeRateInJson;

                                                                                                                                                    $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $appDeveloperUser, "trans_type" => $type, "current_payout" => $currentPayout, "exchange_rate" => $exchangeRateInJson, "payment_mode" => $paymentMode, "content_id" => (string)$contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                                                                                                                                                    $insertUserAppEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                                                                                    $response = json_encode($insertUserAppEarningTrnasaction["errMsg"]);

                                                                                                                                                    if (noError($insertUserAppEarningTrnasaction)) {


                                                                                                                                                        $errMsg = "Success : Insert user transaction for {$appDeveloperUser}.";
                                                                                                                                                        $stepCounterAppDev++; // Step counter

                                                                                                                                                        $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";


                                                                                                                                                        $cdpStepDone["content_creator"]["insert_user_transaction"] = "success";

                                                                                                                                                        $updateAppTransaction = json_encode($cdpStepDone);

                                                                                                                                                        $updateCDPStepForArrTransaction = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateAppTransaction . "'", $kwdDbConn);

                                                                                                                                                        $response = json_encode($updateCDPStepForArrTransaction["errMsg"]);

                                                                                                                                                        if (noError($updateCDPStepForArrTransaction)) {


                                                                                                                                                            $errMsg = "Success : update cdp step done for insert user transaction of {$appDeveloperUser}.";
                                                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                            // commit  Transaction
                                                                                                                                                            $commitAppTransaction = commitTransaction($kwdDbConn);
                                                                                                                                                            if (noError($commitAppTransaction)) {


                                                                                                                                                                $errMsg = "Success : commit transaction.";
                                                                                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                $insertUserAppEarningTrnasaction = $insertUserAppEarningTrnasaction["errMsg"]["committed_transaction"];

                                                                                                                                                                $transId = $insertUserAppEarningTrnasaction["transaction_id"];
                                                                                                                                                                $transTime = $insertUserAppEarningTrnasaction["timestamp"];
                                                                                                                                                                $transTime = $transTime / 1000; // convert millisecond to seconds
                                                                                                                                                                $transTime = date('Y-m-d H:i:s', $transTime);

                                                                                                                                                                // start transaction
                                                                                                                                                                $startTransaction = startTransaction($kwdDbConn);
                                                                                                                                                                if (noError($startTransaction)) {

                                                                                                                                                                    // close keyword database connection
                                                                                                                                                                    mysqli_close($kwdDbConn);

                                                                                                                                                                    // create search database connection
                                                                                                                                                                    $searchDbConn = createDBConnection("dbsearch");
                                                                                                                                                                    if (noError($searchDbConn)) {
                                                                                                                                                                        $searchDbConn = $searchDbConn["connection"];

                                                                                                                                                                        // insert transaction into week wise user search history in search database.
                                                                                                                                                                        $insertAppUserHistory = insertSearchHistory($appDeveloperUser, $keyword, $type, $currentPayout, $exchangeRateInJson, $contentId, $transId, $transTime, $originIp, $searchDbConn);

                                                                                                                                                                        $response = json_encode($insertAppUserHistory["errMsg"]);

                                                                                                                                                                        if (noError($insertAppUserHistory)) {


                                                                                                                                                                            $errMsg = "Success : Insert search history for app developer.";
                                                                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                            // close search database connection
                                                                                                                                                                            mysqli_close($searchDbConn);

                                                                                                                                                                            // create keyword database connection
                                                                                                                                                                            $kwdDbConn = createDBConnection("dbkeywords");
                                                                                                                                                                            if (noError($kwdDbConn)) {
                                                                                                                                                                                $kwdDbConn = $kwdDbConn["connection"];
                                                                                                                                                                                // increase  qualified_interaction search count into keywo stats table instead of pool user.
                                                                                                                                                                                $increaseKeywoQulifiedSearches = increaseKeywoStatsSearches($interactionType, $kwdDbConn);

                                                                                                                                                                                $response = json_encode($increaseKeywoQulifiedSearches["errMsg"]);

                                                                                                                                                                                if (noError($increaseKeywoQulifiedSearches)) {


                                                                                                                                                                                    $errMsg = "Success : Increase keywo qualified interaction by 1.";
                                                                                                                                                                                    $stepCounterAppDev++; // Step counter

                                                                                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                    $cdpStepDone["content_creator"]["insert_app_transaction_search_history"] = "success";

                                                                                                                                                                                    $updateAppHistory = json_encode($cdpStepDone);

                                                                                                                                                                                    $updateCDPStepForAppHistory = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateAppHistory . "'", $kwdDbConn);

                                                                                                                                                                                    if (noError($updateCDPStepForAppHistory)) {


                                                                                                                                                                                        $errMsg = "Success : update cdp step doen for app developer transaction:";
                                                                                                                                                                                        $stepCounterAppDev++; // Step counter

                                                                                                                                                                                        $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                        // udpate cdp_status = 2 on success of app developer process done
                                                                                                                                                                                        $updateAppCDPStatus = udpateContentConsumer($recordId, "cdp_status = 2", $kwdDbConn);
                                                                                                                                                                                        if (noError($updateAppCDPStatus)) {


                                                                                                                                                                                            $errMsg = "Success : update cdp status for app developer: cdp_status = 2";
                                                                                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";


                                                                                                                                                                                            $commitAppTransHistory = commitTransaction($kwdDbConn);
                                                                                                                                                                                            if (noError($commitAppTransHistory)) {


                                                                                                                                                                                                $errMsg = "Success : commit transaction";
                                                                                                                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;
                                                                                                                                                                                                // log to be write
                                                                                                                                                                                            } else {

                                                                                                                                                                                                $errMsg = "Failed : commit transaction";
                                                                                                                                                                                                $stepCounterAppDev++; // Step counter

                                                                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                                                print('Error: Unable to commit transaction for app transaction history');

                                                                                                                                                                                            }
                                                                                                                                                                                        } else {


                                                                                                                                                                                            $errMsg = "Failed : update cdp status for app developer: cdp_status = 2";
                                                                                                                                                                                            $stepCounterAppDev++; // Step counter

                                                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                            $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                            $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                                            print('Error: updating app cdp status ');

                                                                                                                                                                                        }
                                                                                                                                                                                    } else {


                                                                                                                                                                                        $errMsg = "Failed : update cdp step doen for app developer transaction:";
                                                                                                                                                                                        $stepCounterAppDev++; // Step counter

                                                                                                                                                                                        $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                        $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                        $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                                        $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                        $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                                        print('Error: updating cdp step for insert app transaction hisotry');

                                                                                                                                                                                    }


                                                                                                                                                                                } else {


                                                                                                                                                                                    $errMsg = "Failed : Increase keywo qualified interaction by 1.";
                                                                                                                                                                                    $stepCounterAppDev++; // Step counter

                                                                                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                    $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                                    $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                                    print($increaseKeywoQulifiedSearches["errMsg"]);

                                                                                                                                                                                }


                                                                                                                                                                            } else {
                                                                                                                                                                                print('Error: unabkle to create keyword database conneciton');

                                                                                                                                                                            }

                                                                                                                                                                        } else {


                                                                                                                                                                            $errMsg = "Failed : Insert search history for app developer.";

                                                                                                                                                                            $stepCounterAppDev++; // Step counter
                                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                            $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                            $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                            print('Error: Unable to insert app developer transaction earning history into user history table in search database');

                                                                                                                                                                        }

                                                                                                                                                                    } else {
                                                                                                                                                                        print('Error: Unable to create search database connection');

                                                                                                                                                                    }

                                                                                                                                                                } else {
                                                                                                                                                                    print('Error: unable to start transaction to insert search history for app transaction');

                                                                                                                                                                }

                                                                                                                                                            } else {


                                                                                                                                                                $errMsg = "Failed : commit transaction.";
                                                                                                                                                                $stepCounterAppDev++; // Step counter
                                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                                print('Error: unable to commit transaction in cdp step for app transaciton');

                                                                                                                                                            }
                                                                                                                                                        } else {


                                                                                                                                                            $errMsg = "Failed : update cdp step done for insert user transaction of {$appDeveloperUser}.";
                                                                                                                                                            $stepCounterAppDev++; // Step counter
                                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                            $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                            $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                            print('Error: Unabel to update cdp step for app transaction');

                                                                                                                                                        }

                                                                                                                                                    } else {


                                                                                                                                                        $errMsg = "Failed : Insert user transaction for {$appDeveloperUser}.";
                                                                                                                                                        $stepCounterAppDev++; // Step counter
                                                                                                                                                        $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                        $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                        $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                        $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                        $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                        print('Error: Unable to insert app earning transaction');

                                                                                                                                                    }


                                                                                                                                                } else {
                                                                                                                                                    print('Error: start transaction to insert user transaction');

                                                                                                                                                }


                                                                                                                                            } else {


                                                                                                                                                $errMsg = "Failed : commit transaction.";
                                                                                                                                                $stepCounterAppDev++; // Step counter
                                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                                print('Error: unable to commit trnaction for app income');

                                                                                                                                            }
                                                                                                                                        } else {


                                                                                                                                            $errMsg = "Failed : update cdp step done for app income: {$udpateAppIncome}.";
                                                                                                                                            $stepCounterAppDev++; // Step counter
                                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                            $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                                            $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                            print('Error: unable to update cdp step for app income');

                                                                                                                                        }
                                                                                                                                    } else {
                                                                                                                                        print('Error: start transaction');

                                                                                                                                    }

                                                                                                                                } else {


                                                                                                                                    $errMsg = "Success : credit current payout amount to {$appDeveloperUser}.";
                                                                                                                                    $stepCounterAppDev++; // Step counter
                                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                                    $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                                    $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                    print('Error: Unable to increase app payout amount to app developer.');

                                                                                                                                }

                                                                                                                            } else {


                                                                                                                                $errMsg = "Failed : commit transaction.";
                                                                                                                                $stepCounterAppDev++; // Step counter
                                                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                                print('Error: Unable to commit app earning');

                                                                                                                            }

                                                                                                                        } else {


                                                                                                                            $errMsg = "Failed : update cdp step for {$appName} app developer earning: {$response}.";
                                                                                                                            $stepCounterAppDev++; // Step counter
                                                                                                                            $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                            $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                            $xml_data['appDeveloper']["data"] = '';
                                                                                                                            $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                            print('Error: unable to udpate the cdp step for app developer earning in table');

                                                                                                                        }

                                                                                                                    } else {
                                                                                                                        print('Error: keyword database connection');

                                                                                                                    }

                                                                                                                } else {


                                                                                                                    $errMsg = "Failed : Increase {$appName} app developer earning.";
                                                                                                                    $stepCounterAppDev++; // Step counter
                                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                    $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                                    $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                                    // close search database connection
                                                                                                                    mysqli_close($searchDbConn);

                                                                                                                    // create keyword database connection
                                                                                                                    $kwdDbConn = createDBConnection("dbkeywords");
                                                                                                                    if (noError($kwdDbConn)) {
                                                                                                                        $kwdDbConn = $kwdDbConn["connection"];

                                                                                                                        // roll back the current value from cdp_step_done filed
                                                                                                                        $rollBackAppEarning = rollbackTransaction($kwdDbConn);
                                                                                                                        if (!noError($rollBackAppEarning)) {
                                                                                                                            print('Error: rollback steps');
                                                                                                                        }
                                                                                                                    }

                                                                                                                    print($increaseAppDevEarning["errMsg"]);

                                                                                                                }
                                                                                                            } else {
                                                                                                                print('Error: search databsae connection');

                                                                                                            }
                                                                                                        } else {
                                                                                                            print('Error: start transaction');

                                                                                                        }

                                                                                                    } else {
                                                                                                        print('Error: Creating keyword database connection');

                                                                                                    }

                                                                                                } else {


                                                                                                    $errMsg = "Failed : update {$appName} app search details.";
                                                                                                    $stepCounterAppDev++; // Step counter
                                                                                                    $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                    $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                    $xml_data['appDeveloper']["data"] = '';
                                                                                                    $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                    print($updateKeywordSerches["errMsg"]);

                                                                                                }
                                                                                            } else {


                                                                                                $errMsg = "Failed : get app details of {$appName}";
                                                                                                $stepCounterAppDev++; // Step counter
                                                                                                $appDeveloperChild["step{$stepCounterAppDev}"] = "{$stepCounterAppDev}. {$errMsg}";

                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                $xml_data['appDeveloper']["attribute"] = $xml_atrr;

                                                                                                $xml_data['appDeveloper']["data"] = '';
                                                                                                $xml_data['appDeveloper']["childelement"] = $appDeveloperChild;

                                                                                                print('Error: Unable to fetch app details');

                                                                                            }

                                                                                        } else {
                                                                                            print('Error: create search database connection');

                                                                                        }
                                                                                    } elseif ($modeType == "social") {


                                                                                        // coin distribution process done for content creator earning
                                                                                        // 1. Check if content creator have sharer
                                                                                        // if exist then calculate the sharer amount from content creator payout
                                                                                        // 2. credit total payout amount to content creator
                                                                                        // 3. Insert user transaction for content creator earning
                                                                                        // 4. if sharer exist then credit calculated sharer amount to post sharer
                                                                                        // 5  Insert user transaction for post sharer

                                                                                        // close search database connection
                                                                                        mysqli_close($searchDbConn);

                                                                                        // create keyword database connection
                                                                                        $kwdDbConn = createDBConnection("dbkeywords");

                                                                                        if (noError($kwdDbConn)) {
                                                                                            $kwdDbConn = $kwdDbConn["connection"];

                                                                                            // 1. Check if content creator have sharer
                                                                                            // if exist then calculate the sharer amount from content creator payout

                                                                                            if (isset($sharerId) && !empty($sharerId)) {
                                                                                                // get sharer payout
                                                                                                $calculateSharerPayout = calculateSharerPayout($currentPayout, $kwdDbConn);

                                                                                                $response = json_encode($calculateSharerPayout["data"]);
                                                                                                if (noError($calculateSharerPayout)) {

                                                                                                    $xml_atrr = '';
                                                                                                    $xml_atrr["user"] = '';
                                                                                                    $xml_atrr["id"] = $creatorId;
                                                                                                    $xml_atrr["response"] = $response;

                                                                                                    $errMsg = "Success: calculate sharer payout";
                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                    $calculateSharerPayout = $calculateSharerPayout["data"];
                                                                                                    $sharerPayoutAmnt = $calculateSharerPayout["sharerPayoutAmnt"];
                                                                                                    $currentPayout = $calculateSharerPayout["contentCreatorPayoutAmnt"];
                                                                                                } else {

                                                                                                    $xml_atrr = '';
                                                                                                    $xml_atrr["user"] = '';
                                                                                                    $xml_atrr["id"] = $creatorId;
                                                                                                    $xml_atrr["response"] = $response;

                                                                                                    $errMsg = "Failed: calculate sharer payout";
                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                    $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                    $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                    print($calculateSharerPayout["errMsg"]);

                                                                                                }
                                                                                            }

                                                                                            // start transaction
                                                                                            $startTransaction = startTransaction($kwdDbConn);
                                                                                            if (noError($startTransaction)) {

                                                                                                $requestedField = $userRequiredFields . ',email,gender';
                                                                                                $getUserEmail = getUserDetailFromUserId($creatorId, $walletURLIP . 'api/v3/', $requestedField);
                                                                                                if (noError($getUserEmail)) {
                                                                                                    $getUserEmail = $getUserEmail["errMsg"];
                                                                                                    $creatorEmailId = $getUserEmail["email"];
                                                                                                    $contentCreatorGender = $getUserEmail["gender"];
                                                                                                }

                                                                                                // // 2. credit total payout amount to content creator
                                                                                                $creditCreatorEarning = creditUserEarning($creatorId, $currentPayout, "post_creator_earnings", "add");


                                                                                                $response = json_encode($creditCreatorEarning["errMsg"]);

                                                                                                if (noError($creditCreatorEarning)) {


                                                                                                    $xml_atrr["user"] = $creatorEmailId;
                                                                                                    $xml_atrr["id"] = $creatorId;
                                                                                                    $xml_atrr["amount_given_to_user"] = $currentPayout;
                                                                                                    $xml_atrr["interaction_type"] = "post_creator_earning";
                                                                                                    $xml_atrr["post_id"] = $contentId;
                                                                                                    $xml_atrr["creator_gender"] = $contentCreatorGender;


                                                                                                    $xml_atrr["response_creator"] = $response;

                                                                                                    $errMsg = "Success: credit current payout amount to {$creatorId}";
                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                    $cdpStepDone["content_creator"]["update_user_balance"] = "success";
                                                                                                    $updateCreatorEarning = json_encode($cdpStepDone);

                                                                                                    $updateCDPStepForCreatorEarning = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateCreatorEarning . "'", $kwdDbConn);

                                                                                                    $response = json_encode($updateCDPStepForCreatorEarning["errMsg"]);

                                                                                                    if (noError($updateCDPStepForCreatorEarning)) {


                                                                                                        $errMsg = "Success: update cdp step done for creator earning.";
                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                        $commitCreatorEarning = commitTransaction($kwdDbConn);
                                                                                                        if (noError($commitCreatorEarning)) {


                                                                                                            $errMsg = "Success: commit transaction.";
                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                            // start transaction
                                                                                                            $startTransaction = startTransaction($kwdDbConn);
                                                                                                            if (noError($startTransaction)) {


                                                                                                                $recipientEmail = $creatorEmailId;
                                                                                                                $recipientUserId = $creatorId;
                                                                                                                $senderEmail = $communityPoolUser;
                                                                                                                $senderUserId = $communityPoolUserId;
                                                                                                                $amount = $currentPayout;
                                                                                                                $type = "post_creator_earning";
                                                                                                                $paymentMode = 'ITD';
                                                                                                                $originIp = $originIp;
                                                                                                                $exchangeRateInJson = $exchangeRateInJson;

                                                                                                                $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $creatorEmailId, "trans_type" => $type, "current_payout" => $currentPayout, "exchange_rate" => $exchangeRateInJson, "content_id" => $contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0, "post_created_at" => $post_created_at,"post_type"=>$postType);

                                                                                                                $insertCreatorPayoutTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                                                $response = json_encode($insertCreatorPayoutTrnasaction["errMsg"]);

                                                                                                                // need to remove after integrate real api
                                                                                                                //  $insertCreatorPayoutTrnasaction = array("errCode" => -1);
                                                                                                                if (noError($insertCreatorPayoutTrnasaction)) {


                                                                                                                    $errMsg = "Success: Insert user transaction for content creator earning.";
                                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"]["data"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                                    $cdpStepDone["content_creator"]["insert_user_transaction"] = "success";

                                                                                                                    $updateCreatorTrans = json_encode($cdpStepDone);

                                                                                                                    $udpateCDPStepForCreatorTrans = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateCreatorTrans . "'", $kwdDbConn);
                                                                                                                    if (noError($udpateCDPStepForCreatorTrans)) {


                                                                                                                        $errMsg = "Success: update cdp step done for insert user transaction. {$updateCreatorTrans}";
                                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                                        $commitCreatorTrans = commitTransaction($kwdDbConn);


                                                                                                                        if (noError($commitCreatorTrans)) {


                                                                                                                            $errMsg = "Success: commit transaction";
                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                                            // 4. if sharer exist then credit calculated sharer amount to post sharer
                                                                                                                            if (isset($sharerId) && !empty($sharerId)) {

                                                                                                                                $requestedField = $userRequiredFields . ',email,gender';
                                                                                                                                $getUserEmail = getUserDetailFromUserId($sharerId, $walletURLIP . 'api/v3/', $requestedField);
                                                                                                                                if (noError($getUserEmail)) {
                                                                                                                                    $getUserEmail = $getUserEmail["errMsg"];
                                                                                                                                    $sharerEmailId = $getUserEmail["email"];
                                                                                                                                    $sharerGender = $getUserEmail["gender"];
                                                                                                                                }


                                                                                                                                $startTransaction = startTransaction($kwdDbConn);
                                                                                                                                if (noError($startTransaction)) {
                                                                                                                                    // credit sharer payout amount
                                                                                                                                    $sharerCreditEarning = creditUserEarning($sharerId, $sharerPayoutAmnt, "post_sharer_earnings", "add");

                                                                                                                                    $response = json_encode($sharerCreditEarning["errMsg"]);

                                                                                                                                    if (noError($sharerCreditEarning)) {


                                                                                                                                        $xml_atrr["sharer_user"] = $sharerEmailId;
                                                                                                                                        $xml_atrr["sharer_id"] = $sharerId;
                                                                                                                                        $xml_atrr["amount_given_to_sharer_user"] = $sharerPayoutAmnt;
                                                                                                                                        $xml_atrr["interaction_type_share"] = "post_sharer_earning";
                                                                                                                                        $xml_atrr["content_sharer_gender"] = $sharerGender;
                                                                                                                                        $xml_atrr["response_sharer"] = $response;

                                                                                                                                        $errMsg = "Success: Credit sharer payout amount to {$sharerEmailId}";
                                                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                                                        $cdpStepDone["content_sharer"]["update_user_balance"] = "success";

                                                                                                                                        $updateSharerJson = json_encode($cdpStepDone);

                                                                                                                                        $updateCDPStepForSharerEarning = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateSharerJson . "'", $kwdDbConn);

                                                                                                                                        $response = json_encode($updateCDPStepForSharerEarning["errMsg"]);

                                                                                                                                        if (noError($updateCDPStepForSharerEarning)) {


                                                                                                                                            $xml_atrr["response"] = $response;

                                                                                                                                            $errMsg = "Success:  update cdp step done for sharer payout amount as {$updateSharerJson}";
                                                                                                                                            $stepCounterContentCreator++; // Step counter
                                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                                                            $commitSharerEarning = commitTransaction($kwdDbConn);
                                                                                                                                            if (noError($commitSharerEarning)) {


                                                                                                                                                $errMsg = "Success:  commit transaction";
                                                                                                                                                $stepCounterContentCreator++; // Step counter

                                                                                                                                                $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                                                                // start transaction
                                                                                                                                                $startTransaction = startTransaction($kwdDbConn);
                                                                                                                                                if (noError($startTransaction)) {
                                                                                                                                                    // insert suer transaction for sharer earning
                                                                                                                                                    $recipientEmail = $sharerEmailId;
                                                                                                                                                    $recipientUserId = $sharerId;
                                                                                                                                                    $senderEmail = $communityPoolUser;
                                                                                                                                                    $senderUserId = $communityPoolUserId;
                                                                                                                                                    $amount = $sharerPayoutAmnt;
                                                                                                                                                    $type = "post_sharer_earning";
                                                                                                                                                    $paymentMode = 'ITD';
                                                                                                                                                    $originIp = $originIp;
                                                                                                                                                    $exchangeRateInJson = $exchangeRateInJson;

                                                                                                                                                    $metaDetails = array("reference_transaction_id" => $walletTransactionId, "sender" => $communityPoolUser, "receiver" => $sharerEmailId, "trans_type" => $type, "share_payout" => $sharerPayoutAmnt, "exchange_rate" => $exchangeRateInJson, "content_id" => $contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0, "post_created_at" => $post_created_at,"post_type"=>$postType);

                                                                                                                                                    $insertSharerPayoutTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                                                                                    $response = json_encode($insertSharerPayoutTrnasaction["errMsg"]);

                                                                                                                                                    //  $insertSharerPayoutTrnasaction = array("errCode" => -1);
                                                                                                                                                    if (noError($insertSharerPayoutTrnasaction)) {


                                                                                                                                                        $errMsg = "Success:  Insert user transaction for sharer payout";
                                                                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";


                                                                                                                                                        $cdpStepDone["content_sharer"]["insert_user_transaction"] = "success";

                                                                                                                                                        $updateSharerTrans = json_encode($cdpStepDone);

                                                                                                                                                        // update cdpStepDone field in content consumer list
                                                                                                                                                        $updateCDPStepForSharerTrans = udpateContentConsumer($recordId, "cdp_steps_done = '" . $updateSharerTrans . "'", $kwdDbConn);
                                                                                                                                                        if (noError($updateCDPStepForSharerTrans)) {


                                                                                                                                                            $errMsg = "Success: Update cdp step done for insert user transaction for sharer earning. as {$updateSharerTrans}";
                                                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                                            $xml_data['contentCreator']["attribute"] = $xml_atrr;


                                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                                            $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;


                                                                                                                                                            // udpate cdp_status = 2 on success of app developer process done


                                                                                                                                                        } else {


                                                                                                                                                            $errMsg = "Failed: Update cdp step done for insert user transaction for sharer earning. as {$updateSharerTrans}";

                                                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                                            $xml_data['contentCreator']["attribute"] = $xml_atrr;


                                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                                            $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                                            print('Error: updating cdp step for sharer transaction');

                                                                                                                                                        }
                                                                                                                                                    } else {


                                                                                                                                                        $errMsg = "Failed:  Insert user transaction for sharer payout";
                                                                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                                                                        $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                                                                        $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                                        $rollbackSharerTrans = rollbackTransaction($kwdDbConn);
                                                                                                                                                        if (!noError($rollbackSharerTrans)) {
                                                                                                                                                            print('Error: Unable to rollback the sharer transaction');
                                                                                                                                                        }

                                                                                                                                                        print('Error: inserting user transaction for sharer payout');

                                                                                                                                                    }
                                                                                                                                                } else {
                                                                                                                                                    print('Error: start transaction for insert user transaction');

                                                                                                                                                }
                                                                                                                                            } else {

                                                                                                                                                $errMsg = "Failed:  commit transaction";
                                                                                                                                                $stepCounterContentCreator++; // Step counter

                                                                                                                                                $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                                $xml_data['contentCreator']["data"] = '';
                                                                                                                                                $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                                $xml_data['contentCreator']["data"] = '';
                                                                                                                                                $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                                print('Error: commiting sharer cdp steps');

                                                                                                                                            }
                                                                                                                                        } else {


                                                                                                                                            $errMsg = "Failed:  update cdp step done for sharer payout amount as {$updateSharerJson}";
                                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                            $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                                            $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                            print('Error: Updating cdp step for sharer earning');

                                                                                                                                        }
                                                                                                                                    } else {


                                                                                                                                        $errMsg = "Failed: Credit sharer payout amount to {$sharerEmailId}";
                                                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                                                        $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                                                        $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                        $rollbackSharerCredit = rollbackTransaction($kwdDbConn);
                                                                                                                                        if (!noError($rollbackSharerCredit)) {
                                                                                                                                            print('Error: Unable to rollback sharer credit');
                                                                                                                                        }

                                                                                                                                        print('Error: Unable to credit amount to sharer user');

                                                                                                                                    }
                                                                                                                                } else {
                                                                                                                                    print('Error: start transaction for credit sharer earning');

                                                                                                                                }
                                                                                                                            }

                                                                                                                            $updateAppCDPStatus = udpateContentConsumer($recordId, "cdp_status = 2", $kwdDbConn);

                                                                                                                            if (noError($updateAppCDPStatus)) {


                                                                                                                                $errMsg = "Success: Update cdp status for content creator as cdp_status = 2";
                                                                                                                                $stepCounterContentCreator++; // Step counter

                                                                                                                                $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                                                // commit transaction
                                                                                                                                $commitSharerTrans = commitTransaction($kwdDbConn);
                                                                                                                                if (noError($commitSharerTrans)) {


                                                                                                                                    $errMsg = "Success: commit transaction";

                                                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";

                                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                                    $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                                    $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;
                                                                                                                                } else {


                                                                                                                                    $errMsg = "Failed: commit transaction";
                                                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                                    $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                                    $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                    print('Error: Unable to commit sharer transaction into cdp steps');

                                                                                                                                }
                                                                                                                            } else {


                                                                                                                                $errMsg = "Failed: Update cdp status for content creator as cdp_status = 2";
                                                                                                                                $stepCounterContentCreator++; // Step counter

                                                                                                                                $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                                $xml_data['contentCreator']["data"] = '';
                                                                                                                                $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                                $xml_data['contentCreator']["data"] = '';
                                                                                                                                $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                                print('Error: Unable to update cdp status');

                                                                                                                            }
                                                                                                                        } else {


                                                                                                                            $errMsg = "Failed: commit transaction";
                                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                            $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                                            $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;


                                                                                                                            print('Error: Unable to commit transaction for creator transaction');

                                                                                                                        }
                                                                                                                    } else {
                                                                                                                        print('Error: Updating cdp step for creator transaction');

                                                                                                                    }
                                                                                                                } else {


                                                                                                                    $errMsg = "Failed: Insert user transaction for content creator earning.";
                                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                    $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                                    $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                                    print('Error: insert user transaction for creator earning');

                                                                                                                }

                                                                                                            } else {
                                                                                                                print('Error: Unabel to start transaction for insert user transaction');

                                                                                                            }

                                                                                                        } else {


                                                                                                            $errMsg = "Failed: commit transaction.";
                                                                                                            $stepCounterContentCreator++; // Step counter

                                                                                                            $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                            $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                            $xml_data['contentCreator']["data"] = '';
                                                                                                            $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;
                                                                                                            print('Error: unable to commit creator earning');

                                                                                                        }
                                                                                                    } else {


                                                                                                        $errMsg = "Failed: update cdp step done for creator earning.";
                                                                                                        $stepCounterContentCreator++; // Step counter

                                                                                                        $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                        $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                        $xml_data['contentCreator']["data"] = '';
                                                                                                        $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                        print('Error: updating creator earning cdp steps');

                                                                                                    }
                                                                                                } else {


                                                                                                    $errMsg = "Failed: credit current payout amount to {$creatorId}";
                                                                                                    $stepCounterContentCreator++; // Step counter

                                                                                                    $ContentCreatorChild["step{$stepCounterContentCreator}"] = "{$stepCounterContentCreator}. {$errMsg}";
                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                    $xml_data['contentCreator']["attribute"] = $xml_atrr;

                                                                                                    $xml_data['contentCreator']["data"] = '';
                                                                                                    $xml_data['contentCreator']["childelement"] = $ContentCreatorChild;

                                                                                                    $rollbackCreatorEarning = rollbackTransaction($kwdDbConn);
                                                                                                    if (!noError($rollbackCreatorEarning)) {
                                                                                                        print('Error: unable to rollback creator earning from cdp steps');
                                                                                                    }

                                                                                                    print('Error: Unable to credit content creator earning');

                                                                                                }
                                                                                            } else {
                                                                                                print('Error: start transaction');

                                                                                            }
                                                                                        } else {
                                                                                            print('Error : creating keyword databse connection');

                                                                                        }

                                                                                    } // end of else if ($modeType == "social")

                                                                                    /************************ Finish appdeveloper@keywo.com or content_creator earning and transaction *********************************/

                                                                                    /************************* Start keywordowner@keywo.com earning and transaction ******************************/
                                                                                    // increase keyword earning and keyword owner earning

                                                                                    //  mysqli_close($searchDbConn);

                                                                                    // create keyword dataabse connection
                                                                                    $keywordDbConn = createDBConnection("dbkeywords");

                                                                                    if (noError($keywordDbConn)) {
                                                                                        $keywordDbConn = $keywordDbConn["connection"];


                                                                                        $increaseKwdOwnerEarning = updateKwdAndOwnerEarning($keyword, $interactionType, $consumerEmail, $currentPayout, $contentId, $originIp, $exchangeRateInJson, $keywordDbConn, $walletTransactionId, $xml_data, $recordId, $cdpStepDone, $xml_atrr, $stepCounterKwdOwnership, $postType);


                                                                                        $response = json_encode($increaseKwdOwnerEarning);

                                                                                        if (noError($increaseKwdOwnerEarning)) {

                                                                                            $xml_atrr = '';
                                                                                            $xml_atrr["response"] = $response;

                                                                                            $errMsg = "Success: Update keyword owner earning";
                                                                                            $stepCounterKwdOwnership++; // Step counter

                                                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";


                                                                                            // update cdp status field in content consumer list table
                                                                                            $deleteContentConsumerFromList = deleteContentConsumerFromList($recordId, $keywordDbConn);

                                                                                            $response = json_encode($deleteContentConsumerFromList["errMsg"]);
                                                                                            if (noError($deleteContentConsumerFromList)) {


                                                                                                $errMsg = "Success: delete consumer {$consumerEmail} from content consumenr list";
                                                                                                $stepCounterKwdOwnership++; // Step counter

                                                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";

                                                                                                $commitTransaction = commitTransaction($keywordDbConn);
                                                                                                if (noError($commitTransaction)) {

                                                                                                    $kwywordOwnershipChild["keywords"] = ($keyword);

                                                                                                    $errMsg = "Success: commit transaction";
                                                                                                    $stepCounterKwdOwnership++; // Step counter

                                                                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";

                                                                                                    $xml_data['kwywordOwnership']["data"] = '';
                                                                                                    $xml_data['kwywordOwnership']["attribute"] = $xml_atrr;

                                                                                                    $xml_data['kwywordOwnership']["data"] = '';
                                                                                                    $xml_data['kwywordOwnership']["childelement"] = $kwywordOwnershipChild;

                                                                                                    /******************** RESPONSE LOG ****************/
                                                                                                    $responseArr["errCode"] = "-1";
                                                                                                    $responseArr["errMsg"] = "CDP: Processed Successfully!";
                                                                                                    $xml_data['response']["data"] = "";
                                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                                    /******************** RESPONSE LOG ****************/


                                                                                                    /* keyword response */


                                                                                                    print("Successfully process coin distribution process for app earning, keyword earing and keywo earning where content consumer email " . $consumerEmail . "<br /><br />");

                                                                                                } else {


                                                                                                    $errMsg = "Failed: commit transaction";
                                                                                                    $stepCounterKwdOwnership++; // Step counter

                                                                                                    $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";

                                                                                                    $xml_data['kwywordOwnership']["data"] = '';
                                                                                                    $xml_data['kwywordOwnership']["attribute"] = $xml_atrr;

                                                                                                    $xml_data['kwywordOwnership']["data"] = '';
                                                                                                    $xml_data['kwywordOwnership']["childelement"] = $kwywordOwnershipChild;


                                                                                                    /******************** RESPONSE LOG ****************/
                                                                                                    $responseArr["errCode"] = 99;
                                                                                                    $responseArr["errMsg"] = "CDP: Process Failed!";
                                                                                                    $xml_data['response']["data"] = "";
                                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                                    /******************** RESPONSE LOG ****************/

                                                                                                    print('Error: commit transaction for delete user');

                                                                                                }
                                                                                            } else {


                                                                                                $errMsg = "Failed: delete consumer {$consumerEmail} from content consumenr list";
                                                                                                $stepCounterKwdOwnership++; // Step counter

                                                                                                $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";

                                                                                                $xml_data['kwywordOwnership']["data"] = '';
                                                                                                $xml_data['kwywordOwnership']["attribute"] = $xml_atrr;

                                                                                                $xml_data['kwywordOwnership']["data"] = '';
                                                                                                $xml_data['kwywordOwnership']["childelement"] = $kwywordOwnershipChild;

                                                                                                /* keyword response */
                                                                                                $keywordchild["errCode"] = $increaseKwdOwnerEarning["errCode"];
                                                                                                $keywordchild["errMsg"] = $increaseKwdOwnerEarning["errMsg"];


                                                                                                /* keyword response */

                                                                                                print($deleteContentConsumerFromList["errMsg"]);

                                                                                            }
                                                                                        } else {


                                                                                            $errMsg = "Failed: Update keyword owner earning";
                                                                                            $stepCounterKwdOwnership++; // Step counter

                                                                                            $kwywordOwnershipChild["step{$stepCounterKwdOwnership}"] = "{$stepCounterKwdOwnership}. {$errMsg}";

                                                                                            $xml_data['kwywordOwnership']["data"] = '';
                                                                                            $xml_data['kwywordOwnership']["attribute"] = $xml_atrr;

                                                                                            $xml_data['kwywordOwnership']["data"] = '';
                                                                                            $xml_data['kwywordOwnership']["childelement"] = $kwywordOwnershipChild;

                                                                                            print($increaseKwdOwnerEarning["errMsg"]);

                                                                                        }
                                                                                    } else {
                                                                                        print('Error: create keyword database connection');

                                                                                    }
                                                                                } else {


                                                                                    $errMsg = "Failed : update cdp status for {$keywoUser}: cdp_status = 1";
                                                                                    $stepCounterkeywoUser++; // Step counter

                                                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";
                                                                                    $xml_data['keywoUser ']["data"] = '';
                                                                                    $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                                                    print('Error: Updating keywo coin distribution status');

                                                                                }
                                                                            } else {


                                                                                $errMsg = "Failed : commit transaction";
                                                                                $stepCounterkeywoUser++; // Step counter

                                                                                $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                                $xml_data['keywoUser ']["data"] = '';
                                                                                $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                                                print('Error: Unable to commit transaction into search history');

                                                                            }
                                                                        } else {


                                                                            $errMsg = "Failed : update cdp step for insert search history of {$keywoUser}";
                                                                            $stepCounterkeywoUser++; // Step counter

                                                                            $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                            $xml_data['keywoUser ']["data"] = '';
                                                                            $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                                            print('Error: unable to update transaction history json in consumer lsit');;

                                                                        }
                                                                    } else {
                                                                        print('Error: Unable to create keyword database');

                                                                    }
                                                                } else {


                                                                    $errMsg = "Failed : Insert search history for {$keywoUser}";
                                                                    $stepCounterkeywoUser++; // Step counter

                                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                                    $xml_data['keywoUser ']["data"] = '';
                                                                    $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                                    mysqli_close($searchDbConn);

                                                                    $keywordDbConn = createDBConnection("dbkeywords");
                                                                    if (noError(($keywordDbConn))) {
                                                                        $keywordDbConn = $keywordDbConn["connection"];

                                                                        $rollBackSearchHistory = rollbackTransaction($keywordDbConn);

                                                                        if (!noError($rollBackSearchHistory)) {
                                                                            print('Error: Unable to rollback transaction from search history');
                                                                        }

                                                                        print('Error : Inserting transaction details into search history');

                                                                    } else {
                                                                        print('Error: Unable to connect keyword database');

                                                                    }

                                                                }
                                                            } else {
                                                                print('Error: Unable to create search dataabse connection');

                                                            }
                                                        } else {
                                                            print('Error: Unable to start transaction for insert history into table');

                                                        }
                                                    } else {


                                                        $errMsg = "Failed : Commit transaction";
                                                        $stepCounterkeywoUser++; // Step counter

                                                        $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                        $xml_data['keywoUser ']["data"] = '';
                                                        $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                        print('Error: Unable to commit transaction for keywo earning transaction');

                                                    }
                                                } else {


                                                    $errMsg = "Failed : update cdp step for insert user transaction: " . $keywoEarningTrans;
                                                    $stepCounterkeywoUser++; // Step counter

                                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                    $xml_data['keywoUser ']["data"] = '';
                                                    $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                    print('Error: Unable to udpate keywo transaction entry into step done filed');

                                                }
                                            } else {


                                                $errMsg = "Failed : Insert user transaction for {$keywoUser}";
                                                $stepCounterkeywoUser++; // Step counter
                                                $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                                $xml_data['keywoUser ']["data"] = '';
                                                $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                                // rollback keywo earning transaction
                                                $rollBackKeywoTransaction = rollbackTransaction($kwdDbConn);
                                                if (!noError($rollBackKeywoTransaction)) {
                                                    print('Error: Unable to rollback keywo earning transaction');
                                                }

                                                print('Error: Unable to insert keywo earning transaction');

                                            }
                                        } else {
                                            print('Error: Unabel to start transaction for keywo earning transaction');

                                        }

                                    } else {


                                        $errMsg = "Failed : get {$keywoUser} balance details";
                                        $stepCounterkeywoUser++; // Step counter
                                        $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                        $xml_data['keywoUser ']["data"] = '';
                                        $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                        print('Error : Unable to get keywo balance');

                                    }
                                } else {

                                    $errMsg = "Failed: commit transaction ";
                                    $stepCounterkeywoUser++; // Step counter
                                    $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                    $xml_data['keywoUser ']["data"] = '';
                                    $xml_data['keywoUser']["childelement"] = $keywoUserChild;

                                    // commit transaction error for keywo user balance update in cdp_steps_done field
                                    print('Error: Unable to commit transaction');

                                }
                            } else {


                                $errMsg = "Failed: Unable to update cdp_step_for: " . $creditStepDone;
                                $stepCounterkeywoUser++; // Step counter
                                $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                                $xml_data['keywoUser ']["data"] = '';
                                $xml_data['keywoUser']["childelement"] = $keywoUserChild;
                            }

                        } else {

                            $errMsg = "Failed : Credit current payout amount to  {$keywoUser}.";
                            $stepCounterkeywoUser++; // Step counter
                            $keywoUserChild["step{$stepCounterkeywoUser}"] = "{$stepCounterkeywoUser}. {$errMsg}";

                            $xml_data['keywoUser ']["data"] = '';
                            $xml_data['keywoUser']["childelement"] = $keywoUserChild;
                            // rollback keywo user balance udpate
                            $rollBackKeywoCreditFailure = rollbackTransaction($kwdDbConn);
                            if (!noError($rollBackKeywoCreditFailure)) {
                                print('Error: Unable to roll back keywo credit earning failure');
                            }


                            printArr($creditPayoutToKeywoUser["errMsg"]);

                        }
                    } else {
                        print('Error: Unable to start transaction for keywo earning');

                    }
                }  // end of cdpStatus = 0;


                // create or update xml log Files
                $xmlProcessor->writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);
                unset($xml_data);
                $stepCounter = 0;
                $stepCounterkeywoUser = 0;
                $stepCounterAppDev = 0;
                $stepCounterContentCreator = 0;
                $stepCounterKwdOwnership = 0;
            } // end of foreach loop
        } else {
            print('No records found to process coin distribution process');

        }
    } else {

        print($getConsumerDetails["errMsg"]);

    }
} else {

    print('Error: Unable to create database connection');

}

?>
