<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath=explode("controllers/", dirname(__FILE__))[0];


// Include required helpers files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/walletHelper.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}models/keywords/acceptBidModel.php");
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

error_reporting(0);

//for xml writing essential
$xmlProcessor = new xmlProcessor();

// inititalize xml logs activity
$xmlArray = initializeXMLLog('');

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xmlfilename = "reject_bid_cron.xml";

$xml_data['step']["data"] = 'Reject Bids';

$logStorePath = $docrootpath."logs/keyword/bid/";

$i = 0;
$j = 0;

// create database connection
$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Success: Create keyword database connection';

    // get reject bid details in benchmarking
    $getRejectBids = getRejectBidDetails($kwdDbConn);

    if(noError($getRejectBids)){
        $getRejectBids = $getRejectBids["errMsg"];

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Success: Getting reject bid details from reject_bid_queue';

        if(count($getRejectBids) > 0) {

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Success: Bid record found to reject.';

            foreach($getRejectBids as $key => $bidReject){
                $bidQueueId         =   $bidReject["id"];
                $bidderEmail 		= 	$bidReject["email"];
                $bidKeyword 		=	$bidReject["keyword"];
                $bidderUserId 		= 	$bidReject["user_id"];
                $bidderAmount 		= 	$bidReject["bid_amount"];
                $tradingAmt         =   $bidReject["trade_fees"];
                $renewalAmt         =   $bidReject["renewal_amount"];
                $returnAmt          =   $bidderAmount + $tradingAmt + $renewalAmt;
                $returnAmt          =   number_format($returnAmt, 8);

                //$responseInJson = json_encode($bidReject);

                $j = $j + 1;
                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Reject Bid Data : 1] Bidder Email : '.$bidderEmail.' 2] Bid Keyword : '.$bidKeyword.' 3] Bid Amount : '.$bidderAmount.' 4] Trading Amount : '.$tradingAmt.' 5] Renewal Amount : '.$renewalAmt.' 6] Bid Cashback : '.$returnAmt;

                // 1. deduct block for bid amount
                $deductBidAmount = creditUserEarning($bidderUserId, $returnAmt, "blockedbids", "deduct");

                $responseDeduictBidAmt = json_encode($deductBidAmount);

                if(noError($deductBidAmount)){

                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Deduct bid blocked amount. | Response : '.$responseDeduictBidAmt;

                    $deductBidAmount = $deductBidAmount["errMsg"];
                    $firstName = $deductBidAmount["first_name"];
                    $lastName = $deductBidAmount["last_name"];

                    // 2. send notification to bid rejected user.
                    // 3. send an email to  bid reject user.

                    $to      = $bidderEmail;
                    $subject = "Keywo: Bid rejected";
                    $message = '<p>Hi, <br/> <br/> You bid amount '.$bidderAmount.'is rejected on '. $bidKeyword.'</p>';

                    $notification_message = 'You bid amount '.$bidderAmount.' '.$keywoDefaultCurrencyName.' is rejected on '. $bidKeyword;
                    $method_name = "GET";

                    $getNotification = notifyoptions($bidderUserId ,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                    if(noError($getNotification)){
                        $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];

                        $path    = "";

                        $category = "bid";
                        $linkStatus = 1;

                        $i = $i + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Success: getting user preference permission code. | 1] Notification Preference : '.$permissionCode;

                        $permCode = array();
                        $permCode["category"] = $category;
                        $permCode["permissionCode"] = $permissionCode;

                        $xml_data["step".$i.".".$j]["childelement"] = $permCode;


                        $notiSend = sendNotificationBuyPrefrence($to,$subject,$message,$firstName, $lastName,$bidderUserId,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                        if(noError($notiSend)){

                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Success: Send email and notification to bid reject user. Reject bid: '.$bidDataforLog;

                            // delete bid reject record after success
                            $bidRecordDelete = deleteRejectRecord($bidQueueId, $kwdDbConn);
                            if(noError($bidRecordDelete)){
                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Success: '.$bidRecordDelete["errMsg"];

                                print('Bid rejected successfully<br /><br />');

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]="-1";
                                $responseArr["errMsg"]="success";
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }else{
                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Failed: '.$bidRecordDelete["errMsg"];

                                print($bidRecordDelete["errMsg"]);

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]="5";
                                $responseArr["errMsg"]="failed";
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/
                            }

                        }else{

                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Failed: Send email and notification to bid reject user. Reject bid: '.$bidDataforLog;

                            print('Error: Unable to send reject bid email<br /><br />');
                        }
                    }else{
                        $j = $j + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.'.'.$j.'.Failed: getting user preference permission code';

                        print('Error: Getting user perference permission code<br /><br />');
                    }
                }else{
                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Error: Deduct bid blocked amount. | Response : '.$responseDeduictBidAmt;


                    print('Error: Unable to deduct blocked amount<br /><br />');
                }
            }
        }else{
            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Failed: No Bid record found to reject.';

            print('No bid found to reject');
        }
    }else{
        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Failed: Getting reject bid details from reject_bid_queue';

        print('Error: Unable to fetch reject bid details queue');
    }

}else{
    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Failed: Create keyword database connection';

    print('Error: Database connection');
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);






function getRejectBidDetails($conn){
    $returnArr = array();

    $query = "Select * from reject_bid_queue limit 50";

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        $res = array();

        while ($row = mysqli_fetch_assoc($execQuery["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]=$res;

    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $execQuery["errMsg"];
    }


    return $returnArr;
}

function deleteRejectRecord($bidQueueId, $conn){
    $returnArr = array();

    $query = "Delete from reject_bid_queue where id=".$bidQueueId;

    $execQuery = runQuery($query, $conn);
    if(noError($execQuery)){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Successfully delete reject record ".$bidQueueId." from bid queue.";
    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $execQuery["errMsg"];
    }

    return $returnArr;
}

?>