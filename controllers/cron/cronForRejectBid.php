<?php
	
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');

$docrootpath=explode("controllers/", dirname(__FILE__))[0];


	// Include required helpers files
	require_once("{$docrootpath}config/config.php");
	require_once("{$docrootpath}helpers/coreFunctions.php");
	require_once("{$docrootpath}helpers/arrayHelper.php");
	require_once("{$docrootpath}helpers/walletHelper.php");
	require_once("{$docrootpath}helpers/deviceHelper.php");
	require_once("{$docrootpath}models/keywords/acceptBidModel.php");
	require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

    error_reporting(0);

	//for xml writing essential
	$xmlProcessor = new xmlProcessor();

	 // inititalize xml logs activity
    $xmlArray = initializeXMLLog('');

    $xml_data['request']["data"] = '';
    $xml_data['request']["attribute"] = $xmlArray["request"];

	$xmlfilename = "reject_bid_cron.xml";

	$xml_data['step']["data"] = 'Reject Bids';

$logStorePath = $docrootpath."logs/keyword/bid/";

	$i = 0;

	//	1. Read reject bid queue json file. 	
	$rejectJsonDir = $docRoot."json_directory/keywords/reject_bids/";

	if(is_dir($rejectJsonDir)){
		$readRejectJson = $rejectJsonDir."reject_bid_queue.json";
		
		$i = $i + 1;
		$xml_data["step".$i]["data"] = $i.'.Success: Reject bid xml log directory exist. Log Path: '.$rejectJsonDir;


		if(file_exists($readRejectJson)){

			$i = $i + 1;
			$xml_data["step".$i]["data"] = $i.'.Success: Reject bid queue json file exist. Json Path: '.$readRejectJson;

			$fp = fopen($readRejectJson, "r+");
			$fpRead = fread($fp, filesize($readRejectJson));

			$fpRead = json_decode($fpRead, true);
			fclose($fp);

			$fp1 = fopen($readRejectJson, "w");
			fclose($fp1);


			$j = 1;
			foreach($fpRead as $key => $bidReject){

				$i = $i + 1;
				$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'. Process bid reject for 1] Bid Id: '.$bidReject;

				$bidDataforLog = $bidReject;

				$bidReject = explode("~~", $bidReject);

				$bidderEmail 		= 	$bidReject[1];
				$bidderAmount 		= 	$bidReject[3];
				$tradingAmt         =   $bidReject[4];
				$returnAmt          =   $bidderAmount + $tradingAmt;
                $returnAmt          =   number_format($returnAmt, 8);
				$bidKeyword 		=	$bidReject[6];
				$bidderUserId 		= 	$bidReject[7];

				$bid_step = array();
				$bid_step["email"] = $bidderEmail;
				$bid_step["amount"] = $bidderAmount;
				$bid_step["trading_amount"] = $tradingAmt;
				$bid_step["keyword"] = $bidKeyword;
				$bid_step["userid"] = $bidderUserId;

				$xml_data["bid_step".$i]["childelement"] = $bid_step;

				//$bidderAmount = number_format($bidderAmount, 2);

				// 1. deduct block for bid amount
				$deductBidAmount = creditUserEarning($bidderUserId, $returnAmt, "blockedbids", "deduct");

				if(noError($deductBidAmount)){

					$deductBidAmount = $deductBidAmount["errMsg"];
					$firstName = $deductBidAmount["first_name"];
					$lastName = $deductBidAmount["last_name"];

					$i = $i + 1;
					$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Success: Deduct blocked bid amount';

					$xml_data["bid_step".$i]["childelement"] = $deductBidAmount;

					// 2. send notification to bid rejected user.
					// 3. send an email to  bid reject user.

                    $to      = $bidderEmail;
                    $subject = "Keywo: Bid rejected";
                    $message = '<p>Hi, <br/> <br/> You bid amount '.$bidderAmount.'is rejected on '. $bidKeyword.'</p>';

					$notification_message = 'You bid amount '.$bidderAmount.' '.$keywoDefaultCurrencyName.' is rejected on '. $bidKeyword;
					$method_name = "GET";

					$getNotification = notifyoptions($bidderUserId ,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
					if(noError($getNotification)){
						$permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
						
						$path    = "";

						$category = "bid";
						$linkStatus = 1;

						$i = $i + 1;
						$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Success: getting user preference permission code';

						$permCode = array();
						$permCode["category"] = $category;
						$permCode["permissionCode"] = $permissionCode;

						$xml_data["bid_step".$i]["childelement"] = $permCode;


						$notiSend = sendNotificationBuyPrefrence($to,$subject,$message,$firstName, $lastName,$bidderUserId,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);
						if(noError($notiSend)){

							$i = $i + 1;
							$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Success: Send email and notification to bid reject user. Reject bid: '.$bidDataforLog;

							// unset value
							$fpRead[$key] = '';

							print('Bid rejected successfully<br /><br />');
						}else{

							$i = $i + 1;
							$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Failed: Send email and notification to bid reject user. Reject bid: '.$bidDataforLog;

							print('Error: Unable to send reject bid email<br /><br />');
						}
					}else{
						$i = $i + 1;
						$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Failed: getting user preference permission code';

						print('Error: Getting user perference permission code<br /><br />');
					}
				}else{
					$i = $i + 1;
					$xml_data["bid_step".$i]["data"] = $i.'.'.$j.'.Failed: Deduct blocked bid amount';

					print('Error: Unable to deduct blocked amount<br /><br />');
				}

				$j++;
			} //end of foreach loop

		} else{

			$i = $i + 1;
			$xml_data["step".$i]["data"] = $i.'.Failed: Reject bid queue json file not found. Json Path: '.$readRejectJson;

			print('Error: '.$readRejectJson." file not found<br /><br />");

		}
	}else{
		$i = $i + 1;
		$xml_data["step".$i]["data"] = $i.'.Failed: Reject bid xml log directory not exist. Log Path: '.$rejectJsonDir;

		print('Error: '.$rejectJsonDir." directory not found<br /><br />");

	}

$fp2 = fopen($readRejectJson, "r+");
$readNewBid = fread($fp2, filesize($readRejectJson));
$readNewBid = json_decode($readNewBid, true);

// if(count($readNewBid) > 0){
// 	foreach($readNewBid as $key => $addNewBid){
// 		$fpRead[] = $addNewBid;
// 	}
// }

fclose($fp2);

$fpRead = array_values(array_filter($fpRead));

$remainingBids = json_encode($fpRead);

$fp3 = fopen($readRejectJson, "w");
fwrite($fp3, $remainingBids);
fclose($fp3);

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

?>