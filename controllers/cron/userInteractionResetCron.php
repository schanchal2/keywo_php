<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 20/5/17
 * Time: 2:35 PM
 */

$docrootpath=explode("controllers/",dirname(__FILE__))[0];

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once "{$docrootpath}models/wallet/walletKYCModel.php";
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");
//require_once "{$docrootpath}";

error_reporting(0);

$server_tz = date_default_timezone_get();
$serverDate = date("H:i:s");

date_default_timezone_set('Asia/Kolkata');

$script_tz = date_default_timezone_get();
$localDate = date("H:i:s");
//echo $localDate . "<br><br>";

date_default_timezone_set("GMT");
$date = date("H:i");
//echo "GMT Time Now : ".$date . "<br><br>";

date_default_timezone_set($server_tz);

// Calculate time left for next reset
$fixTime = new DateTime('24:00');
$currTime = new DateTime($date);
$interval = $fixTime->diff($currTime);
//echo $interval->format("%H:%i");


$returnArr = array();
$stepCounter = 0;

$logPath = $logPath["cron"];
$logFileNme = "interactionResetCron.xml";

// Initialize XML log
$xmlProcessor = new xmlProcessor();
$xmlArray = initializeXMLLog('');

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]= $xmlArray["request"];

    $msg = "Start user interaction reset process at GMT : {$date}, local time : {$localDate}, Server time : $serverDate ."; // Message for log
    $stepCounter++; // Step counter
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}"; // Set message for step

    // Create Database Connection
    $conn = createDBConnection("dbsearch");
    if(noError($conn)) {
        $msg = "Success : Created database connection";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

        $conn = $conn["connection"];

        $getKycLevels = getKYCSlabByLevel($conn);
        if(noError($getKycLevels))
        {
            $msg = "Success : Getting KYC slab levels";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $getKycLevels = $getKycLevels['errMsg'];
            $KycLevelsParam  = array();

            for($i = 0; $i < count($getKycLevels)-1; $i++)
            {
                $KycLevelsParam[$i]['kyc_level'] = $getKycLevels[$i]['kyc_level_name'];
                $KycLevelsParam[$i]['interaction'] = $getKycLevels[$i]['intraction'];
            }

            $resetInteration = resetUserInteraction("{$NotificationURL}v2/", $KycLevelsParam);
            //printArr($resetInteration);
            if(noError($resetInteration))
            {
                $msg = "Success : Reset all user interaction as per KYC levels";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
            }else{
                $msg = "Error : Reset all user interaction as per KYC levels";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
                $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
            }

        }else{
            $msg = "Error : Getting KYC slab levels";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
            $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
        }
    }else{
        $msg = "Error : Creating database connection";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$msg}";
        $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
    }

    // create or update xml log Files
    $xmlProcessor->writeXML($logFileNme, $logPath, $xml_data, $xmlArray["activity"]);
    echo $msg;


function resetUserInteraction($requestUrl, $levelsParam)
{

    global  $walletPublicKey, $mode;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText      = "kyc_1={$levelsParam[0]['interaction']}&kyc_2={$levelsParam[1]['interaction']}&kyc_3={$levelsParam[2]['interaction']}&publicKey={$walletPublicKey}";
    $postFields   = "kyc_1=" . $levelsParam[0]['interaction'] . "&kyc_2=" . $levelsParam[1]['interaction'] . "&kyc_3=" . $levelsParam[2]['interaction'] . "&publicKey=" . urlencode($walletPublicKey);
    // $requestUrl    = strtolower($requestUrl);
    $apiName      = 'user/interactionpending';
    $curl_type    = 'PUT';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
    //print_r($respArray);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

?>