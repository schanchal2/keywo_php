<?php

/* This cron is used for unblock keywords which is blocked for 15 minutes during payment process */

session_start();

$docrootpath=explode("controllers/",dirname(__FILE__))[0];


require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/walletHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/transactionHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

error_reporting(1);

$retArray = array();
$returnArr = array();
$extraArgs = '';
$stepCounter = 0;
$kwdPurchaseLog = $docrootpath."logs/keyword/purchase/";

$xmlFilename = "clearCart.xml";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$errMsg = "Start keyword purchase process by wallet.";
$stepCounter++;
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


$kwdDbConn = createDBConnection('dbkeywords');

if (noError($kwdDbConn)) {
    $kwdDbConn = $kwdDbConn["connection"];

    $errMsg = "keyword database connection success.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $trans = startTransaction($kwdDbConn);
    if(noError($trans)){

        $errMsg = "transaction start success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $result = getBlockKeywordsDuringPayment($kwdDbConn);
        if(noError($result)){

            $users = $result["errMsg"];
            //printArr($users);
            foreach($users as $value){

                $email = $value["user_email"];
                $userDetails = getUserCartDetails($email,$kwdDbConn);
                if(noError($userDetails))
                {
                    $errMsg = "get user cart details : success";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $cart_end_time = $userDetails["errMsg"]["cart_end_time"];
                    $currDate = date('Y-m-d H:i:s');
                    $currDateInStr = strtotime($currDate);
                    $cartEndTimeInStr = strtotime($cart_end_time);
                    if(!empty($cart_end_time) && isset($cart_end_time)){

                        if($cartEndTimeInStr < $currDateInStr){
                            $result = unblockKeywordsDuringPayment($email,$kwdDbConn);
                            if(noError($result)){
                                $errMsg = "Unblock keywords from cart : success";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }else{
                                $errMsg = "Unblock keywords from cart : failed";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            }
                        }

                    }
                }else{
                    $errMsg = "get user cart details : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                }

            }

            $trans = commitTransaction($kwdDbConn);
            if(noError($trans))
            {
                $errMsg = "Transaction committed : success";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"]  = "Transaction committed : success";
            }else{
                $errMsg = "Transaction committed : success";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = "Transaction committed : failed";
            }

        }else{
            rollbackTransaction($kwdDbConn);
            $errMsg = "Cron Failure :Failed to get block keywords.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = "Cron Failure :Failed to get block keywords.";
        }

    }else{

        $errMsg = "Cron Failure :Unable to start Transaction.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = "Cron Failure :Unable to start Transaction.";

    }

}else{

    $errMsg = "keyword database connection failed.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $returnArr["errCode"] = 2;
    $returnArr["errMsg"]  = "Error establishing database connection";
}

$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

printArr($returnArr);

?>