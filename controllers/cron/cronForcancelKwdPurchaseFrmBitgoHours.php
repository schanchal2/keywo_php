<?php
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath=explode("controllers/",dirname(__FILE__))[0];

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/acceptBidModel.php");
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

//for xml writing essential
$xmlProcessor = new xmlProcessor();

// inititalize xml logs activity
$xmlArray = initializeXMLLog('');

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xmlfilename = "purchase_cancel.xml";

$xml_data['step']["data"] = 'Purchase Cancel';

//$logStorePath = $docrootpath."logs/keyword/bitgoCancelTrans/";
$logStorePath = $docrootpath."logs/keyword/bitgoCancelTrans/";

$i = 0;
$j = 0;
$recordNo = 1;

// getting current date, month and year
$year       = date("Y");
$month      = date("m");
$date       = date("d");

$retArray = array();

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){

    $kwdDbConn = $kwdDbConn["connection"];

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Success: Create keyword database connection';

    $transStats = getTransactionStatus($kwdDbConn,0);

    if(noError($transStats)){
        $transStats = $transStats['errMsg'];

        // stores payment details in json encode response.
        $responseTransStats = json_encode(isset($transStats['errMsg'])?$transStats['errMsg']:"");

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Success: Getting keyord purchase transaction cancel details | response : '.$responseTransStats;


        foreach($transStats as $key => $record){

            $i = $i + 1;
            $j = $j + 1;
            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.". Process {$recordNo} record ".date("Y-m-d H:i:s");

            $orderId = $record['order_id'];
            $recieved_amount = $record["amount_recieved"];
            /* $recieved_amount = $transStats["paid_amount"]; */
            $amount_due = $record['amount_due'];
            $keywords = $record['keyword_cart'];
            $transId = $record['payment_transaction_id'];
            $email = $record['username'];
            $dbTime = strtotime($record['payment_time']);
            $time = time() - $dbTime;
            if(trim($keywords) == "itd_purchase_only"){
                continue;
            }
            $keywordsArray      = explode(',', $keywords);
            $keywordsWithTags = "";

            foreach($keywordsArray as $value){
                if(!empty($value)){
                    $keywordsWithTags.= "#".$value.", ";
                }
            }

            if($time >= 7200){

                $j = $j + 1;
                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.".Record order time {$record['order_time']} above 60 minutes :".date("Y-m-d H:i:s");

               $query = "UPDATE payments SET status = 'cancelled' WHERE order_id = '".$orderId."' AND payment_transaction_id = '".$transId."' ";
               $result = runQuery($query, $kwdDbConn);

                $j = $j + 1;
                $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Cancelling order id {$record['order_id']} :".date("Y-m-d H:i:s");

                if(noError($result)){

                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Transaction cancelled success:".date("Y-m-d H:i:s");

                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Order ID: {$orderId} Transaction ID : {$transId}";

                    $to      = $email;
                    if(trim($keywords) == "itd_purchase_only"){
                        $subject = "Keywo: ".$keywoDefaultCurrencyName." purchase by bitgo - Transaction Cancelled";
                        $message =  '<p>Hi, <br/> <br/>Your transaction has been cancelled<br><br>Total '.$keywoDefaultCurrencyName.' price: ' . number_format($amount_due, 8) . ' BTC<br><br>Paid price: ' . number_format($recieved_amount, 8) . ' BTC<br><br></p>';
                    }else{
                        $subject = "Keywo: Keyword purchase by bitgo - Transaction Cancelled";
                        $message =  '<p>Hi, <br/> <br/>Your transaction has been cancelled<br><br>Keyword(s): '.$keywordsWithTags.'<br><br>Keyword price: ' . number_format($amount_due, 8) . ' BTC<br><br>Paid price: ' . number_format($recieved_amount, 8) . ' BTC<br><br></p>';
                    }

                    // access $userRequiredFields from config file.
                    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
                    if(noError($getUserDetails)){

                        $j = $j + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Getting user details success: ".date("Y-m-d H:i:s");


                        $getUserDetails = $getUserDetails["errMsg"];
                        $user_id = $getUserDetails["_id"];
                        $firstName = $getUserDetails["first_name"];
                        $lastName = $getUserDetails["last_name"];

                        $method_name = "GET";
                        $getNotification = notifyoptions($user_id,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                        if(noError($getNotification)){

                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Getting notification permission code success ".date("Y-m-d H:i:s");

                            $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["buy_opt_container"]["2"]["permissions"]["_id"];

                            $linkStatus = "";
                            $category = "buy";
                            if(trim($keywords) == "itd_purchase_only"){
                                $noti_message = "Your ".$keywoDefaultCurrencyName." purchase transaction process has been cancelled";
                            }else{
                                $noti_message = "Your keyword purchase transaction process has been cancelled";
                            }
                            
                            $sendCancelEamilNotification = sendNotificationBuyPrefrence($to,$subject,$message,$firstName,$lastName,$user_id,$smsText,$mobileNumber,$noti_message,$permissionCode,$category,$linkStatus);
                            if(noError($sendCancelEamilNotification)){

                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j."  Mail send successfully for order cancel ".date("Y-m-d H:i:s");

                                $retArray["errCode"] = -1;
                                $retArray["errMsg"] = "Mail send successfully";
                            }else{

                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Error in Mail send  ".date("Y-m-d H:i:s");

                                $retArray["errCode"] = 8;
                                $retArray["errMsg"] = "Error: Mail send failed";
                            }
                        }else{


                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Getting notification permission code failed ".date("Y-m-d H:i:s");

                            $retArray["errCode"] = 7;
                            $retArray["errMsg"] = "Error: Unable to get notification permission code";
                        }

                    }else{

                        $j = $j + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Getting user details failed: ".date("Y-m-d H:i:s");

                        $retArray["errCode"] = 6;
                        $retArray["errMsg"] = "Error: Fetching user details";
                    }

                }else{

                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Error in cancelling order of Order ID: {$orderId} on :".date("Y-m-d H:i:s");

                    $retArray['errCode'] = 5;
                    $retArray['errMsg'] = "Error in update transaction status";
                }

            }else{

                $j = $j + 1;
                $xml_data["step".$i.".".$j]["data"] = $i.".".$j." Record order time {$record['order_time']} less than 60 minutes : ".date("Y-m-d H:i:s");

                $retArray['errCode'] = 3;
                $retArray['errMsg'] = "No records updated";
            }

            $recordNo++;
            $j = 0;
        }
    }else{
        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Failed: Getting keyord purchase transaction cancel details | response : '.$responseTransStats;

        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "Error : {$transStats['errMsg']}";
    }
}else{
    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Failed: Create keyword database connection';

    $retArray["errCode"] = 1;
    $retArray["errMsg"] = "Error : {$kwdDbConn['errMsg']}";
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

//printArr($retArray);

function getTransactionStatus($conn,$confirmation)
{
   // printArr(func_get_args()); die;
    $retArray = array();
    $records = array();

    if(!empty($conn))
    {
        $query = "";
        if($confirmation > -1){
            $query = "SELECT * FROM payments WHERE no_of_confirmation = {$confirmation} AND (status = 'mispaid' OR status = 'overpaid') AND payment_type = 'bitgo' AND order_time > '2016-11-26 17:00:00' order by payment_time DESC ";  /* SELECT * from payments where */

        } else if(!empty($confirmation) && $confirmation == -1){
            $query = "SELECT * FROM payments WHERE no_of_confirmation = {$confirmation} AND status = 'unpaid' AND payment_type = 'bitgo' AND order_time > '2016-11-26 17:00:00' order by order_time DESC ";  /* OR status = 'overpaid') */
        }



        $result  = runQuery($query, $conn);
        if (noError($result)) {
            while($row = mysqli_fetch_assoc($result['dbResource'])){
                $records[] = $row;
            }

            if(count($records) > 0){
                $retArray['errCode'] = -1;
                $retArray['errMsg'] = $records;
            }else{
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = "No records found";
            }
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $result['errMsg'];
        }
    }
    else{
        $retArray['errCode'] = 5;
        $retArray['errMsg'] = "Error connection is empty";
    }

    return $retArray;
}


?>