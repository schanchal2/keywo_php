<?php
ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

$docrootpath = explode("controllers/",  dirname(__FILE__))[0]; //echo "Testing".$docrootpath; die;

//$docrootpath = __DIR__;
//$docrootpath = explode('/views', $docrootpath);
//$docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordAnalyticsModel.php");


    $conn = createDBConnection("dbkeywords");
        if(noError($conn))
        {
            $conn = $conn["connection"];
        }
        else{
            print_r("Database Error");
        }

/* Update Analytics Table after 24hours */

            $time="-2 day";

            $yesterdayDate =date("Y-m-d", strtotime("{$time}"));
            $date = date("d", strtotime("{$time}"));
            $month=lcfirst(date("M", strtotime("{$time}")));
            $explodeDate=explode("-",$yesterdayDate);
            $year=$explodeDate[0];
            $monthno=$explodeDate[1];
            $date=$explodeDate[2];

            $tablename="daily_keyword_earnings_{$monthno}_{$year}";


            $distinctKeyword=getDistinctKeywordUsed($conn, $tablename, $yesterdayDate)["errMsg"]; //echo "<pre>"; print_r($distinctKeyword); echo "</pre>"; die;

            foreach($distinctKeyword as $keyword) {

             $dailyUpdateEarning = getDailyKeywordEarningAnalytics($yesterdayDate, "search", $keyword, $conn);
             if (noError($dailyUpdateEarning)) {
                 $dailyUpdateEarning = $dailyUpdateEarning["errMsg"][0];
                 $searchEarning = $dailyUpdateEarning["total_interaction_earning"];
             } else {
                 printArr($dailyUpdateEarning);
             }

             $dailyUpdateEarning = getDailyKeywordEarningAnalytics($yesterdayDate, "social", $keyword, $conn);
                printArr($dailyUpdateEarning);
             if (noError($dailyUpdateEarning)) {
                 $dailyUpdateEarning = $dailyUpdateEarning["errMsg"][0];
                 $socialEarning = $dailyUpdateEarning["total_interaction_earning"];
             } else {
                 printArr($dailyUpdateEarning);
             }

             $kwdAnaliticsDetails = getkeywordAnaliticsDetails($conn, $keyword);


             if (noError($kwdAnaliticsDetails)) {
                 $kwdAnalyticsTableName = $kwdAnaliticsDetails["table_name"];
                 $kwdAnaliticsDetails = $kwdAnaliticsDetails["data"][0];
                 $kwdAnaliticsColumnName = $month;
                 $kwdAnaliticsColumn = $kwdAnaliticsDetails[$month];

                 $search_interaction = json_decode($kwdAnaliticsColumn, true);
                 $search_year = $search_interaction["year"];
                 $search_analytics = $search_interaction["analytics"];
                 $metaDetails = $search_interaction["analytics"][$date];
                 $searchInteraction = $searchEarning;
                 $socialInteraction = $socialEarning;
                 $metaDetails["search"]["search_Earning"] = $searchInteraction;
                 $metaDetails["social"]["social_Earning"] = $socialInteraction;
                 $search_interaction["analytics"][$date] = $metaDetails;
                 $appKwdSearchCountJson = json_encode($search_interaction);

                 $query = "UPDATE " . $kwdAnalyticsTableName . " SET $kwdAnaliticsColumnName ='" . $appKwdSearchCountJson . "' WHERE keyword='" . $keyword . "'";

                 $result = runQuery($query, $conn);

                 if (noError($result)) {

                     $returnArr["errCode"] = -1;
                     $returnArr["errMsg"] = "Earning Transaction Detail successfully committed.";

                 } else {
                     $returnArr["errCode"] = 5;
                     $returnArr["errMsg"] = "Transaction Failure";
                 }


             }
         }

