<?php
  ini_set('default_charset', 'utf-8');
  header('Content-type: text/html; charset=utf-8');

$docRootnew=explode("controllers/",dirname(__FILE__))[0];
$docrootpath=$docRootnew;

  // Include require files
  require_once("{$docrootpath}config/config.php");
  require_once("{$docrootpath}config/db_config.php");
  require_once("{$docrootpath}helpers/errorMap.php");
  require_once("{$docrootpath}helpers/coreFunctions.php");
  require_once("{$docrootpath}helpers/deviceHelper.php");
  require_once("{$docrootpath}helpers/arrayHelper.php");
  require_once("{$docrootpath}helpers/stringHelper.php");
  require_once("{$docrootpath}helpers/walletHelper.php");
  require_once("{$docrootpath}helpers/transactionHelper.php");
  require_once("{$docrootpath}models/cdp/cdpUtilities.php");
  require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
  require_once("{$docrootpath}models/keywords/userCartModel.php");
  require_once("{$docrootpath}models/search/searchResultModel.php");
  require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

  $kwdDbConn = createDBConnection("dbkeywords");
  if (noError($kwdDbConn)) {
      $kwdDbConn = $kwdDbConn["connection"];

      $checkKwdArr = array();

      $getConsumerDetails = getContentConsumerDetail($kwdDbConn);
      if(noError($getConsumerDetails)){
          $getConsumerDetails = $getConsumerDetails["data"];

          foreach($getConsumerDetails as $key => $consumerDetails){

              $recordId               =   $consumerDetails["id"];
              $consumerEmail          =   $consumerDetails["consumer_email"];
              $consumerUserId         =   $consumerDetails["consumer_user_id"];
              $walletTransactonId     =   $consumerDetails["wallet_transaction_id"];
              $keyword                =   $consumerDetails["keyword"];
              $currentPayout          =   $consumerDetails["current_payout"];
              $totalPayout            =   $consumerDetails["total_payout"];
              $originIp               =   $consumerDetails["origin_ip"];
              $interactionType        =   $consumerDetails["interaction_type"];
              $contentId              =   $consumerDetails["content_id"];
              $cdpStatus              =   $consumerDetails["cdp_status"];
              $exchangeRateInJson     =   $consumerDetails["exchange_rate_in_json"];
              $timestamp              =   $consumerDetails["timestamp"];
              $creatorId              =   $consumerDetails["post_creator"];
              $sharerId               =   $consumerDetails["post_sharer"];
              $modeType               =   strtolower($consumerDetails["mode"]);
              $cdpStepDone            =   $consumerDetails["cdp_steps_done"];

              // sanitize the keyword
              $keyword                =   searchKeySanitizer($keyword);

              // convert json into array
              $cdpStepDone = json_decode($cdpStepDone, true);
             printArr($cdpStepDone);
              if($cdpStatus != 1 && array_key_exists("keywo", $cdpStepDone)){

                  if(array_key_exists("keywo", $cdpStepDone)){

                      if(!array_key_exists("update_user_balance", $cdpStepDone["keywo"])){
                          if($modeType == "search"){
                              $earningAttribute = "searchearning";
                          }else if($modeType == "social"){
                              $earningAttribute = "post_view_earnings";
                          }

                          // 1. credit current payout amount to keywoUser@keywo.com
                          $creditPayoutToKeywoUser = creditUserEarning($keywoUserId, $currentPayout, $earningAttribute, "add");
                          if(noError($creditPayoutToKeywoUser)){
                              print('Successfully credit payout amount to keywo.<br />');
                          }else{
                              print('Error: Unable to credit payout amount to keywo.<br />');
                          }
                      }

                      if(!array_key_exists("insert_user_transaction", $cdpStepDone["keywo"])){
                          if($modeType == "search"){
                              $transType = "keywo_earning";
                          }else if($modeType == "social"){
                              $transType = "keywo_content_view_earning";
                          }

                          // parameters required to insert in transaction
                          $recipientEmail = $keywoUser;
                          $recipientUserId = $keywoUserId;
                          $senderEmail = $communityPoolUser;
                          $senderUserId = $communityPoolUserId;
                          $amount = $currentPayout;
                          $type = $transType;
                          $paymentMode = 'ITD';
                          $remoteIp = $originIp;
                          $exchangeJson = $exchangeRateInJson;

                          $metaDetails = array("content_id" => (string)$contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                          $insertKeywoEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $remoteIp, $exchangeJson, $metaDetails);
                          if(noError($insertKeywoEarningTrnasaction)){
                              print('Successfully insert user transaction for keywo credit transaction.<br />');
                          }else{
                              print('Error: Unable to insert user transaction for keywo credit transaction.<br />');

                          }
                      }
                  }
              }

              // earning and transaction for either app developer or post creator (content creator)
              if($cdpStatus != 2 && array_key_exists("content_creator", $cdpStepDone)){

                  if($modeType == "search"){ // if search from search engine then appdeveloper@keywo.com earning and transaction
                      if(!array_key_exists("update_user_balance", $cdpStepDone["content_creator"])){
                          // increase total app payout amount of appDeveloper@keywo.com user.
                          $increaseAppEarningToAppDeveloper = creditUserEarning($appDeveloperUserId, $currentPayout, "appincome", "add");
                          if(noError($increaseAppEarningToAppDeveloper)){
                              print('Success: Credit payout amount to app developer user.<br />');
                          }else{
                            print('Error: Unable to credit payout amount to app developer user.<br />');
                          }
                      }

                      if(!array_key_exists("insert_user_transaction", $cdpStepDone["content_creator"])){
                          // insert transaction for app income to appdeveloper @wallet server
                          // parameters required to insert in transaction
                          $recipientEmail = $appDeveloperUser;
                          $recipientUserId = $appDeveloperUserId;
                          $senderEmail = $communityPoolUser;
                          $senderUserId = $communityPoolUserId;
                          $amount = $currentPayout;
                          $type = "app_earning";
                          $paymentMode = 'ITD';
                          $originIp = $originIp;
                          $exchangeRateInJson = $exchangeRateInJson;

                          $metaDetails = array("content_id" => (string)$contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                          $insertUserAppEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                          if(noError($insertUserAppEarningTrnasaction)){
                              print('Success: Insert app develpoper transaction for app earning.<br />');
                          }else{
                              print('Error: Unabel to insert app developer transaction for app earning.<br />');
                          }
                      }

                  }else if($modeType == "social"){ // if post from social app then content_creator earning and transaction

                      if(isset($sharerId) && !empty($sharerId)){
                          // get sharer payout
                          $calculateSharerPayout = calculateSharerPayout($currentPayout, $kwdDbConn);
                          if(noError($calculateSharerPayout)){
                              $calculateSharerPayout    = $calculateSharerPayout["data"];
                              $sharerPayoutAmnt         = $calculateSharerPayout["sharerPayoutAmnt"];
                              $currentPayout            = $calculateSharerPayout["contentCreatorPayoutAmnt"];
                          }else{
                              print($calculateSharerPayout["errMsg"]);
                          }
                      }

                      // handling failed condition for content creator process
                      if(array_key_exists("content_creator", $cdpStepDone)){
                          if(!array_key_exists("update_user_balance", $cdpStepDone["content_creator"])){
                              // 2. credit total payout amount to content creator
                              $creditCreatorEarning = creditUserEarning($creatorId, $currentPayout, "post_creator_earnings", "add");
                              if(noError($creditCreatorEarning)){
                                  print('Successfully credit payout amount to content creator.<br />');
                              }else{
                                  print('Error: Unable to credit payout amount to content creator.<br />');
                              }
                          }

                          // insert content creator earning's transaction
                          if(!array_key_exists("insert_user_transaction", $cdpStepDone["content_creator"])){
                              $requestedField = $userRequiredFields.',email';
                              $getUserEmail = getUserDetailFromUserId($creatorId, $walletURLIP . 'api/v3/', $requestedField);

                              if(noError($getUserEmail)){
                                 $creatorEmailId = $getUserEmail["errMsg"]["email"];
                              }

                              $recipientEmail = $creatorEmailId;
                              $recipientUserId = $creatorId;
                              $senderEmail = $communityPoolUser;
                              $senderUserId = $communityPoolUserId;
                              $amount = $currentPayout;
                              $type = "post_creator_earning";
                              $paymentMode = 'ITD';
                              $originIp = $originIp;
                              $exchangeRateInJson = $exchangeRateInJson;

                              $metaDetails = array("content_id" => (string)$contentId, "keyword" => urlencode(utf8_encode($keyword)), "discount" => 0, "commision" => 0);

                              $insertCreatorPayoutTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                              if(noError($insertCreatorPayoutTrnasaction)){
                                  print('Successfully insert content creator transaction.<br />');
                              }else{
                                  print('Error: Unable to insert content creator transaction.<br />');
                              }
                          }
                      } // end of content creator earning and transaction

                      if(isset($sharerId) && !empty($sharerId)){
                          // credit and transaction for content sharer
                          if(array_key_exists("content_sharer", $cdpStepDone)){
                              if(!array_key_exists("update_user_balance", $cdpStepDone["content_sharer"])){
                                  // credit sharer payout amount
                                  $sharerCreditEarning = creditUserEarning($sharerId, $currentPayout, "post_sharer_earnings", "add");
                                  if(noError($sharerCreditEarning)){
                                      print('Successfully credit sharer payout amount to sharer.<br />');
                                  }else{
                                      print('Error: Unabel to credit sharer payout amount.<br />');
                                  }
                              }
                          } // end of content sharer earning and transaction

                          // insert sharer earning transaction
                          if(!array_key_exists("insert_user_transaction", $cdpStepDone["content_sharer"])){
                              $requestedField = $userRequiredFields.',email';
                              $getUserEmail = getUserDetailFromUserId($sharerId, $walletURLIP . 'api/v3/', $requestedField);
                              if(noError($getUserEmail)){
                                 $sharerEmailId = $getUserEmail["errMsg"]["email"];
                              }

                              // insert suer transaction for sharer earning
                              $recipientEmail = $sharerEmailId;
                              $recipientUserId = $sharerId;
                              $senderEmail = $communityPoolUser;
                              $senderUserId = $communityPoolUserId;
                              $amount = $currentPayout;
                              $type = "post_sharer_earning";
                              $paymentMode = 'ITD';
                              $originIp = $originIp;
                              $exchangeRateInJson = $exchangeRateInJson;

                              $metaDetails = array("content_id" => (string)$param["content_id"], "keyword" => urlencode(utf8_encode($param["search_query"])), "discount" => 0, "commision" => 0);

                              $insertSharerPayoutTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                              if(noError($insertSharerPayoutTrnasaction)){
                                  print('Successfully insert sharer payout earning transaction<br />');
                              }else{
                                  print('Error: Unable to insert sharer payout earning transaction<br />');
                              }
                          }
                      }
                  }
              }

              $kwdDbConn = createDBConnection("dbkeywords");
              if(noError($kwdDbConn)){
                  $kwdDbConn = $kwdDbConn["connection"];
              }else{
                  print('Error: keyword database connection');
              }

              // keyword owner earning and transaction
              if($cdpStatus != 3 && array_key_exists("keyword", $cdpStepDone)){
                    // explode keyword by space seperated
                    $keywordArr = explode(" ", $keyword);
                    // find keyword for  cdp step done field json
                    $keywordKeys = array_keys($cdpStepDone["keyword"]);

                    $keywordDiff  = array_diff($keywordArr, $keywordKeys);

                    $checkKwdArr[] = $keywordKeys;

                    // get last keyword from array to udpate keyword_cdp_status=3 in content_consumer_list table.
                    $lastKeywordInArr = end($keywordArr);

                    $keywordCount  = count($keywordDiff);

                    // calculate per keyword current payout
                    $perKeywordPayout = $currentPayout/$keywordCount;

                    $perKeywordPayout = number_format($perKeywordPayout, 8);


                    foreach($keywordDiff as $key => $keywordValue){


                        if(!array_key_exists($keywordValue, $cdpStepDone)){

                              $getOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keywordValue);
                              if(noError($getOwnershipDetails)){
                                  $getOwnershipDetails  = $getOwnershipDetails["errMsg"];
                                  $kwdOwnerEmail        = $getOwnershipDetails["buyer_id"];

                                  // if keyword owner email is empty then set keyword owner as admin
                                  if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                                        $kwdOwnerEmail = "admin";
                                  }

                                  if($kwdOwnerEmail == "admin"){
                                      startTransaction($kwdDbConn);
                                      // credit keyword owner earning
                                      $addKwdUnownedOwnerIncome = creditUserEarning($unOwnedKwdOwnerUserId, $perKeywordPayout, "kwdincome", "add");

                                      if(noError($addKwdUnownedOwnerIncome)){
                                          commitTransaction($kwdDbConn);
                                          print('Successfully credit amount to unowned keyword owner');
                                      }else{
                                          rollbackTransaction($kwdDbConn);
                                          print('Error: Unable to credit amount to unowned keyword owner');
                                      }

                                      // insert user transaction for keywor owner transaction
                                      $recipientEmail = $unownedKwdOwnerUser;
                                      $recipientUserId = $unOwnedKwdOwnerUserId;
                                      $senderEmail = $communityPoolUser;
                                      $senderUserId = $communityPoolUserId;
                                      $amount = $perKeywordPayout;
                                      $type = "keyword_earning";
                                      $paymentMode = 'ITD';
                                      $originIp = $originIp;
                                      $exchangeRateInJson = $exchangeRateInJson;

                                      $metaDetails = array("content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keywordValue)), "discount" => 0, "commision" => 0);

                                      $insertUnownedEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                      if(noError($insertUnownedEarningTrnasaction)){
                                          print('Successfully insert transaction for keyword owner earning');
                                      }else{
                                          print('Error: insert transaction for keyword owner earning');
                                      }
                                  }else{ // keyword owner exist
                                      // credit keyword owner earning
                                      // get keyword owner user id using getUserInfo method
                                      $kwdOwnerInfo = $userRequiredFields.",user_id";

                                      $kwdOwnerUserInfo =  getUserInfo($kwdOwnerEmail, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                                      if(noError($kwdOwnerUserInfo)){
                                          $kwdOwnerId = $kwdOwnerUserInfo["errMsg"]["_id"];
                                      }else{
                                          print('Error: Fetching user details');
                                      }
                                      // 1. update keyword income to keyword owner
                                      $addKwdOwnerkeywordIncome = creditUserEarning($kwdOwnerId, $perKeywordPayout, "kwdincome", "add");
                                      if(noError($addKwdOwnerkeywordIncome)){
                                            print('Successfully credit keyword owner earning');
                                      }else{
                                          print('Error: Unabel to credit keyword owner earning');
                                      }

                                      // insert tranaction for keyword owner earning
                                      $recipientEmail = $kwdOwnerEmail;
                                      $recipientUserId = $kwdOwnerId;
                                      $senderEmail = $communityPoolUser;
                                      $senderUserId = $communityPoolUserId;
                                      $amount = $perKeywordPayout;
                                      $type = "keyword_earning";
                                      $paymentMode = 'ITD';
                                      $originIp = $originIp;
                                      $exchangeRateInJson = $exchangeRateInJson;

                                      $metaDetails = array("content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keywordValue)), "discount" => 0, "commision" => 0);

                                      $insertKwdOwnerEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                      if(noError($insertKwdOwnerEarningTrnasaction)){
                                          print('Successfully insert keyword owner earning tranaction');
                                      }else{
                                          print('Error: Unabel to insert keyword owner earning transaction');
                                      }
                                  }
                              }else{
                                  print('Error: Unable to get keyword ownership details');
                              }
                        }else{
                            // if array key exist and earning or transaction key is missing then do earning and transaction
                            if(array_key_exists($keywordValue, $cdpStepDone)){
                                  $getOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keywordValue);
                                  if(noError($getOwnershipDetails)){
                                    $getOwnershipDetails  = $getOwnershipDetails["errMsg"];
                                    $kwdOwnerEmail        = $getOwnershipDetails["buyer_id"];

                                    // if keyword owner email is empty then set keyword owner as admin
                                    if(!isset($kwdOwnerEmail) && empty($kwdOwnerEmail)){
                                          $kwdOwnerEmail = "admin";
                                    }

                                    if($kwdOwnerEmail == "admin"){
                                        if(!array_key_exists("update_user_balance", $cdpStepDone[$keywordValue])){
                                            // credit keyword owner earning
                                            $addKwdUnownedOwnerIncome = creditUserEarning($unOwnedKwdOwnerUserId, $perKeywordPayout, "kwdincome", "add");
                                            if(noError($addKwdUnownedOwnerIncome)){
                                                print('Successfully credit amount to unowned keyword owner');
                                            }else{
                                                print('Error: Unable to credit amount to unowned keyword owner');
                                            }
                                        }

                                        if(!array_key_exists("insert_user_transaction", $cdpStepDone[$keywordValue])){
                                            // insert user transaction for keywor owner transaction
                                            $recipientEmail = $unownedKwdOwnerUser;
                                            $recipientUserId = $unOwnedKwdOwnerUserId;
                                            $senderEmail = $communityPoolUser;
                                            $senderUserId = $communityPoolUserId;
                                            $amount = $perKeywordPayout;
                                            $type = "keyword_earning";
                                            $paymentMode = 'ITD';
                                            $originIp = $originIp;
                                            $exchangeRateInJson = $exchangeRateInJson;

                                            $metaDetails = array("content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keywordValue)), "discount" => 0, "commision" => 0);

                                            $insertUnownedEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                            if(noError($insertUnownedEarningTrnasaction)){
                                                print('Successfully insert transaction for keyword owner earning');
                                            }else{
                                                print('Error: insert transaction for keyword owner earning');
                                            }
                                        }
                                    }else{
                                      $kwdOwnerInfo = $userRequiredFields.",user_id";

                                      $kwdOwnerUserInfo =  getUserInfo($kwdOwnerEmail, $walletURLIP.'api/v3/', $kwdOwnerInfo);
                                      if(noError($kwdOwnerUserInfo)){
                                          $kwdOwnerId = $kwdOwnerUserInfo["errMsg"]["_id"];
                                      }else{
                                          print('Error: Fetching user details');
                                      }

                                      if(!array_key_exists("update_user_balance", $cdpStepDone[$keywordValue])){
                                          // 1. update keyword income to keyword owner
                                          $addKwdOwnerkeywordIncome = creditUserEarning($kwdOwnerId, $perKeywordPayout, "kwdincome", "add");
                                          if(noError($addKwdOwnerkeywordIncome)){
                                                print('Successfully credit keyword owner earning');
                                          }else{
                                                print('Error: Unabel to credit keyword owner earning');
                                          }
                                      }

                                      if(!array_key_exists("insert_user_transaction", $cdpStepDone[$keywordValue])){
                                          // insert tranaction for keyword owner earning
                                          $recipientEmail = $kwdOwnerEmail;
                                          $recipientUserId = $kwdOwnerId;
                                          $senderEmail = $communityPoolUser;
                                          $senderUserId = $communityPoolUserId;
                                          $amount = $perKeywordPayout;
                                          $type = "keyword_earning";
                                          $paymentMode = 'ITD';
                                          $originIp = $originIp;
                                          $exchangeRateInJson = $exchangeRateInJson;

                                          $metaDetails = array("content_id" => (string)$appId, "keyword" => urlencode(utf8_encode($keywordValue)), "discount" => 0, "commision" => 0);

                                          $insertKwdOwnerEarningTrnasaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail,$senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                          if(noError($insertKwdOwnerEarningTrnasaction)){
                                              print('Successfully insert keyword owner earning tranaction');
                                          }else{
                                              print('Error: Unabel to insert keyword owner earning transaction');
                                          }
                                      }
                                    }
                                  }else{
                                      print('Error: Unable to get keyword ownership details');
                                  }
                            }
                        }
                    } // end foreach for keyword earning and transaction
              }
          }
      }else{
          print('Error: gettting content consumer details');
          exit;
      }

  }else{
      print('Error: keyword database connection');
      exit;
  }


 ?>
