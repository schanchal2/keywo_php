<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 10/2/17
 * Time: 2:55 PM
 */

session_start();

header('Content-Type: text/html; charset=utf-8');

  $docrootpath = __DIR__;

  $docrootpath = explode('/controllers', $docrootpath);
  $docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
error_reporting(0);

//get request parameters
$curr_code 	  = urldecode($_REQUEST["curCode"]);
$fromCurrCode = "BTC";
$amount       = '1';
$returnArray    = array();
if(isset($curr_code) && !empty($curr_code)){

    //create database connection
    $connSearch = createDbConnection('dbsearch');
    if (noError($connSearch)) {
        $connSearch = $connSearch["connection"];

        $exchangeRateArray = $exchageCurrencyCode;
        array_push($exchangeRateArray, strtoupper($curr_code));

        $getExchangeRate = getCurrencyExchangeRate($exchangeRateArray, $connSearch);
        if(noError($getExchangeRate))
        {
            $currentExchangeRate = $getExchangeRate["exchange_rate"];
            /*$currentExchangeRate = json_encode($getExchangeRate);*/

            $returnArray["errCode"] = -1;
            $returnArray["currRate"] = $currentExchangeRate;

        }else{
            $returnArray["errCode"] = 2;
            $returnArray["errMsg"] = "Error: Fetching current exchange rate.";
        }
    } else {
        $returnArray["errCode"] = 2;
        $returnArray["errMsg"] = "Error: Database connection";
    }
}else{
    $returnArray["errCode"] = 2;
    $returnArray["errMsg"]  = "Mandatory field not found";
}

//send json response
echo json_encode($returnArray);


?>
