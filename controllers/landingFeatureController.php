<?php
/*
* --------------------------------------------------------------------------------------------
*   Landing Page Controller
*---------------------------------------------------------------------------------------------
*
*   This controller is used to retrieve the details of CMS and feature description
*   of keywo's system.
*/

/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");

require_once('../config/config.php');
require_once('../config/db_config.php');
require_once('../helpers/arrayHelper.php');
require_once('../helpers/coreFunctions.php');
require_once('../helpers/errorMap.php');
require_once('../helpers/stringHelper.php');
require_once('../models/landing/landingFeatureModel.php');

error_reporting(0);
/*
* --------------------------------------------------------------------------------------------
*   Create database connection
*---------------------------------------------------------------------------------------------
*/
$returnArr = array();
$extraArgs = array();
$conn = createDBConnection('dbsearch');
$connkeyword = createDBConnection('dbkeywords');
if(noError($conn)&&noError($connkeyword)){
      $conn = $conn["connection"];
      $connkeyword = $connkeyword["connection"];
      $errMsg = "Database connection Successful!";
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

      // Accepting post data
      if(isset($_POST) && !empty($_POST)){
          $postType = cleanXSS(urldecode($_POST["type"]));

          $errMsg = "Post field found successfully!";
          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

          switch($postType){
              case "landing_slider":
                  $getCMS = getSliderData($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve CMS Slider details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting CMS Slider details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;
              case "landing_page":
                  $getCMS = getPageData($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve CMS  Page details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting CMS Page details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;
              case "landing_earn":
                  $getCMS = getHowToEarn($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve CMS HowToEarn details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting CMS HowToEarn details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;
              case "landing_feature":
                  $getCMS = getFeatureData($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve CMS FeatureData details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting CMS FeatureData details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;
              case "total_user":
                  $getCMS = getKeywoStats($connkeyword,array('registered_users'));
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve KeywoStats details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting KeywoStats details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;
              case "total_keyword_sold":
                  $getCMS = getTotalKeywordSold($connkeyword);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve CMS FeatureData details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting CMS FeatureData details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;                                     
              case "landing_video_by_id":
                  $id = trim(urldecode($_POST['vidId']));
                  //$ftype = trim(urldecode($_POST['ftype']));
                 $getCMS = getVideoById($conn, $id);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve VideoById details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting VideoById details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                  break;       
              case "gettotalpayout":
                 $getCMS = getTotalPayout($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve TotalPayout details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting TotalPayout details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                 
                  break;
               case "getincomeearned":
                 $getCMS = getTotalIncomeEarn($conn);
                  if(noError($getCMS)){
                        $getCMS = $getCMS["data"];
                        $errMsg = "Successfully retrieve TotalIncomeEarn details";
                        $extraArgs["errCode"] = -1;
                        $extraArgs["data"] = $getCMS;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                  }else{
                        $errMsg = "Error getting TotalIncomeEarn details";
                        $returnArr = setErrorStack($returnArr, 42, $errMsg, $extraArgs);
                  }
                 
                  break;
      
              case "id":
                  $sql = "select short_description, long_description, content_video_link from landing_page_content where id = ";
                  break;
              default : 
              echo "Please Send Type ";      
          }


      }else{
          $errMsg = "Post fields not found";
          $returnArr = setErrorStack($returnArr, 41, $errMsg, $extraArgs);
      }

}else{
      $returnArr = setErrorStack($returnArr, 1, null, $extraArgs);
}

echo json_encode($returnArr);
 ?>
