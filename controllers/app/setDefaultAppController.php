<?php
header("Access-Control-Allow-Origin: *");

		session_start();
		require_once('../../config/config.php');
		require_once('../../config/db_config.php');
		require_once('../../helpers/coreFunctions.php');
		require_once('../../helpers/errorMap.php');
		require_once ("../../models/app/appModel.php");

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$email = $_SESSION["email"];
	if(is_array($_POST) && sizeof($_POST) > 0){
		$appId = $_POST["appId"];
		$notifyFieldType = 'searchappid';
		$user_id="user_id";
		$getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$user_id);
		if(noError($getUserInfo)){
				$getUserInfo = $getUserInfo["errMsg"];
				$user_id = $getUserInfo["user_id"];
				$setApp = setUserNotifyInfo($user_id, $notifyFieldType, $appId);
				if(noError($setApp)){
					$setApp = $setApp["errMsg"];
					$returnArr['errMsg'] = 'success';
					$returnArr['errCode'] = -1;
				}else{
					$returnArr["errCode"] = 5;
					$returnArr["errMsg"] = $setApp["errMsg"];
				}
			}else{
				$returnArr["errCode"] = 5;
				$returnArr["errMsg"] = $setApp["errMsg"];
			}
	}else{
			$returnArr["errCode"]= 5;
			$returnArr["errMsg"] = 'ERROR IN SETTING Default APP';
	}
}else{
	$returnArr['errMsg'] = 'ERROR IN SETTING Default APP';
	$returnArr['errCode'] = 2;
}
echo json_encode($returnArr);
?>
