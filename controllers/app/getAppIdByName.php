<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/app/appModel.php');

error_reporting(0);

$searchDbConn = createDBConnection("dbsearch");
if(noError($searchDbConn)) {
  $searchDbConn = $searchDbConn["connection"];
  if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
  	$email     = $_SESSION["email"];
		$appName   = $_POST["appName"];
		$appId     = getAppIDbyNameValidationComplete($appName, $searchDbConn);
    $favAppRes = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', 'favourite_app_ids');

		if(noError($appId) && !empty($appId["errMsg"])) {
				$getAppInfo   = $appId["errMsg"];
        $userFavAppId = explode(',', $favAppRes["errMsg"]["favourite_app_ids"]);
        if(in_array($getAppInfo[0]["app_id"], $userFavAppId, TRUE)) {
  				$returnArr['errMsg']  = "Selected app is already exist in favorite app list.";
  				$returnArr['errCode'] = -2;
  			} else {
          $returnArr['appId']   = $getAppInfo[0]["app_id"];
          $returnArr['errMsg']  = 'success';
          $returnArr['errCode'] = -1;
  			}
		} elseif (empty($appName)) {
      $returnArr["errCode"] = 5;
			$returnArr["errMsg"]  = "Please enter something.";
    } else {
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"]  = "Please check the spelling or try different app name.";
		}
  } else {
  	$returnArr['errMsg']  =  $appId["errMsg"];
  	$returnArr['errCode'] = 2;
  }
} else {
  $returnArr['errMsg']  =  $appId["errMsg"];;
  $returnArr['errCode'] = 1;
}
echo json_encode($returnArr);
?>
