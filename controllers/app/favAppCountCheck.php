<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/app/appModel.php');

error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$actionType=$_POST["actionType"];
	$email = $_SESSION["email"];
	if(is_array($_POST) && sizeof($_POST) > 0){
		$userFavAppIds   = $_POST["userFavAppIds"];
		$notifyFieldType = 'favappids';
		$required        = "user_id,favourite_app_ids";
		$getUserInfo     = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$required);
		if(noError($getUserInfo)){
				$getUserInfo = $getUserInfo["errMsg"];
				$user_id     = $getUserInfo["user_id"];
				//check for max 6 fav apps
				$maxCount = 6;
				$action   = $_POST["action"];
				$favApps  = $getUserInfo["favourite_app_ids"];
				$favAppsCount = count(explode(',',$favApps));
				if (($action == "add") && ($favAppsCount == $maxCount)) {
					$returnArr['errMsg']  = "You can not choose more than six favorite apps.";
					$returnArr['errCode'] = 5;
					echo json_encode($returnArr, true);
					exit;
				} else {
          $returnArr['errMsg'] = 'success';
					$returnArr['errCode'] = -1;
        }
			}else{
				$returnArr["errCode"] = 5;
				$returnArr["errMsg"]  = $setApp["errMsg"];
			}
	}else{
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"]  = 'ERROR IN SETTING Default APP';
	}
}else{
	$returnArr['errMsg']  = 'ERROR IN SETTING Default APP';
	$returnArr['errCode'] = 2;
}
echo json_encode($returnArr);
?>
