<?php
header("Access-Control-Allow-Origin: *");

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once ('../../models/app/appModel.php');

error_reporting(0);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
	$email = $_SESSION["email"];

	if (is_array($_POST) && sizeof($_POST) > 0) {

		$userAppId       = $_POST["appId"];
		$actionOnApp   	 = $_POST["action"];
		$changeAppId     = $_POST["changeAppId"];
		$notifyFieldType = "favappids";
		$userId          = "user_id";
		$getUserInfo     = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$userId);
		// print_r($actionOnApp); die;
		if (noError($getUserInfo)) {
				$getUserInfo = $getUserInfo["errMsg"];
				$userId     = $getUserInfo["user_id"];
				//call api according to action to be performed on app ids
				if ($actionOnApp == "add") {
					$setApp = addFavApp($userId, $userAppId);
				} elseif ($actionOnApp == "change") {
					$setApp = updateFavApp($userId, $userAppId, $changeAppId);
				} elseif ($actionOnApp == "remove") {
					$setApp = deleteFavApp($userId, $userAppId);
				}

				if (noError($setApp)) {
					$setApp               = $setApp["errMsg"];
					$returnArr['errMsg']  = "success";
					$returnArr['errCode'] = $setApp["errCode"];
				} else {
					$returnArr["errCode"] = $setApp["errCode"];
					$returnArr["errMsg"]  = $setApp["errMsg"];
				}
			} else {
				$returnArr["errCode"] = $setApp["errCode"];
				$returnArr["errMsg"]  = $setApp["errMsg"];
			}
	} else {
			$returnArr["errCode"] = 5;
			$returnArr["errMsg"]  = "ERROR IN SETTING Default APP";
	}
} else {
	$returnArr['errMsg']  = "ERROR IN SETTING Default APP";
	$returnArr['errCode'] = 2;
}
echo json_encode($returnArr);
?>
