<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/email_helpers.php');

//end helper

//other
require_once('../../helpers/errorMap.php');
//end other
require_once("../../models/customer/customerModel.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();
if (isset($_SESSION["email"]) && isset($_SESSION["email"])) {
    $email = cleanQueryParameter($connKeywords, cleanXSS($_POST["email"]));
    $name = cleanQueryParameter($connKeywords, cleanXSS($_POST["name"]));
    $message = cleanQueryParameter($connKeywords, cleanXSS($_POST["message"]));
    $result = $setFAQs = setContactForm($name, $email, $message, $connKeywords);
    if ($result[errCode] == -1) {
        $getComment            = $getComment["errMsg"];
        $endmsgId              = end($getComment)["id"];
        $returnArr["errCode"]  = "-1";
        $returnArr["errMsg"]   = $endmsgId;
        $subject               = $contactFormSubject;
        $parameters["name"]    = "";
        $parameters["content"] = "<b>".$name."</b> Want to Contact You.<br/><b>Email : </b>".$email.".<br/><b>Message : </b>".$message;

        $sendContactEmail = sendEmail($contactEmailId,$noReplyEmail, $subject,"contact.html",$parameters);
        if (noError($sendContactEmail)) {
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"]  = $sendContactEmail['errMsg'];
        } else {
            $returnArr["errCode"] = 1;
            $returnArr["errMsg"]  = $sendContactEmail['errMsg'];
        }
    } else {
        $returnArr["errCode"] = 41;
        $returnArr["errMsg"]  = $result[errMsg];
    }
}

echo json_encode($returnArr);


?>
