<?php

session_start();

require_once ('../config/config.php');
require_once ('../config/db_config.php');
require_once ('../helpers/errorMap.php');
require_once ('../helpers/arrayHelper.php');
require_once ('../helpers/coreFunctions.php');
require_once ('../helpers/stringHelper.php');
require_once ('../helpers/transactionHelper.php');
require_once ('../models/keywords/userCartModel.php');
require_once ('../models/keywords/keywordSearchModel.php');
require_once ('../models/header/headerModel.php');

error_reporting(0);

$email              =   $_SESSION["email"];
$finalAmount        =   number_format((float)urldecode($_POST["finalAmount"]), 8);
$clientSessionId    =   urldecode($_POST["client_sid"]);
$orderId            =   urldecode($_POST["orderId"]);
$paymentMode        =   urldecode($_POST["mode"]);
//$keywordPrice       =   urldecode($_POST["keywordprice"]);

//get current session Id and refferer page
$currentSessionId = session_id();

$upc = strtolower($_SESSION["CurrPreference"]);

//initialize the return array
$returnArr = array();

if ($clientSessionId == $currentSessionId) {
    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];

        if(isset($email) && !empty($email) && isset($finalAmount) && !empty($finalAmount) &&  isset($paymentMode) && !empty($paymentMode)){

            $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
            if(noError($getAdminSetting)){
               $getAdminSetting = $getAdminSetting["data"];
               $renewalAmt = $getAdminSetting["kwd_renewal_fees_per_year"];


                // check user existence
                $checkUserExistence = checkEmailExistance($email);

                if(noError($checkUserExistence)){

                    $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

                    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);

                    if(noError($getUserDetails)){
                        $getUserBalance = calculateUserBalance($getUserDetails);
                        if(noError($getUserBalance)){
                            $getUserBalance = number_format($getUserBalance["errMsg"]["total_available_balance"], 8);

                            $getCartDetails = getUserCartDetails($email, $kwdDbConn);

                            if(noError($getCartDetails)){
                                $getCartDetails = $getCartDetails["errMsg"]["user_renew_kwd_in_cart"];
                                $getCartDetails = json_decode($getCartDetails, true);

                                $cartCount = COUNT($getCartDetails);
                                $totalRenwalAmount = 0;

                                if($cartCount > 0){
                                    foreach($getCartDetails as $key => $keyword){

                                        $keyword = strtolower($keyword);

                                        // get keyword ownership details
/*
                                        $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                                        if(noError($getKwdOwnershipDetails)){

                                            $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];

                                            $ownerId = $getKwdOwnershipDetails["buyer_id"];

                                            if($email == $ownerId){*/
                                                $renewKeyword .= "#" . $keyword . ", ";
                                                $totalRenwalAmount = $totalRenwalAmount + $renewalAmt;
                                         /*   }else{
                                                $returnArr['errCode'] = 199;
                                                $returnArr["errMsg"]  = "This is not your owned keyword. Please renew your owned keyword";

                                                echo json_encode($returnArr);
                                                exit;
                                            }
                                        }else{
                                            $returnArr['errCode'] = 9;
                                            $returnArr["errMsg"]  = "Error: Fetching keyword ownership details";
                                        }*/


                                    }


                                    $amtToPurchase = (float)($totalRenwalAmount - $getUserBalance);

                                    $amtToPurchase = number_format($amtToPurchase, 8);

                                        $newCartPriceUSD = $totalRenwalAmount;

                                        $getExchangeRate = getExchangeRates($upc);
                                        if(noError($getExchangeRate)){

                                            $getExchangeRate = $getExchangeRate["currRate"];

                                            $currentRateUSD = $getExchangeRate["usd"];

                                            $currentRateSGD = $getExchangeRate["sgd"];

                                            //changes in SGD current rate for accomodating 1% transaction charges by paypal
                                            //$currentRateSGD = $currentRateSGD * 1.1;

                                            //get single amount in SGD
//                                                $singleAmount = $keywordPrice * $currentRateSGD;
//                                                $singleAmount = number_format((float)$singleAmount, 2);

                                            //get final amount in SGD
                                            $totalAmount = $amtToPurchase * $currentRateSGD;
                                            $totalAmount = number_format((float)$totalAmount, 4);

                                            $newCartPriceUPC = (float)$totalRenwalAmount * $getExchangeRate["{$upc}"];
                                            $newCartPriceUPC = number_format((float)($newCartPriceUPC),4,'.','');

                                            $newCartPriceBTC = $amtToPurchase * $getExchangeRate["btc"];
                                            $newCartPriceBTC = number_format(floatval($newCartPriceBTC),8,'.','');

                                            $newCartPriceSGD = $amtToPurchase * $getExchangeRate["sgd"];
                                            $newCartPriceSGD = number_format((float)($newCartPriceSGD),4,'.','');

                                            //create custom variable string to be passed to the paypal express checkout method
                                            $custom = $email . "~" . $finalAmount . "~" . $currentRateSGD . "~" . $currentRateUSD;

                                            $returnArr["errCode"] = -1;
                                            $returnArr["errMsg"] = "success";
                                            $returnArr["newCartPriceUSD"] = $newCartPriceUSD;
                                            $returnArr["newCartPriceSGD"] = $newCartPriceSGD;
                                            $returnArr["newCartPriceBTC"] = $newCartPriceBTC;
                                            $returnArr["newCartPriceUPC"] = $newCartPriceUPC;
                                            $returnArr["userAvailableBalance"] = $getUserBalance;
                                            $returnArr["purchasingAmtInITD"] = number_format($amtToPurchase, 4);
                                            $returnArr["keyword_renew"] = rtrim($renewKeyword,", ");
                                            $returnArr["singleAmount"] = $singleAmount;
                                            $returnArr["custom"] = $custom;
                                        }else{
                                            $returnArr["errCode"] = 10;
                                            $returnArr["errMsg"] = "Error: Getting exchagne rate";
                                        }

                                }else{
                                    $returnArr["errCode"] = 201;
                                    $returnArr["errMsg"] = "User cart is empty";
                                }
                            }else{
                                $returnArr["errCode"] = 5;
                                $returnArr["errMsg"] = "Error: Getting cart details";
                            }
                        }else{
                            $returnArr["errCode"] = 6;
                            $returnArr["errMsg"] = "Error: Unable to get available balance";
                        }
                    }else{
                        $returnArr["errCode"] = 5;
                        $returnArr["errMsg"] = "Error: Unable to get balance details";
                    }

                }else{
                    $returnArr["errCode"] = 4;
                    $returnArr["errMsg"] = "User is not registered";
                }
            }else{
                $returnArr["errCode"] = 6;
                $returnArr["errMsg"] = "Error: Fetching admin setting details";
            }

        }else{
            $returnArr['errCode'] = 3;
            $returnArr["errMsg"]  = "Mandatory field not found";
        }
    }else{
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = "Error: Database Connection";
    }
}else{
    $returnArr["errCode"] = 1;
    $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
}

echo json_encode($returnArr)


?>