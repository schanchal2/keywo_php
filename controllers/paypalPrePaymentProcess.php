<?php
session_start();

require_once ('../config/config.php');
require_once ('../config/db_config.php');
require_once ('../helpers/errorMap.php');
require_once ('../helpers/arrayHelper.php');
require_once ('../helpers/coreFunctions.php');
require_once ('../helpers/stringHelper.php');
require_once ('../helpers/transactionHelper.php');
require_once ('../models/keywords/userCartModel.php');
require_once ('../models/keywords/keywordSearchModel.php');
require_once ('../models/header/headerModel.php');

error_reporting(0);

$email              =   $_SESSION["email"];
$finalAmount        =   number_format((float)urldecode($_POST["finalAmount"]), 8);
$clientSessionId    =   urldecode($_POST["client_sid"]);
$orderId            =   urldecode($_POST["orderId"]);
$paymentMode        =   urldecode($_POST["mode"]);
//$keywordPrice       =   urldecode($_POST["keywordprice"]);

//get current session Id and refferer page
$currentSessionId = session_id();

$upc = strtolower($_SESSION["CurrPreference"]);

//initialize the return array
$returnArr = array();

if ($clientSessionId == $currentSessionId) {
    $kwdDbConn = createDBConnection("dbkeywords");
    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];

        if(isset($email) && !empty($email) && isset($finalAmount) && !empty($finalAmount) && isset($orderId) && !empty($orderId) && isset($paymentMode) && !empty($paymentMode)){

            // check user existence
            $checkUserExistence = checkEmailExistance($email);

            if(noError($checkUserExistence)){

                $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";

                $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);

                if(noError($getUserDetails)){

                    $firstBuyStatus = $getUserDetails["errMsg"]["first_buy_status"];

                    $getUserBalance = calculateUserBalance($getUserDetails);
                    if(noError($getUserBalance)){
                        $getUserBalance = number_format($getUserBalance["errMsg"]["total_available_balance"], 8);

                        //initialize strings to empty
                        $keywordAvailable    = "";
                        $keywordNotAvailable = "";

                        $getCartDetails = getUserCartDetails($email, $kwdDbConn);
                        if(noError($getCartDetails)){
                            $getCartDetails = $getCartDetails["errMsg"]["user_cart"];
                            $getCartDetails = json_decode($getCartDetails, true);

                            $cartCount = COUNT($getCartDetails);

                            if($cartCount > 0){
                                foreach($getCartDetails as $key => $keyword){
                                    if(isset($keyword) && !empty($keyword)){
                                        $keyword = strtolower($keyword);

                                        $keywordStatus = checkKeywordCartStatus($keyword, $kwdDbConn);
                                        if(noError($keywordStatus)){
                                            $keywordStatus = $keywordStatus["errMsg"];

                                            $kwdOwnerEmail = $keywordStatus["buyer_id"];

                                            if(isset($kwdOwnerEmail) && !empty($kwdOwnerEmail)){
                                                $keywordNotAvailable .= "#" . $keyword . ", "; //keywords those are blocked for 15 minutes or sold
                                            }else{
                                                $keywordAvailable .= "#" . $keyword . ", "; //keywords those are available
                                            }
                                        }else{
                                            $returnArr["errCode"] = 7;
                                            $returnArr["errMsg"] = "Error: Unable to get keyword status";
                                        }

                                    }
                                }


                                if(empty($keywordNotAvailable)){

                                    $cartKeywordsDetails  = getUserTotalCartPrice($kwdDbConn, $email);
                                    if(noError($cartKeywordsDetails)){
                                        $total_cart_price_new = number_format((float)$cartKeywordsDetails['errMsg']['total_cart_price'],2,'.','');

                                        $amtToPurchase = (float)($total_cart_price_new - $getUserBalance);

                                        $amtToPurchase = number_format($amtToPurchase, 8);

                                        if ($total_cart_price_new == $finalAmount) {

                                            $getExchangeRate = getExchangeRates($upc);
                                            if(noError($getExchangeRate)){


                                                $getExchangeRate = $getExchangeRate["currRate"];

                                                $currentRateUSD = $getExchangeRate["usd"];

                                                $currentRateSGD = $getExchangeRate["sgd"];

                                                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                                                if (noError($adminSettings)) {
                                                    $errMsg = "getting admin settings of buyer user success.";
                                                    $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];

                                                    if ($firstBuyStatus) {
                                                        $firstBuyCashBack = (float)(($total_cart_price_new * $firstBuyPercent) / 100);
                                                    } else {
                                                        $firstBuyCashBack = 0.00000000;
                                                    }

                                                    //New cart prices
                                                    $newCartPriceUSD = $total_cart_price_new;
                                                    $amtToPurchase = ((float)$amtToPurchase - $firstBuyCashBack);

                                                    //changes in SGD current rate for accomodating 1% transaction charges by paypal
                                                    //$currentRateSGD = $currentRateSGD * 1.1;

                                                    //get single amount in SGD
//                                                $singleAmount = $keywordPrice * $currentRateSGD;
//                                                $singleAmount = number_format((float)$singleAmount, 2);

                                                    //get final amount in SGD
                                                    $totalAmount = $amtToPurchase * $currentRateSGD;
                                                    $totalAmount = number_format((float)$totalAmount, 2);

                                                    $newCartPriceUPC = (float)$amtToPurchase * $getExchangeRate["{$upc}"];
                                                    $newCartPriceUPC = number_format((float)($newCartPriceUPC),4,'.','');

                                                    $newCartPriceBTC = $amtToPurchase * $getExchangeRate["btc"];
                                                    $newCartPriceBTC = number_format(floatval($newCartPriceBTC),8,'.','');

                                                    $newCartPriceSGD = $amtToPurchase * $getExchangeRate["sgd"];
                                                    $newCartPriceSGD = number_format((float)($newCartPriceSGD),2,'.','');

                                                    //create custom variable string to be passed to the paypal express checkout method
                                                    $custom = $email . "~" . $finalAmount . "~" . $currentRateSGD . "~" . $currentRateUSD;

                                                    $query  = "UPDATE payments SET amount_due_in_sgd='" . $totalAmount . "' WHERE order_id='" . $orderId . "' AND username='" . $email . "' AND status='unpaid';";
                                                    $result = runQuery($query, $kwdDbConn);

                                                    if(noError($result)){
                                                        $blockTimeInMinute = 15;
                                                        //block keywords for 15 minutes
                                                        $result = blockKeywordsDuringPayment($blockTimeInMinute, $email, $kwdDbConn);
                                                        if(noError($result)){

                                                            if ($newCartPriceUSD !== $finalAmount) {
                                                                //When rate is fluctuate
                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = "Proceed Payment with new Rates";
                                                                $returnArr["newCartPriceUSD"] = $amtToPurchase;
                                                                $returnArr["newCartPriceSGD"] = $newCartPriceSGD;
                                                                $returnArr["newCartPriceBTC"] = $newCartPriceBTC;
                                                                $returnArr["newCartPriceUPC"] = $newCartPriceUPC;
                                                                $returnArr["userAvailableBalance"] = $getUserBalance;
                                                                $returnArr["purchasingAmtInITD"] = number_format($amtToPurchase, 2);
                                                                $returnArr["keyword_available"] = rtrim($keywordAvailable,", ");
                                                                $returnArr["singleAmount"] = $singleAmount;
                                                                $returnArr["custom"] = $custom;
                                                            } else {
                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = "Proceed Payment with current Rates";
                                                                $returnArr["newCartPriceUSD"] = $amtToPurchase;
                                                                $returnArr["newCartPriceSGD"] = $newCartPriceSGD;
                                                                $returnArr["newCartPriceBTC"] = $newCartPriceBTC;
                                                                $returnArr["newCartPriceUPC"] = $newCartPriceUPC;
                                                                $returnArr["userAvailableBalance"] = $getUserBalance;
                                                                $returnArr["purchasingAmtInITD"] = number_format($amtToPurchase, 2);
                                                                $returnArr["keyword_available"] = rtrim($keywordAvailable,", ");
                                                                $returnArr["singleAmount"] = $singleAmount;
                                                                $returnArr["custom"] = $custom;
                                                            }
                                                        }else{
                                                            $returnArr["errCode"] = 2;
                                                            $returnArr["errMsg"] = "Error blocking keywords for 15 minutes";
                                                        }
                                                    }else{
                                                        $returnArr["errCode"] = 11;
                                                        $returnArr["errMsg"] = "Error: Updating payment details";
                                                    }
                                                }else{
                                                    $returnArr["errCode"] = 10;
                                                    $returnArr["errMsg"] = "Error: Fetching setting details";
                                                }

                                            }else{
                                                $returnArr["errCode"] = 10;
                                                $returnArr["errMsg"] = "Error: Getting exchagne rate";
                                            }
                                        }else{
                                            $returnArr["errCode"] = 9;
                                            $returnArr["errMsg"]  = "Slab rate changed";
                                        }
                                    }else{
                                        $returnArr["errCode"] = 8;
                                        $returnArr["errMsg"] = "Error: Getting total cart price";
                                    }
                                }else{
                                    $returnArr["errCode"]               = 7;
                                    $returnArr["errMsg"]                = "Cart consists of un-available keywords";
                                    $returnArr["keyword_not_available"] = $keywordNotAvailable;
                                }
                            }else{
                                $returnArr["errCode"] = 6;
                                $returnArr["errMsg"] = "User cart is empty";
                            }
                        }else{
                            $returnArr["errCode"] = 5;
                            $returnArr["errMsg"] = "Error: Getting cart details";
                        }
                    }else{
                        $returnArr["errCode"] = 6;
                        $returnArr["errMsg"] = "Error: Unable to get available balance";
                    }
                }else{
                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"] = "Error: Unable to get balance details";
                }

            }else{
                $returnArr["errCode"] = 4;
                $returnArr["errMsg"] = "User is not registered";
            }
        }else{
            $returnArr['errCode'] = 3;
            $returnArr["errMsg"]  = "Mandatory field not found";
        }
    }else{
        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = "Error: Database Connection";
    }
}else{
    $returnArr["errCode"] = 1;
    $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
}

echo json_encode($returnArr)






?>