<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 12/12/16
 * Time: 6:12 PM
 */

header("Access-Control-Allow-Origin: *");

require_once "../config/countryArray.php";
error_reporting(0);

$country = trim(urldecode($_POST['country']));

if(array_key_exists($country,$countryCity)){

      // sort array into alphabetical order.
      sort($countryCity[$country]);

      echo "<option value=''>Select City</option>";
      foreach ($countryCity["{$country}"] as $city) {
          echo "<option value=\"$city\">$city</option>";
      }
}else{
      echo "<option>No cities found</option>";
}
?>
