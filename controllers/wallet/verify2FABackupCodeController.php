<?php 
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/wallet2FAModel.php'); 
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../views/wallet/googleAuth/vendor/autoload.php');

error_reporting(0);
$email = $_SESSION["email"];
$user_id = $_SESSION["id"];
$backup_code = trim(urldecode($_POST['backup_code']));

$backupCodes = getBackupCodes($email,$user_id);
$backup_code_id='';
$available_stat = 0;
foreach ($backupCodes['errMsg']['codes'] as $key => $value) {

	if($backup_code == $value['code'] && $value['state'] == 'available'){
		$backup_code_id = $value['_id'];
	}
	if($value['state'] != 'available'){
		$available_stat = $available_stat + 1;
	}
}

if($available_stat < 5 ){
	if(!empty($backup_code_id)){
		$response = updateCodeState($email,$user_id,$backup_code_id,$backup_code);
		if(noError($response)){
			$_SESSION['verify_status'] = "verified";
			$returnArr['errCode'] = -1;
			$returnArr['errMsg'] = "Backup code has been successfully verified";
		}else{
			$returnArr['errCode'] = 2;
			$returnArr['errMsg'] = "Invalid Backup code";
		}
	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Invalid Backup code";
	}
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "You already used all backup codes.Contact to contact@keywo.com for more info.";	
}

echo json_encode($returnArr);
?>