<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);


$transDetails = json_decode(urldecode($_POST['trans_details']),true);
$action = cleanXSS(urldecode($_POST['action']));
$amountInITD = $transDetails['amount'];

$reciverEmail = $transDetails['request_sender']; 
$transId = $transDetails['_id'];
$note =  cleanXSS(urldecode($_POST['note']));
$user_id = $_SESSION["id"];
$userEmail = $_SESSION["email"];
$ipAddr = getClientIP();
$i = 1;

$logStorePath =$logPath["wallet"];

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "walletAcceptRequest.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);

$metaData['country'] = $xmlArray['activity']['country'];
$metaData['gender'] = $xmlArray['activity']['gender'];
$metaData['device'] = $xmlArray['activity']['device'];

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if(!isset($_SESSION['verify_status'])){

	$unique_id = md5(uniqid(rand(), true));
	$xmlArray['activity']['id'] = $unique_id;
	$_SESSION['activity_id'] = $unique_id;

	$_SESSION['redirect_url'] = $returnURl;
	$msg = "Send IT$ process start.";
	$xml_data['step'.$i]["data"] = $i.". {$msg}";

	$msg = "Send IT$ process start.";
	$xml_data['step'.$i]["data"] = $i.". {$msg}";

	$userBal =  getUserBalance($userEmail);
	if($userBal['errMsg']['total_available_balance'] >= $amountInITD){
		// create search database connection
		$searchDbConn = createDBConnection("dbsearch");
		if(noError($searchDbConn)){
			$searchDbConn = $searchDbConn["connection"];

			//$totalSendCount = getAccountTotalCount($userEmail,$user_id);
			$kycStablDetails = getKYCSlabByLevel($searchDbConn);
			if(noError($kycStablDetails)){
				$kycStablDetails = $kycStablDetails['errMsg'];
			}

			$msg = "Success : Search database connection";
			$xml_data['step'.++$i]["data"] = $i.". {$msg}";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

			// get currency exchange rate of usd, sgd and btc
			$getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
			if(noError($getExchangeRate)){
			  $getExchangeRate = $getExchangeRate["exchange_rate"];
				$msg = "Success : Currency exchange rate";
				$xml_data['step'.++$i]["data"] = $i.". {$msg}";
				$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
				$userRequiredFields = $userRequiredFields.",email,account_handle,kyc_current_level,active";

				//fetch details of sender and receiver                    
				$getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
				if(noError($getSendersDetails)){
				  $senderDetails = $getSendersDetails['errMsg'];
				  $msg = "Success : Fetched sender details";
				  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
				  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

				  $sendLimit = checkSendLimit($kycStablDetails,$senderDetails['kyc_current_level'],$amountInITD,$userEmail,$user_id);
				  if(noError($sendLimit)){
					$msg = "Success : Checked daily and yearly send limit";
					$xml_data['step'.++$i]["data"] = $i.". {$msg}";
					$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
				  

					$getReciversDetails = getUserInfo($reciverEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
					if(noError($getReciversDetails) && $getReciversDetails['errMsg']['active'] == 1 ){
					  $reciversDetails = $getReciversDetails['errMsg'];
					  $msg = "Success : Fetched {$reciverEmail} details";
					  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
					  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs); 

					  //redirecting to 2FA page
					  $msg = "Success : Redirecting to 2FA page";
					  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
					  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
					  $_SESSION['form_data'] = json_encode($_POST);
					  $_SESSION['xml_step'] = $i;

					  $_SESSION['xml_file'] = $logStorePath . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;
					}else{
					  if(noError($getReciversDetails) && $getReciversDetails['erMsg']['active'] != 1 ){
						$errMsg = "Inactive or blocked receiver";
						$xml_data['step'.++$i]["data"] = $i.". Error: Inactive or blocked receiver({$emailHandle})";  
					  }else{
						$errMsg = "Not found details of {$emailHandle}";
						$xml_data['step'.++$i]["data"] = $i.". Error: Fetching {$emailHandle} details";
					  }              
					  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs); 
					}
				  }else{
					$errMsg = $sendLimit['errMsg'];
					$xml_data['step'.++$i]["data"] = $i.". Error: ".$sendLimit['errMsg'];
					$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
				  }
				}else{
				  $errMsg = 'Not found senders details';
				  $xml_data['step'.++$i]["data"] = $i.". Error: Fetching sender details";
					$returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs); 
				} 

			}else{
				$errMsg = 'Error: Fetching exchange rate';
				$xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
				$returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
			}   
			
		}else{

			$errMsg = 'Error: unable to connect search database';
			$xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
			$returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
		}
	}else{
		$errMsg = "Your available balance is insufficient for this transaction";
		$xml_data['step'.++$i]["data"] = $i.". Error: ".$errMsg;
		$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
	}


	// create or update xml log Files
	$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

	echo json_encode($returnArr); die;


}else if($returnURl == $_SESSION['redirect_url']){

  $i = $_SESSION['xml_step'];
  $security_type = $_SESSION['security_type'];
  $form_data = json_decode($_SESSION['form_data'],true);
  $activity_id = $_SESSION['activity_id'];
  $transDetails = json_decode(urldecode($form_data['trans_details']),true);
  $action = cleanXSS(urldecode($form_data['action']));
  $amountInITD = $transDetails['amount'];
  $reciverEmail = $transDetails['request_sender']; 
  $transId = $transDetails['_id'];
  $note =  cleanXSS(urldecode($form_data['note']));
  $xml_file = $_SESSION['xml_file'];

  unset($_SESSION['security_type']);
  unset($_SESSION['xml_step']);
  unset($_SESSION['form_data']);
  unset($_SESSION['activity_id']);
  unset($_SESSION['redirect_url']);
  unset($_SESSION['xml_file']);
  unset($_SESSION['verify_status']);

  $msg = "Success : Callback from 2FA of {$security_type}";
  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

  // create search database connection
  $searchDbConn = createDBConnection("dbsearch");
  if(noError($searchDbConn)){
      $searchDbConn = $searchDbConn["connection"];

      //$totalSendCount = getAccountTotalCount($userEmail,$user_id);
      $kycStablDetails = getKYCSlabByLevel($searchDbConn);
      if(noError($kycStablDetails)){
          $kycStablDetails = $kycStablDetails['errMsg'];
      }

      $msg = "Success : Search database connection";
      $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

      // get currency exchange rate of usd, sgd and btc
      $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
      if(noError($getExchangeRate)){
        $getExchangeRate = $getExchangeRate["exchange_rate"];
          $msg = "Success : Currency exchange rate";
          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
          $userRequiredFields = $userRequiredFields.",email,account_handle,kyc_current_level";

          //fetch details of sender and receiver                    
          $getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
          if(noError($getSendersDetails)){
            $senderDetails = $getSendersDetails['errMsg'];
            $msg = "Success : Fetched sender details";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

            $sendLimit = checkSendLimit($kycStablDetails,$senderDetails['kyc_current_level'],$amountInITD,$userEmail,$user_id);
            if(noError($sendLimit)){
              $msg = "Success : Checked daily and yearly send limit";
              $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
            

              $getReciversDetails = getUserInfo($reciverEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
              if(noError($getReciversDetails)){
                $reciversDetails = $getReciversDetails['errMsg'];
                $msg = "Success : Fetched {$reciverEmail} details";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs); 

                $response = acceptITDRequest($senderDetails,$reciversDetails,$transId,$note,$metaData); 
                if(noError($response)){

                   //send notification to request receiver(Rejecter).  
                  $email = $userEmail;
                  $mailSubject = ' ';
                  $emailBody = ' ';
                  $firstName = $senderDetails['first_name'];
                  $lastName = $senderDetails['last_name'];
                  $userId = $senderDetails['_id'];
                  $notificationBody = sprintf($notificationMessages['accept_req_sender'], $amountInITD, $keywoDefaultCurrencyName, $reciversDetails['email']);
                  $preferenceCode = '';
                  $category = "wallet";

                  $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

                  //send notification to request sender.  
                  $email = $reciversDetails['email'];
                  $mailSubject = ' ';
                  $emailBody = ' ';
                  $firstName = $reciversDetails['first_name'];
                  $lastName = $reciversDetails['last_name'];
                  $userId = $reciversDetails['_id'];
                  $notificationBody = sprintf($notificationMessages['accept_req_reciver'], $userEmail, $amountInITD, $keywoDefaultCurrencyName);
                  $preferenceCode = '';
                  $category = "wallet";

                  $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
                  $msg = "You successfully sent {$amountInITD} ".$keywoDefaultCurrencyName." to {$reciverEmail}";
                   $xml_data['step'.++$i]["data"] = $i.". transaction_id :".$response['errMsg']['committed_transaction']['transaction_id'];
                  $xml_data['step'.++$i]["data"] = $i.". Success : Sent {$amountInITD} ".$keywoDefaultCurrencyName." to {$reciverEmail}";
                  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
                }else{
                  if(!isset($response['errMsg']['payment_failure'])){
                    $errMsg = "Failed to send {$amountInITD} ".$keywoDefaultCurrencyName;
                    $xml_data['step'.++$i]["data"] = $i.". Error: sending {$amountInITD} ".$keywoDefaultCurrencyName."  to {$emailHandle}";
                    $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                  }else{
                    $errMsg = ucfirst($response['errMsg']['payment_failure']);
                    $xml_data['step'.++$i]["data"] = $i.". Error: ".$response['errMsg']['payment_failure'];
                    $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                  }
                     
                } 
              }else{
                $errMsg = "Not found details of {$emailHandle}";
                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs); 
              }
            }else{
              $errMsg = $sendLimit['errMsg'];
              $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
            }
          }else{
            $errMsg = 'Not found senders details';
              $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs); 
          } 

      }else{
          $errMsg = 'Error: Fetching exchange rate';
          $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
      }   
      
  }else{

    $errMsg = 'Error: unable to connect search database';
     $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
  }

  //create or update xml log Files
  $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);
  if(noError($returnArr)){
    $_SESSION['succ_msg'] = $returnArr['errMsg'];
  }else{
    $_SESSION['err_msg'] = $returnArr['errMsg'];  
  }
  header('Location:'.$rootUrl.'views/wallet/requestITD.php');
}else{
  unset($_SESSION['security_type']);
  unset($_SESSION['xml_step']);
  unset($_SESSION['form_data']);
  unset($_SESSION['activity_id']);
  unset($_SESSION['redirect_url']);
  unset($_SESSION['xml_file']);
  unset($_SESSION['verify_status']);
  $_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";  
  header('Location:'.$rootUrl.'views/wallet/requestITD.php');
}
?>