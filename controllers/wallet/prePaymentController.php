<?php
header("Access-Control-Allow-Origin: *");
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
$returnArr = array();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    
    $returnArr['errCode'] = 100;
    $returnArr['errMsg'] = "Invalid session";
    echo json_encode($returnArr); die;
}

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");

error_reporting(0);

//code to check whether session is running or not
$email = $_SESSION["email"];
$upc = $_SESSION['CurrPreference'];
// get current session id
$clientSessionId = session_id();

$xmlArray = initializeXMLLog($email);

$metaData['country'] = $xmlArray['activity']['country'];
$metaData['gender'] = $xmlArray['activity']['gender'];
$metaData['device'] = $xmlArray['activity']['device'];

//Keywords database connection
$kwdConn = createDbConnection('dbkeywords');

if (noError($kwdConn)) {
    $kwdConn = $kwdConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

$keywordBasket = 'itd_purchase_only';
$total_amt = $_POST['itd_amount'];
$payment_mode = $_POST['payment_mode'];
$metaData = json_encode($metaData);
$payment_mode = ($payment_mode=='bitcoin')?'bitgo':$payment_mode;

$upc = strtolower($_SESSION["CurrPreference"]);
$getExchangeRate = getExchangeRates($upc);

if(noError($getExchangeRate)){

    $getExchangeRate = $getExchangeRate["currRate"];
    $currentRateUSD = $getExchangeRate["usd"];
    $currentRateSGD = $getExchangeRate["sgd"];

    //get final amount in SGD
    $totalAmount = $total_amt * $currentRateSGD;
    $totalAmountSGD = number_format((float)$totalAmount, 2);

    $insertBitgoDetails = insertITDDetialsForPayment($kwdConn, $keywordBasket, $total_amt, $totalAmountSGD, $email, $payment_mode, $metaData);
    if(noError($insertBitgoDetails)){
        $orderIdForBitgo = $insertBitgoDetails['orderId'];
        $returnArr['errMsg']['orderId'] = $orderIdForBitgo;
        $returnArr['errCode'] = -1;
    }else{
        $returnArr['errMsg'] = $insertBitgoDetails['errMsg'];
        $returnArr['errCode'] = 2;

    }

}else{
        $returnArr['errMsg'] = $getExchangeRate['errMsg'];
        $returnArr['errCode'] = 3;
}





function insertITDDetialsForPayment($conn, $keywordBasket, $totaAmount, $totalAmountSGD, $email, $type, $metaData)
{
    $returnArr = array();

    $length = "5";
    $strength = "1";
    $nounce = time();
    $randomCode = generateVoucherCode($length, $strength);
    $orderId = $randomCode . $nounce;
    $query = "INSERT INTO payments(order_id,order_time,amount_due_in_usd,amount_due_in_sgd,username,payment_type,keyword_cart,amount_due,meta_details) VALUE('" . $orderId . "',now(),'" . $totaAmount . "','".$totalAmountSGD."','" . $email . "','" . $type . "','" . $keywordBasket . "','" . $totaAmount . "','".$metaData."');";
    //set custom attribute(order-id+email)
    $custom = $orderId . "," . $email;

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Success";
        $returnArr["orderId"] = $orderId;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

echo json_encode($returnArr); die;
?>