<?php 
session_start();
error_reporting(E_ALL);
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../config/country_codes.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);



$returnArr = array();
$uploadDirectoy = $kycUploadDirectoy;
if (!is_dir($uploadDirectoy)) {
    mkdir($uploadDirectoy, 0777, true);
}
$awsPath = $kycAWSPath;
$docMaxSize = 2000000;
$errString = '';
$fileArr = array();
$fileSuccArr = array();
$email = $_SESSION["email"];
$userId = $_SESSION['id']; 

if(!empty($_FILES)){

	foreach ($_FILES as $key => $value) {
		if($value['error']==0){
			$imageFileType = pathinfo($value['name'],PATHINFO_EXTENSION);
			if($imageFileType == "jpg" || $imageFileType == "png" || $imageFileType == "jpeg" || $imageFileType == "pdf" ){
				if($value['size'] <= $docMaxSize){
					$filename = uniqid().".".$imageFileType;
					$targetPath = $uploadDirectoy.$filename;
					if (move_uploaded_file($value["tmp_name"], $targetPath)) {
					    $fileArr[$key]['success'] = "The file ". basename( $value["name"]). " has been uploaded";
					    $fileSuccArr[$key] = $awsPath.$filename;
					} else {
						$fileArr[$key]['error'] = "Failed to upload document this file";
					    $errString .= $value['name'].",";
					}

				}else{
					$fileArr[$key]['error'] = "file size is more than 2MB,Please upload file with lower size";
					$errString .= $value['name'].",";	
				}

			}else{
				$fileArr[$key]['error'] = "Invalid file type";
				$errString .= $value['name'].",";
			}
		}
	}
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "Please select documents";
}

$userRequiredFields = $userRequiredFields.",email,account_handle,trend_preference,kyc_current_level,kyc_3_doctype_1,kyc_3_doctype_2,kyc_3_doctype_3,kyc_3_doctype_4";

//fetch details of sender and receiver                    
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
$userDetails = $userDetails['errMsg'];


if($_FILES['doc_file1']['error'] == 0 && isset($fileArr['doc_file1']['success'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $_POST['belongs_to'];
	$docDetails['doc_type'] = $_POST['doc_type'];
	$docDetails['path'] = $fileSuccArr['doc_file1'];
	$docDetails['status'] = 'pending';
	$docDetails['comment'] = NULL;
	$doc1 = json_encode($docDetails);
}elseif(!empty($userDetails['kyc_3_doctype_1'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $userDetails['kyc_3_doctype_1']['belongs_to'];
	$docDetails['doc_type'] = $userDetails['kyc_3_doctype_1']['doc_type'];
	$docDetails['path'] = $userDetails['kyc_3_doctype_1']['path'];
	$docDetails['status'] = $userDetails['kyc_3_doctype_1']['status'];
	$docDetails['comment'] = NULL;
	$doc1 = json_encode($docDetails);
}



if($_FILES['doc_file2']['error'] == 0 && isset($fileArr['doc_file2']['success'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $_POST['belongs_to'];
	$docDetails['doc_type'] = $_POST['doc_type'];
	$docDetails['path'] = $fileSuccArr['doc_file2'];
	$docDetails['status'] = 'pending';
	$docDetails['comment'] = NULL;
	$doc2 = json_encode($docDetails);
}elseif(!empty($userDetails['kyc_3_doctype_2'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $userDetails['kyc_3_doctype_2']['belongs_to'];
	$docDetails['doc_type'] = $userDetails['kyc_3_doctype_2']['doc_type'];
	$docDetails['path'] = $userDetails['kyc_3_doctype_2']['path'];
	$docDetails['status'] = $userDetails['kyc_3_doctype_2']['status'];
	$docDetails['comment'] = NULL;
	$doc2 = json_encode($docDetails);
}

if($_FILES['doc_file3']['error'] == 0 && isset($fileArr['doc_file3']['success'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $_POST['belongs_to'];
	$docDetails['doc_type'] = $_POST['doc_type'];
	$docDetails['path'] = $fileSuccArr['doc_file3'];
	$docDetails['status'] = 'pending';
	$docDetails['comment'] = NULL;
	$doc3 = json_encode($docDetails);
}elseif(!empty($userDetails['kyc_3_doctype_3'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $userDetails['kyc_3_doctype_3']['belongs_to'];
	$docDetails['doc_type'] = $userDetails['kyc_3_doctype_3']['doc_type'];
	$docDetails['path'] = $userDetails['kyc_3_doctype_3']['path'];
	$docDetails['status'] = $userDetails['kyc_3_doctype_3']['status'];
	$docDetails['comment'] = NULL;
	$doc3 = json_encode($docDetails);
}

if($_FILES['doc_file4']['error'] == 0 && isset($fileArr['doc_file4']['success'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $_POST['belongs_to'];
	$docDetails['doc_type'] = $_POST['doc_type'];
	$docDetails['path'] = $fileSuccArr['doc_file4'];
	$docDetails['status'] = 'pending';
	$docDetails['comment'] = NULL;
	$doc4 = json_encode($docDetails);
}elseif(!empty($userDetails['kyc_3_doctype_4'])){
	$docDetails = array();
	$docDetails['belongs_to'] = $userDetails['kyc_3_doctype_4']['belongs_to'];
	$docDetails['doc_type'] = $userDetails['kyc_3_doctype_4']['doc_type'];
	$docDetails['path'] = $userDetails['kyc_3_doctype_4']['path'];
	$docDetails['status'] = $userDetails['kyc_3_doctype_4']['status'];
	$docDetails['comment'] = NULL;
	$doc4 = json_encode($docDetails);
}


$updateDocs = updaetKYCLevelDocs($userId,$email,$doc1,$doc2,$doc3,$doc4);
if(noError($updateDocs)){
	$returnArr['errCode'] = -1;
	$returnArr['errMsg'] = "KYC documents has been updated successfully";
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "Failed to update KYC documents";
}

echo json_encode($updateDocs); 

?>
