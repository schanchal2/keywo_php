<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../config/country_codes.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);


$email = $_SESSION['email'];
$mobileNo = trim(cleanXSS(urldecode($_POST['mobile_no'])));
$countryCode = trim(cleanXSS($_POST['country_code']));
$action = trim(cleanXSS(urldecode($_POST['action'])));
$otp = trim(cleanXSS(urldecode($_POST['otp'])));
$userId = $_SESSION['id']; 
$returnArr = array();


//Search database connection
$searchConn = createDbConnection('dbsearch');

if (noError($searchConn)) {
    $searchConn = $searchConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

if($action == 'send_otp'){
	if(!empty($email) && !empty($mobileNo) && !empty($countryCode)){
		$mobile_to_check = $countryCode."-".$mobileNo;
		$isNumberExist = isAlreadyExistMobileNo($userId, $email,$mobile_to_check);
		if(noError($isNumberExist)){
			$isValidNo = checknumber(urldecode($countryCode),$mobileNo);
			if($isValidNo){
				$mobileNo = $countryCode.$mobileNo;
				$otpResult = generateOTP($searchConn,$userId,$mobileNo);

				if(noError($otpResult)){
					$returnArr['errCode'] = -1;
					$returnArr['errMsg'] = "Verification OTP has been sent sucessfully";
				}else{
					$returnArr['errCode'] = 3;
					$returnArr['errMsg'] = "Failed to send verification OTP";
				}
			}else{
				$returnArr['errCode'] = 4;
				$returnArr['errMsg'] = "Please enter valid country code and mobile no";
			}
		}else{
			$returnArr['errCode'] = 5;
			$errMsg = (trim($isNumberExist['errMsg']['failed'])== 'cell no already taken')?'Mobile number is already exist':'Please enter valid country code and mobile no';
			$returnArr['errMsg'] = $errMsg;
		}		

	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "All feilds are mandatory";
	}
}elseif($action == 'verify_otp'){

	if(!empty($email) && !empty($mobileNo) && !empty($countryCode) && !empty($otp)){

		$mobileNo1 = $countryCode.$mobileNo;
		$verifyOTP = verifyOTP($searchConn,$mobileNo1,$otp);
		if(noError($verifyOTP)){
			$mobile_no = $countryCode."-".$mobileNo;
			$updaetLevel2 = updateMobileKYCLevel1($userId, $email, $mobile_no);
			if(noError($updaetLevel2)){
				$updateKYCLevel = updateKYCLevelCount($userId, $email,2);
				if(noError($updateKYCLevel)){
					$updateKYCLevelNotify = setUserNotifyInfo($userId, 'kyc_level', 'kyc_2');
				
					if(noError($updateKYCLevelNotify)){
						
						$kycStablDetails = getKYCSlabByLevel($searchConn);
						if(noError($kycStablDetails)){
							$kycStablDetails = $kycStablDetails['errMsg'];
						}
						$userPendingInteraction = "no_of_qualified_interactions_pending";
						$getUserInteractionDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userPendingInteraction);

						if (noError($getUserInteractionDetails)) {
							/* User Pending Interaction Details */
							$UserPendinGinteractionCount = $getUserInteractionDetails['errMsg']['no_of_qualified_interactions_pending'];						
						}
						$usedInteractions = $kycStablDetails[0]['intraction'] - $UserPendinGinteractionCount;

						if($usedInteractions > 0){
							$toUpdateInteractions = $kycStablDetails[1]['intraction'] - $usedInteractions;
						}else{
							$toUpdateInteractions = $kycStablDetails[1]['intraction'];
						}
							
						 
						$updateKYCLevelNotify = setUserNotifyInfo($userId, 'qualified_interact_pending', $toUpdateInteractions);
						
						//send notification to ITD sender.  
						$email = $email;
						$mailSubject = sprintf($notificationEmails['kyc_level_2']['sub']);
						$emailBody = sprintf($notificationEmails['kyc_level_2']['body'], ucfirst($_SESSION['first_name']));
						$firstName = $_SESSION['first_name'];
						$lastName = $_SESSION['last_name'];
						$userId = $_SESSION['id'];
						$notificationBody = sprintf($notificationMessages['kyc_level_2']);
						$preferenceCode = 2;
						$category = "wallet";

						$res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
						$returnArr['errCode'] = -1;
						$returnArr['errMsg'] = "KYC level 2 has been updated successfully";
					}else{
						$returnArr['errCode'] = 4;
						$returnArr['errMsg'] = "Failed to upgrade KYC level 2";
					}

				}else{
					$returnArr['errCode'] = 4;
					$returnArr['errMsg'] = "Failed to upgrade KYC level 2";
				}
			}else{
				
				if(trim($updaetLevel2['errMsg']['failed']) == 'cell no already taken'){
					$returnArr['errCode'] = 10;
					$returnArr['errMsg'] = "Mobile number is already exist,Please try with different number";
				}else{
					$returnArr['errCode'] = 5;
					$returnArr['errMsg'] = "Failed to update mobile number";
				}				
			}

		}else{
			$returnArr = $verifyOTP;
		}
	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "All feilds are mandatory";
	}
}

echo json_encode($returnArr);
?>