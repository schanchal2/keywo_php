<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../config/country_codes.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);


$email = $_SESSION['email'];
$mobileNo = trim(cleanXSS(urldecode($_POST['mobile_no'])));
$countryCode = trim(cleanXSS(urldecode($_POST['country_code'])));
$action = trim(cleanXSS(urldecode($_POST['action'])));
$otp = trim(cleanXSS(urldecode($_POST['otp'])));
$userId = $_SESSION['id']; 
$returnArr = array();


//Keywords database connection
$searchConn = createDbConnection('dbsearch');

if (noError($searchConn)) {
    $searchConn = $searchConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

if($action == 'send_otp'){
	if(!empty($email) && !empty($mobileNo) && !empty($countryCode)){
		$mobileNo = $countryCode.$mobileNo;
		$otpResult = generateOTP($searchConn,$userId,$mobileNo);
		if(noError($otpResult)){
			$returnArr['errCode'] = -1;
			$returnArr['errMsg'] = "Verification OTP has been sent sucessfully";
		}else{
			$returnArr['errCode'] = 3;
			$returnArr['errMsg'] = "Failed to send verification OTP";
		}
	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "All feilds are mandatory";
	}
}elseif($action == 'verify_otp'){

	if(!empty($email) && !empty($mobileNo) && !empty($countryCode) && !empty($otp)){

		$mobileNo = $countryCode.$mobileNo;
		$verifyOTP = verifyOTP($searchConn,$mobileNo,$otp);
		if(noError($verifyOTP)){
			$returnArr = $verifyOTP; 
		}else{
			$returnArr = $verifyOTP;
		}
	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "All feilds are mandatory";
	}
}

echo json_encode($returnArr);

function verifyOTP($conn,$mobile,$otp){

	$returnArr = array();
	$query = "SELECT * FROM otp_gen WHERE  opt_number='".$otp."' AND mob='".$mobile."' AND status='Active'";
    $check = runQuery($query, $conn);
	$row = mysqli_fetch_array($check['dbResource']);
	if($row){
		$update = "UPDATE otp_gen SET status='Inactive' WHERE mob='".$mobile."'";
		$checkUpdate = runQuery($update, $conn);
		if(noError($checkUpdate)){
			$returnArr['errCode'] = -1;
			$returnArr['errMsg'] = "OTP has been successfully verified";
		}else{
			$returnArr['errCode'] = 3;
			$returnArr['errMsg'] = "Failed to verify OTP";
		}

	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Invalid OTP";
	}
	return $returnArr;
}

function generateOTP($conn,$user_id,$mobile){
	$returnArr = array();
	$otp = generateOTPnumber(6, false, 'd');

	$insert_otp = "INSERT INTO otp_gen (user_id,mob,opt_number,status) VALUES('$user_id','$mobile','$otp','Active')";
	$insert_succ = runQuery($insert_otp, $conn);

	if(noError($insert_succ)) {

		//send sms	
		$msg = "Dear User, Your OTP for verification is: ".$otp;
		$response = sendSMS($msg,$mobile); 
		$returnArr['errCode'] = -1;
		$returnArr['errMsg'] = "Otp has been sent to your number";
	}else{
		$returnArr['errCode'] = 3;
		$returnArr['errMsg'] = "Failed.Error in generating otp";
	}

	return $returnArr;

}


//printArr(json_decode($countyCodes,true));

?>