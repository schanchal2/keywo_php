<?php 
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/wallet2FAModel.php'); 
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
error_reporting(0);
$email = $_SESSION["email"];
$user_id = $_SESSION["id"];
$password = trim(urldecode($_POST['password']));
$returnArr = array();
$extraArgs = array();
$i= 1;

$logStorePath = $logPath["security"];

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "passwordAuth.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($email);
$msg = "Password verification start";
$xml_data['step'.$i]["data"] = $i.". {$msg}";


if(!empty($password) && !empty($email) && !empty($user_id)){
	$msg = "Success : Mandatory feilds found";
	$xml_data['step'.++$i]["data"] = $i.". {$msg}";
	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	$paramArray['emailHandle'] = $email;
	$paramArray['password'] = $password;
	$passVerification = verifyPassword($paramArray);
	if(noError($passVerification)){
		$msg = "Password has been verified successfully";
		$xml_data['step'.++$i]["data"] = $i.". Success: Password has been verified successfully";
		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
		$_SESSION['verify_status'] = "verified";
	}else{
		$msg = "Incorect password";
		$xml_data['step'.++$i]["data"] = $i.". Error: Incorect password";
		$returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);
	}
}else{
	$msg = "Mandatory feilds found";
	$xml_data['step'.++$i]["data"] = $i.". Error : Mandatory feilds Not found";
	$returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
}


$responseArr["errCode"] = $returnArr['errCode'];
$responseArr["errMsg"] = $returnArr['errMsg'];
$xml_data['response']["data"] = "";
$xml_data['response']["attributes"] = $responseArr;

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr); 
?>