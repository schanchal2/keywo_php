<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletUserModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$returnArr = array();
$currPref = cleanXSS(urldecode($_POST['curr_pref']));
$userId = $_SESSION["id"];

$result = updateCurrencyPref($userId,$currPref);
if(noError($result)){
	$_SESSION['CurrPreference'] = $currPref;
	$returnArr['errMsg'] = "Successfully updated currency preference";
	$returnArr['errCode'] = -1;
}else{
	$returnArr['errCode'] = "Failed to update currency preference";
	$returnArr['errMsg'] = 2;
}

if(noError($returnArr)){
  $_SESSION['succ_msg'] = $returnArr['errMsg'];
}else{
  $_SESSION['err_msg'] = $returnArr['errMsg'];  
}

header('Location:'.$rootUrl.'views/wallet/currencyPreference.php');
?>