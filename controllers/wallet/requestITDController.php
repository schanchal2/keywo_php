<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$emailHandle = trim(cleanXSS(urldecode($_POST['email_handle'])));
$amountInITD = cleanXSS(urldecode($_POST['amount_itd']));
$amountInUSD = cleanXSS(urldecode($_POST['amount_usd']));
$note = trim(cleanXSS(urldecode($_POST['note'])));
$userEmail = $_SESSION["email"];
$ipAddr = getClientIP();
$i = 1;

$logStorePath =$logPath["wallet"];


//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "requestITD.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);
$metaData = array();
$metaData['country'] = $xmlArray['activity']['country'];
$metaData['gender'] = $xmlArray['activity']['gender'];
$metaData['device'] = $xmlArray['activity']['device'];

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];


$msg = "Send IT$ process start.";
$xml_data['step'.$i]["data"] = $i.". {$msg}";
// create search database connection
$searchDbConn = createDBConnection("dbsearch");
if(noError($searchDbConn)){
    $searchDbConn = $searchDbConn["connection"];

    $msg = "Success : Search database connection";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    // get currency exchange rate of usd, sgd and btc
    $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
    if(noError($getExchangeRate)){
      $getExchangeRate = $getExchangeRate["exchange_rate"];
        $msg = "Success : Currency exchange rate";
        $xml_data['step'.++$i]["data"] = $i.". {$msg}";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
        $userRequiredFields = $userRequiredFields.",email,account_handle,user_id,active";

        //fetch details of sender and receiver                    
        $getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
        if(noError($getSendersDetails)){
          $senderDetails = $getSendersDetails['errMsg'];  
          $msg = "Success : Fetched sender details";
          $xml_data['step'.++$i]["data"] = $i.". {$msg}";
          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

          $getReciversDetails = (filter_var($emailHandle, FILTER_VALIDATE_EMAIL))?getUserInfo($emailHandle, $walletURLIP . 'api/v3/', $userRequiredFields):getUserInfoByHandle($emailHandle, $walletURLIPnotification . 'api/notify/v2/', $userRequiredFields);

          // $getReciversDetails = getUserInfo($emailHandle, $walletURLIP . 'api/v3/', $userRequiredFields);
          if(noError($getReciversDetails) && $getReciversDetails['errMsg']['active'] == 1){
            $reciversDetails = $getReciversDetails['errMsg'];
            $reciversDetails['_id'] = (isset($reciversDetails['user_id']))?$reciversDetails['user_id']:$reciversDetails['_id'];
            $msg = "Success : Fetched {$emailHandle} details";
            $xml_data['step'.++$i]["data"] = $i.". {$msg}";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs); 

            $response = sendITDRequestByWallet($senderDetails,$reciversDetails,$ipAddr,$amountInITD,$getExchangeRate,$note,$metaData);
            if(noError($response)){

              //send notification to request sender.  
              $email = $userEmail;
              $mailSubject = sprintf($notificationEmails['req_itd_sender']['sub']);
              $emailBody = sprintf($notificationEmails['req_itd_sender']['body'], $amountInITD.$keywoDefaultCurrencyName, $reciversDetails['email'], $reciversDetails['email']);
              $firstName = $senderDetails['first_name'];
              $lastName = $senderDetails['last_name'];
              $userId = $senderDetails['_id'];
              $notificationBody = sprintf($notificationMessages['req_itd_sender'], $reciversDetails['email'], $amountInITD, $keywoDefaultCurrencyName);
              $preferenceCode = 2;
              $category = "wallet";

              $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

              //send notification to request receiver.  
              $email = $reciversDetails['email'];
              $mailSubject = ' ';
              $emailBody = ' ';
              $firstName = $reciversDetails['first_name'];
              $lastName = $reciversDetails['last_name'];
              $userId = $reciversDetails['_id'];
              $notificationBody = sprintf($notificationMessages['req_itd_receiver'], $amountInITD, $keywoDefaultCurrencyName,$userEmail);
              $preferenceCode = '';
              $category = "wallet";
              
              $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

              $msg = "Request successfully sent";
              $xml_data['step'.++$i]["data"] = $i.". Success : Sent {$amountInITD} ".$keywoDefaultCurrencyName." request to {$emailHandle}";
              $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
            }else{
              if(trim($response['errMsg']['failed']) == "PR already availed"){
                $errMsg = "Already exists pending request";
              }else{
                $errMsg = "Failed to send {$amountInITD} ".$keywoDefaultCurrencyName." request to {$emailHandle}";
              }
              $xml_data['step'.++$i]["data"] = $i.". Error: sending {$amountInITD} ".$keywoDefaultCurrencyName." request to {$emailHandle}";
                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);   
            } 
          }else{
            if(noError($getReciversDetails) && $getReciversDetails['erMsg']['active'] != 1 ){
              $errMsg = "Inactive or blocked receiver";
              $xml_data['step'.++$i]["data"] = $i.". Error: Inactive or blocked receiver({$emailHandle})";  
            }else{
              $errMsg = "{$emailHandle} is not exists";
              $xml_data['step'.++$i]["data"] = $i.". Error: Fetching {$emailHandle} details";
            }

            $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs); 
          }
        }else{
          $errMsg = 'Not fount senders details';
          $xml_data['step'.++$i]["data"] = $i.". Error: Fetching sender details";
            $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs); 
        } 

    }else{
        $errMsg = 'Error: Fetching exchange rate';
        $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }   
    
}else{

  $errMsg = 'Error: unable to connect search database';
   $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
   $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

if(noError($returnArr)){
  $_SESSION['succ_msg'] = $returnArr['errMsg'];
}else{
  $_SESSION['err_msg'] = $returnArr['errMsg'];  
}
header('Location:'.$rootUrl.'views/wallet/requestITD.php');

?>