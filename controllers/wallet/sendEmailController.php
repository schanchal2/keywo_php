<?php
session_start();
//check for session
$returnArr = array();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    $returnArr['errCode'] = 100;
    $returnArr['errMsg'] = "Invalid Session";
    echo json_encode($returnArr); die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletCashoutModel.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$backup_codes = $_POST['backup_codes'];
$backup_codes = json_decode($backup_codes);
$content = "<br>Hi ".ucfirst($_SESSION['first_name']).",<br> Your backup codes for google authentication are: <br>";
$count = 1;
foreach ($backup_codes as $key => $value) {
	$content .= $count.") ".$value."<br>";
	$count++;
}


$email = $_SESSION["email"];
$mailSubject = 'Keywo: 2FA Backup codes';
$emailBody = $content;
$firstName = $_SESSION['first_name'];
$lastName = $_SESSION['last_name'];
$userId = $_SESSION['id'];
$notificationBody = $content.'<br>';
$preferenceCode = 2;
$category = "Admin";

$res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

if(noError($res)){
	$returnArr['errCode'] = -1;
	$returnArr['errMsg'] = "Email has been sent your registered email address";
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "Failed to send email address";
}

echo json_encode($returnArr);
?>