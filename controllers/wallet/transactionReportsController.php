<?php
$month 	= urldecode($_GET['month']);
$year 	= urldecode($_GET['year']);

session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);
$returnArr 	= array();
$userEmail 	= $_SESSION['email'];
$user_id 	= $_SESSION['id'];
$month 		= cleanXSS(urldecode($_GET['month']));
$year 		= cleanXSS(urldecode($_GET['year']));

if(!empty($month) && !empty($year) && !empty($userEmail)){
	$reportsDetails = getMonthlyTransctionRecords($userEmail, $user_id, $month, $year);

	if(noError($reportsDetails)){
		$reportsDetails = $reportsDetails['errMsg'];
		$transArr = array();
		$transArr[] = "Type";
		$transArr[] = "Amount";
		$transArr[] = "Description";
		$transArr[] = "Sender";
		$transArr[] = "Reciever";
		$transArr[] = "Status";
		$transArr[] = "Date";
		$transArr[] = "Payment Mode";
		$transArr[] = "Origin IP";

		$monthlyTransArr = array();
		$monthlyTransArr[] = $transArr;
		if(isset($reportsDetails['report']) || !empty($reportsDetails['report'])){
			foreach ($reportsDetails['report'] as $key => $transaction) {

				if($transaction['sender']== $userEmail){ 
	                $type = ($transaction['type'] == "transfer")? "Sent": $transaction['type'] ;                                	
	            	$senderDesc = sprintf($descriptionArray['sender_desc'], $transaction['amount'], $keywoDefaultCurrencyName, $transaction['receiver']);

	            	$description = $senderDesc; 
	            }elseif($transaction['sender'] != $userEmail){
	                $type = ($transaction['type'] == "transfer")? "Received": $transaction['type'] ;
	            	$receiverDesc = sprintf($descriptionArray['reciver_desc'], $transaction['amount'], $keywoDefaultCurrencyName, $transaction['sender']);
	            	$description = $receiverDesc; 
	            }else{
	            	$type = $transaction['type'];
	            } 


			 	$transArr = array();
				$transArr[] = ucfirst($type);
				$transArr[] = $transaction['amount']." ".$keywoDefaultCurrencyName;
				$transArr[] = $description;
				$transArr[] = $transaction['sender'];
				$transArr[] = $transaction['receiver'];
				$transArr[] = ucfirst($transaction['transaction_status']);
				$transArr[] = date("jS M, Y",$transaction['time']/1000);
				$transArr[] = ucfirst($transaction['payment_mode']);
				$transArr[] = $transaction['origin_ip'];

				$monthlyTransArr[] = $transArr;
			}
		}else{		
			$returnArr['errCode'] = 2;
			$returnArr['errMsg'] = "Error in fetching transaction details";
			$_SESSION['errMsg'] = "No relevant data found for this month";
			print("<script>");	
		    print("var t = setTimeout(\"window.location='../../views/wallet/index.php';\", 000);");
		    print("</script>");
		    die;
		}

	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Error in fetching transaction details";
		$_SESSION['errMsg'] = "No relevant data found for this month";
		print("<script>");	
	    print("var t = setTimeout(\"window.location='../../views/wallet/index.php';\", 000);");
	    print("</script>");
	    die;
	}

}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "Error in fetching transaction details";
	$_SESSION['errMsg'] = "No relevant data found for this month";
	print("<script>");	
    print("var t = setTimeout(\"window.location='../../views/wallet/index.php';\", 000);");
    print("</script>");
    die;
}

header("Content-Disposition: attachment; filename=\"".$month."_".$year."_transaction_report.xls\"");
header("Content-Type: application/vnd.ms-excel;");
header("Pragma: no-cache");
header("Expires: 0");

$out = fopen("php://output", 'w');
foreach ($monthlyTransArr as $data)
{
    fputcsv($out, $data,"\t");
}
fclose($out);

?>