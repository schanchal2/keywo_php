<?php
session_start();
$returnArr = array();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    $returnArr['errCode'] = 2;
    $returnArr['errMsg'] = "Invalid session";
    echo json_encode($returnArr); die;
}


// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletCashoutModel.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$status = cleanXSS(urldecode($_POST['status']));
$cashout_id = cleanXSS(urldecode($_POST['cashout_id']));
$request_date = cleanXSS(urldecode($_POST['request_date']));
$email = $_SESSION['email'];
$user_id = $_SESSION['id'];


$i = 1;
$xml_data = array();
$xmlArray = array();
$responseArr = array();
$logStorePath =$logPath["wallet"];

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "walletCancelCashoutRequest.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($email);

$msg = "Cancel cashout request process start";
$xml_data['step'.$i]["data"] = $i.". {$msg}";
$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

//connect to keywords database
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $msg = "Success: Connected to keywords database";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    $returnArr = array();
    $conn = $conn["connection"];
    $tableNmae = "queue_processor_".date("m_Y",strtotime($request_date));
    $query = "SELECT * FROM {$tableNmae} WHERE user_email='{$email}' AND id={$cashout_id} AND (status='pending' OR status='pending_manual_verification')";
    
    $queryResult =  runQuery($query, $conn);
    $resultArr =  mysqli_fetch_array($queryResult["dbResource"]);
    if(!empty($resultArr)){
        $msg = "Success: Cashout request is pending";
        $xml_data['step'.++$i]["data"] = $i.". {$msg}";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    	$query = "DELETE FROM {$tableNmae} WHERE user_email='{$email}' AND id={$cashout_id} AND (status='pending' OR status='pending_manual_verification')";
    	$queryResult = runQuery($query, $conn);
    	if(noError($queryResult)){
            $msg = "Success: Cashout request deleted from database";
            $xml_data['step'.++$i]["data"] = $i.". {$msg}";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	    	$totalITDamt = $resultArr['payment_amount'] + $resultArr['cashout_fees'];
	    	$pendingWithdrawal = addDeductBlockedPendingWithdrawal($totalITDamt,$user_id,$email,"deduct");
			if(noError($pendingWithdrawal)){
                $msg = "Cashout request has been removed successfully";
                $xml_data['step'.++$i]["data"] = $i.". Success: {$msg}";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
			}else{
                $msg = "Failed to fetch cashout request details";
                $xml_data['step'.++$i]["data"] = $i.". Error: {$msg}";
                $returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);
			}
    	}else{
            $msg = "Failed to fetch cashout request details";
            $xml_data['step'.++$i]["data"] = $i.". Error: {$msg}";
            $returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);
    	}

    }else{
        $msg = "Cashout request is already cancelled";
        $xml_data['step'.++$i]["data"] = $i.". Error: {$msg}";
        $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
    }

}else{
    $msg = "Failed to create keywords database connection";
    $xml_data['step'.++$i]["data"] = $i.". Error: {$msg}";
    $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
}

$responseArr["errCode"] = $returnArr['errCode'];
$responseArr["errMsg"] = $returnArr['errMsg'];
$xml_data['response']["data"] = "";
$xml_data['response']["attributes"] = $responseArr;

if(noError($returnArr)){
  $_SESSION['succ_msg'] = $returnArr['errMsg'];
}else{
  $_SESSION['err_msg'] = $returnArr['errMsg'];  
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);

?>