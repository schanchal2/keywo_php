<?php 
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../views/wallet/googleAuth/vendor/autoload.php');
error_reporting(0);
$auth_code = trim(cleanXSS(urldecode($_POST['auth_code'])));
$email = $_SESSION["email"];
$returnArr = array();
$extraArgs = array();
$tolerance = 0;
	$i = 1;
$authenticator = new PHPGangsta_GoogleAuthenticator();


$logStorePath = $logPath["security"];

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "googleAuth.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($email);
$msg = "Google authenticator verification start";
$xml_data['step'.$i]["data"] = $i.". {$msg}";

if(!empty($email) && !empty($auth_code)){
  $errMsg = "Success : Mandatory fields found";
  $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
  $secretKey = getGoogleSecretKey();
  if($secretKey){
	$errMsg = "Success : Fetched google auth secret key";
	$xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
	$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
	$secretKey = $secretKey;
	$checkResult = $authenticator->verifyCode($secretKey, $auth_code, $tolerance);    
 
	if($checkResult){
		$errMsg = "Google authentication has been successfull";
	  $xml_data['step'.++$i]["data"] = $i.". Success : Google authentication has been successfull";
	  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
	  $_SESSION['verify_status'] = "verified";
	}else{
		$errMsg = "Invalid google authentication code";
	  $xml_data['step'.++$i]["data"] = $i.". Error: Invalid google authentication code";
	  $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);
	}

  }else{
	  $errMsg = "Error : failed to fetch google security code";
	  $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
	  $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);  	
  }

}else{
  $errMsg = "Success : Mandatory fields found";
  $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
}

$responseArr["errCode"]= $returnArr['errCode'];
$responseArr["errMsg"]= $returnArr['errMsg'];
$xml_data['response']["data"] = "";
$xml_data['response']["attributes"] = $responseArr;

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);

function getGoogleSecretKey(){
	global $userRequiredFields,$walletURLIP;
	$email = $_SESSION['email'];

	$requiredFields = $userRequiredFields . ",security_preference,security_pref_2.gauth_key";
    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
	$securityKey = $getUserDetails['errMsg']['security_pref_2']['gauth_key'];
	return $securityKey;
}

?>