<?php
session_start();
require_once('../../config/config.php');
$returnArr = array();
$bitcoin_add = $_GET['bitcoin_add'];
$retArray = array();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    $returnArr['errCode'] = 100;
    $returnArr['errMsg'] = "Invalid Session";
    echo json_encode($returnArr); die;
}

if(isset($_GET['bitcoin_add'])){

	if($mode == "development"){
		$api_request_url = "http://tbtc.blockr.io/api/v1/address/info/".$bitcoin_add;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $api_request_url);
		$result = curl_exec($ch);
		$resultJson = json_decode($result,true);

		if($resultJson['data']['is_unknown'] || true){
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $resultJson;
			
		}else{
			$returnArr["errCode"] = 2;
			$returnArr["errMsg"] = "invalid withdrawal address";
		}
	}else{
		$api_request_url = "https://blockchain.info/address/".$bitcoin_add."?format=json";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, $api_request_url);
		$result = curl_exec($ch);
		$resultJson = json_decode($result,true);

		if($resultJson == ""){
			$returnArr["errCode"] = 2;
			$returnArr["errMsg"] = "invalid withdrawal address";
		}else{
			$returnArr["errCode"] = -1;
			$returnArr["errMsg"] = $resultJson;
		}
	}

	echo json_encode($returnArr); die;
}

?>