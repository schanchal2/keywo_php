<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}


// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletCashoutModel.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$amountInITD = cleanXSS(urldecode($_POST['valid-itd-amount']));
$amountInUSD = cleanXSS(urldecode($_POST['amount_usd']));
$cashoutMode = strtolower(cleanXSS(urldecode($_POST['method'])));

$userEmail = $_SESSION["email"];
$userId = $_SESSION["id"];
$ipAddr = getClientIP();
$returnArr = array();

$ipAddr = getClientIP();
$i = 1;

$logStorePath =$logPath["wallet"];


//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "walletCashoutRequest.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);

$metaData['country'] = $xmlArray['activity']['country'];
$metaData['gender'] = $xmlArray['activity']['gender'];
$metaData['device'] = $xmlArray['activity']['device'];

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

$xmlArray['activity']['validITDamount']=$amountInITD;
$xmlArray['activity']['amount_usd']=$amountInUSD;
$xmlArray['activity']['method']=$cashoutMode;

if(!isset($_SESSION['verify_status'])){

	$unique_id = md5(uniqid(rand(), true));
	$xmlArray['activity']['id'] = $unique_id;
	$_SESSION['activity_id'] = $unique_id;

	$_SESSION['redirect_url'] = $returnURl;

	$msg = "Add cashout request process start.";
	$xml_data['step'.$i]["data"] = $i.". {$msg}";

	//connect to search database
	$connSearch = createDbConnection('dbsearch');
	if (noError($connSearch)) {
	    $connSearch = $connSearch["connection"];
	    
	    $msg = "Success : Search database connection";
	    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
	    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	    // get currency exchange rate of usd, sgd and btc
		$exchangeRateArray = $exchageCurrencyCode;
		array_push($exchangeRateArray, strtoupper($_SESSION['CurrPreference']));
		$getExchangeRate = getCurrencyExchangeRate($exchangeRateArray, $connSearch);
		if (noError($getExchangeRate)) {
			$msg = "Success : Fetched currency rates";
		    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
		    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

		    $getExchangeRate = $getExchangeRate["exchange_rate"];
		    //$totalSendCount = getAccountTotalCount($userEmail,$user_id);

		    $kycStablDetails = getKYCSlabByLevel($connSearch);
		    if(noError($kycStablDetails)){
		        $kycStablDetails = $kycStablDetails['errMsg'];
		    }
		} else {
		    print("Error: Fetching current exchange rate.");
		    exit;
		}
	} else {
	    print("Error: Database connection");
	    exit;
	}

	//connect to keywords database
	$conn = createDBConnection("dbkeywords");
	if(noError($conn)){
	    $returnArr = array();
	    $conn = $conn["connection"];
		$msg = "Success : Keyword database connection";
	    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
	    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	    //get admin setting for withdrawal fees
	    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
	    if(noError($adminSettings)){

	    	$msg = "Success: Fetched admin setings data";
			$xml_data['step'.++$i]["data"] = $i.". {$msg}";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	    	$adminSettings = $adminSettings['data'];
	    	$withdrawalFees = $adminSettings['withdrawal_fees'];
	    	$minCashoutAmt = $adminSettings['minimum_withdrawal_amount'];
	    }else{
	    	print('Error in fetching admin settings');
	    	exit;
	    }
	}    

	$userRequiredFields = $userRequiredFields.",email,account_handle,user_id,kyc_current_level";

	//fetch details of sender and receiver                    
	$userDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
	if($userDetails){
		$userDetails = $userDetails['errMsg'];	
	}
	$userBal =  getUserBalance($userEmail);
	if($userBal['errMsg']['total_available_balance'] >= $amountInITD){
		$msg = "Success : User having sufficient available balance";
		$xml_data['step'.++$i]["data"] = $i.". {$msg}";
		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	
		$cashoutLimit = checkCashoutLimit($kycStablDetails,$userDetails['kyc_current_level'],$amountInITD,$userEmail,$userId);
		if(noError($cashoutLimit)){
			$msg = "Success : Checked daily and yearly cashout limit";
			$xml_data['step'.++$i]["data"] = $i.". {$msg}";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

			//fetch users wallet details
			$walletDetails = getWalletAddress($userEmail);
			if(noError($walletDetails)){

				$msg = "Success: Fetched users wallet address details";
				$xml_data['step'.++$i]["data"] = $i.". {$msg}";
				$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

				$walletDetails = $walletDetails['errMsg'];
				$mandatoryField = false;
				if($cashoutMode == 'paypal' && !empty($walletDetails['current_paypal_address'])){
					$mandatoryField = true;
				}else if($cashoutMode == 'bitcoin' && !empty($walletDetails['current_wallet_address'])){
					$mandatoryField = true;
				}else{
					$mandatoryField = false;
				}

				//check for mandatory feilds
				if($mandatoryField){
					$payoutAddress = ($cashoutMode == 'paypal')? $walletDetails['current_paypal_address'] : $walletDetails['current_wallet_address'];
					$msg = "Success: All mandatory parameters found";
					$xml_data['step'.++$i]["data"] = $i.". {$msg}";
					$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

					//get users available balance.
					$usersAvailableBalance = getUserBalance($userEmail);

					if(noError($usersAvailableBalance)){

						$msg = "Success: Calculated user balance";
						$xml_data['step'.++$i]["data"] = $i.". {$msg}";
						$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

						$usersAvailableBalance = $usersAvailableBalance['errMsg']['total_available_balance']; 	
						if(isset($amountInITD) && !empty($amountInITD) && isset($userEmail) && !empty($userEmail)){


							$msg = "Success: Checked cashout amount and users email address";
							$xml_data['step'.++$i]["data"] = $i.". {$msg}";
							$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);	

							//check cashout request amount is valid or not
							if($amountInITD <= $usersAvailableBalance && $amountInITD > $minCashoutAmt){

								$msg = "Success: Valid cashout amount";
								$xml_data['step'.++$i]["data"] = $i.". {$msg}";
								$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

								//redirecting to 2FA page
								$msg = "Success : Redirecting to 2FA page";
								$xml_data['step'.++$i]["data"] = $i.". {$msg}";
								$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
								$_SESSION['form_data'] = json_encode($_POST);
								$_SESSION['xml_step'] = $i;

								$_SESSION['xml_file'] = $logStorePath . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;
								

							}else{

								$msg = "Invalid cashout amount";
								$xml_data['step'.++$i]["data"] = $i.". Error: Insufficient user available balance";
								$returnArr = setErrorStack($returnArr, 5, $msg, $extraArgs);

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=5;
                                $responseArr["errMsg"]=$msg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/


                            }
						}else{
							$msg = "Failed to fetch mandatory feilds";
							$xml_data['step'.++$i]["data"] = $i.". Error: Failed to fetch mandatory feilds";
							$returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=4;
                            $responseArr["errMsg"]=$msg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

                        }
					}else{

					$msg = "Faild to calculate users available balance.";
					$xml_data['step'.++$i]["data"] = $i.". Error: Calculating users available balance";
					$returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=4;
                        $responseArr["errMsg"]=$msg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/

                    }

				}else{

					$msg = "Not found mandatory feilds";
					$xml_data['step'.++$i]["data"] = $i.". Error: Not found mandatory feilds";
					$returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=3;
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/


                }
			}else{

				$msg = "Failed to fetch users wallet address details";
				$xml_data['step'.++$i]["data"] = $i.". Error: Fetching users wallet address details";
				$returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$msg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/


            }
		}else{
			$errMsg = $cashoutLimit['errMsg'];
			$xml_data['step'.++$i]["data"] = $i.". Error: ".$cashoutLimit['errMsg'];
			$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=5;
            $responseArr["errMsg"]=$msg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/

        }
	}else{
		$errMsg = "Your available balance is insufficient for this transaction";
		$xml_data['step'.++$i]["data"] = $i.". Error: ".$errMsg;
		$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=5;
        $responseArr["errMsg"]=$msg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/

    }


	// create or update xml log Files
	$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);
	if(noError($returnArr)){
		// $_SESSION['succ_msg'] = $returnArr['errMsg'];
		header('Location:'.$rootUrl.'views/two_factor_auth/index.php'); die;
	}else{
		$_SESSION['err_msg'] = $returnArr['errMsg'];
		header('Location:'.$rootUrl.'views/wallet/cashout.php'); die; 
	}

}else if($returnURl == $_SESSION['redirect_url']){

	$i = $_SESSION['xml_step'];
	$security_type = $_SESSION['security_type'];
	$form_data = json_decode($_SESSION['form_data'],true);
	$activity_id = $_SESSION['activity_id'];

	$amountInITD = cleanXSS(urldecode($form_data['valid-itd-amount']));
	$amountInUSD = cleanXSS(urldecode($form_data['amount_usd']));
	$cashoutMode = strtolower(cleanXSS(urldecode($form_data['method'])));

	$xml_file = $_SESSION['xml_file'];

	unset($_SESSION['security_type']);
	unset($_SESSION['xml_step']);
	unset($_SESSION['form_data']);
	unset($_SESSION['activity_id']);
	unset($_SESSION['redirect_url']);
	unset($_SESSION['xml_file']);
	unset($_SESSION['verify_status']);

	$msg = "Success : Callback from 2FA of {$security_type}";
	$xml_data['step'.++$i]["data"] = $i.". {$msg}";
	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	//connect to search database
	$connSearch = createDbConnection('dbsearch');
	if (noError($connSearch)) {
	    $connSearch = $connSearch["connection"];
	    
	    $msg = "Success : Search database connection";
	    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	    // get currency exchange rate of usd, sgd and btc
		$exchangeRateArray = $exchageCurrencyCode;
		array_push($exchangeRateArray, strtoupper($_SESSION['CurrPreference']));
		$getExchangeRate = getCurrencyExchangeRate($exchangeRateArray, $connSearch);
		if (noError($getExchangeRate)) {
			$msg = "Success : Fetched currency rates";
		    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

		    $getExchangeRate = $getExchangeRate["exchange_rate"];
		    //$totalSendCount = getAccountTotalCount($userEmail,$user_id);

		    $kycStablDetails = getKYCSlabByLevel($connSearch);
		    if(noError($kycStablDetails)){
		        $kycStablDetails = $kycStablDetails['errMsg'];
		    }
		} else {
		    print("Error: Fetching current exchange rate.");
		    exit;
		}
	} else {
	    print("Error: Database connection");
	    exit;
	}

	//connect to keywords database
	$conn = createDBConnection("dbkeywords");
	if(noError($conn)){
	    $returnArr = array();
	    $conn = $conn["connection"];
		$msg = "Success : Keyword database connection";
	    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	    //get admin setting for withdrawal fees
	    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
	    if(noError($adminSettings)){

	    	$msg = "Success: Fetched admin setings data";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
	    	$adminSettings = $adminSettings['data'];
	    	$withdrawalFees = $adminSettings['withdrawal_fees'];
	    }else{
	    	print('Error in fetching admin settings');
	    	exit;
	    }
	}    

	$userRequiredFields = $userRequiredFields.",email,account_handle,user_id,kyc_current_level";

	//fetch details of sender and receiver                    
	$userDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
	if($userDetails){
		$userDetails = $userDetails['errMsg'];	
	}

	$cashoutLimit = checkCashoutLimit($kycStablDetails,$userDetails['kyc_current_level'],$amountInITD,$userEmail,$userId);
	if(noError($cashoutLimit)){
		$msg = "Success : Checked daily and yearly cashout limit";
	    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

		//fetch users wallet details
		$walletDetails = getWalletAddress($userEmail);
		if(noError($walletDetails)){

			$msg = "Success: Fetched users wallet address details";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

		    $walletDetails = $walletDetails['errMsg'];
		    $mandatoryField = false;
			if($cashoutMode == 'paypal' && !empty($walletDetails['current_paypal_address'])){
				$mandatoryField = true;
			}else if($cashoutMode == 'bitcoin' && !empty($walletDetails['current_wallet_address'])){
				$mandatoryField = true;
			}else{
				$mandatoryField = false;
			}

		    //check for mandatory feilds
			if($mandatoryField){
				$payoutAddress = ($cashoutMode == 'paypal')? $walletDetails['current_paypal_address'] : $walletDetails['current_wallet_address'];
				$msg = "Success: All mandatory parameters found";
				$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

				//get users available balance.
				$usersAvailableBalance = getUserBalance($userEmail);

				if(noError($usersAvailableBalance)){

					$msg = "Success: Calculated user balance";
					$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

					$usersAvailableBalance = $usersAvailableBalance['errMsg']['total_available_balance']; 	
					if(isset($amountInITD) && !empty($amountInITD) && isset($userEmail) && !empty($userEmail)){


						$msg = "Success: Checked cashout amount and users email address";
						$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);	

						//check cashout request amount is valid or not
						if($amountInITD <= $usersAvailableBalance && $amountInITD > $minCashoutAmt){

							$msg = "Success: Valid cashout amount";
							$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

							$userRequiredFields = $userRequiredFields.",email,account_handle";

							$currentTable = "queue_processor_".date('m_Y');
							//$currentTable = "queue_processor_05_2017";
							$currentDate = date("Y-m-d H:i:s");
							$currencyRatesJson = json_encode($getExchangeRate);

							$status = ($cashoutMode == 'paypal')?"pending_manual_verification":"pending";
							$deductedAmountInITD = $amountInITD - $withdrawalFees;

							//check for cashout queue processor table exist or not 
							$tableQuery = "SELECT 1 FROM {$currentTable} LIMIT 1";
							$tableResult = runQuery($tableQuery, $conn);
							if(noError($tableResult)){
								$tableExist = 'Yes';
							}else{
								$tableStructure =	"CREATE TABLE IF NOT EXISTS `{$currentTable}` (
													  `id` int(255) NOT NULL AUTO_INCREMENT,
													  `user_id` varchar(255) NOT NULL,
													  `user_email` varchar(255) NOT NULL,
													  `payment_amount` varchar(255) NOT NULL,
													  `cashout_fees` varchar(255) DEFAULT NULL,
													  `payment_mode` varchar(255) NOT NULL,
													  `payout_address` VARCHAR(255) DEFAULT NULL,
													  `node_trans_id` varchar(255) DEFAULT NULL,
													  `request_date` datetime NOT NULL,
													  `request_update_date` datetime DEFAULT NULL,
													  `currency_rates` varchar(255) NOT NULL,
													  `status` varchar(255) NOT NULL,
													  `comment` text,
													  `hook_invoked` tinyint(1) DEFAULT '0',
													  `origin_ip` varchar(255) NOT NULL,
													  `assigned_to` varchar(255) NOT NULL,
													  `actioned_by` varchar(255) NOT NULL,
													  `meta_details` text,
													  PRIMARY KEY (`id`)
													) ENGINE=InnoDB DEFAULT CHARSET=latin1";

								$result = runQuery($tableStructure,$conn);
								if(noError($result)){
									$tableExist = 'Yes';
								}else{
									$tableExist = 'No';
								}
							}
							//if cashout table queue processor exist then add request in table
							if($tableExist == 'Yes'){

								$msg = "Success: Cashout queue processor table is exist";
								$xml_data['step'.++$i]["data"] = $i.". {$msg}";
								$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
								$metaData = json_encode($metaData);
								/* set autocommit to off */
								//mysqli_autocommit($conn, FALSE);

                                /********************************************* for auto assigne cashot to agents ******************************************/

                               if($status=="pending_manual_verification") {
                                   $connAdmin = createDBConnection("acl");
                                   noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

                                   $resultAdmin = selectAdminToAssign($connAdmin);
                                   $resultAdmin = $resultAdmin["errMsg"][0];
                                   $admin_email = $resultAdmin["admin_email"];

                                   $updateAdminCount = updateAdminAssignedCount($admin_email, $connAdmin);
                                   $status="pending_for_uam_approval";
                               }else
                               {
                                   $admin_email="";
                               }
                                /********************************************* for auto assigne cashot to agents ******************************************/


                                $query = "INSERT INTO {$currentTable} (user_id,user_email,payment_amount,cashout_fees,payment_mode,request_date,currency_rates,status,payout_address,origin_ip,meta_details,assigned_to) VALUE('{$userId}','{$userEmail}','{$deductedAmountInITD}','{$withdrawalFees}','{$cashoutMode}','{$currentDate}','{$currencyRatesJson}','{$status}','{$payoutAddress}','{$ipAddr}','{$metaData}','{$admin_email}')";
								$resultQuery = runQuery($query, $conn);
								//echo $query; 
								//printArr($resultQuery);die; 
								if($resultQuery){

									
									$msg = "Success: Added cashout request";
									$xml_data['step'.++$i]["data"] = $i.". {$msg}";
									$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

									$pendingWithdrawal = addDeductBlockedPendingWithdrawal($amountInITD,$userId,$userEmail,"add");
									if(noError($pendingWithdrawal)){

										$msg = "Cashout request has been submitted successfully";
										$xml_data['step'.++$i]["data"] = $i.". Success: Cashout request has been submitted successfully";
										$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

										/* commit transaction */
										/*if (!mysqli_commit($conn)) {
										    print("Transaction commit failed\n");
										    exit();
										}*/

										//send notification to ITD sender.  
										$email = $userEmail;
										$mailSubject = 'Keywo: Withdrawal Request';
										$emailBody = "<br><br>Hi ".$_SESSION['first_name']."<br> You have successfully submitted a withdrawal request of ".$amountInITD." ".$keywoDefaultCurrencyName."<br><br>";
										$firstName = $_SESSION['first_name'];
										$lastName = $_SESSION['last_name'];
										$userId = $_SESSION['id'];
										$notificationBody = "You have successfully submitted a withdrawal request of ".$amountInITD." ".$keywoDefaultCurrencyName;
										$preferenceCode = 2;
										$category = "wallet";

										$res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=-1;
                                        $responseArr["errMsg"]=$msg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/

									}else{

										$msg = "Failed to add cashout amount in pending withdrawal";
										$xml_data['step'.++$i]["data"] = $i.". Error: Failed to add cashout amount in pending withdrawal";
										$returnArr = setErrorStack($returnArr, 5, $msg, $extraArgs);


                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=5;
                                        $responseArr["errMsg"]=$msg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/


                                    }

									
								}else{
									$msg = "Faild to insert cashout request";
									$xml_data['step'.++$i]["data"] = $i.". Error: Faild to insert cashout request";
									$returnArr = setErrorStack($returnArr, 7, $msg, $extraArgs);

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]=7;
                                    $responseArr["errMsg"]=$msg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/

                                }
							}else{

								$msg = "Cashout queue processor table is not exist";
								$xml_data['step'.++$i]["data"] = $i.". Error: Cashout queue processor table is not exist";
								$returnArr = setErrorStack($returnArr, 6, $msg, $extraArgs);

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=6;
                                $responseArr["errMsg"]=$msg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }
							

						}else{

							$msg = "Invalid cashout amount";
							$xml_data['step'.++$i]["data"] = $i.". Error: Insufficient user available balance";
							$returnArr = setErrorStack($returnArr, 5, $msg, $extraArgs);

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=5;
                            $responseArr["errMsg"]=$msg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/


                        }
					}else{
						$msg = "Failed to fetch mandatory feilds";
						$returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=4;
                        $responseArr["errMsg"]=$msg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/

                    }
				}else{

				$msg = "Faild to calculate users available balance.";
				$returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=4;
                    $responseArr["errMsg"]=$msg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/

                }

			}else{

				$msg = "Not found mandatory feilds";
				$returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=3;
                $responseArr["errMsg"]=$msg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/


            }
		}else{

			$msg = "Failed to fetch users wallet address details";
			$returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$msg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/


        }
	}else{
		$errMsg = $cashoutLimit['errMsg'];
		$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=5;
        $responseArr["errMsg"]=$msg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/

    }

	// create or update xml log Files
	$xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);
	if(noError($returnArr)){
		$_SESSION['succ_msg'] = $returnArr['errMsg'];
	}else{
		$_SESSION['err_msg'] = $returnArr['errMsg'];  
	}
	header('Location:'.$rootUrl.'views/wallet/cashout.php');

}else{
  unset($_SESSION['security_type']);
  unset($_SESSION['xml_step']);
  unset($_SESSION['form_data']);
  unset($_SESSION['activity_id']);
  unset($_SESSION['redirect_url']);
  unset($_SESSION['xml_file']);
  unset($_SESSION['verify_status']);
  $_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";  
  header('Location:'.$rootUrl.'views/wallet/cashout.php');
}


?>