<?php 
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/wallet2FAModel.php'); 
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../views/wallet/googleAuth/vendor/autoload.php');
error_reporting(0);
$preference = $_POST['preference'];
$returnArr = array();
$unique_id = md5(uniqid(rand(), true));
$xmlArray['activity']['id'] = $unique_id;
$_SESSION['activity_id'] = $unique_id;
if($preference == 1){
	$returnURl = $rootUrl."controllers/wallet/setPassowordAuthController.php";
	$_SESSION['redirect_url'] = $returnURl;
}else if($preference == 2){
	$returnURl = $rootUrl."views/wallet/setGoogleAuth.php";
	$_SESSION['redirect_url'] = $returnURl;
}else if($preference == 3){
	$returnURl = $rootUrl."views/wallet/setTransactionPinSecurity.php";
	$_SESSION['redirect_url'] = $returnURl;
}

$returnArr['errMsg'] = "Success";
$returnArr['errCode'] = -1;
echo json_encode($returnArr);
?>