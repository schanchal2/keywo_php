<?php

$docrootpath=explode("controllers/",dirname(__FILE__))[0];


// include require files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/geoTrendHelper.php");
require_once("{$docrootpath}helpers/walletHelper.php");
require_once("{$docrootpath}models/cdp/cdpUtilities.php");
require_once("{$docrootpath}models/wallet/walletModel.php");
require_once("{$docrootpath}models/wallet/walletCashoutModel.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");

$returnArr = array();
//xml log path
$logStorePath = $docrootpath."logs/wallet/";
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "cashoutErrorHandlerCron.xml";
$i = 1;
$xmlArray = initializeXMLLog('');
$xml_data['request']["data"]='';
$xml_data['request']["attribute"]= $xmlArray["request"];

$conn = createDBConnection("dbkeywords");
if(noError($conn)){

	$msg = "Success : Created database connection";
	$xml_data['step'.$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    $conn = $conn["connection"];

    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);
    if(noError($adminSettings)){

    	$msg = "Success: Fetched admin setings data";
		$xml_data['step'.++$i]["data"] = $i.". {$msg}";
		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
    	$adminSettings = $adminSettings['data'];
    	$withdrawalFees = $adminSettings['withdrawal_fees'];
    	$minCashoutAmt = $adminSettings['minimum_withdrawal_amount'];
    	$cashoutAutoLimit = $adminSettings['cashout_automode_amount_limit'];
    }else{
    	print('Error in fetching admin settings');
    	exit;
    }

    $cashoutRequest = getRequestsForErrorHandler($conn);
    if(noError($cashoutRequest)){
    	if(!empty($cashoutRequest['errMsg'])){
    		$msg = "Success : Fetchecd cashout requests";
	    	$xml_data['step'.++$i]["data"] = $i.". {$msg}";
	    	$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	    	$cashoutRequest = $cashoutRequest['errMsg'];
	    	$requestCount = 1; 
	    	$requestXML = array();

	    	foreach ($cashoutRequest as $key => $value) {  

	    		$requestProcess = processRequest($conn,$value,$returnArr,$requestCount,$withdrawalFees,$minCashoutAmt,$cashoutAutoLimit);
	    		if(noError($requestProcess)){
	    			$returnArr = $requestProcess;
	    			$requestXML['response'][] ='success';
	    			echo"Successfully processed cashout request";
	    		}else{
	    			echo"Error in processing cashout request";
	    			$requestXML['response'][] ='error';
	    			$returnArr = $requestProcess;
	    		}
	    		$requestXML['xml_data']['request'.$requestCount] = $requestProcess['xml_data']['request'.$requestCount];   		   	
	    		$requestCount = $requestCount + 1;
	    	}

	    	$xml_data = $requestXML['xml_data'];
	    	$response = array_search("error",$requestXML['response']);
	    	if($response){
	    		$responseattribute["errCode"]="2";
				$responseattribute["errMsg"]="failure";
				$xml_data['response']["data"] = '';
				$xml_data['response']["attribute"] =$responseattribute;
	    	}else{
	    		$responseattribute["errCode"]="-1";
				$responseattribute["errMsg"]="success";
				$xml_data['response']["data"] = '';
				$xml_data['response']["attribute"] =$responseattribute;
	    	}

    	}else{
    		echo "Not found cash out request.";
    		$msg = "Success : Not found any cash out request";
    		$xml_data['step'.++$i]["data"] = $i.". {$msg}";
    		$responseattribute["errCode"]="-1";
			$responseattribute["errMsg"]="success";
			$xml_data['response']["data"] = '';
			$xml_data['response']["attribute"] =$responseattribute;
    		$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
    	}
    }else{
    	$msg = "Error : Fetching cashout requests";
    	$xml_data['step'.++$i]["data"] = $i.". {$msg}";
    	$returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);
    }


}else{
	$msg = "Error : Creating database connection";
	$xml_data['step'.$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);
}    

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);
?>