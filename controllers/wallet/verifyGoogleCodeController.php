<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/wallet2FAModel.php');

require '../../views/wallet/googleAuth/vendor/autoload.php';
error_reporting(0);
$returnArr = array();
$authenticator = new PHPGangsta_GoogleAuthenticator();
$secretKey = $_SESSION['google_secret_key'];
$tolerance = 0;
$otp = $_POST['google_code'];
$email = $_SESSION['email'];

$checkResult = $authenticator->verifyCode($secretKey, $otp, $tolerance);    
 
if($checkResult){
	$securityPreference = 2;
	$response = setSecurityPreference($secretKey,$securityPreference,$email);
	if(noError($response)){
		$_SESSION['security_preference'] = 2;
	    $returnArr['errCode'] = -1;
	    $returnArr['errMsg'] = "Successfully verified google authentication code";
	}else{
	    $returnArr['errCode'] = 3;
	    $returnArr['errMsg'] = "Failed to set google authentication";
	}
     
}else{
    $returnArr['errCode'] = 2;
    $returnArr['errMsg'] = "Invalid Google Authenticate Code";
}

echo json_encode($returnArr);
?>