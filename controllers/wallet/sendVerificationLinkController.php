<?php
session_start();
$retArray = array();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    $retArray['errCode'] = 100;
    $retArray['errMsg'] = "Invalid Session";
    echo json_encode($retArray); die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$email = $_SESSION["email"];
$wallet_address = $_POST['address'];
$cashoutMode = trim(urldecode($_POST['mode']));


$isDuplicate = checkforDuplicateAddress($cashoutMode,$wallet_address);

if(noError($isDuplicate)){
  $conn = createDBConnection("dbkeywords");
  if(noError($conn)){
      $conn = $conn["connection"];

      $type = "wallet_address_verif_email";
      $mode = "email";
      $payload = $wallet_address;
      $status = "unverified";
      $currentTime =  date("Y-m-d H:i:s");
      $date = strtotime($currentTime) + 3600;
      $expirytime = date("Y-m-d H:i:s",$date);
      $authToken = sha1($email."~~".$currentTime);
      $checkAuthEmail = checkAuthEmail($email,$payload,$type,$conn);

      if(noError($checkAuthEmail) || true) {
		$deactive = deactivePreviousMail($email,$payload,$type,$conn);  
        $emailDetails = insertauthEmailDetails($conn, $email, $payload, $authToken, $type, $mode, $status);

        if(noError($emailDetails)){

			$userRequiredFields = $userRequiredFields.",email,account_handle";

			//fetch details of sender and receiver                    
			$getSendersDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
			if($cashoutMode == 'paypal_email'){
			$mailSubject = 'Verify Paypal Address';
			$emailBody = '<br>Dear User,<br> Please click on the link to verify your paypal email address <a href="'.$rootUrl.'views/wallet/cashoutSettings.php?payload='.$payload.'&email='.$email.'&auth='.$authToken.'&mode=paypal">Click Here</a><br><br>';
			}else{
			$mailSubject = 'Verify Bitcoin Address';
			$emailBody = '<br>Dear User,<br> Please click on the link to verify your bitcoin wallet address <a href="'.$rootUrl.'views/wallet/cashoutSettings.php?payload='.$payload.'&email='.$email.'&auth='.$authToken.'&mode=bitcoin">Click Here</a><br><br>';
			}

			$firstName = $getSendersDetails['errMsg']['first_name'];
			$lastName = $getSendersDetails['errMsg']['last_name'];
			$userId = $getSendersDetails['errMsg']['_id'];
			$notificationBody = "Verification email has been sent on you authorized email address";
			$preferenceCode = 2;
			$category = "Admin";

			$sendAuthoriseMail = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
          
			if (noError($sendAuthoriseMail)) {
				$retArray["errCode"] = -1;
				$retArray["errMsg"] = "Successfully sent authorisation email";
			} else {
				$retArray["errCode"] = 2;
				$retArray["errMsg"] = "Failed to send authorisation email";
			}
        } else {
          $retArray["errCode"] = 2;
          $retArray["errMsg"] = $emailDetails["errMsg"];
        }
      }else{
        $retArray["errCode"] = 5;
        $retArray["errMsg"] = $checkAuthEmail["errMsg"];
      }


  }
}else{
  $retArray["errCode"] = 7;
  $retArray["errMsg"] = "Address already in use";
}

echo json_encode($retArray); die;

?>