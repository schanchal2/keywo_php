<?php 
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
if(!isset($_SESSION['redirect_url'])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/wallet/twoFASettings.php';\", 000);");
    print("</script>");
    die; 
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/wallet2FAModel.php'); 
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../views/wallet/googleAuth/vendor/autoload.php');
error_reporting(0);
$email = $_SESSION["email"];
$user_id = $_SESSION["id"];
unset($_SESSION['redirect_url']);
$returnArr = array();
if(!empty($email) && !empty($user_id)){
	$passwordPref = setPasswordSecurityPreferene($email,$user_id);
	if(noError($passwordPref)){
		$_SESSION['security_preference'] = 1;
		$returnArr['errCode'] = -1;
		$returnArr['errMsg'] = "Password security preference has been set successfully";
	}else{
		$returnArr['errCode'] = 2;
		$returnArr['errMsg'] = "Failed to set password security preference";
	}
}else{
	$returnArr['errCode'] = 2;
	$returnArr['errMsg'] = "Mandatory feilds not found";
}


print("<script>");
print("var t = setTimeout(\"window.location='../../views/wallet/twoFASettings.php';\", 000);");
print("</script>");
die;
?>