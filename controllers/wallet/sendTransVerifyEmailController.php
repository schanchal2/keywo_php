<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$retArray = array();

$email = $_SESSION["email"];
$trans_pin = urldecode($_POST['trans_pin']);
$verification_path = urldecode($_POST['verification_path']);

if(!empty($trans_pin) && !empty($verification_path)){

  $conn = createDBConnection("dbkeywords");
  if(noError($conn)){
      $conn = $conn["connection"];
      $type = "transaction_pin_verify_email";
      $mode = "email";
      $payload = $trans_pin;
      $status = "unverified";
      $currentTime =  date("Y-m-d H:i:s");
      $date = strtotime($currentTime) + 3600;
      $expirytime = date("Y-m-d H:i:s",$date);
      $authToken = sha1($email."~~".$currentTime);
      $checkAuthEmail = checkAuthEmail($email,$payload,$type,$conn);

      if(noError($checkAuthEmail) || true) {
		$deactive = deactivePreviousMail($email,$payload,$type,$conn);    
        $emailDetails = insertauthEmailDetails($conn, $email, $payload, $authToken, $type, $mode, $status);

        if(noError($emailDetails)){

        	$userRequiredFields = $userRequiredFields.",email,account_handle";

      		//fetch details of sender and receiver                    
      		$getSendersDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
      		
    			$mailSubject = 'Keywo: Verify Transaction Pin';
    			$emailBody = '<br>Dear User,<br> Please click on the link to set transaction pin as security preference <a href="'.$verification_path.'?email='.$email.'&auth='.$authToken.'">Click Here</a><br><br>';
      		
      		$firstName = $getSendersDetails['errMsg']['first_name'];
      		$lastName = $getSendersDetails['errMsg']['last_name'];
      		$userId = $getSendersDetails['errMsg']['_id'];
      		$notificationBody = "Transaction pin verification email has been sent on your authorized email address";
      		$preferenceCode = 2;
      		$category = "Admin";

  		    $sendAuthoriseMail = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
          
          if (noError($sendAuthoriseMail)) {
            $retArray["errCode"] = -1;
            $retArray["errMsg"] = "Successfully sent authorisation email";
          } else {
            $retArray["errCode"] = 2;
            $retArray["errMsg"] = "Failed to send authorisation email";
          }
        } else {
          $retArray["errCode"] = 2;
          $retArray["errMsg"] = $emailDetails["errMsg"];
        }
      }else{
        $retArray["errCode"] = 5;
        $retArray["errMsg"] = $checkAuthEmail["errMsg"];
      }


  }
}else{
  $retArray["errCode"] = 3;
  $retArray["errMsg"] = "All feilds are mandatory";
}

echo json_encode($retArray); die;

?>