<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$emailHandle = trim(cleanXSS(urldecode($_POST['email_handle'])));
$amountInITD = cleanXSS(urldecode($_POST['amount_itd']));
$amountInUSD = cleanXSS(urldecode($_POST['amount_usd']));
$note = trim(cleanXSS(urldecode($_POST['note'])));
$userEmail = $_SESSION["email"];
$user_id = $_SESSION["id"];
$ipAddr = getClientIP();
$metaData = array();
$i = 1;

$logStorePath =$logPath["wallet"];


//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "walletSendITD.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);

$metaData['country'] = $xmlArray['activity']['country'];
$metaData['gender'] = $xmlArray['activity']['gender'];
$metaData['device'] = $xmlArray['activity']['device'];


$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if(!isset($_SESSION['verify_status'])){

  $unique_id = md5(uniqid(rand(), true));
  $xmlArray['activity']['id'] = $unique_id;
  $_SESSION['activity_id'] = $unique_id;

 $_SESSION['redirect_url'] = $returnURl;
  $msg = "Send IT$ process start.";
  $xml_data['step'.$i]["data"] = $i.". {$msg}";

  if(!empty($emailHandle) && !empty($amountInITD)){

    $msg = "Success : Mandatory feilds found";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

	$userBal =  getUserBalance($userEmail);
	if($userBal['errMsg']['total_available_balance'] >= $amountInITD){
		// create search database connection
		$searchDbConn = createDBConnection("dbsearch");
		if(noError($searchDbConn)){
			$searchDbConn = $searchDbConn["connection"];

			$msg = "Success : Search database connection";
			$xml_data['step'.++$i]["data"] = $i.". {$msg}";
			$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);


			//$totalSendCount = getAccountTotalCount($userEmail,$user_id);
			$kycStablDetails = getKYCSlabByLevel($searchDbConn);
			if(noError($kycStablDetails)){
				$kycStablDetails = $kycStablDetails['errMsg'];
			}

			// get currency exchange rate of usd, sgd and btc
			$getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
			if(noError($getExchangeRate)){
			  $getExchangeRate = $getExchangeRate["exchange_rate"];
				$msg = "Success : Currency exchange rate";
				$xml_data['step'.++$i]["data"] = $i.". {$msg}";
				$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
				$userRequiredFields = $userRequiredFields.",email,account_handle,user_id,kyc_current_level,active";

				//fetch details of sender and receiver                    
				$getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);

				if(noError($getSendersDetails)){
				  $senderDetails = $getSendersDetails['errMsg'];  
				  $msg = "Success : Fetched sender details";
				  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
				  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

				  $sendLimit = checkSendLimit($kycStablDetails,$senderDetails['kyc_current_level'],$amountInITD,$userEmail,$user_id);
				  if(noError($sendLimit)){
					$msg = "Success : Checked daily and yearly send limit";
					$xml_data['step'.++$i]["data"] = $i.". {$msg}";
					$returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
				  
					$getReciversDetails = (filter_var($emailHandle, FILTER_VALIDATE_EMAIL))?getUserInfo($emailHandle, $walletURLIP . 'api/v3/', $userRequiredFields):getUserInfoByHandle($emailHandle, $walletURLIPnotification . 'api/notify/v2/', $userRequiredFields);

					//$getReciversDetails = getUserInfo($emailHandle, $walletURLIP . 'api/v3/', $userRequiredFields);
					if(noError($getReciversDetails) && $getReciversDetails['errMsg']['active'] == 1){
					  $reciversDetails = $getReciversDetails['errMsg'];
					  $reciversDetails['_id'] = (isset($reciversDetails['user_id']))?$reciversDetails['user_id']:$reciversDetails['_id'];
					  $msg = "Success : Fetched {$emailHandle} details";
					  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
					  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

					  //redirecting to 2FA page
					  $msg = "Success : Redirecting to 2FA page";
					  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
					  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
					  $_SESSION['form_data'] = json_encode($_POST);
					  $_SESSION['xml_step'] = $i;

					  $_SESSION['xml_file'] = $logStorePath . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;
			   
					}else{
					  if(noError($getReciversDetails) && $getReciversDetails['erMsg']['active'] != 1 ){
						$errMsg = "Inactive or blocked receiver";
						$xml_data['step'.++$i]["data"] = $i.". Error: Inactive or blocked receiver({$emailHandle})";  
					  }else{
						$errMsg = "Not fount details of {$emailHandle}";
						$xml_data['step'.++$i]["data"] = $i.". Error: Fetching {$emailHandle} details";
					  }
					  
					  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

					  /******************** for logs user analytics ****************/
					  $responseArr["errCode"]=5;
					  $responseArr["errMsg"]=$errMsg;
					  $xml_data['response']["data"] = "";
					  $xml_data['response']["attributes"] = $responseArr;
					  /******************** for logs user analytics ****************/
					}
				  }else{
					$errMsg = $sendLimit['errMsg'];
					$xml_data['step'.++$i]["data"] = $i.". Error: ".$sendLimit['errMsg'];
					  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

					  /******************** for logs user analytics ****************/
					  $responseArr["errCode"]=5;
					  $responseArr["errMsg"]=$errMsg;
					  $xml_data['response']["data"] = "";
					  $xml_data['response']["attributes"] = $responseArr;
					  /******************** for logs user analytics ****************/
				  }
				}else{
				  $errMsg = 'Not fount senders details';
				  $xml_data['step'.++$i]["data"] = $i.". Error: Fetching sender details";
					$returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);

					/******************** for logs user analytics ****************/
					$responseArr["errCode"]=4;
					$responseArr["errMsg"]=$errMsg;
					$xml_data['response']["data"] = "";
					$xml_data['response']["attributes"] = $responseArr;
					/******************** for logs user analytics ****************/
				} 

			}else{
				$errMsg = 'Error: Fetching exchange rate';
				$xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
				$returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

				/******************** for logs user analytics ****************/
				$responseArr["errCode"]=3;
				$responseArr["errMsg"]=$errMsg;
				$xml_data['response']["data"] = "";
				$xml_data['response']["attributes"] = $responseArr;
				/******************** for logs user analytics ****************/
			}   
			
		}else{

		  $errMsg = 'Error: unable to connect search database';
		   $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
		   $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);

			/******************** for logs user analytics ****************/
			$responseArr["errCode"]=2;
			$responseArr["errMsg"]=$errMsg;
			$xml_data['response']["data"] = "";
			$xml_data['response']["attributes"] = $responseArr;
			/******************** for logs user analytics ****************/
		}
	}else{
		$errMsg = "Your available balance is insufficient for this transaction";
		$xml_data['step'.++$i]["data"] = $i.". Error: ".$errMsg;
		$returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
		
		$responseArr["errCode"]=5;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
	}
  }else{
    $errMsg = 'Error: Mandatory fields not found';
    $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
    $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]=2;
    $responseArr["errMsg"]=$errMsg;
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
    /******************** for logs user analytics ****************/
  }

  // create or update xml log Files
  $xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);
  if(noError($returnArr)){
    // $_SESSION['succ_msg'] = $returnArr['errMsg'];
    header('Location:'.$rootUrl.'views/two_factor_auth/index.php'); die;
  }else{
    $_SESSION['err_msg'] = $returnArr['errMsg'];
    header('Location:'.$rootUrl.'views/wallet/sendITD.php'); die; 
  }
  
}else if($returnURl == $_SESSION['redirect_url']){

  $i = $_SESSION['xml_step'];
  $security_type = $_SESSION['security_type'];
  $form_data = json_decode($_SESSION['form_data'],true);
  $activity_id = $_SESSION['activity_id'];
  $emailHandle = trim(cleanXSS(urldecode($form_data['email_handle'])));
  $amountInITD = cleanXSS(urldecode($form_data['amount_itd']));
  $amountInUSD = cleanXSS(urldecode($form_data['amount_usd']));
  $note = trim(cleanXSS(urldecode($form_data['note'])));
  $xml_file = $_SESSION['xml_file'];

  unset($_SESSION['security_type']);
  unset($_SESSION['xml_step']);
  unset($_SESSION['form_data']);
  unset($_SESSION['activity_id']);
  unset($_SESSION['redirect_url']);
  unset($_SESSION['xml_file']);
  unset($_SESSION['verify_status']);

  $msg = "Success : Callback from 2FA of {$security_type}";
  $xml_data['step'.++$i]["data"] = $i.". {$msg}";
  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

  // create search database connection
  $searchDbConn = createDBConnection("dbsearch");
  if(noError($searchDbConn)){
      $searchDbConn = $searchDbConn["connection"];

      // get currency exchange rate of usd, sgd and btc
      $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
      if(noError($getExchangeRate)){
        $getExchangeRate = $getExchangeRate["exchange_rate"];
          $userRequiredFields = $userRequiredFields.",email,account_handle,user_id,kyc_current_level";

          //fetch details of sender and receiver                    
          $getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);

          if(noError($getSendersDetails)){
            $senderDetails = $getSendersDetails['errMsg'];  

            $getReciversDetails = (filter_var($emailHandle, FILTER_VALIDATE_EMAIL))?getUserInfo($emailHandle, $walletURLIP . 'api/v3/', $userRequiredFields):getUserInfoByHandle($emailHandle, $walletURLIPnotification . 'api/notify/v2/', $userRequiredFields);

            if(noError($getReciversDetails)){
              $reciversDetails = $getReciversDetails['errMsg'];
              $reciversDetails['_id'] = (isset($reciversDetails['user_id']))?$reciversDetails['user_id']:$reciversDetails['_id'];

              $response = sendMoneyByWallet($senderDetails,$reciversDetails,$ipAddr,$amountInITD,$getExchangeRate,$note,$metaData);
              if(noError($response)){

                //send notification to ITD sender.  
                $email = $userEmail;
                $mailSubject = sprintf($notificationEmails['send_itd_sender']['sub'],$amountInITD);
                $emailBody = sprintf($notificationEmails['send_itd_sender']['body'], ucfirst($senderDetails['first_name']),$amountInITD.' '.$keywoDefaultCurrencyName, $reciversDetails['email']);
                $firstName = $senderDetails['first_name'];
                $lastName = $senderDetails['last_name'];
                $userId = $senderDetails['_id'];
                $notificationBody = sprintf($notificationMessages['send_itd_sender'], $amountInITD, $keywoDefaultCurrencyName, $reciversDetails['email']);
                $preferenceCode = 2;
                $category = "wallet";

                $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

                //send notification to ITD receiver.  
                $email = $reciversDetails['email'];
                $mailSubject = ' ';
                $emailBody = ' ';
                $firstName = $reciversDetails['first_name'];
                $lastName = $reciversDetails['last_name'];
                $userId = $reciversDetails['_id'];
                $notificationBody = sprintf($notificationMessages['send_itd_receiver'], $amountInITD, $keywoDefaultCurrencyName,$userEmail);
                $preferenceCode = '';
                $category = "wallet";
                
                $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
                $xml_data['step'.++$i]["data"] = $i.". Success : Sent {$amountInITD} ".$keywoDefaultCurrencyName." to {$emailHandle}";
                $msg = "Transaction successfully completed";
                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                  /******************** for logs user analytics ****************/
                  $sendMoneyArr["from"]=$userEmail;
                  $sendMoneyArr["to"]=$emailHandle;
                  $sendMoneyArr["amountITD"]=$amountInITD;
                  $sendMoneyArr["amountUSD"]=$amountInUSD;
                  $sendMoneyArr["note"]=$note;
                  $sendMoneyArr["transaction_id"]=$response['errMsg']['committed_transaction']['transaction_id'];
                  $xml_data['sendMoney']["data"] = "";
                  $xml_data['sendMoney']["attributes"] = $sendMoneyArr;
                  /******************** for logs user analytics ****************/


                  /******************** for logs user analytics ****************/
                  $responseArr["errCode"]=-1;
                  $responseArr["errMsg"]=$msg;
                  $xml_data['response']["data"] = "";
                  $xml_data['response']["attributes"] = $responseArr;
                  /******************** for logs user analytics ****************/


              }else{


                if(isset($response['errMsg']['account_clearance']['failed']['failed'])){
                    $errMsg = "Invalid receivers details";
                }else if(isset($response['errMsg']['balances_clearance']['failed'])){
                    $errMsg = "You don't have sufficient balance for this transaction.";
                }else{
                    $errMsg = "Invalid amount";  
                }
                $xml_data['step'.++$i]["data"] = $i.". Error: sending {$amountInITD} ".$keywoDefaultCurrencyName."  to {$emailHandle}";
                
                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);


                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=5;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
              } 
            }
        }
    }   
  }

  // create or update xml log Files
 $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);
  if(noError($returnArr)){
    $_SESSION['succ_msg'] = $returnArr['errMsg'];
  }else{
    $_SESSION['err_msg'] = $returnArr['errMsg'];  
  }
  header('Location:'.$rootUrl.'views/wallet/sendITD.php');

}else{
    unset($_SESSION['security_type']);
    unset($_SESSION['xml_step']);
    unset($_SESSION['form_data']);
    unset($_SESSION['activity_id']);
    unset($_SESSION['redirect_url']);
    unset($_SESSION['xml_file']);
    unset($_SESSION['verify_status']);
    $_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";    
    header('Location:'.$rootUrl.'views/wallet/sendITD.php');
}

?>