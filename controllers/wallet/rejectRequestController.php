<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);


$transDetails = json_decode(urldecode($_POST['trans_details']),true);
$action = cleanXSS(urldecode($_POST['action']));
$amountInITD = $transDetails['amount'];

$reciverEmail = $transDetails['request_sender']; 
$transId = $transDetails['_id'];
$note =  cleanXSS(urldecode($_POST['note']));
$user_id = $_SESSION["id"];
$userEmail = $_SESSION["email"];
$ipAddr = getClientIP();
$i = 1;

$logStorePath =$logPath["wallet"];
	
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "walletRejectRequest.xml";

/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);

$xml_data['request']["data"]='';
$xml_data['request']["attribute"]=$xmlArray["request"];


$msg = "Reject IT$ request process start.";
$xml_data['step'.$i]["data"] = $i.". {$msg}";
// create search database connection
$searchDbConn = createDBConnection("dbsearch");
if(noError($searchDbConn)){
    $searchDbConn = $searchDbConn["connection"];

    $msg = "Success : Search database connection";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    // get currency exchange rate of usd, sgd and btc
    $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
    if(noError($getExchangeRate)){
      $getExchangeRate = $getExchangeRate["exchange_rate"];
        $msg = "Success : Currency exchange rate";
        $xml_data['step'.++$i]["data"] = $i.". {$msg}";
        $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
        $userRequiredFields = $userRequiredFields.",email,account_handle";

        //fetch details of sender and receiver                    
        $getSendersDetails = getUserInfo($userEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
        if(noError($getSendersDetails)){
          $senderDetails = $getSendersDetails['errMsg'];
          $senderDetails['sender_trans_id'] = $senderTransId;  
          $msg = "Success : Fetched sender details";
          $xml_data['step'.++$i]["data"] = $i.". {$msg}";
          $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

          $getReciversDetails = getUserInfo($reciverEmail, $walletURLIP . 'api/v3/', $userRequiredFields);
          if(noError($getReciversDetails)){
            $reciversDetails = $getReciversDetails['errMsg'];
            $reciversDetails['reciver_trans_id'] = $reciverTransId;
            $msg = "Success : Fetched {$reciverEmail} details";
            $xml_data['step'.++$i]["data"] = $i.". {$msg}";
            $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs); 

            $response = rejectITDRequest($senderDetails,$reciversDetails,$transId,$note);
            if(noError($response)){
              //send notification to request receiver(Rejecter).  
              $email = $userEmail;
              $mailSubject = ' ';
              $emailBody = ' ';
              $firstName = $senderDetails['first_name'];
              $lastName = $senderDetails['last_name'];
              $userId = $senderDetails['_id'];
              $notificationBody = sprintf($notificationMessages['reject_req_sender'], $amountInITD, $keywoDefaultCurrencyName, $reciversDetails['email']);
              $preferenceCode = '';
              $category = "wallet";

              $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

              //send notification to request sender.  
              $email = $reciversDetails['email'];
              $mailSubject = ' ';
              $emailBody = ' ';
              $firstName = $reciversDetails['first_name'];
              $lastName = $reciversDetails['last_name'];
              $userId = $reciversDetails['_id'];
              $notificationBody = sprintf($notificationMessages['reject_req_receiver'], $userEmail, $amountInITD, $keywoDefaultCurrencyName);
              $preferenceCode = '';
              $category = "wallet";

              $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

              $msg = "You successfully rejected {$amountInITD} ".$keywoDefaultCurrencyName." request of {$reciverEmail}";
              $xml_data['step'.++$i]["data"] = $i.". Success : Rejected {$amountInITD} ".$keywoDefaultCurrencyName." request of {$reciverEmail}";
              $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
            }else{
              $errMsg = "Failed to reject request";
              $xml_data['step'.++$i]["data"] = $i.". Error: Rejecting {$amountInITD} ".$keywoDefaultCurrencyName."  request of {$emailHandle}";
                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);   
            } 
          }else{
            $errMsg = "Not found details of {$emailHandle}";
            $xml_data['step'.++$i]["data"] = $i.". Error: Fetching {$emailHandle} details";
              $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs); 
          }
        }else{
          $errMsg = 'Not found details of sender';
          $xml_data['step'.++$i]["data"] = $i.". Error: Fetching sender details";
            $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs); 
        } 

    }else{
        $errMsg = 'Error: Fetching exchange rate';
        $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }   
    
}else{

  $errMsg = 'Error: unable to connect search database';
   $xml_data['step'.++$i]["data"] = $i.". {$errMsg}";
   $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);
?>