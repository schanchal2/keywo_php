<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='../../views/prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

// include require files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/geoTrendHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../models/cdp/cdpUtilities.php');
require_once('../../models/wallet/walletModel.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);


$transDetails = json_decode(urldecode($_POST['trans_details']),true);
$order_id = cleanXSS(urldecode($_POST['order_id']));
$email = $_SESSION['email'];
$returnArr = array();

//connect to keywords database
$conn = createDBConnection("dbkeywords");
if(noError($conn)){

    $conn = $conn["connection"];
	$msg = "Success : Keyword database connection";
    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

    $query = "SELECT * FROM payments WHERE order_id='{$order_id}'";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;


        if($res['username'] == $email){

        	$errMsg = "Successfully fetch admin setting table details.";
        	$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
        }else{
        	$errMsg = "Invalid purchase request,Try again";
        	$returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }        

    } else {
        $errMsg = "Error : Fetching payment details";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }
  
}else{
	$errMsg = "Error : Unable to conn with database";
    $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
}  

if(noError($returnArr)){

}else{
	$_SESSION['errMsg'] = $returnArr['errMsg'];
}

echo json_encode($returnArr);

?>