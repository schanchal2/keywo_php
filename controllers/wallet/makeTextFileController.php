<?php
$backup_codes = $_GET['backup_codes'];

$filename = "twoFABackupCodes.txt";
$backup_codes = json_decode($backup_codes);
$content = "Your backup codes for google authentication are: \n";
$count = 1;
foreach ($backup_codes as $key => $value) {
	$content .= $count.") ".$value."\n";
	$count++;
}

header("Cache-Control: public");
header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=$filename");
header("Content-Type: application/octet-stream; "); 
header("Content-Transfer-Encoding: binary");

echo $content;

?>