<?php
//start session
session_start();

//include dependent files
 require_once('../../config/config.php');
 require_once('../../helpers/errorMap.php');
 require_once('../../helpers/coreFunctions.php');
 require_once('../../helpers/stringHelper.php');
 require_once('../../models/header/headerModel.php');
error_reporting(0);
//printArr($_POST);
//die();

$email     = $_SESSION["email"];
$category  = $_POST["category"];
$doc_id = $_POST["doc_id"]; //echo $doc_id;
$uid = $_POST["uid"];
// $cursor_name = $_POST["cursor"];
// $cursor_value=$_POST["value"];
// $offset = $_POST["offset_from"];

 $email = $_SESSION["email"];
 //echo $email;


if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
   global $walletPublicKey, $mode,$walletURLIPnotification;
    $retArray = array();

    if (!empty($uid) && !empty($uid)) {

        $headers = array();

        $apiText = "user_id=".$uid."&del_container=".$doc_id."&publicKey=".$walletPublicKey;
        $postFields = "del_container=".$doc_id."&publicKey=".urlencode($walletPublicKey);
        $apiName = "api/notify/v2/inbox/user/{$uid}";
        $requestUrl = $walletURLIPnotification;
        $curl_type = 'DELETE';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

       if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        //print_r($respArray);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;




}
echo json_encode($retArray);



?>