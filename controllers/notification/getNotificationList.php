<?php
//start session
session_start();

//include dependent files
require_once('../../config/config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/header/headerModel.php');
error_reporting(0);

$email     = $_SESSION["email"];
$category  = $_POST["category"];
$id = $_SESSION["id"];
$notifyCount    = '';

$cursor_name = $_POST["cursor"];
$cursor_value=$_POST["value"];
$offset = $_POST["offset_from"];


$id = $_SESSION["id"];
//echo $id;
//echo $id; echo "<br>";
//echo $category; echo "<br>";
//    echo $notifyCount; echo "<br>";
//echo $cursor_name; echo "<br>";
//echo $cursor_value; echo "<br>";
//echo $offset;
$notificationDetails1     = getNotificationDetail($id,$category,$notifyCount,$cursor_name, $cursor_value,$offset);
$fwd_cursor = $notificationDetails1["errMsg"]["fwd_cursor"]; //echo $fwd_cursor;
$bkwd_cursor = $notificationDetails1["errMsg"]["bkwd_cursor"]; //echo $bkwd_cursor;
$fwd_cursor_offset_from = $notificationDetails1["errMsg"]["fwd_cursor_offset_from"]; //echo $fwd_cursor_offset_from;
$bkwd_cursor_offset_from = $notificationDetails1["errMsg"]["bkwd_cursor_offset_from"]; //echo $bkwd_cursor_offset_from;
//echo '<pre>';
//print_r($notificationDetails1);
//echo '</pre>';
?>

<table class="table table-bordered mailinbox" style="border-collapse:collapse; margin-top:10px;" border="0" cellpadding="5" cellspacing="3" >

    <tbody class="tableData">
    <?php
    if(noError($notificationDetails1)){
        $notificationDetails1 = $notificationDetails1["errMsg"]['batched_container'];
        $xx = 0;
        foreach($notificationDetails1 as $key => $value){
            ?>
            <tr class="<?php if($value['read'] != 1){ $status = 0; ?> unread  <?php  }else{ $status = 1; } ?>
          " >
                <td class="aligncenter">
                    <input type="checkbox" style="text-align: center ;margin: auto ;zoom: 1.2" status="<?php echo $status; ?>" flag="<?php echo $value['archive_boundary']; ?>" class="checkbox" value="<?php echo $value['_id']; ?>" name="" />
                </td>
                <!--<td class="star"><a class="msgstar"></a></td>-->
                <td >
                    <div style="text-transform: capitalize;"><?php echo $value['category']?></div>
                </td>
                <td>
                    <?php if($value['category'] == "bid" ) { ?>
                        <a href="<?php echo $dashbordUrl; ?>#activebid" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body']);?>
                        </a>
                    <?php } else if($value['category'] == "withdrawal"){ ?>
                        <a href="<?php echo $presaleRootURL; ?>MyWallet.php#withdrawalRequest" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body']); ?>
                        </a>
                    <?php  } else if($value['category'] == "buy" || $value['category'] == "ask" || $value['category'] == "sell"){ ?>
                        <a href="<?php echo $dashbordUrl; ?>#mykeyword" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body'])?>
                        </a>
                    <?php }else if($value['category'] == "2FA Setting"){ ?>
                        <a href="<?php echo $rootUrl; ?>stsecurity.php" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body']); ?>
                        </a>
                    <?php  }else if($value['category'] == "2FA Recovery"){ ?>
                        <a href="<?php echo $rootUrl; ?>stsecurity.php" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body']); ?>
                        </a>
                    <?php  }else if($value['category'] == "deposit"){ ?>
                        <a href="<?php echo $presaleRootURL; ?>MyWallet.php#deposit" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                            <?php echo rawurldecode($value['notification_body']); ?>
                        </a>


                    <?php }  ?>
                </td>
                <td class="attachment" style="">
                    <?php
                    if($value['link'] != ''){
                        $a = explode("keywords", $value['link']);
                        $b = "keywords".$a[1];
                    }
                    ?>
                    <a href="<?php echo $b;?>"
                        <?php if($value['link'] == ''){?> hidden
                        <?php } ?> download id='download' class='attachment' >
                        <img src="../../images/attachment.png" alt="" />
                    </a>
                </td>
                <td class="date">
                    <?php
                    $time = $value['created_at'];
                    $original_timestamp = $time;
                    $timestamp = (int) substr($original_timestamp,0,-3);
                    echo date('d m, Y g:i:s A' ,$timestamp);
                    ?>
                </td>
            </tr>


            <?php
            $xx++; }
    }else{
        print("Error: Unable to fetch notification");
        exit;
    } ?>
    </tbody>
</table>
<input type='hidden' value="<?php echo $fwd_cursor; ?>" id="fwd_cursor">
<input type='hidden' value="<?php echo $bkwd_cursor; ?>" id="bkwd_cursor">
<input type='hidden' value="<?php echo $fwd_cursor_offset_from;?>" id="fwd_cursor_offset_from">
<input type='hidden' value="<?php echo $bkwd_cursor_offset_from;?>" id="bkwd_cursor_offset_from">