<?php
session_start();

/**
 * Created by Sublime.
 * User: Nikhil
 * Date: 12/01/2017
 * Time: 4:14 PM
**/


/* Include require files */
	require_once('../../config/config.php');
	 require_once('../../config/db_config.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../models/search/searchResultModel.php');
	require_once('../../models/header/headerModel.php');
	require_once('../../helpers/deviceHelper.php');	
	require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
	error_reporting(0);

	$logStorePathUserMgmt=$logPath["userManagement"];

	//for xml writing essential
	$xmlProcessor =new xmlProcessor();
	$xmlfilename="ModeSwitch.xml";

	$modeId 	  	= cleanXSS($_POST['mode_id']);
	$modeURL 	  	= cleanXSS(urldecode($_POST['analyticSwitchUrl']));
	$userId 		= $_SESSION['id'];
	$searchDbConn 	= createDBConnection('dbsearch');
    $searchDbConn 	= $searchDbConn["connection"];
	$returnArr 		= array();
	$errMsg 		= '';
	$errorCode 		= '';
	$field_name 	= '';
	if ($modeId == 1) {
		$modeId = 2;
	} else {
		$modeId = 1;
		$userId = $_SESSION['id'];


	}

	$user_email=$_SESSION["email"];
	$userId = $_SESSION['id'];

	$xmlArray = initializeXMLLog($user_email);

	$xml_data['request']["data"]='';
	$xml_data['request']["attribute"]=$xmlArray["request"];


	$xml_data['frompage']["data"] = "{$modeURL}";

	$errMsgLogs = "Change Mode process start.";
	$xml_data['step1']["data"] = "1. {$errMsgLogs}";

/* Calling Model Function to Exedcute Curl Request */
	$getUserSystemMode = changeUserSystemMode($userId,$modeId);

    $switchMode = $getUserSystemMode['errMsg'];
    if ($switchMode == 1) {
        $field_name="user_search_mode_switch_count";
    }else {
        $field_name="user_social_mode_switch_count";
    }

	if (noError($getUserSystemMode)) {
		if ($modeId == 1){
			// $searchAppId = getUserInfo($user_email,$walletURLIPnotification.'api/notify/v2/','default_search_appId');
			$connSearch = createDbConnection('dbsearch');

			if(noError($connSearch)){				
				$connSearch = $connSearch["connection"];		
				$userDefaulsSearchAppDetail = getUserDefaultAppDetails($user_email,$connSearch);
				if (noError($userDefaulsSearchAppDetail)) {
					$userDefaulsSearchAppDetail = $userDefaulsSearchAppDetail['errMsg'];
					// $getUserSystemMode['errMsg']['appName'] = strtolower(ucwords($userDefaulsSearchAppDetail['appName']));
					$getUserSystemMode['errMsg']['url'] = $rootUrlSearch.$userDefaulsSearchAppDetail['appLandingUrl'];
					$getUserSystemMode['errMsg']['loginStatus'] = 1;
					$getUserSystemMode['errMsg']['appId'] = $userDefaulsSearchAppDetail['appId'];
					$getUserSystemMode['errMsg']['appLogo'] = $userDefaulsSearchAppDetail['appLogo'];
					$getUserSystemMode['errMsg']['landing'] = $userDefaulsSearchAppDetail['landingPage'];
					$getUserSystemMode['errMsg']['rootUrl'] = $rootUrl;
					$getUserSystemMode['errMsg']['appURL'] = $userDefaulsSearchAppDetail['appURL'];
					$getUserSystemMode['errMsg']['searchRootUrl'] = $rootUrlSearch.strtolower(ucwords($userDefaulsSearchAppDetail['appName']))."/";
					$getUserSystemMode['errMsg']['appName'] = $userDefaulsSearchAppDetail['appName'];
				}

			}
		}
		$errCode = $getUserSystemMode['errCode'];

		$errMsg = $getUserSystemMode['errMsg'];

		$errMsgLogs = "Running changeUserSystemMode Success!.";
		$xml_data['step2']["data"] = "2. {$errMsgLogs}";

        $returnArr['errMsg'] = $errMsg;
        $returnArr['errCode'] = -1;
        $returnArr = setErrorStack($returnArr, -1, $errMsg);


        $returnArrResponse['errMsg'] = $errMsgLogs;
        $returnArrResponse['errCode'] = -1;
        $returnArrResponse['fieldname'] = $field_name;

	} else {
		$errCode = 1;
		$errMsg = $getUserSystemMode['errMsg'];

		$errMsgLogs = "Running changeUserSystemMode Failed!.";
		$xml_data['step2']["data"] = "2. {$errMsgLogs}";

        $returnArr['errMsg'] = $errMsg;
        $returnArr['errCode'] = $errCode;
        $returnArr = setErrorStack($returnArr, 1, $errMsg);

        $returnArrResponse['errMsg'] = $errMsgLogs;
        $returnArrResponse['errCode'] = 1;
        $returnArrResponse['fieldname'] = $field_name;
	}

	$xml_data['response']["data"]="";
    $xml_data['response']["attributes"]=$returnArrResponse;
    $xmlProcessor-> writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);

	echo json_encode($returnArr);
?>