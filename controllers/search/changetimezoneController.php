<?php
header("Access-Control-Allow-Origin: *");
	
	session_start();
	//include dependent files	
	// echo "Herllo </br>";
require_once('../../config/config.php'); // echo "Herllo </br>";
require_once('../../config/db_config.php');// echo "Herllo </br>";
require_once('../../helpers/arrayHelper.php');// echo "Herllo </br>";
require_once('../../helpers/coreFunctions.php');// echo "Herllo </br>";
require_once('../../helpers/errorMap.php');// echo "Herllo </br>";
require_once('../../helpers/stringHelper.php');// echo "Herllo </br>";
require_once('../../models/search/widgetSearchModel.php');// echo "Herllo </br>";

//Create Database Connection
$conn = createDBConnection('dbsearch');
	//print_r($conn); 
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}

//get request parameters
$tm = urldecode($_POST['q']);
$tm = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/", $tm, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
$resulttime = array();
$totimeCode = $tm[4];
$fromtimeCode = $tm[2];
$changetime = $tm[0];
$changetime1 = $tm[1];
$ch_tm = $changetime.' '.$changetime1;

$date = date('Y-m-d');
$dt = strtotime($date . ' ' . $ch_tm);
$formatdate = date("Y-m-d H:i:s", $dt);

if($fromtimeCode == "Asia/Kolkata"){
	$tz1 = 'Asia/Kolkata';
}else{
	$tz1 = $fromtimeCode;
}
if($totimeCode == "Asia/Kolkata"){
	$tz2 = 'Asia/Kolkata';
}else{
	$tz2 = $totimeCode;
}
$time = $formatdate;
$tm_integer = converToTz($conn,$time, $tz2, $tz1);
$formattime = strtotime($time);
$new_date1 = date(' h:i A l',$formattime);
$resulttime["select_date"] = $new_date1;
$resulttime["int_date"] = $tm_integer;
$resulttime["errCode"] = -1;  
//Get JSON Response
echo json_encode($resulttime);
exit;
?>