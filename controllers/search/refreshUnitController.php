<?php
	
	
/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');
	
	error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
//print_r($conn);
	if(noError($conn)){
		$conn = $conn["connection"]; //echo $conn; die;
	}else{
		print_r("Database Error");
	}

//get request parameters
$unitdata 	=  urldecode($_POST['q']);
$resultunit = array();

//get unittype set
$unitTypeset 			= unitset($conn,$unitdata); //print_r($unitTypeset); die;
$unitset 				= trim($unitTypeset["errMsg"],'"');
$resultunit["set"]  	= $unitset;
$resultunit["errCode"]  = -1; 

//send Json response
echo json_encode($resultunit);
exit;
?>