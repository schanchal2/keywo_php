<?php
header("Access-Control-Allow-Origin: *");
	
	session_start();
	//include dependent files	
	// echo "Herllo </br>";
require_once('../../config/config.php'); // echo "Herllo </br>";
require_once('../../config/db_config.php');// echo "Herllo </br>";
require_once('../../helpers/arrayHelper.php');// echo "Herllo </br>";
require_once('../../helpers/coreFunctions.php');// echo "Herllo </br>";
require_once('../../helpers/errorMap.php');// echo "Herllo </br>";
require_once('../../helpers/stringHelper.php');// echo "Herllo </br>";
require_once('../../models/search/widgetSearchModel.php');// echo "Herllo </br>";
error_reporting(0);

//Create Database Connection
$conn = createDBConnection('dbsearch');
	//print_r($conn);
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}

//get request parameters
$currdata     = urldecode($_POST['q']);
$currencyData = explode(" ",$currdata);
$fromCurrCode = $currencyData[1];
$toCurrCode   = $currencyData[2];
$amount       = $currencyData[0];
$flag         = $currencyData[3]; 
$resultamt    = array();

//get currency convert Amount
$newamt = convertCurrency($fromCurrCode, $toCurrCode, $amount,$conn);// print_r($newamt);
	if(noError($newamt)){
		$amount      		= explode(" ",$newamt["errMsg"]);
		$currencyamt 		= trim($amount[0], '"');
		$currencyamt 		= round($currencyamt, "3");
		$resultamt["amt"]   = $currencyamt;
		$resultamt["flag"]  = $flag;
	}else{
		$resultamt["errCode"] = 2;		
		$resultamt["errMsg"]  = "Unable to get Result amount of given currency code";	
	}
//send Json response
echo json_encode($resultamt);
exit;
?>