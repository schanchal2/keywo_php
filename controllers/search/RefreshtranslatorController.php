<?php
/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');
require_once('../../models/search/LanguageTranslationModel.php'); //echo "Hello"; die;
error_reporting(0);

//get request parameters
$q_curr     = urldecode($_POST['q']);
$fresh      = explode(" ",$q_curr);
$resultText = array();

// Prepare variables
$text   	= rawurldecode($_POST['isTyping']);
if(strpos($text,'&') !== false){
	$text = str_replace("&","&amp;", $text);
}
$skuList = explode(PHP_EOL, $text);

$from   = $fresh[0];
$to 	= $fresh[1];

//Function to translate text using language codes
$resultarray = get_translate($skuList, $from, $to);
$resultText["textT"] = $resultarray;

//send Json response
echo json_encode($resultText);
?>
