<?php
session_start();
/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');
require_once('../../backend_libraries/widget-src/Parser.php');

	error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
//echo "<pre>"; print_r($conn);echo "</pre>"; die;
	//Validating User LoggedIn and LoggedOUt status
	if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		$login_status = 1;
	}else{
		$login_status = 0;
	}
	//get query string from url
	$sq = stripcslashes($_GET["q"]);
	$sq = strtolower($sq);

	if(strpos($sq, '&#039;')!== false){
		$keywords = str_replace("&#039;", "'", $sq);
	}
	if(strpos($sq, '&quot;')!== false){
			$sq = str_replace("&quot;", '"', $sq);
	}
	if(strpos($sq, '%')!== false){
		$sq = str_replace("%","/100",$sq);
	}

	/****For Calculator Widget****/
	$trig = array("sqrt","sin","cos","tan"); 
	foreach($trig as $value)
	{	
		if(strpos($sq,$value)!== false){
			if(strpos($sq,'calculator:')!== false){
				$sq = str_replace("calculator:", "", $sq); 
			}
			if(strpos($sq,'Calculator:')!== false){
				$sq = str_replace("Calculator:", "", $sq);
			}
			$sq = str_replace($value, "", $sq);
			$sq = str_replace(array( '(', ')' ), '', $sq);
			if($value === "sqrt"){
				$sq = sqrt($sq);
			}elseif($value === "sin"){
				$sq = sin($sq);
			}elseif($value === "cos"){
				$sq = cos($sq);
			}elseif($value === "tan"){
				$sq = tan($sq);
			}
		}
	}
	$mathString = trim($sq);
	$number =  preg_replace("/[^a-z0-9]/i", '', $mathString); 

	//Function to Query Search 
	$detail = query_analyzer($sq,$conn);
	if(noError($detail)){
		$detail = $detail["errMsg"];
	}else{
		printArr("Error analyzing Query");
	}
	
	$keys = array_keys($detail["types"]);
	$widgetType = $keys[0];
	if($widgetType == ""){
		$widgetType = $keys[1];
	} //echo "Hello"; die;
?>

	<div class="dialog_apps" >
			<?php include ("../../views/widgets/calculator/calculator.php"); ?>
	</div>