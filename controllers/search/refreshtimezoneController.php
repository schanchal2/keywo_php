<?php
header("Access-Control-Allow-Origin: *");
	
	session_start();
	//include dependent files	
	// echo "Herllo </br>";
require_once('../../config/config.php'); // echo "Herllo </br>";
require_once('../../config/db_config.php');// echo "Herllo </br>";
require_once('../../helpers/arrayHelper.php');// echo "Herllo </br>";
require_once('../../helpers/coreFunctions.php');// echo "Herllo </br>";
require_once('../../helpers/errorMap.php');// echo "Herllo </br>";
require_once('../../helpers/stringHelper.php');// echo "Herllo </br>";
require_once('../../models/search/widgetSearchModel.php');// echo "Herllo </br>";
require_once('../../helpers/deviceHelper.php');

//Create Database Connection
$conn = createDBConnection('dbsearch');
	//print_r($conn); 
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	
$tzdata =  urldecode($_POST['q']);
$timezoneData = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/", $tzdata, 0, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
$resultTime = array();
$totimeCode = $timezoneData[0];
$tz_name = $timezoneData[1];
$fromtimeCode = $timezoneData[2];
$flag = $timezoneData[3]; 

//Function to get TimezoneList
$timeznlist = timezonelist($conn);
$dt = date("d/m/Y H:i:s",time());
$df1 = date('d/m/Y H:i:s',time());
$df2 = "Y/m/d h:i:s a";
$ipAddress = getClientIP();//get IP address

$query = @unserialize(file_get_contents('http://ip-api.com/php/'.$ipAddress));
if($query && $query['status'] == 'success') {
	$tzindia = $query['timezone'];//get Timezone
}
foreach ($timeznlist["errMsg"] as $key => $value) {
	$timelist = explode(',', $value);
	if($timelist[2] === $timezoneData[0] && $timelist[0] === $timezoneData[1]){
		$tmlong = $timelist[0];
		$tmshort = $timelist[1];
		$tmtz = $timelist[2];
	}
}
if($totimeCode == "Asia/Kolkata"){
	$tz1 = 'Asia/Kolkata';
}else{
	$tz1 = $totimeCode;
}
if($tm_long == "Indian Standard Time"){
	$tz2 = 'Asia/Kolkata';
}else{
	$tz2 = $tmtz;
}

//Function to Convert one Timezone to Another TimeZone
$changetime = date_convert($conn,$dt, $tz1, $df1, $tz2, $df2);

//Function to get current timedate of TimeZone
$timeInteger = converToTz($conn,$time,$tz2, $tz1);

$resultTime["long"] = $tmlong;
$resultTime["short"] = $tmshort;
$resultTime["flag"] = $flag;
$resultTime["time_diff"] = $changetime["time_diff"];
$resultTime["res_date"] = $changetime["res_date"];
$resultTime["curr_date"] = $changetime["curr_date"];
$resultTime["int_date"] = $timeInteger;
$resultTime["errCode"] = -1;
  
//send Json response
echo json_encode($resultTime);
exit;
?>