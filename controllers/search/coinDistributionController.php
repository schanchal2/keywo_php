<?php
session_start();


if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){

      // include require files
      require_once('../../config/config.php');
      require_once('../../config/db_config.php');
      require_once('../../helpers/errorMap.php');
      require_once('../../helpers/coreFunctions.php');
      require_once('../../helpers/deviceHelper.php');
      require_once('../../helpers/arrayHelper.php');
      require_once('../../helpers/stringHelper.php');
     // require_once('../../helpers/geoTrendHelper.php');
      require_once('../../models/cdp/cdpUtilities.php');
      require_once('../../models/keywords/keywordCdpModel.php');

      if(isset($_POST) && !empty($_POST)){
          // get post filed

          $email = $_SESSION["email"];
          $appId = $_POST["appId"];
          $appName = urldecode($_POST["appName"]);
          $appName = strtolower($appName); // convert into small letters
          $searchQuery = urldecode($_POST["q"]);
          $searchQuery = strtolower($searchQuery); // convert into small letters
          $logFileName = urldecode($_POST["log"]);
          $ipSearchCount = $_POST["searchCountFromIP"];
          $originIp = $_POST["origin_ip"];

          // create search database connection
          $searchDbConn = createDBConnection("dbsearch");

          if(noError($searchDbConn)){
                $searchDbConn = $searchDbConn["connection"];

                // get max ip limit from admin setting table
                $getAdminSetting = getSettingsFromSearchAdmin($searchDbConn);
                if(noError($getAdminSetting)){
                    $getAdminSetting = $getAdminSetting["data"];
                    $ipMaxLimit = $getAdminSetting["IPMaxLimit"];

                    // get currency exchange rate of usd, sgd and btc
                    $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);

                    if(noError($getExchangeRate)){
                          $getExchangeRate = $getExchangeRate["exchange_rate"];
                          $getExchangeRate = json_encode($getExchangeRate);

                          // access $userRequiredFields field from config.php file
                          $userInteractionDetails = "user_id,no_of_qualified_interactions_pending,no_of_searches_in_last_hour,last_hour_search_time";

                          // 1. get current user search type i.e. qualified or unqualified.
                          $interactionType = getSearchType($email, $userInteractionDetails);

                          if(noError($interactionType)){
                              $interactionType        =   $interactionType["data"];
                              $userId                 =   $interactionType["user_id"];
                              $searchType             =   $interactionType["user_search_type"];
                              $noOfInteractionPending =   $interactionType["no_of_qualified_interactions_pending"];
                              $noOfSearchesInLastHour =   $interactionType["no_of_searches_in_last_hour"];
                              $lastHourSearchTime     =   $interactionType["last_hour_search_time"];

                              if($searchType == "Qualified" && $ipSearchCount <= $ipMaxLimit){
                                  $searchType = "Qualified";
                              }else{
                                  $searchType = "Unqualified";
                              }

                              // 1. Get current payout amount from payout.json file.
                              $currentPayout = getCurrentPayout();

                              if(noError($currentPayout)){
                                  $currentPayout = $currentPayout["current_payout"];
                                  $currentPayout = number_format(($currentPayout/4), 2);


                                  /*******************************************************************************************************************
                                   ************* Process follow in coin distribution process if search type is qualified is given below:**************
                                   *
                                   *  2. Check total search count searched on particular IP.
                                   *  - Get max ip limit set by admin from admin_settings table.
                                   * 3. Check if logged in user has referral user or not (in cdp cron structure).
                                   * 4. Credit user interaction earning using API request.
                                   * 5. Insert content_consumer interaction earning transaction by executing API insertUserTransaction()
                                   *  with type = "interaction_earning"
                                   * 6. Insert transaction into week wise user_search_history table into MYSQL search database.
                                   * 7. Increase logged in user's qualified searches in userdetails @notification server
                                   *   - 7.1 Deduct qualified search of logged in user from node side ( on success of increase qualified searches).
                                   * 8. Insert new record into content_consumer_list into mysql table of keyword database.
                                   * 9. Insert/update qualified search count + 1 into keywo_stats table of keyword MYSQL database.
                                   * 10. write CDP process into xml logs.
                                   *
                                   **********************************************************************************************************************/

                                   /*******************************************************************************************************************
                                    ************* Process follow in coin distribution process if search type is unqualified is given below:**************
                                    *
                                    *  1. Update totalUnqualifiedSearches field by 1 into sc_app_details table of corresponding appId.
                                    *  2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
                                    *  3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
                                    *  4. Update week wise user search transaction history table with type = "Unqualified" into mysql search database
                                    *  5. Increasing logged in user's unqualified search count into "userdetails" using API (@notification server).
                                    *  6. Update unqualified_searches count by 1 into keywo_stats table of corresponding appId (instead of updating
                                    *   pool unqualified searches in pool user).
                                    *
                                    **********************************************************************************************************************/

                                    // array that contains function parameters
                                    $cdpGlobalParam = array(
                                        "user_id" => $userId,
                                        "email" => $email,
                                        "search_type" => $searchType,
                                        "search_query" => $searchQuery,
                                        "current_payout" => $currentPayout,
                                        "origin_ip" => $originIp,
                                        "app_id" => $appId,
                                        "app_name" => $appName,
                                        "exchange_rate_in_json" => $getExchangeRate,
                                        "no_of_qualified_interactions_pending" => $noOfInteractionPending,
                                        "no_of_searches_in_last_hour" => $noOfSearchesInLastHour,
                                        "last_hour_search_time" => $lastHourSearchTime
                                      );

                                      $coinDistribution = coinDistribution($cdpGlobalParam, $searchDbConn);
                                      if(noError($coinDistribution)){

                                          // update geo trend database with keyword details
                                          //$updateGeoTrendsData = updateGeoTrendData($searchQuery, $appId, $originIp);

                                          // on success
                                          $extraArgs["no_of_pending_interaction"] = $coinDistribution["no_of_pending_interaction"];
                                          $extraArgs["total_available_balance"] = $coinDistribution["total_available_balance"];
                                          $extraArgs["search_type"] = $coinDistribution["search_type"];
                                          $errCode    = $coinDistribution["errCode"];
                                          $errMsg     = $coinDistribution["errMsg"];
                                          $returnArr  = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                      }else{
                                          // on failure
                                          $errCode    = $coinDistribution["errCode"];
                                          $errMsg     = $coinDistribution["errMsg"];
                                          $returnArr  = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
                                      }
                              }else{
                                $errMsg = $currentPayout["errMsg"];
                                $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);
                              }
                          }else{
                              $errMsg = 'Error: Fetching interaction type';
                              $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                          }
                    }else{
                        $errMsg = 'Error: Fetching exchange rate';
                        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                    }
                }else{
                    $errMsg = "Error: Fetching setting details";
                    $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                }
          }else{
                $errMsg = 'Error: unable to connect search database';
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
          }
      }else{
          $errMsg = "Error: Mandatory field not found";
          $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
      }

      echo json_encode($returnArr);
}else{
  echo "Authorisation failure";
  exit;
}

 ?>
