<?php

	
/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');
	
	error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	
//get request parameters
$unitamt =  urldecode($_POST['q']);
$conversionamt =  urldecode($_POST['p']);
$unitvar =  urldecode($_POST['x']);
$type = urldecode($_POST['type']);
$action =  urldecode($_POST['action']);
$unitvalue =  urldecode($_POST['y']);
if($action === "tonumber"){
	$getQuery = "SELECT value FROM `unit_formulas` WHERE from_value='".$unitvalue."' AND to_value='".$unitvar."'";
    $resultQuery = runQuery($getQuery,$conn);
	$returnVal = array();
	$row;
	if(noError($resultQuery)){
		$row = mysqli_fetch_assoc($resultQuery["dbResource"]);
		$returnVal["errCode"] = -1;
		$returnVal["errMsg"] = $row;
	}else{
		$returnVal['errMsg'] = $resultQuery['errMsg'];
		$returnVal['errCode'] = 5;
	}
	if($unitvar == "Kilometre per litre" && $unitvalue == "Litre per 100 kilometres"){
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($row["value"]/$conversionamt),5);
	}elseif($unitvar == "Litre per 100 kilometres" && $unitvalue == "Kilometre per litre"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($row["value"]/$conversionamt),5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($conversionamt*9/5)+32, 5);
	}elseif($unitvar == "Celsius" && $unitvalue == "Fahrenheit"){
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($conversionamt -32)*5/9, 5);
	}elseif($unitvar == "Celsius" && $unitvalue == "Kelvin"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($conversionamt - 273.150), 5);
	}elseif($unitvar == "Kelvin" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($conversionamt + 273.150), 5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Kelvin"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round((($conversionamt - 273.15) * 9/5 + 32), 5);
	}elseif($unitvar == "Kelvin" && $unitvalue == "Fahrenheit"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round((($conversionamt - 32) * 5/9 + 273.15), 5);
	}elseif($unitvar == "Celsius" && $unitvalue == "Rankine"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round((($conversionamt - 32 - 459.67) / 1.8), 5);
	}elseif($unitvar == "Rankine" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($conversionamt * 1.8 + 32 + 459.67), 5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Rankine"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($unitamt- 459.67), 5);
	}elseif($unitvar == "Rankine" && $unitvalue == "Fahrenheit"){		
		$returnVal["Msg"] = 'tovalue';
		$row["value"] = round(($unitamt + 459.67), 5);
	}else{
		$row["value"] = $row["value"]*$conversionamt;	
		$returnVal["Msg"] = 'tovalue';
	}
	$returnVal["errMsg"] = $row;	
	echo json_encode($returnVal);
	exit;
}else{
	$getQuery = "SELECT value FROM `unit_formulas` WHERE from_value='".$unitvar."' AND to_value='".$unitvalue."'";
	$resultQuery = runQuery($getQuery,$conn);
	$returnVal = array();
	$row;
	if(noError($resultQuery)){
		$row = mysqli_fetch_assoc($resultQuery["dbResource"]);
		$returnVal["errCode"] = -1;
		$returnVal["errMsg"] = $row;
	}else{
		$returnVal['errMsg'] = $resultQuery['errMsg'];
		$returnVal['errCode'] = 5;
	}
	if($unitvar == "Second" && $unitvalue == "Minute" && $unitamt == "60"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round($row["value"]*$unitamt);		 
	}elseif($unitvar == "Kilometre per litre" && $unitvalue == "Litre per 100 kilometres"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($row["value"]/$unitamt),5);
	}elseif($unitvar == "Litre per 100 kilometres" && $unitvalue == "Kilometre per litre"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($row["value"]/$unitamt),5);
	}elseif($unitvar == "Second" && $unitvalue == "Minute" && $unitamt == "120"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round($row["value"]*$unitamt);	
	}elseif($unitvar == "Minute" && $unitvalue == "Day" && $unitamt == "1440"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = 1;	
	}elseif($unitvar == "Minute" && $unitvalue == "Hour" && $unitamt == "60"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = 1;	
	}elseif($unitvar == "Celsius" && $unitvalue == "Fahrenheit"){
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt*9/5)+32, 5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt -32)*5/9, 5);
	}elseif($unitvar == "Celsius" && $unitvalue == "Kelvin"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt + 273.15), 5);
	}elseif($unitvar == "Kelvin" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt - 273.15), 5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Kelvin"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round((($unitamt - 32) * 5/9 + 273.15), 5);
	}elseif($unitvar == "Kelvin" && $unitvalue == "Fahrenheit"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round((($unitamt - 273.15) * 9/5 + 32), 5);
	}elseif($unitvar == "Fahrenheit" && $unitvalue == "Rankine"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt + 459.67), 5);
	}elseif($unitvar == "Rankine" && $unitvalue == "Fahrenheit"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt- 459.67), 5);
	}elseif($unitvar == "Celsius" && $unitvalue == "Rankine"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round(($unitamt * 1.8 + 32 + 459.67), 5);
	}elseif($unitvar == "Rankine" && $unitvalue == "Celsius"){		
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = round((($unitamt - 32 - 459.67) / 1.8), 5);
	}else{
		$returnVal["Msg"] = 'fromvalue';
		$row["value"] = $row["value"]*$unitamt;	
	}
	$returnVal["errMsg"] = $row;
	echo json_encode($returnVal);
	exit;
}?>
