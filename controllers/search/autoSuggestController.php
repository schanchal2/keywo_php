<?php

/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/searchResultModel.php');

error_reporting(0);
//Search database connection
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
}else{
    print("Error: Database connection");
    exit;
}

$term =cleanXSS($_GET["term"]);
$appStatus = urldecode($_GET["app_status"]);
$result = autoSuggest($connSearch, $term, $appStatus);

 if (noError($result)) {
     echo json_encode($result['data']);
 }



?>