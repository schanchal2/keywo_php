<?php
	//header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

	error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch'); //print_r($conn); die;
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	
	
	// getting search query from url.
	$city = rawurldecode($_REQUEST['city']);
	if(strpos($city,' ')!== false){
		$city = str_replace(" ","%20",$city); 
	}
	
	// getting current time from server where files are stored.
	$time = time();
	$timestamp = "Wednesday, 31 Dec 1969 06:00:00 PM";

	// creating directory to store weather result in json format.
	$thisdir = "{$docRoot}json_directory/Weather";
	
	if(!is_dir($thisdir)){
		mkdir($thisdir, 0777, true);
	}
	$keywds = explode(" ",$city);
	$i = 0;
	$key= '';	
	foreach($keywds as $p){
		$p = strtolower($p);
		if($i > 0) $key.= "_";
		$key .= $p;
		$i++;
	}
	
	// creating json file with search keywords.	
	$jsonFile = $key.".json"; 
	/*
	Purpose: 	1) check whether file exist or not
				2) if exist check file creation time
				3) creation time greater than 1 hour (i.e 3600 seconds) then
				   file get deleted and create a new file with current time and create new		
				   file with search query.
	*/
	if(file_exists($thisdir."/".$jsonFile)){
		if(($time - filemtime($thisdir."/".$jsonFile)) >= 3600)  // 60*60
		{
			unlink($thisdir."/".$jsonFile);
		}
	}

	//To get Current dateTime of given/selected city	
	$newdate_city = getdateTimeCity($city, $conn);

	if(noError($newdate_city)){

		$newdate_city = $newdate_city["errMsg"];
	}else{
		print("Error: Fetching city details");
		exit;
	}
	
	/*
	Purpose:	1) check whether file exist or not
				2) If file exist then get file from server using file_get_contents() function without calling weather api.
	*/
	
	if(file_exists($thisdir."/".$jsonFile)){	
		$getJsonResult = file_get_contents($thisdir."/".$jsonFile);
		$data = json_decode($getJsonResult,true);
		$datetime = $data['dt'];
		$speed = $data['wind']['speed'];
		$wind = ($speed*18)/5;	
		$windSpeed = round($wind);
		$wdesc = $data['weather'][0]['main'];
		$wpic = $data['weather'][0]['icon'];
		$humidity = $data['main']['humidity'];
		if($newdate_city === $timestamp){
			$newdate = " ";
		}else{
			$newdate = $newdate_city;
		} 
		$weather = array();
		$weather["temp"] = round($data['main']['temp']);
		$weather["desc"] = $wdesc;
		$weather["wind_speed"] = $windSpeed;
		$weather["hm"] = $humidity;
		$weather["w_pic"] = $wpic;
		$weather["datetime"] = $newdate;		
	}else{	
		$url = "http://api.openweathermap.org/data/2.5/weather?q=".$city."&units=metric&cnt=7&lang=en&appid=0bc46f2d74d94dcf5f4a3241666472d4";
		$json = file_get_contents($url);
		file_put_contents($thisdir."/".$jsonFile,$json);
		$data = file_get_contents($thisdir."/".$jsonFile,$json);
		$data = json_decode($data , true);
		$datetime = $data['dt'];
		$speed = $data['wind']['speed'];
		$wind = ($speed*18)/5;	
		$windSpeed = round($wind);
		$wdesc = $data['weather'][0]['main'];
		$wpic = $data['weather'][0]['icon'];
		$humidity = $data['main']['humidity'];
		$weather = array();
		 if($newdate_city === $timestamp){
			$newdate = " ";
		}else{
			$newdate = $newdate_city;
		} 
		$weather["temp"] = round($data['main']['temp']);
		$weather["desc"] = $wdesc;
		$weather["wind_speed"] = $windSpeed;
		$weather["hm"] = $humidity;
		$weather["w_pic"] = $wpic;
		$weather["datetime"] = $newdate;				
	}	
	
	echo json_encode($weather);
	
 ?>
