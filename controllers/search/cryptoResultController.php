<?php

/*
* --------------------------------------------------------------------------------------------
*   Include helper files
*---------------------------------------------------------------------------------------------
*/
header("Access-Control-Allow-Origin: *");
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

	error_reporting(0);

	//For database connection
	$conn = createDBConnection('dbsearch');
	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}

	//Validating User LoggedIn and LoggedOUt status
	if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		$login_status = 1;
	}else{
		$login_status = 0;
	}

	//get query string from url
	$sq = stripcslashes($_GET["q"]);
	$sq = strtolower($sq);
	if(strpos($sq, '&#039;')!== false){
		$sq = str_replace("&#039;","'",$sq);
	}
	if(strpos($sq, '&quot;')!== false){
		$sq = str_replace("&quot;",'"', $sq);
	}

	/****For Weather Widget****/
	if(strpos($sq,'singapore')!== false){
		$sq = str_replace("singapore","Singapore",$sq);
	}
	if(strpos($sq,'coins:')!== false){
		$sq = str_replace("coins:", "", $sq);
	}
	if(strpos($sq,'translator:')!== false){
		$sq1 = str_replace("translator:","",$sq);
		if(empty($sq1)){
			$sq = "translator:";
		}
	}
	//Function to Query Search
	$detail = query_analyzer($sq,$conn);
	if(noError($detail)){
		$detail = $detail["errMsg"];
	}else{
		printArr("Error analyzing Query");
	}

	$keys = array_keys($detail["types"]);
	$widgetType = $keys[0];
	if($widgetType == ""){
		$widgetType = $keys[1];
	}
	$status = "N";
	if (array_key_exists('btc', $detail["words"])) {
		$status = "Y";
	}else{
		$status = "N";
	}
?>
	<div class="dialog_apps" style="padding-bottom:0%;">
			<?php  include("../../views/widgets/crypto_currency/crypto_currency.php"); ?>
	</div>
