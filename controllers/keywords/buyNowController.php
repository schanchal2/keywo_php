<?php
ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

// Include required helpers files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/transactionHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../models/keywords/keywordBidModel.php');
require_once('../../models/keywords/acceptBidModel.php');
require_once('../../models/keywords/keywordCdpModel.php');
require_once('../../models/keywords/userCartModel.php');
require_once('../../models/keywords/keywordPurchaseModel.php');
require_once('../../models/header/headerModel.php');
require_once('../smtp/Send_Mail.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once("../../models/analytics/keyword_stat_analytics.php");
error_reporting(0);

$keyword = trim(urldecode($_POST["buyNowKeyword"]));
$email   = $_SESSION["email"];
$username = $_SESSION["first_name"];

$returnArr    = array();
$responseArr = array();
$credential  = array();

$redirectUrl = "{$rootUrl}views/keywords/";


$userIp = getClientIP();

//for xml writing essential
$xmlProcessor = new xmlProcessor();

$logStorePath = $logPath["keywordPurchase"];

$xmlfilename = "buy_now.xml";
$xmlArray    = initializeXMLLog(trim(urldecode($email)));

$xml_data['request']["data"]      = '';
$xml_data['request']["attribute"] = $xmlArray["request"];


// initialize log step number variable to increase dynamically.
$i = 0;
$m = 0;

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

if(!empty($_SESSION)){

    if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){

        if(!isset($_SESSION["verify_status"])){

            $xml_data['step']["data"] = 'Buy Now';

            $unique_id = md5(uniqid(rand(), true));
            $xmlArray['activity']['id'] = $unique_id;
            $_SESSION['activity_id'] = $unique_id;

            $_SESSION['redirect_url'] = $returnURl;
            $msg = "Start {$xmlHeader} process.";
            $xml_data['step'.$i]["data"] = $i.". {$msg}";

            // create search database connection
            $searchDbConn = createDBConnection('dbsearch');
            if(noError($searchDbConn)){

                $searchDbConn = $searchDbConn["connection"];

                $kycDetails = getCurrentKYCLimits($searchDbConn,$email);
                if(noError($kycDetails)){
                    $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];

                    // close search database connection
                    mysqli_close($searchDbConn);

                    $kwdDbConn = createDBConnection('dbkeywords');
                    if(noError($kwdDbConn)){
                        $kwdDbConn = $kwdDbConn["connection"];

                        $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

                        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
                        if(noError($getUserDetails)){

                            $getUserBalance = calculateUserBalance($getUserDetails);
                            if(noError($getUserBalance)){

                                $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];

                                $KeywordDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                                $KeywordOwnerEmail  = $KeywordDetails["errMsg"]["buyer_id"];
                                $KeywordAskPrice    = number_format((float) $KeywordDetails["errMsg"]["ask_price"], 8);
                                $highestBidId       = $KeywordDetails["errMsg"]["highest_bid_id"];
                                $highestBidAmt      = number_format((float) $KeywordDetails["errMsg"]["highest_bid_amount"], 8);
                                $activBids          = $KeywordDetails["errMsg"]["active_bids"];
                                $previousOwners     = $KeywordDetails["errMsg"]["previous_owner"];
                                $kwdLastTradedPrice = number_format((float) $KeywordDetails["errMsg"]["kwd_price"], 8);
                                $kwdLastTradedTime  = $KeywordDetails["errMsg"]["purchase_timestamp"];
                                $askTransactionId   = $KeywordDetails["errMsg"]["ask_transaction_id"];

                                if($KeywordAskPrice > 0){

                                    //get Admin Settings
                                    $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);

                                    $tradingCommPercent = $tradingCommPercent;
                                    $kwdRenewalAmt = $adminSettings["data"]["kwd_renewal_fees_per_year"];

                                    //gross bid amount
                                    $tradingCommAmt = ($KeywordAskPrice * ($tradingCommPercent / 100));
                                    $kwdRenewalAmt = number_format((float)$kwdRenewalAmt, 8);
                                    //adding renewal fees in buy gross price
                                    $grossBuyPrice  = $KeywordAskPrice + $tradingCommAmt + $kwdRenewalAmt;

                                    if ($userAvailableBalance >= $grossBuyPrice) {

                                        $errMsg = "Success : user balance is greater then the ask amount";
                                        $xml_data['step'.++$m]["data"] = $m.". Success : user balance is greater then the ask amount";
                                        $returnArr["errCode"] = -2;
                                        $returnArr["errMsg"] = $errMsg;

                                        //redirecting to 2FA page
                                        $msg = "Success : Redirecting to 2FA page";
                                        $xml_data['step'.++$m]["data"] = $m.". {$msg}";
                                        $returnArr["errCode"] = -2;
                                        $_SESSION['form_data'] = json_encode($_POST);
                                        $_SESSION['xml_step'] = $m;

                                        $_SESSION['xml_file'] = $logStorePath . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;

                                    }else{
                                        $errMsg    = "You have insufficient balance to buy this keyword";
                                        $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                                        $returnArr = setErrorStack($returnArr, 24, $errMsg, $extraArgs);
                                    }
                                }else{
                                    $errMsg    = "Currently this keyword is not available to buy at Ask price.";
                                    $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                                    $returnArr = setErrorStack($returnArr, 23, $errMsg, $extraArgs);
                                }
                            }else{
                                $errMsg = 'Error: unable to calculate user available balance';
                                $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                            }
                        }else{
                            $errMsg = 'Error: unable to get user info';
                            $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                        }
                    }else{
                        $errMsg = 'Error:  unable to connect keyword database';
                        $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                        $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                    }
                }else{
                    $errMsg = 'Error: unable to get current kyc level';
                    $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                    $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
                }
            }else{
                $errMsg = 'Error: unable to connect search database';
                $xml_data['step'.++$m]["data"] = $m.". {$errMsg}";
                $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
            }

            // create or update xml log Files
            $xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

            echo json_encode($returnArr); die;

        }else if($returnURl == $_SESSION['redirect_url']){


            $stepCounter = $_SESSION["xml_step"];
            $security_type = $_SESSION['security_type'];
            $form_data = json_decode($_SESSION['form_data'], true);
            $keyword = trim(urldecode($form_data["buyNowKeyword"]));
            $activity_id = $_SESSION['activity_id'];
            $xml_file = $_SESSION['xml_file'];
            $i = $_SESSION['xml_step'];

            unset($_SESSION['security_type']);
            unset($_SESSION['xml_step']);
            unset($_SESSION['form_data']);
            unset($_SESSION['activity_id']);
            unset($_SESSION['redirect_url']);
            unset($_SESSION['xml_file']);
            unset($_SESSION['verify_status']);

            // create search database connection
            $searchDbConn = createDBConnection('dbsearch');
            if (noError($searchDbConn)) {
                $searchDbConn = $searchDbConn["connection"];

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Success : Search database connection';

                $errMsg    = 'Success : Search database connection';
               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $errMsg;

                // get current exchange rate
                $currentExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
                if (noError($currentExchangeRate)) {
                    $currentExchangeRate = $currentExchangeRate["exchange_rate"];
                    // convert array into json encode
                    $currentExchangeRate = json_encode($currentExchangeRate);

                    $kycDetails = getCurrentKYCLimits($searchDbConn,$email);
                    // $tradingCommPercent = $adminSettings["data"]["trading_commision_percent"];
                    $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Success : Current exchange rate ' . $currentExchangeRate;

                    $errMsg    = 'Success : Current exchange rate ' . $currentExchangeRate;
                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $errMsg;

                } else {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Failed : Current exchange rate ' . $currentExchangeRate;

                    $errMsg    = ' Failed : Current exchange rate ' . $currentExchangeRate;
                 //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                    $returnArr["errCode"] = 99;
                    $returnArr["errMsg"] = $errMsg;
                }
            } else {

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Failed : Search database connection';

                $errMsg    = 'Failed : Search database connection';
                //$returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                $returnArr["errCode"] = 98;
                $returnArr["errMsg"] = $errMsg;

                //print('Database connection error');
            }

            // close search database connection
            mysqli_close($searchDbConn);

            //create database connection
            $conn = createDBConnection("dbkeywords");
            if (noError($conn)) {
                $conn = $conn["connection"];

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Success : keywords db connection ';

                $errMsg    = 'Success: keywords db connection ';
              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $errMsg;
            } else {

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Failed : keywords db connection ';

                $errMsg    = 'Failed : keywords db connection ';
               // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                $returnArr["errCode"] = 97;
                $returnArr["errMsg"] = $errMsg;

                // print("Database Error");
            }

            $userDetails = $userRequiredFields . ",_id";

            $userInfo = getUserInfo($email, $walletURLIP . 'api/v3/', $userDetails);

            if (noError($userInfo)) {
                $userInfo  = $userInfo["errMsg"];
                $userId    = $userInfo["_id"];
                $firstName = $userInfo["first_name"];
                $lastName  = $userInfo["last_name"];

                $errMsg    = 'Success: Get ' . $email . " info";
               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $errMsg;

            } else {
                $errMsg    = 'Failed: Get ' . $email . " info";
                $errCode   = $userInfo["errCode"];
                $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);

                $returnArr["errCode"] = 96;
                $returnArr["errMsg"] = $errMsg;
            }

            $result = checkForKeywordOwnershipLock($keyword, $conn);

            if (noError($result)) {

                // get user available balance
               // $getUserBalance = getUserWalletAvalibleBalance($email);

                $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

                $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
                if(noError($getUserDetails)){
                    $getUserBalance = calculateUserBalance($getUserDetails);

                        if (noError($getUserBalance)) {

                            $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . '. Success : getting user current wallet available balance: ' . $userAvailableBalance;


                            $KeywordDetails = getKeywordOwnershipDetails($conn, $keyword);

                            $KeywordOwnerEmail  = $KeywordDetails["errMsg"]["buyer_id"];
                            $KeywordAskPrice    = number_format((float) $KeywordDetails["errMsg"]["ask_price"], 8);
                            $highestBidId       = $KeywordDetails["errMsg"]["highest_bid_id"];
                            $highestBidAmt      = number_format((float) $KeywordDetails["errMsg"]["highest_bid_amount"], 8);
                            $activBids          = $KeywordDetails["errMsg"]["active_bids"];
                            $previousOwners     = $KeywordDetails["errMsg"]["previous_owner"];
                            $kwdLastTradedPrice = number_format((float) $KeywordDetails["errMsg"]["kwd_price"], 8);
                            $kwdLastTradedTime  = $KeywordDetails["errMsg"]["purchase_timestamp"];
                            $askTransactionId   = $KeywordDetails["errMsg"]["ask_transaction_id"];

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . '. Success : getKeywordOwnershipDetails';


                            $credential["current_session_user"]      = $email;
                            $credential["buyer_id"]                  = $KeywordOwnerEmail;
                            $credential["keyword"]                   = $keyword;
                            $credential["keyword_ask_price"]         = $KeywordAskPrice;
                            $credential["highest_bid_id"]            = $highestBidId;
                            $credential["highest_bid_amount"]        = $highestBidAmt;
                            $credential["active_bids"]               = $activBids;
                            $credential["previous_owner"]            = $previousOwners;
                            $credential["keyword_last_traded_price"] = $kwdLastTradedPrice;
                            $credential["keyword_last_traded_time"]  = $kwdLastTradedTime;
                            $credential["ask_transaction_id"]        = $askTransactionId;

                            if($KeywordAskPrice > 0){

                                //get Admin Settings
                                $adminSettings = getAdminSettingsFromKeywordAdmin($conn);

                                // $tradingCommPercent = $adminSettings["data"]["trading_commision_percent"];
                                $tradingCommPercent = $tradingCommPercent;
                                $kwdRenewalAmt = $adminSettings["data"]["kwd_renewal_fees_per_year"];

                                //gross bid amount
                                $tradingCommAmt = ($KeywordAskPrice * ($tradingCommPercent / 100));
                                $kwdRenewalAmt = number_format((float)$kwdRenewalAmt, 8);
                                //adding renewal fees in buy gross price
                                $grossBuyPrice  = $KeywordAskPrice + $tradingCommAmt + $kwdRenewalAmt;

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . ". Success : getAdminSettings| tradingCommPercent:" . urlencode($tradingCommPercent) . ",tradingCommAmt:" . urlencode($tradingCommAmt) . ",grossBuyPrice:" . urlencode($grossBuyPrice);


                                //get ask details
                                $askDetails = explode("/", $askTransactionId);

                                $askTableName = $askDetails[0];
                                $ownerEmail   = $askDetails[1];

                                $credential["ask_table_name"]      = $askTableName;
                                $credential["keyword_owner_email"] = $ownerEmail;

                                if ($userAvailableBalance >= $grossBuyPrice) {

                                    $i                             = $i + 1;
                                    $xml_data["step" . $i]["data"] = $i . ". Success : User available balance is greater then the gross buy price";

                                    //new active bids json
                                    $activBids        = json_decode($activBids, TRUE);
                                    $activBids        = array_values($activBids);
                                    $newActivBidsJson = json_encode($activBids, JSON_UNESCAPED_UNICODE);

                                    $newActivBidsJsonName = "reject_bid_queue.json";
                                    if (!empty($newActivBidsJson) || $newActivBidsJson != null) {


                                        // $writeActiveBid = writeActiveBidToJson($newActivBidsJsonName, $newActivBidsJson);
                                        $writeActiveBid = addRejectActiveBids($newActivBidsJson, $conn);

                                        if (noError($writeActiveBid)) {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Success : Insert reject bid details in reject_bid_queue';

                                            $errMsg    = 'Success : Insert reject bid details in reject_bid_queue ';
                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                            $returnArr["errCode"] = -1;
                                            $returnArr["errMsg"] = $errMsg;
                                        } else {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Failed : Insert reject bid details in reject_bid_queue';

                                            $errMsg    = 'Failed :  Insert reject bid details in reject_bid_queue';
                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                            $returnArr["errCode"] = 95;
                                            $returnArr["errMsg"] = $errMsg;

                                        }
                                    }
                                    //make json for previousOwners
                                    $previousOwners   = json_decode($previousOwners, TRUE);
                                    $previousOwners[] = array(
                                        'buyer_id' => $KeywordOwnerEmail,
                                        'kwd_purchase_price' => $kwdLastTradedPrice,
                                        'kwd_purchase_timestamp' => $kwdLastTradedTime,
                                        'kwd_last_traded_price' => $KeywordAskPrice,
                                        'kwd_new_owner' => $email
                                    );
                                    //json for previous owners
                                    $previousOwners   = json_encode($previousOwners, JSON_UNESCAPED_UNICODE);

                                    $credential["previous_owner_with_new_owner"] = $previousOwners;

                                    $trans = startTransaction($conn);

                                    if (noError($trans)) {

                                        $i                             = $i + 1;
                                        $xml_data["step" . $i]["data"] = $i . ". Success : start transaction";


                                        $query  = "UPDATE " . $askTableName . " SET ask_status = '2' WHERE ask_transaction_id='" . $askTransactionId . "'";
                                        $result = runQuery($query, $conn);

                                        if (noError($result)) {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . ". Success : upadating status in ask table";

                                            $errMsg    = "Success : upadating status in ask table";
                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                            $returnArr["errCode"] = -1;
                                            $returnArr["errMsg"] = $errMsg;

                                            //get keyword table name
                                            $ownershipTableName = getKeywordOwnershipTableName($keyword);

                                            $query = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='',highest_bid_amount='',no_of_active_bids='',active_bids='',ask_transaction_id='',ask_price='',order_status='0',previous_owner ='" . $previousOwners . "',buyer_id='" . $email . "',kwd_price='" . $KeywordAskPrice . "',purchase_timestamp = now(),ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . $keyword . "'";

                                            $result_ownership = runQuery($query, $conn);
                                            //change bid_status from active to reject for rest active bids
                                            $rejectBidsArr    = json_decode($newActivBidsJson, true);
                                            $no_i             = 1;
                                            $no_j             = 1;
                                            $no_k             = 1;

                                            foreach ($rejectBidsArr as $value) {
                                                $rejectBidDetails   = explode("~~", $value);
                                                $rejectBidTableName = $rejectBidDetails[0];
                                                $rejectBidderEmail  = $rejectBidDetails[1];

                                                $credential["reject_bid"][$no_i]["reject_bid_table_name"] = $rejectBidTableName;
                                                $credential["reject_bid"][$no_i]["reject_bidder_email"]   = $rejectBidderEmail;

                                                $query  = "UPDATE " . $rejectBidTableName . " SET bid_status = '2' WHERE bid_transaction_id='" . $value . "'";
                                                $result = runQuery($query, $conn);
                                                if (noError($result)) {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . ". Success :  update rejectBidTable, 1] Reject bid table name: " . $rejectBidTableName . "  2] bid_status = 2";

                                                    $errMsg    = "Success : update rejectBidTable, 1] Reject bid table name: " . $rejectBidTableName . "  2] bid_status = 2";
                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;
                                                } else {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . ". Failed :  update rejectBidTable, 1] Reject bid table name: " . $rejectBidTableName . "  2] bid_status = 2";

                                                    $errMsg    = "Failed : update rejectBidTable, 1] Reject bid table name: " . $rejectBidTableName . "  2] bid_status = 2";
                                                   // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = 94;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }
                                                //delete from active bids of bidder and add to accepted bids
                                                $deductActiveBidsCountOfUser = deductActiveBidsCountOfUser($rejectBidderEmail, $conn);

                                                if (noError($deductActiveBidsCountOfUser)) {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Success :  deductActiveBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Success : deductActiveBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;
                                                } else {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Failed :  deductActiveBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Failed : deductActiveBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                   // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = 93;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }
                                                $addRejectedBidsCountOfUser = addRejectedBidsCountOfUser($rejectBidderEmail, $conn);
                                                if (noError($addRejectedBidsCountOfUser)) {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Success :  addRejectedBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Success : addRejectedBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                  //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                } else {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Failed :  addRejectedBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Failed : addRejectedBidsCountOfUser|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = 92;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }
                                                $removeBidDetailsByBuyerId = removeBidDetailsByBuyerId($conn, $rejectBidderEmail, $value, $keyword);

                                                if (noError($removeBidDetailsByBuyerId)) {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Success :  update bid details of patticular user|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Success : update bid details of patticular user|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                } else {
                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . "." . $no_i . "." . $no_k . ". Failed :  update bid details of patticular user|rejectBidderEmail:" . urlencode($rejectBidderEmail);

                                                    $errMsg    = "Failed : update bid details of patticular user|rejectBidderEmail:" . urlencode($rejectBidderEmail);
                                                 //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = 91;
                                                    $returnArr["errMsg"] = $errMsg;
                                                }
                                                $no_i++;
                                                $no_j++;
                                                $no_k++;
                                            } // end of foreach loop

                                            if (noError($result_ownership)) {
                                                $purchase_timestamp = date("j F, Y");
                                                $result             = updateKwdDetailsAfterTrade($conn, $email, $KeywordOwnerEmail, $purchase_timestamp, $keyword, $KeywordAskPrice, "trade");

                                                if (noError($result)) {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . ". Success : updateKwdDetailsAfterTrade";

                                                    $errMsg    = "Success :  updateKwdDetailsAfterTrade";
                                                  //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                    //$result   = addPurchases($email, $KeywordAskPrice);
                                                    $result = creditUserEarning($userId, $KeywordAskPrice, "purchase", "add");

                                                    $credential["add_purchase_amount"] = $result["errMsg"];

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . ". Success : Credit purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                        $errMsg    = "Success :  Credit purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        //$result   = addTrade($email, $tradingCommAmt);
                                                        $result = creditUserEarning($userId, $tradingCommAmt, "trade", "add");

                                                        $credential["add_trade_amount"] = $result["errMsg"];

                                                        if (noError($result)) {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . ". Success : Credit trade amount  | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                            $errMsg    = "Success :  Credit trade amount  | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                            //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                            $returnArr["errCode"] = -1;
                                                            $returnArr["errMsg"] = $errMsg;


                                                            // credit renewal amount to buyer account

                                                            $creditRenewalAmtToBuyer = creditUserEarning($userId, $kwdRenewalAmt, "renewalfees", "add");

                                                            $credential["increase_renewal_fees_earning"] = $creditRenewalAmtToBuyer["errMsg"];

                                                            $response = json_encode($creditRenewalAmtToBuyer);

                                                            if(noError($creditRenewalAmtToBuyer)){

                                                                // credit trade fees earning to pool user.
                                                                $result = creditUserEarning($communityPoolUserId, $tradingCommAmt, "trade", "add");

                                                                $credential["increase_total_fees_earning"] = $result["errMsg"];


                                                                if (noError($result)) {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Success : credit total fees earning | tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                    $errMsg    = "Success : credit total fees earning | tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                    $returnArr["errCode"] = -1;
                                                                    $returnArr["errMsg"] = $errMsg;

                                                                    // credit renewal fees earning to pool user.
                                                                    $creditRenewalAmtToPool = creditUserEarning($communityPoolUserId, $kwdRenewalAmt, "renewalfees", "add");

                                                                    if(noError($creditRenewalAmtToPool)){


                                                                        // insert user transaction for trade fees.
                                                                        // parameters required to insert in transaction
                                                                        $recipientEmail     = $communityPoolUser;
                                                                        $recipientUserId    = $communityPoolUserId;
                                                                        $senderEmail        = $email;
                                                                        $senderUserId       = $userId;
                                                                        $amount             = $tradingCommAmt;
                                                                        $type               = "trade_fees";
                                                                        $paymentMode        = 'ITD';
                                                                        $originIp           = $userIp;
                                                                        $exchangeRateInJson = $currentExchangeRate;

                                                                        $metaDetails = array(
                                                                            "sender" => $email,
                                                                            "receiver" => $communityPoolUser,
                                                                            "trade_amount" => $tradingCommAmt,
                                                                            "ask_transaction_id" => $askTransactionId,
                                                                            "reject_bids" => json_encode($activBids),
                                                                            "keyword" => urlencode(utf8_encode($keyword)),
                                                                            "discount" => 0,
                                                                            "commision" => 0,
                                                                            "description" => "The trading fees amount earned by " . $communityPoolUser . " of trade amount " . $tradingCommAmt . " ".$keywoDefaultCurrencyName." from " . $email
                                                                        );

                                                                        $result = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                        $credential["insert_user_transaction_fees_earning"]["sender"]                = $email;
                                                                        $credential["insert_user_transaction_fees_earning"]["receiver"]              = $communityPoolUser;
                                                                        $credential["insert_user_transaction_fees_earning"]["trading_amount"]        = $tradingCommAmt;
                                                                        $credential["insert_user_transaction_fees_earning"]["payment_mode"]          = $paymentMode;
                                                                        $credential["insert_user_transaction_fees_earning"]["exchange_rate_in_json"] = $exchangeRateInJson;

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : calling insertUserTransaction API for community pool user fees earning";

                                                                            $errMsg    = "Success : calling insertUserTransaction API for community pool user fees earning";
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;


                                                                            // insert user transaction for renewal fees.
                                                                            // parameters required to insert in transaction
                                                                            $recipientEmail     = $communityPoolUser;
                                                                            $recipientUserId    = $communityPoolUserId;
                                                                            $senderEmail        = $email;
                                                                            $senderUserId       = $userId;
                                                                            $amount             = $kwdRenewalAmt;
                                                                            $type               = "renewal_fees";
                                                                            $paymentMode        = 'ITD';
                                                                            $originIp           = $userIp;
                                                                            $exchangeRateInJson = $currentExchangeRate;

                                                                            $metaDetails = array(
                                                                                "sender" => $email,
                                                                                "receiver" => $communityPoolUser,
                                                                                "trade_amount" => $kwdRenewalAmt,
                                                                                "ask_transaction_id" => $askTransactionId,
                                                                                "reject_bids" => json_encode($activBids),
                                                                                "keyword" => urlencode(utf8_encode($keyword)),
                                                                                "discount" => 0,
                                                                                "commision" => 0,
                                                                                "description" => "The trading fees amount earned by " . $communityPoolUser . " of trade amount " . $kwdRenewalAmt . " ".$keywoDefaultCurrencyName." from " . $email
                                                                            );

                                                                            $insertTransForRenewal = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                            $credential["insert_user_transaction_renewal_fees_earning"]["sender"]                = $email;
                                                                            $credential["insert_user_transaction_renewal_fees_earning"]["receiver"]              = $communityPoolUser;
                                                                            $credential["insert_user_transaction_renewal_fees_earning"]["trading_amount"]        = $kwdRenewalAmt;
                                                                            $credential["insert_user_transaction_renewal_fees_earning"]["payment_mode"]          = $paymentMode;
                                                                            $credential["insert_user_transaction_renewal_fees_earning"]["exchange_rate_in_json"] = $exchangeRateInJson;

                                                                            $response = json_encode($insertTransForRenewal);

                                                                            if(noError($insertTransForRenewal)){
                                                                                $i                             = $i + 1;
                                                                                $xml_data["step" . $i]["data"] = $i . ". Success : calling insertUserTransaction API for community pool user for renewal fees earning.";

                                                                                $errMsg    = "Success : calling insertUserTransaction API for community pool user for renewal fees earning.";
                                                                               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                                $returnArr["errCode"] = -1;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                            }else{

                                                                                $i                             = $i + 1;
                                                                                $xml_data["step" . $i]["data"] = $i . ". Failed : calling insertUserTransaction API for community pool user for renewal fees earning. |response:" . urlencode($response);

                                                                                $errMsg    = "Failed : calling insertUserTransaction API for community pool user for renewal fees earning. |response:" . urlencode($response);
                                                                              //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                                $returnArr["errCode"] = 89;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                            }

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : calling insertUserTransaction API for community pool user fees earning";

                                                                            $errMsg    = "Failed : calling insertUserTransaction API for community pool user fees earning. |response:" . urlencode($response);
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 88;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }


                                                                        //commmit transaction
                                                                        $result = commitTransaction($conn);
                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : commitTransaction";

                                                                            $errMsg    = "Success : commit transaction";
                                                                            //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;
                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : commitTransaction";

                                                                            $errMsg    = "Failed : commit transaction";
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 87;
                                                                            $returnArr["errMsg"] = $errMsg;
                                                                        }



                                                                        // Enable database default autocommit functionality
                                                                        $result = mysqli_autocommit($conn, TRUE);
                                                                        if (!$result) {
                                                                            $returnArr["errCode"] = 7;
                                                                            $returnArr["errMsg"]  = "Could not start transaction: " . mysqli_error($conn);

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : Could not start transaction: ' . mysqli_error($conn);


                                                                        } else {
                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"]  = "Transaction started";

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : Transaction started';

                                                                        }

                                                                        //clear the row-locking on keyword ownerhip table
                                                                        $clearKeywordOwnershipLock = clearKeywordOwnershipLock($keyword, $conn);

                                                                        if (noError($clearKeywordOwnershipLock)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : clear keyword ownerhip lock';

                                                                            $errMsg    = "Success : clear keyword ownerhip lock";
                                                                            //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : clear keyword ownerhip lock';

                                                                            $errMsg    = "Failed : clear keyword ownerhip lock";
                                                                         //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 86;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        // add to latest trade table
                                                                        $addToLatestTrade = addToLatestTrade($conn, $keyword, $email, $KeywordAskPrice);

                                                                        if (noError($addToLatestTrade)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : addToLatestTrade';

                                                                            $errMsg    = "Success : addToLatestTrade";
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : addToLatestTrade';

                                                                            $errMsg    = "Failed : addToLatestTrade";
                                                                            //$returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 85;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        //delete from highest ask table
                                                                        $deleteFromHighestAsk = deleteFromHighestAskByKeyword($conn, $keyword);

                                                                        if (noError($deleteFromHighestAsk)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromHighestAsk';

                                                                            $errMsg    = "Success : deleteFromHighestAsk";
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromHighestAsk';

                                                                            $errMsg    = "Failed : deleteFromHighestAsk";
                                                                          //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 83;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        //delete from most bid table
                                                                        $deleteFromMostBid = deleteFromMostBidByKeyword($conn, $keyword);

                                                                        if (noError($deleteFromMostBid)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromMostBid';

                                                                            $errMsg    = "Success : deleteFromMostBid";
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromMostBid';

                                                                            $errMsg    = "Failed : deleteFromMostBid";
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 82;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        //delete from latest ask
                                                                        $deleteFromLatestAsk = deleteFromLatestAskByKeyword($conn, $keyword);

                                                                        if (noError($deleteFromLatestAsk)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromLatestAsk';

                                                                            $errMsg    = "Success : deleteFromLatestAsk";
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromLatestAsk';

                                                                            $errMsg    = "Failed : deleteFromLatestAsk";
                                                                          //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 81;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        //delete from latest bid
                                                                        $deleteFromLatestBid = deleteFromLatestBidByKeyword($conn, $keyword);

                                                                        if (noError($deleteFromLatestBid)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromLatestBid';

                                                                            $errMsg    = "Success : deleteFromLatestBid";
                                                                            //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromLatestBid';

                                                                            $errMsg    = "Failed : deleteFromLatestBid";
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 80;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $deleteFromLatestBidByKeywordAll = deleteFromLatestBidByKeywordAll($conn, $keyword);

                                                                        if (noError($deleteFromLatestBid)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromLatestBidByKeywordAll';

                                                                            $errMsg    = "Success : deleteFromLatestBidByKeywordAll";
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;
                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromLatestBidByKeywordAll';

                                                                            $errMsg    = "Failed : deleteFromLatestBidByKeywordAll";
                                                                          //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 79;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $deleteFromLatestAskByKeywordAll = deleteFromLatestAskByKeywordAll($conn, $keyword);

                                                                        if (noError($deleteFromLatestBid)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : deleteFromLatestAskByKeywordAll';

                                                                            $errMsg    = "Success : deleteFromLatestAskByKeywordAll";
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : deleteFromLatestAskByKeywordAll';

                                                                            $errMsg    = "Failed : deleteFromLatestAskByKeywordAll";
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 78;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $kwdOwnerInfoReq = $userRequiredFields . ",_id";
                                                                        $keywordUserInfo = getUserInfo($KeywordOwnerEmail, $walletURLIP . 'api/v3/', $kwdOwnerInfoReq);

                                                                        $response = json_encode($keywordUserInfo);
                                                                        if (noError($keywordUserInfo)) {
                                                                            $keywordUserInfo   = $keywordUserInfo["errMsg"];
                                                                            $kwdOwnerId        = $keywordUserInfo["_id"];
                                                                            $kwdOwnerFirstName = $keywordUserInfo["first_name"];
                                                                            $kwdOwnerLastName  = $keywordUserInfo["last_name"];

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Success : Getting  ' . $KeywordOwnerEmail ;

                                                                            $errMsg    = "Success :  Getting  '.$KeywordOwnerEmail. ' info - response : '. urlencode($response)";
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . '. Failed : Getting  ' . $KeywordOwnerEmail . ' info';

                                                                            $errMsg    = "Failed :  Getting  ".$KeywordOwnerEmail;
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 76;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                            //print('Error: fetching buyer details');
                                                                        }


                                                                        //$result   = addSales($KeywordOwnerEmail, $KeywordAskPrice);
                                                                        $result                         = creditUserEarning($kwdOwnerId, $KeywordAskPrice, "sales", "add");
                                                                        $credential["add_sales_amount"] = $result["errMsg"];
                                                                        $response                       = json_encode($result);
                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : Credit sales amount |KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                            $errMsg    = "Success : Credit sales amount |KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                            //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : Credit sales amount |KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                            $errMsg    = "Failed : Credit sales amount |KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 75;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        // parameters required to insert in transaction
                                                                        $recipientEmail     = $KeywordOwnerEmail;
                                                                        $recipientUserId    = $kwdOwnerId;
                                                                        $senderEmail        = $email;
                                                                        $senderUserId       = $userId;
                                                                        $amount             = $KeywordAskPrice;
                                                                        $type               = "trade";
                                                                        $paymentMode        = 'ITD';
                                                                        $originIp           = $userIp;
                                                                        $exchangeRateInJson = $currentExchangeRate;

                                                                        $metaDetails = array(
                                                                            "sender" => $email,
                                                                            "receiver" => $KeywordOwnerEmail,
                                                                            "ask_amount" => $KeywordAskPrice,
                                                                            "ask_transaction_id" => $askTransactionId,
                                                                            "reject_bids" => json_encode($activBids),
                                                                            "keyword" => urlencode(utf8_encode($keyword)),
                                                                            "discount" => 0,
                                                                            "commision" => 0
                                                                        );

                                                                        $result = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                        $credential["insert_user_transaction_for_keyword_trade"]["sender"]            = $email;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["receiver"]          = $KeywordOwnerEmail;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["keyword_ask_price"] = $KeywordAskPrice;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["type"]              = $type;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["payment_mode"]      = $paymentMode;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["exchange_rate"]     = $exchangeRateInJson;
                                                                        $credential["insert_user_transaction_for_keyword_trade"]["meta_details"]      = $metaDetails;

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : calling insertUserTransaction API|email:" . urlencode($email);

                                                                            $errMsg    = "Success :  calling insertUserTransaction API|email:" . urlencode($email);
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : calling insertUserTransaction API|email:" . urlencode($email);

                                                                            $errMsg    = "Failed :  calling insertUserTransaction API|email:" . urlencode($email);
                                                                            $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 74;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $activBids = json_decode($activBids, TRUE);
                                                                        $no_b      = 1;
                                                                        foreach ($activBids as $value) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . "." . $no_b . ". Success : Bid-ID: '" . urlencode($value);

                                                                            $no_b++;
                                                                        }

                                                                        $result = getRevenueDetailsByKeyword($conn, $keyword);

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : getRevenueDetailsByKeyword|keyword:" . urlencode($keyword);

                                                                            $errMsg    = "Success :  getRevenueDetailsByKeyword|keyword:" . urlencode($keyword);
                                                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : getRevenueDetailsByKeyword|keyword:" . urlencode($keyword);

                                                                            $errMsg    = "Success :  getRevenueDetailsByKeyword|keyword:" . urlencode($keyword);
                                                                            $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 73;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $user_kwd_search_count       = $result["errMsg"]["user_kwd_search_count"];
                                                                        $user_kwd_ownership_earnings = $result["errMsg"]["user_kwd_ownership_earnings"];

                                                                        $credential["user_keyword_search_count"]   = $user_kwd_search_count;
                                                                        $credential["user_kwd_ownership_earnings"] = $user_kwd_ownership_earnings;

                                                                        $result = deleteUserStatsFromRevenueTable($conn, $keyword);

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : deleteUserStatsFromRevenueTable|keyword:" . urlencode($keyword);

                                                                            $errMsg    = "Success :  deleteUserStatsFromRevenueTable|keyword:" . urlencode($keyword);
                                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : deleteUserStatsFromRevenueTable|keyword:" . urlencode($keyword);

                                                                            $errMsg    = "Success :  deleteUserStatsFromRevenueTable|keyword:" . urlencode($keyword);
                                                                          //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 72;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $URLTradingHistory = $docRoot . "json_directory/keywords/trading_history/";
                                                                        if (!is_dir($URLTradingHistory)) {
                                                                            mkdir($URLTradingHistory, 0777, true);
                                                                        }

                                                                        $URLTradingHistory = $URLTradingHistory . $KeywordOwnerEmail . ".json";
                                                                        if (!file_exists($URLTradingHistory)) {
                                                                            //create new array
                                                                            $tradingHistory   = array();
                                                                            $tradingHistory[] = array(
                                                                                'buyer_id' => $KeywordOwnerEmail,
                                                                                'keyword' => $keyword,
                                                                                'kwd_purchase_price' => $kwdLastTradedPrice,
                                                                                'kwd_purchase_timestamp' => $kwdLastTradedTime,
                                                                                'kwd_last_traded_price' => $KeywordAskPrice,
                                                                                'kwd_new_owner' => $email,
                                                                                'user_kwd_search_count' => $user_kwd_search_count,
                                                                                'user_kwd_ownership_earnings' => $user_kwd_ownership_earnings
                                                                            );

                                                                            $tradingHistory = json_encode($tradingHistory, JSON_UNESCAPED_UNICODE);
                                                                            //put content in json file
                                                                            file_put_contents($URLTradingHistory, $tradingHistory);
                                                                        } else {
                                                                            $tradingHistory   = file_get_contents($URLTradingHistory);
                                                                            $tradingHistory   = json_decode($tradingHistory, TRUE);
                                                                            $tradingHistory[] = array(
                                                                                'buyer_id' => $KeywordOwnerEmail,
                                                                                'keyword' => $keyword,
                                                                                'kwd_purchase_price' => $kwdLastTradedPrice,
                                                                                'kwd_purchase_timestamp' => $kwdLastTradedTime,
                                                                                'kwd_last_traded_price' => $KeywordAskPrice,
                                                                                'kwd_new_owner' => $email,
                                                                                'user_kwd_search_count' => $user_kwd_search_count,
                                                                                'user_kwd_ownership_earnings' => $user_kwd_ownership_earnings
                                                                            );
                                                                            $tradingHistory   = json_encode($tradingHistory, JSON_UNESCAPED_UNICODE);
                                                                            file_put_contents($URLTradingHistory, $tradingHistory);
                                                                        }

                                                                        $insertTradeProfit = insertTradeProfit($keyword, $KeywordOwnerEmail, $kwdLastTradedPrice, $email, $KeywordAskPrice, $user_kwd_ownership_earnings, $conn);
                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : Insert trade profit";

                                                                            $errMsg    = "Success :  Insert trade profit";
                                                                         //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : Insert trade profit";

                                                                            $errMsg    = "Failed :  Insert trade profit";
                                                                          //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 70;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $to                   = $email;
                                                                        $subject              = "Keyword purchase success";
                                                                        $message              =
                                                                            '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                                    <tr> <td style="padding: 10px 20px;">
                                                                                       You have successfully purchased the license for <b> #'.$keyword.'</b> via keywo Wallet
                                                                                    <br><br>Regards,<br>Team Keywo
                                                                                    </td>  </tr>';

                                                                        //You have successfully purchased the license for #keyword via SearchTrade Wallet
                                                                        $notification_message = 'You have successfully purchased the license for <b>#' . $keyword . '</b> via Keywo Wallet.';
                                                                        $method_name          = "GET";

                                                                        $getNotification = notifyoptions($userId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                                        $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];

                                                                        $path    = "";
                                                                        $keyword = $keyword;

                                                                        $category   = "buy";
                                                                        $linkStatus = 1;

                                                                        $notiSend = sendNotificationBuyPrefrence($to, $subject, $message, $firstName, $lastName, $userId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                        if (noError($notiSend)) {
                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success : send email to " . $email;

                                                                            $errMsg    = "Success :  send email to " . $email;
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                            $linkStatus = 1;

                                                                            //send mail for keyword owner
                                                                            $to      = $KeywordOwnerEmail;
                                                                            $subject = "Keywo: Keyword Licence cancellation Confirmation";
                                                                            $message =
                                                                                '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$kwdOwnerFirstName.',</td>                                                    
                                                                                    <tr> <td style="padding: 10px 20px;">
                                                                                       Your ask price'.number_format((float) $KeywordAskPrice,4).' '.$keywoDefaultCurrencyName.' accepted on your keyword<b> #'.$keyword.'</b> 
                                                                                    <br><br>Regards,<br>Team Keywo
                                                                                    </td>  </tr>';

                                                                            $notification_message = 'You sold #' . $keyword . ' for ' . number_format((float) $KeywordAskPrice, 4) . ''.$keywoDefaultCurrencyName.'.';
                                                                            $method_name          = "GET";
                                                                            $getNotification      = notifyoptions($kwdOwnerId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                                            $permissionCode       = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["0"]["permissions"]["_id"];
                                                                            $path                 = "";
                                                                            $keyword              = $keyword;

                                                                            //SendMail($to, $subject, $message, $path, $keyword);
                                                                            $notiSend = sendNotificationBuyPrefrence($to, $subject, $message, $kwdOwnerFirstName, $kwdOwnerLastName, $kwdOwnerId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                            //send ownership zip mail to admin
                                                                            sendKwdOwnershipZipToAdmin($keyword, $conn, "trade");
                                                                            if (noError($notiSend)) {
                                                                                $i                             = $i + 1;
                                                                                $xml_data["step" . $i]["data"] = $i . ". Success : send email to " . $KeywordOwnerEmail;

                                                                                $errMsg    = "Success : send email to " . $KeywordOwnerEmail;
                                                                               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                                $returnArr["errCode"] = -1;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                                //print('Success: You have successfully bought this keyword');


                                                                                /******************** for Keyword Statistics ****************/
                                                                                insert_keyword_statistics("total_kwd_traded","0",$conn);
                                                                                insert_keyword_statistics("total_keyword_traded_amount",$KeywordAskPrice,$conn);
                                                                                insert_keyword_statistics("total_keyword_traded_fees",$tradingCommAmt,$conn);
                                                                                insert_keyword_statistics("total_keyword_renewal_fees",$kwdRenewalAmt,$conn);
                                                                                /******************** for Keyword Statistics ****************/

                                                                                /******************** for logs user analytics ****************/
                                                                                $responseArr["errCode"]="-1";
                                                                                $responseArr["errMsg"]=$errMsg;
                                                                                $xml_data['response']["data"] = "";
                                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                                /******************** for logs user analytics ****************/


                                                                            } else {
                                                                                $i                             = $i + 1;
                                                                                $xml_data["step" . $i]["data"] = $i . ". Failed : send email to " . $KeywordOwnerEmail;

                                                                                //print('Failed: Unable to buy now '.$keyword);
                                                                                $errMsg    = "Failed : send email to " . $KeywordOwnerEmail;
                                                                              //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                                $returnArr["errCode"] = 71;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                                /******************** for logs user analytics ****************/
                                                                                $responseArr["errCode"]=71;
                                                                                $responseArr["errMsg"]=$errMsg;
                                                                                $xml_data['response']["data"] = "";
                                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                                /******************** for logs user analytics ****************/

                                                                            }


                                                                            // remove renew keyword from presale table
                                                                            $getPresaleDetails = getUserCartDetails($KeywordOwnerEmail, $conn);
                                                                            if(noError($getPresaleDetails)){
                                                                                $getPresaleDetails = $getPresaleDetails["errMsg"];

                                                                                $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];
                                                                                $getRenewKwd = json_decode($getRenewKwd, true);

                                                                                if(in_array($keyword, $getRenewKwd)){
                                                                                    //Remove Keyword From Cart Array
                                                                                    $getRenewKwd = array_diff($getRenewKwd, array(
                                                                                        $keyword
                                                                                    ));

                                                                                    $getRenewKwd = json_encode($getRenewKwd,JSON_UNESCAPED_UNICODE);

                                                                                    $updateRenewKwd = updateRenewKwdInPresale($KeywordOwnerEmail, $getRenewKwd, $conn);

                                                                                    if(noError($updateRenewKwd)){
                                                                                        $errMsg = "Successfully update renew keyword details";
                                                                                        //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                                        $responseArr["errCode"] = -1;
                                                                                        $responseArr["errMsg"]  = $errMsg;

                                                                                        $xml_data['response']["data"]      = '';
                                                                                        $xml_data['response']["attribute"] = $responseArr;

                                                                                        $returnArr["errCode"] = -1;
                                                                                        $returnArr["errMsg"] = $errMsg;

                                                                                        /******************** for logs user analytics ****************/
                                                                                        $responseArr["errCode"]="-1";
                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                        $xml_data['response']["data"] = "";
                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                        /******************** for logs user analytics ****************/

                                                                                    }else{
                                                                                        $errMsg    = 'Error: Update renew keyword details in presale.';
                                                                                        //   $returnArr = setErrorStack($returnArr, 28, $errMsg, $extraArgs);

                                                                                        $responseArr["errCode"] = $returnArr["errCode"];
                                                                                        $responseArr["errMsg"]  = $errMsg;

                                                                                        $xml_data['response']["data"]      = '';
                                                                                        $xml_data['response']["attribute"] = $responseArr;

                                                                                        $returnArr["errCode"] = 69;
                                                                                        $returnArr["errMsg"] = $errMsg;

                                                                                        /******************** for logs user analytics ****************/
                                                                                        $responseArr["errCode"]=69;
                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                        $xml_data['response']["data"] = "";
                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                        /******************** for logs user analytics ****************/


                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $errMsg    = 'Error: Getting presale details';
                                                                                //  $returnArr = setErrorStack($returnArr, 27, $errMsg, $extraArgs);

                                                                                $responseArr["errCode"] = $returnArr["errCode"];
                                                                                $responseArr["errMsg"]  = $errMsg;

                                                                                $xml_data['response']["data"]      = '';
                                                                                $xml_data['response']["attribute"] = $responseArr;

                                                                                $returnArr["errCode"] = 68;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                                /******************** for logs user analytics ****************/
                                                                                $responseArr["errCode"]=68;
                                                                                $responseArr["errMsg"]=$errMsg;
                                                                                $xml_data['response']["data"] = "";
                                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                                /******************** for logs user analytics ****************/
                                                                            }


                                                                            if ($returnArr["errCode"] == -1) {

                                                                                $errMsg    = "You have successfully bought #".$keyword." keyword";
                                                                                $extraArgs["redirect_url"] = "{$redirectUrl}user_dashboard/owned_keyword.php";
                                                                                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                                $responseArr["errCode"] = -1;
                                                                                $responseArr["errMsg"]  = $errMsg;

                                                                                $xml_data['response']["data"]      = '';
                                                                                $xml_data['response']["attribute"] = $responseArr;

                                                                                $_SESSION["accept_bid_error_code"] = -1;
                                                                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                                $returnArr["errCode"] = -1;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                            } else {

                                                                                $i         = $i + 1;
                                                                                $errMsg    = "There was an error while buying this keyword";
                                                                                $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                                $responseArr["errCode"] = $returnArr["errCode"];
                                                                                $responseArr["errMsg"]  = $returnArr["errMsg"];

                                                                                $xml_data['response']["data"]      = '';
                                                                                $xml_data['response']["attribute"] = $responseArr;

                                                                                $_SESSION["accept_bid_error_code"] = 26;
                                                                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                                $returnArr["errCode"] = 69;
                                                                                $returnArr["errMsg"] = $errMsg;

                                                                            }

                                                                        } else {
                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : send email to " . $email;

                                                                            $errMsg    = "Failed : send email to " . $email;
                                                                            $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $_SESSION["accept_bid_error_code"] = 26;
                                                                            $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                            $returnArr["errCode"] = 68;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                            /******************** for logs user analytics ****************/
                                                                            $responseArr["errCode"]=68;
                                                                            $responseArr["errMsg"]=$errMsg;
                                                                            $xml_data['response']["data"] = "";
                                                                            $xml_data['response']["attributes"] = $responseArr;
                                                                            /******************** for logs user analytics ****************/

                                                                        }

                                                                    }else{
                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Failed : increase renewal fees earning";

                                                                        $errMsg    = "Failed : increase renewal fees earning";
                                                                       // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = 67;
                                                                        $returnArr["errMsg"] = $errMsg;

                                                                        rollbackTransaction($conn);

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback'";


                                                                        // $result   = deductPurchases($email, $KeywordAskPrice);

                                                                        $result = creditUserEarning($userId, $KeywordAskPrice, "purchase", "deduct");

                                                                        $credential["deduct_purchase_amount"]["user_id"]           = $userId;
                                                                        $credential["deduct_purchase_amount"]["keyword_ask_price"] = $KeywordAskPrice;

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                            $errMsg    = " Success: Deduct purchase amount |email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;
                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                            $errMsg    = " Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 66;
                                                                            $returnArr["errMsg"] = $errMsg;
                                                                        }

                                                                        // $result   = deductTrade($email, $tradingCommAmt);

                                                                        $result = creditUserEarning($userId, $tradingCommAmt, "trade", "deduct");

                                                                        $credential["deduct_trade_amount"]["user_id"]      = $userId;
                                                                        $credential["deduct_trade_amount"]["trade_amount"] = $tradingCommAmt;

                                                                        if (noError($result)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                            $errMsg    = " Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                            $errMsg    = " Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                         //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 68;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        // deduct renewal amount
                                                                        $deductRenewalAmt = creditUserEarning($userId, $kwdRenewalAmt, "renewalfees", "deduct");
                                                                        $response = json_encode($deductRenewalAmt);
                                                                        if (noError($deductRenewalAmt)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success: Deduct renewal amount |email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);

                                                                            $errMsg    = " Success: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);

                                                                            $errMsg    = " Failed: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);
                                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 67;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        // deduct trade fees from community pool
                                                                        $deductTradeFees = creditUserEarning($communityPoolUserId, $tradingCommAmt, "trade", "deduct");
                                                                        $response = json_encode($deductTradeFees);
                                                                        if (noError($deductTradeFees)) {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Success: Deduct trade fees amount from pool | email:" . urlencode($email) . ",bidTradeAmt:" . urlencode($tradingCommAmt);

                                                                            $errMsg    = " Success: Deduct trade fees amount from pool | email:" . urlencode($email) . ",bidTradeAmt:" . urlencode($tradingCommAmt);
                                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = -1;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        } else {

                                                                            $i                             = $i + 1;
                                                                            $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct trade fees amount from pool | email:" . urlencode($email) . ",renewalAmt:" . urlencode($tradingCommAmt);

                                                                            $errMsg    = " Failed: Deduct trade fees amount from pool  | email:" . urlencode($email) . ",bidTradeAmt:" . urlencode($tradingCommAmt);
                                                                         //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                            $returnArr["errCode"] = 63;
                                                                            $returnArr["errMsg"] = $errMsg;

                                                                        }

                                                                        $_SESSION["accept_bid_error_code"] = 27;
                                                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                        /******************** for logs user analytics ****************/
                                                                        $responseArr["errCode"]=27;
                                                                        $responseArr["errMsg"]=$errMsg;
                                                                        $xml_data['response']["data"] = "";
                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                        /******************** for logs user analytics ****************/
                                                                    }

                                                                } else {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Failed : increase total fees earning";

                                                                    $errMsg    = "Failed : increase total fees earning";
                                                                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                    rollbackTransaction($conn);

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback'";


                                                                    // $result   = deductPurchases($email, $KeywordAskPrice);

                                                                    $result = creditUserEarning($userId, $KeywordAskPrice, "purchase", "deduct");

                                                                    $credential["deduct_purchase_amount"]["user_id"]           = $userId;
                                                                    $credential["deduct_purchase_amount"]["keyword_ask_price"] = $KeywordAskPrice;

                                                                    if (noError($result)) {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                        $errMsg    = " Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                        //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = -1;
                                                                        $returnArr["errMsg"] = $errMsg;
                                                                    } else {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                        $errMsg    = " Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                      //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = 62;
                                                                        $returnArr["errMsg"] = $errMsg;
                                                                    }

                                                                    // $result   = deductTrade($email, $tradingCommAmt);

                                                                    $result = creditUserEarning($userId, $tradingCommAmt, "trade", "deduct");

                                                                    $credential["deduct_trade_amount"]["user_id"]      = $userId;
                                                                    $credential["deduct_trade_amount"]["trade_amount"] = $tradingCommAmt;

                                                                    if (noError($result)) {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                        $errMsg    = " Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = -1;
                                                                        $returnArr["errMsg"] = $errMsg;

                                                                    } else {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                        $errMsg    = " Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                      //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = 61;
                                                                        $returnArr["errMsg"] = $errMsg;

                                                                    }

                                                                    // deduct renewal amount
                                                                    $deductRenewalAmt = creditUserEarning($userId, $kwdRenewalAmt, "renewalfees", "deduct");
                                                                    $response = json_encode($deductRenewalAmt);
                                                                    if (noError($deductRenewalAmt)) {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Success: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);

                                                                        $errMsg    = " Success: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);
                                                                     //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = -1;
                                                                        $returnArr["errMsg"] = $errMsg;

                                                                    } else {

                                                                        $i                             = $i + 1;
                                                                        $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);

                                                                        $errMsg    = " Failed: Deduct renewal amount | email:" . urlencode($email) . ",renewalAmt:" . urlencode($kwdRenewalAmt);
                                                                       // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                        $returnArr["errCode"] = 59;
                                                                        $returnArr["errMsg"] = $errMsg;

                                                                    }

                                                                    $_SESSION["accept_bid_error_code"] = 25;
                                                                    $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                    /******************** for logs user analytics ****************/
                                                                    $responseArr["errCode"]=25;
                                                                    $responseArr["errMsg"]=$errMsg;
                                                                    $xml_data['response']["data"] = "";
                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                    /******************** for logs user analytics ****************/
                                                                }



                                                            }else{
                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". Failed : increase renewal fees earning";

                                                                $errMsg    = "Failed : increase renewal fees earning";
                                                               // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                rollbackTransaction($conn);

                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback'";


                                                                // $result   = deductPurchases($email, $KeywordAskPrice);

                                                                $result = creditUserEarning($userId, $KeywordAskPrice, "purchase", "deduct");

                                                                $credential["deduct_purchase_amount"]["user_id"]           = $userId;
                                                                $credential["deduct_purchase_amount"]["keyword_ask_price"] = $KeywordAskPrice;

                                                                if (noError($result)) {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                    $errMsg    = " Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                    $returnArr["errCode"] = -1;
                                                                    $returnArr["errMsg"] = $errMsg;
                                                                } else {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                    $errMsg    = " Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                                //    $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                    $returnArr["errCode"] = 58;
                                                                    $returnArr["errMsg"] = $errMsg;
                                                                }

                                                                // $result   = deductTrade($email, $tradingCommAmt);

                                                                $result = creditUserEarning($userId, $tradingCommAmt, "trade", "deduct");

                                                                $credential["deduct_trade_amount"]["user_id"]      = $userId;
                                                                $credential["deduct_trade_amount"]["trade_amount"] = $tradingCommAmt;

                                                                if (noError($result)) {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                    $errMsg    = " Success: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                  //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                    $returnArr["errCode"] = -1;
                                                                    $returnArr["errMsg"] = $errMsg;

                                                                } else {

                                                                    $i                             = $i + 1;
                                                                    $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                                    $errMsg    = " Failed: Deduct trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                    $returnArr["errCode"] = 57;
                                                                    $returnArr["errMsg"] = $errMsg;

                                                                }

                                                                $_SESSION["accept_bid_error_code"] = 25;
                                                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]=25;
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/
                                                            }

                                                        } else {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . ". Failed: Credit trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);

                                                            $errMsg    = " Failed: Credit trade amount | email:" . urlencode($email) . ",tradingCommAmt:" . urlencode($tradingCommAmt);
                                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                            rollbackTransaction($conn);

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback'";

                                                            // $result   = deductPurchases($email, $KeywordAskPrice);

                                                            $result = creditUserEarning($userId, $KeywordAskPrice, "purchase", "deduct");

                                                            $credential["deduct_purchase_amount"]["user_id"]           = $userId;
                                                            $credential["deduct_purchase_amount"]["keyword_ask_price"] = $KeywordAskPrice;

                                                            if (noError($result)) {

                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                $errMsg    = " Success: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = $errMsg;

                                                            } else {

                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                                $errMsg    = " Failed: Deduct purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                              //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                                $returnArr["errCode"] = 56;
                                                                $returnArr["errMsg"] = $errMsg;

                                                            }

                                                            $_SESSION["accept_bid_error_code"] = 24;
                                                            $_SESSION["accept_bid_error_message"] = $errMsg;

                                                            /******************** for logs user analytics ****************/
                                                            $responseArr["errCode"]=24;
                                                            $responseArr["errMsg"]=$errMsg;
                                                            $xml_data['response']["data"] = "";
                                                            $xml_data['response']["attributes"] = $responseArr;
                                                            /******************** for logs user analytics ****************/
                                                        }
                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . ". Failed: credit purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);

                                                        $errMsg    = " Failed: credit purchase amount | email:" . urlencode($email) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice);
                                                      //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                        //rollback transction
                                                        rollbackTransaction($conn);

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback' ";

                                                        $errMsg    = "There was an error while buying this keyword";
                                                      //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                        $_SESSION["accept_bid_error_code"] = 27;
                                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                                        $returnArr["errCode"] = 27;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=27;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/
                                                    }

                                                } else {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . ". Failed: updateKwdDetailsAfterTrade| email:" . urlencode($email) . ",KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",purchase_timestamp:" . urlencode($purchase_timestamp) . ",keyword:" . urlencode($keyword) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice) . "type:trade ";

                                                    $errMsg    = "Failed: updateKwdDetailsAfterTrade| email:" . urlencode($email) . ",KeywordOwnerEmail:" . urlencode($KeywordOwnerEmail) . ",purchase_timestamp:" . urlencode($purchase_timestamp) . ",keyword:" . urlencode($keyword) . ",KeywordAskPrice:" . urlencode($KeywordAskPrice) . "type:trade ";
                                                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    //rollback transction
                                                    rollbackTransaction($conn);

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . ".transaction :'rollback' ";

                                                    $errMsg    = "There was an error while buying this keyword";
                                                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                    $_SESSION["accept_bid_error_code"] = 23;
                                                    $_SESSION["accept_bid_error_message"] = $errMsg;

                                                    $returnArr["errCode"] = 54;
                                                    $returnArr["errMsg"] = $errMsg;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]=23;
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/
                                                }

                                            } else {

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . ". Failed: update ownershishp details";

                                                $errMsg    = " Failed: update ownershishp details";
                                                //$returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                //rollback transction
                                                rollbackTransaction($conn);

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . ". transaction :'rollback' ";

                                                $errMsg    = "There was an error while buying this keyword";
                                               // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                                $_SESSION["accept_bid_error_code"] = 22;
                                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                                $returnArr["errCode"] =22;
                                                $returnArr["errMsg"] = $errMsg;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]=22;
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/
                                            }
                                        } else {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . ". Failed:  upadating status in ask table";

                                            $errMsg    = "Failed:  upadating status in ask table";
                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                            $errMsg    = "There was an error while buying this keyword";
                                           // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                            $_SESSION["accept_bid_error_code"] = 21;
                                            $_SESSION["accept_bid_error_message"] = $errMsg;

                                            $returnArr["errCode"] = 21;
                                            $returnArr["errMsg"] = $errMsg;

                                            /******************** for logs user analytics ****************/
                                            $responseArr["errCode"]=22;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;
                                            /******************** for logs user analytics ****************/
                                        }

                                    } else {

                                        $i                             = $i + 1;
                                        $xml_data["step" . $i]["data"] = $i . ". Failed:  unable to start transaction";

                                        $errMsg    = " Failed:  unable to start transaction";
                                      //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                        $errMsg    = "There was an error while buying this keyword";
                                        //$returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                        $returnArr["errCode"] = 20;
                                        $returnArr["errMsg"] = $errMsg;

                                        $_SESSION["accept_bid_error_code"] = 20;
                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=20;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }


                                } else {

                                    $i                             = $i + 1;
                                    $xml_data["step" . $i]["data"] = $i . ". Failed:  User available balance is greater then the gross buy price";

                                    $errMsg    = "You have insufficient balance to buy this keyword";
                                  //  $returnArr = setErrorStack($returnArr, 24, $errMsg, $extraArgs);

                                    $_SESSION["accept_bid_error_code"] = 19;
                                    $_SESSION["accept_bid_error_message"] = $errMsg;

                                    $returnArr["errCode"] = 19;
                                    $returnArr["errMsg"] = $errMsg;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]=19;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/

                                }
                            }else{
                                $i = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . ". Failed:  ".$keyword." is unavailable";

                                $errMsg    = "Currently this keyword is not available to buy at Ask price.";
                               // $returnArr = setErrorStack($returnArr, 23, $errMsg, $extraArgs);

                                $_SESSION["accept_bid_error_code"] = 22;
                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                $returnArr["errCode"] = 22;
                                $returnArr["errMsg"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=22;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }

                        } else {

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . ". Failed: getting user current wallet available balance";

                            $errMsg    = "Sorry! unable to get current wallet balance";
                           // $returnArr = setErrorStack($returnArr, 22, $errMsg, $extraArgs);

                            $_SESSION["accept_bid_error_code"] = 17;
                            $_SESSION["accept_bid_error_message"] = $errMsg;

                            $returnArr["errCode"] = 17;
                            $returnArr["errMsg"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=17;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }

                }else{
                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . ". Failed: Getting user balance details";

                    $errMsg    = "Failed: Getting user balance details";
                    //$returnArr = setErrorStack($returnArr, 22, $errMsg, $extraArgs);

                    $_SESSION["accept_bid_error_code"] = 16;
                    $_SESSION["accept_bid_error_message"] = $errMsg;

                    $returnArr["errCode"] = 16;
                    $returnArr["errMsg"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=16;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }

            } else {

                $i         = $i + 1;
                $errMsg    = "The keyword status has been changed.Sometimes it takes upto 1 minute to reflect.Please try again after sometime";
             //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                $_SESSION["accept_bid_error_code"] = 15;
                $_SESSION["accept_bid_error_message"] = $errMsg;

                $returnArr["errCode"] = 15;
                $returnArr["errMsg"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=15;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }

            $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

            header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

        }else{

            unset($_SESSION['security_type']);
            unset($_SESSION['xml_step']);
            unset($_SESSION['form_data']);
            unset($_SESSION['activity_id']);
            unset($_SESSION['redirect_url']);
            unset($_SESSION['xml_file']);
            unset($_SESSION['verify_status']);

            /*$_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";*/
            $returnArr["errCode"] = 2;

          //  $xmlProcessor-> writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/

            header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');
        }

    }else{

        $i         = $i + 1;
        $errMsg    = "Sorry!!! Your session is expired, Please login again.";
        // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

        $responseArr["errCode"] = 39;
        $responseArr["errMsg"]  = $errMsg;

        $xml_data['response']["data"]      = '';
        $xml_data['response']["attribute"] = $responseArr;

        $returnArr["errCode"] = 40;
        $returnArr["errMsg"] = $errMsg;

        header('Location:'.$rootUrl.'views/prelogin/index.php');

    }
}else{

    $i         = $i + 1;
    $errMsg    = "Sorry!!! Your session is expired, Please login again.";
    //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

    $returnArr["errCode"] = 40;
    $returnArr["errMsg"] = $errMsg;

    $responseArr["errCode"] = 40;
    $responseArr["errMsg"]  = $errMsg;

    $xml_data['response']["data"]      = '';
    $xml_data['response']["attribute"] = $responseArr;

}

$credential = json_encode($credential);

$xml_data['credential']["attribute"] = $credential;

//$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

$returnArr["errStack"] = '';

echo json_encode($returnArr);

?>