<?php

session_start();

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/walletHelper.php');
require_once ('../../models/keywords/keywordPurchaseModel.php');
require_once ('../../models/header/headerModel.php');
require_once ('../../models/keywords/userCartModel.php');

error_reporting(0);

$ip              = getClientIP();
$paypal_response = $_SESSION["paypal_response"];

$json_val = json_encode($paypal_response);

if (isset($paypal_response) && !empty($paypal_response)){

    //success getting paypal response
    $totalPrice      = urldecode($paypal_response["AMT"]);
    $totalPrice      = (float)$totalPrice;
    $custom          = $paypal_response["CUSTOM"];
    $custom          = urldecode($custom);
    $originalOrderId = $paypal_response["L_NUMBER0"];
    $originalOrderId = urldecode($originalOrderId);
    $paypalEmail     = $paypal_response["EMAIL"];
    $paypalEmail     = urldecode($paypalEmail);
    $transactionId   = $paypal_response["PAYMENTREQUEST_0_TRANSACTIONID"];
    $transactionId   = urldecode($transactionId);
    $status          = "paid";
    $keywordBasket = $paypal_response["L_NAME0"];
    $keywordBasket = urldecode($keywordBasket);

    //decrypting custom
    $decryptedCustom = explode('~', $custom);

    $kwdOwnerEmail = $decryptedCustom[0];
    //$keywordBasket    = $decryptedCustom[1];
    $priceInBtc       = $decryptedCustom[1];
    $conversionRate   = number_format($decryptedCustom[2], 2);
    $conversionRateUSD   = $decryptedCustom[3];
    $amount_paid_usd     = $totalPrice / $conversionRate;

    //convert from USD amount to BTC
//	$currentRateBTC = getBTCExchangeRateUSD("BTC");
//	$amount_paid_btc = $amount_paid_usd * $currentRateBTC;

    $errCode = null;

    // create search database connection
    $searchDbConn = createDBConnection('dbsearch');
    if(noError($searchDbConn)){
        $searchDbConn = $searchDbConn["connection"];

        $currentExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
        if(noError($currentExchangeRate)){
            $currentExchangeRate = $currentExchangeRate["exchange_rate"];
            // convert array into json encode
            $currentExchangeRate = json_encode($currentExchangeRate);

            mysqli_close($searchDbConn);


            //Create database connection
            $conn = createDBConnection("dbkeywords");

            if (noError($conn)) {

                $conn = $conn["connection"];

                $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

                $getUserDetails = getUserInfo($kwdOwnerEmail, $walletURLIP . 'api/v3/', $userAvailableBalance);


               /* $result     = getPaymentDetailsByOrderId($originalOrderId, $kwdOwnerEmail, $conn);

                $response = json_encode($result);

                $amount_due_in_sgd = $result["errMsg"]["amount_due_in_sgd"];
                $amount_due = $result["errMsg"]["amount_due"];
                $keywordBasket  = $result["errMsg"]["keyword_cart"];*/

                //getting keyword price
                // $paymentDetails = getTotalPaymentDetails($originalOrderId, $kwdOwnerEmail, $conn);
                // $keywordBasket  = $paymentDetails["errMsg"]["keyword_cart"];

              //  if ($totalPrice <= (floor($amount_due_in_sgd)+1) && $totalPrice >= (floor($amount_due_in_sgd)-1)) {

                  //  if (noError($result)) {

                        $getAdminSetting = getAdminSettingsFromKeywordAdmin($conn);
                        $renewalAmt = $getAdminSetting["data"]["kwd_renewal_fees_per_year"];

                         $userReqField = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                        // get user info
                        $getUserDetails = getUserInfo($kwdOwnerEmail, $walletURLIP . 'api/v3/', $userReqField);

                        if(noError($getUserDetails)){
                            $userId = $getUserDetails["errMsg"]["_id"];
                            $firstName = $getUserDetails["errMsg"]["first_name"];
                            $lastName = $getUserDetails["errMsg"]["last_name"];

                            $getUserBalance = calculateUserBalance($getUserDetails);
                            if(noError($getUserBalance)) {
                                $getUserBalance = number_format($getUserBalance["errMsg"]["total_available_balance"], 8);

                                $getCartDetails = getUserCartDetails($kwdOwnerEmail, $conn);
                                if(noError($getCartDetails)){
                                    $getCartDetails = $getCartDetails["errMsg"]["user_renew_kwd_in_cart"];
                                    $getCartDetails = json_decode($getCartDetails, true);

                                    $cartCount = COUNT($getCartDetails);
                                    $totalRenwalAmount = 0;

                                    if($cartCount > 0){
                                        foreach($getCartDetails as $key => $keyword){
                                            $totalRenwalAmount = $totalRenwalAmount + $renewalAmt;
                                        }
                                    }

                                    $amount_due = (float)($totalRenwalAmount - $getUserBalance);

                                    /*  Credit itd purchase amount to user balance */
                                    $creditEarning   = creditUserEarning($userId, $amount_paid_usd, 'purchase_itd', 'add');

                                    if(noError($creditEarning)){

                                        // insert transaction for purchase itd from paypal

                                        // insert user transaction for trades fees
                                        // parameters required to insert in transaction
                                        $recipientEmail     = $kwdOwnerEmail;
                                        $recipientUserId    = $userId;
                                        $senderEmail        = $communityPoolUser;
                                        $senderUserId       = $communityPoolUserId;
                                        $amount             = number_format($amount_paid_usd, 8);
                                        $type               = "purchase_itd";
                                        $paymentMode        = 'paypal';
                                        $originIp           = $ip;
                                        $exchangeRateInJson = $currentExchangeRate;
                                        $metaDetails = array("buyer_id" => $kwdOwnerEmail, "raise_on" => "ITD purchase by paypal", "purchase_amount_in_sgd" => $totalPrice);

                                        $insertPaypalPaymentTrans = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                        if(noError($insertPaypalPaymentTrans)){
                                            echo "<form style='display:none' id='renewForm' method='post' action ='".$rootUrl."controllers/keywords/renewKeywordByPaypal.php' >
                                         <input type='hidden' name='email' value='".$kwdOwnerEmail."'><input>
                                         <input type='hidden' name='keywordBasket' value='".$keywordBasket."'><input>
                                         <input type='hidden' name='totalPrice' value='".$amount_due."'><input>
                                         <input type='hidden' name='payment_mode' value='paypal'><input>
                                         <input type='hidden' name='conversionRate' value='".$conversionRate."'><input>
                                         <input type='hidden' name='conversionRateUSD' value='".$conversionRateUSD."'><input>
    
                                         </form>";
                                            $errCode = -1;
                                            echo "Processing ..";

                                            unset($_SESSION["paypal_response"]);
                                            unset($paypal_response);
                                            echo "<script>document.getElementById('renewForm').submit();</script>";

                                        }else{

                                            $errCode = 2;
                                        }
                                    }else{
                                        $errCode = 2;
                                    }
                                }else{
                                    $errCode = 2;
                                }
                            }else{
                                $errCode = 2;
                            }
                        }else{
                            $errCode = 2;
                        }
                   /* } else {
                        $errCode = 2;

                    }*/
               /* } else {

                    $errCode = 2;
                }*/
            } else {

                $errCode = 2;
            }

        }else{
            $errCode = 2;
        }
    }else{
        $errCode = 2;
    }


    if($errCode == 2){
        //error buying keyword
        $errorMessage        = "Error renew keywords";
        $redirectURL = "../../views/keywords/cart/renew_checkout.php";
        print("<script>");
        print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
        print("</script>");
    }
} else {
    $xml .= "<step1 title='paypal response' >1. paypal response : 'empty'";
    $xml .= "</step1>\n";
    //failure getting paypal response
    $errorMessage        = "Error renew keywords";
    $redirectURL = "../../views/keywords/cart/renew_checkout.php";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}

//closing deposit log file
$_SESSION['errMsg']  = $errorMessage;
$_SESSION['errCode'] = $errorCode;
$_SESSION['count'] = 1;

?>