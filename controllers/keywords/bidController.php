<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 14/2/17
 * Time: 7:59 PM
 */

header("Access-Control-Allow-Origin: *");
session_start();
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/deviceHelper.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/walletHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/transactionHelper.php";
require_once "../../models/header/headerModel.php";
require_once "../../models/keywords/userCartModel.php";
require_once "../../models/keywords/keywordBidModel.php";
require_once "../../models/keywords/acceptBidModel.php";
require_once("../../backend_libraries/xmlProcessor/xmlProcessor.php");


error_reporting(0);
// add globla variales here
$returnArr = array();
$extraArgs = array();

// get request parameters
$keyword = urldecode($_POST['keyword']);
$type = urldecode($_POST['type']);
$action = urldecode($_POST['action']);
$bidAmount = urldecode($_POST['amount']);
$bidAmount = (float) $bidAmount;
$stepCounter = 0;

$email    = $_SESSION["email"];
$id       = $_SESSION["id"];
$username = $_SESSION["first_name"];

// clean xss script
$keyword = cleanXSS(trim($keyword));
$type = cleanXSS(trim($type));
$action = cleanXSS(trim($action));

//get current date details
$currentDate = date("Y_m_d", time());
$dateArr     = explode("_", $currentDate);
$year        = $dateArr[0];
$month       = $dateArr[1];
$date        = $dateArr[2];
//get table partion
if ($date <= 7) {
    $partitionNo = 1;
} elseif ($date <= 14) {
    $partitionNo = 2;
} elseif ($date <= 21) {
    $partitionNo = 3;
} else {
    $partitionNo = 4;
}


/* XML log file path */
$kwdPurchaseLog = $logPath["keywordBid"];

/* Set xml filename depending on user action */
$xmlAction = '';
$xmlTitle = '';
$xmlHeader = '';
if(!empty($action) && $action == 1) {
    $xmlAction = "placeBid";
    $xmlTitle = "Set bid price";
    $xmlHeader = "place bid";
}else if(!empty($action) && $action == 2){
    $xmlAction = "editBid";
    $xmlTitle = "Edit bid price";
    $xmlHeader = "edit bid";
}else if(!empty($action) && $action == 3){
    $xmlAction = "deleteBid";
    $xmlTitle = "Delet bid price";
    $xmlHeader = "delete bid";
}

$xmlFilename   = "{$xmlAction}.xml";

//get database name eg kwd_bids_2015_11_1
$databaseName = "kwd_bids_" . $year . "_" . $month . "_" . $partitionNo;

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$i = 1;

/* Initialize XML logs */
$xmlProcessor = new xmlProcessor();
$xmlArray = initializeXMLLog(trim(urldecode($email)));
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

/* XML title attribute */
$xml_atrr = array();
$xml_atrr["title"] = $xmlTitle;

if(isset($_SESSION["email"]) && !empty($_SESSION)){

    if(!isset($_SESSION["verify_status"])) {

        $errMsg = "Start {$xmlAction} process."; // Message for log
        $stepCounter++; // Step counter
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr; // Set attribute in step
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}"; // Set message for step

        $unique_id = md5(uniqid(rand(), true));
        $xmlArray['activity']['id'] = $unique_id;
        $_SESSION['activity_id'] = $unique_id;

        $_SESSION['redirect_url'] = $returnURl;
        $msg = "Start {$xmlHeader} process.";
        $xml_data['step'.$i]["data"] = $i.". {$msg}";

        if($bidAmount >= $minimumTradingAmount){

            $errMsg = "success : Bid amount is greater then minimum trading amount.";
            $xml_data['step'.++$i]["data"] = $i.". Error: Bid amount greater than minimum trade amount.";
            $returnArr["errCode"] = -2;
            $returnArr["errMsg"] = $errMsg;

            // get user details with available balance if login
            $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

            $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
            if(noError($getUserDetails)){

                $errMsg = "Success: Fetching {$email} info";
                $xml_data['step'.++$i]["data"] = $i.". Success: Fetching {$email} info.";
                $returnArr["errCode"] = -2;
                $returnArr["errMsg"] = $errMsg;

                $getUserBalance = calculateUserBalance($getUserDetails);
                if(noError($getUserBalance)){

                    $errMsg = "Success: Calculate available balance";
                    $xml_data['step'.++$i]["data"] = $i.". Success: Calculate available balance.";
                    $returnArr["errCode"] = -2;
                    $returnArr["errMsg"] = $errMsg;

                    $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];
                    $userAvailableBalance = (float)$userAvailableBalance;

                    $kwdDbConn = createDBConnection('dbkeywords');
                    if(noError($kwdDbConn)){
                        $kwdDbConn = $kwdDbConn["connection"];

                        //get Admin Settings
                        $adminSettings  = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                        if(noError($adminSettings)){

                            $searchConn = createDBConnection("dbsearch");
                            if(noError($searchConn)){
                                $searchConn = $searchConn["connection"];

                                $kycDetails = getCurrentKYCLimits($searchConn,$email);
                                $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];
                                $kwdRenewalFeesPerYear = $adminSettings["data"]["kwd_renewal_fees_per_year"];

                                //check for keyword owner
                                $bidKeywordDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                                if(noError($bidKeywordDetails)){

                                    $ifCondition = '';

                                    if($action == 1){

                                        //gross bid amount
                                        $tradingCommAmt = number_format((float)($bidAmount * $tradingCommPercent)/100,8);

                                        $tradingCommAmt = number_format((float)$tradingCommAmt, 8);
                                        $kwdRenewalFeesPerYear = number_format((float)$kwdRenewalFeesPerYear, 8);

                                        $bidAmtGross    = ((float) $bidAmount + $tradingCommAmt + $kwdRenewalFeesPerYear);

                                        $ifCondition = "{$userAvailableBalance} >= {$bidAmtGross}";

                                    }else if($action == 2){

                                        $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];
                                        $kwdRenewalFeesPerYear = $adminSettings["data"]["kwd_renewal_fees_per_year"];

                                        //gross bid amount
                                        $tradingCommAmt = number_format((float)($bidAmount * $tradingCommPercent)/100,8);
                                        $tradingCommAmt = number_format((float)$tradingCommAmt, 8);
                                        $kwdRenewalFeesPerYear  = number_format((float)$kwdRenewalFeesPerYear, 8);
                                        $bidAmtGross = $bidAmount + $tradingCommAmt + $kwdRenewalFeesPerYear;

                                        $bidAmtGross = (float)$bidAmtGross;

                                        $activeBidsForKeyword = $bidKeywordDetails["errMsg"]["active_bids"];
                                        $activeBidsForKeyword = json_decode($activeBidsForKeyword, true);

                                        //check if the bid for keyword by above bidder exists
                                        $editBidtxID = null;
                                        foreach ($activeBidsForKeyword as $value) {
                                            $activeBidArr = explode("~~", $value);
                                            $bidderEmail = $activeBidArr[1];
                                            if ($bidderEmail == $email) {
                                                $myBidAmt = $activeBidArr[3];
                                                $editBidtxID = $value;
                                            }
                                        }

                                        //remove last bidtxId from active bids array
                                        $activeBidsForKeyword = array_diff($activeBidsForKeyword, array(
                                            $editBidtxID
                                        ));

                                        //get current bid Amount
                                        $currentBidDetails = explode("~~", $editBidtxID);

                                        $returnBidAmt      = $currentBidDetails[3];
                                        $lastCommision     = $currentBidDetails[4];
                                        $lastRenewalAmt    = $currentBidDetails[8];
                                        $returnBidAmtGross = number_format((float)$returnBidAmt + $lastCommision + $lastRenewalAmt,8);

                                        $ifCondition = "{$userAvailableBalance} >= ({$bidAmtGross} - {$returnBidAmtGross})";

                                    }else if($action == 3){

                                        $deleteStatus = 0;
                                        $ifCondition = "{$deleteStatus} == 0";
                                    }

                                    if ($ifCondition) {

                                        $errMsg = "Success : user balance is greater then the bid amount";
                                        $xml_data['step'.++$i]["data"] = $i.". Success : user balance is greater then the bid amount";
                                        $returnArr["errCode"] = -2;
                                        $returnArr["errMsg"] = $errMsg;

                                        //redirecting to 2FA page
                                        $msg = "Success : Redirecting to 2FA page";
                                        $xml_data['step'.++$i]["data"] = $i.". {$msg}";
                                        $returnArr["errCode"] = -2;
                                        $_SESSION['form_data'] = json_encode($_POST);
                                        $_SESSION['xml_step'] = $i;
                                        $_SESSION["action"] = $action;

                                        $_SESSION['xml_file'] = $kwdPurchaseLog . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlFilename;

                                    }else{
                                        $errMsg = "Your bid amount exceed than your available balance";
                                        $xml_data['step'.++$i]["data"] = $i.". Bid amount exceed then the available balance";
                                        $returnArr["errCode"] = 8;
                                        $returnArr["errMsg"] = $errMsg;
                                    }

                                }else{
                                    $errMsg = "Error: Fetching keyword ownership details";
                                    $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                                    $returnArr["errCode"] = 7;
                                    $returnArr["errMsg"] = $errMsg;
                                }
                            }else{
                                $errMsg = "Error: Unable to connect src database";
                                $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                                $returnArr["errCode"] = 6;
                                $returnArr["errMsg"] = $errMsg;
                            }
                        }else{
                            $errMsg = "Error: Fetching setting details";
                            $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                            $returnArr["errCode"] = 5;
                            $returnArr["errMsg"] = $errMsg;
                        }

                    }else{
                        $errMsg = "Error: Unable to connect database";
                        $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                        $returnArr["errCode"] = 4;
                        $returnArr["errMsg"] = $errMsg;
                    }
                }else{
                    $errMsg = "Error: Calculate available balance";
                    $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = $errMsg;
                }
            }else{
                $errMsg = "Error: Fetching {$email} info";
                $xml_data['step'.++$i]["data"] = $i.". ". $errMsg;
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = $errMsg;

            }

        }else{
            $errMsg = "Bid amount should be greater then minimum trading amount.";
            $xml_data['step'.++$i]["data"] = $i.". Error: Bid amount greater than minimum trade amount.";
            $returnArr["errCode"] = 1;
            $returnArr["errMsg"] = $errMsg;

        }

        // create or update xml log Files
        $xmlProcessor->writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

        echo json_encode($returnArr); die;

    }else if($returnURl == $_SESSION['redirect_url']) {

        $stepCounter = $_SESSION["xml_step"];
        $security_type = $_SESSION['security_type'];
        $form_data = json_decode($_SESSION['form_data'], true);
        $keyword = urldecode($form_data["keyword"]);
        $bidAmount = urldecode($form_data["amount"]);
        $type = urldecode($form_data["type"]);
        $action = $form_data["action"];
        $activity_id = $_SESSION['activity_id'];
        $xml_file = $_SESSION['xml_file'];
        $action = $_SESSION["action"];


        if(!empty($action) && $action == 1) {
            $xmlAction = "setAsk";
        }else if(!empty($action) && $action == 2){
            $xmlAction = "editAsk";
        }else if(!empty($action) && $action == 3){
            $xmlAction = "deleteAsk";
        }

        $xmlFilename   = "{$xmlAction}.xml";

        unset($_SESSION['security_type']);
        unset($_SESSION['xml_step']);
        unset($_SESSION['form_data']);
        unset($_SESSION['activity_id']);
        unset($_SESSION['redirect_url']);
        unset($_SESSION['xml_file']);
        unset($_SESSION['verify_status']);
        unset($_SESSION["action"]);


    $searchDbConn = createDBConnection('dbsearch');
    if (noError($searchDbConn)) {
        $errMsg = "search db connection: success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
        $searchConn = $searchDbConn['connection'];

        $kwdDbConn = createDBConnection('dbkeywords');
        if (noError($kwdDbConn)) {

            $errMsg = "keywords db connection: success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $kwdDbConn = $kwdDbConn["connection"];


                switch ($type){
                    case "bid":
                        if($action == 1)
                        {
                            /* Set ask */
                            $returnArr = setBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $bidAmount, $id, $xml_data, $stepCounter, $xml_atrr);
                        }else if($action == 2){
                            $returnArr = editBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $bidAmount, $id, $xml_data, $stepCounter, $xml_atrr);
                        }else{
                            $returnArr = deleteBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $rootUrl, $bidAmount, $id, $xml_data, $stepCounter, $xml_atrr);
                        }
                        break;
                }

            }else{
                $errMsg = "keywords db connection: failed.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                /* return error message */
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = "Database error";
            }
        }else{
            $errMsg = "search db connection: failed.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            /* return error message */
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = "Database error";
        }

        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr["errMsg"]  = $returnArr['errMsg'];
        $xmlCredentialData = json_encode($returnArr['credential']);
        $xml_data         = $returnArr["xml_data"];
        unset($returnArr['credential']);
        unset($returnArr['xml_data']);

        /* Write response attribute in XML file */
        $xml_resp_atrr['response'] = json_encode($returnArr);
        $xml_cred_atrr['credential'] = $xmlCredentialData;

        $xml_data['response']["data"] ="";
        $xml_data['response']["attribute"] =$xml_resp_atrr;
        $xml_data['credential']["data"] ="";
        $xml_data['credential']["attribute"] =$xml_cred_atrr;

        //create or update xml log Files
        $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

        /*if(noError($returnArr)){
            $_SESSION['succ_msg'] = $returnArr['errMsg'];
        }else{
            $_SESSION['err_msg'] = $returnArr['errMsg'];
        }*/

        header('Location:'.$rootUrl.'views/keywords/marketplace/keyword_search.php?q='.$keyword);
    }else{
        unset($_SESSION['security_type']);
        unset($_SESSION['xml_step']);
        unset($_SESSION['form_data']);
        unset($_SESSION['activity_id']);
        unset($_SESSION['redirect_url']);
        unset($_SESSION['xml_file']);
        unset($_SESSION['verify_status']);
        unset($_SESSION["action"]);

        /*$_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";*/
        $returnArr["errCode"] = 2;

      //  $xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

        header('Location:'.$rootUrl.'views/keywords/marketplace/keyword_search.php?q='.$keyword);
    }
}else{
    $errMsg = "Session is expired";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $returnArr["errCode"] = 2;
    $returnArr["redirect_url"] = "{$rootUrl}views/prelogin/index.php";
    $returnArr["errMsg"]  = "Sorry!!! Your session is expired, Please login again.";

   // $xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);
}


echo json_encode($returnArr);

function setBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $amount, $user_id, &$xml_data, &$stepCounter, $xml_atrr){

    global $userRequiredFields, $minimumTradingAmount, $walletURLIP,$keywoDefaultCurrencyName;
    $retArray = array();
    $logCred = array();
    $extraArgs = '';

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount);

    //bid time
    $bidTime = time();
    $bidDate = date("Y-m-d h:i:s", $bidTime);

    $logCred["bidTime"] = $bidTime;
    $logCred["bidDate"] = $bidDate;

    $errMsg = "Start executing place bid method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    if ($amount >= $minimumTradingAmount) {

        $errMsg = "Bid amount greater than minimum trade amount : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        // get user details with available balance if login
        $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
        if (noError($getUserDetails)) {

            $firstName = $getUserDetails["errMsg"]["first_name"];
            $lastName = $getUserDetails["errMsg"]["last_name"];

            $errMsg = "Get bidder user info : success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $getUserBalance = calculateUserBalance($getUserDetails);
            if (noError($getUserBalance)) {
                $errMsg = "calculating user available balance : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];
                $userAvailableBalance = (float)$userAvailableBalance;

                //get Admin Settings
                $adminSettings  = getAdminSettingsFromKeywordAdmin($kwdDbConn);

                if (noError($adminSettings)) {

                    $errMsg = "Get admin setting from keyword admin : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $kycDetails = getCurrentKYCLimits($searchConn,$email);
                    // $tradingCommPercent = $adminSettings["data"]["trading_commision_percent"];
                    $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];
                    $logCred["tradingCommPercent"] = $tradingCommPercent;
                    $kwdRenewalFeesPerYear = $adminSettings["data"]["kwd_renewal_fees_per_year"];
                    $logCred["renewalAmount"] = $kwdRenewalFeesPerYear;

                    //gross bid amount
                    $tradingCommAmt = number_format((float)($amount * $tradingCommPercent)/100,8);

                    $tradingCommAmt = number_format((float)$tradingCommAmt, 8);
                    $kwdRenewalFeesPerYear = number_format((float)$kwdRenewalFeesPerYear, 8);

                    //added renewal fees in bid gross amount
                    //$bidAmtGross    = $bidAmt + $tradingCommAmt + $kwdRenewalFeesPerYear;
                    $bidAmtGross    = (float) $amount + $tradingCommAmt + $kwdRenewalFeesPerYear;

                    //$bidAmtGross = (float) $bidAmtGross;

                    //bid time
                    $bidTime = time();
                    $bidDate = date("Y-m-d h:i:s", $bidTime);
                    //get bids transaction id
                    //added renewal fees in bid transaction ID
                    $bidTransId = $databaseName . "~~" . $email . "~~" . rand(111111, 999999) . "~~" . $amount . "~~" . $tradingCommAmt . "~~" . $bidDate. "~~". $keyword."~~".$user_id."~~".$kwdRenewalFeesPerYear;

                    $logCred["tradingCommAmt"] = $tradingCommPercent;
                    $logCred["bidAmtGross"] = (float) $bidAmtGross;
                    $logCred["bidTransId"] = $bidTransId;

                    //check for keyword owner
                    $bidKeywordDetails    = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                    if (noError($bidKeywordDetails)) {
                        $bidKeywordOwnerId    = $bidKeywordDetails["errMsg"]["buyer_id"];
                        $activeBidsForKeyword = $bidKeywordDetails["errMsg"]["active_bids"];
                        $activeBidsForKeyword = json_decode($activeBidsForKeyword, true);

                        $errMsg = "Get keyword ownership details : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                        //check if the bid for keyword by above bidder exists
                        $bidderBidExistStatus = null;
                        foreach ($activeBidsForKeyword as $value) {
                            $bidArray    = explode("~~", $value);
                            $bidderEmail = $bidArray[1];
                            if ($bidderEmail == $email) {
                                $bidderBidExistStatus = "exist";
                            }
                        }


                        if ($userAvailableBalance >= $bidAmtGross) {

                            $errMsg = "User available balance greater than bid gross amount : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $logCred["userAvailableBalance"] = $userAvailableBalance;

                            if ($bidderBidExistStatus != "exist") {

                                $errMsg = "Bidder bid exist : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                //insert current bid in active bids
                                $activeBidsForKeyword[] = $bidTransId;
                                $activeBidsForKeyword   = json_encode($activeBidsForKeyword,JSON_UNESCAPED_UNICODE);
                                $highestBidAmountForKwd = $bidKeywordDetails["errMsg"]["highest_bid_amount"];
                                /*$highestBidAmountForKwd = number_format((float)$highestBidAmountForKwd,8);*/

                                 $highestBidAmountForKwd = (float)$highestBidAmountForKwd;
                                if (!isset($highestBidAmountForKwd) && empty($highestBidAmountForKwd)) {
                                    $highestBidAmountForKwd = 0;
                                }

                                $noOfActiveBidForKwd = $bidKeywordDetails["errMsg"]["no_of_active_bids"];
                                if (!isset($noOfActiveBidForKwd) && empty($noOfActiveBidForKwd)) {
                                    $noOfActiveBidForKwd = 0;
                                }
                                $noOfActiveBid = $noOfActiveBidForKwd + 1;

                                if ($bidKeywordOwnerId != $email) {

                                    $errMsg = "Bideder is not keyword owner id : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                                    $trans = startTransaction($kwdDbConn);
                                    if (noError($trans)) {

                                        $errMsg = "Start transaction : success.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        //insert into related bids table
                                        if (isset($noOfDays) && !empty($noOfDays)) {
                                            $query = "INSERT INTO " . $databaseName . "(bidder_id,bidder_email,bidder_mobile,keyword,bid_price,kwd_owner_id,kwd_owner_email,kwd_owner_mobile,bid_transaction_id,bid_expire_time,commission) VALUES('','" . cleanQueryParameter($kwdDbConn, $email) . "','','" . cleanQueryParameter($kwdDbConn, $keyword) . "','" . cleanQueryParameter($kwdDbConn, $amount) . "','','" . cleanQueryParameter($kwdDbConn, $bidKeywordOwnerId) . "','','" . cleanQueryParameter($kwdDbConn, $bidTransId) . "',now() + Interval $noOfDays $selectedVal,'" . cleanQueryParameter($kwdDbConn, $tradingCommAmt) . "')";

                                            $logCred["bid_transaction_id"] = $bidTransId;
                                            $logCred["commission"] = $tradingCommAmt;

                                        } else {
                                            $query = "INSERT INTO " . $databaseName . "(bidder_id,bidder_email,bidder_mobile,keyword,bid_price,kwd_owner_id,kwd_owner_email,kwd_owner_mobile,bid_transaction_id,commission) VALUES('','" . cleanQueryParameter($kwdDbConn, $email) . "','','" . cleanQueryParameter($kwdDbConn, $keyword) . "','" . cleanQueryParameter($kwdDbConn, $amount) . "','','" . cleanQueryParameter($kwdDbConn, $bidKeywordOwnerId) . "','','" . cleanQueryParameter($kwdDbConn, $bidTransId) . "','" . cleanQueryParameter($kwdDbConn, $tradingCommAmt) . "')";

                                            $logCred["bid_transaction_id"] = $bidTransId;
                                            $logCred["commission"] = $tradingCommAmt;
                                        }

                                        $result = runQuery($query, $kwdDbConn);

                                        if (noError($result)) {

                                            $errMsg = "Inserted into related bids table : success.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            //get keyword table name
                                            $ownershipTableName = getKeywordOwnershipTableName($keyword);

                                            if ($highestBidAmountForKwd >= $amount) {
                                                /*echo "if highestBidAmountForKwd : {$highestBidAmountForKwd}, amount : {$amount}";die;*/
                                                $query = "UPDATE " . $ownershipTableName . " SET no_of_active_bids='" . cleanQueryParameter($kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword. "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                            } else {
                                                /*echo "else highestBidAmountForKwd : {$highestBidAmountForKwd}, amount : {$amount}";die;*/
                                                $query = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='" . $bidTransId. "',highest_bid_amount='" . cleanQueryParameter($kwdDbConn, $amount) . "',no_of_active_bids='" . cleanQueryParameter($kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                            }


                                            $result = runQuery($query, $kwdDbConn);

                                            if (noError($result)) {

                                                $errMsg = "Update number of active bids in ownership table : success.";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $logCred["bid_transaction_id"] = $bidTransId;

                                                $result   = creditUserEarning($user_id, $bidAmtGross, 'blockedbids', 'add');

                                                $response = json_encode($result);

                                                //$result["errCode"][-1] = -1;
                                                if (noError($result)) {

                                                    $errMsg = "Update wallet for addBlockForBids : success.";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $logCred["blockForBidAmount"] = $bidAmtGross;

                                                    //add active bids count of user
                                                    $addBidDetailsByBuyerId = addBidDetailsByBuyerId($kwdDbConn,$email,$bidTransId);

                                                    if (noError($addBidDetailsByBuyerId)) {

                                                        $errMsg = "Update bid details of particular user : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $logCred["blockForBidAmount"] = $bidAmtGross;

                                                    } else {
                                                        $errMsg = "Update bid details of particular user : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    //add active bids count of user
                                                    $addActiveBidsCountOfUser = addActiveBidsCountOfUser($email, $kwdDbConn);

                                                    if (noError($addActiveBidsCountOfUser)) {

                                                        $errMsg = "Update active_bid_count in presale_user table : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $logCred["activeBidsCountOfUser"] = $addActiveBidsCountOfUser;

                                                    } else {

                                                        $errMsg = "Update active_bid_count in presale_user table : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    //code for index page boxes
                                                    $addToHighestBid = addToHighestBid($kwdDbConn, $keyword, $email, $amount, $bidTransId);
                                                    if (noError($addToHighestBid)) {

                                                        $errMsg = "Update addToHighestBid table : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $logCred["addToHighestBid"] = $amount;

                                                    } else {

                                                        $errMsg = "Update addToHighestBid table : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    $addToMostBid = addToMostBid($kwdDbConn, $keyword, $noOfActiveBid, $activeBidsForKeyword);

                                                    if (noError($addToMostBid)) {
                                                        /*$xml .= "<step12 title='set bid price' >12.update addToMostBid table:'success'";
                                                        $xml .= "</step12>\n";*/
                                                        $errMsg = "Update addToMostBid table : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        $logCred["activeBidsForKeyword"] = $activeBidsForKeyword;
                                                    } else {
                                                        /*$xml .= "<step12 title='set bid price' >12.update addToMostBid table:'fail'";
                                                        $xml .= "</step12>\n";*/
                                                        $errMsg = "Update addToMostBid table : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }
                                                    $addToLatestBid = addToLatestBid($kwdDbConn, $keyword, $email, $amount, $bidTransId);

                                                    if (noError($addToLatestBid)) {
                                                        /*$xml .= "<step13 title='set bid price' >13.update addToLatestBid table:'success'";
                                                        $xml .= "</step13>\n";*/
                                                        $errMsg = "Update addToLatestBid table : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    } else {
                                                        /*$xml .= "<step13 title='set bid price' >13.update addToLatestBid table:'fail'";
                                                        $xml .= "</step13>\n";*/
                                                        $errMsg = "Update addToLatestBid table : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    $addToLatestBidAll = addToLatestBidAll($kwdDbConn,$keyword,$email,$amount,$bidTransId);
                                                    if (noError($addToLatestBidAll)) {

                                                        $errMsg = "Add to latestBidAll table : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        $logCred["addToLatestBid"] = $bidTransId;
                                                    } else {

                                                        $errMsg = "Deleted from latestBidAll table : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    $trans = commitTransaction($kwdDbConn);
                                                    if (noError($trans)) {
                                                        $errMsg = "Transaction committed : success.";
                                                        $errCode = -1;
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    } else {
                                                        $errMsg = "Transaction committed : failed.";
                                                        $errCode = 2;
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    mysqli_autocommit($kwdDbConn, TRUE);

                                                    // send email to place bid buyer
                                                    $to      = $email;

                                                    $subject = 'You have placed a bid of '.number_format((float)$amount,4).' '.$keywoDefaultCurrencyName.' on #'. $keyword .' keyword ';
                                                    $message =  '
                                                    <td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>
                                                    
                                                    <tr> <td style="padding: 10px 20px;">
                                                    You have placed a bid of # '. $keyword .' keyword for ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event the keyword owner accepts your bid.
                                                    <br><br>Regards,<br>Team Keywo
                                                    </td>
                                                                                </tr>';
                                                    $method_name = "GET";
                                                    $notification_message = 'You have placed a bid of #'.$keyword.' keyword for ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event the keyword owner accepts your bid.';
                                                    $path    = "";
                                                    $category = "bid";

                                                    $getNotification = notifyoptions($user_id,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                    $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["0"]["permissions"]["_id"];

                                                    $sendNotifEmailToBuyer = sendNotificationBuyPrefrence($to,$subject,$message,$firstName,$lastName,$user_id,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);
                                                    if(noError($sendNotifEmailToBuyer)){
                                                        $errMsg = "Send notification and email to ".$email." : success.";
                                                        $errCode = -1;
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $kwdOwnerUserInfo = $userRequiredFields.",_id";

                                                        $getKwdOwnerUserInfo = getUserInfo($bidKeywordOwnerId, $walletURLIP . 'api/v3/', $kwdOwnerUserInfo);
                                                        if(noError($getKwdOwnerUserInfo)){

                                                            $getKwdOwnerUserInfo = $getKwdOwnerUserInfo["errMsg"];
                                                            $kwdOwnerUserId = $getKwdOwnerUserInfo["_id"];
                                                            $kwdOwnerFirstName = $getKwdOwnerUserInfo["first_name"];
                                                            $kwdOwnerLastName = $getKwdOwnerUserInfo["last_name"];


                                                            // send email and notification to keyword owner.
                                                            $to      = $bidKeywordOwnerId;
                                                            $subject = "Bid received for $keyword";
                                                            $message =
                                                                '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$kwdOwnerFirstName.',</td>                                                    
                                                                <tr> <td style="padding: 10px 20px;">
                                                                    A bid of ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. has been received for #'. $keyword .' keyword.
                                                                <br><br>Regards,<br>Team Keywo
                                                                </td>  </tr>';

                                                            $notification_message = 'A bid of ' . number_format((float)$amount, 4) . ' BTC has been received for #' . $keyword . '.';
                                                            $path    = "";


                                                            $errMsg = "Get ".$bidKeywordOwnerId." user info : success.";
                                                            $errCode = -1;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


                                                            $method_name = "GET";
                                                            $getNotification = notifyoptions($kwdOwnerUserId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                            $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["0"]["permissions"]["_id"];
                                                            $category = "bid";
                                                            $sendNotifEmailToKwdOwner = sendNotificationBuyPrefrence($to,$subject,$message,$kwdOwnerFirstName,$kwdOwnerLastName,$kwdOwnerUserId,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                                                            if(noError($sendNotifEmailToKwdOwner)){
                                                                $errMsg = "You have successfully placed a bid for #".$keyword." keyword";
                                                                $errCode = -1;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                $_SESSION["get_error_code"] = -1;
                                                                $_SESSION["get_error_message"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]="-1";
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/
                                                            }else{
                                                                $errMsg = "There was an error while placing the bid for #".$keyword." keyword.";
                                                                $errCode = 2;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                $_SESSION["get_error_code"] = $errCode;
                                                                $_SESSION["get_error_message"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]=$errCode;
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/
                                                            }
                                                        }else{

                                                            $errMsg = "Get ".$bidKeywordOwnerId." user info : failed.";
                                                            $errCode = 2;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            $_SESSION["get_error_code"] = $errCode;
                                                            $_SESSION["get_error_message"] = $errMsg;

                                                            /******************** for logs user analytics ****************/
                                                            $responseArr["errCode"]=$errCode;
                                                            $responseArr["errMsg"]=$errMsg;
                                                            $xml_data['response']["data"] = "";
                                                            $xml_data['response']["attributes"] = $responseArr;
                                                            /******************** for logs user analytics ****************/
                                                        }
                                                    }else{
                                                        $errMsg = "Send notification and email to ".$email." : failed.";
                                                        $errCode = 2;
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    }

                                                    $_SESSION["get_error_code"] = $errCode;
                                                    $_SESSION["get_error_message"] = $errMsg;

                                                    $retArray["errCode"] = $errCode;
                                                    $retArray["errMsg"]  = $errMsg;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]=$errCode;
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/
                                                } else {

                                                    $errMsg = "Update wallet for addBlockForBids : failed.";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    //rollback transction
                                                    rollbackTransaction($kwdDbConn);
                                                    $retArray["errCode"] = 2;
                                                    $retArray["errMsg"]  = $errMsg;

                                                    $_SESSION["get_error_code"] = 2;
                                                    $_SESSION["get_error_message"] = $errMsg;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]=2;
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/
                                                }

                                            } else {

                                                $errMsg = "Update number of active bids in ownership table : failed.";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                //rollback transction
                                                rollbackTransaction($kwdDbConn);
                                                $retArray["errCode"] = 2;
                                                $retArray["errMsg"]  = $errMsg;

                                                $_SESSION["get_error_code"] = 2;
                                                $_SESSION["get_error_message"] = $errMsg;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]=2;
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/
                                            }

                                        } else {

                                            $errMsg = "Inserted into related bids table : failed.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            //rollback transction
                                            rollbackTransaction($kwdDbConn);
                                            $retArray["errCode"] = 2;
                                            $retArray["errMsg"]  = $errMsg;

                                            $_SESSION["get_error_code"] = 2;
                                            $_SESSION["get_error_message"] = $errMsg;

                                            /******************** for logs user analytics ****************/
                                            $responseArr["errCode"]=2;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;
                                            /******************** for logs user analytics ****************/
                                        }

                                    }else{

                                        $errMsg = "Start transaction : failed.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $retArray["errCode"] = 2;
                                        $retArray["errMsg"] = $errMsg;

                                        $_SESSION["get_error_code"] = 2;
                                        $_SESSION["get_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=2;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }
                                }else{

                                    $errMsg = "Bidder is not keyword owner id : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $retArray["errCode"] = 2;
                                    $retArray["errMsg"] = $errMsg;

                                    $_SESSION["get_error_code"] = 2;
                                    $_SESSION["get_error_message"] = $errMsg;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]=2;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/
                                }

                            }else{

                                $errMsg = "Bidder bid not exist : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $retArray["errCode"] = 2;
                                $retArray["errMsg"] = $errMsg;

                                $_SESSION["get_error_code"] = 2;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/
                            }
                        }else{

                            $errMsg = "User available balance greater than bid gross amount : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $retArray["errCode"] = 51;
                            $retArray["errMsg"] = "Your bid amount exceed than your available balance.";

                            $_SESSION["get_error_code"] = $retArray["errCode"];
                            $_SESSION["get_error_message"] = $retArray["errMsg"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=51;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }
                        
                    }else{

                        $errMsg = "Get keyword ownership details : failed.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $retArray["errCode"] = 2;
                        $retArray["errMsg"] = $errMsg;

                        $_SESSION["get_error_code"] = 2;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=51;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }
                    
                }else{

                    $errMsg = "Get admin setting from keyword admin : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $retArray["errCode"] = 2;
                    $retArray["errMsg"] = $errMsg;

                    $_SESSION["get_error_code"] = 2;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
                
            } else {

                $errMsg = "Error calculating user available balance : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $retArray["errCode"] = 2;
                $retArray["errMsg"] = $errMsg;

                $_SESSION["get_error_code"] = 2;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }
        } else {

            $errMsg = "Get bidder user info : failed.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $retArray["errCode"] = 2;
            $retArray["errMsg"]  = $errMsg;

            $_SESSION["get_error_code"] = 2;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }

    } else {

        $errMsg = "Bid amount greater than minimum trade amount : failed.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $retArray["errCode"] = 2;
        $retArray["errMsg"]  = $errMsg;

        $_SESSION["get_error_code"] = 2;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $retArray["credential"]  = $logCred;
    $retArray["xml_data"]  = $xml_data;

   /* if($retArray["errCode"] == -1){
        $retArray["errMsg"] = "You have successfully placed bid on ".$keyword;
    }else{
        $retArray["errCode"] = $retArray["errCode"];
        $retArray["errMsg"] = $errMsg;

    }*/

    return $retArray;
}

function editBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $amount, $user_id, &$xml_data, &$stepCounter, $xml_atrr){

    global $userRequiredFields, $minimumTradingAmount, $walletURLIP,$keywoDefaultCurrencyName;
    $retArray = array();
    $logCred = array();
    $extraArgs = '';

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount);

    //bid time
    $bidTime = time();
    $bidDate = date("Y-m-d h:i:s", $bidTime);

    $logCred["bidTime"] = $bidTime;
    $logCred["bidDate"] = $bidDate;

    $errMsg = "Start executing edit bid method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    if ($amount >= $minimumTradingAmount) {

        $errMsg = "Bid amount greater than minimum trading amount : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        // get user details with available balance if login
        $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);

        if (noError($getUserDetails)) {
            //$editBidderUserId = $getUserDetails["errMsg"]["_id"];
            $editBidderFirstName = $getUserDetails["errMsg"]["first_name"];
            $editBidderLastName = $getUserDetails["errMsg"]["last_name"];

            $errMsg = "Get bidder user info : success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $getUserBalance = calculateUserBalance($getUserDetails);
            if (noError($getUserBalance)) {

                $errMsg = "Error calculating user available balance : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];

                //get Admin Settings
                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                if (noError($adminSettings)) {

                    $errMsg = "Get admin setting from keyword admin : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $kycDetails = getCurrentKYCLimits($searchConn,$email);
    
                    $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];
                    // $tradingCommPercent = $adminSettings["data"]["trading_commision_percent"];
                    $kwdRenewalFeesPerYear = $adminSettings["data"]["kwd_renewal_fees_per_year"];
                    $logCred["tradingCommPercent"] = $tradingCommPercent;
                    $logCred["renewalAmount"] = $kwdRenewalFeesPerYear;

                    //gross bid amount
                    $tradingCommAmt = number_format((float)($amount * $tradingCommPercent)/100,8);
                    $tradingCommAmt = number_format((float)$tradingCommAmt, 8);
                    $kwdRenewalFeesPerYear  = number_format((float)$kwdRenewalFeesPerYear, 8);
                    //added renewal fees in bid gross amount
                    //$bidAmtGross    = $bidAmt + $tradingCommAmt + $kwdRenewalFeesPerYear;
                    $bidAmtGross = $amount + $tradingCommAmt + $kwdRenewalFeesPerYear;
                    //$bidAmtGross = number_format($bidAmtGross, 8);

                    $bidAmtGross = (float)$bidAmtGross;
                    //bid time
                    $bidTime = time();
                    $bidDate = date("Y-m-d h:i:s", $bidTime);
                    //get bids transaction id
                    //added renewal fees in bid transaction ID
                    $bidTransId = $databaseName . "~~" . $email . "~~" . rand(111111, 999999) . "~~" . $amount . "~~" . $tradingCommAmt . "~~" . $bidDate . "~~" . $keyword . "~~" . $user_id."~~".$kwdRenewalFeesPerYear;

                    $logCred["tradingCommAmt"] = $tradingCommPercent;
                    $logCred["bidAmtGross"] = $bidAmtGross;
                    $logCred["bidTransId"] = $bidTransId;

                    //check for keyword owner
                    $bidKeywordDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                    if (noError($bidKeywordDetails)) {
                        $bidKeywordOwnerId = $bidKeywordDetails["errMsg"]["buyer_id"];
                        $activeBidsForKeyword = $bidKeywordDetails["errMsg"]["active_bids"];
                        $highestActiveBidIdForKeyword = $bidKeywordDetails["errMsg"]["highest_bid_id"];
                        $activeBidsForKeyword = json_decode($activeBidsForKeyword, true);

                        $errMsg = "Get keyword ownership details : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                        //check if the bid for keyword by above bidder exists
                        $editBidtxID = null;
                        foreach ($activeBidsForKeyword as $value) {
                            $activeBidArr = explode("~~", $value);
                            $bidderEmail = $activeBidArr[1];
                            if ($bidderEmail == $email) {
                                $myBidAmt = $activeBidArr[3];
                                $editBidtxID = $value;
                            }
                        }

                        //remove last bidtxId from active bids array
                        $activeBidsForKeyword = array_diff($activeBidsForKeyword, array(
                            $editBidtxID
                        ));

                        //if editBidTx ID is heighest bidTxID
                        // get highest bid amount from current active bids
                        //if($highestActiveBidIdForKeyword  == $editBidtxID){
                        $newHighestTxnId = "";
                        $newHighestTxnAmt = 0;
                        foreach ($activeBidsForKeyword as $value) {
                            $activeBidsArr = explode("~~", $value);
                            $otherBidAmt = $activeBidsArr[3];
                            $otherBidId = $activeBidsArr[3];
                            if ($otherBidAmt > $newHighestTxnAmt) {
                                $newHighestTxnAmt = $otherBidAmt;
                                $newHighestTxnId = $value;
                            }
                        }


                        //get current bid Amount
                        $currentBidDetails = explode("~~", $editBidtxID);

                        $returnBidAmt      = $currentBidDetails[3];
                        $lastCommision     = $currentBidDetails[4];
                        $lastRenewalAmt    = $currentBidDetails[8];
                        $returnBidAmtGross = number_format((float)$returnBidAmt + $lastCommision + $lastRenewalAmt,8);

                        //get bids table name to
                        $bidTableName = explode("~~", $editBidtxID);
                        $bidTableName = $bidTableName[0];

                        if ($userAvailableBalance >= ($bidAmtGross - $returnBidAmtGross)) {
                            //insert current bid in active bids
                            $activeBidsForKeyword[] = $bidTransId;
                            $activeBidsForKeyword = json_encode($activeBidsForKeyword, JSON_UNESCAPED_UNICODE);

                            $highestBidAmountForKwd = $bidKeywordDetails["errMsg"]["highest_bid_amount"];

                            /*$highestBidAmountForKwd = number_format((float)$highestBidAmountForKwd, 8);*/
                            $highestBidAmountForKwd = (float)$highestBidAmountForKwd;

                            if (!isset($highestBidAmountForKwd) && empty($highestBidAmountForKwd)) {
                                $highestBidAmountForKwd = 0;
                            }
                            $noOfActiveBidForKwd = $bidKeywordDetails["errMsg"]["no_of_active_bids"];
                            if (!isset($noOfActiveBidForKwd) && empty($noOfActiveBidForKwd)) {
                                $noOfActiveBidForKwd = 0;
                            }
                            $noOfActiveBid = $noOfActiveBidForKwd + 1;

                            $errMsg = "User available balance greater than bid gross amount : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $logCred["activeBidsForKeyword"] = $activeBidsForKeyword;

                            if ($bidKeywordOwnerId != $email) {

                                $errMsg = "Bidder is not keyword owner id : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                                $trans = startTransaction($kwdDbConn);
                                if (noError($trans)) {

                                    $errMsg = "Start transaction : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    //insert into related bids table
                                    //echo $noOfDays;
                                    if (isset($noOfDays) && !empty($noOfDays)) {
                                        $query = "INSERT INTO " . $databaseName . "(bidder_id,bidder_email,bidder_mobile,keyword,bid_price,kwd_owner_id,kwd_owner_email,kwd_owner_mobile,bid_transaction_id,bid_expire_time,commission) VALUES('','" . cleanQueryParameter($kwdDbConn, $email) . "','','" . cleanQueryParameter($kwdDbConn, $keyword) . "','" . cleanQueryParameter($kwdDbConn, $amount) . "','','" . cleanQueryParameter($kwdDbConn, $bidKeywordOwnerId) . "','','" . cleanQueryParameter($kwdDbConn, $bidTransId) . "',now() + Interval $noOfDays $selectedVal,'" . cleanQueryParameter($kwdDbConn, $tradingCommAmt) . "')";
                                        //echo "***1***";

                                        $logCred["bid_transaction_id"] = $bidTransId;
                                        $logCred["commission"] = $tradingCommAmt;

                                    } else {
                                        //echo "***2***";
                                        $query = "INSERT INTO " . $databaseName . "(bidder_id,bidder_email,bidder_mobile,keyword,bid_price,kwd_owner_id,kwd_owner_email,kwd_owner_mobile,bid_transaction_id,commission) VALUES('','" . cleanQueryParameter($kwdDbConn, $email) . "','','" . cleanQueryParameter($kwdDbConn, $keyword) . "','" . cleanQueryParameter($kwdDbConn, $amount) . "','','" . cleanQueryParameter($kwdDbConn, $bidKeywordOwnerId) . "','','" . cleanQueryParameter($kwdDbConn, $bidTransId) . "','" . cleanQueryParameter($kwdDbConn, $tradingCommAmt) . "')";

                                        $logCred["bid_transaction_id"] = $bidTransId;
                                        $logCred["commission"] = $tradingCommAmt;
                                    }

                                    $result = runQuery($query, $kwdDbConn);

                                    if (noError($result)) {

                                        $errMsg = "Inserted into related bids table : success.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                        //get keyword table name
                                        $ownershipTableName = getKeywordOwnershipTableName($keyword);
                                        // remaining ....


                                        if ($newHighestTxnAmt >= $amount) {
                                            $query = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='" . cleanQueryParameter($kwdDbConn, $newHighestTxnId) . "',highest_bid_amount='" . cleanQueryParameter($kwdDbConn, $newHighestTxnAmt) . "',no_of_active_bids='" . cleanQueryParameter($kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "'";

                                            $errMsg = "New highest bid is greater than amount : success.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $logCred["newHighestBidTxnId"] = $newHighestTxnId;
                                            $logCred["newHighestBidTxnAmt"] = $newHighestTxnAmt;
                                        } else {
                                            $query = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='" . cleanQueryParameter($kwdDbConn, $bidTransId) . "',highest_bid_amount='" . cleanQueryParameter($kwdDbConn, $amount) . "',no_of_active_bids='" . cleanQueryParameter($kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                        }

                                        //echo $query;die();
                                        $result = runQuery($query, $kwdDbConn);

                                        if (noError($result)) {

                                            $errMsg = "update ownershipTable table : success.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $query = "UPDATE " . $bidTableName . " SET bid_status = '4' WHERE bid_transaction_id = '" . $editBidtxID . "' AND bid_status = '0';";
                                            $result = runQuery($query, $kwdDbConn);
                                            if (noError($result)) {
                                                ///////////////////////////////////////////////////////////

                                                $errMsg = "Update bid status in bidTable : success.";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $mysql_affected_rows = mysqli_affected_rows($kwdDbConn);
                                                if ($mysql_affected_rows > 0) {

                                                    $errMsg = "Editing the bid keyword : success.";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $logCred["mysqlAffectedRows"] = $mysql_affected_rows;

                                                    $result = creditUserEarning($user_id, $bidAmtGross, 'blockedbids', 'add');
                                                    $response = json_encode($result);
                                                    if (noError($result)) {

                                                        $errMsg = "Update wallet for addBlockForBids : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $logCred["blockForBidAmount"] = $bidAmtGross;

                                                        $deductBlockedForBids = creditUserEarning($user_id, $returnBidAmtGross, 'blockedbids', 'deduct');
                                                        $response = json_encode($deductBlockedForBids);
                                                        if (noError($result)) {
                                                            /*$xml .= "<step9 title='edit Bid price' >9.|deductBlockedForBids| response:'" . urlencode($response) . ",returnBidAmtGross:" . urlencode($returnBidAmtGross) . "' 'success'";
                                                            $xml .= "</step9>\n";*/

                                                            $errMsg = "Update wallet for deductBlockedForBids : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            $logCred["returnBidAmtGross"] = $returnBidAmtGross;

                                                            //add active bids to users json
                                                            $addBidDetailsByBuyerId = addBidDetailsByBuyerId($kwdDbConn, $email, $bidTransId);
                                                            if (noError($addBidDetailsByBuyerId)) {

                                                                $errMsg = "Update/add bid details of particular user : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                $logCred["addActiveBids"] = $bidTransId;

                                                            } else {

                                                                $errMsg = "Update bid details of particular user : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            }

                                                            //remove active bids from users json
                                                            $removeBidDetailsByBuyerId = removeBidDetailsByBuyerId($kwdDbConn, $email, $editBidtxID, $keyword);
                                                            if (noError($removeBidDetailsByBuyerId)) {

                                                                $errMsg = "Remove active bid details from user json : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                $logCred["removeActiveBids"] = $editBidtxID;

                                                            } else {

                                                                $errMsg = "Remove active bid details from user json : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }
                                                            //code for index page box
                                                            $deleteFromHighestBid = deleteFromHighestBid($kwdDbConn, $editBidtxID);
                                                            if (noError($deleteFromHighestBid)) {
                                                                /*$xml .= "<step10 title='edit Bid price' >10.update deleteFromHighestBid table 'success'";
                                                                $xml .= "</step10>\n";*/
                                                                $errMsg = "Update deleteFromHighestBid table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            } else {
                                                                /*$xml .= "<step10 title='edit Bid price' >10.update deleteFromHighestBid table 'fail'";
                                                                $xml .= "</step10>\n";*/
                                                                $errMsg = "Update deleteFromHighestBid table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $addToHighestBid = addToHighestBid($kwdDbConn, $keyword, $email, $amount, $bidTransId);
                                                            if (noError($addToHighestBid)) {
                                                                /*$xml .= "<step11 title='edit Bid price' >11.update addToHighestBid table 'success'";
                                                                $xml .= "</step11>\n";*/
                                                                $errMsg = "Update addToHighestBid table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            } else {
                                                                /*$xml .= "<step11 title='edit Bid price' >11.update addToHighestBid table 'fail'";
                                                                $xml .= "</step11>\n";*/
                                                                $errMsg = "Update addToHighestBid table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $addToLatestBid = addToLatestBid($kwdDbConn, $keyword, $email, $amount, $bidTransId);
                                                            if (noError($addToLatestBid)) {

                                                                $errMsg = "Update addToLatestBid table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            } else {

                                                                $errMsg = "Update addToLatestBid table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            }
                                                            $deleteFromLatestBid = deleteFromLatestBid($kwdDbConn, $editBidtxID);
                                                            if (noError($deleteFromLatestBid)) {

                                                                $errMsg = "Update deleteFromLatestBid table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            } else {

                                                                $errMsg = "Update deleteFromLatestBid table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $rectifyHighestBidTable = rectifyHighestBidTable($kwdDbConn, $keyword);
                                                            if (noError($rectifyHighestBidTable)) {

                                                                $errMsg = "Update rectifyHighestBidTable table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            } else {

                                                                $errMsg = "Update rectifyHighestBidTable table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $deleteFromLatestBidAll = deleteFromLatestBidAll($kwdDbConn, $editBidtxID);
                                                            if (noError($deleteFromLatestBidAll)) {

                                                                $errMsg = "Deleted from  latestBidAll table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                                $logCred["deleteFromLatestBid"] = $editBidtxID;

                                                            } else {

                                                                $errMsg = "Deleted from latestBidAll table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $addToLatestBidAll = addToLatestBidAll($kwdDbConn, $keyword, $email, $amount, $bidTransId);
                                                            if (noError($addToLatestBidAll)) {

                                                                $errMsg = "Add to latestBidAll table : success.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                                $logCred["addToLatestBid"] = $bidTransId;
                                                            } else {

                                                                $errMsg = "Deleted from latestBidAll table : failed.";
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $trans = commitTransaction($kwdDbConn);
                                                            if (noError($trans)) {
                                                                $errMsg = "Transaction committed : success.";
                                                                $errCode = -1;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            } else {
                                                                $errMsg = "Transaction committed : failed.";
                                                                $errCode = 2;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            mysqli_autocommit($kwdDbConn, TRUE);

                                                            //send mail for edit bid-buyer
                                                            $to      = $email;
                                                            $subject = "You have edited bid amount on #$keyword";
                                                            $message =
                                                                '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                <tr> 
                                                                    <td style="padding: 10px 20px;">
                                                                    You have changed the bidding amount placed on #'.$keyword.' from ' . number_format((float)$returnBidAmt, 4) . ' '.$keywoDefaultCurrencyName.' to ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event the keyword owner accepts the bid.<br><br>Regards,<br>Team Keywo.
                                                                     </td>
                                                                </tr>';

                                                            $notification_message = 'You have changed the bidding amount placed on #' . $keyword . ' from ' . number_format((float)$returnBidAmt, 4) . ' '.$keywoDefaultCurrencyName.' to ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event the keyword owner accepts the bid.';

                                                            $path    = "";

                                                            $method_name = "GET";
                                                            $getNotification = notifyoptions($user_id,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                            $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["1"]["permissions"]["_id"];

                                                            //SendMail($to, $subject, $message, $path, $bidKeyword);
                                                            $category = "bid";
                                                            $sendNotifEmail = sendNotificationBuyPrefrence($to,$subject,$message,$editBidderFirstName,$editBidderLastName,$user_id,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                                                            if(noError($sendNotifEmail)){

                                                                $errMsg = "Send email and notification to ".$email." : success.";
                                                                $errCode = -1;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
//
                                                                $kwdOwnerReqField = $userRequiredFields.",_id";

                                                                $getKeywordOwnerInfo = getUserInfo($bidKeywordOwnerId, $walletURLIP . 'api/v3/', $kwdOwnerReqField);
                                                                if(noError($getKeywordOwnerInfo)){
                                                                    $errMsg = "Getting user info of ".$bidKeywordOwnerId." : success.";
                                                                    $errCode = -1;
                                                                    $stepCounter++;
                                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                    $getKeywordOwnerInfo = $getKeywordOwnerInfo["errMsg"];
                                                                    $kwdOwnerUserId =  $getKeywordOwnerInfo["_id"];
                                                                    $kwdOwnerFirstName = $getKeywordOwnerInfo["first_name"];
                                                                    $kwdOwnerLastName = $getKeywordOwnerInfo["last_name"];

                                                                    //send mail for edit bid-seller
                                                                    $to      = $bidKeywordOwnerId;
                                                                    $subject = "Keywo: Keyword bid changed";
                                                                    $message =
                                                                        '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$kwdOwnerFirstName.',</td>                                                    
                                                                <tr> <td style="padding: 10px 20px;">
                                                                    A bid on your keyword of ' . number_format((float)$returnBidAmt, 4) . ' '.$keywoDefaultCurrencyName.'. has been changed to  ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'.
                                                                <br><br>Regards,<br>Team Keywo
                                                                </td>  </tr>';

                                                                    $notification_message = 'A bid on your keyword of ' . number_format((float)$returnBidAmt, 4) . ' '.$keywoDefaultCurrencyName.' has been changed to ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'.';
                                                                    $path    = "";
                                                                    $method_name = "GET";
                                                                    $getNotification = notifyoptions($kwdOwnerUserId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                                    $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["1"]["permissions"]["_id"];
                                                                    $category = "bid";
                                                                    $sendNotifEmailToKwdOwner = sendNotificationBuyPrefrence($to,$subject,$message,$kwdOwnerFirstName,$kwdOwnerLastName,$kwdOwnerUserId,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                                                                    if(noError($sendNotifEmailToKwdOwner)){

                                                                        $errMsg = "You have successfully edited the bid on #".$keyword." keyword";
                                                                        $errCode = -1;
                                                                        $stepCounter++;
                                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                        $_SESSION["get_error_code"] = -1;
                                                                        $_SESSION["get_error_message"] = $errMsg;

                                                                        /******************** for logs user analytics ****************/
                                                                        $responseArr["errCode"]="-1";
                                                                        $responseArr["errMsg"]=$errMsg;
                                                                        $xml_data['response']["data"] = "";
                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                        /******************** for logs user analytics ****************/


                                                                    }else{
                                                                        $errMsg = "There was an error while editing the bid on #".$keyword." keyword.";
                                                                        $errCode = 2;
                                                                        $stepCounter++;
                                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                        $_SESSION["get_error_code"] = $errCode;
                                                                        $_SESSION["get_error_message"] = $errMsg;

                                                                        /******************** for logs user analytics ****************/
                                                                        $responseArr["errCode"]=$errCode;
                                                                        $responseArr["errMsg"]=$errMsg;
                                                                        $xml_data['response']["data"] = "";
                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                        /******************** for logs user analytics ****************/
                                                                    }
                                                                }else{
                                                                    $errMsg = "Getting user info of ".$bidKeywordOwnerId." : failed.";
                                                                    $errCode = 2;
                                                                    $stepCounter++;
                                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                    $_SESSION["get_error_code"] = $errCode;
                                                                    $_SESSION["get_error_message"] = $errMsg;

                                                                    /******************** for logs user analytics ****************/
                                                                    $responseArr["errCode"]=$errCode;
                                                                    $responseArr["errMsg"]=$errMsg;
                                                                    $xml_data['response']["data"] = "";
                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                    /******************** for logs user analytics ****************/
                                                                }
                                                            }else{
                                                                $errMsg = "Send email and notification to ".$email." : failed.";
                                                                $errCode = 2;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            }

                                                            $retArray["errCode"] = $errCode;
                                                            $retArray["errMsg"] = $errMsg;

                                                            $_SESSION["get_error_code"] = $errCode;
                                                            $_SESSION["get_error_message"] = $errMsg;

                                                            /******************** for logs user analytics ****************/
                                                            $responseArr["errCode"]=$errCode;
                                                            $responseArr["errMsg"]=$errMsg;
                                                            $xml_data['response']["data"] = "";
                                                            $xml_data['response']["attributes"] = $responseArr;
                                                            /******************** for logs user analytics ****************/

                                                        } else {

                                                            $errMsg = "Update wallet for deductBlockedForBids : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            /* Error in deductBlockedForBids */
                                                            //rollback transction
                                                            rollbackTransaction($kwdDbConn);
                                                            $retArray["errCode"] = 2;
                                                            $retArray["errMsg"] = $errMsg;

                                                            $_SESSION["get_error_code"] = 2;
                                                            $_SESSION["get_error_message"] = $errMsg;

                                                            /******************** for logs user analytics ****************/
                                                            $responseArr["errCode"]=2;
                                                            $responseArr["errMsg"]=$errMsg;
                                                            $xml_data['response']["data"] = "";
                                                            $xml_data['response']["attributes"] = $responseArr;
                                                            /******************** for logs user analytics ****************/
                                                        }

                                                    } else {

                                                        $errMsg = "Update wallet for addBlockForBids : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        /*Error in addBlockedForBids */
                                                        rollbackTransaction($kwdDbConn);
                                                        $retArray["errCode"] = 2;
                                                        $retArray["errMsg"] = $errMsg;

                                                        $_SESSION["get_error_code"] = 2;
                                                        $_SESSION["get_error_message"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=2;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/
                                                    }
                                                } else {

                                                    $errMsg = "Editing the bid keyword : failed.";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    /* Error in edit bid price details*/
                                                    $retArray["errCode"] = 2;
                                                    $retArray["errMsg"] = $errMsg;

                                                    $_SESSION["get_error_code"] = 2;
                                                    $_SESSION["get_error_message"] = $errMsg;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]=2;
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/
                                                }
                                            } else {

                                                $errMsg = "Update bid status in bidTable : failed.";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                /*error in update set bid status*/
                                                $retArray["errCode"] = 2;
                                                $retArray["errMsg"] = $errMsg;

                                                $_SESSION["get_error_code"] = 2;
                                                $_SESSION["get_error_message"] = $errMsg;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]=2;
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/
                                            }

                                        } else {

                                            $errMsg = "update ownershipTable table : failed.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                            /*error update highet bit table */
                                            $retArray["errCode"] = 2;
                                            $retArray["errMsg"] = $errMsg;

                                            $_SESSION["get_error_code"] = 2;
                                            $_SESSION["get_error_message"] = $errMsg;

                                            /******************** for logs user analytics ****************/
                                            $responseArr["errCode"]=2;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;
                                            /******************** for logs user analytics ****************/
                                        }

                                    } else {

                                        $errMsg = "Inserted into related bids table : failed.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                        /*error in insert query*/
                                        $retArray["errCode"] = 2;
                                        $retArray["errMsg"] = $errMsg;

                                        $_SESSION["get_error_code"] = 2;
                                        $_SESSION["get_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=2;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }

                                } else {

                                    $errMsg = "Start transaction : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    /* error start transaction */
                                    $retArray["errCode"] = 2;
                                    $retArray["errMsg"] = $errMsg;

                                    $_SESSION["get_error_code"] = 2;
                                    $_SESSION["get_error_message"] = $errMsg;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]=2;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/
                                }
                            } else {

                                $errMsg = "Bidder is not keyword owner id : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                /* keyword owner id not match */
                                $retArray["errCode"] = 2;
                                $retArray["errMsg"] = $errMsg;

                                $_SESSION["get_error_code"] = 2;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/
                            }

                        } else {

                            $errMsg = "User available balance greater than bid gross amount : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $retArray["errCode"] = 51;
                            $retArray["errMsg"] = "Your bid amount exceed than your available balance.";

                            $_SESSION["get_error_code"] = $retArray["errCode"];
                            $_SESSION["get_error_message"] = $retArray["errMsg"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=51;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }
                    }else{

                        $errMsg = "Get keyword ownership details : failed.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /*error getting keyword ownership details*/
                        $retArray["errCode"] = 2;
                        $retArray["errMsg"] = $errMsg;

                        $_SESSION["get_error_code"] = 2;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=2;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }
                }else{

                    $errMsg = "Get admin setting from keyword admin : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /*error getting keyword admin settings */
                    $retArray["errCode"] = 2;
                    $retArray["errMsg"] = $errMsg;

                    $_SESSION["get_error_code"] = 2;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

                $errMsg = "Error calculating user available balance : failed.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                /*error calculation user available bal*/
                $retArray["errCode"] = 2;
                $retArray["errMsg"] = $errMsg;

                $_SESSION["get_error_code"] = 2;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }
        }else{

            $errMsg = "Get bidder user info : failed.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            /*error getting user details*/
            $retArray["errCode"] = 2;
            $retArray["errMsg"] = $errMsg;

            $_SESSION["get_error_code"] = 2;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }
    }else{

        $errMsg = "Bid amount greater than minimum trading amount : failed.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        /*error amount is less than trading amount*/
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = $errMsg;

        $_SESSION["get_error_code"] = 2;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $retArray["credential"]  = $logCred;
    $retArray["xml_data"]  = $xml_data;

    return $retArray;
}

function deleteBidPrice($kwdDbConn, $searchConn, $databaseName, $keyword, $email, $username, $rootUrl, $amount, $user_id, &$xml_data, &$stepCounter, $xml_atrr){

    global $userRequiredFields, $minimumTradingAmount, $walletURLIP;
    $retArray = array();
    $logCred = array();
    $extraArgs = '';

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount);

    //bid time
    $bidTime = time();
    $bidDate = date("Y-m-d h:i:s", $bidTime);

    $logCred["bidTime"] = $bidTime;
    $logCred["bidDate"] = $bidDate;

    $errMsg = "Start executing delete bid method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    if ($amount >= $minimumTradingAmount) {
        $errMsg = "Bid amount greater than minimum trading amount : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
        // get user details with available balance if login
        $requiredFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

        $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);
        if (noError($getUserDetails)) {

            $deleteBidderFirstName = $getUserDetails["errMsg"]["first_name"];
            $deleteBidderLastName = $getUserDetails["errMsg"]["last_name"];

            $errMsg = "Get bidder user info : success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $getUserBalance = calculateUserBalance($getUserDetails);
            if (noError($getUserBalance)) {

                $errMsg = "Error calculating user available balance : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $userAvailableBalance = $getUserBalance['errMsg']['total_available_balance'];

                //get Admin Settings
                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                if (noError($adminSettings)) {
                    // $tradingCommPercent = $adminSettings["data"]["trading_commision_percent"];
                    $kycDetails = getCurrentKYCLimits($searchConn,$email);
                    $tradingCommPercent = $kycDetails["errMsg"]["transaction_fees"];
                    $kwdRenewalFeesPerYear = $adminSettings["data"]["kwd_renewal_fees_per_year"];
                    $logCred["tradingCommPercent"] = $tradingCommPercent;
                    $logCred["renewalAmount"] = $kwdRenewalFeesPerYear;

                    $errMsg = "Get admin setting from keyword admin : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    //gross bid amount
                    $tradingCommAmt = number_format((float)($amount * $tradingCommPercent)/100,8);
                    $tradingCommAmt = number_format((float)$tradingCommAmt, 8);
                    //added renewal fees in bid gross amount
                    //$bidAmtGross    = $bidAmt + $tradingCommAmt + $kwdRenewalFeesPerYear;
                    $bidAmtGross = $amount + $tradingCommAmt + $kwdRenewalFeesPerYear;
                    $bidAmtGross = number_format($bidAmtGross, 8);

                    //bid time
                    $bidTime = time();
                    $bidDate = date("Y-m-d h:i:s", $bidTime);
                    //get bids transaction id
                    //added renewal fees in bid transaction ID
                    $bidTransId = $databaseName . "~~" . $email . "~~" . rand(111111, 999999) . "~~" . $amount . "~~" . $tradingCommAmt . "~~" . $bidDate . "~~" . $keyword . "~~" . $user_id."~~".$kwdRenewalFeesPerYear;

                    $logCred["tradingCommAmt"] = $tradingCommPercent;
                    $logCred["bidAmtGross"] = $bidAmtGross;
                    $logCred["bidTransId"] = $bidTransId;

                    //check for keyword owner
                    $bidKeywordDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                    if (noError($bidKeywordDetails)) {
                        $bidKeywordOwnerId = $bidKeywordDetails["errMsg"]["buyer_id"];
                        $activeBidsForKeyword = $bidKeywordDetails["errMsg"]["active_bids"];
                        $highestActiveBidIdForKeyword = $bidKeywordDetails["errMsg"]["highest_bid_id"];
                        $activeBidsForKeyword = json_decode($activeBidsForKeyword, true);

                        $errMsg = "Get keyword ownership details : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                        //check if the bid for keyword by above bidder exists
                        $editBidtxID = null;
                        foreach ($activeBidsForKeyword as $value) {
                            $activeBidArr = explode("~~", $value);
                            $bidderEmail = $activeBidArr[1];
                            if ($bidderEmail == $email) {
                                $myBidAmt = $activeBidArr[3];
                                $editBidtxID = $value;
                            }
                        }

                        //remove last bidtxId from active bids array
                        $activeBidsForKeyword = array_diff($activeBidsForKeyword, array(
                            $editBidtxID
                        ));

                        //if editBidTx ID is heighest bidTxID
                        if ($highestActiveBidIdForKeyword == $editBidtxID) {
                            $newHighestTxnId  = "";
                            $newHighestTxnAmt = 0;
                            foreach ($activeBidsForKeyword as $value) {
                                $activeBidsArr = explode("~~", $value);
                                $otherBidAmt   = $activeBidsArr[3];
                                $otherBidId    = $activeBidsArr[3];
                                if ($otherBidAmt > $newHighestTxnAmt) {
                                    $newHighestTxnAmt = $otherBidAmt;
                                    $newHighestTxnId  = $value;
                                }
                            }
                        }


                        //get current bid Amount
                        $currentBidDetails = explode("~~", $editBidtxID);
                        $bidAmt              = $currentBidDetails[3];
                        $commissionBidAmt    = $currentBidDetails[4];
                        $renewalAmt          = $currentBidDetails[8];
                        $bidAmtGross = number_format((float)$bidAmt + $commissionBidAmt + $renewalAmt,8);

                        //get bids table name to
                        $bidTableName = explode("~~", $editBidtxID);
                        $bidTableName = $bidTableName[0];


                        //insert current bid in active bids
                        /*$activeBidsForKeyword[] = $bidTransId;*/
                        $activeBidsForKeyword = json_encode($activeBidsForKeyword, JSON_UNESCAPED_UNICODE);
                        $highestBidAmountForKwd = $bidKeywordDetails["data"]["highest_bid_amount"];
                        $highestBidAmountForKwd = number_format((float)$highestBidAmountForKwd, 8);
                        if (!isset($highestBidAmountForKwd) && empty($highestBidAmountForKwd)) {
                            $highestBidAmountForKwd = 0;
                        }
                        $noOfActiveBidForKwd = $bidKeywordDetails["errMsg"]["no_of_active_bids"];
                        if (!isset($noOfActiveBidForKwd) && empty($noOfActiveBidForKwd)) {
                            $noOfActiveBidForKwd = 0;
                        }
                        $noOfActiveBid = $noOfActiveBidForKwd - 1;
                        if ($noOfActiveBid < 1) {
                            $noOfActiveBid = 0;
                        }

                        if ($bidKeywordOwnerId != $email) {

                            $errMsg = "Bidder is not keyword owner id : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $logCred["bidKeywordOwnerId"] = $bidKeywordOwnerId;

                            $trans = startTransaction($kwdDbConn);
                            if (noError($trans)) {

                                    $errMsg = "Start transaction : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    //get keyword table name
                                    $ownershipTableName = getKeywordOwnershipTableName($keyword);
                                    // remaining ....

                                    if ($highestActiveBidIdForKeyword == $editBidtxID) {
                                        $query = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='" . cleanQueryParameter($kwdDbConn, $editBidtxID) . "',highest_bid_amount='" . cleanQueryParameter($kwdDbConn, $editBidtxID) . "',no_of_active_bids='" . cleanQueryParameter( $kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword . "' WHERE keyword='" . cleanQueryParameter( $kwdDbConn, $keyword) . "'";
                                    } else {
                                        $query = "UPDATE " . $ownershipTableName . " SET no_of_active_bids='" . cleanQueryParameter($kwdDbConn, $noOfActiveBid) . "',active_bids='" . $activeBidsForKeyword . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                    }

                                    //echo $query;die();
                                    $result = runQuery($query, $kwdDbConn);

                                    if (noError($result)) {
                                        $errMsg = "update ownershipTable table : success.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $query = "UPDATE " . $bidTableName . " SET bid_status = '3' WHERE bid_transaction_id = '" . $editBidtxID. "' AND bid_status = '0';";

                                        $result = runQuery($query, $kwdDbConn);
                                        if (noError($result)) {
                                            ///////////////////////////////////////////////////////////
                                            $errMsg = "Update bid status in bidTable : success.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $mysql_affected_rows = mysqli_affected_rows($kwdDbConn);
                                            if ($mysql_affected_rows > 0) {

                                                    $errMsg = "Editing the bid keyword : success.";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $logCred["mysqlAffectedRows"] = $mysql_affected_rows;

                                                    $deductBlockedForBids = creditUserEarning($user_id, $bidAmtGross, 'blockedbids', 'deduct');
                                                    $response = json_encode($deductBlockedForBids);
                                                    if (noError($result)) {

                                                        $errMsg = "Update wallet for deductBlockedForBids : success.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        $logCred["deductBlockedForBids"] = $bidAmtGross;

                                                        //remove active bids from users json
                                                        $removeBidDetailsByBuyerId = removeBidDetailsByBuyerId($kwdDbConn, $email, $editBidtxID, $keyword);
                                                        if (noError($removeBidDetailsByBuyerId)) {
                                                            /*$xml .= "<step9.2 title='edit Bid price' >9.2.update bid details of patticular user:'success'";
                                                            $xml .= "</step9.2>\n";*/
                                                            $errMsg = "Remove active bid details from user json : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                            $logCred["removeActiveBids"] = $editBidtxID;

                                                        } else {
                                                            /*$xml .= "<step9.2 title='edit Bid price' >9.2.update bid details of patticular user:'fail'";
                                                            $xml .= "</step9.2>\n";*/
                                                            $errMsg = "Remove active bid details from user json : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }


                                                        //deduct from active bids of user and add to cancelled bids
                                                        $deductActiveBidsCountOfUser = deductActiveBidsCountOfUser($email, $kwdDbConn);
                                                        if (noError($deductActiveBidsCountOfUser)) {
                                                            /*$xml .= "<step8 title='cancel Bid price' >8.update deductActiveBidsCountOfUser table 'success'";
                                                            $xml .= "</step8>\n";*/
                                                            $errMsg = "Update deductActiveBidsCountOfUser table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            /*$xml .= "<step8 title='cancel Bid price' >8.update deductActiveBidsCountOfUser table 'fail'";
                                                            $xml .= "</step8>\n";*/
                                                            $errMsg = "Update deductActiveBidsCountOfUser table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $addCancelledBidsCountOfUser = addCancelledBidsCountOfUser($email, $kwdDbConn);
                                                        if (noError($addCancelledBidsCountOfUser)) {
                                                            /*$xml .= "<step9 title='cancel Bid price' >9.update addCancelledBidsCountOfUser table 'success'";
                                                            $xml .= "</step9>\n";*/
                                                            $errMsg = "Update addCancelledBidsCountOfUser table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            /*$xml .= "<step9 title='cancel Bid price' >9.update addCancelledBidsCountOfUser table 'fail'";
                                                            $xml .= "</step9>\n";*/
                                                            $errMsg = "Update addCancelledBidsCountOfUser table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        //code for index page box
                                                        $deleteFromHighestBid = deleteFromHighestBid($kwdDbConn, $editBidtxID);
                                                        if (noError($deleteFromHighestBid)) {
                                                            $errMsg = "Update deleteFromHighestBid table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            $errMsg = "Update deleteFromHighestBid table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $deleteFromMostBids = deleteFromMostBids($kwdDbConn, $keyword);
                                                        if (noError($deleteFromMostBids)) {
                                                            $errMsg = "Update deleteFromMostBid table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            $errMsg = "Update deleteFromMostBid table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $deleteFromLatestBid = deleteFromLatestBid($kwdDbConn, $editBidtxID);
                                                        if (noError($deleteFromLatestBid)) {
                                                            $errMsg = "Update deleteFromLatestBid table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            $errMsg = "Update deleteFromLatestBid table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $rectifyHighestBidTable = rectifyHighestBidTable($kwdDbConn, $keyword);
                                                        if (noError($rectifyHighestBidTable)) {
                                                            $errMsg = "Update rectifyHighestBidTable table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        } else {
                                                            $errMsg = "Update rectifyHighestBidTable table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $deleteFromLatestBidAll = deleteFromLatestBidAll($kwdDbConn, $editBidtxID);
                                                        if (noError($deleteFromLatestBidAll)) {
                                                            $errMsg = "Deleted from  latestBidAll table : success.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                            $logCred["deleteFromLatestBid"] = $editBidtxID;
                                                        } else {
                                                            $errMsg = "Update latestBidAll table : failed.";
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $trans = commitTransaction($kwdDbConn);
                                                        if (noError($trans)) {
                                                            $errMsg = "Transaction committed : success.";
                                                            $errCode = -1;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        } else {
                                                            $errMsg = "Transaction committed : failed.";
                                                            $errCode = 2;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        mysqli_autocommit($kwdDbConn, TRUE);

                                                        //send mail for cancel Bid-buyer
                                                        $to      = $email;
                                                        $subject = "You have deleted bid placed on #$keyword";
                                                        $message =

                                                            '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                <tr> 
                                                                    <td style="padding: 10px 20px;">
                                                                    You have successfully cancelled your buy order for #'.$keyword.'. Click <a href="'.$rootUrl.'views/prelogin/index.php">here </a>to login to Keywo and set a new bid on the keyword<br><br>Regards,<br>Team Keywo
                                                                     </td>
                                                                </tr>';

                                                        $notification_message = 'You have successfully cancelled your buy order for #' . $keyword . '.';
                                                        $method_name = "GET";
                                                        $getNotification = notifyoptions($user_id,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                        $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["2"]["permissions"]["_id"];

                                                        $path    = "";
                                                        $category = "bid";
                                                        $sendNotifEmail = sendNotificationBuyPrefrence($to,$subject,$message,$deleteBidderFirstName,$deleteBidderLastName,$user_id,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                                                        if(noError($sendNotifEmail)){
                                                            $errMsg = "Send notification and email to ".$email." : failed.";
                                                            $errCode = -1;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


                                                            // get bid seller user info
                                                            $kwdOwnerReqField = $userRequiredFields.",_id";
                                                            $kwdOwnerUserInfo =  getUserInfo($bidKeywordOwnerId, $walletURLIP . 'api/v3/', $kwdOwnerReqField);

                                                            if(noError($kwdOwnerUserInfo)){

                                                                $kwdOwnerUserInfo = $kwdOwnerUserInfo["errMsg"];
                                                                $kwdOwnerUserId = $kwdOwnerUserInfo["_id"];
                                                                $kwdOwnerFirstName = $kwdOwnerUserInfo["first_name"];
                                                                $kwdOwnerLastName = $kwdOwnerUserInfo["last_name"];

                                                                $errMsg = "Get user info of ".$bidKeywordOwnerId." : success.";
                                                                $errCode = -1;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                //send mail for cancel Bid-seller
                                                                $to      = $bidKeywordOwnerId;
                                                                $subject = "Bid cancelled for $keyword";
                                                                $message =

                                                                    '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$kwdOwnerFirstName.',</td>                                                    
                                                                <tr> 
                                                                    <td style="padding: 10px 20px;">
                                                                    A bid placed on #'.$keyword.'. has been cancelled
                                                                     </td>
                                                                </tr>';

                                                                $notification_message = 'A bid placed by '.$email.' for #' . $keyword . ' has been cancelled.';


                                                                $method_name = "GET";
                                                                $getNotification = notifyoptions($kwdOwnerUserId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                                                $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["bid_opt_container"]["2"]["permissions"]["_id"];

                                                                $path    = "";
                                                                $category = "bid";
                                                                $sendNotifEmailToKwdOwner = sendNotificationBuyPrefrence($to,$subject,$message,$kwdOwnerFirstName,$kwdOwnerLastName,$kwdOwnerUserId,$smsText,$mobileNumber,$notification_message,$permissionCode,$category,$linkStatus);

                                                                if(noError($sendNotifEmailToKwdOwner)){
                                                                    $errMsg = "You have successfully deleted the bid on #".$keyword." keyword.";
                                                                    $errCode = -1;
                                                                    $stepCounter++;
                                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                    $_SESSION["get_error_code"] = -1;
                                                                    $_SESSION["get_error_message"] = $errMsg;

                                                                    /******************** for logs user analytics ****************/
                                                                    $responseArr["errCode"]="-1";
                                                                    $responseArr["errMsg"]=$errMsg;
                                                                    $xml_data['response']["data"] = "";
                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                    /******************** for logs user analytics ****************/

                                                                }else{
                                                                    $errMsg = "There was an error while deleting the bid on ".$keyword." keyword.";
                                                                    $errCode = 2;
                                                                    $stepCounter++;
                                                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                    $_SESSION["get_error_code"] = 2;
                                                                    $_SESSION["get_error_message"] = $errMsg;

                                                                    /******************** for logs user analytics ****************/
                                                                    $responseArr["errCode"]=$errCode;
                                                                    $responseArr["errMsg"]=$errMsg;
                                                                    $xml_data['response']["data"] = "";
                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                    /******************** for logs user analytics ****************/
                                                                }
                                                            }else{
                                                                $errMsg = "Get user info of ".$bidKeywordOwnerId." : failed.";
                                                                $errCode = 2;
                                                                $stepCounter++;
                                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                                $_SESSION["get_error_code"] = 2;
                                                                $_SESSION["get_error_message"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]=$errCode;
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/
                                                            }

                                                        }else{
                                                            $errMsg = "Send notification and email to ".$email." : failed.";
                                                            $errCode = 2;
                                                            $stepCounter++;
                                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        }

                                                        $retArray["errCode"] = $errCode;
                                                        $retArray["errMsg"] = $errMsg;

                                                        $_SESSION["get_error_code"] = 2;
                                                        $_SESSION["get_error_message"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=2;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {

                                                        $errMsg = "Update wallet for deductBlockedForBids : failed.";
                                                        $stepCounter++;
                                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                        /* Error in deductBlockedForBids */
                                                        //rollback transction
                                                        rollbackTransaction($kwdDbConn);
                                                        $retArray["errCode"] = 2;
                                                        $retArray["errMsg"] = $errMsg;

                                                        $_SESSION["get_error_code"] = 2;
                                                        $_SESSION["get_error_message"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=2;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/
                                                    }

                                            } else {

                                                $errMsg = "Editing the bid keyword : failed.";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}"]["data"] ="";
                                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                /* Error in edit bid price details*/
                                                $retArray["errCode"] = 2;
                                                $retArray["errMsg"] = $errMsg;

                                                $_SESSION["get_error_code"] = 2;
                                                $_SESSION["get_error_message"] = $errMsg;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]=2;
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/
                                            }
                                        } else {

                                            $errMsg = "Update bid status in bidTable : failed.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] ="";
                                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            /*error in update set bid status*/
                                            $retArray["errCode"] = 2;
                                            $retArray["errMsg"] = $errMsg;

                                            $_SESSION["get_error_code"] = 2;
                                            $_SESSION["get_error_message"] = $errMsg;

                                            /******************** for logs user analytics ****************/
                                            $responseArr["errCode"]=2;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;
                                            /******************** for logs user analytics ****************/
                                        }

                                    } else {

                                        $errMsg = "update ownershipTable table : failed.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        /*error update highet bit table */
                                        $retArray["errCode"] = 2;
                                        $retArray["errMsg"] = $errMsg;

                                        $_SESSION["get_error_code"] = 2;
                                        $_SESSION["get_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=2;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }

                            } else {

                                $errMsg = "Start transaction : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                /* error start transaction */
                                $retArray["errCode"] = 2;
                                $retArray["errMsg"] = $errMsg;

                                $_SESSION["get_error_code"] = 2;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/
                            }
                        } else {

                            $errMsg = "Bidder is not keyword owner id : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            /* keyword owner id not match */
                            $retArray["errCode"] = 2;
                            $retArray["errMsg"] = $errMsg;

                            $_SESSION["get_error_code"] = 2;
                            $_SESSION["get_error_message"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=2;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }


                    }else{

                        $errMsg = "Get keyword ownership details : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /*error getting keyword ownership details*/
                        $retArray["errCode"] = 2;
                        $retArray["errMsg"] = $errMsg;

                        $_SESSION["get_error_code"] = 2;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=2;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }
                }else{

                    $errMsg = "Get admin setting from keyword admin : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /*error getting keyword admin settings */
                    $retArray["errCode"] = 2;
                    $retArray["errMsg"] = $errMsg;

                    $_SESSION["get_error_code"] = 2;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

                $errMsg = "Error calculating user available balance : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                /*error calculation user available bal*/
                $retArray["errCode"] = 2;
                $retArray["errMsg"] = $errMsg;

                $_SESSION["get_error_code"] = 2;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }
        }else{

            $errMsg = "Get bidder user info : success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            /*error getting user details*/
            $retArray["errCode"] = 2;
            $retArray["errMsg"] = $errMsg;

            $_SESSION["get_error_code"] = 2;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }
    }else{

        $errMsg = "Bid amount greater than minimum trading amount : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        /*error amount is less than trading amount*/
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = $errMsg;

        $_SESSION["get_error_code"] = 2;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $retArray["credential"]  = $logCred;
    $retArray["xml_data"]  = $xml_data;

    return $retArray;

}

?>