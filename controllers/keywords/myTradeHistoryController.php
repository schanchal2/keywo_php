<?php

session_start();
//start config
require_once ('../../config/config.php');
require_once ('../../config/db_config.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../models/keywords/keywordCdpModel.php');
require_once ('../../models/keywords/userDashboardModel.php');
require_once ('../../models/keywords/userCartModel.php');

  $email = $_SESSION["email"];
  $connKeywords = createDBConnection("dbkeywords");
  noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

  $totalKwdSold = 0;
  $totalKwdPurchase = 0;
  $totalKwdEarning = 0;
  $totalProfitLoss = 0;

?>
        <!-- content starts here -->
        <div class="pagination-content text-center">
            <!-- pagination head starts here -->
            <div class="pagination-head half innerAll padding-key-market-data">
                <div class="row">
                    <div class="col-xs-2 text-white innerMT innerMR pull-left text-left">keyword</div>
                    <div class="col-xs-2 text-white text-center inner-1x innerMR ">Sold
                        <br>(Price)</div>
                    <div class="col-xs-2 text-white text-center inner-1x innerMR ">Purchase
                        <br>(Price)</div>
                    <div class="col-xs-2 text-white text-center innerMLR innerMT">Earning</div>
                    <div class="col-xs-2 text-white pull-right text-right innerMT padding-left-none">Profit / Loss</div>
                </div>
            </div>
                                    <!-- pagination head ends here -->

                                    <?php
                                    $pageUrl = "views/keywords/analytics/keyword_analytics.php";
                                    // get user json file from user trading history
                                    $tradingJson = $rootUrl."json_directory/keywords/trading_history/";
                                    $tradingJson = $tradingJson.$email.".json";

                                    $tradingData = file_get_contents($tradingJson);
                                    $data      = json_decode($tradingData, TRUE);
                                    $data = array_reverse($data);
                                    $dataCount = count($data);
                                    $limit     = cleanQueryParameter($connKeywords, cleanXss($_POST["limit"]));

                                    if (isset($_POST["page"])) {
                                    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
                                    if (!is_numeric($page_number)) {
                                        die('Invalid page number!');
                                    } //incase of invalid page number
                                } else {
                                    $page_number = 1; //if there's no page number, set it to 1
                                }
                                if ($page_number == 0) {
                                    $page_number = 1;
                                }
                                $item_per_page = $limit; 
                                $get_total_rows = $dataCount;
                                $total_pages = ceil($get_total_rows / $item_per_page);
                                //position of records
                                $page_position = (string)(($page_number - 1) * $item_per_page); 
                                $lastpage = ceil($total_pages);

                                if ($dataCount == 0) {
                                    $page_number = $page_number - 1; //if there's no page number, set it to 1
                                    $item_per_page = $limit;
                                    $get_total_rows = $dataCount;
                                    $total_pages = ceil($get_total_rows / $item_per_page);

                                //position of records
                                    $page_position = (($page_number - 1) * $item_per_page);
                                    $lastpage = ceil($total_pages);

                                }


                                    ?>



                                    <!-- pagination body starts here -->
                                    <ul class="border-all">

                                        <?php
                                            if(count($data) > 0) {
                                               // printArr($data);

                                                $keywords    = $data;
                                                $count       = count($keywords);    
                                                $limits      = intval($limit); 
                                                $pagePostion = intval($page_position);
                                                $limits      = intval($pagePostion + ($limits -1));

                                                for($i=$pagePostion;$i<$count && $i<=$limits; $i++){

                                                // foreach ($data as $key => $value) {

                                                $keyword = $keywords[$i]["keyword"];

                                                $sold = $keywords[$i]["kwd_last_traded_price"];
                                                $purchase = $keywords[$i]["kwd_purchase_price"];
                                                $earning = $keywords[$i]["user_kwd_ownership_earnings"];
                                                $profitLoss = ($sold + $earning) - $purchase;

                                                if(getType($earning) == "string"){
                                                    $earning = 0;
                                                }

                                                if ($profitLoss < 0) {
                                                    $cls = "error-color";
                                                    $profitLoss = abs($profitLoss);
                                                } else if ($profitLoss > 0) {
                                                    $cls = "text-color-App-Success";
                                                }

                                                ?>
                                                <!-- li starts here -->
                                                <li class="half innerAll padding-bottom-none" id="trading_history">
                                                    <!-- row starts here   -->
                                                    <div class="row padding-none half innerAll">
                                                        <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                            <span class="txt-blue ellipses text-left"><a href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo "#" . $keyword; ?>"><?php echo $keyword; ?></a></span>
                                                        </div>
                                                        <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"  origPrice="<?php echo number_format("{$sold}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$sold}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$sold}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                        </div>
                                                        <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"  origPrice="<?php echo number_format("{$purchase}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$purchase}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$purchase}", 4); ?><?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                         </div>
                                                        <div class="col-xs-2 text-white text-center innerMLR">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"  origPrice="<?php echo number_format("{$earning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$earning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$earning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                        </div>
                                                        <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                            <label class="text-black text-right ellipses margin-bottom-none margin-top-none">
                                                                <a href="#" class="<?php echo $cls; ?>" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo number_format("{$profitLoss}", 4,'.',''); echo "{$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$profitLoss}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$profitLoss}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </li>
                                                <!-- li ends here -->
                                                <?php
                                                }
                                            }else{
                                              //  echo "No trading history";
                                                ?>
                                                <!-- row starts here   -->
                                                <div class="bg-white search-query innerMT">
                                                    <div class="half innerAll">
                                                        <!-- row starts here   -->
                                                        <div class="row half innerAll padding-left-none padding-right-none" style="text-align: center">
                                                            No Trading History
                                                        </div>
                                                        <!-- row ends here -->
                                                    </div>
                                                </div>
                                                <!-- row ends here -->

                                                <?php
                                            }
                                        ?>
                                        <!-- li ends here -->
                                    </ul>
                                    <!-- pagination body ends here -->
                                </div>
                                <!-- content ends here -->
    <div class="row">
      <div class="col-md-5 padding-left-none padding-right-none"></div>
        <div class="col-md-7 padding-left-none half innerR innerT">
          <div class="pagination-cont pull-right gotovalues">


    <?php

    getPaginationData($lastpage, $page_number, $limit);
    function getPaginationData($lastpage, $pageno, $limit)
    {
        //printArr(func_get_args());

        
echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
echo '<ul class="pagination">';


if ($pageno > 1) {

    $pagenum = 1;
    print('<li class="status"><a href="#"onclick=getNewTradingHistoryData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
}

if ($pageno > 1) {
    $pagenumber = $pageno - 1;
    print('<li class="status"><a href="#" onclick=getNewTradingHistoryData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
}

if ($pageno == 1) {
    $startLoop = 1;
    $endLoop = ($lastpage < 5) ? $lastpage : 5;
} else if ($pageno == $lastpage) {
    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
    $endLoop = $lastpage;
} else {
    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
}

for ($i = $startLoop; $i <= $endLoop; $i++) {
    if ($i == $pageno) {
        print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
    } else {
        $pagenumber = $i;
        print('<li class="status"><a href="#" onclick=getNewTradingHistoryData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
    }
}
if ($pageno < $lastpage) {
    $pagenumber = $pageno + 1;
    print('<li class="status"><a href="#" onclick=getNewTradingHistoryData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

}

if ($pageno != $lastpage) {
    print('<li class="status"><a href="#" onclick=getNewTradingHistoryData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
}


echo '</ul>';
echo '</div>';
}


?>

    </div>
  </div>
</div>

<script>
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    })
</script>