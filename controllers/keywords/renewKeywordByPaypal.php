<?php

session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/transactionHelper.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/walletHelper.php');
require_once('../../models/header/headerModel.php');
require_once('../../models/keywords/userCartModel.php');
require_once('../../models/keywords/keywordPurchaseModel.php');
require_once('../../models/keywords/acceptBidModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

$email = $_SESSION["email"];
$currPref = strtolower($_SESSION["CurrPreference"]);

$keyword = $_POST["keywordBasket"];
//$keyword = "#".$keyword;
$totalPrice = $_POST["totalPrice"];
$paymentMode = $_POST["payment_mode"];

if(strpos($keyword, ',') !== false ){
    $keyword = trim($keyword);

    $keywordArr = explode(',', $keyword);
}else{
    $keywordArr = array(0 => $keyword);
}

// create an object
$xmlProcessor = new xmlProcessor();

$xmlfilename = "keyword_renewal_by_paypal.xml";
/* XML title attribute */
$xml_atrr = array();

$xml_data['request']["data"]      = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xml_data['step']["data"] = 'Keyword Renewal';
$xmlArray    = initializeXMLLog(trim(urldecode($email)));

$kwdPurchaseLog = $logPath["keywordPurchase"];


// initialize log step number
$i = 0;
$j = 0;

// get client ip
$userIp = getClientIP();

// create database connection
$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Success: Create keyword database connection';

    // get currency exchange rate
    $getExchangeRate = getExchangeRates($currPref);
    if(noError($getExchangeRate)){
        $getExchangeRate = $getExchangeRate["currRate"];

        $getExchangeRate = json_encode($getExchangeRate);

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Success: Getting current exchange rate 1] '.$getExchangeRate;

        $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
        if(noError($getAdminSetting)){
            $getAdminSetting = $getAdminSetting["data"];

            // get renewal amount set by admin in admin_setting table.
            $kwdRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Success: Getting admin setting details to fetch renewal amount 1] Renewal Amount: '.$kwdRenewalAmt;


            foreach($keywordArr as $key => $keyword){

                $keyword = trim($keyword);
                // remove # from word
                $keyword=  substr_replace($keyword, '', 0, 1);

                // calculate user availablw balance
                $renewUsrBalanceField = $userRequiredFields . "_id,last_name,deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                $renewUsrBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $renewUsrBalanceField);
                if(noError($renewUsrBalanceDetails)){

                    $userId = $renewUsrBalanceDetails["errMsg"]["_id"];
                    $firstName = $renewUsrBalanceDetails["errMsg"]["first_name"];
                    $lastName = $renewUsrBalanceDetails["errMsg"]["last_name"];

                    $i = $i + 1;
                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting user info of '.$email.' user.';


                    $getUserBalance = calculateUserBalance($renewUsrBalanceDetails);

                    if(noError($getUserBalance)){

                        $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                        $userBalance = number_format($userBalance, 8);

                        $j = $j + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting '.$email.' available balance 1] Available balance: '.$userBalance;


                        $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                        if(noError($getKwdOwnershipDetails)){

                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting #'.$keyword.' keyword ownership details';

                            $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];

                            $ownerId = $getKwdOwnershipDetails["buyer_id"];
                            $expiryTimestamp = $getKwdOwnershipDetails["ownership_expiry_time"];
                            $purchaseTimestamp = $getKwdOwnershipDetails["purchase_timestamp"];

                            $expiredOn = strtotime($expiryTimestamp);
                            $expiredOn = date("j F, Y", $expiredOn);

                            $purchasedOn = strtotime($purchaseTimestamp);
                            $purchasedOn = date("j F, Y", $purchasedOn);

                            if(isset($ownerId) && !empty($ownerId)){
                                if($email == $ownerId){

                                    $j = $j + 1;
                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Validating keyword owner Id';


                                    if($userBalance >= $kwdRenewalAmt){

                                        $j = $j + 1;
                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: User available balance is greater then the renewal amount';


                                        // step 1. add renewal amount in renewal field of session user account.
                                        $creditRenewAmountToKwdOwner = creditUserEarning($userId, $kwdRenewalAmt, "renewalfees", "add");
                                        if(noError($creditRenewAmountToKwdOwner)){

                                            $j = $j + 1;
                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Credit renewal amount to '.$email.' account and increase the expenditure amount.';

                                            // 2. add renewal amount in renewal field of communitypool@keywo.com user.
                                            $creditRenewAmtToCommunityPool = creditUserEarning($communityPoolUserId, $kwdRenewalAmt, "renewalfees", "add");

                                            if(noError($creditRenewAmtToCommunityPool)){

                                                $j = $j + 1;
                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Credit renewal amount to '.$communityPoolUser.' account to increase the income.';

                                                // 3. insert transaction for renewal earning where sender = $email and receiver = $communitypool@keywo.com with type="renewal_fees"

                                                // parameters required to insert in transaction
                                                $recipientEmail     = $communityPoolUser;
                                                $recipientUserId    = $communityPoolUserId;
                                                $senderEmail        = $email;
                                                $senderUserId       = $userId;
                                                $amount             = $kwdRenewalAmt;
                                                $type               = "renewal_fees";
                                                $paymentMode        = 'paypal';
                                                $originIp           = $userIp;
                                                $exchangeRateInJson = $getExchangeRate;

                                                $metaDetails = array(
                                                    "sender" => $email,
                                                    "receiver" => $communityPoolUser,
                                                    "renewal_amount" => $kwdRenewalAmt,
                                                    "keyword" => urlencode(utf8_encode($keyword)),
                                                    "discount" => 0,
                                                    "commision" => 0,
                                                    "desc" => "User ".$email." has renewed his/her #".$keyword." keyword for the session ".$purchasedOn." - ".$expiredOn." and credited renewal amount with ".number_format($kwdRenewalAmt, 2)." ".$keywoDefaultCurrencyName." to ".$communityPoolUser." on dated ".date("j F, Y")
                                                );

                                                $insertRenewalTransaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                if(noError($insertRenewalTransaction)){

                                                    $j = $j + 1;
                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Insert transaction for renewal amount where 1] Sender: '.$email.' 2], Receiver: '.$communityPoolUser.', 3] Renewal Amount: '.$kwdRenewalAmt.' '.$keywoDefaultCurrencyName.', 4] Keyword: '.$keyword.'.';

                                                    // 4. update expiry timestamp in keyword ownership table
                                                    $updateExpiryTimestamp = updateExpiryTimestamp($keyword, $kwdDbConn);

                                                    if(noError($updateExpiryTimestamp)){

                                                        $j = $j + 1;
                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Update keyword : #'.$keyword.' expiry timestamp in ownership table';

                                                        // 5. Send email and notification to current session user.

                                                        $to      = $email;
                                                        $subject = "Keywo: Keyword Renew Confirmation";
                                                        $message = '<p>Hi, <br/> <br/>You have successfully renewed the license for <b>#' . $keyword . '</b> keyword via Paypal.</p>';

                                                        $notification_message = 'You have successfully renewed the license for <b>#' . $keyword . '</b> keyword via Paypal.';
                                                        $method_name          = "GET";

                                                        $getNotification = notifyoptions($userId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);

                                                        if(noError($getNotification)){
                                                            $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                                                            $path    = "";
                                                            $category   = "buy";
                                                            $linkStatus = 1;

                                                            $j = $j + 1;
                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting user notification preference for sending email and notification.';

                                                            $sendRenewEmailAndNotification   = sendNotificationBuyPrefrence($to, $subject, $message, $firstName, $lastName, $userId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                            if(noError($sendRenewEmailAndNotification)){

                                                                $j = $j + 1;
                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Sent email and notification to '.$email.' user.';


                                                                $removeRenewKwdFrmPresale = removeRenewKwdFrmPresale($email, $keyword, $kwdDbConn);

                                                                if(noError($removeRenewKwdFrmPresale)){
                                                                    $errCode = -1;

                                                                    $j = $j + 1;
                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Removing renew keyword from presale user table';

                                                                }else{
                                                                    $errCode = 15;

                                                                    $j = $j + 1;
                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Removing renew keyword from presale user table';
                                                                }
                                                            }else{
                                                                $errCode = 14;

                                                                $j = $j + 1;
                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Sent email and notification to '.$email.' user.';
                                                            }
                                                        }else{
                                                            $errCode = 13;

                                                            $j = $j + 1;
                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting user notification preference for sending email and notification.';

                                                        }
                                                    }else{
                                                        $errCode = 12;

                                                        $j = $j + 1;
                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Update keyword : #'.$keyword.' expiry timestamp in ownership table';
                                                    }

                                                }else{
                                                    $errCode = 11;

                                                    $j = $j + 1;
                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Insert transaction for renewal amount where 1] Sender: '.$email.' 2], Receiver: '.$communityPoolUser.', 3] Renewal Amount: '.$kwdRenewalAmt.' '.$keywoDefaultCurrencyName.', 4] Keyword: '.$keyword.'.';
                                                }

                                            }else{
                                                $errCode = 10;

                                                $j = $j + 1;
                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Credit renewal amount to '.$communityPoolUser.' account to increase the income.';

                                            }

                                        }else{
                                            $errCode = 9;

                                            $j = $j + 1;
                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Credit renewal amount to '.$email.' account and increase the expenditure amount.';
                                        }

                                    }else{
                                        $msg = "You don't have sufficient balance to renew keyword";
                                        $errCode = 8;

                                        $j = $j + 1;
                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: User available balance is greater then the renewal amount';


                                    }
                                }else{
                                    $msg = "This is not your owned keyword. Please renew your owned keyword";;
                                    $errCode = 7;

                                    $j = $j + 1;
                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Validating keyword owner Id';

                                }
                            }

                        }else{
                            $errCode = 6;

                            $j = $j + 1;
                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting #'.$keyword.' keyword ownership details';

                        }
                    }else{
                        $errCode = 5;

                        $j = $j + 1;
                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting '.$email.' available balance 1] Available balance: '.$userBalance;
                    }
                }else{
                    $errCode = 4;

                    $j = $j + 1;
                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting user info of '.$email.' user.';
                }

                $j = 0;
            }
        }else{
            $errCode = 3;

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Failed: Getting admin setting details to fetch renewal amount 1] Renewal Amount: '.$kwdRenewalAmt;
        }

    }else{
        $errCode = 2;

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Success: Getting current exchange rate 1] '.$getExchangeRate;
    }
}else{
    $errCode = 1;

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Failed: Create database connection';
}

$xmlProcessor->writeXML($xmlfilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

if($errCode == -1){

    $errorMessage        = "You have successfully renew this keyword";
    $redirectURL = $rootUrl."views/keywords/user_dashboard/owned_keyword.php";
    $hashTab = "#keywords";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}else{
    $redirectURL = $rootUrl."views/keywords/cart/renew_checkout.php";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}

?>