<?php

session_start();

//start config
require_once ('../../config/config.php');
require_once ('../../config/db_config.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../models/keywords/keywordCdpModel.php');
require_once ('../../models/keywords/userDashboardModel.php');
require_once ('../../models/keywords/userCartModel.php');
require_once("../../helpers/sessionHelper.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

checkForSession($connKeywords);

$email      = $_SESSION["email"];
$clientSessionId = session_id();

$totalInteraction = 0;
$totalEarning = 0;

$limit      = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;

$getMyKeywordDetails = getMyKeywordDetails($connKeywords,$email);
    if(noError($getMyKeywordDetails)) {

        $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
        $myKeywords = $getMyKeywordDetails["transaction_details"];
        $myKeywordsData = json_decode($myKeywords, true);
        $myKeywordsCount = array_reverse($myKeywordsData);
        $kwdCount = count($myKeywordsCount);
    }

$get_total_rows = $kwdCount;

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page); 
$lastpage = ceil($total_pages);

$results = getMyKeywordDetails($connKeywords,$email);
if(noError($results))
{
    $errMsg = $results["errMsg"];
}
$count = count($myKeywordsCount);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = $kwdCount;
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

}

?>



<div class="pagination-content text-center">
    <!-- pagination head starts here -->
    <div class="pagination-head half innerAll padding-key-market-data">
        <div class="row">
            <div class="col-xs-2 text-white">keyword</div>
            <div class="col-xs-3 text-white text-center">Interaction</div>
            <div class="col-xs-2 text-white text-center">Earning</div>
            <div class="col-xs-2 text-white text-center">Ask</div>
            <div class="col-xs-3 text-white text-center pull-right padding-right-none">Action</div>
        </div>
    </div>
    <!-- pagination head ends here -->

    <!-- pagination body starts here -->

    <ul class="border-all" id="trading_history">

        <?php
        $pageUrl = "views/keywords/analytics/keyword_analytics.php";
            
            if($kwdCount == 0){
                ?>
                <!-- li starts here -->
                <li class="half innerAll">
                    <!-- row starts here   -->
                    <div class="row half innerAll padding-left-none padding-right-none">
                        No Keywords
                    </div>
                    <!-- row ends here -->
                </li>
                <!-- li ends here -->
                <?php exit; 
            }else{
                
                $keywords    = $myKeywordsCount;
                $count       = count($keywords);
                $limits      = intval($limit);
                $pagePostion = intval($page_position);
                $limits      = intval($pagePostion + ($limits -1));

                for($i=$pagePostion; $i<$count && $i<=$limits; $i++){
                    
                    $keyword = $keywords[$i]["keyword"];
                    $getKwdOwnershipDetails = getKeywordOwnershipDetails($connKeywords, $keyword); 
                    // printArr($getKwdOwnershipDetails);
                    if(noError($getKwdOwnershipDetails)){

                        $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                        $orderStatus = $getKwdOwnershipDetails["order_status"];
                        $askPrice = $getKwdOwnershipDetails["ask_price"];
                        $noOfActiveBids = $getKwdOwnershipDetails["no_of_active_bids"];
                        $expiredWithinDays = $getKwdOwnershipDetails["days_to_expire"];

                        // get revenue details of keyword
                        $getKwdRevenueDetails = getRevenueDetailsByKeyword($connKeywords, $keyword);
                        
                         //printArr($getKwdRevenueDetails);
                        if(noError($getKwdRevenueDetails)){
                            $getKwdRevenueDetails = $getKwdRevenueDetails["data"][0];
                            // printArr($getKwdRevenueDetails);
                            $interactionCount = $getKwdRevenueDetails["app_kwd_search_count"];
                            $ownershipEarning = $getKwdRevenueDetails["user_kwd_ownership_earnings"];

                            $interactionCount = json_decode($interactionCount, true);
                            //printArr($interactionCount["total"]);

                            $totalInteraction = $totalInteraction + $interactionCount["total"];
                            $interactionCount = $interactionCount["total"];
                            $totalEarning = $totalEarning + $ownershipEarning;

                            if(!isset($interactionCount) && empty($interactionCount)){
                                $interactionCount = 0;
                            }

                            if(!isset($askPrice) && empty($askPrice)){
                                $askPrice = 0.00;
                            }

                            if(!isset($ownershipEarning) && empty($ownershipEarning)){
                                $ownershipEarning = 0.00;
                            }
                            ?>
                            <!-- li starts here -->
                            <li class="half innerAll">
                                <!-- row starts here   -->
                                <div class="row half innerAll padding-left-none padding-right-none">

                                    <div class="col-md-12 padding-left-none padding-right-none" style="height:25px;">
                                        <div class="row">
                                            <div class="col-md-9 half innerL padding-right-none">
                                                <div class="col-md-3 text-left"><span class="txt-blue ellipses text-left"><a href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $keyword; ?>" target="_blank"><?php echo $keyword; ?></a></span></div>
                                                <div class="col-md-3 text-center"><label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" data-toggle="tooltip" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo $interactionCount;  ?>"><?php echo $interactionCount;  ?></a></label></div>
                                                <div class="col-md-3 text-center">
                                                        <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                            <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                               origPrice="<?php echo number_format("{$ownershipEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$ownershipEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$ownershipEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                            </label>
                                                </div>
                                                <div class="col-md-3 text-center">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                    <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                       origPrice="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$askPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$askPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="col-xs-12 text-right">

                                                    <?php  if(isset($askPrice) && !empty($askPrice)){ ?>
                                                        <button class="btn-trading-wid-auto"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                                                    <?php   }else{  ?>
                                                        <button class="btn-trading-wid-auto" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                                    <?php  }  ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    if(isset($expiredWithinDays) && !empty($expiredWithinDays) && $expiredWithinDays <= 30 && $expiredWithinDays > 0){
                                        $message = '<p class="float-left-k margin-bottom-none">Your <span class="txt-blue ellipses width-keyword-owned"><a href="#" class="display-in-keywrd-owned" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="'.$keyword .' "> #'. $keyword .' </a></span> keyword will expires within <span class="text-red">'. $expiredWithinDays .'</span> days.</div></p>';

                                    }else if(isset($expiredWithinDays) && !empty($expiredWithinDays) && $expiredWithinDays < 0  && $expiredWithinDays >= -15){
                                        $message = '<p class="float-left-k margin-bottom-none">Your <span class="txt-blue ellipses width-keyword-owned"><a href="#" class="display-in-keywrd-owned" title="" data-toggle="tooltip" data-placement="bottom" data-original-title=" '.$keyword .' " > #'. $keyword.' </a></span> keyword is under grace peroid of <span class="text-red"> '. abs($expiredWithinDays) .'</span> days.</div></p>';
                                    }

                                    if((isset($expiredWithinDays) && !empty($expiredWithinDays) && $expiredWithinDays <= 30 && $expiredWithinDays > 0) || (isset($expiredWithinDays) && !empty($expiredWithinDays) && $expiredWithinDays < 0  && $expiredWithinDays >= -15)){
                                    ?>
                                    <div class="col-md-12 half innerL padding-right-none">
                                        <div class="row">
                                            <div class="col-md-9 padding-left-none padding-right-none innerMT">
                                                <div class="col-md-12 text-left">
                                                    <?php echo $message; ?>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="col-xs-12 text-right innerMT">
                                                        <input value="  Renew " type="button" class="btn-trading-wid-auto-green" data-toggle="modal" data-target="#" onclick="keywordRenewNow('<?php echo $keyword; ?>','<?php echo $clientSessionId; ?>' , '<?php echo $rootUrl; ?>')">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        }

                                        ?>

                                    </div>
                                    <!-- row ends here -->

                            </li>
                            <!-- li ends here -->
                            <?php

                        }else{
                            print('Error: Fetching #'.$keyword.' revenue details');
                        }
                    }else{
                        print('Error: Fetching #'.$keyword.' details');
                    }

                }

            }
    
        ?>
    </ul>
    </div>

    <div class="row">
      <div class="col-md-5 padding-left-none padding-right-none"></div>
        <div class="col-md-7 padding-left-none half innerR innerT">
          <div class="pagination-cont pull-right gotovalues">


    <?php

    getPaginationData($lastpage, $page_number, $limit);
    function getPaginationData($lastpage, $pageno, $limit)
    {
        //printArr(func_get_args());

        
echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
echo '<ul class="pagination">';


if ($pageno > 1) {

    $pagenum = 1;
    print('<li class="status"><a href="#"onclick=getNewEarningData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
}

if ($pageno > 1) {
    $pagenumber = $pageno - 1;
    print('<li class="status"><a href="#" onclick=getNewEarningData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
}

if ($pageno == 1) {
    $startLoop = 1;
    $endLoop = ($lastpage < 5) ? $lastpage : 5;
} else if ($pageno == $lastpage) {
    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
    $endLoop = $lastpage;
} else {
    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
}

for ($i = $startLoop; $i <= $endLoop; $i++) {
    if ($i == $pageno) {
        print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
    } else {
        $pagenumber = $i;
        print('<li class="status"><a href="#" onclick=getNewEarningData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
    }
}
if ($pageno < $lastpage) {
    $pagenumber = $pageno + 1;
    print('<li class="status"><a href="#" onclick=getNewEarningData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

}

if ($pageno != $lastpage) {
    print('<li class="status"><a href="#" onclick=getNewEarningData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
}


echo '</ul>';
echo '</div>';
}


?>

    </div>
  </div>
</div>

<script>
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>

