<?php

ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

// Include required helpers files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/transactionHelper.php');
require_once('../../helpers/walletHelper.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../models/keywords/keywordBidModel.php');
require_once('../../models/keywords/acceptBidModel.php');
require_once('../../models/keywords/keywordCdpModel.php');
require_once('../../models/keywords/userCartModel.php');
require_once('../../models/keywords/keywordPurchaseModel.php');
require_once('../smtp/Send_Mail.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once("../../models/analytics/keyword_stat_analytics.php");

error_reporting(0);

$email      = $_SESSION["email"];
$username   = $_SESSION["first_name"];
$keyword    = $_POST["acceptBidKeyword"];
$returnArr  = array();
$credential = array();

$logStorePath = $logPath["keywordBid"];
/*$appStatus =  $_POST["app_status"];*/
$redirectUrl = "{$rootUrl}views/keywords/";

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

//for xml writing essential
$xmlProcessor = new xmlProcessor();


$xmlfilename = "accept_bid.xml";
$xmlArray    = initializeXMLLog(trim(urldecode($email)));

$xml_data['request']["data"]      = '';
$xml_data['request']["attribute"] = $xmlArray["request"];


// initialize log step number variable to increase dynamically.
$i = 0;
$m = 0;

if(!empty($_SESSION)){

    if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){

        $userIp = getClientIP();

        if(!isset($_SESSION["verify_status"])){

            $xml_data['step']["data"] = 'Accept Bid';

            $unique_id = md5(uniqid(rand(), true));
            $xmlArray['activity']['id'] = $unique_id;
            $_SESSION['activity_id'] = $unique_id;

            $_SESSION['redirect_url'] = $returnURl;
            $msg = "Start {$xmlHeader} process.";
            $xml_data['step'.$m]["data"] = $m.". {$msg}";

            $checkEmailExistence = checkEmailExistance($email);
            if(noError($checkEmailExistence)){
                $errMsg = "Success : {$email} is a valid user";
                $xml_data['step'.++$m]["data"] = $m.". Success :  {$email} is a valid user";
                $returnArr["errCode"] = -2;
                $returnArr["errMsg"] = $errMsg;

                //redirecting to 2FA page
                $msg = "Success : Redirecting to 2FA page";
                $xml_data['step'.++$m]["data"] = $m.". {$msg}";
                $returnArr["errCode"] = -2;
                $_SESSION['form_data'] = json_encode($_POST);
                $_SESSION['xml_step'] = $m;


                $_SESSION['xml_file'] = $logStorePath . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;

            }else{
                $errMsg = "Not a valid user";
                $xml_data['step'.++$m]["data"] = $m.". Failed : Not a valid user";
                $returnArr["errCode"] = 1;
                $returnArr["errMsg"] = $errMsg;
            }

            // create or update xml log Files
            $xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

            echo json_encode($returnArr); die;


        }else if($returnURl == $_SESSION['redirect_url']){

            $i = $_SESSION["xml_step"];
            $security_type = $_SESSION['security_type'];
            $form_data = json_decode($_SESSION['form_data'], true);
            $keyword = urldecode($form_data["acceptBidKeyword"]);
            $activity_id = $_SESSION['activity_id'];
            $xml_file = $_SESSION['xml_file'];

            unset($_SESSION['security_type']);
            unset($_SESSION['xml_step']);
            unset($_SESSION['form_data']);
            unset($_SESSION['activity_id']);
            unset($_SESSION['redirect_url']);
            unset($_SESSION['xml_file']);
            unset($_SESSION['verify_status']);

            // start accept bid process

            // create search database connection
            $searchDbConn = createDBConnection('dbsearch');

            if (noError($searchDbConn)) {
                $searchDbConn = $searchDbConn["connection"];

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Success : Search database connection';

                $errMsg    = 'Success : Search database connection';
               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $errMsg;


                // get current exchange rate
                $currentExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
                if (noError($currentExchangeRate)) {
                    $currentExchangeRate = $currentExchangeRate["exchange_rate"];
                    // convert array into json encode
                    $currentExchangeRate = json_encode($currentExchangeRate);

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Success : Current exchange rate ' . $currentExchangeRate;

                    $errMsg    = 'Success : Current exchange rate ' . $currentExchangeRate;
                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $errMsg;

                } else {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Failed : Current exchange rate ' . $currentExchangeRate;

                    $errMsg    = ' Failed : Current exchange rate ' . $currentExchangeRate;
                   // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                    $returnArr["errCode"] = 99;
                    $returnArr["errMsg"] = $errMsg;

                    //print('Error: fetching currency exchange rate');
                }
            } else {

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Failed : Search database connection';

                $errMsg    = 'Failed : Search database connection';
               // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                $returnArr["errCode"] = 98;
                $returnArr["errMsg"] = $errMsg;
            }

            // close search database connection
            mysqli_close($searchDbConn);

            //create database connection
            $conn = createDBConnection("dbkeywords");

            if (noError($conn)) {
                $conn = $conn["connection"];

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Success : keywords db connection ';

                $errMsg    = 'Success: keywords db connection ';
               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $errMsg;


            } else {

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Failed : keywords db connection ';

                $errMsg    = 'Failed : keywords db connection ';
                //$returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

                $returnArr["errCode"] = 3;
                $returnArr["errMsg"] = $errMsg;

                // print("Database Error");
            }


            //getting form values
            $result = checkForKeywordOwnershipLock($keyword, $conn);

            if (noError($result)) {

                $KeywordDetails = getKeywordOwnershipDetails($conn, $keyword);

                $KeywordOwnerId     = $KeywordDetails["errMsg"]["buyer_id"];
                $highestBidId       = $KeywordDetails["errMsg"]["highest_bid_id"];
                $highestBidAmt      = $KeywordDetails["errMsg"]["highest_bid_amount"];
                $highestBidAmt      = number_format((float) $highestBidAmt, 8);
                $activBids          = $KeywordDetails["errMsg"]["active_bids"];
                $previousOwners     = $KeywordDetails["errMsg"]["previous_owner"];
                $kwdLastTradedPrice = $KeywordDetails["errMsg"]["kwd_price"];
                $kwdLastTradedPrice = number_format((float) $kwdLastTradedPrice, 8);
                $kwdLastTradedTime  = $KeywordDetails["errMsg"]["purchase_timestamp"];

                $credential["current_session_user"]     = $email;
                $credential["buyer_id"]                 = $KeywordOwnerId;
                $credential["keyword"]                  = $keyword;
                $credential["highest_bid_id"]           = $highestBidId;
                $credential["highest_bid_amount"]       = $highestBidAmt;
                $credential["active_bids"]              = $activBids;
                $credential["previous_owner"]           = $previousOwners;
                $credential["keyword_last_trade_price"] = $kwdLastTradedTime;
                $credential["keyword_last_traded_time"] = $kwdLastTradedTime;


                $previousOwners = json_decode($previousOwners, TRUE);
                if($email == $previousOwners[0]["buyer_id"]){

                    $errMsg    = 'You have already accepted bid for this keyword.';
                    //$returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                    $returnArr["errCode"] = 97;
                    $returnArr["errMsg"] = $errMsg;

                    // create or update xml log Files
                    $xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

                    echo json_encode($returnArr);
                    exit;
                }

                //get bid detais
                $bidDetails = explode("~~", $highestBidId);

                $bidTableName = $bidDetails[0];
                $bidderEmail  = $bidDetails[1];
                $bidAmt       = number_format((float) $bidDetails[3], 8);
                $bidAmtComm   = number_format((float) $bidDetails[4], 8);
                $kwdRenewalAmt = number_format((float)$bidDetails[8], 8);

                //get active bids json to reject
                $activBids = json_decode($activBids, TRUE);

                //remove last bidtxId from active bids array
                $activBids = array_diff($activBids, array(
                    $highestBidId
                ));

                $activBids = array_values($activBids);

                //new active bids json
                $newActivBidsJson = json_encode($activBids, JSON_UNESCAPED_UNICODE);

                $writeActiveBid = addRejectActiveBids($newActivBidsJson, $conn);
                if (noError($writeActiveBid)) {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Success : Insert reject bid details in reject_bid_queue';

                    $errMsg    = 'Success :  Insert reject bid details in reject_bid_queue';
                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $errMsg;

                } else {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Failed :  Insert reject bid details in reject_bid_queue';

                    $errMsg    = 'Failed :  Insert reject bid details in reject_bid_queue';
                   // $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);

                    $returnArr["errCode"] = 97;
                    $returnArr["errMsg"] = $errMsg;
                }

                //make json for previousOwners
                $previousOwners   = json_decode($previousOwners, TRUE);

                $previousOwners[] = array(
                    'buyer_id' => $KeywordOwnerId,
                    'kwd_purchase_price' => $kwdLastTradedPrice,
                    'kwd_purchase_timestamp' => $kwdLastTradedTime,
                    'kwd_last_traded_price' => $bidAmt,
                    'kwd_new_owner' => $bidderEmail
                );
                //json for previous owners
                $previousOwners   = json_encode($previousOwners, JSON_UNESCAPED_UNICODE);

                $credential["previous_owner_with_new_owner"] = $previousOwners;

                $i                             = $i + 1;
                $xml_data["step" . $i]["data"] = $i . '. Success : get details';

                // get bidder user info

                $bidderDetails = $userRequiredFields . ",_id";

                $getBidderUserInfo = getUserInfo($bidderEmail, $walletURLIP . 'api/v3/', $bidderDetails);


                if (noError($getBidderUserInfo)) {
                    $getBidderUserInfo = $getBidderUserInfo["errMsg"];
                    $bidderUserId      = $getBidderUserInfo["_id"];
                    $bidderFirstName   = $getBidderUserInfo["first_name"];
                    $bidderLastName    = $getBidderUserInfo["last_name"];

                    $errMsg    = 'Success :  getting user info ';
                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $errMsg;


                } else {

                    $errMsg    = 'Failed :  getting user info ';
                   // $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

                    $returnArr["errCode"] = 96;
                    $returnArr["errMsg"] = $errMsg;

                    //print('Error: fetching user info');
                }

                $trans = startTransaction($conn);
                if (noError($trans)) {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Success : start transaction';


                    $errMsg    = 'Success : start transaction ';
                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $errMsg;


                    $query  = "UPDATE " . $bidTableName . " SET bid_status = '1' WHERE bid_transaction_id='" . $highestBidId . "'";
                    $result = runQuery($query, $conn);
                    if (noError($result)) {

                        $i                             = $i + 1;
                        $xml_data["step" . $i]["data"] = $i . '. Success : update kwd_bid table';

                        $errMsg    = 'Success : update kwd_bid table';
                        //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"] = $errMsg;


                        //get keyword table name
                        $ownershipTableName = getKeywordOwnershipTableName(cleanQueryParameter($conn, $keyword));

                        $query      = "UPDATE " . $ownershipTableName . " SET highest_bid_id ='',highest_bid_amount='',no_of_active_bids='',active_bids='',ask_transaction_id='',ask_price='',order_status='0',previous_owner ='" . $previousOwners . "',buyer_id='" . $bidderEmail . "',kwd_price='" . $bidAmt . "',purchase_timestamp = now(),ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . $keyword . "'";
                        // $upd_result = array("errCode" => -1);
                        $upd_result = runQuery($query, $conn);

                        //change bid_status from active to reject for rest active bids
                        $rejectBidsArr = json_decode($newActivBidsJson, true);

                        $rejI = 1;

                        foreach ($rejectBidsArr as $value) {
                            $rejectBidDetails   = explode("~~", $value);
                            $rejectBidTableName = $rejectBidDetails[0];
                            $rejectBidderEmail  = $rejectBidDetails[1];

                            $credential["reject_bid"][$rejI]["reject_bid_table_name"] = $rejectBidTableName;
                            $credential["reject_bid"][$rejI]["reject_bidder_email"]   = $rejectBidderEmail;

                            $query  = "UPDATE " . $rejectBidTableName . " SET bid_status = '2' WHERE bid_transaction_id='" . $value . "'";
                            $result = runQuery($query, $conn);
                            if (noError($result)) {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Success : update rejectBid table : ' . urlencode($rejectBidTableName) . ' ,bid_transaction_id: ' . urlencode($value);

                                $errMsg    = 'Success : update rejectBid table';
                             //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = $errMsg;


                            } else {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Failed : update rejectBid table :"' . urlencode($rejectBidTableName) . '",bid_transaction_id:"' . urlencode($value);

                                $errMsg    = 'Failed : update rejectBid table';
                               // $returnArr = setErrorStack($returnArr, 6, $errMsg, $extraArgs);

                                $returnArr["errCode"] = 95;
                                $returnArr["errMsg"] = $errMsg;

                            }
                            //delete from active bids of bidder and add to accepted bids
                            $deductActiveBidsCountOfUser = deductActiveBidsCountOfUser($rejectBidderEmail, $conn);

                            if (noError($deductActiveBidsCountOfUser)) {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Success :  update deductActiveBidsCountOfUser table';

                                $errMsg    = 'Success : update deductActiveBidsCountOfUser table';
                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = $errMsg;


                            } else {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Failed :  update deductActiveBidsCountOfUser table';

                                $errMsg    = 'Failed : update deductActiveBidsCountOfUser table';
                               // $returnArr = setErrorStack($returnArr, 7, $errMsg, $extraArgs);

                                $returnArr["errCode"] = 94;
                                $returnArr["errMsg"] = $errMsg;

                            }

                            $addRejectedBidsCountOfUser = addRejectedBidsCountOfUser($rejectBidderEmail, $conn);
                            if (noError($addRejectedBidsCountOfUser)) {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Success :  update addRejectedBidsCountOfUser table';

                                $errMsg    = 'Success : update addRejectedBidsCountOfUser table';
                               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = $errMsg;


                            } else {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Failed :  update addRejectedBidsCountOfUser table';

                                $errMsg    = 'Failed : update addRejectedBidsCountOfUser table';
                                //$returnArr = setErrorStack($returnArr, 8, $errMsg, $extraArgs);

                                $returnArr["errCode"] = 93;
                                $returnArr["errMsg"] = $errMsg;

                            }
                            $removeBidDetailsByBuyerId = removeBidDetailsByBuyerId($conn, $rejectBidderEmail, $value, $keyword);
                            if (noError($removeBidDetailsByBuyerId)) {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Success :  update bid details of patticular user';

                                $errMsg    = 'Success : update bid details of patticular user';
                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = $errMsg;


                            } else {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '.' . $rejI . '. Failed :  update bid details of patticular user';

                                $errMsg    = 'Success : update bid details of patticular user';
                                //$returnArr = setErrorStack($returnArr, 9, $errMsg, $extraArgs);

                                $returnArr["errCode"] = 92;
                                $returnArr["errMsg"] = $errMsg;

                            }
                            $rejI++;
                        } // end of foreach loop

                        if (noError($upd_result)) {

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . '. Success :  update ownershipTableName ';


                            $purchase_timestamp = date("j F, Y");
                            $result             = updateKwdDetailsAfterTrade($conn, $bidderEmail, $email, $purchase_timestamp, $keyword, $bidAmt, "trade");

                            if (noError($result)) {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '. Success : update mykeyword_details table ** values ** bidderEmail: ' . urlencode($bidderEmail) . ',email:' . urlencode($email) . ',purchase_timestamp:' . urlencode($purchase_timestamp) . ',keyword:' . urlencode($keyword) . ',bidAmt: ' . urlencode($bidAmt) . ',bidAmtComm:' . urlencode($bidAmtComm) . ',trade_type:trade';

                                $errMsg    = 'Success : update mykeyword_details table';
                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = $errMsg;

                                // calculate gross amount of keyword
                                $bidAmtGross = number_format((float) $bidAmt + $bidAmtComm + $kwdRenewalAmt , 8);

                                //Deduct blocked for bid amount
                                $result = creditUserEarning($bidderUserId, $bidAmtGross, "blockedbids", "deduct");

                                $credential["deduct_block_bid_amount"] = $result["errMsg"];

                                if (noError($result)) {

                                    $i                             = $i + 1;
                                    $xml_data["step" . $i]["data"] = $i . '. Success : dedcut blcok for bid amount API| bidderEmail:' . urlencode($bidderEmail) . ',bidAmtGross:' . urlencode($bidAmtGross);

                                    $errMsg    = 'Success : dedcut blcok for bid amount';
                                    //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                    $returnArr["errCode"] = -1;
                                    $returnArr["errMsg"] = $errMsg;

                                    //Successful deducting blocked bid amount from bidder wallet

                                    // add purchase amount to bidder account
                                    // $result   = addPurchases($bidderEmail, $bidAmt);

                                    $result = creditUserEarning($bidderUserId, $bidAmt, "purchase", "add");

                                    $credential["add_purchase_amount"] = $result["errMsg"];

                                    if (noError($result)) {

                                        $i                             = $i + 1;
                                        $xml_data["step" . $i]["data"] = $i . '. Success : Credit add purchase amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                        $errMsg    = 'Success : Credit add purchase amount';
                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                        $returnArr["errCode"] = -1;
                                        $returnArr["errMsg"] = $errMsg;

                                        // add trade amount to bidder account
                                        $result = creditUserEarning($bidderUserId, $bidAmtComm, "trade", "add");

                                        $credential["add_trade_amount"] = $result["errMsg"];

                                        if (noError($result)) {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Success : Credit add trade amount. API| bidderEmail:' . urlencode($bidderEmail) . ',bidAmtComm:' . urlencode($bidAmtComm);

                                            $errMsg    = 'Success : Credit add trade amount';
                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                            $returnArr["errCode"] = -1;
                                            $returnArr["errMsg"] = $errMsg;

                                            // credit renewal amount to bidder account
                                            $creditRenewalAmtToBidder = creditUserEarning($bidderUserId, $kwdRenewalAmt, "renewalfees", "add");

                                            $credential["add_renewal_amount"] = $creditRenewalAmtToBidder["errMsg"];

                                            if(noError($creditRenewalAmtToBidder)){
                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Success : Credit add renewal amount. API| bidderEmail:' . urlencode($bidderEmail) . ',RenewalAmount:' . urlencode($kwdRenewalAmt);

                                                $errMsg    = 'Success : Credit add renewal amount';
                                             //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                $returnArr["errCode"] = -1;
                                                $returnArr["errMsg"] = $errMsg;

                                                // integrate increaseTotalFeesEarning API after conformation form rihan
                                                //$result   = increaseTotalFeesEarning($bidAmtComm);
                                                $result = creditUserEarning($communityPoolUserId, $bidAmtComm, "trade", "add");

                                                $credential["increase_total_fees_earning"] = $result["errMsg"];

                                                if (noError($result)) {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Success : credit trade fees amount to community pool. API| bidAmtComm: ' . urlencode($bidAmtComm);

                                                    $errMsg    = 'Success : calling increaseTotalFeesEarning';
                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;


                                                    // credit renewal amount to community pool user.
                                                    $creditRenewalAmtToPool = creditUserEarning($communityPoolUserId, $kwdRenewalAmt, "renewalfees", "add");
                                                    if(noError($creditRenewalAmtToPool)){

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : credit renewal fees amount to community pool. API| renewalAmt: ' . urlencode($kwdRenewalAmt);

                                                        $errMsg    = 'Success : calling increaseTotalFeesEarning';
                                                        //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;


                                                        // insert user transaction for trades fees
                                                        // parameters required to insert in transaction
                                                        $recipientEmail     = $communityPoolUser;
                                                        $recipientUserId    = $communityPoolUserId;
                                                        $senderEmail        = $bidderEmail;
                                                        $senderUserId       = $bidderUserId;
                                                        $amount             = $bidAmtComm;
                                                        $type               = "trade_fees";
                                                        $paymentMode        = 'ITD';
                                                        $originIp           = $userIp;
                                                        $exchangeRateInJson = $currentExchangeRate;

                                                        $metaDetails = array(
                                                            "sender" => $bidderEmail,
                                                            "receiver" => $communityPoolUser,
                                                            "trade_amount" => $bidAmtComm,
                                                            "highest_bid" => $highestBidId,
                                                            "reject_bids" => json_encode($activBids),
                                                            "keyword" => urlencode(utf8_encode($keyword)),
                                                            "discount" => 0,
                                                            "commision" => 0,
                                                            "description" => "The trading fees amount earned by " . $communityPoolUser . " of trade amount " . $bidAmtComm . " ".$keywoDefaultCurrencyName." from " . $bidderEmail
                                                        );

                                                        $result = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                        $credential["insert_user_transaction_fees_earning"]["sender"]             = $bidderEmail;
                                                        $credential["insert_user_transaction_fees_earning"]["receiver"]           = $communityPoolUser;
                                                        $credential["insert_user_transaction_fees_earning"]["bid_trading_amount"] = $bidAmtComm;
                                                        $credential["insert_user_transaction_fees_earning"]["type"]               = $type;
                                                        $credential["insert_user_transaction_fees_earning"]["payment_mode"]       = $paymentMode;
                                                        $credential["insert_user_transaction_fees_earning"]["exchange_rate"]      = $exchangeRateInJson;
                                                        $credential["insert_user_transaction_fees_earning"]["origin_ip"]          = $originIp;
                                                        $credential["insert_user_transaction_fees_earning"]["meta_details"]       = $metaDetails;

                                                        if (noError($result)) {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . ". Success : calling insertUserTransaction API for community pool user trading fees earning. ";

                                                            $errMsg    = 'Success : Insert transaction for trade fees';
                                                          //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                            $returnArr["errCode"] = -1;
                                                            $returnArr["errMsg"] = $errMsg;

                                                            // insert user transaction for trades fees
                                                            // parameters required to insert in transaction
                                                            $recipientEmail     = $communityPoolUser;
                                                            $recipientUserId    = $communityPoolUserId;
                                                            $senderEmail        = $bidderEmail;
                                                            $senderUserId       = $bidderUserId;
                                                            $amount             = $kwdRenewalAmt;
                                                            $type               = "renewal_fees";
                                                            $paymentMode        = 'ITD';
                                                            $originIp           = $userIp;
                                                            $exchangeRateInJson = $currentExchangeRate;

                                                            $metaDetails = array(
                                                                "sender" => $bidderEmail,
                                                                "receiver" => $communityPoolUser,
                                                                "trade_amount" => $kwdRenewalAmt,
                                                                "highest_bid" => $highestBidId,
                                                                "reject_bids" => json_encode($activBids),
                                                                "keyword" => urlencode(utf8_encode($keyword)),
                                                                "discount" => 0,
                                                                "commision" => 0,
                                                                "description" => "The renewal fees amount earned by " . $communityPoolUser . " of renewal amount " . $kwdRenewalAmt . " ".$keywoDefaultCurrencyName." from " . $bidderEmail
                                                            );

                                                            $insertTransForRenewalEarning = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                            $credential["insert_user_transaction_renewal_fees_earning"]["sender"]             = $bidderEmail;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["receiver"]           = $communityPoolUser;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["bid_trading_amount"] = $kwdRenewalAmt;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["type"]               = $type;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["payment_mode"]       = $paymentMode;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["exchange_rate"]      = $exchangeRateInJson;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["origin_ip"]          = $originIp;
                                                            $credential["insert_user_transaction_renewal_fees_earning"]["meta_details"]       = $metaDetails;

                                                            if(noError($insertTransForRenewalEarning)){
                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". Success : calling insertUserTransaction API for community pool user for renewal fees earning.";

                                                                $errMsg    = 'Success : Insert transaction for renewal fees';
                                                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = $errMsg;

                                                            }else{
                                                                $i                             = $i + 1;
                                                                $xml_data["step" . $i]["data"] = $i . ". Failed: calling insertUserTransaction API for community pool user for renewal fees earning. |response:" . urlencode($response);

                                                                $errMsg    = 'Failed : Insert transaction for renewal fees';
                                                               // $returnArr = setErrorStack($returnArr, 32, $errMsg, $extraArgs);

                                                                $returnArr["errCode"] = 90;
                                                                $returnArr["errMsg"] = $errMsg;
                                                            }
                                                        } else {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . ". Failed : calling insertUserTransaction API for community pool user trading fees earning. |response:" . urlencode($response);

                                                            $errMsg    = 'Failed : Insert transaction for trade fees';
                                                           // $returnArr = setErrorStack($returnArr, 31, $errMsg, $extraArgs);

                                                            $returnArr["errCode"] = 89;
                                                            $returnArr["errMsg"] = $errMsg;
                                                        }

                                                    }else{

                                                        $i  = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : credit renewal fees amount to community pool. API| renewalAmt: ' . urlencode($kwdRenewalAmt);

                                                        $errMsg    = 'Failed : Credit renewal amount to pool';
                                                     //   $returnArr = setErrorStack($returnArr, 30, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 88;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        // deduct trade amount from
                                                        $result   = creditUserEarning($communityPoolUserId, $bidAmtComm, "trade", "deduct");

                                                        if (noError($result)) {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . '. Success: Deduct puchase amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                            $errMsg    = 'Success : Deduct puchase amount';
                                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                            $returnArr["errCode"] = -1;
                                                            $returnArr["errMsg"] = $errMsg;

                                                        } else {

                                                            $i                             = $i + 1;
                                                            $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductPurchases API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                            $errMsg    = 'Success : Deduct puchase amount';
                                                          //  $returnArr = setErrorStack($returnArr, 28, $errMsg, $extraArgs);

                                                            $returnArr["errCode"] = 87;
                                                            $returnArr["errMsg"] = $errMsg;

                                                        }

                                                    }


                                                    //commmit transaction
                                                    $result = commitTransaction($conn);
                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : commit transaction';

                                                        $errMsg    = 'Success : commit transaction';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;


                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : commit transaction';

                                                        $errMsg    = 'Failed : commit transaction';
                                                       // $returnArr = setErrorStack($returnArr, 10, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 86;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    // Enable database default autocommit functionality
                                                    $result = mysqli_autocommit($conn, TRUE);
                                                    if (!$result) {
                                                        $returnArr["errCode"] = 7;
                                                        $returnArr["errMsg"]  = "Could not start transaction: " . mysqli_error($conn);

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : Could not start transaction: ' . mysqli_error($conn);


                                                    } else {
                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"]  = "Transaction started";

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : Transaction started';

                                                    }

                                                    //remove active bids from users json
                                                    $removeBidDetailsByBuyerId = removeBidDetailsByBuyerId($conn, $bidderEmail, $highestBidId, $keyword);

                                                    if (noError($removeBidDetailsByBuyerId)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update bid details of particular user';

                                                        $errMsg    = 'Success : update bid details of particular user';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update bid details of patticular user';

                                                        $errMsg    = 'Success : update bid details of particular user';
                                                       // $returnArr = setErrorStack($returnArr, 11, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 85;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //clear the row-locking on keyword ownerhip table
                                                    $clearKeywordOwnershipLock = clearKeywordOwnershipLock($keyword, $conn);

                                                    if (noError($clearKeywordOwnershipLock)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : clear keyword ownerhip lock';

                                                        $errMsg    = 'Success : clear keyword ownerhip lock';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;


                                                    } else {
                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : clear keyword ownerhip lock';

                                                        $errMsg    = 'Failed : clear keyword ownerhip lock';
                                                       // $returnArr = setErrorStack($returnArr, 12, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 84;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //delete from active bids of bidder and add to accepted bids
                                                    $deductActiveBidsCountOfUser = deductActiveBidsCountOfUser($bidderEmail, $conn);

                                                    if (noError($deductActiveBidsCountOfUser)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update  deductActiveBidsCountOfUser';

                                                        $errMsg    = 'Success : update  deductActiveBidsCountOfUser';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update  deductActiveBidsCountOfUser';

                                                        $errMsg    = 'Failed : update  deductActiveBidsCountOfUser';
                                                        //$returnArr = setErrorStack($returnArr, 13, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 83;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $addAcceptedBidsCountOfUser = addAcceptedBidsCountOfUser($bidderEmail, $conn);

                                                    if (noError($addAcceptedBidsCountOfUser)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update  addAcceptedBidsCountOfUser';

                                                        $errMsg    = 'Success : update addAcceptedBidsCountOfUser';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update  addAcceptedBidsCountOfUser';

                                                        $errMsg    = 'Failed : update addAcceptedBidsCountOfUser';
                                                      //  $returnArr = setErrorStack($returnArr, 14, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 82;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }


                                                    $buyerDetails  = $userRequiredFields . ",_id";
                                                    $buyerUserInfo = getUserInfo($email, $walletURLIP . 'api/v3/', $buyerDetails);

                                                    if (noError($buyerUserInfo)) {
                                                        $buyerUserInfo  = $buyerUserInfo["errMsg"];
                                                        $buyerId        = $buyerUserInfo["_id"];
                                                        $buyerFirstName = $buyerUserInfo["first_name"];
                                                        $buyerLastName  = $buyerUserInfo["last_name"];

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : Getting  ' . $email ;

                                                        $errMsg    = 'Success : Getting  ' . $email . ' info';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : Getting  ' . $email . ' info';

                                                        $errMsg    = 'Failed : Getting  ' . $email . ' info';
                                                      //  $returnArr = setErrorStack($returnArr, 15, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 82;
                                                        $returnArr["errMsg"] = $errMsg;


                                                        //	print('Error: fetching buyer details');
                                                    }

                                                    //add to sales for keyword owner
                                                    //$result   = addSales($email, $bidAmt);
                                                    $result = creditUserEarning($buyerId, $bidAmt, "sales", "add");

                                                    $credential["add_sales_amount"] = $result["errMsg"];

                                                    if (noError($result)) {
                                                        //success adding Seller sales balance

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : Credit add sales amount. API| email: ' . urlencode($email) . ',bidAmt: ' . urlencode($bidAmt);

                                                        $errMsg    = 'Success : Credit add sales amount';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {
                                                        //error crediting Seller wallet balance

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : calling addSales API| email: ' . urlencode($email) . ',bidAmt: ' . urlencode($bidAmt);

                                                        $errMsg    = 'Failed : Credit add sales amount';
                                                      //  $returnArr = setErrorStack($returnArr, 16, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 81;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    // add to latest trade table
                                                    $addToLatestTrade = addToLatestTrade($conn, $keyword, $bidderEmail, $bidAmt);

                                                    $credential["add_trade_amount"] = $result["errMsg"];

                                                    if (noError($addToLatestTrade)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update addToLatestTrade table';

                                                        $errMsg    = 'Success : update addToLatestTrade table';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update addToLatestTrade table';

                                                        $errMsg    = 'Failed : update addToLatestTrade table';
                                                       // $returnArr = setErrorStack($returnArr, 16, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 81;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //delete from highest ask table
                                                    $deleteFromHighestAsk = deleteFromHighestAskByKeyword($conn, $keyword);

                                                    if (noError($deleteFromHighestAsk)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update deleteFromHighestAsk table';

                                                        $errMsg    = 'Success : update deleteFromHighestAsk table';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update deleteFromHighestAsk table';

                                                        $errMsg    = 'Failed : update deleteFromHighestAsk table';
                                                       // $returnArr = setErrorStack($returnArr, 17, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 80;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $deleteFromMostBid = deleteFromMostBidByKeyword($conn, $keyword);

                                                    if (noError($deleteFromMostBid)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update deleteFromMostBid table';

                                                        $errMsg    = 'Success : update  deleteFromMostBid table';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update deleteFromMostBid table';

                                                        $errMsg    = 'Failed : update  deleteFromMostBid table';
                                                        $returnArr = setErrorStack($returnArr, 18, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 79;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //delete from highest bid table
                                                    $deleteFromHighestBid = deleteFromHighestBidByKeyword($conn, $keyword);

                                                    if (noError($deleteFromHighestBid)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update deleteFromHighestBid table';

                                                        $errMsg    = 'Success : update deleteFromHighestBid table';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update deleteFromHighestBid table';

                                                        $errMsg    = 'Failed : update deleteFromHighestBid table';
                                                    //    $returnArr = setErrorStack($returnArr, 19, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 78;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //delete from latest ask
                                                    $deleteFromLatestAsk = deleteFromLatestAskByKeyword($conn, $keyword);

                                                    if (noError($deleteFromLatestAsk)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update deleteFromLatestAsk table ';

                                                        $errMsg    = 'Success :  update deleteFromLatestAsk table';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update deleteFromLatestAsk table ';

                                                        $errMsg    = 'Failed :  update deleteFromLatestAsk table';
                                                       // $returnArr = setErrorStack($returnArr, 19, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 77;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //delete from latest bid
                                                    $deleteFromLatestBid = deleteFromLatestBidByKeyword($conn, $keyword);

                                                    if (noError($deleteFromLatestBid)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : update deleteFromLatestBid table ';

                                                        $errMsg    = 'Success :  update deleteFromLatestBid table';
                                                     //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : update deleteFromLatestBid table ';

                                                        $errMsg    = 'Failed :  update deleteFromLatestBid table';
                                                     //   $returnArr = setErrorStack($returnArr, 20, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 76;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }


                                                    // parameters required to insert in transaction
                                                    $recipientEmail     = $email;
                                                    $recipientUserId    = $buyerId;
                                                    $senderEmail        = $bidderEmail;
                                                    $senderUserId       = $bidderUserId;
                                                    $amount             = $bidAmt;
                                                    $type               = "trade";
                                                    $paymentMode        = 'ITD';
                                                    $originIp           = $userIp;
                                                    $exchangeRateInJson = $currentExchangeRate;

                                                    $metaDetails = array(
                                                        "sender" => $bidderEmail,
                                                        "receiver" => $email,
                                                        "bid_amount" => $bidAmt,
                                                        "highest_bid" => $highestBidId,
                                                        "reject_bids" => json_encode($activBids),
                                                        "keyword" => urlencode(utf8_encode($keyword)),
                                                        "discount" => 0,
                                                        "commision" => 0
                                                    );

                                                    $result = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                    $credential["insert_user_transaction_for_keyword_trade"]["sender"]        = $bidderEmail;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["receiver"]      = $email;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["bid_amount"]    = $bidAmt;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["type"]          = $type;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["payment_mode"]  = $paymentMode;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["origin_ip"]     = $originIp;
                                                    $credential["insert_user_transaction_for_keyword_trade"]["exchange_rate"] = $exchangeRateInJson;

                                                    // $result   = insertUserTransaction($bidderEmail, $email, $bidAmt, 'trade', $deviceType, $keyword, 'bitcoin', '', $bidAmtComm, $origin_ip, $usd, $sgd);

                                                    if (noError($result)) {
                                                        //success inserting keyword purchase transaction

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : calling API insertUserTransaction.';

                                                        $errMsg    = 'Success : calling API insertUserTransaction';
                                                     //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {
                                                        //error inserting keyword purchase transaction

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : calling API insertUserTransaction';

                                                        $errMsg    = 'Failed : calling API insertUserTransaction';
                                                        $returnArr = setErrorStack($returnArr, 21, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 75;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $activeBidI = 1;
                                                    foreach ($activBids as $value) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '.' . $activeBidI . '. Success :  Bid Id : ' . urlencode($value);

                                                        $activeBidI++;
                                                    }

                                                    //insert trading history
                                                    //get revenue details
                                                    $result   = getRevenueDetailsByKeyword($conn, $keyword);

                                                    if (noError($result)) {
                                                        //success updating fees pool

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : calling function getRevenueDetailsByKeyword| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Success : get revenue details by keyword';
                                                     //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {
                                                        //error updating fees pool

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : calling function getRevenueDetailsByKeyword| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Failed : get revenue details by keyword';
                                                      //  $returnArr = setErrorStack($returnArr, 22, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 74;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $user_kwd_search_count       = $result["errMsg"]["user_kwd_search_count"];
                                                    $user_kwd_ownership_earnings = $result["errMsg"]["user_kwd_ownership_earnings"];

                                                    $credential["user_kwd_search_count"]       = $user_kwd_search_count;
                                                    $credential["user_kwd_ownership_earnings"] = $user_kwd_ownership_earnings;

                                                    $result = deleteUserStatsFromRevenueTable($conn, $keyword);

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : updating keyword owner revenue stats deleteUserStatsFromRevenueTable| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Success : updating keyword owner revenue stats deleteUserStatsFromRevenueTable';
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : updating keyword owner revenue stats deleteUserStatsFromRevenueTable| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Failed : updating keyword owner revenue stats deleteUserStatsFromRevenueTable';
                                                      //  $returnArr = setErrorStack($returnArr, 23, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 73;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $URLTradingHistory = $docRoot . "json_directory/keywords/trading_history/";
                                                    if (!is_dir($URLTradingHistory)) {
                                                        mkdir($URLTradingHistory, 0777, true);
                                                    }

                                                    $URLTradingHistory = $URLTradingHistory . $email . ".json";;

                                                    if (!file_exists($URLTradingHistory)) {
                                                        //create new array
                                                        $tradingHistory   = array();
                                                        $tradingHistory[] = array(
                                                            'buyer_id' => $KeywordOwnerId,
                                                            'keyword' => $keyword,
                                                            'kwd_purchase_price' => $kwdLastTradedPrice,
                                                            'kwd_purchase_timestamp' => $kwdLastTradedTime,
                                                            'kwd_last_traded_price' => $bidAmt,
                                                            'kwd_new_owner' => $bidderEmail,
                                                            'user_kwd_search_count' => $user_kwd_search_count,
                                                            'user_kwd_ownership_earnings' => $user_kwd_ownership_earnings
                                                        );

                                                        $tradingHistory = json_encode($tradingHistory, JSON_UNESCAPED_UNICODE);
                                                        //put content in json file
                                                        file_put_contents($URLTradingHistory, $tradingHistory);
                                                    } else {
                                                        $tradingHistory   = file_get_contents($URLTradingHistory);
                                                        $tradingHistory   = json_decode($tradingHistory, TRUE);
                                                        $tradingHistory[] = array(
                                                            'buyer_id' => $KeywordOwnerId,
                                                            'keyword' => $keyword,
                                                            'kwd_purchase_price' => $kwdLastTradedPrice,
                                                            'kwd_purchase_timestamp' => $kwdLastTradedTime,
                                                            'kwd_last_traded_price' => $bidAmt,
                                                            'kwd_new_owner' => $bidderEmail,
                                                            'user_kwd_search_count' => $user_kwd_search_count,
                                                            'user_kwd_ownership_earnings' => $user_kwd_ownership_earnings
                                                        );
                                                        $tradingHistory   = json_encode($tradingHistory, JSON_UNESCAPED_UNICODE);
                                                        file_put_contents($URLTradingHistory, $tradingHistory);
                                                    }

                                                    // insert trade profit
                                                    $result = insertTradeProfit($keyword, $KeywordOwnerId, $kwdLastTradedPrice, $bidderEmail, $bidAmt, $user_kwd_ownership_earnings, $conn);

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : updating keyword owner revenue stats deleteUserStatsFromRevenueTable| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Success : updating keyword owner revenue stats deleteUserStatsFromRevenueTable';
                                                   //     $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : updating keyword owner revenue stats deleteUserStatsFromRevenueTable| keyword: ' . urlencode($keyword);

                                                        $errMsg    = 'Failed : updating keyword owner revenue stats deleteUserStatsFromRevenueTable';
                                                      //  $returnArr = setErrorStack($returnArr, 24, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 72;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $ownershipDetailByKeyword = getKeywordOwnershipTableName(cleanQueryParameter($conn, $keyword));
                                                    if(noError($ownershipDetailByKeyword))
                                                    {
                                                        $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                                        $daysOfExpire                = $checkForKeywordAvailability["ownership_expiry_time"];
                                                    }


                                                    //send mail for bidder
                                                    $to      = $bidderEmail;
                                                    $subject = "Keyword purchase success";
                                                    $message =
                                                        '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$bidderFirstName.',</td>                                                    
                                                                <tr> 
                                                                    <td style="padding: 10px 20px;">
                                                                    We are pleased to inform that you are now the owner of the license for <b>#'.$keyword.'</b>.This license is valid till '.$daysOfExpire.' <br>If you have any questions or concerns, please feel free to contact us at support@keywo.com <br><br>Regards,<br>Team Keywo.
                                                                     </td>
                                                                </tr>';
                                                    $notification_message = 'You have successfully purchased the license for <b>#' . $keyword . '</b> This license is valid till '.$daysOfExpire.'.';
                                                    $method_name          = "GET";

                                                    //$bidderUserId $bidderFirstName $bidderLastName
                                                    $getNotification = notifyoptions($bidderUserId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                    $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];

                                                    $path    = "";
                                                    $keyword = $keyword;

                                                    $category   = "buy";
                                                    $linkStatus = 1;
                                                    $notiSend   = sendNotificationBuyPrefrence($to, $subject, $message, $bidderFirstName, $bidderLastName, $bidderUserId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);
                                                    $linkStatus = "";
                                                    if (noError($notiSend)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : Send email to ' . $bidderEmail;

                                                        $errMsg    = 'Success : Send email to ' . $bidderEmail;
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : Send email to ' . $bidderEmail;

                                                        $errMsg    = 'Failed : Send email to ' . $bidderEmail;
                                                     //   $returnArr = setErrorStack($returnArr, 25, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 71;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    //send mail for keyword owner
                                                    //	$buyerId $buyerFirstName $buyerLastName
                                                    $to      = $email;
                                                    $subject = "Accept Bid Confirmation";
                                                    $message =
                                                        '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                <tr> 
                                                                    <td style="padding: 10px 20px;">
                                                                    You have accepted a bid of ' . number_format((float) $bidAmt, 8) . ' '.$keywoDefaultCurrencyName.' on <b>#' . $keyword . '</b> owned by you. Same amount will be credited to your Keywo wallet shortly.<br>If you have any questions or concerns, please feel free to contact us at support@keywo.com.<br><br>Regards,<br>Team Keywo
                                                                     </td>
                                                                </tr>';

                                                    $notification_message = 'You have accepted a bid of ' . number_format((float) $bidAmt, 8) . ' BTC for #' . $keyword . 'owned by you. Same amount will be credited to your Keywo wallet shortly.';
                                                    $method_name          = "GET";
                                                    $getNotification      = notifyoptions($buyerId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                    $permissionCode       = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["0"]["permissions"]["_id"];
                                                    $path                 = "";
                                                    $keyword              = $keyword;
                                                    $notiSend             = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);
                                                    if (noError($notiSend)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success : Send email to ' . $email;

                                                        $errMsg    = 'Success : Send email to ' . $email;
                                                      //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed : Send email to ' . $email;

                                                        $errMsg    = 'Failed : Send email to ' . $email;
                                                        //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = 70;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    // remove renew keyword from presale table
                                                    $getPresaleDetails = getUserCartDetails($email, $conn);
                                                    if(noError($getPresaleDetails)){
                                                        $getPresaleDetails = $getPresaleDetails["errMsg"];

                                                        $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];
                                                        $getRenewKwd = json_decode($getRenewKwd, true);

                                                        if(in_array($keyword, $getRenewKwd)){
                                                            //Remove Keyword From Cart Array
                                                            $getRenewKwd = array_diff($getRenewKwd, array(
                                                                $keyword
                                                            ));

                                                            $getRenewKwd = json_encode($getRenewKwd,JSON_UNESCAPED_UNICODE);

                                                            $updateRenewKwd = updateRenewKwdInPresale($email, $getRenewKwd, $conn);

                                                            if(noError($updateRenewKwd)){
                                                                $errMsg = "Successfully update renew keyword details";
                                                             //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                                $responseArr["errCode"] = -1;
                                                                $responseArr["errMsg"]  = $errMsg;

                                                                $xml_data['response']["data"]      = '';
                                                                $xml_data['response']["attribute"] = $responseArr;

                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]="-1";
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/

                                                            }else{
                                                                $errMsg    = 'Error: Update renew keyword details in presale.';
                                                             //   $returnArr = setErrorStack($returnArr, 28, $errMsg, $extraArgs);

                                                                $responseArr["errCode"] = $returnArr["errCode"];
                                                                $responseArr["errMsg"]  = $errMsg;

                                                                $xml_data['response']["data"]      = '';
                                                                $xml_data['response']["attribute"] = $responseArr;

                                                                $returnArr["errCode"] = 69;
                                                                $returnArr["errMsg"] = $errMsg;

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]=69;
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/


                                                            }
                                                        }
                                                    }else{
                                                        $errMsg    = 'Error: Getting presale details';
                                                      //  $returnArr = setErrorStack($returnArr, 27, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = $returnArr["errCode"];
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $returnArr["errCode"] = 68;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=68;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/
                                                    }


                                                    $sendOwnershipZip = sendKwdOwnershipZipToAdmin($keyword, $conn, "trade");

                                                    if ($returnArr["errCode"] == -1) {
                                                        $errMsg    = 'You have successfully accepted the bid of #'.$keyword.' keyword';
                                                      //  $extraArgs["redirect_url"] = "{$redirectUrl}user_dashboard/owned_keyword.php";
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = -1;
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $_SESSION["accept_bid_error_code"] = -1;
                                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;


                                                        /******************** for Keyword Statistics ****************/
                                                        insert_keyword_statistics("total_kwd_traded","0",$conn);
                                                        insert_keyword_statistics("total_keyword_traded_amount",$bidAmt,$conn);
                                                        insert_keyword_statistics("total_keyword_traded_fees",$bidAmtComm,$conn);
                                                        insert_keyword_statistics("total_keyword_renewal_fees",$kwdRenewalAmt,$conn);
                                                        /******************** for Keyword Statistics ****************/

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=-1;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {
                                                        $errMsg    = 'There is an error while accepting bid';
                                                      //  $returnArr = setErrorStack($returnArr, 26, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = $returnArr["errCode"];
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $_SESSION["accept_bid_error_code"] = 26;
                                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                                        $returnArr["errCode"] = 68;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=68;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    }


                                                } else {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Failed : calling increaseTotalFeesEarning API| bidAmtComm: ' . urlencode($bidAmtComm);

                                                    rollbackTransaction($conn);

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '.transaction : rollback';

                                                    // $result   = addBlockedForBids($bidderEmail, $bidAmtGross);
                                                    $result   = creditUserEarning($bidderUserId, $bidAmtGross, "blockedbids", "add");

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success: Credit add block bid amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                        $errMsg    = 'Success : add block bid amount';
                                                     //   $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]="-1";
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed: calling addBlockedForBids API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                        $errMsg    = 'Failed : add block bid amount';
                                                        //$returnArr = setErrorStack($returnArr, 27, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = 27;
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $returnArr["errCode"] = 67;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=67;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    }

                                                    // $result   = deductPurchases($bidderEmail, $bidAmt);
                                                    $result   = creditUserEarning($bidderUserId, $bidAmt, "purchase", "deduct");

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success: Deduct puchase amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                        $errMsg    = 'Success : Deduct puchase amount';
                                                        //$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=-1;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductPurchases API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                        $errMsg    = 'Success : Deduct puchase amount';
                                                      //  $returnArr = setErrorStack($returnArr, 28, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = 28;
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $returnArr["errCode"] = 66;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=66;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    }
                                                    //$result   = deductTrade($bidderEmail, $bidAmtComm);
                                                    // deduct trade amount
                                                    $result = creditUserEarning($bidderUserId, $bidAmtComm, "trade", "deduct");

                                                    $credential["deduct_trade_amount"] = $result["errMsg"];

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success: Deduct trade amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                        $errMsg    = 'Success : Deduct trade amount.';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=-1;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductTrade API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                        $errMsg    = 'Failed : Deduct trade amount.';
                                                      //  $returnArr = setErrorStack($returnArr, 29, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = 29;
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $returnArr["errCode"] = 65;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=65;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    }

                                                    // deduct renewal amount
                                                    $result = creditUserEarning($bidderUserId, $kwdRenewalAmt, "renewalfees", "deduct");

                                                    $credential["deduct_renewal_amount"] = $result["errMsg"];

                                                    if (noError($result)) {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Success: Deduct renewal amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                        $errMsg    = 'Success : Deduct trade amount.';
                                                       // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                        $returnArr["errCode"] = -1;
                                                        $returnArr["errMsg"] = $errMsg;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]=-1;
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                    } else {

                                                        $i                             = $i + 1;
                                                        $xml_data["step" . $i]["data"] = $i . '. Failed: Deduct renewal amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                        $errMsg    = 'Failed : Deduct trade amount.';
                                                       // $returnArr = setErrorStack($returnArr, 29, $errMsg, $extraArgs);

                                                        $responseArr["errCode"] = 29;
                                                        $responseArr["errMsg"]  = $errMsg;

                                                        $xml_data['response']["data"]      = '';
                                                        $xml_data['response']["attribute"] = $responseArr;

                                                        $returnArr["errCode"] = 64;
                                                        $returnArr["errMsg"] = $errMsg;

                                                    }

                                                    $_SESSION["accept_bid_error_code"] = 29;
                                                    $_SESSION["accept_bid_error_message"] = $errMsg;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]=29;
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/
                                                }

                                            }else{
                                                $i = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Failed : Credit add renewal amount. API| bidderEmail:' . urlencode($bidderEmail) . ',RenewalAmount:' . urlencode($kwdRenewalAmt);

                                                rollbackTransaction($conn);

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                                                // $result   = addBlockedForBids($bidderEmail, $bidAmtGross);

                                                $result = creditUserEarning($bidderUserId, $bidAmtGross, "blockedbids", "add");

                                                $credential["add_blcok_for_bid"] = $result["errMsg"];
                                                if (noError($result)) {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Success: Credit add blcoked for bid amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                    $errMsg    = 'Success :  Credit add blcoked for bid amount.';
                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                } else {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Failed: calling addBlockedForBids API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                    $errMsg    = 'Failed :  Credit add blcoked for bid amount.';
                                                  //  $returnArr = setErrorStack($returnArr, 28, $errMsg, $extraArgs);

                                                    $responseArr["errCode"] = 28;
                                                    $responseArr["errMsg"]  = $errMsg;

                                                    $xml_data['response']["data"]      = '';
                                                    $xml_data['response']["attribute"] = $responseArr;

                                                    $returnArr["errCode"] = 63;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }
                                                // $result   = deductPurchases($bidderEmail, $bidAmt);
                                                $result = creditUserEarning($bidderUserId, $bidAmt, "purchase", "deduct");

                                                $credential["deduct_purchase_amount"] = $result["errMsg"];

                                                if (noError($result)) {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Success: Deduct purchase amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                    $errMsg    = 'Success :  Deduct purchase amount.';
                                                   // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                } else {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductPurchases API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                    $errMsg    = 'Failed :  Deduct purchase amount.';
                                                  //  $returnArr = setErrorStack($returnArr, 30, $errMsg, $extraArgs);

                                                    $responseArr["errCode"] = 30;
                                                    $responseArr["errMsg"]  = $errMsg;

                                                    $xml_data['response']["data"]      = '';
                                                    $xml_data['response']["attribute"] = $responseArr;

                                                    $returnArr["errCode"] = 62;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }

                                                // deduct trade amount
                                                $result = creditUserEarning($bidderUserId, $bidAmtComm, "trade", "deduct");

                                                $credential["deduct_trade_amount"] = $result["errMsg"];

                                                if (noError($result)) {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Success: Deduct trade amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                    $errMsg    = 'Success : Deduct trade amount.';
                                                  //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                    $returnArr["errCode"] = -1;
                                                    $returnArr["errMsg"] = $errMsg;

                                                } else {

                                                    $i                             = $i + 1;
                                                    $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductTrade API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                                    $errMsg    = 'Failed : Deduct trade amount.';
                                                   // $returnArr = setErrorStack($returnArr, 29, $errMsg, $extraArgs);

                                                    $responseArr["errCode"] = 29;
                                                    $responseArr["errMsg"]  = $errMsg;

                                                    $xml_data['response']["data"]      = '';
                                                    $xml_data['response']["attribute"] = $responseArr;

                                                    $returnArr["errCode"] = 61;
                                                    $returnArr["errMsg"] = $errMsg;

                                                }

                                                $_SESSION["accept_bid_error_code"] = 30;
                                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]=30;
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/

                                            }


                                        } else {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Success: calling addTrade API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtComm: ' . urlencode($bidAmtComm);

                                            rollbackTransaction($conn);

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                                            // $result   = addBlockedForBids($bidderEmail, $bidAmtGross);

                                            $result = creditUserEarning($bidderUserId, $bidAmtGross, "blockedbids", "add");

                                            $credential["add_blcok_for_bid"] = $result["errMsg"];
                                            if (noError($result)) {

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Success: Credit add blcoked for bid amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                $errMsg    = 'Success :  Credit add blcoked for bid amount.';
                                              //  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                $returnArr["errCode"] = -1;
                                                $returnArr["errMsg"] = $errMsg;

                                            } else {

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Failed: calling addBlockedForBids API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                                $errMsg    = 'Failed :  Credit add blcoked for bid amount.';
                                              //  $returnArr = setErrorStack($returnArr, 29, $errMsg, $extraArgs);

                                                $responseArr["errCode"] = 29;
                                                $responseArr["errMsg"]  = $errMsg;

                                                $xml_data['response']["data"]      = '';
                                                $xml_data['response']["attribute"] = $responseArr;

                                                $returnArr["errCode"] = 60;
                                                $returnArr["errMsg"] = $errMsg;

                                            }
                                            // $result   = deductPurchases($bidderEmail, $bidAmt);
                                            $result = creditUserEarning($bidderUserId, $bidAmt, "purchase", "deduct");

                                            $credential["deduct_purchase_amount"] = $result["errMsg"];

                                            if (noError($result)) {

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Success: Deduct purchase amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                $errMsg    = 'Success :  Deduct purchase amount.';
                                               // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                                $returnArr["errCode"] = -1;
                                                $returnArr["errMsg"] = $errMsg;

                                            } else {

                                                $i                             = $i + 1;
                                                $xml_data["step" . $i]["data"] = $i . '. Failed: calling deductPurchases API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt: ' . urlencode($bidAmt);

                                                $errMsg    = 'Failed :  Deduct purchase amount.';
                                               // $returnArr = setErrorStack($returnArr, 30, $errMsg, $extraArgs);

                                                $responseArr["errCode"] = 30;
                                                $responseArr["errMsg"]  = $errMsg;

                                                $xml_data['response']["data"]      = '';
                                                $xml_data['response']["attribute"] = $responseArr;

                                                $returnArr["errCode"] = 59;
                                                $returnArr["errMsg"] = $errMsg;

                                            }

                                            $_SESSION["accept_bid_error_code"] = 30;
                                            $_SESSION["accept_bid_error_message"] = $errMsg;

                                            /******************** for logs user analytics ****************/
                                            $responseArr["errCode"]=30;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;
                                            /******************** for logs user analytics ****************/
                                        }
                                    } else {

                                        $i                             = $i + 1;
                                        $xml_data["step" . $i]["data"] = $i . '. Failed: calling addPurchases API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmt:' . urlencode($bidAmt);

                                        rollbackTransaction($conn);

                                        $i                             = $i + 1;
                                        $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                                        // add blocked for bid

                                        $result = creditUserEarning($bidderUserId, $bidAmtGross, "blockedbids", "add");
                                        // $result   = addBlockedForBids($bidderEmail, $bidAmtGross);

                                        $credential["add_block_for_bid"] = $result["errMsg"];

                                        if (noError($result)) {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Success: Credit add block for bid amount. API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                            $errMsg    = 'Success : Credit add block for bid amount.';
                                           // $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                                            $returnArr["errCode"] = -1;
                                            $returnArr["errMsg"] = $errMsg;

                                        } else {

                                            $i                             = $i + 1;
                                            $xml_data["step" . $i]["data"] = $i . '. Failed: calling addBlockedForBids API| bidderEmail: ' . urlencode($bidderEmail) . ',bidAmtGross: ' . urlencode($bidAmtGross);

                                            $errMsg    = 'Failed : Credit add block for bid amount.';
                                          //  $returnArr = setErrorStack($returnArr, 31, $errMsg, $extraArgs);

                                            $responseArr["errCode"] = 31;
                                            $responseArr["errMsg"]  = $errMsg;

                                            $xml_data['response']["data"]      = '';
                                            $xml_data['response']["attribute"] = $responseArr;

                                            $returnArr["errCode"] = 58;
                                            $returnArr["errMsg"] = $errMsg;

                                        }

                                        $_SESSION["accept_bid_error_code"] = 31;
                                        $_SESSION["accept_bid_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=31;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }
                                } else {

                                    $i                             = $i + 1;
                                    $xml_data["step" . $i]["data"] = $i . '. Failed: Deduct block for bid amount. API| bidderEmail:' . urlencode($bidderEmail) . ',bidAmtGross:' . urlencode($bidAmtGross);

                                    //error deducting blocked bid amount from bidder wallet
                                    rollbackTransaction($conn);

                                    $i                             = $i + 1;
                                    $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                                    $errMsg    = 'Failed : There was an error while accepting the bid for this keyword.';
                                 //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                    $returnArr["errCode"] = 57;
                                    $returnArr["errMsg"] = $errMsg;

                                    $responseArr["errCode"] = 32;
                                    $responseArr["errMsg"]  = $errMsg;

                                    $xml_data['response']["data"]      = '';
                                    $xml_data['response']["attribute"] = $responseArr;

                                    $_SESSION["accept_bid_error_code"] = 32;
                                    $_SESSION["accept_bid_error_message"] = $errMsg;

                                }
                            } else {

                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '. Failed: update mykeyword_details table ** values ** bidderEmail:' . urlencode($bidderEmail) . ',email:' . urlencode($email) . ',purchase_timestamp:' . urlencode($purchase_timestamp) . ',keyword:' . urlencode($keyword) . ',bidAmt:' . urlencode($bidAmt) . ',bidAmtComm:' . urlencode($bidAmtComm) . ',trade_type:trade';

                                //rollback transction
                                rollbackTransaction($conn);
                                $i                             = $i + 1;
                                $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                                $errMsg    = 'Failed : There was an error while accepting the bid for this keyword.';
                              //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                                $responseArr["errCode"] = 33;
                                $responseArr["errMsg"]  = $errMsg;

                                $xml_data['response']["data"]      = '';
                                $xml_data['response']["attribute"] = $responseArr;

                                $_SESSION["accept_bid_error_code"] = 33;
                                $_SESSION["accept_bid_error_message"] = $errMsg;

                                $returnArr["errCode"] = 56;
                                $returnArr["errMsg"] = $errMsg;

                            }
                        } else {

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . '. Failed: update ownershipTableName';

                            //rollback transction
                            rollbackTransaction($conn);

                            $i                             = $i + 1;
                            $xml_data["step" . $i]["data"] = $i . '. transaction : rollback';

                            $errMsg    = 'Failed : There was an error while accepting the bid for this keyword.';
                         //   $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                            $responseArr["errCode"] = 35;
                            $responseArr["errMsg"]  = $errMsg;

                            $xml_data['response']["data"]      = '';
                            $xml_data['response']["attribute"] = $responseArr;

                            $_SESSION["accept_bid_error_code"] = 35;
                            $_SESSION["accept_bid_error_message"] = $errMsg;

                            $returnArr["errCode"] = 35;
                            $returnArr["errMsg"] = $errMsg;


                        }
                    } else {
                        $i                             = $i + 1;
                        $xml_data["step" . $i]["data"] = $i . '. Failed: update kwd_bid table';

                        $errMsg    = 'Failed : There was an error while accepting the bid for this keyword.';
                       // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                        $responseArr["errCode"] = 36;
                        $responseArr["errMsg"]  = $errMsg;

                        $xml_data['response']["data"]      = '';
                        $xml_data['response']["attribute"] = $responseArr;

                        $_SESSION["accept_bid_error_code"] = 36;
                        $_SESSION["accept_bid_error_message"] = $errMsg;

                        $returnArr["errCode"] = 53;
                        $returnArr["errMsg"] = $errMsg;

                    }
                } else {

                    $i                             = $i + 1;
                    $xml_data["step" . $i]["data"] = $i . '. Failed: unable to start transaction';

                    $errMsg    = 'Failed : There was an error while accepting the bid for this keyword.';
                  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                    $responseArr["errCode"] = 37;
                    $responseArr["errMsg"]  = $errMsg;

                    $xml_data['response']["data"]      = '';
                    $xml_data['response']["attribute"] = $responseArr;

                    $_SESSION["accept_bid_error_code"] = 37;
                    $_SESSION["accept_bid_error_message"] = $errMsg;

                    $returnArr["errCode"] = 52;
                    $returnArr["errMsg"] = $errMsg;
                }

            } else {
                $errMsg    = 'The keyword status has been changed.Sometimes it takes upto 1 minute to reflect.Please try again after sometime.';
              //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

                $responseArr["errCode"] = 38;
                $responseArr["errMsg"]  = $errMsg;

                $xml_data['response']["data"]      = '';
                $xml_data['response']["attribute"] = $responseArr;

                $_SESSION["accept_bid_error_code"] = 38;
                $_SESSION["accept_bid_error_message"] = $errMsg;

                $returnArr["errCode"] = 51;
                $returnArr["errMsg"] = $errMsg;
            }

            $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

            header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

        }else{

            unset($_SESSION['security_type']);
            unset($_SESSION['xml_step']);
            unset($_SESSION['form_data']);
            unset($_SESSION['activity_id']);
            unset($_SESSION['redirect_url']);
            unset($_SESSION['xml_file']);
            unset($_SESSION['verify_status']);

            /*$_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";*/
            $returnArr["errCode"] = 2;

           // $xmlProcessor-> writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

            header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');
        }

    }else{

        $i         = $i + 1;
        $errMsg    = "Sorry!!! Your session is expired, Please login again.";
       // $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

        $responseArr["errCode"] = 39;
        $responseArr["errMsg"]  = $errMsg;

        $xml_data['response']["data"]      = '';
        $xml_data['response']["attribute"] = $responseArr;

        $returnArr["errCode"] = 40;
        $returnArr["errMsg"] = $errMsg;

        header('Location:'.$rootUrl.'views/prelogin/index.php');
    }

}else{

    $i         = $i + 1;
    $errMsg    = "Sorry!!! Your session is expired, Please login again.";
  //  $returnArr = setErrorStack($returnArr, $i, $errMsg, $extraArgs);

    $returnArr["errCode"] = 40;
    $returnArr["errMsg"] = $errMsg;

    $responseArr["errCode"] = 40;
    $responseArr["errMsg"]  = $errMsg;

    $xml_data['response']["data"]      = '';
    $xml_data['response']["attribute"] = $responseArr;


}

$credential = json_encode($credential);

$xml_data['credential']["attribute"] = $credential;

// create or update xml log Files
//$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

$returnArr["errStack"] = '';

echo json_encode($returnArr);
?>
