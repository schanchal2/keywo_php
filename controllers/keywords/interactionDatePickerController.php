<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); 
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}/helpers/arrayHelper.php");
//  require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
error_reporting(0);

$email = $_SESSION["email"]; 
$clientSessionId = session_id();

//For database connection
$conn = createDBConnection('dbkeywords');

if(noError($conn)){
	$conn = $conn["connection"];
}else{
	print_r("Database Error");
}

$pageUrl = "views/keywords/analytics/keyword_analytics.php";

$userCartDetails = getUserCartDetails($email, $conn);

    if(noError($userCartDetails)){
        $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
        $userCartDetails = json_decode($userCartDetails, TRUE);
    }else{
        print('Error: Fetching cart details');
        exit;
    }


    $startDate   = $_POST["fromDate"];
    $toDate      = $_POST["toDate"];



?>

<ul class="border-all">
<?php
for ($i=$startDate; $i<$toDate; $i++)
{
	$monthwiseDate = date('m_Y', strtotime("-$i month"));
    $tableName     = "daily_keyword_earnings_{$monthwiseDate}";

  $limit       = cleanQueryParameter($conn, cleanXSS($_POST["limit"])); 
  $type        = cleanQueryParameter($conn, cleanXSS($_POST["type"]));

if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit; 

  $get_total_rows = getAnalyticsDetailsCount($tableName,$conn);
  if(noError($get_total_rows))
  {
    $getTotalRows = $get_total_rows["errMsg"][0];
    $getTotalRow[]= $getTotalRows["count"];
    $getTotalRecords = array_sum($getTotalRow); 
  }

  $total_pages = ceil($getTotalRecords / $item_per_page); 

  $page_position = (string)(($page_number - 1) * $item_per_page);
  $lastpage = ceil($total_pages);

 $getAnalyticsDetails = getAnalyticsDetails($page_position,$limit,$tableName,$type,$conn);
 if(noError($getAnalyticsDetails))
    {
    	$getAnalyticsDetails = $getAnalyticsDetails["errMsg"];
      $getAnalyticsDetails[] = count($getAnalyticsDetails); //printArr($getAnalyticsDetails);
      $getTotalDataRecord = array_sum($getAnalyticsDetails);

      if ($getTotalDataRecord == 0) {
         $page_number = $page_number - 1; //if there's no page number, set it to 1
         $item_per_page = $limit;

         $page_position = (($page_number - 1) * $item_per_page);
         $lastpage = ceil($total_pages);
}

    foreach($getAnalyticsDetails as $getDetails)
    {
      
    	$keyword          = $getDetails["keyword"]; 
    	$interactionCount = $getDetails["total_count"];
    
         
?>

<li>
		
			<div class="row">
				<div class="col-md-4">
					<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e"><?php echo $keyword; ?></a></span>
				</div>

				<div class="col-md-3 text-black">
                    <?php if($interactionCount > 999) { ?>
                        <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$interactionCount} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo "{$interactionCount} {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"> <?php echo formatNumberToSort("{$interactionCount}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
                    <?php } else {?>
                        <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="javascript:;" class="display-in-block-txt-blk" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$interactionCount} {$keywoDefaultCurrencyName}"; ?>"> <?php echo formatNumberToSort("{$interactionCount}", 2); ?> <?php echo $keywoDefaultCurrencyName; ?></a></label>
                    <?php } ?>
               </div>
               
				<div class="col-md-5 pull-right text-right">
				<?php 

				$checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);
				if(noError($checkForKeywordAvailability))
				{
					$availabilityFlag = $checkForKeywordAvailability['errMsg'];
					$checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
					$highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
					$kwdAskPrice = $checkForKeywordAvailability['ask_price'];
					$kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
					$CartStatus = $checkForKeywordAvailability['status'];
					$activeBids = $checkForKeywordAvailability["active_bids"];

		   			if($availabilityFlag == 'keyword_available'){
				?>

				<div class="row pull-right">
				    <div class="col-xs-6 text-right">	
				    <?php if(in_array($keyword, $userCartDetails)){ ?>			        				    
						<input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
						<?php }else{ ?>
						<input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" /> 
						<?php } ?>                       
					</div>				
				</div>
					
                  <?php } elseif($availabilityFlag == 'keyword_not_available'){ 
                  	      $activeBids = json_decode($activeBids, true);
                  	      foreach($activeBids as $key => $bidValue)
                  	      {
                  	      	$bidValue = explode('~~', $bidValue);
                            $bidderEmail[] = $bidValue[1];
                          }

                          if(in_array($email, $bidderEmail))
                          { $bidStatus = true; }
                          else{ $bidStatus = false; }

                          if($email == $kwdOwnerId){
                          	if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                           ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                              <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                           </div>

                           <?php }else{ // set ask ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                               <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                           </div>

                           <?php }if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                               <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                           </div>

                           <?php }}else{ if($CartStatus == "sold"){ if(empty($activeBids)){ ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                           </div>

                           <?php }else{ if($bidStatus){ ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                           </div>

                           <?php }else{ ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                           </div>

                           <?php }} if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>

                           <div class="pull-left col-xs-6 col-md-12 innerMB ">
                                 <button class="btn-trading-dark"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                           </div>

                  <?php }}}?>

                  		<div class="row pull-right">
				            <div class="col-xs-6 text-right">
                   <?php } elseif($availabilityFlag == "keyword_blocked"){ ?>	
				
							<input value="Not Available" type="text" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
							<?php } ?>
							</div>
							</div>							
					

					<?php }?>


						
				</li>

				<?php }} else {  }} ?>	</ul>

<div class="row">
<div class="col-md-5 padding-left-none padding-right-none"><div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div></div>
<div class="col-md-7 padding-left-none padding-right-none">
  <div class="pagination-cont pull-right">
       <span class="span-keyword-market pull-left">Go to : &nbsp;</span>
       <input type="number" min="1" value="" placeholder="" id="gotovalue" class="span-blue-keyword-market border-all allownumericwithoutdecimal">

        <?php

            getPaginationData($lastpage, $page_number, $limit);
            function getPaginationData($lastpage, $pageno, $limit)
            {

echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
echo '<ul class="pagination">';


if ($pageno > 1) {

    $pagenum = 1;
    print('<li><a href="#"onclick=getNewInteractionData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
}

if ($pageno > 1) {
    $pagenumber = $pageno - 1;
    print('<li><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
}

if ($pageno == 1) {
    $startLoop = 1;
    $endLoop = ($lastpage < 5) ? $lastpage : 5;
} else if ($pageno == $lastpage) {
    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
    $endLoop = $lastpage;
} else {
    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
}

for ($i = $startLoop; $i <= $endLoop; $i++) {
    if ($i == $pageno) {
        print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
    } else {
        $pagenumber = $i;
        print('<li><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
    }
}
if ($pageno < $lastpage) {
    $pagenumber = $pageno + 1;
    print('<li><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

}

if ($pageno != $lastpage) {
    print('<li><a href="#" onclick=getNewInteractionData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
}


echo '</ul>';
echo '</div>';
}


?>


	</div>
</div>
</div>