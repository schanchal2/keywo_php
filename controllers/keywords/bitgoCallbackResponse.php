<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 3/4/17
 * Time: 12:34 PM
 */

if(!isset($_SESSION)){session_start();}
/*require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/walletHelper.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/header/headerModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");*/

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/walletHelper.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../helpers/transactionHelper.php");
require_once("../../models/keywords/keywordCdpModel.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");
require_once("../../models/keywords/keywordSearchModel.php");
require_once("../../models/header/headerModel.php");
require_once("../../models/keywords/acceptBidModel.php");
require_once('../smtp/Send_Mail.php');
require_once("../../backend_libraries/xmlProcessor/xmlProcessor.php");
require_once("../../models/analytics/keyword_stat_analytics.php");
error_reporting(0);

$extraArgs = '';
$stepCounter = 0;

$userIp = getClientIP();

$kwdPurchaseLog = $logPath["keywordPurchase"];
$xmlFilename = "payByBitgo.xml";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$errMsg = "Start keyword purchase process by wallet.";
$stepCounter++;
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


// Getting response from callback body
$callback_response = file_get_contents('php://input');

$data1 = json_decode($callback_response, true);

$data = $data1['extra'];

/*echo "Testing response"; 
printArr($data);die;*/


$TID = $data["hash"];
$WALLETID = $data["walletId"];
$TYPE = $data["type"];
$btcAddress = $data1["purchase_address"];
// Get Transaction details using transaction Id

if($mode == "development"){
    //$transDetails = getBitgoTransactionDetailsTest($TID,$btcAddress);
	$transDetails = getBitgoTransactionDetails($TID,$btcAddress); 
}else{
   $transDetails = getBitgoTransactionDetails($TID,$btcAddress); 
}

$chkArray = array();
$retArray = array();
$returnArr = array();

if((noError($transDetails))){
    $errMsg = "Get transaction details: success.";
    $stepCounter++;
    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
    $errorMessage = "Get transaction details.";

    $transDetails = $transDetails['errMsg'];
  
    //for test only 
    // $transDetails = array();
    // $transDetails['out'][0]['addr'] = $data["address"];
    // $transDetails['out'][0]['value']= 324000;


    /*$noOfConfirmation = $transDetails['confirmations'];*/
    if($data1['hook_invokes'] < 2){
        $noOfConfirmation = 0;
    }else{
        $noOfConfirmation = 3;
    }


    //$noOfConfirmation = $data1['errMsg']['state']['hook_invokes'];
    /*$confirmationTime = date("Y-m-d H:i:s");*/
    // Update payment tables according recived confirmations

    if($noOfConfirmation == 0)
    {

        $recieverAddress = '';
        $bitgoRecievedAmount = '';

        $kwdConn = createDbConnection('dbkeywords');

        if(noError($kwdConn)){
            $errMsg = "Databse connection on {$noOfConfirmation} confirmation: success.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
            $errorMessage = "Error getting transaction details.";

            $kwdConn = $kwdConn["connection"];
            // Get receiver address and receive amount
            if(array_key_exists('addr',$transDetails['out'][0]))
            {
                $recieverAddress = $transDetails['out'][0]['addr'];
                $bitgoRecievedAmount = $transDetails['out'][0]['value'];
            }

            //$recieverAddress = '2NCVJghQD7xRB7r9xLgYZpLDk3WKtFfCdYz';

            //Get payment info from payments table using receiver address
            $paymentInfo = getPaymentInfoByBtcAddress($recieverAddress,$kwdConn);

            if(noError($paymentInfo)){

                $time = date('Y-m-d H:i:s');
                $errMsg = "Get payment info: success | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                $stepCounter++;
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                $errorMessage = "Error getting transaction details.";

                $paymentInfo = $paymentInfo['errMsg'];
                $order_id = $paymentInfo["order_id"];
                $email = $paymentInfo["username"];
                $recieved_amount = $paymentInfo["paid_amount"];
                $amount_due = $paymentInfo['amount_due'];
                $keywords = rtrim($paymentInfo['keyword_cart']);

                /************very important for logs************/
                $xml_data["keywordBasket"]["data"] = $keywords;
                /************very important for logs************/

                $status = $paymentInfo['status'];

                // Create json of response and store in payment database
                $resArray['reciever_address'] = $recieverAddress;
                $resArray['recieved_amount'] = $bitgoRecievedAmount;
                $resArray['amount_due'] = $amount_due;
                $resArray['sender_email'] = $email;
                $resArray['confirmation'] = $noOfConfirmation;
                $resArray['transaction_id'] = $TID;

                $resArray2['confirmation'] = $resArray;
                $callback_response = json_encode($resArray2);

                $keywordsArray      = explode(',', $keywords);
                $keywordsWithTags = "";
                foreach($keywordsArray as $value){
                    if(!empty($value)){
                        $keywordsWithTags.= "#".$value.", ";
                    }
                }
                $xml .= "<request parameters='username:" . $email . ",order_id:" . $order_id . ",keywords:" . $keywords . ",total_price:" . $amount_due . ",payment_mode: Bitgo' >";
                $xml .= "</request>";

                $buyerRequireFields = "ref_email,system_mode,currencyPreference";
                $buyerDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $buyerRequireFields);

                /*echo "getUserInfo</br>";
                printArr($buyerDetails);*/
                if(noError($buyerDetails)) {
                    $errMsg = "getting buyer reference details: success.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $upc = $buyerBalanceDetails['errMsg']['currencyPreference'];
                    $upc = 'INR';

                    $getExchangeRate = getExchangeRates($upc);
                    /*echo "getExchangeRates<br>";
                    printArr($getExchangeRate);*/
                    if (noError($getExchangeRate)) {
                        $exchangeRates = $getExchangeRate['currRate'];
                    } else {
                        print("Error: getting exchange rate");
                    }

                    $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,first_buy_status,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,purchase_itd";
                    $buyerBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $buyerBalanceFields);
                    if (noError($buyerBalanceDetails)) {

                        $time = date('Y-m-d H:i:s');
                        $errMsg = "Getting user info details: success | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $firstBuyStatus = $buyerBalanceDetails["errMsg"]["first_buy_status"];

                        $bitgoRecievedAmountBTC = number_format((float)($bitgoRecievedAmount / 100000000), 8);
                        $bitgoRecievedAmountSGD = number_format((float)($bitgoRecievedAmountBTC * $exchangeRates['sgd']), 8);
                        $bitgoRecievedAmountUPC = number_format((float)($bitgoRecievedAmountBTC * $exchangeRates[strtolower($upc)]), 8);

                        $btcToUsdRate = ((float)$exchangeRates['usd'] / $exchangeRates['btc']);
                        $bitgoRecievedAmountITD = ((float)$bitgoRecievedAmountBTC * $btcToUsdRate);

                        $getUserBalance = calculateUserBalance($buyerBalanceDetails);
                        /*echo "calculateUserBalance<br>";
                        printArr($getUserBalance);*/
                        if (noError($getUserBalance)) {
                            $time = date('Y-m-d H:i:s');
                            $errMsg = "Get user balance deatils: Success | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                            $stepCounter++;
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                            $userId = $buyerBalanceDetails["errMsg"]["_id"];

                            $cartKeywordsDetails = getUserTotalCartPrice($kwdConn, $email);
                            /*echo "getUserTotalCartPrice<br>";
                            printArr($cartKeywordsDetails);*/
                            if (noError($cartKeywordsDetails)) {
                                $time = date('Y-m-d H:i:s');
                                $errMsg = "Get user cart details: Success | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $total_cart_price_new = number_format((float)$cartKeywordsDetails['errMsg']['total_cart_price'], 2);

                                $totalAvailableBal = (float)($userBalance + $bitgoRecievedAmountITD);

                                //Update payment table on 0 confirmation
                                $update = updatePaymentDetails($kwdConn, $order_id, $TID, $status, $recieverAddress, $userIp, $email, $amount_due, $bitgoRecievedAmountBTC, $bitgoRecievedAmountITD, $totalAvailableBal, $noOfConfirmation, $callback_response,$keywords);

                                if (noError($update)) {
                                    $errMsg = "update payments table: success | payment status => {$update['status']}, no_of_confirmation => {$noOfConfirmation}, bitgo_received_amount = {$bitgoRecievedAmountBTC}, payment_transaction_id = {$TID}, payment_address = {$recieverAddress}, request_ip_address = {$userIp}, customer_email = {$email}.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    $errorMessage = "Update payment table successful after {$noOfConfirmation} confirmation.";

                                    $returnArr["errCode"] = -1;
                                    $returnArr["errMsg"] = $errorMessage;
                                    http_response_code(200);
                                } else {
                                    $errMsg = "update payments table: success | payment status => {$update['status']}, no_of_confirmation => {$noOfConfirmation}, bitgo_received_amount = {$bitgoRecievedAmountBTC}, payment_transaction_id = {$TID}, payment_address = {$recieverAddress}, request_ip_address = {$userIp}, customer_email = {$email}.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    $errorMessage = "Update payment table successful after {$noOfConfirmation} confirmation.";

                                    $returnArr["errCode"] = 2;
                                    $returnArr["errMsg"] = $errorMessage;

                                    http_response_code(400);
                                }

                            } else{
                                $time = date('Y-m-d H:i:s');
                                $errMsg = "Get user cart details: failed | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                $errorMessage = "Error getting user cart details.";

                                $returnArr["errCode"] = 2;
                                $returnArr["errMsg"] = $errorMessage;
                                 http_response_code(400);
                            }


                        } else {
                            $time = date('Y-m-d H:i:s');
                            $errMsg = "Get user balance deatils: failed | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                            $stepCounter++;
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            $errorMessage = "Error getting user balance deatils.";

                            $returnArr["errCode"] = 2;
                            $returnArr["errMsg"] = $errorMessage;
                            //Error getting user balance
                            $userBalance = 0;
                            http_response_code(400);
                        }
                    } else {
                        $time = date('Y-m-d H:i:s');
                        $errMsg = "Getting user info details: failed | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                        $errorMessage = "Error getting user info details.";

                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"] = $errorMessage;
                        http_response_code(400);
                        //Error getting user info
                    }
                }else{
                    $errMsg = "getting buyer reference details failed.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                    $errorMessage = "Error getting buyer reference details.";

                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = $errorMessage;
                    http_response_code(400);
                }

            } else {
                $time = date('Y-m-d H:i:s');
                $errMsg = "Get payment info: success | payment status : {$paymentInfo['status']}, timestamp: {$time} .";
                $stepCounter++;
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                $errorMessage = "Error getting transaction details.";

                $returnArr['errCode'] = 2;
                $returnArr['errMsg']  = $errMsg;

                http_response_code(400);
            }

        } else {
            $errMsg = "Databse connection on {$noOfConfirmation} confirmation: failed.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
            $errorMessage = "Error getting transaction details.";

            $returnArr['errCode'] = 2;
            $returnArr['errMsg']  = $errMsg;

            http_response_code(400);
        }

    

    } else if($noOfConfirmation == 3){

        $recieverAddress = '';
        $bitgoRecievedAmount = '';

        $kwdConn = createDbConnection('dbkeywords');

        if(noError($kwdConn)){
            $xml .= "<step3 title='Bitgo payment callback' >3. Databse connection on 3rd confirmation: 'success'";
            $xml .= "</step3>\n";

            $errMsg = "Databse connection on 3rd confirmation: success.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $kwdConn = $kwdConn["connection"];
            // Get receiver address and receive amount
            if(array_key_exists('addr',$transDetails['out'][0]))
            {
                $recieverAddress = $transDetails['out'][0]['addr'];
                $bitgoRecievedAmount = $transDetails['out'][0]['value'];
            }


            // Get payment info from payments table using receiver address
            $paymentInfo = getPaymentInfoByBtcAddress($recieverAddress,$kwdConn);

            if(noError($paymentInfo)) {
                $errMsg = "Getting payment details from payment table: Success, payment status : {$paymentInfo['status']} | timestamp : ".date('Y-m-d H:i:s').".";
                $stepCounter++;
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $paymentInfo = $paymentInfo['errMsg'];

                if($paymentInfo['no_of_confirmation'] < 3){
                    $errMsg = "no of confirmation:".$paymentInfo['no_of_confirmation'];
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


                    $order_id = $paymentInfo["order_id"];
                    $email = $paymentInfo["username"];
                    $recieved_amount = $paymentInfo["paid_amount"];
                    $amount_due = $paymentInfo['amount_due'];
                    $status = $paymentInfo['status'];
                    $keywords = rtrim($paymentInfo['keyword_cart']);

                    /************very important for logs************/
                    $xml_data["keywordBasket"]["data"] = $keywords;
                    /************very important for logs************/
                    $meta_data = $paymentInfo['meta_details'];
                    // Create json of response and store in payment database
                    $newArray['reciever_address'] = $recieverAddress;
                    $newArray['recieved_amount'] = $bitgoRecievedAmount;
                    $newArray['amount_due'] = $amount_due;
                    $newArray['sender_email'] = $email;
                    $newArray['confirmation'] = $noOfConfirmation;
                    $newArray['transaction_id'] = $TID;

                    $resArray['total_payment_data'] = json_decode($paymentInfo["total_payment_data"], true);
                    $resArray2['confirmation'][] = $resArray['total_payment_data']['confirmation'];
                    $resArray2['confirmation'][] = $newArray;

                    //echo $callback_response = json_encode($resArray2);
                    //$callback_response = $resArray2;

                    $keywordsArray = explode(',', $keywords);
                    $keywordsWithTags = "";
                    foreach ($keywordsArray as $value) {
                        if (!empty($value)) {
                            $keywordsWithTags .= "#" . $value . ", ";
                        }
                    }

                    $buyerRequireFields = "ref_email,system_mode,currencyPreference";

                    $buyerDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $buyerRequireFields);
                    if(noError($buyerDetails)) {

                        $errMsg = "getting buyer reference details success.";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $upc = $buyerDetails['errMsg']['currencyPreference'];
                        $buyerRefferedBy = $buyerDetails["errMsg"]["ref_email"];
                        $upc = 'INR';

                        $getExchangeRate = getExchangeRates(strtolower($upc));
                        /*echo "getExchangeRates</br>";
                        printArr($getExchangeRate);*/
                        if (noError($getExchangeRate)) {
                            $exchangeRates = $getExchangeRate['currRate'];
                        } else {
                            print("Error: getting exchange rate");
                        }

                        $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,first_buy_status,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,purchase_itd";
                        $buyerBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $buyerBalanceFields);
                        /*echo "getUserInfo 1st</br>";
                        printArr($buyerBalanceDetails); die;*/
                        if (noError($buyerBalanceDetails)) {

                            $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                            $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];
                            $firstBuyStatus = $buyerBalanceDetails["errMsg"]["first_buy_status"];

                            $bitgoRecievedAmountBTC = number_format((float)($bitgoRecievedAmount / 100000000), 8);
                            $bitgoRecievedAmountSGD = number_format((float)($bitgoRecievedAmountBTC * $exchangeRates['sgd']), 8);
                            $bitgoRecievedAmountUPC = number_format((float)($bitgoRecievedAmountBTC * $exchangeRates[$upc]), 8);

                            $btcToUsdRate = ((float)$exchangeRates['usd'] / $exchangeRates['btc']);
                            $bitgoRecievedAmountITD = ((float)$bitgoRecievedAmountBTC * $btcToUsdRate);
                            $bitgoRecievedAmountITD = number_format($bitgoRecievedAmountITD, 4);
                            $getUserBalance = calculateUserBalance($buyerBalanceDetails);
                            if (noError($getUserBalance)) {
                                $errMsg = "getting buyer available balance details success.";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                                $userId = $buyerBalanceDetails["errMsg"]["_id"];

                                $totalAvailableBal = (float)($userBalance + $bitgoRecievedAmountITD);


                                /* get referrer details */
                                // get referrer details with available balance
                                $referrerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,first_buy_status,search_affiliate_earnings,purchases";
                                $referrerBalanceDetails = getUserInfo($buyerRefferedBy, $walletURLIP . 'api/v3/', $referrerBalanceFields);
                                /*echo "getUserInfo</br>";
                                printArr($referrerBalanceDetails); die;*/
                                if(noError($referrerBalanceDetails)){

                                    $errMsg = "getting referral id of referrer user success.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $referrerID = $referrerBalanceDetails['errMsg']['_id'];
                                    $referrerFirstName = $referrerBalanceDetails["errMsg"]["first_name"];
                                    $referrerLastName = $referrerBalanceDetails["errMsg"]["last_name"];

                                }else{

                                    $errMsg = "getting referral id of referrer user failed.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $referrerID = '';
                                }

                                $cartKeywordsDetails = getUserTotalCartPrice($kwdConn, $email);
                                if (noError($cartKeywordsDetails)) {
                                    $errMsg = "getting user total cart price: success.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    $errorMessage = "getting user total cart price success.";

                                    $returnArr['errCode'] = -1;
                                    $returnArr['errMsg']  = $errMsg;

                                    $total_cart_price_new = number_format((float)$cartKeywordsDetails['errMsg']['total_cart_price'], 2);

                                    /* Credit ITD to user account */
                                    $creditUserItd = creditUserItdPurchase($email,$userId,$recieverAddress,$bitgoRecievedAmountITD,$userIp,json_encode($exchangeRates),$meta_data,$keywords);
                                    /*echo "creditUserItdPurchase<br>";*/
                                   
                                    if(noError($creditUserItd)) {
                                        $errMsg = "Deposit purchase ITD to user wallet: success | payment status => {$status}, Email = {$email}, Address = {$recieverAddress}, UserIp = {$userIp}, recieved_ITD = {$bitgoRecievedAmountITD}.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        //Update payment table on 3rd confirmation
                                        $update = updatePaymentDetails($kwdConn,$order_id,$TID,$status,$recieverAddress,$userIp,$email,$amount_due,$bitgoRecievedAmountBTC,$bitgoRecievedAmountITD,$totalAvailableBal,$noOfConfirmation,$callback_response,$keywords);
                                       // echo "updatePaymentDetails<br>";

                                        if(noError($update)){
                                            $errMsg = "update payment details after {$noOfConfirmation}rd confirmation: success, update payments table | no_of_confirmation : {$noOfConfirmation}, purchase_itd :{$bitgoRecievedAmountITD},total_cart_price_new => {$total_cart_price_new},payment_transaction_id:{$TID} ,status:{$update['status']} ,payment_address:{$recieverAddress} .";
                                            $stepCounter++;
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                            $errorMessage = "update payment details success.";

                                            $returnArr['errCode'] = -1;
                                            $returnArr['errMsg']  = $errMsg;

                                            $payment_status = $update['status'];

                                            $paramArr = array(
                                                'conn' => $kwdConn,
                                                'orderId' => $order_id,
                                                'transId' => $TID,
                                                'paymentAdd' => $recieverAddress,
                                                'status' => $payment_status,
                                                'ip' => $userIp,
                                                'buyerEmail' => $email,
                                                'buyerId' => $userId,
                                                'amountDue' => $amount_due,
                                                'recievedAmount' => $bitgoRecievedAmountITD,
                                                'totalAvailableAmount' => $totalAvailableBal,
                                                'keywords' => $keywords,
                                                'exchangeRate' => $exchangeRates,
                                                'buyerRefferedBy' => $buyerRefferedBy,
                                                'referrerID' => $referrerID,
                                                'firstBuyStatus' => $firstBuyStatus,
                                                'referrerFirstName' => $referrerFirstName,
                                                'referrerLastName' => $referrerLastName,
                                                'buyerLastName' => $buyerLastName,
                                                'buyerFirstName' => $buyerFirstName,
                                                'keywordWithTags' => $keywordsWithTags
                                            );


                                            if(($payment_status === 'overpaid' || $payment_status === 'exact') && $keywords != 'itd_purchase_only'){


                                                if ($totalAvailableBal >= $total_cart_price_new) {
                                                    $errMsg = "user available balance sufficient for purchase: success, total_available_balance => {$totalAvailableBal} | total_cart_price_new => {$total_cart_price_new}.";
                                                    $stepCounter++;
                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    $errorMessage = "user available balance sufficient for purchase.";


                                                    //Purchase new keyword
                                                    $purchaseKwd = purchaseNewKeyword($paramArr,$stepCounter,$xml_data,$blanks);
                                                   // echo "purchaseNewKeyword</br>";
                                                    //printArr($purchaseKwd);
                                                    if(noError($update)){
                                                        $errMsg = "User purchase keyword successfully: success.";
                                                        $stepCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        $errorMessage = "User purchase keyword successfully.";

                                                        $returnArr['errCode'] = -1;
                                                        $returnArr['errMsg']  = $errorMessage;

                                                        http_response_code(200);

                                                    } else {
                                                        $errMsg = "User purchase keyword: failed.";
                                                        $stepCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                        $errorMessage = "Error while purchase keyword.";

                                                        $returnArr['errCode'] = 2;
                                                        $returnArr['errMsg']  = $errMsg;

                                                        http_response_code(400);

                                                    }

                                                } else {
                                                    $errMsg = "user available balance in-sufficient for purchase, total_available_balance => {$totalAvailableBal} | total_cart_price_new => {$total_cart_price_new}.";
                                                    $stepCounter++;
                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                    $errorMessage = "user available balance in-sufficient for purchase.";

                                                    $returnArr['errCode'] = 2;
                                                    $returnArr['errMsg']  = $errMsg;
                                                    http_response_code(400);
                                                }



                                                /*if($payment_status === 'overpaid'){
                                                    //return extra ITD to user wallet
                                                    $refunAmount = refundOrDepositAmountToUser($paramArr,$stepCounter,$xml,$blanks);

                                                }*/

                                            } else if($payment_status === 'mispaid'){

                                                $errMsg = "user has mispaid amount for purchase, total_available_balance => {$totalAvailableBal} | total_cart_price_new => {$total_cart_price_new}.";
                                                $stepCounter++;
                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                                $errorMessage = "user pay mis-paid amount for purchase.";

                                                $returnArr['errCode'] = 2;
                                                $returnArr['errMsg']  = $errorMessage;
                                                http_response_code(200);
                                            }

                                            if(($payment_status === 'overpaid' || $payment_status === 'exact') && $keywords == 'itd_purchase_only'){
                                                //send notification to ITD sender.  
                                                $email = $email;
                                                $mailSubject = sprintf($notificationEmails['purchase_by_paypal']['sub']);
                                                $emailBody = sprintf($notificationEmails['purchase_by_paypal']['body'], ucfirst($buyerFirstName),$bitgoRecievedAmountITD.' '.$keywoDefaultCurrencyName);
                                                $firstName = $buyerFirstName;
                                                $lastName = $buyerLastName;
                                                $userId = $userId;
                                                $notificationBody = sprintf($notificationMessages['purchase_by_paypal']);
                                                $preferenceCode = 2;
                                                $category = "wallet";

                                                $res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

                                                http_response_code(200);
                                            }                                        

                                        } else {
                                            $errMsg = "update payment details after 3rd confirmation: failed, update payments table | no_of_confirmation : {$noOfConfirmation}, paid_amount:{$bitgoRecievedAmount},total_cart_price_new => {$total_cart_price_new},payment_transaction_id:{$TID} ,status:{$update['status']} ,payment_address:{$recieverAddress} .";
                                            $stepCounter++;
                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                            $errorMessage = "update payment details failed.";

                                            $returnArr['errCode'] = 2;
                                            $returnArr['errMsg']  = $errorMessage;
                                            http_response_code(400);
                                        }

                                    } else {
                                        $errMsg = "Deposit purchase ".$keywoDefaultCurrencyName." to user wallet: failed | payment status => {$param['status']}, Email = {$email}, Address = {$payment_addresss}, UserIp = {$userIp}, recieved_ITD = {$recieved_amount}.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                        $errorMessage = "Error in deposit purchase ".$keywoDefaultCurrencyName." to user wallet failed.";

                                        $returnArr['errCode'] = 2;
                                        $returnArr['errMsg']  = $errorMessage;
                                        http_response_code(400);
                                    }

                                } else {
                                    $errMsg = "getting user total cart price: failed.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                    $errorMessage = "Error getting user total cart price.";

                                    $returnArr['errCode'] = 2;
                                    $returnArr['errMsg']  = $errorMessage;
                                    http_response_code(400);
                                }

                            } else {
                                $errMsg = "Calculating buyer available balance: failed.";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                $errorMessage = "Error calculating buyer available balance.";

                                $userAvailableBalance = "Error";

                                $returnArr['errCode'] = 2;
                                $returnArr['errMsg']  = $errorMessage;
                                http_response_code(400);
                            }

                        } else {
                            $errMsg = "getting buyer available balance details failed.";
                            $stepCounter++;
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            $errorMessage = "Error getting buyer available balance details.";

                            http_response_code(400);
                        }

                    } else {
                        $errMsg = "getting buyer reference details failed.";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                        $errorMessage = "Error getting buyer reference details.";

                        $returnArr['errCode'] = 2;
                        $returnArr['errMsg']  = $errorMessage;
                        http_response_code(400);
                    }
                }else{
                    $errMsg = "this transaction is already completed";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $returnArr['errCode'] = 2;
                    $returnArr['errMsg']  = $errMsg;
                    http_response_code(200);
                }
                

            } else {
                $errMsg = "Getting payment details from payment table: failed, payment status : {$paymentInfo['status']} | timestamp : ".date('Y-m-d H:i:s').".";
                $stepCounter++;
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                $errorMessage = "Error getting payment details from payment.";

                $returnArr['errCode'] = 2;
                $returnArr['errMsg']  = $errorMessage;
                http_response_code(400);
            }
        } else {
            $errMsg = "Databse connection on {$noOfConfirmation}rd confirmation: failed.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
            $errorMessage = "Error in databse connection on {$noOfConfirmation}rd confirmation.";

            $returnArr['errCode'] = 2;
            $returnArr['errMsg']  = $errorMessage;
            http_response_code(400);
        }

    }
} else {
    $errMsg = "Get transaction details: failed.";
    $stepCounter++;
    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
    $errorMessage = "Error getting transaction details.";

    $returnArr['errCode'] = 2;
    $returnArr['errMsg']  = $errMsg;
    http_response_code(400);
}


$xml_resp_atrr['response'] = json_encode($returnArr);

$xml_data['response']["data"] ="";
$xml_data['response']["attribute"] =$xml_resp_atrr;

$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);

function purchaseNewKeyword($param, &$stepCounter,&$xml_data,$blanks)
{
    global $communityPoolUser, $keywoUser, $communityPoolUserId, $keywoUserId;
    $retArray = array();
    $returnArr = array();
    $checkArr = array();
    $extraArgs = array();

    //Get required values
    $kwdDbConn = $param['conn'];
    $order_id = $param['orderId'];
    $transId = $param['transId'];
    $paymentAddress = $param['paymentAdd'];
    $payment_status = $param['status'];
    $originIp = $param['ip'];
    $email = $param['buyerEmail'];
    $buyerID = $param['buyerId'];
    $amount_due = $param['amountDue'];
    $recieved_amount = $param['recievedAmount'];
    $totalAvailable_amount = $param['totalAvailableAmount'];
    $keywordBasket = $param['keywords'];
    $keywordsWithTags = $param['keywordWithTags'];
    $exchangeRate = $param['exchangeRate'];
    $buyerRefferedBy = $param['buyerRefferedBy'];
    $referrerID = $param['referrerID'];
    $firstBuyStatus = $param['firstBuyStatus'];
    $referrerFirstName = $param['referrerFirstName'];
    $referrerLastName = $param['referrerLastName'];
    $buyerFirstName = $param['buyerFirstName'];
    $buyerLastName = $param['buyerLastName'];

    if($totalAvailable_amount >= $amount_due) {

        $errMsg = "amount paid match : success, payment status => {$payment_status}.";
        $stepCounter++;
        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $refund_amount = number_format((float)($totalAvailable_amount - $amount_due), 8);
        $final_amount = number_format((float)($recieved_amount - $refund_amount), 8);

        //Get Presale Status
        $presaleStatus = getPresaleStatus($kwdDbConn);

        if(noError($presaleStatus)){
            $errMsg = "getting presale status: success.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $presaleStatus = $presaleStatus["errMsg"]["presale_status"];
            if ($presaleStatus != 0) {
                $errMsg = "Presale status is active.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                /* Get user admin settings from keyword admin */
                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
               /* echo "getAdminSettingsFromKeywordAdmin</br>";
                printArr($adminSettings);die;*/
                if(noError($adminSettings))
                {
                    $errMsg = "getting admin settings of buyer user success.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];
                    $affiliateCommision = $adminSettings["data"]["affiliate_earning_percent"];


                    $reqArray = array("buyer_id" => urlencode($email), "buyerBalance" => urlencode($totalAvailable_amount), "first_buy_status" => urlencode($firstBuyStatus), "first_buy_percent" => urlencode($firstBuyPercent), "buyerReferral" => urlencode($buyerRefferedBy), "affiliate_percent" => urlencode($affiliateCommision),);
                    $xml_data["parameters"]["data"] = "";
                    $xml_data['parameters']["attributes"]= $reqArray;

                    //Fetch keywords into an array
                    $keywordBasket = strip_tags(trim($keywordBasket));
                    $keywordBasket = preg_replace("/\s\s([\s]+)?/", " ", $keywordBasket);
                    $keywords      = explode(',', $keywordBasket);
                    $keywords = array_filter($keywords);

                    //loop through keywords
                    $keywords_i = 1;

                    $errMsg = "Purchase keyword start using loop.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    foreach ($keywords as $keyword) {
                        $keywordCounter = 0;
                        //check if keyword is set and not empty
                        if (isset($keyword) && !empty($keyword)) {
                            // Clean keyword
                            //$keyword = strip_tags($keyword);
                            //$keyword = strtolower($keyword);

                            $logmd5kwd=md5($keyword);

                            $errMsg = "start purchase keyword {$keyword}.";
                            $keywordCounter++;
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                            //Get Keyword Ownerhip Table Name
                            $tableName = getKeywordOwnershipTableName($keyword);

                            $errMsg = "get keyword ownership tablename for keyword {$keyword}, tablename {$tableName}.";
                            $keywordCounter++;
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                            /*echo "$tableName</br>";
                            printArr($adminSettings);*/
                            //Get current Active Slab
                            $getCurrActiveSlab        = getCurrentActiveSlab($kwdDbConn);

                            $currentSlabId = $getCurrActiveSlab["errMsg"]["id"];

                            $errMsg = "get current active slab rate for {$keyword}, Slabrate id {$currentSlabId}.";
                            $keywordCounter++;
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                            /*echo "getCurrentActiveSlab</br>";
                            printArr($getCurrActiveSlab);*/

                            $kwd_price_USD     = number_format($getCurrActiveSlab["errMsg"]["ITD_Price"],8);
                            //convert from USD amount to USD
                            // get currency exchange rate of usd, sgd and btc
                            $getExchangeRate = getExchangeRates('BTC');
                            /*echo "getExchangeRates</br>";
                            printArr($getExchangeRate);die;*/
                            if (noError($getExchangeRate)) {
                                $getExchangeRate = $getExchangeRate['currRate'];

                                $errMsg = "get current currency exchange rate ". json_encode($getExchangeRate);
                                $keywordCounter++;
                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                            }

                            $kwd_sold      = $getCurrActiveSlab["errMsg"]["kwd_sold"];

                            if ($firstBuyStatus) {
                                $firstBuyCashBack = number_format((float)$kwd_price_USD * ($firstBuyPercent / 100),8,'.','');

                                $errMsg = "calculate first buy cashback amount {$firstBuyCashBack}.";
                                $keywordCounter++;
                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                            }

                            $trans = startTransaction($kwdDbConn);
                            /*echo "startTransaction</br>";
                            printArr($trans);*/
                            if (noError($trans)) {
                                $errMsg = "start transaction success.";
                                $keywordCounter++;
                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                //transaction started
                                //Insert into ownership tables
                                //Insert into alphabet tables
                                $getOwnershipFromCart = getKeywordOwnershipDetailsInCart($kwdDbConn,$keyword);
                                $ownershipId = $getOwnershipFromCart["errMsg"]["buyer_id"];
                                $ownershipStatus = $getOwnershipFromCart["errMsg"]["status"];
                                if($ownershipId == $email && $ownershipStatus =="inCart"){

                                    $errMsg = "get keyword ownership details from cart success, updating ownership details.";
                                    $keywordCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                    $query  = "UPDATE " . cleanQueryParameter($kwdDbConn, $tableName) . " SET purchase_timestamp=NOW(),status='sold',claim_status=1,kwd_price='" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "',payment_mode='ITD',ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $email) . "' ;";
                                }else{

                                    $query = "insert into " . $tableName . " (`buyer_id`,`purchase_timestamp`,`keyword`,`status`,`claim_status`,`kwd_price`,`currency`,ownership_expiry_time) values( '";
                                    $query .= cleanQueryParameter($kwdDbConn, $email) . "'";
                                    $query .= ", NOW() ";
                                    $query .= ",'" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                    $query .= ",'sold'";
                                    $query .= ",1";
                                    $query .= ",'" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "'";
                                    $query .= ",'" . cleanQueryParameter($kwdDbConn, 'ITD') . "'";
                                    $intervals='INTERVAL 1 YEAR';
                                    $query .= ",DATE_ADD(`purchase_timestamp`, $intervals))";

                                    $errMsg = "get keyword ownership details from cart failed, inserting new ownership details.";
                                    $keywordCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                }

                                $result = runQuery($query, $kwdDbConn);

                                if (noError($result)) {
                                    $errMsg = "inserting ownership details success.";
                                    $keywordCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                    //Update Pool Distribution
                                    $addToRecentSold = addToRecentSold($kwdDbConn, $keyword, $kwd_price_USD, $email);
                                    /*echo "addToRecentSold</br>";
                                    printArr($addToRecentSold);*/
                                    if (noError($addToRecentSold)) {
                                        $errMsg = "keyword add to recent sold success.";
                                        $keywordCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                        $kwd_sold = $kwd_sold + 1;

                                        /* Delete bitgo payment record from payments table after buy keyword from wallet */
                                        $delrecord = deleteBitgoPaymentRecord($kwdDbConn,$keyword,'bitgo');
                                        if(noError($delrecord)){
                                            $errMsg = "Remove bitgo payment record from table success.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                        }else{
                                            $errMsg = "Remove bitgo payment record from table failed.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                        }

                                    } else {
                                        $errMsg = "keyword add to recent sold failed.";
                                        $keywordCounter++;
                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                    }

                                    //Update Keyword Sold Count Of the current slab
                                    $query  = "UPDATE `first_purchase_slabs` SET `kwd_sold`='" . cleanQueryParameter($kwdDbConn, $kwd_sold) . "' WHERE `id`='" . cleanQueryParameter($kwdDbConn, $currentSlabId) . "'";

                                    $result = runQuery($query, $kwdDbConn);
                                    /*echo "runQuery</br>";
                                    printArr($result);*/
                                    if (noError($result)) {
                                        $errMsg = "Increase keyword sold count in purchase slab success.";
                                        $keywordCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                        $noOfVouchers = 1;
                                        $oneVoucher   = getVoucherForSaleBySlabId($currentSlabId, $noOfVouchers, $kwdDbConn);
                                        $voucher_id   = $oneVoucher["errMsg"][0]["voucher_id"];
                                        $voucher_code = $oneVoucher["errMsg"][0]["voucher_code"];

                                        //update mykeyword table
                                        $purchase_timestamp      = date("j F, Y");
                                        $payment_mode            = "bitgo";
                                        $updatemykeyword_details = updateMykeywordDetails($kwdDbConn, $email, $purchase_timestamp, $keyword, $kwd_price_USD, $voucher_code, $payment_mode, $fund_amount);

                                        /*echo "updateMykeywordDetails</br>";
                                        printArr($updatemykeyword_details);*/
                                        if (noError($updatemykeyword_details)) {
                                            $errMsg = "update my keyword details success.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                        } else {
                                            $errMsg = "update my keyword details failed.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                        }
                                        //update used status for voucher_code
                                        $query  = "UPDATE presale_vouchers SET voucher_status='used' WHERE voucher_id='" . cleanQueryParameter($kwdDbConn, $voucher_id) . "';";

                                        $result = runQuery($query, $kwdDbConn);
                                        /*echo "runQuery</br>";
                                        printArr($result);*/

                                        if (noError($result)) {
                                            $errMsg = "update presale voucher status as used : success.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                            //update keyword ownership table for voucher_code
                                            $query  = "UPDATE " . cleanQueryParameter($kwdDbConn, $tableName) . " SET voucher_allocated='" . cleanQueryParameter($kwdDbConn, $voucher_id) . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $email) . "' ;";
                                            $result = runQuery($query, $kwdDbConn);
                                            /*echo "runQuery</br>";
                                            printArr($result);*/

                                            if (noError($result)) {
                                                $errMsg = "update voucher status in ownership details  : success, voucher code {$voucher_code}.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                //Clear Keyword From User Cart
                                                $result = removeKeywordFromCart(html_entity_decode($keyword), $email, $kwdDbConn, $keywordCounter, $xml_data);

                                                /*echo "removeKeywordFromCart</br>";
                                                printArr($result);*/
                                                if (noError($result)) {
                                                    $errMsg = "removed keyword from cart : success, keyword {$keyword}.";
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                    //update first buy information
                                                    if ($firstBuyStatus) {
                                                        /*echo "update first buy information</br>";
                                                        printArr($firstBuyStatus);*/
                                                        $errMsg = "user first buy status true .";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                        if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {

                                                            $errMsg = "buyer referred by {$buyerRefferedBy} .";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            $firstBuyUpdateStatus = null;
                                                            $result               = updateFirstBuyStatus($buyerID, '0');
                                                            /*echo "updateFirstBuyStatus</br>";
                                                            printArr($result);*/
                                                            $response             = json_encode($result);
                                                            if (noError($result)) {
                                                                $errMsg = "update buyer first buy preference : success .";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                //success updating first buy status
                                                                $firstBuyUpdateStatus = 1;

                                                            } else {
                                                                $errMsg = "update buyer first buy preference : success .";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                //error updating first buy status
                                                                $firstBuyUpdateStatus = 0;
                                                            }
                                                            ////
                                                        } else {
                                                            $errMsg = "buyer referred by {$buyerRefferedBy} .";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                        }
                                                    } else {
                                                        $errMsg = "user first buy status false .";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                    }

                                                    if (($firstBuyStatus == 1 && $firstBuyUpdateStatus == 1) || $firstBuyStatus == 0) {
                                                        $errMsg = "first buy status match : success.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                        /* Deduct user balance */
                                                        $amount = number_format(($kwd_price_USD - $firstBuyCashBack),8);
                                                        $result   = creditUserEarning($buyerID, $amount, 'purchase', 'add');
                                                        /*echo "Deduct user balance creditUserEarning</br>";
                                                        printArr($result);*/
                                                        $response = json_encode($result);
                                                        if (noError($result)) {
                                                            $errMsg = "credit buyer purchase field : success.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            $reqArray = array("buyer_id" => urlencode($email), "keyword_price" => urlencode($amount));
                                                            $xml_data["parameters"]["data"] = "";
                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                            //successfully deducted user balance

                                                            // add to latest trade table
                                                            //$addToLatestTrade  = addToLatestTrade($conn,$keyword,$email,$kwd_price);
                                                            //commit transaction

                                                            $result = commitTransaction($kwdDbConn);
                                                            /*echo "Deduct user balance commitTransaction</br>";
                                                            printArr($result);*/
                                                            if (noError($result)) {
                                                                $errMsg = "commit transaction : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            } else {

                                                                $errMsg = "commit transaction : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                            }

                                                            // Enable database default autocommit functionality
                                                            $result = mysqli_autocommit($kwdDbConn, TRUE);

                                                            if (!$result) {
                                                                $returnArr["errCode"] = 7;
                                                                $returnArr["errMsg"] = "Could not start transaction: ".mysqli_error($kwdDbConn);
                                                            } else {
                                                                $returnArr["errCode"] = -1;
                                                                $returnArr["errMsg"] = "Transaction started";
                                                            }
                                                            //reset user stats
                                                            $result   = deleteUserStatsFromRevenueTable($kwdDbConn, $keyword);
                                                            /*echo "deleteUserStatsFromRevenueTable</br>";
                                                            printArr($result);*/
                                                            $response = json_encode($result);
                                                            if (noError($result)) {
                                                                $errMsg = "remove user stats from revenue table: success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                $reqArray = array("Response" => urlencode(json_decode($result)));
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                            } else {
                                                                $errMsg = "remove user stats from revenue table: failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                $reqArray = array("Response" => urlencode(json_decode($result)));
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;
                                                            }

                                                            //update keywords purchase amount to community pool user
                                                            $kwdPurchaseAmount = number_format(($kwd_price_USD-$firstBuyCashBack)/2,8);
                                                            $exchangeRateInJson = json_encode($getExchangeRate);
                                                            //$result   = creditKeywordsPoolBalnceWalletAPI($poolAmount);
                                                            $result   = creditUserEarning($communityPoolUserId, $kwdPurchaseAmount, 'purchase', 'add');
                                                            /*echo "keywords purchase amount to community pool user creditUserEarning</br>";
                                                            printArr($result);*/
                                                            $response = json_encode($result);
                                                            if (noError($result)) {
                                                                //success updating keywords purchase amount to community pool user
                                                                $errMsg = "update keyword purchase amount to community pool : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($kwdPurchaseAmount));
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                $result   = creditUserEarning($keywoUserId, $kwdPurchaseAmount, 'purchase', 'add');
                                                                /*echo "updating keywords purchase amount to keywo user creditUserEarning</br>";
                                                                printArr($result);*/
                                                                if(noError($result)){
                                                                    //success updating keywords purchase amount to keywo pool user
                                                                    $errMsg = "update keyword purchase amount to keywo : success.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                    $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($kwdPurchaseAmount));
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                    /* Set variables for make insert user transaction first buy keyword */
                                                                    $recipientEmail = $keywoUser;
                                                                    $recipientUserId = $keywoUserId;
                                                                    $senderEmail = $communityPoolUser;
                                                                    $senderUserId = $communityPoolUserId;
                                                                    $type = 'keyword_purchase';
                                                                    $paymentMode = 'ITD';
                                                                    $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $email, "commision" => 0, "buyer_id" => $email, "raise_on" => 'first buy');

                                                                    $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwdPurchaseAmount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                                                     /*echo "updating keywords purchase amount to keywo user insertUserTransaction</br>";
                                                                     printArr($result);*/
                                                                    $response = json_encode($result);
                                                                    if(noError($result)){
                                                                        $errMsg = "transaction for keyword purchase amount to keywo : success.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                        $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                        $xml_data["parameters"]["data"] = "";
                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                    }else{
                                                                        $errMsg = "transaction for keyword purchase amount to keywo : failed.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                        $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                        $xml_data["parameters"]["data"] = "";
                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                    }

                                                                }else{
                                                                    //error updating keywords purchase amount to keywo pool user
                                                                    $errMsg = "update keyword purchase amount to community pool : failed.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                }

                                                                // Update search trade maintenance fees in pool
                                                                $pool = feesForPoolMaintenance($kwdPurchaseAmount,$keyword,$email,$originIp,$exchangeRateInJson,$kwdDbConn);
                                                                $poolResponse = json_encode($pool);

                                                                if(noError($pool))
                                                                {
                                                                    $errMsg = "update keywo maintainance fees : success.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                    $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                }else{
                                                                    $errMsg = "update keywo maintainance fees : failed.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                }

                                                            } else {
                                                                //error updating keywords purchase amount to community pool user
                                                                $errMsg = "update keyword purchase amount to community pool : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($kwdPurchaseAmount), 'buyer_id' => $email);
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;
                                                            }

                                                            if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {

                                                                $errMsg = "buyer referred id match : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                if ($firstBuyStatus) {

                                                                    $errMsg = "buyer first buy status active.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                    $affiliateEarnings = number_format((($kwd_price_USD - $firstBuyCashBack) * $affiliateCommision)/100,8);
                                                                    //$affiliateEarnings=number_format((float)$affiliateEarnings,8,".","");

                                                                    $reqArray = array("affiliateEarnings" => urlencode($affiliateEarnings), 'keyword_price' => urlencode($kwd_price_USD), 'buyer_id' => $email, 'first_buy_amount' => $firstBuyCashBack, 'affiliateComm'=>$affiliateCommision);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                } else {

                                                                    $errMsg = "buyer first buy status inactive.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $affiliateEarnings = number_format(($kwd_price_USD * $affiliateCommision)/100,8);
                                                                    //$affiliateEarnings=number_format((float)$affiliateEarnings,8,".","");

                                                                    $reqArray = array("affiliateEarnings" => urlencode($affiliateEarnings), 'keyword_price' => urlencode($kwd_price_USD), 'buyer_id' => $email, 'affiliateComm'=>$affiliateCommision);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                }
                                                                //update referrer user balance for affiliate earnings
                                                                $result   = creditUserEarning($referrerID, $affiliateEarnings, 'affearning', 'add');
                                                                /*echo "update referrer user balance for affiliate earnings creditUserEarning</br>";
                                                                printArr($result);*/
                                                                $response = json_encode($result);
                                                                if (noError($result)) {

                                                                    $errMsg = "credit referral amount to referral user: success.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"] = "{$buyerRefferedBy}";
                                                                    $kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"] = "{$affiliateEarnings}";

                                                                    $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $buyerRefferedBy);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                    //successfully updated community pool user balance with affiliate earnings
                                                                    $result   = creditUserEarning($communityPoolUserId, $affiliateEarnings, 'affearning', 'add');
                                                                    $response = json_encode($result);
                                                                    /*echo "updated community pool user balance with affiliate earnings creditUserEarning</br>";
                                                                    printArr($result);*/
                                                                    if (noError($result)) {

                                                                        $errMsg = "credit referral amount to pool: success.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                        $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $communityPoolUserId);
                                                                        $xml_data["parameters"]["data"] = "";
                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                        /* Set variables for make insert user transaction affiliate earnings */
                                                                        $recipientEmail = $buyerRefferedBy;
                                                                        $recipientUserId = $referrerID;
                                                                        $senderEmail = $communityPoolUser;
                                                                        $senderUserId = $communityPoolUserId;
                                                                        $type = 'affiliate_earnings';
                                                                        $paymentMode = 'ITD';
                                                                        $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $email, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $email, "raise_on" => 'first buy');

                                                                        $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $affiliateEarnings, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                        $response = json_encode($result);
                                                                        if (noError($result)) {

                                                                            $errMsg = "Insert user transaction for affiliate earnings : success.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                            $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'receiver' => $buyerRefferedBy, 'sender' => $communityPoolUser);
                                                                            $xml_data["parameters"]["data"] = "";
                                                                            $xml_data['parameters']["attributes"]= $reqArray;
                                                                            //success inserting cashback transaction
                                                                        } else {
                                                                            $errMsg = "Insert user transaction for affiliate earnings : failed.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                            $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'receiver' => $buyerRefferedBy, 'sender' => $communityPoolUser);
                                                                            $xml_data["parameters"]["data"] = "";
                                                                            $xml_data['parameters']["attributes"]= $reqArray;
                                                                            //error inserting cashback transaction
                                                                        }

                                                                        //send mail for affiliate earnings to referrer user


                                                                        //send mail for affiliate earnings
                                                                        $to                   = $buyerRefferedBy;
                                                                        $subject              = "Keywo: Refferal earnings notification";
                                                                        $message              = '<p>Hi, <br/> <br/>Your Referral earnings of ' . $affiliateEarnings . ' BTC has been credited to your Keywo Wallet.<br><br>Please <a href="' . $rootUrl . 'views/prelogin/index.php">login</a> to your Keywo account to access your wallet.</p>';
                                                                        //Your Referral earnings of 1.23 BTC has been credited to your SearchTrade Wallet
                                                                        $notification_message = 'Your Referral earnings of ' . $affiliateEarnings . ' BTC has been credited to your SearchTrade Wallet.';
                                                                        $permissionCode = 2;
                                                                        $path     = "";
                                                                        $keyword  = $keyword;

                                                                        $category = "buy";
                                                                        $sendNotifEamil = sendNotificationBuyPrefrence($to, $subject, $message, $referrerFirstName, $referrerLastName, $referrerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                        if(noError($sendNotifEamil)){
                                                                            $errMsg = "Send referral earning notification and email : success.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                        }else{
                                                                            $errMsg = "Send referral earning notification and email : failed.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                        }

                                                                    } else {
                                                                        $errMsg = "credit referral amount to pool: failed.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                        $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $communityPoolUserId);
                                                                        $xml_data["parameters"]["data"] = "";
                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                        //error updating affiliate pool
                                                                    }
                                                                } else {
                                                                    $errMsg = "credit referral amount to referral user: failed.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $buyerRefferedBy);
                                                                    $xml_data["parameters"]["data"] = "";
                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                    //Error updating user balance with affiliate earnings

                                                                }

                                                            } else {
                                                                $errMsg = "buyer referred id match : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                            }

                                                            /* Set variables for make insert user transaction first buy keyword */
                                                            $recipientEmail = $communityPoolUser;
                                                            $recipientUserId = $communityPoolUserId;
                                                            $senderEmail = $email;
                                                            $senderUserId = $buyerID;
                                                            $type = 'keyword_purchase';
                                                            $paymentMode = 'ITD';
                                                            $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $email, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $email, "raise_on" => 'first buy');

                                                            //insert keyword purchase transaction
                                                            $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwdPurchaseAmount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                                            /*echo "insert keyword purchase transaction insertUserTransaction</br>";
                                                            printArr($result);*/
                                                            $response = json_encode($result);
                                                            if (noError($result)) {

                                                                $errMsg = "transaction for keyword purchase from admin : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'keyword' => urlencode($keyword), 'buyer_id' => $email, 'payment_mode'=> 'wallet');
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                //success inserting cashback transaction
                                                            } else {

                                                                $errMsg = "transaction for keyword purchase from admin : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($kwdPurchaseAmount), 'keyword' => urlencode($keyword), 'buyer_id' => $email, 'payment_mode'=> 'wallet');
                                                                $xml_data["parameters"]["data"] = "";
                                                                $xml_data['parameters']["attributes"]= $reqArray;
                                                                //error inserting cashback transaction
                                                            }

                                                            //send keyword purchase mail to buyer
                                                            $to                   = $email;
                                                            $subject              = "Keywo: Keyword Licence Purchase Confirmation";
                                                            $message              = '<p>Hi, <br/> <br/>You have successfully purchased the license for <b>#' . $keyword . '</b> via Keywo Wallet.</p>';
                                                            //You have successfully purchased the license for #keyword via SearchTrade Wallet
                                                            $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Keywo Wallet.';

                                                            //  $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                                                            //  $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];
                                                            $method_name     = "GET";
                                                            $getNotification = notifyoptions($buyerID, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                            $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                                                            $permissionCode1 = $getNotification["errMsg"]["notify_options_fk_key"]["buy_opt_container"]["2"]["permissions"]["_id"];

                                                            $path    = "";
                                                            $keyword = $keyword;
                                                            //SendMail($to, $subject, $message, $path, $keyword);

                                                            $category   = "buy";
                                                            $linkStatus = 1;
                                                            $notiSend   = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                            $subject  = "Keywo: Keyword Licence Purchase Confirmation";
                                                            $message  = '<p>Hi, <br/> <br/>You have successfully purchased the license for <b>#' . $keyword . '</b> via Keywo Wallet.</p>';
                                                            $category = "buy";

                                                            $linkStatus           = "";
                                                            $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Keywo Wallet.';
                                                            $sendNotifEmailToBuyer = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode1, $category, $linkStatus);
                                                            if(noError($sendNotifEmailToBuyer)){
                                                                $errMsg = "Send email and notification to ".$email." : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            }else{
                                                                $errMsg = "Send email and notification to ".$email." : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                            }

                                                            //send ownership zip mail to admin
                                                            $sendOwnershipZip =  sendKwdOwnershipZipToAdmin($keyword, $kwdDbConn,"purchase");
                                                            if(noError($sendOwnershipZip)){ //Successfully send ownership zip to admin
                                                                $errMsg = "Send ownership zip to admin : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";


                                                            }else{
                                                                $errMsg = "Send ownership zip to admin : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                            }

                                                            /******************** for Keyword Statistics ****************/
                                                            insert_keyword_statistics("total_kwd_sold_by_btc","0",$kwdDbConn);
                                                            insert_keyword_statistics("total_keyword_sold_amount",$kwd_price_USD,$kwdDbConn);
                                                            /******************** for Keyword Statistics ****************/

                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;



                                                            unset($kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"]);
                                                            unset($kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"]);

                                                        } else {
                                                            $errMsg = "credit buyer purchase field : failed.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                            $reqArray = array("buyer_id" => urlencode($email), "keyword_price" => urlencode($amount));
                                                            $xml_data["parameters"]["data"] = "";
                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                            //rollbackTransaction
                                                            $trans = rollbackTransaction($kwdDbConn);

                                                            $errorMessage = "There was an error while buying this keyword";
                                                            $returnArr['errCode'] = 2;
                                                            $returnArr['errMsg']  = $errorMessage;
                                                        }

                                                    } else {
                                                        //rollbackTransaction
                                                        $errMsg = "first buy status match : failed.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                        $trans = rollbackTransaction($kwdDbConn);

                                                        $errorMessage = "There was an error while buying this keyword";
                                                        $returnArr['errCode'] = 2;
                                                        $returnArr['errMsg']  = $errorMessage;
                                                    }


                                                } else {
                                                    $errMsg = "removed keyword from cart : failed, keyword {$keyword}.";
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                    //Buy keyword error
                                                    $trans = rollbackTransaction($kwdDbConn);

                                                    $errorMessage = "There was an error while buying this keyword";
                                                    $returnArr['errCode'] = 2;
                                                    $returnArr['errMsg']  = $errorMessage;

                                                }

                                            } else {
                                                $errMsg = "update voucher status in ownership details : failed, voucher code {$voucher_code}.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                //Buy keyword error
                                                $trans = rollbackTransaction($kwdDbConn);

                                                $errorMessage = "There was an error while buying this keyword";
                                                $returnArr['errCode'] = 2;
                                                $returnArr['errMsg']  = $errorMessage;
                                            }

                                        } else {
                                            $errMsg = "update presale voucher status : failed.";
                                            $keywordCounter++;
                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                            //Buy keyword error
                                            $trans = rollbackTransaction($kwdDbConn);

                                            $errorMessage = "There was an error while buying this keyword";
                                            $returnArr['errCode'] = 2;
                                            $returnArr['errMsg']  = $errorMessage;

                                        }

                                    } else {
                                        $errMsg = "Increase keyword sold count in purchase slab failed.";
                                        $keywordCounter++;
                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                         $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                        //Buy keyword error
                                        $trans = rollbackTransaction($kwdDbConn);
                                        $errorMessage = "There was an error while buying this keyword";
                                        $returnArr['errCode'] = 2;
                                        $returnArr['errMsg']  = $errorMessage;

                                    }

                                } else {
                                    $errMsg = "inserting ownership details failed.";
                                    $keywordCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                     $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                    //Buy keyword error
                                    $trans = rollbackTransaction($kwdDbConn);

                                    $errorMessage = "There was an error while buying this keyword";
                                    $returnArr['errCode'] = 2;
                                    $returnArr['errMsg']  = $errorMessage;
                                }

                            } else {
                                $errMsg = "start transaction failed.";
                                $keywordCounter++;
                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                 $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                //System Error.Please try again after some time.
                                $errorMessage = "System Error.Please try again after some time";
                                $returnArr['errCode'] = 2;
                                $returnArr['errMsg']  = $errorMessage;

                            }

                        } else {
                            $errMsg = "No keyword in cart.";
                            $keywordCounter++;
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                             $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                            $returnArr['errCode'] = 2;
                            $returnArr['errMsg']  = $errMsg;
                        }

                        $keywords_i++;
                    }

                }else{
                    $errMsg = "getting admin settings of buyer user failed.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $returnArr['errCode'] = 2;
                    $returnArr['errMsg']  = $errMsg;

                }

            } else {
                $errMsg = "Presale status is inactive.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                //Presale is paused
                $returnArr['errCode'] = 2;
                $returnArr['errMsg']  = $errMsg;
            }
        } else {
            $errMsg = "Error getting presale status.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            //Presale is paused
            $returnArr['errCode'] = 2;
            $returnArr['errMsg'] = $errMsg;
        }

    } else {
        $errMsg = "amount paid match : failed.";
        $stepCounter++;
        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $returnArr['errCode'] = 2;
        $returnArr['errMsg'] = $errMsg;
    }

    if($returnArr["errCode"]  == -1){
        $returnArr["errCode"]  = -1;
        $returnArr['errMsg'] = "You have successfully brought this keyword";
    }else{
        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr['errMsg'] = $returnArr['errMsg'];
    }

    return $returnArr;
}

function refundOrDepositAmountToUser($param,&$stepCounter,$xml_data,$blanks)
{
    global $communityPoolUser,$communityPoolUserId;

    $retArray = array();
    $returnArr = array();
    $extraArgs = array();

    //Get required values
    /*$kwdsConn = $param['conn'];
    $order_id = $param['orderId'];
    $trans_id = $param['transId'];
    $payment_addresss = $param['paymentAdd'];
    $status = $param['status'];
    $userIp = $param['ip'];
    $reciever_email = $param['recieverEmail'];
    $amount_due = $param['amountDue'];
    $recieved_amount = $param['recievedAmount'];
    $keywords = $param['keywords'];
    $keywordWithTags = $param['keywordWithTags'];
    $rateforusd = $param['usdRate'];
    $rateforsgd = $param['sgdRate'];*/

    $errMsg = "Start refund amount to user, payment status => {$param['status']}.";
    $stepCounter++;
    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $errMsg = "Cleaning input parameters, payment status => {$param['status']}.";
    $stepCounter++;
    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    //Pass array through Clean Query Parameter
    array_walk($param, "cleanQueryParameter");

    $errMsg = "Validated cleanQueryParameter, payment status => {$param['status']}.";
    $stepCounter++;
    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    //Get required values
    $kwdsConn = $param['conn'];
    $order_id = $param['orderId'];
    $trans_id = $param['transId'];
    $payment_addresss = $param['paymentAdd'];
    $payment_status = $param['status'];
    $userIp = $param['ip'];
    $email = $param['buyerEmail'];
    $buyerID = $param['buyerId'];
    $amount_due = $param['amountDue'];
    $recieved_amount = $param['recievedAmount'];
    $totalAvailable_amount = $param['totalAvailableAmount'];
    $keywordBasket = $param['keywords'];
    $keywordsWithTags = $param['keywordWithTags'];
    $exchangeRate = $param['exchangeRate'];
    $buyerRefferedBy = $param['buyerRefferedBy'];
    $referrerID = $param['referrerID'];
    $firstBuyStatus = $param['firstBuyStatus'];
    $referrerFirstName = $param['referrerFirstName'];
    $referrerLastName = $param['referrerLastName'];
    $buyerFirstName = $param['buyerFirstName'];
    $buyerLastName = $param['buyerLastName'];


    if(!in_array($kwdsConn,$blanks) && !in_array($order_id,$blanks) && !in_array($trans_id,$blanks) && !in_array($payment_addresss,$blanks) && !in_array($email,$blanks) && !in_array($amount_due,$blanks) && !in_array($recieved_amount,$blanks) && !in_array($payment_status,$blanks))
    {
        /*$paidAmount = number_format((float)($recieved_amount/100000000),8);*/
        $refundAmount = -1;

        $errMsg = "Checking empty required fields: Success, payment status => {$param['status']}.";
        $stepCounter++;
        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        if($totalAvailable_amount > $amount_due){
            $refundAmount = number_format((float)($totalAvailable_amount - $amount_due),8);
            $status = 'overpaid';
        }else if($totalAvailable_amount < $amount_due){
            $refundAmount = number_format((float) $recieved_amount,8);
            $status = 'mispaid';
        }

        if(!empty($recieved_amount) && $recieved_amount > 0)
        {

            $errMsg = "amount paid match : success, payment status => {$param['status']}.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $trans = startTransaction($kwdsConn);
            /*$checkArr['errCode'][] = $trans['errCode'];
            $checkArr['errFunc'][] = 'startTransaction';*/
            if (noError($trans)) {

                $errMsg = "Start Transaction: success, payment status => {$param['status']}.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $query = "UPDATE payments SET refund_amount=" . $refundAmount . ",status='paid',payment_time=now(),request_ip_address='" . $userIp . "',transaction_type='keyword_purchase by bitgo',customer_email='networksearchcoin@gmail.com' WHERE order_id='" . $order_id . "' AND username = '" . $email . "' AND payment_transaction_id = '".$trans_id."'";
                $result = runQuery($query, $kwdsConn);
                /*$checkArr['errCode'][] = $result['errCode'];
                $checkArr['errFunc'][] = 'runQuery update payment';*/
                if (noError($result)) {
                    $errMsg = "update payments table : success | transaction_id = {$param['status']} refund_amount = {$refundAmount} request_ip_address: {$userIp} customer_email = $email";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    //send keyword purchase mail to buyer
                    $to                   = $email;
                    $subject              = "Keywo: Keyword Licence Purchase Confirmation";
                    $message              = '<p>Hi, <br/> <br/>You have successfully purchased the license for <b>#' . $keyword . '</b> via Keywo Wallet.</p>';
                    //You have successfully purchased the license for #keyword via SearchTrade Wallet
                    $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Keywo Wallet.';

                    //  $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                    //  $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];
                    $method_name     = "GET";
                    $getNotification = notifyoptions($buyerID, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                    $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                    $permissionCode1 = $getNotification["errMsg"]["notify_options_fk_key"]["buy_opt_container"]["2"]["permissions"]["_id"];

                    $path    = "";
                    $keyword = $keyword;
                    //SendMail($to, $subject, $message, $path, $keyword);

                    $category   = "buy";
                    $linkStatus = 1;
                    $notiSend   = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                    $subject  = "Keywo: SearchTrade: Deposit Confirmation";
                    $message = "<p>You Have successfully deposited " . $refundAmount . " BTC in your SearchTrade wallet</p>";
                    $category = "fund";

                    $linkStatus           = "";
                    $notification_message = "You Have successfully deposited {$refundAmount} BTC in your SearchTrade wallet.";
                    $sendNotifEmailToBuyer = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode1, $category, $linkStatus);
                    if(noError($sendNotifEmailToBuyer)){
                        $errMsg = "Send email and notification to ".$email." : success.";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                    }else{
                        $errMsg = "Send email and notification to ".$email." : failed.";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                        http_response_code(400);
                    }

                } else {
                    $errMsg = "update payments table : failed | transaction_id = {$param['status']} refund_amount = {$refundAmount} request_ip_address: {$userIp} customer_email = $email";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $returnArr['errCode'] = 2;
                    $returnArr['errMsg']  = $errMsg;
                    http_response_code(400);
                }
            }else {
                $errMsg = "Start Transaction: success, payment status => {$param['status']}.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr['errCode'] = 2;
                $returnArr['errMsg']  = $errMsg;
                http_response_code(404);
            }

        } else {
            $errMsg = "amount paid match : success, payment status => {$param['status']}.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $returnArr['errCode'] = 2;
            $returnArr['errMsg']  = $errMsg;
            http_response_code(400);
        }
    } else {
        $errMsg = "Checking empty required fields: failed, payment status => {$param['status']}.";
        $stepCounter++;
        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $returnArr['errCode'] = 2;
        $returnArr['errMsg']  = $errMsg;
        http_response_code(400);
    }

    if($returnArr["errCode"]  == -1){
        $returnArr["errCode"]  = -1;
        $returnArr['errMsg'] = "You have successfully brought this keyword";
    }else{
        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr['errMsg'] = $returnArr['errMsg'];
    }

    return $returnArr;
}

?>