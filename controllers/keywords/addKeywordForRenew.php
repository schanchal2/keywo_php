<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once ("{$docrootpath}config/config.php");
require_once ("{$docrootpath}config/db_config.php");
require_once ("{$docrootpath}helpers/coreFunctions.php");
require_once ("{$docrootpath}helpers/errorMap.php");
require_once ("{$docrootpath}helpers/arrayHelper.php");
require_once ("{$docrootpath}helpers/stringHelper.php");
require_once ("{$docrootpath}models/keywords/keywordPurchaseModel.php");
require_once ("{$docrootpath}models/keywords/userCartModel.php");

$clientSessionFromCheckout = cleanXSS(urldecode($_POST["clientSessionId"]));
$currentSession = session_id();
$flag = 0;
$returnArr = array();

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
        $email = $_SESSION["email"];

    if($clientSessionFromCheckout == $currentSession){
        // create database connection
        $kwdDbConn = createDBConnection("dbkeywords");
        if(noError($kwdDbConn)){

            $kwdDbConn = $kwdDbConn["connection"];

            $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);

            if(noError($getPresaleDetails)){
                $getPresaleDetails = $getPresaleDetails["errMsg"];
                $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];

                $getRenewKwd = json_decode($getRenewKwd, true);

                $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);

                if(noError($getMyKeywordDetails)){

                    $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
                    $myKeyword = $getMyKeywordDetails["transaction_details"];
                    $myKeyword = json_decode($myKeyword, true);
                    $myKeyword = array_reverse($myKeyword);
                    $kwdCount  = count($myKeyword);

                    if($kwdCount == 0){
                        echo "No Keyword Available For Renew";
                    }else{
                        foreach($myKeyword as $key => $keyword){
                            $keyword = $keyword["keyword"];
                            $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
                            if(noError($getKwdOwnershipDetails)){
                                $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                                $expireWithinDays = $getKwdOwnershipDetails["days_to_expire"];

                                if(isset($expireWithinDays) && !empty($expireWithinDays) && $expireWithinDays <= 30 && $expireWithinDays > -15){
                                    if(!in_array($keyword, $getRenewKwd)){
                                    ?>

                                        <div class="row border-all border-top-none">
                                            <div class="col-md-5 text-center innerML padding-right-none">
                                                <div class="text-left innerML">
                                                    <div class="checkbox checkbox-primary margin-bottom-none">
                                                        <input id="checkbox" class="renewCheckbox" type="checkbox"  value="<?php echo $keyword; ?>" />
                                                        <label for="checkbox1" class="text-blue keyword-name ellipses half innerL renew-label">
                                                            <a class="display-in-block-txt-blk"  title="" data-toggle="tooltip" data-placement="bottom"  data-original-title="#tridev"><?php echo $keyword; ?></a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 innerMT text-right pull-right">
                                                <span class="text-red margin-bottom-none"><?php echo abs($expireWithinDays); ?> days to expire</span>
                                            </div>
                                        </div>
                                        <?php
                                        $flag = 1;
                                    }
                                }
                            }else{
                                print("Error: Fetching #".$keyword." details");
                            }
                        }

                        if($flag == 0){
                            //echo "No Keyword Available For Renew";

                            ?>
                            <div class="col-md-11 text-center innerML padding-right-none">
                                <div class="text-left innerxL text-center">
                                    No Keyword Available For Renew
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                        <input type="hidden" value="<?php echo $flag; ?>" />
                        <?php
                    }
                }else{
                    print("Error: Fetching keyword details");
                }
            }else{
                print('Error: Fetching renew keyword details');
            }



        }else{
            print("Error: Database connection");
        }
    }else{
        print("Authorisation Failure");
    }
}else{
    print("Mendatory field not found");
}

?>


