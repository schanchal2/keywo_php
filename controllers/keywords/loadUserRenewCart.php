<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

// create keywrod database connection
$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    // get admin setting details
    $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
    if(noError($getAdminSetting)){
        $getAdminSetting = $getAdminSetting["data"];

        $getRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

        // calculate btc amount from renewal amount
        $btcRate = number_format($getExchangeRate["btc"], 8);
        $calcBtcOfRenewalAmt =  number_format(($getRenewalAmt*$btcRate), 8);

        // calculate upc amount from renewal amount.
        $upcRate = $getExchangeRate[$currPreference];
        $calcUpcRateOfRenewalAmt = number_format(($upcRate * $getRenewalAmt), 8);

        $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
        if(noError($getPresaleDetails)){
            $getPresaleDetails = $getPresaleDetails["errMsg"];
            $getRenewKwd = json_decode($getPresaleDetails["user_renew_kwd_in_cart"], true);

        }else{
            print('Error: Fetching renew keyword details');
        }
    }else{
        print('Error: Fetching admin setting details.');
    }
}else{
    print('Error: Database connection');
}

?>



<thead class="">
<tr>

    <th>Keyword</th>

    <th>
        <?php echo $_SESSION['CurrPreference']; ?>

    </th>
    <th> <?php echo $keywoDefaultCurrencyName; ?> </th>
    <th></th>
</tr>


</thead>
<tbody>

<?php
if(strtolower($_SESSION['CurrPreference']) == "btc"){
    $numAfterDecimal = 8;
}else{
    $numAfterDecimal = 4;
}

foreach($getRenewKwd as  $keyword){
    $renewKwdInCart .= "#".$keyword.",";

    ?>

    <tr>
        <td>
            <span class="ellipses ellipses-general"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo $keyword;  ?>');" title="" data-toggle="tooltip" data-placement="bottom" href="?q=<?php echo $keyword;  ?>" data-original-title="<?php echo "#{$keyword}"; ?>"><?php echo "#{$keyword}"; ?></a>
                              </span><br>
            <label class="ellipses ellipses-general" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$keyword}"; ?>' data-original-title="<?php echo number_format($calcBtcOfRenewalAmt, 8); ?>" >BTC : <?php echo number_format($calcBtcOfRenewalAmt, 8); ?></label>
        </td>
        <td>
            <label class="ellipses ellipses-general" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$keyword}"; ?>' data-original-title="<?php echo number_format($calcUpcRateOfRenewalAmt, $numAfterDecimal); ?>" ><?php echo number_format($calcUpcRateOfRenewalAmt, $numAfterDecimal); ?></label>
        </td>
        <td>
            <label class="ellipses ellipses-general" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$keyword}"; ?>' data-original-title="<?php echo number_format($getRenewalAmt, 4); ?>" ><?php echo number_format($getRenewalAmt, 4); ?></label>
        </td>
        <td>
            <!--  <button type="button" value="Remove" id="cht_cartButton_<?php /*echo cleanXSS($key); */?>" onclick="return addRemoveFromCart('<?php /*echo $email;  */?>' ,'<?php /*echo cleanXSS($key); */?>','<?php /*echo cleanXSS($key); */?>', '<?php /*echo $clientSessionId; */?>', '<?php /*echo $rootUrl; */?>', 'cht')" class="close" data-dismiss="modal">×</button>-->
            <button type="button" value="Remove" id="cht_cartButton_<?php echo cleanXSS($keyword); ?>" class="close" onclick="removeRenewKwd('<?php echo cleanXSS($keyword); ?>')" data-dismiss="modal">×</button>

        </td>

    </tr>

    <?php

    $totalUPCAmt = $totalUPCAmt + $calcUpcRateOfRenewalAmt;
    $totalITDAmt = $totalITDAmt + $getRenewalAmt;
    $totalBTCAmt = $totalBTCAmt + $calcBtcOfRenewalAmt;
}

?>



</tbody>
<tfoot>
<tr>
    <td>
        <span><b>SUBTOTAL</b></span><br>
    </td>
    <td></td>
    <td></td>
    <td></td>
</tr>
<tr>
    <td>
        <label class="ellipses ellipses-general" title="<?php echo number_format("{$totalBTCAmt}", 8,'.',''); ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo number_format($totalBTCAmt, 8); ?>" >BTC : <?php echo number_format($totalBTCAmt, 8); ?></label>
    </td>
    <td>
        <label class="ellipses ellipses-general" title="<?php echo number_format("{$totalUPCAmt}", $numAfterDecimal,'.',''); echo " {$_SESSION['CurrPreference']}"; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo number_format($totalUPCAmt, $numAfterDecimal); echo " {$_SESSION['CurrPreference']}"; ?>" ><?php echo number_format($totalUPCAmt, $numAfterDecimal); ?></label>
    </td>
    <td>
        &nbsp; <label class="ellipses ellipses-general" title="<?php echo number_format("{$totalITDAmt}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="<?php echo number_format($totalITDAmt, 4); echo " {$keywoDefaultCurrencyName}"; ?>" ><?php echo number_format($totalITDAmt, 4); ?></label>
    </td>
    <td></td>
</tr>
</tfoot>


<script>

    //get keyword unit price

    //get keyword total price in USD
    var  finalITDAmt = <?php echo $totalITDAmt;?>;
    //get keyword basket with tags
    var  kwdRenewInCart = '<?php echo rtrim($renewKwdInCart,','); ?>';




</script>


<div>
    <form style="display:block;" id="renewByPaypalForm" method="post" action="<?php echo $rootUrl; ?>controllers/paypal/Paypal-Express/processForRenew.php">
        <input  name="itemname" id="keywordBasketPaypal" value="keywordBasket" type="hidden" />
        <input  name="itemnumber" id="itemnumberPaypal" value="<?php echo $orderIdForPaypal; ?>" type="hidden"/>
        <input  name="itemdesc" value="Keyword Purchase at scoinz.com. <?php echo $presaleRootURL;?>views/orderRecords.php?orderId=<?php echo $orderIdForPaypal;?>" type="hidden"/>
        <input  name="grandTotal" id="totalPricePaypal" type="hidden"/>
        <input  name="itemprice" id="itemPricePaypal" type="hidden"/>
        <input  name="itemQty" id="itemQtyPaypal" value="<?php echo $usercartCount;?>" type="hidden"/>
        <input  name="custom" id="customPaypal" type="hidden"/>
    </form>
</div>

