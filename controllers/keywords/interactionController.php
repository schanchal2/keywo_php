<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}/helpers/arrayHelper.php");
//  require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
error_reporting(0);

$email = $_SESSION["email"];
$clientSessionId = session_id();

//For database connection
$conn = createDBConnection('dbkeywords');

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$pageUrl = "views/keywords/analytics/keyword_analytics.php";

$userCartDetails = getUserCartDetails($email, $conn);

if(noError($userCartDetails)){
    $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
    $userCartDetails = json_decode($userCartDetails, TRUE);
}else{
    print('Error: Fetching cart details');
    exit;
}

?>

<ul class="border-all short">
    <?php
    $pageUrl = "views/keywords/analytics/keyword_analytics.php";
    $monthwiseDate  = date('m_Y');
    $datePickerDate = cleanQueryParameter($conn, cleanXSS($_POST["tablename"]));
    $remove_slash = stripslashes($datePickerDate);
    $var = str_replace(array('/'), array('_'), $remove_slash);

    if(empty($datePickerDate))
    {
        $tableName      = "daily_keyword_earnings_{$monthwiseDate}";
    }
    else
    {
        $tableName      = "daily_keyword_earnings_{$var}";
    }

    $limit         = cleanQueryParameter($conn, cleanXSS($_POST["limit"]));
    $type          = cleanQueryParameter($conn, cleanXSS($_POST["type"]));

    if (isset($_POST["page"])) {
        $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
        if (!is_numeric($page_number)) {
            die('Invalid page number!');
        } //incase of invalid page number
    } else {
        $page_number = 1; //if there's no page number, set it to 1
    }
    if ($page_number == 0) {
        $page_number = 1;
    }
    $item_per_page = $limit;

    $get_total_rows = getAnalyticsDetailsCount($tableName,$conn);
    if(noError($get_total_rows))
    {
        $getTotalRows = $get_total_rows["errMsg"][0];
        $getTotalRow[]= $getTotalRows["count"];
        $getTotalRecords = array_sum($getTotalRow);
    }

    $total_pages = ceil($getTotalRecords / $item_per_page);

    $page_position = (string)(($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $getAnalyticsDetails = getAnalyticsDetails($page_position,$limit,$tableName,$type,$conn);
    if(noError($getAnalyticsDetails))
    {
        $getAnalyticsDetails = $getAnalyticsDetails["errMsg"];
        $abc[] = $getAnalyticsDetails;
        $getAnalyticsDetail[] = count($getAnalyticsDetails);
        $getTotalDataRecord = array_sum($getAnalyticsDetail);

        if ($getTotalDataRecord == 0) {
            $page_number = $page_number - 1; //if there's no page number, set it to 1
            $item_per_page = $limit;

            $page_position = (($page_number - 1) * $item_per_page);
            $lastpage = ceil($total_pages);
        }

        foreach($getAnalyticsDetails as $getDetails)
        {
            $keyword          = $getDetails["keyword"];
            $interactionCount = $getDetails["total_count"];

            ?>

            <li>

                <div class="row">
                    <div class="col-md-4">
                        <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $keyword; ?>" title="<?php echo $keyword; ?>" data-placement="bottom" data-toggle="tooltip" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></label>
                    </div>

                    <?php if($type == 'total_interaction_count'){ ?>

                        <div class="col-md-3 text-black">
                            <span class="text-black ellipses margin-bottom-none margin-top-none">&nbsp;<a href="javascript:;" title="<?php echo $interactionCount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$interactionCount}", 0); ?></a> </span>

                        </div> <?php } else{ ?>

                        <div class="col-md-3 text-black">
                 <span class="text-black ellipses margin-bottom-none margin-top-none">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                               origPrice="<?php echo number_format("{$interactionCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$interactionCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$interactionCount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                        </div> <?php } ?>

                    <div class="col-md-5 pull-right text-right">
                        <?php

                        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);
                        if(noError($checkForKeywordAvailability))
                        {
                        $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                        $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                        $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
                        $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
                        $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                        $CartStatus = $checkForKeywordAvailability['status'];
                        $activeBids = $checkForKeywordAvailability["active_bids"];

                        if($availabilityFlag == 'keyword_available'){
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right">
                                    <?php if(in_array($keyword, $userCartDetails)){ ?>
                                        <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-dark button-text" onclick="addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                    <?php }else{ ?>
                                        <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-dark button-text" onclick="addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                    <?php } ?>
                                </div>
                            </div>

                        <?php } elseif($availabilityFlag == 'keyword_not_available'){
                        $activeBids = json_decode($activeBids, true);
                        foreach($activeBids as $key => $bidValue)
                        {
                            $bidValue = explode('~~', $bidValue);
                            $bidderEmail[] = $bidValue[1];
                        }

                        if(in_array($email, $bidderEmail))
                        { $bidStatus = true; }
                        else{ $bidStatus = false; }

                        if($email == $kwdOwnerId){
                            if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                ?>
                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                                    </div>
                                </div>

                            <?php }else{ // set ask ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                    </div>
                                </div>

                            <?php }if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right">
                                        <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                                    </div>
                                </div>

                            <?php }}else{ if($CartStatus == "sold"){ if(empty($activeBids)){ ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 col-md-12 innerMB">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                                </div>
                            </div>

                        <?php }else{ if($bidStatus){ ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                                </div>
                            </div>

                        <?php }else{ ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                                </div>
                            </div>

                        <?php }} if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>

                        <div class="row pull-right">
                            <div class="col-xs-6 col-md-12 innerMB ">
                                <button class="btn-trading-dark"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                            </div>
                        </div>
                    </div>
                <?php }}}?>


                <div class="row pull-right">
                <div class="col-xs-6 text-right">
                <?php } elseif($availabilityFlag == "keyword_blocked"){ ?>

                    <input value="Not Available" type="text" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
                <?php } ?>
                </div>
                </div>


                <?php }?>



            </li>

        <?php }} else {  }
    if ($getTotalRecords == 0) { ?>
        <li>
            <div> No Data Found </div>

        </li><?php exit; }  ?>
</ul>

<div class="pagination-cont pull-right gotovalues">
    <span class="span-keyword-market pull-left">Go to : &nbsp;</span>

    <?php

    getPaginationData($lastpage, $page_number, $limit);
    function getPaginationData($lastpage, $pageno, $limit)
    {
        ?>

        <input type="number" value="<?php echo $pageno; ?>" placeholder="" title="please press enter key" data-placement="bottom" data-toggle="tooltip" id="LimitedResultData" class="span-blue-keyword-market border-all allownumericwithoutdecimal LimitedResultData">



        <?php
        echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
        echo '<ul class="pagination">';


        if ($pageno > 1) {

            $pagenum = 1;
            print('<li class="status"><a href="#"onclick=getNewInteractionData("' . $pagenum . '","' . $limit . '")>&laquo;</a></li>');
        }

        if ($pageno > 1) {
            $pagenumber = $pageno - 1;
            print('<li class="status"><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>Previous</a></li>');
        }

        if ($pageno == 1) {
            $startLoop = 1;
            $endLoop = ($lastpage < 5) ? $lastpage : 5;
        } else if ($pageno == $lastpage) {
            $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
            $endLoop = $lastpage;
        } else {
            $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
            $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
        }

        for ($i = $startLoop; $i <= $endLoop; $i++) {
            if ($i == $pageno) {
                print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
            } else {
                $pagenumber = $i;
                print('<li class="status"><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>' . $i . '</a></li>');
            }
        }
        if ($pageno < $lastpage) {
            $pagenumber = $pageno + 1;
            print('<li class="status"><a href="#" onclick=getNewInteractionData("' . $pagenumber . '","' . $limit . '")>Next</a></li>');

        }

        if ($pageno != $lastpage) {
            print('<li class="status"><a href="#" onclick=getNewInteractionData("' . $lastpage . '","' . $limit . '")>&raquo;</a></li>');
        }


        echo '</ul>';
        echo '</div>';
    }

    ?>
</div>
<div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>

<script>

    $('#LimitedResultData').tooltip({ placement: "left", trigger: "focus" });

    $("#LimitedResultData").keypress(function(e)
    {
        if(e.which == 13) {
            var limit = '<?php echo $limit; ?>';
            var type = '<?php echo $type; ?>';
            var pageNo = parseInt($(this).val()); //alert(pageNo);
            var lastPage = parseInt('<?php echo $lastpage; ?>'); //alert(lastPage);

            if (pageNo == 0 || pageNo > lastPage) {
                pageNo = 1;
                showToast("success", "This page is not available");
            }

            getNewInteractionData(pageNo, limit,type);
        }

    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var charCode = event.which;

        if(charCode == 8 || charCode == 0)
        {
            return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode);
            return /[a-zA-Z0-9]/.test(keyChar);
        }

    });

</script>