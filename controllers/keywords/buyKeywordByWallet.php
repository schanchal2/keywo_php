<?php

/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 20/2/17
 * Time: 6:11 PM
 */

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/walletHelper.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../helpers/transactionHelper.php");
require_once("../../models/keywords/keywordCdpModel.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");
require_once("../../models/keywords/keywordSearchModel.php");
require_once("../../models/header/headerModel.php");
require_once("../../models/keywords/acceptBidModel.php");
require_once('../smtp/Send_Mail.php');
require_once("../../backend_libraries/xmlProcessor/xmlProcessor.php");
require_once("../../models/analytics/keyword_stat_analytics.php");
error_reporting(0);

$retArray = array();
$reqArray = array();
$returnArr = array();
$extraArgs = '';
$stepCounter = 0;

$redirectUrl = "{$rootUrl}views/keywords/";

$buyer_id         = $_SESSION["email"];
$username         = $_SESSION["first_name"];
$buyer_id         = cleanXSS(urldecode($buyer_id));
$buyerID          = $_SESSION['id'];
$keywordBasket    = rtrim(cleanXSS($_POST['keywordBasket']),',');
$mode    = cleanXSS(urldecode($_POST['mode']));
$termsFlag    = cleanXSS(urldecode($_POST['termsFlag']));
$clientSid    = cleanXSS(urldecode($_POST['client_sid']));

/*$redirectURL  = "{$rootUrl}views/keyword/cart/checkout.php";*/
$originIp         = getClientIP();

// concatenate # with first keyword
$keywordBasket = "#".$keywordBasket;

$kwdPurchaseLog = $logPath["keywordPurchase"];
$xmlFilename = "payByWallet.xml";

$i =0;

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xmlArray    = initializeXMLLog(trim(urldecode($buyer_id)));

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

/************very important for logs************/
$logbasket=str_replace(",","",$keywordBasket);
$logbasket=str_replace("#","",$logbasket);
$xml_data["keywordBasket"]["data"] = $logbasket;
/************very important for logs************/

$errMsg = "Start keyword purchase process by wallet.";
$stepCounter++;
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


if(!isset($_SESSION["verify_status"])){
    $unique_id = md5(uniqid(rand(), true));
    $xmlArray['activity']['id'] = $unique_id;
    $_SESSION['activity_id'] = $unique_id;

    $_SESSION['redirect_url'] = $returnURl;
    $msg = "Start {$xmlHeader} process.";
    $xml_data['step'.$m]["data"] = $m.". {$msg}";

    $checkEmailExistence = checkEmailExistance($buyer_id);
    if(noError($checkEmailExistence)){

        $kwdDbConn = createDBConnection('dbkeywords');
        if(noError($kwdDbConn)){
            $kwdDbConn = $kwdDbConn["connection"];

            $userTotalCartPrice = getUserTotalCartPrice($kwdDbConn, $buyer_id);
            if(noError($userTotalCartPrice)){
                $totalPrice = number_format((float)$userTotalCartPrice["errMsg"]["total_cart_price"],4,'.','');

                $presaleStatus = getPresaleStatus($kwdDbConn);
                if(noError($presaleStatus)){

                    $presaleStatus = $presaleStatus["errMsg"]["presale_status"];

                    if ($presaleStatus != 0) {

                        $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";
                        $buyerBalanceDetails = getUserInfo($buyer_id, $walletURLIP . 'api/v3/', $buyerBalanceFields);

                        if(noError($buyerBalanceDetails)){

                            $userBalance = calculateUserBalance($buyerBalanceDetails);
                            /*echo "calculateUserBalance</br>";
                            printArr($userBalance);*/
                            if (noError($userBalance)) {
                                $userAvailableBalance = $userBalance['errMsg']['total_available_balance'];

                                if ($totalPrice <= $userAvailableBalance) {

                                    $errMsg = "Success : User balance is greater  then the total cart price";
                                    $xml_data['step'.++$i]["data"] = $i.". Success :  User balance is greater  then the total cart price";
                                    $returnArr["errCode"] = -2;
                                    $returnArr["errMsg"] = $errMsg;

                                    //redirecting to 2FA page
                                    $msg = "Success : Redirecting to 2FA page";
                                    $xml_data['step'.++$i]["data"] = $i.". {$msg}";
                                    $returnArr["errCode"] = -2;
                                    $_SESSION['form_data'] = json_encode($_POST);
                                    $_SESSION['xml_step'] = $i;

                                    $_SESSION['xml_file'] = $kwdPurchaseLog . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlFilename;

                                }else{
                                    $errMsg = "You have insufficient balance to buy this keyword";
                                    $xml_data['step'.++$i]["data"] = $i.". Failed : user wallet balance is insufficient";
                                    $returnArr["errCode"] = 8;
                                    $returnArr["errMsg"] = $errMsg;
                                }

                            }else{
                                $errMsg = "Error: Calculate available balance";
                                $xml_data['step'.++$i]["data"] = $i.". Failed : Calculate available balance";
                                $returnArr["errCode"] = 7;
                                $returnArr["errMsg"] = $errMsg;
                            }
                        }else{
                            $errMsg = "Error: Fetching get user info";
                            $xml_data['step'.++$i]["data"] = $i.". Failed : Fetching get user info";
                            $returnArr["errCode"] = 6;
                            $returnArr["errMsg"] = $errMsg;
                        }

                    }else{
                        $errMsg = "Presale status is inactive.";
                        $xml_data['step'.++$i]["data"] = $i.". Failed : Presale status is inactive";
                        $returnArr["errCode"] = 5;
                        $returnArr["errMsg"] = $errMsg;
                    }
                }else{
                    $errMsg = "Error: Fetching presale status";
                    $xml_data['step'.++$i]["data"] = $i.". Failed : Fetching presale status";
                    $returnArr["errCode"] = 4;
                    $returnArr["errMsg"] = $errMsg;
                }
            }else{
                $errMsg = "Error: Fetching cart total price";
                $xml_data['step'.++$i]["data"] = $i.". Failed : Fetching cart total price";
                $returnArr["errCode"] = 3;
                $returnArr["errMsg"] = $errMsg;
            }
        }else{
            $errMsg = "Error: Database connection";
            $xml_data['step'.++$i]["data"] = $i.". Failed : Database connection";
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = $errMsg;
        }

    }else{
        $errMsg = "Not a valid user";
        $xml_data['step'.++$i]["data"] = $m.". Failed : Not a valid user";
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $errMsg;
    }

    // create or update xml log Files
    $xmlProcessor->writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

    echo json_encode($returnArr); die;

}else if($returnURl == $_SESSION['redirect_url']){

    $stepCounter = $_SESSION["xml_step"];
    $security_type = $_SESSION['security_type'];
    $form_data = json_decode($_SESSION['form_data'], true);
    $activity_id = $_SESSION['activity_id'];
    $xml_file = $_SESSION['xml_file'];

    $mode    = cleanXSS(urldecode($form_data['mode']));
    $clientSid    = cleanXSS(urldecode($form_data['client_sid']));
    $termsFlag    = cleanXSS(urldecode($form_data['termsFlag']));
    $keywordBasket    = rtrim(cleanXSS($form_data['keywordBasket']),',');
    $keywordBasket = "#".$keywordBasket;


    unset($_SESSION['security_type']);
    unset($_SESSION['xml_step']);
    unset($_SESSION['form_data']);
    unset($_SESSION['activity_id']);
    unset($_SESSION['redirect_url']);
    unset($_SESSION['xml_file']);
    unset($_SESSION['verify_status']);

    if ($termsFlag === 'true') {

        $errMsg = "terms and condition accepted";
        $stepCounter++;
        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $kwdDbConn = createDBConnection('dbkeywords');
        /*echo "createDBConnection</br>";
        printArr($kwdDbConn);*/
        if (noError($kwdDbConn)) {
            $kwdDbConn = $kwdDbConn["connection"];

            $errMsg = "keyword database connection success.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $userTotalCartPrice = getUserTotalCartPrice($kwdDbConn, $buyer_id);
            /*echo "getUserTotalCartPrice</br>";
            printArr($userTotalCartPrice);*/
            if (noError($userTotalCartPrice)) {

                $errMsg = "get user total cart price success.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $totalPrice = number_format((float)$userTotalCartPrice["errMsg"]["total_cart_price"],4,'.','');

                if (!isset($_SESSION["email"])) {
                    $errMsg = "user not recognised.";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                } else {
                    $errMsg = "user recognised .";
                    $stepCounter++;
                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /* Check input fields are empty or not */
                    if (is_null($totalPrice) || is_null($keywordBasket) || empty($keywordBasket) || empty($buyer_id)) {

                        $errMsg = "All fields are mandatory";
                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                        $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                        $xml_data["parameters"]["data"] = "";
                        $xml_data['parameters']["attributes"]= $reqArray;

                    } else {

                        $errMsg = "Field validation success.";
                        $stepCounter++;
                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                        $xml_data["parameters"]["data"] = "";
                        $xml_data['parameters']["attributes"]= $reqArray;

                        //Get Presale Status
                        $presaleStatus = getPresaleStatus($kwdDbConn);
                        /*echo "getPresaleStatus</br>";
                        printArr($presaleStatus);*/
                        $presaleStatus = $presaleStatus["errMsg"]["presale_status"];
                        if ($presaleStatus != 0) {
                            $errMsg = "Presale status is active.";
                            $stepCounter++;
                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            /* Get buyer reference details */
                            $buyerRequireFields = "ref_email,system_mode,currencyPreference";
                            $buyerDetails = getUserInfo($buyer_id, $walletURLIPnotification . 'api/notify/v2/', $buyerRequireFields);
                            /*echo "getUserInfo</br>";
                            printArr($buyerDetails);*/
                            if(noError($buyerDetails))
                            {
                                $errMsg = "getting buyer reference details success.";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                //$buyerDetails     = json_decode($buyerDetails, TRUE);
                                $buyerRefferedBy = $buyerDetails["errMsg"]["ref_email"];

                                // get buyer details with available balance if login
                                $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";
                                $buyerBalanceDetails = getUserInfo($buyer_id, $walletURLIP . 'api/v3/', $buyerBalanceFields);
                                /* echo "buyer getUserInfo</br>";
                                 printArr($buyerBalanceDetails); die;*/
                                if (noError($buyerBalanceDetails)) {

                                    $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                                    $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];

                                    $userBalance = calculateUserBalance($buyerBalanceDetails);
                                    /*echo "calculateUserBalance</br>";
                                    printArr($userBalance);*/
                                    if (noError($userBalance)) {
                                        $userAvailableBalance = $userBalance['errMsg']['total_available_balance'];
                                        $firstBuyStatus  = $buyerBalanceDetails["errMsg"]["first_buy_status"];
                                    } else {
                                        $userAvailableBalance = "Error";
                                    }

                                    $errMsg = "getting buyer available balance details success.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    /* get referrer details */
                                    // get referrer details with available balance
                                    $referrerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,first_buy_status,social_affiliate_earnings";
                                    $referrerBalanceDetails = getUserInfo($buyerRefferedBy, $walletURLIP . 'api/v3/', $referrerBalanceFields);
                                    /*echo "referral getUserInfo</br>";
                                    printArr($referrerBalanceDetails); die;*/
                                    if(noError($referrerBalanceDetails)){

                                        $errMsg = "getting referral id of referrar user success.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $referrerID = $referrerBalanceDetails['errMsg']['_id'];
                                        $referrerFirstName = $referrerBalanceDetails["errMsg"]["first_name"];
                                        $referrerLastName = $referrerBalanceDetails["errMsg"]["last_name"];
                                    }else{

                                        $errMsg = "getting referral id of referrar user success.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $referrerID = '';
                                    }

                                    $reqArray = array("buyer_id" => urlencode($buyer_id), "buyerBalance" => urlencode($userAvailableBalance), "first_buy_status" => urlencode($firstBuyStatus), "buyerReferral" => urlencode($buyerRefferedBy));
                                    $xml_data["parameters"]["data"] = "";
                                    $xml_data['parameters']["attributes"]= $reqArray;

                                } else {

                                    $errMsg = "getting buyer available balance details failed.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $userAvailableBalance = "Error";
                                }

                                /* Get user admin settings from keyword admin */
                                $adminSettings = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                                /*echo "getAdminSettingsFromKeywordAdmin</br>";
                                printArr($adminSettings);die;*/
                                if(noError($adminSettings))
                                {
                                    $errMsg = "getting admin settings of buyer user success.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];
                                    $affiliateCommision = $adminSettings["data"]["affiliate_earning_percent"];


                                    $reqArray = array("buyer_id" => urlencode($buyer_id), "buyerBalance" => urlencode($userAvailableBalance), "first_buy_status" => urlencode($firstBuyStatus), "first_buy_percent" => urlencode($firstBuyPercent), "buyerReferral" => urlencode($buyerRefferedBy), "affiliate_percent" => urlencode($affiliateCommision),);
                                    $xml_data["parameters"]["data"] = "";
                                    $xml_data['parameters']["attributes"]= $reqArray;

                                    /* Validate user balance */
                                    if ($totalPrice <= $userAvailableBalance) {

                                        //totalPrice less than wallet balance
                                        $errMsg = "check for sufficient wallet balance success.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        //Fetch keywords into an array
                                        $keywordBasket = strip_tags(trim($keywordBasket));
                                        $keywordBasket = preg_replace("/\s\s([\s]+)?/", " ", $keywordBasket);
                                        $keywords      = explode(',', $keywordBasket);

                                        //loop through keywords
                                        $keywords_i = 1;

                                        $errMsg = "Purchase keyword start using loop.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        foreach ($keywords as $keyword) {

                                            $keyword = trim($keyword);
                                            // remove # from word
                                            $keyword=  substr_replace($keyword, '', 0, 1);

                                            $keywordCounter = 0;
                                            //check if keyword is set and not empty
                                            if (isset($keyword) && !empty($keyword)) {

                                                $logmd5kwd=md5($keyword);
                                                // Clean keyword
                                                //$keyword = strip_tags($keyword);
                                                //$keyword = strtolower($keyword);

                                                $errMsg = "start purchase keyword {$keyword}.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                //Get Keyword Ownerhip Table Name
                                                $tableName = getKeywordOwnershipTableName($keyword);

                                                $errMsg = "get keyword ownership tablename for keyword {$keyword}, tablename {$tableName}.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                /*echo "$tableName</br>";
                                                printArr($adminSettings);*/
                                                //Get current Active Slab
                                                $getCurrActiveSlab        = getCurrentActiveSlab($kwdDbConn);
                                                $currentSlabId = $getCurrActiveSlab["errMsg"]["id"];

                                                $errMsg = "get current active slab rate for {$keyword}, Slabrate id {$currentSlabId}.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                /*echo "getCurrentActiveSlab</br>";
                                                printArr($getCurrActiveSlab);*/

                                                $kwd_price_USD     = number_format($getCurrActiveSlab["errMsg"]["ITD_Price"],8);
                                                //convert from USD amount to USD
                                                // get currency exchange rate of usd, sgd and btc
                                                $getExchangeRate = getExchangeRates('BTC');
                                                /*echo "getExchangeRates</br>";
                                                printArr($getExchangeRate);die;*/
                                                if (noError($getExchangeRate)) {
                                                    $getExchangeRate = $getExchangeRate['currRate'];

                                                    $errMsg = "get current currency exchange rate ". json_encode($getExchangeRate);
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                }

                                                $kwd_sold      = $getCurrActiveSlab["errMsg"]["kwd_sold"];

                                                if ($firstBuyStatus) {
                                                    $firstBuyCashBack = number_format((float)$kwd_price_USD * ($firstBuyPercent / 100),8,'.','');

                                                    $errMsg = "calculate first buy cashback amount {$firstBuyCashBack}.";
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                }

                                                $trans = startTransaction($kwdDbConn);
                                                /*echo "startTransaction</br>";
                                                printArr($trans);*/
                                                if (noError($trans)) {
                                                    $errMsg = "start transaction success.";
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                    //transaction started
                                                    //Insert into ownership tables
                                                    //Insert into alphabet tables
                                                    $getOwnershipFromCart = getKeywordOwnershipDetailsInCart($kwdDbConn,$keyword);
                                                    /*echo "getKeywordOwnershipDetailsInCart</br>";
                                                    printArr($getOwnershipFromCart); die;*/
                                                    $ownershipId = $getOwnershipFromCart["errMsg"]["buyer_id"];
                                                    $ownershipStatus = $getOwnershipFromCart["errMsg"]["status"];
                                                    if($ownershipId == $buyer_id && $ownershipStatus =="inCart"){

                                                        $errMsg = "get keyword ownership details from cart success, updating ownership details.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                        $query  = "UPDATE " . cleanQueryParameter($kwdDbConn, $tableName) . " SET purchase_timestamp=NOW(),status='sold',claim_status=1,kwd_price='" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "',payment_mode='ITD',ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $buyer_id) . "' ;";

                                                    }else{
                                                        $query = "insert into " . cleanQueryParameter($kwdDbConn, $tableName) . " (`buyer_id`,`purchase_timestamp`,`keyword`,`status`,`claim_status`,`kwd_price`,`currency`,ownership_expiry_time) values( '";
                                                        $query .= cleanQueryParameter($kwdDbConn, $buyer_id) . "'";
                                                        $query .= ", NOW() ";
                                                        $query .= ",'" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                                        $query .= ",'sold'";
                                                        $query .= ",1";
                                                        $query .= ",'" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "'";
                                                        $query .= ",'" . cleanQueryParameter($kwdDbConn, 'ITD') . "'";
                                                        $intervals='INTERVAL 1 YEAR';
                                                        $query .= ",DATE_ADD(`purchase_timestamp`, $intervals))";

                                                        $errMsg = "get keyword ownership details from cart failed, inserting new ownership details.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                    }

                                                    $result = runQuery($query, $kwdDbConn);

                                                    if (noError($result)) {
                                                        $errMsg = "inserting ownership details success.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                        //Update Pool Distribution
                                                        $addToRecentSold = addToRecentSold($kwdDbConn, $keyword, $kwd_price_USD, $buyer_id);
                                                        /*echo "addToRecentSold</br>";
                                                        printArr($addToRecentSold);*/
                                                        if (noError($addToRecentSold)) {
                                                            $errMsg = "keyword add to recent sold success.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            $kwd_sold = $kwd_sold + 1;

                                                            /* Delete bitgo payment record from payments table after buy keyword from wallet */
                                                            $delrecord = deleteBitgoPaymentRecord($kwdDbConn,$keyword);
                                                            if(noError($delrecord)){
                                                                $errMsg = "Remove bitgo payment record from table success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            }else{
                                                                $errMsg = "Remove bitgo payment record from table failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                            }

                                                        } else {
                                                            $errMsg = "keyword add to recent sold failed.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                        }

                                                        // remove keyword from suggested table.
                                                        $removeSuggestedKeyword = deleteSuggestedKeyword($kwdDbConn, $keyword);
                                                        if(noError($removeSuggestedKeyword)){
                                                            $errMsg = "Keyword deleted from suggested keyword : success";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            $kwd_sold = $kwd_sold + 1;

                                                        }else{
                                                            $errMsg = "Keyword deleted from suggested keyword : Failed";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                        }

                                                        //Update Keyword Sold Count Of the current slab
                                                        $query  = "UPDATE `first_purchase_slabs` SET `kwd_sold`='" . cleanQueryParameter($kwdDbConn, $kwd_sold) . "' WHERE `id`='" . cleanQueryParameter($kwdDbConn, $currentSlabId) . "'";

                                                        $result = runQuery($query, $kwdDbConn);
                                                        /*echo "runQuery<br />";
                                                        printArr($result);*/
                                                        if (noError($result)) {
                                                            $errMsg = "Increase keyword sold count in purchase slab success.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                            $noOfVouchers = 1;
                                                            $oneVoucher   = getVoucherForSaleBySlabId($currentSlabId, $noOfVouchers, $kwdDbConn);
                                                            $voucher_id   = $oneVoucher["errMsg"][0]["voucher_id"];
                                                            $voucher_code = $oneVoucher["errMsg"][0]["voucher_code"];

                                                            /*echo "getVoucherForSaleBySlabId</br>";
                                                            printArr($oneVoucher);*/

                                                            //update mykeyword table
                                                            $purchase_timestamp      = date("j F, Y");
                                                            $payment_mode            = "wallet";
                                                            $updatemykeyword_details = updateMykeywordDetails($kwdDbConn, $buyer_id, $purchase_timestamp, $keyword, $kwd_price_USD, $voucher_code, $payment_mode, $fund_amount);

                                                            /*echo "updateMykeywordDetails</br>";
                                                            printArr($updatemykeyword_details);*/
                                                            if (noError($updatemykeyword_details)) {
                                                                $errMsg = "update my keyword details success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            } else {
                                                                $errMsg = "update my keyword details failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                            }
                                                            //update used status for voucher_code
                                                            $query  = "UPDATE presale_vouchers SET voucher_status='used' WHERE voucher_id='" . cleanQueryParameter($kwdDbConn, $voucher_id) . "';";

                                                            $result = runQuery($query, $kwdDbConn);
                                                            /*echo "runQuery<br />";
                                                            printArr($result);*/

                                                            if (noError($result)) {
                                                                $errMsg = "update presale voucher status as used : success.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                //update keyword ownership table for voucher_code
                                                                $query  = "UPDATE " . cleanQueryParameter($kwdDbConn, $tableName) . " SET voucher_allocated='" . cleanQueryParameter($kwdDbConn, $voucher_id) . "' WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $buyer_id) . "' ;";
                                                                $result = runQuery($query, $kwdDbConn);
                                                                /*echo "runQuery</br>";
                                                                printArr($result);*/

                                                                if (noError($result)) {
                                                                    $errMsg = "update voucher status in ownership details  : success, voucher code {$voucher_code}.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                    //Clear Keyword From User Cart
                                                                    $result = removeKeywordFromCart(html_entity_decode($keyword), $buyer_id, $kwdDbConn, $keywordCounter, $xml_data);

                                                                    /*echo "removeKeywordFromCart</br>";
                                                                    printArr($result);*/
                                                                    if (noError($result)) {
                                                                        $errMsg = "removed keyword from cart : success, keyword {$keyword}.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                        //update first buy information
                                                                        if ($firstBuyStatus) {
                                                                            /*echo "update first buy information</br>";
                                                                            printArr($firstBuyStatus);*/
                                                                            $errMsg = "user first buy status true .";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                            if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {

                                                                                $errMsg = "buyer referred by {$buyerRefferedBy} .";
                                                                                $keywordCounter++;
                                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                $firstBuyUpdateStatus = null;
                                                                                $result               = updateFirstBuyStatus($buyerID, '0');
                                                                                /*echo "updateFirstBuyStatus</br>";
                                                                                printArr($result);*/
                                                                                $response             = json_encode($result);
                                                                                if (noError($result)) {
                                                                                    $errMsg = "update buyer first buy preference : success .";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    //success updating first buy status
                                                                                    $firstBuyUpdateStatus = 1;

                                                                                } else {
                                                                                    $errMsg = "update buyer first buy preference : success .";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                    //error updating first buy status
                                                                                    $firstBuyUpdateStatus = 0;
                                                                                }
                                                                                ////
                                                                            } else {
                                                                                $errMsg = "buyer referred by {$buyerRefferedBy} .";
                                                                                $keywordCounter++;
                                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                            }
                                                                        } else {
                                                                            $errMsg = "user first buy status false .";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                        }

                                                                        if (($firstBuyStatus == 1 && $firstBuyUpdateStatus == 1) || $firstBuyStatus == 0) {
                                                                            $errMsg = "first buy status match : success.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                            /* Deduct user balance */
                                                                            $amount = number_format(($kwd_price_USD - $firstBuyCashBack),8);
                                                                            $result   = creditUserEarning($buyerID, $amount, 'purchase', 'add');
                                                                            /*echo "Deduct user balance creditUserEarning</br>";
                                                                            printArr($result);*/
                                                                            $response = json_encode($result);
                                                                            if (noError($result)) {
                                                                                $errMsg = "credit buyer purchase field : success.";
                                                                                $keywordCounter++;
                                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                $reqArray = array("buyer_id" => urlencode($buyer_id), "keyword_price" => urlencode($amount));
                                                                                $xml_data["parameters"]["data"] = "";
                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                //successfully deducted user balance

                                                                                // add to latest trade table
                                                                                //$addToLatestTrade  = addToLatestTrade($conn,$keyword,$buyer_id,$kwd_price);
                                                                                //commit transaction

                                                                                $result = commitTransaction($kwdDbConn);
                                                                                //echo "Deduct user balance commitTransaction</br>";
                                                                                //printArr($result);
                                                                                if (noError($result)) {
                                                                                    $errMsg = "commit transaction : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                } else {

                                                                                    $errMsg = "commit transaction : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                }

                                                                                // Enable database default autocommit functionality
                                                                                $result = mysqli_autocommit($kwdDbConn, TRUE);

                                                                                if (!$result) {
                                                                                    $returnArr["errCode"] = 7;
                                                                                    $returnArr["errMsg"] = "Could not start transaction: ".mysqli_error($kwdDbConn);
                                                                                } else {
                                                                                    $returnArr["errCode"] = -1;
                                                                                    $returnArr["errMsg"] = "Transaction started";
                                                                                }
                                                                                //reset user stats
                                                                                $result   = deleteUserStatsFromRevenueTable($kwdDbConn, $keyword);
                                                                                /*echo "deleteUserStatsFromRevenueTable</br>";
                                                                                printArr($result);*/
                                                                                $response = json_encode($result);
                                                                                if (noError($result)) {
                                                                                    $errMsg = "remove user stats from revenue table: success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    $reqArray = array("Response" => urlencode(json_decode($result)));
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                } else {
                                                                                    $errMsg = "remove user stats from revenue table: failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                    $reqArray = array("Response" => urlencode(json_decode($result)));
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                                }

                                                                                $totalKeywordPrice = ($kwd_price_USD-$firstBuyCashBack);

                                                                                //update keywords purchase amount to community pool user
                                                                                $kwdPurchaseAmount = number_format(($kwd_price_USD-$firstBuyCashBack)/2,8);
                                                                                $exchangeRateInJson = json_encode($getExchangeRate);
                                                                                //$result   = creditKeywordsPoolBalnceWalletAPI($poolAmount);
                                                                                $result   = creditUserEarning($communityPoolUserId, $kwdPurchaseAmount, 'purchase', 'add');
                                                                                /*echo "keywords purchase amount to community pool user creditUserEarning</br>";
                                                                                printArr($result);*/
                                                                                $response = json_encode($result);
                                                                                if (noError($result)) {
                                                                                    //success updating keywords purchase amount to community pool user
                                                                                    $errMsg = "update keyword purchase amount to community pool : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($totalKeywordPrice));
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                    $result   = creditUserEarning($keywoUserId, $kwdPurchaseAmount, 'purchase', 'add');
                                                                                    /*echo "updating keywords purchase amount to keywo user creditUserEarning</br>";
                                                                                    printArr($result);*/
                                                                                    if(noError($result)){
                                                                                        //success updating keywords purchase amount to keywo pool user
                                                                                        $errMsg = "update keyword purchase amount to keywo : success.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($totalKeywordPrice));
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                        /* Set variables for make insert user transaction first buy keyword */
                                                                                        $recipientEmail = $keywoUser;
                                                                                        $recipientUserId = $keywoUserId;
                                                                                        $senderEmail = $communityPoolUser;
                                                                                        $senderUserId = $communityPoolUserId;
                                                                                        $type = 'keyword_purchase';
                                                                                        $paymentMode = 'ITD';
                                                                                        $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy');

                                                                                        $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwdPurchaseAmount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                                                                        /* echo "updating keywords purchase amount to keywo user insertUserTransaction</br>";
                                                                                         printArr($result);*/
                                                                                        $response = json_encode($result);
                                                                                        if(noError($result)){
                                                                                            $errMsg = "transaction for keyword purchase amount to keywo : success.";
                                                                                            $keywordCounter++;
                                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                            $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;
                                                                                        }else{
                                                                                            $errMsg = "transaction for keyword purchase amount to keywo : failed.";
                                                                                            $keywordCounter++;
                                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                            $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;
                                                                                        }

                                                                                    }else{
                                                                                        //error updating keywords purchase amount to keywo pool user
                                                                                        $errMsg = "update keyword purchase amount to community pool : failed.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";


                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                        $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    }

                                                                                    // Update search trade maintenance fees in pool
                                                                                    $pool = feesForPoolMaintenance($kwdPurchaseAmount,$keyword,$buyer_id,$originIp,$exchangeRateInJson,$kwdDbConn);
                                                                                    $poolResponse = json_encode($pool);

                                                                                    if(noError($pool))
                                                                                    {
                                                                                        $errMsg = "update keywo maintainance fees : success.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    }else{
                                                                                        $errMsg = "update keywo maintainance fees : failed.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                        $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    }

                                                                                } else {
                                                                                    //error updating keywords purchase amount to community pool user
                                                                                    $errMsg = "update keyword purchase amount to community pool : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $reqArray = array("Response" => urlencode(json_decode($result)), 'keyword_price' => urlencode($totalKeywordPrice), 'buyer_id' => $buyer_id);
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                                }

                                                                                if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {

                                                                                    $errMsg = "buyer referred id match : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    if ($firstBuyStatus) {

                                                                                        $errMsg = "buyer first buy status active.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $affiliateEarnings = number_format((($kwd_price_USD - $firstBuyCashBack) * $affiliateCommision)/100,8);
                                                                                        //$affiliateEarnings=number_format((float)$affiliateEarnings,8,".","");

                                                                                        $reqArray = array("affiliateEarnings" => urlencode($affiliateEarnings), 'keyword_price' => urlencode($kwd_price_USD), 'buyer_id' => $buyer_id, 'first_buy_amount' => $firstBuyCashBack, 'affiliateComm'=>$affiliateCommision);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    } else {

                                                                                        $errMsg = "buyer first buy status inactive.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $affiliateEarnings = number_format(($kwd_price_USD * $affiliateCommision)/100,8);
                                                                                        //$affiliateEarnings=number_format((float)$affiliateEarnings,8,".","");

                                                                                        $reqArray = array("affiliateEarnings" => urlencode($affiliateEarnings), 'keyword_price' => urlencode($kwd_price_USD), 'buyer_id' => $buyer_id, 'affiliateComm'=>$affiliateCommision);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    }
                                                                                    //update referrer user balance for affiliate earnings
                                                                                    $result   = creditUserEarning($referrerID, $affiliateEarnings, 'affearning', 'add');
                                                                                    /*echo "update referrer user balance for affiliate earnings creditUserEarning</br>";
                                                                                    printArr($result);*/
                                                                                    $response = json_encode($result);
                                                                                    if (noError($result)) {

                                                                                        $errMsg = "credit referral amount to referral user: success.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"] = "{$buyerRefferedBy}";
                                                                                        $kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"] = "{$affiliateEarnings}";


                                                                                        $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $buyerRefferedBy);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                        //successfully updated community pool user balance with affiliate earnings
                                                                                        $result   = creditUserEarning($communityPoolUserId, $affiliateEarnings, 'affearning', 'add');
                                                                                        $response = json_encode($result);
                                                                                        /*echo "updated community pool user balance with affiliate earnings creditUserEarning</br>";
                                                                                        printArr($result);*/
                                                                                        if (noError($result)) {

                                                                                            $errMsg = "credit referral amount to pool: success.";
                                                                                            $keywordCounter++;
                                                                                            $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                            $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $communityPoolUserId);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                                                            /* Set variables for make insert user transaction affiliate earnings */
                                                                                            $recipientEmail = $buyerRefferedBy;
                                                                                            $recipientUserId = $referrerID;
                                                                                            $senderEmail = $communityPoolUser;
                                                                                            $senderUserId = $communityPoolUserId;
                                                                                            $type = 'affiliate_earnings';
                                                                                            $paymentMode = 'ITD';
                                                                                            $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy');

                                                                                            $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $affiliateEarnings, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                            $response = json_encode($result);
                                                                                            if (noError($result)) {

                                                                                                $errMsg = "Insert user transaction for affiliate earnings : success.";
                                                                                                $keywordCounter++;
                                                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                                $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'receiver' => $buyerRefferedBy, 'sender' => $communityPoolUser);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;
                                                                                                //success inserting cashback transaction
                                                                                            } else {
                                                                                                $errMsg = "Insert user transaction for affiliate earnings : failed.";
                                                                                                $keywordCounter++;
                                                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                                $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'receiver' => $buyerRefferedBy, 'sender' => $communityPoolUser);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;
                                                                                                //error inserting cashback transaction
                                                                                            }

                                                                                            //send mail for affiliate earnings to referrer user


                                                                                            //send mail for affiliate earnings
                                                                                            $to                   = $buyerRefferedBy;
                                                                                            $subject              = "Keywo: Refferal earnings notification";
                                                                                            $message              = '<p>Hi, <br/> <br/>Your Referral earnings of ' . $affiliateEarnings . ' '.$keywoDefaultCurrencyName.' has been credited to your Keywo Wallet.<br><br>Please <a href="' . $rootUrl . 'views/prelogin/index.php">login</a> to your Keywo account to access your wallet.</p>';
                                                                                            //Your Referral earnings of 1.23 BTC has been credited to your SearchTrade Wallet
                                                                                            $notification_message = 'Your Referral earnings of ' . $affiliateEarnings . ' '.$keywoDefaultCurrencyName.' has been credited to your Keywo Wallet.';
                                                                                            $permissionCode = 2;
                                                                                            $path     = "";
                                                                                            $keyword  = $keyword;

                                                                                            $category = "buy";
                                                                                            $sendNotifEamil = sendNotificationBuyPrefrence($to, $subject, $message, $referrerFirstName, $referrerLastName, $referrerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                                            if(noError($sendNotifEamil)){
                                                                                                $errMsg = "Send referral earning notification and email : success.";
                                                                                                $keywordCounter++;
                                                                                                $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                            }else{
                                                                                                $errMsg = "Send referral earning notification and email : failed.";
                                                                                                $keywordCounter++;
                                                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                            }

                                                                                        } else {
                                                                                            $errMsg = "credit referral amount to pool: failed.";
                                                                                            $keywordCounter++;
                                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                            $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $communityPoolUserId);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;
                                                                                            //error updating affiliate pool
                                                                                        }
                                                                                    } else {
                                                                                        $errMsg = "credit referral amount to referral user: failed.";
                                                                                        $keywordCounter++;
                                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                        $reqArray = array("response" => urlencode($result), 'affiliate_amount' => urlencode($affiliateEarnings), 'user' => $buyerRefferedBy);
                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                        $xml_data['parameters']["attributes"]= $reqArray;
                                                                                        //Error updating user balance with affiliate earnings

                                                                                    }

                                                                                } else {
                                                                                    $errMsg = "buyer referred id match : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                }

                                                                                /* Set variables for make insert user transaction first buy keyword */
                                                                                $recipientEmail = $communityPoolUser;
                                                                                $recipientUserId = $communityPoolUserId;
                                                                                $senderEmail = $buyer_id;
                                                                                $senderUserId = $buyerID;
                                                                                $type = 'keyword_purchase';
                                                                                $paymentMode = 'ITD';
                                                                                $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy');

                                                                                $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$kwdDbConn);
                                                                                if(noError($checkForKeywordAvailability))
                                                                                {
                                                                                    $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                                                                    $keywordPrice                = $checkForKeywordAvailability["kwd_price"];

                                                                                }
                                                                                //insert keyword purchase transaction
                                                                                $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwd_price_USD, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                                                              /* echo "insert keyword purchase transaction insertUserTransaction</br>";
                                                                                printArr($result);die;*/
                                                                                $response = json_encode($result);
                                                                                if (noError($result)) {

                                                                                    $errMsg = "transaction for keyword purchase from admin : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'keyword' => urlencode($keyword), 'buyer_id' => $buyer_id, 'payment_mode'=> 'wallet');
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                    //success inserting cashback transaction
                                                                                } else {

                                                                                    $errMsg = "transaction for keyword purchase from admin : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                    $reqArray = array("Response" => urlencode($result), 'keyword_price' => urlencode($totalKeywordPrice), 'keyword' => urlencode($keyword), 'buyer_id' => $buyer_id, 'payment_mode'=> 'wallet');
                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                    $xml_data['parameters']["attributes"]= $reqArray;
                                                                                    //error inserting cashback transaction
                                                                                }

                                                                                $kwdPurchaseChild["errCode"]=-1;
                                                                                $kwdPurchaseChild["errMsg"]="Keyword Purchase Success";

                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;


                                                                                //send keyword purchase mail to buyer
                                                                                $to                   = $buyer_id;
                                                                                $subject              = "Keyword purchase success";
                                                                                $message              =

                                                                                    '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                                    <tr> <td style="padding: 10px 20px;">
                                                                                       Congratulations! You have successfully purchased <b> #'.$keyword.'</b> keyword for a total sum of ' . number_format((float)$keywordPrice, 4) . ' '.$keywoDefaultCurrencyName.'. from Keyword market. Now start earning on ' . $keyword . ' everytime a post mentioning it is liked.<br>If you have any questions or concerns, please feel free to contact us at support@keywo.com.
                                                                                    <br><br>Regards,<br>Team Keywo
                                                                                    </td>  </tr>';

                                                                                $method_name     = "GET";
                                                                                //You have successfully purchased the license for #keyword via SearchTrade Wallet
                                                                                $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Keywo Wallet.';

                                                                                //  $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                                                                                //  $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];

                                                                                $getNotification = notifyoptions($buyerID, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                                                $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                                                                                $permissionCode1 = $getNotification["errMsg"]["notify_options_fk_key"]["buy_opt_container"]["2"]["permissions"]["_id"];

                                                                                $path    = "";
                                                                                $keyword = $keyword;
                                                                                //SendMail($to, $subject, $message, $path, $keyword);

                                                                                $category   = "buy";
                                                                                $linkStatus = 1;
                                                                                $notiSend   = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                                $subject  = "Keyword purchase success";
                                                                                $message  = '<p>Hi '.$username.', <br/> <br/>You have successfully purchased the license for <b>#' . $keyword . '</b> via Keywo Wallet.</p>';
                                                                                $category = "buy";

                                                                                $linkStatus           = "";
                                                                                $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Keywo Wallet.';
                                                                                $sendNotifEmailToBuyer = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode1, $category, $linkStatus);
                                                                                if(noError($sendNotifEmailToBuyer)){
                                                                                    $errMsg = "Send email and notification to ".$buyer_id." : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                }else{
                                                                                    $errMsg = "Send email and notification to ".$buyer_id." : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                }

                                                                                //send ownership zip mail to admin
                                                                                $sendOwnershipZip =  sendKwdOwnershipZipToAdmin($keyword, $kwdDbConn,"purchase");
                                                                                if(noError($sendOwnershipZip)){ //Successfully send ownership zip to admin
                                                                                    $errMsg = "Send ownership zip to admin : success.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, -1, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                    $_SESSION["accept_bid_error_code"] = -1;
                                                                                    $_SESSION["accept_bid_error_message"] = "You have successfully brought this keywords.";

                                                                                }else{
                                                                                    $errMsg = "Send ownership zip to admin : failed.";
                                                                                    $keywordCounter++;
                                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                    $_SESSION["get_error_code"] = 9;
                                                                                    $_SESSION["get_error_message"] = "There was an error while buying this keywords";
                                                                                }

                                                                                /******************** for Keyword Statistics ****************/
                                                                                insert_keyword_statistics("total_kwd_sold_by_itd","0",$kwdDbConn);
                                                                                insert_keyword_statistics("total_keyword_sold_amount",$kwd_price_USD,$kwdDbConn);
                                                                                /******************** for Keyword Statistics ****************/

                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;



                                                                                unset($kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"]);
                                                                                unset($kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"]);

                                                                            } else {
                                                                                $errMsg = "credit buyer purchase field : failed.";
                                                                                $keywordCounter++;
                                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";

                                                                                $reqArray = array("buyer_id" => urlencode($buyer_id), "keyword_price" => urlencode($totalKeywordPrice));
                                                                                $xml_data["parameters"]["data"] = "";
                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                //rollbackTransaction
                                                                                $trans = rollbackTransaction($kwdDbConn);

                                                                                $errorMessage = "There was an error while buying this keyword";
                                                                                $returnArr['errCode'] = 2;
                                                                                $returnArr['errMsg']  = $errorMessage;
                                                                            }

                                                                        } else {
                                                                            //rollbackTransaction
                                                                            $errMsg = "first buy status match : failed.";
                                                                            $keywordCounter++;
                                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                            $trans = rollbackTransaction($kwdDbConn);

                                                                            $errorMessage = "There was an error while buying this keyword";
                                                                            $returnArr['errCode'] = 2;
                                                                            $returnArr['errMsg']  = $errorMessage;
                                                                        }


                                                                    } else {
                                                                        $errMsg = "removed keyword from cart : failed, keyword {$keyword}.";
                                                                        $keywordCounter++;
                                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                        //Buy keyword error
                                                                        $trans = rollbackTransaction($kwdDbConn);

                                                                        $errorMessage = "There was an error while buying this keyword";
                                                                        $returnArr['errCode'] = 2;
                                                                        $returnArr['errMsg']  = $errorMessage;

                                                                    }

                                                                } else {
                                                                    $errMsg = "update voucher status in ownership details : failed, voucher code {$voucher_code}.";
                                                                    $keywordCounter++;
                                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    //Buy keyword error
                                                                    $trans = rollbackTransaction($kwdDbConn);

                                                                    $errorMessage = "There was an error while buying this keyword";
                                                                    $returnArr['errCode'] = 2;
                                                                    $returnArr['errMsg']  = $errorMessage;
                                                                }

                                                            } else {
                                                                $errMsg = "update presale voucher status : failed.";
                                                                $keywordCounter++;
                                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                //Buy keyword error
                                                                $trans = rollbackTransaction($kwdDbConn);

                                                                $errorMessage = "There was an error while buying this keyword";
                                                                $returnArr['errCode'] = 2;
                                                                $returnArr['errMsg']  = $errorMessage;

                                                            }

                                                        } else {
                                                            $errMsg = "Increase keyword sold count in purchase slab failed.";
                                                            $keywordCounter++;
                                                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                            $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                            //Buy keyword error
                                                            $trans = rollbackTransaction($kwdDbConn);
                                                            $errorMessage = "There was an error while buying this keyword";
                                                            $returnArr['errCode'] = 2;
                                                            $returnArr['errMsg']  = $errorMessage;

                                                        }

                                                    } else {
                                                        $errMsg = "inserting ownership details failed.";
                                                        $keywordCounter++;
                                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                        $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                        //Buy keyword error
                                                        $trans = rollbackTransaction($kwdDbConn);

                                                        $errorMessage = "There was an error while buying this keyword";
                                                        $returnArr['errCode'] = 2;
                                                        $returnArr['errMsg']  = $errorMessage;
                                                    }

                                                } else {
                                                    $errMsg = "start transaction failed.";
                                                    $keywordCounter++;
                                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                    $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                    //System Error.Please try again after some time.
                                                    $errorMessage = "System Error.Please try again after some time";
                                                    $returnArr['errCode'] = 2;
                                                    $returnArr['errMsg']  = $errorMessage;

                                                }

                                            } else {
                                                $errMsg = "No keyword in cart.";
                                                $keywordCounter++;
                                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                                $kwdPurchaseChild["step{$keywordCounter}"] = "{$keywordCounter}. {$errMsg}";
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                $returnArr['errCode'] = 2;
                                                $returnArr['errMsg']  = $errMsg;
                                            }

                                            $keywords_i++;

                                        }

                                    } else {

                                        $errMsg = "user wallet balance is insufficient.";
                                        $stepCounter++;
                                        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        //totalPrice greater than wallet price
                                        $returnArr['errCode'] = 2;
                                        $returnArr['errMsg']  = $errMsg;
                                    }

                                }else{
                                    $errMsg = "getting admin settings of buyer user failed.";
                                    $stepCounter++;
                                    $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $returnArr['errCode'] = 2;
                                    $returnArr['errMsg']  = $errMsg;

                                }

                            }else{
                                $errMsg = "getting buyer reference details failed.";
                                $stepCounter++;
                                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $returnArr['errCode'] = 2;
                                $returnArr['errMsg']  = $errMsg;
                            }

                        } else {
                            $errMsg = "Presale status is inactive.";
                            $stepCounter++;
                            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            //Presale is paused
                            $returnArr['errCode'] = 2;
                            $returnArr['errMsg']  = $errMsg;
                        }
                    }

                }

            } else {
                $errMsg = "Error getting cart total price from Db.";
                $stepCounter++;
                $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                $errorMessage = "Error getting cart total price from Db";

                $returnArr['errCode'] = 2;
                $returnArr['errMsg']  = $errMsg;
            }
        }else{
            $errMsg = "keyword database connection failed.";
            $stepCounter++;
            $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $returnArr['errCode'] = 2;
            $returnArr['errMsg']  = $errMsg;
        }
    }else{
        $errMsg = "denied term and condition";
        $stepCounter++;
        $retArray = setErrorStack($retArray, 2, $errMsg, $extraArgs);
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $returnArr['errCode'] = 2;
        $returnArr['errMsg']  = $errMsg;
    }

    //$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

    $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

    header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

} else {
    unset($_SESSION['security_type']);
    unset($_SESSION['xml_step']);
    unset($_SESSION['form_data']);
    unset($_SESSION['activity_id']);
    unset($_SESSION['redirect_url']);
    unset($_SESSION['xml_file']);
    unset($_SESSION['verify_status']);

    /*$_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";*/
    $returnArr["errCode"] = 2;

  //  $xmlProcessor-> writeXML($xmlfilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

    header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');
}


/*$returnArr["errCode"] = $returnArr['errCode'];
$returnArr["errMsg"]  = $returnArr['errMsg'];*/

if($returnArr["errCode"]  == -1){
    $returnArr["errCode"]  = -1;
    $returnArr['errMsg'] = "You have successfully brought this keyword";
    $returnArr['redirect_url'] = "{$redirectUrl}user_dashboard/owned_keyword.php";

    $_SESSION["accept_bid_error_code"] = -1;
    $_SESSION["accept_bid_error_message"] = "You have successfully brought this keywords.";

   // $xmlProcessor-> writeXML($xmlfilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

    header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

}else{
    $returnArr["errCode"] = $returnArr['errCode'];
    $returnArr['errMsg'] = $returnArr['errMsg'];

    $_SESSION["get_error_code"] = $returnArr["errCode"];
    $_SESSION["get_error_message"] = $returnArr['errMsg'];

  //  $xmlProcessor-> writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

    header('Location:'.$rootUrl.'views/keywords/marketplace/keyword_search.php');
}

// Write response attribute in XML file */
/*$xml_resp_atrr['response'] = json_encode($returnArr);

$xml_data['response']["data"] ="";
$xml_data['response']["attribute"] =$xml_resp_atrr;

$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);



echo json_encode($returnArr);*/

?>