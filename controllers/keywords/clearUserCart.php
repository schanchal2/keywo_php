<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/transactionHelper.php');
require_once('../../models/keywords/userCartModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

error_reporting(0);

$email = $_SESSION["email"];
$clientSessionIdFromCart = urldecode($_POST["clinetSessionId"]);

// get current session
$currentSessionId = session_id();

$returnArr = array();
$extraArgs = array();

//for xml writing essential
$xmlProcessor = new xmlProcessor();

// inititalize xml logs activity
$xmlArray = initializeXMLLog('');

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xmlfilename = "clear_cart.xml";

$xml_data['step']["data"] = $title;

$logStorePath = $logPath["cart"] ;

$i = 0;


if($clientSessionIdFromCart == $currentSessionId){

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'. Validate client session : success';

    if(isset($email) && !empty($email)){

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Validate session email : success';

      // create databas connection
      $kwdDbConn = createDBConnection("dbkeywords");
      if(noError($kwdDbConn)){
          $kwdDbConn = $kwdDbConn["connection"];

          $i = $i + 1;
          $xml_data["step".$i]["data"] = $i.'. Keyword database connection : success';

          // check email existence
          $checkUserExistence = checkEmailExistance($email);
          if(noError($checkUserExistence)){

              $i = $i + 1;
              $xml_data["step".$i]["data"] = $i.'. '.$email.' is a registered user : success';

              $clearCart = clearAllKeywordFromCart($email, $kwdDbConn, $xml_data, $i);

              if($clearCart){
                  $i = $clearCart["stepCounter"];
                  $xml_data = $clearCart["xml_data"];
                  $errMsg = $clearCart["errMsg"];
                  $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

                  /******************** for logs user analytics ****************/
                  $responseArr["errCode"]="-1";
                  $responseArr["errMsg"]= "User cart clear successfully";
                  $xml_data['response']["data"] = "";
                  $xml_data['response']["attributes"] = $responseArr;
                  /******************** for logs user analytics ****************/

              }else{

                  $i = $clearCart["stepCounter"];
                  $xml_data = $clearCart["xml_data"];
                  $errMsg = $clearCart["errMsg"];
                  $returnArr = setErrorStack($returnArr, 5, $errMsg, $extraArgs);

                  /******************** for logs user analytics ****************/
                  $responseArr["errCode"]="5";
                  $responseArr["errMsg"]= "User cart clear : Failed";
                  $xml_data['response']["data"] = "";
                  $xml_data['response']["attributes"] = $responseArr;
                  /******************** for logs user analytics ****************/
              }
          }else{

              $i = $i + 1;
              $xml_data["step".$i]["data"] = $i.'. '.$email.' is not a registered user : failed';

              $errMsg = "User is not registered";
              $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);

              /******************** for logs user analytics ****************/
              $responseArr["errCode"]="4";
              $responseArr["errMsg"]= "User is not registered";
              $xml_data['response']["data"] = "";
              $xml_data['response']["attributes"] = $responseArr;
              /******************** for logs user analytics ****************/
          }
      }else{

          $i = $i + 1;
          $xml_data["step".$i]["data"] = $i.'. Keyword database connection : failed';

          $errMsg = "Error: database connection";
          $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);

          /******************** for logs user analytics ****************/
          $responseArr["errCode"]="3";
          $responseArr["errMsg"]= "Keyword database connection : failed";
          $xml_data['response']["data"] = "";
          $xml_data['response']["attributes"] = $responseArr;
          /******************** for logs user analytics ****************/
      }
    }else{

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Validate session email : failed';

        $errMsg = "Mandatory field not found";
        $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]="2";
        $responseArr["errMsg"]= " Validate session email : failed";
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }
}else{

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'. Validating client session : failed';

    $errMsg = "Invlid session";
    $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]="1";
    $responseArr["errMsg"]= " Validating client session : failed";
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
    /******************** for logs user analytics ****************/
}

// create or update xml log Files
$xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);

?>
