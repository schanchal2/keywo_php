<?php
/**
 *
 * whole file copied from f212571
 * to recover scroll for cart
 *
 */

header("Access-Control-Allow-Origin: *");
session_start();
$docrootpath=$_SERVER["DOCUMENT_ROOT"]."/keywo/";
/*require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");*/

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../models/keywords/userCartModel.php");

error_reporting(0);

$postEmail = urldecode($_POST["user_id"]);

//code to check whether session is running or not
$email = $_SESSION["email"];

// get current client session id
$clientSessionId = session_id();

if($postEmail == $email){

        if(!empty($email)){
        //Keywords database connection
        $kwdConn = createDbConnection('dbkeywords');
        if(noError($kwdConn)){
            $kwdConn = $kwdConn["connection"];
        }else{
            print("Error: Database connection");
            exit;
        }

        $presaleStatus = getPresaleStatus($kwdConn);
        if(noError($presaleStatus)){
            $presaleStatus = $presaleStatus["errMsg"]["presale_status"];
        }else{
            echo "error: Presale status is closed";
        }

        $cartData = getUserTotalCartPrice($kwdConn, $email);
        if(noError($cartData)){
            $cartData = $cartData["errMsg"];
            $cartKeyword = $cartData["keywords"];

            $totalCount = count($cartData["keywords"]);
            $total_cart_price = $cartData["total_cart_price"];

        ?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_keyword.css" type="text/css" />
    <div class="keyword-marketplace">
        <!-- <h3>Marketplace - Buy and Trade Keywords</h3> -->
        <!-- mobile-width starts here -->
        <?php  if($totalCount > 0){ ?>
        <!-- add to cart list starts here -->
        <div class="row">
            <div class="col-xs-12 padding-left-none padding-right-none">
                <div class="add-to-cart-list">
                    <ul>
                        <?php
                                    foreach($cartKeyword as $keyword => $price){
                                    ?>
                            <li>
                                <div class="row" id="loadErrMsg">
                                    <div class="col-xs-12 padding-left-none padding-right-none">
                                        <div class="col-md-7 padding-left-none padding-right-none">
                                            <div class="text-left">
                                                <span class="text-blue keyword-name ellipses margin-bottom-none margin-top-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo "#".$keyword; ?>');" title="" data-toggle="tooltip" data-placement="bottom" href="?q=<?php echo "#".$keyword; ?>" data-original-title="<?php echo "#".$keyword; ?>"><?php echo "#".$keyword; ?></a></span>
                                            </div>
                                        </div>
                                        <!--                                                                    <span class="cart-keyd-name">-->
                                        <?php //echo "#".$keyword; ?>
                                        <!--</span>-->
                                        <button value="Remove" id="crt_cartButton_<?php echo cleanXSS($keyword); ?>" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'crt')" type="button" class="close" data-dismiss="modal">×</button>
                                    </div>
                                    <div class="col-xs-12 padding-left-none padding-right-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Price :
                                                <?php echo ($price != 'NA')? $price." ".$keywoDefaultCurrencyName : $price; ?> </label>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <?php
                                    }
                                    ?>
                    </ul>
                </div>
                <div class="keyword-sub-total">
                    <div class="horizontal-line"></div>
                    <ul>
                        <li>
                            <div class="row">
                                <div class="col-xs-12 padding-left-none padding-right-none">
                                    <span class="cart-keyd-name">SUBTOTAL</span>
                                </div>
                                <div class="col-xs-12 padding-left-none padding-right-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>
                                            <?php echo $total_cart_price." ".$keywoDefaultCurrencyName; ?> </label>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 innerMT padding-left-none padding-right-none">
                                    <a href="<?php echo " {$rootUrl}views/keywords/cart/checkout.php "; ?>">
                                        <input value="Check Out" id="" type="button" class="btn-trading-dark half innerAll">
                                    </a>&nbsp;&nbsp;
                                    <a href="#"><input value="Clear Cart" onclick="clearUserCart('<?php echo $clientSessionId; ?>','<?php echo $email; ?>', '<?php echo $rootUrl; ?>')" id="" type="button" class="btn-trading half innerAll"></a>
                                </div>
                                <!-- clearUserCart() method is an javascript method which is define in header.js file inside js/ directory-->
                            </div>
                            <div class="row">
                                <div class="col-xs-12 padding-left-none padding-right-none inner-2x innerMT">
                                    <span class="cart-keyd-name">Payment Options</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 innerMT padding-left-none padding-right-none">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img src="../../../images/paypal.png">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="../../../images/bitcoin.png">
                                        </div>
                                        <div class="col-md-4">
                                            <img src="../../../images/itd_wallet.png">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <input type="hidden" id="basketCount" value="<?php echo $totalCount; ?>" />
        </div>
        <?php }else{?>
        <div class="row">
            <div class="col-xs-12">
                <h2 class="text-danger inner-3x innerAll">No Keywords</h2>
            </div>
            <div class="col-xs-12">
                <h4 class="text-primary inner-3x innerAll text-center">
                            <strong>
                            <a href="<?php echo $rootUrl; ?>/views/keywords/marketplace/keyword_search.php">Buy Keywords Now</a>
                            </strong>
                            </h4>
            </div>
        </div>
        <?php } ?>
        <!-- mobile-width ends here -->
    </div>
    <!-- container ends here -->
    <!-- place bid popup starts here -->
    <div id="edit-ask" class="modal fade in" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content row">
                <div class="modal-header custom-modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Enquire Now</h4>
                </div>
                <div class="modal-body">
                    content
                </div>
            </div>
        </div>
    </div>
    <!-- place bid popup ends here -->
    </div>
    <?php

        }else{
            print('Error: Fetching cart details');
        }
    } else {
        echo "Session Expired";
    }
}else{
    print('Error: Not a valid user');
}
?>
        <!-- jquery for tootltip -->
        <script>
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
        <!-- jquery for tootltip -->
