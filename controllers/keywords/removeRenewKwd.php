<?php
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();


$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

$returnArr = array();

$email = $_SESSION["email"];
$keyword = urldecode($_POST["keyword"]);
$currCode = strtolower($_SESSION['CurrPreference']);
$keyword = strtolower($keyword);

$totalUPCAmt = 0;
$totalITDAmt = 0;
$totalBTCAmt = 0;

$kwdDbConn = createDBConnection("dbkeywords");

if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
    if(noError($getPresaleDetails)){
        $getPresaleDetails = $getPresaleDetails["errMsg"];

        $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];
        $getRenewKwd = json_decode($getRenewKwd, true);

        if(count($getRenewKwd) > 0){

            if(in_array($keyword, $getRenewKwd)){

                //Remove Keyword From Cart Array
                $getRenewKwd = array_diff($getRenewKwd, array(
                    $keyword
                ));

                $getRenewKwd = json_encode($getRenewKwd,JSON_UNESCAPED_UNICODE);

                $updateRenewKwd = updateRenewKwdInPresale($email, $getRenewKwd, $kwdDbConn);
                if(noError($updateRenewKwd)){

                    // get admin setting details
                    $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                    if(noError($getAdminSetting)){
                        $getAdminSetting = $getAdminSetting["data"];

                        $getRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

                        $getCurrExchangeRate = getExchangeRates($currCode);
                        if(noError($getCurrExchangeRate)){
                            $getExchangeRate = $getCurrExchangeRate["currRate"];

                             // calculate btc amount from renewal amount
                            $btcRate = number_format($getExchangeRate["btc"], 8);

                            $calcBtcOfRenewalAmt =  number_format(($getRenewalAmt*$btcRate), 8);

                            // calculate upc amount from renewal amount.
                            $upcRate = $getExchangeRate[$currCode];
                            $calcUpcRateOfRenewalAmt = number_format(($upcRate * $getRenewalAmt), 8);

                            $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
                            if(noError($getPresaleDetails)){
                                $getPresaleDetails = $getPresaleDetails["errMsg"];
                                $getRenewKwd = json_decode($getPresaleDetails["user_renew_kwd_in_cart"], true);
                                $renewKwdCount = count($getRenewKwd);

                                    ?>
                                    <thead class="">
                                    <tr>

                                        <th>Keyword</th>

                                        <th>
                                            <?php echo $_SESSION['CurrPreference']; ?>

                                        </th>
                                        <th> <?php echo $keywoDefaultCurrencyName; ?> </th>
                                        <th></th>
                                    </tr>


                                    </thead>
                                    <tbody>

                                    <?php

                                    foreach($getRenewKwd as  $keyword){
                                        $renewKwdInCart .= "#".$keyword.",";
                                        ?>

                                        <tr>
                                            <td>
                                                <span><?php echo "#{$keyword}"; ?></span><br>
                                                <label>BTC : <?php echo $calcBtcOfRenewalAmt; ?></label>
                                            </td>
                                            <td>
                                                <label><?php echo number_format($calcUpcRateOfRenewalAmt, 2); ?></label>
                                            </td>
                                            <td>
                                                <label><?php echo number_format($getRenewalAmt, 2); ?></label>
                                            </td>
                                            <td>
                                                <!--  <button type="button" value="Remove" id="cht_cartButton_<?php /*echo cleanXSS($key); */?>" onclick="return addRemoveFromCart('<?php /*echo $email;  */?>' ,'<?php /*echo cleanXSS($key); */?>','<?php /*echo cleanXSS($key); */?>', '<?php /*echo $clientSessionId; */?>', '<?php /*echo $rootUrl; */?>', 'cht')" class="close" data-dismiss="modal">×</button>-->
                                                <button type="button" value="Remove" id="cht_cartButton_<?php echo cleanXSS($keyword); ?>" class="close" onclick="removeRenewKwd('<?php echo cleanXSS($keyword); ?>')" data-dismiss="modal">×</button>

                                            </td>

                                        </tr>

                                        <?php

                                        $totalUPCAmt = $totalUPCAmt + $calcUpcRateOfRenewalAmt;
                                        $totalITDAmt = $totalITDAmt + $getRenewalAmt;
                                        $totalBTCAmt = $totalBTCAmt + $calcBtcOfRenewalAmt;
                                    }

                                    ?>

                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td>
                                            <span><b>SUBTOTAL</b></span><br>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label> BTC: <?php echo $totalBTCAmt; ?></label>
                                        </td>
                                        <td>
                                            <label><?php echo number_format($totalUPCAmt, 2); ?></label>
                                        </td>
                                        <td>
                                            &nbsp; <label><?php echo number_format($totalITDAmt, 2); ?></label>
                                        </td>
                                        <td></td>
                                    </tr>
                                    </tfoot>

                                    <input type="hidden" id="flagCheck" value="0" />
                                    <input type="hidden" id="currentRenewKwdCount" value="<?php echo $renewKwdCount; ?>" />
                                <?php
                            }else{
                                print('Error: Fetching renew keyword details');
                            }
                        }else{
                            print("Error: Fetching currency exchange rate");
                        }

                    }else{
                        print('Error: Fetching admin setting details.');
                    }
                }else{
                    print("Error: Updating renewal keyword");
                }
           }
        }

        $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);
        if(noError($getPresaleDetails)){
            $getPresaleDetails = $getPresaleDetails["errMsg"];

            $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];
            $getRenewKwd = json_decode($getRenewKwd, true);
            if(count($getRenewKwd) == 0){
                ?>
                    <input type="text" id="checkoutCheck" value="99" />
                <?php
            }
        }else{
            print('Error: Fetching user cart details');
        }
    }else{
        print("Error: Fetching renew keyword details");
    }
}else{
    print("Error: Database connection");
}

?>

<script>

    //get keyword unit price

    //get keyword total price in USD
    var  finalITDAmt = <?php echo $totalITDAmt;?>;
    //get keyword basket with tags
    var  kwdRenewInCart = '<?php echo rtrim($renewKwdInCart,','); ?>';


</script>
