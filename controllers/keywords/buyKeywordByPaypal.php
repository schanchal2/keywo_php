<?php


ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/walletHelper.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../helpers/transactionHelper.php");
require_once("../../models/keywords/keywordCdpModel.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");
require_once("../../models/keywords/keywordSearchModel.php");
require_once("../../models/header/headerModel.php");
require_once("../../models/keywords/acceptBidModel.php");
require_once('../smtp/Send_Mail.php');
require_once("../../backend_libraries/xmlProcessor/xmlProcessor.php");
require_once("../../models/analytics/keyword_stat_analytics.php");

error_reporting(0);

$kwdPurchaseLog = $logPath["keywordPurchase"];
$xmlFilename = "payByPaypal.xml";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xmlArray    = initializeXMLLog(trim(urldecode($_SESSION["email"])));
$username    = $_SESSION["first_name"];

$stepCounter = 0;
$originIp         = getClientIP();

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$errMsg = "Start keyword purchase process by paypal.";
$stepCounter++;
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $conn = $conn["connection"];

    $errMsg = "keyword database connection success.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    if(!isset($_SESSION["email"])){
        //Invalid Login*
        $errorMessage        = "Invalid Login";
        $redirectURL = "../../views/keywords/cart/checkout.php";
        print("<script>");
        print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
        print("</script>");

        $errMsg = "Invalid Login.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    }else{
        //Get Input Parameters
        $buyer_id      = $_POST["email"];
        $keywordBasket = $_POST['keywordBasket'];
       // $keywordBasket = "#".$keywordBasket;
        $totalPrice    = $_POST['totalPrice'];
        $sgd           = $_POST['conversionRate'];
        $usd           = $_POST['conversionRateUSD'];

        /*$buyer_id      = "don@grr.la";
        $keywordBasket = "retro";
        $keywordBasket = "#".$keywordBasket;
        $totalPrice    = 20;
        $sgd           = 1.40;
        $usd           = 1;*/

        $upc   = strtolower($_SESSION["CurrPreference"]);
        $buyerID         = $_SESSION['id'];

        $postResponse = json_encode($_POST);


        /************very important for logs************/
        $logbasket=str_replace(","," ",$keywordBasket);
        $logbasket=str_replace("#","",$logbasket);
        $xml_data["keywordBasket"]["data"] = $logbasket;
        /************very important for logs************/



        $errMsg = "Valid Login.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $errMsg = "Post parameteters from paypal response ".$postResponse.".";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        if (is_null($totalPrice) || in_array($totalPrice, $blanks) || is_null($keywordBasket) || in_array($keywordBasket, $blanks) || is_null($buyer_id) || in_array($buyer_id, $blanks)) {

            $errMsg = "Mandatory field not found";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
        }else{

            $errMsg = "Parameters => 1] keyword basket : ".$keywordBasket." 2] Purchase amount in ".$keywoDefaultCurrencyName." : ".$totalPrice." 3] USD Rate : ".$usd." 4] SGD Rate : ".$sgd;
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            //Get Presale Status
            $presaleStatus = getPresaleStatus($conn);
            if(noError($presaleStatus)){
                $presaleStatus = $presaleStatus["errMsg"]["presale_status"];

                $errMsg = "Presale status : ".$presaleStatus;
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                if($presaleStatus != 0){
                    $buyerRequireFields = ",ref_email,system_mode,currencyPreference";
                    $buyerDetails = getUserInfo($buyer_id, $walletURLIPnotification . 'api/notify/v2/', $buyerRequireFields);
                    if(noError($buyerDetails)){
                        $buyerDetails    =  $buyerDetails["errMsg"];
                        $buyerRefferedBy =  $buyerDetails["ref_email"];

                        $errMsg = "Get buyer info : Success, 1] Buyer Referred By: ".$buyerRefferedBy;
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";


                        // get buyer details with available balance if login
                        $buyerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,first_buy_status,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,purchase_itd";
                        $buyerBalanceDetails = getUserInfo($buyer_id, $walletURLIP . 'api/v3/', $buyerBalanceFields);

                        $buyerBalanceDetResponse = json_encode($buyerBalanceDetails);


                        if(noError($buyerBalanceDetails)){
                            $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                            $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];
                            $firstBuyStatus  =  $buyerBalanceDetails["errMsg"]["first_buy_status"];

                            $errMsg = " Get buyer balance details : Success, 1] Buyer first name : ".$buyerFirstName." 2] Buyer last name : ".$buyerLastName." 3] Buyer first buy status : ".$firstBuyStatus;
                            $stepCounter++;
                            $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                            $xml_data["parameters"]["data"] = "";
                            $xml_data['parameters']["attributes"]= $reqArray;

                            $userBalance = calculateUserBalance($buyerBalanceDetails);

                            if(noError($userBalance)){
                                $userAvailableBalance = $userBalance['errMsg']['total_available_balance'];

                                $errMsg = "Calculate available balance of ".$buyer_id." : Success, Available balance : ".$userAvailableBalance;
                                $stepCounter++;
                                $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                                $xml_data["parameters"]["data"] = "";
                                $xml_data['parameters']["attributes"]= $reqArray;

                                // get exchange rate
                                $getExchangeRate = getExchangeRates($upc);
                                if(noError($getExchangeRate)){
                                    $getExchangeRate = $getExchangeRate["currRate"];

                                    $currentSGDRate = $getExchangeRate["sgd"];

                                    $exchangeRateInJson = json_encode($getExchangeRate);

                                    $errMsg = "Fetching current exchange rate : success";
                                    $stepCounter++;
                                    $reqArray = array($getExchangeRate);
                                    $xml_data["parameters"]["data"] = "";
                                    $xml_data['parameters']["attributes"]= $reqArray;

                                    $referrerBalanceFields = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,first_buy_status,search_affiliate_earnings,purchases";
                                    $referrerBalanceDetails = getUserInfo($buyerRefferedBy, $walletURLIP . 'api/v3/', $referrerBalanceFields);

                                    $referrerID = $referrerBalanceDetails['errMsg']['_id'];
                                    $referrerFirstName = $referrerBalanceDetails["errMsg"]["first_name"];
                                    $referrerLastName = $referrerBalanceDetails["errMsg"]["last_name"];

                                    $errMsg = "Get Referral user of ".$buyer_id.", Buyer Referred by : ".$buyerRefferedBy;
                                    $stepCounter++;
                                    $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                                    $xml_data["parameters"]["data"] = "";
                                    $xml_data['parameters']["attributes"]= $reqArray;

                                    /* Get user admin settings from keyword admin */
                                    $adminSettings = getAdminSettingsFromKeywordAdmin($conn);

                                    if(noError($adminSettings)){
                                        $adminSettings = $adminSettings["data"];
                                        $firstBuyPercent = $adminSettings["first_buy_percent"];
                                        $affiliateCommision = $adminSettings["affiliate_earning_percent"];

                                        $errMsg = "Get admin setting details : success";
                                        $stepCounter++;
                                        $reqArray = array("first_buy_percent" => $firstBuyPercent, "affiliate_commision" => $affiliateCommision);
                                        $xml_data["parameters"]["data"] = "";
                                        $xml_data['parameters']["attributes"]= $reqArray;

                                        //Fetch keywords into an array
                                        $keywordBasket = strip_tags(trim($keywordBasket));
                                        $keywordBasket = preg_replace("/\s\s([\s]+)?/", " ", $keywordBasket);
                                        $keywords      = explode(',', $keywordBasket);
                                        // remove emepty value from array

                                        $keywords =  array_values(array_filter($keywords));

                                        //loop through keywords
                                        $keywords_i = 1;

                                        foreach($keywords as $key => $keyword){

                                            $keyword = trim($keyword);
                                            // remove # from word
                                           // $keyword=  substr_replace($keyword, '', 0, 1);

                                            //$keywordCounter = 0;
                                            $keywordCounter = 1;
                                            $keywords_i = 0;

                                            $logmd5kwd=md5($keyword);//for xml logs

                                            if(isset($keyword) && !empty($keyword)){

                                                $errMsg = "start purchase keyword {$keyword}.";
                                                $keywords_i++;
                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                //Get Keyword Ownerhip Table Name
                                                $tableName = getKeywordOwnershipTableName($keyword);

                                                $errMsg = "Keyword ownership table name : ".$tableName;
                                                $keywords_i++;
                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                //Get current Active Slab
                                                $result        = getCurrentActiveSlab($conn);

                                                if(noError($result)){
                                                    $currentSlabId = $result["errMsg"]["id"];
                                                    $kwd_price_USD     = number_format((float)$result["errMsg"]["ITD_Price"],8);
                                                    $kwd_sold      = $result["errMsg"]["kwd_sold"];

                                                    $errMsg = "Get current active slab details : success, 1]Current Slab Id : ".$currentSlabId.", 2] Current keyword price : ".$kwd_price_USD;
                                                    $keywords_i++;
                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                    // $kwd_price = $kwd_price_USD * $currentSGDRate;
                                                    $kwd_price = $kwd_price_USD ;

                                                    if ($firstBuyStatus) {
                                                        $firstBuyCashBack = number_format((float)$kwd_price * ($firstBuyPercent / 100),8);

                                                        $errMsg = "First buy cash back amount : ".$firstBuyCashBack;
                                                        $keywords_i++;
                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";


                                                    }

                                                    // start transaction
                                                    $trans = startTransaction($conn);
                                                    if(noError($trans)){

                                                        $errMsg = "Start transaction : success";
                                                        $keywords_i++;
                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                        $getOwnershipFromCart = getKeywordOwnershipDetailsInCart($conn,$keyword);

                                                        if(noError($getOwnershipFromCart)){
                                                            $getOwnershipFromCart = $getOwnershipFromCart["errMsg"];
                                                            $ownershipId = $getOwnershipFromCart["buyer_id"];
                                                            $ownershipStatus = $getOwnershipFromCart["status"];

                                                            $errMsg = "Get keyword ownership details in cart : success, 1] Ownership Id : ".$ownershipId." 2] Ownership Status : ".$ownershipStatus;
                                                            $keywords_i++;
                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";


                                                            if($ownershipId == $buyer_id && $ownershipStatus =="inCart"){

                                                                $query  = "UPDATE " . cleanQueryParameter($conn, $tableName) . " SET purchase_timestamp=NOW(),status='sold',claim_status=1,kwd_price='" . cleanQueryParameter($conn, $kwd_price_USD) . "',payment_mode='paypal',ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . cleanQueryParameter($conn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($conn, $buyer_id) . "' ;";

                                                            }else{
                                                                $query = "insert into " . cleanQueryParameter($conn, $tableName) . " (`buyer_id`,`purchase_timestamp`,`keyword`,`status`,`claim_status`,``kwd_price`,`currency`,ownership_expiry_time) values( '";

                                                                $query .= cleanQueryParameter($conn, $buyer_id) . "'";
                                                                $query .= ", NOW() ";
                                                                $query .= ",'" . cleanQueryParameter($conn, $keyword) . "'";
                                                                $query .= ",'sold'";
                                                                $query .= ",1";
                                                                $query .= ",'" . cleanQueryParameter($conn, $kwd_price_USD) . "'";
                                                                $query .= ",'" . cleanQueryParameter($conn, 'paypal') . "'";
                                                                $intervals='INTERVAL 1 YEAR';
                                                                $query .= ",DATE_ADD(`purchase_timestamp`, $intervals))";

                                                            }

                                                            $result = runQuery($query, $conn);
                                                            // $result = array("errCode" => -1);

                                                            if(noError($result)){

                                                                $errMsg = "Update record in keyword ownership table : success, 1] Table Name : ".$tableName." 2] Keyword Owner : ".$buyer_id." 3] keyword : ".$keyword;
                                                                $keywords_i++;
                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                $addToRecentSold = addToRecentSold($conn, $keyword, $kwd_price_USD, $buyer_id);

                                                                if (noError($addToRecentSold)) {

                                                                    $kwd_sold = $kwd_sold + 1;

                                                                    $errMsg = "Add keyword in recent sold table : success";
                                                                    $keywords_i++;
                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                    /* Delete bitgo payment record from payments table after buy keyword from paypal */
                                                                    $delrecord = deleteBitgoPaymentRecord($conn,$keyword,'paypal');
                                                                    if(noError($delrecord)){
                                                                        $errMsg = "Remove bitgo payment record from table success.";
                                                                        $keywords_i++;
                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                    }else{
                                                                        $errMsg = "Remove bitgo payment record from table failed.";
                                                                        $keywords_i++;
                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                    }

                                                                } else {
                                                                    $errMsg = "Add keyword in recent sold table : failed";
                                                                    $keywords_i++;
                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $errCode = 21;
                                                                }

                                                                //Update Keyword Sold Count Of the current slab
                                                                 $query  = "UPDATE `first_purchase_slabs` SET `kwd_sold`='" . $kwd_sold. "' WHERE `id`='" . $currentSlabId . "'";

                                                                $result = runQuery($query, $conn);

                                                                if(noError($result)){

                                                                    $errMsg = "Update first purchase slab with keyword sold count : success";
                                                                    $keywords_i++;
                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";


                                                                    $noOfVouchers = 1;
                                                                    $oneVoucher   = getVoucherForSaleBySlabId($currentSlabId, $noOfVouchers, $conn);
                                                                    $voucher_id   = $oneVoucher["errMsg"][0]["voucher_id"];
                                                                    $voucher_code = $oneVoucher["errMsg"][0]["voucher_code"];

                                                                    //update mykeyword table
                                                                    $purchase_timestamp      = date("j F, Y");
                                                                    $payment_mode            = "PayPal";
                                                                    $updatemykeyword_details = updateMykeywordDetails($conn, $buyer_id, $purchase_timestamp, $keyword, $kwd_price_USD, $voucher_code, $payment_mode, $fund_amount);
                                                                    if(noError($updatemykeyword_details)){

                                                                        $errMsg = "Update my keyword details : success";
                                                                        $keywords_i++;
                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                        //update used status for voucher_code
                                                                        $query  = "UPDATE presale_vouchers SET voucher_status='used' WHERE voucher_id='" . cleanQueryParameter($conn, $voucher_id) . "';";

                                                                        $result = runQuery($query, $conn);

                                                                        if(noError($result)){

                                                                            //update keyword ownership table for voucher_code
                                                                            $query  = "UPDATE " . cleanQueryParameter($conn, $tableName) . " SET voucher_allocated='" . cleanQueryParameter($conn, $voucher_id) . "' WHERE keyword='" . cleanQueryParameter($conn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $buyer_id) . "' ;";
                                                                            $result = runQuery($query, $conn);

                                                                            if(noError($result)){

                                                                                //Clear Keyword From User Cart
                                                                                $result = removeKeywordFromCart($keyword, $buyer_id, $conn,$keywordCounter, $xml_data); //  $keywordCounter, $xml_data - dummy in this controllers
                                                                                //   $result = array("errCode" => -1);
                                                                                if(noError($result)){

                                                                                    $errMsg = "Remove #".$keyword." from cart : success";
                                                                                    $keywords_i++;
                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                    if($firstBuyStatus){

                                                                                        if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {

                                                                                            $firstBuyUpdateStatus = null;

                                                                                            $keywordPurAmt = number_format(($kwd_price_USD - $firstBuyCashBack),8);

                                                                                            // Update search trade maintenance fees in pool
                                                                                            $pool = feesForPoolMaintenance($keywordPurAmt,$keyword,$buyer_id,$originIp,$exchangeRateInJson,$conn);

                                                                                            $poolResponse = json_encode($pool);

                                                                                            if(noError($pool))
                                                                                            {
                                                                                                $errMsg = "update keywo maintainance fees : success";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                $result               = updateFirstBuyStatus($buyerID, '0');

                                                                                                /* echo "updateFirstBuyStatus</br>";
                                                                                                 printArr($result); die;*/
                                                                                                $response             = json_encode($result);
                                                                                                if (noError($result)) {
                                                                                                    //success updating first buy status
                                                                                                    $firstBuyUpdateStatus = 1;

                                                                                                    $errMsg = "update first buy status : success";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                } else {
                                                                                                    //error updating first buy status
                                                                                                    $firstBuyUpdateStatus = 0;

                                                                                                    $errMsg = "update first buy status : failed";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;
                                                                                                }
                                                                                            }else{

                                                                                                $errMsg = "update keywo maintainance fees : failed";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                            }
                                                                                        } else {
                                                                                            $errorMessage = "buyer referred by {$buyerRefferedBy} .";
                                                                                        }
                                                                                    }else{
                                                                                        $errorMessage = "user first buy status false ";

                                                                                    }

                                                                                    if (($firstBuyStatus == 1 && $firstBuyUpdateStatus == 1) || $firstBuyStatus == 0) {

                                                                                        /* Deduct user balance */
                                                                                        $amount = number_format(($kwd_price_USD - $firstBuyCashBack),8);

                                                                                        // add purchase amount in user account.
                                                                                        $creditPurchase   = creditUserEarning($buyerID, $amount, 'purchase', 'add');

                                                                                        $creditPurchaseResponse = json_encode($creditPurchase);

                                                                                        if(noError($creditPurchase)){

                                                                                            $errMsg = "Credit purcahase amount  : success";
                                                                                            $keywords_i++;
                                                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                            $reqArray = array("credit_purchase_response" => $creditPurchaseResponse);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                                                            $result = commitTransaction($conn);
                                                                                            if(noError($result)){
                                                                                                $errorMessage = "commit transaction : success";
                                                                                            }else{
                                                                                                $errorMessage = "commit transaction : failed";
                                                                                            }

                                                                                            // Enable database default autocommit functionality
                                                                                            mysqli_autocommit($conn, TRUE);

                                                                                            $result   = deleteUserStatsFromRevenueTable($conn, $keyword);

                                                                                            if(noError($result)){
                                                                                                $errorMessage = "remove user stats from revenue table: success.";
                                                                                            }else{
                                                                                                $errorMessage = "remove user stats from revenue table: failed.";
                                                                                            }

                                                                                            //update keywords purchase amount to community pool user
                                                                                            $kwdPurchaseAmount = number_format(($kwd_price_USD-$firstBuyCashBack)/2,8);


                                                                                            //$result   = creditKeywordsPoolBalnceWalletAPI($poolAmount);
                                                                                            $creditPurchaseAmtToPool   = creditUserEarning($communityPoolUserId, $kwdPurchaseAmount, 'purchase', 'add');

                                                                                            $creditPurhasePoolREsponse = json_encode($creditPurchaseAmtToPool);
                                                                                            if(noError($creditPurchaseAmtToPool)){

                                                                                                $errMsg = "Credit purcahase amount to pool : success";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                $reqArray = array("credit_purchase_response_to_pool" => $creditPurhasePoolREsponse);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                $creditPrchAmtToKeywo   = creditUserEarning($keywoUserId, $kwdPurchaseAmount, 'purchase', 'add');

                                                                                                $creditPurKeywoResponse = json_encode($creditPrchAmtToKeywo);
                                                                                                if(noError($creditPrchAmtToKeywo)){

                                                                                                    $errMsg = "Credit purcahase amount to pool : success";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                    $reqArray = array("credit_purchase_response_to_keywo" => $creditPurKeywoResponse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    /* Set variables for make insert user transaction first buy keyword */
                                                                                                    $recipientEmail = $keywoUser;
                                                                                                    $recipientUserId = $keywoUserId;
                                                                                                    $senderEmail = $communityPoolUser;
                                                                                                    $senderUserId = $communityPoolUserId;
                                                                                                    $type = 'keyword_purchase';
                                                                                                    $paymentMode = 'ITD';
                                                                                                    $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy');

                                                                                                    $result   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwdPurchaseAmount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                                    $insertTrasnForKeywoResponse = json_encode($result);
                                                                                                    if(noError($result)){

                                                                                                        $errMsg = "Insert user transaction for credit keywo purchase amount : success";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                        $reqArray = array("credit_purchase_response_to_keywo" => $insertTrasnForKeywoResponse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;


                                                                                                    }else{
                                                                                                        $errMsg = "Insert user transaction for credit keywo purchase amount : success";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                        $reqArray = array("credit_purchase_response_to_keywo" => $insertTrasnForKeywoResponse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                        $errCode = 30;

                                                                                                        /******************** for logs user analytics ****************/
                                                                                                        $responseArr["errCode"]=$errCode;
                                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                                        $xml_data['response']["data"] = "";
                                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                                        /******************** for logs user analytics ****************/

                                                                                                    }

                                                                                                }else{

                                                                                                    $errMsg = "Credit purcahase amount to keywo : failed";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                    $reqArray = array("credit_purchase_response_to_keywo" => $creditPurKeywoResponse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    $errCode = 29;

                                                                                                    /******************** for logs user analytics ****************/
                                                                                                    $responseArr["errCode"]=$errCode;
                                                                                                    $responseArr["errMsg"]=$errMsg;
                                                                                                    $xml_data['response']["data"] = "";
                                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                                    /******************** for logs user analytics ****************/

                                                                                                }
                                                                                            }else{

                                                                                                $errMsg = "Credit purcahase amount to pool : failed";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                $reqArray = array("credit_purchase_response_to_pool" => $creditPurhasePoolREsponse);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                $errCode = 28;

                                                                                                /******************** for logs user analytics ****************/
                                                                                                $responseArr["errCode"]=$errCode;
                                                                                                $responseArr["errMsg"]=$errMsg;
                                                                                                $xml_data['response']["data"] = "";
                                                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                                                /******************** for logs user analytics ****************/

                                                                                            }

                                                                                            if (isset($buyerRefferedBy) && !empty($buyerRefferedBy)) {
                                                                                                if ($firstBuyStatus) {
                                                                                                    $affiliateEarnings = number_format((($kwd_price_USD - $firstBuyCashBack) * $affiliateCommision)/100,8);
                                                                                                } else {
                                                                                                    $affiliateEarnings = number_format(($kwd_price_USD * $affiliateCommision)/100,8);
                                                                                                }

                                                                                                //update referrer user balance for affiliate earnings
                                                                                                $creditAffiliatePayout   = creditUserEarning($referrerID, $affiliateEarnings, 'affearning', 'add');

                                                                                                $creditAffiliteResponse = json_encode($creditAffiliatePayout);

                                                                                                if(noError($creditAffiliatePayout)){

                                                                                                    $errMsg = "Credit keyword affiliate amount to ".$buyerRefferedBy." : success";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                    $kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"] = "{$buyerRefferedBy}";
                                                                                                    $kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"] = "{$affiliateEarnings}";


                                                                                                    $reqArray = array("credit_purchase_response_to_keywo" => $creditAffiliteResponse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    /* Set variables for make insert user transaction affiliate earnings */
                                                                                                    $recipientEmail = $buyerRefferedBy;
                                                                                                    $recipientUserId = $referrerID;
                                                                                                    $senderEmail = $communityPoolUser;
                                                                                                    $senderUserId = $communityPoolUserId;
                                                                                                    $type = 'affiliate_earnings';
                                                                                                    $paymentMode = 'paypal';
                                                                                                    $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy', "payment_mode" => "paypal");

                                                                                                    $insertUserTransAffiliate   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $affiliateEarnings, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                                    $insertTransaAffiliateREsponse = json_encode($insertUserTransAffiliate);
                                                                                                    if(noError($insertUserTransAffiliate)){

                                                                                                        $errMsg = "Insert user transaction for affiliate earning : success";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                        $reqArray = array("credit_affiliate_response" => $insertTransaAffiliateREsponse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;


                                                                                                        //send mail for affiliate earnings
                                                                                                        $to                   = $buyerRefferedBy;
                                                                                                        $subject              = "Keywo: Refferal earnings notification";
                                                                                                        $message              = '<p>Hi, <br/> <br/>Your Referral earnings of ' . $affiliateEarnings . ' '.$keywoDefaultCurrencyName.' has been credited to your Keywo Wallet.<br><br>Please <a href="' . $rootUrl . 'views/prelogin/index.php">login</a> to your Keywo account to access your wallet.</p>';
                                                                                                        //Your Referral earnings of 1.23 BTC has been credited to your SearchTrade Wallet
                                                                                                        $notification_message = 'Your Referral earnings of ' . $affiliateEarnings . ' '.$keywoDefaultCurrencyName.' has been credited to your Keywo Wallet.';
                                                                                                        $permissionCode = 2;
                                                                                                        $path     = "";
                                                                                                        $keyword  = $keyword;

                                                                                                        $category = "buy";
                                                                                                        $sendNotifEamil = sendNotificationBuyPrefrence($to, $subject, $message, $referrerFirstName, $referrerLastName, $referrerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                                                        $sendEamilRepsonse = json_encode($sendNotifEamil);
                                                                                                        if(noError($sendNotifEamil)){

                                                                                                            $errMsg = "Send email and notification for referral earning : success";
                                                                                                            $keywords_i++;
                                                                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                            $reqArray = array("send_email_notitification_response" => $sendEamilRepsonse);
                                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                        }else{
                                                                                                            $errMsg = "Send email and notification for referral earning : failed";
                                                                                                            $keywords_i++;
                                                                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                            $reqArray = array("send_email_notitification_response" => $sendEamilRepsonse);
                                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                            $errCode = 33;

                                                                                                            /******************** for logs user analytics ****************/
                                                                                                            $responseArr["errCode"]=$errCode;
                                                                                                            $responseArr["errMsg"]=$errMsg;
                                                                                                            $xml_data['response']["data"] = "";
                                                                                                            $xml_data['response']["attributes"] = $responseArr;
                                                                                                            /******************** for logs user analytics ****************/
                                                                                                        }
                                                                                                    }else{

                                                                                                        $errMsg = "Insert user transaction for affiliate earning : failed";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                        $reqArray = array("credit_affiliate_response" => $insertTransaAffiliateREsponse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                        $errCode = 32;

                                                                                                        /******************** for logs user analytics ****************/
                                                                                                        $responseArr["errCode"]=$errCode;
                                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                                        $xml_data['response']["data"] = "";
                                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                                        /******************** for logs user analytics ****************/

                                                                                                    }
                                                                                                }else{

                                                                                                    $errMsg = "Credit keyword affiliate amount to ".$buyerRefferedBy." : failed";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                    $reqArray = array("credit_purchase_response_to_keywo" => $creditAffiliteResponse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    $errCode = 31;

                                                                                                    /******************** for logs user analytics ****************/
                                                                                                    $responseArr["errCode"]=$errCode;
                                                                                                    $responseArr["errMsg"]=$errMsg;
                                                                                                    $xml_data['response']["data"] = "";
                                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                                    /******************** for logs user analytics ****************/
                                                                                                }

                                                                                            }else{
                                                                                                $errorMessage = "buyer referred id match : failed.";
                                                                                            }

                                                                                            $kwdPurchaseChild["errCode"]=-1;
                                                                                            $kwdPurchaseChild["errMsg"]="Keyword Purchase Success";

                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                            /* Set variables for make insert user transaction first buy keyword */
                                                                                            $recipientEmail = $communityPoolUser;
                                                                                            $recipientUserId = $communityPoolUserId;
                                                                                            $senderEmail = $buyer_id;
                                                                                            $senderUserId = $buyerID;
                                                                                            $type = 'keyword_purchase';
                                                                                            $paymentMode = 'paypal';
                                                                                            $metaDetails = array("keyword" => urlencode(utf8_encode($keyword)), "gross_kwd_price" => $kwd_price_USD, "discount" => $firstBuyCashBack, "discount_percentage"=> $firstBuyPercent, "discount_reciepent"=> $buyer_id, "affiliate_user"=> $buyerRefferedBy, "affiliate_percentage"=> $affiliateCommision, "commision" => 0, "buyer_id" => $buyer_id, "raise_on" => 'first buy', "payment_mode" => $paymentMode);

                                                                                            //insert keyword purchase transaction
                                                                                            $insertTransForKwdPurchase   = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $kwdPurchaseAmount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

                                                                                            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$kwdDbConn);
                                                                                            if(noError($checkForKeywordAvailability))
                                                                                            {
                                                                                                $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                                                                                $keywordPrice                = $checkForKeywordAvailability["kwd_price"];

                                                                                            }

                                                                                            $insertTransKwdPurResponse = json_encode($insertTransForKwdPurchase);
                                                                                            if(noError($insertTransForKwdPurchase)){

                                                                                                $errMsg = "Insert user transaction for keyword purchase : success";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                $reqArray = array("insert_trans_for_kwd_purchase" => $insertTransKwdPurResponse);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                //send keyword purchase mail to buyer
                                                                                                $to                   = $buyer_id;
                                                                                                $subject              = "Keyword purchase success";
                                                                                                $message              =
                                                                                                    '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                                                                                    <tr> <td style="padding: 10px 20px;">
                                                                                                       Congratulations! You have successfully purchased <b> #'.$keyword.'</b> keyword for a total sum of ' . number_format((float)$keywordPrice, 4) . ' '.$keywoDefaultCurrencyName.'. from Keyword market. Now start earning on ' . $keyword . ' everytime a post mentioning it is liked.<br>If you have any questions or concerns, please feel free to contact us at support@keywo.com.
                                                                                                    <br><br>Regards,<br>Team Keywo
                                                                                                    </td>  </tr>';
                                                                                                //You have successfully purchased the license for #keyword via SearchTrade Wallet
                                                                                                $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Paypal.';

                                                                                                //  $buyerFirstName = $buyerBalanceDetails["errMsg"]["first_name"];
                                                                                                //  $buyerLastName = $buyerBalanceDetails["errMsg"]["last_name"];
                                                                                                $method_name     = "GET";
                                                                                                $getNotification = notifyoptions($buyerID, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                                                                $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                                                                                                $permissionCode1 = $getNotification["errMsg"]["notify_options_fk_key"]["buy_opt_container"]["2"]["permissions"]["_id"];

                                                                                                $path    = "";
                                                                                                $keyword = $keyword;
                                                                                                //SendMail($to, $subject, $message, $path, $keyword);

                                                                                                $category   = "buy";
                                                                                                $linkStatus = 1;
                                                                                                $notiSend   = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                                                                                $subject  = "Keyword purchase success";
                                                                                                $message  = '<p>Hi '.$username.', <br/> <br/>You have successfully purchased the license for <b>#' . $keyword . '</b> via Paypal.</p>';
                                                                                                $category = "buy";

                                                                                                $linkStatus           = "";
                                                                                                $notification_message = 'You have successfully purchased the license for #' . $keyword . ' via Paypal.';
                                                                                                $sendNotifEmailToBuyer = sendNotificationBuyPrefrence($to, $subject, $message, $buyerFirstName, $buyerLastName, $buyerID, $smsText, $mobileNumber, $notification_message, $permissionCode1, $category, $linkStatus);

                                                                                                $sendNotifRepsonse = json_encode($sendNotifEmailToBuyer);
                                                                                                if(noError($sendNotifEmailToBuyer)){

                                                                                                    $errMsg = "Send email and notification to ".$buyer_id." : success.";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";

                                                                                                    $reqArray = array("send_email_to_buyer" => $sendNotifRepsonse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    //send ownership zip mail to admin
                                                                                                    $sendOwnershipZip =  sendKwdOwnershipZipToAdmin($keyword, $kwdDbConn,"purchase");
                                                                                                    if(noError($sendOwnershipZip)){ //Successfully send ownership zip to admin
                                                                                                        $errMsg = "Send ownership zip to admin : success.";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";


                                                                                                        $reqArray = array("send_email_to_buyer" => $sendNotifRepsonse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                        $errCode = -1;

                                                                                                        /* $errorMessage        = "You have successfully bought this keyword";
                                                                                                         $redirectURL = $rootUrl."views/keywords/user_dashboard/owned_keyword.php";
                                                                                                         $hashTab = "#keywords";
                                                                                                         print("<script>");
                                                                                                         print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
                                                                                                         print("</script>")*/;


                                                                                                        /******************** for logs user analytics ****************/
                                                                                                        $responseArr["errCode"]=$errCode;
                                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                                        $xml_data['response']["data"] = "";
                                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                                        /******************** for logs user analytics ****************/
                                                                                                    }else{
                                                                                                        $errMsg = "Send ownership zip to admin : failed.";
                                                                                                        $keywords_i++;
                                                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                        $reqArray = array("send_email_to_buyer" => $sendNotifRepsonse);
                                                                                                        $xml_data["parameters"]["data"] = "";
                                                                                                        $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                        $errCode = 31;

                                                                                                        /******************** for logs user analytics ****************/
                                                                                                        $responseArr["errCode"]=$errCode;
                                                                                                        $responseArr["errMsg"]=$errMsg;
                                                                                                        $xml_data['response']["data"] = "";
                                                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                                                        /******************** for logs user analytics ****************/
                                                                                                    }

                                                                                                }else{

                                                                                                    $errMsg = "Send email and notification to ".$buyer_id." : success.";
                                                                                                    $keywords_i++;
                                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                    $reqArray = array("send_email_to_buyer" => $sendNotifRepsonse);
                                                                                                    $xml_data["parameters"]["data"] = "";
                                                                                                    $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                    $errCode = 30;

                                                                                                    /******************** for logs user analytics ****************/
                                                                                                    $responseArr["errCode"]=$errCode;
                                                                                                    $responseArr["errMsg"]=$errMsg;
                                                                                                    $xml_data['response']["data"] = "";
                                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                                    /******************** for logs user analytics ****************/
                                                                                                }



                                                                                                /******************** for Keyword Statistics ****************/
                                                                                                insert_keyword_statistics("total_kwd_sold_by_paypal", "0", $conn);
                                                                                                insert_keyword_statistics("total_keyword_sold_amount", $kwd_price_USD, $conn);
                                                                                                /******************** for Keyword Statistics ****************/

                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                unset($kwdPurchaseChild["buyerRefferedBy{$logmd5kwd}"]);
                                                                                                unset($kwdPurchaseChild["affiliateEarnings{$logmd5kwd}"]);

                                                                                            }else{

                                                                                                $errMsg = "Insert user transaction for keyword purchase : failed";
                                                                                                $keywords_i++;
                                                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                                $reqArray = array("insert_trans_for_kwd_purchase" => $insertTransKwdPurResponse);
                                                                                                $xml_data["parameters"]["data"] = "";
                                                                                                $xml_data['parameters']["attributes"]= $reqArray;

                                                                                                $errCode = 29;

                                                                                                /******************** for logs user analytics ****************/
                                                                                                $responseArr["errCode"]=$errCode;
                                                                                                $responseArr["errMsg"]=$errMsg;
                                                                                                $xml_data['response']["data"] = "";
                                                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                                                /******************** for logs user analytics ****************/

                                                                                            }
                                                                                        }else{
                                                                                            $errorMessage = "Error: Credit purchase amount to user account";
                                                                                            $errCode = 27;

                                                                                            $errMsg = "Credit purcahase amount  : failed";
                                                                                            $keywords_i++;
                                                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                            $reqArray = array("credit_purchase_response" => $creditPurchaseResponse);
                                                                                            $xml_data["parameters"]["data"] = "";
                                                                                            $xml_data['parameters']["attributes"]= $reqArray;

                                                                                            /******************** for logs user analytics ****************/
                                                                                            $responseArr["errCode"]="14";
                                                                                            $responseArr["errMsg"]=$errMsg;
                                                                                            $xml_data['response']["data"] = "";
                                                                                            $xml_data['response']["attributes"] = $responseArr;
                                                                                            /******************** for logs user analytics ****************/
                                                                                        }
                                                                                    }else{
                                                                                        $errorMessage = "first buy status match : failed.";

                                                                                        $trans = rollbackTransaction($conn);
                                                                                    }
                                                                                }else{
                                                                                    $errCode = 26;
                                                                                    $trans = rollbackTransaction($conn);

                                                                                    $errMsg = "Remove #".$keyword." from cart : failed";
                                                                                    $keywords_i++;
                                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                                    /******************** for logs user analytics ****************/
                                                                                    $responseArr["errCode"]="13";
                                                                                    $responseArr["errMsg"]=$errMsg;
                                                                                    $xml_data['response']["data"] = "";
                                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                                    /******************** for logs user analytics ****************/
                                                                                }
                                                                            }else{
                                                                                $errorMessage = "Error: Updating voucher details in keyword owner details";
                                                                                $errCode = 25;
                                                                                $trans = rollbackTransaction($conn);
                                                                            }
                                                                        }else{
                                                                            $errorMessage = "Error: Update voucher details";
                                                                            $errCode = 24;
                                                                            $trans = rollbackTransaction($conn);
                                                                        }
                                                                    }else{

                                                                        $errMsg = "Update my keyword details : failed";
                                                                        $keywords_i++;
                                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                        $errCode = 23;
                                                                        $trans = rollbackTransaction($conn);

                                                                        /******************** for logs user analytics ****************/
                                                                        $responseArr["errCode"]="12";
                                                                        $responseArr["errMsg"]=$errMsg;
                                                                        $xml_data['response']["data"] = "";
                                                                        $xml_data['response']["attributes"] = $responseArr;
                                                                        /******************** for logs user analytics ****************/

                                                                    }
                                                                }else{

                                                                    $errMsg = "Update first purchase slab with keyword sold count : success";
                                                                    $keywords_i++;
                                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                    $errCode = 22;
                                                                    $trans = rollbackTransaction($conn);

                                                                    /******************** for logs user analytics ****************/
                                                                    $responseArr["errCode"]="11";
                                                                    $responseArr["errMsg"]=$errMsg;
                                                                    $xml_data['response']["data"] = "";
                                                                    $xml_data['response']["attributes"] = $responseArr;
                                                                    /******************** for logs user analytics ****************/
                                                                }


                                                            }else{

                                                                $errMsg = "Update record in keyword ownership table : failed";
                                                                $keywords_i++;
                                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                                $trans = rollbackTransaction($conn);

                                                                /******************** for logs user analytics ****************/
                                                                $responseArr["errCode"]="10";
                                                                $responseArr["errMsg"]=$errMsg;
                                                                $xml_data['response']["data"] = "";
                                                                $xml_data['response']["attributes"] = $responseArr;
                                                                /******************** for logs user analytics ****************/
                                                                $errCode = 21;

                                                            }

                                                        }else{

                                                            $errMsg = "Get keyword ownership details in cart : failed";
                                                            $keywords_i++;
                                                            $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                            $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                            /******************** for logs user analytics ****************/
                                                            $responseArr["errCode"]="9";
                                                            $responseArr["errMsg"]=$errMsg;
                                                            $xml_data['response']["data"] = "";
                                                            $xml_data['response']["attributes"] = $responseArr;
                                                            /******************** for logs user analytics ****************/

                                                            $errCode = 20;
                                                        }
                                                    }else{
                                                        $errMsg = "Start transaction : failed";
                                                        $keywords_i++;
                                                        $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                        $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                        /******************** for logs user analytics ****************/
                                                        $responseArr["errCode"]="8";
                                                        $responseArr["errMsg"]=$errMsg;
                                                        $xml_data['response']["data"] = "";
                                                        $xml_data['response']["attributes"] = $responseArr;
                                                        /******************** for logs user analytics ****************/

                                                        $errCode = 19;
                                                    }



                                                }else{

                                                    $errMsg = "Get current active slab details : failed";
                                                    $keywords_i++;
                                                    $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                    $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                    /******************** for logs user analytics ****************/
                                                    $responseArr["errCode"]="7";
                                                    $responseArr["errMsg"]=$errMsg;
                                                    $xml_data['response']["data"] = "";
                                                    $xml_data['response']["attributes"] = $responseArr;
                                                    /******************** for logs user analytics ****************/

                                                    $errCode = 18;
                                                }

                                            }else{

                                                $errMsg = "No keyword in cart.";
                                                $keywords_i++;
                                                $kwdPurchaseChild["step{$keywords_i}"] = "{$keywords_i}. {$errMsg}";
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["data"] = '';
                                                $xml_data["kwywordPurchase{$logmd5kwd}"]["childelement"] = $kwdPurchaseChild;

                                                /******************** for logs user analytics ****************/
                                                $responseArr["errCode"]="6";
                                                $responseArr["errMsg"]=$errMsg;
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;
                                                /******************** for logs user analytics ****************/

                                                $errCode = 17;

                                            }
                                            $keywords_i = 0;
                                            $keywordCounter++;
                                        }
                                    }else{

                                        $errMsg = "Get admin setting details : failed";
                                        $stepCounter++;
                                        $reqArray = array("first_buy_percent" => $firstBuyPercent, "affiliate_commision" => $affiliateCommision);
                                        $xml_data["parameters"]["data"] = "";
                                        $xml_data['parameters']["attributes"]= $reqArray;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]="7";
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/

                                        $errCode = 16;

                                    }

                                }else{

                                    $errMsg = "Fetching current exchange rate : failed";
                                    $stepCounter++;
                                    $reqArray = array($getExchangeRate);
                                    $xml_data["parameters"]["data"] = "";
                                    $xml_data['parameters']["attributes"]= $reqArray;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]="6";
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/

                                    $errCode = 15;
                                }


                            }else{
                                $errMsg = "Calculate available balance of ".$buyer_id." : failed";
                                $stepCounter++;
                                $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                                $xml_data["parameters"]["data"] = "";
                                $xml_data['parameters']["attributes"]= $reqArray;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]="5";
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                                $errCode = 14;
                            }
                        }else{


                            $errMsg = " Get buyer balance details : Failed";
                            $reqArray = array("buyer_id" => urlencode($buyer_id), "keywords" => urlencode($keywordBasket), "totalPrice" => urlencode($totalPrice));
                            $xml_data["parameters"]["data"] = "";
                            $xml_data['parameters']["attributes"]= $reqArray;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]="4";
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

                            $errCode = 13;
                        }
                    }else{
                        $errMsg = "Get buyer info : Failed, 1] Buyer Referred By: ".$buyerRefferedBy;
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]="3";
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/

                        $errCode = 12;
                    }
                }else{

                    $errMsg = "Presale is paused";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]="3";
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/

                    $errCode = 11;
                }
            }else{

                $errMsg = "Fetching presale status: failed";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="2";
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/

                $errCode = 10;
            }

        }
    }

}else{
    print('Database error');

    $errMsg = "keyword database connection failed.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]="1";
    $responseArr["errMsg"]=$errMsg;
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
    /******************** for logs user analytics ****************/

    $errCode = 9;
}

// create or update xml log Files
$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

if($errCode == -1){


    $errorMessage        = "You have successfully bought this keyword";
    $redirectURL = $rootUrl."views/keywords/user_dashboard/owned_keyword.php";
    $hashTab = "#keywords";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}else{
    $redirectURL = "../../views/keywords/cart/checkout.php";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}




?>