<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 2/6/17
 * Time: 2:40 PM
 */
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

$docRootPath=explode("controllers/", dirname(__FILE__))[0];

require_once "{$docRootPath}config/config.php";
require_once "{$docRootPath}config/db_config.php";
require_once("{$docRootPath}helpers/arrayHelper.php");
require_once("{$docRootPath}helpers/deviceHelper.php");
require_once("{$docRootPath}helpers/coreFunctions.php");
require_once("{$docRootPath}helpers/transactionHelper.php");
require_once("{$docRootPath}helpers/walletHelper.php");
require_once("{$docRootPath}helpers/errorMap.php");
require_once("{$docRootPath}helpers/stringHelper.php");
require_once("{$docRootPath}helpers/transactionHelper.php");
require_once("{$docRootPath}models/keywords/keywordPurchaseModel.php");
require_once("{$docRootPath}models/keywords/userCartModel.php");
require_once("{$docRootPath}backend_libraries/xmlProcessor/xmlProcessor.php");

$retArray = array();
$stepCounter = 0;

$request = $_POST['type'];
$page = $_POST['page'];
$sid  = cleanXSS(trim(urldecode($_POST['sid'])));
$currSid = session_id();

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlArray    = initializeXMLLog(trim(urldecode($buyer_id)));
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$kwdPurchaseLog = $logPath["keywordPurchase"];
$xmlFilename = "claimKeywords.xml";

$errMsg = "Start keyword claim process by old user.";
$stepCounter++;
$xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    if($currSid == $sid){

        if(isset($_POST) && !empty($_POST)){

            $email = $_SESSION['email'];
            $userId = $_SESSION['id'];

            $errMsg = "Success: session is active";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $kwdDbConn = createDBConnection("dbkeywords");
            if(noError($kwdDbConn)){
                $kwdDbConn = $kwdDbConn["connection"];

                $errMsg = "Success: database connection";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                if($request == 'claim'){

                    if(!isset($_SESSION['claimStatus'])){

                        $errMsg = "Success: Start claim user process";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
                        if(noError($getMyKeywordDetails)){

                            $errMsg = "Success: get user keyword details";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
                            $buyer_id = $getMyKeywordDetails['buyer_id'];
                            $myKeywords = $getMyKeywordDetails["transaction_details"];
                            $myKeywords = json_decode($myKeywords, true);
                            $myKeywords = array_reverse($myKeywords);
                            $kwdCount = count($myKeywords);

                            $i = 1;

                            if($kwdCount > 0){
                                $errMsg = "Success: user keyword available";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $start = startTransaction($kwdDbConn);
                                if(noError($start)){

                                    $errMsg = "Success: Transaction started";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    foreach ($myKeywords as $key => $kwdArray){
                                        $getCurrActiveSlab        = getCurrentActiveSlab($kwdDbConn);
                                        $keyword = $kwdArray['keyword'];

                                        $errMsg = "Success: find keyword {$keyword}";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        if(noError($getCurrActiveSlab)){

                                            $errMsg = "Success get current active slab";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $retArray['errCode'][] = $getCurrActiveSlab['errCode'];
                                            $retArray['errMsg'][] = $errMsg;

                                            $currentSlabId = $getCurrActiveSlab["errMsg"]["id"];
                                            $kwd_sold      = $getCurrActiveSlab["errMsg"]["kwd_sold"];
                                            $kwd_price_USD = number_format($getCurrActiveSlab["errMsg"]["ITD_Price"],8);

                                            //get keyword ownership table name
                                            $kwdTableName = getKeywordOwnershipTableName($keyword);

                                            $errMsg = "Success get ownership tablename {$kwdTableName}, keyword {$keyword}.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            //cleaning of parameters for SQL Injection
                                            $tableName = cleanQueryParameter($kwdDbConn, $kwdTableName);
                                            $keyword = cleanQueryParameter($kwdDbConn, $keyword);

                                            /*$query = "insert into " . cleanQueryParameter($kwdDbConn, $tableName) . " (`buyer_id`,`purchase_timestamp`,`keyword`,`payment_mode`,`status`,`kwd_price`,`currency`,ownership_expiry_time) values( '";
                                            $query .= cleanQueryParameter($kwdDbConn, $buyer_id) . "'";
                                            $query .= ", NOW() ";
                                            $query .= ",'" . cleanQueryParameter($kwdDbConn, $keyword) . "'";
                                            $query .= ",'" . cleanQueryParameter($kwdDbConn, 'ITD') . "'";
                                            $query .= ",'sold'";
                                            $query .= ",'" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "'";
                                            $query .= ",'" . cleanQueryParameter($kwdDbConn, 'ITD') . "'";
                                            $intervals='INTERVAL 1 YEAR';
                                            $query .= ",DATE_ADD(`purchase_timestamp`, $intervals))";*/

                                            $query  = "UPDATE " . cleanQueryParameter($kwdDbConn, $tableName) . " SET claim_status = 1,kwd_price='" . cleanQueryParameter($kwdDbConn, $kwd_price_USD) . "',payment_mode='ITD',ownership_expiry_time = DATE_ADD(purchase_timestamp, INTERVAL 1 YEAR) WHERE keyword='" . cleanQueryParameter($kwdDbConn, $keyword) . "' AND buyer_id='" . cleanQueryParameter($kwdDbConn, $buyer_id) . "' ;";

                                            $insert = runQuery($query, $kwdDbConn);

                                            if (noError($insert)) {

                                                $errMsg = "Success: insert ownership details";
                                                $retArray['errCode'][] = $insert['errCode'];
                                                $retArray['errMsg'][] = $errMsg;

                                                $kwd_sold = $kwd_sold + 1;

                                                $errMsg = "Success: insert ownership details, {$keyword}";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                //Update Keyword Sold Count Of the current slab
                                                $query  = "UPDATE `first_purchase_slabs` SET `kwd_sold`='" . cleanQueryParameter($kwdDbConn, $kwd_sold) . "' WHERE `id`='" . cleanQueryParameter($kwdDbConn, $currentSlabId) . "'";

                                                $update = runQuery($query, $kwdDbConn);
                                                if (noError($update)) {;

                                                    $errMsg = "Success: update keword sold count in slab, {$keyword}";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $retArray['errCode'][] = $update['errCode'];
                                                    $retArray['errMsg'][] = $errMsg;

                                                }else{

                                                    $errMsg = "Error: update keword sold count in slab failed, {$keyword}";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $retArray['errCode'] = $update['errCode'];
                                                    $retArray['errMsg'] = $errMsg;
                                                }

                                            }else{

                                                $errMsg = "Error: insert ownership details failed, {$keyword}";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $retArray['errCode'][] = $insert['errCode'];
                                                $retArray['errMsg'][] = $errMsg;

                                            }

                                        }else{

                                            $errMsg = "Error get current active slab";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $retArray['errCode'][] = $getCurrActiveSlab['errCode'];
                                            $retArray['errMsg'][] = $errMsg;
                                        }

                                        $i++;
                                    }

                                    /* If user claim his all keywords successfully, then count error code */
                                    $count = count(array_count_values($retArray['errCode']));
                                    if($count == 1){

                                        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);

                                        if(noError($status)){

                                            $errMsg = "Success: Update user status";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $commit = commitTransaction($kwdDbConn);

                                            if(noError($commit)){

                                                $errMsg = "Success: Keyword claim process success";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $_SESSION['claimStatus'] = 1;

                                                $retArray['errCode'] = $commit['errCode'];
                                                $retArray['errMsg'] = $errMsg;

                                            }else{

                                                $errMsg = "Success: Keyword claim process failed";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $rollBack = rollbackTransaction($kwdDbConn);

                                                $retArray['errCode'] = $commit['errCode'];
                                                $retArray['errMsg'] = $errMsg;
                                            }

                                        }else{

                                            $errMsg = "Error: Update user process failed";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $rollBack = rollbackTransaction($kwdDbConn);

                                            $retArray['errCode'] = $status['errCode'];
                                            $retArray['errMsg'] = $errMsg;

                                        }

                                    }else{
                                        $errMsg = "Error: User all keyword ownership not done.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $rollBack = rollbackTransaction($kwdDbConn);

                                        $retArray['errCode'] = 2;
                                        $retArray['errMsg'] = $errMsg;
                                    }

                                }else{
                                    $errMsg = "Error: Transaction not started.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $retArray['errCode'] = $start['errCode'];
                                    $retArray['errMsg'] = $errMsg;
                                }

                            }else{
                                $errMsg = "Error: user keyword not available";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $retArray['errCode'] = 2;
                                $retArray['errMsg'] = $errMsg;
                            }
                        }else{
                            $errMsg = "Error: get user keyword details failed";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $retArray['errCode'] = 2;
                            $retArray['errMsg'] = "Error getting keyword details.";
                        }

                    }else if($_SESSION['claimStatus'] == 1){
                        $retArray['errCode'] = -1;
                        $retArray['errMsg'] = "You have already claimed your keywords";
                    }else if($_SESSION['claimStatus'] == 2){
                        $errMsg = "You have already un-claimed yor keywords.";
                        $retArray['errCode'] = -1;
                        $retArray['errMsg'] = $errMsg;
                    }

                }else if($request == 'unclaim'){

                    if(!isset($_SESSION['claimStatus'])){

                        $errMsg = "Success: Start unclaim user process";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
                        if(noError($getMyKeywordDetails)){

                            $errMsg = "Success: get user keyword details";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
                            $buyer_id = $getMyKeywordDetails['buyer_id'];
                            $myKeywords = $getMyKeywordDetails["transaction_details"];
                            $myKeywords = json_decode($myKeywords, true);
                            $myKeywords = array_reverse($myKeywords);
                            $kwdCount = count($myKeywords);

                            if($kwdCount > 0){
                                $errMsg = "Success: user keyword available";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $start = startTransaction($kwdDbConn);
                                if(noError($start)){

                                    foreach ($myKeywords as $key => $kwdArray){
                                        $keyword = $kwdArray['keyword'];

                                        //get keyword ownership table name
                                        $kwdTableName = getKeywordOwnershipTableName($keyword);

                                        $errMsg = "Success get ownership tablename {$kwdTableName}, keyword {$keyword}.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        //cleaning of parameters for SQL Injection
                                        $tableName = cleanQueryParameter($kwdDbConn, $kwdTableName);
                                        $keyword = cleanQueryParameter($kwdDbConn, $keyword);

                                        $query  = "delete from  ". $tableName ." WHERE `buyer_id`='" . cleanQueryParameter($kwdDbConn, $buyer_id) . "' and `keyword`='".$keyword."'";

                                        $delete = runQuery($query, $kwdDbConn);

                                        if (noError($delete)) {

                                            $errMsg = "Success: delete records from ownership table by {$email}.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $retArray['errCode'][] = -1;
                                            $retArray['errMsg'][] = $errMsg;

                                        }else{

                                            $errMsg = "Error: delete records from ownership table failed {$email}.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $retArray['errCode'][] = 2;
                                            $retArray['errMsg'][] = $errMsg;
                                        }
                                    }

                                    /* If user all keyword ownership delete from ownership table, then count error code */
                                    $count = count(array_count_values($retArray['errCode']));
                                    if($count == 1){

                                        $query = "delete from `mykeyword_details` where `buyer_id` = '" .$buyer_id. "';";

                                        $delete = runQuery($query, $kwdDbConn);

                                        if (noError($delete)) {

                                            $errMsg = "Success: delete record from my keyword details {$email}.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $status = updateFirstTimeUserStatus($kwdDbConn, $email, $userId, $page);

                                            if(noError($status)){

                                                $errMsg = "Success: Update user status";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $commit = commitTransaction($kwdDbConn);

                                                if(noError($commit)){

                                                    $errMsg = "Transaction committed successfully";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $_SESSION['claimStatus'] = 2;

                                                    $retArray['errCode'] = -1;
                                                    $retArray['errMsg'] = $errMsg;

                                                }else{

                                                    $errMsg = "Transaction committed failed";
                                                    $stepCounter++;
                                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                    $rollBack = rollbackTransaction($kwdDbConn);

                                                    $retArray['errCode'] = 25;
                                                    $retArray['errMsg'] = $errMsg;
                                                }

                                            }else{

                                                $errMsg = "Error: Update user process failed";
                                                $stepCounter++;
                                                $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                                $rollBack = rollbackTransaction($kwdDbConn);

                                                $retArray['errCode'] = 2;
                                                $retArray['errMsg'] = $errMsg;

                                            }

                                        }else{

                                            $errMsg = "Error: delete record from my keyword details failed {$email}.";
                                            $stepCounter++;
                                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                            $rollBack = rollbackTransaction($kwdDbConn);

                                            $retArray['errCode'] = 2;
                                            $retArray['errMsg'] = $errMsg;

                                        }

                                    }else{

                                        $errMsg = "Error: User all keyword ownership not removed.";
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $rollBack = rollbackTransaction($kwdDbConn);

                                        $retArray['errCode'] = 2;
                                        $retArray['errMsg'] = $errMsg;

                                    }

                                }else{

                                    $errMsg = "Error: Transaction not started.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}.{$i}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $retArray['errCode'] = 2;
                                    $retArray['errMsg'] = $errMsg;

                                }

                            }else{
                                $errMsg = "Error: user keyword not available";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $retArray['errCode'] = 2;
                                $retArray['errMsg'] = $errMsg;
                            }

                        }else{

                            $errMsg = "Error getting keyword details..";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $retArray['errCode'] = 2;
                            $retArray['errMsg'] = $errMsg;
                        }

                    }else if($_SESSION['claimStatus'] == 1){
                        $retArray['errCode'] = -1;
                        $retArray['errMsg'] = "You have already claimed your keywords.";

                    }else if($_SESSION['claimStatus'] == 2){
                        $retArray['errCode'] = -1;
                        $retArray['errMsg'] = "You have already un-claimed your keywords.";
                    }

                }

            }else{
                $errMsg = "Error: database connection failed";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $errMsg;
            }

        }else{
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = 'Error : Fields does not match';
        }

    }else{
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = 'Error : unauthorized access';
    }

}else{
    $retArray['errCode'] = 2;
    $retArray['errMsg'] = $errMsg;
}

$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

echo json_encode($retArray);

?>