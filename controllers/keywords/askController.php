<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 14/2/17
 * Time: 3:34 PM
 */


header("Access-Control-Allow-Origin: *");
session_start();
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/deviceHelper.php");
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../models/header/headerModel.php";
require_once "../../helpers/transactionHelper.php";
require_once "../../models/keywords/userCartModel.php";
require_once "../../models/keywords/keywordAskModel.php";
require_once "../../models/keywords/acceptBidModel.php";
require_once("../../backend_libraries/xmlProcessor/xmlProcessor.php");


error_reporting(0);
// add globla variales here
$returnArr = array();
$extraArgs = array();

// get request parameters
$keyword = urldecode($_POST['keyword']);
$type = urldecode($_POST['type']);
$action = urldecode($_POST['action']);
$amount = urldecode($_POST['amount']);
$stepCounter = 0;

$email  = $_SESSION["email"];
$username = $_SESSION["first_name"];
$id = $_SESSION["id"];
$first_name = $_SESSION["first_name"];
$last_name = $_SESSION["last_name"];

//$originIp         = getClientIP();

/*echo "keyword: {$keyword}, type: {$type}, action: {$action}";*/

// clean xss script
$keyword = cleanXSS(trim($keyword));
$type = cleanXSS(trim($type));
$action = cleanXSS(trim($action));


//get current date details
$currentDate = date("Y_m_d", time());
$dateArr     = explode("_", $currentDate);
$year        = $dateArr[0];
$month       = $dateArr[1];
$date        = $dateArr[2];
//get table partion
if ($date <= 7) {
    $partitionNo = 1;
} elseif ($date <= 14) {
    $partitionNo = 2;
} elseif ($date <= 21) {
    $partitionNo = 3;
} else {
    $partitionNo = 4;
}

//get database name eg kwd_asks_2015_11_1
$databaseName = "kwd_asks_" . $year . "_" . $month . "_" . $partitionNo;
//get ask transaction id
$askTransId = $databaseName . "/" . $email . "/" . rand(111111, 999999);

/* XML log file path */
$kwdPurchaseLog = $logPath["keywordPurchase"];

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

/* Set xml filename depending on user action */
$xmlAction = '';
$xmlTitle = '';
$xmlHeader = '';
if(!empty($action) && $action == 1) {
    $xmlAction = "setAsk";
    $xmlTitle = "Set ask price";
    $xmlHeader = 'set ask';
}else if(!empty($action) && $action == 2){
    $xmlAction = "editAsk";
    $xmlTitle = "Edit ask price";
    $xmlHeader = 'edit ask';
}else if(!empty($action) && $action == 3){
    $xmlAction = "deleteAsk";
    $xmlTitle = "Delet ask price";
    $xmlHeader = 'delete ask';
}

$xmlFilename   = "{$xmlAction}.xml";

//echo "upper xml file name ".$xmlFilename;

/* Initialize XML logs */
$xmlProcessor = new xmlProcessor();
$xmlArray = initializeXMLLog(trim(urldecode($email)));
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

/* XML title attribute */
$xml_atrr = array();
$xml_atrr["title"] = $xmlTitle;


if(isset($_SESSION["email"]) && !empty($_SESSION)){

    if(!isset($_SESSION["verify_status"])){

        $errMsg = "Start {$xmlAction} process."; // Message for log
        $stepCounter++; // Step counter
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr; // Set attribute in step
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}"; // Set message for step

        $unique_id = md5(uniqid(rand(), true));
        $xmlArray['activity']['id'] = $unique_id;
        $_SESSION['activity_id'] = $unique_id;

        $_SESSION['redirect_url'] = $returnURl;
        $msg = "Start {$xmlHeader} process.";
        $xml_data['step'.$i]["data"] = $i.". {$msg}";

        $checkEmailExistence = checkEmailExistance($email);
        if(noError($checkEmailExistence)){
            $errMsg = "Success : {$email} is a valid user";
            $xml_data['step'.++$i]["data"] = $i.". Success :  {$email} is a valid user";
            $returnArr["errCode"] = -2;
            $returnArr["errMsg"] = $errMsg;

            //redirecting to 2FA page
            $msg = "Success : Redirecting to 2FA page";
            $xml_data['step'.++$i]["data"] = $i.". {$msg}";
            $returnArr["errCode"] = -2;
            $_SESSION['form_data'] = json_encode($_POST);
            $_SESSION['xml_step'] = $i;
            $_SESSION["action"] = $action;

            $_SESSION['xml_file'] = $kwdPurchaseLog . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlFilename;

        }else{
            $errMsg = "Not a valid user";
            $xml_data['step'.++$i]["data"] = $i.". Failed : Not a valid user";
            $returnArr["errCode"] = 1;
            $returnArr["errMsg"] = $errMsg;
        }
        // create or update xml log Files
        $xmlProcessor->writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

        echo json_encode($returnArr); die;

    }else if($returnURl == $_SESSION['redirect_url']){

        $stepCounter = $_SESSION["xml_step"];
        $security_type = $_SESSION['security_type'];
        $form_data = json_decode($_SESSION['form_data'], true);
        $keyword = urldecode($form_data["keyword"]);
        $amount = urldecode($form_data["amount"]);
        $type = urldecode($form_data["type"]);
        $action = $form_data["action"];
        $activity_id = $_SESSION['activity_id'];
        $xml_file = $_SESSION['xml_file'];

        $action = $_SESSION["action"];

        if(!empty($action) && $action == 1) {
            $xmlAction = "setAsk";
        }else if(!empty($action) && $action == 2){
            $xmlAction = "editAsk";
        }else if(!empty($action) && $action == 3){
            $xmlAction = "deleteAsk";
        }

        $xmlFilename   = "{$xmlAction}.xml";

        unset($_SESSION['security_type']);
        unset($_SESSION['xml_step']);
        unset($_SESSION['form_data']);
        unset($_SESSION['activity_id']);
        unset($_SESSION['redirect_url']);
        unset($_SESSION['xml_file']);
        unset($_SESSION['verify_status']);
        unset($_SESSION["action"]);

        $kwdDbConn = createDBConnection('dbkeywords');
        if (noError($kwdDbConn)) {

            $errMsg = "keywords db connection: success.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $kwdDbConn = $kwdDbConn["connection"];

            switch ($type){
                case "ask":
                    if($action == 1)
                    {
                        /* Set ask */
                        $returnArr = setAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $amount, $askTransId, $xml_data, $stepCounter, $xml_atrr);
                    }else if($action == 2){
                        $returnArr = editAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $amount, $askTransId, $xml_data, $stepCounter, $xml_atrr);
                    }else{
                        $returnArr = deleteAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $rootUrl,$amount, $askTransId, $xml_data, $stepCounter, $xml_atrr);
                    }
                    break;
            }

            /*return $returnArr;*/
        }else{
            /* return error message */
            $errMsg = "keywords db connection: failed.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = "Database error";

        }

        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr["errMsg"]  = $returnArr['errMsg'];
        $xmlCredentialData = json_encode($returnArr['credential']);
        $xml_data         = $returnArr["xml_data"];
        unset($returnArr['credential']);
        unset($returnArr['xml_data']);

        /* Write response attribute in XML file */
        $xml_resp_atrr['response'] = json_encode($returnArr);
        $xml_cred_atrr['credential'] = $xmlCredentialData;

        $xml_data['response']["data"] ="";
        $xml_data['response']["attribute"] =$xml_resp_atrr;
        $xml_data['credential']["data"] ="";
        $xml_data['credential']["attribute"] =$xml_cred_atrr;

        //create or update xml log Files

        //$xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);
        $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

        /*if(noError($returnArr)){
            $_SESSION['succ_msg'] = $returnArr['errMsg'];
        }else{
            $_SESSION['err_msg'] = $returnArr['errMsg'];
        }*/

        header('Location:'.$rootUrl.'views/keywords/marketplace/keyword_search.php?q='.$keyword);

    }else{
        unset($_SESSION['security_type']);
        unset($_SESSION['xml_step']);
        unset($_SESSION['form_data']);
        unset($_SESSION['activity_id']);
        unset($_SESSION['redirect_url']);
        unset($_SESSION['xml_file']);
        unset($_SESSION['verify_status']);
        $_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";
        $returnArr["errCode"] = 2;

      //  $xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

        header('Location:'.$rootUrl.'views/keywords/marketplace/keyword_search.php?q='.$keyword);
    }

}else{
    $errMsg = "Session is expired";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    $returnArr["errCode"] = 2;
    $returnArr["redirect_url"] = "{$rootUrl}views/prelogin/index.php";
    $returnArr["errMsg"]  = "Sorry!!! Your session is expired, Please login again.";
}

//$xmlProcessor-> writeXML($xmlFilename, $kwdPurchaseLog, $xml_data, $xmlArray["activity"]);

echo json_encode($returnArr);


function setAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $amount, $askTransId, &$xml_data, &$stepCounter, $xml_atrr){

    $returnArr = array();
    $logCred = array();
    $extraArgs = '';
    global $userRequiredFields, $walletURLIP,$keywoDefaultCurrencyName;

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount,"askTransId"=>$askTransId);

    $errMsg = "Start executing set ask method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    //check for keyword owner
    $keywordDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);
    if (noError($keywordDetails)) {

        $errMsg = "Check for keyword ownership details : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $keywordOwnerId = $keywordDetails["errMsg"]["buyer_id"];
        $keywordStatus  = $keywordDetails["errMsg"]["order_status"];

        if ($keywordStatus != '1') {

            $errMsg = "Ask price already set for keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            if ($keywordOwnerId == $email) {

                $errMsg = "Keyword owner id match : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $trans = startTransaction($kwdDbConn);
                if (noError($trans)) {

                    $errMsg = "Start transaction : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $query  = "INSERT INTO " . $databaseName . "(kwd_owner_id,kwd_owner_email,kwd_owner_mobile,keyword,kwd_ask_price,ask_status,ask_transaction_id) VALUES('','" . $email . "','','" . $keyword . "','" . $amount . "','0','" . $askTransId . "')";
                    $result = runQuery($query, $kwdDbConn);
                    if (noError($result)) {

                        $errMsg = "Data inserted in keyword ask table : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $ownershipTableName = getKeywordOwnershipTableName($keyword);
                        $query  = "UPDATE " . $ownershipTableName . " SET order_status ='1',ask_price='" . $amount . "',ask_transaction_id='" . $askTransId . "' WHERE keyword='" . $keyword . "'";
                        $result = runQuery($query, $kwdDbConn);
                        $errMsg = $result["errMsg"];
                        if (noError($result)) {

                            $errMsg = "Update ownership table : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            //for index page boxes
                            $addToHighestAsk = addToHighestAsk($kwdDbConn, $keyword, $email, $amount, $askTransId);
                            //end of code for index page boxes
                            if (noError($addToHighestAsk)) {

                                $errMsg = "Update addToHighestAsk table : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            } else {
                                $errMsg = "Update addToHighestAsk table : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }
                            //add to latest ask
                            $result = addToLatestAsk($kwdDbConn, $keyword, $email, $amount, $askTransId);
                            if (noError($result)) {
                                $errMsg = "Update addToLatestAsk table : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            } else {
                                $errMsg = "Update addToLatestAsk table : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }
                            //end of code for latest ask


                            $addToLatestAskAll = addToLatestAskAll($kwdDbConn, $keyword, $email, $amount, $askTransId);
                            if (noError($addToLatestAskAll)) {
                                $errMsg = "Update addToLatestAskAll table : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            } else {
                                $errMsg = "Update addToLatestAskAll table : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }

                            $trans = commitTransaction($kwdDbConn);
                            if (noError($trans)) {
                                $errMsg = "Transaction committed : success.";
                                $errCode = -1;
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            } else {
                                $errMsg = "Transaction committed : failed.";
                                $errCode = 2;
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }

                            // get bid seller user info
                            $kwdSellerReqField = $userRequiredFields.",_id";
                            $kwdSellerUserInfo =  getUserInfo($email, $walletURLIP . 'api/v3/', $kwdSellerReqField);

                            if(noError($kwdSellerUserInfo)){
                                $errMsg = "Getting ".$email." info : success.";
                                $errCode = -1;
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $kwdSellerUserInfo = $kwdSellerUserInfo["errMsg"];
                                $userId = $kwdSellerUserInfo["_id"];
                                $firstName = $kwdSellerUserInfo["first_name"];
                                $lastName = $kwdSellerUserInfo["last_name"];

                                // send email to seller

                                $to      = $email;
                                $setAskSubject = "You have set an ask of $amount on #$keyword";
                                $message =
                                    '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                    <tr> <td style="padding: 10px 20px;">
                                       You have successfully set asking price for <b> #'.$keyword.'</b> at ' . number_format((float)$amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event a bid is placed on it.
                                    <br><br>Regards,<br>Team Keywo
                                    </td>  </tr>';

                                $notification_message = 'You have successfully set asking price for #' . $keyword . ' at ' . number_format($amount, 4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event a bid is placed on it';

                                $path     = "";
                                $method_name = "GET";
                                $getNotification = notifyoptions($userId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);

                                $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["ask_opt_container"]["2"]["permissions"]["_id"];

                                $category = "ask";
                                $sendNotifEmailToSeller = sendNotificationBuyPrefrence($to, $setAskSubject, $message, $firstName, $lastName, $userId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                if(noError($sendNotifEmailToSeller)){
                                    $errMsg = "You have successfully placed the ask price on #".$keyword." Keyword";
                                    $errCode = -1;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $_SESSION["get_error_code"] = -1;
                                    $_SESSION["get_error_message"] = $errMsg;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]="-1";
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/
                                }else{
                                    $errMsg = "There was an error while placing the ask price on #".$keyword." keyword.";
                                    $errCode = 2;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $_SESSION["get_error_code"] = $errCode;
                                    $_SESSION["get_error_message"] = $errMsg;

                                    /******************** for logs user analytics ****************/
                                    $responseArr["errCode"]=$errCode;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;
                                    /******************** for logs user analytics ****************/
                                }

                            }else{
                                $errMsg = "Getting ".$email." info : failed.";
                                $errCode = -1;
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                            }

                            $returnArr["errCode"] = $errCode;
                            $returnArr["errMsg"]  = $errMsg;

                            $_SESSION["get_error_code"] = $errCode;
                            $_SESSION["get_error_message"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }else{
                            /*update ownershipTable failed*/
                            $errMsg = "Update ownership table : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $returnArr["errCode"] = 2;
                            $returnArr["errMsg"]  = $errMsg;

                            $_SESSION["get_error_code"] = 2;
                            $_SESSION["get_error_message"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=2;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }
                        
                    }else{

                        $errMsg = "Data inserted in keyword ask table : failed.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /*query error*/
                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"]  = $errMsg;

                        $_SESSION["get_error_code"] = 2;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=2;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }
                    
                }else{

                    $errMsg = "Start transaction : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /*Errror start trans*/
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"]  = $errMsg;

                    $_SESSION["get_error_code"] = 2;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

                $errMsg = "Keyword owner id match : failed.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = $errMsg;

                $_SESSION["get_error_code"] = 2;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }

        }else{

            $errMsg = "You have already set the ask price on #".$keyword." keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            /* error get keyword ownership */
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = $errMsg;

            $_SESSION["get_error_code"] = 2;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }

    }else{

        $errMsg = "Check for keyword ownership details : failed.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        /* error get keyword ownership */
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = $errMsg;

        $_SESSION["get_error_code"] = 2;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $returnArr["credential"]  = $logCred;
    $returnArr["xml_data"]  = $xml_data;

    return $returnArr;
}

function editAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $amount, $askTransId, &$xml_data, &$stepCounter, $xml_atrr){

    $returnArr = array();
    $logCred = array();
    $extraArgs = '';
    global $userRequiredFields, $walletURLIP,$keywoDefaultCurrencyName;

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount,"askTransId"=>$askTransId);

    $errMsg = "Start executing edit ask method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    //check for keyword owner
    $keywordDetails           = getKeywordOwnershipDetails($kwdDbConn, $keyword);
    if (noError($keywordDetails)) {

        $errMsg = "Check for keyword ownership details : success.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        $keywordOwnerId           = $keywordDetails["errMsg"]["buyer_id"];
        $lastAskTransactionId        = $keywordDetails["errMsg"]["ask_transaction_id"];
        $previousAskPrice            = $keywordDetails["errMsg"]["ask_price"];
        $keywordStatus            = $keywordDetails["errMsg"]["order_status"];
        $lastAskTransactionIdDeatils = explode("/", $lastAskTransactionId);
        $lastAskTransactionIdTable   = $lastAskTransactionIdDeatils[0];

        if ($keywordStatus == '1') {

            $errMsg = "Ask price exist for the keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            if ($keywordOwnerId == $email) {

                $errMsg = "keyword owner id match : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $trans = startTransaction($kwdDbConn);
                if (noError($trans)) {

                    $errMsg = "Start transaction : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $query  = "INSERT INTO " . $databaseName . "(kwd_owner_id,kwd_owner_email,kwd_owner_mobile,keyword,kwd_ask_price,ask_status,ask_transaction_id) VALUES('','" . $email . "','','" . $keyword . "','" . $amount . "','0','" . $askTransId . "')";
                    $result = runQuery($query, $kwdDbConn);
                    if (noError($result)) {

                        $errMsg = "Update kwd ask table : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $ownershipTableName = getKeywordOwnershipTableName($keyword);
                        $query  = "UPDATE " . $ownershipTableName . " SET order_status ='1',ask_price='" . $amount . "',ask_transaction_id='" . $askTransId . "' WHERE keyword='" . $keyword . "'";
                        $result = runQuery($query, $kwdDbConn);
                        $errMsg = $result["errMsg"];
                        if (noError($result)) {

                            $errMsg = "Update keyword ownership table : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $query  = "UPDATE " . $lastAskTransactionIdTable . " SET ask_status = '1' WHERE ask_transaction_id = '" . $lastAskTransactionId . "';";
                            $result = runQuery($query, $kwdDbConn);
                            if (noError($result)) {

                                $errMsg = "Update last ask transactionId table : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                //for index page boxes
                                $addToHighestAsk = addToHighestAsk($kwdDbConn, $keyword, $email, $amount, $askTransId);
                                //end of code for index page boxes
                                if (noError($addToHighestAsk)) {
                                    $errMsg = "Add to HighestAsk table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Add to HighestAsk table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                $deleteFromHighestAsk = deleteFromHighestAsk($kwdDbConn, $lastAskTransactionId);
                                if (noError($deleteFromHighestAsk)) {
                                    $errMsg = "Deleted from highest ask table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from highest ask table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                //add to latest ask
                                $result = addToLatestAsk($kwdDbConn, $keyword, $email, $amount, $askTransId);
                                if (noError($result)) {

                                    $errMsg = "Add to latest ask table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Add to latest ask table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }
                                //end of code for latest ask

                                //delete from latest ask
                                $result = deleteFromLatestAsk($kwdDbConn, $lastAskTransactionId);
                                if (noError($result)) {
                                    $errMsg = "Deleted from latest ask table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from latest ask table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }
                                //end of code for delete from latest ask

                                $result = deleteFromLatestAskAll($kwdDbConn,$lastAskTransactionId);
                                if (noError($result)) {
                                    $errMsg = "Deleted from latestAskAll table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from latestAskAll table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                $addToLatestAskAll = addToLatestAskAll($kwdDbConn, $keyword, $email, $amount, $askTransId);

                                if (noError($addToLatestAskAll)) {
                                    $errMsg = "Add to latestAskAll table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Add to latestAskAll table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }
                                $trans = commitTransaction($kwdDbConn);
                                if (noError($trans)) {
                                    $errMsg = "Transaction committed : success.";
                                    $errCode = -1;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                } else {
                                    $errMsg = "Transaction committed : failed.";
                                    $errCode = 2;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }


                                // get bid seller user info
                                $kwdSellerReqField = $userRequiredFields.",_id";
                                $kwdSellerUserInfo =  getUserInfo($email, $walletURLIP . 'api/v3/', $kwdSellerReqField);
                                if(noError($kwdSellerUserInfo)){

                                    $errMsg = "Get user info of ".$email." : success.";
                                    $errCode = -1;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";



                                    $kwdSellerUserInfo = $kwdSellerUserInfo["errMsg"];
                                    $kwdSellerUserId = $kwdSellerUserInfo["_id"];
                                    $kwdSellerFirstName = $kwdSellerUserInfo["first_name"];
                                    $kwdSellerLastName = $kwdSellerUserInfo["last_name"];

                                    //send mail for  edit Ask
                                    $to                   = $email;
                                    $editSubject              = "You have edited ask on #$keyword";
                                    $message              =
                                        '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                    <tr> <td style="padding: 10px 20px;">
                                       You have changed the asking price for <b> #'.$keyword.'</b> from ' . number_format((float)$previousAskPrice, 4) . ' '.$keywoDefaultCurrencyName.'. to '. number_format($amount,4) . ' '.$keywoDefaultCurrencyName.'. You will be notified in the event a bid is placed on it.
                                    <br><br>Regards,<br>Team Keywo
                                    </td>  </tr>';

                                    $notification_message = 'You have changed the asking price for #' . $keyword . ' from ' . number_format($previousAskPrice, 4) . ' '.$keywoDefaultCurrencyName.' to ' . number_format($amount, 4) . ' '.$keywoDefaultCurrencyName.'.';

                                    $path     = "";
                                    $method_name = "GET";
                                    $getNotification = notifyoptions($kwdSellerUserId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                    $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["ask_opt_container"]["2"]["permissions"]["_id"];
                                    $category = "ask";
                                    $sendNotifEmailToSeller = sendNotificationBuyPrefrence($to, $editSubject, $message, $kwdSellerFirstName, $kwdSellerLastName, $kwdSellerUserId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);

                                    if(noError($sendNotifEmailToSeller)){

                                        $errMsg = "Successfully edited the ask price of #".$keyword." keyword.";
                                        $errCode = -1;
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $_SESSION["get_error_code"] = -1;
                                        $_SESSION["get_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]="-1";
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/

                                    }else{
                                        $errMsg = "There was an error while editing the ask price of #".$keyword." keyword";
                                        $errCode = 2;
                                        $stepCounter++;
                                        $xml_data["step{$stepCounter}"]["data"] ="";
                                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                        $_SESSION["get_error_code"] = 2;
                                        $_SESSION["get_error_message"] = $errMsg;

                                        /******************** for logs user analytics ****************/
                                        $responseArr["errCode"]=2;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;
                                        /******************** for logs user analytics ****************/
                                    }

                                }else{

                                    $errMsg = "Get user info of ".$email." : failed.";
                                    $errCode = 2;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                }

                                $returnArr["errCode"] = $errCode;
                                $returnArr["errMsg"]  = $errMsg;

                                $_SESSION["get_error_code"] = $returnArr["errCode"];
                                $_SESSION["get_error_message"] = $returnArr["errMsg"];

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=$errCode;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }else{

                                $errMsg = "Update last ask transactionId table : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $returnArr["errCode"] = 2;
                                $returnArr["errMsg"]  = "update set ask table";

                                $_SESSION["get_error_code"] = 2;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/
                            }

                        }else{

                            $errMsg = "Update keyword ownership table : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            /*update ownershipTable failed*/
                            $returnArr["errCode"] = 2;
                            $returnArr["errMsg"]  = $errMsg;

                            $_SESSION["get_error_code"] = 2;
                            $_SESSION["get_error_message"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=2;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }

                    }else{

                        $errMsg = "Update kwd ask table : failed.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /*query error*/
                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"]  = $errMsg;

                        $_SESSION["get_error_code"] = 2;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=2;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }

                }else{

                    $errMsg = "Start transaction : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /*Errror start trans*/
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"]  = $errMsg;

                    $_SESSION["get_error_code"] = 2;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

                $errMsg = "keyword owner id match : failed.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = $errMsg;

                $_SESSION["get_error_code"] = 2;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }

        }else{

            $errMsg = "Ask price not exist for the keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            /* error get keyword ownership */
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = $errMsg;

            $_SESSION["get_error_code"] = 2;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }

    }else{

        $errMsg = "Check for keyword ownership details : failed.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        /* error get keyword ownership */
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = $errMsg;

        $_SESSION["get_error_code"] = 2;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $returnArr["credential"]  = $logCred;
    $returnArr["xml_data"]  = $xml_data;

    return $returnArr;
}

function deleteAskPrice($kwdDbConn, $databaseName, $keyword, $email, $username, $rootUrl, $amount, $askTransId, &$xml_data, &$stepCounter, $xml_atrr){

    $returnArr = array();
    $logCred = array();
    $extraArgs = '';
    global $userRequiredFields, $walletURLIP,$keywoDefaultCurrencyName;

    $logCred = array("keyword"=>$keyword,"user"=>$email,"amount"=>$amount,"askTransId"=>$askTransId);

    $errMsg = "Start executing edit ask method.";
    $stepCounter++;
    $xml_data["step{$stepCounter}"]["data"] ="";
    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

    //check for keyword owner
    $keywordDetails           = getKeywordOwnershipDetails($kwdDbConn, $keyword);
    if (noError($keywordDetails)) {
        $keywordOwnerId           = $keywordDetails["errMsg"]["buyer_id"];
        $lastAskTransactionId        = $keywordDetails["errMsg"]["ask_transaction_id"];
        $previousAskPrice            = $keywordDetails["errMsg"]["ask_price"];
        $keywordStatus            = $keywordDetails["errMsg"]["order_status"];
        $lastAskTransactionIdDeatils = explode("/", $lastAskTransactionId);
        $lastAskTransactionIdTable   = $lastAskTransactionIdDeatils[0];

        if ($keywordStatus == '1') {

            $errMsg = "Ask price exist for the keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            if ($keywordOwnerId == $email) {

                $errMsg = "keyword owner id match : success.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $trans = startTransaction($kwdDbConn);
                if (noError($trans)) {

                    $errMsg = "Start transaction : success.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    $query  = "INSERT INTO " . $databaseName . "(kwd_owner_id,kwd_owner_email,kwd_owner_mobile,keyword,kwd_ask_price,ask_status,ask_transaction_id) VALUES('','" . $email . "','','" . $keyword . "','" . $amount . "','0','" . $askTransId . "')";
                    $result = runQuery($query, $kwdDbConn);
                    if (noError($result)) {

                        $errMsg = "Update kwd ask table : success.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        $ownershipTableName = getKeywordOwnershipTableName($keyword);
                        $query  = "UPDATE " . $ownershipTableName . " SET order_status ='0',ask_price='',ask_transaction_id='" . $askTransId . "' WHERE keyword='" . $keyword . "'";
                        $result = runQuery($query, $kwdDbConn);
                        $errMsg = $result["errMsg"];
                        if (noError($result)) {

                            $errMsg = "Update keyword ownership table : success.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            $query  = "UPDATE " . $lastAskTransactionIdTable . " SET ask_status = '1' WHERE ask_transaction_id = '" . $lastAskTransactionId . "';";
                            $result = runQuery($query, $kwdDbConn);
                            if (noError($result)) {

                                $errMsg = "Update last ask transactionId table : success.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $deleteFromHighestAsk = deleteFromHighestAsk($kwdDbConn, $lastAskTransactionId);
                                if (noError($deleteFromHighestAsk)) {
                                    $errMsg = "Deleted from highestAsk table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from highestAsk table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                //delete from latest ask
                                $result = deleteFromLatestAsk($kwdDbConn, $lastAskTransactionId);
                                if (noError($result)) {
                                    /*$xml .= "<step12 title='edit Ask price' >12.deleted from deleteFromLatestAsk table : 'success'";
                                    $xml .= "</step12>\n";*/
                                    $errMsg = "Deleted from latestAsk table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from latestAsk table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }
                                //end of code for delete from latest ask

                                $result = deleteFromLatestAskAll($kwdDbConn,$lastAskTransactionId);
                                if (noError($result)) {
                                    /*$xml .= "<step12 title='edit Ask price' >12.deleted from deleteFromLatestAsk table : 'success'";
                                    $xml .= "</step12>\n";*/
                                    $errMsg = "Deleted from latestAskAll table : success.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                } else {
                                    $errMsg = "Deleted from latestAskAll table : failed.";
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                $trans = commitTransaction($kwdDbConn);
                                if (noError($trans)) {
                                    $errMsg = "Transaction committed : success.";
                                    $errCode = -1;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                } else {
                                    $errMsg = "Transaction committed : failed.";
                                    $errCode = 2;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";
                                }

                                // get bid seller user info
                                $kwdSellerReqField = $userRequiredFields.",_id";
                                $kwdSellerUserInfo =  getUserInfo($email, $walletURLIP . 'api/v3/', $kwdSellerReqField);
                                if($kwdSellerUserInfo){
                                    $errMsg = "Getting ". $email ." info : success.";
                                    $errCode = -1;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $kwdSellerUserInfo = $kwdSellerUserInfo["errMsg"];
                                    $kwdSellerUserId = $kwdSellerUserInfo["_id"];
                                    $kwdSellerFirstName = $kwdSellerUserInfo["first_name"];
                                    $kwdSellerLastName = $kwdSellerUserInfo["last_name"];


                                    //send mail for cancel Ask
                                    $to                   = $email;
                                    $cancelSubject              = "You have deleted ask on #$keyword";
                                    $message              =
                                        '<td style="padding: 60px 20px 10px; font-size: 20px; color: #0b6796;">Hi '.$username.',</td>                                                    
                                    <tr> <td style="padding: 10px 20px;">
                                       You have deleted the asking price set for <b> #'.$keyword.'</b>. Click <a href="'.$rootUrl.'views/prelogin/index.php">here</a> to login to Keywo and set a new ask on the keyword.
                                    <br><br>Regards,<br>Team Keywo
                                    </td>  </tr>';

                                    //You have successfully cancelled the ask price of #keyword
                                    $notification_message = 'You have deleted the asking price set for #' . $keyword . '.';

                                    $path     = "";
                                    $category = "ask";
                                    $method_name = "GET";
                                    $getNotification = notifyoptions($kwdSellerUserId,$optionid,$method_name,$buy_container,$ask_container,$bid_container,$withdrawal_container,$deposit_container,$kwd_license_container,$buy_perm_code,$ask_perm_code,$bid_perm_code,$kwd_license_perm_code,$deposit_perm_code,$withdrawal_perm_code);
                                    $permissionCode = $getNotification["errMsg"]["notify_options_fk_key"]["ask_opt_container"]["2"]["permissions"]["_id"];

                                    $sendNotifEamilToSeller = sendNotificationBuyPrefrence($to, $cancelSubject, $message, $kwdSellerFirstName, $kwdSellerLastName, $kwdSellerUserId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);
                                   if(noError($sendNotifEamilToSeller)){
                                       $errMsg = "You have successfully cancelled the ask price of #".$keyword." keyword.";
                                       $errCode = -1;
                                       $stepCounter++;
                                       $xml_data["step{$stepCounter}"]["data"] ="";
                                       $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                       $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                       $_SESSION["get_error_code"] = -1;
                                       $_SESSION["get_error_message"] = $errMsg;

                                       /******************** for logs user analytics ****************/
                                       $responseArr["errCode"]="-1";
                                       $responseArr["errMsg"]=$errMsg;
                                       $xml_data['response']["data"] = "";
                                       $xml_data['response']["attributes"] = $responseArr;
                                       /******************** for logs user analytics ****************/
                                   }else{
                                       $errMsg = "There was an error while cancelling the ask price of #".$keyword." keyword.";
                                       $errCode = 2;
                                       $stepCounter++;
                                       $xml_data["step{$stepCounter}"]["data"] ="";
                                       $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                       $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                       $_SESSION["get_error_code"] = $errCode;
                                       $_SESSION["get_error_message"] = $errMsg;

                                       /******************** for logs user analytics ****************/
                                       $responseArr["errCode"]=2;
                                       $responseArr["errMsg"]=$errMsg;
                                       $xml_data['response']["data"] = "";
                                       $xml_data['response']["attributes"] = $responseArr;
                                       /******************** for logs user analytics ****************/
                                   }
                                }else{
                                    $errMsg = "Getting ". $email ." info : failed.";
                                    $errCode = 2;
                                    $stepCounter++;
                                    $xml_data["step{$stepCounter}"]["data"] ="";
                                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                    $_SESSION["get_error_code"] = $errCode;
                                    $_SESSION["get_error_message"] = $errMsg;
                                }

                                $returnArr["errCode"] = $errCode;
                                $returnArr["errMsg"]  = $errMsg;

                                $_SESSION["get_error_code"] = $errCode ;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }else{

                                $errMsg = "Update last ask transactionId table : failed.";
                                $stepCounter++;
                                $xml_data["step{$stepCounter}"]["data"] ="";
                                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                                $returnArr["errCode"] = 2;
                                $returnArr["errMsg"]  = $errMsg;

                                $_SESSION["get_error_code"] = 2 ;
                                $_SESSION["get_error_message"] = $errMsg;

                                /******************** for logs user analytics ****************/
                                $responseArr["errCode"]=2;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;
                                /******************** for logs user analytics ****************/

                            }

                        }else{

                            $errMsg = "Update keyword ownership table : failed.";
                            $stepCounter++;
                            $xml_data["step{$stepCounter}"]["data"] ="";
                            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                            /*update ownershipTable failed*/
                            $returnArr["errCode"] = 2;
                            $returnArr["errMsg"]  = $errMsg;

                            $_SESSION["get_error_code"] = 2 ;
                            $_SESSION["get_error_message"] = $errMsg;

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=2;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/
                        }

                    }else{

                        $errMsg = "Update kwd ask table : failed.";
                        $stepCounter++;
                        $xml_data["step{$stepCounter}"]["data"] ="";
                        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                        /*query error*/
                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"]  = $errMsg;

                        $_SESSION["get_error_code"] = 2 ;
                        $_SESSION["get_error_message"] = $errMsg;

                        /******************** for logs user analytics ****************/
                        $responseArr["errCode"]=2;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;
                        /******************** for logs user analytics ****************/
                    }

                }else{

                    $errMsg = "Start transaction : failed.";
                    $stepCounter++;
                    $xml_data["step{$stepCounter}"]["data"] ="";
                    $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                    $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                    /*Errror start trans*/
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"]  = $errMsg;

                    $_SESSION["get_error_code"] = 2 ;
                    $_SESSION["get_error_message"] = $errMsg;

                    /******************** for logs user analytics ****************/
                    $responseArr["errCode"]=2;
                    $responseArr["errMsg"]=$errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;
                    /******************** for logs user analytics ****************/
                }
            }else{

                $errMsg = "keyword owner id match : failed.";
                $stepCounter++;
                $xml_data["step{$stepCounter}"]["data"] ="";
                $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
                $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]  = $errMsg;

                $_SESSION["get_error_code"] = 2 ;
                $_SESSION["get_error_message"] = $errMsg;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]=2;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
            }

        }else{

            $errMsg = "Ask price not exist for the keyword.";
            $stepCounter++;
            $xml_data["step{$stepCounter}"]["data"] ="";
            $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
            $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]  = $errMsg;

            $_SESSION["get_error_code"] = 2 ;
            $_SESSION["get_error_message"] = $errMsg;

            /******************** for logs user analytics ****************/
            $responseArr["errCode"]=2;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;
            /******************** for logs user analytics ****************/
        }

    }else{
        $errMsg = "Check for keyword ownership details : failed.";
        $stepCounter++;
        $xml_data["step{$stepCounter}"]["data"] ="";
        $xml_data["step{$stepCounter}"]["attribute"] =$xml_atrr;
        $xml_data["step{$stepCounter}"]["data"] = "{$stepCounter}. {$errMsg}";

        /* error get keyword ownership */
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = $errMsg;

        $_SESSION["get_error_code"] = 2 ;
        $_SESSION["get_error_message"] = $errMsg;

        /******************** for logs user analytics ****************/
        $responseArr["errCode"]=2;
        $responseArr["errMsg"]=$errMsg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        /******************** for logs user analytics ****************/
    }

    $returnArr["credential"]  = $logCred;
    $returnArr["xml_data"]  = $xml_data;

    return $returnArr;
}


?>