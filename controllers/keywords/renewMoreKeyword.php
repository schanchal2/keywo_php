<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once ("{$docrootpath}config/config.php");
require_once ("{$docrootpath}config/db_config.php");
require_once ("{$docrootpath}helpers/coreFunctions.php");
require_once ("{$docrootpath}helpers/errorMap.php");
require_once ("{$docrootpath}helpers/arrayHelper.php");
require_once ("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

$keywords = $_POST["renewMoreKeyword"];
$clientSessionId = urldecode($_POST["clientSessionId"]);

$currentSessionId = session_id();
$upc = strtolower($_SESSION['CurrPreference']);

$totalBTCRate = 0;
$totalUPCRate = 0;
$totalITDRate = 0;


if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
    if($currentSessionId == $clientSessionId){

            $email = $_SESSION["email"];

            // create database connection
           $kwdDbConn = createDBConnection("dbkeywords");
           if(noError($kwdDbConn)){
               $kwdDbConn = $kwdDbConn["connection"];

               // get admin setting details
               $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
               if(noError($getAdminSetting)){
                   $getAdminSetting = $getAdminSetting["data"];

                   $getRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

                   $getExchangeRate = getExchangeRates($upc);
                   if(noError($getExchangeRate)){
                        $getExchangeRate = $getExchangeRate["currRate"];


                       // calculate btc amount from renewal amount
                       $btcRate = number_format($getExchangeRate["btc"], 8);
                       $calcBtcOfRenewalAmt =  number_format(($getRenewalAmt*$btcRate), 8);

                       // calculate upc amount from renewal amount.
                      $upcRate = $getExchangeRate[$upc];
                       $calcUpcRateOfRenewalAmt = number_format(($upcRate * $getRenewalAmt), 8);


                       $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);

                       if(noError($getPresaleDetails)){
                           $getPresaleDetails = $getPresaleDetails["errMsg"];
                           $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];

                           if(empty($getRenewKwd) && !isset($getRenewKwd)){
                               // 1. if user_renew_kwd_in_cart is empty then insert keyword into presale user.
                               $renewArr = array($keyword);
                               $renewArr = json_encode($renewArr);
                           }else{
                               // retrieve the user_renew_kwd_in_cart values.
                               // decode the values
                               // check keyword exist in in_array
                               // if exist do nothing
                               // else update the user_renew_kwd_in_cart value in presale user table.

                               $renewArr = json_decode($getRenewKwd, true);
                               foreach($keywords as $key => $value){
                                   if(!in_array($value, $renewArr)){
                                       $renewArr[] = $value;
                                   }

                               }

                               $renewArr = json_encode($renewArr);
                           }

                           $updateRenewKwd = updateRenewKwdInPresale($email, $renewArr, $kwdDbConn);
                           if(noError($updateRenewKwd)){

                               $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);

                               if(noError($getPresaleDetails)){
                                   $getPresaleDetails = $getPresaleDetails["errMsg"];
                                   $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];

                                   $getRenewKwd = json_decode($getRenewKwd, true);
                               }else{
                                   print("Error: Fetching user renew keyword details");
                               }

                           }else{
                               print("Error: Unable to update renew keyword details");
                           }
                       }else{
                           print("Error: Fetching presale details");
                       }



                       ?>

                       <thead class="">
                       <tr>

                           <th>Keyword</th>

                           <th>
                               <?php echo $_SESSION['CurrPreference']; ?>

                           </th>
                           <th> <?php echo $keywoDefaultCurrencyName; ?> </th>
                           <th></th>
                       </tr>


                       </thead>
                       <tbody>

                       <?php
                       if(count($getRenewKwd) > 0){

                           foreach($getRenewKwd as  $keyword){
                               $renewKwdInCart .= "#".$keyword.",";
                               ?>

                               <tr>
                                   <td>
                                       <span class="ellipses ellipses-general">
<a class="display-in-block-txt-blk"  title="<?php echo "#{$keyword}"; ?>" data-toggle="tooltip" data-placement="bottom" href="?q=safdsfc" data-original-title="<?php echo "#{$keyword}"; ?>"><?php echo "#{$keyword}"; ?></a>
                              </span><br>
                                       <label>BTC : <?php echo $calcBtcOfRenewalAmt; ?></label>
                                   </td>
                                   <td>
                                       <label><?php echo number_format($calcUpcRateOfRenewalAmt, 2); ?></label>
                                   </td>
                                   <td>
                                       <label><?php echo number_format($getRenewalAmt, 2); ?></label>
                                   </td>
                                   <td>
                                       <!--  <button type="button" value="Remove" id="cht_cartButton_<?php /*echo cleanXSS($key); */?>" onclick="return addRemoveFromCart('<?php /*echo $email;  */?>' ,'<?php /*echo cleanXSS($key); */?>','<?php /*echo cleanXSS($key); */?>', '<?php /*echo $clientSessionId; */?>', '<?php /*echo $rootUrl; */?>', 'cht')" class="close" data-dismiss="modal">×</button>-->
                                       <button type="button" value="Remove" id="cht_cartButton_<?php echo cleanXSS($keyword); ?>" class="close" onclick="removeRenewKwd('<?php echo cleanXSS($keyword); ?>')" data-dismiss="modal">×</button>

                                   </td>

                               </tr>

                               <?php

                               $totalUPCAmt = $totalUPCAmt + $calcUpcRateOfRenewalAmt;
                               $totalITDAmt = $totalITDAmt + $getRenewalAmt;
                               $totalBTCAmt = $totalBTCAmt + $calcBtcOfRenewalAmt;
                           }
                       }else{

                           ?>

                           <input type="text" value="0" id="noRenewKwdAvailable" />

                           <?php
                       }

                       ?>

                       </tbody>
                       <tfoot>
                       <tr>
                           <td>
                               <span><b>SUBTOTAL</b></span><br>
                           </td>
                           <td></td>
                           <td></td>
                           <td></td>
                       </tr>
                       <tr>
                           <td>
                               <label> BTC: <?php echo $totalBTCAmt; ?></label>
                           </td>
                           <td>
                               <label><?php echo number_format($totalUPCAmt, 2); ?></label>
                           </td>
                           <td>
                               &nbsp; <label><?php echo number_format($totalITDAmt, 2); ?></label>
                           </td>
                           <td></td>
                       </tr>
                       </tfoot>
                            <input type="hidden" id="totalITDAmount" value="<?php echo $totalITDRate; ?>" />
                            <input type="hidden" id="flagCheck" value="1" />

                    <?php
                   }else{
                       print('Error: Fetching current exchange rate');
                   }
               }else{
                   print('Oops!!! Request not handle, Please try after sometime !!!');
               }
           }else{
               print('Something wents wrong, Please try after sometime !!!!');
           }
    }else{
        print('Error: Authorisation failure');
    }
}else{
    print('Error: Mandatory field not found');
}

?>


<script>

    //get keyword unit price

    //get keyword total price in USD
    var  finalITDAmt = <?php echo $totalITDAmt;?>;
    //get keyword basket with tags
    var  kwdRenewInCart = '<?php echo rtrim($renewKwdInCart,','); ?>';

</script>




