<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 18/2/17
 * Time: 1:09 PM
 */


header("Access-Control-Allow-Origin: *");
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/header/headerModel.php");

error_reporting(0);

//code to check whether session is running or not
$email = $_SESSION["email"];
$upc = $_SESSION['CurrPreference'];
// get current session id
$clientSessionId = session_id();

$errCode = 1;
$errMsg = '';
if (!empty($email)) {
    //Keywords database connection
    $kwdConn = createDbConnection('dbkeywords');

    if (noError($kwdConn)) {
        $kwdConn = $kwdConn["connection"];
    } else {
        print("Error: Database connection");
        exit;
    }

    $presaleStatus = getPresaleStatus($kwdConn);

    if (noError($presaleStatus)) {
        $presaleStatus = $presaleStatus["errMsg"]["presale_status"];
    } else {
        echo "error: Presale status is closed";
    }


    $buyerBalanceFields = $userRequiredFields . ",cashback,first_buy_status";
    $buyerBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $buyerBalanceFields);
    /*echo "buyer getUserInfo</br>";
    printArr($buyerBalanceDetails);*/
    if (noError($buyerBalanceDetails)) {

        $firstBuyStatus  = $buyerBalanceDetails["errMsg"]["first_buy_status"];

        /* Get user admin settings from keyword admin */
        $adminSettings = getAdminSettingsFromKeywordAdmin($kwdConn);
        if(noError($adminSettings)) {
            $errMsg = "getting admin settings of buyer user success.";
            $firstBuyPercent = $adminSettings["data"]["first_buy_percent"];
        } else {
            print("Error: getting user settings");
            exit;
        }
    } else {
        print("Error: getting user details");
        exit;
    }

    // get user total available balance
    $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";

    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);
    if (noError($getUserDetails)) {
        $userCurrencyPreference = $getUserDetails['errMsg']['currencyPreference'];
        $getUserBalance = calculateUserBalance($getUserDetails);

        if (noError($getUserBalance)) {
            $userBalance = $getUserBalance['errMsg']['total_available_balance'];
        } else {
            $userBalance = "Error: Getting user available balance";
            exit;
        }
    } else {
        $userBalance = "Error: Getting balance details";
        exit;
    }

    $cartData = getUserTotalCartPrice($kwdConn, $email);

    if (noError($cartData)) {
        $cartData = $cartData["errMsg"];
        $cartKwdData = $cartData["keywords"];

        $totalCount = count($cartData["keywords"]);
        $total_cart_price = $cartData["total_cart_price"];

        if($total_cart_price > $userBalance){
            $purchaseITDAmt = number_format((float)$total_cart_price - $userBalance, 2);
        }else{
            $purchaseITDAmt = 0.00000000;
        }

        $getExchangeRate = getExchangeRates($upc);

        if (noError($getExchangeRate)) {
            $exchangeRates = $getExchangeRate['currRate'];
        }else{
            print("Error: getting exchange rate");
        }

        $upc = strtolower($upc);

        if($upc == 'btc'){
            $precision = 8;
        }else{
            $precision = 4;
        }

        if($firstBuyStatus){
            $firstBuyCashBack = number_format(($total_cart_price * $firstBuyPercent)/100,8);
        }else{
            $firstBuyCashBack = 0.00000000;
        }

      // $cartPriceAfterDiscount = number_format((float)$total_cart_price - $firstBuyCashBack,2);
       $cartPriceAfterDiscount = ((float)$total_cart_price - $firstBuyCashBack);

        // subtotal cart price in UPC and ITD
        $subTotalInBtc = $total_cart_price * $exchangeRates["btc"];
        $subTotalInUPC = $total_cart_price * $exchangeRates["{$upc}"];
        $subTotalInITD = $total_cart_price * $exchangeRates['usd'];

        // less discount
        $lessDiscountInBTC = $firstBuyCashBack * $exchangeRates["btc"];
        $lessDiscountInUPC = $firstBuyCashBack * $exchangeRates["{$upc}"];
        $lessDiscountInITD = $firstBuyCashBack * $exchangeRates['usd'];

        // total cart price in BTC, ITD, UPC
        $totalCartPriceInBTC = $cartPriceAfterDiscount * $exchangeRates['btc'];
        $totalCartPriceInUPC = $cartPriceAfterDiscount * $exchangeRates["{$upc}"];
        $totalCartPriceInITD = $cartPriceAfterDiscount * $exchangeRates['usd'];

        /*$currentCurrrency=$_SESSION["curr_pref"];
        $BtcExchangeRate= getBTCExchangeRateUSD($currentCurrrency);*/

        if ($totalCount > 0) {
            $errCode = -1;
            ?>
            <thead class="">
            <tr>

                <th>Keyword</th>
                <th>
                    <?php echo $_SESSION['CurrPreference']; ?>

                </th>
                <th><?php echo $keywoDefaultCurrencyName;  ?></th>

            </tr>
            </thead>
            <tbody id="tooltip-arrw--left" >
            <?php foreach ($cartKwdData as $key => $price) {
                $keywordBasket .= $key.",";
                $keywordBasketwithTags .= "#".$key.",";
                $checkForKeywordAvailability = checkForKeywordAvailability($key, $kwdConn);
                if(noError($checkForKeywordAvailability)){
                    $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                    //printArr($availabilityFlag);
                    //$availabilityFlag = "Not available";
                    if ($availabilityFlag == "keyword_available") { ?>

                        <tr>
                            <td>
                              <span class="ellipses ellipses-general"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo $key;  ?>');" title="" data-toggle="tooltip" data-placement="bottom" href="?q=<?php echo $key;  ?>" data-original-title="<?php echo "#{$key}"; ?>"><?php echo "#{$key}"; ?></a>
                              </span><br>
                                <label class="ellipses ellipses-general" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$key}"; ?>' data-original-title="<?php echo number_format($price * $exchangeRates['btc'], 8); ?>" >BTC : <?php echo number_format($price * $exchangeRates['btc'], 8); ?></label>
                            </td>
                            <td>
                                <label class="ellipses ellipses-check" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$key}"; ?>' data-original-title="<?php echo number_format($price  * $exchangeRates["{$upc}"], $precision); ?>"><?php echo number_format($price  * $exchangeRates["{$upc}"], $precision); ?></label>
                            </td>
                            <td>
                                <label class="ellipses ellipses-check-itd" title="" data-toggle="tooltip" data-placement="top" href='?q=<?php echo "#{$key}"; ?>' data-original-title="<?php echo number_format($price * $exchangeRates['usd'], 4); ?>"><?php echo number_format($price * $exchangeRates['usd'], 4); ?></label>

                                <i class="fa fa-close cancel-keyword-btn" id="cht_cartButton_<?php echo cleanXSS($key); ?>" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($key); ?>','<?php echo cleanXSS($key); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'cht')"></i>
                            </td>
                      </tr>

                    <?php } else { ?>

                        <tr>
                            <td>
                                <span><?php echo "#{$key}"; ?></span><br>
                                <label class="ellipses ellipses-general" title="" data-toggle="tooltip" data-placement="bottom" href='?q=<?php echo "#{$key}"; ?>' data-original-title="<?php echo number_format($price * $exchangeRates['btc'], 8); ?>" >BTC : <?php echo number_format($price * $exchangeRates['btc'], 8); ?></label>
                            </td>
                            <td>
                                <label><?php echo "keyword NA"; ?></label>
                            </td>
                            <td>
                              <i class="fa fa-close cancel-keyword-btn" id="cht_cartButton_<?php echo cleanXSS($key); ?>" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($key); ?>','<?php echo cleanXSS($key); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'cht')"></i>
                            </td>
                             <!-- <td>
                                <button type="button" type="button" value="Remove" id="cht_cartButton_<?php echo cleanXSS($key); ?>" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($key); ?>','<?php echo cleanXSS($key); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'cht')" class="close" data-dismiss="modal">×</button>
                            </td> -->
                        </tr>

                    <?php }

                }else{
                    $errCode = 1;
                    $errMsg = "Error checking keyword availabilty";
                }
            } ?>
            </tbody>
            <tfoot>
            <tr>
                <td>
                    <span><b>SUBTOTAL</b></span><br>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <label class="ellipses ellipses-check" title="<?php echo number_format("{$subTotalInBtc}", 8,'.',''); ?>" data-toggle="tooltip" data-placement="bottom">BTC : <?php echo formatNumberToSort($subTotalInBtc, 8); ?></label>
                </td>
                <td>
                    <label class="ellipses ellipses-check" title="<?php echo number_format("{$subTotalInUPC}", $precision,'.',''); echo "&nbsp;".strtoupper($upc); ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($subTotalInUPC, $precision); ?></label>
                </td>
                <td>
                    <label class="ellipses ellipses-check-itd" title="<?php echo number_format("{$subTotalInITD}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($subTotalInITD, 4); ?></label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Less Discount</label><br>
                </td>
                <!--<td>
                    <label class="ellipses ellipses-check" title="<?php /*echo number_format("{$lessDiscountInBTC}", 8,'.',''); */?>" data-toggle="tooltip" data-placement="bottom">BTC : <?php /*echo formatNumberToSort($lessDiscountInBTC, 8); */?></label>
                </td>-->
                <td>
                    <label class="ellipses-check" title="<?php echo number_format("{$lessDiscountInUPC}", $precision,'.',''); echo "&nbsp;".strtoupper($upc); ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($lessDiscountInUPC, $precision); ?></label>
                </td>
                <td>
                    <label class="ellipses-check-itd" title="<?php echo number_format("{$lessDiscountInITD}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($lessDiscountInITD, 4); ?></label>
                </td>
                 <!-- <td>
                    &nbsp;
                </td> -->
            </tr>
            <tr>
                <td>
                    <span><b>TOTAL</b></span><br>
                    <label class="ellipses ellipses-general" title="<?php echo number_format("{$totalCartPriceInBTC}", 8,'.',''); ?>" data-toggle="tooltip" data-placement="bottom">BTC : <?php echo number_format($totalCartPriceInBTC, 8); ?></label>
                </td>
                <td>
                    <label class="ellipses ellipses-check" title="<?php echo number_format("{$totalCartPriceInUPC}", $precision,'.',''); echo "&nbsp;".strtoupper($upc); ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($totalCartPriceInUPC, $precision); ?></label>
                </td>
                <td>
                    <label class="ellipses ellipses-check-itd" title="<?php echo number_format("{$totalCartPriceInITD}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort($totalCartPriceInITD, 4); ?></label>
                </td>
                 <!-- <td>
                    &nbsp;
                </td> -->
            </tr>
            </tfoot>
        <?php
            /* total count if close */

            $insertBitgoDetails = insertUserCartKeywordForPayment($kwdConn, rtrim($keywordBasket,','), $total_cart_price, $email, 'bitgo', $purchaseITDAmt);
            if(noError($insertBitgoDetails)){

                $orderIdForBitgo = $insertBitgoDetails['orderId'];
                /* Insert record fpr bitgo */
                $insertPaypalDetails = insertUserCartKeywordForPayment($kwdConn, rtrim($keywordBasket,','), $total_cart_price, $email, 'paypal', $purchaseITDAmt);
                if(noError($insertPaypalDetails)){

                    $orderIdForPaypal = $insertPaypalDetails['orderId'];
                    /* Insert record fpr paypal */
                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = "Success";
                } else {
                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"] = $insertPaypalDetails["errMsg"];
                }
            } else {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = $insertBitgoDetails["errMsg"];
            }

        } else {
            $errCode = 1;
            $errMsg = "No keyword in cart";
        } ?>

    <?php } else {
        $errCode = 2;
        $errMsg = "Error: Fetching cart details";
    }

} else {
    $errCode = 3;
    $errMsg = "Session Expired";
}

if ($errCode != -1) { ?>
    <thead class="">
    <tr>

        <th>Keyword</th>
        <th class="ellipses-check">
            <?php echo $_SESSION['CurrPreference']; ?>

        </th>
        <th class="ellipses-check-itd">itd</th>
        <!-- <th></th> -->
    </tr>
    </thead>
    <tbody>
    <tr>
        <td></td>
        <td class="ellipses-check">
            <label><?php echo $errMsg; ?></label>
        </td>
        <td class="ellipses-check-itd"></td>
        <!-- <td></td> -->
    </tr>
    </tbody>
    <tfoot>
    <tr>
        <td>
            <span>Balance</span><br>
            <label>BTC : 0.00</label>
        </td>
        <td class="ellipses-check">
            <label>0.00</label>
        </td>
        <td class="ellipses-check-itd">
            <label>0.00</label>
        </td>
        <!-- <td></td> -->
    </tr>
    </tfoot>
<?php }


?>


<script>
    //get keyword unit price

    //get keyword total price in USD
    var  finalamtUSD= <?php echo $total_cart_price;?>;
    //get keyword basket with tags
    var  keywordBasketwithTags= '<?php echo cleanDisplayParameter($kwdConn,rtrim($keywordBasketwithTags,',')); ?>';
    var  keywordBasket= '<?php echo cleanDisplayParameter($kwdConn,rtrim($keywordBasket,',')); ?>';
    var orderIdForPaypal = '<?php echo $orderIdForPaypal; ?>';
    var orderIdForBitgo = '<?php echo $orderIdForBitgo; ?>';
    //var keywordBasketwithTags = $("<div/>").html(keywordBasketwithTags).text();
</script>
<?php

?>

<div>
    <form style="display:block;" id="buyByPaypalForm" method="post" action="<?php echo $rootUrl; ?>controllers/paypal/Paypal-Express/process.php">
        <input  name="itemname" id="keywordBasketPaypal" value="keywordBasket" type="hidden" />
        <input  name="itemnumber" id="itemnumberPaypal" value="<?php echo $orderIdForPaypal; ?>" type="hidden"/>
        <input  name="itemdesc" value="Keyword Purchase at scoinz.com. <?php echo $presaleRootURL;?>views/orderRecords.php?orderId=<?php echo $orderIdForPaypal;?>" type="hidden"/>
        <input  name="grandTotal" id="totalPricePaypal" type="hidden"/>
        <input  name="itemprice" id="itemPricePaypal" type="hidden"/>
        <input  name="itemQty" id="itemQtyPaypal" value="<?php echo $usercartCount;?>" type="hidden"/>
        <input  name="custom" id="customPaypal" type="hidden"/>
    </form>
</div>

<?php
function parseFloat($value) {
    return floatval(preg_replace('#^([-]*[0-9\.,\' ]+?)((\.|,){1}([0-9-]{1,3}))*$#e', "str_replace(array('.', ',', \"'\", ' '), '', '\\1') . '.\\4'", $value));
}

?>
