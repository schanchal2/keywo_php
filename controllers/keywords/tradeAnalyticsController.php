<?php

session_start();
header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/keywords/ViewallShortingModel.php");
require_once("{$docrootpath}helpers/sessionHelper.php");
error_reporting(0);

$conn = createDBConnection('dbkeywords');
if(noError($conn))
{
    $conn = $conn["connection"];
}
else
{
    print_r("Database Error");
}
$clientSessionId = session_id();

$email        = cleanQueryParameter($conn, cleanXSS($_POST["email"]));
$limit        = cleanQueryParameter($conn, cleanXSS($_POST["limit"]));
$tableName    = cleanQueryParameter($conn, cleanXSS($_POST["tableName"]));
$type         = cleanQueryParameter($conn, cleanXSS($_POST["type"]));
$orderby      = cleanQueryParameter($conn, cleanXSS($_POST["orderby"]));
$startDate    = $_POST["startDate"]; //printArr($startDate);
$endDate      = $_POST["endDate"]; //printArr($endDate);
$keywordPrice = cleanQueryParameter($conn, cleanXSS($_POST["keywordPrice"]));

if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}

if ($page_number == 0 ) {
    $page_number = 1;
}

$item_per_page = $limit;

$get_total_rows = getRequestCount($conn, $tableName, $type, $orderby,$startDate,$endDate)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);

$results = getRequestData($page_position, $limit, $tableName, $type, $orderby, $startDate,$endDate,$conn);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getRequestCount($conn, $tableName, $type, $orderby,$startDate,$endDate)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getRequestData($page_position, $limit, $tableName, $type, $orderby, $startDate,$endDate,$conn);
    $results = $results["errMsg"];

}

?>
<ul class="border-all short" id="latesttrade">
    <?php
    $pageUrl = "views/keywords/analytics/keyword_analytics.php";
    if($count == 0)
    {
        ?>

        <li>
            <div class="row">
                <div class="col-md-12">
                    No Data Available
                </div>

            </div>
        </li>

        <?php exit; }
    else
    {

        foreach($results as $value){

            $purchase_price = $value[$keywordPrice];
            $keyword = $value["keyword"]; //echo $keyword;

            ?>

            <!-- row ends here -->
            <li id="latesttradevalue">
            <div class="row">

            <div class="col-md-4 text-left" id="tooltip-arrw--left">
                <label class="ellipses-market-trade-label"><a class="display-in-block" value="<?php echo $keyword; ?>" title="<?php echo $keyword; ?>" data-placement="bottom" data-toggle="tooltip" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></label>
            </div>

            <div class="col-md-3 text-black">
            <span class="text-black ellipses margin-bottom-none margin-top-none">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                          origPrice="<?php echo number_format("{$purchase_price}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$purchase_price}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$purchase_price}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
            </div>

            <div class="col-md-5 pull-right text-right">

            <?php

            //print_r($getLatestBid); die;
            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);

            //  printArr($checkForKeywordAvailability);
            if(noError($checkForKeywordAvailability)){

                $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                $highestBidAmtForKwd = $checkForKeywordAvailability["highest_bid_amount"];
                $kwdAskPrice = $checkForKeywordAvailability['ask_price'];
                $kwdOwnerId = $checkForKeywordAvailability['buyer_id']; //printArr($kwdOwnerId);
                $CartStatus = $checkForKeywordAvailability['status'];
                $activeBids = $checkForKeywordAvailability["active_bids"]; //print_r($activeBids);

                $activeBids = json_decode($activeBids, true);
                foreach($activeBids as $key => $bidValue){
                    $bidValue = explode('~~', $bidValue);
                    $bidderEmail[] = $bidValue[1]; //print_r($bidderEmail);

                }
                if(in_array($email, $bidderEmail)){
                    $bidStatus = true;
                }else{
                    $bidStatus = false;
                }

                if($email == $kwdOwnerId){

                    // echo "Testing".$email.$kwdOwnerId;

                    ?>

                    <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                        ?>

                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>
                            </div>
                        </div>
                        <?php
                    }else{

                        ?>
                        <div class="row pull-right">
                            <div class="col-xs-6 text-right padding-right-none">
                                <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                            </div>
                        </div>

                        <?php
                    }

                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                        //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Accept Bid</button></div>';
                        ?>

                        <div class="col-md-5 pull-right text-right">
                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                }else{

                    if($CartStatus == "sold"){

                        if(empty($activeBids)){
                            //echo "activeBids ".$activeBids;
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                            ?>

                            <div class="row pull-right">
                                <div class="col-xs-6 text-right padding-right-none">
                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                </div>
                            </div>

                            <?php

                        }else{
                            if($bidStatus){
                                //echo "bidStatus ".$bidStatus
                                // echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Edit Bid</button></div>';
                                ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>
                                    </div>
                                </div>

                                <?php
                            }else{
                                //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Place Bid</button></div>';
                                ?>

                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>
                                    </div>
                                </div>

                            <?php }}

                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                            //echo '<button class="btn-trading" value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" >Buy Now</button></div>';
                            ?>

                            <div class="col-md-5 pull-right text-right">
                                <div class="row pull-right">
                                    <div class="col-xs-6 text-right padding-right-none">
                                        <button class="btn-trading-dark"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                                    </div>
                                </div>
                            </div>

                        <?php }}} ?>


                </div>

                </div>

                </li>
            <?php } else if($checkForKeywordAvailability["errCode"] == 4) {?>

                <div class="col-md-5 pull-right text-right">
                    <div class="row pull-right">

                        Not Available

                    </div>
                </div>

            <?php }}}?></ul>

<div class="pagination-cont pull-right gotovalues">
    <span class="span-keyword-market pull-left">Go to : &nbsp;</span>

    <?php

    getPaginationData($lastpage, $page_number, $type,$orderby,$startDate,$endDate,$limit);
    function getPaginationData($lastpage, $pageno, $type,$orderby,$startDate,$endDate,$limit)
    {
        ?>

        <input type="text" value="<?php echo $pageno; ?>" placeholder="" title="please press enter key" data-placement="bottom" data-toggle="tooltip" id="LimitedResultData" class="span-blue-keyword-market border-all allownumericwithoutdecimal">

        <?php
        echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
        echo '<ul class="pagination">';


        if ($pageno > 1) {

            $pagenum = 1;
            print('<li class="status"><a href="#"onclick=getNewTradeData("' . $pagenum . '","' . $type . '","' . $orderby . '","' . $startDate . '","' . $endDate . '","' . $limit . '")>&laquo;</a></li>');
        }

        if ($pageno > 1) {
            $pagenumber = $pageno - 1;
            print('<li class="status"><a href="#" onclick=getNewTradeData("' . $pagenumber . '","'. $type . '","' . $orderby . '","' . $startDate . '","' . $endDate . '","' . $limit . '")>Previous</a></li>');
        }

        if ($pageno == 1) {
            $startLoop = 1;
            $endLoop = ($lastpage < 5) ? $lastpage : 5;
        } else if ($pageno == $lastpage) {
            $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
            $endLoop = $lastpage;
        } else {
            $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
            $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
        }

        for ($i = $startLoop; $i <= $endLoop; $i++) {
            if ($i == $pageno) {
                print('   <li class = "status active"><a href = "#">' . $pageno . '</a></li>');
            } else {
                $pagenumber = $i;
                print('<li class="status"><a href="#" onclick=getNewTradeData("' . $pagenumber . '","' . $type . '","' . $orderby . '","' . $startDate . '","' . $endDate . '","' . $limit . '")>' . $i . '</a></li>');
            }
        }
        if ($pageno < $lastpage) {
            $pagenumber = $pageno + 1;
            print('<li class="status"><a href="#" onclick=getNewTradeData("' . $pagenumber . '","' . $type . '","' . $orderby . '","' . $startDate . '","' . $endDate . '","' . $limit . '")>Next</a></li>');

        }

        if ($pageno != $lastpage) {
            print('<li class="status"><a href="#" onclick=getNewTradeData("' . $lastpage . '","' . $type . '","' . $orderby . '","' . $startDate . '","' . $endDate . '","' . $limit . '")>&raquo;</a></li>');
        }


        echo '</ul>';
        echo '</div>';
    }


    ?>

</div>

<script>

    $('#LimitedResultData').tooltip({ placement: "left", trigger: "focus" });

    $("#LimitedResultData").keypress(function(e)
    {
        if(e.which == 13) {
            var limit = '<?php echo $limit; ?>';
            var id = ''; //alert(id);
            var type = '<?php echo $type; ?>';
            var pageNo = parseInt($(this).val()); //alert(pageNo);
            var lastPage = parseInt('<?php echo $lastpage; ?>'); //alert(lastPage);

            if (pageNo == 0 || pageNo > lastPage) {
                pageNo = 1;
                showToast("success", "This page is not available");
            }

            getNewTradeData(pageNo, type,"","","", limit);
        }

    });

    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var charCode = event.which;

        if(charCode == 8 || charCode == 0)
        {
            return;
        }
        else
        {
            var keyChar = String.fromCharCode(charCode);
            return /[a-zA-Z0-9]/.test(keyChar);
        }

    });

</script>