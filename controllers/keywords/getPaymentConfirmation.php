<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 30/3/17
 * Time: 1:29 PM
 */

if(!isset($_SESSION)){session_start();}
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");

error_reporting(0);

$returnArr = array();
$clientSessionId = $_POST['client_sid'];
$recieverAddress = $_POST['recieverAddress'];
$orderId  = $_POST['orderId'];

$clientSessionId  = urldecode($clientSessionId);
//get current session Id and refferer page
$currentSessionId    = session_id();

if ($clientSessionId == $currentSessionId) {
    $kwdConn = createDbConnection('dbkeywords');

    if(noError($kwdConn))
    {
        $kwdConn = $kwdConn["connection"];
        //checkForSession($kwdConn);
        $email = $_SESSION["email"];

        if(isset($email) && !empty($email) && !empty($orderId) && !empty($recieverAddress)) {

            $query = "SELECT `no_of_confirmation`,`amount_recieved`,`paid_amount`,`status` from payments WHERE order_id='" . $orderId . "' AND username = '" . $email . "' AND payment_address='" . $recieverAddress . "'";
            $result = runQuery($query, $kwdConn);

            if (noError($result)) {
                $data = array();

                while ($row = mysqli_fetch_assoc($result["dbResource"]))
                    $data = $row;

                if($data['no_of_confirmation'] > -1 && $data['amount_recieved'] > -1){
                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"]["amount_recieved"] = $data['amount_recieved'];
                    $returnArr["errMsg"]["status"] = $data['status'];
                    $returnArr["noOfConfirmation"] = $data['no_of_confirmation'];
                } else {
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = $data['status'];
                }
                //$trans = commitTransaction($conn);
            } else {
                //$trans = rollbackTransaction($conn);
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"]     = "Error getting payment details";
            }

        } else {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]      = "Mandatory field not found";
        }
    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = "Error establishing database connection";
    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
}

echo json_encode($returnArr);

?>