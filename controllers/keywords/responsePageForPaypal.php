<?php

session_start();

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/walletHelper.php');
require_once ('../../models/keywords/keywordPurchaseModel.php');
require_once ('../../backend_libraries/xmlProcessor/xmlProcessor.php');
error_reporting(0);

$ip              = getClientIP();
$paypal_response = $_SESSION["paypal_response"];
$json_val = json_encode($paypal_response);

$logStorePath = $logPath["wallet"];
$xmlFilename = "purchaseITDByPaypal.xml";

/****** log write section ******/
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$custom          = $paypal_response["CUSTOM"];
$custom          = urldecode($custom);
$decryptedCustom = explode('~', $custom);    
$userEmail = $decryptedCustom[0];
/* Log initialization start */
$xmlArray = initializeXMLLog($userEmail);

$i= 1;
$errMsg = "Start ITD purchase process by paypal";
$xml_data["step".$i]["data"] = $i.". {$errMsg}";

if (isset($paypal_response) && !empty($paypal_response)){
    $errMsg = "Success:Madatory fields check";
    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
    //success getting paypal response
    $totalPrice      = urldecode($paypal_response["AMT"]);
    $totalPrice      = (float)$totalPrice;
    $custom          = $paypal_response["CUSTOM"];
    $custom          = urldecode($custom);
    $originalOrderId = $paypal_response["L_NUMBER0"];
    $originalOrderId = urldecode($originalOrderId);
    $paypalEmail     = $paypal_response["EMAIL"];
    $paypalEmail     = urldecode($paypalEmail);
    $transactionId   = $paypal_response["PAYMENTREQUEST_0_TRANSACTIONID"];
    $transactionId   = urldecode($transactionId);
    $status          = "paid";
    
     //decrypting custom
    $decryptedCustom = explode('~', $custom);
    
    $originalUserName = $decryptedCustom[0];
    //$keywordBasket    = $decryptedCustom[1];
    $priceInBtc       = $decryptedCustom[1];
    $conversionRate   = number_format($decryptedCustom[2], 2);
    $conversionRateUSD   = $decryptedCustom[3]; 
    $amount_paid_usd     = $totalPrice / $conversionRate;
	
	//convert from USD amount to BTC
//	$currentRateBTC = getBTCExchangeRateUSD("BTC");
//	$amount_paid_btc = $amount_paid_usd * $currentRateBTC;
	
	$errCode = null;

    // create search database connection
    $searchDbConn = createDBConnection('dbsearch');
    if(noError($searchDbConn)) {
        $searchDbConn = $searchDbConn["connection"];

        $errMsg = "Success:Connected to search database";
        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
        $currentExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
        if(noError($currentExchangeRate)){
            $errMsg = "Success:Fetched current exchange rates";
            $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
            $currentExchangeRate = $currentExchangeRate["exchange_rate"];
            // convert array into json encode
            $currentExchangeRate = json_encode($currentExchangeRate);

            mysqli_close($searchDbConn);


            //Create database connection
            $conn = createDBConnection("dbkeywords");

            if (noError($conn)) {

                $conn = $conn["connection"];
                $errMsg = "Success: Connected to keywords database";
                $xml_data["step".++$i]["data"] = $i.". {$errMsg}";

                $result     = getPaymentDetailsByOrderId($originalOrderId, $originalUserName, $conn);

                $response = json_encode($result);

                $amount_due_in_sgd = $result["errMsg"]["amount_due_in_sgd"];
                $amount_due = $result["errMsg"]["amount_due"];
                $keywordBasket  = $result["errMsg"]["keyword_cart"];

                //getting keyword price
                // $paymentDetails = getTotalPaymentDetails($originalOrderId, $originalUserName, $conn);
                // $keywordBasket  = $paymentDetails["errMsg"]["keyword_cart"];

                if ($totalPrice <= (floor($amount_due_in_sgd)+1) && $totalPrice >= (floor($amount_due_in_sgd)-1)) {
                    $errMsg = "Success: Checked paid amount ";
                    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                    if (noError($result)) {
                        $errMsg = "Success: Fetched payments details";
                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                        // $userReqField = $userRequiredFields.",_id";
                        // get user info
                        $getUserDetails = getUserInfo($originalUserName, $walletURLIP . 'api/v3/', $userRequiredFields);
                        if(noError($getUserDetails)){
                            $errMsg = "Success: Fetched user details";
                            $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                            $getUserDetails = $getUserDetails["errMsg"];
                            $userId = $getUserDetails["_id"];
                            $firstName = $getUserDetails["first_name"];
                            $lastName = $getUserDetails["last_name"];

                            /*  Credit itd purchase amount to user balance */
                            $creditEarning   = creditUserEarning($userId, $amount_due, 'purchase_itd', 'add');

                            if(noError($creditEarning)){
                                $errMsg = "Success: Credited user earnings";
                                $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                // insert transaction for purchase itd from paypal

                                // insert user transaction for trades fees
                                // parameters required to insert in transaction
                                $recipientEmail     = $originalUserName;
                                $recipientUserId    = $userId;
                                $senderEmail        = $communityPoolUser;
                                $senderUserId       = $communityPoolUserId;
                                $amount             = number_format($amount_due, 8);
                                $type               = "purchase_itd";
                                $paymentMode        = 'paypal';
                                $originIp           = $ip;
                                $exchangeRateInJson = $currentExchangeRate;
                                $metaDetails = array("buyer_id" => $originalUserName, "raise_on" => "ITD purchase by paypal", "purchase_amount_in_sgd" => $totalPrice,'country' => $xmlArray['activity']['country'],'gender' => $xmlArray['activity']['gender'],'device' => $xmlArray['activity']['device']);

                                $insertPaypalPaymentTrans = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);

									if(noError($insertPaypalPaymentTrans)){
                                    $errMsg = "Success: Inserted purchase itd transaction";
                                    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                    //Update payments table
                                    if($keywordBasket =="itd_purchase_only"){
                                        $query = "UPDATE payments SET paid_amount=" . $amount_paid_usd . ",payment_transaction_id='" . $transactionId . "',status='" . $status . "',payment_address='" . $paymentAddress . "',payment_time=now(),request_ip_address='" . $ip . "',total_payment_data='" . $json_val . "',refund_address='" . $refundAddress . "',customer_email='" . $paypalEmail . "' WHERE order_id='" . $originalOrderId . "' AND username = '" . $originalUserName . "'";
                                    }else{
                                        $query = "UPDATE payments SET paid_amount=" . $amount_paid_usd . ",payment_transaction_id='" . $transactionId . "',status='" . $status . "',payment_address='" . $paymentAddress . "',payment_time=now(),request_ip_address='" . $ip . "',total_payment_data='" . $json_val . "',refund_address='" . $refundAddress . "',customer_email='" . $paypalEmail . "',payment_type='keyword_purchase' WHERE order_id='" . $originalOrderId . "' AND username = '" . $originalUserName . "'";
                                    }

                                    $result = runQuery($query, $conn);
                                    if(noError($result) && $keywordBasket =="itd_purchase_only"){
                                        $errMsg = "Success: Updated payment details";
                                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                        $errCode = -1;
										unset($_SESSION["paypal_response"]);
                                        unset($paypal_response);
										
										 //send notification to ITD sender.  
										$email = $originalUserName;
										$mailSubject = sprintf($notificationEmails['purchase_by_paypal']['sub']);
										$emailBody = sprintf($notificationEmails['purchase_by_paypal']['body'], ucfirst($firstName),$amount.' CREDIT');
										$firstName = $firstName;
										$lastName = $lastName;
										$userId = $userId;
										$notificationBody = sprintf($notificationMessages['purchase_by_paypal']);
										$preferenceCode = 2;
										$category = "wallet";

										$res = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);
                                    }else {
                                        $errMsg = "Error: Failed to update payment details";
                                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                        $errCode = 2;
                                    }


                                     if (noError($result) && $keywordBasket !="itd_purchase_only") {
                                         $errMsg = "Success: Updated payment details and redirecting to buyKeywordByPaypal.php page";
                                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";

                                        $respData["errCode"]="-1";
                                        $respData["errMsg"]=$errMsg;

                                         $xml_data['response']["data"] = "";
                                         $xml_data['response']["attributes"] = $respData;

                                         $xml_data['purchaseAmount']["data"] = $amount;

                                         echo "<form style='display:none' id='buyForm' method='post' action ='".$rootUrl."controllers/keywords/buyKeywordByPaypal.php' >
                                         <input type='hidden' name='email' value='".$originalUserName."'><input>
                                         <input type='hidden' name='keywordBasket' value='".$keywordBasket."'><input>
                                         <input type='hidden' name='totalPrice' value='".$amount_due."'><input>
                                         <input type='hidden' name='payment_mode' value='paypal'><input>
                                         <input type='hidden' name='conversionRate' value='".$conversionRate."'><input>
                                         <input type='hidden' name='conversionRateUSD' value='".$conversionRateUSD."'><input>
    
                                         </form>";
                                         $errCode = -1;
                                         // create or update xml log Files
                                        $xmlProcessor->writeXML("purchaseITDByPaypal.xml", $logStorePath, $xml_data, $xmlArray["activity"]);
                                         echo "Processing ..";

                                         // unset the paypal response to prevent increase ITD amount on click on browser back button
                                         unset($_SESSION["paypal_response"]);
                                         unset($paypal_response);

                                         echo "<script>document.getElementById('buyForm').submit();</script>";

                                    } else if($keywordBasket !="itd_purchase_only"){
                                        $errMsg = "Error: Failed to update payment details";
                                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                        $errCode = 2;
                                    }
                                }else{
                                    $errMsg = "Error: Failed to inserted purchase itd transaction";
                                    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                   $errCode = 2;
                                }
                            }else{
                                $errMsg = "Error: Failed to credit user earnings";
                                $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                                $errCode = 2;
                            }
                        }else{
                            $errMsg = "Error: Failed to fetched user details";
                            $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                            $errCode = 2;
                        }
                    } else {
                        $errMsg = "Error: Failed to fetch payments details";
                        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                        $errCode = 2;

                    }
                } else {
                    $errMsg = "Error: Failed to checked paid amount";
                    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                    $errCode = 2;
                }
            } else {
                $errMsg = "Error: failed to connect keywords database";
                $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
                $errCode = 2;
            }

        } else{
            $errMsg = "Error: Failed to fetch current exchange rates";
            $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
            $errCode = 2;
        }
    } else{
        $errMsg = "Error: failed to connect search database";
        $xml_data["step".++$i]["data"] = $i.". {$errMsg}";
        $errCode = 2;
    }

} else {
    $errMsg = "Error: Madatory fields not found.";
    $xml_data["step".++$i]["data"] = $i.". {$errMsg}";

    //failure getting paypal response
    $errCode = 2;
}
$responseArr["errCode"]= $errCode;
$responseArr["errMsg"]= $errMsg;
$xml_data['response']["data"] = "";
$xml_data['response']["attributes"] = $responseArr;

if($errCode == 2 && $keywordBasket !="itd_purchase_only"){

    // create or update xml log Files
    $xmlProcessor->writeXML("purchaseITDByPaypal.xml", $logStorePath, $xml_data, $xmlArray["activity"]);

    //error buying keyword
    $errorMessage        = "Error buying keywords";
    $redirectURL = "../../views/keywords/cart/checkout.php";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");
}elseif($keywordBasket =="itd_purchase_only"){
    // create or update xml log Files
    $xmlProcessor->writeXML("purchaseITDByPaypal.xml", $logStorePath, $xml_data, $xmlArray["activity"]);
	if($errCode == -1){
		$errorMessage        = "Purchase ITD successful. ";
	}else{
		$errorMessage        = "Error in buying ITD by Paypal";
	}
	
	//closing deposit log file
	$_SESSION['errMsg']  = $errorMessage;
    $redirectURL = "../../views/wallet/index.php";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $redirectURL . "';\", 000);");
    print("</script>");

}

//closing deposit log file
$_SESSION['errMsg']  = $errorMessage;
$_SESSION['errCode'] = $errorCode;
$_SESSION['count'] = 1;

?>