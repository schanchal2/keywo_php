<?php

if(!isset($_SESSION)){session_start();}
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/stringHelper.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/keywords/keywordPurchaseModel.php");
/*require_once '../controller/session.php';*/

error_reporting(0);

/*User currency preference*/
$upc = $_SESSION['CurrPreference'];

// Get value from post
$returnArr = array();
$clientSessionId = $_POST['client_sid'];
$finalAmountInUSD = $_POST['finalAmount'];
$keywordBasket = $_POST['keywordBasket'];
$paymentMode = $_POST['mode'];
$orderId  = $_POST['encryptedCustom'];

//Clean XSS
$clientSessionId           = urldecode($clientSessionId);
$totalPriceInUSD   = urldecode($finalAmountInUSD);
$keywordBasket       = urldecode($keywordBasket);
$orderId = urldecode($orderId);
$userId = $_SESSION['id'];

//get current session Id and refferer page
$currentSessionId    = session_id();

if ($clientSessionId == $currentSessionId) {

    $kwdConn = createDbConnection('dbkeywords');

    if(noError($kwdConn))
    {
        $kwdConn = $kwdConn["connection"];
        /*checkForSession($kwdConn);*/
        $email 		      = $_SESSION["email"];

        if(isset($email) && !empty($email) && !empty($orderId)) {


            /*$result = getMyCartTotalPriceUSD($email, $kwdConn);
            $totalPrice = $result["errMsg"]["total_cart_price"];

            //convert from USD amount to BTC
            $currentRateBTC = getBTCExchangeRateUSD("BTC");
            $totalPrice = $totalPrice * $currentRateBTC;

            $finalamt = $totalPrice;*/


            // Wallet ID for Bitgo Account (triloknathrajbhar@bitstreet.in)
            //$walletId = '2N3Koruw9uJ1RGo4kYzxaWP2RC6VQmgNSWZ';
            //$accessToken = 'dcc0a07e43537bf936952e2a2d048749b0a8be5f3eaebfb71e4e92ae88f2a4f4';

            // Wallet Id for Bitgo Account (payment@searchtrade.com) Bitgo Live Account
            //$walletId = '3NKSG9WeRLUruVsGJiHz25dKVFfdfpim4q';
            // Wallet access token for live account (payment@searchtrade.com)
            //$accessToken = 'e8f8f3402d75761ba09f322eb8089a182fc53fbcacbe81057432a71d40cef33e';
            $chain = 0;

            // Generate new address from bitgo
            $btcAddress = generateNewBtcAddress($userId);
            /*printArr($btcAddress);*/
            $paymentBtcAddress = $btcAddress['errMsg']['address'];
            if(noError($btcAddress)){

                $initialize = initialisePurchase($paymentBtcAddress,$email,$userId);
                if(noError($initialize)){

                    $getExchangeRate = getExchangeRates($upc);

                    if (noError($getExchangeRate)) {
                        $getExchangeRate = $getExchangeRate['currRate'];

                        $totalPriceInBtc = number_format((float)($finalAmountInUSD * $getExchangeRate['btc']),8);
                        $totalPriceInSGD = number_format((float)($finalAmountInUSD * $getExchangeRate['sgd']),2);

                        $query = "UPDATE payments SET payment_address='" . $paymentBtcAddress . "',amount_due_in_usd = '".$finalAmountInUSD."',amount_due_in_sgd = '".$totalPriceInSGD."',amount_due = '".$totalPriceInBtc."' WHERE order_id='" . $orderId . "' AND username = '" . $email . "'";
                        $result = runQuery($query, $kwdConn);

                        if (noError($result)) {
                            $returnArr["errCode"] = -1;
                            $returnArr["errMsg"]      = "payment address updated successfully";
                            $returnArr["keywords"] = $keywordBasketwithTags;
                            $returnArr["paymentAddress"] = $paymentBtcAddress;
                            $returnArr["amountInBTC"] = $totalPriceInBtc;
                            $returnArr["amountInUSD"] = $totalPriceInUSD;

                        } else {
                            $returnArr["errCode"] = 5;
                            $returnArr["errMsg"]     = "Error update payment address";

                        }

                    }else{
                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"]  = "Error: Fetching current exchange rate.";
                    }

                }else{
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"]      = "Error generating new btc address";
                }
            } else{
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"]      = "Error generating new btc address";
            }
        }else {

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"]      = "Mandatory field not found";
        }
    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"]  = "Error establishing database connection";
    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"]  = "Unauthorized token: forbidden request";
}


echo json_encode($returnArr);

?>