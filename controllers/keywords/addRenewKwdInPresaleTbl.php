<?php
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/controllers', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");

$returnArr = array();

$email = $_SESSION["email"];
$keyword = cleanXSS(urldecode($_POST["keyword"]));

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];

    $getPresaleDetails = getUserCartDetails($email, $kwdDbConn);

    if(noError($getPresaleDetails)){
        $getPresaleDetails = $getPresaleDetails["errMsg"];
        $getRenewKwd = $getPresaleDetails["user_renew_kwd_in_cart"];

        if(empty($getRenewKwd) && !isset($getRenewKwd)){
            // 1. if user_renew_kwd_in_cart is empty then insert keyword into presale user.
            $renewArr = array($keyword);
            $renewArr = json_encode($renewArr);
        }else{
            // retrieve the user_renew_kwd_in_cart values.
            // decode the values
            // check keyword exist in in_array
            // if exist do nothing
            // else update the user_renew_kwd_in_cart value in presale user table.

            $renewArr = json_decode($getRenewKwd, true);
            if(!in_array($keyword, $renewArr)){
                $renewArr[] = $keyword;
            }
            $renewArr = json_encode($renewArr);
        }
        $updateRenewKwd = updateRenewKwdInPresale($email, $renewArr, $kwdDbConn);
        if(noError($updateRenewKwd)){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = "success";
        }else{
            $returnArr["errCode"] = $updateRenewKwd["errCode"];
            $returnArr["errMsg"] = $updateRenewKwd["errMsg"];
        }
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Error: Fetching renew keyword details";
    }
}else{
    $returnArr["errCode"] = 1;
    $returnArr["errMsg"] = "Error: Database connection";
}

echo json_encode($returnArr);


?>