<?php

	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');

	session_start();

	require_once('../../config/config.php');
	require_once('../../config/db_config.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../helpers/transactionHelper.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/deviceHelper.php');
	require_once('../../helpers/arrayHelper.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../models/keywords/userCartModel.php');
	require_once('../../models/keywords/keywordSearchModel.php');
    require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

	error_reporting(0);

	$email           	= 	$_SESSION["email"];
	$keyword         	= 	$_POST["keyword"];
	$appStatus       	= 	urldecode($_POST["app_status"]);
	$clientSessionId 	= 	urldecode($_POST["clientSessionId"]);
	$cartAction 		=	urldecode($_POST["action"]);

	if($keyword != "+"){
	  	$keyword         = urldecode($keyword);
	}

    //for xml writing essential
    $xmlProcessor = new xmlProcessor();

    // inititalize xml logs activity
    $xmlArray = initializeXMLLog('');

    $xml_data['request']["data"] = '';
    $xml_data['request']["attribute"] = $xmlArray["request"];

    if($cartAction == "add"){
        $xmlfilename = "add_to_cart.xml";
        $title = "Add to cart";
    }elseif($cartAction == "remove"){
        $xmlfilename = "remove_from_cart.xml";
        $title = "Remove from cart";
    }

    $xml_data['step']["data"] = $title;

    $logStorePath = $logPath["cart"];

    $i = 0;

	//get current session Id
	$currentSessionId = session_id();

	//initialize the return array
	$returnArr = array();

	// get browser name
	$browserAgent   = getBrowserName();
	// get remote ip
	$userIp         = getClientIP();

	if($clientSessionId  == $currentSessionId){

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Validating client session : success';
		
		if(isset($email) && !empty($email) && isset($keyword) && !empty($keyword) && isset($clientSessionId) && !empty($clientSessionId)){

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Getting post parameters [1] email: '.$email.', [2] keyword: '.$keyword;

		    // create keyword database connection
			$kwdDbConn = createDBConnection("dbkeywords");

			if(noError($kwdDbConn)){
				$kwdDbConn = $kwdDbConn["connection"];

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'. Keyword database connection: success';


                // check email existence
				$checkEmailExistence = checkEmailExistance($email);
				if(noError($checkEmailExistence)){

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. Checking email existence: success';

					if($cartAction == "add"){

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. Start add to cart process';

						// add keyword in cart
						$addkwdInCart = addKeywordToCart($keyword, $email, $kwdDbConn, $i, $xml_data);

						if(noError($addkwdInCart)){
							$errMsg = $addkwdInCart["errMsg"];
							$errCode = $addkwdInCart["errCode"];
							$extraArgs["cart_total_count"] = $addkwdInCart["cartCount"];
							$extraArgs["cart_button_text"] = "Remove";

							$i = $addkwdInCart["step_number"];
							$xml_data = $addkwdInCart["xml_data"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

							$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
						}else{
							$errMsg = $addkwdInCart["errMsg"];
							$errCode = $addkwdInCart["errCode"];
                            $extraArgs["cart_total_count"] = $addkwdInCart["cartCount"];

                            $i = $addkwdInCart["step_number"];
                            $xml_data = $addkwdInCart["xml_data"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

							$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
						}
					}else{
                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'. Start remove from cart process';

						// remove keyword from cart
						$removeKwdFromCart = removeKeywordFromCart($keyword, $email, $kwdDbConn, $i, $xml_data);

						if(noError($removeKwdFromCart)){
							$errMsg = $removeKwdFromCart["errMsg"];
							$errCode = $removeKwdFromCart["errCode"];
							$extraArgs["cart_total_count"] = $removeKwdFromCart["cartCount"];
							$extraArgs["cart_button_text"] = "Add To Cart";

                            $i = $removeKwdFromCart["step_number"];
                            $xml_data = $removeKwdFromCart["xml_data"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

							$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
						}else{
							$errMsg = $removeKwdFromCart["errMsg"];
							$errCode = $removeKwdFromCart["errCode"];
                            $extraArgs["cart_total_count"] = $removeKwdFromCart["cartCount"];

                            $i = $removeKwdFromCart["step_number"];
                            $xml_data = $removeKwdFromCart["xml_data"];

                            /******************** for logs user analytics ****************/
                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;
                            /******************** for logs user analytics ****************/

							$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
						}
					}
				}else{

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'. Checking email existence: failed';

					$errMsg = "Email Id not exist";
       				$returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
				}

			}else{

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'. Keyword database connection: failed';

				$errMsg = "Unable to create database connection";
       			$returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
			}

		}else{
            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'. Mandatory field not found';

			$errMsg = "Mandatory field not found";
       		$returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
		}

	}else{

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'. Validating client session : failed';

		$errMsg = "Unauthorized token: forbidden request";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
	}

    // create or update xml log Files
    $xmlProcessor->writeXML($xmlfilename, $logStorePath, $xml_data, $xmlArray["activity"]);

	echo json_encode($returnArr);

?>