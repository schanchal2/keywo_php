<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/stringHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/transactionHelper.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/walletHelper.php');
require_once('../../models/header/headerModel.php');
require_once('../../models/keywords/userCartModel.php');
require_once('../../models/keywords/keywordPurchaseModel.php');
require_once('../../models/keywords/acceptBidModel.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

$email = $_SESSION["email"];
$currentSession = session_id();
$returnArr = array();

$keyword = cleanXSS(urldecode($_POST["keyword"]));
$clientSessionIdFrmRenewPg = cleanXSS(urldecode($_POST["clientSessionId"]));
$termAndCondition = cleanXSS(urldecode($_POST["termNcondition"]));

if(strpos($keyword, ',') !== false ){
    $keyword = trim($keyword);

    $keywordArr = explode(',', $keyword);
}else{
    $keywordArr = array(0 => $keyword);
}


// create an object
$xmlProcessor = new xmlProcessor();

$xmlfilename = "keyword_renewal.xml";
/* XML title attribute */
$xml_atrr = array();

$xml_data['request']["data"]      = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xml_data['step']["data"] = 'Keyword Renewal';
$xmlArray    = initializeXMLLog(trim(urldecode($email)));

$returnURl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

// initialize log step number
$i = 0;
$j = 0;
$m = 0;

// get client ip
$userIp = getClientIP();

if(!isset($_SESSION["verify_status"])){

    $renewAmount = 0;

    $unique_id = md5(uniqid(rand(), true));
    $xmlArray['activity']['id'] = $unique_id;
    $_SESSION['activity_id'] = $unique_id;

    $_SESSION['redirect_url'] = $returnURl;
    $msg = "Start {$xmlHeader} process.";
    $xml_data['step'.$m]["data"] = $m.". {$msg}";

    $checkEmailExistence = checkEmailExistance($email);
    if(noError($checkEmailExistence)){

        $kwdDbConn = createDBConnection("dbkeywords");
        if(noError($kwdDbConn)){
            $kwdDbConn = $kwdDbConn["connection"];

            // get admin setting details
            $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
            if(noError($getAdminSetting)){
                $getAdminSetting = $getAdminSetting["data"];
                // get renewal amount set by admin in admin_setting table.
                $kwdRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

                foreach($keywordArr as $key => $renewKwd){
                    $totalRenwwalAmount = $totalRenwwalAmount + $renewAmount;
                }

                $totalRenwwalAmount = number_format($totalRenwwalAmount, 8);

                // calculate user availablw balance
                $renewUsrBalanceField = $userRequiredFields . "_id,last_name,deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                $renewUsrBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $renewUsrBalanceField);
                if(noError($renewUsrBalanceDetails)){

                    $getUserBalance = calculateUserBalance($renewUsrBalanceDetails);
                    if(noError($getUserBalance)){

                        $userBalance = $getUserBalance['errMsg']['total_available_balance'];
                        if($userBalance >= $kwdRenewalAmt){

                            $errMsg = "Success : User balance is greater  then the total renew price";
                            $xml_data['step'.++$i]["data"] = $i.". Success :  User balance is greater  then the total renew price";
                            $returnArr["errCode"] = -2;
                            $returnArr["errMsg"] = $errMsg;

                            //redirecting to 2FA page
                            $msg = "Success : Redirecting to 2FA page";
                            $xml_data['step'.++$i]["data"] = $i.". {$msg}";
                            $returnArr["errCode"] = -2;
                            $_SESSION['form_data'] = json_encode($_POST);
                            $_SESSION['xml_step'] = $i;

                            $_SESSION['xml_file'] = $logPath["keywordPurchase"] . date("Y") . "/" . date("m") . "/" . date("d") . "/" . date("H") . "_OClock_" . $xmlfilename;

                        }else{
                            $errMsg = "You have insufficient balance to renew this keywords.";
                            $xml_data['step'.++$m]["data"] = $m.". Failed : Calculating user available balance";
                            $returnArr["errCode"] = 6;
                            $returnArr["errMsg"] = $errMsg;
                        }

                    }else{
                        $errMsg = "Error: Calculating user available balance";
                        $xml_data['step'.++$m]["data"] = $m.". Failed : Calculating user available balance";
                        $returnArr["errCode"] = 5;
                        $returnArr["errMsg"] = $errMsg;
                    }
                }else{
                    $errMsg = "Error: Getting user info";
                    $xml_data['step'.++$m]["data"] = $m.". Failed : Getting user info";
                    $returnArr["errCode"] = 4;
                    $returnArr["errMsg"] = $errMsg;
                }
            }else{
                $errMsg = "Error: Getting setting details";
                $xml_data['step'.++$m]["data"] = $m.". Failed : Getting admin setting details";
                $returnArr["errCode"] = 3;
                $returnArr["errMsg"] = $errMsg;
            }
        }else{
            $errMsg = "Error: Database connection";
            $xml_data['step'.++$m]["data"] = $m.". Failed : Database connection error";
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = $errMsg;
        }
    }else{
        $errMsg = "Not a valid user";
        $xml_data['step'.++$i]["data"] = $m.". Failed : Not a valid user";
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $errMsg;
    }

    // create or update xml log Files
    $xmlProcessor->writeXML($xmlfilename, $logPath["keywordPurchase"], $xml_data, $xmlArray["activity"]);

    echo json_encode($returnArr); die;

}else if($returnURl == $_SESSION['redirect_url']){

    $i = $_SESSION["xml_step"];
    $security_type = $_SESSION['security_type'];
    $form_data = json_decode($_SESSION['form_data'], true);

    $activity_id = $_SESSION['activity_id'];
    $xml_file = $_SESSION['xml_file'];

    $keyword = cleanXSS(urldecode($form_data["keyword"]));
    $clientSessionIdFrmRenewPg = cleanXSS(urldecode($form_data["clientSessionId"]));
    $termAndCondition = cleanXSS(urldecode($form_data["termNcondition"]));

    if(strpos($keyword, ',') !== false ){
        $keyword = trim($keyword);

        $keywordArr = explode(',', $keyword);
    }else{
        $keywordArr = array(0 => $keyword);
    }

    $renewedKeyword = $keyword;

    unset($_SESSION['security_type']);
    unset($_SESSION['xml_step']);
    unset($_SESSION['form_data']);
    unset($_SESSION['activity_id']);
    unset($_SESSION['redirect_url']);
    unset($_SESSION['xml_file']);
    unset($_SESSION['verify_status']);

    $i = $i + 1;
    $xml_data["step".$i]["data"] = $i.'.Success: Validate client session';

    if($termAndCondition == "true"){

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Success: Accept term and condition';

        $searchDbConn = createDBConnection("dbsearch");
        if(noError($searchDbConn)){
            $searchDbConn = $searchDbConn["connection"];

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Success: Create search database connection';

            $currentExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);

            if(noError($currentExchangeRate)){
                $currentExchangeRate = $currentExchangeRate["exchange_rate"];
                // convert array into json encode
                $currentExchangeRate = json_encode($currentExchangeRate);

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'.Success: Getting current exchange rate 1] '.$currentExchangeRate;

                mysqli_close($searchDbConn);

                // create keyword database connection
                $kwdDbConn = createDBConnection("dbkeywords");
                if(noError($kwdDbConn)){
                    $kwdDbConn = $kwdDbConn["connection"];

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'.Success: Create keyword database connection';

                    // get admin setting details
                    $getAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                    if(noError($getAdminSetting)){

                        $getAdminSetting = $getAdminSetting["data"];
                        // get renewal amount set by admin in admin_setting table.
                        $kwdRenewalAmt = number_format($getAdminSetting["kwd_renewal_fees_per_year"], 8);

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'.Success: Getting admin setting details to fetch renewal amount 1] Renewal Amount: '.$kwdRenewalAmt;

                        foreach($keywordArr as $key => $keyword) {

                            $keyword = trim($keyword);
                            // remove # from word
                            $keyword = substr_replace($keyword, '', 0, 1);

                            echo "keyword ".$keyword;

                            // calculate user availablw balance
                            $renewUsrBalanceField = $userRequiredFields . "_id,last_name,deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,social_affiliate_earnings";
                            $renewUsrBalanceDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $renewUsrBalanceField);
                            if(noError($renewUsrBalanceDetails)){

                                $userId = $renewUsrBalanceDetails["errMsg"]["_id"];
                                $firstName = $renewUsrBalanceDetails["errMsg"]["first_name"];
                                $lastName = $renewUsrBalanceDetails["errMsg"]["last_name"];

                                $i = $i + 1;
                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting user info of '.$email.' user.';


                                $getUserBalance = calculateUserBalance($renewUsrBalanceDetails);


                                if (noError($getUserBalance)) {
                                    $userBalance = $getUserBalance['errMsg']['total_available_balance'];

                                    //$i = $i + 1;
                                    $j = $j + 1;
                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting '.$email.' available balance 1] Available balance: '.$userBalance;

                                    $getKwdOwnershipDetails = getKeywordOwnershipDetails($kwdDbConn, $keyword);

                                    if(noError($getKwdOwnershipDetails)){

                                        //$i = $i + 1;
                                        $j = $j + 1;
                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting '.$keyword.' ownership details';

                                        $getKwdOwnershipDetails = $getKwdOwnershipDetails["errMsg"];
                                        $ownerId = $getKwdOwnershipDetails["buyer_id"];
                                        $expiryTimestamp = $getKwdOwnershipDetails["ownership_expiry_time"];
                                        $purchaseTimestamp = $getKwdOwnershipDetails["purchase_timestamp"];

                                        $expiredOn = strtotime($expiryTimestamp);
                                        $expiredOn = date("j F, Y", $expiredOn);

                                        $purchasedOn = strtotime($purchaseTimestamp);
                                        $purchasedOn = date("j F, Y", $purchasedOn);

                                        if(isset($ownerId) && !empty($ownerId)){
                                            // validate session user and keyword owner
                                            if($email == $ownerId){

                                                // $i = $i + 1;
                                                $j = $j + 1;
                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Validating keyword owner Id';

                                                // 1. add renewal amount in renewal field of  keyword buyer account.
                                                // 2. add renewal amount in renewal field of communitypool@keywo.com user.
                                                // 3. insert transaction for renewal earning where sender = $email and receiver = $communitypool@keywo.com with type="renewal_fees"
                                                // 4. update expiry timestamp in keyword ownership table
                                                // 5. Send email and notification to current session user.

                                                if($userBalance >= $kwdRenewalAmt){

                                                    //$i = $i + 1;
                                                    $j = $j + 1;
                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: User available balance is greater then the renewal amount';

                                                    // step 1. add renewal amount in renewal field of session user account.
                                                    $creditRenewAmountToKwdOwner = creditUserEarning($userId, $kwdRenewalAmt, "renewalfees", "add");
                                                    if(noError($creditRenewAmountToKwdOwner)){

                                                        $j = $j + 1;
                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Credit renewal amount to '.$email.' account and increase the expenditure amount.';

                                                        // get community pool user details.
                                                        $getCommunityPoolReqField = $userRequiredFields."_id,last_name";
                                                        $getCommunityPoolDetails = getUserInfo($communityPoolUser,  $walletURLIP . 'api/v3/', $getCommunityPoolReqField);
                                                        if(noError($getCommunityPoolDetails)){

                                                            $j = $j + 1;
                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting '.$communityPoolUser.' user details.';

                                                            $getCommunityPoolDetails = $getCommunityPoolDetails["errMsg"];
                                                            $communityPoolUserId = $getCommunityPoolDetails["_id"];

                                                            // 2. add renewal amount in renewal field of communitypool@keywo.com user.
                                                            $creditRenewAmtToCommunityPool = creditUserEarning($communityPoolUserId, $kwdRenewalAmt, "renewalfees", "add");
                                                            if(noError($creditRenewAmtToCommunityPool)){

                                                                $j = $j + 1;
                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Credit renewal amount to '.$communityPoolUser.' account to increase the income.';

                                                                // 3. insert transaction for renewal earning where sender = $email and receiver = $communitypool@keywo.com with type="renewal_fees"

                                                                // parameters required to insert in transaction
                                                                $recipientEmail     = $communityPoolUser;
                                                                $recipientUserId    = $communityPoolUserId;
                                                                $senderEmail        = $email;
                                                                $senderUserId       = $userId;
                                                                $amount             = $kwdRenewalAmt;
                                                                $type               = "renewal_fees";
                                                                $paymentMode        = 'ITD';
                                                                $originIp           = $userIp;
                                                                $exchangeRateInJson = $currentExchangeRate;

                                                                $metaDetails = array(
                                                                    "sender" => $email,
                                                                    "receiver" => $communityPoolUser,
                                                                    "renewal_amount" => $kwdRenewalAmt,
                                                                    "keyword" => urlencode(utf8_encode($keyword)),
                                                                    "discount" => 0,
                                                                    "commision" => 0,
                                                                    "desc" => "User ".$email." has renewed his/her #".$keyword." keyword for the session ".$purchaseTimestamp." - ".$expiryTimestamp." and credited renewal amount with ".number_format($kwdRenewalAmt, 2)." ".$keywoDefaultCurrencyName." to ".$communityPoolUser." on dated ".date("j F, Y")
                                                                );

                                                                $insertRenewalTransaction = insertUserTransaction($recipientEmail, $recipientUserId, $senderEmail, $senderUserId, $amount, $type, $paymentMode, $originIp, $exchangeRateInJson, $metaDetails);
                                                                if(noError($insertRenewalTransaction)){

                                                                    $j = $j + 1;
                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Insert transaction for renewal amount where 1] Sender: '.$email.' 2], Receiver: '.$communityPoolUser.', 3] Renewal Amount: '.$kwdRenewalAmt.' '.$keywoDefaultCurrencyName.', 4] Keyword: '.$keyword.'.';

                                                                    // 4. update expiry timestamp in keyword ownership table
                                                                    $updateExpiryTimestamp = updateExpiryTimestamp($keyword, $kwdDbConn);
                                                                    if(noError($updateExpiryTimestamp)){

                                                                        $j = $j + 1;
                                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Update keyword : #'.$keyword.' expiry timestamp in ownership table';

                                                                        // 5. Send email and notification to current session user.

                                                                        $to      = $email;
                                                                        $subject = "Keywo: Keyword Renew Confirmation";
                                                                        $message = '<p>Hi, <br/> <br/>You have successfully renewed the license for <b>#' . $keyword . '</b> keyword via Keywo Wallet.</p>';

                                                                        $notification_message = 'You have successfully renewed the license for <b>#' . $keyword . '</b> keyword via Keywo Wallet.';
                                                                        $method_name          = "GET";

                                                                        $getNotification = notifyoptions($userId, $optionid, $method_name, $buy_container, $ask_container, $bid_container, $withdrawal_container, $deposit_container, $kwd_license_container, $buy_perm_code, $ask_perm_code, $bid_perm_code, $kwd_license_perm_code, $deposit_perm_code, $withdrawal_perm_code);
                                                                        if(noError($getNotification)){
                                                                            $permissionCode  = $getNotification["errMsg"]["notify_options_fk_key"]["kwd_license_opt_container"]["1"]["permissions"]["_id"];
                                                                            $path    = "";
                                                                            $category   = "buy";
                                                                            $linkStatus = 1;

                                                                            $j = $j + 1;
                                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Getting user notification preference for sending email and notification.';

                                                                            $sendRenewEmailAndNotification   = sendNotificationBuyPrefrence($to, $subject, $message, $firstName, $lastName, $userId, $smsText, $mobileNumber, $notification_message, $permissionCode, $category, $linkStatus);
                                                                            if(noError($sendRenewEmailAndNotification)){
                                                                                $msg = "You have successfully renew the licence for #".$keyword." keyword";
                                                                                $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                                                                                $j = $j + 1;
                                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Sent email and notification to '.$email.' user.';

                                                                                $removeRenewKwdFrmPresale = removeRenewKwdFrmPresale($email, $keyword, $kwdDbConn);
                                                                                if(noError($removeRenewKwdFrmPresale)){
                                                                                    $msg = $removeRenewKwdFrmPresale["errMsg"];
                                                                                    $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);

                                                                                    $j = $j + 1;
                                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Success: Removing renew keyword from presale user table';


                                                                                }else{
                                                                                    $msg = $removeRenewKwdFrmPresale["errMsg"];
                                                                                    $returnArr = setErrorStack($returnArr, 19, $msg, $extraArgs);

                                                                                    $j = $j + 1;
                                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Removing renew keyword from presale user table';
                                                                                }
                                                                            }else{
                                                                                $msg = $sendRenewEmailAndNotification["errMsg"];
                                                                                $returnArr = setErrorStack($returnArr, 18, $msg, $extraArgs);

                                                                                $j = $j + 1;
                                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Sent email and notification to '.$email.' user.';
                                                                            }
                                                                        }else{
                                                                            $msg = $getNotification["errMsg"];
                                                                            $returnArr = setErrorStack($returnArr, 17, $msg, $extraArgs);

                                                                            $j = $j + 1;
                                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting user notification preference for sending email and notification.';
                                                                        }
                                                                    }else{
                                                                        $msg = $updateExpiryTimestamp["errMsg"];
                                                                        $returnArr = setErrorStack($returnArr, 16, $msg, $extraArgs);

                                                                        $j = $j + 1;
                                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Update keyword : #'.$keyword.' expiry timestamp in ownership table';
                                                                    }
                                                                }else{
                                                                    $msg = $insertRenewalTransaction["errMsg"];
                                                                    $returnArr = setErrorStack($returnArr, 15, $msg, $extraArgs);

                                                                    $j = $j + 1;
                                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Insert transaction for renewal amount where 1] Sender: '.$email.' 2], Receiver: '.$communityPoolUser.', 3] Renewal Amount: '.$kwdRenewalAmt.' '.$keywoDefaultCurrencyName.', 4] Keyword: '.$keyword.'.';
                                                                }
                                                            }else{
                                                                $msg = "Error: Credit renew amount to pool user";
                                                                $returnArr = setErrorStack($returnArr, 14, $msg, $extraArgs);

                                                                $j = $j + 1;
                                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Credit renewal amount to '.$communityPoolUser.' account to increase the income.';
                                                            }
                                                        }else{
                                                            $msg = "Error: Getting pool details";
                                                            $returnArr = setErrorStack($returnArr, 13, $msg, $extraArgs);

                                                            $j = $j + 1;
                                                            $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting '.$communityPoolUser.' user details.';
                                                        }

                                                    }else{
                                                        $msg = "Error: Credit renewal fees amount to keyword owner account";
                                                        $returnArr = setErrorStack($returnArr, 12, $msg, $extraArgs);

                                                        $j = $j + 1;
                                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Credit renewal amount to '.$email.' account and increase the renewal_fees amount.';
                                                    }
                                                }else{
                                                    $msg = "You don't have sufficient balance to renew keyword";
                                                    $returnArr = setErrorStack($returnArr, 10, $msg, $extraArgs);

                                                    $j = $j + 1;
                                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Insufficient balance i.e. available balance is less then the renewal amount';

                                                }
                                            } else{        // end $email == $ownerId

                                                $msg = "This is not your owned keyword. Please renew your owned keyword";
                                                $returnArr = setErrorStack($returnArr, 109, $msg, $extraArgs);

                                                $j = $j + 1;
                                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: '.$keyword.' is belongs to '.$ownerId;

                                            }
                                        } //end  if(isset($ownerId) && !empty($ownerId))
                                    }else{
                                        $msg = "Error: Fetching ownership details";
                                        $returnArr = setErrorStack($returnArr, 9, $msg, $extraArgs);

                                        $j = $j + 1;
                                        $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting '.$keyword.' ownership details';
                                    }
                                } else {
                                    $msg = "Error: Fetching user details";
                                    $returnArr = setErrorStack($returnArr, 8, $msg, $extraArgs);

                                    $j = $j + 1;
                                    $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting '.$email.' available balance 1] Available balance: '.$userBalance;
                                }
                            }else{
                                $msg = "Error: Fetching admin setting";
                                $returnArr = setErrorStack($returnArr, 7, $msg, $extraArgs);

                                $j = $j + 1;
                                $xml_data["step".$i.".".$j]["data"] = $i.".".$j.'.Failed: Getting user info of '.$email.' user.';
                            }

                            $j = 0;

                        } // end of foreach loop
                    }else{
                        $msg = "Error: Fetching admin setting";
                        $returnArr = setErrorStack($returnArr, 6, $msg, $extraArgs);

                        $i = $i + 1;
                        $xml_data["step".$i]["data"] = $i.'.Failed: Getting admin setting details to fetch renewal amount 1] Renewal Amount: '.$kwdRenewalAmt;
                    }
                }else{
                    $msg = "Error: Unabel to create connection with database";
                    $returnArr = setErrorStack($returnArr, 5, $msg, $extraArgs);

                    $i = $i + 1;
                    $xml_data["step".$i]["data"] = $i.'.Failed: Create keyword database connection';
                }
            }else{
                $msg = "Error: Unable to get exchage rate";
                $returnArr = setErrorStack($returnArr, 4, $msg, $extraArgs);

                $i = $i + 1;
                $xml_data["step".$i]["data"] = $i.'.Failed: Getting current exchange rate 1] '.$currentExchangeRate;
            }
        }else{
            $msg = "Error: Database connection";
            $returnArr = setErrorStack($returnArr, 3, $msg, $extraArgs);

            $i = $i + 1;
            $xml_data["step".$i]["data"] = $i.'.Failed: Create database connection';
        }
    }else{
        $msg = "Denied term and condition";
        $returnArr = setErrorStack($returnArr, 2, $msg, $extraArgs);

        $i = $i + 1;
        $xml_data["step".$i]["data"] = $i.'.Failed: Accept term and condition.';
    }

    $xmlProcessor->updateXML($xml_file, $xml_data, $xmlArray["activity"],$activity_id);

   // header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');
    if($returnArr["errCode"]  == -1){
        $returnArr["errCode"]  = -1;

        $_SESSION["accept_bid_error_code"] = -1;
        $_SESSION["accept_bid_error_message"] = "You have successfully renewed ".$renewedKeyword." keywords.";

        header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

    }else if($returnArr["errCode"] == 109){

        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr['errMsg'] = $returnArr['errMsg'];

        $_SESSION["get_error_code"] = $returnArr["errCode"];
        $_SESSION["get_error_message"] = $returnArr['errMsg'];

        header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

    }else{
        $returnArr["errCode"] = $returnArr['errCode'];
        $returnArr['errMsg'] = $returnArr['errMsg'];

        $_SESSION["get_error_code"] = $returnArr["errCode"];
        $_SESSION["get_error_message"] = $returnArr['errMsg'];

        header('Location:'.$rootUrl.'views/keywords/cart/renew_checkout.php');
    }


}else{
    unset($_SESSION['security_type']);
    unset($_SESSION['xml_step']);
    unset($_SESSION['form_data']);
    unset($_SESSION['activity_id']);
    unset($_SESSION['redirect_url']);
    unset($_SESSION['xml_file']);
    unset($_SESSION['verify_status']);

    $_SESSION['err_msg'] = "Oops! Something went wrong,Please try again.";
    $returnArr["errCode"] = 2;

  //  $xmlProcessor-> writeXML($xmlfilename, $logPath["keywordPurchase"], $xml_data, $xmlArray["activity"]);

    header('Location:'.$rootUrl.'views/keywords/cart/renew_checkout.php');
}


if($returnArr["errCode"]  == -1){
    $returnArr["errCode"]  = -1;

    $_SESSION["accept_bid_error_code"] = -1;
    $_SESSION["accept_bid_error_message"] = "You have successfully renewed ".$renewedKeyword." keywords.";

    header('Location:'.$rootUrl.'views/keywords/user_dashboard/owned_keyword.php');

}else{
    $returnArr["errCode"] = $returnArr['errCode'];
    $returnArr['errMsg'] = $returnArr['errMsg'];

    $_SESSION["get_error_code"] = $returnArr["errCode"];
    $_SESSION["get_error_message"] = $returnArr['errMsg'];

    header('Location:'.$rootUrl.'views/keywords/cart/renew_checkout.php');
}

?>