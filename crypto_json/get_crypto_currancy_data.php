<?php

$jsonMainDataUrl = 'currencyData.json';  //echo $jsonMainDataUrl;
$json_data = json_decode(file_get_contents($jsonMainDataUrl),true);

$currancyType = explode(' (', $_POST['searchCurrancy']);
$dataSymbol = explode(')',$currancyType[1]);


$currancyName = $currancyType[0];
$currancySymbol = $dataSymbol[0];

unset($id,$name,$symbol,$rank,$price_usd,$one_day_volume_usd,$market_cap_usd,$available_supply,$percent_change_1h,$percent_change_24h,$percent_change_7d);

for($i = 0; $i <= count($json_data); $i++){
	
	$name = $json_data[$i]['name'];
	$symbol = $json_data[$i]['symbol'];

	if( ($name == $currancyName) || ($symbol == $currancySymbol) ){
		$id = $json_data[$i]['id'];
		$rank = $json_data[$i]['rank'];

		$num_price_usd = number_format($json_data[$i]['price_usd'],2);
		$num_24h_volume_usd = number_format($json_data[$i]['24h_volume_usd'],2);
		$num_market_cap_usd = number_format($json_data[$i]['market_cap_usd'],2);

		$price_usd = empty($num_price_usd) ? 0 : $num_price_usd;
		$one_day_volume_usd = empty($num_24h_volume_usd) ? 0 : $num_24h_volume_usd;
		$market_cap_usd = empty($num_market_cap_usd) ? 0 : $num_market_cap_usd;
		$available_supply = empty($json_data[$i]['available_supply']) ? 0 : number_format($json_data[$i]['available_supply']);
		$percent_change_1h = empty($json_data[$i]['percent_change_1h']) ? 0 : $json_data[$i]['percent_change_1h'];
		$percent_change_24h = empty($json_data[$i]['percent_change_24h']) ? 0 : $json_data[$i]['percent_change_24h'];
		$percent_change_7d = empty($json_data[$i]['percent_change_7d']) ? 0 : $json_data[$i]['percent_change_7d'];
		break;
	}

}

$arrData = array('id' => $id, 'name' => $name, 'symbol' => $symbol, 'rank' => $rank, 'price_usd' => $price_usd, 'one_day_volume_usd' => $one_day_volume_usd, 'market_cap_usd' => $market_cap_usd, 'available_supply' => $available_supply, 'percent_change_1h' => $percent_change_1h, 'percent_change_24h' => $percent_change_24h, 'percent_change_7d' => $percent_change_7d);
//print_r($arrData); 

echo json_encode($arrData, true);
?>
