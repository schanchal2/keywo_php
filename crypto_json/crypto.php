<?php

require_once '/stweb/protected/controller/utilities.php';	
require_once('/stweb/protected/controller/config.php');

//To send mail if any error occurs.
function sendNewsMail($message){		
	$to = "searchtradedeveloper@gmail.com";	
	$subject = "Crypto - Error in file updating";		
	$result = Send_Mail($to,'',$subject,$message);
}

$month = date("MY"); 		
$logDir = "/home/searchc9/public_html/crypto_json/log/".$month;
if(!is_dir($logDir)){
	mkdir($logDir, 0777, true);
}
$logMsg = $logData = "";	
$file = fopen("".$logDir."/".date("d")." log.txt","a");				
$time1 = date("Y-m-d H:i:s");		
$logData .="\n*********************************************************************************************\n";		
$logData .="******************************CRON RUN TIME:".$time1."******************************\n";		

$url = "https://api.coinmarketcap.com/v1/ticker/";
$jsonMainDataUrl = "/home/searchc9/public_html/crypto_json/currencyData.json"; 
$json_data = json_decode(file_get_contents($url),true);
$fp = fopen($jsonMainDataUrl, 'w');
fwrite($fp, json_encode($json_data));
fclose($fp);
chmod( $jsonMainDataUrl , 0777 );
if(isset($fp)){
	$logMsg = "File updated successfully.";
}else{
	$logMsg = "Error in File updating.";
	sendNewsMail($logMsg);
}

$logData .= $logMsg;
$logData .= "\n*********************************************************************************************\n";		
$logData .= "**************************************** *** END **** ***************************************\n\n";	
fwrite($file,$logData);		
fclose($file);
?>
