<?php

//global array to define all error messages
$errorArray = array(
    "-1" => "success",
    "1" => "Error in creating database connection.",
    "2" => "Error in selecting database.",
    "3" => "Error in mysql query.",
    "4" => "Error in starting Mysql Transaction.",
    "5" => "Error in commit Mysql Transaction.",
    "6" => "Error in rollback Mysql Transaction.",
    "7" => "Error in Mysql Transaction (Transaction Failed.)",
    "8" => "Admin Login failed.Invalid Password.",
    "9" => "User Password updation failed.",
    "10" => "Error in adding IP address.",
    "11" => "Error getting IP address.",
    "12" => "Error getting list of IP address.",
    "13" => "Error in deleting IP Address.",
    "14" => "Error in Updating Profile Picture.",
    "15" => "Error Failed to Add New User.",
    "16" => "Error in getting Admin User List.",
    "17" => "Error in getting Admin User Data.",
    "18" => "Error in getting All Modules List.",
    "19" => "Error in Adding Group Data.",
    "20" => "Error in Deleting Admin User.",
    "21" => "Error in Deleting Group.",
    "22" => "Error in Editing Group.",
    "23" => "Error getting Admin User Data By Group.",
    "24" => "Error getting Group Name By Group ID.",
    "25" => "Error getting Group ID By Group Name.",
    "26" => "Error Updating User Password.",
    "27" => "Error Updating User Data.",
    "28" => "Error Updating User Rights.",
    "29" => "Error getting Page data.",
    "30" => "Error getting All Roles Data.",
    "31" => "IP address is Required.",
    "32" => "Please add Valid IP address.",
    "33" => "New IP address added allrady present in database.",
    "34" => "Admin User Group Name is Required .",
    "35" => "This Group Allready Added .",
    "36" => "Admin Group Permissions are required .",
    "37" => "Please Select Module on you want to add this group .",
    "38" => "Please Select at list one submodule .",
    "39" => "Please Select Sub-Module on you want to add this group .",
    "40" => "Old password is mandatory .",
    "41" => "Error in curl execution .",
    "42" => "Post field is missing or empty .",
    "43" => "Api text is missing or empty .",
    "44" => "Api name is missing or empty .",
    "45" => "Parameters are blank .",
    "46" => "All fields are mandatory .",
    "47" => "Error in selecting Table row count.",
    "48" => "Error in curl initialization.",
    "49" => "Curl type field is missing or empty.",
    "A01" => "Error while Inserting Analytics activity.",
    "A02" => "Error while Updating Analytics activity.",
    "A03" => "Error while creating New keywo User activity table.",
    "A04" => "Error: keywo User activity table not exist.",
    "50" => "Error: in Updating Fees,Please try Again.",
    "51" => "Error: in Updating Channels.",
    "52" => "Error: in Getting Channels.",
    "53" => "Error: in checking channels table.",
    "59" => "Error: in Updating channels table.",
    "60" => "Error: in Selecting Signup Analytics.",
    "61" => "Error: in Selecting All Tracklist Category.",
    "62" => "Error: Data Into Subcategory.",
    "63" => "Error: Adding User In Tracklist.",
    "64" => "Error: Fetching KYC Level.",
    "65" => "Error: in Selecting Keyword Analytics.",
    "D1" => "Error: in Creating Table for Domain Analytics.",
    "D2" => "Error: in Updating Domain Data.",
    "D3" => "Error: in Inserting Domain Data.",
    "66" => "Error: in Inserting FAQ.",
    "67" => "Error: in Inserting Newsletter in Queue.Try Again.",
    "68" => "Error: in Updating Newsletter Stat.",
    "69" => "Error: in Select Keyword Users.",
    "70" => "Error: in query updation/insert.",
    "71" => "Error: duplicate query."

);

/**
 * function name: getErrorMsg();
 * parameters: $errCode(number)
 * purpose: to getting perticuler error message from error array.
 */
function getErrMsg($errCode)
{
    global $errorArray;
    $errMsg = $errorArray[$errCode];
    if (!isset($errMsg) || empty($errMsg)) {
        $errMsg = "Undefined error code and error message";
    }
    return $errMsg;
}

/**
 * function name: setErrorStack();
 * parameters: $returnArr(array),$errCode(number)
 * purpose: to getting error stack.
 */
function setErrorStack($returnArr, $errCode, $errMsg, $extraArgs){
    $resArr = array();
    if(!isset($errMsg) && empty($errMsg)){
        $errMsg = getErrMsg($errCode);
    }
    if(isset($returnArr["errStack"])) {
        $resArr["errStack"] = $returnArr["errStack"] . $errCode . ":" . $errMsg . " ";
    }
    $resArr["errCode"] = $errCode;
    $resArr["errMsg"] = $errMsg;
    if(isset($extraArgs) && !empty($extraArgs)){
        $resArr = array_merge($resArr,$extraArgs);
    }
    return $resArr;
}
function getErrorStack($resArray)
{
    $returnArr = array();
    $errStack = $resArray["errCode"];
    foreach ($errStack as $errCode) {
        echo "<br>Error code: " . $errCode . " Error msg: " . getErrMsg($errCode);
    }
}

function checkMode($error)
{
    global $mode;
    global $adminView;
    if ($mode != "development") {
        header("Location:{$adminView}");
    } else {
        print($error);
    }
}


?>