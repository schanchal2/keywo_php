<?php
function writeTextLogs($logStorePath,$filename,$data)
{
    $year = date("Y");
    $month = date("m");
    $date = date("d");

    $logDir = $logStorePath . $year . "/" . $month . "/" . $date;
    if (!is_dir($logDir)) {
        mkdir($logDir, 0777, true);
    }

    $URL = $logStorePath . $year . "/" . $month . "/" . $date . "/". $filename;

    if (!file_exists($URL)) {
        $method = (file_exists($URL)) ? 'a' : 'w';
        $myfile = fopen($URL, $method); //or die("Unable to open file!");
        fwrite($myfile, $data);
        fclose($myfile);
    }else{

        $myfile = fopen($URL, "a"); //or die("Unable to open file!");
        fwrite($myfile, $data);
        fclose($myfile);
    }
}