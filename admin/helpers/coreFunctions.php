<?php
/*
*-----------------------------------------------------------------------------------------------------------
*    CORE FUNCTIONS
*-----------------------------------------------------------------------------------------------------------
*
*   Description : This file contains all the core function required to call on all controllers and
*                 models files.
*/

/*
*-----------------------------------------------------------------------------------------------------------
*    Function curlRequest()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :    curlRequest()
*   Purpose       :    Make a curl request to given api to wallet and handle related errors
*   Arguments     :   (array) $curlParamArr($apiName, $apiText, $curlPostFields, $log)
 *                    (array) $headerArr (x-ts : value, x-cnonce : value)
*   Returns       :   Return response about curl request
*/


function curlRequest($curlParamArr, $headerArr)
{
    global $walletPublicKey, $walletPrivateKey;
    $retArray = array();

    /*
      @ other curl request function are not sending $curlParamArr['requestUrl'] parameter that's why if Else condition is written

      @ After when all other function will send $curlParamArr['requestUrl'] parameter then if else statement must delete And $apiURL = $curlParamArr['requestUrl']; must execute.

    */
    $apiURL = '';
    if (isset($curlParamArr['requestUrl']) || !empty($curlParamArr['requestUrl'])) {
        $apiURL = $curlParamArr['requestUrl'];
    } /*else {
      $apiURL = $walletURL;
    }  */

    /* Check required fields in array */
    if (count($curlParamArr) > 0) {
        /* Check required fields is empty */
        if (isset($curlParamArr['apiName']) && !empty($curlParamArr['apiName'])) {
            if (isset($apiURL) && !empty($apiURL)) {
                /* Check required fields is empty */
                if (isset($curlParamArr['apiText']) && !empty($curlParamArr['apiText'])) {
                    /* Check required fields is empty */
                    if (isset($curlParamArr['curlPostFields']) && !empty($curlParamArr['curlPostFields'])) {

                        if (isset($curlParamArr['curlType']) && !empty($curlParamArr['curlType'])) {

                            /* Initialize curl */
                            $ch = curl_init();

                            /* Check error in curl initialization */
                            if ($ch != false) {
                                /* Create signature */
                                $apiText = $curlParamArr['apiText'];
                                $signature = hash_hmac('sha512', $apiText, $walletPrivateKey);
                                /* Add signature in post fields*/
                                $postFields = "{$curlParamArr['curlPostFields']}&signature={$signature}";

                                /* Set curl options */
                                curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}");

                                if (!empty($headerArr)) {
                                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArr);
                                }
                                if ($curlParamArr['curlType'] == 'GET') {
                                    curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}?{$postFields}");
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                                } else if ($curlParamArr['curlType'] == 'POST') {
                                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                    // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $curlParamArr['curlType']);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

                                } else if ($curlParamArr['curlType'] == 'PUT') {
                                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $curlParamArr['curlType']);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

                                } else if ($curlParamArr['curlType'] == 'DELETE') {
                                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $curlParamArr['curlType']);
                                    curl_setopt($ch, CURLOPT_POST, true);
                                    curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);
                                } else if ($curlParamArr['curlType'] == 'PUTbyID') {

                                    curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}?publicKey={$walletPublicKey}&signature={$signature}");
                                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                                    curl_setopt($ch, CURLOPT_POSTFIELDS, $curlParamArr['curlPostFields']);
                                }
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                /* Execute curl */

                                $result = curl_exec($ch);

                                curl_close($ch);

                                if ($result != false) {
                                    $resultArr = json_decode($result, true);
                                    /* get curl response */
                                    $errCode = $resultArr["errCode"];
                                    $errMsg = $resultArr["errMsg"];
                                    if ($errCode == -1) {
                                        $retArray['errCode'] = -1;
                                        $retArray['errMsg'] = $errMsg;
                                    } else {
                                        $retArray['errCode'] = $errCode;
                                        $retArray['errMsg'] = $errMsg;
                                    }
                                } else {
                                    $retArray['errCode'] = 27;
                                    $retArray['errMsg'] = 'Error in curl execution';
                                }
                            } else {
                                $retArray['errCode'] = 26;
                                $retArray['errMsg'] = 'Error in curl initialization';
                            }

                        } else {
                            $retArray['errCode'] = 25;
                            $retArray['errMsg'] = 'Curl type field is missing or empty';
                        }

                    } else {
                        $retArray['errCode'] = 24;
                        $retArray['errMsg'] = 'Post field is missing or empty';
                    }
                } else {
                    $retArray['errCode'] = 23;
                    $retArray['errMsg'] = 'Api text is missing or empty';
                }
            } else {

                $retArray['errCode'] = 22;
                $retArray['errMsg'] = 'Wrong Api server';
            }

        } else {
            $retArray['errCode'] = 21;
            $retArray['errMsg'] = 'Api name is missing or empty';
        }

    } else {
        $retArray['errCode'] = 20;
        $retArray['errMsg'] = 'Parameters are blank';
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function runQuery
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   runQuery()
*   Purpose       :   To execute the sql queries.
*   Arguments     :   (string)$query, (string)$conn
*   Returns       :   Array that contains errCode and errMsg having success or failure message.
*/

function runQuery($query, $conn)
{
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_query($conn, $query);

    if (!$result) {
        // echo("Error description: " . mysqli_error($conn));
        $extraArg['query'] = $query;
        $returnArr = setErrorStack($returnArr, 3, null, $extraArg);
    } else {
        $extraArg['dbResource'] = $result;
        $errMsg = 'Query Successful';
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    }
    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function noError
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   noError()
*   Purpose       :   This function valdiate the condition, if array contains errCode = -1 then true
*                      else false.
*   Arguments     :   (array)$resArray
*   Returns       :   boolean (true or false)
*/

function noError($resArray)
{
    $noError = false;
    if ($resArray["errCode"] == -1) {
        $noError = true;
    }
    return $noError;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getEncryptedNonce()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getEncryptedNonce()
*   Purpose       :   creating cipher object using Rijndael encryption algorithm with Cipher-block chaining
*                     (CBC) as mode of AES encryption Here using 128 bit Rijndael encryption
*   Arguments     :   (string)$nonce_aes_mode_key, (string)$nonce_secret
*   Returns       :   Nonce cipher text in encrypted form.
*/

function getEncryptedNonce()
{

    global $nonce_aes_mode_key, $nonce_secret;

    $returnArr = array();
    /*
    creating cipher object using Rijndael encryption algorithm with Cipher-block chaining (CBC) as mode of AES encryption
    Here I have chosen 128 bit Rijndael encryption
    */
    $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

    /*
    for 256 bit AES encryption key size should be of 32 bytes (256 bits)
    for 128 bit AES encryption key size should be of 16 bytes (128 bits)
    here i am doing 256-bit AES encryption
    choose a strong key
    */
    $key = $nonce_aes_mode_key;

    /*
    for 128 bit Rijndael encryption, initialization vector (iv) size should be 16 bytes
    for 256 bit Rijndael encryption, initialization vector (iv) size should be 32 bytes
    here I have chosen 128 bit Rijndael encryption, so $iv size is 16 bytes
    */
    $initVector = getVectorCounter();

    if (noError($initVector)) {

        $initVector = $initVector["errMsg"]["counter"];

        $nonceSecret = $nonce_secret;
        mcrypt_generic_init($cipher, $key, $initVector);
        // PHP pads with NULL bytes if $plainText is not a multiple of the block size
        $getNonce = mcrypt_generic($cipher, $nonceSecret);
        mcrypt_generic_deinit($cipher);
        /*
        $nonceBinToHex stores encrypted text in hex
        we will be decrypting data stored in $cipherHexText256 from node js
        */
        $nonceBinToHex = bin2hex($getNonce);

        /*
        echoing $nonceBinToHex (copy the output)
        */
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $nonceBinToHex;
    } else {
        $returnArr["errCode"] = $initVector["errCode"];
        $returnArr["errMsg"] = $initVector["errMsg"];
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function generateSignatureWithNonce
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   generateSignatureWithNonce()
*   Purpose       :   Append current timestamp in signature.
*   Arguments     :   (string) (apiText)
*/


function generateSignatureWithNonce($apiTextVal)
{
    if (!empty($apiTextVal)) {
        $get_nonce = getEncryptedNonce();
        if (noError($get_nonce)) {
            $nonce_value = $get_nonce["errMsg"];
            $timestamp = round(microtime(true) * 1000);
            $api_text = "{$apiTextVal}&ts={$timestamp}";
            /*$post_fields = "{$reqParam['curlPostFields']}&nonce={$nonce_value}&timstamp={$timestamp}";*/

            $retArray["errCode"] = -1;
            $retArray["errMsg"]["apiText"] = $api_text;
            $retArray["errMsg"]["timestamp"] = $timestamp;
            $retArray["errMsg"]["cnonce"] = $nonce_value;
        } else {
            $retArray['errCode'] = $get_nonce['errCode'];
            $retArray['errMsg'] = $get_nonce["errMsg"];;
        }
    } else {
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = "Mandatory fields not found";
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getVectorCounter
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getVectorCounter()
*   Purpose       :   Generate vector counter to create encoded cipher text.
*   Arguments     :   none
*/

function getVectorCounter()
{
    global $docRoot;
    $returnArr = array();

    // nonce directory
    $nonceDir = $docRoot . "controllers/nonce";
    if (is_dir($nonceDir)) {
        // nonce json file that stores current timestamp and next request vector counter.
        $nonceCounterJson = $nonceDir . "/nonceCounter.json";

        if (file_exists($nonceCounterJson)) {
            // if json exist then read the current vector counter
            $fp = fopen($nonceCounterJson, "r");
            $readJson = fread($fp, filesize($nonceCounterJson));
            fclose($fp);
            $readJson = json_decode($readJson, true);
            $currentCounter = $readJson["vector_count"];

            $returnArr["errCode"] = -1;
            $returnArr["errMsg"]["counter"] = $currentCounter;
            $returnArr["errMsg"]["counter_json_path"] = $nonceCounterJson;
        } else {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Error: " . $nonceCounterJson . " not exist.";
        }
    } else {
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error: " . $nonceDir . " not exist.";
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function incrementVectorCounter
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   incrementVectorCounter()
*   Purpose       :   This function is used to increment vector counter by 1 and write into nonceCounter.json
*                     file.
*   Arguments     :   none
*/
function incrementVectorCounter()
{

    $returnArr = array();
    $getVectorCounter = getVectorCounter();

    if (noError($getVectorCounter)) {
        $getVectorCounter = $getVectorCounter["errMsg"];
        $currentCounter = (double)$getVectorCounter["counter"];
        $nonceCounterJsonPath = $getVectorCounter["counter_json_path"];

        if ($currentCounter < 1000000000000000) {
            $nextCounter = $currentCounter + 1;
            $nextCounter = sprintf("%'.016d\n", $nextCounter);
            $nextCounter = trim(str_replace("\n", '', $nextCounter));
        } else {
            $nextCounter = $currentCounter + 1;
        }

        // After getting the current vector counter prepare for the next vector counter and write
        // the nonce json file.

        $fp1 = fopen($nonceCounterJsonPath, "w");
        $currentTime = time();
        $arr = array("time" => $currentTime, "vector_count" => $nextCounter);
        $arr = json_encode($arr);
        fwrite($fp1, $arr);
        fclose($fp1);

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Successfully counter increases by 1";
    } else {
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = $getVectorCounter["errMsg"];
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function createVectorCounterArchieveCron()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   createVectorCounterArchieveCron()
*   Purpose       :   This is a cron file, which is used to take backup of nonceCounterJson file which
*                     contains the vector counter current timestamp and current vector count.
*   Arguments     :   none
*/

function createVectorCounterArchieveCron()
{
    $returnArr = array();
    global $docRoot;

    $nonceDir = $docRoot . "controllers/nonce";
    if (is_dir($nonceDir)) {
        $nonceCounterJson = $nonceDir . "/nonceCounter.json";
        if (file_exists($nonceCounterJson)) {
            // If file exist then .
            // 1. create nonceCounter.zip archieve file which stores the nonceCounter.json file.
            // 2. create a json back up file with name nonceCounter.json.bak.
            $fileBackUp = fileIO($nonceDir, $nonceCounterJson);
            if ($fileBackUp["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
            } else {
                $returnArr["errCode"] = 1;
                $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
            }
        } else {
            // if file not exist then
            // 1. call API to get current vector counter.
            // 2. create a nonceCounter.json file with current time and vector counter get from API.
            // 3. create nonceCounter.zip archieve file which stores the nonceCounter.json file.
            // 4. create a json back up file with name nonceCounter.json.bak.

            // get API response here
            $apiResult = '{"errCode":"-1","errMsg":"0000000000000048"}';
            // end API response here
            $apiResult = json_decode($apiResult, true);
            if ($apiResult["errCode"] == -1) {
                // get current timestamp.
                $currentTime = time();
                // get vector counter from API result.
                $vector_counter = $apiResult["errMsg"];

                $arr = array("time" => $currentTime, "vector_count" => $vector_counter);
                // json encode the data.
                $arr = json_encode($arr);

                // create a file pointer
                $filePointer = fopen($nonceCounterJson, "w");
                // write into the file i.e. $nonceCounterJson.
                fwrite($filePointer, $arr);
                // close the file pointer
                fclose($filePointer);
                // give full permission to the json file.
                chmod($nonceCounterJson, 0777);

                $fileBackUp = fileIO($nonceDir, $nonceCounterJson);
                if ($fileBackUp["errCode"] == -1) {
                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
                } else {
                    $returnArr["errCode"] = 1;
                    $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
                }
            } else {
                $returnArr["errCode"] = 1;
                $returnArr["errMsg"] = "Error: Unable to get API response.";
            }
        }
    } else {
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error: " . $nonceDir . " not found";
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function fileIO()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   fileIO()
*   Purpose       :   To create archeve and nonceCounter.json backup file.
*   Arguments     :   (string)$nonceDir, (string)$nonceCounterJson
*/

function fileIO($nonceDir, $nonceCounterJson)
{
    $returnArr = array();

    // If file exist then create or orverwrite the existing json file and create new json file with current time.
    $zip = new ZipArchive;
    $URL = $nonceDir . "/nonceCounter.zip";

    if ($zip->open($URL, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE) === TRUE) {
        $URL = $nonceDir . "/nonceCounter.json";
        $zip->addFile($URL, 'nonceCounter.json');
        $zip->close();

        // create a back up file i.e. nonceCounter.json.bak, if incase by mistake json file get deleted.

        // open nonce counter json file.
        $fp = fopen($nonceCounterJson, "r+");
        if ($fp) {
            // read the json file.
            $fpRead = fread($fp, filesize($nonceCounterJson));
            // close the file pointer ($fp).
            fclose($fp);

            // declear json backup variable
            $nonceBackUp = $nonceDir . "/nonceCounter.json.bak";

            // create a new file pointer that open new file IO.
            $newFP = fopen($nonceBackUp, "w");
            if ($newFP) {
                // write content into json backup file that  end extension with '.bak' i.e. nonceCounter.josn.bk.
                fwrite($newFP, $fpRead);

                // close file pointer
                fclose($newFP);

                // change mode permission to read, write and execute
                chmod($nonceBackUp, 0777);

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Successfully create json backup and archieve file.";
            } else {
                $returnArr["errCode"] = 3;
                $returnArr["errMsg"] = "Error: Unable to open file pointer for " . $nonceBackUp . " file.";
            }
        } else {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Error: Unable to open file pointer for " . $nonceCounterJson . " file.";
        }
    } else {
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error: Unable to create archive file.";
    }

    return $returnArr;
}


function initializeXMLLog($userEmail)
{

    $retArray = array();
    $deviceType = "web";
    $userIp = getClientIP();
    $data = getLocationUserFromIP($userIp);

    // check whether request parameter is email or account handle, if account handle then accordingly make the
    // activity attributes

    if (preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $userEmail)) {
        $activity = "email";
    } else {
        $activity = "account_handle";
    }

    $activity_attribute = array();
    $activity_attribute[$activity] = $userEmail;
    $activity_attribute['timestamp']=uDateTime("Y-m-d h:i:s A",date("Y-m-d h:i:s A"));
    $activity_attribute['browser'] = getBrowserName();
    $activity_attribute['userIp'] = $data["userIP"];
    $activity_attribute['device'] = $deviceType;
    $activity_attribute['country'] = $data["country"];
    $activity_attribute['state'] = $data["state"];
    $activity_attribute['city'] = $data["city"];
    $activity_attribute['gender'] = $_SESSION["gender"];
    $activity_attribute['username'] = $_SESSION["first_name"] . " " . $_SESSION["last_name"];

    $request_attribute = array();

    $retArray = array('activity' => $activity_attribute, 'request' => $request_attribute);

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getExchangeRate
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getExchangeRate()
*   Purpose       :   This function returns Value of exhange rates .
                      e.g: BTC To USD
*
*   Arguments     :   1.$exchangeRate is global Var in Config 2.(obj)$conn [connection object] to connect database.
*   Returns       :   Returns exchange rate from btc to all available parameters in exchangeRate.
*/
function getExchangeRate($exchangeRate, $conn)
{

    $returnArr = array();

    $exchangeRate = explode(",", $exchangeRate);
    foreach ($exchangeRate as $key => $currency) {
        $currency = explode("-", $currency);
        $fromCode = trim($currency[0]);
        $toCode = trim($currency[1]);
//echo $fromCode.":::".$toCode;
        // getting from currency rate.
        $fromCurrRate = getCurrentRate($fromCode, $conn);

        if (noError($fromCurrRate)) {
            $fromCurrRate = $fromCurrRate["errMsg"]["current_price"];

            // getting to currency rate
            $toCurrRate = getCurrentRate($toCode, $conn);

            if (noError($toCurrRate)) {
                $toCurrRate = $toCurrRate["errMsg"]["current_price"];
                if ($fromCode == "BTC") {
                    $amount = $toCurrRate / $fromCurrRate;
                } else {
                    $amount = (float)$toCurrRate;
                }
                $currKey = $fromCode . "-" . $toCode;
                $returnArr["errCode"][-1] = -1;
                $returnArr["errMsg"][$currKey] = $amount;
            } else {
                $returnArr["errCode"][4] = 4;
                $returnArr["errMsg"] = $toCurrRate["errMsg"];
            }
        } else {
            $returnArr["errCode"][5] = 5;
            $returnArr["errMsg"] = $fromCurrRate["errMsg"];
        }
    }
    return $returnArr;
}


function getCurrentRate($currency, $conn)
{

    $returnArr = array();
    $query = "SELECT current_price from currency_value where shortforms ='" . $currency . "'";

    $currResult = runQuery($query, $conn);
    if (noError($currResult)) {
        while ($row = mysqli_fetch_assoc($currResult["dbResource"])) {
            $returnArr["errCode"][-1] = -1;
            $returnArr["errMsg"] = $row;
        }
    } else {
        $returnArr["errCode"][2] = 2;
        $returnArr["errMsg"] = $currResult["errMsg"];
    }
    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getTotalKeywordSold
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getTotalKeywordSold()
*   Purpose       :   This function returns Total Keyword sold.
*
*   Arguments     :   (obj)$conn connection object of respective Database where table exists.
*   Returns       :   (int) Total Total Keyword sold.
*/

function getTotalKeywordSold($conn)
{
    $returnArr = array();
    $extraArgs = array();
    $query = "select sum(kwd_sold) as total_keyword_sold from first_purchase_slabs";

    $execQuery = runQuery($query, $conn);

    if (noError($execQuery)) {
        $cmsRecords = array();
        while ($row = mysqli_fetch_assoc($execQuery["dbResource"])) {
            $cmsRecords[] = $row;
        }

        $errMsg = "Successfully fetch Total Keyword Sold";
        $extraArgs["data"] = $cmsRecords;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    } else {
        $errMsg = "Error getting Total Keyword Sold ";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function getUserIDHandle($email)
{
    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */
    $fields = "&fieldnames=_id,account_handle";
    $apiText = "email={$email}{$fields}&publicKey={$walletPublicKey}";
    $postFields = "email=" . $email . "&publicKey=" . $walletPublicKey . $fields;
    $apiName = 'user/userdetails';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getUserInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getUserInfo()
*   Purpose       :   Get user balance details from wallet database by making an API request.
*   Arguments     :   1.(string) $email, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/


function getUserInfo($email, $requestUrl, $userRequiredFields)
{
    global $walletPublicKey, $mode;
    $retArray = array();
    $headers = array();

    /* create signature */
    $apiText = "email={$email}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
    $postFields = "email=" . $email . "&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
    // $requestUrl    = strtolower($requestUrl);
    $apiName = 'user/userdetails';
    $curl_type = 'GET';


    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }

    return $retArray;
}

function setUserNotifyInfo($id, $fieldType, $fieldValue)
{
    global $walletPublicKey, $walletURLIPnotification, $mode;
    $retArray = array();
    $headers = array();

    $apiText = "user_id={$id}&{$fieldType}={$fieldValue}&publicKey={$walletPublicKey}";
    $postFields = "{$fieldType}={$fieldValue}";

    $apiName = "api/notify/v2/user/{$id}/usermetadata";
    $curl_type = 'PUTbyID';

    $requestUrl = $walletURLIPnotification;

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }

    return $retArray;
}




/**
 * To send email or insert notification
 * To send email and notification both set preferenceCode 2
 * To send insert only notification keep  preferenceCode blank
 */
function sendNotification($to, $mailSubject, $emailBody, $firstName, $lastName, $userId, $smsText, $mobileNumber, $notificationBody, $preferenceCode, $category)
{
    global $walletPublicKey, $mode, $NotificationURL;
    $retArray = array();
    $headers = array();

    if (isset($to) && isset($mailSubject) && isset($userId) && isset($preferenceCode)) {

        $postFields = array(
            "mail_to" => $to,
            "mail_subject" => utf8_encode($mailSubject),
            "email_body" => utf8_encode($emailBody),
            "first_name" => $firstName,
            "last_name" => $lastName,
            "user_id" => $userId,
            "smsText" => utf8_encode($smsText),
            "mobileNumber" => $mobileNumber,
            "notification_body" => utf8_encode($notificationBody),
            "preference_code" => $preferenceCode,
            "category" => $category,
            "publicKey" => urlencode($walletPublicKey)
        );

        //create signature
        $apiText = "mail_to={$to}&mail_subject={$mailSubject}&user_id={$userId}&preference_code={$preferenceCode}&publicKey={$walletPublicKey}";
        $postFields=http_build_query($postFields);
        $apiName = "v2/inbox/user";
        $requestUrl = "{$NotificationURL}";
        $curl_type = 'POST';
        $content_type = "content-type: application/json";
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["ergenerateSignatureWithNoncerCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray['errCode'] = 7;
        $retArray['errMsg'] = "Mandatory feild not found";
    }

    return $retArray;
}




/**
 * @param $date
 * @param $timezone
 * @param $format
 * @return false|string
 */
function uDateTime($format,$date)
{


    if ($format == "") {
        $format = "d-m-Y h:i:s A";
    }

    if ($date == "") {
        $date = date($format);
    }
    $timezone=$_SESSION["timezone"];

    if ($timezone == "") {
        $timezone = "UTC";
    }

    $dateTimeZonec = new DateTimeZone($timezone);
    $dateTimeZone = new DateTime("now", $dateTimeZonec);
    $timeOffset = $dateTimeZonec->getOffset($dateTimeZone);
    if (strpos($date, '-') !== false || strpos($date, '/') !== false) {
        $offset_time = strtotime($date) + $timeOffset;
    } else {
        $offset_time = strtotime(date($format, $date / 1000)) + $timeOffset;
    }
    $finalDate = date($format, $offset_time);
    return $finalDate;
}


function backgroundProcessPost($url,$fieldsArray=array())
{
    $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
    session_write_close();
    $c = curl_init();
    curl_setopt($c, CURLOPT_URL, $url);
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);  // Follow the redirects (needed for mod_rewrite)
    curl_setopt($c, CURLOPT_HEADER, false);         // Don't retrieve headers
    curl_setopt($c, CURLOPT_NOBODY, true);          // Don't retrieve the body
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);  // Return from curl_exec rather than echoing
    curl_setopt($c, CURLOPT_FRESH_CONNECT, true);   // Always ensure the connection is fresh
    curl_setopt($c, CURLOPT_POST, count($fieldsArray));
    curl_setopt($c, CURLOPT_POSTFIELDS, makePostUrlForCurl($fieldsArray));
    curl_setopt($c, CURLOPT_COOKIE, $strCookie);

// Timeout super fast once connected, so it goes into async.
    curl_setopt($c, CURLOPT_TIMEOUT, 1);
    return curl_exec( $c );
}

function internalCurlRequestPost($url,$fieldsArray=array())
{
    $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
    session_write_close();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fieldsArray));
    curl_setopt($ch, CURLOPT_POSTFIELDS, makePostUrlForCurl($fieldsArray));
    curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function runBackgroundProcessSocket($url,$port){
    $parts=parse_url($url);

    $fp = fsockopen($parts['host'],
        isset($parts['port'])?$parts['port']:$port,
        $errno, $errstr, 30);
    if (!$fp) {
        return false;
    } else {
        $out = "POST ".$parts['path']." HTTP/1.1\r\n";
        $out.= "Host: ".$parts['host']."\r\n";
        $out.= "Content-Type: application/x-www-form-urlencoded\r\n";
        $out.= "Content-Length: ".strlen($parts['query'])."\r\n";
        $out.= "Connection: Close\r\n\r\n";
        if (isset($parts['query'])) $out.= $parts['query'];

        fwrite($fp, $out);
        fclose($fp);
        return true;
    }
}

function makePostUrlForCurl($fieldsArray)
{
    $fields_string = "";
    foreach ($fieldsArray as $key => $value) {
        $fields_string .= $key . '=' . $value . '&';
    }
    return rtrim($fields_string, '&');
}
?>
