<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 28/11/16
 * Time: 7:03 PM
 */


function cleanQueryParameter($conn, $string)
{

    $string = trim($string);
    $string = addslashes($string);
    $string = mysqli_real_escape_string($conn, $string);

    return $string;
}

function cleanXSS($string)
{
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
}

function cleanDisplayParameter($conn, $string)
{

    /*$string = stripslashes($string);
    $string = sanitize_data($string, ENT_QUOTES | ENT_HTML5);*/
    $string=htmlspecialchars_decode($string);
    $string=html_entity_decode($string,ENT_QUOTES);
    $string = sanitize_data($string);
    $string = mysqli_real_escape_string($conn, $string);

    return $string;
}

function sanitize_data($input_data)
{
    return htmlentities(stripslashes($input_data), ENT_QUOTES | ENT_HTML5);
}


function decodeURLparameter($datastring)
{
    $decodedquery = urldecode($datastring);
    parse_str($decodedquery, $queryStringForTransaction);

    return $queryStringForTransaction;
}

function decodeRequestParameter($string)
{
    $reqParam = trim(urldecode($string));
    $reqParam = utf8_decode($reqParam);

    return $reqParam;

}

?>