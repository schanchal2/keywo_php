<?php

function getTotalKeyword_Sold($conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "SELECT sum(kwd_sold) AS total_sold_kwd from first_purchase_slabs;";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}


function getTotalTradedKeywords($conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "select count(*) as count from latest_trades  where id IN (SELECT MAX(id) FROM latest_trades GROUP BY keyword) GROUP BY keyword ORDER by trade_time DESC;";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt=$res[0]["count"];
        if($datacnt=="")
        {
            $datacnt=0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}

?>