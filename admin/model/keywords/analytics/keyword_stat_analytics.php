<?php
function insert_keyword_statistics($statType,$amount,$conn)
{

    $amount=(double) $amount;

    $extraArg = array();
    $returnArr = array();

    switch ($statType) {

        case "keyword_lifeTime_intractions":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "keyword_lifeTime_earnings":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+{$amount}";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_kwd_sold_by_paypal":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_kwd_sold_by_btc":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_kwd_sold_by_itd":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_kwd_traded":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+1";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_keyword_sold_amount":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+{$amount}";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_keyword_traded_amount":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+{$amount}";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_keyword_traded_fees":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+{$amount}";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;

        case "total_keyword_renewal_fees":

            $query = "UPDATE keywords_stat SET  {$statType} = {$statType}+{$amount}";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Keywords Statistics update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

            break;
    }
    return $returnArr;

}

?>