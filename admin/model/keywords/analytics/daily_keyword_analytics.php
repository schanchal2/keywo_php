<?php

function getTransactionDetail($date_jumpers_lt, $date_jumpers_gt, $short_type, $transaction_status)
{

    global $short_type, $date_jumpers_lt, $date_jumpers_gt, $walletPublicKey, $mode, $walletURL, $transaction_status;
    $retArray = array();

    if (!empty($short_type)) {

        $headers = array();
        // user_id="."all"."&user_email=".''."&publicKey=".urlencode($walletPublicKey);
        $apiText = "user_id=" . 'all' . "&publicKey=" . urlencode($walletPublicKey);
        echo "<br><br>";

        $postFields = "date_jumpers_lt=" . $date_jumpers_lt . "&date_jumpers_gt=" . urlencode($date_jumpers_gt) . "&short_type=" . $short_type . "&transaction_status=" . $transaction_status . "&publicKey=" . $walletPublicKey;

        $apiName = "pocket/all/transactions/";

        $requestUrl = $walletURL;    //.$api_request_url;
        $curl_type = 'GET';

        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }

    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;

}

function createTable_daily_keyword_earnings($current_year, $current_month, $conn)
{

    $extraArg = array();
    $returnArr = array();


    $lastYear = date("Y", strtotime("-1 year"));
    $lastMonth = date("m", strtotime("-1 month"));

    // $query = "CREATE TABLE IF NOT EXISTS daily_keyword_earnings_{$current_month}_{$current_year} LIKE daily_keyword_earnings_{$lastMonth}_{$lastYear}";

    $query = "CREATE TABLE IF NOT EXISTS `daily_keyword_earnings_{$current_month}_{$current_year}` (
              `id` bigint(22) NOT NULL AUTO_INCREMENT,
              `country` varchar(255) DEFAULT NULL,
              `gender` varchar(255) DEFAULT NULL,
              `device` varchar(255) DEFAULT NULL,
              `browser` varchar(255) DEFAULT NULL,
              `keyword` varchar(255) DEFAULT NULL,
              `keyword_owner` varchar(255) DEFAULT NULL,
              `interaction_type` varchar(255) DEFAULT NULL,
              `type_qualification` varchar(255) DEFAULT NULL,
              `post_type` varchar(255) DEFAULT NULL,
              `app_id` varchar(255) DEFAULT NULL,
              `total_interaction_count` bigint(22) DEFAULT '0',
              `total_interaction_earning` double DEFAULT '0',
              `tracking_date` date DEFAULT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "daily_keyword_analytics table created sucessfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {
        $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
    }


    return $returnArr;
}


function insert_keywordEarning_activity($intraction_type, $update_field_name, $country, $device, $browser, $gender, $app, $keyword, $type_qualification, $post_type, $intractionEarning, $keyword_owner, $current_date, $conn, $count)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    if ($intractionEarning == "") {
        $intractionEarning = 0;
    }
    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM daily_keyword_earnings_{$current_month}_{$current_year} WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app_id = '{$app}' AND interaction_type = '{$intraction_type}' AND type_qualification = '{$type_qualification}' AND post_type = '{$post_type}' AND keyword_owner='{$keyword_owner}' AND tracking_date = '{$current_date}' AND keyword='{$keyword}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE daily_keyword_earnings_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}+{$count},total_interaction_earning = total_interaction_earning+$intractionEarning WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app_id = '{$app}' AND interaction_type = '{$intraction_type}' AND type_qualification = '{$type_qualification}' AND post_type ='{$post_type}' AND keyword_owner='{$keyword_owner}' AND tracking_date = '{$current_date}' AND keyword='{$keyword}'";

            $result = runQuery($query, $conn);


            if (noError($result)) {

                $errMsg = "Intraction Keyword Earning avtivity updated sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO daily_keyword_earnings_{$current_month}_{$current_year} (country,device,browser,gender,app_id,interaction_type,type_qualification,post_type,total_interaction_earning,keyword_owner,tracking_date,{$update_field_name},keyword)
                                   VALUES('{$country}','{$device}','{$browser}','{$gender}','{$app}','{$intraction_type}','{$type_qualification}','{$post_type}','{$intractionEarning}','{$keyword_owner}','{$current_date}', {$count},'{$keyword}');";

            $result = runQuery($query, $conn);


            if (noError($result)) {

                $errMsg = "Intraction Keyword Earning avtivity Insert sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}


function insert_top1000_keywordEarning_interaction($keyword, $earning, $interaction, $conn)
{

    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM top_1000_keyword_earnings_interaction WHERE keyword='{$keyword}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE top_1000_keyword_earnings_interaction SET  kwd_earnings = kwd_earnings+{$earning},kwd_interaction = kwd_interaction+{$interaction} WHERE keyword='{$keyword}'";

            $result = runQuery($query, $conn);


            if (noError($result)) {

                $errMsg = "Interaction Keyword Earning activity updated successfully for top 1000 keywords";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO top_1000_keyword_earnings_interaction (keyword,kwd_earnings,kwd_interaction)
                                   VALUES('{$keyword}','{$earning}',{$interaction});";

            $result = runQuery($query, $conn);


            if (noError($result)) {

                $query = "SELECT count(*) as count FROM top_1000_keyword_earnings_interaction";
                $result = runQuery($query, $conn);
                if (noError($result)) {
                    if ($row = mysqli_num_rows($result["dbResource"])) {
                        $count = $row["count"];
                        if ($count > 1000) {
                            $query = "SELECT id,kwd_earnings FROM `top_1000_keyword_earnings_interaction` ORDER BY `kwd_earnings` ASC LIMIT 1";

                            $result = runQuery($query, $conn);
                            if (noError($result)) {
                                if ($row = mysqli_num_rows($result["dbResource"])) {
                                    $id = $row["id"];
                                    if($id!="")
                                    {
                                        $query = "DELETE FROM `top_1000_keyword_earnings_interaction` where id={$id}";

                                        $result = runQuery($query, $conn);
                                        if (noError($result)) {
                                            $errMsg = "Interaction Keyword Earning activity Insert successfully for top 1000 Keywords";
                                            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
                                        }
                                    }
                                }
                                }
                        }
                    }


                }

                $errMsg = "Interaction Keyword Earning activity Insert successfully for top 1000 Keywords";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}

?>