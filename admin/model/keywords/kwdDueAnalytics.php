<?php
/**
 * Created by PhpStorm.
 * User: dinesh
 * Date: 2/5/17
 * Time: 11:24 AM
 */

function keywordDueForRenewal($connKeywords,$expiry) //get table rows count
{

    $expiry = str_replace("_"," ",cleanQueryParameter($connKeywords, cleanXSS($expiry)));


    if($expiry=="")
    {
        $expiry="3 month";
    }
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as count FROM view_keyword_due_for_renewal WHERE ownership_expiry_time BETWEEN now() AND now() + INTERVAL {$expiry};";
    $result = runQuery($query, $connKeywords);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["count"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }

    return $returnArr;
}

function keywordsExpired($connKeywords) //get table rows count
{

    $extraArg = array();
    $returnArr = array();

    $query = "SELECT COUNT(*) as count FROM view_expired_keywords WHERE ownership_expiry_time < NOW()";
    $result = runQuery($query, $connKeywords);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["count"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }

    return $returnArr;
}