<?php

function deleteSuggestedKwd($kwdid, $connAKwd)
{
$returnArr = array();
$extraArg = array();
$query = "DELETE FROM `suggested_keywords` WHERE id='" . $kwdid . "'";

$result = runQuery($query, $connAKwd);

if (noError($result)) {

$errMsg = "Keyword Deleted Successfully.";
$returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
} else {
$returnArr = setErrorStack($returnArr, 20, null, $extraArg);
}
return $returnArr;
}


function deleteBlockedKwd($kwdid, $connAKwd)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `blocked_keywords` WHERE id='" . $kwdid . "'";

    $result = runQuery($query, $connAKwd);

    if (noError($result)) {

        $errMsg = "Keyword Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteBlockedDomain($kwdid, $connAKwd)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `blocked_domains` WHERE id='" . $kwdid . "'";

    $result = runQuery($query, $connAKwd);

    if (noError($result)) {

        $errMsg = "Domian Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteBlockedIP($id, $conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `blocked_ip` WHERE id='" . $id . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "IP Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function getBlcokedIP($id, $conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "SELECT ip_address FROM `blocked_ip` WHERE id='" . $id . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}
?>