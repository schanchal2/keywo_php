<?php



function getKeywordOwnershipDetails($kwdConn, $keyword) {

    //get keyword ownership table name
    $kwdTableName = getKeywordOwnershipTableName($keyword);

    $returnArr    = array();

    //cleaning of parameters for SQL Injection
    $kwdTableName = cleanQueryParameter($kwdConn, $kwdTableName);
    $keyword = cleanQueryParameter($kwdConn, $keyword);

    $query  = "SELECT * FROM " . $kwdTableName . " WHERE keyword = '" . $keyword . "' AND status='sold'";
    $result = runQuery($query, $kwdConn);
//    print_r($result);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }
    return $returnArr;

}

function getKeywordOwnershipTableName($keyword) {

    //$keyword = strip_tags($keyword);
    $keyword = strtolower($keyword);

    //Get first char
    $firstchar     = substr($keyword, 0, 1);
    $firstchar_low = strtolower($firstchar);
    if (strcasecmp($firstchar_low, $firstchar) == 0) {
        $firstchar = $firstchar_low;
    }

    //Get correct table-name
    $tableName = "";

    if(strlen($keyword) != mb_strlen($keyword, 'utf-8')){
        /*  if(strlen($keyword) != strlen($keyword, 'utf-8')){*/
        $tableName = "keywords_ownership_other";
    }else if (ctype_alpha($firstchar)) {
        $tableName = "keywords_ownership_" . $firstchar;
    } elseif (ctype_digit($firstchar)) {
        $tableName = "keywords_ownership_num";
    } elseif (ctype_punct($firstchar)) {
        $tableName = "keywords_ownership_num";
    } elseif (ctype_space($firstchar)) {
        $str = preg_replace('/\s+/', '', $firstchar);
    }else{
        $tableName = "keywords_ownership_num";
    }

    return $tableName;

}

function checkForKeywordAvailability($keyword,$conn){

    $keyword = cleanQueryParameter($conn, $keyword); //echo "<pre>".$keyword."</pre>";

    $retArray = array();
    if(isset($keyword) && !empty($keyword)){
        $result = getKeywordOwnershipDetails($conn, $keyword); //print_r($result);
        if(noError($result)){
            $kwdDetails = $result["errMsg"];
            $kwdOwnerEmail = $kwdDetails["buyer_id"]; //print_r("kwdOwnerEmail").($kwdOwnerEmail);
            $result= getBlockedKeyword($keyword,$conn);
            if(noError($result)){
                $returnArr["errCode"]=-1;
                $returnArr["keyword_details"]=$kwdDetails;
                $id = $result["errMsg"]["id"];
                if(isset($id) && !empty($id)){
                    $returnArr["errMsg"]="keyword_blocked";
                }else{
                    if($kwdOwnerEmail!=""){
                        $returnArr["errMsg"]="keyword_not_available";
                    }else{
                        $returnArr["errMsg"]="keyword_available";
                    }
                }
            }else{
                $returnArr["errCode"]=3;
                $returnArr["errMsg"]="Error getting keyword's blocked status";
            }
        }else{
            $returnArr["errCode"]=5;
            $returnArr["errMsg"]="Error getting ownership details";
        }
    }else{
        $returnArr["errCode"]=6;
        $returnArr["errMsg"]="Keyword not found";
    }
    
    return $returnArr;
}

function getBlockedKeyword($words,$conn){ /*  By Dinesh for Bloking Keywords*/

    //cleaning of parameters for SQL Injection
    $words = cleanQueryParameter($conn, $words); //echo $words;

    $returnArr = array();

    $query = "select * from blocked_keywords where keyword='" . $words . "' ";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;
    //echo "<pre>"; print_r($returnArr); echo "</pre>";die;
}



function getMyKeywordDetails($conn, $buyer_id)
{

    $returnArr = array();

    $query = "select * from mykeyword_details where buyer_id='" . $buyer_id . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}



function getAllRecentlySoldKeywords($offset, $no_of_rows = 10, $conn, $searchQuery, $owner)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT rsk.id,rsk.keyword,rsk.keyword_price,rsk.purchase_timestamp,rsk.buyer_id,mkd.transaction_details
			FROM recent_sold_keyword as rsk
			INNER JOIN mykeyword_details as mkd
			ON rsk.buyer_id=mkd.buyer_id
				where keyword not in (select DISTINCT keyword from latest_trades order by trade_time desc)";

    if (!in_array($searchQuery, $blanks)) {
        $searchQuery = explode(" ", $searchQuery);
        $searchQuery = "'" . implode("','", $searchQuery) . "'";
        $query .= " AND rsk.keyword IN (" . $searchQuery . ")";
    }

    if ($owner) {
        $query .= " AND rsk.buyer_id = '" . $owner . "'";
    }


    $query .= " ORDER by purchase_timestamp DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($no_of_rows, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $no_of_rows;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllRecentlySoldKeywordsCount($conn, $searchQuery, $owner)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT rsk.id,rsk.keyword,rsk.keyword_price,rsk.purchase_timestamp,rsk.buyer_id,mkd.transaction_details
			FROM recent_sold_keyword as rsk
			INNER JOIN mykeyword_details as mkd
			ON rsk.buyer_id=mkd.buyer_id
				where keyword not in (select DISTINCT keyword from latest_trades order by trade_time desc)";

    if (!in_array($searchQuery, $blanks)) {
        $searchQuery = explode(" ", $searchQuery);
        $searchQuery = "'" . implode("','", $searchQuery) . "'";
        $query .= " AND rsk.keyword IN (" . $searchQuery . ")";
    }

    if ($owner) {
        $query .= " AND rsk.buyer_id = '" . $owner . "'";
    }


    $query .= " ORDER by purchase_timestamp DESC";

     $finalQuery="select count(*) as count from ({$query}) as t";

    //echo $query;
    $result = runQuery($finalQuery, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllRecentlyTradedKeywordsData($offset, $no_of_rows = 10, $conn, $searchQuery, $owner)
{
    $returnArr = array();
    global $blanks;

    //$query = "select * from recent_sold_keyword where keyword not in (select DISTINCT keyword from latest_trades order by trade_time desc);";
    //if(empty($searchQuery))
    //{
    $query = " select * from latest_trades  where id IN (SELECT MAX(id) FROM latest_trades GROUP BY keyword )";
    //}
    if (!in_array($searchQuery, $blanks)) {
        $searchQuery = explode(" ", $searchQuery);
        $searchQuery = "'" . implode("','", $searchQuery) . "'";
        $query .= " AND keyword IN (" . $searchQuery . ")";
    }

    if ($owner) {
        $query .= " AND trader_email = '" . $owner . "'";
    }


    $query .= "  GROUP BY keyword ORDER by trade_time DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($no_of_rows, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $no_of_rows;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllRecentlyTradedKeywordsDataCount($conn, $searchQuery, $owner)
{

    $returnArr = array();
    global $blanks;

    $query = " select * from latest_trades  where id IN (SELECT MAX(id) FROM latest_trades GROUP BY keyword )";

    if (!in_array($searchQuery, $blanks)) {
        $searchQuery = explode(" ", $searchQuery);
        $searchQuery = "'" . implode("','", $searchQuery) . "'";
        $query .= " AND keyword IN (" . $searchQuery . ")";
    }

    if ($owner) {
        $query .= " AND trader_email = '" . $owner . "'";
    }

    $finalQuery="select count(*) as count from ({$query}) as t";

    //echo $query;
    $result = runQuery($finalQuery, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getKeywordStat($conn)
{

    $returnArr = array();

    $query = "select * from keywords_stat";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}