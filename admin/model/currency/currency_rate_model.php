<?php


function insertRatesRecords($usd, $sgd, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "INSERT INTO currentRatesRecord(timestamp,usd_rates,sgd_rates) VALUES(NOW(),'" . $usd . "','" . $sgd . "');";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        $result = $returnArr["errMsg"] = $res;
        $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);

    } else {
        $result = $result["errMsg"];
        $returnArr = setErrorStack($returnArr, 5, $result, $extraArg);
    }

    return $returnArr;

}


function getCurrencyRate($currency)
{
    $jsonn = file_get_contents("https://bitpay.com/rates/" . $currency . "");
    $json = json_decode($jsonn, true);
    return $json["data"]["rate"];
}

function getAmountInCurrentBtcRate($amount,$currency)
{
    $amount=$amount/$currency;
    return $amount;
}