<?php
$docrootpath = $_SERVER["DOCUMENT_ROOT"] . "/keywo/admin/";
//start config
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
//end config

//start helper
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}core/errorMap.php");
//To load RSS feeds.


function callRSS($rssInfo)
{

    $returnVal = array();
    $extraArg = array();
    $rssURL = $rssInfo['rss'];
    $connSearch = createDBConnection("dbsearch");
    noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

    if ($newsItemData = simplexml_load_file($rssURL)) {

        $saveNewsItems = saveNewsItems($newsItemData, $rssInfo, $connSearch);
        if (noError($saveNewsItems)) {
            $error = $saveNewsItems["errMsg"];
            $returnVal = setErrorStack($returnVal, -1, $error, $extraArg);
        } else {
            $error = $saveNewsItems["errMsg"];
            $returnVal = setErrorStack($returnVal, 55, $error, $extraArg);
        }
    } else {
        $error = $rssInfo["file_url"] . " RSS Feed failed to load.";
        $returnVal = setErrorStack($returnVal, 54, $error, $extraArg);

    }
    echo json_encode($returnVal);
}


//To Store RSS channel data,item data and item images data.	
function saveNewsItems($newsItemData, $rssInfo, $connSearch)
{

    $returnVal = array();
    $extraArg = array();

    $result = '';
    $updateFrequency = $rssInfo['updateFrequency'];
    $category = $rssInfo['category'];
    $fileURL = $rssInfo['file_url'];
    $rssURL = $rssInfo['rss'];

    foreach ($newsItemData as $newsData) {
        $channelTitle = $newsData->title;
        $channelDescription = $newsData->description;
        $channelLanguage = $newsData->language;
        $channelLink = $newsData->link;
        $pubDate = $newsData->pubDate;
        $channelLastUpdate = date('Y-m-d H:i:s');

        // $channelLastUpdate = date('Y-m-d H:i:s', strtotime($pubDate));
        //to check if channel is present or not.
        $sql = 'SELECT source FROM channels WHERE rss_url ="' . $rssURL . '"';

        $ress = mysqli_query($connSearch, $sql);
        while ($newsresult = mysqli_fetch_assoc($ress)) {
            $link = $newsresult['source'];
        }

        if (null == $link) {
            $sql = 'INSERT INTO channels(title, description, language, category, last_update, update_frequency, source, file_url, rss_url) VALUES ("' . $channelTitle . '", "' . $channelDescription . '", "' . $channelLanguage . '", "' . $category . '", "' . $channelLastUpdate . '", "' . $updateFrequency . '", "' . $channelLink . '", "' . $fileURL . '", "' . $rssURL . '")';
        } else {
            $sql = 'UPDATE channels SET  category="' . $category . '", last_update="' . $channelLastUpdate . '",update_frequency="' . $updateFrequency . '", file_url="' . $fileURL . '" WHERE rss_url ="' . $rssURL . '" ';
        }
        $resultQuery = runQuery($sql, $connSearch);

        if (noError($resultQuery)) {
            $sql = 'SELECT channel_id FROM channels WHERE rss_url ="' . $rssURL . '" ';
            $resultQuery = runQuery($sql, $connSearch);

            while ($newsresult = mysqli_fetch_assoc($resultQuery["dbResource"])) {
                $channelId = $newsresult['channel_id'];
            }


            //to store unique news items in items.
            foreach ($newsData->item as $item) {
                $image_url = null;
                $itemTitle = strip_tags($item->title);
                $itemDescription = strip_tags($item->description);
                // $itemDescription = $item->description;
                $itemLink = $item->link;
                $pubDate = $item->pubDate;
                $itemUpdatedate = date('Y-m-d H:i:s', strtotime($pubDate));
                $itemSource = $item->source;
                if (empty($itemSource)) {
                    $itemSource = $rssInfo['source'];
                }

                $pieces = explode(" ", addslashes($itemTitle));
                $revPieces = array_reverse($pieces);
                $revPieces = array_reverse(array_splice($revPieces, 0, 5));
                $first_part = '%' . implode("%", array_splice($pieces, 0, 5)) . '%';
                $last_part = '%' . implode("%", $revPieces) . '%';

                //check for duplicate news item
                $sql = "SELECT link FROM items WHERE link = '" . $itemLink . "' OR title LIKE '" . $first_part . "' OR   title LIKE '" . $last_part . "'";
                // $res1 = mysqli_query($conn, $sql);
                $resultQuery = runQuery($sql, $connSearch);
                $newsresult = mysqli_fetch_array($resultQuery["dbResource"]);
                $checkItem = $newsresult['link'];
                if (!empty($checkItem)) {
                    continue;
                }
                $sql = "INSERT INTO items(channel_id, title, description, link, source, publish_date) VALUES ('$channelId', '" . addslashes($itemTitle) . "', '" . addslashes(
                        $itemDescription) . "', '$itemLink', '$itemSource', '$itemUpdatedate')";

                $resultQuery = runQuery($sql, $connSearch);
                if (noError($resultQuery)) {

                    //if image is absent in the item
                    if (isset($item->content)) {
                        $sql_item_id = 'SELECT item_id FROM items WHERE link = "' . $itemLink . '"';
                        $resultQuery = runQuery($sql_item_id, $connSearch);
                        // $res = mysqli_query($conn, $sql);
                        $newsresult = mysqli_fetch_array($resultQuery["dbResource"]);
                        $itemId = $newsresult['item_id'];
                        if (!empty($itemId)) {

                            $media = $item->children('media', 'http://search.yahoo.com/mrss/');
                            $content = $media->content;
                            $url = $content[0]->attributes()->url;
                            $type = $content[0]->attributes()->type;
                            $width = $content[0]->attributes()->width;
                            $height = $content[0]->attributes()->height;
                            $sql = 'INSERT INTO images(channel_id, item_id, url, type, width, height) VALUES ("' . $channelId . '", "' . $itemId . '", "' . $url . '", "' . $type . '", "' . $width . '", "' . $height . '")';
                            $resultQuery = runQuery($sql, $connSearch);
                            if (noError($resultQuery)) {
                                $error = $resultQuery["errMsg"];
                                $returnVal = setErrorStack($returnVal, -1, $error, $extraArg);
                            } else {
                                $error = $resultQuery["errMsg"];
                                $returnVal = setErrorStack($returnVal, 59, $error, $extraArg);
                            }
                        }
                    } elseIf (isset($item->thumbnail)) {
                        $sql_item_id = 'SELECT item_id FROM items WHERE link = "' . $itemLink . '"';
                        $resultQuery = runQuery($sql_item_id, $connSearch);
                        // $res = mysqli_query($conn, $sql);
                        $newsresult = mysqli_fetch_array($resultQuery["dbResource"]);
                        $itemId = $newsresult['item_id'];
                        if (!empty($itemId)) {
                            $namespace = "http://search.yahoo.com/mrss/";
                            $image = $item->children($namespace)->thumbnail[1]->attributes();
                            $url = $image['url'];
                            $width = $image['width'];
                            $height = $image['height'];
                            $sql = 'INSERT INTO images(channel_id, item_id, url, type, width, height) VALUES ("' . $channelId . '", "' . $itemId . '", "' . $url . '", "' . $type . '", "' . $width . '", "' . $height . '")';
                            $resultQuery = runQuery($sql, $connSearch);
                            if (noError($resultQuery)) {
                                $error = $resultQuery["errMsg"];
                                $returnVal = setErrorStack($returnVal, -1, $error, $extraArg);
                            } else {
                                $error = $resultQuery["errMsg"];
                                $returnVal = setErrorStack($returnVal, 58, $error, $extraArg);
                            }
                        }
                    } else {
                        $error = $resultQuery["errMsg"];
                        $returnVal = setErrorStack($returnVal, -1, $error, $extraArg);
                    }
                } else {
                    $error = $resultQuery["errMsg"];
                    $returnVal = setErrorStack($returnVal, 57, $error, $extraArg);
                }
            }
            if (!$returnVal["errCode"]) {
                $error = $returnVal["errMsg"] = "All news are up to date.";
                $returnVal = setErrorStack($returnVal, -1, $error, $extraArg);
            }
        } else {
            $error = $resultQuery["errMsg"];
            $returnVal = setErrorStack($returnVal, 56, $error, $extraArg);
        }
    }

    return $returnVal;
}

?>