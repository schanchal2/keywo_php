<?php

function insert_intraction_activity($intraction_type,$update_field_name, $country, $device, $browser, $gender,$app,$sub_type_qualification,$post_type,$intractionEarning,$current_date, $conn)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    if($intractionEarning=="")
    {
        $intractionEarning=0;
    }

    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM daily_keywo_intraction_analytics_{$current_month}_{$current_year} WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app = '{$app}' AND intraction_type = '{$intraction_type}' AND sub_type_qualification = '{$sub_type_qualification}' AND post_type = '{$post_type}' AND tracking_date = '{$current_date}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


             $query = "UPDATE daily_keywo_intraction_analytics_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}+1,intraction_earnings = intraction_earnings+$intractionEarning WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app = '{$app}' AND intraction_type = '{$intraction_type}' AND sub_type_qualification = '{$sub_type_qualification}' AND post_type = '{$post_type}' AND tracking_date = '{$current_date}'";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Intraction avtivity updated sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO daily_keywo_intraction_analytics_{$current_month}_{$current_year} (country,device,browser,gender,app,intraction_type,sub_type_qualification,post_type,intraction_earnings,tracking_date,{$update_field_name})
                                   VALUES('{$country}','{$device}','{$browser}','{$gender}','{$app}','{$intraction_type}','{$sub_type_qualification}','{$post_type}','{$intractionEarning}','{$current_date}', '1');";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Intraction avtivity Insert sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}


function insert_intractionEarning_activity($intraction_type,$update_field_name, $country, $device, $browser, $gender,$app,$sub_type_qualification,$post_type,$intractionEarning,$email,$current_date,$username,$conn,$count)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    if($intractionEarning=="")
    {
        $intractionEarning=0;
    }
    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM daily_keywo_userEarning_analytics_{$current_month}_{$current_year} WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app = '{$app}' AND intraction_type = '{$intraction_type}' AND sub_type_qualification = '{$sub_type_qualification}' AND post_type = '{$post_type}' AND email='{$email}' AND tracking_date = '{$current_date}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE daily_keywo_userEarning_analytics_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}+{$count},intraction_earnings = intraction_earnings+$intractionEarning WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND app = '{$app}' AND intraction_type = '{$intraction_type}' AND sub_type_qualification = '{$sub_type_qualification}' AND post_type = '{$post_type}' AND email='{$email}' AND tracking_date = '{$current_date}'";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Intraction userEarning avtivity updated sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

              $query = "INSERT INTO daily_keywo_userEarning_analytics_{$current_month}_{$current_year} (country,device,browser,gender,app,intraction_type,sub_type_qualification,post_type,intraction_earnings,email,tracking_date,{$update_field_name},user_name)
                                   VALUES('{$country}','{$device}','{$browser}','{$gender}','{$app}','{$intraction_type}','{$sub_type_qualification}','{$post_type}','{$intractionEarning}','{$email}','{$current_date}', {$count},'{$username}');";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "Intraction userEarning avtivity Insert sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}



function createTable_keywo_intraction_analytics($current_year, $current_month, $conn)
{
    $extraArg = array();
    $returnArr = array();


    $lastYear = date("Y", strtotime("-1 year"));
    $lastMonth = date("m", strtotime("-1 month"));


        //$query = "CREATE TABLE IF NOT EXISTS daily_keywo_intraction_analytics_{$current_month}_{$current_year} LIKE daily_keywo_intraction_analytics_{$lastMonth}_{$lastYear}";
        $query = "
        CREATE TABLE `daily_keywo_intraction_analytics_{$current_month}_{$current_year}` (
                      `id` bigint(20) NOT NULL AUTO_INCREMENT,
                      `country` varchar(255) DEFAULT NULL,
                      `device` varchar(255) DEFAULT NULL,
                      `browser` varchar(255) DEFAULT NULL,
                      `gender` varchar(255) DEFAULT NULL,
                      `app` varchar(255) DEFAULT NULL,
                      `intraction_type` varchar(255) DEFAULT NULL,
                      `sub_type_qualification` varchar(255) DEFAULT NULL,
                      `post_type` varchar(255) DEFAULT NULL,
                      `no_of_searchORview_social` bigint(20) NOT NULL DEFAULT '0',
                      `intraction_earnings` double DEFAULT '0',
                      `tracking_date` date NOT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;";

        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "daily_keywo_intraction_analytics_ table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
        }


    return $returnArr;
}


function createTable_keywo_userEarning_analytics($current_year, $current_month, $conn)
{
    $extraArg = array();
    $returnArr = array();


    $lastYear = date("Y", strtotime("-1 year"));
    $lastMonth = date("m", strtotime("-1 month"));


    //$query = "CREATE TABLE IF NOT EXISTS daily_keywo_intraction_analytics_{$current_month}_{$current_year} LIKE daily_keywo_intraction_analytics_{$lastMonth}_{$lastYear}";
    $query = "
              CREATE TABLE `daily_keywo_userEarning_analytics_{$current_month}_{$current_year}` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `country` varchar(255) DEFAULT NULL,
              `device` varchar(255) DEFAULT NULL,
              `browser` varchar(255) DEFAULT NULL,
              `gender` varchar(255) DEFAULT NULL,
              `app` varchar(255) DEFAULT NULL,
              `email` varchar(255) DEFAULT NULL,
              `user_name` varchar(255) DEFAULT NULL,
              `intraction_type` varchar(255) DEFAULT NULL,
              `sub_type_qualification` varchar(255) DEFAULT NULL,
              `post_type` varchar(255) DEFAULT NULL,
              `no_of_searchORview_social` bigint(20) NOT NULL DEFAULT '0',
              `intraction_earnings` double DEFAULT '0',
              `tracking_date` date NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;";

    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "daily_keywo_userEarning_analytics_ table created sucessfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {
        $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
    }


    return $returnArr;
}



function getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,$qtype,$postType,$intractionType,$connSearch)
{
    $returnArr = array();
    $extraArg = array();

    $queryBuild=getMonthWiseQuery($fromdate,$todate,$device,$browser,$gender,$country,$app,$qtype,$postType,$intractionType,$connSearch);

    $query="select sum(no_of_searchORview_social) as no_of_searchORview_social,sum(intraction_earnings) as intraction_earning from (";
    $query .=$queryBuild;
    $query .= " ) as t";


    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 65, null, $extraArg);

    }

    return $returnArr;
}




function getMonthWiseQuery($fromdate,$todate,$device,$browser,$gender,$country,$app,$qtype,$postType,$intractionType,$connSearch)
{

    $fromexplode=explode("-",$fromdate);
    $startyear=$fromexplode[0];
    $startmonth=$fromexplode[1];
    $startdate=$fromexplode[2];


    $toexplode=explode("-",$todate);
    $endyear=$toexplode[0];
    $endmonth=$toexplode[1];
    $enddate=$toexplode[2];

    $queryBuild=array();
    $startmonthnew = $startmonth;
    $count=0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {


            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }


                if($count!=1)
                {
                    $query=" UNION ALL";
                    $query.= " select sum(no_of_searchORview_social) as no_of_searchORview_social,sum(intraction_earnings) as intraction_earnings from daily_keywo_intraction_analytics_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";

                }else{
                    $query = "select  sum(no_of_searchORview_social) as no_of_searchORview_social,sum(intraction_earnings) as intraction_earnings from daily_keywo_intraction_analytics_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                }


                if($device!="")
                {
                    $query.="and device=\"{$device}\"";
                }

                if($browser!="")
                {
                    $query.="and browser=\"{$browser}\"";
                }

                if($gender!="")
                {
                    $query.="and gender=\"{$gender}\"";
                }

                if($country!="")
                {
                    $query.="and country=\"{$country}\"";
                }

                if($app!="")
                {
                    $query.="and app=\"{$app}\"";
                }

                if($qtype!="")
                {
                    $query.="and sub_type_qualification=\"{$qtype}\"";
                }

                if($postType!="")
                {
                    $query.="and post_type=\"{$postType}\"";
                }

                 if($intractionType!="")
                 {
                     $query.="and intraction_type=\"{$intractionType}\"";
                 }



                array_push($queryBuild,$query);

            }


            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }


            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }


        }


    }


    return implode(" ",$queryBuild);
}


function getDayWiseQueryForReferrer($date,$subtype,$referralType, $connSearch)
{

    $returnArr = array();
    $extraArg = array();

    $fromexplode = explode("-", $date);
    $year = $fromexplode[0];
    $month = $fromexplode[1];

    $tablename = "daily_keywo_userReferral_analytics_{$month}_{$year}";

    $countries = getAllCountryListUsedInDay($connSearch, $tablename, $date)["errMsg"];
    //$browsers = getAllbrowserListUsedInDay($connSearch, $tablename, $date)["errMsg"];

    $devices = array("web", "mobile");
    $genders = array("male", "female", "other");

   // createTable_daily_TOP1000_Referrer($year, $month,"Referrer", $connSearch);

    foreach ($countries as $countryselect) {

        //if ($countryselect != "") {
                foreach ($devices as $deviceTypeselect) {
                    foreach ($genders as $genderTypeselect) {
                        $query = "SELECT intraction_earnings,no_of_referrals,email,user_name,tracking_date from(SELECT s.intraction_earnings,r.no_of_referrals,r.email,r.user_name,r.gender,r.device,r.country,r.tracking_date FROM daily_keywo_userReferral_analytics_{$month}_{$year} as r LEFT JOIN daily_keywo_userEarning_analytics_{$month}_{$year} as s ON r.email = s.email and r.device=s.device and r.gender=s.gender and r.country=s.country and r.tracking_date=s.tracking_date and s.intraction_type='{$subtype}' and r.tracking_date='{$date}')as top_refererrs where country='{$countryselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' LIMIT 1000";

                        $result = runQuery($query, $connSearch);

                        if (noError($result)) {
                            $res = array();
                            while ($row = mysqli_fetch_assoc($result["dbResource"]))
                                array_push($res, $row);

                            if (!empty($res)) {
                                $top1000Json = json_encode($res, true);


                                 $querydata = "SELECT * FROM daily_TOP1000_Referrer_analytics_{$month}_{$year} WHERE country='{$countryselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' AND tracking_date = '{$date}'";

                                $resultnew = runQuery($querydata, $connSearch);


                                if (noError($resultnew)) {

                                    if (mysqli_num_rows($resultnew["dbResource"])) {

                                        $returnArr["errCode"] = "-1";
                                        $returnArr["errMsg"] = "already inserted";
                                    } else {
                                            $queryInsert = "INSERT INTO daily_TOP1000_Referrer_analytics_{$month}_{$year} (country,device,gender,referral_type,tracking_date,usersJson)
                                              VALUES('{$countryselect}','{$deviceTypeselect}','{$genderTypeselect}','{$referralType}','{$date}','{$top1000Json}');";


                                        $result = runQuery($queryInsert, $connSearch);

                                        if (noError($result)) {

                                            $returnArr["errCode"] = "-1";
                                            $returnArr["errMsg"] = "TOP 1000 User Insert sucessfully";

                                        } else {

                                        }
                                        $returnArr["errCode"] = "INS_FAIL";
                                        $returnArr["errMsg"] = "TOP 1000 User Insert Failed";

                                    }
                                } else {
                                    $returnArr["errCode"] = "INS_FAIL";
                                    $returnArr["errMsg"] = "TOP 1000 User Select Failed";

                                }


                            } else {

                                $returnArr["errCode"] = "INS_FAIL";
                                $returnArr["errMsg"] = "TOP 1000 User RES EMPTY";

                            }

                        } else {

                            $returnArr["errCode"] = "INS_FAIL";
                            $returnArr["errMsg"] = "Select main 1000 Failed";

                        }


                    }
               // }

        }

    }

    return $returnArr;
}
?>