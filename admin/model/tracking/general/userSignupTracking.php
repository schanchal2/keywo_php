<?php
function getNewSignupUser($fromdate,$todate,$skip,$limit,$flag,$country,$gender,$status)
{
    global $walletPublicKey, $mode, $walletURL;
    $requestUrl = $walletURL;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "from={$fromdate}&to={$todate}&skip={$skip}&limit={$limit}&flag={$flag}&country={$country}&gender={$gender}&status={$status}&publicKey={$walletPublicKey}";
    $postFields = "from=" . $fromdate ."&to=" . $todate . "&skip=" . $skip . "&limit=" . $limit . "&flag=" . $flag . "&publicKey=" . $walletPublicKey. "&country=" . rawurlencode($country). "&gender=" . $gender. "&status=" . $status;
    $apiName = 'admin/bysignuptime';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}





function getSignupAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$connSearch)
{
    $returnArr = array();
    $extraArg = array();

    $month=date('m');
    $year=date('Y');

    $fromexplode=explode("-",$fromdate);
    $fromyear=$fromexplode[0];


    $toexplode=explode("-",$todate);
    $toyear=$toexplode[0];
    $queryBuild=getMonthWiseQuery($fromdate,$todate,$device,$browser,$gender,$country);




        $query="select sum(Total_UserBy_referral) as Total_UserBy_referral , sum(TotalUnverifiedUsers) as TotalUnverifiedUsers,sum(TotalRegisteredUsers) as TotalRegisteredUsers, sum(TotalActivatedUsers) as TotalActivatedUsers from (";
        $query .=$queryBuild;
        $query .= " ) as t";


    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $userwithoutRefferal= $res[0]["TotalRegisteredUsers"]-$res[0]["Total_UserBy_referral"];
        $errMsg = $res;
        $errMsg[0]["UsersRegisteredWithoutRefferal"]=$userwithoutRefferal;
        $returnArr = setErrorStack($returnArr, -1, $errMsg[0], $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 60, null, $extraArg);

    }

    return $returnArr;
}




function getMonthWiseQuery($fromdate,$todate,$device,$browser,$gender,$country)
{

    $fromexplode=explode("-",$fromdate);
    $startyear=$fromexplode[0];
    $startmonth=$fromexplode[1];
    $startdate=$fromexplode[2];


    $toexplode=explode("-",$todate);
    $endyear=$toexplode[0];
    $endmonth=$toexplode[1];
    $enddate=$toexplode[2];

    $queryBuild=array();
    $startmonthnew = $startmonth;
    $count=0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {


                $count++;
                if ($fromdate) {

                    if (strlen($j) == 1) {
                        $j = "0" . $j;
                    }


                    if($count!=1)
                    {
                        $query=" UNION ALL";
                        $query.= " select sum(user_with_referral) as Total_UserBy_referral,sum(total_unverified_users) as TotalUnverifiedUsers,sum(user_signup_count) as TotalRegisteredUsers,sum(user_signUpactivationSuccess_count) as TotalActivatedUsers from daily_keywo_UAM_analytics_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";

                    }else{
                        $query = "select sum(user_with_referral) as Total_UserBy_referral,sum(total_unverified_users) as TotalUnverifiedUsers,sum(user_signup_count) as TotalRegisteredUsers,sum(user_signUpactivationSuccess_count) as TotalActivatedUsers from daily_keywo_UAM_analytics_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                    }


                    if($device!="")
                    {
                        $query.="and device=\"{$device}\"";
                    }

                    if($browser!="")
                    {
                        $query.="and browser=\"{$browser}\"";
                    }

                    if($gender!="")
                    {
                        $query.="and gender=\"{$gender}\"";
                    }

                    if($country!="")
                    {
                        $query.="and country=\"{$country}\"";
                    }





                    array_push($queryBuild,$query);

                }


            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }


            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }


        }


    }


return implode(" ",$queryBuild);
}
?>