<?php
function getBlockedUsers($fromdate,$todate,$skip,$limit,$flag,$country,$gender)
{
    global $walletPublicKey, $mode, $walletURLnotificationServer;
    $requestUrl = $walletURLnotificationServer;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "from={$fromdate}&to={$todate}&skip={$skip}&limit={$limit}&flag={$flag}&publicKey={$walletPublicKey}";
    $postFields = "from=" . $fromdate ."&to=" . $todate . "&skip=" . $skip . "&limit=" . $limit . "&flag=" . $flag . "&publicKey=" . $walletPublicKey. "&country=" . rawurlencode($country). "&gender=" . $gender;
    $apiName = 'admin/block_users';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}

?>