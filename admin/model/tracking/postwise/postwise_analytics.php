<?php
/**
 * Created by PhpStorm.
 * User: dinesh
 * Date: 28/4/17
 * Time: 5:46 PM
 */

function getPostInteractions($from,$to,$postType,$gender,$device,$country)
{

    global $walletPublicKey, $mode,$mongoServerSocial;
    $requestUrl =$mongoServerSocial; //notification

    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "from={$from}&to={$to}&publicKey={$walletPublicKey}&post_type={$postType}&gender={$gender}&device={$device}&country={$country}";
    $country=rawurlencode($country);
    $postFields ="from={$from}&to={$to}&publicKey={$walletPublicKey}&post_type={$postType}&gender={$gender}&device={$device}&country={$country}";
    $apiName = 'admin/post_interaction';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function getTopPostEarnings($from,$to,$postType,$gender,$device,$country,$flag)
{

    global $walletPublicKey, $mode,$mongoServerSocial;
    $requestUrl =$mongoServerSocial; //notification

    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "from={$from}&to={$to}&publicKey={$walletPublicKey}&post_type={$postType}&gender={$gender}&device={$device}&country={$country}&flag={$flag}";
    $country=rawurlencode($country);
    $postFields = "from={$from}&to={$to}&publicKey={$walletPublicKey}&post_type={$postType}&gender={$gender}&device={$device}&country={$country}&flag={$flag}";
    $apiName = 'admin/post_earnings';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function getReportedPost($from,$to,$categoryType,$postType,$flag,$skip,$limit)
{

    global $walletPublicKey, $mode,$mongoServerSocial;
    $requestUrl =$mongoServerSocial; //notification

    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "publicKey={$walletPublicKey}";
    $categoryType=rawurlencode($categoryType);
    $postFields = "from={$from}&to={$to}&publicKey={$walletPublicKey}&post_type={$postType}&flag={$flag}&skip={$skip}&limit={$limit}&report_category={$categoryType}";
    $apiName = "admin/post_type/{$postType}/report";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}