<?php
function count_cashout_analytics($fromdate, $todate, $country, $gender, $device,$transaction_type)
{
    $fromdate = strtotime($fromdate . " 00:00:00") * 1000;
    $todate = strtotime($todate . " 23:59:59") * 1000;
    //$todate = (strtotime($todate . " 23:59:59") * 1000) + 1000;

    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}";
    $country=rawurlencode($country);
    $postFields = "date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}&device={$device}&gender={$gender}&country={$country}&type={$transaction_type}";

    $apiName = "pocket/analytics/cashout_data";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function idt_purchaseNtransfer_analytics($fromdate, $todate, $country, $gender, $device,$transaction_type)
{
    $fromdate = strtotime($fromdate . " 00:00:00") * 1000;
    $todate = strtotime($todate . " 23:59:59") * 1000;
    //$todate = (strtotime($todate . " 23:59:59") * 1000) + 1000;

    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}";
    $country=rawurlencode($country);
    $postFields = "date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}&device={$device}&gender={$gender}&country={$country}&type={$transaction_type}";

    $apiName = "pocket/analytics/transfer_purchaseitd";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}

function highestWalletAnalytics($fromdate, $todate, $country, $gender, $device, $groupBy, $orderBy, $sort_type, $transactionType,$apiName,$flag)
{

    //analytics for api --> transfer|purchase_itd|cashout

    $fromdate = strtotime($fromdate . " 00:00:00") * 1000;
    $todate= strtotime($todate . " 23:59:59") * 1000;
    //$todate = (strtotime($todate . " 23:59:59") * 1000) + 1000;

    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */

     $apiText ="orderBy={$orderBy}&groupBy={$groupBy}&date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}";
    $country=rawurlencode($country);
    $postFields ="order_by={$orderBy}&group_by={$groupBy}&date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}&sort_type={$sort_type}&filter_device={$device}&filter_gender={$gender}&country={$country}&transaction_type={$transactionType},&flag={$flag}";

    $apiName = "pocket/analytics/{$apiName}";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}




function getHighestWalletBalanceUser($fromdate, $todate, $country, $gender, $device, $accountLevel, $accountStatus,$apiName)
{
    $fromdate = strtotime($fromdate . " 00:00:00") * 1000;
    $todate= strtotime($todate . " 23:59:59") * 1000;
    //$todate = (strtotime($todate . " 23:59:59") * 1000) + 1000;

    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText ="date_jumpers_lt={$todate}&date_jumpers_gt={$fromdate}&publicKey={$walletPublicKey}";
    $country=rawurlencode($country);
    $postFields ="account_level={$accountLevel}&account_status={$accountStatus}&date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}&filter_device={$device}&gender={$gender}&country={$country}";

    $apiName = "admin/analytics/{$apiName}";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}
?>