<?php

/*************************** first way ***************************************************/
function getAllbrowserListUsedInDay($connDemo, $tablename, $date)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select distinct(browser) from {$tablename} where tracking_date='{$date}'";

    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row["browser"]);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getAllCountryListUsedInDay($connDemo, $tablename, $date)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select distinct(country) from {$tablename} where tracking_date='{$date}'";

    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row["country"]);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getAllPost_TypeListUsedInDay($connDemo, $tablename, $date)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select distinct(post_type) from {$tablename} where tracking_date='{$date}'";

    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row["post_type"]);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}

function createTable_daily_TOP1000_UsersEarnings($current_year, $current_month, $type, $conn)
{

    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as existStatus FROM information_schema.TABLES WHERE (TABLE_SCHEMA = 'dbsearch') AND (TABLE_NAME = 'daily_TOP1000_{$type}_analytics_{$current_month}_{$current_year}')";

    $result = runQuery($query, $conn);
    while ($row = mysqli_fetch_assoc($result["dbResource"]))
        $res = $row;
    if (!$res["existStatus"]) {

        $query = "
        CREATE TABLE `daily_TOP1000_{$type}_analytics_{$current_month}_{$current_year}` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `country` varchar(255) DEFAULT NULL,
              `device` varchar(255) DEFAULT NULL,
              `gender` varchar(255) DEFAULT NULL,
              `post_type` varchar(255) DEFAULT NULL,
              `usersJson` text DEFAULT NULL,
              `tracking_date` date NOT NULL,
               PRIMARY KEY (`id`)
                )";

        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "daily_TOP1000_{$type}_analytics_ table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
        }


    } else {

        $errMsg = "table all ready exist";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    }
    return $returnArr;
}


function createTable_daily_TOP1000_Referrer($current_year, $current_month, $type, $conn)
{

    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as existStatus FROM information_schema.TABLES WHERE (TABLE_SCHEMA = 'dbsearch') AND (TABLE_NAME = 'daily_TOP1000_{$type}_analytics_{$current_month}_{$current_year}')";

    $result = runQuery($query, $conn);
    while ($row = mysqli_fetch_assoc($result["dbResource"]))
        $res = $row;
    if (!$res["existStatus"]) {

        $query = "
        CREATE TABLE `daily_TOP1000_{$type}_analytics_{$current_month}_{$current_year}` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `country` varchar(255) DEFAULT NULL,
              `device` varchar(255) DEFAULT NULL,
              `gender` varchar(255) DEFAULT NULL,
              `referral_type` varchar(255) DEFAULT NULL,
              `usersJson` text DEFAULT NULL,
              `tracking_date` date NOT NULL,
               PRIMARY KEY (`id`)
                )";

        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "daily_TOP1000_{$type}_analytics_ table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
        }


    } else {

        $errMsg = "table all ready exist";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    }
    return $returnArr;
}

function getDayWiseQuery($date, $intractionType, $connSearch)
{

    $returnArr = array();
    $extraArg = array();

    $fromexplode = explode("-", $date);
    $year = $fromexplode[0];
    $month = $fromexplode[1];

    $tablename = "daily_keywo_userEarning_analytics_{$month}_{$year}";

    $countries = getAllCountryListUsedInDay($connSearch, $tablename, $date)["errMsg"];
    //$browsers = getAllbrowserListUsedInDay($connSearch, $tablename, $date)["errMsg"];
    $postTypes = getAllPost_TypeListUsedInDay($connSearch, $tablename, $date)["errMsg"];


    $devices = array("web", "mobile");
    $genders = array("male", "female", "other");

    //createTable_daily_TOP1000_UsersEarnings($year, $month, $intractionType, $connSearch);

    foreach ($countries as $countryselect) {

        //if ($countryselect != "") {

            foreach ($postTypes as $postTypeselect) {
                if ($intractionType == "keyword_ownership_earning_social") {
                    if ($postTypeselect == "") {
                        continue;
                    }
                }
                foreach ($devices as $deviceTypeselect) {
                    foreach ($genders as $genderTypeselect) {
                        $query = "select tracking_date,email,user_name,sum(intraction_earnings) as total_Earnings,sum(no_of_searchORview_social) as no_ofViewSearch from {$tablename} where tracking_date='{$date}' AND country='{$countryselect}' AND post_type='{$postTypeselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' and intraction_type='{$intractionType}' GROUP BY email,user_name ORDER BY total_Earnings DESC LIMIT 1000 ";
                        $result = runQuery($query, $connSearch);

                        if (noError($result)) {
                            $res = array();
                            while ($row = mysqli_fetch_assoc($result["dbResource"]))
                                array_push($res, $row);

                            if (!empty($res)) {
                                $top1000Json = json_encode($res, true);


                                $querydata = "SELECT * FROM daily_TOP1000_{$intractionType}_analytics_{$month}_{$year} WHERE country='{$countryselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' AND tracking_date = '{$date}' AND post_type='{$postTypeselect}'";
                                $resultnew = runQuery($querydata, $connSearch);


                                if (noError($resultnew)) {

                                    if (mysqli_num_rows($resultnew["dbResource"])) {

                                        $returnArr["errCode"] = "-1";
                                        $returnArr["errMsg"] = "already inserted";
                                    } else {
                                        $queryInsert = "INSERT INTO daily_TOP1000_{$intractionType}_analytics_{$month}_{$year} (country,device,gender,post_type,tracking_date,usersJson)
                                              VALUES('{$countryselect}','{$deviceTypeselect}','{$genderTypeselect}','{$postTypeselect}','{$date}','{$top1000Json}');";


                                        $result = runQuery($queryInsert, $connSearch);

                                        if (noError($result)) {

                                            $returnArr["errCode"] = "-1";
                                            $returnArr["errMsg"] = "TOP 1000 User Insert sucessfully";

                                        } else {

                                        }
                                        $returnArr["errCode"] = "INS_FAIL";
                                        $returnArr["errMsg"] = "TOP 1000 User Insert Failed";

                                    }
                                } else {
                                    $returnArr["errCode"] = "INS_FAIL";
                                    $returnArr["errMsg"] = "TOP 1000 User Select Failed";

                                }


                            } else {

                                $returnArr["errCode"] = "INS_FAIL";
                                $returnArr["errMsg"] = "TOP 1000 User RES EMPTY";

                            }

                        } else {

                            $returnArr["errCode"] = "INS_FAIL";
                            $returnArr["errMsg"] = "Select main 1000 Failed";

                        }


                    }
                }
            //}
        }

    }

    return $returnArr;
}


function getDayWiseQueryTopToatalEarnersSS($date, $connSearch)
{

    $returnArr = array();
    $extraArg = array();

    $fromexplode = explode("-", $date);
    $year = $fromexplode[0];
    $month = $fromexplode[1];

    $tablename = "daily_keywo_userEarning_analytics_{$month}_{$year}";

    $countries = getAllCountryListUsedInDay($connSearch, $tablename, $date)["errMsg"];
    //$browsers = getAllbrowserListUsedInDay($connSearch, $tablename, $date)["errMsg"];
    $postTypes = getAllPost_TypeListUsedInDay($connSearch, $tablename, $date)["errMsg"];


    $devices = array("web", "mobile");
    $genders = array("male", "female", "other");

   // createTable_daily_TOP1000_UsersEarnings($year, $month, "TopTotalEarnersSearchSocial", $connSearch);

    foreach ($countries as $countryselect) {

        //if ($countryselect != "") {

            foreach ($postTypes as $postTypeselect) {
                foreach ($devices as $deviceTypeselect) {
                    foreach ($genders as $genderTypeselect) {
                        $query = "select tracking_date,email,user_name,sum(intraction_earnings) as total_Earnings,sum(no_of_searchORview_social) as no_ofViewSearch from {$tablename} where tracking_date='{$date}' AND country='{$countryselect}' AND post_type='{$postTypeselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' GROUP BY email,user_name ORDER BY total_Earnings DESC LIMIT 1000 ";
                        $result = runQuery($query, $connSearch);

                        if (noError($result)) {
                            $res = array();
                            while ($row = mysqli_fetch_assoc($result["dbResource"]))
                                array_push($res, $row);

                            if (!empty($res)) {
                                $top1000Json = json_encode($res, true);


                                $querydata = "SELECT * FROM daily_TOP1000_TopTotalEarnersSearchSocial_analytics_{$month}_{$year} WHERE country='{$countryselect}' AND device='{$deviceTypeselect}' AND gender='{$genderTypeselect}' AND tracking_date = '{$date}' AND post_type='{$postTypeselect}'";
                                $resultnew = runQuery($querydata, $connSearch);


                                if (noError($resultnew)) {

                                    if (mysqli_num_rows($resultnew["dbResource"])) {

                                        $returnArr["errCode"] = "-1";
                                        $returnArr["errMsg"] = "already inserted";
                                    } else {
                                        $queryInsert = "INSERT INTO daily_TOP1000_TopTotalEarnersSearchSocial_analytics_{$month}_{$year} (country,device,gender,post_type,tracking_date,usersJson)
                                              VALUES('{$countryselect}','{$deviceTypeselect}','{$genderTypeselect}','{$postTypeselect}','{$date}','{$top1000Json}');";


                                        $result = runQuery($queryInsert, $connSearch);

                                        if (noError($result)) {

                                            $returnArr["errCode"] = "-1";
                                            $returnArr["errMsg"] = "TOP 1000 User Insert sucessfully";

                                        } else {

                                        }
                                        $returnArr["errCode"] = "INS_FAIL";
                                        $returnArr["errMsg"] = "TOP 1000 User Insert Failed";

                                    }
                                } else {
                                    $returnArr["errCode"] = "INS_FAIL";
                                    $returnArr["errMsg"] = "TOP 1000 User Select Failed";

                                }


                            } else {

                                $returnArr["errCode"] = "INS_FAIL";
                                $returnArr["errMsg"] = "TOP 1000 User RES EMPTY";

                            }

                        } else {

                            $returnArr["errCode"] = "INS_FAIL";
                            $returnArr["errMsg"] = "Select main 1000 Failed";

                        }


                    }
                }
           // }
        }

    }

    return $returnArr;
}

function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
{

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while ($current <= $last) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}

function getTOP1000Earners($connDemo, $earningType, $fromdate, $todate, $country, $gender, $device, $post_type)
{


    $dateRange = date_range($fromdate, $todate, $step = '+1 day', $output_format = 'Y-m-d');


    $returnArr = array();
    $extraArg = array();

    $query = getMonthWiseQueryFortop1000($earningType, $fromdate, $todate, $country, $gender, $device, $post_type);

    $result = runQuery($query, $connDemo);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {

            $innerArray = json_decode($row["usersJson"], true);

            foreach ($innerArray as $value) {

                $emailCheck = my_array_search($res, "email", $value["email"])[0];
                $emailCheckEmail = $emailCheck["email"];
                $oldTotal = (double)$emailCheck["total_Earnings"];
                $newTotal = (double)$value["total_Earnings"];


                foreach ($dateRange as $dateranges) {

                    if ($dateranges != $value["tracking_date"] && $emailCheck["email"] != $value["email"]) {


                        $fromexplode = explode("-", $dateranges);
                        $year = $fromexplode[0];
                        $month = $fromexplode[1];
                        $tablename = "daily_keywo_userEarning_analytics_{$month}_{$year}";

                        $querys = "select sum(intraction_earnings) as total_Earnings,sum(no_of_searchORview_social) as no_ofViewSearch from {$tablename} where tracking_date='{$dateranges}' AND intraction_type='{$earningType}' AND email='{$value["email"]}'";

                        if ($device != "") {
                            $querys .= "and device=\"{$device}\"";
                        }

                        if ($gender != "") {
                            $querys .= "and gender=\"{$gender}\"";
                        }

                        if ($country != "") {
                            $querys .= "and country=\"{$country}\"";
                        }

                        if ($post_type != "") {
                            $querys .= "and post_type=\"{$post_type}\"";
                        }


                        $results = runQuery($querys, $connDemo);

                        if (noError($results)) {
                            $resultnewdata = array();
                            while ($rowsdata = mysqli_fetch_assoc($results["dbResource"])) {
                                array_push($resultnewdata, $rowsdata);
                            }
                            $value["total_Earnings"] = $value["total_Earnings"] + $resultnewdata[0]["total_Earnings"];
                            $value["no_ofViewSearch"] = $value["no_ofViewSearch"] + $resultnewdata[0]["no_ofViewSearch"];

                        }


                    }

                }
                if (($emailCheckEmail == $value["email"])) {

                    if (!($oldTotal >= $newTotal)) {

                        //array_push($res, $value);
                        $key = array_search($value["email"], array_column($res, 'email'));
                        $res[$key]["total_Earnings"] = $res[$key]["total_Earnings"] + $value["total_Earnings"];
                        $res[$key]["no_ofViewSearch"] = $res[$key]["no_ofViewSearch"] + $value["no_ofViewSearch"];
                    } else {

                        $key = array_search($value["email"], array_column($res, 'email'));
                        $res[$key]["total_Earnings"] = $res[$key]["total_Earnings"] + $value["total_Earnings"];
                        $res[$key]["no_ofViewSearch"] = $res[$key]["no_ofViewSearch"] + $value["no_ofViewSearch"];

                    }

                } else {

                    array_push($res, $value);


                }

            }


        }



        sortBy("total_Earnings", $res, "desc");
        $finalArray = array_slice($res, 0, 1000);

        $errMsg = $finalArray;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getMonthWiseQueryFortop1000($earningType, $fromdate, $todate, $country, $gender, $device, $post_type)
{

    $fromexplode = explode("-", $fromdate);
    $startyear = $fromexplode[0];
    $startmonth = $fromexplode[1];
    $startdate = $fromexplode[2];


    $toexplode = explode("-", $todate);
    $endyear = $toexplode[0];
    $endmonth = $toexplode[1];
    $enddate = $toexplode[2];

    $queryBuild = array();
    $startmonthnew = $startmonth;
    $count = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {


            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }

                $tablename = "daily_TOP1000_{$earningType}_analytics_{$j}_{$i}";

                if ($count != 1) {
                    $query = "  UNION ALL";
                    $query .= " select usersJson from {$tablename} where tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";

                } else {
                    $query = "select usersJson from {$tablename} where tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";
                }


                if ($device != "") {
                    $query .= "and device=\"{$device}\"";
                }

                if ($gender != "") {
                    $query .= "and gender=\"{$gender}\"";
                }

                if ($country != "") {
                    $query .= "and country=\"{$country}\"";
                }

                if ($post_type != "") {
                    $query .= "and post_type=\"{$post_type}\"";
                }


                array_push($queryBuild, $query);

            }


            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }


            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }


        }


    }

    return implode(" ", $queryBuild);


}

/**********************get TOP 1000 Referrer *****************************************************************************/

function getTOP1000EarnersReferrer($connDemo, $earningType, $fromdate, $todate, $country, $gender, $device, $referral_type)
{


    $dateRange = date_range($fromdate, $todate, $step = '+1 day', $output_format = 'Y-m-d');


    $returnArr = array();
    $extraArg = array();

    $query = getMonthWiseQueryFortop1000Referrer($earningType, $fromdate, $todate, $country, $gender, $device, $referral_type);

    $result = runQuery($query, $connDemo);

    if ($referral_type == "search") {
        $referral_type = "search_referral_earnings";
    } elseif ($referral_type == "social") {
        $referral_type = "social_referral_earnings";
    } elseif ($referral_type == "keyword") {
        $referral_type = "keyword_affiliate_earning";
    }

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {

            $innerArray = json_decode($row["usersJson"], true);

            foreach ($innerArray as $value) {

                $emailCheck = my_array_search($res, "email", $value["email"])[0];
                $emailCheckEmail = $emailCheck["email"];
                $oldTotal = (double)$emailCheck["intraction_earnings"];
                $newTotal = (double)$value["intraction_earnings"];


                foreach ($dateRange as $dateranges) {

                    if ($dateranges != $value["tracking_date"] && $emailCheck["email"] != $value["email"]) {

                        $fromexplode = explode("-", $dateranges);
                        $year = $fromexplode[0];
                        $month = $fromexplode[1];


                        //$querys = "select sum(intraction_earnings) as total_Earnings,sum(no_of_searchORview_social) as no_ofViewSearch from {$tablename} where tracking_date='{$dateranges}' AND intraction_type='{$earningType}' AND email='{$value["email"]}'";
                        $querys = "SELECT * from(SELECT s.intraction_earnings,r.no_of_referrals,r.email,r.user_name FROM daily_keywo_userReferral_analytics_{$month}_{$year} as r INNER JOIN daily_keywo_userEarning_analytics_{$month}_{$year} as s ON r.email = '{$value["email"]}' ";

                        if ($device != "") {
                            $querys .= "and r.device=s.device";
                        }

                        if ($gender != "") {
                            $querys .= "and r.gender=s.gender";
                        }

                        if ($country != "") {
                            $querys .= "and r.country=s.country";
                        }

                        if ($referral_type != "") {
                            $querys .= "and s.intraction_type=\"{$referral_type}\"";
                        }
                        $querys .= "and r.tracking_date='{$dateranges}')as e";

                        $results = runQuery($querys, $connDemo);

                        if (noError($results)) {
                            $resultnewdata = array();
                            while ($rowsdata = mysqli_fetch_assoc($results["dbResource"])) {
                                array_push($resultnewdata, $rowsdata);
                            }
                            $value["intraction_earnings"] = $value["intraction_earnings"] + $resultnewdata[0]["intraction_earnings"];
                            $value["no_of_referrals"] = $value["no_of_referrals"] + $resultnewdata[0]["no_of_referrals"];

                        }


                    }

                }
                if (($emailCheckEmail == $value["email"])) {
                    if (!($oldTotal >= $newTotal)) {
                        //array_push($res, $value);
                        $key = array_search($value["email"], array_column($res, 'email'));
                        $res[$key]["intraction_earnings"] = $res[$key]["intraction_earnings"] + $value["intraction_earnings"];
                        $res[$key]["no_of_referrals"] = $res[$key]["no_of_referrals"] + $value["no_of_referrals"];

                    } else {

                        $key = array_search($value["email"], array_column($res, 'email'));
                        $res[$key]["intraction_earnings"] = $res[$key]["intraction_earnings"] + $value["intraction_earnings"];
                        $res[$key]["no_of_referrals"] = $res[$key]["no_of_referrals"] + $value["no_of_referrals"];

                    }

                } else {

                    array_push($res, $value);


                }

            }


        }

        sortBy("no_of_referrals", $res, "desc");
        $finalArray = array_slice($res, 0, 1000);

        $errMsg = $finalArray;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getMonthWiseQueryFortop1000Referrer($earningType, $fromdate, $todate, $country, $gender, $device, $referral_type)
{

    $fromexplode = explode("-", $fromdate);
    $startyear = $fromexplode[0];
    $startmonth = $fromexplode[1];
    $startdate = $fromexplode[2];


    $toexplode = explode("-", $todate);
    $endyear = $toexplode[0];
    $endmonth = $toexplode[1];
    $enddate = $toexplode[2];

    $queryBuild = array();
    $startmonthnew = $startmonth;
    $count = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {


            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }

                $tablename = "daily_TOP1000_{$earningType}_analytics_{$j}_{$i}";

                if ($count != 1) {
                    $query = "  UNION ALL";
                    $query .= " select usersJson from {$tablename} where tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";

                } else {
                    $query = "select usersJson from {$tablename} where tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";
                }


                if ($device != "") {
                    $query .= "and device=\"{$device}\"";
                }

                if ($gender != "") {
                    $query .= "and gender=\"{$gender}\"";
                }

                if ($country != "") {
                    $query .= "and country=\"{$country}\"";
                }

                if ($referral_type != "") {
                    $query .= "and referral_type=\"{$referral_type}\"";
                }


                array_push($queryBuild, $query);

            }


            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }


            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }


        }


    }


    return implode(" ", $queryBuild);


}

/**********************get TOP 1000 Referrer *****************************************************************************/

/**************************** TOP 1000 Direct Query *********************************************************************/


function getMonthWiseQueryFortop1000EarnerTable($intraction_type, $isqualified, $fromdate, $todate, $country, $gender, $device, $app, $postType)
{

    $fromexplode = explode("-", $fromdate);
    $startyear = $fromexplode[0];
    $startmonth = $fromexplode[1];
    $startdate = $fromexplode[2];


    $toexplode = explode("-", $todate);
    $endyear = $toexplode[0];
    $endmonth = $toexplode[1];
    $enddate = $toexplode[2];

    $queryBuild = array();
    $startmonthnew = $startmonth;
    $count = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {


            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }

                $tablename = "daily_keywo_userEarning_analytics_{$j}_{$i}";

                if ($count != 1) {
                    $query = "  UNION ALL";
                    $query .= " select sum(no_of_searchORview_social) AS no_of_searchORview_social,sum(intraction_earnings) AS intraction_earnings,email,user_name FROM {$tablename} WHERE tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";

                } else {
                    $query = "select sum(no_of_searchORview_social) AS no_of_searchORview_social,sum(intraction_earnings) AS intraction_earnings,email,user_name FROM {$tablename} WHERE tracking_date BETWEEN '{$fromdate}' AND '{$todate}'";
                }


                if ($device != "") {
                    $query .= "and device=\"{$device}\"";
                }

                if ($gender != "") {
                    $query .= "and gender=\"{$gender}\"";
                }

                if ($country != "") {
                    $query .= "and country=\"{$country}\"";
                }

                if ($app != "") {
                    $query .= "and app=\"{$app}\"";
                }

                if ($intraction_type != "") {
                    $query .= "and intraction_type=\"{$intraction_type}\"";
                }

                if ($isqualified != "") {
                    $query .= "and sub_type_qualification=\"{$isqualified}\"";
                }

                if ($postType != "") {
                    $query .= "and post_type=\"{$postType}\"";
                }


                $query .=" GROUP BY email";
                array_push($queryBuild, $query);

            }


            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }


            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }


        }


    }


    return implode(" ", $queryBuild);


}


function getTOP1000EarnersTable($connSearch, $intraction_type, $isqualified, $fromdate, $todate, $country, $gender, $device, $app, $postType)
{


    $returnArr = array();
    $extraArg = array();

    $queryBuild = getMonthWiseQueryFortop1000EarnerTable($intraction_type, $isqualified, $fromdate, $todate, $country, $gender, $device, $app, $postType);


    $query = "SELECT sum(no_of_searchORview_social) AS no_ofViewSearch,sum(intraction_earnings) AS total_Earnings,email,user_name from (";
    $query .= $queryBuild;
    $query .= " ) as t";
    $query .= " GROUP BY email ORDER BY intraction_earnings DESC LIMIT 0,1000";


    $result = runQuery($query, $connSearch);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 65, null, $extraArg);

    }

    return $returnArr;
}

/**************************** TOP 1000 Direct Query *********************************************************************/

?>