<?php

/*
 * Function	    : 	getKeywordEarningData()
 * Purpose	    : 	To retrieve keyword analytics details .
 * Arguments	: 	(date)$fromdate, (date)$todate, {string)$device, (string)$country, (string)$connKeywords.
 * */
function getKeywordEarningData($fromdate, $todate, $device, $country, $page_position, $item_per_page, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];

    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate, $device, $country);

    $query       = "select sum(total_interaction_earning) as total_interaction_earning,sum(total_searchInteraction_count) as total_searchInteraction_count,sum(total_searchInteraction_earning) as total_searchInteraction_earning,sum(total_socialInteraction_count) as total_socialInteraction_count,sum(total_socialInteraction_earning)  as total_socialInteraction_earning,keyword,keyword_owner from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "group by keyword ";
    $query      .= "LIMIT $page_position, $item_per_page";

    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}

/*
 * Function	    : 	getMonthWiseQuery()
 * Purpose	    : 	To retrieve month wise query details .
 * Arguments	: 	(date)$fromdate, (date)$todate, {string)$device, (string)$country.
 * */
function getMonthWiseQuery($fromdate, $todate, $device, $country)
{
    $fromexplode  = explode("-", $fromdate);
    $startyear    = $fromexplode[0];
    $startmonth   = $fromexplode[1];
    $startdate    = $fromexplode[2];

    $toexplode    = explode("-", $todate);
    $endyear      = $toexplode[0];
    $endmonth     = $toexplode[1];
    $enddate      = $toexplode[2];

    $queryBuild    = array();
    $startmonthnew = $startmonth;
    $count         = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {

            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }
                if($count!=1)
                {
                    $query  = " UNION ALL";
                    $query  .= " select sum(total_interaction_earning) as total_interaction_earning,sum(case when interaction_type = 'search' then total_interaction_count end) AS total_searchInteraction_count,sum(case when interaction_type = 'search' then total_interaction_earning end) AS total_searchInteraction_earning,sum(case when interaction_type = 'social' then total_interaction_count end) AS total_socialInteraction_count,sum(case when interaction_type = 'social' then total_interaction_earning end) AS total_socialInteraction_earning,keyword,keyword_owner,interaction_type from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                }else{
                    $query  = "select sum(total_interaction_earning) as total_interaction_earning, sum(case when interaction_type = 'search' then total_interaction_count end) AS total_searchInteraction_count,sum(case when interaction_type = 'search' then total_interaction_earning end) AS total_searchInteraction_earning,sum(case when interaction_type = 'social' then total_interaction_count end) AS total_socialInteraction_count,sum(case when interaction_type = 'social' then total_interaction_earning end) AS total_socialInteraction_earning,keyword,keyword_owner,interaction_type from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                }
                if($device!="")
                {
                    $query.="and device=\"{$device}\"";
                }

                if($country!="")
                {
                    $query.="and country=\"{$country}\"";
                }

                //$query.="group by keyword,interaction_type";
                $query.="group by keyword";


                array_push($queryBuild,$query);
            }

            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }

            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }
        }
    }
    return implode(" ", $queryBuild);
}




function getKeywordEarningCount($fromdate, $todate, $device, $country, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];


    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate, $device, $country);

    $query       = "select count(*) as count from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "group by keyword ";
    $result = runQuery($query, $connKeywords);

    if (noError($result)) {

        $row = mysqli_num_rows($result['dbResource']);

        $returnArr = setErrorStack($returnArr, -1, $row, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}




/*
 * Function	    : 	getKeywordEarningData()
 * Purpose	    : 	To retrieve keyword analytics details .
 * Arguments	: 	(date)$fromdate, (date)$todate, {string)$device, (string)$country, (string)$connKeywords.
 * */
function getKeywordEarningAnalytics($fromdate, $todate, $device, $country, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];

    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate, $device, $country);

    $query       = "select sum(total_interaction_earning) as total_interaction_earning, sum(total_searchInteraction_count) as total_searchInteraction_count, sum(total_socialInteraction_count) as total_socialInteraction_count from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";

    $result      = runQuery($query, $connKeywords);


    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res[0], $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 60, null, $extraArg);
    }
    return $returnArr;
}
?>