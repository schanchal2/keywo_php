<?php

/*
 * Function	    : 	getKeywordEarningData()
 * Purpose	    : 	To retrieve keyword analytics details .
 * Arguments	: 	(date)$fromdate, (date)$todate, {string)$device, (string)$country, (string)$connKeywords.
 * */
function getKeywordSearchInteractionData($fromdate, $todate, $device, $country, $searchType, $page_position, $item_per_page, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];

    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate, $device, $country, $searchType);

    $query       = "select sum(total_interaction_count) as total_interaction_count, keyword, keyword_owner from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    $query      .= "group by keyword ";
    $query      .= "LIMIT $page_position, $item_per_page";

    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}

/*
 * Function	    : 	getMonthWiseQuery()
 * Purpose	    : 	To retrieve month wise query details .
 * Arguments	: 	(date)$fromdate, (date)$todate, {string)$device, (string)$country.
 * */
function getMonthWiseQuery($fromdate, $todate, $device, $country, $searchType)
{
    $fromexplode  = explode("-", $fromdate);
    $startyear    = $fromexplode[0];
    $startmonth   = $fromexplode[1];
    $startdate    = $fromexplode[2];

    $toexplode    = explode("-", $todate);
    $endyear      = $toexplode[0];
    $endmonth     = $toexplode[1];
    $enddate      = $toexplode[2];

    $queryBuild    = array();
    $startmonthnew = $startmonth;
    $count         = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {

            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }
                if($count!=1)
                {
                    $query  = " UNION ALL";
                    $query  .= " select  sum(total_interaction_count) AS total_interaction_count, keyword, keyword_owner from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                }else{
                    $query  = "select  sum(total_interaction_count) AS total_interaction_count, keyword, keyword_owner from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" ";
                }
                if($device!="")
                {
                    $query.="and device=\"{$device}\"";
                }

                if($country!="")
                {
                    $query.="and country=\"{$country}\"";
                }

                if($searchType != "")
                {
                    $query.="and type_qualification=\"{$searchType}\"";
                }

                //$query.="group by keyword,interaction_type";
                $query.="group by keyword";


                array_push($queryBuild,$query);
            }

            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }

            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }
        }
    }
    return implode(" ", $queryBuild);
}




function getKeywordSearchInteractionCount($fromdate, $todate, $device, $country, $searchType, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];


    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate, $device, $country, $searchType);

    $query       = "select count(*) as count from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "group by keyword ";
    $result = runQuery($query, $connKeywords);

    if (noError($result)) {

        $row = mysqli_num_rows($result['dbResource']);

        $returnArr = setErrorStack($returnArr, -1, $row, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}



function getKeywordBidInDateRange($fromdate, $todate,$bidstatus,$sortBy, $connKeywords)
{

    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];


    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $fromdate=$fromdate." 00:00:00";
    $todate=$todate." 23:59:59";
    $queryBuild  = getMonthWiseQueryBid($fromdate, $todate, $bidstatus);
    $query       = "select * from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "ORDER BY {$sortBy} desc limit 1000";

    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}


function getMyLastBids($email, $connKeywords)
{

    $returnArr   = array();
    $extraArg    = array();

    $query       = "select * from latest_bid where bid_time < now() and bidder_email='{$email}' ORDER BY bid_time desc";
    
    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}


function getKeywordBidInDateRangeCount($fromdate, $todate,$bidstatus,$sortBy, $connKeywords)
{

    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];


    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];

    $fromdate=$fromdate." 00:00:00";
    $todate=$todate." 23:59:59";
    $queryBuild  = getMonthWiseQueryBid($fromdate, $todate, $bidstatus);
    $query       = "select sum(bid_price) AS bid_price,count(keyword) as bid_count from (select * from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "ORDER BY {$sortBy} desc limit 1000) as data";
    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}
function getMonthWiseQueryBid($fromdate, $todate, $bidstatus)
{
    $fromexplode  = explode("-", $fromdate);
    $startyear    = $fromexplode[0];
    $startmonth   = $fromexplode[1];
    $startdate    = $fromexplode[2];

    $toexplode    = explode("-", $todate);
    $endyear      = $toexplode[0];
    $endmonth     = $toexplode[1];
    $enddate      = $toexplode[2];

    $queryBuild    = array();
    $startmonthnew = $startmonth;
    $count         = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {

            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }

                if($count!=1)
                {

                    $query   = " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_1 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_2 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_3 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_4 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                }else{
                    $query   = "        select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_1 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_2 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_3 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                    $query  .= " UNION  select  sum(bid_price) AS bid_price,count(keyword) as bid_count,keyword,kwd_owner_email from kwd_bids_{$i}_{$j}_4 where bid_placed_time BETWEEN \"{$fromdate}\" and \"{$todate}\" and bid_status='{$bidstatus}' group by keyword";
                }

                array_push($queryBuild,$query);
            }

            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }

            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }
        }
    }

    return implode(" ", $queryBuild);
}

?>