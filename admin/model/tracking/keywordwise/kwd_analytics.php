<?php
function keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device, $apiName, $flag, $payment_mode, $transaction_type)
{
    $fromdate = strtotime($fromdate . " 00:00:00") * 1000;
    $todate = strtotime($todate . " 23:59:59") * 1000;
    //$todate = (strtotime($todate . " 23:59:59") * 1000) + 1000;

    global $walletPublicKey, $mode, $walletURLIP;
    $requestUrl = $walletURLIP . "api/v3/";
    $retArray = array();

    $headers = array();
    /* create signature */

    $apiText = "date_jumpers_lt={$todate}&date_jumpers_gt={$fromdate}&publicKey={$walletPublicKey}";
    $country=rawurlencode($country);
    $postFields = "date_jumpers_gt={$fromdate}&date_jumpers_lt={$todate}&publicKey={$walletPublicKey}&device={$device}&gender={$gender}&country={$country}&flag={$flag}&payment_mode={$payment_mode}&transaction_type={$transaction_type}";

    $apiName = "admin/analytics/{$apiName}";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function getBlockedForBidUsers($fromdate, $todate, $device, $country, $gender, $connKeywords)
{
    $returnArr = array();
    $extraArg = array();
    $fromdate=$fromdate." 00:00:00";
    $todate=$todate." 23:59:59";

    $query = "SELECT sum(bid_price) AS bid_price,name,bidder_email,keyword ,bid_transaction_id,bid_time FROM latest_bid_all where bid_time BETWEEN '{$fromdate}' and '{$todate}'";

    if ($device != "") {
        $query .= " and device='{$device}'";
    }

    if ($country != "") {
        $query .= " and country='{$country}'";
    }

    if ($gender != "") {
        $query .= " and gender='{$gender}'";
    }
    $query .= " GROUP BY bidder_email,keyword ORDER BY bid_price DESC LIMIT 1000";

    $result = runQuery($query, $connKeywords);

    if (noError($result)) {

        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}

function getBlockedForBidUsersAnalytics($fromdate, $todate, $device, $country, $gender, $connKeywords)
{
    $returnArr = array();
    $extraArg = array();
    $fromdate=$fromdate." 00:00:00";
    $todate=$todate." 23:59:59";

    $query = "SELECT count(distinct bidder_email) AS user_participated_in_bid,count(keyword) AS bids_placed,sum(bid_price) as total_blocked_amount FROM latest_bid_all where bid_time BETWEEN '{$fromdate}' and '{$todate}'";

    if ($device != "") {
        $query .= " and device='{$device}'";
    }

    if ($country != "") {
        $query .= " and country='{$country}'";
    }

    if ($gender != "") {
        $query .= " and gender='{$gender}'";
    }

    $result = runQuery($query, $connKeywords);

    if (noError($result)) {

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
        $returnArr = setErrorStack($returnArr, -1, $row, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


?>