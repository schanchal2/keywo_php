
	--view_keyword_due_for_renewal <-view_name
	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_a
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_b
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"


UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_c
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_d
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_e
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"


UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_f
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_g
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_h
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_i
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_j
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_k
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_l
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_m
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_n
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_o
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_p
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_q
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_r
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_s
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_t
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_u
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_v
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_w
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_x
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"


UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_y
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_z
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_num
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_other
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month and status="sold"


-- other format

SELECT
	`keywords_ownership_a`.`buyer_id` AS `buyer_id`,
	`keywords_ownership_a`.`keyword` AS `keyword`,
	`keywords_ownership_a`.`purchase_timestamp` AS `purchase_timestamp`,
	`keywords_ownership_a`.`ownership_expiry_time` AS `ownership_expiry_time`
FROM
	`keywords_ownership_a`
WHERE
	(
		(
			`keywords_ownership_a`.`ownership_expiry_time` BETWEEN now()
			AND (now() + INTERVAL 3 MONTH)
		)
		AND (
			`keywords_ownership_a`.`status` = 'sold'
		)
	)
UNION
	SELECT
		`keywords_ownership_b`.`buyer_id` AS `buyer_id`,
		`keywords_ownership_b`.`keyword` AS `keyword`,
		`keywords_ownership_b`.`purchase_timestamp` AS `purchase_timestamp`,
		`keywords_ownership_b`.`ownership_expiry_time` AS `ownership_expiry_time`
	FROM
		`keywords_ownership_b`
	WHERE
		(
			(
				`keywords_ownership_b`.`ownership_expiry_time` BETWEEN now()
				AND (now() + INTERVAL 3 MONTH)
			)
			AND (
				`keywords_ownership_b`.`status` = 'sold'
			)
		)
	UNION
		SELECT
			`keywords_ownership_c`.`buyer_id` AS `buyer_id`,
			`keywords_ownership_c`.`keyword` AS `keyword`,
			`keywords_ownership_c`.`purchase_timestamp` AS `purchase_timestamp`,
			`keywords_ownership_c`.`ownership_expiry_time` AS `ownership_expiry_time`
		FROM
			`keywords_ownership_c`
		WHERE
			(
				(
					`keywords_ownership_c`.`ownership_expiry_time` BETWEEN now()
					AND (now() + INTERVAL 3 MONTH)
				)
				AND (
					`keywords_ownership_c`.`status` = 'sold'
				)
			)
		UNION
			SELECT
				`keywords_ownership_d`.`buyer_id` AS `buyer_id`,
				`keywords_ownership_d`.`keyword` AS `keyword`,
				`keywords_ownership_d`.`purchase_timestamp` AS `purchase_timestamp`,
				`keywords_ownership_d`.`ownership_expiry_time` AS `ownership_expiry_time`
			FROM
				`keywords_ownership_d`
			WHERE
				(
					(
						`keywords_ownership_d`.`ownership_expiry_time` BETWEEN now()
						AND (now() + INTERVAL 3 MONTH)
					)
					AND (
						`keywords_ownership_d`.`status` = 'sold'
					)
				)
			UNION
				SELECT
					`keywords_ownership_e`.`buyer_id` AS `buyer_id`,
					`keywords_ownership_e`.`keyword` AS `keyword`,
					`keywords_ownership_e`.`purchase_timestamp` AS `purchase_timestamp`,
					`keywords_ownership_e`.`ownership_expiry_time` AS `ownership_expiry_time`
				FROM
					`keywords_ownership_e`
				WHERE
					(
						(
							`keywords_ownership_e`.`ownership_expiry_time` BETWEEN now()
							AND (now() + INTERVAL 3 MONTH)
						)
						AND (
							`keywords_ownership_e`.`status` = 'sold'
						)
					)
				UNION
					SELECT
						`keywords_ownership_f`.`buyer_id` AS `buyer_id`,
						`keywords_ownership_f`.`keyword` AS `keyword`,
						`keywords_ownership_f`.`purchase_timestamp` AS `purchase_timestamp`,
						`keywords_ownership_f`.`ownership_expiry_time` AS `ownership_expiry_time`
					FROM
						`keywords_ownership_f`
					WHERE
						(
							(
								`keywords_ownership_f`.`ownership_expiry_time` BETWEEN now()
								AND (now() + INTERVAL 3 MONTH)
							)
							AND (
								`keywords_ownership_f`.`status` = 'sold'
							)
						)
					UNION
						SELECT
							`keywords_ownership_g`.`buyer_id` AS `buyer_id`,
							`keywords_ownership_g`.`keyword` AS `keyword`,
							`keywords_ownership_g`.`purchase_timestamp` AS `purchase_timestamp`,
							`keywords_ownership_g`.`ownership_expiry_time` AS `ownership_expiry_time`
						FROM
							`keywords_ownership_g`
						WHERE
							(
								(
									`keywords_ownership_g`.`ownership_expiry_time` BETWEEN now()
									AND (now() + INTERVAL 3 MONTH)
								)
								AND (
									`keywords_ownership_g`.`status` = 'sold'
								)
							)
						UNION
							SELECT
								`keywords_ownership_h`.`buyer_id` AS `buyer_id`,
								`keywords_ownership_h`.`keyword` AS `keyword`,
								`keywords_ownership_h`.`purchase_timestamp` AS `purchase_timestamp`,
								`keywords_ownership_h`.`ownership_expiry_time` AS `ownership_expiry_time`
							FROM
								`keywords_ownership_h`
							WHERE
								(
									(
										`keywords_ownership_h`.`ownership_expiry_time` BETWEEN now()
										AND (now() + INTERVAL 3 MONTH)
									)
									AND (
										`keywords_ownership_h`.`status` = 'sold'
									)
								)
							UNION
								SELECT
									`keywords_ownership_i`.`buyer_id` AS `buyer_id`,
									`keywords_ownership_i`.`keyword` AS `keyword`,
									`keywords_ownership_i`.`purchase_timestamp` AS `purchase_timestamp`,
									`keywords_ownership_i`.`ownership_expiry_time` AS `ownership_expiry_time`
								FROM
									`keywords_ownership_i`
								WHERE
									(
										(
											`keywords_ownership_i`.`ownership_expiry_time` BETWEEN now()
											AND (now() + INTERVAL 3 MONTH)
										)
										AND (
											`keywords_ownership_i`.`status` = 'sold'
										)
									)
								UNION
									SELECT
										`keywords_ownership_j`.`buyer_id` AS `buyer_id`,
										`keywords_ownership_j`.`keyword` AS `keyword`,
										`keywords_ownership_j`.`purchase_timestamp` AS `purchase_timestamp`,
										`keywords_ownership_j`.`ownership_expiry_time` AS `ownership_expiry_time`
									FROM
										`keywords_ownership_j`
									WHERE
										(
											(
												`keywords_ownership_j`.`ownership_expiry_time` BETWEEN now()
												AND (now() + INTERVAL 3 MONTH)
											)
											AND (
												`keywords_ownership_j`.`status` = 'sold'
											)
										)
									UNION
										SELECT
											`keywords_ownership_k`.`buyer_id` AS `buyer_id`,
											`keywords_ownership_k`.`keyword` AS `keyword`,
											`keywords_ownership_k`.`purchase_timestamp` AS `purchase_timestamp`,
											`keywords_ownership_k`.`ownership_expiry_time` AS `ownership_expiry_time`
										FROM
											`keywords_ownership_k`
										WHERE
											(
												(
													`keywords_ownership_k`.`ownership_expiry_time` BETWEEN now()
													AND (now() + INTERVAL 3 MONTH)
												)
												AND (
													`keywords_ownership_k`.`status` = 'sold'
												)
											)
										UNION
											SELECT
												`keywords_ownership_l`.`buyer_id` AS `buyer_id`,
												`keywords_ownership_l`.`keyword` AS `keyword`,
												`keywords_ownership_l`.`purchase_timestamp` AS `purchase_timestamp`,
												`keywords_ownership_l`.`ownership_expiry_time` AS `ownership_expiry_time`
											FROM
												`keywords_ownership_l`
											WHERE
												(
													(
														`keywords_ownership_l`.`ownership_expiry_time` BETWEEN now()
														AND (now() + INTERVAL 3 MONTH)
													)
													AND (
														`keywords_ownership_l`.`status` = 'sold'
													)
												)
											UNION
												SELECT
													`keywords_ownership_m`.`buyer_id` AS `buyer_id`,
													`keywords_ownership_m`.`keyword` AS `keyword`,
													`keywords_ownership_m`.`purchase_timestamp` AS `purchase_timestamp`,
													`keywords_ownership_m`.`ownership_expiry_time` AS `ownership_expiry_time`
												FROM
													`keywords_ownership_m`
												WHERE
													(
														(
															`keywords_ownership_m`.`ownership_expiry_time` BETWEEN now()
															AND (now() + INTERVAL 3 MONTH)
														)
														AND (
															`keywords_ownership_m`.`status` = 'sold'
														)
													)
												UNION
													SELECT
														`keywords_ownership_n`.`buyer_id` AS `buyer_id`,
														`keywords_ownership_n`.`keyword` AS `keyword`,
														`keywords_ownership_n`.`purchase_timestamp` AS `purchase_timestamp`,
														`keywords_ownership_n`.`ownership_expiry_time` AS `ownership_expiry_time`
													FROM
														`keywords_ownership_n`
													WHERE
														(
															(
																`keywords_ownership_n`.`ownership_expiry_time` BETWEEN now()
																AND (now() + INTERVAL 3 MONTH)
															)
															AND (
																`keywords_ownership_n`.`status` = 'sold'
															)
														)
													UNION
														SELECT
															`keywords_ownership_o`.`buyer_id` AS `buyer_id`,
															`keywords_ownership_o`.`keyword` AS `keyword`,
															`keywords_ownership_o`.`purchase_timestamp` AS `purchase_timestamp`,
															`keywords_ownership_o`.`ownership_expiry_time` AS `ownership_expiry_time`
														FROM
															`keywords_ownership_o`
														WHERE
															(
																(
																	`keywords_ownership_o`.`ownership_expiry_time` BETWEEN now()
																	AND (now() + INTERVAL 3 MONTH)
																)
																AND (
																	`keywords_ownership_o`.`status` = 'sold'
																)
															)
														UNION
															SELECT
																`keywords_ownership_p`.`buyer_id` AS `buyer_id`,
																`keywords_ownership_p`.`keyword` AS `keyword`,
																`keywords_ownership_p`.`purchase_timestamp` AS `purchase_timestamp`,
																`keywords_ownership_p`.`ownership_expiry_time` AS `ownership_expiry_time`
															FROM
																`keywords_ownership_p`
															WHERE
																(
																	(
																		`keywords_ownership_p`.`ownership_expiry_time` BETWEEN now()
																		AND (now() + INTERVAL 3 MONTH)
																	)
																	AND (
																		`keywords_ownership_p`.`status` = 'sold'
																	)
																)
															UNION
																SELECT
																	`keywords_ownership_q`.`buyer_id` AS `buyer_id`,
																	`keywords_ownership_q`.`keyword` AS `keyword`,
																	`keywords_ownership_q`.`purchase_timestamp` AS `purchase_timestamp`,
																	`keywords_ownership_q`.`ownership_expiry_time` AS `ownership_expiry_time`
																FROM
																	`keywords_ownership_q`
																WHERE
																	(
																		(
																			`keywords_ownership_q`.`ownership_expiry_time` BETWEEN now()
																			AND (now() + INTERVAL 3 MONTH)
																		)
																		AND (
																			`keywords_ownership_q`.`status` = 'sold'
																		)
																	)
																UNION
																	SELECT
																		`keywords_ownership_r`.`buyer_id` AS `buyer_id`,
																		`keywords_ownership_r`.`keyword` AS `keyword`,
																		`keywords_ownership_r`.`purchase_timestamp` AS `purchase_timestamp`,
																		`keywords_ownership_r`.`ownership_expiry_time` AS `ownership_expiry_time`
																	FROM
																		`keywords_ownership_r`
																	WHERE
																		(
																			(
																				`keywords_ownership_r`.`ownership_expiry_time` BETWEEN now()
																				AND (now() + INTERVAL 3 MONTH)
																			)
																			AND (
																				`keywords_ownership_r`.`status` = 'sold'
																			)
																		)
																	UNION
																		SELECT
																			`keywords_ownership_s`.`buyer_id` AS `buyer_id`,
																			`keywords_ownership_s`.`keyword` AS `keyword`,
																			`keywords_ownership_s`.`purchase_timestamp` AS `purchase_timestamp`,
																			`keywords_ownership_s`.`ownership_expiry_time` AS `ownership_expiry_time`
																		FROM
																			`keywords_ownership_s`
																		WHERE
																			(
																				(
																					`keywords_ownership_s`.`ownership_expiry_time` BETWEEN now()
																					AND (now() + INTERVAL 3 MONTH)
																				)
																				AND (
																					`keywords_ownership_s`.`status` = 'sold'
																				)
																			)
																		UNION
																			SELECT
																				`keywords_ownership_t`.`buyer_id` AS `buyer_id`,
																				`keywords_ownership_t`.`keyword` AS `keyword`,
																				`keywords_ownership_t`.`purchase_timestamp` AS `purchase_timestamp`,
																				`keywords_ownership_t`.`ownership_expiry_time` AS `ownership_expiry_time`
																			FROM
																				`keywords_ownership_t`
																			WHERE
																				(
																					(
																						`keywords_ownership_t`.`ownership_expiry_time` BETWEEN now()
																						AND (now() + INTERVAL 3 MONTH)
																					)
																					AND (
																						`keywords_ownership_t`.`status` = 'sold'
																					)
																				)
																			UNION
																				SELECT
																					`keywords_ownership_u`.`buyer_id` AS `buyer_id`,
																					`keywords_ownership_u`.`keyword` AS `keyword`,
																					`keywords_ownership_u`.`purchase_timestamp` AS `purchase_timestamp`,
																					`keywords_ownership_u`.`ownership_expiry_time` AS `ownership_expiry_time`
																				FROM
																					`keywords_ownership_u`
																				WHERE
																					(
																						(
																							`keywords_ownership_u`.`ownership_expiry_time` BETWEEN now()
																							AND (now() + INTERVAL 3 MONTH)
																						)
																						AND (
																							`keywords_ownership_u`.`status` = 'sold'
																						)
																					)
																				UNION
																					SELECT
																						`keywords_ownership_v`.`buyer_id` AS `buyer_id`,
																						`keywords_ownership_v`.`keyword` AS `keyword`,
																						`keywords_ownership_v`.`purchase_timestamp` AS `purchase_timestamp`,
																						`keywords_ownership_v`.`ownership_expiry_time` AS `ownership_expiry_time`
																					FROM
																						`keywords_ownership_v`
																					WHERE
																						(
																							(
																								`keywords_ownership_v`.`ownership_expiry_time` BETWEEN now()
																								AND (now() + INTERVAL 3 MONTH)
																							)
																							AND (
																								`keywords_ownership_v`.`status` = 'sold'
																							)
																						)
																					UNION
																						SELECT
																							`keywords_ownership_w`.`buyer_id` AS `buyer_id`,
																							`keywords_ownership_w`.`keyword` AS `keyword`,
																							`keywords_ownership_w`.`purchase_timestamp` AS `purchase_timestamp`,
																							`keywords_ownership_w`.`ownership_expiry_time` AS `ownership_expiry_time`
																						FROM
																							`keywords_ownership_w`
																						WHERE
																							(
																								(
																									`keywords_ownership_w`.`ownership_expiry_time` BETWEEN now()
																									AND (now() + INTERVAL 3 MONTH)
																								)
																								AND (
																									`keywords_ownership_w`.`status` = 'sold'
																								)
																							)
																						UNION
																							SELECT
																								`keywords_ownership_x`.`buyer_id` AS `buyer_id`,
																								`keywords_ownership_x`.`keyword` AS `keyword`,
																								`keywords_ownership_x`.`purchase_timestamp` AS `purchase_timestamp`,
																								`keywords_ownership_x`.`ownership_expiry_time` AS `ownership_expiry_time`
																							FROM
																								`keywords_ownership_x`
																							WHERE
																								(
																									(
																										`keywords_ownership_x`.`ownership_expiry_time` BETWEEN now()
																										AND (now() + INTERVAL 3 MONTH)
																									)
																									AND (
																										`keywords_ownership_x`.`status` = 'sold'
																									)
																								)
																							UNION
																								SELECT
																									`keywords_ownership_y`.`buyer_id` AS `buyer_id`,
																									`keywords_ownership_y`.`keyword` AS `keyword`,
																									`keywords_ownership_y`.`purchase_timestamp` AS `purchase_timestamp`,
																									`keywords_ownership_y`.`ownership_expiry_time` AS `ownership_expiry_time`
																								FROM
																									`keywords_ownership_y`
																								WHERE
																									(
																										(
																											`keywords_ownership_y`.`ownership_expiry_time` BETWEEN now()
																											AND (now() + INTERVAL 3 MONTH)
																										)
																										AND (
																											`keywords_ownership_y`.`status` = 'sold'
																										)
																									)
																								UNION
																									SELECT
																										`keywords_ownership_z`.`buyer_id` AS `buyer_id`,
																										`keywords_ownership_z`.`keyword` AS `keyword`,
																										`keywords_ownership_z`.`purchase_timestamp` AS `purchase_timestamp`,
																										`keywords_ownership_z`.`ownership_expiry_time` AS `ownership_expiry_time`
																									FROM
																										`keywords_ownership_z`
																									WHERE
																										(
																											(
																												`keywords_ownership_z`.`ownership_expiry_time` BETWEEN now()
																												AND (now() + INTERVAL 3 MONTH)
																											)
																											AND (
																												`keywords_ownership_z`.`status` = 'sold'
																											)
																										)
																									UNION
																										SELECT
																											`keywords_ownership_num`.`buyer_id` AS `buyer_id`,
																											`keywords_ownership_num`.`keyword` AS `keyword`,
																											`keywords_ownership_num`.`purchase_timestamp` AS `purchase_timestamp`,
																											`keywords_ownership_num`.`ownership_expiry_time` AS `ownership_expiry_time`
																										FROM
																											`keywords_ownership_num`
																										WHERE
																											(
																												(
																													`keywords_ownership_num`.`ownership_expiry_time` BETWEEN now()
																													AND (now() + INTERVAL 3 MONTH)
																												)
																												AND (
																													`keywords_ownership_num`.`status` = 'sold'
																												)
																											)
																										UNION
																											SELECT
																												`keywords_ownership_other`.`buyer_id` AS `buyer_id`,
																												`keywords_ownership_other`.`keyword` AS `keyword`,
																												`keywords_ownership_other`.`purchase_timestamp` AS `purchase_timestamp`,
																												`keywords_ownership_other`.`ownership_expiry_time` AS `ownership_expiry_time`
																											FROM
																												`keywords_ownership_other`
																											WHERE
																												(
																													(
																														`keywords_ownership_other`.`ownership_expiry_time` BETWEEN now()
																														AND (now() + INTERVAL 3 MONTH)
																													)
																													AND (
																														`keywords_ownership_other`.`status` = 'sold'
																													)
																												)


--final query

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	view_keyword_due_for_renewal
WHERE
	ownership_expiry_time BETWEEN now()
AND now() + INTERVAL 3 month













/********************expired Keywords ***/
	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_a
WHERE
	ownership_expiry_time < NOW() and status="sold"  UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_b
WHERE
	ownership_expiry_time < NOW() and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_c
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_d
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_e
WHERE
	ownership_expiry_time < NOW() and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_f
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_g
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_h
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_i
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_j
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_k
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_l
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_m
WHERE
	ownership_expiry_time < NOW() and status="sold"  UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_n
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_o
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_p
WHERE
	ownership_expiry_time < NOW() and status="sold"  UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_q
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_r
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_s
WHERE
	ownership_expiry_time < NOW() and status="sold"  UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_t
WHERE
	ownership_expiry_time < NOW() and status="sold"  UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_u
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_v
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_w
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_x
WHERE
	ownership_expiry_time < NOW() and status="sold"

UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_y
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_z
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_num
WHERE
	ownership_expiry_time < NOW() and status="sold"
UNION

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	keywords_ownership_other
WHERE
	ownership_expiry_time < NOW() and status="sold"




/*****************view expired keyword **********************************/

SELECT
	`keywords_ownership_a`.`buyer_id` AS `buyer_id`,
	`keywords_ownership_a`.`keyword` AS `keyword`,
	`keywords_ownership_a`.`purchase_timestamp` AS `purchase_timestamp`,
	`keywords_ownership_a`.`ownership_expiry_time` AS `ownership_expiry_time`
FROM
	`keywords_ownership_a`
WHERE
	(
		(
			`keywords_ownership_a`.`ownership_expiry_time` < now()
		)
		AND (
			`keywords_ownership_a`.`status` = 'sold'
		)
	)
UNION
	SELECT
		`keywords_ownership_b`.`buyer_id` AS `buyer_id`,
		`keywords_ownership_b`.`keyword` AS `keyword`,
		`keywords_ownership_b`.`purchase_timestamp` AS `purchase_timestamp`,
		`keywords_ownership_b`.`ownership_expiry_time` AS `ownership_expiry_time`
	FROM
		`keywords_ownership_b`
	WHERE
		(
			(
				`keywords_ownership_b`.`ownership_expiry_time` < now()
			)
			AND (
				`keywords_ownership_b`.`status` = 'sold'
			)
		)
	UNION
		SELECT
			`keywords_ownership_c`.`buyer_id` AS `buyer_id`,
			`keywords_ownership_c`.`keyword` AS `keyword`,
			`keywords_ownership_c`.`purchase_timestamp` AS `purchase_timestamp`,
			`keywords_ownership_c`.`ownership_expiry_time` AS `ownership_expiry_time`
		FROM
			`keywords_ownership_c`
		WHERE
			(
				(
					`keywords_ownership_c`.`ownership_expiry_time` < now()
				)
				AND (
					`keywords_ownership_c`.`status` = 'sold'
				)
			)
		UNION
			SELECT
				`keywords_ownership_d`.`buyer_id` AS `buyer_id`,
				`keywords_ownership_d`.`keyword` AS `keyword`,
				`keywords_ownership_d`.`purchase_timestamp` AS `purchase_timestamp`,
				`keywords_ownership_d`.`ownership_expiry_time` AS `ownership_expiry_time`
			FROM
				`keywords_ownership_d`
			WHERE
				(
					(
						`keywords_ownership_d`.`ownership_expiry_time` < now()
					)
					AND (
						`keywords_ownership_d`.`status` = 'sold'
					)
				)
			UNION
				SELECT
					`keywords_ownership_e`.`buyer_id` AS `buyer_id`,
					`keywords_ownership_e`.`keyword` AS `keyword`,
					`keywords_ownership_e`.`purchase_timestamp` AS `purchase_timestamp`,
					`keywords_ownership_e`.`ownership_expiry_time` AS `ownership_expiry_time`
				FROM
					`keywords_ownership_e`
				WHERE
					(
						(
							`keywords_ownership_e`.`ownership_expiry_time` < now()
						)
						AND (
							`keywords_ownership_e`.`status` = 'sold'
						)
					)
				UNION
					SELECT
						`keywords_ownership_f`.`buyer_id` AS `buyer_id`,
						`keywords_ownership_f`.`keyword` AS `keyword`,
						`keywords_ownership_f`.`purchase_timestamp` AS `purchase_timestamp`,
						`keywords_ownership_f`.`ownership_expiry_time` AS `ownership_expiry_time`
					FROM
						`keywords_ownership_f`
					WHERE
						(
							(
								`keywords_ownership_f`.`ownership_expiry_time` < now()
							)
							AND (
								`keywords_ownership_f`.`status` = 'sold'
							)
						)
					UNION
						SELECT
							`keywords_ownership_g`.`buyer_id` AS `buyer_id`,
							`keywords_ownership_g`.`keyword` AS `keyword`,
							`keywords_ownership_g`.`purchase_timestamp` AS `purchase_timestamp`,
							`keywords_ownership_g`.`ownership_expiry_time` AS `ownership_expiry_time`
						FROM
							`keywords_ownership_g`
						WHERE
							(
								(
									`keywords_ownership_g`.`ownership_expiry_time` < now()
								)
								AND (
									`keywords_ownership_g`.`status` = 'sold'
								)
							)
						UNION
							SELECT
								`keywords_ownership_h`.`buyer_id` AS `buyer_id`,
								`keywords_ownership_h`.`keyword` AS `keyword`,
								`keywords_ownership_h`.`purchase_timestamp` AS `purchase_timestamp`,
								`keywords_ownership_h`.`ownership_expiry_time` AS `ownership_expiry_time`
							FROM
								`keywords_ownership_h`
							WHERE
								(
									(
										`keywords_ownership_h`.`ownership_expiry_time` < now()
									)
									AND (
										`keywords_ownership_h`.`status` = 'sold'
									)
								)
							UNION
								SELECT
									`keywords_ownership_i`.`buyer_id` AS `buyer_id`,
									`keywords_ownership_i`.`keyword` AS `keyword`,
									`keywords_ownership_i`.`purchase_timestamp` AS `purchase_timestamp`,
									`keywords_ownership_i`.`ownership_expiry_time` AS `ownership_expiry_time`
								FROM
									`keywords_ownership_i`
								WHERE
									(
										(
											`keywords_ownership_i`.`ownership_expiry_time` < now()
										)
										AND (
											`keywords_ownership_i`.`status` = 'sold'
										)
									)
								UNION
									SELECT
										`keywords_ownership_j`.`buyer_id` AS `buyer_id`,
										`keywords_ownership_j`.`keyword` AS `keyword`,
										`keywords_ownership_j`.`purchase_timestamp` AS `purchase_timestamp`,
										`keywords_ownership_j`.`ownership_expiry_time` AS `ownership_expiry_time`
									FROM
										`keywords_ownership_j`
									WHERE
										(
											(
												`keywords_ownership_j`.`ownership_expiry_time` < now()
											)
											AND (
												`keywords_ownership_j`.`status` = 'sold'
											)
										)
									UNION
										SELECT
											`keywords_ownership_k`.`buyer_id` AS `buyer_id`,
											`keywords_ownership_k`.`keyword` AS `keyword`,
											`keywords_ownership_k`.`purchase_timestamp` AS `purchase_timestamp`,
											`keywords_ownership_k`.`ownership_expiry_time` AS `ownership_expiry_time`
										FROM
											`keywords_ownership_k`
										WHERE
											(
												(
													`keywords_ownership_k`.`ownership_expiry_time` < now()
												)
												AND (
													`keywords_ownership_k`.`status` = 'sold'
												)
											)
										UNION
											SELECT
												`keywords_ownership_l`.`buyer_id` AS `buyer_id`,
												`keywords_ownership_l`.`keyword` AS `keyword`,
												`keywords_ownership_l`.`purchase_timestamp` AS `purchase_timestamp`,
												`keywords_ownership_l`.`ownership_expiry_time` AS `ownership_expiry_time`
											FROM
												`keywords_ownership_l`
											WHERE
												(
													(
														`keywords_ownership_l`.`ownership_expiry_time` < now()
													)
													AND (
														`keywords_ownership_l`.`status` = 'sold'
													)
												)
											UNION
												SELECT
													`keywords_ownership_m`.`buyer_id` AS `buyer_id`,
													`keywords_ownership_m`.`keyword` AS `keyword`,
													`keywords_ownership_m`.`purchase_timestamp` AS `purchase_timestamp`,
													`keywords_ownership_m`.`ownership_expiry_time` AS `ownership_expiry_time`
												FROM
													`keywords_ownership_m`
												WHERE
													(
														(
															`keywords_ownership_m`.`ownership_expiry_time` < now()
														)
														AND (
															`keywords_ownership_m`.`status` = 'sold'
														)
													)
												UNION
													SELECT
														`keywords_ownership_n`.`buyer_id` AS `buyer_id`,
														`keywords_ownership_n`.`keyword` AS `keyword`,
														`keywords_ownership_n`.`purchase_timestamp` AS `purchase_timestamp`,
														`keywords_ownership_n`.`ownership_expiry_time` AS `ownership_expiry_time`
													FROM
														`keywords_ownership_n`
													WHERE
														(
															(
																`keywords_ownership_n`.`ownership_expiry_time` < now()
															)
															AND (
																`keywords_ownership_n`.`status` = 'sold'
															)
														)
													UNION
														SELECT
															`keywords_ownership_o`.`buyer_id` AS `buyer_id`,
															`keywords_ownership_o`.`keyword` AS `keyword`,
															`keywords_ownership_o`.`purchase_timestamp` AS `purchase_timestamp`,
															`keywords_ownership_o`.`ownership_expiry_time` AS `ownership_expiry_time`
														FROM
															`keywords_ownership_o`
														WHERE
															(
																(
																	`keywords_ownership_o`.`ownership_expiry_time` < now()
																)
																AND (
																	`keywords_ownership_o`.`status` = 'sold'
																)
															)
														UNION
															SELECT
																`keywords_ownership_p`.`buyer_id` AS `buyer_id`,
																`keywords_ownership_p`.`keyword` AS `keyword`,
																`keywords_ownership_p`.`purchase_timestamp` AS `purchase_timestamp`,
																`keywords_ownership_p`.`ownership_expiry_time` AS `ownership_expiry_time`
															FROM
																`keywords_ownership_p`
															WHERE
																(
																	(
																		`keywords_ownership_p`.`ownership_expiry_time` < now()
																	)
																	AND (
																		`keywords_ownership_p`.`status` = 'sold'
																	)
																)
															UNION
																SELECT
																	`keywords_ownership_q`.`buyer_id` AS `buyer_id`,
																	`keywords_ownership_q`.`keyword` AS `keyword`,
																	`keywords_ownership_q`.`purchase_timestamp` AS `purchase_timestamp`,
																	`keywords_ownership_q`.`ownership_expiry_time` AS `ownership_expiry_time`
																FROM
																	`keywords_ownership_q`
																WHERE
																	(
																		(
																			`keywords_ownership_q`.`ownership_expiry_time` < now()
																		)
																		AND (
																			`keywords_ownership_q`.`status` = 'sold'
																		)
																	)
																UNION
																	SELECT
																		`keywords_ownership_r`.`buyer_id` AS `buyer_id`,
																		`keywords_ownership_r`.`keyword` AS `keyword`,
																		`keywords_ownership_r`.`purchase_timestamp` AS `purchase_timestamp`,
																		`keywords_ownership_r`.`ownership_expiry_time` AS `ownership_expiry_time`
																	FROM
																		`keywords_ownership_r`
																	WHERE
																		(
																			(
																				`keywords_ownership_r`.`ownership_expiry_time` < now()
																			)
																			AND (
																				`keywords_ownership_r`.`status` = 'sold'
																			)
																		)
																	UNION
																		SELECT
																			`keywords_ownership_s`.`buyer_id` AS `buyer_id`,
																			`keywords_ownership_s`.`keyword` AS `keyword`,
																			`keywords_ownership_s`.`purchase_timestamp` AS `purchase_timestamp`,
																			`keywords_ownership_s`.`ownership_expiry_time` AS `ownership_expiry_time`
																		FROM
																			`keywords_ownership_s`
																		WHERE
																			(
																				(
																					`keywords_ownership_s`.`ownership_expiry_time` < now()
																				)
																				AND (
																					`keywords_ownership_s`.`status` = 'sold'
																				)
																			)
																		UNION
																			SELECT
																				`keywords_ownership_t`.`buyer_id` AS `buyer_id`,
																				`keywords_ownership_t`.`keyword` AS `keyword`,
																				`keywords_ownership_t`.`purchase_timestamp` AS `purchase_timestamp`,
																				`keywords_ownership_t`.`ownership_expiry_time` AS `ownership_expiry_time`
																			FROM
																				`keywords_ownership_t`
																			WHERE
																				(
																					(
																						`keywords_ownership_t`.`ownership_expiry_time` < now()
																					)
																					AND (
																						`keywords_ownership_t`.`status` = 'sold'
																					)
																				)
																			UNION
																				SELECT
																					`keywords_ownership_u`.`buyer_id` AS `buyer_id`,
																					`keywords_ownership_u`.`keyword` AS `keyword`,
																					`keywords_ownership_u`.`purchase_timestamp` AS `purchase_timestamp`,
																					`keywords_ownership_u`.`ownership_expiry_time` AS `ownership_expiry_time`
																				FROM
																					`keywords_ownership_u`
																				WHERE
																					(
																						(
																							`keywords_ownership_u`.`ownership_expiry_time` < now()
																						)
																						AND (
																							`keywords_ownership_u`.`status` = 'sold'
																						)
																					)
																				UNION
																					SELECT
																						`keywords_ownership_v`.`buyer_id` AS `buyer_id`,
																						`keywords_ownership_v`.`keyword` AS `keyword`,
																						`keywords_ownership_v`.`purchase_timestamp` AS `purchase_timestamp`,
																						`keywords_ownership_v`.`ownership_expiry_time` AS `ownership_expiry_time`
																					FROM
																						`keywords_ownership_v`
																					WHERE
																						(
																							(
																								`keywords_ownership_v`.`ownership_expiry_time` < now()
																							)
																							AND (
																								`keywords_ownership_v`.`status` = 'sold'
																							)
																						)
																					UNION
																						SELECT
																							`keywords_ownership_w`.`buyer_id` AS `buyer_id`,
																							`keywords_ownership_w`.`keyword` AS `keyword`,
																							`keywords_ownership_w`.`purchase_timestamp` AS `purchase_timestamp`,
																							`keywords_ownership_w`.`ownership_expiry_time` AS `ownership_expiry_time`
																						FROM
																							`keywords_ownership_w`
																						WHERE
																							(
																								(
																									`keywords_ownership_w`.`ownership_expiry_time` < now()
																								)
																								AND (
																									`keywords_ownership_w`.`status` = 'sold'
																								)
																							)
																						UNION
																							SELECT
																								`keywords_ownership_x`.`buyer_id` AS `buyer_id`,
																								`keywords_ownership_x`.`keyword` AS `keyword`,
																								`keywords_ownership_x`.`purchase_timestamp` AS `purchase_timestamp`,
																								`keywords_ownership_x`.`ownership_expiry_time` AS `ownership_expiry_time`
																							FROM
																								`keywords_ownership_x`
																							WHERE
																								(
																									(
																										`keywords_ownership_x`.`ownership_expiry_time` < now()
																									)
																									AND (
																										`keywords_ownership_x`.`status` = 'sold'
																									)
																								)
																							UNION
																								SELECT
																									`keywords_ownership_y`.`buyer_id` AS `buyer_id`,
																									`keywords_ownership_y`.`keyword` AS `keyword`,
																									`keywords_ownership_y`.`purchase_timestamp` AS `purchase_timestamp`,
																									`keywords_ownership_y`.`ownership_expiry_time` AS `ownership_expiry_time`
																								FROM
																									`keywords_ownership_y`
																								WHERE
																									(
																										(
																											`keywords_ownership_y`.`ownership_expiry_time` < now()
																										)
																										AND (
																											`keywords_ownership_y`.`status` = 'sold'
																										)
																									)
																								UNION
																									SELECT
																										`keywords_ownership_z`.`buyer_id` AS `buyer_id`,
																										`keywords_ownership_z`.`keyword` AS `keyword`,
																										`keywords_ownership_z`.`purchase_timestamp` AS `purchase_timestamp`,
																										`keywords_ownership_z`.`ownership_expiry_time` AS `ownership_expiry_time`
																									FROM
																										`keywords_ownership_z`
																									WHERE
																										(
																											(
																												`keywords_ownership_z`.`ownership_expiry_time` < now()
																											)
																											AND (
																												`keywords_ownership_z`.`status` = 'sold'
																											)
																										)
																									UNION
																										SELECT
																											`keywords_ownership_num`.`buyer_id` AS `buyer_id`,
																											`keywords_ownership_num`.`keyword` AS `keyword`,
																											`keywords_ownership_num`.`purchase_timestamp` AS `purchase_timestamp`,
																											`keywords_ownership_num`.`ownership_expiry_time` AS `ownership_expiry_time`
																										FROM
																											`keywords_ownership_num`
																										WHERE
																											(
																												(
																													`keywords_ownership_num`.`ownership_expiry_time` < now()
																												)
																												AND (
																													`keywords_ownership_num`.`status` = 'sold'
																												)
																											)
																										UNION
																											SELECT
																												`keywords_ownership_other`.`buyer_id` AS `buyer_id`,
																												`keywords_ownership_other`.`keyword` AS `keyword`,
																												`keywords_ownership_other`.`purchase_timestamp` AS `purchase_timestamp`,
																												`keywords_ownership_other`.`ownership_expiry_time` AS `ownership_expiry_time`
																											FROM
																												`keywords_ownership_other`
																											WHERE
																												(
																													(
																														`keywords_ownership_other`.`ownership_expiry_time` < now()
																													)
																													AND (
																														`keywords_ownership_other`.`status` = 'sold'
																													)
																												)





/***********************************************************************************************/

--final query expired

	SELECT
	buyer_id,
	keyword,
	purchase_timestamp,
	ownership_expiry_time
FROM
	view_expired_keywords
WHERE
ownership_expiry_time < NOW()