<?php

/*
 * Function	    : 	getKeywordAverageEarnersData()
 * Purpose	    : 	To retrieve keyword average earners data .
 * Arguments	: 	(date)$fromdate, (date)$todate, (int)$page_position, (int)$item_per_page, (string)$connKeywords.
 * */
function getKeywordAverageEarnersData($fromdate, $todate, $page_position, $item_per_page, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];

    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate);

    $query       = "select sum(total_interaction_count) as total_interaction_count, sum(total_interaction_earning) as total_interaction_earning, keyword from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    $query      .= "group by keyword ";
    $query      .= "LIMIT $page_position, $item_per_page";
    $result      = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 65, null, $extraArg);
    }
    return $returnArr;
}

/*
 * Function	    : 	getMonthWiseQuery()
 * Purpose	    : 	To retrieve month wise query details .
 * Arguments	: 	(date)$fromdate, (date)$todate.
 * */
function getMonthWiseQuery($fromdate, $todate)
{
    $fromexplode  = explode("-", $fromdate);
    $startyear    = $fromexplode[0];
    $startmonth   = $fromexplode[1];
    $startdate    = $fromexplode[2];

    $toexplode    = explode("-", $todate);
    $endyear      = $toexplode[0];
    $endmonth     = $toexplode[1];
    $enddate      = $toexplode[2];

    $queryBuild    = array();
    $startmonthnew = $startmonth;
    $count         = 0;
    for ($i = $startyear; $i <= $endyear; $i++) {
        for ($j = $startmonthnew; $j < 13; $j++) {

            $count++;
            if ($fromdate) {

                if (strlen($j) == 1) {
                    $j = "0" . $j;
                }
                if($count!=1)
                {
                    $query  = "UNION ALL";
                    $query  .= "  select  sum(total_interaction_count) AS total_interaction_count, sum(total_interaction_earning) AS total_interaction_earning, keyword from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" and type_qualification='qualified' ";
                }else{
                    $query  = " select  sum(total_interaction_count) AS total_interaction_count, sum(total_interaction_earning) AS total_interaction_earning, keyword from daily_keyword_earnings_{$j}_{$i} where tracking_date BETWEEN \"{$fromdate}\" and \"{$todate}\" and type_qualification='qualified' ";
                }

                //$query.="group by keyword,interaction_type";
                $query.="group by keyword";


                array_push($queryBuild,$query);
            }

            if ($j == 12) {
                $startmonthnew = 1;
                break;
            }

            if ($endyear == $i) {
                if ($j == $endmonth) {
                    break;
                }
            }
        }
    }
    return implode(" ", $queryBuild);
}

/*
 * Function	    : 	getKeywordAverageEarnersAnalytics()
 * Purpose	    : 	To retrieve keyword average earners analytics details .
 * Arguments	: 	(date)$fromdate, (date)$todate, (string)$connKeywords.
 * */


function getKeywordAverageEarnersAnalytics($fromdate, $todate, $connKeywords)
{
    $returnArr   = array();
    $extraArg    = array();

    $month       = date('m');
    $year        = date('Y');
    $fromexplode = explode("-", $fromdate);
    $fromyear    = $fromexplode[0];


    $toexplode   = explode("-", $todate);
    $toyear      = $toexplode[0];
    $queryBuild  = getMonthWiseQuery($fromdate, $todate);

    $query       = "select sum(total_interaction_count) as total_interaction_count, sum(total_interaction_earning) as total_interaction_earning from (";
    $query      .= $queryBuild;
    $query      .= " ) as t ";
    //$query      .= "group by keyword,interaction_type ";
    $query      .= "group by keyword ";

    $result = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $returnArr                                   = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr                                   = setErrorStack($returnArr, 60, null, $extraArg);
    }
    return $returnArr;
}



?>