<?php

function getAllAgentTracklistAnalytics($connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "SELECT admin_user.admin_email,count(my_tracklist.admin_id) as teacklist_added,
              CONCAT(admin_user.admin_first_name,' ',admin_user.admin_last_name) as agent_name
              FROM admin_user
              RIGHT JOIN my_tracklist ON admin_user.admin_id = my_tracklist.admin_id
              GROUP BY admin_email";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
           // array_push($res, $row);
        $res[]=$row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}

function getAllTracklistCategory($connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from category";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function getCategoryByID($categoryID, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from sub_categories where cat_id='" . $categoryID . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}


function getSubCategoryByID($categoryID, $SubcategoryID, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from sub_categories where cat_id='" . $categoryID . "' and sub_cat_id='" . $SubcategoryID . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}


function getSubjectByID($subjectToEdit, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from my_tracklist where subject_id='" . $subjectToEdit . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}


function getMainCategoryByID($categoryID, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from category where cat_id='" . $categoryID . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}

function getMainCategoryByName($categoryName, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from category where cat_name='" . $categoryName . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}

function addSubcategory($agentID, $categoryID, $subcategoryName, $updatedBy, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "INSERT INTO sub_categories (`admin_id`, `cat_id`, `sub_cat_name`, `created_on`, `updated_on`, `updated_by`) VALUES ($agentID,$categoryID,'" . $subcategoryName . "',NOW(),NOW(),'" . $updatedBy . "');";

    $result = runQuery($query, $connAdmin);


    if (noError($result)) {

        $errMsg = "New Sub Category Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 62, null, $extraArg);
    }
    return $returnArr;
}

function addCategory($categoryName, $updatedBy, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "INSERT INTO category (`cat_name`, `created_on`, `updated_on`, `updated_by`) VALUES ('" . $categoryName . "',NOW(),NOW(),'" . $updatedBy . "');";

    $result = runQuery($query, $connAdmin);


    if (noError($result)) {

        $errMsg = "New Category Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 62, null, $extraArg);
    }
    return $returnArr;
}


function addTracklist($categoryID, $subcategoryId, $agentID, $subject, $owner, $notes, $subjectType, $updatedBy, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "INSERT INTO my_tracklist (`cat_id`,`sub_cat_id`,`admin_id`,`subject`,`subject_owner`,`note`,`sub_types`,`created_on`,`updated_on`,`updated_by`) VALUES ($categoryID,$subcategoryId,$agentID,'" . $subject . "','" . $owner . "','" . $notes . "','" . $subjectType . "',NOW(),NOW(),'" . $updatedBy . "');";


    $result = runQuery($query, $connAdmin);


    if (noError($result)) {

        $errMsg = "User Added In Tracklist Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 63, null, $extraArg);
    }
    return $returnArr;
}


function getSubCategoryByName($subcategoryName, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from sub_categories where sub_cat_name='" . $subcategoryName . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}


function getTotalTracklistAddedInCategory($agentEmail, $category, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count  from (SELECT ct.cat_name,ad.admin_email FROM my_tracklist AS mt INNER JOIN category AS ct ON ct.cat_id = mt.cat_id INNER JOIN admin_user AS ad ON ad.admin_id = mt.admin_id) as data where admin_email='" . $agentEmail . "'   and cat_name='" . $category . "'  ";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getTotalTracklistAddedInSubCategory($agentEmail, $category, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count  from (SELECT sc.sub_cat_name,ad.admin_email FROM my_tracklist AS mt INNER JOIN sub_categories AS sc ON sc.sub_cat_id = mt.sub_cat_id INNER JOIN admin_user AS ad ON ad.admin_id = mt.admin_id) as data where admin_email='" . $agentEmail . "'   and sub_cat_name='" . $category . "'  ";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}

function getTotalTracklist($admin, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count from  my_tracklist where admin_id='" . $admin . "'";

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getTotalTracklistCount($conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count from  my_tracklist";

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}

function getTotalTracklistByCatIDAdminID($admin, $catid, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count from  my_tracklist where admin_id='" . $admin . "' and cat_id='" . $catid . "'";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}

function getTotalSubFolders($admin, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query = "SELECT count(*) as count from  sub_categories where admin_id='" . $admin . "'";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $datacnt = $res[0]["count"];
        if ($datacnt == "") {
            $datacnt = 0;
        }
        $returnArr = setErrorStack($returnArr, -1, $datacnt, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function checkTracklistEntryExistance($categoryID, $SubcategoryID, $Subject, $SubjectOwner, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from my_tracklist where subject='" . $Subject . "' and cat_id='" . $categoryID . "' and sub_cat_id='" . $SubcategoryID . "' and subject_owner='" . $SubjectOwner . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}

function checkTracklistEntryExistanceAll($categoryID, $Subcat_name, $Subject, $SubjectOwner, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "SELECT * from (SELECT mt.subject,mt.subject_id, mt.cat_id,mt.sub_cat_id,mt.note,mt.subject_owner, mt.created_on,mt.updated_on,mt.updated_by,sc.sub_cat_name, ct.cat_name,ad.admin_email,ad.admin_id FROM my_tracklist AS mt INNER JOIN sub_categories AS sc ON sc.sub_cat_id = mt.sub_cat_id INNER JOIN category AS ct ON ct.cat_id = mt.cat_id INNER JOIN admin_user AS ad ON ad.admin_id = mt.admin_id GROUP BY mt.subject_id) as data WHERE cat_id='" . $categoryID . "' and sub_cat_name='" . $Subcat_name . "' and subject='" . $Subject . "'  and subject_owner='" . $SubjectOwner . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;
}


function deleteTracklist($admin_id, $catid, $subcatid, $subject, $subowner, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `my_tracklist` WHERE admin_id='" . $admin_id . "' and cat_id='" . $catid . "' and sub_cat_id='" . $subcatid . "' and subject='" . $subject . "' and subject_owner='" . $subowner . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Tracklist Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteTracklistbyCatID($catid, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `my_tracklist` WHERE cat_id='" . $catid . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Tracklist Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteSubCategoryBulk($adminid, $subcatID, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `sub_categories` WHERE admin_id='" . $adminid . "' and sub_cat_id='" . $subcatID . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Subcategory Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}


function deleteSubCategoryByCatID($catID, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `sub_categories` WHERE cat_id='" . $catID . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Subcategory Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteCategoryByCatIDUpdatedBY($catID, $adminEmail, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `category` WHERE cat_id='" . $catID . "' and updated_by='" . $adminEmail . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Category Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}


function deleteTracklistbySubcategory($admin_id, $subcatid, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `my_tracklist` WHERE admin_id='" . $admin_id . "' and sub_cat_id='" . $subcatid . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Tracklist Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}


function getCategoryByIDAdminID($categoryID, $agent_id, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from sub_categories where cat_id='" . $categoryID . "' and admin_id='" . $agent_id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}

function deleteTracklistBulk($adminid, $Subjectid, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `my_tracklist` WHERE admin_id='" . $adminid . "' and subject_id='" . $Subjectid . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Tracklist Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function updateTracklist($subjectToEdit, $categoryID, $subcatidnew, $notenew, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE my_tracklist SET cat_id='" . $categoryID . "',sub_cat_id='" . $subcatidnew . "',note='" . $notenew . "' WHERE subject_id='" . $subjectToEdit . "' ;";

    $result = runQuery($query, $connAdmin);


    if (noError($result)) {
        $errMsg = "Tracklist Edited Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}

function getCategoryIDbyName($categoryName, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select cat_id from category where cat_name='" . $categoryName . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 61, null, $extraArg);
    }
    return $returnArr;

}


function updateNote($categoryID, $SubcategoryID, $subject, $subowner, $notenew, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE my_tracklist SET note='" . $notenew . "' WHERE cat_id='" . $categoryID . "' and sub_cat_id='" . $SubcategoryID . "' and subject='" . $subject . "' and subject_owner='" . $subowner . "'";
    $result = runQuery($query, $connAdmin);


    if (noError($result)) {
        $errMsg = "Tracklist Edited Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}