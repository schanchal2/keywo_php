<?php

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateServiceRequestList()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateServiceRequestList()
*   Purpose       :   This function will insert/update service_request table and store queryInfo
*                 :   $queryList will bring json format data
*   Arguments     :   (json) $queryList, (object)$conn
*/

function updateServiceRequestList($tickets,$requestList,$sessionEmail,$queryList,$kycDoc,$kycDocDate,$conn)
{
    $returnArr=array();
    $extraArg=array();
    $srID = getNextOrderNumber($conn);
    $tableName   = "service_request";
    $requestDate = date('Y-m-d h:i:s');

    if($tickets!="") {
        $ticket = implode("','", $tickets);
    }else
    {
        $ticket="";
    }

    $requestStatus = "";
    $escalateRequest = "";
    $escalateRequest = $requestList["escalate"];
    $requestData = getRequestStatus($ticket,$escalateRequest,$conn); 
    if(noError($requestData))
    {
        if(isset($requestData["errMsg"]) || !empty($requestData["errMsg"]))
        {
            $errMsg          = $requestData["errMsg"];
            if(isset($errMsg["request_status"]) || !empty($errMsg["request_status"]))
            {
                $requestStatus   = $errMsg["request_status"];
            }
             $escalateRequests = "";
        }
        else
        {
            $errMsg = "";
            $requestStatus = "";
            $escalateRequests = "";
        }
    }

    $dueDate = date('Y-m-d H:s:i', strtotime('+2 days'));

    $getServiceRequestInfo = getServiceRequestInfo($conn, $ticket);

    if (noError($getServiceRequestInfo)) {
        if(count($getServiceRequestInfo["data"])>0) {

            $getServiceRequestInfo = $getServiceRequestInfo["data"][0];
            $request_id = $getServiceRequestInfo["request_no"];
        }else{
            $request_id="";
        }
    }

    if(empty($request_id) || $request_id=="")
    {
       $query = "INSERT into " . $tableName . "(request_date,request_type,request_no,ticket_id,username,txn_id,duedate,agent_comment,hod_comment,mm_comment,ip_address,ip_domain,block_reason,raised_by,raised_by_department,kyc_doc,kyc_doc_date,hold_by,req_priority,request_status,request_status_by,escalate_request,escalate_by,comments) VALUES('$requestDate','".$queryList['callingPageName']."','$srID','".$queryList['kycTicketID']."','".$queryList['kycUsername']."','".$queryList['txnID']."','$dueDate','".$queryList['kycagentComment']."','','','".$queryList['ipaddress']."','".$queryList['domainName']."','".$queryList['reason']."','$sessionEmail','agent','$kycDoc','$kycDocDate','','".$queryList["reqPriority"]."','pending','','','','')";
    }

        else if($requestStatus == 'approved' || $requestStatus == 'rejected')
        {
            $returnArr = setErrorStack($returnArr, 71, null, $extraArg);    
            return $returnArr;
        }
        else if($escalateRequests == 'escalate_to_mm')
            {
                $returnArr = setErrorStack($returnArr, 71, null, $extraArg);    
                return $returnArr;
            }

        else {
        $query = "UPDATE ".$tableName."  ";

        if($requestList["status"]!="" || $requestList["reqStatusBy"]!="" || $requestList["escalate"]!="" || $requestList["escalateby"]!="" || $requestList["raisedBy"]!="" || $requestList["comments"]!="" || $requestList["hod_comments"]!="" || $requestList["mm_comments"]!="")
        {

        }
            if ($requestList["status"]!="") {
            $query .= "SET request_status='" . $requestList["status"] . "'";
           }
            if ($requestList["reqStatusBy"]!="") {
                $query .= ",request_status_by='" . $requestList["reqStatusBy"] . "'";
            }

            if ($requestList["escalate"]!="") {
                $query .= ",escalate_request='" . $requestList["escalate"] . "'";
            }

            if ($requestList["escalateby"]!="") {
                $query .= ",escalate_by='" . $requestList["escalateby"] . "'";
            }

            if ($requestList["raisedBy"]!="") {
                $query .= ",raised_by_department='" . $requestList["raisedBy"] . "'";
            }

            if ($requestList["comments"]!="") {
                $query .= ",comments='" . $requestList["comments"] . "'";
            }

            if ($requestList["hod_comments"]!="") {
                $query .= ",hod_comment='" . $requestList["hod_comments"] . "'";
            }

            if ($requestList["mm_comments"]!="") {
                $query .= ",mm_comment='" . $requestList["mm_comments"] . "'";
            }

            $query .= " where request_no IN ('" . $ticket . "')";

          //$query = "UPDATE ".$tableName." SET request_status='".$requestList["status"]."',request_status_by='".$requestList["reqStatusBy"]."',escalate_request='".$requestList["escalate"]."',escalate_by='".$requestList["escalateby"]."',raised_by_department='".$requestList["raisedBy"]."',comments='".$requestList["comments"]."',hod_comment='".$requestList["hod_comments"]."',mm_comment='".$requestList["mm_comments"]."' WHERE request_no IN ('" . $ticket . "')";
}
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $returnArr = setErrorStack($returnArr, -1, "Success", $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 70, null, $extraArg);
    }
    return $returnArr;
}

function selectmaxservicerequest($conn){

    $returnArr = array();

    $query = "SELECT MAX(id) as lastid FROM service_request";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}


function getNextOrderNumber($conn)
{
    $result=selectmaxservicerequest($conn);
    $lastOrder=$result["errMsg"]["lastid"];
    $lastOrder = $lastOrder+1;
    return 'SR-' .$lastOrder;
}

function getQueryListDetail($conn,$queryType) {

    $returnArr = array();

    $query = "SELECT ticket_id FROM support_requests where request_status <> 'closed' and request_type LIKE '%{$queryType}%'ORDER BY id DESC ";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"]  = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"]  = $result["errMsg"];

    }

    return $returnArr;
}

function getUserEmail($ticketid,$conn) {

    $returnArr = array();

    $query = "SELECT reg_email,transaction_hash FROM support_requests where ticket_id = '$ticketid'";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"]  = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"]  = $result["errMsg"];

    }

    return $returnArr;
}

function getAllRequestData($offset, $limit, $conn, $timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus,$agentIDS,$escalateBy)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from service_request";

    if($timestamp!="" || $srNo!="" || $srType!="" || $userName!="" || $raisedBy!="" || $priority!="" || $holdBy!="" || $type!="" || $requestStatus!="" || $agentIDS!="" || $escalateBy!="")
    {
        $query .= " where  1=1 ";
    }

    if ($timestamp!="") {
        $query .= " AND  request_date='" . $timestamp . "'";
    }
    if ($srType!="") {
        $query .= " AND  request_type='" . $srType . "'";
    }

    if ($userName!="") {
        $query .= " AND  username='" . $userName . "'";
    }

    if ($raisedBy!="") {
        $query .= " AND  raised_by='" . $raisedBy . "'";
    }

    if ($priority!="") {
        $query .= " AND  req_priority='" . $priority . "'";
    }

    if ($holdBy!="") {
        $query .= " AND  hold_by='" . $holdBy . "'";
    }

    if ($type!="") {
        $query .= " AND  raised_by_department='" . $type . "'";
    }

    if ($srNo!="") {
        $query .= " AND  request_no='" . $srNo . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  request_status='" . $requestStatus . "'";
    }

     if($agentIDS == 'agent'){
        $query .= " AND raised_by_department= '" . $agentIDS . "'";
    }

    if($agentIDS == 'escalate_to_mm'){
        $query .= " AND escalate_request= '" . $agentIDS . "'";
    }

    $query .= " ORDER by id DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

  // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllRequestCount($conn,$timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus,$agentIDS,$escalateBy)
{
    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from service_request";

    if($timestamp!="" || $srNo!="" || $srType!="" || $userName!="" || $raisedBy!="" || $priority!="" || $holdBy!="" || $type!="" || $requestStatus!="" || $agentIDS!="" || $escalateBy=="")
    {
        $query .= " where  1=1 ";
    }

    if ($timestamp!="") {
        $query .= " AND  request_date='" . $timestamp . "'";
    }
    if ($srType!="") {
        $query .= " AND  request_type='" . $srType . "'";
    }

    if ($userName!="") {
        $query .= " AND  username='" . $userName . "'";
    }

    if ($raisedBy!="") {
        $query .= " AND  raised_by='" . $raisedBy . "'";
    }

    if ($priority!="") {
        $query .= " AND  req_priority='" . $priority . "'";
    }

    if ($holdBy!="") {
        $query .= " AND  hold_by='" . $holdBy . "'";
    }

    if ($type!="") {
        $query .= " AND  raised_by_department='" . $type . "'";
    }

    if ($srNo!="") {
        $query .= " AND  request_no='" . $srNo . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  request_status='" . $requestStatus . "'";
    }

    if($agentIDS == 'agent'){
        $query .= " AND raised_by_department= '" . $agentIDS . "'";
    }

    if($agentIDS == 'escalate_to_mm'){
        $query .= " AND escalate_request= '" . $agentIDS . "'";
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getRequestAnalytics($email,$type,$conn){
   // printArr(func_get_args());
    $returnArr = array();

   $query="SELECT count(*) as total_count,count(case when request_status = 'approved' then request_status end) as total_resolved,count(case when request_status = 'pending' then request_status end) as total_open,count(case when request_status = 'rejected' then request_status end) as rejected from service_request where raised_by_department='{$type}' AND raised_by ='{$email}'";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 10;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}

function getRequestMMAnalytics($hold_by,$type,$conn){
    $returnArr = array();

    $query="SELECT count(*) as total_count,count(case when request_status = 'approved' then request_status end) as total_resolved,count(case when request_status = 'pending' then request_status end) as total_open,count(case when request_status = 'rejected' then request_status end) as rejected from service_request where escalate_request ='{$hold_by}' OR request_status_by='{$type}';";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 10;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}

function getServiceRequestInfo($conn, $requestNo) {

    $returnArr    = array();
    $extraArgs    = array();

    $query  = "SELECT * FROM `service_request` WHERE request_no IN ('" . $requestNo . "')";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[] = $row;
        }

        $extraArgs["data"] = $res;
        $errMsg    =   $result["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    } else {

        $errMsg   =   $result["errMsg"];
        $errCode  =   $result["errCode"];
        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }

    return $returnArr;
}


function getAllmmsmRequestData($offset, $limit, $conn, $timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from service_request";

    if($timestamp!="" || $srNo!="" || $srType!="" || $userName!="" || $raisedBy!="" || $type!="" || $requestStatus!="" || $agentIDS!="" || $escalateBy!="")
    {
        $query .= " where  1=1 ";
    }

    if ($timestamp!="") {
        $query .= " AND  request_date='" . $timestamp . "'";
    }
    if ($srType!="") {
        $query .= " AND  request_type='" . $srType . "'";
    }

    if ($userName!="") {
        $query .= " AND  username='" . $userName . "'";
    }

    if ($srNo!="") {
        $query .= " AND  request_no='" . $srNo . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  request_status='" . $requestStatus . "'";
    }

    // if ($type!="" || $type=="") {
    //     $query .= " AND  escalate_request='" . $type . "'";
    // }

    if($agentIDS == 'hod'){
        $query .= " AND escalate_by= '" . $agentIDS . "'";
    }

    if($agentIDS == 'escalate_to_sm'){
        $query .= " AND escalate_request= '" . $agentIDS . "'";
    }

    $query .= " ORDER by id DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

   // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllmmsmRequestCount($conn,$timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from service_request";

    if($timestamp!="" || $srNo!="" || $srType!="" || $userName!="" || $raisedBy!="" || $type!="" || $requestStatus!="" || $agentIDS!="" || $escalateBy!="")
    {
        $query .= " where  1=1 ";
    }

    if ($timestamp!="") {
        $query .= " AND  request_date='" . $timestamp . "'";
    }
    if ($srType!="") {
        $query .= " AND  request_type='" . $srType . "'";
    }

    if ($userName!="") {
        $query .= " AND  username='" . $userName . "'";
    }

    if ($raisedBy!="") {
        $query .= " AND  raised_by='" . $raisedBy . "'";
    }

    if ($srNo!="") {
        $query .= " AND  request_no='" . $srNo . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  request_status='" . $requestStatus . "'";
    }

    // if ($type!="" || $type=="") {
    //     $query .= " AND  escalate_request='" . $type . "'";
    // }

    if($agentIDS == 'hod'){
        $query .= " AND escalate_by= '" . $agentIDS . "'";
    }

    if($agentIDS == 'escalate_to_sm'){
        $query .= " AND escalate_request= '" . $agentIDS . "'";
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function userLastServiceRequests($conn,$operation, $userEmail) {

    $returnArr    = array();
    $extraArgs    = array();
    if($operation=="count") {
        $query = "select count(*) as count from service_request where userEmail='{$userEmail}' ORDER BY request_date DESC limit 1000";
    }else{
        $query = "select request_no,request_date,request_type,username,raised_by,raised_by_department,request_status,request_status_by from service_request where username='{$userEmail}' ORDER BY request_date DESC limit 1000";

    }
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[] = $row;
        }

        $extraArgs["data"] = $res;
        $errMsg    =   $result["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    } else {

        $errMsg   =   $result["errMsg"];
        $errCode  =   $result["errCode"];
        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function getRequestStatus($ticket,$escalateRequest,$conn){

    $returnArr = array();

    $query = "SELECT request_status,escalate_request from service_request where request_no IN ('" . $ticket . "') AND escalate_request = '{$escalateRequest}'";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}
?>