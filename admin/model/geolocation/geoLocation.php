<?php


function getCountryCityandState($stateid, $countryid, $cityid, $connDemo)
{

    $returnArr = array();


    $query = "SELECT name from countries where country_id='" . $countryid . "';";
    $result = runQuery($query, $connDemo);
    $row = mysqli_fetch_assoc($result["dbResource"]);
    if ($row) {
        $returnArr1["country"] = $row["name"];
    }


    $query = "SELECT name,id from states where state_id='" . $stateid . "';";
    $result = runQuery($query, $connDemo);
    $row = mysqli_fetch_assoc($result["dbResource"]);
    if ($row) {
        $returnArr1["states"] = $row["name"] . "/" . $row["id"];
    }


    $query = "SELECT name,id from cities where id='" . $cityid . "';";
    $result = runQuery($query, $connDemo);
    $row = mysqli_fetch_assoc($result["dbResource"]);
    if ($row) {
        $returnArr1["cities"] = $row["name"] . "/" . $row["id"];
    }

    if (noError($result)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $returnArr1;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllCountry($connDemo)
{


    $returnArr = array();
    $query = "SELECT name,id FROM countries;";
    $result = runQuery($query, $connDemo);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            //array_push($res,$row["name"]);
            $res[] = $row;
        }

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}


function getAllStateinCountry($country, $connDemo)
{


    $returnArr = array();
    $query = "SELECT name,id FROM states where country_id='" . $country . "';";

    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            //array_push($res,$row["name"]);
            $res[] = $row;
        }

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}

function getAllCitiesinState($state, $connDemo)
{


    $returnArr = array();
    $query = "SELECT name,id FROM cities where state_id='" . $state . "';";

    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            //array_push($res,$row["name"]);
            $res[] = $row;
        }

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}

function getCountryCode($countryid, $connDemo)
{


    $returnArr = array();
    $query = "SELECT country_code_number from countries where country_id='" . $countryid . "';";
    $result = runQuery($query, $connDemo);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            //array_push($res,$row["name"]);
            $res[] = $row;
        }

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;
}