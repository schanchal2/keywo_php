<?php

function getUserLiveStatus($email)
{
    $result=getUserIDHandle($email)["errMsg"];
    $admin_id=$result["_id"];
     $handle=$result["account_handle"];

    global $walletPublicKey, $mode, $mongoServerSocial;
    $requestUrl = $mongoServerSocial;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "publicKey={$walletPublicKey}";
    $postFields =$apiText;
    $apiName = "admin/check/online/{$admin_id}/{$handle}";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function getAllServiceRequest($offset, $limit, $conn, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$escalation_status)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from support_requests";

    if($ticketId!="" || $tickedUserEmail!="" || $ticketAgent!="" || $country!="" || $ticketStatus!="" || $creationTime!="" || $escalation_status!="")
    {
        $query .= " where 1=1 ";
    }else
    {
        $query .= " where request_status <> 'closed' and hod_escalation_status <> 1";
    }

    if ($ticketId!="") {
        $query .= " AND  ticket_id='" . $ticketId . "'";
    }

    if ($tickedUserEmail!="") {
        $query .= " AND  reg_email='" . $tickedUserEmail . "'";
    }

    if ($ticketAgent!="") {
        $query .= " AND  assigned_to='" . $ticketAgent . "'";
    }

    if ($country!="") {
        $query .= " AND  user_country='" . $country . "'";
    }

    if ($ticketStatus!="") {
        $query .= " AND  request_status='" . $ticketStatus . "'";
    }else{
        $query .= " AND  request_status <> 'closed'";
    }

    if ($escalation_status!="") {
        $query .= " AND  hod_escalation_status='" . $escalation_status . "'";
    }else{
        $query .= " AND  hod_escalation_status <> 1";
    }



    if ($creationTime!="") {
        $time=explode("-",$creationTime);
        $query .= " AND  requestTime BETWEEN {$time[0]} and {$time[1]}";
    }

    $query .= " ORDER by requestTime DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllServiceRequestCount($conn, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$escalation_status)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT count(*) as count from support_requests";

    if($ticketId!="" || $tickedUserEmail!="" || $ticketAgent!="" || $country!="" || $ticketStatus!="" || $creationTime!="" || $escalation_status!="")
    {
        $query .= " where 1=1 ";
    }else
    {
        $query .= " where request_status <> 'closed' and hod_escalation_status <> 1";
    }

    if ($ticketId!="") {
        $query .= " AND  ticket_id='" . $ticketId . "'";
    }

    if ($tickedUserEmail!="") {
        $query .= " AND  reg_email='" . $tickedUserEmail . "'";
    }

    if ($ticketAgent!="") {
        $query .= " AND  assigned_to='" . $ticketAgent . "'";
    }

    if ($country!="") {
        $query .= " AND  user_country='" . $country . "'";
    }

    if ($ticketStatus!="") {
        $query .= " AND  request_status='" . $ticketStatus . "'";
    }else{
        $query .= " AND  request_status <> 'closed'";
    }

    if ($escalation_status!="") {
        $query .= " AND  hod_escalation_status='" . $escalation_status . "'";
    }else{
        $query .= " AND  hod_escalation_status <> 1";
    }

    if ($creationTime!="") {
        $time=explode("-",$creationTime);
        $query .= " AND  requestTime BETWEEN {$time[0]} and {$time[1]}";
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAgentServiceRequestCount($conn, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id)
{

    $returnArr = array();
    global $blanks;
    $escalationStatus=0;
    $query = "SELECT count(*) as count from support_requests";

    if($ticketId!="" || $tickedUserEmail!="" || $ticketAgent!="" || $country!="" || $ticketStatus!="" || $creationTime!="" || $agent_id!="" && $escalationStatus==0)
    {
        $query .= " where 1=1 ";
    }



    if ($agent_id!="") {
        $query .= " AND  assigned_agent_id={$agent_id}";
    }

    if ($ticketId!="") {
        $query .= " AND  ticket_id='" . $ticketId . "'";
    }

    if ($tickedUserEmail!="") {
        $query .= " AND  reg_email='" . $tickedUserEmail . "'";
    }

    if ($ticketAgent) {
        $query .= " AND  assigned_to='" . $ticketAgent . "'";
    }

    if ($country!="") {
        $query .= " AND  user_country='" . $country . "'";
    }

    if ($ticketStatus!="") {
        $query .= " AND  request_status='" . $ticketStatus . "'";
    }

    if ($escalationStatus==0) {
        $query .= " AND  hod_escalation_status={$escalationStatus}";
    }
    if ($creationTime!="") {
        $time=explode("-",$creationTime);
        $query .= " AND  requestTime BETWEEN {$time[0]} and {$time[1]}";
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getAgentServiceRequest($offset, $limit, $conn, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id)
{

    $returnArr = array();
    global $blanks;
    $escalationStatus=0;

    $query = "SELECT * from support_requests";

    if($ticketId!="" || $tickedUserEmail!="" || $ticketAgent!="" || $country!="" || $ticketStatus!="" || $creationTime!="" || $agent_id!="" && $escalationStatus==0)
    {
        $query .= " where 1=1 ";
    }



      if ($agent_id!="") {
          $query .= " AND  assigned_agent_id={$agent_id}";
      }

    if ($ticketId!="") {
        $query .= " AND  ticket_id='" . $ticketId . "'";
    }

    if ($tickedUserEmail!="") {
        $query .= " AND  reg_email='" . $tickedUserEmail . "'";
    }

    if ($ticketAgent) {
        $query .= " AND  assigned_to='" . $ticketAgent . "'";
    }

    if ($country!="") {
        $query .= " AND  user_country='" . $country . "'";
    }

    if ($ticketStatus!="") {
        $query .= " AND  request_status='" . $ticketStatus . "'";
    }

    if ($escalationStatus==0) {
        $query .= " AND  hod_escalation_status={$escalationStatus}";
    }
    if ($creationTime!="") {
        $time=explode("-",$creationTime);
        $query .= " AND  requestTime BETWEEN {$time[0]} and {$time[1]}";
    }

    $query .= " ORDER by requestTime DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function changeTicketStatus($ticketID,$status,$conn)
{

    $returnArr = array();
    $extraArg = array();

    if($status=="closed")
    {
     $time= ", closed_on = NOW()";
    }else{
        $time="";
    }
    $query = "UPDATE support_requests SET request_status='{$status}' {$time} WHERE ticket_id='" . $ticketID . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $errMsg = "Ticket status changed scuccessfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }

    return $returnArr;
}

function escalateTicketToHod($ticketID,$conn)
{
    global $hodGroupCS,$superAdminGroupCS;
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE support_requests SET hod_escalation_status=1 WHERE ticket_id='" . $ticketID . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $errMsg = "Ticket status changed scuccessfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }
    
    return $returnArr;
}


function getAgentTicketAnalytics($agent_ID,$conn)
{

    $returnArr = array();

    $query = "select count(*) as total_queries,count(case when request_status=\"new\" and hod_escalation_status=0 then request_status end ) as new,count(case when request_status=\"open\" and hod_escalation_status=0 then request_status end ) as open,count(case when request_status=\"pending\" and hod_escalation_status=0 then request_status end ) as pending,count(case when request_status=\"closed\" and hod_escalation_status=0 then request_status end ) as closed ,count(case when  hod_escalation_status=1 then hod_escalation_status end ) as escalated from support_requests where assigned_agent_id={$agent_ID}";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 10;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}


?>