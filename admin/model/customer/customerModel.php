<?php
//name to id convert apps
function updateCommentText($commentText, $emailID,$tickectId,$userType, $conn){
  global $hodGroupCS;
  $extraArg = array();
  $returnArr = array();
  $rowdata="";
  $msgValidator = false;
  $queryToCheck = "SELECT request_status as status, assigned_to as assigned, hod_escalation_status as escalation_status, support_msgs FROM support_requests WHERE ticket_id='" . $tickectId . "'";

  $resultCheck = runQuery($queryToCheck, $conn);
  if (noError($resultCheck)) {
    while ($row = mysqli_fetch_assoc($resultCheck["dbResource"])) {
      $rowdata         = isset($row['status'])?$row['status']:"";
      $rowdataUser     = isset($row['assigned'])?$row['assigned']:"";
      $rowMsg          = isset($row['support_msgs'])?$row['support_msgs']:"";
      $ticketEscStatus = isset($row['escalation_status'])?$row['escalation_status']:"";
    }
    if ($rowdata != 'closed') {
      if ($ticketEscStatus == 0 && $rowdataUser == $emailID) {
        $msgValidator = true;
      } else if ($ticketEscStatus == 1) {
        $connAdmin = createDBConnection("acl");
        noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

        $query1 = "SELECT count(*) as count from admin_user where group_id LIKE '%{$hodGroupCS}%' AND admin_email='".$emailID."'";

        $result1 = runQuery($query1, $connAdmin);
        if (noError($result1)) {
            while ($row1 = mysqli_fetch_assoc($result1["dbResource"])) {
              $rowdata1[]         = $row1;
            }
            $hodResultCount = 0;
            if (!empty($rowdata1)) {
              $hodResultCount = $rowdata1[0]['count'];
            }
            if ($hodResultCount > 0) {
              $msgValidator = true;
            }            
        } else {
            $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
            return $returnArr;
        }        
      }

      if ($msgValidator) {
        $msg = json_decode($rowMsg, true);
        $newMsg = array( 
            "id"               => (count($msg)+1),
            "msg_content"      => "{$commentText}",
            "msg_writer_email" => "{$emailID}",
            "user_type"        => "{$userType}",
            "msg_time"         => date('Y-m-d H:i:s'));
        if (!empty($msg)) {
            array_push( $msg, $newMsg);
        } else {
            $msg = array($newMsg);
        }
        $finalArray = json_encode($msg);                
        
        $query = "UPDATE support_requests SET support_msgs='" . $finalArray . "' WHERE ticket_id='" . $tickectId . "'";
        $result = runQuery($query, $conn);
        if (noError($result)) {
            $errMsg = "Message Added Successfully.";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
        }
      } else {
        $returnArr = setErrorStack($returnArr, 13, null, $extraArg);
      }
    } else {
      $returnArr = setErrorStack($returnArr, 12, null, $extraArg);
    }  
  } else {
    $returnArr = setErrorStack($returnArr, 11, null, $extraArg);
  }
  return $returnArr;
}

function setTicketTimerStart($tickectId, $conn) {
  $extraArg = array();
  $returnArr = array();
  $ticketRasiedon = date('Y-m-d h:i:s');
  $query = "UPDATE support_requests SET response_start= '" . $ticketRasiedon . "' WHERE ticket_id='".$tickectId."' AND response_start is NULL";
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $errMsg = "Start Time Added Successfully.";
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
  } else {
      $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
  }
  return $returnArr;
}

function getTicketTimerStart($tickectId, $conn) {
  $extraArg = array();
  $returnArr = array();
  $query = "SELECT response_start, closed_on   FROM support_requests WHERE ticket_id='".$tickectId."'";
  $result = runQuery($query, $conn);

  if (noError($result)) {
    while ($row = mysqli_fetch_assoc($result["dbResource"]))
      $res[] = $row;
      $errMsg =  $res;
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
  } else {
      $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
  }
  return $returnArr;
}

function getCommentText($tickectId, $conn, $lastmsgid=""){
  $extraArg        = array();
  $returnArr       = array();
  $res3            = array();
  $res             = array();
  $newMessageArray = array();
  $arrayFound      = false;
  $arrayStart      = false;
  if($lastmsgid    == ""){
      $lastmsgid  = 1;
      $arrayStart = true;
  }
  $query = "SELECT support_msgs FROM support_requests where ticket_id ='".$tickectId ."'";
  $result = runQuery($query, $conn);
  if (noError($result)) {        
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
          $res[] = $row;

      $res2 = json_decode($res[0]['support_msgs'], true);        
      
      foreach ($res2 as $key => $value) {
          if($arrayStart) {
              $arrayFound = true;
          }
          if ($arrayFound == false) {
              if ($value['id'] == $lastmsgid) {
                  $arrayFound = true;
                  continue;
              }  
          }
          if ($arrayFound) {
              array_push($newMessageArray, $res2[$key]);  
          }            
      }
      $res3[] = $newMessageArray;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res3;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }
  return $returnArr;
}


function getLastCount($email, $conn){
 $extraArg = array();
  $returnArr = array();
  $query = "SELECT max(id) FROM support_requests where reg_email = '".$email ."' " ;
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"]))
      $res = $row;
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }
  return $returnArr;

}



function getAllFAQs($faqSearch,$start,$limit,$conn){
    $extraArg = array();
    $returnArr = array();
    $query = "select * from faqs where 1=1 AND status = '1'";
    if($faqSearch!=""){
       $query .= " AND ques_title like '%" . $faqSearch . "%'";
    }

    $query .= " LIMIT $start, $limit";

    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}

function countFAQs($conn){
    $extraArg = array();
    $returnArr = array();
    $query = "SELECT count(*) as total  FROM faqs " ;
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}

function getTicketDetailsByTicketId($id, $conn){
    $extraArg = array();
    $returnArr = array();
    $query = "SELECT * FROM support_requests where ticket_id = '".$id ."' " ;
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}

//set Category
function setFAQs($id, $category_faq,$question_faq,$answer_faq, $createdBy, $approvedBy,$status, $conn){
    $extraArg = array();
    $returnArr = array();
    if (empty($id) || !isset($id)) {
      $query = "INSERT INTO faqs(category,ques_title,ans,created_by,approved_by,status) value('$category_faq','$question_faq','$answer_faq','$createdBy','$approvedBy','$status')";
    } else {
      $query = "UPDATE faqs SET ques_title='" . $question_faq . "', ans='" . $answer_faq . "', category='" . $category_faq . "' WHERE id='" . $id . "' ;";
    }
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "FAQ Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }
    return $returnArr;
}

//set faq right side
function setRightSideFAQs($id, $category_faq,$question_faq,$answer_faq,$video_link, $conn){
    $extraArg = array();
    $returnArr = array();
    if (empty($id) || !isset($id)) {
      $query = "INSERT INTO keywo_features_faq(category,question,answer, video_link) value('$category_faq','$question_faq','$answer_faq','$video_link')";
    } else {
      $query = "UPDATE keywo_features_faq SET question='" . $question_faq . "', answer='" . $answer_faq . "', video_link = '".$video_link."' WHERE id='" . $id . "' ;";
    }
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "FAQ Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 66, null, $extraArg);
    }
    return $returnArr;
}

//get All Category
function getAllCategory($conn){
    $extraArg = array();
    $returnArr = array();
    $query = "SELECT * FROM category" ;
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }
    return $returnArr;

}



function getAllfaqsData($offset, $limit, $conn, $category, $faqStatus)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from faqs";

   if($category!="")
   {
       $query .= " where 1=1 ";
   }

     if ($category!="") {
        $query .= " AND  category='" . $category . "' AND status='".$faqStatus."' ";
    }

    $query .= " ORDER by created DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllfaqsDataCount($conn,$category, $faqStatus)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from faqs";

   if($category!="")
   {
       $query .= " where 1=1 ";
   }

    if ($category!="") {
        $query .= " AND  category='" . $category . "' AND status='".$faqStatus."' ";
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function deleteFaqData($id, $conn) {
  $returnArr = array();
  $query = "DELETE FROM faqs WHERE id='".$id."';";
  $result = runQuery($query, $conn);

  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"])) {
          $res[] = $row;
      }
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}


function updateFaqStatus($id, $adminId ,$conn) {
  $returnArr = array();
  $query = "UPDATE faqs SET status='1', approved_by='".$adminId."' WHERE id='".$id."';";
  $result = runQuery($query, $conn);

  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"])) {
          $res[] = $row;
      }
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}

function checkAdminGroup($admin_id, $conn) {
    global $agentsGroupCS;
    $returnArr = array();
    $query = "select count(*) as count from admin_user where group_id like '%{$agentsGroupCS}%' and admin_id={$admin_id}";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllSupportfaqsData($offset, $limit, $conn, $category)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from keywo_features_faq";

     if ($category!="") {
        $query .= " WHERE category='" . $category . "'";
    }

    $query .= " ORDER by created DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllSupportfaqsDataCount($conn,$category)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from keywo_features_faq";

   if($category!="")
   {
       $query .= " where 1=1 ";
   }

    if ($category!="") {
        $query .= " AND  category='" . $category . "'";
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function deleteSupportFaqData($id, $conn) {
  $returnArr = array();
  $query = "DELETE FROM keywo_features_faq WHERE id='".$id."';";
  $result = runQuery($query, $conn);

  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"])) {
          $res[] = $row;
      }
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res;
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}


function getAllAgentEmailData($offset, $limit, $conn, $email,$ticketAdminArray)
{

    $returnArr = array();
    global $blanks, $agentsGroupCS, $defaultAdmin;

    $query = "SELECT admin_id, admin_email, admin_first_name, admin_last_name from admin_user WHERE group_id LIKE '%{$agentsGroupCS}%' AND admin_email != '". $defaultAdmin ."'";

     if ($email!="") {
        $query .= " AND admin_email LIKE '" . $email . "%' AND admin_status = '1'";
    }

    if (!empty($ticketAdminArray)) {
        $query .= " AND admin_email NOT LIKE '".$ticketAdminArray[0]."'";
    }

    $query .= " ORDER by admin_email DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllAgentEmailDataCount($conn,$email,$ticketAdminArray)
{


    $returnArr = array();
    global $blanks, $agentsGroupCS, $defaultAdmin;
    $query = "SELECT count(*) as count from admin_user WHERE group_id LIKE '%{$agentsGroupCS}%' AND admin_email != '". $defaultAdmin ."'";

   if($email!="")
   {
       $query .= " AND 1=1 ";
   }

    if ($email!="") {
        $query .= " AND  admin_email LIKE '" . $email . "%' AND admin_status = '1'";
    }

    if (!empty($ticketAdminArray)) {
        $query .= " AND admin_email NOT LIKE '".$ticketAdminArray[0]."'";
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function assignTicketToAgent($ticketId, $agentEmail, $agentId, $assignedBy, $conn) {
  $extraArg = array();
    $returnArr = array();


    
    $query = "UPDATE support_requests SET assigned_to='" . $agentEmail . "', assigned_agent_id='" . $agentId . "', assigned_by='" . $assignedBy . "', assigned_on = NOW() WHERE ticket_id='" . $ticketId . "' ;";
    
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "Ticket Assigned Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 66, null, $extraArg);
    }
    return $returnArr;
}


function getTicketViewerAuth($email, $conn)
{

    $returnArr = array();
    global $blanks, $agentsGroupCS, $hodGroupCS;
    $query = "SELECT count(*) as count from admin_user WHERE group_id LIKE '%{$agentsGroupCS}%'";

   if($email!="")
   {
       $query .= " AND 1=1 ";
   }

    if ($email!="") {
        $query .= " AND  admin_email='" . $email . "'";
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getTicketEscalationStatus($ticketId, $conn)
{

    $returnArr = array();
    global $blanks, $agentsGroupCS;
    $query = "SELECT count(*) as count from support_requests WHERE hod_escalation_status='1'";

   if($ticketId!="")
   {
       $query .= " AND 1=1 ";
   }

    if ($ticketId!="") {
        $query .= " AND ticket_id='" . $ticketId . "'";
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getTicketViewerIsHod($email, $conn)
{

  $returnArr = array();
  global $blanks, $agentsGroupCS, $hodGroupCS, $superAdminGroupCS;

// SELECT * from admin_user WHERE admin_email='kavitamaurya@bitstreet.in' AND group_id NOT LIKE '%csagent%' AND group_id NOT LIKE '%cshod%'


  $query = "SELECT count(*) as count from admin_user WHERE admin_email='" . $email . "'";

  if ($email!="") {
    $query .= " AND 1=1 ";
  }

  if ($email!="") {
    $query .= " AND group_id NOT LIKE '%{$agentsGroupCS}%' AND group_id NOT LIKE '%{$hodGroupCS}%'";
  }

  // echo "<br>".$query."<br>";
  $result = runQuery($query, $conn);

  if (noError($result)) {
      $res = array();
      while ($row = mysqli_fetch_assoc($result["dbResource"])) {
          $res[] = $row;
      }
      $returnArr["errCode"] = -1;
      $returnArr["errMsg"] = $res[0]["count"];
  } else {
      $returnArr["errCode"] = 5;
      $returnArr["errMsg"] = $result["errMsg"];
  }

  return $returnArr;
}
?>