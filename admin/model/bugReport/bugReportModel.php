<?php

/*
*-----------------------------------------------------------------------------------------------------------
*   Function updateBugData()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   updateBugData()
*   Purpose       :   This function will insert/update bug_reports table and store bugReport Data
*                 :   $data will bring json format data
*   Arguments     :   (json) $data, (object)$conn
*/

function updateBugData($bug_id,$data,$conn)
{
    $bugID = getNextOrderNumber($conn);
    $tableName   = "bug_reports";
    $requestDate = date('Y-m-d h:i:s');
    $returnArr=array();
    $extraArg=array();
    $getQueryListByID = getQueryListByID($conn, $bug_id);

    if (noError($getQueryListByID)) {
        if(isset($getQueryListByID["data"][0]))
        {
            $getQueryListByID = $getQueryListByID["data"][0];
            $bugid = $getQueryListByID["bug_id"];
        }


    }
    
    $getBugRequestStatus = getBugRequestStatus($bug_id,$conn);
    if(noError($getBugRequestStatus))
    {
        if(isset($getBugRequestStatus["errMsg"]) || !empty($getBugRequestStatus["errMsg"]))
        {
            $errMsg    = $getBugRequestStatus["errMsg"];
        }
        if(isset($errMsg["bug_status"]) || !empty($errMsg["bug_status"]))
        {
            $bugStatus = $errMsg["bug_status"]; 
        }
    }
    else
    {
        $errMsg    = "";
        $bugStatus = "";
    }

    if(empty($bugid))
    {
        $query = "INSERT into " . $tableName . "(bug_id,bug_createdAt,bug_desc,bug_rasied_by,bug_status,created_by,imagePath) VALUES('$bugID','$requestDate','".$data["bug_desc"]."','".$data["email"]."','open','".$data["bug_type"]."','".$data["imagePath"]."')";
    }

    else if($bugStatus == 'closed')
    {
        $returnArr = setErrorStack($returnArr, 71, null, $extraArg);    
        return $returnArr;
    }
    else
    {
        $query = "UPDATE " . $tableName . " SET bug_status='closed' WHERE bug_id='" . $bug_id . "'";
    }

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $returnArr = setErrorStack($returnArr, -1, "Data Updated Success.", $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }
    return $returnArr;
}

// Getting Max ID from table.

function selectmaxbuglist($conn){

    $returnArr = array();

    $query = "SELECT MAX(id) as lastid FROM bug_reports";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}

function getNextOrderNumber($conn)
{
    $result=selectmaxbuglist($conn);
    $lastOrder=$result["errMsg"]["lastid"];
    $lastOrder = $lastOrder+1;
    return 'BG-' .$lastOrder;
}


// Getter of bug_reports table.

function getBugReportDetails($createdBy,$status,$conn){
    $returnArr = array();

    $query = "select * from bug_reports where (created_by = 'user_report' OR created_by = 'internal_report') and (bug_status = 'open' OR bug_status = 'closed') order by id DESC";

    $result = runQuery($query, $conn);
//        print_r($result);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 10;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}


function getAllBugsData($offset, $limit, $conn, $bug_id,$bug_status,$bugType)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from bug_reports";

    if($bug_id!="" || $bug_status!="" || $bugType!="")
    {
        $query .= " where 1=1 ";
    }

    if ($bug_id!="") {
        $query .= " AND  bug_id='" . $bug_id . "'";
    }
    if ($bug_status!="") {
        $query .= " AND  bug_status='" . $bug_status . "'";
    }

    if ($bugType!="") {
        $query .= " AND  created_by='" . $bugType . "'";
    }

    $query .= " ORDER by bug_createdAt DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[array_shift($row)] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllBugsCount($conn,$bug_id,$bug_status,$bugType)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from bug_reports";

    if($bug_id!="" || $bug_status!="" || $bugType!="")
    {
        $query .= " where 1=1 ";
    }

    if ($bug_id!="") {
        $query .= " AND  bug_id='" . $bug_id . "'";
    }
    if ($bug_status!="") {
        $query .= " AND  bug_status='" . $bug_status . "'";
    }

    if ($bugType!="") {
        $query .= " AND  created_by='" . $bugType . "'";
    }

   // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}




function getBugsAnalytics($createdBy,$conn){
    $returnArr = array();

    $query="SELECT count(*) as total_count,count(case when bug_status = 'closed' then bug_status end) as total_resolved,count(case when bug_status = 'open' then bug_status end) as total_open from bug_reports where created_by='{$createdBy}'";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 10;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;
}


function getQueryListByID($conn, $bug_id) {

    $returnArr    = array();
    $extraArgs    = array();

    $query  = "SELECT * FROM `bug_reports` WHERE bug_id = '" . $bug_id . "'";
    // $query  = "SELECT * FROM `support_requests`";
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])){
            $res[] = $row;
        }

        $extraArgs["data"] = $res;
        $errMsg    =   $result["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
    } else {

        $errMsg   =   $result["errMsg"];
        $errCode  =   $result["errCode"];
        $returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }

    return $returnArr;
}

function getBugRequestStatus($bugId,$conn){

    $returnArr = array();

    $query = "SELECT bug_status from bug_reports where bug_id = '{$bugId}'";

    $result = runQuery($query, $conn);

    if(noError($result)){
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"]=-1;

        $returnArr["errMsg"]=$res;

    } else {

        $returnArr["errCode"]=5;

        $returnArr["errMsg"]=$result["errMsg"];

    }

    return $returnArr;

}
?>