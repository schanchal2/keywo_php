<?php

function getAppNames($conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "SELECT app_id,app_name from sc_app_details where status=1;";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))
        {
            array_push($res,$row);
        }

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

?>