<?php

function getKYCSlabSettingAdmin($connSearch)
{

    $returnArr = array();

    $query = "SELECT * FROM kyc_levels";


    $result = runQuery($query, $connSearch);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function getKYCSlabSettingAdminbyID($id, $connSearch)
{

    $returnArr = array();

    $query = "SELECT * FROM kyc_levels where slab_id='" . $id . "'";


    $result = runQuery($query, $connSearch);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

function updateKycSlabLevel($slabId, $sendLimit, $receiveLimit, $withdrawalLimit, $intractionLimit, $depositLimit,$yearly_send_fund_limit,$yearly_withdrawal_limit,$yearly_income_limit,$transaction_fees, $connSearch)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE kyc_levels SET send_fund_limit='" . $sendLimit . "',receive_fund_limit='" . $receiveLimit . "',withdrawal_limit='" . $withdrawalLimit . "',intraction='" . $intractionLimit . "',deposit='" . $depositLimit . "',yearly_send_fund_limit='" . $yearly_send_fund_limit . "',yearly_withdrawal_limit='" . $yearly_withdrawal_limit . "',yearly_income_limit='" . $yearly_income_limit . "',transaction_fees='" . $transaction_fees . "' WHERE slab_id='" . $slabId . "' ;";

    $result = runQuery($query, $connSearch);


    if (noError($result)) {
        $errMsg = "KYC Slab Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}


function getKYCSlabByLevel($level = "1", $connSearch)
{
    $returnArr = array();
    $extraArg = array();

    $level = "Level-" . $level;

    $query = "SELECT * FROM kyc_levels where kyc_level_name='" . $level . "'";
    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 64, null, $extraArg);
    }
    return $returnArr;
}


?>