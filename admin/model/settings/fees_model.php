<?php


function addUpdateKeywoSearchFees($fieldtoupdate, $fieldValue, $connSearch)
{

    $adminTableFields = array("default_app_id", "current_max_payout", "IPMaxLimit", "default_system_mode");
    $adminTableFields = implode(',', $adminTableFields);

    $returnArr = array();
    $extraArg = array();

    $query = "insert into admin_setting({$adminTableFields}) SELECT {$adminTableFields} FROM admin_setting ORDER BY timestamp DESC LIMIT 1;";
    $result = runQuery($query, $connSearch);
    if (noError($result)) {

        $query = "update admin_setting set {$fieldtoupdate} ='{$fieldValue}' ORDER BY timestamp DESC LIMIT 1";

        $result = runQuery($query, $connSearch);
        if (noError($result)) {
            $errMsg = "Fees Updated Successfully!!";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $query = "delete from admin_setting  ORDER BY timestamp DESC LIMIT 1";
            $result = runQuery($query, $connSearch);
            if (noError($result)) {
                $returnArr = setErrorStack($returnArr, -1, "Fees Updation Failed !", $extraArg);
            } else {
                $returnArr = setErrorStack($returnArr, 50, null, $extraArg);
            }
        }

    } else {
        $returnArr = setErrorStack($returnArr, 50, null, $extraArg);
    }

    return $returnArr;
}


function getAllApps($connDemo)
{

    $returnArr = array();

    $query = "SELECT *,(`totalAnonymousSearch`+`totalQualifiedSearches`+`totalUnqualifiedSearches`) As TotSearches from sc_app_details where status=1 order by TotSearches DESC";


    $result = runQuery($query, $connDemo);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function getAdminSettingsFromSearchAdmin($connDemo)
{

    $returnArr = array();

    $query = "SELECT * FROM admin_setting ORDER BY timestamp DESC LIMIT 1";


    $result = runQuery($query, $connDemo);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function getAdminSettingsFromKeywordAdmin($connkeyword)
{

    $returnArr = array();

    $query = "SELECT * FROM admin_settings ORDER BY update_timestamp DESC LIMIT 1";


    $result = runQuery($query, $connkeyword);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function addUpdateKeywoKeywordsFees($fieldtoupdate, $fieldValue, $connkeyword)
{
    $adminTableFields = array("trading_commision_percent", "affiliate_earning_percent", "first_buy_percent", "minimum_withdrawal_amount", "withdrawal_fees", "transferFees", "kwd_renewal_fees_per_year", "search_affiliate_percent", "searchtrade_maintenance_percent","first_confirmation_minutes","final_confirmation_minutes","payment_confirmation_numbers,social_affiliate_percent,cashout_packing_list_duration,cashout_automode_amount_limit,cashout_manual_mode_amount_uamDepartment,cashout_manual_mode_amount_mmDepartment,cashout_manual_mode_amount_smDepartment");
    $adminTableFields = implode(',', $adminTableFields);

    $returnArr = array();
    $extraArg = array();

    $query = "insert into admin_settings({$adminTableFields}) SELECT {$adminTableFields} FROM admin_settings ORDER BY update_timestamp DESC LIMIT 1;";
    $result = runQuery($query, $connkeyword);

    if (noError($result)) {

        $query = "update admin_settings set {$fieldtoupdate} ='{$fieldValue}' ORDER BY update_timestamp DESC LIMIT 1";

        $result = runQuery($query, $connkeyword);

        if (noError($result)) {
            $errMsg = "Fees Updated Successfully!!";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        } else {
            $query = "delete from admin_settings  ORDER BY update_timestamp DESC LIMIT 1";
            $result = runQuery($query, $connkeyword);
            if (noError($result)) {
                $returnArr = setErrorStack($returnArr, -1, "Fees Updation Failed !", $extraArg);
            } else {
                $returnArr = setErrorStack($returnArr, 50, null, $extraArg);
            }
        }

    } else {
        $returnArr = setErrorStack($returnArr, 50, null, $extraArg);
    }

    return $returnArr;
}


function setBlockUserThreshold($threshold){
    global $walletPublicKey, $mode, $mongoServerSocial;
    $requestUrl =$mongoServerSocial;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "threshold={$threshold}&publicKey={$walletPublicKey}";
    $postFields = $apiText;
    $apiName = 'admin/blockuserthreshold';
    $curl_type = 'PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function get2fA_status($conn)
{

    $returnArr = array();

    $query = "SELECT * FROM two_factor_auth_setting_admin";


    $result = runQuery($query, $conn);


    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

function two_faOnOff($field,$status,$conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE two_factor_auth_setting_admin SET {$field}={$status}";

    $result = runQuery($query, $conn);


    if (noError($result)) {
        $errMsg = "2FA Status Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}