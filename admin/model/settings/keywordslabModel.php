<?php
function get_all_slab($conn)
{
    $returnArr = array();

    $query = "SELECT * FROM first_purchase_slabs;";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res[] = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function get_currentActiveSlab($conn)
{
    $returnArr = array();

    $query = "select * from first_purchase_slabs where (kwd_Sold != (end_limit - start_limit +1)) order by id asc limit 1;";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function get_first_purchase_slabs_MAX_ID($conn)
{

    $returnArr = array();

    $query = "select max(id) as id from first_purchase_slabs;";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"]= 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}


function get_last_first_purchase_slabs_from_MAX_ID($id, $conn)
{

    $returnArr = array();

    $query = "select * from first_purchase_slabs where id = '" . $id . "';";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $returnArr["errCode"] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

?>