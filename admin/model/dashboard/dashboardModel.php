<?php 

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getKeywordsStats
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getKeywordsStats()
*   Purpose       :   This function returns Value of columns passed through array .
*                     e.g: total registered users
*   Arguments     :   1.(obj)$conn [connection object] to connect database
*   Returns       :   (int) Total count of each column passed in arguments (array)$columns_arr .
*/

function getKeywordsStats($conn) {

    $returnArr = array();
    $res = array();
    $extraArgs = array();

    $query = "select * from keywords_stat";

    $execQuery = runQuery($query, $conn);

    if( noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res[] = $row;
        }

        $errMsg = "Successfully : fetch keywo-stats details";
        $extraArgs["data"] = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    }else{
        $errMsg = "Error : Fetching keywo-stats details";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getallBidStatus
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getallBidStatus()
*   Purpose       :   This function returns Value of columns passed through array .
*                     e.g: total placed bid details
*   Arguments     :   1.(obj)$conn [connection object] to connect database
*   Returns       :   (int) Total count of each column passed in arguments (array)$columns_arr .
*/

function getallBidStatus($conn) {

    $returnArr = array();
    $res = array();
    $extraArgs = array();

    $query = "select count(distinct(`keyword`)) as count,sum(bid_price) as bidPrice from latest_bid_all;";

    $execQuery = runQuery($query, $conn);

    if( noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res[] = $row;
        }

        $errMsg = "Successfully : fetch keywo-stats details";
        $extraArgs["data"] = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    }else{
        $errMsg = "Error : Fetching keywo-stats details";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getallAskStatus
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getallAskStatus()
*   Purpose       :   This function returns Value of columns passed through array .
*                     e.g: total placed bid details
*   Arguments     :   1.(obj)$conn [connection object] to connect database
*   Returns       :   (int) Total count of each column passed in arguments (array)$columns_arr .
*/

function getallAskStatus($conn) {

    $returnArr = array();
    $res = array();
    $extraArgs = array();

    $query = "select count(distinct(`keyword`)) as count,sum(ask_price) as askPrice from latest_ask_all;";

    $execQuery = runQuery($query, $conn);

    if( noError($execQuery)){
        while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
            $res[] = $row;
        }

        $errMsg = "Successfully : fetch keywo-stats details";
        $extraArgs["data"] = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    }else{
        $errMsg = "Error : Fetching keywo-stats details";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
    }

    return $returnArr;
}

?>