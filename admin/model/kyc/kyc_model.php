<?php
function getLevel3KYCusersToVerify($email)
{

    global $walletPublicKey, $mode,$walletURL;
    $requestUrl =$walletURL; //notification

    $retArray = array();

    $headers = array();
    /* create signature */
    $apiText = "publicKey={$walletPublicKey}";//notification

    $postFields = "email=" . $email . "&publicKey=" . $walletPublicKey;
    $apiName = 'pocket/kyc/level/docs';  //notification
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function changeKYCdocStatus($email,$doc_id,$doc_type_num,$status_to_set)
{

    global $walletPublicKey, $mode,$walletURL;
    $requestUrl =$walletURL; //notification

   // $user_id=getUserID($email)["errMsg"]["_id"];

    $retArray = array();


    $userJsonData["_id"]=$doc_id;
    $userJsonData["type"]=$doc_type_num;
    $userJsonData["doc_status"]=$status_to_set;

   $userJson= json_encode(array($userJsonData));
    $headers = array();
    /* create signature */
    $apiText ="email=" . $email ."&publicKey={$walletPublicKey}";
    $postFields = "user=".$userJson."&email=" . $email . "&publicKey=" . $walletPublicKey;
    $apiName = 'pocket/kyc/level/update';
    $curl_type = 'PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function updateKYCLevelCount($user_id, $email,$KYClevel){
    $retArray = array();
    $headers = array();
    global $walletPublicKey, $mode, $walletURL;

    if(!empty($user_id) && !empty($email) && !empty($KYClevel)){
        $postFields = array(
            "email" => $email,
            "publicKey" =>  urlencode($walletPublicKey)
        );
        $apiText = "email={$email}&move={$KYClevel}&user_id={$user_id}&publicKey={$walletPublicKey}";

        $apiName = "pocket/{$user_id}/kyc/level/shift/{$KYClevel}";
        $requestUrl = "{$walletURL}";
        $curl_type = 'PUT';
        $content_type = "content-type: application/json";
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    }else{
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = "All feilds are mandatory";
    }

    return $retArray;

}
?>