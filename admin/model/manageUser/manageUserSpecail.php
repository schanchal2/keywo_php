<?php




function getUsersListSpecial($skip,$flag,$limit)
{

    global $walletPublicKey, $mode,$mongoServerSocial;
    $requestUrl =$mongoServerSocial; //notification

    $retArray = array();

    $headers = array();
    /* create signature */
    $apiText = "skip={$skip}&limit={$limit}&flag={$flag}&publicKey={$walletPublicKey}";//notification

    $postFields = "skip=" . $skip . "&publicKey=" . $walletPublicKey . "&flag=" . $flag . "&limit=" . $limit;
    $apiName = 'admin/specialfollowing';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}




function getDataForSpecialUserCheck($email)
{
        global $walletPublicKey, $mode, $walletURLnotificationServer;
        $requestUrl = $walletURLnotificationServer;
        $retArray = array();

        $headers = array();
        $fields = "&fieldnames=_id,user_id,account_handle,active";
        $apiText = "email={$email}{$fields}&publicKey={$walletPublicKey}";
        $postFields = "email=" . $email . "&publicKey=" . $walletPublicKey . $fields;
        $apiName = 'user/userdetails';
        $curl_type = 'GET';
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);

        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }


        return $retArray;
}


function addNewSpecialUser($specialUserid, $specialUseraccount_handle,$specialUserEmail)
{
    global $walletPublicKey, $mode, $mongoServerSocial,$walletURLnotificationServer;
    $requestUrl =$mongoServerSocial;
    $retArray = array();
    $headers = array();
    /* create signature */
    $ref_id= getUserInfo($specialUserEmail, $walletURLnotificationServer, "_id")["errMsg"]["_id"];
    $apiText = "email={$specialUserEmail}&userid={$specialUserid}&account_handle={$specialUseraccount_handle}&publicKey={$walletPublicKey}";
    $postFields = "user_ref=".$ref_id."&email=" . $specialUserEmail . "&userid=" . $specialUserid . "&account_handle=".$specialUseraccount_handle."&publicKey=" . $walletPublicKey;
    $apiName = 'admin/specialfollowing';
    $curl_type = 'POST';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function deleteSpecialUser($specialUserEmail)
{
    global $walletPublicKey, $mode, $mongoServerSocial;
    $requestUrl =$mongoServerSocial;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText = "email={$specialUserEmail}&publicKey={$walletPublicKey}";
    $postFields = "email=" . $specialUserEmail . "&publicKey=" . $walletPublicKey;
    $apiName = 'admin/specialfollowing';
    $curl_type = 'DELETE';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


?>