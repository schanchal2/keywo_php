<?php

function insert_user_activity($update_field_name, $country, $device, $browser, $gender, $current_date, $conn)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM daily_keywo_UAM_analytics_{$current_month}_{$current_year} WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND tracking_date = '{$current_date}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE daily_keywo_UAM_analytics_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}+1 WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND tracking_date = '{$current_date}' ";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User avtivity update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO daily_keywo_UAM_analytics_{$current_month}_{$current_year} (country,device,browser,gender,tracking_date,{$update_field_name})
                                   VALUES('{$country}','{$device}','{$browser}','{$gender}','{$current_date}', '1');";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User avtivity Insert sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}

function deduct_field_count($update_field_name, $country, $device, $browser, $gender, $current_date, $conn)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    $extraArg = array();
    $returnArr = array();


    $query = "UPDATE daily_keywo_UAM_analytics_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}-1 WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND tracking_date = '{$current_date}' ";

    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "User avtivity update sucessfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


    } else {
        $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
    }


    return $returnArr;

}


function insert_userReferral_activity($update_field_name, $country, $email, $device, $browser, $gender, $current_date, $conn)
{

    // $current_date = date("Y-m-d", strtotime("-1 days"));
    $newdate = explode("-", $current_date);
    $current_year = $newdate["0"];
    $current_month = $newdate["1"];

    $extraArg = array();
    $returnArr = array();


    $query = "SELECT * FROM daily_keywo_userReferral_analytics_{$current_month}_{$current_year} WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND email='{$email}' AND tracking_date = '{$current_date}'";

    $result = runQuery($query, $conn);


    if (noError($result)) {

        if (mysqli_num_rows($result["dbResource"])) {


            $query = "UPDATE daily_keywo_userReferral_analytics_{$current_month}_{$current_year} SET  {$update_field_name} = {$update_field_name}+1 WHERE country='{$country}' AND device='{$device}' AND browser='{$browser}' AND gender='{$gender}' AND email='{$email}' AND tracking_date = '{$current_date}' ";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Referral avtivity update sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


            } else {
                $returnArr = setErrorStack($returnArr, "A02", null, $extraArg);
            }

        } else {

            $query = "INSERT INTO daily_keywo_userReferral_analytics_{$current_month}_{$current_year} (country,device,browser,gender,tracking_date,email,{$update_field_name})
                                   VALUES('{$country}','{$device}','{$browser}','{$gender}','{$current_date}','{$email}','1');";

            $result = runQuery($query, $conn);

            if (noError($result)) {

                $errMsg = "User Referral avtivity Insert sucessfully";
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

            } else {
                $returnArr = setErrorStack($returnArr, "A01", null, $extraArg);
            }
        }
    } else {

        $returnArr = setErrorStack($returnArr, "3", null, $extraArg);
    }

    return $returnArr;

}

function createTable_keywo_UAM_analytics($current_year, $current_month, $conn)
{

    $extraArg = array();
    $returnArr = array();


    $lastYear = date("Y", strtotime("-1 year"));
    $lastMonth = date("m", strtotime("-1 month"));

        // $query = "CREATE TABLE IF NOT EXISTS daily_keywo_UAM_analytics_{$current_month}_{$current_year} LIKE daily_keywo_UAM_analytics_{$lastMonth}_{$lastYear}";
        $query = "CREATE TABLE `daily_keywo_UAM_analytics_{$current_month}_{$current_year}` (
                                      `id` smallint(20) NOT NULL AUTO_INCREMENT,
                                      `country` varchar(255) DEFAULT NULL,
                                      `device` varchar(255) DEFAULT NULL,
                                      `browser` varchar(255) DEFAULT NULL,
                                      `gender` varchar(255) DEFAULT NULL,
                                      `user_loginCount` bigint(20) NOT NULL,
                                      `user_signup_count` bigint(20) NOT NULL,
                                      `user_signUpactivationSuccess_count` bigint(20) NOT NULL,
                                      `user_signUpactivationFailed_count` bigint(20) NOT NULL,
                                      `user_forget_password_count` bigint(20) NOT NULL,
                                      `user_social_mode_switch_count` bigint(20) NOT NULL,
                                      `user_search_mode_switch_count` bigint(20) NOT NULL,
                                      `user_forget_passSucc_count` bigint(20) NOT NULL,
                                      `user_forget_passFailed_count` bigint(20) NOT NULL,
                                      `total_unverified_users` bigint(20) NOT NULL,
                                      `user_with_referral` bigint(20) NOT NULL,
                                      `total_blocked_userstoday` bigint(20) NOT NULL DEFAULT '0',
                                      `total_unblocked_userstoday` bigint(20) NOT NULL DEFAULT '0',
                                      `tracking_date` date NOT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
                    ";
        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "daily_keywo_analytics table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
        }



    return $returnArr;
}

function createTable_keywo_Referral_analytics($current_year, $current_month, $conn)
{

    $extraArg = array();
    $returnArr = array();

    $lastYear = date("Y", strtotime("-1 year"));
    $lastMonth = date("m", strtotime("-1 month"));


        //$query = "CREATE TABLE IF NOT EXISTS daily_keywo_userReferral_analytics_{$current_month}_{$current_year} LIKE daily_keywo_userReferral_analytics_{$lastMonth}_{$lastYear}";
        $query = "CREATE TABLE `daily_keywo_userReferral_analytics_{$current_month}_{$current_year}` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `gender` varchar(255) DEFAULT NULL,
                      `browser` varchar(255) DEFAULT NULL,
                      `device` varchar(255) DEFAULT NULL,
                      `country` varchar(255) DEFAULT NULL,
                      `email` varchar(255) DEFAULT NULL,
                      `user_name` varchar(255) DEFAULT NULL,
                      `no_of_referrals` bigint(20) DEFAULT '0',
                      `tracking_date` date DEFAULT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;";

        $result = runQuery($query, $conn);

        if (noError($result)) {

            $errMsg = "daily_keywo_Referral_analytics table created sucessfully";
            $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

        } else {
            $returnArr = setErrorStack($returnArr, "A03", null, $extraArg);
        }

    return $returnArr;
}

?>