<?php


function getCategoryandLimit()
{
    global $walletPublicKey, $mode, $mongoServerSocial;
    $requestUrl = $mongoServerSocial;
    $retArray = array();
    $headers = array();
    $apiText = "publicKey={$walletPublicKey}";
    $postFields = "publicKey=" . $walletPublicKey;
    $apiName = 'admin/report_category';
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


function addNewCategoryforSpamPost($category, $limit,$action)
{
    $category=rawurlencode($category);
    global $walletPublicKey, $mode, $mongoServerSocial;
    $requestUrl =$mongoServerSocial;
    $retArray = array();
    $headers = array();
    /* create signature */
    $apiText ="publicKey={$walletPublicKey}";
    //$category=rawurlencode($category);
    $postFields ="category=" . $category . "&limit=" . $limit . "&publicKey=" . $walletPublicKey;
    $apiName ="admin/report_post/category/{$category}/{$action}";
    $curl_type='PUT';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;
}


?>