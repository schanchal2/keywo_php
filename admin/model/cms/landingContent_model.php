<?php

/*
* --------------------------------------------------------------------------------------------
*       Landing Page CMS Content Model
*---------------------------------------------------------------------------------------------
*   This Landing Page CMS Content Model is used for CRUD of CMS .
*/


/*------------------------------------------------------------------------------------------------------------
*       Function addUpdateConsumerUploaderImage
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getMetaContent()
*     Purpose       :   To Add or Update Consumer and Uploader Data to database.
*     Arguments     :   $consumerImage,$uploaderImage,$orders,$contentId
*     Response      :   Success/Fail Error Array
*/

function addUpdateConsumerUploaderImage($consumerImage, $uploaderImage, $orders, $contentId, $conn)
{
    $extraArg=array();
    $returnArr = array();
    if (empty($contentId) || $contentId == "undefined") {
        $time="NOW()";
        $updated_by=$_SESSION["admin_id"];
        $query = "INSERT INTO cms_slider(slider_image_consumer,slider_image_producer,orders,updated_time,updated_by) VALUE ('" . $consumerImage . "','" . $uploaderImage . "'," . $orders . "," . $time . ",'".$updated_by."')";
    } else {
        $query = "UPDATE cms_slider SET slider_image_consumer='" . $consumerImage . "',slider_image_producer='" . $uploaderImage . "' WHERE id=" . $contentId;
    }

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Content details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Content details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function deleteConsumerUploaderImage
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   deleteConsumerUploaderImage()
*     Purpose       :   To Delete Conusmer and Uploader Data from Database.
*     Arguments     :   $contentId
*     Response      :   Success/Fail Error Array
*/


function deleteConsumerUploaderImage($contentId, $conn)
{
    $extraArg=array();
    $returnArr = array();

    $query = "DELETE from cms_slider WHERE id=" . $contentId;

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully Deleted Content details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 43, "Error in Deleting Content details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getConsumerUploaderImage
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getConsumerUploaderImage()
*     Purpose       :   To get Conusmer and Uploader Data from Database.
*     Arguments     :   $contentId
*     Response      :   Associative Array of Data
*/


function getConsumerUploaderImage($contentId, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from cms_slider where id='" . $contentId . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}


/*------------------------------------------------------------------------------------------------------------
*       Function getConsumerUploader
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getConsumerUploaderImage()
*     Purpose       :   To get Conusmer and Uploader Data from Database.
*     Response      :   Associative Array of Data
*/


function getConsumerUploader($conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from cms_slider";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function addUpdateFeatureVideo
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   addUpdateFeatureVideo()
*     Purpose       :   To Add OR Update Content Data to Database.
*     Arguments     :   $tablename,$contentTitle,$contentVideoURL,$contentShortDesc,$contentLongDesc,$featureType,$contentId
*     Response      :   Success/Fail Error Array
*/

function addUpdateFeatureVideo($tablename, $contentTitle, $contentVideoURL, $contentShortDesc, $contentLongDesc, $featureType, $contentId, $conn)
{
    $returnArr = array();
    $extraArg = array();
    if (empty($contentId) || $contentId == "undefined") {
        $query = "INSERT INTO $tablename(content_title,short_description,long_description,content_video_link,status,timestamp,feature_type) VALUE ('" . $contentTitle . "','" . $contentShortDesc . "','" . $contentLongDesc . "','" . $contentVideoURL . "','0',now(),'" . $featureType . "')";
    } else {
        $query = "UPDATE $tablename SET content_title='" . $contentTitle . "',short_description='" . $contentShortDesc . "',long_description='" . $contentLongDesc . "',content_video_link='" . $contentVideoURL . "',feature_type='" . $featureType . "' WHERE id=" . $contentId;
    }

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Content details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Content details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function deleteFeatureVideo
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   deleteFeatureVideo()
*     Purpose       :   To Delete Content Data to Database.
*     Arguments     :   $tablename,$contentId
*     Response      :   Success/Fail Error Array
*/
function deleteFeatureVideo($tablename, $contentId, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "DELETE from $tablename WHERE id=" . $contentId;

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully Deleted Content details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 43, "Error in Deleting Content details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getVideoCMSData
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getVideoCMSData()
*     Purpose       :   To get Content Data from Database.
*     Arguments     :   $tablename,$contentId
*     Response      :   Associative Array of Data
*/

function getVideoCMSData($tablename, $contentId, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from $tablename where id='" . $contentId . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function getBannerTitle
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getBannerTitle()
*     Purpose       :   To get Banner Title Data from Database.
*     Arguments     :   $tablename,$contentId
*     Response      :   Associative Array of Data
*/


function getBannerTitle($conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from cms_LandingPage ORDER BY last_updated_timestamp DESC LIMIT 1";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function UpdateBannerTitle
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   UpdateBannerTitle()
*     Purpose       :   To Add OR Update Banner Content Data to Database.
*     Arguments     :   $tablename,$pageTitle,$bannerImage,$contentShortDesc,$contentId
*     Response      :   Success/Fail Error Array
*/

function UpdateBannerTitle($tablename, $pageTitle, $bannerImage, $contentId, $conn)
{
    $returnArr = array();
    $extraArg = array();

    $query = "UPDATE $tablename SET page_title='" . $pageTitle . "',banner_image='" . $bannerImage . "' WHERE id=" . $contentId;

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Content details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Content details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function getBannerByTitle
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getBannerByTitle()
*     Purpose       :   To get Banner Title Data from Database.
*     Arguments     :   $contentId
*     Response      :   Associative Array of Data
*/

function getBannerByTitle($id, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from cms_LandingPage where id={$id}";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function cms_sliderOrders
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   cms_sliderOrders()
*     Purpose       :   To get CMS slider order Data from Database.
*     Response      :   Associative Array of Data
*/


function cms_sliderOrders($conn)
{
    $extraArg = array();
    $returnArr = array();
    $query = "select `orders` from cms_slider";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 45, null, $extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function selectTableRowCount
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   selectTableRowCount()
*     Purpose       :   Return total row count
*     Response      :   Associative Array of Data
*/
function selectTableRowCount($tablename, $conn) //get table rows count
{
    $extraArg = array();
    $returnArr = array();
    $query = "select count(*) as count from {$tablename}";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr = setErrorStack($returnArr, -1, $res, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}