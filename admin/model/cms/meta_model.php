<?php

/*
* --------------------------------------------------------------------------------------------
*       Meta Model
*---------------------------------------------------------------------------------------------
*   This Meta model is used to add dynamic meta tag to website pages
*/


/*------------------------------------------------------------------------------------------------------------
*       Function getMetaContent
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   getMetaContent()
*     Purpose       :   To get meta content from database.
*     Arguments     :   $contentId
*     Response      :   Associative array of database data
*/


function getMetaContent($contentId, $conn)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from meta_tags where id='" . $contentId . "'";

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;

}

/*------------------------------------------------------------------------------------------------------------
*       Function addUpdateMetatag
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   addUpdateMetatag()
*     Purpose       :   To Add OR insert meta content to database.
*     Arguments     :   $metaId,$pageName,$pageTitle,$metaName,$metaContent
*     Response      :   Success/Fail Error Message
*/

function addUpdateMetatag($metaId, $pageName, $pageTitle, $metaName, $metaContent, $conn)
{
    $extraArg = array();
    $returnArr = array();
    if (empty($metaId) || $metaId == "undefined") {
        $query = "INSERT INTO meta_tags(page_name,page_title,meta_name,meta_content) VALUE ('" . $pageName . "','" . $pageTitle . "','" . $metaName . "','" . $metaContent . "')";
    } else {
        $query = "UPDATE meta_tags SET page_name='" . $pageName . "',page_title='" . $pageTitle . "',meta_name='" . $metaName . "',meta_content='" . $metaContent . "' WHERE id=" . $metaId;
    }

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Metatag details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating meta details",$extraArg);
    }
    return $returnArr;
}

/*------------------------------------------------------------------------------------------------------------
*       Function removeMetatag
*-------------------------------------------------------------------------------------------------------------
*     Function Name :   removeMetatag()
*     Purpose       :   To remove meta content from database.
*     Arguments     :   $metaId
*     Response      :   Success/Fail Error Message
*/

function removeMetatag($metaId, $conn)
{

    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM meta_tags WHERE id=" . $metaId;
    $queryResult = runQuery($query, $conn);
    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully deleted Metatag details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in deleting meta details",$extraArg);
    }
    return $returnArr;
}

?>