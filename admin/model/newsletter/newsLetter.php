<?php


function addNewsletterToQueue($newsLetterContent,$subject,$usersType,$sender,$gender,$country,$city,$status,$conn)
{
    $extraArg = array();
    $returnArr = array();
    $query = "INSERT INTO `newsletter_queue`(`content`,`subject`,`category`,`sender`,`gender`,`country`,`city`,`status`,`created`) VALUES ('" . $newsLetterContent . "','" . $subject . "','" . $usersType . "','" . $sender. "','" . $gender . "','" . $country . "','" . $city . "'," . $status . ",NOW());";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "Thank You,Newsletter Added to Queue Successfully,it will be processed within 1 Hour .";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 67, null, $extraArg);
    }
    return $returnArr;
}

function getNewsletterFromQueue($conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "select * from newsletter_queue where status='0'";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;
}

function updateNewsletter($newsletterid,$count,$conn)
{
    $returnArr=array();
    $extraArg=array();
    $statType="no_of_user_sent";
    $query = "UPDATE newsletter_queue SET  {$statType} = {$count},status=1 where id={$newsletterid}";
    $result = runQuery($query, $conn);

    if (noError($result)) {

        $errMsg = "Newsletter Statistics update sucessfully";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, "68", null, $extraArg);
    }

    return $returnArr;
}


function userBatchForNewsletter($time, $limit,$status)
{
    global $walletPublicKey, $mode, $walletURLIPnotification;
    $requestUrl = $walletURLIPnotification;
    $retArray = array();

    $headers = array();
    /* create signature */
    $apiText = "time={$time}&limit={$limit}&status={$status}&publicKey={$walletPublicKey}";
    $postFields =$apiText;
    $apiName = "api/notify/v2/admin/getUsers";
    $curl_type = 'GET';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'requestUrl' => $requestUrl);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }


    return $retArray;

}

/************************************************************** all users *******************************************************/
function sendBatchEmail($usersList,$from,$emailSubject,$content)
{
    $to = "";
    $recptArray = array();
    foreach ($usersList as $users) {
        $to .= $users["email"] . ",";

        $emailLoop = $users["email"];
        $recptArray[$emailLoop]['fullname'] = $users["fullname"];
        $recptArray[$emailLoop]['id'] = $users["_id"];
        $lastUserTime=$users["creationTime"];
    }
    $to = rtrim($to, ",");
    $parameters["name"] = "%recipient.fullname%";
    $parameters["content"] = $content;
    $recipientVars = json_encode($recptArray);
    $sendMail = sendEmailBatch($to, $from, $recipientVars, $emailSubject, "admin_default.html", $parameters);
    $sendMail["lastUserTime"]=$lastUserTime;
    return $sendMail;
}


function sendNewsletterInBatch($time,$limit,$status,$from,$emailSubject,$content,$count,$userListCount,$connKeywords,$id)
{

    $retArray=array();
    $usersList=userBatchForNewsletter($time,$limit,$status);
    if(noError($usersList)) {

        if($usersList["errMsg"]!="No More Records") {
            $userCount=count($usersList["errMsg"]);

        }else
        {
            $userCount=0;
        }
        $userListCount=$userCount+$userListCount;
        if($usersList["errMsg"]!="No More Records") {
            $usersList = $usersList["errMsg"];
            $result = sendBatchEmail($usersList, $from, $emailSubject, $content);
            if (noError($result)) {
                echo "Sent Batch:".$count.".To <strong>".$userListCount." </strong> Users.<br/>";
                if (isset($result["lastUserTime"]) || $result["lastUserTime"] != "") {
                    $count=$count+1;
                    return sendNewsletterInBatch($result["lastUserTime"], $limit, $status, $from, $emailSubject, $content,$count,$userListCount,$connKeywords,$id);
                }

            } else {
                printArr($retArray);
            }
        }else{
            echo "no more records<br/>";
            $UpdateCount=updateNewsletter($id,$userListCount,$connKeywords);
            if(noError($UpdateCount))
            {
                echo "Updated Sent Count and Status in DB<br/>";
            }else
            {
                echo "Failed to Update Sent Count and Status in DB<br/>";
            }
        }
    }else{
        printArr($usersList);
    }
}

/************************************************************** all users *******************************************************/

/************************************************************** keywords users *******************************************************/
function getKeywordUsers($conn,$id,$limit)
{
    $returnArr = array();
    $extraArg = array();
    $query = "select id as kWid,buyer_id as email from mykeyword_details where id > {$id} limit {$limit}";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $errMsg = $res;
        if(count($errMsg)<=0)
        {
            $errMsg="No More Records";
        }
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 69, null, $extraArg);
    }
    return $returnArr;
}

function sendBatchEmailKeywords($usersList,$from,$emailSubject,$content)
{
    $to = "";
    $recptArray = array();
    foreach ($usersList as $users) {
        $to .= $users["email"] . ",";

        $emailLoop = $users["email"];
     //   $recptArray[$emailLoop]['fullname'] = $users["fullname"];
        $recptArray[$emailLoop]['kWid'] = $users["kWid"];
        $lastUserTime=$users["kWid"];
    }
    $to = rtrim($to, ",");
    $parameters["name"] = "User";
    $parameters["content"] = $content;
    $recipientVars = json_encode($recptArray);
    $sendMail = sendEmailBatch($to, $from, $recipientVars, $emailSubject, "admin_default.html", $parameters);
    $sendMail["kWid"]=$lastUserTime;
    return $sendMail;
}


function sendNewsletterInBatchKeywordUsers($kWid,$limit,$from,$emailSubject,$content,$count,$userListCount,$connKeywords,$id)
{

    $retArray=array();
    $usersList=getKeywordUsers($connKeywords,$kWid,$limit);
    if(noError($usersList)) {

        if($usersList["errMsg"]!="No More Records") {
            $userCount=count($usersList["errMsg"]);

        }else
        {
            $userCount=0;
        }
        $userListCount=$userCount+$userListCount;
        if($usersList["errMsg"]!="No More Records") {
            $usersList = $usersList["errMsg"];
            $result = sendBatchEmailKeywords($usersList, $from, $emailSubject, $content);
            if (noError($result)) {
                echo "Sent Batch:".$count.".To <strong>".$userListCount." </strong> Users.<br/>";
                if (isset($result["kWid"]) || $result["kWid"] != "") {
                    $count=$count+1;
                    return sendNewsletterInBatchKeywordUsers($result["kWid"], $limit,$from, $emailSubject, $content,$count,$userListCount,$connKeywords,$id);
                }

            } else {
                printArr($retArray);
            }
        }else{
            echo "no more records<br/>";
            $UpdateCount=updateNewsletter($id,$userListCount,$connKeywords);
            if(noError($UpdateCount))
            {
                echo "Updated Sent Count and Status in DB<br/>";
            }else
            {
                echo "Failed to Update Sent Count and Status in DB<br/>";
            }
        }
    }else{
        printArr($usersList);
    }
}


function sendBackgroundNewsletter($url)
{
    global $adminRoot;
    // check if fastcgi_finish_request is callable
    if (is_callable('fastcgi_finish_request')) {
        /*
         * This works in Nginx but the next approach not
         */
        session_write_close();
        fastcgi_finish_request();

        return;
    }

    ignore_user_abort(true);


    $serverProtocole = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
    header($serverProtocole.' 200 OK');
    header('Content-Encoding: none');
    header('Content-Length: '.ob_get_length());
    header('Connection: close');
    header('fastcgi_pass_header Connection-close');

    ob_end_flush();
    ob_flush();
    flush();
    if (session_id()) session_write_close();
    file_get_contents($url);

    //internalCurlRequestPost($url, 0, "");
}

/************************************************************** keywords users *******************************************************/