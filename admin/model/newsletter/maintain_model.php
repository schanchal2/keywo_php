<?php
function setMaintainStatus($id, $status,$conn)
{
    $extraArg = array();
    $returnArr = array();

        $query = "UPDATE maintenance_mode SET status={$status} WHERE id={$id}";

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Maintenance Mode details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Maintenance Mode  details",$extraArg);
    }
    return $returnArr;
}


function checkOpenMessage($status,$conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "select count(*) count from maintenance_mode where status={$status}";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;
}


function addNewMaintainMsg($msg,$by,$conn)
{
    $extraArg = array();
    $returnArr = array();

    $query = "INSERT INTO maintenance_mode(message,last_updated_by,status,last_updated_on) VALUE ('" . $msg . "','" . $by . "',0,NOW())";

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Maintainence details", $extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Maintainence details", $extraArg);
    }
    return $returnArr;
}


function deleteMsg($msgid,$conn)
{
    $extraArg = array();
    $returnArr = array();

    $query = "DELETE from maintenance_mode where id={$msgid}";

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully Deleted Maintainence details", $extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in Deleting Maintainence details", $extraArg);
    }
    return $returnArr;
}



function getMsgtoEdit($id,$conn)
{
    $returnArr = array();
    $extraArg = array();
    $query = "select * from maintenance_mode where id={$id}";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 44, null, $extraArg);
    }
    return $returnArr;
}


function updateMaintainsg($id,$msg,$by,$conn)
{
    $extraArg = array();
    $returnArr = array();

    $query = "UPDATE maintenance_mode SET message='{$msg}',last_updated_by='{$by}' WHERE id={$id}";

    $queryResult = runQuery($query, $conn);

    if (noError($queryResult)) {
        $returnArr = setErrorStack($queryResult, -1, "Successfully updated Maintenance Mode details",$extraArg);
    } else {
        $returnArr = setErrorStack($queryResult, 41, "Error in updating Maintenance Mode  details",$extraArg);
    }
    return $returnArr;
}
?>