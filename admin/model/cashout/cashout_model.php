<?php


function getAllCashoutData($offset, $limit, $conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail, $actionEmail,$agentEmail) {
    $returnArr = array();
    global $blanks;

    $query = "SELECT * from {$tablename}";

    if($userEmail!="" || $walletAddress!="" || $requestStatus!="" || $country!="" || $paymentMode!="" || $adminEmail!="" || $actionEmail!="" || $agentEmail!="") {
        $query .= " where 1=1 ";
    }

    if ($userEmail!="") {
        $query .= " AND  user_email LIKE '%" . $userEmail . "%'";
    }

    if ($agentEmail!="") {
        $query .= " AND  assigned_to LIKE '%" . $agentEmail . "%'";
    }

    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  status='" . $requestStatus . "'";
    } 

    if ($actionEmail!="") {
        $query .= " AND  actioned_by='" . $actionEmail . "'";
    }

    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }


    $query .= " ORDER by request_date DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            //$res[array_shift($row)] = $row;
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllCashoutCount($conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail, $actionEmail ,$agentEmail)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from {$tablename}";

    if($userEmail!="" || $walletAddress!="" || $requestStatus!="" || $country!="" || $paymentMode!="" || $adminEmail!="" || $actionEmail!="" || $agentEmail!="")
    {
        $query .= " where 1=1 ";
    }

    if ($userEmail!="") {
        $query .= " AND  user_email LIKE '%" . $userEmail . "%'";
    }

    if ($agentEmail!="") {
        $query .= " AND  assigned_to LIKE '%" . $agentEmail . "%'";
    }

    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus!="") {
        $query .= " AND  status='" . $requestStatus . "'";
    } 

    if ($actionEmail!="") {
        $query .= " AND  actioned_by='" . $actionEmail . "'";
    }

    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($paymentMode!="") {
        $query .= " AND  payment_mode='" . $paymentMode . "'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


/* To Update cashout request in table to approve, hold and reject. */
function CheckCashoutRequestStatus($RequestedId, $tableName, $conn) {
    $returnArr = array();
    $query     = '';
    
    global $blanks;
    if($tableName != "" || $RequestedId != "") {
        $query = "SELECT count(*) as count From ".$tableName." WHERE id = '".$RequestedId."' AND status LIKE '%reject%'";   
    }
    
    // echo "QUERY : ".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        if ($res[0]['count'] > 0) {
            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = $res;
        } else {
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $res;
        }
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


/* To Update cashout request in table to approve, hold and reject. */
function updateCashoutRequest($RequestedId, $tableName, $updatingStatus, $actionPerformedBy, $actionMessage, $conn) {
    $returnArr = array();
    $query     = '';
    
    global $blanks;
    if($tableName != "" || $updatingStatus != "" || $actionPerformedBy != "") {
        $query = "UPDATE {$tableName} SET status='" . $updatingStatus . "' , actioned_by='" . $actionPerformedBy . "' ";   
    }
    if ($actionMessage != "") {
        $query .=  ", comment='" . $actionMessage . "'";
    }
    if ($query != "") {
        $query .=  " WHERE id='" . $RequestedId . "'";   
    }
    // echo "QUERY : ".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            //$res[array_shift($row)] = $row;
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


/* To Get cashout request details. */
function GetCashOutDetail($RequestedId, $tableName, $conn) {

    $returnArr = array();
    global $blanks;
    if($tableName != "") {
        $query = "SELECT * FROM {$tableName}";      
    }

    if($RequestedId != "") {
        $query .= " WHERE 1=1 ";
    }

    if($RequestedId != "") {
        $query .= " AND  id='" . $RequestedId . "'";
    }

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            //$res[array_shift($row)] = $row;
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


/********************************************* for all cashout ****************************************/
function getAllCashoutDataSearch($offset, $limit, $conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from {$tablename}";

    if($userEmail!="" || $walletAddress!="" || $requestStatus!="" || $country!="" || $paymentMode!="" || $adminEmail!="")
    {
        $query .= " where 1=1 ";
    }

    if ($userEmail!="") {
        $query .= " AND  user_email='" . $userEmail . "'";
    }
    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus!="") {
       // $query .= " AND  status='" . $requestStatus . "'";
        $query .= " AND  status like '%" . $requestStatus . "%'";
    }

    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }


    $query .= " ORDER by request_date DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            //$res[array_shift($row)] = $row;
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllCashoutCountSearch($conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from {$tablename}";

    if($userEmail!="" || $walletAddress!="" || $requestStatus!="" || $country!="" || $paymentMode!="" || $adminEmail!="")
    {
        $query .= " where 1=1 ";
    }

    if ($userEmail!="") {
        $query .= " AND  user_email='" . $userEmail . "'";
    }
    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus!="") {
        // $query .= " AND  status='" . $requestStatus . "'";
        $query .= " AND  status like '%" . $requestStatus . "%'";
    }
    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($paymentMode!="") {
        $query .= " AND  payment_mode='" . $paymentMode . "'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function getAllCashoutAgentEmailData($offset, $limit, $conn, $email, $cashoutAdminArray) {
    $returnArr = array();
    global $blanks, $coagent, $defaultAdmin;

    $query = "SELECT admin_id, admin_email, admin_first_name, admin_last_name from admin_user WHERE group_id LIKE '%{$coagent}%' AND admin_email != '". $defaultAdmin ."'";

     if ($email!="") {
        $query .= " AND admin_email LIKE '" . $email . "%' AND admin_status = '1'";
    }
    if (!empty($cashoutAdminArray)) {
        $query .= " AND admin_email NOT LIKE '".$cashoutAdminArray[0]."'";
    }

    $query .= " ORDER by admin_email DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;

        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllCashoutAgentEmailDataCount($conn,$email,$cashoutAdminArray)
{

    $returnArr = array();
    global $blanks, $coagent, $defaultAdmin;
    $query = "SELECT count(*) as count from admin_user WHERE group_id LIKE '%{$coagent}%' AND admin_email != '". $defaultAdmin ."'";

   if($email!="")
   {
       $query .= " AND 1=1 ";
   }

    if ($email!="") {
        $query .= " AND  admin_email LIKE '" . $email . "%' AND admin_status = '1'";
    }

    if (!empty($cashoutAdminArray)) {
        $query .= " AND admin_email NOT LIKE '".$cashoutAdminArray[0]."'";
    }

    // echo "<br>".$query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function assignCashRequestToAgent($id, $agentEmail, $tableName, $conn) {
    $extraArg  = array();
    $returnArr = array();
    $query     = "UPDATE {$tableName} SET assigned_to='" . $agentEmail . "' WHERE id='" . $id . "' ;";
    
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $errMsg = "Ticket Assigned Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 66, null, $extraArg);
    }
    return $returnArr;
}
/********************************************* for all cashout ****************************************/


/******************************************* all agent *************************************************/
function getAllCashoutDataAllAgent($offset, $limit, $conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail, $actionEmail,$agentEmail) {
    $returnArr = array();
    global $blanks;

    $query = "SELECT * from {$tablename} where 1=1";

    if ($userEmail!="") {
        $query .= " AND  user_email LIKE '%" . $userEmail . "%'";
    }

    if ($agentEmail!="") {
        $query .= " AND  assigned_to LIKE '%" . $agentEmail . "%'";
    }else
    {
        $query .= " AND assigned_to <> ''";
    }

    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus=="") {
        //$query .= " AND  status='" . $requestStatus . "'";
        $query .= " AND  status IN ('pending_for_uam_approval','pending_for_hod_approval','pending_for_mm_approval','pending_for_sm_approval','hold_by_uam','hold_by_hod','hold_by_mm','hold_by_sm')";
    }

    if ($actionEmail!="") {
        $query .= " AND  actioned_by='" . $actionEmail . "'";
    }

    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }


    $query .= " ORDER by request_date DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

     //echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            //$res[array_shift($row)] = $row;
            $res[] = $row;

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}



function getAllCashoutCountAllAgent($conn, $tablename,$userEmail,$walletAddress,$requestStatus,$country,$paymentMode,$adminEmail, $actionEmail ,$agentEmail)
{

    $returnArr = array();
    global $blanks;
    $query = "SELECT count(*) as count from {$tablename} where 1=1";


    if ($userEmail!="") {
        $query .= " AND  user_email LIKE '%" . $userEmail . "%'";
    }

    if ($agentEmail!="") {
        $query .= " AND  assigned_to LIKE '%" . $agentEmail . "%'";
    }else
    {
        $query .= " AND assigned_to <> ''";
    }

    if ($walletAddress!="") {
        $query .= " AND  payout_address='" . $walletAddress . "'";
    }

    if ($requestStatus=="") {
        //$query .= " AND  status='" . $requestStatus . "'";
        $query .= " AND  status IN ('pending_for_uam_approval','pending_for_hod_approval','pending_for_mm_approval','pending_for_sm_approval','hold_by_uam','hold_by_hod','hold_by_mm','hold_by_sm')";
    }

    if ($actionEmail!="") {
        $query .= " AND  actioned_by='" . $actionEmail . "'";
    }

    if ($country!="") {
        $query .= " AND  meta_details like '%" . $country . "%'";
    }

    if ($paymentMode!="") {
        $query .= " AND  payment_mode='" . $paymentMode . "'";
    }

    if ($adminEmail!="") {
        $query .= " AND  assigned_to='" . $adminEmail . "'";
    }

    // echo $query;
    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $res[0]["count"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}
/******************************************* all agent *************************************************/



/**
 *   Function Name :   addBlockedPendingWithdrawal()
 *   Purpose       :   This function adds cashout amount in pending withdrawal.
 *   Arguments     :   (string) $amountInITD, (string)$userId, (string)$userEmail
 */
function addDeductBlockedPendingWithdrawal($amountInITD,$userId,$userEmail,$action){

    global $walletPublicKey, $mode, $walletURL;
    global $descriptionArray;
    $retArray = array();
    $headers = array();
    $attr = 'blockedpendingwithdrawals';
    if (!empty($amountInITD) && !empty($userId) && !empty($userEmail) && !empty($action)) {

        //create signature
        $apiText = "user_id={$userId}&amount={$amountInITD}&attr={$attr}&action={$action}&publicKey={$walletPublicKey}";

        $postFields = $apiText;
        $apiName = "user/{$userId}/blockedpendingwithdrawals/{$action}";
        $requestUrl = "{$walletURL}";
        $curl_type = 'PUT';
        $content_type = "content-type: application/json";
        $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);
        if ($mode == 'production') {
            $nonce_result = generateSignatureWithNonce($apiText);
            if (noError($nonce_result)) {
                $nonce_result = $nonce_result['errMsg'];
                $retArray['errCode'] = -1;
                //$retArray['errMsg'] = $nonce_result['errMsg'];
                $headers[] = "x-ts: {$nonce_result['timestamp']}";
                $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
            } else {
                $retArray['errCode'] = 2;
                $retArray['errMsg'] = $nonce_result['errMsg'];
            }
        }

        $respArray = curlRequest($curlReqParam, $headers);
        if (noError($respArray)) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = -1;
            $retArray['errMsg'] = $respArray['errMsg'];
            $retArray['mode'] = $mode;

        } else if ($respArray["errCode"] != 73) {
            if ($mode == 'production') {
                $nonce_counter = incrementVectorCounter();
            }
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        } else {
            $retArray['errCode'] = $respArray['errCode'];
            $retArray['errMsg'] = $respArray['errMsg'];
        }
    } else {
        $retArray["errCode"] = 2;
        $retArray["errMsg"] = "All fields are mandatory";
    }

    return $retArray;
}

function rejectManualCashoutRequest($id,$tablename,$conn)
{
    $returnArr = array();
    global $walletURLIPnotification;
    $query = "SELECT payment_amount,cashout_fees,user_id,user_email from {$tablename} where id={$id}";
    $result = runQuery($query, $conn);

    if (noError($result)) {
       $row = mysqli_fetch_assoc($result["dbResource"]);
        //$amount = $row['payment_amount'] + $row['cashout_fees'];
        $amount = $row['payment_amount'];
        $user_id=$row['user_id'];
        $userEmail=$row['user_email'];
        $result = addDeductBlockedPendingWithdrawal($amount, $user_id, $userEmail, "deduct");


        $userFields = "first_name,last_name,mobile_number";
        $getUserDataDetails = getUserInfo($userEmail, $walletURLIPnotification . 'api/notify/v2/', $userFields);

        $firstName = $getUserDataDetails['errMsg']['first_name'];
        $lastName = $getUserDataDetails['errMsg']['last_name'];
        $mobileNumber = $getUserDataDetails['errMsg']['mobile_number'];
        $smsText="";

        $mailSubject = 'Your Cashout Request has been Rejected';
        $emailBody = '<br>Dear User,<br> Yor Cashout Request has been rejected.<br>';
        $notificationBody = "Your Cashout Request has been Rejected.";

        $preferenceCode = 2;
        $category = "wallet";
        $sendMail = sendNotification($userEmail,$mailSubject,$emailBody,$firstName,$lastName,$user_id,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

    }

    return $result;
}
?>