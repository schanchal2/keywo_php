<?php
//name to id convert apps
function setTemplate($tempID,$subject,$content, $conn){
  $extraArg = array();
  $returnArr = array();
  if(!empty($tempID)){
    $query = "UPDATE template SET subject = '".$subject."', content = '".$content."' WHERE  id = $tempID";
  }else{
   $query = "INSERT INTO template(subject, content) value('$subject','$content')";
  }
  $result = runQuery($query, $conn);
  if (noError($result)) {
      $errMsg = "Message Added Successfully.";
      $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
  } else {
      $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
  }
  return $returnArr;
}

function getAllTemplateRequest($offset, $limit,$id,$conn)
{

    $returnArr = array();
    global $blanks;

    $query = "SELECT * from template";


    if($id!="" )
    {
        $query .= " where id = $id ";
    }

    $query .= " ORDER by id DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($limit, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $limit;
    }

    $result = runQuery($query, $conn);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            if(!empty($id)){
                $res = $row;
            }else{
                $res[array_shift($row)] = $row;
            }
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = $res;
    } else {
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}


function deleteTemplateData($id, $conn) {
    $returnArr = array();
    $query = "DELETE FROM template WHERE id='".$id."';";
    $result = runQuery($query, $conn);
    if (noError($result)) {
        $res = array();
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $result["errMsg"];
    } else {
        $returnArr["errCode"] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

?>