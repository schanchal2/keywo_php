<?php

function getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $logpath, $finalrooturl)
{

    if ($fromdateinput) {

        $fromdatenew = explode("-", trim($fromdateinput));


        $starttime = 0;


        $startyear = $fromdatenew["0"];
        $startmonth = $fromdatenew["1"];
        $startdate = $fromdatenew["2"];


        $todateenew = explode("-", trim($todateinput));

        $endtime = 24;


        $endyear = $todateenew["0"];
        $endmonth = $todateenew["1"];
        $enddate = $todateenew["2"];

        $startmonthnew = $startmonth;
        $xmlstring = array();
        $hourfiles = array();
        for ($i = $startyear; $i <= $endyear; $i++) {
            for ($j = $startmonthnew; $j < 13; $j++) {
                $number = cal_days_in_month(CAL_GREGORIAN, $j, $i);

                for ($k = $startdate; $k <= $number; $k++) {




                    for ($l = $starttime; $l <= 23; $l++) {


                        if (strlen($j) == 1) {
                            $j = "0" . $j;
                        }

                        if (strlen($k) == 1) {
                            $k = "0" . $k;
                        }

                        if (strlen($l) == 1) {
                            $l = "0" . $l;
                        }

                        $rootUrlnew = $logpath . $i . "/" . $j . "/" . $k;
                        $data = scandir($rootUrlnew);

                        $filenames = $l . $filenameread;
                        $name = $l . $filenameread;


                        $URLs = $finalrooturl . $i . "/" . $j . "/" . $k . "/" . $filenames;

                        if (in_array($name, $data)) {

                            $segs = loadMYxml($URLs, $emailuser);


                            foreach ($segs as $value) {
                                array_push($xmlstring, $value);
                            }
                       }


                        if ($l == 23) {
                            $starttime = 0;
                            break;
                        }

                        if ($enddate == $k) {
                            if ($l == $endtime) {
                                break;
                            }
                        }


                    }


                    if ($k == $number) {
                        $startdate = 1;
                        break;
                    }


                    if ($endmonth == $j) {
                        if ($k == $enddate) {
                            break;
                        }
                    }


                }

                if ($j == 12) {
                    $startmonthnew = 1;
                    break;
                }


                if ($endyear == $i) {
                    if ($j == $endmonth) {
                        break;
                    }
                }


            }


        }

        return array_reverse($xmlstring);
    }
}

function loadMYxml($file, $emailuser)
{
    if (simplexml_load_file($file)) {
        $use_errors = libxml_use_internal_errors(true);
        $data = simplexml_load_file($file);
        if ($emailuser !== "") {
            $data = $data->xpath("//activity[@email=\"{$emailuser}\"]");
        }
        libxml_clear_errors();
        libxml_use_internal_errors($use_errors);
        return $data;
    } else {
        echo '<span style="color:red;font-weight: bold;font-size: medium">Error in XML File Please Repair File:' . $file . '</span><br/>';
    }
}
?>