<?php
function loadDepartmentWiseCashoutModules($department)
{
    global $adminRoot;

    if(in_array("coagent",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/cashout/uam/uamAgent/couamAgent.php'>CO-Agent</a></li>";
    }


    if(in_array("cohod",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/cashout/uam/uamHod/couamHod.php'>CO-HOD</a></li>";
    }

    if(in_array("MM",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/cashout/mm/comm.php'>MM</a></li>";
    }

    if(in_array("SM",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/cashout/sm/cosm.php'>SM</a></li>";
    }

}


function loadDepartmentWiseServiceRequestModules($department)
{

    global $adminRoot;

    if(in_array("sragent",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/ServiceRequest/agents/sragents.php'>AGENT</a></li>";
    }

    if(in_array("srhod",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/ServiceRequest/uam-hod/sruam-hod.php'>HOD</a></li>";
    }

    if(in_array("MM",$department) || in_array("SuperAdmin",$department))
    {
        echo " <li><a href='{$adminRoot}view/ServiceRequest/mm/srmm.php'>MM</a></li>";
    }

    if(in_array("SM",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/ServiceRequest/sm/srsm.php'>SM</a></li>";
    }


}

function loadDepartmentWiseCustomerSupportModules($department)
{
    global $adminRoot;

    if(in_array("csagent",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/customerSupport/hod_agent/csagent.php'>CS-AGENT</a></li>";
    }

    if(in_array("cshod",$department) || in_array("SuperAdmin",$department))
    {
        echo "<li><a href='{$adminRoot}view/customerSupport/hod_agent/cshod.php'>CS-HOD</a></li>";
    }


}