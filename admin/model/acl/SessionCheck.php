<?php

if(!isset($_SESSION))
{
    session_start();
}
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

//start config
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
//end config

//start helper
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
//end helper

//other
require_once("{$docrootpath}core/errorMap.php");
require_once("{$docrootpath}model/acl/acl_model.php");
//end other

global $adminRoot;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$adminEmail=strtolower($_SESSION["admin_id"]);


if($adminEmail!="")
{
    $data=getAdminData($_SESSION["admin_id"], $connAdmin);
    $dataEmail=strtolower($data["errMsg"]["admin_email"]);

    isipaddresshasaccess($connAdmin);

    if($adminEmail!=$dataEmail)
    {
        $redirectURL = $adminRoot . "index.php";
        echo '<script>window.location.replace("'.$redirectURL.'");</script>';
        exit;
    }
}else
{
    isipaddresshasaccess($connAdmin);
    $redirectURL = $adminRoot . "index.php";
    echo '<script>window.location.replace("'.$redirectURL.'");</script>';
    exit;
}

?>