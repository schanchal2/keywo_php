<?php

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    require_once("{$docrootpath}config/config.php");
    require_once("{$docrootpath}helpers/deviceHelper.php");
    require_once("{$docrootpath}model/acl/acl_model.php");


    $connAdmin = createDBConnection("acl");
    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


    $largesttpermission = array();

    $email = $_SESSION["admin_id"];
    $result = getAdminData($email, $connAdmin);

    $result = $result["errMsg"];
    $gps = $result["group_id"];
    $userp = $result["rights"];
    array_push($largesttpermission, $userp);
    $groups = explode(" ", $gps);
    $superadmin = "superadmin";


    $datatt1 = getAdminData($email, $connAdmin);
    $datatt2 = $datatt1["errMsg"];
    $datatt = $datatt2["group_id"];


    $permsiion = array();
    $modulestt = array();
    $modulesSubs = array();
    $groupnamess = array();
    $resultsss = explode(" ", $datatt);


    foreach ($resultsss as $res) {

        $result = selectGroups($res, $connAdmin);
        $resultnew = $result["errMsg"];
        $rightss = $resultnew["group_rights"];
        $group_name = $resultnew["group_name"];
        $modules = $resultnew["right_on_module"];
        $modulesSub = $resultnew["right_on_submodule"];
        array_push($permsiion, trim($rightss));
        array_push($modulestt, trim($modules));
        array_push($modulesSubs, trim($modulesSub));
        array_push($groupnamess, trim($group_name));

    }


    function getMypermission()
    {
        global $groupnamess;
        return $groupnamess;
    }


    $rightsnew = $datatt2["rights"];
    array_push($permsiion, trim($rightsnew));

    /* printArr($permsiion);
     $c = array_count_values($permsiion);
     $val = array_search(max($c), $c);
     $largest=$val;*/
    //$largest = max($permsiion);

    $modulesttdecoded = array();
    foreach ($modulestt as $value) {
        $result = json_decode($value, true);

        foreach ($result as $data) {
            array_push($modulesttdecoded, trim($data));
        }

    }

    $pages

        = array();


    foreach ($modulesSubs as $values) {


        $modulesSubs = explode(",", $values);

        foreach ($modulesSubs as $val) {
            array_push($pages, trim($val));
        }
    }

    $mymodules = array();


    foreach ($modulestt as $val) {
        $modulestt = json_decode($val, true);


        foreach ($modulestt as $val) {
            array_push($mymodules, trim($val));
        }
    }


    $groupnamess = getMypermission();

    $links = $_SERVER['PHP_SELF'];
    $link_arrayy = explode('/', $links);
    $mypagepage = trim(end($link_arrayy));
    foreach ($groupnamess as $values) {

        $result = checkgroupadded(trim($values), $connAdmin);
        $result = $result["errMsg"];
        $modulepages = explode(",", $result["right_on_submodule"]);
        foreach ($modulepages as $mypage) {
            if ($mypagepage == trim($mypage)) {
                $result = checkgroupadded(trim($values), $connAdmin);
                $result = $result["errMsg"];
                $resultname = $result["group_rights"];
                array_push($largesttpermission, trim($resultname));
                $moduleright = $result["right_on_module"];

            }

        }
    }


    $largest = max($largesttpermission);
    function mypermissions($largest)
    {

        define('CAN_READ', 1 << 0);   // 000001 CAN_WRITE 1
        define('CAN_WRITE', 1 << 1); // 000010 2
        define('CAN_EXECUTE', 1 << 2);   // 000100 4

        $permission = str_split($largest);

        foreach ($permission as $per) {
            if ($per == 1) {
                $read = CAN_READ;
            }

            if ($per == 2) {
                $write = CAN_WRITE;
            }

            if ($per == 4) {
                $execute = CAN_EXECUTE;
            }
        }


        $individualpermission = $read + $write + $execute;

        function isCanRead($sRule)
        {
            return ($sRule & CAN_READ) ? 'read' : '';
        }

        function isCanwrite($sRule)
        {
            return ($sRule & CAN_WRITE) ? 'write' : '';
        }

        function isCanAll($sRule)
        {
            return ($sRule & CAN_EXECUTE) ? 'execute' : '';
        }


        $sRule = $individualpermission;

        $myp = array();


        $sRuleread = isCanRead($sRule);
        if (!empty($sRuleread)) {
            array_push($myp, $sRuleread);
        }
        $sRulewrite = isCanwrite($sRule);
        if (!empty($sRulewrite)) {
            array_push($myp, $sRulewrite);
        }
        $sRuleexe = isCanAll($sRule);
        if (!empty($sRuleexe)) {
            array_push($myp, $sRuleexe);
        }


        return $myp;
    }


} else {

    $link = $adminView . "index.php";

    header("Location:" . $link);

}
?>