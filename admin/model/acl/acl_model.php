<?php


error_reporting(0);

function ValidateAdminLogin($email, $hash, $adminStatus, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from admin_user where admin_email='" . $email . "' and admin_password_hash='" . $hash . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 8, null, $extraArg);

    }

    return $returnArr;

}

function updateToken($email, $token, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $query = "UPDATE admin_user SET auth_token='" . $token . "' WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);
    if (noError($result)) {
        $errMsg = "User Password updation sucess";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 9, null, $extraArg);
    }
    return $returnArr;
}

function addipaddress($ipaddress,$ipOwner, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $query = "INSERT INTO `ip_access`(`id`, `ip_address`,`ip_owner`) VALUES (0,'" . $ipaddress . "','" . $ipOwner . "');";
    $result = runQuery($query, $connAdmin);
    if (noError($result)) {
        $errMsg = "IP Address Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 10, null, $extraArg);
    }
    return $returnArr;

}

function getipaddress($ip, $connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from ip_access where ip_address='" . $ip . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 11, null, $extraArg);

    }

    return $returnArr;

}

function getallipaddress($connAdmin)
{


    $returnArr = array();
    $extraArg = array();

    $query = "select * from ip_access order by id desc";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 12, null, $extraArg);

    }

    return $returnArr;

}

function deleteip($id, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $query = "DELETE FROM `ip_access` WHERE id='" . $id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {


        $errMsg = "IP Address Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 13, null, $extraArg);
    }
    return $returnArr;
}

function activateUser($email, $hash, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $hash = sha1Md5DualEncryption($hash);

    $query = "UPDATE admin_user SET admin_password_hash='" . $hash . "',admin_status='1' WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Password updation sucess";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 9, null, $extraArg);
    }
    return $returnArr;
}

function updateLoginTime($email,$connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "UPDATE admin_user SET last_login_time=NOW() WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Password updation sucess";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 9, null, $extraArg);
    }
    return $returnArr;
}

function updateLogoutTime($email,$connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "UPDATE admin_user SET last_logout_time=NOW() WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Password updation sucess";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 9, null, $extraArg);
    }
    return $returnArr;
}


function changeAdminProfilePic($email, $imageName, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $query = "UPDATE admin_user SET admin_profile_pic='" . $imageName . "' WHERE admin_email='" . $email . "' ;";
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $adminRootURLProPic = "../uploads/adminProfilePics/";
        $adminRootURLProPictt = $adminRootURLProPic . $_SESSION["admin_profile_pic"];
        if ($_SESSION["admin_profile_pic"] != "admin.jpg") {
            unlink($adminRootURLProPictt);
        }

        if ($_SESSION["admin_profile_pic"] != "admin.jpg") {
            unset($_SESSION["admin_profile_pic"]);
        }
        $_SESSION["admin_profile_pic"] = $imageName;
        $errMsg = "Profile Picture Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 14, null, $extraArg);
    }
    return $returnArr;
}

function addNewAdmin($email, $fname, $lname, $randomnumber, $group, $token, $connAdmin)
{
    $extraArg = array();
    $returnArr = array();
    $randomnumber = sha1Md5DualEncryption($randomnumber);
    $query = "INSERT INTO admin_user (`admin_email`, `admin_first_name`, `admin_last_name`, `admin_password_hash`, `admin_profile_pic`, `user_creation_date`, `rights`, `group_id`, `rights_on_module`, `admin_status`,`auth_token`) VALUES ('" . $email . "','" . $fname . "','" . $lname . "','" . $randomnumber . "','admin.jpg',NOW(),'1','" . $group . "','','0','" . $token . "');";


    $result = runQuery($query, $connAdmin);


    if (noError($result)) {

        $errMsg = "New User Added Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 15, null, $extraArg);
    }
    return $returnArr;
}


function getAllAdminPagination($offset, $no_of_rows = 10, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $returnArr = array();
    global $blanks;

    $query = "SELECT * FROM admin_user";

    $query .= " ORDER by user_creation_date DESC";
    if ((!in_array($offset, $blanks)) && (!in_array($no_of_rows, $blanks))) {
        $query .= " LIMIT " . $offset . "," . $no_of_rows;
    }

    // echo $query;
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 16, null, $extraArg);
    }


    return $returnArr;
}


function getAdminData($email, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from admin_user where admin_email='" . $email . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 17, null, $extraArg);

    }

    return $returnArr;

}


function getallmodules($connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select module_name from modules";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 18, null, $extraArg);

    }

    return $returnArr;

}


function submitUserdatagroup($submodulename, $groupname, $modulename, $permsissions, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $data = getPageListtoAccess($submodulename, $connAdmin);
    $subpages = implode(",", $data);
    $query = "INSERT INTO `groups`(`group_name`, `group_rights`, `right_on_module`,`right_on_submodule`) VALUES ('" . $groupname . "','" . $permsissions . "','" . $modulename . "','" . $subpages . "')";
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Group Data Submitted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 19, null, $extraArg);
    }
    return $returnArr;


}

function getPagesinModule($modulename, $connAdmin)
{

    $allmod = array();
    foreach ($modulename as $value) {
        $query = "select module_pages from modules where module_name = '" . $value . "'";
        $result = runQuery($query, $connAdmin);

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $res = $res["module_pages"];
        $data = explode(",", $res);

        foreach ($data as $value) {
            array_push($allmod, $value);
        }

    }

    return $allmod;

}

function getPageListtoAccess($data, $connAdmin)
{

    $allmod = array();
    $result = getPageMapping($connAdmin);
    $result = $result["errMsg"];

    foreach ($data as $value) {
        foreach ($result as $newdata) {
            $module_pagename = $newdata["module_pagename"];
            $module_virtualpagename = $newdata["module_virtualpagename"];

            if ($value == $module_virtualpagename) {
                $submodulename = $module_pagename;
                array_push($allmod, $submodulename);
            }

        }

    }

    return $allmod;

}

function deleteAdmin($email, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "DELETE FROM `admin_user` WHERE admin_email='" . $email . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "Admin User Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 20, null, $extraArg);
    }
    return $returnArr;
}

function deleteGroup($id, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();
    $mygrouptodelete = getGroupName($id, $connAdmin);
    $mygrouptodelete = $mygrouptodelete["errMsg"][0];
    $mygrouptodelete = $mygrouptodelete["group_name"];

    $query = "DELETE FROM `groups` WHERE group_id='" . $id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        /********************************** change group detail after delete group *************************************/


        $getallgroupsid = getallgroupsid($connAdmin);
        $getallgroupsid = $getallgroupsid["errMsg"];

        foreach ($getallgroupsid as $value) {
            $usergroup = $value["group_id"];
            $usergroup = explode(" ", $usergroup);

            foreach ($usergroup as $groupname) {
                if ($mygrouptodelete == $groupname) {
                    $admin = $value["admin_email"];
                    $admindata = getAdminData($admin, $connAdmin);
                    $admindata = $admindata["errMsg"];
                    $admindata = $admindata["group_id"];
                    $admindata = explode(" ", $admindata);

                    $key = array_search($mygrouptodelete, $admindata);
                    if ($key !== false) {
                        unset($admindata[$key]);
                    }

                    $groupdata = implode(" ", $admindata);

                    $result = editAdminGroupAfterDelete($admin, $groupdata, $connAdmin);

                }
            }


        }

        /********************************** change group detail after delete group *************************************/

        /*****************************************************************************************/

        $errMsg = "Group Deleted Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 21, null, $extraArg);
    }

    return $returnArr;
}

function editAdminGroupAfterDelete($email, $groupdata, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET group_id='" . $groupdata . "' WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);


    if (noError($result)) {
        $errMsg = "Group Edited Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 22, null, $extraArg);
    }
    return $returnArr;
}

function getallgroupsid($connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select admin_email,group_id from admin_user";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 23, null, $extraArg);

    }

    return $returnArr;

}


function getGroupName($id, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select group_name from groups where group_id='" . $id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


    } else {
        $returnArr = setErrorStack($returnArr, 24, null, $extraArg);
    }


    return $returnArr;

}

function getGroupID($name, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select group_id,group_rights from groups where group_name='" . $name . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);


    } else {
        $returnArr = setErrorStack($returnArr, 25, null, $extraArg);
    }


    return $returnArr;

}


function changePassword($email, $newp, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET admin_password_hash='" . $newp . "' WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Password Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 26, null, $extraArg);
    }
    return $returnArr;
}


function updateuser($email, $fname, $lname, $group, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "UPDATE admin_user SET admin_first_name='" . $fname . "',admin_last_name='" . $lname . "',group_id='" . $group . "' WHERE admin_email='" . $email . "' ;";

//    unset($_SESSION["mygroups"]);
//    $_SESSION["mygroups"]=$group;
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $errMsg = "User Data Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 27, null, $extraArg);
    }
    return $returnArr;
}


function editAdminrights($email, $rights, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET rights='" . $rights . "' WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Rights Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 28, null, $extraArg);
    }
    return $returnArr;
}


function checkgroupadded($groupname, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select * from groups where group_name='" . $groupname . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 25, null, $extraArg);

    }

    return $returnArr;

}

function selectGroups($groups, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from groups where group_name='$groups'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 25, null, $extraArg);

    }

    return $returnArr;

}


function setUserPermissions($email, $permission, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET rights='" . $permission . "' WHERE admin_email='" . $email . "' ;";
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Permissions Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 28, null, $extraArg);

    }
    return $returnArr;
}


function getallgroups($connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select * from groups";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 25, null, $extraArg);

    }

    return $returnArr;

}

function getModules($modulename, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select module_pages from modules where module_name='" . $modulename . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 18, null, $extraArg);

    }

    return $returnArr;

}


function ispagehasaccess($pages, $connAdmin)
{
    isipaddresshasaccess($connAdmin);
    $link = $_SERVER['PHP_SELF'];
    $link_array = explode('/', $link);
    $page = end($link_array);


    if (!in_array($page, $pages)) {
        echo "<script type=\"text/javascript\">location.href = 'dashboard.php';</script>";
        // header('Location: dashboard.php');
    }
}

function getGroup($id, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "select * FROM `groups` WHERE group_id='" . $id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 24, null, $extraArg);
    }
    return $returnArr;
}


function editUserdatagroup($submodulename, $id, $groupname, $modulename, $permsissions, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $data = getPageListtoAccess($submodulename, $connAdmin);
    $subpages = implode(",", $data);

    $query = "UPDATE groups SET group_name='" . $groupname . "',group_rights='" . $permsissions . "',right_on_module='" . $modulename . "',right_on_submodule='" . $subpages . "' WHERE group_id='" . $id . "' ;";
    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        /***************************** change user permission if group permission changed ************************************/
        $mygrouptodelete = getGroupName($id, $connAdmin);
        $mygrouptodelete = $mygrouptodelete["errMsg"][0];
        $mygrouptodelete = $mygrouptodelete["group_name"];


        $data = getAllAdmins($connAdmin);
        $admin = $data["errMsg"];


        $permsiion = array();
        foreach ($admin as $adminuser) {
            $group_idAdmin = $adminuser["group_id"];
            $group_idAdminEmail = $adminuser["admin_email"];
            $group_idAdmin = explode(" ", $group_idAdmin);


            foreach ($group_idAdmin as $adminusergroup) {
                if (strtolower($adminusergroup) != strtolower("SuperAdmin")) {
                    if (strtolower($adminusergroup) == strtolower($mygrouptodelete)) {
                        $mygrouptodelete = getGroupID($adminusergroup, $connAdmin);
                        $mygrouptodelete = $mygrouptodelete["errMsg"][0];
                        $mygrouptodelete = $mygrouptodelete["group_rights"];

                        array_push($permsiion, $mygrouptodelete);


                    }
                }
            }

            if ($permsiion) {
                $largest = max($permsiion);

                setUserPermissions($group_idAdminEmail, $largest, $connAdmin);

                unset($foo);
            }

        }

        /***************************** change user permission if group permission changed ************************************/
        $errMsg = "User Permissions Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 28, null, $extraArg);
    }
    return $returnArr;


}

function getAllAdmins($connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select admin_email,group_id from admin_user";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 16, null, $extraArg);
    }
    return $returnArr;
}

function isAdminExists($email, $connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $returnArr = array();

    $query = "select * from admin_user where admin_email='" . $email . "'  and admin_status='1'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 17, null, $extraArg);
    }
    return $returnArr;
}


function isipaddresshasaccess($connAdmin)
{

    $result = getallipaddress($connAdmin);
    $datas = $result["errMsg"];


    $ips = array();


    foreach ($datas as $value) {
        array_push($ips, $value["ip_address"]);
    }


    $clientip = getClientIP();
    global $adminView;

    $deniedPage = $adminView . "acl/denied.php";
    if (!in_array($clientip, $ips)) {
        echo "<script type=\"text/javascript\">location.href = '" . $deniedPage . "';</script>";

    }
}


function getsubmodules($submodule, $connAdmin)
{

    $allmod = array();
    foreach ($submodule as $value) {
        $query = "select submodule_virtual_name from modules where module_name='" . $value . "'";
        $result = runQuery($query, $connAdmin);

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $res = $res["submodule_virtual_name"];
        $data = explode(",", $res);

        foreach ($data as $value) {
            array_push($allmod, $value);
        }


    }
    return $allmod;

}

function getPageList($data, $connAdmin)
{

    $allmod = array();

    $result = getPageMapping($connAdmin);
    $result = $result["errMsg"];

    foreach ($data as $value) {
        foreach ($result as $newdata) {
            $module_pagename = $newdata["module_pagename"];
            $module_virtualpagename = $newdata["module_virtualpagename"];

            if ($value == $module_pagename) {
                $submodulename = $module_virtualpagename;
                array_push($allmod, $submodulename);
            }

        }

    }

    return $allmod;

}


function getPageMapping($connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select module_pagename,module_virtualpagename from page_mapping";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            array_push($res, $row);
        }

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 29, null, $extraArg);
    }
    return $returnArr;
}

function getAllRoles($connAdmin)
{

    $returnArr = array();
    $extraArg = array();

    $query = "select * from groups";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 30, null, $extraArg);
    }
    return $returnArr;
}


function editAdminInfoProfilenew($email, $firstname, $lastname, $mobileno, $country, $selectState, $citySelect, $address, $zip, $gender, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();
    $query = "UPDATE admin_user SET admin_first_name='" . $firstname . "',admin_last_name='" . $lastname . "',mobile_no='" . $mobileno . "',country='" . $country . "',state='" . $selectState . "',city='" . $citySelect . "',address='" . $address . "',pincode='" . $zip . "',gender='" . $gender . "'  WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        unset($_SESSION['admin_name']);

        $name = ucfirst($firstname) . " " . ucfirst($lastname);
        $_SESSION['admin_name'] = $name;

        $errMsg = "Profile Picture Updated Successfully.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 14, null, $extraArg);
    }
    return $returnArr;
}


function getipbyID($id, $connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select ip_address,ip_owner from ip_access where id='" . $id . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            array_push($res, $row);
        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 11, null, $extraArg);
    }
    return $returnArr;
}

function updateLastActivityTime($email,$connAdmin)
{
    $extraArg = array();
    $returnArr = array();

    $query = "UPDATE admin_user SET last_activity_time=NOW() WHERE admin_email='" . $email . "' ;";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {

        $errMsg = "User Time updation sucess";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 9, null, $extraArg);
    }
    return $returnArr;
}


function getAdminGroup($connAdmin)
{
    $returnArr = array();
    $extraArg = array();

    $query = "select group_id from admin_user where admin_id='" . $_SESSION["agent_id"] . "'";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            $res = $row;

        $errMsg = $res;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    } else {

        $returnArr = setErrorStack($returnArr, 25, null, $extraArg);

    }

    return $returnArr;

}

?>