<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Cashouts</h1><span>Auto+ Manual Packing List Summary</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="row m-b-20">
              <div class="col-xs-4">
                <div class="input-group">

                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <input type="text" id="userListDateRange" class="form-control" value="2017-06-06 to 2017-06-06" placeholder="Select Date Range">
                    </div>
              </div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <table  border="1" class="table text-center table-responsive table-bordered autoPack" id="cashoutAutopack_table">
                            <thead>
                                <tr>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">No. of Transaction</th>
                                    <th colspan="3">Amount</th>

                                </tr>
                                <tr>
                                    <th>ITD</th>
                                    <th>BTC</th>
                                    <th>USD</th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php for($i = 0; $i <15; $i++){?>
                                <tr>
                                    <td><a href="autoPackLink/secondCash.php">1 Nov 2015</a></td>
                                    <td>150</td>
                                    <td>12453</td>
                                    <td>2.5</td>
                                    <td>548$</td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script type="text/javascript">

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
