<?php include "../../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../../model/geolocation/geoLocation.php";
checkGroupAccess();
?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<!-- <link href="<?php // echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.css" rel="stylesheet"> -->
<main>

    <div id="adminUserDetail" adminType="<?php echo $cohod; ?>" class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Cashouts</h1><span>UAM HOD Cashout Approval</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div id="filteredTabs" class="nav-tabs-custom ">
            <div class="btn-group btn-group-justified " role="group" aria-label="...">
                <ul class="nav nav-tabs nav-justified" role="tablist">
                    <li role="presentation" class="active bdrR"><a class="text-white" href="#cashoutRequests" aria-controls="cashoutRequests" role="tab" data-toggle="tab">Cashout Requests for HOD Approval</a></li>
                    <li role="presentation" class="bdrR" onclick="getAllCashoutRequests();"><a class="text-white" href="#allCashoutData" aria-controls="allCashoutData" role="tab" data-toggle="tab">All Agent Bucket Cashout Data</a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div id="cashoutRequests" role="tabpanel" class="tab-pane active">
                    <div class="row m-t-10">
                        <div id="filter" class="clearfix">
                            <div class="col-xs-12">
                                <form action="" method="POST" class="" role="form" name="filter">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <input type="text" id="dateYearCashout" class="form-control" value="" placeholder="Select Date Range">
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="text" id="userEmailId" onkeyup="getCashoutRequests()" class="form-control" value="" placeholder="Email of User">
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="text" id="userWalletAddress" onkeyup="getCashoutRequests()" class="form-control" value="" placeholder="Wallet Address User">
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="dropdownOptions">
                                                <select id="cashoutReqStatus" class="form-control">
                                                    <option value="pending_for_hod_approval" selected> Pending</option>
                                                    <option value="hold_by_hod"> Hold</option>
                                                    <option value="rejected_by_hod"> Rejected</option>
                                                </select>
                                            </label>
                                        </div>
                                        <div class="col-xs-2">
                                            <label class="select--1 dropdownOptions">
                                                <?php
                                                $result=getAllCountry($connSearch)["errMsg"];
                                                ?>
                                                    <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                                        <option data-live-search=\ "true\" data-tokens='All' id='All' value=''>All</option>
                                                        <?php
                                                    foreach($result as $value){
                                                    echo $value["name"];
                                                    echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                                    }
                                                    ?>
                                                    </select>
                                            </label>
                                            <div class="styled-select">
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="filter" class="sign-upverification clearfix filter-status-change  m-t-20" style="">
                            <div class="col-xs-2">
                                <button type="button" class="bg-red btn btn-block border-none text-white bg-green" onclick="approveCashoutRequests();" name="">Approve</button>
                            </div>
                            <div id="filter-hold" class="col-xs-2">
                                <button type="button" class="bg-red btn btn-block border-none text-white bg-orange--1" onclick="holdCashoutRequests();" name="">Hold</button>
                            </div>
                            <div class="col-xs-2 ">
                                <button type="button" class="bg-red btn btn-block border-none text-white bg-red" onclick="rejectCashoutRequests();" name="">Reject</button>
                            </div>
                            <div class="col-xs-3">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="errors" class="col-xs-12 m-t-10"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="admin_cashout_data">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="allCashoutData" role="tabpanel" class="tab-pane">
                    <div class="row m-t-10">
                        <div id="filter" class="clearfix">
                            <div class="col-xs-12">
                                <form action="" method="POST" class="" role="form" name="filter">
                                    <div class="row">
                                        <div class="col-xs-2">
                                            <input type="text" id="allDateYearCashout" class="form-control dateYearCashout" value="" placeholder="Select Date Range">
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="text" id="AllUserEmailId" onkeyup="getAllCashoutRequests()" class="form-control" value="" placeholder="Email of User">
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="text" id="AllAgentEmailId" onkeyup="getAllCashoutRequests()" class="form-control" value="" placeholder="Email of Admin">
                                        </div>
                                        <div class="col-xs-6">
                                            <button type="button" name="Reassign" value="Reassign" class="btn btn-primary pull-right" onclick="reasignCashReqModal();">
                                                Reassign
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row m-t-10  ">
                        <div class="col-xs-12">
                            <div id="admin_all_cashout_data">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<div class="modal madal--1 fade" id="actionCommentModal" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal" style="background-color: #fff;">
            <!-- Modal content-->
            <div class="">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" style="font-weight: bold">Please Enter Comment To <span id= "actionName">Reject</span</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <div id="actionRequest" style="display:none;"></div>
                        <textarea class="form-control noresize" id="actionComment" name="actionComment" rows="4"></textarea>
                    </form>
                </div>
                <div class="modal-footer border-none">
                    <button type="button" class="btn btn-danger btn-social-wid-auto-dark" data-dismiss="modal">Cancle</button>
                    <button type="button" class="btn btn-success btn-social-wid-auto yes-remove-post-btn" data-dismiss="modal" onclick="holdCashout();">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!--modal to display editing data-->
<div class="modal madal--1 fade" id="ReasignCashoutRequest" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" style="width: 870px;">
        <div class="    ">
            <div class="modal-header ModalHeaderBackground">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                <h4 class="modal-title" style="font-weight: bold">Assigne Cashout to agents</span</h4>
            </div>
            <div class="modal-body" style="background-color: #eeeeee; min-height: 485px;">
                <form>
                    <div class="form-group">
                        <label class="sr-only" for="">Username</label>
                        <input type="email" onkeyup="loadEmailDetailsOfCagent();" class="form-control" id="agentEmailId" placeholder="Agent Email">
                    </div>
                </form>
                <div id="ReasignCashoutEmailData">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<!--====================================
=            month selector            =
=====================================-->
<script type="text/javascript" src="<?php echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="<?php //echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"> -->
<!--====  End of month selector  ====-->
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
var rooturl = '<?= $adminRoot; ?>controller/cashout/cashout_dept_data.php';
var controllerUrl = '<?= $adminRoot; ?>controller/';

$("#countryToSearch").chosen();
$(document).ready(function() {
    $("#dateYearCashout").val("<?php echo date('m')." / ".date('Y'); ?>");
    $("#allDateYearCashout").val("<?php echo date('m')." / ".date('Y'); ?>");
    getCashoutRequests();
    // getAllCashoutRequests();
});

$("#actionCommentModal").on('hide.bs.modal', function() {
    $('#actionName').text('');
    $('#actionComment').val('');
    $('#actionRequest').text('');
});



// $("#LimitedResult").change(function(){
//     getCashoutRequests();
// });

$('#dateYearCashout').MonthPicker({
    ShowIcon: false,
    OnAfterChooseMonth: function() {
        getCashoutRequests();
    }
});
$('#allDateYearCashout').MonthPicker({
    ShowIcon: false,
    OnAfterChooseMonth: function() {
        getCashoutRequests();
    }
});
$("#cashoutReqStatus").change(function() {
    $("#errors").html('');
    var cahoutStatus = $(this).val();
    if (cahoutStatus == 'rejected_by_hod') {
        $('.filter-status-change').hide();
    } else if (cahoutStatus == 'hold_by_hod') {
        $('.filter-status-change').show();
        $('#filter-hold').hide();
    } else if (cahoutStatus == '') {
        $('.filter-status-change').hide();
    } else {
        $('.filter-status-change').show();
        $('#filter-hold').show();
    }
    getCashoutRequests();
});


$("#countryToSearch").change(function() {
    getCashoutRequests();
});

function getCashoutRequests() {

    var dateYearCashout = ($("#dateYearCashout").val());
    var resSplitDate = dateYearCashout.split("/");

    var tablename = "queue_processor_" + resSplitDate[0] + "_" + resSplitDate[1];
    var userEmailId = ($("#userEmailId").val());
    var userWalletAddress = ($("#userWalletAddress").val());
    var cashoutReqStatus = ($("#cashoutReqStatus").val());
    var countryToSearch = ($("#countryToSearch").val());

    var payment_mode = "";

    var limit = ($("#LimitedResult").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 5;
    }
    // alert('LMIT : '+limit);

    $.ajax({
        type: "POST",
        dataType: "html",
        url: rooturl,
        data: {
            limit: limit,
            tablename: tablename,
            userEmailId: userEmailId,
            userWalletAddress: userWalletAddress,
            cashoutReqStatus: cashoutReqStatus,
            countryToSearch: countryToSearch,
            payment_mode: payment_mode
        },
        beforeSend: function(){
            $('#loadng-image').show();
        },
        success: function(data) {
            $("#admin_cashout_data").html("");
            $("#admin_cashout_data").html(data);
            $('#loadng-image').hide();
        },
        error: function() {
            console.log("fail");
        }
    });

}


function getCashoutRequestsPages(pageNo, limit, tablename, userEmailId, userWalletAddress, cashoutReqStatus, countryToSearch, payment_mode) {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: rooturl,
        data: {
            page: pageNo,
            limit: limit,
            tablename: tablename,
            userEmailId: userEmailId,
            userWalletAddress: userWalletAddress,
            cashoutReqStatus: cashoutReqStatus,
            countryToSearch: countryToSearch,
            payment_mode: payment_mode
        },
        beforeSend: function(){
            $('#loadng-image').show();
        },
        success: function(data) {
            $("#admin_cashout_data").html("");
            $("#admin_cashout_data").html(data);
            $('#loadng-image').hide();
        },
        error: function() {
            console.log("fail");
        }
    });

}

function getAllCashoutRequests() {

    var dateYearCashout = ($("#allDateYearCashout").val());
    var resSplitDate = dateYearCashout.split("/");

    var tablename = "queue_processor_" + resSplitDate[0].trim() + "_" + resSplitDate[1].trim();
    var userEmailId = ($("#AllUserEmailId").val());
    var AgentEmailId = ($("#AllAgentEmailId").val());
    var payment_mode = "";
    var limit = ($("#AllLimitedResult").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 5;
    }
    // alert(limit);

    $.ajax({
        type: "POST",
        dataType: "html",
        url: controllerUrl + 'cashout/all_cashout_dept_data_agents.php',
        data: {
            limit: limit,
            tablename: tablename,
            userEmailId: userEmailId,
            AgentEmailId: AgentEmailId,
            payment_mode: payment_mode
        },
        beforeSend: function(){
            $('#loadng-image').show();
        },
        success: function(data) {
            $("#admin_all_cashout_data").html("");
            $("#admin_all_cashout_data").html(data);
            $('#loadng-image').hide();
        },
        error: function() {
            console.log("fail");
        }
    });

}


function getAllCashoutRequestsPages(pageNo, limit, tablename, userEmailId, AgentEmailId, payment_mode) {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: controllerUrl + 'cashout/all_cashout_dept_data_agents.php',
        data: {
            page: pageNo,
            limit: limit,
            tablename: tablename,
            userEmailId: userEmailId,
            AgentEmailId: AgentEmailId,
            payment_mode: payment_mode
        },
        beforeSend: function(){
            $('#loadng-image').show();
        },
        success: function(data) {
            $("#admin_all_cashout_data").html("");
            $("#admin_all_cashout_data").html(data);
            $('#loadng-image').hide();
        },
        error: function() {
            console.log("fail");
        }
    });

}
/***************************** common to all ************************************/
function getAllSelectedCashoutIds() {

    var tags = document.getElementsByName('user_checkbox[]');
    var cashoutId = new Array();
    for (var i = 0; i < tags.length; ++i) {
        if (tags[i].checked) {
            var abc = $('#user_checkbox' + tags[i].value).attr('requestDate');
            cashoutId.push({
                id: tags[i].value,
                requestTime: abc
            }); // add a new object  
        }
    }
    return cashoutId;
}
/***************************** common to all ************************************/

/************************************************Approve cash requests *****************************************************/
function approveCashoutRequests() {

    var cashoutids = getAllSelectedCashoutIds();
    // console.log(cashoutids);
    if (cashoutids.length > 0) {
        bootbox.confirm({
            message: "<h4>Are you sure to Approve Selected Cashout Requests?</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    approveCashout(cashoutids);
                } else {
                    console.log("no selected");
                }
            }
        });
    }
}


function showErrorPopUp(msg) {
    bootbox.confirm({
        message: "<h4>" + msg + "</h4>",
        buttons: {
            confirm: {
                label: 'OK',
                className: 'btn-success'
            },
        },
        callback: function(result) {
            if (result == true) {
                location.reload();
            } else {
                console.log("no selected");
            }
        }

    });
}

function approveCashout(cashoutids) {
    console.log(cashoutids);
    var cashIds = JSON.stringify(cashoutids);
    var adminUserType = $("#adminUserDetail").attr('adminType');
    var action = "Approve";
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: controllerUrl + "cashout/cashout_change_status.php",
        data: {
            adminUserType: adminUserType,
            cashoutIds: cashIds,
            action: action
        },
        success: function(data) {
            if (data.errCode == -1) {
                $("#error").html('');
                $("#errors").html(bsAlert("success", "Request Approved Successfully..!"));
                getCashoutRequests();
            } else if (data.errCode == 2) {
                $("#error").html('');
                $("#errors").html(bsAlert("danger", "Request Allready Rejected..!"));
            } else {
                $("#errors").html(bsAlert("danger", "Error in Processing Request..!"));
            }
        },
        error: function() {
            console.log("fail");
        }
    });


}

/************************************************Approve cash requests *****************************************************/



/************************************************hold cash requests *****************************************************/
function holdCashoutRequests() {

    var cashoutids = getAllSelectedCashoutIds();
    if (cashoutids.length > 0) {
        bootbox.confirm({
            message: "<h4>Are you sure to Hold Selected Cashout Requests?</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    var action = 'Hold';
                    holdRejectCommentModel(cashoutids, action);
                } else {
                    console.log("no selected");
                }
            }
        });
    }
}

function holdRejectCommentModel(cashoutids, action) {
    $('#actionName').text(action);
    $("#actionCommentModal").modal('show');
    $('#actionRequest').text(JSON.stringify(cashoutids));
}

function holdCashout() {

    var comment = $("#actionCommentModal #actionComment").val();
    var cashoutIds = $("#actionCommentModal #actionRequest").text();
    var action = $("#actionCommentModal #actionName").text();
    var adminUserType = $("#adminUserDetail").attr('adminType');
    $.ajax({
        type: "POST",
        dataType: "JSON",
        url: controllerUrl + "cashout/cashout_change_status.php",
        data: {
            action: action,
            comment: comment,
            cashoutIds: cashoutIds,
            adminUserType: adminUserType
        },
        success: function(data) {
            console.log(data);
            if (data.errCode == -1) {
                $("#error").html('');
                if (action == "Reject") {
                    var actionMsg = "Rejected";
                } else {
                    var actionMsg = action;
                }
                $("#errors").html(bsAlert("success", "Request " + actionMsg + " Successfully..!"));
                getCashoutRequests();
            } else if (data.errCode == 2) {
                $("#error").html('');
                $("#errors").html(bsAlert("danger", "Request Allready Rejected..!"));
            } else {
                $("#errors").html(bsAlert("danger", "Error in Processing Request..!"));
            }
        },
        error: function() {
            console.log("fail");
        }
    });
}

/************************************************hold cash requests *****************************************************/


/************************************************reject cash requests *****************************************************/
function rejectCashoutRequests() {

    var cashoutids = getAllSelectedCashoutIds();
    if (cashoutids.length > 0) {
        bootbox.confirm({
            message: "<h4>Are you sure to Reject Selected Cashout Requests?</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    var action = 'Reject';
                    holdRejectCommentModel(cashoutids, action);
                } else {
                    console.log("no selected");
                }
            }
        });
    }
}

/************************************************reject cash requests *****************************************************/

function loadEmailDetailsOfCagent() {

}

function reasignCashReqModal() {
    var tickets = getAllSelectedCashoutIds();
    if (tickets.length > 0) {
        $('#ReasignCashoutRequest').modal('show');
        loadEmailDetailsOfCagent('0');
    }
}

function loadEmailDetailsOfCagent(pageNo) {
    var email = $('#agentEmailId').val();
    var limit = ($("#LimitedResultPopup").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 10;
    }

    if (((typeof pageNo) == "undefined") || pageNo == "") {
        var pageNo = $('#hiddenpagePopup').val();
    }
    // pageNo = 1;
    var cashoutAgentsArray = new Array();  

    $("#admin_all_cashout_data .highlight .assigned_to_agent").each(function() {
       if($.inArray($( this ).text(), cashoutAgentsArray) === -1) cashoutAgentsArray.push($( this ).text());
    }); 
    console.log("cashoutAgentsArray :" + cashoutAgentsArray);
    $.ajax({
        type: "POST",
        dataType: "html",
        url: controllerUrl + "cashout/reassignCashoutRequestData.php",
        data: {
            limit:              limit,
            email:              email,
            page:               pageNo,
            cashoutAgentsArray: cashoutAgentsArray
        },
        success: function(data) {
            $("#ReasignCashoutEmailData").html("");
            $("#ReasignCashoutEmailData").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });
}

$("#ReasignCashoutRequest").on('hide.bs.modal', function() {
    $('#agentEmailId').val('');
    $('#ReasignCashoutEmailData').html('');
});

function reassignCashReqToAgent(agentId) {
    var cashReq = getAllSelectedCashoutIds();

    if (cashReq.length > 0) {
        var agentEmail = $('#tcktAssignedTo' + agentId).attr('agent_email');
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: controllerUrl + "cashout/assignCashReqToAgent.php",
            async: false,
            data: {
                cashReq: cashReq,
                agentEmail: agentEmail,
                agentId: agentId
            },
            success: function(data) {
                if (data.errCode == -1) {
                    $('#ReasignCashoutRequest').modal('hide');
                    getAllCashoutRequests();
                }
            },
            beforeSend: function(data) {
                $('.tckt_assigning_btn').css('pointer-event', 'none');
                $('.tckt_assigning_btn').removeClass('btn-primary');
                $('.tckt_assigning_btn').addClass('btn-danger');
                $('#tcktAssignedTo' + agentId).removeClass('btn-danger');
                $('#tcktAssignedTo' + agentId).addClass('btn-success');
            },
            complete: function(data) {
                $('.tckt_assigning_btn').css('pointer-event', 'cursor');
                $('.tckt_assigning_btn').removeClass('btn-success');
                $('.tckt_assigning_btn').removeClass('btn-danger');
                $('.tckt_assigning_btn').addClass('btn-primary');
            },
            error: function() {
                console.log("fail");
                alert('failed');
            }
        });
    }
}
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
