<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../model/geolocation/geoLocation.php";
checkGroupAccess();
?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<!-- <link href="<?php // echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.css" rel="stylesheet"> -->
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Cashouts</h1><span>All Cashout Data</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>

        <div class="row m-t-10">
            <div id="filter" class="clearfix">
                <div class="col-xs-12">
                    <form action="" method="POST" class="" role="form" name="filter">
                        <div class="row">
                            <div class="col-xs-2">

                                <input type="text" id="dateYearCashout" class="form-control" value="" placeholder="Select Date Range">

                            </div>
                            <div class="col-xs-2">
                                <input type="text" id="userEmailId" onkeyup="getCashoutRequests()" class="form-control" value="" placeholder="Email of User">
                            </div>
                            <div class="col-xs-2">
                                <input type="text" id="userWalletAddress" onkeyup="getCashoutRequests()" class="form-control" value="" placeholder="Wallet Address User">
                            </div>
                            <div class="col-xs-2">
                                <label class="dropdownOptions">
                                    <select id="cashoutReqStatus" class="form-control">
                                        <option disabled="">Status</option>
                                        <option value="approved" selected> Approved</option>
                                        <option value="pending"> Pending</option>
                                        <option value="hold"> Hold</option>
                                        <option value="rejected"> Rejected</option>
                                        <option value="paid"> Paid</option>
                                    </select>
                                </label>
                            </div>
                            <div class="col-xs-2">
                                <label class="select--1 dropdownOptions">
                                    <?php
                                    $result=getAllCountry($connSearch)["errMsg"];
                                    ?>
                                    <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select form-control">
                                        <option data-live-search=\ "true\" data-tokens='All' id='All' value=''>All</option>
                                        <?php
                                        foreach($result as $value){
                                            echo $value["name"];
                                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                        }
                                        ?>
                                    </select>
                                </label>
                                <div class="styled-select">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div id="admin_cashout_data">
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<!--====================================
=            month selector            =
=====================================-->
<script type="text/javascript" src="<?php echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="<?php //echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"> -->
<!--====  End of month selector  ====-->
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
    var rooturl = '<?= $adminRoot; ?>controller/cashout/cashout_all.php';

    $("#countryToSearch").chosen();
    $(document).ready(function() {
        $("#dateYearCashout").val("<?php echo date('m')."/".date('Y'); ?>");
        getCashoutRequests();
    });

    $('#dateYearCashout').MonthPicker({
        ShowIcon: false,
        OnAfterChooseMonth: function() {
            getCashoutRequests();
        }
    });
    $("#cashoutReqStatus").change(function() {
        getCashoutRequests();
    });


    $("#countryToSearch").change(function() {
        getCashoutRequests();
    });

    function getCashoutRequests() {

        var dateYearCashout = ($("#dateYearCashout").val());
        var resSplitDate = dateYearCashout.split("/");

        var tablename = "queue_processor_" + resSplitDate[0] + "_" + resSplitDate[1];
        var userEmailId = ($("#userEmailId").val());
        var userWalletAddress = ($("#userWalletAddress").val());
        var cashoutReqStatus = ($("#cashoutReqStatus").val());
        var countryToSearch = ($("#countryToSearch").val());

        var payment_mode = "";

        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 5;
        }

        $.ajax({
            type: "POST",
            dataType: "html",
            url: rooturl,
            data: {
                limit: limit,
                tablename: tablename,
                userEmailId: userEmailId,
                userWalletAddress: userWalletAddress,
                cashoutReqStatus: cashoutReqStatus,
                countryToSearch: countryToSearch,
                payment_mode: payment_mode
            },
            success: function(data) {
                $("#admin_cashout_data").html("");
                $("#admin_cashout_data").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }


    function getCashoutRequestsPages(pageNo, limit, tablename, userEmailId, userWalletAddress, cashoutReqStatus, countryToSearch, payment_mode) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: rooturl,
            data: {
                page: pageNo,
                limit: limit,
                tablename: tablename,
                userEmailId: userEmailId,
                userWalletAddress: userWalletAddress,
                cashoutReqStatus: cashoutReqStatus,
                countryToSearch: countryToSearch,
                payment_mode: payment_mode
            },
            success: function(data) {
                $("#admin_cashout_data").html("");
                $("#admin_cashout_data").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
