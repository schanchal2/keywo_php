<?php include "../../layout/header.php"; ?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Cashouts</h1><span>Packing List Data</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="row m-b-20">
              <div id="filter" class="sign-upverification clearfix">
                <div class="col-xs-8">
                  <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="row">
                      <div class="col-xs-3">
                        <div class="input-group">
                          <input type="text" id="userEmailId" class="form-control" value="" placeholder="Packing list 1">
                          <span class="btn input-group-addon" type="submit" onclick="getKeywordsSoldList()"><i class="fa fa-folder-open"></i>  </span>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="input-group">
                            <input type="text" id="userEmailId" class="form-control" value="" placeholder="Email ID">
                            <span class="btn input-group-addon" type="submit" onclick="getKeywordsSoldList()"><i class="fa fa-search"></i>  </span>
                        </div>
                      </div>
                      <div class="col-xs-3">
                        <div class="form-inline pull-left m-r-10">
                            <label class="dropdownOptions">
                                <select id="paymentType" class="form-control">
                                    <option value="" disabled="" selected="">Country</option>
                                    <option value="Bitcoin" =""="">Bitcoin</option>
                                    <option value="Wallet">Wallet</option>
                                    <option value="Paypal">Paypal</option>
                                    <option value="all">All</option>
                                </select>
                            </label>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="col-xs-4 ">
<!--                  <div class="row ">-->
                      <div class="input-group  pull-right">

                          <button type="button" onclick="" class="btn  bg-blue text-white m-r-5" disabled=""><i class="fa fa-angle-left"></i>
                          </button>
                          <button type="button" onclick="" class="btn  bg-blue text-white"><i class="fa fa-angle-right"></i>
                          </button>
                      </div>
<!--                  </div>-->
                </div>


                </div>

            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <table  border="1" class="table text-center table-responsive table-bordered autoPack" id="cashoutAutopackThird_table">
                            <thead>
                                <tr>
                                    <th rowspan="2">
                                        <input class="user_checkbox" id="abc" type="checkbox" name="user_checkbox[]" value="">
                                        <label for="abc" class="fa"></label>
                                    </th>
                                    <th rowspan="2">Time <br> Stamp</th>
                                    <th rowspan="2">User Email</th>
                                    <th colspan="3">Cashout Amount </th>
                                    <th rowspan="2">Cashout Fees </th>
                                    <th rowspan="2">Country</th>
                                  </tr>
                                <tr>
                                    <th><?= $adminCurrency; ?></th>
                                    <th>BTC</th>
                                    <th>USD</th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php for($i = 0; $i <15; $i++){?>
                                <tr>
                                    <td>
                                        <input class="user_checkbox" id="xyz<?php echo $i ?>" type="checkbox" name="user_checkbox[]" value="">
                                        <label for="xyz<?php echo $i ?>" class="fa"></label>
                                    </td>
                                    <td>
                                      <span class="date">
                                        01-11-2015
                                      </span><br>
                                      <span class="time text-blue">
                                        12:05:26
                                      </span>
                                    </td>
                                    <td><a>demo@searchtrade.com</a></td>
                                    <td>2525 </td>
                                    <td>2525 </td>
                                    <td>2525 </td>
                                    <td>25</td>
                                    <td>Country</td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script type="text/javascript">

        $(document).ready(function() {
            $('#sidebarChanger').click();
        });
    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
