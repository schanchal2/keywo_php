<?php include "../../layout/header.php"; ?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Cashouts</h1><span>Packing List Summary</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="row m-b-20">
              <div class="col-xs-2">
                <div class="input-group">
                  <input type="text" id="userEmailId" class="form-control" value="" placeholder="1 june 2017">
                  <span class="btn input-group-addon" type="submit" onclick="getKeywordsSoldList()"><i class="fa fa-folder-open"></i></span>
                </div>
              </div>
            </div>


            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <table  border="1" class="table text-center table-responsive table-bordered autoPack" id="cashoutAutopack_table">
                            <thead>
                                <tr>
                                    <th rowspan="2">Date</th>
                                    <th rowspan="2">No. of Transaction</th>
                                    <th colspan="3">Amount</th>
                                </tr>
                                <tr>
                                    <th><?= $adminCurrency; ?></th>
                                    <th>BTC</th>
                                    <th>USD</th>
                                </tr>
                            </thead>

                            <tbody>
                              <?php for($i = 0; $i <15; $i++){?>
                                <tr>
                                    <td><a href="thirdCash.php">Packing list <?php echo $i+1 ;?></a></td>
                                    <td>150</td>
                                    <td>12453</td>
                                    <td>2.5</td>
                                    <td>548$</td>
                                </tr>

                                <?php } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script type="text/javascript">

        $(document).ready(function() {
            $('#sidebarChanger').click();
        });
    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
