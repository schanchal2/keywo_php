<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <style>
        #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
            width: 170px;
        }
    </style>
    <main>
        <div class="container-fluid">
            <div class="title clearfix without-subtitle">
                <h1 class="pull-left">App-Management</h1><span></span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="">Apps Integrated (Nos.)</th>
                                <th class="">Searches (Nos.)</th>
                                <th class="">App Earnings</th>
                                <th class=""> <i class="fa fa-calendar" id=""></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> 0 </td>
                                <td id=""> 0 </td>
                                <td id=""> 0 </td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <?php
                                    $date=date("Y-m-d");
                                    ?>
                                    <input style="width: 190px" type="text" id="appMgmtDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                                </div>
                                <div class="input-group styled-select" style="z-index: 1;">
                                    <?php

                                    $result=getAllCountry($connSearch)["errMsg"];

                                    ?>
                                    <select id="countryToSearch" class="countryToSearch">
                                        <option value="" disabled selected>Country</option>
                                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                        <?php
                                        foreach($result as $value){
                                            echo $value["name"];
                                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                        }
                                        ?>

                                    </select>
                                </div>
                                <div class="input-group styled-select" >

                                    <input style="width: 155px;z-index: 1" type="text" id="AppOwneruserEmailId" class="form-control" value='' placeholder="App Owner Email ID">

                                    <span class="btn input-group-addon" type="submit" onclick="getappMgmtList()"><i class="fa fa-search"></i>  </span>

                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="appMgmtAjaxData">
            </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $( document ).ready(function() {
            getappMgmtList();
        });

        function getappMgmtList()
        {

            var refferaldatevalue=($("#appMgmtDate").val());
            var datedata=refferaldatevalue.split("to");
            var fromdate=(datedata[0]).trim();
            var todate=(datedata[1]).trim();

            var country=($("#countryToSearch").val());
            var userEmailId=($("#AppOwneruserEmailId").val());

            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/appMgmt/appsDetailsMgmt.php",
                data: {
                    fromdate: fromdate, todate: todate,country:country,limit:limit,userEmailId:userEmailId,listtype:"first"
                },
                success: function (data) {
                    $("#appMgmtAjaxData").html("");
                    $("#appMgmtAjaxData").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#appMgmtDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function(event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function(event, obj) {

            getappMgmtList();
        });

        $("#countryToSearch").change(function(){
            getappMgmtList();
        });

        $("#LimitedResult").change(function(){
            getappMgmtList();
        });

        $('#countryToSearch').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });


    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>