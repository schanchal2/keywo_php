<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/js/myjs.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/js/header.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/bootbox/bootbox.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/ladda/dist/spin.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/ladda/dist/ladda.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/ladda/js/ladda.jquery.js"></script>
<style>
    .bootbox .modal-dialog{
        width:70%;
    }
</style>
<script>

    function operateLadda(butionId,operation)
    {
        var btn="btn_"+butionId;
        btn = $('#'+butionId).ladda();
        switch(operation)
        {
            case "start":
                btn.ladda('start');
                break;

            case "stop":
                btn.ladda('stop');
                break;
        }

    }

     $(function () {
         var currenturl = window.location.href;
          $('a[href="' + currenturl + '"]').parent().parent().parent().addClass("in");
     });


</script>


