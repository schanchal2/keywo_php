<?php
if(!isset($_SESSION))
{
    session_start();
}
header('Content-Type: text/html; charset=utf-8');
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

//start config
require_once("{$docrootpath}config/config.php");

//session check
if(isset($_SESSION['user']) && isset($_SESSION['admin']))
{
    $sessinUser=$_SESSION['user'];
    $sessinAdmin=$_SESSION['admin'];
}else{
    $sessinUser="";
    $sessinAdmin="";
}

if ($sessinUser != "admin" && $sessinAdmin != 1) {
    $url=$adminView."acl/index.php";
    echo '<script>window.location.replace("'.$url.'");</script>';
    exit;
}

//end session check

require_once("{$docrootpath}config/db_config.php");
//end config

//start helper
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}helpers/date_helpers.php");
require_once("{$docrootpath}helpers/cryptoHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
//end helper

//other
require_once("{$docrootpath}core/errorMap.php");
require_once("{$docrootpath}model/acl/acl_model.php");
//end other

//require_once("../keywordsale/controller/utility_sale.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);



$myname = $_SESSION["admin_name"];

$name = explode(" ", $myname);
$adminEmail = $_SESSION["admin_id"];
$adminImage = $_SESSION["admin_profile_pic"];


// Tell PHP that we're using UTF-8 strings until the end of the script
mb_internal_encoding('UTF-8');

// Tell PHP that we'll be outputting UTF-8 to the browser
mb_http_output('UTF-8');

global $inactive;
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Keywo Admin</title>
        <!-- Bootstrap -->
        <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
        <link href="<?php echo $adminRoot; ?>frontend_libraries/css/myapp.css" rel="stylesheet">
        <link href="<?php echo $adminRoot; ?>frontend_libraries/css/custom.css" rel="stylesheet">
        <link href="<?php echo $adminRoot; ?>frontend_libraries/bootbox/bootb.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <link rel="shortcut icon" href="<?php echo $adminRoot; ?>frontend_libraries/images/Keywo_favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/ladda/dist/ladda_themeless.min.css">
        <script src="<?php echo $adminRoot; ?>frontend_libraries/tzDetector/jstz.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo $adminRoot; ?>">
                <img src="<?php echo $adminRoot; ?>frontend_libraries/img/Keywo.png" class="center-block" width="100">
            </a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <!--<ul class="nav navbar-nav">
                        <li>
                            <a href="#"> <i id="hit_admin_settings" class="fa fa-2x fa-gear"></i>
                                <!-- <span class="notification_count"> </span> --></a>
                        <!--</li>
                        <li>
                            <a href="#"> <i class="fa fa-2x fa-bell"></i><span class="notification_count"> 5+</span></a>
                        </li>
                        <li>
                            <a href="#"> <i class="fa fa-2x fa-envelope"></i><span class="notification_count"> 5+ </span></a>
                        </li>
                    </ul>-->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <!-- <span class="fa-stack fa-lg"> -->
                                <img class="img-circle" src="<?php echo $adminRoot; ?>uploads/adminProfilePics/<?php echo $adminImage; ?>">
                                <!--  
                             <i class="fa fa-circle fa-stack-2x"></i>
                                <i class="fa fa-user fa-stack-1x fa-inverse"></i>
                             -->
                                <!-- </span> -->
                                <span class="ko_agnt_name"><?php echo $_SESSION['admin_name']; ?></span> <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header  circular">
                                    <img style="cursor: pointer" src="<?php echo $adminRoot; ?>uploads/adminProfilePics/<?php echo $adminImage; ?>" class="img-circle tool" alt="User Image" onclick="changeProfilePic();">
                                    <form name="form_uploadpic" id="form_uploadpic" method="POST" enctype="multipart/form-data" action="<?php echo $adminRoot; ?>controller/acl/uploadImage_controller.php">
                                        <div>
                                            <input style="position:absolute;top:-500px" type="file" id="changenewprofile" name="image">
                                            <!-- <input style="position:absolute;top:-500px"  type="submit"/>-->
                                        </div>
                                    </form>
                                    <p>
                                        Keywo Admin
                                        <small><span class="ko_agnt_name"><?php echo $myname; ?></span></small>
                                    </p>
                                </li>
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?php echo $adminView; ?>acl/mySettings.php" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?php echo $adminRoot; ?>controller/acl/logout_controller.php" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <aside>
            <?php
            include "{$docRootAdmin}view/layout/leftmenu.php";

            function checkGroupAccess()
            {
                global $adminRoot;
                global $pages;
                if(!in_array(basename($_SERVER["PHP_SELF"]),$pages))
                {
                    $redirectURL = $adminRoot . "index.php";
                    echo '<script>window.location.replace("'.$redirectURL.'");</script>';
                    exit;
                }
            }
            ?>
        </aside>
    <script>
        function session_checking() {
            var adminroot="<?php echo $adminRoot; ?>";
            $.post(adminroot+"controller/acl/checkSessionTimeout.php", function (data) {
                if (data == "-1") {
                    window.location = adminroot+"index.php";
                }

            });
        }

        var validateSession = setInterval(session_checking, <?php echo $checkSession; ?>);// microsecond of 600 second=600000

        </script>
