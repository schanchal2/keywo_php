<?php
session_start();
require_once "{$docrootpath}model/acl/sidebarAccess.php";
require_once "{$docrootpath}model/acl/checkAccess.php";
$groupsAdmin=explode(" ",getAdminGroup($connAdmin)["errMsg"]["group_id"]);
$pages=array_unique($pages);

$department=$groupsAdmin;
?>

<div class="sidbarWrapper pull-left">
    <span class="fa-stack fa-lg sidebar-toggler">
    <i class="fa fa-circle fa-stack-2x"></i>
    <i id="sidebarChanger" class="fa fa-angle-double-left fa-stack-1x fa-inverse"></i>
</span>
    <div class="sidebarCurtain">
        <ul id="sidebar" class="list-unstyled">

            <?php
            if(in_array("Dashboard",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Dashboard" aria-expanded="true">
                        <i class="fa fa-2x fa-dashboard"></i>
                        <span>Dashboard        </span>
                        <i class="fa fa-angle-right"></i> </a>
                    <div id="Dashboard" class="collapse " role="tabpanel">
                        <ul><?php
                            if(in_array("dashboard.php",$pages)) {
                                ?>
                                <li><a data-value=""
                                       href="<?php echo $adminRoot; ?>view/dashboard/dashboard.php">Dashboard</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("AccessControl",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Access_Control" aria-expanded="true">
                        <i class="fa fa-2x  ">
                            <img src="<?php echo $adminRoot;?>images/sidebar/access_control.png">

                        </i>
                        <span>Access Control</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Access_Control" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("AccessControl.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/acl/AccessControl.php">Access Control</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("AdminSettings",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" id="hit_settings" data-parent="#sidebar" href="#Admin_Setting" aria-expanded="true">
                        <i class="fa fa-2x fa-user"></i>
                        <span>Admin Setting</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Admin_Setting" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array($page,$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/tatSettings.php">TAT Settings</a></li>
                                <?php
                            }
                            if(in_array("kycSlab.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/kycSlab.php">KYC Slab Settings</a></li>
                                <?php
                            }
                            if(in_array("2faSettings.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/2faSettings.php">Transaction Security Settings</a></li>
                                <?php
                            }
                            if(in_array("itdCashout.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/itdCashout.php"><?= $adminCurrency; ?> Cashout Settings</a></li>
                                <?php
                            }
                            if(in_array("keywordslabsettings.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/keywordslabsettings.php">Keyword Slab Settings</a></li>
                                <?php
                            }
                            if(in_array("blockedKwds.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/blockedKwds.php">Block Keyword</a></li>
                                <?php
                            }
                            if(in_array("suggestedKwds.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/suggestedKwds.php">Suggest Keyword</a></li>
                                <?php
                            }
                            if(in_array("blockIP.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/blockIP.php">Block IP</a></li>
                                <?php
                            }
                            if(in_array("blockedDomain_view.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/blockedDomain_view.php">Block Domain</a></li>
                                <?php
                            }
                            if(in_array("feesAndCommission.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/feesAndCommission.php">Fees & Commission</a></li>
                                <?php
                            }
                            if(in_array("UserActiveTimeSetting.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/UserActiveTimeSetting.php">Keywo Settings</a></li>
                                <?php
                            }
                            if(in_array("reserved_handle_settings.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/settings/reserved_handle_settings.php">Blocked Handle</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("Accounts",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Accounts" aria-expanded="true">
                        <i class="fa fa-2x fa-gears"></i>
                        <span>Accounts</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Accounts" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("keywordSubscriptionRep.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/accounts/kwdSubReport/keywordSubscriptionRep.php">Keyword Subscription Report</a></li>
                                <?php
                            }
                            if(in_array("<?= $adminCurrency; ?>WalletReport.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/accounts/<?= $adminCurrency; ?>WalletReport/<?= $adminCurrency; ?>WalletReport.php"><?= $adminCurrency; ?> Wallet Report</a></li>
                                <?php
                            }
                            if(in_array("dailyRateRecords.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/accounts/dailyRateRecords.php">Daily Rate Records</a></li>
                                <?php
                            }

                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("ManageUsers",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#User_Managment" aria-expanded="true">
                        <i class="fa fa-2x "> <img src="<?php echo $adminRoot;?>images/sidebar/user_management.png">  </i>
                        <span>User Managment</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="User_Managment" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("manageUsers.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/manageUsers/manageUsers.php">UAM</a></li>
                                <?php
                            }
                            if(in_array("specialUsers.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/manageUsers/specialUsers.php">Universal Users</a></li>
                                <?php
                            }
                            if(in_array("referral.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/referral/referral.php">Referral User</a></li>
                                <?php
                            }
                            if(in_array("kyc.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/kyc/kyc.php">KYC Verification</a></li>
                                <?php
                            }

                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("TrackingMonitoring",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Trcking_Monitoring" aria-expanded="true">
                        <i class="fa fa-2x fa-map-marker"></i>
                        <span>Tracking & Monitoring</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Trcking_Monitoring" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("general.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/tracking/general/general.php">General</a></li>
                                <?php
                            }
                            if(in_array("userwise.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/tracking/userwise/userwise.php">Userwise</a></li>
                                <?php
                            }
                            if(in_array("keywordwise.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/tracking/keywordwise/keywordwise.php">Keywordwise</a></li>
                                <?php
                            }
                            if(in_array("postwise.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/tracking/postwise/postwise.php">Post wise</a></li>
                                <?php
                            }
                            if(in_array("tracklistSummary.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/tracking/tracklist/tracklistSummary.php">Tracklist</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("Keywords",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Keywords" aria-expanded="true">
                        <i class="fa fa-2x fa-tags"></i>
                        <span>Keywords</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Keywords" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("keywordsSold.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/keywords/keywordsSold.php">Keyword Sold</a></li>
                                <?php
                            }
                            if(in_array("keywordsTrade.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/keywords/keywordsTrade.php">Keyword Trading</a></li>
                                <?php
                            }
                            if(in_array("keywordsRenewal.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/keywords/keywordsRenewal.php">Keyword Renewal</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("Cashout",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Cashout" aria-expanded="true">
                        <i class="fa fa-2x ">    <img src="<?php echo $adminRoot;?>images/sidebar/wallet.png">   </i>
                        <span>Cashout</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Cashout" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("cashoutAutoPack.php",$pages)) {
                            ?>
                            <li><a href="<?php echo $adminRoot; ?>view/cashout/cashoutAutoPack.php">All Cashout List</a></li>
                            <?php } ?>
                            <?php
                            loadDepartmentWiseCashoutModules($department);
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("ServiceRequests",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Service_Request" aria-expanded="true">
                        <i class="fa fa-2x fa-wrench"></i>
                        <span>Service Request</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Service_Request" class="collapse" role="tabpanel">
                        <ul>
                            <?php

                            loadDepartmentWiseServiceRequestModules($department);
                            ?>

                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("CustomerSupport",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Customer_Support" aria-expanded="true">
                        <i class="fa fa-2x fa-th"></i>
                        <span>Customer Support</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Customer_Support" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("knowledge_main.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/customerSupport/knowledgeBased/knowledge_main.php">Knowledge Base</a></li>
                            <?php } ?>

                            <?php
                            loadDepartmentWiseCustomerSupportModules($department);
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("AdminLogFiles",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#Admin_Log_Files" aria-expanded="true">
                        <i class="fa fa-2x fa-clipboard"></i>
                        <span>Admin Log Files</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="Admin_Log_Files" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("ADL.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/adminLogs/ADL.php">Logs</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("SystemLogFiles",$mymodules)){
                ?>

                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#System_Log_Files" aria-expanded="true">
                        <i class="fa fa-2x fa-clipboard"></i>
                        <span>System Log Files</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="System_Log_Files" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("sys_userMgmtLogs.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/systemLogs/sys_userMgmtLogs.php">User Management</a></li>
                                <?php
                            }
                            if(in_array("sys_trading.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/systemLogs/sys_trading.php">Trading</a></li>
                                <?php
                            }
                            if(in_array("sys_kwdBuy.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/systemLogs/sys_kwdBuy.php">Buy Keywords</a></li>
                                <?php
                            }
                            if(in_array("sys_Cashout.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/systemLogs/sys_Cashout.php">Cashout</a></li>
                                <?php
                            }
                            if(in_array("sys_Deposit.php",$pages)) {
                                ?>
                                <li><a href="<?php echo $adminRoot; ?>view/systemLogs/sys_Deposit.php"><?= $adminCurrency; ?> Purchase</a></li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("BugReport",$mymodules)){
                ?>

                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#bugReport" aria-expanded="true">
                        <i class="fa fa-2x fa-bug"></i>
                        <span>Bug Report</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="bugReport" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("internalReport.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/bugReport/internalReport.php">Internal Report</a>
                                </li>
                                <?php
                            }
                            if(in_array("userReport.php",$pages)) {
                                ?>
                                <li><a data-value="" href="<?php echo $adminRoot; ?>view/bugReport/userReport.php">User Generated</a>
                                </li>
                                <?php
                            }
                            ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("Communication",$mymodules)){
                ?>

                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#sendNewsletter" aria-expanded="true">
                        <i class="fa fa-2x fa-newspaper-o"></i>
                        <span>Communication</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="sendNewsletter" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("newsletter.php",$pages)) {
                            ?>
                            <li><a data-value="" href="<?php echo $adminRoot; ?>view/newsletter/newsletter.php">Newsletter</a>
                                <?php
                                }
                                if(in_array("about_view.php",$pages)) {
                                ?>
                            <li><a data-value="" href="<?php echo $adminRoot; ?>view/newsletter/about_view.php">Announcement</a></li>
                            </li>
                        <?php
                        }

                        ?>
                        </ul>
                    </div>
                </li>
                <?php
            }
            if(in_array("ReportPost",$mymodules)){
                ?>
                <li class="panel">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#sidebar" href="#spamPostData" aria-expanded="true">
                        <i class="fa fa-2x fa-newspaper-o"></i>
                        <span>Reported Post</span>
                        <i class="fa fa-angle-right"></i>
                    </a>
                    <div id="spamPostData" class="collapse" role="tabpanel">
                        <ul>
                            <?php
                            if(in_array("spamPostSetting.php",$pages)) {
                            ?>
                            <li><a data-value="" href="<?php echo $adminRoot; ?>view/spamPost/spamPostSetting.php">Report Post Limit</a>
                                <?php
                                }
                                if(in_array("spamPost.php",$pages)) {
                                ?>
                            <li><a data-value="" href="<?php echo $adminRoot; ?>view/spamPost/spamPost.php">Reportd Post</a>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </div>
                </li>
                <?php
            }

            ?>
        </ul>
    </div>
</div>
