<?php include "../layout/header.php";
checkGroupAccess();
?>
    <style>
        .without-subtitle h1:after{
            content: " ";
        }
    </style>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix without-subtitle">
                <h1 class="pull-left">Access Control</h1><span>Admin Management</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <div id="slabGenaration" class="nav-tabs-custom">
                <ul class="nav nav-pills" style="background: #97e5f8;" role="tablist">

                    <li role="presentation" class="active"><a href="#ManageStaff" aria-controls="ManageStaff" role="tab" data-toggle="tab">Manage Staff</a></li>
                    <li role="presentation"><a href="#ManageGroup" aria-controls="ManageGroup" role="tab" data-toggle="tab"> Manage Group</a></li>
                    <li role="presentation"><a href="#ManageIP" aria-controls="ManageIP" role="tab" data-toggle="tab"> Manage IP Access</a></li>
                </ul>
                <div class="tab-content">
                   <div id="errors" style="margin-top: 20px;"></div>
                    <div role="tabpanel" class="tab-pane active" id="ManageStaff">

                            <div class="box-header" style="margin-top: 20px">
                                <div class="row " style="float:right">
                                    <form class="form-inline">
                                        <a href="javascript:;" data-toggle="modal" data-target="#basicModal" class="btn btn-success" id="addNewUser"  style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                            New User <span
                                                    class="fa fa-plus-circle"></span></a>


                                    </form>

                                </div>

                        <div id="ManageStaffList">

                        </div>
                    </div>
                    </div>
                    <div role="tabpanel" class="tab-pane " id="ManageGroup">
                        <div class="box-header" style="margin-top: 20px">
                            <div class="row " style="float:right">
                                <form class="form-inline">
                                    <a href="javascript:;" data-toggle="modal" data-target="#basicModal2" class="btn btn-success" id="addNewGroup"  style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                        New Group <span
                                                class="fa fa-plus-circle"></span></a>


                                </form>

                            </div>
                        </div>
                        <div id="ManageGroupList">

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane " id="ManageIP">

                        <div class="box-header" style="margin-top: 20px">
                            <div class="row " style="float:right">
                                <form class="form-inline">
                                    <a href="javascript:;" data-toggle="modal" data-target="#basicModal3" class="btn btn-success" id="addNewIP"  style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                        New IP <span
                                                class="fa fa-plus-circle"></span></a>


                                </form>

                            </div>
                        </div>
                        <div id="ManageIPList">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>

    <div class="modal madal--1 fade in" id="basicModal" style="" tabindex="-1" role="dialog" aria-labelledby="basicModal"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Register a new
                        member (ADMIN)</h4>
                </div>
                <div class="modal-body">

                    <div id="rederror" style="">
                        <div id="errmsg4" style="padding: 0px;color:red;"></div>
                        <br/>
                    </div>

                    <div id="greenerror" style="">
                        <div id="errmsgsuc4" style="padding: 0px;color:green;"></div>
                        <br/>
                    </div>
                    <div class="register-box-body">
                        <form id="addstaff" name="addstaff" action="javascript:;" data-parsley-validate
                              class="form-horizontal form-label-left">

                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">First Name* :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="fname" maxlength="50"
                                           onkeyup="AllowAlphabetfname()" class="form-control" id="" placeholder="" required="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">Last Name* :</label>
                                <div class="col-sm-9">
                                    <input type="text" name="lname" maxlength="50"
                                           onkeyup="AllowAlphabetlname()" class="form-control" id="" placeholder="" required="">
                                </div>
                            </div>


                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">Email* :</label>
                                <div class="col-sm-9">
                                    <input type="text" id="selectdate" name="email"
                                           maxlength="50"  class="form-control" id="" placeholder="" required="">
                                </div>
                            </div>



                            <div class="modal-footer">
                                <div class="form-group m-b-0">
                                    <label for="" class="col-sm-3 control-label p-r-0 p-t-0">Groups :</label>
                                    <div class="col-sm-9">


                                    <div class="checkbox checkbox--1 p-t-0 clearfix">
                                        <?php
                                        $result = getAllRoles($connAdmin);
                                        $data = $result["errMsg"];
                                        $cnt = 1;
                                        foreach ($data as $value) {
                                            $cnt++;
                                            if ($cnt == "6") {
                                                echo "<br/>";
                                                $cnt = 1;
                                            }
                                            ?>
                                        <label class="p-r-10 pull-left">
                                            <input type="checkbox" id="groups" name="groups[]"
                                                   value="<?php echo cleanDisplayParameter($connAdmin, $value['group_name']); ?>">
                                            <?php echo (strtolower($value["group_name"])); ?>
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                            <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                                </div>
                                <div style="margin-top:15px">
                                <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit_usr_data_lad" onclick="submitUserdata()" data-style="expand-right" class="btn btn-sm btn-success border-default-border ajaxhide">Save changes</button>
                                </div>
                            </div>



                        </form>
                    </div>
                </div><!-- /.form-box -->
            </div>
        </div>
    </div>
    <div class="modal madal--1 fade in" id="editnewadmindataadmin" tabindex="-1" role="dialog"
         aria-labelledby="editnewadmindataadmin" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>

                    <h4 class="modal-title" id="editnewadmindataadminLabel" style="color:white;font-weight: bold">
                        Edit
                        Admin Info.</h4>
                </div>
                <div class="modal-body">

                    <div class="register-box-body" id="editnewadmindata">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal madal--1 fade in" id="basicModal2" tabindex="-1" role="dialog" aria-labelledby="basicModal2"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground" style="">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Register a new
                        Group</h4>
                </div>
                <div class="modal-body">
                    <div id="rederror11" style="">
                        <div id="errmsg11" style="padding: 0px;color:red;"></div>
                    </div>

                    <div id="greenerror11" style="">
                        <div id="errmsgsuc11" style="padding: 0px;color:green;"></div>
                    </div>

                    <div class="x_content">
                        <br/>
                        <form id="addgroup" name="addgroup" action="javascript:;" data-parsley-validate
                              class="form-horizontal form-label-left">



                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0">Group Name * :</label>
                                <div class="col col-355">

                                    <input id="groupname" class="date-picker form-control col-md-7 col-xs-12"
                                           onkeyup="AllowAlphabetlgroupname()" name="groupname" type="text"
                                           maxlength="50">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-l-0">Permision group * :</label>
                                <div class="col col-355">

<!--                                        <div class="checkbox checkbox--1" style="padding:0px;">-->

                                            <?php
                                            $dts = str_split("12");
                                            foreach ($dts as $dt) {


                                                ?>
                                        <div class="checkbox checkbox--1" style="">
                                                <label>
                                                <input type="checkbox" id="permsissions" name="permsissions[]"
                                                       value="<?php echo cleanDisplayParameter($connAdmin, $dt); ?>">
                                                <?php
                                                            if ($dt == 1) {
                                                                echo "View";
                                                            }
                                                            if ($dt == 2) {
                                                                echo "Edit";
                                                            }
                                                            ?>
                                                    <i class="fa fa-checkbox--1"> </i>
                                                </label>
                                        </div>

                                                <?php

                                            }

                                            ?>



                                    </div>
                                </div>




                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on module :</label>
                                <div class="col col-355">
                                    <div class="select--1">
                                        <?php
                                        $result = getallmodules($connAdmin);
                                        $resultnew = $result["errMsg"];
                                        // printArr($result);
                                        ?>
                                        <select class="chosen-select" multiple id="modulename" name="modulename">

                                            <?php

                                            foreach ($resultnew as $value) {
                                                echo "<option>" . cleanDisplayParameter($connAdmin, $value["module_name"]) . "</option>";
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group" id="submodulesdata">

                            </div>

                            <div class="modal-footer">
                                <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Cancel</button>
                                <button type="submit" id="submitusersGrpDta" onclick="submitUserdatagroup()" class="btn btn-sm btn-success border-default-border" ajaxhide>Submit</button>
                            </div>

                        </form>
                    </div>

                </div>


            </div><!-- nav-tabs-custom -->


        </div><!-- /.form-box -->
    </div>
    <div class="modal madal--1 fade in" id="editnewadmindataadmingroup" tabindex="-1" role="dialog"
         aria-labelledby="editnewadmindataadmingroup" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground" style="">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>

                    <h4 class="modal-title" id="editnewadmindataadmingroupLabel"
                        style="color:white;font-weight: bold">Edit
                        Group Info.</h4>
                </div>
                <div class="modal-body">

                    <div class="register-box-body" id="editnewadmindatagroup">

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal madal--1 fade in" id="basicModal3" tabindex="-1" role="dialog" aria-labelledby="basicModal3"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title">Add New IP</h4>
                </div>
                <form class="form-horizontal" id="addip" name="addip">
                <div class="modal-body">
                    <div id="rederror" style="">
                        <div id="errmsg" style="padding: 0px;color:red;"></div>

                    </div>

                    <div id="greenerror" style="">
                        <div id="errmsgsuc" style="padding: 0px;color:green;"></div>

                    </div>

                    <br/>




                            <div class="form-group m-b-0">
                                <label for="" class="col-sm-3 control-label p-r-0">IP Name* :</label>
                                <div class="col-sm-9">
                                    <input id="ipname" class="form-control"
                                           name="ipname" type="text" maxlength="50">
                                </div>
                            </div>

                            <div class="form-group m-b-0" style="margin-top: 15px">
                                <label for="" class="col-sm-3 control-label p-r-0">IP Owner* :</label>
                                <div class="col-sm-9">
                                    <input id="ipOwner" class="form-control"
                                           name="ipOwner" type="text" maxlength="50">
                                </div>
                            </div>

                            <div class="modal-footer" style="margin-top: 15px">
                                <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Cancel</button>
                                <button type="submit" id="submit_ip_adr_lad" onclick="submitaipddress()" class="btn btn-sm btn-success border-default-border" ajaxhide>Submit</button>
                            </div>


                    </div>

                </div>
                </form>


            </div><!-- nav-tabs-custom -->


        </div><!-- /.form-box -->


    <div class="modal madal--1 fade in" id="editnewadmindataadminrights" tabindex="-1" role="dialog"
         aria-labelledby="editnewadmindataadminrights" aria-hidden="true" data-backdrop="static"
         data-keyboard="false">
        <div class="modal-dialog" >
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>

                    <h4 class="modal-title" id="editnewadmindataadminrightsLabel"
                        style="color:white;font-weight: bold">Change Rights For User</h4>
                </div>
                <div class="modal-body">

                    <div class="register-box-body" id="editnewadmindatarights">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script type="text/javascript">

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");// activated tab
            switch (target)
            {
                case "#ManageStaff":
                    $("#errors").html("");
                    $("#ManageStaffList").load("../../controller/acl/manageStaff.php");
                    break;

                case "#ManageGroup":
                    $("#errors").html("");
                    $("#ManageGroupList").load("../../controller/acl/manageGroup.php");
                    break;

                case "#ManageIP":
                    $("#errors").html("");
                    $("#ManageIPList").load("../../controller/acl/manageIP.php");
                    break;
            }
        });


        $(document).ready(function () {

            $('#modulename').chosen();
            getAgentListsAcl(1);
            //getGroupListDataPagination(1);
        });

        /**************************************************  Admin User Details **********************************************/
        function getAgentListsAcl(pageno) {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/acl/manageStaff.php",
                data: {
                    page: pageno
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#ManageStaffList").html("");
                    $("#ManageStaffList").html(data);

                },
                error: function () {
                    console.log("fail");
                }
            });
        }


        function submitUserdata()
        {



            var email= document.forms["addstaff"]["email"].value;
            var fname= document.forms["addstaff"]["fname"].value;
            var lname= document.forms["addstaff"]["lname"].value;
            // var group= document.forms["addstaff"]["group"].value;
            var tags = document.getElementsByName('groups[]');

            var permissions=new Array();
            for(var i = 0; i < tags.length; ++i)
            {
                //alert(tags[i].value);
                if(tags[i].checked)
                {
                    permissions.push(tags[i].value);
                }

            }

            operateLadda("submit_usr_data_lad","start");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    email: email,fname:fname,lname:lname,group:permissions,type:"addnew"
                },
                success: function(user) {
                    operateLadda("submit_usr_data_lad","stop");
                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg4").innerHTML = msg;
                            document.getElementById("errmsgsuc4").innerHTML = "";

                        }
                        else
                        {
                            $("#errors").html(bsAlert("success",msg));
                            document.getElementById("addstaff").reset();
                            getAgentListsAcl(1);
                            $("#basicModal .close").click();

                        }
                    }

                },
                error: function() {

                    bootbox.alert("<h4>Failed to add new admin</h4>", function() {

                    });

                }
            });

        }

        function editAdmin(email,msg){

            $.ajax({
                type: 'POST',
                url: "../../controller/acl/admin_controller.php",
                dataType: 'html',
                data: {
                    email:email,type:'editadmin'
                },
                beforeSend: function(){
                    $('#editnewadmindata').html("<center>"+showGIFDataDOTload())+"</center>";
                },
                success: function (data) {

                    // alert(data);
                    $("#editnewadmindata").html('');
                    $("#editnewadmindata").html(data);
                    if(msg!=undefined) {
                        document.getElementById("errmsg1").innerHTML = "";
                        document.getElementById("errmsgsuc1").innerHTML = msg;
                    }

                }
            });
            return false;
        }

        function submitUserdataupdate(email)
        {

            var pageno = $("#hiddenagentpage").val();

            var fname= document.forms["addstaff1"]["fname"].value;
            var lname= document.forms["addstaff1"]["lname"].value;
            var tags = document.getElementsByName('groups[]');

            var permissions=new Array();

            for(var i = 0; i < tags.length; ++i)
            {
                //alert(tags[i].value);
                if(tags[i].checked)
                {
                    permissions.push(tags[i].value);
                }

            }


            var tags1 = document.getElementsByName('permsissions[]');

            var permissions1=new Array();

            for(var i = 0; i < tags1.length; ++i)
            {
                //alert(tags[i].value);
                if(tags1[i].checked)
                {
                    permissions1.push(tags1[i].value);
                }

            }

            operateLadda("submit_usr_edit_lda","start");

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    email: email,fname:fname,lname:lname,group:permissions,userpermission:permissions1,type:"updateuser"
                },
                success: function(user) {
                    operateLadda("submit_usr_edit_lda","stop");

                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg1").innerHTML = msg;
                            document.getElementById("errmsgsuc1").innerHTML = "";
                            // document.getElementById("useradmionburron").disabled = false;
                        }
                        else
                        {

                            $("#errors").html(bsAlert("success",msg));
                            document.getElementById("addstaff1").reset();
                            // document.getElementById("useradmionburron").disabled = false;
                            editAdmin(email,msg);

                            getAgentListsAcl(pageno);
                            $("#editnewadmindataadmin .close").click();
                        }
                    }

                },
                error: function() {

                    bootbox.alert("<h4>Failed to update admin information</h4>", function() {

                    });

                }
            });

        }

        function deleteAdmin(email){

            bootbox.confirm({
                message: "<h4>Are you sure to Delete agents from keywo?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result==true)
                    {
            var pageno = $("#hiddenagentpage").val();

                        $.ajax({

                            type: "POST",
                            dataType:"json",
                            url: "../../controller/acl/admin_controller.php",
                            data: {
                                email: email,type:"deleteadmin"
                            },
                            success: function(user) {

                                if(user["errCode"]){
                                    var msg=user["errMsg"];

                                    if(user["errCode"]!="-1")
                                    {

                                        //alert(user["errMsg"]);
                                        $("#errors").html(bsAlert("success",user["errMsg"]));

                                    }
                                    else
                                    {
                                        $("#errors").html(bsAlert("success",user["errMsg"]));
                                       // alert(user["errMsg"]);
                                        getAgentListsAcl(pageno);
                                    }
                                }

                            },
                            error: function() {

                                console.log("Failed to  delete admin");

                            }
                        });
                    }else {
                        console.log("no selected");
                    }
                }
            });
        }

        function editadminfname(){
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff1.fname.value.match(/^[a-zA-Z0-9]+$/) && addstaff1.fname.value !="")
            {
                addstaff1.fname.value="";
                addstaff1.fname.focus();

                alert("Please Enter only alphabets in text");


            }
        }

        function editadminlname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff1.lname.value.match(/^[a-zA-Z0-9]+$/) && addstaff1.lname.value != "") {
                addstaff1.lname.value = "";
                addstaff1.lname.focus();
                alert("Please Enter only alphabets in text");
            }
        }


        function changeRights(email,msg){
            $.ajax({
                type: 'POST',
                url: "../../controller/acl/admin_controller.php",
                dataType: 'html',
                data: {
                    email:email,type:'changeRights'
                },
                beforeSend: function(){
                    $('#editnewadmindatarights').html("<center>"+showGIFDataDOTload())+"</center>";
                },
                success: function (data) {

                    // alert(data);
                    $("#editnewadmindatarights").html('');
                    $("#editnewadmindatarights").html(data);
                    if(msg!==undefined) {
                        document.getElementById("errmsg1").innerHTML = "";
                        document.getElementById("errmsgsuc1").innerHTML = msg;
                    }
                }
            });
            return false;
        }

        function submitUserRights(email)
        {

            var pageno = $("#hiddenagentpage").val();

            var tags1 = document.getElementsByName('permsissions[]');

            var permissions1=new Array();

            for(var i = 0; i < tags1.length; ++i)
            {
                //alert(tags[i].value);
                if(tags1[i].checked)
                {
                    permissions1.push(tags1[i].value);
                }

            }


            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    email: email,userpermission:permissions1,type:"editrights"
                },
                success: function(user) {


                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg1").innerHTML = msg;
                            document.getElementById("errmsgsuc1").innerHTML = "";
                            // document.getElementById("useradmionburron").disabled = false;
                        }
                        else
                        {
                            document.getElementById("errmsg1").innerHTML = "";
                            document.getElementById("errmsgsuc1").innerHTML = msg;
                           // changeRights(email,msg);

                            getAgentListsAcl(pageno);
                            $("#editnewadmindataadminrights .close").click();

                        }
                    }

                },
                error: function() {

                    console.log("Failed to edit  admin right");

                }
            });

        }

        /**************************************************  Admin User Details **********************************************/


        /**************************************************  Admin User group **********************************************/

        function getGroupListDataPagination(pageno) {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/acl/manageGroup.php",
                data: {
                    page: pageno
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#ManageGroupList").html("");
                    $("#ManageGroupList").html(data);

                },
                error: function () {
                    alert("fail");
                }
            });
        }

        function submitUserdatagroup()
        {
            var pageno = $("#hiddenagentpagegroup").val();
            var groupname= document.forms["addgroup"]["groupname"].value;
            // var modulename= document.forms["addgroup"]["modulename"].value;
            var tags = document.getElementsByName('permsissions[]');

            function getModules() {
                var select1 = document.getElementById("modulename");
                var selected1 = [];
                for (var i = 0; i < select1.length; i++) {
                    if (select1.options[i].selected) selected1.push(select1.options[i].value);
                }
                return selected1;
            }

            function getSubModules() {
                var select1 = document.getElementById("submodulename");
                if(select1!=null) {
                    var selected1 = [];
                    for (var i = 0; i < select1.length; i++) {
                        if (select1.options[i].selected) selected1.push(select1.options[i].value);
                    }
                    return selected1;
                }
            }

            var modulename=getModules();
            var submodulename=getSubModules();
            //alert(modulename);



            var permissions=new Array();
            for(var i = 0; i < tags.length; ++i)
            {
                //alert(tags[i].value);
                if(tags[i].checked)
                {
                    permissions.push(tags[i].value);
                }

            }
            //alert(permissions);
            operateLadda("submitusersGrpDta","start");
            $.ajax({

                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    groupname:groupname,modulename:modulename,submodulename:submodulename,permissions:permissions,type:"addgroup"
                },
                success: function(user) {
                    operateLadda("submitusersGrpDta","stop");
                    if(user["errCode"]){

                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg11").innerHTML = msg;
                            document.getElementById("errmsgsuc11").innerHTML = "";
                            // document.getElementById("useradmionburron").disabled = false;
                        }
                        else
                        {
                            $("#errors").html(bsAlert("success",msg));
                            document.getElementById("addgroup").reset();
                            getGroupListDataPagination(pageno);
                            $("#basicModal2 .close").click();
                        }
                    }

                },
                error: function() {

                    console.log("Failed to add new group");

                }
            });

        }

        function AllowAlphabetlgroupname(){
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addgroup.groupname.value.match(/^[a-zA-Z0-9]+$/) && addgroup.groupname.value !="")
            {
                addgroup.groupname.value="";
                addgroup.groupname.focus();
                alert("Please Enter only alphabets in text");


            }
        }

        $('#modulename').on('change', function () {

            var optionSelected = $("option:selected", this);
            var len=optionSelected.length;
            var i;
            var allmodules = [];
            for(i=0;i<len;i++)
            {
                var data=optionSelected[i].value;
                allmodules.push(data);
            }

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    allmodules:allmodules,type:"getsubmodules"
                },
                success: function(user) {

                    $("#submodulesdata").html('');
                    $("#submodulesdata").html(user);

                },
                error: function() {

                    bootbox.alert("<h4>Failed to get Submodules</h4>", function() {

                    });

                }
            });
        });

        function deleteGroup(id){

            var pageno = $("#hiddenagentpagegroup").val();

                bootbox.confirm({
                message: "<h4>Are you sure delete Group?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
            $.ajax({

                type: "POST",
                dataType:"json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    groupid: id,type:"deletegroup"
                },
                success: function(user) {

                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            $("#errors").html(bsAlert("danger",user["errMsg"]));
                        }
                        else
                        {

                            $("#errors").html(bsAlert("success",user["errMsg"]));
                            getGroupListDataPagination(pageno);
                        }
                    }

                },
                error: function() {

                    console.log("Failed to delete group");

                }
            });
                }
            });

        }

        function editGroup(id){
            $.ajax({
                type: 'POST',
                url: "../../controller/acl/admin_controller.php",
                dataType: 'html',
                data: {
                    groupid:id,type:'editgroup'
                },
                beforeSend: function(){
                    $('#editnewadmindatagroup').html("<center>"+showGIFDataDOTload())+"</center>";
                },
                success: function (data) {

                    //alert(data);
                    $("#editnewadmindatagroup").html('');
                    $("#editnewadmindatagroup").html(data);

                }
            });
            return false;
        }

        function getModules() {
            var select1 = document.getElementById("modulenameedit");
            var selected1 = [];
            for (var i = 0; i < select1.length; i++) {
                if (select1.options[i].selected) selected1.push(select1.options[i].value);
            }
            return selected1;
        }

        function getSubModules() {
            var select1 = document.getElementById("submodulename");
            var selected1 = [];
            for (var i = 0; i < select1.length; i++) {
                if (select1.options[i].selected) selected1.push(select1.options[i].value);
            }
            return selected1;
        }

        function getSubModulesEdit() {
            var select1 = document.getElementById("submodulenameeditdata");
            var selected1 = [];
            for (var i = 0; i < select1.length; i++) {
                if (select1.options[i].selected) selected1.push(select1.options[i].value);
            }
            return selected1;
        }

        function submiteditedGroup(id)
        {

            var pageno = $("#hiddenagentpagegroup").val();

            var groupname= document.forms["editedgroup"]["groupname"].value;
            // var modulename= document.forms["addgroup"]["modulename"].value;
            var tags = document.getElementsByName('permsissionsedit[]');

            var modulename=getModules();
            var submodulename=getSubModulesEdit();




            var permissions=new Array();
            for(var i = 0; i < tags.length; ++i)
            {
                //alert(tags[i].value);
                if(tags[i].checked)
                {
                    permissions.push(tags[i].value);
                }

            }
            operateLadda("submit_user_grp_edit","start");
            $.ajax({

                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    groupid:id,groupname:groupname,submodulename:submodulename,modulename:modulename,permissions:permissions,type:"editgroupnew"
                },
                success: function(user) {

                    operateLadda("submit_user_grp_edit","stop");
                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg112").innerHTML = msg;
                            document.getElementById("errmsgsuc112").innerHTML = "";

                        }
                        else
                        {
                            $("#errors").html(bsAlert("success",msg));
                            getGroupListDataPagination(pageno);
                            $("#editnewadmindataadmingroup .close").click();

                        }
                    }

                },
                error: function() {
                    console.log("Failed to edit group");

                }
            });

        }

        function editsubmodule() {

            var optionSelected = $("option:selected", "#modulenameedit");
            var len=optionSelected.length;
            var i;
            var allmodules = [];
            for(i=0;i<len;i++)
            {
                var data=optionSelected[i].value;
                allmodules.push(data);
            }

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    allmodules:allmodules,type:"getsubmodules"
                },
                success: function(user) {

                    $("#submodulesdatanew").html('');
                    $("#submodulesdatanew").html(user);

                },
                error: function() {

                    console.log("Failed to get Submodules");

                }
            });
        }

        function AllowAlphabetlgroupnameedit(){

            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!editedgroup.groupname.value.match(/^[a-zA-Z0-9]+$/) && editedgroup.groupname.value !="")
            {
                editedgroup.groupname.value="";
                editedgroup.groupname.focus();
                alert("Please Enter only alphabets in text");


            }
        }
        /**************************************************  Admin User group **********************************************/




        /**************************************************  Admin User IP **********************************************/
        function getIPListDataPagination(pageno)
        {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/acl/manageIP.php",
                data: {
                    page: pageno
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#ManageIPList").html("");
                    $("#ManageIPList").html(data);

                },
                error: function () {
                    alert("fail");
                }
            });
        }


        function submitaipddress()
        {

            var pageno = $("#hiddenagentpageIP").val();
            var ipaddress= document.forms["addip"]["ipname"].value;
            var ipOwner= document.forms["addip"]["ipOwner"].value;
            //lert(ipaddress);
            operateLadda("submit_ip_adr_lad","start");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/acl/admin_controller.php",
                data: {
                    ipaddress: ipaddress,type:"addnewip",ipOwner:ipOwner
                },
                success: function(user) {
                    operateLadda("submit_ip_adr_lad","stop");

                    if(user["errCode"]){
                        var msg=user["errMsg"];
                        if(user["errCode"]!="-1")
                        {
                            document.getElementById("errmsg").innerHTML = msg;
                            document.getElementById("errmsgsuc").innerHTML = "";
                            // document.getElementById("useradmionburron").disabled = false;
                        }
                        else
                        {
                            $("#errors").html(bsAlert("success",msg));
                            document.getElementById("addip").reset();
                            getIPListDataPagination(pageno);
                            $("#basicModal3 .close").click();
                        }
                    }

                },
                error: function() {

                    console.log("Failed to add new admin");

                }
            });

        }


        function deleteip(id){
            bootbox.confirm({
                message: "<h4>Are you sure delete IP Address?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
            var pageno = $("#hiddenagentpageIP").val();

                        $.ajax({

                            type: "POST",
                            dataType:"json",
                            url: "../../controller/acl/admin_controller.php",
                            data: {
                                id: id,type:"deleteip"
                            },
                            success: function(user) {

                                if(user["errCode"]){
                                    var msg=user["errMsg"];
                                    if(user["errCode"]!="-1")
                                    {
                                        $("#errors").html(bsAlert("danger",user["errMsg"]));
                                    }
                                    else
                                    {

                                        $("#errors").html(bsAlert("success",user["errMsg"]));
                                        getIPListDataPagination(pageno);
                                    }
                                }

                            },
                            error: function() {

                                console.log("Failed to  delete ip address");

                            }
                        });
                }
            });


        }


        /**************************************************  Admin User ip **********************************************/


        function AllowAlphabetfname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff.fname.value.match(/^[a-zA-Z0-9]+$/) && addstaff.fname.value != "") {
                addstaff.fname.value = "";
                addstaff.fname.focus();
                $("#errmsg4").html("Please Enter only alphabets in first name text!");
                $("[name='fname']").focus();
            }
        }

        function AllowAlphabetlname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff.lname.value.match(/^[a-zA-Z0-9]+$/) && addstaff.lname.value != "") {
                addstaff.lname.value = "";
                addstaff.lname.focus();
                $("#errmsg4").html("Please Enter only alphabets in last name text!");

                $("[name='lname']").focus();

            }
        }
        function editadminfname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff1.fname.value.match(/^[a-zA-Z0-9]+$/) && addstaff1.fname.value != "") {
                addstaff1.fname.value = "";
                addstaff1.fname.focus();
                $("#errmsg1").html("Please Enter only alphabets in first name text!");

                $("[name='fname']").focus();

            }
        }

        function editadminlname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addstaff1.lname.value.match(/^[a-zA-Z0-9]+$/) && addstaff1.lname.value != "") {
                addstaff1.lname.value = "";
                addstaff1.lname.focus();
                $("#errmsg1").html("Please Enter only alphabets in last name text!");

                document.getElementsByName("lname").focus();


            }
        }
        function AllowAlphabetlgroupname() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!addgroup.groupname.value.match(/^[a-zA-Z0-9]+$/) && addgroup.groupname.value != "") {
                addgroup.groupname.value = "";
                addgroup.groupname.focus();
                // bootbox.alert("<h4>Please Enter only alphabets in text</h4>", function() {

                //});

                $("#errmsg11").html("Please Enter only alphabets in group name text!");

                $("[name='groupname']").focus();
            }
        }


        function AllowAlphabetlgroupnameedit() {
            //if (!frm.alphabet.value.match(/^[a-zA-Z]+$/) && frm.alphabet.value !="")
            if (!editedgroup.groupname.value.match(/^[a-zA-Z0-9]+$/) && editedgroup.groupname.value != "") {
                editedgroup.groupname.value = "";
                editedgroup.groupname.focus();
                // bootbox.alert("<h4>Please Enter only alphabets in text</h4>", function() {

                //});

                $("#errmsg112").html("Please Enter only alphabets in group name text!");

                $("[name='selectdate']").focus();
            }
        }


        $('#basicModal').on('show.bs.modal', function (e) {
            $("#errmsg4").html("");
        });
        $('#basicModal2').on('show.bs.modal', function (e) {
            $("#errmsg11").html("");
        });
        $('#basicModal3').on('show.bs.modal', function (e) {
            $("#errmsg").html("");
        });




    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>