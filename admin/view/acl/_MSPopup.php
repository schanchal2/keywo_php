<?php include "../layout/header.php"; ?>
<main>
    <div class="container-fluid">
        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
        <div class="modal madal--1 fade" id="modal-id" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                        <h4 class="modal-title">Register a new member (ADMIN)</h4>
                    </div>
                    <form class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">First Name* :</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">Last Name* :</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-3 control-label p-r-0">Email* :</label>
                                <div class="col-sm-9">
                                    <input type="email" class="form-control" id="" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="form-group m-b-0">
                                <label for="" class="col-sm-3 control-label p-r-0 p-t-0">Groups :</label>
                                <div class="col-sm-9">
                                    <div class="checkbox checkbox--1 p-t-0 clearfix">
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Superadm
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Sumin 8
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Superadmin 2
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Superadmin
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Superadmin 15
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                        <label class="p-r-10pull-left">
                                            <input type="checkbox" value=""> Superadmin 2
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-sm bg-transparent border-default-border">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
