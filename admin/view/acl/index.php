<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}config/config.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keywo Admin| Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->

    <link rel="stylesheet" href="<?php echo $adminRoot;?>frontend_libraries/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $adminRoot;?>frontend_libraries/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo $adminRoot;?>frontend_libraries/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="<?php echo $adminRoot;?>frontend_libraries/images/Keywo_favicon.ico"
          type="image/x-icon">
</head>

<?php


//start config

require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');

//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
//end other

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$result = getAllRoles($connAdmin);
$rolesArray = $result["errMsg"];

$ermsg = $_GET["ermsg"];

isipaddresshasaccess($connAdmin);

 $redirectURL = $adminRoot . "index.php";

$adminEmail=strtolower($_SESSION["admin_id"]);
if($adminEmail!="")
{
    $data=getAdminData($adminEmail, $connAdmin);
    $dataEmail=strtolower($data["errMsg"]["admin_email"]);
    if ($adminEmail==$dataEmail) {
        header("Location:" . $redirectURL);
        exit;
    }
}

if($_SESSION["loginError"]!=="")
{
    $ermsg=$_SESSION["loginError"];
    unset($_SESSION["loginError"]);
}
?>


<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="javascript:;">Keywo <b>Admin</b></a>
    </div><!-- /.login-logo -->


    <div id="invalid" class="errMsg" style="text-align:center"><h4 id="errorid"><?php echo $ermsg; ?></h4></div>


    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form name="loginform" action="../../controller/acl/login_controller.php" method="POST" id="forms"
              role="search" style="text-align: left">
            <div class="form-group has-feedback">
                <input id="username" name="username" type="email" class="form-control" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>


            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <!-- <input type="checkbox"> Remember Me-->

                            <a href="rsp.php" class="cleanLabel"> Forgot Password ?</a>
                        </label>
                    </div>
                </div>

                <div class="col-xs-4">
                    <button type="submit"
                            class="btn btn-primary btn-block btn-flat">
                        Log In
                    </button>
                </div><!-- /.col -->
            </div>
        </form>


    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<script src="<?php echo $adminRoot;?>frontend_libraries/jquery/jquery.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="<?php echo $adminRoot;?>frontend_libraries/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="<?php echo $adminRoot;?>frontend_libraries/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo $adminRoot;?>frontend_libraries/js/index.js" type="text/javascript"></script>
<script>

    $("#inp_rst_pad_for").keyup(function(event){
        rstDt();
        if(event.keyCode == 13){
            $("#rstfrmsndreq").click();
        }
    });

    function rstDt()
    {
        $(".ajaxhide").attr('disabled', false);
        $("#errmsgsuc").html("");
        $("#errmsg").html("");
    }

    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>


<style>
    .errMsg {
        color: red
    }
</style>
</body>
</html>