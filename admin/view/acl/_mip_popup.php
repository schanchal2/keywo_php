<?php include "../layout/header.php"; ?>
<main>
    <div class="container-fluid">
        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Add New IP</a>
        <div class="modal madal--1 fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                        <h4 class="modal-title">Add New IP</h4>
                    </div>
                    <form class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group m-b-0">
                                <label for="" class="col-sm-3 control-label p-r-0">IP Name* :</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="" placeholder="" required>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Submit</button>
                            <button type="button" class="btn btn-sm bg-transparent border-default-border">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
