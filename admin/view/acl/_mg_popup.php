<?php include "../layout/header.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Register a new Group</a>
        <div class="modal madal--1 fade" id="modal-id">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                        <h4 class="modal-title">Register a new Group</h4>
                    </div>
                    <form class="form-horizontal">
                        <div class="modal-body">
                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0">Group Name * :</label>
                                <div class="col col-355">
                                    <input type="text" class="form-control" id="" placeholder="" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-l-0">Permision group * :</label>
                                <div class="col col-355">
                                    <div class="checkbox checkbox--1">
                                        <label>
                                            <input type="checkbox" value=""> Read
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                    </div>
                                    <div class="checkbox checkbox--1">
                                        <label>
                                            <input type="checkbox" value=""> Write
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on module :</label>
                                <div class="col col-355">
                                    <div class="select--1">
                                        <select class="chosen-select" multiple id="permision-on-module">
                                            <option value=""></option>
                                            <option value="United States">United States</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Aland Islands">Aland Islands</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on </br> Sub-Module :</label>
                                <div class="col col-355">
                                    <div class="select--1">
                                        <select class="select--1 chosen-select" multiple id="permision-on-sub-module">
                                            <option value=""></option>
                                            <option value="United States">United States</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="Afghanistan">Afghanistan</option>
                                            <option value="Aland Islands">Aland Islands</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antarctica">Antarctica</option>
                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Submit</button>
                            <button type="button" class="btn btn-sm bg-transparent border-default-border">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
$('.chosen-select').chosen();
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
