<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Access Denied !! </title>

    <link rel="stylesheet" href="../../frontend_libraries/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../frontend_libraries/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="../../frontend_libraries/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="../../frontend_libraries/images/Keywo_favicon.ico"
          type="image/x-icon">
</head>


<body class="nav-md" style="color: rgb(143, 141, 141);background: rgb(230, 224, 224);">
<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');

//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
//end other

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


$result = getAllRoles($connAdmin);
$rolesArray = $result["errMsg"];

$ermsg = $_GET["ermsg"];

$result = getallipaddress($connAdmin);
$datas = $result["errMsg"];


$ips = array();


foreach ($datas as $value) {
    array_push($ips, trim($value["ip_address"]));
}


echo $clientip = getClientIP();

if (in_array($clientip, $ips)) {
    echo "<script type=\"text/javascript\">location.href = '../index.php';</script>";

}


?>

<div class="container body">

    <div class="main_container">

        <!-- page content -->
        <div class="col-md-12">
            <div class="col-middle">
                <div class="text-center text-center">


                    <h1 class="error-number">401</h1>
                    <div class="col-xs-12 text-center"><i class="fa fa-exclamation-triangle"
                                                          style="font-size:80px;margin-bottom: 20px;color:orange;"></i>
                    </div>
                    <h2>You are not Authorized user to use this page.</h2>

                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->

</div>
<!-- footer content -->


<div id="custom_notifications" class="custom-notifications dsp_none">
    <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
    </ul>
    <div class="clearfix"></div>
    <div id="notif-group" class="tabbed_notifications"></div>
</div>


<!-- /footer content -->
</body>

</html>