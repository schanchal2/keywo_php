<?php
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";

$dataadmin=getAdminData($_SESSION["admin_id"], $connAdmin);
$dataadmin=$dataadmin["errMsg"];


?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <!-- <link href="<?php //echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet"> -->
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">My Profile</h1>
            </div>
            <br/>
            <div id="errors">

            </div>
            <div class="row">
                <div class="col-xs-3">
                    <div class="text-center">
                        <img style="cursor: pointer" onclick="changeProfilePic();" class="img-thumbnail img-responsive" width="200" src="<?php echo $adminRoot; ?>uploads/adminProfilePics/<?php echo $adminImage; ?>">
                    </div>
                </div>
                <div class="col-xs-9 p-l-0">
                    <form class="" role="form" id="updateHeaderfromnew">
                        <div class="form-group row">
                            <label for="textinput" class="col-xs-2 control-label m-l-0">First Name:</label>
                            <div class="col-xs-6">
                                <input type="text" disabled class="form-control disabledCls" name="fisrtname" value="<?= $dataadmin['admin_first_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="textinput" class="col-xs-2 control-label">Last Name:</label>
                            <div class="col-xs-6">
                                <input type="text" disabled class="form-control disabledCls" name="lastname" value="<?= $dataadmin['admin_last_name']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="textinput" class="col-xs-2 control-label">Gender:</label>
                            <div class="col-sm-6">
                                <label class="radio-inline">
                                    <input class="disabledCls" <?php if($dataadmin[ "gender"]=="Male" ){ echo "checked"; } ?> name="gender" id="input-gender-male" value="Male" type="radio" disabled />Male
                                </label>
                                <label class="radio-inline">
                                    <input class="disabledCls" <?php if($dataadmin[ "gender"]=="Female" ){ echo "checked"; } ?> name="gender" id="input-gender-female" value="Female" type="radio" disabled />Female
                                </label>
                                <label class="radio-inline">
                                    <input class="disabledCls" <?php if($dataadmin[ "gender"]=="Other" ){ echo "checked"; } ?> name="gender" id="input-gender-other" value="Other" type="radio" disabled />Other
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="select" class="col-xs-2 control-label">Country:</label>
                            <div class="col-xs-6">
                                <label id="countrySelectLabel" class="dropdownOptions">
                                    <?php
                                         $result=getAllCountry($connSearch)["errMsg"];
                                         ?>
                                        <select id="countrySelect" name="country" data-placeholder="Choose a Country..." class="chosen-select form-control disabledCls" disabled>
                                            <option data-live-search=\ "true\" data-tokens='All' id='All' value=''>All</option>
                                            <?php
                                             foreach($result as $country)
                                             {
                                                 if($dataadmin["country"]==$country['name'])
                                                 {
                                                     echo "<option value='".$country['id']."' selected>".$country['name']."</option>";
                                                 }else{
                                                     echo "<option value='".$country['id']."'>".$country['name']."</option>";
                                                 }
                                             } ?>
                                        </select>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="select" class="col-xs-2 control-label">State:</label>
                            <div class="col-xs-6">
                                <!-- <div class="input-group styled-select bg-lightgray"> -->
                                <label id="selectStateLabel" class="dropdownOptions">
                                    <select id="selectState" name="selectState" class="chosen-select form-control disabledCls" name="country" disabled>
                                        <?php

                                            $statedata=explode("/",$dataadmin["state"]);

                                            if($dataadmin["state"]!=="")
                                            {
                                                echo '<option value="'.$statedata["1"].'" selected>'.$statedata["0"].'</option>';
                                            }else
                                            {
                                                echo '<option value="selectState" selected>Select State</option>';
                                            }

                                            ?>
                                    </select>
                                </label>
                                <!-- </div> -->
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="select" class="col-xs-2 control-label">City:</label>
                            <div class="col-xs-6">
                                <!-- <div class="input-group styled-select bg-lightgray"> -->
                                <label id="selectCityLabel" class="dropdownOptions">
                                    <select id="citySelect" name="citySelect" class="chosen-select form-control disabledCls" name="country" disabled>
                                        <?php
                                            $statedata=explode("/",$dataadmin["city"]);

                                            if($dataadmin["city"]!=="")
                                            {
                                                echo '<option value="'.$statedata["1"].'" selected>'.$statedata["0"].'</option>';
                                            }else
                                            {
                                                echo '<option value="selectState" selected>Select City</option>';
                                            }

                                            ?>
                                    </select>
                                </label>
                                <!-- </div> -->
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textinput" class="col-xs-2 control-label">Mobile Number:</label>
                            <div class="col-xs-6">
                                <div class="input-group">
                                    <span class="input-group-addon" id="myphonecode">+91</span>
                                <input type="number" disabled class="form-control disabledCls" name="mobileno" value="<?= $dataadmin['mobile_no']; ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="textinput" class="col-xs-2 control-label">Zip:</label>
                            <div class="col-xs-6">
                                <input name="zip" id="zip" type="text" disabled class="form-control disabledCls" value="<?= $dataadmin['pincode']; ?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="textareainput" class="col-xs-2 control-label m-l-0">Address:</label>
                            <div class="col-xs-6">
                                <textarea name="address" maxlength="210" disabled class="form-control noresize disabledCls"><?= $dataadmin['address']; ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <label for="emailinput" class="col-xs-2 control-label">Email:</label>
                            <div class="col-xs-6">
                                <p class="skyeblue">
                                    <?= $dataadmin['admin_email']; ?>
                                </p>
                            </div>
                        </div>
                        <div class="border-top"></div>
                        <div class="row">
                            <div class="col-xs-8">
                                <div class="border-none m-t-10 formButtons">
                                    <button type="button" id="chg_dt_ps" class="btn bg-darkblue text-white m-l-5 pull-right" data-toggle="modal" data-target="#changePassword">Change Password</button>
                                    <button type="button" id="UpdateNewAdminprofnew" data-admin_id="<?php echo $_SESSION['admin_id']; ?>" class="btn bg-darkblue text-white m-l-5 pull-right">Edit Profile</button>
                                    <!-- updateNewAdminDataNew -->
                                    <a href="../dashboard/dashboard.php" type="button" class="btn btn-danger pull-right">Dashboard</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!--container closed-->
        <div class="modal madal--1 fade in" id="changePassword" role="dialog" aria-labelledby="passwordLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                        <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <div id="rederror" style="">
                            <div id="errmsgpass" style="padding: 0px;color:red;"></div>
                            <br/>
                        </div>
                        <form class="form-horizontal" name="changepassformnew" id="changepassformnew" action="javascript:;" method="post">
                            <div class="form-group row">
                                <label class="col-xs-4 control-label" for="oldpassword">Old Password:</label>
                                <div class="col-xs-8">
                                    <input type="password" id="oldpassword" name="oldpassword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xs-4 control-label" for="newpassword">New Password:</label>
                                <div class="col-xs-8">
                                    <input type="password" id="newpassword" name="newpassword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xs-4 control-label" for="renewpassword">Confirm Password:</label>
                                <div class="col-xs-8">
                                    <input type="password" id="renewpassword" name="renewpassword" class="form-control">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="modal-footer border-none p-t-0">
                                <button type="button" class="btn bg-darkblue text-white" data-dismiss="modal">Close</button>
                                <button type="button" id="useradmionburronchpass" onclick="changePassword('<?php echo $_SESSION['admin_id']; ?>')" class="btn bg-darkblue text-white"> Change Password
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script type="text/javascript">


        $( document ).ready(function() {

            var cc=$("#countrySelect").val();
            getCountryCode(cc);
        });
    $("#oldpassword,#newpassword,#renewpassword").keyup(function(event){
        if(event.keyCode == 13){
            $("#useradmionburronchpass").click();
        }
    });

    $('#changePassword').on('show.bs.modal', function (e) {
        $("#errmsgpass").html("");
    });

    $('#countrySelect').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var countryID = this.value;

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/geolocation/getStates.php",
            data: {
                countryid: countryID
            },
            success: function(states) {
                $("#selectState").html("");
                $("#selectState").append(states);
                $("#selectState").trigger("chosen:updated");
                getCountryCode(countryID);
                $('#citySelect').val('').trigger('chosen:updated');
            },
            error: function() {
                console.log("Failed to get States");
            }
        });

    });


    function getCountryCode(countryID)
    {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/geolocation/getPhonecode.php",
            data: {
                countryid: countryID
            },
            success: function(data) {

                if(data["errCode"]=="-1") {
                    $("#myphonecode").text("");
                    $("#myphonecode").text('+' + data["errMsg"].country_code_number);
                }
            },
            error: function() {
                console.log("Failed to get States");
            }
        });
    }

    $('#selectState').on('change', function(e) {
        var optionSelected = $("option:selected", this);
        var countryID = this.value;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/geolocation/getCities.php",
            data: {
                countryid: countryID
            },
            success: function(states) {
                $("#citySelect").html("");
                $("#citySelect").append(states);
                $("#citySelect").trigger("chosen:updated");
            },
            error: function() {
                console.log("Failed to get Cities");
            }
        });

    });

    function updateNewAdminDataNew(email) {

        var formData = new FormData($('#updateHeaderfromnew')[0]);
        formData.append("type", "updatemyprofile");
        formData.append("email", email);

        var str=$('#updateHeaderfromnew').serialize();
        var str=str.split("&");
        var str1=str[0].split("=");
        var str2=str[1].split("=");

        var name=str1[1]+" "+str2[1];
        $.ajax({
            type: "POST",
            dataType: "json",
            processData: false,
            contentType: false,
            url: "../../controller/acl/admin_controller.php",
            data: formData,
            beforeSend: function() {
                $("#UpdateNewAdminprofnew").attr('disabled', true);
                $('#loadng-image').show();
            },
            success: function(user) {
                $('#loadng-image').hide();
                if (user["errCode"]) {
                    var msg = user["errMsg"];
                    if (user["errCode"] != "-1") {
                        $("#errors").html(bsAlert("danger",user["errMsg"]));
                    } else {
                        $(".disabledCls").attr('disabled', true);
                       // $('.disabledCls').prop('disabled', true).trigger("chosen:updated");
                        $("#UpdateNewAdminprofnew").text("Edit Profile");
                        $(".ko_agnt_name").text(name);
                        $("#chg_dt_ps").show();

                        $("#errors").html(bsAlert("success",user["errMsg"]));
//                        $("#citySelect").chosen("destroy");
                        $("#selectCityLabel").removeClass("select--1").addClass("dropdownOptions").find("#citySelect").chosen("destroy");
                        $("#selectStateLabel").removeClass("select--1").addClass("dropdownOptions").find("#selectState").chosen("destroy");
                        $("#countrySelectLabel").removeClass("select--1").addClass("dropdownOptions").find("#countrySelect").chosen("destroy");

                        $("#countrySelect").chosen("destroy");
                        $("#selectState").chosen("destroy");


                    }
                }
                $("#UpdateNewAdminprofnew").attr('disabled', false);
            },
            error: function() {
                bootbox.alert("<h4>Failed to add Info</h4>", function() {});
            }
        });

    }


    function changePassword(email) {

        var oldp = document.forms["changepassformnew"]["oldpassword"].value;
        var newp = document.forms["changepassformnew"]["newpassword"].value;
        var rnewp = document.forms["changepassformnew"]["renewpassword"].value;

        $.ajax({

            type: "POST",
            dataType: "json",
            url: "../../controller/acl/admin_controller.php",
            data: {
                email: email,
                oldp: oldp,
                newp: newp,
                rnewp: rnewp,
                type: 'chngpass'
            },
            beforeSend: function() {
                $("#useradmionburronchpass").attr('disabled', true);
            },
            success: function(user) {
                $("#errmsgpass").html("");

                if (user["errCode"]) {
                    var msg = user["errMsg"];
                    if (user["errCode"] != "-1") {
                        //                            bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function() {
                        //
                        //                            });
                        $("#errmsgpass").html(user["errMsg"]);
                    } else {
                        bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function() {
                            location.reload();
                        });
                    }
                }
                $("#useradmionburronchpass").attr('disabled', false);
            },
            error: function() {


                bootbox.alert("<h4>Failed to change Password admin</h4>", function() {

                });

            }
        });

    }
    /**
     * to chage edit profile button to update profile with its functionality.
     * considered that button text will alwase "Edit profile" and all inputes are disabled at the begninng 
     */
    $('.formButtons').on('click',"#UpdateNewAdminprofnew", function(event) {
       // $('.disabledCls').prop('disabled', false).trigger("chosen:updated");
        // event.preventDefault();
        if ($(this).text()!="Update Profile") {
            $("#errors").html("");
            console.log($(this));
            var admin_id = $(this).data('admin_id')
            $(this).text("Update Profile")
            .attr('onclick', "updateNewAdminDataNew('"+admin_id+"')");
            $(this).parents("form").find('input, select, textarea').removeAttr('disabled');
            $("#countrySelect").chosen();
            $("#selectState").chosen();
            $("#citySelect").chosen();
            //
            $('#countrySelect').removeAttr('disabled');
            $('#selectState').removeAttr('disabled');
            $('#citySelect').removeAttr('disabled');
            $('#countrySelectLabel,#selectStateLabel,#selectCityLabel').toggleClass( 'select--1 dropdownOptions');

            $("#chg_dt_ps").hide();
        }else {
            $("#chg_dt_ps").show();
        }
        if ($(this).text()==="Update Profile") {
            console.log($(this))


        }

    });

    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
