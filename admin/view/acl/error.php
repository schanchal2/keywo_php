<?php
session_start();
require_once('../../config/config.php');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Keywo | Error</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../../frontend_libraries/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../frontend_libraries/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../../frontend_libraries/dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <link rel="shortcut icon" href="../../frontend_libraries/images/Keywo_favicon.ico"
          type="image/x-icon">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" style="background-color: #ecf0f5;">


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <!-- Content Header (Page header) -->
        <section class="content">
            <h1>
                Error
            </h1>
            <ol class="breadcrumb">
                <li><a href="../dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="index.php">Login</a></li>

            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="error-page">
                <h2 class="headline text-yellow fa fa-warning"></h2>

                <div class="error-content">
                    <h2><i class="headline text-yellow"></i> The application has encountered an unknown error.
                        It doesn't appear to have affected your data, but our technical staff have been automatically
                        notified and will be looking into this with the utmost urgency.</h2>

                </div>
                <!-- /.error-content -->
            </div>
            <!-- /.error-page -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->


</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../frontend_libraries/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="../../frontend_libraries/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../frontend_libraries/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../frontend_libraries/dist/js/demo.js"></script>
</body>
</html>