<?php
session_start();
require_once "../config/config.php";
if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    $url=$adminView."dashboard/dashboard.php";
    header("Location:{$url}");
}else
{
    $url=$adminView."acl/index.php";
    header("Location:{$url}");
}
?>