<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Internal Tickets</h1><span>Admin</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="">Department</th>
                                <th class="">IT Received Total</th>
                                <th class="">Open</th>
                                <th class="">Closed</th>
                                <th class=""><i class="fa fa-calendar" id=""></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> CS</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td id=""> UAM</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td id=""> Tracking</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td id=""> Accounts</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td id=""> MM</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>

                            <tr>
                                <td id=""> Total</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <?php
                                    $date = date("Y-m-d");
                                    ?>
                                    <input style="width: 190px" type="text" id="mainModuleDate" class="form-control"
                                           value='<?php echo $date . " to " . $date; ?>'
                                           placeholder="Select Date Range">
                                </div>

                                <div class="input-group styled-select" >

                                    <input style="width: 110px;z-index: 1" type="text" id="TickedID" class="form-control" value='' placeholder="Ticket ID">

                                    <span class="btn input-group-addon" type="submit" onclick="mainModuleAdminTickets()"><i class="fa fa-search"></i>  </span>

                                </div>

                                <div class="input-group styled-select" >
                                    <select id="departmetData" style="width: 150px">
                                        <option value="" disabled selected> Department</option>
                                        <option value="cs"> CS</option>
                                        <option value="uam"> UAM</option>
                                        <option value="tracking"> Tracking</option>
                                        <option value="accounts"> Accounts</option>
                                        <option value="mm"> MM</option>
                                        <option value="all"> All</option>
                                    </select>
                                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                </div>

                                <div class="input-group styled-select" >
                                    <select id="assignedAgent" style="width: 145px">
                                        <option value="" disabled selected> Assigned to</option>
                                        <option value="cs"> dd</option>
                                        <option value="uam"> ddd</option>
                                        <option value="tracking"> ddd</option>
                                        <option value="all"> All</option>
                                    </select>
                                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                </div>

                                <div class="form-inline pull-right" style="margin-right: 10px;">

                                    <button type="button" class="btn btn-info">Re-Open</button>
                                </div>

                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="mainModuleAdminAjax">
            </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $(document).ready(function () {
            mainModuleAdminTickets();
        });

        function mainModuleAdminTickets() {

            var refferaldatevalue = ($("#mainModuleDate").val());
            var datedata = refferaldatevalue.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var departmetData = ($("#departmetData").val());
            var assignedAgent = ($("#assignedAgent").val());
            var TickedID = ($("#TickedID").val());

            var limit = ($("#LimitedResult").val());
            if (((typeof limit) == "undefined") || limit == "") {
                limit = 10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/interTickets/mainModule.php",
                data: {
                    fromdate: fromdate,
                    todate: todate,
                    departmetData: departmetData,
                    assignedAgent: assignedAgent,
                    TickedID: TickedID,
                    limit: limit,
                    listtype: "first"
                },
                success: function (data) {
                    $("#mainModuleAdminAjax").html("");
                    $("#mainModuleAdminAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#mainModuleDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function (event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function (event, obj) {

            mainModuleAdminTickets();
        });


        $("#LimitedResult").change(function () {
            mainModuleAdminTickets();
        });

        $("#departmetData").change(function () {
            mainModuleAdminTickets();
        });

        $("#assignedAgent").change(function () {
            mainModuleAdminTickets();
        });



    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>