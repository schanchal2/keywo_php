<?php 

include "../layout/header.php"; ?>
<main>
    <div class="container-fluid">
        <?php include "{$docRootAdmin}view/layout/titlebar.php" ?>
        <!-- 








         -->
        <div class="row">
            <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
            <!-- 


         -->
            <div class="modal internalTicket fade" id="modal-id">
                <div class="modal-dialog internalTicket-dialog">
                    <form>
                        <div class="modal-content internalTicket-content">
                            <div class="modal-header internalTicket-header border-none padding-bottom-none">
                                <button type="button" class="close text-blue" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <!-- 


                         -->
                            <div class="modal-body internalTicket-body p-t-0">
                                <div class="row">
                                    <div class="col-xs-12 ">
                                        <div class="form-horizontal">
                                            <fieldset class="m-b-10">
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">To :</label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group materialize  border-lightGray text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">Cc :</label>
                                                    <div class="col-sm-5">
                                                        <div class="input-group materialize border-lightGray text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="bg-lightGray1 p-b-10>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">IT Query Type :</label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group materialize border-transparent text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">IT Ticket ID :</label>
                                                    <div class="col-sm-7">
                                                        <div class="input-group materialize border-transparent text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                    <label for="inputPassword3" class="col-sm-1 control-label p-r-0 text-right">Dated :</label>
                                                    <div class="col-sm-2">
                                                        <div class="input-group materialize border-transparent text-Gray">
                                                            <input placeholder="__/__/____" type="text" class="form-control text-Gray p-l-0" value="">
                                                            <span class="input-group-addon text-Gray p-r-0"> <i class="fa fa-calendar" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="m-b-10">
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">Agent comments :</label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group materialize border-lightGray text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon   text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">MM comments :</label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group materialize border-lightGray text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="" placeholder="">
                                                            <span class="input-group-addon text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <fieldset class="bg-lightGray1 p-b-10>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 control-label p-r-0 text-right">Raised By :</label>
                                                    <div class="col-sm-10">
                                                        <div class="input-group materialize border-transparent text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="Jerry (UAM)" plceholder="">
                                                            <span class="input-group-addon  text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-0">
                                                    <label for="inputPassword3" class="col-sm-2 col-sm-offset-1 control-label p-l-0">(TAT - 3 woerking days)</label>
                                                    <!-- <div class="col-sm-6">
                                                        <div class="input-group materialize border-lightGray text-Gray">
                                                            <input placeholder="" type="text" class="form-control text-Gray p-l-0" value="3 woerking days" plceholder="">
                                                            <span class="input-group-addon  text-Gray"> <i class="fa fa-group" ></i> </span>
                                                        </div>
                                                    </div> -->
                                                    <label for="inputPassword3" class="col-sm-1 col-sm-offset-6 control-label p-r-0 text-right ">Closed On :</label>
                                                    <div class="col-sm-2">
                                                        <div class="input-group materialize border-transparent text-Gray">
                                                            <input placeholder="__/__/____" type="text" class="form-control text-Gray p-l-0" value="">
                                                            <span class="input-group-addon  text-Gray"> <i class="fa fa-calendar" ></i> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer internalTicket-footer bg-lightBlue">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <div class="form-group pull-left">
                                                <label for="exampleInputName2" class="text-white">Closed By :</label>
                                                <input type="text" class="form-control bg-transparent border-none materialize box-shadow-none " id="exampleInputName2" placeholder="">
                                            </div>
                                            <div class="form-group pull-right">
                                                <label class="text-white">Priority :</label>
                                                <div class="btn-group text-white " role="group" aria-label="...">
                                                    <a class="text-white" href=""> High </a> /
                                                    <a class="text-white" href=""> Normal </a> /
                                                    <a class="text-white" href=""> Low </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group pull-right m-b-0">
                                            <div class="btn-group btn-group-sm" role="group" aria-label="...">
                                                <button type="button" class="btn bg-gray border-none text-white">Submit</button>
                                                <button type="button" class="btn border-none ">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>
                </div>
                </form>
            </div>
        </div>
        <!-- <a class="btn btn-primary" data-toggle="modal" href='#modal-id2'>Trigger modal</a> -->
        <div class="modal fade" id="modal-id2">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox"> Remember me
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-default">Sign in</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
