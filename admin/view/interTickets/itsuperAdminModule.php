<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <style>
        #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
            width: 170px;
        }
    </style>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Internal Tickets</h1><span>Admin</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="">Raised Ticket</th>
                                <th class="">Closed Ticket</th>
                                <th class="">Open Ticket</th>
                                <th class="">Re-Open Ticket</th>
                                <th class=""><i class="fa fa-calendar" id=""></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td id=""> 0</td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">
                                <div class="input-group">
                                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                    <?php
                                    $date = date("Y-m-d");
                                    ?>
                                    <input style="width: 190px" type="text" id="superAdminDate" class="form-control"
                                           value='<?php echo $date . " to " . $date; ?>'
                                           placeholder="Select Date Range">
                                </div>

                                <div class="form-inline pull-right" style="margin-right: 10px;">
                                    <label class="dropdownOptions">
                                        <select id="ticketType" class="form-control" style="    width: 145px;">
                                            <option value="" disabled selected> Ticket Type</option>
                                            <option value="Raised"="">Raised</option>
                                            <option value="Open"="">Open</option>
                                            <option value="Re-Open">Re-Open</option>
                                            <option value="Closed">Closed</option>
                                            <option value="all">All</option>
                                        </select>
                                    </label>
                                </div>

                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="superAdminAjax">
            </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $(document).ready(function () {
            superAdminModule();
        });

        function superAdminModule() {

            var refferaldatevalue = ($("#superAdminDate").val());
            var datedata = refferaldatevalue.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var ticketType = ($("#ticketType").val());

            var limit = ($("#LimitedResult").val());
            if (((typeof limit) == "undefined") || limit == "") {
                limit = 10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/interTickets/superAdminModule.php",
                data: {
                    fromdate: fromdate,
                    todate: todate,
                    ticketType: ticketType,
                    limit: limit,
                    listtype: "first"
                },
                success: function (data) {
                    $("#superAdminAjax").html("");
                    $("#superAdminAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#superAdminDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function (event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function (event, obj) {

            superAdminModule();
        });


        $("#LimitedResult").change(function () {
            superAdminModule();
        });

        $("#ticketType").change(function () {
            superAdminModule();
        });



    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>