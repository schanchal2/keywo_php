<?php
session_start();

require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/cms/landingContent_model.php");


?>

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Content Settings</h1><span>Landing Page</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-pills" style="background: #97e5f8;">
                                <li class="active"><a href="#manage_image" data-toggle="tab">How Does One</a></li>
                                <!--<li><a href="#manage_video" data-toggle="tab">Feature-How to Earn</a></li>
                                <li><a href="#manage_keywo" data-toggle="tab">Keywo Information</a></li>
                                <li><a href="#manage_title" data-toggle="tab">Page Title & Banner</a></li>-->
                            </ul>
                            <br>
                            <div class="row " style="float:right">      
                                <div id="how_does_on_window" class="">          
                                    <a class="btn btn-success" id="addNewUser" data-toggle="modal" data-target="#addFAQ"  style="margin-bottom: 15px;margin-right: 15px;background: #0299be;">
                                        Add New Record <span class="fa fa-plus-circle"></span>
                                    </a>
                                    
                                </div>
                            </div>
                            <div id="how_does_on_data" class="">                                   

                                    </div>
                            
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                </div>
            </div>
        </div>        
    </main>
    </div>

    <!--modal to display editing data-->
    <div class="modal fade" id="addFAQ" role="dialog">
        <div class="modal-dialog" style="width: 870px;">
            <div class="    ">
                <div class="clearfix closebutton-absolute text-right">
                    <i class="fa fa-times close-button close-dialog" data-dismiss="modal" style="font-size: 25px; color: #ddd;"></i>
                </div>
                <div class="modal-body" style="background-color: #eeeeee;">
                    <div style="margin: 50px">
                        <form>   
                        <input type="hidden" name="faq-id" id = "faq-id" value = "">
                        <input type="hidden" name="categosry_faq" id = "categosry_faq" value="how does one">                         
                            <div class="form-group">
                                <label for="question_faq">Question:</label>
                                <textarea class="form-control" rows="3" name = "question_faq" id="question_faq"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="answer_faq">Answer:</label>
                                <textarea class="form-control" rows="3" name = "answer_faq" id="answer_faq"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="video_link_faq">Video Link:</label>
                                <input type = "text" class="form-control" name = "video_link_faq" id="video_link_faq">
                            </div>
                            <div id = "faq_video_src" class="form-group" >
                                
                            </div>
                            <div class = "form-group " id = "add_faq_error" style="color:red;"></div>
                            <button id = "FAQSubmit" type="button" class="btn btn-default pull-right" onclick="submitFaqDetailModel();" >Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/js/app.js"></script>
    <script>

    $(document).ready(function() {
        LoadSupportFAQList('how does one');
        // getSupportFaqPages(0, 'how does one',10);
    });



    $('#addFAQ #video_link_faq').on('keyup',function(e){
        showVideoLink();
    });
    $('#addFAQ #video_link_faq').on('click',function(e){
        showVideoLink();
    });

    function showVideoLink() {
        var link = $('#addFAQ #video_link_faq').val();
        if (link != '') {
            link = link.replace("watch?v=", "embed/");
            var iframe = '<center><iframe id = "faq_video_src" src="'+link+'" width="600" height="300"></center>';
            $('#addFAQ #faq_video_src').html(iframe);
        }
    }

    $("#addFAQ").on('hide.bs.modal', function() {
        $('#faq-questn-id').val('');
        $('#question_faq').val('');
        $('#answer_faq').val('');
        $('#video_link_faq').val('');
        $('#faq_video_src').html('');
        $('#add_faq_error').text('');
    });

    function submitFaqDetailModel() {
        $('#add_faq_error').text('');
        var id             = $("#addFAQ #faq-id").val();
        var category_faq   = $("#addFAQ #categosry_faq").val();
        var question_faq   = $("#addFAQ #question_faq").val();
        var answer_faq     = $("#addFAQ #answer_faq").val();
        var video_link_faq = $("#addFAQ #video_link_faq").val();
        video_link_faq = video_link_faq.replace("watch?v=", "embed/");
        if (category_faq  != '' & question_faq != '' & video_link_faq != '') {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../controller/customerSupport/hod/supportRightSideFaqSubmit.php",
                data: {
                    category_faq:   category_faq,
                    question_faq:   question_faq,
                    answer_faq:     answer_faq,
                    video_link_faq: video_link_faq,
                    id:             id
                },
                success: function (data) {     
                    if (data.errCode == -1) {
                        console.log(data);      
                        $("#addFAQ").modal("hide");                        
                        if (id){                            
                            loadSupportPageAfterAction()
                        } else {
                            LoadSupportFAQList('how does one');
                        }    
                    }                
                },
                beforeSend: function(data) {
                    $('#addFAQ #FAQSubmit').css('pointer-events','none');
                },
                complete: function(data) {
                    $('#addFAQ #FAQSubmit').css('pointer-events','auto');
                },
                error: function(data) {
                    alert('Failed');
                } 

            });
        } else {
            $('#add_faq_error').text('All Fields Are Mandetory...!');
        }
    }

    function LoadSupportFAQList(category) {
        
        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 10;
        }
        $.ajax({
            type: "POST",
            dataType: "HTML",
            url: "../../controller/customerSupport/faq/support_Faq_table.php",
            data: {
                category:category,limit:limit
            },
            success: function (data) {
                $("#how_does_on_data").html("");
                $("#how_does_on_data").html(data);
            },
            beforeSend: function(data) {
                
            },
            complete: function(data) {
                
            },
            error: function(data) {
                alert('Failed');
            } 

        });
    }



    function getSupportFaqPages(pageNo, category,limit) {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/customerSupport/faq/support_Faq_table.php",
            data: {
                limit: limit,
                category: category,
                page: pageNo
            },
            success: function (data) {
                $("#how_does_on_data").html("");
                $("#how_does_on_data").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

    function loadSupportPageAfterAction()
        {
            var pageNo   = $('#hiddenpage').val();
            var limit    = ($("#LimitedResult").val());
            var category = $('#knowledgeCategory').val();
            getSupportFaqPages(pageNo, category,limit);
        }

    function deleteFaq(id) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "../../controller/customerSupport/faq/deleteSupportFaq.php",
            data: {
                id: id
            },
            success: function (data) {
                if (data.errCode == -1) {
                    loadSupportPageAfterAction();  
                } else {
                    alert('Error In Delete');
                }      
            },
            beforeSend: function(data) {
                
            },
            complete: function(data) {
                
            },
            error: function(data) {
                alert('Failed');
            } 
        });
    }

    function EditSupportFaq(id, que, ans, link) {       
        $('#faq-id').val(id);
        $('#question_faq').val(que);
        $('#answer_faq').val(ans);
        var iframe = '<center><iframe id = "faq_video_src" src="'+link+'" width="600" height="300"></center>';
        $('#faq_video_src').html(iframe);
        $('#video_link_faq').val(link);
        $("#addFAQ").modal("show");
    }

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>