<?php
session_start();

require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
    require_once("../../model/cms/landingContent_model.php");
    ?>

<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Content Settings</h1><span>News RSS</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-pills" style="background: #97e5f8;" role="tablist">
                            <li class="pull-left header"><i class="fa fa-flag"></i> </li>
                            <li class="active"><a href="#edit" id="editTabLine" data-toggle="tab">Edit/Delete Channels</a></li>
                                <li><a href="#Add" data-toggle="tab">Add Channels</a></li>



                        </ul>
                        <br/>
                        <div class="tab-content">
                            <div class="tab-pane" id="Add" style=" border: 1px solid #0299be;padding: 20px;">

                                <form id="addForm" method="POST" action="javascript:;">
                                    <div class="form-group col-xs-12">
                                        <label for="fileName">Channel Name : <span style="color:#3BB9FF"> (ex.yahooPolitics)</span></label>
                                        <input type="text" class="form-control" name="fileName" id="fileName" >
                                        <span id="status1"></span>
                                    </div>
                                    <div class="form-group col-xs-12">
                                        <label for="rssUrl">RSS URL:</label>
                                        <input type="text" class="form-control" name="rssUrl" id="rssUrl" >
                                        <span id="status"></span>
                                    </div>

                                    <div class="form-group col-xs-6">
                                        <label>Category <span style="color:#3BB9FF"> (ex.yahoo politics)</span></label>
                                        <input class="form-control" name="category" type="text" >
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <label>Update Frequency in Hours<span
                                                    style="color:#3BB9FF"> (ex.2)</span></label>
                                        <input class="form-control" name="frequency" type="number" min="1" >
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <label>Source <span style="color:#3BB9FF"> (ex.Yahoo News)</span></label>
                                        <input class="form-control" name="source" type="text" >
                                    </div>
                                    <div class="form-group col-xs-6">
                                        <label>Country</label>
                                        <select name="country" class="form-control" >
                                            <option value="">Country...</option>
                                            <option value="International" style="color:red">International</option>
                                            <option value="Afganistan">Afghanistan</option>
                                            <option value="Albania">Albania</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="American Samoa">American Samoa</option>
                                            <option value="Andorra">Andorra</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Anguilla">Anguilla</option>
                                            <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                            <option value="Argentina">Argentina</option>
                                            <option value="Armenia">Armenia</option>
                                            <option value="Aruba">Aruba</option>
                                            <option value="Australia">Australia</option>
                                            <option value="Austria">Austria</option>
                                            <option value="Azerbaijan">Azerbaijan</option>
                                            <option value="Bahamas">Bahamas</option>
                                            <option value="Bahrain">Bahrain</option>
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Barbados">Barbados</option>
                                            <option value="Belarus">Belarus</option>
                                            <option value="Belgium">Belgium</option>
                                            <option value="Belize">Belize</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Bermuda">Bermuda</option>
                                            <option value="Bhutan">Bhutan</option>
                                            <option value="Bolivia">Bolivia</option>
                                            <option value="Bonaire">Bonaire</option>
                                            <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Brazil">Brazil</option>
                                            <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                            <option value="Brunei">Brunei</option>
                                            <option value="Bulgaria">Bulgaria</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cambodia">Cambodia</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Canada">Canada</option>
                                            <option value="Canary Islands">Canary Islands</option>
                                            <option value="Cape Verde">Cape Verde</option>
                                            <option value="Cayman Islands">Cayman Islands</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Channel Islands">Channel Islands</option>
                                            <option value="Chile">Chile</option>
                                            <option value="China">China</option>
                                            <option value="Christmas Island">Christmas Island</option>
                                            <option value="Cocos Island">Cocos Island</option>
                                            <option value="Colombia">Colombia</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo">Congo</option>
                                            <option value="Cook Islands">Cook Islands</option>
                                            <option value="Costa Rica">Costa Rica</option>
                                            <option value="Cote DIvoire">Cote D'Ivoire</option>
                                            <option value="Croatia">Croatia</option>
                                            <option value="Cuba">Cuba</option>
                                            <option value="Curaco">Curacao</option>
                                            <option value="Cyprus">Cyprus</option>
                                            <option value="Czech Republic">Czech Republic</option>
                                            <option value="Denmark">Denmark</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Dominica">Dominica</option>
                                            <option value="Dominican Republic">Dominican Republic</option>
                                            <option value="East Timor">East Timor</option>
                                            <option value="Ecuador">Ecuador</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="El Salvador">El Salvador</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Estonia">Estonia</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Falkland Islands">Falkland Islands</option>
                                            <option value="Faroe Islands">Faroe Islands</option>
                                            <option value="Fiji">Fiji</option>
                                            <option value="Finland">Finland</option>
                                            <option value="France">France</option>
                                            <option value="French Guiana">French Guiana</option>
                                            <option value="French Polynesia">French Polynesia</option>
                                            <option value="French Southern Ter">French Southern Ter</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Georgia">Georgia</option>
                                            <option value="Germany">Germany</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Gibraltar">Gibraltar</option>
                                            <option value="Great Britain">Great Britain</option>
                                            <option value="Greece">Greece</option>
                                            <option value="Greenland">Greenland</option>
                                            <option value="Grenada">Grenada</option>
                                            <option value="Guadeloupe">Guadeloupe</option>
                                            <option value="Guam">Guam</option>
                                            <option value="Guatemala">Guatemala</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guyana">Guyana</option>
                                            <option value="Haiti">Haiti</option>
                                            <option value="Hawaii">Hawaii</option>
                                            <option value="Honduras">Honduras</option>
                                            <option value="Hong Kong">Hong Kong</option>
                                            <option value="Hungary">Hungary</option>
                                            <option value="Iceland">Iceland</option>
                                            <option value="India">India</option>
                                            <option value="Indonesia">Indonesia</option>
                                            <option value="Iran">Iran</option>
                                            <option value="Iraq">Iraq</option>
                                            <option value="Ireland">Ireland</option>
                                            <option value="Isle of Man">Isle of Man</option>
                                            <option value="Israel">Israel</option>
                                            <option value="Italy">Italy</option>
                                            <option value="Jamaica">Jamaica</option>
                                            <option value="Japan">Japan</option>
                                            <option value="Jordan">Jordan</option>
                                            <option value="Kazakhstan">Kazakhstan</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Kiribati">Kiribati</option>
                                            <option value="Korea North">Korea North</option>
                                            <option value="Korea Sout">Korea South</option>
                                            <option value="Kuwait">Kuwait</option>
                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                            <option value="Laos">Laos</option>
                                            <option value="Latvia">Latvia</option>
                                            <option value="Lebanon">Lebanon</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Liechtenstein">Liechtenstein</option>
                                            <option value="Lithuania">Lithuania</option>
                                            <option value="Luxembourg">Luxembourg</option>
                                            <option value="Macau">Macau</option>
                                            <option value="Macedonia">Macedonia</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malaysia">Malaysia</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Maldives">Maldives</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Malta">Malta</option>
                                            <option value="Marshall Islands">Marshall Islands</option>
                                            <option value="Martinique">Martinique</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Mayotte">Mayotte</option>
                                            <option value="Mexico">Mexico</option>
                                            <option value="Midway Islands">Midway Islands</option>
                                            <option value="Moldova">Moldova</option>
                                            <option value="Monaco">Monaco</option>
                                            <option value="Mongolia">Mongolia</option>
                                            <option value="Montserrat">Montserrat</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Myanmar">Myanmar</option>
                                            <option value="Nambia">Nambia</option>
                                            <option value="Nauru">Nauru</option>
                                            <option value="Nepal">Nepal</option>
                                            <option value="Netherland Antilles">Netherland Antilles</option>
                                            <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                            <option value="Nevis">Nevis</option>
                                            <option value="New Caledonia">New Caledonia</option>
                                            <option value="New Zealand">New Zealand</option>
                                            <option value="Nicaragua">Nicaragua</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Niue">Niue</option>
                                            <option value="Norfolk Island">Norfolk Island</option>
                                            <option value="Norway">Norway</option>
                                            <option value="Oman">Oman</option>
                                            <option value="Pakistan">Pakistan</option>
                                            <option value="Palau Island">Palau Island</option>
                                            <option value="Palestine">Palestine</option>
                                            <option value="Panama">Panama</option>
                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                            <option value="Paraguay">Paraguay</option>
                                            <option value="Peru">Peru</option>
                                            <option value="Phillipines">Philippines</option>
                                            <option value="Pitcairn Island">Pitcairn Island</option>
                                            <option value="Poland">Poland</option>
                                            <option value="Portugal">Portugal</option>
                                            <option value="Puerto Rico">Puerto Rico</option>
                                            <option value="Qatar">Qatar</option>
                                            <option value="Republic of Montenegro">Republic of Montenegro</option>
                                            <option value="Republic of Serbia">Republic of Serbia</option>
                                            <option value="Reunion">Reunion</option>
                                            <option value="Romania">Romania</option>
                                            <option value="Russia">Russia</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="St Barthelemy">St Barthelemy</option>
                                            <option value="St Eustatius">St Eustatius</option>
                                            <option value="St Helena">St Helena</option>
                                            <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                            <option value="St Lucia">St Lucia</option>
                                            <option value="St Maarten">St Maarten</option>
                                            <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                            <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines
                                            </option>
                                            <option value="Saipan">Saipan</option>
                                            <option value="Samoa">Samoa</option>
                                            <option value="Samoa American">Samoa American</option>
                                            <option value="San Marino">San Marino</option>
                                            <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Serbia">Serbia</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Singapore">Singapore</option>
                                            <option value="Slovakia">Slovakia</option>
                                            <option value="Slovenia">Slovenia</option>
                                            <option value="Solomon Islands">Solomon Islands</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="Spain">Spain</option>
                                            <option value="Sri Lanka">Sri Lanka</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Suriname">Suriname</option>
                                            <option value="Swaziland">Swaziland</option>
                                            <option value="Sweden">Sweden</option>
                                            <option value="Switzerland">Switzerland</option>
                                            <option value="Syria">Syria</option>
                                            <option value="Tahiti">Tahiti</option>
                                            <option value="Taiwan">Taiwan</option>
                                            <option value="Tajikistan">Tajikistan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Thailand">Thailand</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tokelau">Tokelau</option>
                                            <option value="Tonga">Tonga</option>
                                            <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Turkey">Turkey</option>
                                            <option value="Turkmenistan">Turkmenistan</option>
                                            <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                            <option value="Tuvalu">Tuvalu</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Ukraine">Ukraine</option>
                                            <option value="United Arab Erimates">United Arab Emirates</option>
                                            <option value="United Kingdom">United Kingdom</option>
                                            <option value="United States of America">United States of America</option>
                                            <option value="Uraguay">Uruguay</option>
                                            <option value="Uzbekistan">Uzbekistan</option>
                                            <option value="Vanuatu">Vanuatu</option>
                                            <option value="Vatican City State">Vatican City State</option>
                                            <option value="Venezuela">Venezuela</option>
                                            <option value="Vietnam">Vietnam</option>
                                            <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                            <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                            <option value="Wake Island">Wake Island</option>
                                            <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                            <option value="Yemen">Yemen</option>
                                            <option value="Zaire">Zaire</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>
                                    </div>


                                    <input type="hidden" name="add" value="add">
                                    <?php


                                    echo '<button type="submit" id="addbt" onclick="addNewsChannel()" class="btn btn-primary center-block">Submit
                                        </button>';

                                    ?>
                                </form>
                            </div><!-- /.tab-pane -->
                            <div class="tab-pane  active" id="edit">

                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-4 pull-right" style="">
                                            <label>Search Channels:</label>
                                            <input class="col-md" id="myInputTextField" type="text"
                                                   onkeyup=getAllChannelsSearch() placeholder="search channels here" style="  border: 1px solid #008B8B;" size="25">
                                        </div>
                                    </div>
                                    <div class="" id="channelsNewsByID">

                                    </div>
                                    <div class="modal fade" id="myModal" role="dialog">
                                        <div class="modal-dialog modal-lg" style="text-align:left">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close"
                                                            data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Edit News Channel RSS</h4>
                                                </div>
                                                <div class="modal-body" id="editChannel"></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                        Close
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.tab-pane -->
                        </div><!-- /.tab-content -->
                    </div><!-- nav-tabs-custom -->
                </div><!-- /.col -->
            </div> <!-- /.row -->


        </div>
    </div>


</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>
    $(document).ready(function () {
        getAllChannels(1,"");


        function alignModal() {
            var modalDialog = $(this).find(".modal-dialog");

            // Applying the top margin on modal dialog to align it vertically center
            modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 3));
        }

        // Align modal when it is displayed
        $(".modal").on("shown.bs.modal", alignModal);

        // Align modal when user resize the window
        $(window).on("resize", function () {
            $(".modal:visible").each(alignModal);
        });
    });


    document.getElementById("rssUrl").onblur = function () {
        var xmlhttp;
        var rssUrl = document.getElementById("rssUrl");
        if (rssUrl.value != "") {
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            }
            else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                    var str = xmlhttp.responseText;
                    var resp = str.slice(0, -1);
                    document.getElementById("status").innerHTML = resp;
                    var lastChar = str.substr(str.length - 1);
                    if (lastChar == 1) {
                        document.getElementById("addbt").disabled = true;
                    } else {
                        document.getElementById("addbt").disabled = false;
                    }
                }
            };
            xmlhttp.open("GET", "../../controller/news/news_crud/rssAvailability.php?rssUrl=" + encodeURIComponent(rssUrl.value), true);
            xmlhttp.send();
        }
    };

    document.getElementById("fileName").onblur = function () {
        var xmlhttp;
        var fileName = document.getElementById("fileName");
        if (fileName.value != "") {
            if (window.XMLHttpRequest) {
                xmlhttp = new XMLHttpRequest();
            }
            else {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                    var str = xmlhttp.responseText;
                    var resp = str.slice(0, -1);
                    document.getElementById("status1").innerHTML = resp;
                    var lastChar = str.substr(str.length - 1);
                    if (lastChar == 1) {
                        document.getElementById("addbt").disabled = true;
                    } else {
                        document.getElementById("addbt").disabled = false;
                    }
                }
            };
            xmlhttp.open("GET", "../../controller/news/news_crud/rssAvailability.php?fileName=" + encodeURIComponent(fileName.value), true);
            xmlhttp.send();
        }
    };

  function getAllChannels(pageno,channel) {
        var limit = 10;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/cms/getChannels.php",
            data: {
                page: pageno,channel:channel
            },
            success: function (data) {
                $("#channelsNewsByID").html("");
                $("#channelsNewsByID").html(data);

            },
            error: function () {
                console.log("fail");
            }
        });
    }

    function getAllChannelsSearch()
    {

        var channel=$("#myInputTextField").val();

        getAllChannels(1,channel);
    }

    function deleteChannel(id) {
        bootbox.confirm({
            title: 'Delete News Channels',
            message: "<h4>Are you sure to  delete this channel  with id " + id+"</h4>",
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-default'
                },
                'confirm': {
                    label: 'Continue',
                    className: 'btn-success pull-right'
                }
            },
            callback: function (result) {

                if (result) {
                    $.ajax({
                        type: "POST",
                        url: "../../controller/news/news_crud/delete_news_channel.php",
                        data: {id: id},
                        dataType: "json",
                        success: function (user) {


                            if (user["errCode"]) {
                                if (user["errCode"] == "-1") {
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                        getAllChannels(1,"");
                                    });

                                }
                                else {
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                    });
                                }

                            }
                        },
                        error: function () {
                            bootbox.alert("<h4>Failed to Delete Channel !!</h4>", function () {
                            });
                        }
                    });

                }
            }
        });


    }


    function editChannel(id) {


        $.ajax({
            type: 'GET',
            url: '../../controller/news/news_crud/newsrss_toUpdate.php',
            dataType: 'html',
            data: {channel_id: id},
            success: function (data) {
                $("#editChannel").html('');
                $("#editChannel").html(data);
            }
        });
        return false;
    }


    function updateNewsChannel()
    {

        var limit=$("#hiddenagentpage").val();
        var channel=$("#myInputTextField").val();


        var formData = new FormData($("#editForm")[0]);

                    $.ajax({
                        type: "POST",
                        url: "../../controller/news/news_crud/update_news_channel.php",
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function (user) {


                            if (user["errCode"]) {
                                if (user["errCode"] == "-1") {
                                    $("#myModal").modal("hide");
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                        getAllChannels(limit,channel);
                                        document.getElementById("editForm").reset();
                                    });

                                }
                                else {
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                    });
                                }

                            }
                        },
                        error: function () {
                            bootbox.alert("<h4>Failed to Update Channel !!</h4>", function () {
                            });
                        }
                    });


    }


    function addNewsChannel()
    {

        var limit=$("#hiddenagentpage").val();
        var channel=$("#myInputTextField").val();


        var formData = new FormData($("#addForm")[0]);

                    $.ajax({
                        type: "POST",
                        url: "../../controller/news/news_crud/newsAdminInterface.php",
                        data: formData,
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        dataType: "json",
                        success: function (user) {


                            if (user["errCode"]) {
                                if (user["errCode"] == "-1") {
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                        getAllChannels(limit,channel);
                                        $("#editTabLine").trigger("click");
                                        document.getElementById("addForm").reset();
                                    });

                                }
                                else {
                                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function () {
                                    });
                                }

                            }
                        },
                        error: function () {
                            bootbox.alert("<h4>Failed to Add Channel !!</h4>", function () {
                            });
                        }
                    });


    }



</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>