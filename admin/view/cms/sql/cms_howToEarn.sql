/*
Navicat MySQL Data Transfer

Source Server         : localServerAWS
Source Server Version : 100120
Source Host           : 192.168.2.250:3306
Source Database       : dbsearch

Target Server Type    : MYSQL
Target Server Version : 100120
File Encoding         : 65001

Date: 2017-06-22 10:38:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_howToEarn
-- ----------------------------
DROP TABLE IF EXISTS `cms_howToEarn`;
CREATE TABLE `cms_howToEarn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_title` varchar(255) DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `long_description` text,
  `content_video_link` varchar(255) DEFAULT NULL,
  `feature_type` varchar(255) DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
