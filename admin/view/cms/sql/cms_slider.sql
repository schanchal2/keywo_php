/*
Navicat MySQL Data Transfer

Source Server         : localServerAWS
Source Server Version : 100120
Source Host           : 192.168.2.250:3306
Source Database       : dbsearch

Target Server Type    : MYSQL
Target Server Version : 100120
File Encoding         : 65001

Date: 2017-06-22 10:38:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_slider
-- ----------------------------
DROP TABLE IF EXISTS `cms_slider`;
CREATE TABLE `cms_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slider_image_consumer` varchar(255) DEFAULT NULL,
  `slider_image_producer` varchar(255) DEFAULT NULL,
  `shortDescConsumer` varchar(255) DEFAULT NULL,
  `shortDescProducer` varchar(255) DEFAULT NULL,
  `orders` int(11) DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
