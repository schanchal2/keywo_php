/*
Navicat MySQL Data Transfer

Source Server         : localServerAWS
Source Server Version : 100120
Source Host           : 192.168.2.250:3306
Source Database       : dbkeywords

Target Server Type    : MYSQL
Target Server Version : 100120
File Encoding         : 65001

Date: 2017-06-21 17:43:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for keywo_features_faq
-- ----------------------------
DROP TABLE IF EXISTS `keywo_features_faq`;
CREATE TABLE `keywo_features_faq` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category` varchar(1000) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `video_link` varchar(1000) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS=1;
