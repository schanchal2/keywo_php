/*
Navicat MySQL Data Transfer

Source Server         : localServerAWS
Source Server Version : 100120
Source Host           : 192.168.2.250:3306
Source Database       : dbsearch

Target Server Type    : MYSQL
Target Server Version : 100120
File Encoding         : 65001

Date: 2017-06-22 10:39:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for meta_tags
-- ----------------------------
DROP TABLE IF EXISTS `meta_tags`;
CREATE TABLE `meta_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) NOT NULL,
  `page_title` varchar(255) NOT NULL,
  `meta_name` text NOT NULL,
  `meta_content` text NOT NULL,
  `timestamp` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
SET FOREIGN_KEY_CHECKS=1;
