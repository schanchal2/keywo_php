/*
Navicat MySQL Data Transfer

Source Server         : localServerAWS
Source Server Version : 100120
Source Host           : 192.168.2.250:3306
Source Database       : dbsearch

Target Server Type    : MYSQL
Target Server Version : 100120
File Encoding         : 65001

Date: 2017-06-22 10:38:39
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cms_LandingPage
-- ----------------------------
DROP TABLE IF EXISTS `cms_LandingPage`;
CREATE TABLE `cms_LandingPage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) DEFAULT NULL,
  `banner_image` varchar(255) DEFAULT NULL,
  `last_updated_timestamp` datetime DEFAULT NULL,
  `last_updated_by` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
