<?php
session_start();

require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
    require_once("../../model/cms/landingContent_model.php");
    ?>

<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Content Settings</h1><span>Metadata</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div class="content-wrapper">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="row " style="float:right">
                                <form class="form-inline">
                                    <a href="#" class="btn btn-success" data-toggle="modal"
                                       data-target="#addContent"
                                       style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                        New Content <span
                                            class="fa fa-plus-circle"></span></a>
                                </form>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                      <div id="metaContentAjax">

                      </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->


                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="addContent" tabindex="-1" role="dialog" aria-labelledby="addContent" aria-hidden="true"
         data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="addContentLabel" style="color:white;font-weight: bold">Add New
                        Content</h4>
                </div>
                <div class="modal-body CustomPaddingModal" >

                    <div id="redError" style="">
                        <div id="errMsgFailed" style="padding: 0px;color:red;"></div>
                    </div>

                    <div id="greenError" style="">
                        <div id="errMsgSuccess" style="padding: 0px;color:green;"></div>
                    </div>
                    <div class="register-box-body">
                        <form id="addContentForm" enctype="multipart/form-data" method="post" name="addContentForm"
                              class="form-horizontal form-label-left">

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label class="control-label">Page Name<span class="">*</span>
                                                </label>
                                                <div class="">
                                                    <input type="text" class="form-control col-md-7 col-xs-12"
                                                           name="pageName" id="pageName">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label class="control-label">Page Title<span class="">*</span>
                                                </label>
                                                <div class="">
                                                    <input type="text" class="form-control col-md-7 col-xs-12"
                                                           name="pageTitle" id="pageTitle">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label class="control-label">Meta Name<span class="">*</span>
                                                </label>
                                                <div class="">
                                                    <input type="text" class="form-control col-md-7 col-xs-12"
                                                           name="metaName" id="metaName">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="form-group">
                                                <label class="control-label">Meta Content<span class="">*</span>
                                                </label>
                                                <div class="">
                        <textarea class="form-control col-md-7 col-xs-12" style="text-indent: 1px;" name="metaContent"
                                  id="metaContent">
                          </textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="ln_solid"></div>
                            <div class="form-group">
                                <div class="col-sm-12 text-right">
                                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div><!-- /.form-box -->
            </div>
        </div>
    </div>
    <!--code start model add new images-->
    <div class="modal fade" id="editContent" tabindex="-1" role="dialog" aria-labelledby="editContent"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="editContentLabel" style="color:white;font-weight: bold">Edit
                        Content</h4>
                </div>
                <div class="modal-body CustomPaddingModal">

                    <div id="redError" style="">
                        <div id="errMsgFailedMeta" style="padding: 0px;color:red;"></div>
                    </div>

                    <div id="greenError" style="">
                        <div id="errMsgSuccessMeta" style="padding: 0px;color:green;"></div>
                    </div>
                    <div class="register-box-body" id="editContenBodyCon">
                    </div>
                </div><!-- /.form-box -->
            </div>
        </div>
    </div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>
    $(document).ready(function () {
        getMetaTagPagination(1);
    });
    function getMetaTagPagination(pageno) {
        var limit = 10;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/cms/metaContentAjax.php",
            data: {
                page: pageno
            },
            success: function (data) {
                $("#metaContentAjax").html("");
                $("#metaContentAjax").html(data);

            },
            error: function () {
                alert("fail");
            }
        });
    }

    function deleteMetaData(id) {
        var pageno = $("#hiddenpagemetatag").val();
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/cms/metacontent_manager.php",
            data: {
                id: id, type: "delete"
            },
            success: function (data) {
                getMetaTagPagination(pageno);
            },
            error: function () {
                console.log("Error in Deleting Meta Data");

            }
        });
    }

    function editMetaData(id) {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/cms/metacontent_manager.php",
            data: {
                id: id, type: "edit"
            },
            success: function (user) {

                $("#editContenBodyCon").html(user);

            },
            error: function () {
                console.log("Error in Editing Meta Data");

            }
        });
    }

    $("#addContentForm").submit(function (event) {

        var pageno = $("#hiddenpagemetatag").val();
        //disable the default form submission

        event.preventDefault();
        //grab all form data
        var formData = new FormData($(this)[0]);
        formData.append("type", "insert");
        $.ajax({
            url: '../../controller/cms/metacontent_manager.php',
            type: 'POST',
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function (returndata) {
                var errCode = returndata["errCode"];
                var errMsg = returndata["errMsg"];
                if (errCode == "-1") {
                    $("#errMsgSuccess").text("");
                    $("#errMsgFailed").text("");
                    $("#errMsgSuccess").text(errMsg);
                    getMetaTagPagination(pageno);
                    $('#addContent').modal('hide');
                    document.getElementById("addContentForm").reset();
                    $("#errMsgSuccess").text("");
                } else {
                    $("#errMsgSuccess").text("");
                    $("#errMsgFailed").text("");
                    $("#errMsgFailed").text(errMsg);
                }
            }
        });

        return false;
    });

    function UpdatedFeature(id) {

        var pageno = $("#hiddenpagemetatag").val();
        var formData = new FormData($("#addContentFormMeta")[0]);
        formData.append("type", "update");
        formData.append("id", id);
        $.ajax({
            url: '../../controller/cms/metacontent_manager.php',
            type: 'POST',
            data: formData,
            dataType: "json",
            cache: false,
            processData: false,
            contentType: false,
            success: function (returndata) {
                var errCode = returndata["errCode"];
                var errMsg = returndata["errMsg"];
                if (errCode == "-1") {
                    $("#errMsgSuccessMeta").text("");
                    $("#errMsgFailedMeta").text("");
                    $("#errMsgSuccessMeta").text(errMsg);
                getMetaTagPagination(pageno);
                $('#editContent').modal('hide');
                    $("#errMsgSuccessMeta").text("");
                } else {
                    $("#errMsgSuccessMeta").text("");
                    $("#errMsgFailedMeta").text("");
                    $("#errMsgFailedMeta").text(errMsg);
                }
            }
        });
    }

</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>