<?php
session_start();

require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/cms/landingContent_model.php");

$data = selectTableRowCount("cms_slider", $connSearch);
$countSlider = $data["errMsg"][0]["count"] + 1;
$countSliderold = $data["errMsg"][0]["count"];

$cms_FeatureCount = selectTableRowCount("cms_keywoFeatures", $connSearch);
$cms_keywoFeatures = $cms_FeatureCount["errMsg"][0]["count"];

$cms_howToEarn = selectTableRowCount("cms_howToEarn", $connSearch);
$cms_howToEarn = $cms_howToEarn["errMsg"][0]["count"];

$getBannerTitle = getBannerTitle($connSearch);
$getBannerTitle = $getBannerTitle["errMsg"][0];
?>

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Content Settings</h1><span>Landing Page</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-pills" style="background: #97e5f8;">
                                <li class="active"><a href="#manage_image" data-toggle="tab">Consumer Producer</a></li>
                                <li><a href="#manage_video" data-toggle="tab">Feature-How to Earn</a></li>
                                <li><a href="#manage_keywo" data-toggle="tab">Keywo Information</a></li>
                                <li><a href="#manage_title" data-toggle="tab">Page Title & Banner</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="manage_image">

                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-header">
                                                    <br/>
                                                    <div class="row " style="float:right">
                                                        <form class="form-inline" id="SlidererrorStatus">
                                                            <?php
                                                            if ($countSliderold == 10) {
                                                                echo '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';

                                                            } else {
                                                                echo '<a href="#" class="btn btn-success" data-toggle="modal"
                                                       data-target="#addContent"
                                                       style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                                        New Content <span class="fa fa-plus-circle"></span></a>';
                                                            }
                                                            ?>
                                                        </form>

                                                    </div>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="loading-div"></div>
                                                <div class="box-body" id="CMSConsumerUploaderAjax">

                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->


                                            <!-- /.box -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->

                                </div>


                                <div class="tab-pane" id="manage_video">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-header">
                                                    <br/>
                                                    <div class="row " style="float:right">
                                                        <form class="form-inline" id="SlidererrorStatusFeatureInfo">
                                                            <?php
                                                            if ($cms_howToEarn == 10) {
                                                                echo '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';

                                                            } else {
                                                                echo '<a href="#" class="btn btn-success" data-toggle="modal"
                                                       data-target="#addContentFeature"
                                                       style="margin-bottom: 5px;margin-right: 15px;background: #0299be;" >Add
                                                        New Content <span class="fa fa-plus-circle"></span></a>';
                                                            }
                                                            ?>

                                                        </form>

                                                    </div>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body">
                                                    <div class="box-body" id="CMSVideoInfoAjax">

                                                    </div>

                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->


                                            <!-- /.box -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>

                                <div class="tab-pane" id="manage_keywo">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="box">
                                                <div class="box-header">
                                                    <br/>
                                                    <div class="row " style="float:right">
                                                        <form class="form-inline" id="SlidererrorStatusKeywoInfo">
                                                            <?php
                                                            if ($cms_keywoFeatures == 10) {
                                                                echo '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';

                                                            } else {
                                                                echo '<a href="#" class="btn btn-success" data-toggle="modal"
                                                       data-target="#addContentFeaturekeywo"
                                                       style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                                        New Content <span class="fa fa-plus-circle"></span></a>';
                                                            }
                                                            ?>
                                                        </form>

                                                    </div>
                                                </div>
                                                <!-- /.box-header -->
                                                <div class="box-body" id="CMSFeatureInfokeywoAjax">

                                                </div>
                                                <!-- /.box-body -->
                                            </div>
                                            <!-- /.box -->


                                            <!-- /.box -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="manage_title">
                                    <div class="row">
                                        <br/>
                                        <div class="col-xs-12">
                                            <table id="CMSFeatureInfo" class="table table-bordered table-hover">
                                                <?php
                                                $page_title = $getBannerTitle["page_title"];
                                                $banner_image = $getBannerTitle["banner_image"];
                                                ?>
                                                <tbody>
                                                <tr>
                                                    <th style="width:100px">Page Title</th>
                                                    <td id="LandingTitleAjax"><?php echo $page_title; ?></td>
                                                </tr>
                                                <tr>
                                                    <th style="width:100px">Banner Title</th>
                                                    <td id="LandingBannerAjax">

                                                        <?php echo $banner_image; ?>

                                                    </td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>

                                                    <td colspan="2" style="text-align: right">
                                                        <a href="#" class="" id="editTitleBannerbutton"
                                                           data-toggle="modal"
                                                           data-target="#editTitleBanner"
                                                           onclick="editTitleBanner('<?php echo $getBannerTitle["id"]; ?>')"><span
                                                                class="btn btn-xs btn-primary fa fa-edit"></span>
                                                    </td>
                                                </tr>

                                                </tfoot>
                                            </table>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>

                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addContent" tabindex="-1" role="dialog" aria-labelledby="addContent"
             aria-hidden="true"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addContentLabel" style="color:white;font-weight: bold">Add New
                            Content</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailedc" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccessc" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body">
                            <form id="addContentForm" enctype="multipart/form-data" method="post" name="addContentForm"
                                  class="form-horizontal form-label-left">

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Uploader Image<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="file" class="form-control col-md-7 col-xs-12"
                                                               name="consumerImage" id="consumerImage">
                                                        <div id="consumerImageHolder"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Consumer Image<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="file" class="form-control col-md-7 col-xs-12"
                                                               name="uploaderImage" id="uploaderImage">
                                                        <div id="uploaderImageHolder"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <!--<div class="col-sm-12">
                                            <div class="col-sm-2" style="float:right">
                                                <div class="form-group">
                                                    <label class="control-label">Image Order<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <select class="form-control col-sm-2 text-right" name="contentorder" id="contentorder">

                                                            <?php
                                    //                                                            for($i=1;$i<=$countSlider;$i++) {
                                    //
                                    //                                                                if(!in_array($i,$myorders))
                                    //                                                                {
                                    //                                                                    echo "<option value='{$i}'>{$i}</option>";
                                    //                                                                }
                                    //
                                    //                                                            }
                                    ?>
                                                        </select>
                                                    </div>
                                                </div>

                                    </div>
                                </div>-->


                                    <div class="col-sm-12 text-right">
                                        <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel
                                        </button>

                                        <button type="submit" id="submitSlider" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>
        <div class="modal fade" id="editTitleBanner" tabindex="-1" role="dialog" aria-labelledby="editTitleBanner"
             aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editTitleBannerkeywo" style="color:white;font-weight: bold">Edit
                            Title &
                            Banner</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailedt" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccesst" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body" id="editTitleBannerHTML">

                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="addContentFeature" tabindex="-1" role="dialog" aria-labelledby="addContentFeature"
             aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addContentLabelFeature" style="color:white;font-weight: bold">Add
                            New
                            Content</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailedFeature" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccessFeature" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body">
                            <form id="addContentFormFeature" enctype="multipart/form-data" method="post"
                                  name="addContentFormFeature" class="form-horizontal form-label-left">


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Feature<span class="">*</span>
                                                    </label>
                                                    <div class="">

                                                        <select class="form-control col-md-7 col-xs-12"
                                                                name="contentType"
                                                                id="contentType" disabled="disabled">
                                                            <option value="Keywo Features">Keywo Features</option>
                                                            <option value="How to Earn?">How to Earn?</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Content Title<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="text" class="form-control col-md-7 col-xs-12"
                                                               name="contentTitle" id="contentTitle">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Content Video URL<span
                                                            class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="text" class="form-control col-md-7 col-xs-12"
                                                               name="contentVideoURL" id="contentVideoURL">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Short Description<span
                                                            class="">*</span>
                                                    </label>
                                                    <div class="">
                                                <textarea class="form-control col-md-7 col-xs-12 noresize"
                                                          name="contentShortDesc" id="contentShortDesc"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Long Description<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                <textarea class="form-control col-md-7 col-xs-12 noresize" name="contentLongDesc"
                                                          id="contentLongDesc"> </textarea></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel
                                        </button>

                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="editContentFeature" tabindex="-1" role="dialog" aria-labelledby="editContentFeature"
             aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editContentLabelFeature" style="color:white;font-weight: bold">Edit
                            Content</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailed" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccess" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body" id="editVideoDataContent">


                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="addContentFeaturekeywo" tabindex="-1" role="dialog"
             aria-labelledby="addContentFeaturekeywo"
             aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="addContentLabelFeaturekeywo" style="color:white;font-weight: bold">
                            Add
                            New
                            Content</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailedFeaturek" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccessFeaturek" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body">
                            <form id="addContentFormFeaturekeywo" enctype="multipart/form-data" method="post"
                                  name="addContentFormFeaturekeywo" class="form-horizontal form-label-left">


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Feature<span class="">*</span>
                                                    </label>
                                                    <div class="">

                                                        <select class="form-control col-md-7 col-xs-12"
                                                                name="contentType"
                                                                id="contentType" disabled="disabled">
                                                            <option value="Keywo Features">Keywo Features</option>
                                                            <option value="How to Earn?">How to Earn?</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                            <div class="col-sm-4">
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Content Title<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="text" class="form-control col-md-7 col-xs-12"
                                                               name="contentTitle" id="contentTitle">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Content Video URL<span
                                                            class="">*</span>
                                                    </label>
                                                    <div class="">
                                                        <input type="text" class="form-control col-md-7 col-xs-12"
                                                               name="contentVideoURL" id="contentVideoURL">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Short Description<span
                                                            class="">*</span>
                                                    </label>
                                                    <div class="">
                                                <textarea class="form-control col-md-7 col-xs-12 noresize"
                                                          name="contentShortDesc" id="contentShortDesc"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="form-group">
                                                    <label class="control-label">Long Description<span class="">*</span>
                                                    </label>
                                                    <div class="">
                                                <textarea class="form-control col-md-7 col-xs-12 noresize" name="contentLongDesc"
                                                          id="contentLongDesc"> </textarea></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-sm-12 text-right">
                                        <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel
                                        </button>

                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>
        <div class="modal fade" id="editContentFeaturekeywo" tabindex="-1" role="dialog"
             aria-labelledby="editContentFeaturekeywo" aria-hidden="true" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" style="width:850px">
                <div class="modal-content">
                    <div class="modal-header ModalHeaderBackground">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editContentLabelFeaturekeywo" style="color:white;font-weight: bold">
                            Edit
                            Content</h4>
                    </div>
                    <div class="modal-body CustomPaddingModal">

                        <div id="redError" style="">
                            <div id="errMsgFailedk" style="padding: 0px;color:red;"></div>
                        </div>

                        <div id="greenError" style="">
                            <div id="errMsgSuccessk" style="padding: 0px;color:green;"></div>
                        </div>
                        <div class="register-box-body" id="editVideoDataContentkeywo">


                        </div>
                    </div><!-- /.form-box -->
                </div>
            </div>
        </div>
    </main>
    </div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/js/app.js"></script>
    <script>

        $("#consumerImage").on('change', function () {
            previewImage("consumerImage", "consumerImageHolder", "thumb-image");
        });

        $("#uploaderImage").on('change', function () {
            previewImage("uploaderImage", "uploaderImageHolder", "thumb-image");
        });

        $("#addContentForm").submit(function (event) {
            //disable the default form submission
            event.preventDefault();
            //grab all form data
            var formData = new FormData($(this)[0]);
            var pageno = $("#hiddenpage").val();
            formData.append("type", "insert");
            $.ajax({
                url: '../../controller/cms/landingcontent_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {
                    var errCode = returndata["errCode"];
                    var errMsg = returndata["errMsg"];
                    if (errCode == "-1") {
                        $("#errMsgSuccessc").text("");
                        $("#errMsgFailedc").text("");
                        $("#errMsgSuccessc").text(errMsg);
                        var count = checkCount();
                        if (count == 10) {
                            var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                            $("#SlidererrorStatus").html("");
                            $("#SlidererrorStatus").html(data);
                        } else {
                            var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContent" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add New Content <span class="fa fa-plus-circle"></span></a>';
                            $("#SlidererrorStatus").html("");
                            $("#SlidererrorStatus").html(data);
                        }

                        getSliderPagination(1);
                        $('#addContent').modal('hide');
                        document.getElementById("addContentForm").reset();
                        $("#errMsgSuccessc").text("");
                        $("#consumerImageHolder").html("");
                        $("#uploaderImageHolder").html("");
                    }else
                    {
                        $("#errMsgSuccessc").text("");
                        $("#errMsgFailedc").text("");
                        $("#errMsgFailedc").text(errMsg);
                    }
                }
            });

            return false;
        });

        $(document).ready(function () {
            getSliderPagination(1);
            getFeatureInfoPagination(1);
            getKeywoInfoPagination(1);
        });
        function getSliderPagination(pageno) {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/landingSlider.php",
                data: {
                    page: pageno
                },
                success: function (data) {
                    $("#CMSConsumerUploaderAjax").html("");
                    $("#CMSConsumerUploaderAjax").html(data);

                },
                error: function () {
                    alert("fail");
                }
            });
        }

        function deleteChannel(id) {

            bootbox.confirm({
                message: "<h4>Are you sure to delete data?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result==true)
                    {
                        var pageno = $("#hiddenpage").val();
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "../../controller/cms/landingcontent_manager.php",
                            data: {
                                id: id, type: "delete"
                            },
                            success: function (data) {
                                if(data["errCode"]=="-1") {
                                    alert(data["errMsg"]);
                                    var count = checkCount();
                                    if (count == 10) {
                                        var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                                        $("#SlidererrorStatus").html("");
                                        $("#SlidererrorStatus").html(data);
                                    } else {
                                        var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContent" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add New Content <span class="fa fa-plus-circle"></span></a>';
                                        $("#SlidererrorStatus").html("");
                                        $("#SlidererrorStatus").html(data);
                                    }

                                    getSliderPagination(pageno);
                                }else
                                {
                                    alert(data["errMsg"]);
                                }
                            },
                            error: function () {
                                console.log("Error in Deleting Slider Data");

                            }
                        });
                    }else {
                        console.log("no selected");
                    }
                }
            });

        }

        function checkCount() {

            var count = "";
            $.ajax({
                type: "POST",
                dataType: "json",
                async: false,
                url: "../../controller/cms/landingcontent_manager.php",
                data: {
                    type: "sliderCount"
                },
                success: function (data) {
                    count = data["errMsg"];
                },
                error: function () {
                    console.log("Error in Counting Slider Data");

                }
            });
            return count;
        }


        function editTitleBanner(id) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/landingcontentInfo_manager.php",
                data: {
                    id: id, type: "editBanner", table: "cms_LandingPage"
                },
                success: function (user) {

                    $("#editTitleBannerHTML").html(user);

                },
                error: function () {
                    bootbox.alert("<h4>Failed to Edit Data.</h4>", function () {
                    });

                }
            });
        }


        function UpdatedFeatureData(id) {
            var formData = new FormData($("#editContentFormTitle")[0]);
            formData.append("type", "updateBanner");
            formData.append("table", "cms_LandingPage");
            formData.append("id", id);
            $.ajax({
                url: '../../controller/cms/landingcontentInfo_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {
                    var msg = returndata['errMsg'];
                    if (returndata['errCode'] == -1) {
                        $("#errMsgFailedt").html("");
                        $("#errMsgSuccesst").html(msg);
                        $("#LandingTitleAjax").html("");
                        $("#LandingTitleAjax").html(returndata['pageTitle']);
                        $("#LandingBannerAjax").html("");
                        $("#LandingBannerAjax").html(returndata['pageBanner']);
                        $('#editTitleBanner').modal('hide');
                    } else {
                        $("#errMsgSuccesst").html("");
                        $("#errMsgFailedt").html(msg);
                    }
                }
            });
        }


        function getFeatureInfoPagination(pageno) {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/featureInfoVideo.php",
                data: {
                    page: pageno
                },
                success: function (data) {
                    $("#CMSVideoInfoAjax").html("");
                    $("#CMSVideoInfoAjax").html(data);

                },
                error: function () {
                    alert("fail");
                }
            });
        }

        $("#addContentFormFeature").submit(function (event) {
            //disable the default form submission

            event.preventDefault();
            //grab all form data
            var formData = new FormData($(this)[0]);
            var pageno = $("#hiddenpagefeatureinfo").val();
            formData.append("type", "insert");
            formData.append("table", "cms_howToEarn");
            $.ajax({
                url: '../../controller/cms/landingcontentInfo_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {

                    var errCode = returndata["errCode"];
                    var errMsg = returndata["errMsg"];
                    if (errCode == "-1") {
                        $("#errMsgSuccessFeature").text("");
                        $("#errMsgFailedFeature").text("");
                        $("#errMsgSuccessFeature").text(errMsg);

                    var count = checkFeatureInfoCount();
                    if (count == 10) {
                        var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                        $("#SlidererrorStatusFeatureInfo").html("");
                        $("#SlidererrorStatusFeatureInfo").html(data);
                    } else {
                        var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContentFeature" style="margin-bottom: 5px;margin-right: 15px;" >Add New Content <span class="fa fa-plus-circle"></span></a>';
                        $("#SlidererrorStatusFeatureInfo").html("");
                        $("#SlidererrorStatusFeatureInfo").html(data);
                    }

                    getFeatureInfoPagination(pageno);

                        document.getElementById("addContentFormFeature").reset();
                        $('#addContentFeature').modal('hide');
                        $("#errMsgSuccessFeature").text("");
                    } else {
                        $("#errMsgFailedFeature").text("");
                        $("#errMsgSuccessFeature").text("");
                        $("#errMsgFailedFeature").text(errMsg);
                    }
                }
            });

            return false;
        });

        function deleteCMSFeatureInfo(id) {

            bootbox.confirm({
                message: "<h4>Are you sure delete Data?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result==true)
                    {
                        var pageno = $("#hiddenpagefeatureinfo").val();
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "../../controller/cms/landingcontentInfo_manager.php",
                            data: {
                                id: id, type: "delete", table: "cms_howToEarn"
                            },
                            success: function (user) {
                                if(user["errCode"]=="-1") {
                                    alert(user["errMsg"]);

                                    var count = checkFeatureInfoCount();
                                    if (count == 10) {
                                        var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                                        $("#SlidererrorStatusFeatureInfo").html("");
                                        $("#SlidererrorStatusFeatureInfo").html(data);
                                    } else {
                                        var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContentFeature" style="margin-bottom: 5px;margin-right: 15px;" >Add New Content <span class="fa fa-plus-circle"></span></a>';
                                        $("#SlidererrorStatusFeatureInfo").html("");
                                        $("#SlidererrorStatusFeatureInfo").html(data);
                                    }

                                    getFeatureInfoPagination(pageno);
                                }else
                                {
                                    alert(user["errMsg"]);
                                }
                            },
                            error: function () {
                                console.log("Error in Deleting Feature Info Data");

                            }
                        });
                    }else {
                        console.log("no selected");
                    }
                }
            });

        }

        function editCMSFeatureInfo(id) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/landingcontentInfo_manager.php",
                data: {
                    id: id, type: "edit", table: "cms_howToEarn"
                },
                success: function (user) {

                    $("#editVideoDataContent").html(user);

                },
                error: function () {
                    console.log("Failed to Edit Data");

                }
            });
        }

        function UpdatedFeature(id) {
            var pageno = $("#hiddenpagefeatureinfo").val();
            var formData = new FormData($("#editContentFormFeature")[0]);
            formData.append("type", "update");
            formData.append("table", "cms_howToEarn");
            formData.append("id", id);
            $.ajax({
                url: '../../controller/cms/landingcontentInfo_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {
                    var msg = returndata['errMsg'];
                    if (returndata['errCode'] == -1) {
                        $("#errMsgSuccess").text("msg");
                        $("#errMsgFailed").text("");
                        $("#errMsgSuccess").text("msg");
                        getFeatureInfoPagination(pageno);
                        $('#editContentFeature').modal('hide');
                        $("#errMsgSuccess").text("");
                    } else {
                        $("#errMsgFailed").text("");
                        $("#errMsgSuccess").text("");
                        $("#errMsgFailed").text(msg);
                        console.log("error to update feature data");
                    }
                }
            });
        }


        function checkFeatureInfoCount() {

            var count = "";
            $.ajax({
                type: "POST",
                dataType: "json",
                async: false,
                url: "../../controller/cms/landingcontent_manager.php",
                data: {
                    type: "cms_howToEarnCount"
                },
                success: function (data) {
                    count = data["errMsg"];
                },
                error: function () {
                    console.log("Error in Counting Slider Data");

                }
            });
            return count;
        }

        function getKeywoInfoPagination(pageno) {
            var limit = 10;
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/featureInfoKeywo.php",
                data: {
                    page: pageno
                },
                success: function (data) {
                    $("#CMSFeatureInfokeywoAjax").html("");
                    $("#CMSFeatureInfokeywoAjax").html(data);

                },
                error: function () {
                    alert("fail");
                }
            });
        }


        function deleteCMSFeatureInfokeywo(id) {


            bootbox.confirm({
                message: "<h4>Are you sure to Delete this data?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result==true)
                    {
                        var pageno = $("#hiddenpagekeywoinfo").val();
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "../../controller/cms/landingcontentInfo_manager.php",
                            data: {
                                id: id, type: "delete", table: "cms_keywoFeatures"
                            },
                            success: function (user) {
                                if(user["errCode"]=="-1") {

                                    alert(user["errMsg"]);
                                    var count = checkKeywoInfoCount();
                                    if (count == 10) {
                                        var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                                        $("#SlidererrorStatusKeywoInfo").html("");
                                        $("#SlidererrorStatusKeywoInfo").html(data);
                                    } else {
                                        var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContentFeaturekeywo" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add New Content <span class="fa fa-plus-circle"></span></a>';
                                        $("#SlidererrorStatusKeywoInfo").html("");
                                        $("#SlidererrorStatusKeywoInfo").html(data);
                                    }

                                    getKeywoInfoPagination(pageno);
                                }else {
                                    alert(user["errMsg"]);
                                }

                            },
                            error: function () {
                                console.log("Error in Deleting Feature Info Data");

                            }
                        });
                    }else {
                        console.log("no selected");
                    }
                }
            });

        }

        function editCMSFeatureInfokeywo(id) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/cms/landingcontentInfo_manager.php",
                data: {
                    id: id, type: "edit", table: "cms_keywoFeatures"
                },
                success: function (user) {

                    $("#editVideoDataContentkeywo").html(user);

                },
                error: function () {
                    console.log("Failed to Edit Data");

                }
            });
        }

        function UpdatedFeatureKeywo(id) {
            var pageno = $("#hiddenpagekeywoinfo").val();
            var formData = new FormData($("#editContentFormFeature")[0]);
            formData.append("type", "update");
            formData.append("table", "cms_keywoFeatures");
            formData.append("id", id);
            $.ajax({
                url: '../../controller/cms/landingcontentInfo_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {
                    var msg = returndata['errMsg'];
                    if (returndata['errCode'] == -1) {
                        $("#errMsgSuccessk").text("msg");
                        $("#errMsgFailedk").text("");
                        $("#errMsgSuccessk").text("msg");
                        getKeywoInfoPagination(pageno);
                        $('#editContentFeaturekeywo').modal('hide');
                        $("#errMsgSuccessk").text("");
                    } else {
                        $("#errMsgFailedk").text("");
                        $("#errMsgSuccessk").text("");
                        $("#errMsgFailedk").text(msg);
                        console.log("error to update feature data");
                    }
                }
            });
        }

        function checkKeywoInfoCount() {

            var count = "";
            $.ajax({
                type: "POST",
                dataType: "json",
                async: false,
                url: "../../controller/cms/landingcontent_manager.php",
                data: {
                    type: "cms_keywoFeatures"
                },
                success: function (data) {
                    count = data["errMsg"];
                },
                error: function () {
                    console.log("Error in Counting Slider Data");

                }
            });
            return count;
        }

        $("#addContentFormFeaturekeywo").submit(function (event) {
            //disable the default form submission

            event.preventDefault();
            //grab all form data
            var formData = new FormData($(this)[0]);
            var pageno = $("#hiddenpagekeywoinfo").val();
            formData.append("type", "insert");
            formData.append("table", "cms_keywoFeatures");
            $.ajax({
                url: '../../controller/cms/landingcontentInfo_manager.php',
                type: 'POST',
                data: formData,
                dataType: "json",
                cache: false,
                processData: false,
                contentType: false,
                success: function (returndata) {
                    var errCode = returndata["errCode"];
                    var errMsg = returndata["errMsg"];
                    if (errCode == "-1") {
                        $("#errMsgSuccessFeaturek").text("");
                        $("#errMsgFailedFeaturek").text("");
                        $("#errMsgSuccessFeaturek").text(errMsg);
                        var count = checkKeywoInfoCount();
                        if (count == 10) {
                            var data = '<a href="#" class="btn btn-warning" style="margin-bottom: 5px;margin-right: 15px;">You Reached Maximum limit.Only 10 Records Allowed to add.</a>';
                            $("#SlidererrorStatusKeywoInfo").html("");
                            $("#SlidererrorStatusKeywoInfo").html(data);
                        } else {
                            var data = '<a href="#" class="btn btn-success" data-toggle="modal" data-target="#addContentFeaturekeywo" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add New Content <span class="fa fa-plus-circle"></span></a>';
                            $("#SlidererrorStatusKeywoInfo").html("");
                            $("#SlidererrorStatusKeywoInfo").html(data);
                        }

                        getKeywoInfoPagination(pageno);
                        document.getElementById("addContentFormFeaturekeywo").reset();
                        $('#addContentFeaturekeywo').modal('hide');
                        $("#errMsgSuccessFeaturek").text("");
                    } else {
                        $("#errMsgFailedFeaturek").text("");
                        $("#errMsgSuccessFeaturek").text("");
                        $("#errMsgFailedFeaturek").text(errMsg);
                    }
                }
            });

            return false;
        });

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>