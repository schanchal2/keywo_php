<?php
session_start();
include "../layout/header.php";

require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "{$docrootpath}model/acl/sidebarAccess.php";
$groupsAdmin=explode(" ",getAdminGroup($connAdmin)["errMsg"]["group_id"]);
if (in_array("SM", $groupsAdmin)) {
    $group = "SM";
}
if (in_array("MM", $groupsAdmin)) {
    $group = "MM";
}

if (in_array("cshod", $groupsAdmin)) {
    $group = "cshod";
}
if (in_array("TrackingMonitoring", $groupsAdmin)) {
    $group = "TrackingMonitoring";
}

if (in_array("SuperAdmin", $groupsAdmin)) {
    $group = "SM";
}

if ($group == "") {
    $group = "other";
}
?>
<style>
    table tr th {
        border: #35ccf1 solid 1px !important;
    }

    table tr td {
        border: #35ccf1 solid 1px !important;
    }
</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Dashboard</h1><span>General Information</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
       

        <div id="loadMydashboard">

        </div>

    </div>
</main>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

<script type="text/javascript">
    $( document ).ready(function() {
var group='<?php echo $group; ?>';
        switch(group)
        {
            case "MM":
                $("#loadMydashboard").html(showLoadingDiv()).load("mm.php");
                break;

            case "SM":
                $("#loadMydashboard").html(showLoadingDiv()).load("sm.php");
                break;

            case "TrackingMonitoring":
                $("#loadMydashboard").html(showLoadingDiv()).load("tm.php");
                break;

            case "cshod":
                $("#loadMydashboard").html(showLoadingDiv()).load("csdashboard.php");
                break;

            case "other":
                $("#loadMydashboard").html(showLoadingDiv()).load("other.php");
                break;
        }
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
