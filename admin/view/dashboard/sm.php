<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("../../model/acl/SessionCheck.php");  
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');  
require_once('../../helpers/arrayHelper.php'); 
require_once('../../helpers/stringHelper.php');  
require_once('../../helpers/date_helpers.php');
require_once('../../model/dashboard/dashboardModel.php');
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
    $getKeywordsStats = getKeywordsStats($connKeywords);
        if(noError($getKeywordsStats))
        {            
            $getKeywoStatsData     = $getKeywordsStats["data"][0]; 
            $getTotalKeywordTraded = $getKeywoStatsData["total_kwd_traded"];
            $getTotalTradedAmount  = $getKeywoStatsData["total_keyword_traded_amount"]; 
            $getTradedAmount       = number_format($getTotalTradedAmount,2);
            $getTotalTradedFees    = $getKeywoStatsData["total_keyword_traded_fees"]; 
            $getTradedFees         = number_format($getTotalTradedFees,2);
        }
        $totalAmount  = $getTotalTradedAmount + $getTotalTradedFees;
        $totalAmounts = number_format($totalAmount,2);
        $getallBidStatus = getallBidStatus($connKeywords); 
        if(noError($getallBidStatus))
        {            
            $getallBidStatusData   = $getallBidStatus["data"][0]; 
            $getKeywordBidCount    = $getallBidStatusData["count"];  
            $getKeywordBidValue    = $getallBidStatusData["bidPrice"];
        }
         $getKeywordBidPrice = number_format($getKeywordBidValue,2);
         $getallAskStatus    = getallAskStatus($connKeywords);
        if(noError($getallAskStatus))
        {            
            $getallAskStatusData   = $getallAskStatus["data"][0]; 
            $getKeywordAskCount    = $getallAskStatusData["count"];  
            $getKeywordAskValue    = $getallAskStatusData["askPrice"];
        }
         $getKeywordAskPrice = number_format($getKeywordAskValue,2);
?>
    <div role="tabpanel" class="m-t-15">
        <ul class="nav nav-pills nav-justified nav-pills--1 m-b-20" role="tablist">
            <li role="presentation" class="active">
                <a href="#userstatistics" role="tab" data-toggle="tab" aria-expanded="true"> User Statistics </a>
            </li>
            <li role="presentation" class="">
                <a href="#wallet" role="tab" data-toggle="tab" aria-expanded="false"> Wallet </a>
            </li>
            <li role="presentation" class="">
                <a href="#keywordstatistics" role="tab" data-toggle="tab" aria-expanded="false"> Keyword Statistics </a>
            </li>
            <li role="presentation" class="">
                <a href="#socialstatistics" role="tab" data-toggle="tab" aria-expanded="false"> Social Statistics </a>
            </li>
            <li role="presentation" class="">
                <a href="#referralstatistics" role="tab" data-toggle="tab" aria-expanded="false"> Referral Statistics </a>
            </li>
        </ul>
        <div class="tab-content p-t-10">
          
            <div role="tabpanel" class="tab-pane active" id="userstatistics">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">User Statistics</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="table--lightBlue2 m-b-0">
                    <div class="row ">
                        <div class="col-xs-12">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th>Dr.</th>
                                        <th>Total Transactions</th>
                                        <th>Amount</th>
                                        <th>Credit</th>
                                        <th>Credit</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5">Approved Cashouts</td>
                                        <td class="p-5">458</td>
                                        <td class="p-5 skyeblue">0.00000000 BTC</td>
                                        <td class="p-5">
                                            <?= $adminCurrency; ?> Purchase(Deposite)</td>
                                        <td class="p-5">785</td>
                                        <td class=" p-5 skyeblue">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Send Funds</td>
                                        <td class="p-5">258</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                        <td class="p-5">Received Funds</td>
                                        <td class="p-5">289</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Keyword Purchase</td>
                                        <td class="p-5">460</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                        <td class="p-5"> keyword Trading(Sales)</td>
                                        <td class="p-5">705</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Transaction Fees</td>
                                        <td class="p-5">789</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                        <td class="p-5">Total Keyword Income</td>
                                        <td class="p-5">10</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Blocked for Bids</td>
                                        <td class="p-5">251</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                        <td class="p-5">Total Keyword Income</td>
                                        <td class="p-5">4589</td>
                                        <td class="skyeblue">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Pending Cashouts</td>
                                        <td class="p-5">150</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                        <td class="text-darkblue p-5"><b>Referral Payout-</b></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5">Keyword Affiliate Earning</td>
                                        <td class="p-5">5697</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5">Social Affiliate Earning</td>
                                        <td class="p-5">5689</td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="text-blue p-5"><b>Social Payout-</b></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5">Post Earning</td>
                                        <td class="p-5"></td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                        <td class="p-5">Post Share Earnings</td>
                                        <td class="p-5"></td>
                                        <td class="skyeblue p-5">0.00000000 BTC</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--UserList Closed-->
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="wallet">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Wallet</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--lightBlue2">
                            <table class="table table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="8" class="p-5">KEYWO WALLET BALANCE</th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" class="color-light-blue text-blue p-5 text-center">Income</th>
                                        <th colspan="3" class="color-light-blue text-blue p-5 text-center">Expence</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue2 text-black p-5 text-left">Particulars</th>
                                        <th class="color-light-blue2 text-black p-5 text-left">Amount</th>
                                        <th class="color-light-blue2 text-black p-5 text-left ">Particulars</th>
                                        <th class="color-light-blue2 text-black p-5 text-left">Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5">Search Earnings</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5">Keyword Refferal Payout(50%)</td>
                                        <td class="p-5">xxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">App Earnings</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Keyword Earnings</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Keyword Sale@50%</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Social Earning</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5"></td>
                                        <td class="p-5"></td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Total</td>
                                        <td class="p-5">xxxxx</td>
                                        <td class="p-5">Total</td>
                                        <td class="p-5">xxxxx</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="p-5">Send & Received</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Sent Txns(Nos.)</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Sent Amt</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Recd Txns(Nos.)</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Recd Amt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">&nbsp;</td>
                                        <td class="p-5 text-left">&nbsp;</td>
                                        <td class="p-5 text-left">&nbsp;</td>
                                        <td class="p-5 text-left">&nbsp;</td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Renewal Fees Collected</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="table--lightBlue2 m-t-0">
                    <div class="row">
                        <div class="col-xs-6">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="font-normal f-sz16 p-5">Pool Income</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-blue font-normal p-5">Particulars</th>
                                        <th class="color-light-blue text-blue font-normal p-5">Total Txns(Nos)</th>
                                        <th class="color-light-blue text-blue font-normal p-5">Total Amt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5">Ad-Revenue</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Keywords Sale</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Transaction Fees</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 font-normal">Renewal Fees</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Unclaimed Keyword Income</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="color-light-blue p-5">Total</td>
                                        <td class="color-light-blue p-5"></td>
                                        <td class="color-light-blue p-5">xxxxxx</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-xs-6">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="font-normal f-sz16 p-5">Pool Expense</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-blue font-normal p-5">Particulars</th>
                                        <th class="color-light-blue text-blue font-normal p-5">Total Txns(Nos)</th>
                                        <th class="color-light-blue text-blue font-normal p-5">Total Amt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5">Search Payout</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Server Maintenance</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Social Payout</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">Keyword Referral Payout</td>
                                        <td class="p-5"></td>
                                        <td class="p-5">xxxxxx</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5">&nbsp;</td>
                                        <td class="p-5">&nbsp;</td>
                                        <td class="p-5">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="color-light-blue p-5">Total</td>
                                        <td class="color-light-blue p-5"></td>
                                        <td class="color-light-blue p-5">xxxxx</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="keywordstatistics">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Keyword Trade Statistics</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="p-5">Keywords Traded</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left"></th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Traded (Amt.)</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Trading Fees</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">
                                            <?= isset($getTotalKeywordTraded)?($getTotalKeywordTraded):0 ?>
                                        </td>
                                        <td class="p-5 text-left">
                                            <?= isset($getTradedAmount)?($getTradedAmount):0 ?>
                                        </td>
                                        <td class="p-5 text-left">
                                            <?= isset($getTradedFees)?($getTradedFees):0 ?>
                                        </td>
                                        <td class="p-5 text-left">
                                            <?= isset($totalAmounts)?($totalAmounts):0 ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="p-5">Bid</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Total No. of Bid Placed</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Bid</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">
                                            <?= isset($getKeywordBidCount)?($getKeywordBidCount):0 ?>
                                        </td>
                                        <td class="p-5 text-left">
                                            <?= isset($getKeywordBidPrice)?($getKeywordBidPrice):0 ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="p-5">Ask Price</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Keywords Sold</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Set 'Ask' price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">
                                            <?= isset($getKeywordAskCount)?($getKeywordAskCount):0 ?>
                                        </td>
                                        <td class="p-5 text-left">
                                            <?= isset($getKeywordAskPrice)?($getKeywordAskPrice):0 ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                </div>
                <div class="row m-t-5">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Keyword Statistics</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th class="p-5 text-left">Total keywords Subscribed(Nos.)</th>
                                        <th class="p-5 text-left">3000(Nos)</th>
                                        <th class="p-5 text-left">458 IT$(Amt)</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left"></th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Paypal</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Bitcoin(Amt)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">Txn.Nos.</td>
                                        <td class="p-5 text-left">200</td>
                                        <td class="p-5 text-left">100</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 text-left">1000</td>
                                        <td class="p-5 text-left">400$</td>
                                        <td class="p-5 text-left">25$</td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                    <div class="col-xs-6">
                        <table class="table text-center table-bordered table-responsive">
                            <div class="table--lightBlue2">
                                <thead>
                                    <tr>
                                        <th colspan="2" class="p-5">Keyword Renewal</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Keywords Renewed(Nos.)</th>
                                        <th class="color-light-blue text-black font-normal p-5 text-left">Renewal Fees</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">200</td>
                                        <td class="p-5 text-left">200</td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="socialstatistics">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Social Statistics</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--lightBlue2">
                            <table class="table text-center table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th class="p-5">Post Type</th>
                                        <th class="p-5">Post (Nos.)</th>
                                        <th class="p-5">Blocked Post(Nos.)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">Audio</td>
                                        <td class="p-5 text-left">10</td>
                                        <td class="p-5 text-left">89</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 text-left">video</td>
                                        <td class="p-5 text-left">10</td>
                                        <td class="p-5 text-left">76</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 text-left">Blog</td>
                                        <td class="p-5 text-left">10</td>
                                        <td class="p-5 text-left">987</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 text-left">Image</td>
                                        <td class="p-5 text-left">10</td>
                                        <td class="p-5 text-left">87</td>
                                    </tr>
                                    <tr>
                                        <td class="p-5 text-left">All</td>
                                        <td class="p-5 text-left">10</td>
                                        <td class="p-5 text-left">2222</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="referralstatistics">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table--blue">
                            <table class="table text-center table-responsive">
                                <thead>
                                    <th class="f-sz16">Referral Statistics</th>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="table text-center table-bordered table-responsive">
                            <div id="userlist">
                                <thead>
                                    <tr>
                                        <th colspan="4" class="font-normal p-5">Referral</th>
                                    </tr>
                                    <tr>
                                        <th class="color-light-blue2 text-black font-normal text-left p-5">Social Affiliate(Nos)</th>
                                        <th class="color-light-blue2 text-black font-normal text-left p-5">Social Affiliate(Amt)</th>
                                        <th class="color-light-blue2 text-black font-normal text-left p-5">Keyword Affiliate(Nos)</th>
                                        <th class="color-light-blue2 text-black font-normal text-left p-5">Keyword Affiliate(Amt)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="p-5 text-left">400</td>
                                        <td class="p-5 text-left">0.45</td>
                                        <td class="p-5 text-left">403</td>
                                        <td class="p-5 text-left">0.578</td>
                                    </tr>
                                </tbody>
                            </div>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
