<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("../../model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../model/dashboard/dashboardModel.php');

$requestorIP=getClientIP();
$data=getLocationUserFromIP($requestorIP);

?>


<div class="row">
    <div class="col-xs-12">
        <table class="table text-center table-bordered table-responsive">
            <div class="table--lightBlue2">
                <thead >
                <tr>
                    <th style="width:25%" class="color-light-blue text-black font-normal p-6 f-sz13 text-center">Name</th>
                    <th style="width:25%" class="color-light-blue text-black font-normal p-6 f-sz13 text-center">Current Time</th>
                    <th style="width:25%" class="color-light-blue text-black font-normal p-6 f-sz13 text-center">Country</th>
                    <th style="width:25%" class="color-light-blue text-black font-normal p-6 f-sz13 text-center">IP</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="p-6 f-sz13 text-center"><?php echo $_SESSION["admin_name"]; ?></td>
                    <td class="p-6 f-sz13 text-center" id="mytime">sd</td>
                    <td class="p-6 f-sz13 text-center"><?php echo $data["country"]; ?></td>
                    <td class="p-6 f-sz13 text-center"><?php echo $data["userIP"]; ?></td>
                </tr>
                </tbody>
            </div>
        </table>
    </div>
</div>

<script>

    $( document ).ready(function() {
        startTime();
    });

    function startTime() {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);


        var mid = (h >= 12) ? "PM" : "AM";

        document.getElementById('mytime').innerHTML =
            h + ":" + m + ":" + s + " " + mid;
        var t = setTimeout(startTime, 500);
    }
    function checkTime(i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    }
</script>