<div id="filteredTab" class="nav-tabs-custom">
    <div class="btn-group btn-group-justified" role="group" aria-label=".....">
        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active bdrR"><a class="bg-darkblue text-white" href="#Service Request" aria-controls="Service Request" role="tab" data-toggle="tab">Service Request</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#Cashouts" aria-controls="Cashouts" role="tab" data-toggle="tab">Cashouts</a></li>
            <li role="presentaion" class="bdrR"><a class="bg-darkblue text-white" href="#Internal Ticket" aria-controls="Internal Ticket" role="tab" data-toggle="tab">Internal Ticket</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#KYC" aria-controls="KYC" role="tab" data-toggle="tab">KYC</a></li>
        </ul>

    </div>
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="ServiceRequest">
            <div id="#"></div>
        </div>

        <div role="tabpanel"  class="tab-pane" id="Cashouts">
            <div id="#"></div>
        </div>

        <div role="tabpanel" class="tab-pane" id="InternalTicket">
            <div id="#"></div>
        </div>

        <div role="tabpanel" class="tab-pane" id="KYC">
            <div id="#"></div>
        </div>
    </div><!--tab content closed-->
</div><!--filteredTab Closed -->


<div class="row">
    <div class="col-xs-12">
        <div id="userList" style="margin-bottom:-15px";>
            <table class="table text-center table-responsive" style="margin-bottom:-20px";>
                <thead>
                <tr>
                    <th>Service Request</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList Closed-->

        <div id="userRefferalAnalatics">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>UAM Agents</th>
                    <th>SR Received</th>
                    <th>Hold/Pending</th>
                    <th>Approved</th>
                    <th>Rejected</th>
                    <th><i class="fa fa-calendar" id="ServiceRequestDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="UAMAgents">Tom</td>
                    <td id="SRReceived">8</td>
                    <td id="Hold/Pending">3</td>
                    <td id="Approved">4</td>
                    <td id="Rejected">1</td>
                    <td id="ServiceRequestDateRange"></td>
                </tr>
                <tr>
                    <td id="UAMAgents">Jerry</td>
                    <td id="SRReceived">5</td>
                    <td id="Hold/Pending">2</td>
                    <td id="Approved">2</td>
                    <td id="Rejected">1</td>
                    <td id="ServiceRequestDateRange"></td>
                </tr>
                <tr>
                    <td id="UAMAgents">Total</td>
                    <td id="SRReceived">13</td>
                    <td id="Hold/Pending">5</td>
                    <td id="Approved">6</td>
                    <td id="Rejected">2</td>
                    <td id="ServiceRequestDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="userList" style="margin-bottom:-15px";>
            <table class="table text-center table-responsive" style="margin-bottom:-20px";>
                <thead>
                <tr>
                    <th>Cashouts</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList Closed-->

        <div id="userRefferalAnalatics">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>UAM Agents</th>
                    <th>Manual Cashout Recd.</th>
                    <th>Hold</th>
                    <th>Approved</th>
                    <th>Rejected</th>
                    <th><i class="fa fa-calendar" id="CashoutsDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="UAMAgents">Tom</td>
                    <td id="ManualCashoutRecd">8</td>
                    <td id="Hold">3</td>
                    <td id="Approved">4</td>
                    <td id="Rejected">1</td>
                    <td id="CashoutsDateRange"></td>
                </tr>

                <tr>
                    <td id="UAMAgents">Jerry</td>
                    <td id="ManualCashoutRecd">5</td>
                    <td id="Hold">2</td>
                    <td id="Approved">2</td>
                    <td id="Rejected">1</td>
                    <td id="ServiceRequestDateRange"></td>
                </tr>
                <tr>
                    <td id="UAMAgents">Total</td>
                    <td id="ManualCashoutRecd">13</td>
                    <td id="Hold">5</td>
                    <td id="Approved">6</td>
                    <td id="Rejected">2</td>
                    <td id="ServiceRequestDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="userList" style="margin-bottom:-15px";>
            <table class="table text-center table-responsive" style="margin-bottom:-20px";>
                <thead>
                <tr>
                    <th>Internal Ticket</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList Closed-->

        <div id="userRefferalAnalatics">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>UAM Agents</th>
                    <th>IT Ticket Received</th>
                    <th>IT Ticket Pending</th>
                    <th>Rejected</th>
                    <th><i class="fa fa-calendar" id="InternalTicketDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="UAMAgents">Tom</td>
                    <td id="ITTicketReceived">8</td>
                    <td id="ITTicketPending">3</td>
                    <td id="Rejected">1</td>
                    <td id="InternalTicketDateRange"></td>
                </tr>

                <tr>
                    <td id="UAMAgents">Jerry</td>
                    <td id="ITTicketReceived">5</td>
                    <td id="ITTicketPending">3</td>
                    <td id="Rejected">1</td>
                    <td id="InternalTicketDateRange"></td>
                </tr>
                <tr>
                    <td id="UAMAgents">Total</td>
                    <td id="ITTicketReceived">13</td>
                    <td id="ITTicketPending">6</td>
                    <td id="Rejected">2</td>
                    <td id="InternalTicketDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div id="userList" style="margin-bottom:-15px";>
            <table class="table text-center table-responsive" style="margin-bottom:-20px";>
                <thead>
                <tr>
                    <th>KYC Doc. Acceptance</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList Closed-->

        <div id="userRefferalAnalatics">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>UAM Agents</th>
                    <th>Total Recd.</th>
                    <th>Total Approved</th>
                    <th>Total Rejected</th>
                    <th><i class="fa fa-calendar" id="KYCDoc.AcceptanceDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="UAMAgents">Tom</td>
                    <td id="TotalRecd.">8</td>
                    <td id="TotalApproved">3</td>
                    <td id="TotalRejected">1</td>
                    <td id="KYCDoc.AcceptanceDateRange"></td>
                </tr>

                <tr>
                    <td id="UAMAgents">Jerry</td>
                    <td id="TotalRecd.">5</td>
                    <td id="TotalApproved">2</td>
                    <td id="TotalRejected">1</td>
                    <td id="KYCDoc.AcceptanceDateRange"></td>
                </tr>
                <tr>
                    <td id="UAMAgents">Total</td>
                    <td id="TotalRecd.">13</td>
                    <td id="TotalApproved">5</td>
                    <td id="TotalRejected">2</td>
                    <td id="KYCDoc.AcceptanceDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>