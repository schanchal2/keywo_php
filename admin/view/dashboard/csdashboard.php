<div id="filteredTabs" class="nav-tabs-custom">
    <div class="btn-group btn-group-justified " role="group" aria-label="...">
        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active bdrR"><a class="bg-darkblue text-white" href="#AgentBucket" aria-controls="Agent Bucket" role="tab" data-toggle="tab">Agent Bucket</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#EscalationHODTickets" aria-controls="EscalationHODTickets" role="tab" data-toggle="tab">Escalation[HOD Tickets]</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#PendingBucketHOD" aria-controls="PendingBucketHOD" role="tab" data-toggle="tab">Pending Bucket-HOD</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#ClosedHOD" aria-controls="ClosedHOD" role="tab" data-toggle="tab">Closed(HOD)</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="AgentBucket">
            <div id="newTopSearchUsersFilter"></div>
        </div>


        <div role="tabpanel" class="tab-pane" id="EscalationHODTickets">
            <div id="newTopKeywordUsersFilter"></div>
        </div>


        <div role="tabpanel" class="tab-pane" id="PendingBucketHOD">
            <div id="newTopAppUsersFilter"></div>
        </div>


        <div role="tabpanel" class="tab-pane" id="ClosedHOD">
            <div id="newTopSearchAffUsersFilter"></div>
        </div>
    </div>
</div><!-- Closes filteredTab id-->


<div class="row">
    <div class="col-xs-12">
        <div class="table--lightBlue2 p-t-20">
            <table class="table text-center table-responsive m-b-10 ">
                <thead>
                <tr>
                    <th>Support Queries</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList closed-->

        <div class="table--blue">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th class="text-center text-white">New</th>
                    <th class="text-center text-white">Open</th>
                    <th class="text-center text-white">Pending</th>
                    <th class="text-center text-white">Escalated</th>
                    <th class="text-center text-white">Closed</th>
                    <th class="text-center text-white">Total Queries</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="new">45</td>
                    <td id="open">35</td>
                    <td id="pending">20</td>
                    <td id="escalated">3</td>
                    <td id="closed">125</td>
                    <td id="totalQuery">288</td>
                </tr>
                </tbody>
            </table>
        </div><!--userRefferalAnalatics-->
    </div>
</div><!-- row closed-->


<div class="row">
    <div class="col-xs-12">
        <div class="table--lightBlue2">
            <table class="table text-center table-responsive m-b-10 ">
                <thead>
                <tr>
                    <th>Query Ticket</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList closed-->

        <div class="table--blue">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th class="text-center text-white">Agent Name</th>
                    <th class="text-center text-white">Total Ticket Assigned</th>
                    <th class="text-center text-white">Resolved/Closed</th>
                    <th class="text-center text-white">Pending</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="agentName">Tom</td>
                    <td id="totalTicket">8</td>
                    <td id="resolved">3</td>
                    <td id="pending">45</td>
                </tr>

                <tr>
                    <td id="agentName">Jerry</td>
                    <td id="totalTicket">5</td>
                    <td id="resolved">2</td>
                    <td id="pending">55</td>
                </tr>

                <tr>
                    <td id="agentName">Total</td>
                    <td id="totalTicket">13</td>
                    <td id="resolved">5</td>
                    <td id="pending">100</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="table--lightBlue2">
            <table class="table text-center table-responsive m-b-10 ">
                <thead>
                <tr>
                    <th>Service Request</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList closed-->

        <div class="table--blue">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th class="text-center text-white">Agent Name</th>
                    <th class="text-center text-white">SR Raised(on Tech Team)</th>
                    <th class="text-center text-white">SR Raised(on UAM Team)</th>
                    <th class="text-center text-white">Total SR Raised</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="agentName">Tom</td>
                    <td id="raisedTech">8</td>
                    <td id="raisedUam">3</td>
                    <td id="totalSr">5</td>

                </tr>
                <tr>
                    <td id="agentName">Jerry</td>
                    <td id="raisedTech">5</td>
                    <td id="raisedUam">2</td>
                    <td id="totalSr">2</td>

                </tr>
                <tr>
                    <td id="agentName">Total</td>
                    <td id="raisedTech">13</td>
                    <td id="raisedUam">5</td>
                    <td id="totalSr">7</td>

                </tr>
                </tbody>
            </table>
        </div><!--userRefferalAnalatics-->
    </div>
</div><!-- row closed-->



<div class="row">
    <div class="col-xs-12">
        <div class="table--lightBlue2">
            <table class="table text-center table-responsive m-b-10 ">
                <thead>
                <tr>
                    <th>Internal Ticket</th>
                </tr>
                </thead>
            </table>
        </div><!--UserList closed-->
        <div class="table--blue">
            <table class="table  text-center table-responsive m-0">
                <thead>
                <tr>

                    
                    <th class="text-center text-white">Agent Name</th>
                    <th class="text-center text-white">IT Ticket Received</th>
                    <th class="text-center text-white">IT Ticket Pending</th>
                    <th class="text-center text-white">IT Ticket Closed</th>


                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="agentName">Tom</td>
                    <td id="received">2</td>
                    <td id="pending">2</td>
                    <td id="closed">2</td>
                </tr>
                <tr>
                    <td id="agentName">Jerry</td>
                    <td id="received">0</td>
                    <td id="pending">0</td>
                    <td id="closed">0</td>
                </tr>
                <tr>
                    <td id="agentName">Total</td>
                    <td id="received">0</td>
                    <td id="pending">0</td>
                    <td id="closed">0</td>
                </tr>
                </tbody>
            </table>
        </div><!--userRefferalAnalatics-->
    </div>
</div><!-- row closed-->