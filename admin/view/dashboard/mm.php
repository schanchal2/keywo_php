<div class="row" style="background-color:#A9A9A9; margin-top:-20px; margin-right:4px;text-align: center;" >
    <span style="color:white; font-size:15pt;"><strong>Service Request</strong></span>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="userList">
            <table class="table text-center tabel-responsive" style="margin-top:20px;">
                <thead>
                <tr>
                    <th>Open</th>
                    <th>Closed</th>
                    <th>Total Received</th>
                    <th id="MMDateRange"><i class="fa fa-calendar"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="open">15</td>
                    <td id="close">15</td>
                    <td id="Total Received">20</td>
                    <td></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="userList">
    <div class="row">
        <div class="col-lg-12">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>Department(Recd.From)</th>
                    <th>Total Received</th>
                    <th>Open</th>
                    <th>Hold</th>
                    <th>Approved</th>
                    <th>Rejected</th>
                    <th><i class="fa fa-calendar" id="MMDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="DepartmentRecdFrom">User</td>
                    <td id="TotalReceived"> 15</td>
                    <td id="Open"> 15</td>
                    <td id="Hold"> 15</td>
                    <td id="Approved"> 15</td>
                    <td id="Rejected"> 15</td>
                    <td id="MMDateRange"></td>
                </tr>
                <tr>
                    <td id="Department(Recd.From)">CS</td>
                    <td id="TotalReceived"> 10</td>
                    <td id="Open"> 10</td>
                    <td id="Hold"> 10</td>
                    <td id="Approved"> 10</td>
                    <td id="Rejected"> 10</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td id="DepartmentRecdFrom">UAM</td>
                    <td id="TotalReceived"> 10</td>
                    <td id="Open"> 10</td>
                    <td id="Hold"> 10</td>
                    <td id="Approved"> 10</td>
                    <td id="Rejected"> 10</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td id="DepartmentRecdFrom">Tracking & Monitoring</td>
                    <td id="TotalReceived"> 10</td>
                    <td id="Open"> 10</td>
                    <td id="Hold"> 10</td>
                    <td id="Approved"> 10</td>
                    <td id="Rejected"> 10</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td id="DepartmentRecdFrom">Tech Team</td>
                    <td id="TotalReceived"> 15</td>
                    <td id="Open"> 15</td>
                    <td id="Hold"> 15</td>
                    <td id="Approved"> 15</td>
                    <td id="Rejected"> 15</td>
                    <td id="id="MMDateRange""></td>
                </tr>

                <tr>
                    <td id="DepartmentRecdFrom">Accounts</td>
                    <td id="TotalReceived"> 10</td>
                    <td id="Open"> 10</td>
                    <td id="Hold"> 10</td>
                    <td id="Approved"> 10</td>
                    <td id="Rejected"> 10</td>
                    <td id="id="MMDateRange""></td>
                </tr>

                <tr>
                    <td id="DepartmentRecdFrom"> Total</td>
                    <td id="TotalReceived"> 70</td>
                    <td id="Open"> 70</td>
                    <td id="Hold"> 70</td>
                    <td id="Approved"> 70</td>
                    <td id="Rejected"> 70</td>
                    <td id="MMDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="userList">
            <table class="table text-center tabel-responsive" style="margin-top:20px;">
                <thead>
                <tr>
                    <th>MM Name</th>
                    <th>Total Assigned</th>
                    <th>Actioned</th>
                    <th>Hold</th>
                    <th>Un-Actioned</th>
                    <th><i class="fa fa-calendar" id="MMDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="MMName">Tom</td>
                    <td id="TotalAssigned">75</td>
                    <td id="Actioned">15</td>
                    <td id="Hold">15</td>
                    <td id="UnActioned">30</td>
                    <td id="MMDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row" style="background-color:#A9A9A9; margin-top:20px; margin-right:4px;text-align: center;" >
    <span style="color:white; font-size:15pt;"><strong>Cashouts</strong></span>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="userList">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th rowspan="2">MMName</th>
                    <th rowspan="2">Cashouts Mode</th>
                    <th class="bg-darkblue" colspan="2">Approved Cashouts(By MM) </th>
                    <th class="bg-darkblue" colspan="2">Pending Cashouts(With MM) </th>
                    <th class="bg-darkblue" colspan="2">Rejected Cashouts(By MM )</th>
                    <th rowspan="2"><i class="fa fa-calendar" id="MMDateRange" style="padding:20px;"></i></th>
                </tr>
                <tr>
                    <th>Txn nos</th>
                    <th>Txn Amt</th>
                    <th>Txn nos</th>
                    <th>Txn Amt</th>
                    <th>Txn nos</th>
                    <th>Txn Amt</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="MMName">Tom</td>
                    <td id="CashoutsMode">Auto</td>
                    <td id="Txn nos">125</td>
                    <td id="TxnAmt">25 BTC</td>
                    <td id="Txn nos">50</td>
                    <td id="TxnAmt">4 BTC</td>
                    <td id="Txn nos">1</td>
                    <td id="TxnAmt">10 BTC</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td id="MMName">Tom</td>
                    <td id="CashoutsMode">Auto</td>
                    <td id="Txn nos">25</td>
                    <td id="TxnAmt">10 BTC</td>
                    <td id="Txn nos">15</td>
                    <td id="TxnAmt">4 BTC</td>
                    <td id="Txn nos">1</td>
                    <td id="TxnAmt">10 BTC</td>
                    <td id="MMDateRange"></td>

                </tr>

                <tr>
                    <td id="MMName">Tom</td>
                    <td id="CashoutsMode">Auto</td>
                    <td id="Txn nos">150</td>
                    <td id="TxnAmt">35 BTC</td>
                    <td id="Txn nos">65</td>
                    <td id="TxnAmt">4 BTC</td>
                    <td id="Txn nos">1</td>
                    <td id="TxnAmt">10 BTC</td>
                    <td id="MMDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row" style="background-color:#A9A9A9; margin-top:20px; margin-right:4px;text-align: center;" >
    <span style="color:white; font-size:15pt;"><strong>Internal Tickets</strong></span>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="userList">
            <table class="table text-center tabel-responsive" style="margin-top:20px;">
                <thead>
                <tr>
                    <th>Total Ticket Raised on(MM)</th>
                    <th>Total Action By(MM)</th>
                    <th>Total Un-Actioned(MM)</th>
                    <th><i class="fa fa-calendar"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="TotalTicketRaisedonMM">15</td>
                    <td id="TotalActionByMM">15</td>
                    <td id="TotalUnActioned">20</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td id="TotalTicketRaisedonMM">15</td>
                    <td id="TotalActionByMM">15</td>
                    <td id="TotalUnActioned">20</td>
                    <td id="MMDateRange"></td>
                </tr>
                <tr>
                    <td id="TotalTicketRaisedonMM">15</td>
                    <td id="TotalActionByMM">15</td>
                    <td id="TotalUnActioned">20</td>
                    <td id="MMDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div id="userList">
            <table class="table text-center tabel-responsive" style="margin-top:20px;">
                <thead>
                <tr>
                    <th>MM Name</th>
                    <th>Total IT Ticket Assigned</th>
                    <th>Total Actioned</th>
                    <th>Ticket Re-Opened(Nos.)</th>
                    <th>Total Un-Actioned</th>
                    <th><i class="fa fa-calendar" id="MMDateRange"></i></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Tom</td>
                    <td id="TotalITTicketAssigned">15</td>
                    <td id="Total Actioned">15</td>
                    <td id="Ticket Re-OpenedNos.">20</td>
                    <td id="">10</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td>Jerry</td>
                    <td id="TotalITTicketAssigned">15</td>
                    <td id="Total Actioned">15</td>
                    <td id="Ticket Re-OpenedNos.">20</td>
                    <td id="">10</td>
                    <td id="MMDateRange"></td>
                </tr>

                <tr>
                    <td>Total</td>
                    <td id="TotalITTicketAssigned">15</td>
                    <td id="Total Actioned">15</td>
                    <td id="Ticket Re-OpenedNos.">20</td>
                    <td id="">10</td>
                    <td id="MMDateRange"></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>