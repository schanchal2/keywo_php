<?php

include "../../config/config.php";
require_once("../../model/acl/SessionCheck.php");
require_once("../../config/db_config.php");
//start helper
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/date_helpers.php");
require_once("../../helpers/cryptoHelper.php");
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/stringHelper.php");
//end helper

//other
require_once("../../core/errorMap.php");
require_once("../../model/tracking/tracklist/tracklist.php");


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$result = getAllAgentTracklistAnalytics($connAdmin);
$result = $result["errMsg"];

?>

<div class="row">
    <div class="col-xs-12">
        <div id="userList">
            <table class="table text-center tabel-responsive">
                <thead>
                <tr>
                    <th class="bg-blue">Agents</th>
                    <th class="bg-blue">Added to track list</th>

                </tr>
                </thead>
                <tbody>
                <?php
                if (count($result) > 0) {
                    foreach ($result as $data) {
                        ?>
                        <tr>
                            <td class="f-sz13"><?= $data["agent_name"]; ?></td>
                            <td class="f-sz13"><?= $data["teacklist_added"]; ?></td>

                        </tr>
                        <?php
                    }
                } else {
                    ?>
                    <tr>
                        <td class="f-sz13" colspan="2">No Data Found</td>


                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-3">
        <table  class="table text-center tabel-responsive border-all box-shadow dashboard-table">
            <thead>
            <th>General</th>
            </thead>
            <tbody class="text-left">
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/general/general.php?target=DOMIANEmail'; ?>" target="_blank" class="f-sz13">Domain(Email)</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/general/general.php?target=SIGNUP'; ?>" target="_blank" class="f-sz13">Signup</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/general/general.php?target=BLOCKEDUSER'; ?>" target="_blank" class="f-sz13">Blocked user list</a></td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="col-xs-3">
        <table  class="table text-center tabel-responsive border-all box-shadow dashboard-table" id="">
            <thead>
            <th>Userwise</th>
            </thead>
            <tbody class="text-left">
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=topSocialEarners'; ?>" target="_blank" class="f-sz13">Top Social Earners</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=highestItdPurchase'; ?>" target="_blank" class="f-sz13">Highest <?php echo $adminCurrency; ?> Purchased</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=highestTotalItdPurchaseAmt'; ?>" target="_blank" class="f-sz13">Highest <?php  echo $adminCurrency; ?> Purchased Amt</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=highestCashout'; ?>" target="_blank" class="f-sz13">Highest Cashouts</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=highestTotalCashoutAmt'; ?>" target="_blank" class="f-sz13">Highest Cashouts Amt</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=BlockedForBids'; ?>" target="_blank" class="f-sz13">Blocked for Bids</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=TopReferrer'; ?>" target="_blank" class="f-sz13">Top Referrer</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=TopSent'; ?>" target="_blank" class="f-sz13">Top Sent</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=TopTotalSent'; ?>" target="_blank" class="f-sz13">Top Total Sent</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=TopReceived'; ?>" target="_blank" class="f-sz13">Top Received</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=TopTotalReceived'; ?>" target="_blank" class="f-sz13">Top Total Received</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=CashoutFees'; ?>" target="_blank" class="f-sz13">Cashout Fees</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/userwise/userwise.php?target=HighestWalletBalance'; ?>" target="_blank" class="f-sz13">Highest Wallet Balance</a></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="col-xs-3">
        <table  class="table text-center tabel-responsive border-all box-shadow dashboard-table" id="">
            <thead>
            <th>Keywordwise</th>
            </thead>
            <tbody class="text-left">
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordEarnings'; ?>" target="_blank" class="f-sz13">Keyword Earnings</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordInteractionSearch'; ?>" target="_blank" class="f-sz13">Keyword Interactions</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordAverageEarnings'; ?>" target="_blank" class="f-sz13">Keyword Avg. Earnings</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordsDueForRenewal'; ?>" target="_blank" class="f-sz13">Due for Renewal</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=Bids'; ?>" target="_blank" class="f-sz13">Keyword Bid</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordTrade'; ?>" target="_blank" class="f-sz13">Keyword Trade</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=KeywordSubscribed'; ?>" target="_blank" class="f-sz13">Keyword Subscribed</a></td>
            </tr>

            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=TradingFees'; ?>" target="_blank" class="f-sz13">Trading Fees</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/keywordwise/keywordwise.php?target=RenewalFees'; ?>" target="_blank" class="f-sz13">Renewal Fees</a></td>
            </tr>

            </tbody>
        </table>
    </div>
    <div class="col-xs-3">
        <table  class="table text-center tabel-responsive border-all box-shadow dashboard-table" id="">
            <thead>
            <th>Postwise</th>
            </thead>
            <tbody class="text-left">
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/postwise/postwise.php?target=TopPostEarnings'; ?>" target="_blank" class="f-sz13">Top Post Earnings</a></td>
            </tr>
            <tr>
                <td><a href="<?php echo $adminRoot.'view/tracking/postwise/postwise.php?target=TopPostInteractions'; ?>" target="_blank" class="f-sz13">Top Post Interactions</a></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>