<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">


<<a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
<div class="modal madal--1 fade" id="ContentModal">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title"  style="color:white;font-weight: bold">Content</h4>
                </div>
            <div class="modal-body">
                
            </div>
            
        </div>
    </div>
</div>

    <main>
        <div class="container-fluid">
            <div class="title clearfix without-subtitle">
                <h1 class="pull-left">Newsletter</h1>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <!--
         -->
            <!--=================================================
         =            base code copied from  page            =
         ==================================================-->
            <!-- made some modifications -->
            <div id="filteredTabs" class="nav-tabs-custom">
                <div class="btn-group btn-group-justified" role="group" aria-label="...">
                    <ul class="nav nav-tabs nav-justified tab--1" role="tablist">
                        <?php
                    if((in_array("write", $myp))) {
                    ?>
                            <li role="presentation" class="bdrR active"><a class="text-white" href="#create" aria-controls="create" role="tab" data-toggle="tab" aria-expanded="false">Create
                            Newsletter</a></li>
                            <?php } ?>
                            <li role="presentation" class="bdrR"><a class="text-white" id="myhistory" href="#archive" aria-controls="archive" role="tab" data-toggle="tab" aria-expanded="false">History</a></li>
                            <li role="presentation" class="bdrR"><a id="templates_window" class="text-white" href="#templates" aria-controls="templates" role="tab" data-toggle="tab" aria-expanded="false">Templets</a></li>
                    </ul>
                </div>
                <div class="tab-content p-t-15">
                    <div role="tabpanel" class="tab-pane  active" id="create">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="filter" class="sign-upverification">
                                    <div class="filter">
                                        <form class="">
                                            <div class="form-group p-r-10 clearfix">
                                                <div class="input-group pull-left">
                                                    <div class="filter-AllRegisteredUsers ">
                                                        <label class="dropdownOptions pull-right">
                                                            <select class="form-control" id="userFilter">
                                                                <option value="" selected disabled> Select Users Type
                                                                </option>
                                                                <option value="all_users">All Registered Users
                                                                </option>
                                                                <option value="active_users">All Active Users
                                                                </option>
                                                                <option value="inactive_users">All In-Active Users
                                                                </option>
                                                                <option value="keyword_owners">Keyword Owners</option>
                                                                <option value="blocked_users">Blocked Users</option>
                                                            </select>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="input-group pull-left p-l-20">
                                                    <div class="filter-AllRegisteredUsers ">
                                                        <label class="dropdownOptions pull-right">
                                                            <select class="form-control" id="senderFilter">
                                                                <option value="" selected disabled>Sender
                                                                </option>
                                                                <?php
                                                            foreach($sendersNews as $key=>$senders)
                                                            {
                                                                echo " <option value='".$key."'> ".$senders."</option>";
                                                            }
                                                            ?>
                                                            </select>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="col-lg-12">
                                    <form class="form-horizontal bg-lightGray1 p-t-15 border-all" name="createNewsletter">
                                        <div class="form-group">
                                            <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control" id="subject" placeholder="">
                                            </div>
                                        </div>
                                        <div class="form-group clearfix">
                                            <label for="createNewsletter-message" class="col-sm-2 control-label">Message :
                                            </label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control createNewsletter-message" name="createNewsletter-message" id="createNewsletter-message" rows="6" onkeyup="countChar(this,1000, '#createNewsletter-recieverDetails-info' );"></textarea>
                                                <div class="textareaInfo" id="createNewsletter-recieverDetails-info">
                                                    1000
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <label for="inputPassword3" class="col-sm-2 control-label"></label>
                                            <div class="col-sm-8">
                                                <div id="add_faq_error" style="color:red;">
                                                </div>
                                                <div id="add_faq_error_success" style="color:green;">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-8">
                                                <div class="pull-right ">
                                                    <button type="button" class="btn rounded-corner bg-darkblue text-white btn-default" data-toggle="modal" data-target="#Preview">Preview
                                                    </button>
                                                    <button type="button" id="templateSubmit" onclick="submitTemplate()" class="btn rounded-corner bg-darkblue text-white btn-default">
                                                        Save as Template
                                                    </button>
                                                    <button onclick="sendEmailtoUsers()" type="button" id="sendNewBtn" class="btn rounded-corner bg-darkblue text-white btn-default">
                                                        Send
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--modal to display editing data-->
                    <div class="modal madal--1 fade" id="addTempalateList" role="dialog">
                        <div class="modal-dialog" style="width: 920px">
                            <div class="modal-content">
                                <div class="modal-header ModalHeaderBackground" style="">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                                    <h4 class="modal-title" style="font-weight: bold">Add New Recored</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="popUpModal">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane " id="archive">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <div id="newRegisteredUsers">
                                    <table class="table table-hover text-center border-all border-lightBlue newsletterTable" id="news_history">
                                        <thead>
                                            <tr>
                                                <th  class="p-t-10 dtTableHeadPadding no-sort"> Date</th>
                                                <th  class="dtTableHeadPadding no-sort"> Category</th>
                                                <th  class="dtTableHeadPadding no-sort"> Sender</th>
                                                <th  class="dtTableHeadPadding no-sort"> Subject</th>
                                                <th  class="dtTableHeadPadding no-sort"> Content</th>
                                                <th  class="dtTableHeadPadding no-sort"> No. of Users</th>
                                                <th  class="dtTableHeadPadding no-sort status_news"> Status</th>
                                            </tr>
                                        </thead>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END #drafts -->
                    <div role="tabpanel" class="tab-pane " id="templates">
                        <div id="errors" style="margin-top: 10px"></div>
                        <div class="" id="ManageTemplate"> </div>
                    </div>
                </div>
            </div>
    </main>
    <!-- Modal -->
    <div class="createNewsletter-Preview-modal madal--1 modal fade" id="Preview" tabindex="-1" role="dialog" aria-labelledby="" style="margin-top:30vh">
        <div class="createNewsletter-Preview-modal-dialog modal-dialog" role="document">
            <div class="createNewsletter-Preview-modal-content modal-content">
                <div class="createNewsletter-Preview-modal-header modal-header p-5 bg-darkblue text-white">
                    <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="createNewsletter-Preview-modal-title modal-title text-center">Preview</h4>
                </div>
                <div class="createNewsletter-Preview-modal-body modal-body ">
                    <div class="text-center" id="previewBody">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="progressbar" tabindex="-1" role="dialog" aria-labelledby="" style="margin-top:30vh">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header p-5 bg-darkblue text-white">
                    <button type="button" class="close  text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="createNewsletter-Preview-modal-title modal-title text-center">Sending in Progress ...</h4>
                </div>
                <div class=" modal-body ">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 60%;">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/ckeditor/ckeditor.js"></script>
    <script type="text/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href"); // activated tab
        $("#errors").html("");
    });
    $(document).ready(function() {

        <?php
        if((!in_array("write",$myp))) {
        ?>
        $("#myhistory").trigger('click');
        <?php
        }
        ?>

        // CKEDITOR.replace('createNewsletter-message');

        var preview = CKEDITOR.document.getById('previewBody');


        function syncPreview() {
            var data = editor.getData();
            if (data != "") {
                data = data;
            } else {
                data = "No Data Found!"
            }
            preview.setHtml(data);
        }

        var editor = CKEDITOR.replace('createNewsletter-message', {
            on: {
                // Synchronize the preview on user action that changes the content.
                change: syncPreview,

                // Synchronize the preview when the new data is set.
                contentDom: syncPreview
            }
        });



    });


    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href"); // activated tab
        switch (target) {
            case "#templates":
                $("#ManageTemplate").load("../../controller/newsLetter/template.php");
                break;

            case "#archive":

                var table = $('#news_history').DataTable();
                table.destroy();
                $('#news_history').DataTable({
                    "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
                    "ordering": true,
                    "order": [],
                    "columnDefs": [{
                        orderable: false,
                        targets: "no-sort",
                    }, {
                        targets: "status_news",
                        "createdCell": function(td, cellData, rowData, row, col) {
                            if (cellData == 1) {
                                $(td).text("Sent");
                                $(td).css('color', 'green');
                            } else if (cellData == 0) {
                                $(td).text("Queued");
                                $(td).css('color', 'red');
                            }
                        }
                    }],
                    "ajax": "../../controller/newsLetter/history.php"
                });
                break;
            case "#create":
                $("#add_faq_error_success").html("");
                break;
        }
    });

    function addTemplate(id) {
        if (id) {
            var url = "../../controller/newsLetter/addTemplate.php?id=" + id;
        } else {
            var url = "../../controller/newsLetter/addTemplate.php";
        }
        $.ajax({
            type: "POST",
            dataType: "html",
            url: url,
            success: function(data) {
                $("#popUpModal").html("");
                $("#popUpModal").html(data);
                $("#addTempalateList").modal('show');

            },
            error: function() {
                console.log("fail");
            }
        });

    }

    function submitTemplate() {
        //var newsLetter = $(".cke_wysiwyg_frame").contents().find("body").html();
        var newsLetter = CKEDITOR.instances['createNewsletter-message'].getData();
        var subject = $("#subject").val();
        $("templateSubmit").css('pointer-events', 'none');
        if (newsLetter != '' & subject != '') {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../controller/newsLetter/templateSubmit.php",
                data: {
                    newsLetter: newsLetter,
                    subject: subject
                },
                success: function(data) {


                    if (data["errCode"] == -1) {

                        $(".cke_wysiwyg_frame").contents().find("body").html('');
                        $("#subject").val('');
                        $("#email").val('');
                        $("#templates_window").trigger("click");
                        $("#errors").html(bsAlert("success", "Template Saved Successfully."));
                    }
                },
                beforeSend: function(data) {

                },
                complete: function(data) {

                },
                error: function(data) {

                }

            });
        } else {
            $('#add_faq_error').text('All Fields Are Mandetory...!');
        }
    }

    function deleteTemplate(id) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "../../controller/newsLetter/templateDelete.php",
            data: {
                id: id
            },
            success: function(data) {

                if (data["errCode"] == -1) {
                    $("#ManageTemplate").load("../../controller/newsLetter/template.php");
                    $("#errors").html(bsAlert("success", "Template Deleted Successfully."));
                } else {
                    $("#errors").html(bsAlert("danger", "There was Error in deleting Template please try again."));
                }
            },
            beforeSend: function(data) {

            },
            complete: function(data) {

            },
            error: function(data) {
                console.log('Failed');
            }
        });
    }

    function sendEmailtoUsers() {

        //$("#progressbar").modal("show");
        //var newsLetterContent = $(".cke_wysiwyg_frame").contents().find("body").html();
        var newsLetterContent = CKEDITOR.instances['createNewsletter-message'].getData();
        var subject = $("#subject").val();
        var usersType = $("#userFilter").val();
        var sender = $("#senderFilter").val();

        operateLadda("sendNewBtn", "start");
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "../../controller/newsLetter/sendBulkEmail.php",
            data: {
                newsLetterContent: newsLetterContent,
                subject: subject,
                usersType: usersType,
                sender: sender
            },
            success: function(data) {
                operateLadda("sendNewBtn", "stop");
                if (data["errCode"] != "-1") {
                    $("#add_faq_error_success").html("");
                    $("#add_faq_error").html(data["errMsg"]);
                } else {
                    $("#add_faq_error").html("");
                    $("#add_faq_error_success").html(data["errMsg"]);
                    $("#subject").val("");
                    CKEDITOR.instances['createNewsletter-message'].setData('');
                }

            },
            beforeSend: function(data) {
                $("#sendNewBtn").attr("disabled", "true");
            },
            complete: function(data) {
                $("#sendNewBtn").removeAttr("disabled");
            },
            error: function(data) {
                //                    alert('Failed');
            }

        });
    }

    function viewContent(data)
    {
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

        data=Base64.decode(data);


        $('#ContentModal').modal("show");
        $('#ContentModal .modal-body').html(data);



    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
