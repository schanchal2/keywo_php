<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');

//end helper

//other
require_once('../../../core/errorMap.php');
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
?>

<div class="row">
<div class="col-lg-12 m-t-20">
    <div id="filter" class="sign-upverification">
        <form action="" method="POST" class="form-inline pull-right" role="form" name="filter">
            <div class="input-group styled-select">
                <select id="bulletin_status" style="width: 105px">
                    <option value="" disabled selected> Status</option>
                    <option value="enabled"> Enabled</option>
                    <option value="disabled"> Disabled</option>
                    <option value="all"> All</option>
                </select>
                <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
            </div>
            <?php
            if((in_array("write", $myp))) {
            ?>
            <div class="input-group styled-select" style="z-index: 1;">

                <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#anouncement">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                </button>

            </div>
            <?php } ?>
        </form>
    </div>
    </div>
</div>
<div class="table--lightBlue2 m-t-10">
    <div id="errors"></div>
    <div id="bulletin_pagination">
    </div>
</div>
<div class="modal madal--1  fade" id="edit_anouncement" role="dialog" aria-labelledby="edit_anouncement" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Announcement</h5>
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
            </div>
            <div class="modal-body p-b-5">
                <div id="redError" style="">
                    <div id="errMsgFailedEditB" style="padding-bottom: 10px;color:red;"></div>
                </div>
                <div id="greenError" style="">
                    <div id="errMsgSuccessEditB" style="padding-bottom: 10px;color:green;"></div>
                </div>
                <form>
                    <div id="editBulletinData">
                    </div>
                </form>
            </div>
            <div class="modal-footer border-none p-t-0">
                <div class="    p-t-10">
                    <button type="button" class="btn bg-darkblue text-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-darkblue text-white" onclick="updateBulletinData()">Update</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal madal--1  fade" id="anouncement" role="dialog" aria-labelledby="anouncement" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h5 class="modal-title">Create Announcement</h5>
            </div>
            <div class="modal-body p-b-5">
                <div id="redError" style="">
                    <div id="errMsgFailed" style="padding-bottom: 10px;color:red;"></div>
                </div>
                <div id="greenError" style="">
                    <div id="errMsgSuccess" style="padding-bottom: 10px;color:green;"></div>
                </div>
                <form id="announcement_form">
                    <div class="form-group row">
                        <label class="col-xs-2 col-form-label" for="bulletin_title">Title:</label>
                        <div class="col-xs-10">
                            <input type="text" id="bulletin_title" class="form-control bg-lightgray">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xs-2 col-form-label" for="bulletin_description">Description:</label>
                        <div class="col-xs-10">
                            <textarea class="form-control bg-lightgray noresize scrollBar" rows="3" id="bulletin_description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer border-none p-t-0">
                <div class="    p-t-10">
                    <button type="button" class="btn bg-darkblue text-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-darkblue text-white" onclick="addNewBulletin()">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
    getBulletinList();
});

function getBulletinList() {

    var bulletin_status = ($("#bulletin_status").val());

    if (bulletin_status == null) {
        bulletin_status = "all";
    }

    var limit = ($("#LimitedResult").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 10;
    }


    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/cms/about/bulletin_content.php",
        data: {
            limit: limit,
            listtype: "first",
            bulletin_status: bulletin_status
        },
        success: function(data) {
            $("#bulletin_pagination").html("");
            $("#bulletin_pagination").html(data);
        },
        error: function() {
            alert("fail");
        }
    });

}

$("#LimitedResults").change(function() {
    getBulletinList();
});


function getBulletinListPagination(pageno, bulletin_status, limit) {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/cms/about/bulletin_content.php",
        data: {
            bulletin_status: bulletin_status,
            limit: limit,
            listtype: 'pagination',
            pageno: pageno
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            $("#bulletin_pagination").html("");
            $("#bulletin_pagination").html(data);

        },
        error: function() {
            console.log("fail");
        }

    });
}


$("#bulletin_status").change(function() {
    getBulletinList();
});


function loadBulletinState() {
    var pageno = $("#hiddenpage").val();
    var bulletin_status = ($("#bulletin_status").val());

    if (bulletin_status == null) {
        bulletin_status = "all";
    }

    var limit = ($("#LimitedResult").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 10;
    }

    getBulletinListPagination(pageno, bulletin_status, limit);
}

function addNewBulletin() {

    var bulletin_title = $("#bulletin_title").val();
    var bulletin_description = $("#bulletin_description").val();

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/cms/about/bulletin_controller.php",
        data: {
            bulletin_title: bulletin_title,
            bulletin_description: bulletin_description,
            operation_type: "add_bulletin"
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            if (data["errCode"] != -1) {
                $("#errMsgFailed").text("");
                $("#errMsgFailed").text(data["errMsg"]);
            } else {
                $("#anouncement .close").click();
                $("#errors").html(bsAlert("success", "Successfully added Announcement"));
                loadBulletinState();
            }


        },
        error: function() {
            console.log("fail");
        }

    });
}

function setBulletinStatus(id, status, opType) {

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/cms/about/bulletin_controller.php",
        data: {
            bulletin_id: id,
            operation_type: opType,
            bulletin_status: status
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            if (data["errCode"] != -1) {
                if (status == "delete") {
                    $("#errors").html(bsAlert("danger", "There was error in Deleting announcement  please try again"));

                } else {
                    $("#errors").html(bsAlert("danger", "There was error in changing announcement status please try again"));

                }

            } else {
                loadBulletinState();

                if (status == "delete") {
                    $("#errors").html(bsAlert("success", "Successfully Deleted announcement."));
                }
            }


        },
        error: function() {
            console.log("fail");
        }

    });
}

function editBulletin(id) {
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/cms/about/bulletin_controller.php",
        data: {
            bulletin_id: id,
            operation_type: "edit_bulletin"
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            $("#editBulletinData").html("");
            $("#editBulletinData").html(data);

        },
        error: function() {
            console.log("fail");
        }

    });
}


function updateBulletinData() {

    var bulletin_title = $("#edit_bulletin_title").val();
    var bulletin_description = $("#edit_bulletin_description").val();
    var bulletin_id = $("#hiddenBulletinId").val();



    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/cms/about/bulletin_controller.php",
        data: {
            bulletin_id: bulletin_id,
            operation_type: "update_bulletin_data",
            bulletin_title: bulletin_title,
            bulletin_description: bulletin_description,
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            if (data["errCode"] != -1) {
                $("#errMsgFailedEditB").text("");
                $("#errMsgFailedEditB").text(data["errMsg"]);
                $("#errors").html(bsAlert("danger", "There was error in updating announcement details please try again."));

            } else {
                $("#edit_anouncement .close").click();
                $("#errors").html(bsAlert("success", "Announcement Details Updated Successfully."));
                loadBulletinState();
            }

        },
        error: function() {
            console.log("fail");
        }

    });
}


$('#anouncement').on('shown.bs.modal', function(e) {
    document.getElementById("errMsgFailed").innerHTML = "";
    document.getElementById("bulletin_title").value = "";
    document.getElementById("bulletin_description").value = "";
});
</script>
