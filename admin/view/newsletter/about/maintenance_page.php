<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');

//end helper

//other
require_once('../../../core/errorMap.php');
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
?>

<div class="row">
    <div class="col-lg-12 m-t-20">
        <div id="filter" class="sign-upverification">
            <form action="" method="POST" class="form-inline pull-right" role="form" name="filter">
                <div class="input-group styled-select">
                    <select id="main_window_status" style="width: 105px">
                        <option value="" disabled selected> Status</option>
                        <option value="1"> Active</option>
                        <option value="0"> De-Active</option>
                        <option value=""> All</option>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                </div>
                <?php
                if((in_array("write", $myp))) {
                ?>
                <div class="input-group styled-select" style="z-index: 1;">
                    <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#anouncement">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </button>
                </div>
                <?php } ?>
            </form>
        </div>
    </div>
</div>
<div class="table--lightBlue2 m-t-10">
    <div class="table-responsive">
        <table class="table table-hover text-center border-all border-lightBlue newsletterTable" id="maintainance_pagination">
            <thead>
                <tr>
                    <th style="width:20%" class="p-t-10 dtTableHeadPadding no-sort"> Date</th>
                    <th style="width:50%" class="dtTableHeadPadding no-sort"> Message</th>
                    <th style="width:30%" class="dtTableHeadPadding no-sort status_news"> Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
<div class="modal  madal--1 fade " id="anouncement" role="dialog" aria-labelledby="anouncement" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-headerPadding">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h5 class="modal-title skyeblue display-line">Create Maintenance Message</h5>
            </div>
            <div class="modal-body p-b-5">
                <div id="redError" style="">
                    <div id="errMsgFailed" style="padding-bottom: 10px;color:red;"></div>
                </div>
                <div id="greenError" style="">
                    <div id="errMsgSuccess" style="padding-bottom: 10px;color:green;"></div>
                </div>
                <form>
                    <div class="form-group row">
                        <label class="col-xs-2 col-form-label" for="maintain_msg_description">Message Description:</label>
                        <div class="col-xs-10">
                            <textarea class="form-control bg-lightgray noresize scrollBar" rows="3" id="maintain_msg_description"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer border-none p-t-0">
                <div class="p-t-10">
                    <button type="button" class="btn bg-darkblue text-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-darkblue text-white" onclick="addNewMaintinenceMsg()">Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal madal--1  fade " id="edit_anouncement" role="dialog" aria-labelledby="edit_anouncement" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-headerPadding">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                <h5 class="modal-title skyeblue display-line">Edit Maintenance Message</h5>
            </div>
            <div class="modal-body p-b-5">
                <div id="redError" style="">
                    <div id="errMsgFailedEditB" style="padding-bottom: 10px;color:red;"></div>
                </div>
                <div id="greenError" style="">
                    <div id="errMsgSuccessEditB" style="padding-bottom: 10px;color:green;"></div>
                </div>
                <form>
                    <div id="editBulletinData">
                    </div>
                </form>
            </div>
            <div class="modal-footer border-none p-t-0">
                <div class="p-t-10">
                    <button type="button" class="btn bg-darkblue text-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn bg-darkblue text-white" onclick="updateBulletinData()">Update
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>


    $(document).ready(function () {


        loadMaintainWindow("");
    });
    $("#main_window_status").change(function () {
        var newstatus = $(this).val();
        loadMaintainWindow(newstatus)

    });

    $('#anouncement').on('show.bs.modal', function (e) {
        $("#errMsgFailed").html("");
        $("#errMsgSuccess").html("");
        $("#maintain_msg_description").val("");
    });

    $('#edit_anouncement').on('show.bs.modal', function (e) {
        $("#errMsgFailedEditB").html("");
        $("#errMsgSuccessEditB").html("");
    });


        function loadMaintainWindow(status) {
        var table = $('#maintainance_pagination').DataTable();
        table.destroy();
        $('#maintainance_pagination').DataTable({
            "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
            "ordering": true,
            "order": [],
            "columnDefs": [{
                orderable: false,
                targets: "no-sort",
            }],
            <?php
            if((!in_array("write",$myp))) {
            ?>
            "aoColumnDefs": [{ "bVisible": false, "aTargets": [2] }],
            <?php
            }
            ?>

            "ajax": {
                url: "../../controller/newsLetter/maintanence_mode.php",
                type: "GET",
                data: {
                    status: status
                }
            }
    });
}

function changeMaintainanceStatus(id, maintain_status) {

    var maintain_id = id.replace("switch_", "");

    if (maintain_status == 1) {
        maintain_status = 0;
    } else if (maintain_status == 0) {
        maintain_status = 1;
    }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/newsLetter/maintain_controller.php",
            data: {
                maintain_id: maintain_id,
                operation_type: "change_maintainence_status",
                maintain_status: maintain_status
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                if (data["errCode"] != -1) {
                    $("#errors").html(bsAlert("danger", data["errMsg"]));
                    loadMaintainWindow("");
                } else {
                    $("#errors").html(bsAlert("success", data["errMsg"]));
                    loadMaintainWindow("");
            }


        },
        error: function() {
            console.log("fail");
        }

    });

}

function addNewMaintinenceMsg() {
    var maintain_description = $("#maintain_msg_description").val();
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/newsLetter/maintain_controller.php",
        data: {
            maintain_description: maintain_description,
            operation_type: "add_maintain"
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            if (data["errCode"] != -1) {
                $("#errMsgFailed").text("");
                $("#errMsgFailed").text(data["errMsg"]);
            } else {
                $("#errors").html(bsAlert("success", data["errMsg"]));
                loadMaintainWindow("");
                $("#anouncement").modal("hide");
            }
        },
        error: function() {
            console.log("fail");
        }

    });
}

function setMaintainStatus(maintain_id, status, opType) {

    if (status == "1") {
        $("#errors").html(bsAlert("danger", "You can not delete active Messages."));
        return false;

    } else {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/newsLetter/maintain_controller.php",
            data: {
                operation_type: opType,
                maintain_id: maintain_id
            },
            beforeSend: function() {
                $('#loadng-image').show();
            },
            success: function(data) {
                $('#loadng-image').hide();
                if (data["errCode"] != -1) {

                    $("#errors").html(bsAlert("danger", data["errMsg"]));

                } else {
                    $("#errors").html(bsAlert("success", data["errMsg"]));
                    loadMaintainWindow("");
                }


            },
            error: function() {
                console.log("fail");
            }

        });
    }
}

function editMaintain(maintain_id, status) {
    if (status == "1") {
        $("#errors").html(bsAlert("danger", "You can not Edit active Messages."));
        return false;

    } else {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/newsLetter/maintain_controller.php",
            data: {
                operation_type: "edit_messages",
                maintain_id: maintain_id
            },
            beforeSend: function() {
                $('#loadng-image').show();
            },
            success: function(data) {
                $('#loadng-image').hide();
                $("#editBulletinData").html("");
                $("#editBulletinData").html(data);
                $("#edit_anouncement").modal("show");
            },
            error: function() {
                console.log("fail");
            }

        });
    }
}



function updateBulletinData() {

    var maintain_description = $("#edit_bulletin_description").val();
    var maintain_id = $("#hiddenBulletinId").val();



    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/newsLetter/maintain_controller.php",
        data: {
            maintain_id: maintain_id,
            operation_type: "update_maintain_data",
            maintain_description: maintain_description,
        },
        beforeSend: function() {
            $('#loadng-image').show();
        },
        success: function(data) {
            $('#loadng-image').hide();
            if (data["errCode"] != -1) {
                $("#errMsgFailedEditB").text("");
                $("#errMsgFailedEditB").text(data["errMsg"]);

            } else {
                $("#edit_anouncement .close").click();
                $("#errors").html(bsAlert("success", data["errMsg"]));
                loadMaintainWindow("");
            }

        },
        error: function() {
            console.log("fail");
        }

    });
}
</script>