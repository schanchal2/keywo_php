<?php
session_start();
require_once "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/cms/about_model.php");
?>
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/onof/bootstrap-toggle.min.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Communication</h1><span>Announcement & Maintenance</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="filteredTabs" class="">
                        <div role="tabpanel">
                            <ul class="nav nav-pills nav-justified nav-pills--1 m-b-20" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#Announcements" role="tab" data-toggle="tab" aria-expanded="true"> Announcements </a>
                                </li>
                                <li role="presentation" class="">
                                    <a href="#maintainanceMode" role="tab" data-toggle="tab" aria-expanded="false"> Maintenance Mode </a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div id="errors" class="m-t-10"></div>
                            <div role="tabpanel" class="tab-pane active" id="Announcements">
                                <div id="AnnouncementsFilter">
                                    <div id="bulletin_data" class="resetTableData">
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="maintainanceMode">
                                <div id="newRegisteredUsers">
                                <div id="maintainanceModeFilter">
                                    <div id="maintainanceMode_data" class="resetTableData">
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/onof/bootstrap-toggle.min.js"></script>
    <script>
    $(document).ready(function() {
        $(".resetTableData").html("");
        $("#bulletin_data").load("about/bulletin_page.php");
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href"); // activated tab
        $(".resetTableData").html("");
        $("#errors").html("");
        switch (target) {
            case "#Announcements":
                $("#bulletin_data").load("about/bulletin_page.php");
                break;

            case "#maintainanceMode":
                $("#maintainanceMode_data").load("about/maintenance_page.php");
                break;
        }
    });
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
