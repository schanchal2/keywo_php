<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>Bid Expiry</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <style>
            
            .jumbotron
             {
                background: #f1f1f1!important;
                height:250px;
                margin-left:-60px;
                margin-right:-60px;
             }
            </style>
            <div class="container">
               <div class="jumbotron">
                    <div class="row">
                    <form class="form-horizontal">
                        <div class="form-group" style="margin-top:80px; text-style:bold">
                            <label class="control-label col-xs-4" for="setBidExpiryTime">Set Bid Expiry Time</label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="setBidExpiryTime">
                                <span class="fa fa-pencil form-control-feedback" style="padding:10px; margin-right:14px;"></span>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="control-label col-xs-4" for="minimumAmtToPlaceBid">Minimum Amt. To Place Bid</label>
                            <div class="col-xs-6">
                                <input type="text" class="form-control" id="minimumAmtToPlaceBid">
                                <span class="fa fa-pencil form-control-feedback" style="padding:10px; margin-right:14px;"></span>
                            </div>
                        </div>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

    <script type="text/javascript">

    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>