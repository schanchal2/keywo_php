<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>




<!-- <link rel="stylesheet" property="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/custom.css" rel="stylesheet" type="text/css" />
<style>

    div.jtable-main-container table.jtable tbody > tr > td .jtable-delete-command-button {
        background: url(../../frontend_libraries/img/deletess.png) no-repeat;
        width: 16px;
        height: 16px;
        opacity: 0.85;
    }


</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Block Domain</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div id="BlockedDomains" STYLE="" class=""></div>
        <!-- 



         -->
        <!-- <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>dh</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>fgh</td>
                    </tr>
                </tbody>
            </table>
            <span class="pull-left recordCountsShow  styled-select"> Show
            <select id="LimitedResults">
                    <option value="10" selected=""> 10 </option>
                    <option value="20"> 20 </option>
                    <option value="30"> 30 </option>
                    <option value="40"> 40 </option>
                    <option value="50"> 50 </option>
                </select>
            </span>
        </div> -->

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/jtable/jquery.jtable.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#BlockedDomains').jtable({
        title: 'Table of Blocked Domains',
        paging: true,
        pageSize: 10,
        sorting: true,
        useBootstrap: true,
        selecting: true, //Enable selecting
        multiselect: true, //Allow multiple selecting
        selectingCheckboxes: true, //Show checkboxes on first column
        actions: {
            listAction: '../../controller/settings/BlockedDomains.php?action=list',
            <?php
            if((in_array("write", $myp))) {
            ?>
            createAction: '../../controller/settings/BlockedDomains.php?action=create',
            <?php
            }
            ?>
            <?php
            if((in_array("write", $myp))) {
            ?>
            updateAction: '../../controller/settings/BlockedDomains.php?action=update',
            <?php
            }
            ?>
            <?php
            if((in_array("write", $myp))) {
            ?>
            deleteAction: '../../controller/settings/BlockedDomains.php?action=delete'
            <?php
            }
            ?>

        },
        fields: {
            id: {
                title: 'ID',
                key: true,
                list:false
            },
            domain_name: {
                title: 'Domain Name',
                width: '40%',
                input: function (data) {
                    if (data.record) {
                        return '<input type="text" style="width:100%" id="Edit-domain_name" name="EditDomain" style="" value="' + data.record.domain_name + '" />';
                    }else
                    {
                        return '<input type="text" style="width:100%" id="Edit-domain_name" name="addDomain" style="" value="" />';
                    }
                },
                display: function (data) {
                    return data.record.domain_name;
                }
            }
        },
        rowUpdated: function (event, data) {
            if (data.row) {

                $('#BlockedDomains').jtable('reload');
            }
        },
        <?php
        if((in_array("write", $myp))) {
        ?>
        toolbar: {
            items: [{
                icon: '../../frontend_libraries/img/deletess.png',
                text: 'Delete',
                click: function () {
                    $selectedRows = $('#BlockedDomains').jtable('selectedRows');
                    if ($selectedRows.length > 0) {
                        var ids = [];
                        $selectedRows.each(function () {
                            ids.push($(this).data('record').id);
                        });

                        var idsjoined = ids.join(); //will be such a string '2,5,7'
                        console.log(idsjoined);
                        // var url = '/delete.php?ids=' + idsjoined;


                        $.ajax({
                            type: "POST",
                            url: "../../controller/keywords/deletKeywordJtable.php",
                            data: {
                                id:idsjoined,type:"blockedDomain"
                            },
                            dataType: "json",
                            success: function (user) {


                                if (user["errCode"]==-1) {
                                    console.log("ajax success");
                                    $('#BlockedDomains').jtable('reload');

                                }
                            },
                            error: function () {
                                console.log("ajax error");
                            }
                        });


                    }
                }
            }]
        }
        <?php
        }
        ?>
    });
    $('#BlockedDomains').jtable('load');
});
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
