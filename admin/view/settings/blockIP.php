<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>




<!-- <link rel="stylesheet" property="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/custom.css" rel="stylesheet" type="text/css" />
<style>

    div.jtable-main-container table.jtable tbody > tr > td .jtable-delete-command-button {
        background: url(../../frontend_libraries/img/deletess.png) no-repeat;
        width: 16px;
        height: 16px;
        opacity: 0.85;
    }


</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Block IP</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div id="BlockedIPData" STYLE="" class=""></div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/jtable/jquery.jtable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#BlockedIPData').jtable({
            title: 'Table of Blocked IP Address',
            paging: true,
            pageSize: 10,
            sorting: true,
            useBootstrap: true,
            selecting: true, //Enable selecting
            multiselect: true, //Allow multiple selecting
            selectingCheckboxes: true, //Show checkboxes on first column
            actions: {
                listAction: '../../controller/settings/Blocked_ip.php?action=list',
                <?php
                if((in_array("write", $myp))) {
                ?>
                createAction: '../../controller/settings/Blocked_ip.php?action=create',
                <?php
                }
                ?>
                <?php
                if((in_array("write", $myp))) {
                ?>
                updateAction: '../../controller/settings/Blocked_ip.php?action=update',
                <?php
                }
                ?>
                <?php
                if((in_array("write", $myp))) {
                ?>
                deleteAction: '../../controller/settings/Blocked_ip.php?action=delete'
                <?php
                }
                ?>
            },
            fields: {
                id: {
                    title: 'ID',
                    key: true,
                    list:false
                },
                ip_address: {
                    title: 'IP Address',
                    width: '35%',
                    input: function (data) {
                        if (data.record) {
                            return '<input type="text" style="width:100%" id="Edit-ip_address" name="Editip" style="" value="' + data.record.ip_address + '" />';
                        }else
                        {
                            return '<input type="text" style="width:100%" id="Edit-ip_address" name="addip" style="" value="" />';
                        }
                    },
                    display: function (data) {
                        return data.record.ip_address;
                    }
                },
                reason_blocked: {
                    title: 'Block Reason',
                    width: '35%',
                    input: function (data) {
                        if (data.record) {
                            return '<textarea class="noresize" style="width:100%" id="Edit-reason_blocked" name="Editreason" style="">' + data.record.reason_blocked + '</textarea>';
                        }else
                        {
                            return '<textarea class="noresize" style="width:100%" id="Edit-reason_blocked" name="addreason" style=""></textarea>';
                        }
                    },
                    display: function (data) {
                        return data.record.reason_blocked;
                    }
                },
            },
            rowUpdated: function (event, data) {
                if (data.row) {

                    $('#BlockedIPData').jtable('reload');
                }
            },
            <?php
            if((in_array("write", $myp))) {
            ?>
            toolbar: {
                items: [{
                    icon: '../../frontend_libraries/img/deletess.png',
                    text: 'Delete',
                    click: function () {
                        $selectedRows = $('#BlockedIPData').jtable('selectedRows');
                        if ($selectedRows.length > 0) {
                            var ids = [];
                            $selectedRows.each(function () {
                                ids.push($(this).data('record').id);
                            });

                            var idsjoined = ids.join(); //will be such a string '2,5,7'
                            console.log(idsjoined);
                            // var url = '/delete.php?ids=' + idsjoined;


                            $.ajax({
                                type: "POST",
                                url: "../../controller/keywords/deletKeywordJtable.php",
                                data: {
                                    id:idsjoined,type:"BlockedIP"
                                },
                                dataType: "json",
                                success: function (user) {


                                    if (user["errCode"]==-1) {
                                        console.log("ajax success");
                                        $('#BlockedIPData').jtable('reload');

                                    }
                                },
                                error: function () {
                                    console.log("ajax error");
                                }
                            });


                        }
                    }
                }]
            }
            <?php
            }
            ?>
        });
        $('#BlockedIPData').jtable('load');
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
