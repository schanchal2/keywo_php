<?php
session_start();
require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/settings/activeTimeSocial.php");
/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

/******************************************************************/
$result=getActiveTimeSocial();
$data=$result["errMsg"];
$active_time=$data["active_time"];
$idle_time=$data["idle_time"];
$block_post_threshold=$data["block_post_threshold"];
/******************************************************************/

require_once("../../model/settings/fees_model.php");

$result=getAdminSettingsFromKeywordAdmin($connKeywords);
$data=$result["errMsg"];
$first_confirmation_minutes = (int) $data["first_confirmation_minutes"];
$final_confirmation_minutes = (int) $data["final_confirmation_minutes"];
$payment_confirmation_numbers = (int) $data["payment_confirmation_numbers"];

/******************************************************************/
?>
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">

<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Keywo Settings</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div id="feesAndCommission">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th colspan="4"> Set Time for Check User Status(Online,Offline,Idle)
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button style="background: #35ccf1;border: 1px solid #bdeefa;" type="button" class="btn btn-default pull-right" onclick="updateTimeDialog('Social Active Status Check Time')"><i class="fa fa-pencil"></i></button>
<?php } ?>
                            </th>
                        </tr>

                    </thead>
                    <tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Active Time </td>
                            <td> <span id="updatedActiveTime"><?php echo $active_time; ?></span> Minutes </td>
                            <td>
                            </td>
                        </tr>
                        <tr>

                            <td> 2 </td>
                            <td> Idle Time</td>
                            <td> <span id="updatedIdleTime"><?php echo $idle_time; ?></span> Minutes </td>
                            <td>
                             </td>
                        </tr>

                    </tbody>
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th colspan="4"> Set Social Block Post Threshold
                            <?php
                            if((in_array("write",$myp))) {

                            ?>
                            <button style="background: #35ccf1;border: 1px solid #bdeefa;" type="button" class="btn btn-default pull-right"
                                    onclick="updateBlockPostThreshold('Set Block Post Threshold')"><i class="fa fa-pencil"></i></button>
<?php } ?>
                        </th>
                    </tr>

                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td> Threshold Value </td>
                        <td> <span id="updatedBlockPostThreshold"><?php echo $block_post_threshold; ?></span> </td>
                        <td></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th colspan="4"> Payment Time and Confirmations Settings</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td> 1 </td>
                        <td> First Confirmation Minutes </td>
                        <td> <span id="UPfirst_confirmation_minutes"><?php echo $first_confirmation_minutes; ?></span>  </td>
                        <td>
                            <?php
                            if((in_array("write",$myp))) {

                            ?>
                            <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Trading Fees','first_confirmation_minutes','UPfirst_confirmation_minutes','firstConfUpdateField','Enter First Confirmation Minutes','')"><i class="fa fa-pencil"></i></button>
                        <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> 2 </td>
                        <td> Final Confirmation Minutes</td>
                        <td> <span id="UPfinal_confirmation_minutes"><?php echo $final_confirmation_minutes; ?></span>  </td>
                        <td>
                            <?php
                            if((in_array("write",$myp))) {

                            ?>
                            <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Withdrawal Fees','final_confirmation_minutes','UPfinal_confirmation_minutes','finalConfUpdateField','Enter Final Confirmation Minutes','')"><i class="fa fa-pencil"></i>
                            </button>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td> 3 </td>
                        <td> Payment Confirmation Numbers </td>
                        <td> <span id="UPpayment_confirmation_numbers"><?php echo $payment_confirmation_numbers; ?></span>  </td>
                        <td>
                            <?php
                            if((in_array("write",$myp))) {

                            ?>
                            <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Transfer Fees','payment_confirmation_numbers','UPpayment_confirmation_numbers','payConfUpdateField','Enter Payment Confirmation Numbers','')"><i class="fa fa-pencil"></i></button>
                        <?php } ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<div id="TimeValues"></div>
<div id="errDialog"></div>
<div id="feesValues"></div>
<div id="BlockPostThresholdData"></div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>
    $("#TimeValues").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Edit",
        width: 500,
        height: 300,
        buttons: {
            "Update": function (e) {

            }

        }

    });

    $("#BlockPostThresholdData").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Edit",
        width: 500,
        height: 200,
        buttons: {
            "Update": function (e) {

            }

        }

    });

    $("#feesValues").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Edit",
        buttons: {
            "Update": function (e) {

            }

        }

    });

    $("#errDialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Error !",
        width: 350,
        height: 180,
        buttons: {

        }
    });

    function updateTimeDialog(title)
    {
    var html='<form class="">'+
        '<div class="form-group has-feedback">'+
        '<label class="control-label col-sm-4" for="ActiveTime">	Active Time</label>'+
        ' <div class="col-sm-8">'+
        '<input type="number" class="" min="1" class="form-control" id="ActiveTime" placeholder="Enter Active Time">'+
        '</div>'+
        '</div>'+
            '<br/><br/><br/>'+
        '<div class="form-group has-feedback">'+
        '<label class="control-label col-sm-4" for="ActiveTime">Idle Time</label>'+
        '<div class="col-sm-8">'+
        '<input type="number" class="" min="1" class="form-control" id="idleTime" placeholder="Enter Idle Time">'+
        '</div>'+
        '</div>'+
        '</form>';

        $("#TimeValues").html(html);
        $("#TimeValues").dialog('option', 'title',title);

        var buttons = $('#TimeValues').dialog('option', 'buttons');
        buttons.Update = function() {
            updateTimeAjax();
        };

        $('#TimeValues').dialog('option', 'buttons', buttons);
        $("#TimeValues").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
    }


    function updateTimeAjax()
    {
        var activeTime=document.getElementById("ActiveTime").value;
        var idleTime=document.getElementById("idleTime").value;

        if(!isNaN(activeTime) && !isNaN(idleTime)) {
            if (activeTime % 1 != 0 && idleTime % 1 != 0) {
                $("#ActiveTime").val("");
                $("#idleTime").val("");
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Time Required in decimal only.</h4></center>");
                $("#errDialog").dialog('open');
                return false;
            }
        }
        if(!isNaN(activeTime)) {
            if (activeTime % 1 != 0) {
                $("#ActiveTime").val("");
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Time Required in decimal only.</h4></center>");
                $("#errDialog").dialog('open');
                return false;
            }
        }

        if(!isNaN(idleTime)) {
            if (idleTime % 1 != 0) {
                $("#idleTime").val("");
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Time Required in decimal only.</h4></center>");
                $("#errDialog").dialog('open');
                return false;
            }
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/updateTime_controller.php",
            data: {
                activeTime:activeTime, idleTime: idleTime
            },
            success: function (user) {

                if(user["errCode"]=="INVALID" || user["errCode"]!=-1)
                {
                    var error=user["errMsg"];
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                    $("#errDialog").dialog('open');
                }else if(user["errCode"]==-1)
                {
                    document.getElementById("updatedActiveTime").innerHTML = activeTime;
                    document.getElementById("updatedIdleTime").innerHTML = idleTime;
                    $("#TimeValues").dialog('close');
                }
            },
            error: function () {
                console.log("Failed to Update Data.");

            }
        });
    }

    function updateBlockPostThreshold(title)
    {
        var html='<form class="">'+
            '<div class="form-group has-feedback">'+
            '<label class="control-label col-sm-4" for="blockPostThresholdCount">Block Post Threshold</label>'+
            ' <div class="col-sm-8">'+
            '<input type="number" min="1" class="form-control dataInputLimit" id="blockPostThresholdCount" placeholder="Block Post Threshold">'+
            '</div>'+
            '</div>'+
            '</form>';

        $("#BlockPostThresholdData").html(html);
        $("#BlockPostThresholdData").dialog('option', 'title',title);

        var buttons = $('#BlockPostThresholdData').dialog('option', 'buttons');
        buttons.Update = function() {
            setBlockPostThresholdAjax();
        };

        $('#BlockPostThresholdData').dialog('option', 'buttons', buttons);
        $("#BlockPostThresholdData").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
    }


    $("#BlockPostThresholdData").delegate(".dataInputLimit", "keypress", function(e){
        if( e.keyCode ==46 || e.keyCode ==110)
        {
            //e.keyCode == 48 || e.keyCode ==96 ||
            return false;
        }
    });
    function setBlockPostThresholdAjax()
    {
        var blockedPostThreshold=document.getElementById("blockPostThresholdCount").value;

        if(!isNaN(blockedPostThreshold)) {
            if (blockedPostThreshold % 1 != 0) {
                $("#blockPostThresholdCount").val("");
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Block Threshold Required in decimal only.</h4></center>");
                $("#errDialog").dialog('open');
                return false;
            }
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/updateFees_controller.php",
            data: {
                updateValue:blockedPostThreshold, fieldToUpdate: "blockedPostThreshold"
            },
            success: function (user) {

                if(user["errCode"]=="INVALID" || user["errCode"]!=-1)
                {
                    var error=user["errMsg"];
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                    $("#errDialog").dialog('open');
                }else if(user["errCode"]==-1)
                {
                    document.getElementById("updatedBlockPostThreshold").innerHTML = blockedPostThreshold;
                    $("#BlockPostThresholdData").dialog('close');
                }
            },
            error: function () {
                console.log("Failed to Update Data.");

            }
        });
    }

    /***************************** Admin Settings keyword*********************************************/

    function updateFeesDialog(title,fieldname,idelementtoupdate,idtofileddynamic,placeholders,isfloat)
    {
        if(isfloat=="float")
        {
            var html='<div class="form-group has-feedback"> <input type="number"  min="1" step="0.01" class="form-control dataInputLimit" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
        }else {
            var html='<div class="form-group has-feedback"> <input type="number"  min="1" class="form-control dataInputLimit" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
        }
        $("#feesValues").html(html);
        $("#feesValues").dialog('option', 'title',title);

        var buttons = $('#feesValues').dialog('option', 'buttons');
        buttons.Update = function() {
            updateFeesAjaxPHP(idtofileddynamic,idelementtoupdate,fieldname);
        };

        $('#feesValues').dialog('option', 'buttons', buttons);
        $("#feesValues").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
    }


    $("#feesValues").delegate(".dataInputLimit", "keypress", function(e){
        if(e.keyCode == 48 || e.keyCode ==96 || e.keyCode ==46 || e.keyCode ==110)
        {
            return false;
        }
    });

    function updateFeesAjaxPHP(idtofileddynamic,idelementtoupdate,fieldname)
    {
        var fieldvalue=document.getElementById(idtofileddynamic).value;

        if(!isNaN(fieldvalue)) {
            if (fieldvalue % 1 != 0) {
                $("#"+idtofileddynamic).val("");
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Confirmation Minutes Required in decimal only.</h4></center>");
                $("#errDialog").dialog('open');
                return false;
            }
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/updateFees_controller.php",
            data: {
                fieldToUpdate:fieldname, updateValue: fieldvalue
            },
            success: function (user) {

                var msg = user["errMsg"];
                if (user["errCode"] == "-1") {
                    if(idelementtoupdate)
                    {
                        document.getElementById(""+idelementtoupdate).innerHTML = fieldvalue;
                        $("#feesValues").dialog('close');
                    }else
                    {
                        var error=user["errMsg"];
                        $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                        $("#errDialog").dialog('open');
                    }
                }
                else if(user["errCode"]=="INVALID") {
                    var error=user["errMsg"];
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                    $("#errDialog").dialog('open');
                }else {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Oop's Something went Wrong please try again!<h4>"+error+"</h4></center>");
                    $("#errDialog").dialog('open');
                }

            },
            error: function () {
                console.log("Failed to Update Data.");

            }
        });
    }
    /***************************** Admin Settings keyword *********************************************/
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
