<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

require_once("../../model/settings/fees_model.php");
/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

/******************************************************************/
$result=getAdminSettingsFromKeywordAdmin($connKeywords);
$data=$result["errMsg"];
$packing_list_duration = $data["cashout_packing_list_duration"];
$cashoutAutomodeAmount = number_format($data["cashout_automode_amount_limit"], 6);
$manualModeUamDept = number_format($data["cashout_manual_mode_amount_uamDepartment"], 6);
$manualModeMMDept = number_format($data["cashout_manual_mode_amount_mmDepartment"], 6);
$manualModeSMDept = number_format($data["cashout_manual_mode_amount_smDepartment"], 6);
/******************************************************************/
?>
    <link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>Cashout</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <style>
            .table-bordered
             {
                border: 1px solid #0299be;
             }
              .table-bordered > thead > tr > th, 
              .table-bordered > thead > tr > td, 
              .table-bordered > tbody > tr > th, 
              .table-bordered > tbody > tr > td, 
              .table-bordered > tfoot > tr > th, 
              .table-bordered > tfoot > tr > td 
               {
                border: 1px solid #0299be;
               }


                tbody tr td{
                    color:#337ab7;
                }

                .table-bordered tbody tr th
                {
                        border-collapse: collapse;
                        margin: 0 0 1em 0;
                        caption-side:top;
                }
                caption, td, th {
                  padding: 0.3em;
                }
                                
                .table-bordered  tr:first-child td #border:first-child {
                border-top: none;
                    }

            </style>
            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                       <table class="table text-center table-responsive table-bordered">
                            <thead>
                                <tr>
                                    <th colspan="6" style="height:60px; padding:20px;border-bottom: 0px solid transparent;">Amount List</th>
                                </tr>
                            
                            <thead>
                            <tbody>
                                <tr>
                                    <th rowspan="2" style="color:#0299be; text-align:center; padding:20px;background:#BDECF6;border-top: 0px solid transparent; border-bottom: 0px solid transparent;">Automode</th>
                                    <th colspan="4" style="color:#0299be; text-align:center; background:#BDECF6;border-top: 0px solid transparent;">Manual Mode</th>
                                </tr>
                                <tr>
                                    
                                    <td style="background:#BDECF6; color:#0299be;">UAM</td>
                                    <td style="background:#BDECF6; color:#0299be;">MM</td>
                                    <td style="background:#BDECF6; color:#0299be;">SM</td>

                                </tr>
                                <tr>
                                    <td style="border-top: 0px solid transparent;">
                                        =< <span id="cashout_auto_mode_amount"><?= $cashoutAutomodeAmount; ?></span> <?= $adminCurrency; ?>
                                        <span>
                                              <?php
                                              if((in_array("write",$myp))) {

                                              ?>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Autocashout Limit','cashout_automode_amount_limit','cashout_auto_mode_amount','cashout_auto_modeUpdateField','Enter Cashout Automode Amount Limit','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                              <?php } ?>
                                        </span>
                                    </td>
                                    <td>=< <span id="cashout_man_mode_amount_uam"><?= $manualModeUamDept; ?> <?= $adminCurrency; ?> </span>
                                        <span>
                                              <?php
                                              if((in_array("write",$myp))) {

                                              ?>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Limit for UAM Department ','cashout_manual_mode_amount_uamDepartment','cashout_man_mode_amount_uam','cashout_man_mode_uam_UpdateField','Enter Cashout Manual Mode Amount Limit for UAM DEPT.','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                              <?php } ?>
                                        </span>
                                    </td>

                                    <td>=< <span id="cashout_man_mode_amount_mm"><?= $manualModeMMDept; ?> <?= $adminCurrency; ?></span>
                                        <span>
                                              <?php
                                              if((in_array("write",$myp))) {

                                              ?>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Limit for MM Department ','cashout_manual_mode_amount_mmDepartment','cashout_man_mode_amount_mm','cashout_man_mode_mm_UpdateField','Enter Cashout Manual Mode Amount Limit for MM DEPT.','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                              <?php } ?>
                                        </span>
                                    </td>
                                    <td>=< <span id="cashout_man_mode_amount_sm"><?= $manualModeSMDept; ?> <?= $adminCurrency; ?></span>
                                        <span>
                                              <?php
                                              if((in_array("write",$myp))) {

                                              ?>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Limit for SM Department ','cashout_manual_mode_amount_smDepartment','cashout_man_mode_amount_sm','cashout_man_mode_sm_UpdateField','Enter Cashout Manual Mode Amount Limit for SM DEPT.','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                              <?php } ?>
                                        </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

             <div class="row" id="border">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <table class="table text-center table-responsive table-bordered">
                            <thead>
                                 <th colspan="2">Timings</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="color:#484040;">Packing List</td>  
                                    <td style="color:#C9C4C4; text-align:left;" >

                                        <=  <span id="cashout_packing_list_duration_id"><?= $packing_list_duration; ?></span> Hours

                                        <span>
                                              <?php
                                              if((in_array("write",$myp))) {

                                                  ?>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Packing List Time Duration ','cashout_packing_list_duration','cashout_packing_list_duration_id','cashout_packing_list_duration_UpdateField','Enter Cashout Packing List Duration Time','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                            <?php } ?>
                                        </span></td>
                                </tr>


                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <div id="feesValues"></div>
    <div id="errDialog"></div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

    <script>
        $("#feesValues").delegate(".dataInputLimit", "keypress", function(e){
            if(e.keyCode == 46 || e.keyCode ==110)
            {
                return false;
            }
        });

        $("#feesValues").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            width: 500,
            title: "Edit",
            buttons: {
                "Update": function (e) {

                }

            }

        });

        $("#errDialog").dialog({
            autoOpen: false,
            modal: true,
            resizable: false,
            title: "Error !",
            width: 350,
            height: 180,
            buttons: {

            }
        });


        function updateFeesDialog(title,fieldname,idelementtoupdate,idtofileddynamic,placeholders,isfloat)
        {
            if(idtofileddynamic=="cashout_packing_list_duration_UpdateField") {
                if (isfloat == "float") {
                    var html = '<div class="form-group has-feedback"> <input type="number"  min="1" class="form-control dataInputLimit" id="' + idtofileddynamic + '" name="' + idtofileddynamic + '" placeholder="' + placeholders + '"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                } else {
                    var html = '<div class="form-group has-feedback"> <input type="number"  min="1" class="form-control dataInputLimit" id="' + idtofileddynamic + '" name="' + idtofileddynamic + '" placeholder="' + placeholders + '"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                }
            }else
            {
                if (isfloat == "float") {
                    var html = '<div class="form-group has-feedback"> <input type="number"  min="1" class="form-control " id="' + idtofileddynamic + '" name="' + idtofileddynamic + '" placeholder="' + placeholders + '"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                } else {
                    var html = '<div class="form-group has-feedback"> <input type="number"  min="1" class="form-control " id="' + idtofileddynamic + '" name="' + idtofileddynamic + '" placeholder="' + placeholders + '"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                }
            }
            $("#feesValues").html(html);
            $("#feesValues").dialog('option', 'title',title);

            var buttons = $('#feesValues').dialog('option', 'buttons');
            buttons.Update = function() {
                updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname);
            };

            $('#feesValues').dialog('option', 'buttons', buttons);
            $("#feesValues").dialog('open');
            var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
        }

        function updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname)
        {
            var fieldvalue=parseFloat(document.getElementById(idtofileddynamic).value);

            var am_amount=parseFloat($("#cashout_auto_mode_amount").text());
            var sm_mm_amount=parseFloat($("#cashout_man_mode_amount_sm").text());
            var uam_mm_amount=parseFloat($("#cashout_man_mode_amount_uam").text());
            var mm_mm_amount=parseFloat($("#cashout_man_mode_amount_mm").text());


            if(idelementtoupdate=="cashout_auto_mode_amount")
            {
                if(fieldvalue>=uam_mm_amount)
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Automode Cashout Amount should not be greater than UAM Department .</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }
            }

            if(idelementtoupdate=="cashout_man_mode_amount_uam")
            {
                if(fieldvalue>=mm_mm_amount)
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Manual Mode Cashout Amount for UAM should not be greater MM Department.</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }else if(fieldvalue<=am_amount)
                {

                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Manual Mode Cashout Amount for UAM should not less than Automode .</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }
            }



            if(idelementtoupdate=="cashout_man_mode_amount_mm")
            {
                if(fieldvalue>=sm_mm_amount)
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Manual Mode Cashout Amount for MM should not be greater than SM Department.</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }else if(fieldvalue<=uam_mm_amount )
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Manual Mode Cashout Amount for MM should not less that UAM Department.</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }
            }

            if(idelementtoupdate=="cashout_man_mode_amount_sm")
            {
                if(fieldvalue<=mm_mm_amount)
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Manual Mode Cashout Amount for SM should not be less than MM Department.</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }
            }


            if(idelementtoupdate=="cashout_packing_list_duration_id")
            {
                if(!isNaN(fieldvalue)) {
                    if (fieldvalue % 1 != 0) {
                        $("#cashout_packing_list_duration_UpdateField").val("");
                        $("#errDialog").html("<br/><br/><center style='color:red'><h4>Time Duration Required in decimal only.</h4></center>");
                        $("#errDialog").dialog('open');
                        return false;
                    }
                }

                if(fieldvalue>24)
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Cashout Packing List Duration shold not greater than 24 Hours.</h4></center>");
                    $("#errDialog").dialog('open');
                    return false;
                }
            }

            if(idelementtoupdate!="cashout_packing_list_duration_id") {
                fieldvalue = fieldvalue.toFixed(6);
            }

            if(fieldvalue == "")
            {
                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Please enter digits only.</h4></center>");
                $("#errDialog").dialog('open');
            }else {
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../controller/settings/updateFees_controller.php",
                    data: {
                        fieldToUpdate:fieldname, updateValue: fieldvalue
                    },
                    success: function (user) {

                        var msg = user["errMsg"];
                        if (user["errCode"] == "-1") {
                            if(idelementtoupdate)
                            {
                                document.getElementById(""+idelementtoupdate).innerHTML = fieldvalue;
                                $("#feesValues").dialog('close');
                            }else
                            {
                                var error=user["errMsg"];
                                $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                                $("#errDialog").dialog('open');
                            }
                        }
                        else if(user["errCode"]=="INVALID") {
                            var error=user["errMsg"];
                            $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                            $("#errDialog").dialog('open');
                        }else {
                            $("#errDialog").html("<br/><br/><center style='color:red'><h4>Oop's Something went Wrong please try again!<h4>"+error+"</h4></center>");
                            $("#errDialog").dialog('open');
                        }

                    },
                    error: function () {
                        console.log("Failed to Update Data.");

                    }
                });
            }

        }


    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>