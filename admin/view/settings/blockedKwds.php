<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>

    <link rel="stylesheet" property="stylesheet"
          href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/custom.css" rel="stylesheet" type="text/css"/>
    <style>

        div.jtable-main-container table.jtable tbody > tr > td .jtable-delete-command-button {
            background: url(../../frontend_libraries/img/deletess.png) no-repeat;
            width: 16px;
            height: 16px;
            opacity: 0.85;
        }


    </style>

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>Block Keyword</span>
                <form action="" method="POST" class="form-inline pull-right" role="form">
                </form>
            </div>

            <br/>
            <div id="blockedKeywords" STYLE="" class=""></div>
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/jtable/jquery.jtable.min.js"></script>

    <script type="text/javascript">

        $(document).ready(function () {
            $('#blockedKeywords').jtable({
                title: 'Table of Blocked Keywords',
                paging: true,
                pageSize: 10,
                sorting: true,
                useBootstrap: true,
                saveUserPreferences: false,
                selecting: true, //Enable selecting
                multiselect: true, //Allow multiple selecting
                selectingCheckboxes: true, //Show checkboxes on first column
                actions: {
                    listAction: '../../controller/keywords/calling_pageKeywordBlockedActions.php?action=list',
                    <?php
                    if((in_array("write", $myp))) {
                    ?>
                    createAction: '../../controller/keywords/calling_pageKeywordBlockedActions.php?action=create',
                    <?php
                    }
                    ?>
                    <?php
                    if((in_array("write", $myp))) {
                    ?>
                    updateAction: '../../controller/keywords/calling_pageKeywordBlockedActions.php?action=update',
                    <?php
                    }
                    ?>
                    <?php
                    if((in_array("write", $myp))) {
                    ?>
                    deleteAction: '../../controller/keywords/calling_pageKeywordBlockedActions.php?action=delete'
                    <?php
                    }
                    ?>
                },
                fields: {
                    id: {
                        title: 'ID',
                        key: true,
                        list: false
                    },
                    keyword: {
                        title: 'Keywords ',
                        width: '40%',
                        input: function (data) {
                            if (data.record) {
                                return '<input type="text" style="width:100%" id="Edit-keyword" name="Editkeyword" style="" value="' + data.record.keyword + '" />';
                            } else {
                                return '<input type="text" style="width:100%" id="Edit-keyword" name="addKeyword" style="" value="" />';
                            }
                        },
                        display: function (data) {
                            return data.record.keyword;
                        }
                    }
                },
                rowUpdated: function (event, data) {
                    if (data.row) {

                        $('#blockedKeywords').jtable('reload');
                    }
                },
                <?php
                if((in_array("write", $myp))) {
                ?>
                toolbar: {
                    items: [{
                        icon: '../../frontend_libraries/img/deletess.png',
                        text: 'Delete',
                        click: function () {
                            $selectedRows = $('#blockedKeywords').jtable('selectedRows');
                            if ($selectedRows.length > 0) {
                                var ids = [];
                                $selectedRows.each(function () {
                                    ids.push($(this).data('record').id);
                                });

                                var idsjoined = ids.join(); //will be such a string '2,5,7'
                                console.log(idsjoined);
                                // var url = '/delete.php?ids=' + idsjoined;


                                $.ajax({
                                    type: "POST",
                                    url: "../../controller/keywords/deletKeywordJtable.php",
                                    data: {
                                        id: idsjoined, type: "blockedDelete"
                                    },
                                    dataType: "json",
                                    success: function (user) {


                                        if (user["errCode"] == -1) {
                                            console.log("ajax success");
                                            $('#blockedKeywords').jtable('reload');

                                        }
                                    },
                                    error: function () {
                                        console.log("ajax error");
                                    }
                                });


                            }
                        }
                    }]
                }
                <?php
                }
                ?>
            });
            $('#blockedKeywords').jtable('load');


        });


    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>