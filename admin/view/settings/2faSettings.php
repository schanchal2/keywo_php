<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
checkGroupAccess();
?>
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/onof/bootstrap-toggle.min.css">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>Transaction Security Settings</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <style type="text/css">

             .table-bordered 
             {
                border: 1px solid #0299be;

             }


             .table > thead > tr > th,  
             .table-bordered > tbody > tr > td, 
             .table-bordered > tfoot > tr > th 
             {
              border: 1px solid #0299be;

             }

            
            #border th{
                border-bottom:1px solid #0299be; 
            }

           .table-bordered > tbody > tr td

          {
            border-bottom: 1px solid #e1dddd;
            border-top: 1px solid #e1dddd;
          }
           table tr th{
                text-align: left;
            }
          table tr td{
            text-align: left;
            color:#0299be;
          }
            </style>
            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <div id="border">
                           <div id="two_factor_auth_setter">

                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/onof/bootstrap-toggle.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            get2faAuthSetter();
        });

        function get2faAuthSetter()
        {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/settings/2fasetter.php",
                data: {},
                success: function (data) {
                    $("#two_factor_auth_setter").html("");
                    $("#two_factor_auth_setter").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
        }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>