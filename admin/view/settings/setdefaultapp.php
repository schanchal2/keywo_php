
<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/settings/fees_model.php");
?>



<style>
	
  .box {
    background: #ffffff none repeat scroll 0 0;
    border-radius: 3px;
    border-top: 3px solid #d2d6de;
    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
    margin-bottom: 20px;
    margin-top: 6px;
    overflow: auto;
    padding-left: 15px;
    padding-top: 15px;
    position: absolute;
    width: 98%;
}
.gutterBig {
    float: left;
    height: 200px;
    width: 3%;
}
	.defaultBtn {
  background-color: #339de8;
  border: medium none;
  color: #fff;
  font-size: 12px;
  height: 30px;
  margin-right: 4%;
  padding: 7px 0;
  width: 80%;
  cursor: pointer;

}
.cardheader {
    background-color: #f7f7f7;
    box-shadow: 0 0 6px -2px;
    color: #797979;
    font-weight: bold;
    height: 40px;
    margin-left: -15px;
    margin-right: -15px;
    padding: 8px 0 0 12px;
}

.card {
    background-color: #fff;
    box-shadow: 0 0 6px -2px;
    height: 200px;
    margin-bottom: 20px;
}
.cardfooter {
    height: 50px;
    margin-left: -15px;
    margin-right: -15px;
}
.cardbody {
    height: 100px;
    margin-left: -15px;
    margin-right: -15px;
    margin-top: 15px;
}
</style>
<main>
    <div class="container-fluid" >

        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Set Default App</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>

        <?php

        $data=getAdminSettingsFromSearchAdmin($connSearch);
        $data=$data['errMsg'];
        $defaultappid=$data['default_app_id'];
        $result=getAllApps($connSearch);
        $result=$result['errMsg'];

        ?>


<div id="maintables">
<?php
//code for fetch the app image in database
foreach($result as $value)
{ ?>

           <div class="gutterBig"></div>
        <div class="col-md-2  card">
						<div class="cardheader"><?php echo $value['app_name'];
						                                     
						?></div>
						<div class="cardbody text-center"> <a href="javascript:;" target="_blank"><img style="width: 99%;height:92%"   src="../../../<?php echo $value['app_logo'];?>"> </a></div> <!-- <?php //echo $rootUrl.$value['app_url'];?> -->
						<div class="cardfooter text-center">
						<form method="post" onsubmit="javascript:;" id="defaultForm_1" action="javascript:;">
							<input type="hidden" value="1" name="app_id">
							<?php
							if(1) {
								?>
								<button id="javascript:;" onclick="setdefaultapp('defaultApp','<?php echo $value['app_id']; ?>','');"
										style="<?php if ($defaultappid == $value["app_id"]) {
											echo "background-color:#e7e7e7 !important;color: #193c55;";
										} ?>" class="defaultBtn" type="button"
								<?php if ($defaultappid == $value["app_id"]) {

									echo "disabled";

								}



								?>><?php if ($defaultappid == $value["app_id"]) {
									echo "Default App";
								} else {
									echo "Set Default App";
								}

								?></button>

								<?php
							}

							?>
						</form>
						</div>

						
						
</div><?php }?>







</div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

<script type="text/javascript">

    function setdefaultapp(fieldname,fieldvalue,idelementtoupdate) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/updateFees_controller.php",
            data: {
                fieldToUpdate:fieldname, updateValue: fieldvalue
            },
            success: function (user) {

                var msg = user["errMsg"];
                if (user["errCode"] == "-1") {

                        location.reload();


                }
                else {
                    console.log("Oop's Something went Wrong please try again!");
                }

            },
            error: function () {
                console.log("Failed to Update Data.");
            }
        });
    }



</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>