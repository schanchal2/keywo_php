<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

require_once("../../model/settings/fees_model.php");

/******************************************************************/
$result=getAdminSettingsFromKeywordAdmin($connKeywords);
$data=$result["errMsg"];
$packing_list_duration = $data["cashout_packing_list_duration"];
$cashoutAutomodeAmount = number_format($data["cashout_automode_amount_limit"], 6);
$manualModeUamDept = number_format($data["cashout_manual_mode_amount_uamDepartment"], 6);
$manualModeMMDept = number_format($data["cashout_manual_mode_amount_mmDepartment"], 6);
$manualModeSMDept = number_format($data["cashout_manual_mode_amount_smDepartment"], 6);
/******************************************************************/
checkGroupAccess();
?>
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">

<main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>TAT</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <style>
            .table-bordered
            {
                border: 1px solid #0299be;
            }
              .table-bordered > thead > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > tfoot > tr > td {
              border: 1px solid #0299be;
                }


                tbody tr td{
                    color:#337ab7;
                }

                .btn-group, .btn-group-vertical {
                    position: relative;
                    display: inline-block;
                    vertical-align: middle;
                    margin-bottom: 12px;
                }

                .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 3px 11px;
                text-align: center;
                text-decoration: none;
                font-size: 14px;
                margin: 4px 2px;
                cursor: pointer;
                border-radius: 8px;
            }
            .saveButtion {
                background-color:#a2def2;
            }

            .saveButtionSetting {
                background-color: #a2def2;
            }



            </style>
            <!--<div class="btn-group pull-right" style="margin-buttom:10px;">
               <button class="button" style="background-color: #4CAF50;">Approve</button>
               <button class="button saveButtion">Save Changes</button>
                <button class="button saveButtionSetting">Applying Previous Setting</button>
           </div>

          <div class="row">
               <div class="col-xs-12">
                   <div id="userList" class="m-t-15">
                       <table class="table text-center table-responsive table-bordered">
                           <thead>
                               <tr>
                                   <th>Request Type</th>
                                   <th>Hight Priority</th>
                                   <th>Normal Priority</th>
                                   <th>Low Priority</th>
                               </tr>
                           </thead>
                           <tbody>
                               <tr>
                                   <td>Service Request</td>
                                   <td><i class="fa fa-check"></i></td>
                                   <td></td>
                                   <td><i class="fa fa-check"></i></td>
                               </tr>

                               <tr>
                                   <td>Internal Ticket</td>
                                   <td></td>
                                   <td><i class="fa fa-check"></i></td>
                                   <td></td>
                               </tr>

                               <tr>
                                   <td>Cashouts</td>
                                   <td><i class="fa fa-check"></i></td>
                                   <td></td>
                                   <td><i class="fa fa-check"></i></td>
                               </tr>

                           </tbody>
                       </table>
                   </div>
               </div>
           </div><!-- Row Closed-->

            <div class="row">
                <div class="col-xs-12">
                    <div id="userList" class="m-t-15">
                        <table class="table text-center table-responsice table-bordered">
                            <thead>
                                 <th colspan="2">Request Type</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="color:#484040;">Query Ticket</td>  
                                    <td style="color:#C9C4C4; text-align:left;" >
                                        <=  <span id="cashout_packing_list_duration_id"><?= $packing_list_duration; ?></span> Hours

                                        <span>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Packing List Time Duration ','cashout_packing_list_duration','cashout_packing_list_duration_id','cashout_packing_list_duration_UpdateField','Enter Cashout Packing List Duration Time','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                        </span></td>
                                </tr>

                                 <tr>
                                    <td style="color:#484040;">Service Request</td>
                                    <td style="color:#C9C4C4; text-align:left;">
                                        <=  <span id="cashout_packing_list_duration_id"><?= $packing_list_duration; ?></span> Hours

                                        <span>
                                              <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Cashout Packing List Time Duration ','cashout_packing_list_duration','cashout_packing_list_duration_id','cashout_packing_list_duration_UpdateField','Enter Cashout Packing List Duration Time','float')"><i class="fa fa-pencil"></i>
                                              </button>
                                        </span></td>
                                </tr>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div>
        <div id="feesValues"></div>
        <div id="errDialog"></div>
        </div>
</main>
        <!-- praposed to remove from here and place in "footer.php"   -->
        <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

        <script>
            $("#feesValues").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                width: 500,
                title: "Edit",
                buttons: {
                    "Update": function (e) {

                    }

                }

            });

            $("#errDialog").dialog({
                autoOpen: false,
                modal: true,
                resizable: false,
                title: "Error !",
                width: 350,
                height: 180,
                buttons: {

                }
            });


            function updateFeesDialog(title,fieldname,idelementtoupdate,idtofileddynamic,placeholders,isfloat)
            {
                if(isfloat=="float")
                {
                    var html='<div class="form-group has-feedback"> <input type="number"  min="0" step="0.01" class="form-control" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                }else {
                    var html='<div class="form-group has-feedback"> <input type="number"  min="0" class="form-control" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
                }
                $("#feesValues").html(html);
                $("#feesValues").dialog('option', 'title',title);

                var buttons = $('#feesValues').dialog('option', 'buttons');
                buttons.Update = function() {
                    updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname);
                };

                $('#feesValues').dialog('option', 'buttons', buttons);
                $("#feesValues").dialog('open');
                var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
            }

            function updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname)
            {
                var fieldvalue=parseFloat(document.getElementById(idtofileddynamic).value);

                if(idelementtoupdate!="cashout_packing_list_duration_id") {
                    fieldvalue = fieldvalue.toFixed(6);
                }

                if(fieldvalue=="")
                {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>Please enter digits only.</h4></center>");
                    $("#errDialog").dialog('open');
                }else {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "../../controller/settings/updateFees_controller.php",
                        data: {
                            fieldToUpdate:fieldname, updateValue: fieldvalue
                        },
                        success: function (user) {

                            var msg = user["errMsg"];
                            if (user["errCode"] == "-1") {
                                if(idelementtoupdate)
                                {
                                    document.getElementById(""+idelementtoupdate).innerHTML = fieldvalue;
                                    $("#feesValues").dialog('close');
                                }else
                                {
                                    var error=user["errMsg"];
                                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                                    $("#errDialog").dialog('open');
                                }
                            }
                            else if(user["errCode"]=="INVALID") {
                                var error=user["errMsg"];
                                $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                                $("#errDialog").dialog('open');
                            }else {
                                $("#errDialog").html("<br/><br/><center style='color:red'><h4>Oop's Something went Wrong please try again!<h4>"+error+"</h4></center>");
                                $("#errDialog").dialog('open');
                            }

                        },
                        error: function () {
                            console.log("Failed to Update Data.");

                        }
                    });
                }

            }
        </script>