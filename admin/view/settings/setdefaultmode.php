
<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/settings/fees_model.php");
$data=getAdminSettingsFromSearchAdmin($connSearch);
$data=$data['errMsg'];
$defaultappid=$data['default_system_mode'];


?>

<style>
    .box {
        background: #ffffff none repeat scroll 0 0;
        border-radius: 3px;
        border-top: 3px solid #d2d6de;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        margin-bottom: 20px;
        margin-top: 6px;
        overflow: auto;
        padding-left: 15px;
        padding-top: 15px;
        position: absolute;
        width: 98%;
    }
    .card {
        background-color: #fff;
        box-shadow: 0 0 6px -2px;
        height: 200px;
        margin-bottom: 20px;
    }
    .defaultBtn {
        background-color: #339de8;
        border: medium none;
        color: #fff;
        font-size: 12px;
        height: 30px;
        margin-right: 4%;
        padding: 7px 0;
        width: 80%;
        cursor: pointer;
    }
    .gutterBig {
        float: left;
        height: 200px;
        width: 3%;
    }
    .cardheader {
        background-color: #f7f7f7;
        box-shadow: 0 0 6px -2px;
        color: #797979;
        font-weight: bold;
        height: 40px;
        margin-left: -15px;
        margin-right: -15px;
        padding: 8px 0 0 12px;
    }

    .cardfooter {
        height: 50px;
        margin-left: -15px;
        margin-right: -15px;
    }
    .cardbody {
        height: 100px;
        margin-left: -15px;
        margin-right: -15px;
        margin-top: 15px;
    }

    .disabledBtnclass
    {
        background-color:#e7e7e7 !important;
        color: #193c55;
    }
</style>

<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Set Default Mode</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>


        <div id="maintables">

            <div class="gutterBig"></div>
            <div class="col-md-2  card">
                <div class="cardheader">Search Mode</div>
                <div class="cardbody text-center"> <a href="javascript:;" target="_blank"></a><i class="fa fa-search-plus fa-5x" aria-hidden="true" style="color: #193C55"></i>
                </div> <!-- <?php //echo $rootUrl.$value['app_url'];?> -->

                <div class="cardfooter text-center">
                    <form method="post" onsubmit="javascript:;" id="defaultForm_1" action="javascript:;">
                        <input type="hidden" value="1" name="mode_id">

                        <button id="searchBtn" onclick="setdefaultmode('defaultMode','1','searchBtn');"
                                class="defaultBtn <?php if ($defaultappid == 1) {
                                    echo "disabledBtnclass";
                                } ?>" type="button"
                            <?php if ($defaultappid == 1) {

                                echo "disabled";

                            }



                            ?>><?php if ($defaultappid == 1) {
                                echo "Default Mode";
                            } else {
                                echo "Set Default Mode";
                            }

                            ?></button>

                    </form>
                </div>



            </div>

            <div class="gutterBig"></div>
            <div class="col-md-2  card">
                <div class="cardheader">Social Mode</div>
                <div class="cardbody text-center"> <a href="javascript:;" target="_blank"> <i class="fa fa-users fa-5x" aria-hidden="true" style="color: #193C55"></i></a></div> <!-- <?php //echo $rootUrl.$value['app_url'];?> -->



                <div class="cardfooter text-center">
                    <form method="post" onsubmit="javascript:;" id="defaultForm_2" action="javascript:;">
                        <input type="hidden" value="2" name="mode_id">

                        <button id="socialBtn" onclick="setdefaultmode('defaultMode','2','socialBtn');"
                                style="" class="defaultBtn <?php if ($defaultappid == 2) {
                            echo "disabledBtnclass";
                        } ?>" type="button"
                            <?php if ($defaultappid == 2) {

                                echo "disabled";

                            }





                            ?>><?php if ($defaultappid == 2) {
                                echo "Default Mode";
                            } else {
                                echo "Set Default Mode";
                            }

                            ?></button>

                    </form>
                </div>



            </div>


        </div>





    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

<script type="text/javascript">

    function setdefaultmode(fieldname,fieldvalue,idelementtoupdate) {
        var classes;
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/updateFees_controller.php",
            data: {
                fieldToUpdate:fieldname, updateValue: fieldvalue
            },
            success: function (user) {

                var msg = user["errMsg"];
                if (user["errCode"] == "-1") {

                    // location.reload();
                    $(".disabledBtnclass").removeClass("disabledBtnclass");

                    $("#"+idelementtoupdate).prop('disabled', true);
                    $("#"+idelementtoupdate).addClass("disabledBtnclass");


                    $(":not(#"+idelementtoupdate+")").prop('disabled', false);
                    $(".defaultBtn").text("Set Default Mode");
                    $("#"+idelementtoupdate).text("Default Mode");


                }
                else {
                    console.log("Oop's Something went Wrong please try again!");
                }

            },
            error: function () {
                console.log("Failed to Update Data.");

            }
        });
    }



</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>


