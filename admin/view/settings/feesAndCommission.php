<?php
session_start();
require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/settings/fees_model.php");
/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

/******************************************************************/
$result=getAdminSettingsFromKeywordAdmin($connKeywords);
$data=$result["errMsg"];
$tradingFess = $data["trading_commision_percent"];
$transferFess = $data["transferFees"];
$withdrawalFess = number_format($data["withdrawal_fees"], 8);
$renewalFees = $data["kwd_renewal_fees_per_year"];
$affearningper = $data["affiliate_earning_percent"];
$fbpercent = $data["first_buy_percent"];
$mwithdrawal = $data["minimum_withdrawal_amount"];
$searchaffper = $data["search_affiliate_percent"];
$firstememberren = $data["first_remember_period_for_renewal"];
$dailyememberren = $data["daily_remember_period_for_renewal"];
$graceememberren = $data["grace_period_for_renewal"];
$maintenancepercent = $data["searchtrade_maintenance_percent"];
$post_share_percent = $data["post_share_percent"];

/******************************************************************/
$result=getAdminSettingsFromSearchAdmin($connSearch);
$result=$result["errMsg"];
$currentpayout= $result["current_max_payout"];
$Ipmaxlimit = $result["IPMaxLimit"];
/******************************************************************/
?>
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Fees and Commission</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div id="feesAndCommission">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th colspan="4"> Fees And Commision</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Trading fees </td>
                            <td> <span id="updatedTradingFees"><?php echo $tradingFess; ?></span> % </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Trading Fees','tradeFees','updatedTradingFees','tradeFessUpdateField','Enter Trading Fees','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> Withdrawal Fees</td>
                            <td> <span id="updatedwithdrawalFees"><?php echo $withdrawalFess; ?></span> <?= $adminCurrency; ?> </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Withdrawal Fees','withdrawalFees','updatedwithdrawalFees','withdrawalFeesUpdateField','Enter Withdrawal Fees','float')"><i class="fa fa-pencil"></i>
                                </button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> Transfer Fees </td>
                            <td> <span id="updateTransferFees"><?php echo $transferFess; ?></span>  <?= $adminCurrency; ?></td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Transfer Fees','TransferFees','updateTransferFees','TransferFeesUpdateField','Enter Transfer Fees','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> Renewal Fees  </td>
                            <td> <span id="updateRenewalFees"><?php echo $renewalFees; ?></span> <?= $adminCurrency; ?> </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                    ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Renewal Fees','RenewalFees','updateRenewalFees','RenewalFeesUpdateField','Enter Renewal Fees','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> Keyword Referral (%)</td>
                            <td> <span id="updatedkwdReferralFees"><?php echo $affearningper; ?> </span>% </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Keyword Referral Fees','kwdReferralFees','updatedkwdReferralFees','kwdReferralUpdateField','Enter  Keyword Referral Fees','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 6 </td>
                            <td> Search Refferral (%)</td>
                            <td> <span id="searchaffnewtypepayout"><?php echo $searchaffper; ?></span> % </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Search Referral %','searchaffnewtype','searchaffnewtypepayout','searchaffnewtypeUpdateField','Enter Search Referral %','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 7 </td>
                            <td> Refferal Discount (Cashback) (%)</td>
                            <td> <span id="UpdatefirstBuyCashback"><?php echo $fbpercent; ?></span> % </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit First Buy Cashback %','firstBuyCashback','UpdatefirstBuyCashback','firstBuyCashbackUpdateField','Enter First Buy Cashback %','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 8 </td>
                            <td> Minimum Cashout Amt</td>
                            <td> <span id="updatedminimumWithdrawalFees"><?php echo $mwithdrawal; ?> </span> <?= $adminCurrency; ?> </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Minimum Cashout Fees','minimumWithdrawalFees','updatedminimumWithdrawalFees','minimumWithdrawalFeestoupdate','Enter Minimum Cashout Fees','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>

                        <tr>
                            <td> 9 </td>
                            <td> Post Share (%) </td>
                            <td> <span id="updatepost_share_percent"><?php echo $post_share_percent; ?> </span> %</td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Post Share %','post_sharePercentage','updatepost_share_percent','updatestPostShareFeesUpdateField','Enter Post Share %','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 10 </td>
                            <td> Maintenance(%)</td>
                            <td>  <span id="updatestMaintainanceFees"><?php echo $maintenancepercent; ?></span> % </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Maintainance %','stMaintainanceFees','updatestMaintainanceFees','updatestMaintainanceFeesUpdateField','Enter Maintainance %','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 11 </td>
                            <td> Current Max Payout</td>
                            <td> <span id="updatedcurrentpayout"><?php echo $currentpayout; ?></span> <?= $adminCurrency; ?> </td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit Current MAX Payout','currentpayout','updatedcurrentpayout','currentpayoutUpdateField','Enter Current Payout','float')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <td> 12 </td>
                            <td> IP Max Limit </td>
                            <td> <span id="maxiplimitpayout"><?php echo $Ipmaxlimit; ?> </span></td>
                            <td>
                                <?php
                                if((in_array("write",$myp))) {

                                ?>
                                <button type="button" class="btn btn-default" onclick="updateFeesDialog('Edit MAX IP Limit','maxiplimit','maxiplimitpayout','maxiplimitUpdateField','Enter MAX IP Limit','nfloat')"><i class="fa fa-pencil"></i></button>
                                <?php } ?>
                            </td>

                        </tr>
                        <!--<tr>
                            <td> 14 </td>
                            <td> First Remember - Renewal Days </td>
                            <td> <?php //echo $firstememberren; ?> Days </td>
                            <td>
                                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td> 10 </td>
                            <td> daily Remember - Renewal Days</td>
                            <td> <?php //echo $dailyememberren; ?> Days</td>
                            <td>
                                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>
                        <tr>
                            <td> 10 </td>
                            <td> Grace Remember - Renewal Days</td>
                            <td> <?php //echo $graceememberren; ?> Days </td>
                            <td>
                                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i></button>
                            </td>
                        </tr>-->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<div id="feesValues"></div>
<div id="errDialog"></div>

</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>
    $("#feesValues").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Edit",
        buttons: {
            "Update": function (e) {

            }

        }

    });

    $("#errDialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Error !",
        width: 350,
        height: 180,
        buttons: {

        }
    });

    function updateFeesDialog(title,fieldname,idelementtoupdate,idtofileddynamic,placeholders,isfloat)
    {
        if(isfloat=="float")
        {
            var html='<div class="form-group has-feedback"> <input type="number"  min="0" step="0.01" class="form-control" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
        }else {
            var html='<div class="form-group has-feedback"> <input type="number"  min="0" class="form-control" id="'+idtofileddynamic+'" name="'+idtofileddynamic+'" placeholder="'+placeholders+'"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';
        }
        $("#feesValues").html(html);
        $("#feesValues").dialog('option', 'title',title);

        var buttons = $('#feesValues').dialog('option', 'buttons');
        buttons.Update = function() {
            updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname);
        };

        $('#feesValues').dialog('option', 'buttons', buttons);
        $("#feesValues").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
    }

    function updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname)
    {
        var fieldvalue=document.getElementById(idtofileddynamic).value;

        if(fieldvalue=="")
        {
            $("#errDialog").html("<br/><br/><center style='color:red'><h4>Please enter digits only.</h4></center>");
            $("#errDialog").dialog('open');
        }else {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/settings/updateFees_controller.php",
                data: {
                    fieldToUpdate:fieldname, updateValue: fieldvalue
                },
                success: function (user) {

                    var msg = user["errMsg"];
                    if (user["errCode"] == "-1") {
                        if(idelementtoupdate)
                        {
                            document.getElementById(""+idelementtoupdate).innerHTML = fieldvalue;
                            $("#feesValues").dialog('close');
                        }else
                        {
                            var error=user["errMsg"];
                            $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                            $("#errDialog").dialog('open');
                        }
                    }
                    else if(user["errCode"]=="INVALID") {
                        var error=user["errMsg"];
                        $("#errDialog").html("<br/><br/><center style='color:red'><h4>"+error+"</h4></center>");
                        $("#errDialog").dialog('open');
                    }else {
                        $("#errDialog").html("<br/><br/><center style='color:red'><h4>Oop's Something went Wrong please try again!<h4>"+error+"</h4></center>");
                        $("#errDialog").dialog('open');
                    }

                },
                error: function () {
                    console.log("Failed to Update Data.");

                }
            });
        }

    }
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
