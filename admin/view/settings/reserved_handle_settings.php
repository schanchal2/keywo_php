<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
    <style>
    .dataTables_filter {
        display: none;
    }
    
    .sorting_desc:after,
    .sorting:after,
    .sorting_asc:after {
        content: "" !important;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding: 0em !important;
    }
    
    .dtTableHeadPadding {
        padding: 5px !important;
    }
    </style>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>Blocked Handle</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="nav-tabs-custom">
                <ul class="nav nav-pills" style="background: #97e5f8;">
                    <li><a href="#reservhandle" id="reserveClick" data-toggle="tab">Reserved Handle</a></li>
                    <li class="active"><a href="#assignHandles" id="assignClick" data-toggle="tab">Assigned Handle</a></li>
                </ul>
                <div class="tab-content">
                    <div id="errors" style="margin-top: 10px;"></div>
                    <div role="tabpanel" class="tab-pane " id="assignHandles">
                        <div id="assignedHandledata">
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="reservhandle">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="box">
                                    <?php
                                if((in_array("write",$myp))) {


                                ?>
                                        <div class="box-header">
                                            <div class="row " >
                                                <div class="col-xs-12">
                                                    <!--  <a href="javascript:;" class="btn btn-success pull-right" id="addNewHandlebtn" data-toggle="modal" data-target="#addNewHandle" style="text-decoration: none;margin-bottom: 5px;margin-right: 15px;background: #0299be;">
                                                Add Handle <span
                                                    class="fa fa-plus-circle"></span></a> -->
                                                    <div class="pull-right" style="z-index: 1;">
                                                        <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#addNewHandle" id="addNewHandlebtn">
                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                }
                                ?>
                                            <!-- /.box-header -->
                                            <div class="box-body">
                                                <div id="reservedHandledata">
                                                </div>
                                            </div>
                                            <!-- /.box-body -->
                                </div>
                                <!-- /.box -->
                                <!-- /.box -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal fade" id="addNewHandle" tabindex="-1" role="dialog" aria-labelledby="addNewHandle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center" style="width:35%">
                <div class="modal-content">
                    <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                        <button type="button" style="opacity: 1;font-size: xx-large;color: white;" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="assignHandleModallable" style="color:white;font-weight: bold">Add New Handle</h4>
                    </div>
                    <div class="modal-body">
                        <div id="rederror" style="">
                            <div id="errmsgnew" style="padding: 0px;color:red;"></div>
                        </div>
                        <form>
                            <div class="register-box-body" id="assignHandleModalbody">
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-3">
                                        <h5>Handle ID:</h5>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="handleID">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="">
                                    <div class="col-lg-4 ">
                                    </div>
                                    <div class=" pull-right" style="margin: 0px;padding: 0px">
                                        <div class="col-lg-4 ">
                                            <button type="submit" id="save_newly_added" class="btn btn-success ajaxhide" onclick="addNewHandle()">Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editHandleModal" tabindex="-1" role="dialog" aria-labelledby="editHandleModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center" style="width:35%">
                <div class="modal-content">
                    <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                        <button type="button" style="opacity: 1;font-size: xx-large;color: white;" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editHandleModallable" style="color:white;font-weight: bold">Edit Handle</h4>
                    </div>
                    <div class="modal-body">
                        <div id="rederror" style="">
                            <div id="errmsgupdateHAndle" style="padding: 0px;color:red;"></div>
                        </div>
                        <form>
                            <div class="register-box-body" id="editHandleModalbody">
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-4">
                                        <h5>Handle Name:</h5>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="userHandleIdedit">
                                            <input type="hidden" class="form-control" id="oldHandleID">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="">
                                    <div class="col-lg-4 ">
                                    </div>
                                    <div class=" pull-right" style="margin: 0px;padding: 0px">
                                        <div class="col-lg-4 ">
                                            <button type="submit" id="update_old" class="btn btn-success ajaxhide" onclick="updateReservedHandle()">Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="assignHandleModal" tabindex="-1" role="dialog" aria-labelledby="assignHandleModal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center" style="width:35%">
                <div class="modal-content">
                    <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                        <button type="button" style="opacity: 1;font-size: xx-large;color: white;" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="assignHandleModallable" style="color:white;font-weight: bold">Assign to</h4>
                    </div>
                    <div class="modal-body">
                        <div id="rederror" style="">
                            <div id="errmsgassign" style="padding: 0px;color:red;"></div>
                        </div>
                        <form>
                            <div class="register-box-body" id="assignHandleModalbody">
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-3">
                                        <h5>User Name:</h5>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="addHandleuserName">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-3">
                                        <h5>Email ID:</h5>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="addHandleuserEmail">
                                            <input type="hidden" class="form-control" id="userHandleId">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="">
                                    <div class="col-lg-4 ">
                                    </div>
                                    <div class=" pull-right" style="margin: 0px;padding: 0px">
                                        <div class="col-lg-4 ">
                                            <button type="submit" id="save_new" class="btn btn-success ajaxhide" onclick="saveReservedHandle()">Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editHandleModalassigned" tabindex="-1" role="dialog" aria-labelledby="editHandleModalassigned" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center" style="width:35%">
                <div class="modal-content">
                    <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                        <button type="button" style="opacity: 1;font-size: xx-large;color: white;" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="editHandleModallableassigned" style="color:white;font-weight: bold">Edit Handle</h4>
                    </div>
                    <div class="modal-body">
                        <div id="rederror" style="">
                            <div id="errmsgedithandle" style="padding: 0px;color:red;"></div>
                        </div>
                        <form>
                            <div class="register-box-body" id="editHandleModalbodyassigned">
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-4">
                                        <h5>User Name:</h5>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="userNameAssignedHAndle">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top:20px">
                                    <div class="col-lg-4">
                                        <h5>User Email:</h5>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="userEmailAssignedHandle">
                                            <input type="hidden" class="form-control" id="handleIdAssigned">
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="">
                                    <div class="col-lg-4 ">
                                    </div>
                                    <div class=" pull-right" style="margin: 0px;padding: 0px">
                                        <div class="col-lg-4 ">
                                            <button type="submit" id="save_old" class="btn btn-success ajaxhide" onclick="updateAssignedHandle()">Update
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        $("#reserveClick").trigger('click');
    });

    $('#editHandleModalassigned').on('show.bs.modal', function(e) {
        $("#errmsgedithandle").html("");
    });

    $('#editHandleModal').on('show.bs.modal', function(e) {
        $("#errmsgupdateHAndle").html("");
    });

    $('#addNewHandle').on('show.bs.modal', function(e) {
        $("#errmsgnew").html("");
        $("#handleID").val("");
    });

    //showGIFDataDOTload()
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $("#errors").html("");
        var target = $(e.target).attr("href"); // activated tab
        switch (target) {

            case "#reservhandle":
                $("#reservedHandledata").html(showLoadingDiv()).load("../../controller/settings/reserveHandle.php");
                break;


            case "#assignHandles":
                $("#assignedHandledata").html(showLoadingDiv()).load("../../controller/settings/assignedHandle.php");
                break;
        }
    });


    function saveReservedHandle() {
        var userName = $("#addHandleuserName").val();
        var userEmail = $("#addHandleuserEmail").val();
        var handleID = $("#userHandleId").val();
        operateLadda("save_new", "start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/reserve_handle_crud.php",
            data: {
                handleID: handleID,
                userName: userName,
                userEmail: userEmail,
                action: "assign"
            },
            success: function(user) {
                operateLadda("save_new", "stop");
                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {


                    $("#assignHandleModal").modal('hide');
                    $("#assignClick").trigger('click');


                    $("#errors").html(bsAlert("success", "Handle Assigned Successfully!"));

                } else if (user["errCode"] == "5") {
                    $("#errmsgassign").html("");
                    $("#errmsgassign").html(msg);
                }
            },
            error: function() {
                console.log("Failed to assign handle Data.");

            }
        });
    }


    function addNewHandle() {
        var handleID = $("#handleID").val();



        operateLadda("save_newly_added", "start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/reserve_handle_crud.php",
            data: {
                handleID: handleID,
                action: "add"
            },
            success: function(user) {
                operateLadda("save_newly_added", "stop");

                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {

                    $("#addNewHandle").modal('hide');
                    $("#reservedHandledata").html(showLoadingDiv()).load("../../controller/settings/reserveHandle.php");


                    $("#errors").html(bsAlert("success", "Handle Added Successfully!"));

                    $("#handleID").val(""); //make it blank

                } else if (user["errCode"] == "5") {
                    $("#errmsgnew").html("");
                    $("#errmsgnew").html(msg);
                }
            },
            error: function() {
                console.log("Failed to add handle.");

            }
        });
    }


    function deleteHandleData(handleID, tab) {

        bootbox.confirm({
            message: "<h4>Are you sure to delete this Handle?</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "../../controller/settings/reserve_handle_crud.php",
                        data: {
                            handleID: handleID,
                            action: "delete"
                        },
                        success: function(user) {

                            var msg = user["errMsg"];

                            if (user["errCode"] == "-1") {



                                if (tab == "reserved") {
                                    $("#reservedHandledata").html(showLoadingDiv()).load("../../controller/settings/reserveHandle.php");
                                } else {
                                    $("#assignedHandledata").html(showLoadingDiv()).load("../../controller/settings/assignedHandle.php");
                                }

                                $("#errors").html(bsAlert("success", "Handle Deleted Successfully!"));

                            }
                        },
                        error: function() {
                            console.log("Failed to delete handle.");

                        }
                    });
                } else {
                    console.log("no selected");
                }
            }
        });

    }

    function updateAssignedHandle() {

        var handleID = $("#handleIdAssigned").val();
        var userName = $("#userNameAssignedHAndle").val();
        var userEmail = $("#userEmailAssignedHandle").val();

        operateLadda("save_old", "start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/reserve_handle_crud.php",
            data: {
                handleID: handleID,
                userName: userName,
                userEmail: userEmail,
                action: "editHandleData"
            },
            success: function(user) {
                operateLadda("save_old", "stop");
                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {

                    $("#editHandleModalassigned").modal('hide');
                    $("#assignedHandledata").html(showLoadingDiv()).load("../../controller/settings/assignedHandle.php");

                    $("#errors").html(bsAlert("success", "Handle Data Updated Successfully!"));

                } else {
                    $("#errmsgedithandle").html("");
                    $("#errmsgedithandle").html(msg);
                }
            },
            error: function() {
                console.log("Failed to Update handle Data.");

            }
        });
    }


    function updateReservedHandle() {

        var handleID = $("#userHandleIdedit").val();
        var oldhandleID = $("#oldHandleID").val();
        operateLadda("update_old", "start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/reserve_handle_crud.php",
            data: {
                oldhandleID: oldhandleID,
                handleID: handleID,
                action: "editHandleName"
            },
            success: function(user) {
                operateLadda("update_old", "stop");
                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {


                    $("#editHandleModal").modal('hide');
                    $("#reservedHandledata").html(showLoadingDiv()).load("../../controller/settings/reserveHandle.php");


                    $("#errors").html(bsAlert("success", "Handle Name Updated Successfully!"));

                } else {
                    $("#errmsgupdateHAndle").html("");
                    $("#errmsgupdateHAndle").html(msg);
                }
            },
            error: function() {
                console.log("Failed to Update handle Name.");

            }
        });
    }

    function addHandletoProcess(handle) {
        $("#userHandleId").val("");
        $("#userHandleId").val(handle);
    }

    function editHandletoProcess(handle) {
        $("#userHandleIdedit").val("");
        $("#userHandleIdedit").val(handle);
        $("#oldHandleID").val("");
        $("#oldHandleID").val(handle);
    }



    function editAssignedHandletoProcess(handle, name, email) {
        $("#handleIdAssigned").val("");
        $("#handleIdAssigned").val(handle);

        $("#userNameAssignedHAndle").val("");
        $("#userNameAssignedHAndle").val(name);

        $("#userEmailAssignedHandle").val("");
        $("#userEmailAssignedHandle").val(email);
    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
