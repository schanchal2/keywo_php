<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

?>
<style type="text/css">
  /*  #slabListTable table > tbody > tr > td
    {
        padding:10px;
    }

    #slabListTable table > thead > tr > th
    {
        padding:12px;
    }*/

</style>
<main>

    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Settings</h1><span>Keyword Slab Settings</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div id="slabGenaration" class="nav-tabs-custom">
            <ul class="nav nav-pills" style="background: #97e5f8;" role="tablist">
                <?php
                if((in_array("write",$myp))) {

                    ?>

                    <li role="presentation" class="active"><a href="#SlabsList" aria-controls="" role="tab"
                                                              data-toggle="tab">Generate Slab</a></li>
                    <?php
                }
                ?>
                <li role="presentation"><a href="#GenerateSlab" id="myslablist" aria-controls="SlabsList" role="tab" data-toggle="tab">
                        Slabs List</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane " id="GenerateSlab">
                    <div id="slabListingsPriceWise">

                    </div>
                </div>
                <div role="tabpanel" class="tab-pane    <?php
                if((in_array("write",$myp))) {
                    echo 'active';
                }
                ?>" id="SlabsList">
                    <div class="row">
                        <div class="col-md-8 ">
                            <form class="form-horizontal" name="myFormSlab" id="myFormSlab" action="javascript:;">
                                <fieldset class="bg-lightBlue1">
                                    <div class="form-group">
                                        <label for="inputStartlimit" class="col-sm-3 control-label">Start limit of
                                            Slab</label>
                                        <div class="col-sm-9">
                                            <input min="0"  type="number" class="form-control" id="limitstart" name="limitstart"
                                                   placeholder="Enter Start limit">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEndlimit" class="col-sm-3 control-label">End limit of
                                            Slab</label>
                                        <div class="col-sm-9">
                                            <input min="0"  type="number" class="form-control" id="limitend" name="limitend"
                                                   placeholder="Enter End limit">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-3 control-label">Select Currency for
                                            Slab</label>
                                        <div class="col-sm-9">
                                            <div class="modal-form">
                                                <label for="game" class="dropdownOptions">
                                                    <select class="form-control" id="CurrencySlab" disabled>
                                                        <option value="ITD" selected>ITD</option>
                                                        <option value="USD">USD</option>
                                                        <option value="SGD">SGD</option>
                                                        <option value="BTC">BTC</option>
                                                    </select>
                                                </label>
                                            </div>
                                            <!-- <input type="password" class="form-control" id="inputPassword3" placeholder="USD"> -->
                                            <div class="input-group">
                                                <!-- styled-select -->
                                                <!-- <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span> -->
                                                <!-- <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> -->
                                                <!--        <select>
                                                    <option value="" disabled="" selected=""> USD</option>
                                                    <option> SGD </option>
                                                    <option> BTC </option>
                                                    <option> ITD </option>
                                                </select> -->
                                                <!--                                                 <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
 -->
                                                <!-- <select id="soflow">
                                                    <option>Selectssss an Option</option>
                                                    <option>Option 1</option>
                                                    <option>Optiosssn 2</option>
                                                </select> -->
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset class="bg-lightBlue2">
                                    <div class="form-group">
                                        <label for="inputPriceIn" class="col-sm-3 control-label">Price of Slab in
                                            ITD</label>
                                        <div class="col-sm-9">
                                            <input min="0"  type="number" step="" class="form-control" id="itd" name="itd"
                                                   placeholder="Enter price for Slab in ITD">

                                            <div class="">
                                                <br/>
                                                <b><span id="ErrorMsg" class=""></span></b>
                                            </div>

                                        </div>


                                    </div>
                                </fieldset>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" id="submitSlab"
                                                class="btn btn-default pull-right bg-lightBlue">Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <p>Click slab list tab to check last slab first slab and active slab in slab table. so, you
                                will find next end limit and start limit for Slab.</p>
                            <ul class="list-unstyled slabsLegend">
                                <li><span class="bg-lightBlue1"></span> Current Active Slab</li>
                                <li><span class="bg-green"></span> Last Slab</li>
                                <li><span class="bg-darkblue"></span> First Slab</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="keywordslabsettings">
            </div>
        </div>
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>

    $(document).ready(function () {
        <?php
        if((!in_array("write",$myp))) {
            ?>
        $("#myslablist").trigger('click');
        <?php
        }
        ?>
    });

    //showGIFDataDOTload()
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab
        switch (target) {

            case "#GenerateSlab":
                $("#ErrorMsg").html("");
                $("#slabListingsPriceWise").html(showLoadingDiv()).load("../../controller/settings/slabList.php");
                break;
        }
    });

    $("#submitSlab").click(function () {
        var formdata = new FormData($("#myFormSlab")[0]);

        $.ajax({
            type: "POST",
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            url: "../../controller/settings/updateSlab.php",
            data: formdata,
            success: function (user) {

                if (user["errCode"]) {
                    if (user["errCode"] == "-1") {
                        $("#ErrorMsg").html("");
                        $("#ErrorMsg").html(user["errMsg"]);
                        $("#ErrorMsg").attr("class", "text-success");

                        document.getElementById("myFormSlab").reset();

                    }
                    else {
                        $("#ErrorMsg").html("");
                        $("#ErrorMsg").html(user["errMsg"]);
                        $("#ErrorMsg").attr("class", "text-danger");

                    }

                }
            },
            error: function () {

                console.log("Error in inserting slab");

            }
        });
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
