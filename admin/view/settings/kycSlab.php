<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../model/settings/kyc_slab.php");

$slabResult = getKYCSlabSettingAdmin($connSearch);
$slabResult = $slabResult["errMsg"];
unset($slabResult[3]);

/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/

?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Admin Settings</h1><span>KYC Slab</span>

            </div>

            <br/>
            <style>


                table > thead > tr > th {
                    padding: 10px 40px;
                    resize: both;
                    overflow: auto;
                }

                input, button, select, textarea {

                    line-height: 18px;
                    width: 151px;
                }



                .table-bordered > thead > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > tfoot > tr > td {
                    color: #09708a;
                    border: 1px solid #1ba4ef;
                    padding: 4px;
                    margin: 3px;
                    border-radius: 0px;
                }



            </style>
            <div id="wrapper">
                <div id="errors"></div>
                <div class="row">
                    <?php
                    foreach ($slabResult as $slab) {
                        ?>
                        <form id="<?php echo $slab["slab_id"]; ?>formDataLatest" name="<?php echo $slab["slab_id"]; ?>formDataLatest">

                        <div class="col-xs-4">
                            <div id="userList" class="m-t-15">
                                <table class="table table-responsive table-bordered" style="border-color:#337ab7;">
                                    <thead class="header">
                                    <tr style="height:35px">
                                        <th colspan="2" style="text-align:left;font-size:17px; color:white"><?php echo $slab["kyc_level_name"]; ?></th>
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="background:#BDECF6;">
                                            <small><?php echo $slab["description"]; ?></small>
                                        </th>
                                    </tr>
                                    <tr>
                                        <?php
                                        if((in_array("write",$myp))) {

                                            ?>
                                            <th colspan="2" style="text-align:left; color:white;">Set Limit<a
                                                        class="pull-right" style="color:white"
                                                        id="<?php echo $slab["slab_id"]; ?>-saveEditButton"><i
                                                            style="padding-right: 2px;"
                                                            onclick="enableEdit('<?php echo $slab["slab_id"]; ?>')"
                                                            class="btn fa fa-pencil fa-lg"></i></a></th>
                                            <?php
                                        }else
                                        {
                                            ?>
                                            <th colspan="2" style="text-align:left; color:white;">Set Limit<a
                                                        class="pull-right" style="color:white"
                                                        id="<?php echo $slab["slab_id"]; ?>-saveEditButton"><i
                                                            style="padding-right: 2px;"></i></a></th>
                                        <?php
                                        }
                                        ?>
                                    </tr>
                                    </thead>
                                    <tbody style="background:#CAE9F2;">
                                    <tr>
                                        <td>Daily Send Fund Limit</td>
                                        <td><input min="0" name="sendFundLimit" id="<?php echo $slab["slab_id"]; ?>-sendFundLimit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled'style="border:none;text-align: center" value="<?php echo number_format($slab["send_fund_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Daily Receive Fund Limit</td>
                                        <td><input min="0" name="receiveFundLimit" id="<?php echo $slab["slab_id"]; ?>-receiveFundLimit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled'style="border:none;text-align: center" value="<?php echo number_format($slab["receive_fund_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Daily Withdrawal Limit</td>
                                        <td><input min="0" name="withdrawalFundLimit" id="<?php echo $slab["slab_id"]; ?>-withdrawalFundLimit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled'style="border:none;text-align: center" value="<?php echo number_format($slab["withdrawal_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Daily Interaction Limit</td>
                                        <td><input min="0" name="intractionLimit" id="<?php echo $slab["slab_id"]; ?>-intractionLimit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled'style="border:none;text-align: center" value="<?php echo $slab["intraction"]; ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Daily Deposit Limit</td>
                                        <td><input min="0" name="depositLimit" id="<?php echo $slab["slab_id"]; ?>-depositLimit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled' style="border:none;text-align: center" value="<?php echo number_format($slab["deposit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Yearly Send Fund Limit</td>
                                        <td><input min="0" name="yearly_send_fund_limit" id="<?php echo $slab["slab_id"]; ?>-yearly_send_fund_limit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled' style="border:none;text-align: center" value="<?php echo number_format($slab["yearly_send_fund_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Yearly Withdrawal Fund Limit</td>
                                        <td><input min="0" name="yearly_withdrawal_limit" id="<?php echo $slab["slab_id"]; ?>-yearly_withdrawal_limit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled' style="border:none;text-align: center" value="<?php echo number_format($slab["yearly_withdrawal_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Yearly Income Limit</td>
                                        <td><input min="0" name="yearly_income_limit" id="<?php echo $slab["slab_id"]; ?>-yearly_income_limit" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled' style="border:none;text-align: center" value="<?php echo number_format($slab["yearly_income_limit"],6); ?>"></td>
                                    </tr>

                                    <tr>
                                        <td>Daily Transaction Fees</td>
                                        <td><input min="0" name="transaction_fees" id="<?php echo $slab["slab_id"]; ?>-transaction_fees" type="number"  class='<?php echo $slab["slab_id"]; ?>-enableOnInput' disabled='disabled' style="border:none;text-align: center" value="<?php echo number_format($slab["transaction_fees"],6); ?>"></td>
                                    </tr>


                                    <tr>

                                        <td colspan="2"><div style="height:17px;"></div></td>

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        </form>
                        <?php
                    }
                    ?>


                </div>
            </div><!--wrapper closed-->
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

    <script type="text/javascript">

        function enableEdit(slab_id)
        {
            var elements=[1,2,3,4];


            for(var i = 0; i <= elements.length; i++){
                if(elements[i] == slab_id)
                {
                    $("."+slab_id+"-enableOnInput").prop('disabled', false);
                }else
                {
                    exchangeIconsUpdate(elements[i])
                }
            }


            $("#"+slab_id+"-saveEditButton").html("");

            var html='<i style="padding-right: 2px;" onclick=updateKycData("'+slab_id+'") class="btn fa fa-floppy-o fa-lg"></i>';
            $("#"+slab_id+"-saveEditButton").html(html);
        }


        function exchangeIconsUpdate(slab_id)
        {
            $("."+slab_id+"-enableOnInput").prop('disabled', true);
            $("#"+slab_id+"-saveEditButton").html("");

            var html='<i style="padding-right: 2px;" onclick=enableEdit("'+slab_id+'") class="btn fa fa-pencil fa-lg"></i>';
            $("#"+slab_id+"-saveEditButton").html(html);
        }

        function updateKycData(slab_id)
        {

            exchangeIconsUpdate(slab_id);
            var sendLimit=$("#"+slab_id+"-sendFundLimit").val();
            var receiveLimit=$("#"+slab_id+"-receiveFundLimit").val();
            var withdrawalLimit=$("#"+slab_id+"-withdrawalFundLimit").val();
            var intractionLimit=$("#"+slab_id+"-intractionLimit").val();
            var depositLimit=$("#"+slab_id+"-depositLimit").val();
            var yearly_send_fund_limit=$("#"+slab_id+"-yearly_send_fund_limit").val();
            var yearly_withdrawal_limit=$("#"+slab_id+"-yearly_withdrawal_limit").val();
            var yearly_income_limit=$("#"+slab_id+"-yearly_income_limit").val();
            var transaction_fees=$("#"+slab_id+"-transaction_fees").val();

            if(parseFloat(sendLimit) >= parseFloat(yearly_send_fund_limit))
            {
                $("#errors").html(bsAlert("danger","Update Failed ! Send Fund limit should be less than yearly Send Fund limit."));
                $("#"+slab_id+"-sendFundLimit").css("background-color","red");
                enableEdit(slab_id);
                return false;
            }else
            {
                $("#"+slab_id+"-sendFundLimit").css("background-color","");
            }

            if(parseFloat(withdrawalLimit) >= parseFloat(yearly_withdrawal_limit))
            {
                $("#errors").html(bsAlert("danger","Update Failed ! Withdrawal Fund limit should be less than yearly Withdrawal Fund limit."));
                $("#"+slab_id+"-withdrawalFundLimit").css("background-color","red");
                enableEdit(slab_id);
                return false;
            }else
            {
                $("#"+slab_id+"-withdrawalFundLimit").css("background-color","");
            }

            $.ajax({
                type: "POST",
                url: "../../controller/settings/CRUDKYCSlab.php",
                data: {operationType:"update",slabId: slab_id,sendLimit:sendLimit,receiveLimit:receiveLimit,withdrawalLimit:withdrawalLimit,intractionLimit:intractionLimit,depositLimit:depositLimit,yearly_send_fund_limit:yearly_send_fund_limit,yearly_withdrawal_limit:yearly_withdrawal_limit,yearly_income_limit:yearly_income_limit,transaction_fees:transaction_fees},
                dataType: "json",
                success: function (user) {


                    if (user["errCode"]) {
                        if (user["errCode"] == "-1") {
                                getSlabData(slab_id);
                                $("#errors").html(bsAlert("success",user["errMsg"]));


                        }
                        else {
                                $("#"+slab_id+"-sendFundLimit").css("background-color",rgb(235, 235, 228));
                                enableEdit(slab_id);
                                getSlabData(slab_id);
                            $("#errors").html(bsAlert("danger",user["errMsg"]));

                        }

                    }
                },
                error: function () {
                   console.log("ajax error");
                }
            });
        }



        function getSlabData(slab_id)
        {

            $.ajax({
                type: "POST",
                url: "../../controller/settings/CRUDKYCSlab.php",
                data: {operationType:"getSlabbyId",slabId: slab_id},
                dataType: "json",
                success: function (user) {
                    if (user["errCode"]) {
                        if (user["errCode"] == "-1") {
                            $("#"+slab_id+"-sendFundLimit").val("");
                            $("#"+slab_id+"-sendFundLimit").val(parseFloat(user["errMsg"][0].send_fund_limit).toFixed(6));

                            $("#"+slab_id+"-receiveFundLimit").val("");
                            $("#"+slab_id+"-receiveFundLimit").val(parseFloat(user["errMsg"][0].receive_fund_limit).toFixed(6));

                            $("#"+slab_id+"-withdrawalFundLimit").val("");
                            $("#"+slab_id+"-withdrawalFundLimit").val(parseFloat(user["errMsg"][0].withdrawal_limit).toFixed(6));

                            $("#"+slab_id+"-intractionLimit").val("");
                            $("#"+slab_id+"-intractionLimit").val(user["errMsg"][0].intraction);

                            $("#"+slab_id+"-depositLimit").val("");
                            $("#"+slab_id+"-depositLimit").val(parseFloat(user["errMsg"][0].deposit).toFixed(6));


                        }

                    }
                },
                error: function () {
                    console.log("ajax error");
                }
            });
        }
    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>