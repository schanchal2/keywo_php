<?php include "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">


    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">KYC</h1><span>Doc. Verification</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="">Registered Users(Nos.)</th>
                                <th class="">Level 3 Verified Users </th>
             
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="registeredusersData"> 0</td>
                                <td id="level3usersData"> 0</td>

                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>
                <div id="errors"></div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">

                                <div class="input-group styled-select" >

                                    <input style="width: 300px;z-index: 1" type="text" onkeyup="kycAdminData()" id="UserEmail" class="form-control" value='' placeholder="User Email">


                                </div>
                                

                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="kycModuleAdminAjax">
            </div>
    </main>
    </div>

    <!--modal to display editing data-->
    <div class="modal  madal--1  fade" id="kycDocumentShow" role="dialog">
        <div class="modal-dialog" style="width: 870px;">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Document Link</h4>
                </div>
                <div class="modal-body" style="background-color: #eeeeee;">
                </div>
            </div>
        </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>

    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>

    <script>

        $(document).ready(function () {
            kycAdminData();
            getLevel3UsersCount();
        });

        function kycAdminData() {

            var UserEmail = ($("#UserEmail").val());

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/KYC/kyc.php",
                data: {
                    UserEmail: UserEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#kycModuleAdminAjax").html("");
                    $("#kycModuleAdminAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        function kycDocDisplay(docDetail,email) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/KYC/kycDocument.php",
                data: {
                    docDetail: docDetail,
                    email: email
                },
                success: function (data) {
                    $("#kycDocumentShow .modal-body").html("");
                    $("#kycDocumentShow .modal-body").html(data);
                    $("#kycDocumentShow").modal('show');
                },
                error: function () {
                    alert("fail");
                }
            });
        }

        function actionOnKycDoc(docDetail, type) {

            if(type=="approved")
            {
                operateLadda("kycAccept_btn","start");
            }else {
                operateLadda("kycReject_btn","start");
            }
            $('#errMsg-kyc').hide();
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../controller/KYC/kycDocAction.php",
                data: {
                    docDetail: docDetail,
                    type:      type
                },
                success: function (data) {
                    if (data.errCode == -1) {
                        if(type=="approved")
                        {
                            operateLadda("kycAccept_btn","stop");
                            $("#errors").html(bsAlert("success","KYC Document Approved Successfully."));
                        }else{
                            operateLadda("kycReject_btn","stop");
                            $("#errors").html(bsAlert("success","KYC Document Rejected Successfully."));
                        }

                        getLevel3UsersCount();
                        $("#kycDocumentShow").modal('hide');
                        kycAdminData();
                    } else {
                        $('#errMsg-kyc').text('Something Went Wrong...!');
                        $('#errMsg-kyc').show();
                    }
                },
                beforeSend: function(data) {
                    $('.kyc-action').css('pointer-events', 'none');
                },
                complete: function(data) {
                    $('.kyc-action').css('pointer-events', 'auto');
                },
                error: function (data) {
                    alert("fail");
                }
            });
        }

        function getLevel3UsersCount()
        {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/KYC/kycUsersCount.php",
                data: {},
                success: function (data) {
                    $("#registeredusersData").html("");
                    $("#level3usersData").html("");

                    if(data["errCode"]==-1)
                    {
                        $("#registeredusersData").html(data["errMsg"]["registered_users"]);
                        $("#level3usersData").html(data["errMsg"]["kyc_level_3_users"]);
                    }else {
                        $("#registeredusersData").html("0");
                        $("#level3usersData").html("0");
                    }

                },
                error: function () {
                    console.log("fail");
                }
            });
        }
    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>