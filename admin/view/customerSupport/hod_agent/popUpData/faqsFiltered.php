<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$ticket_id=$_POST["ticket_id"];

?>

<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
    <div class="p-5 bg-darkblue clearfix">
        <div class="col-xs-5">

        </div>

        <div class="col-xs-7">
            <input class="form-control" placeholder="Search Ques in Knowledge Base."  type="text" onkeyup="getFAQs()" id="searchFAQs">
            <!--            <input type="text" class="form-control" placeholder=".col-xs-3">-->
        </div>
        <!--        <div class="pull-right"></div>-->
    </div>
    <div id="getFAQsData">

    </div>

</div>
<script>
    $(document).ready(function () {
        getFAQs();
    });

    function getFAQs() {
       var limit = 5;
        var searchFAQs = ($("#searchFAQs").val());
        console.log(searchFAQs)
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/customerSupport/hod/faqs.php",
            data: {
                faqKey: searchFAQs
            },
            success: function (data) {
                $("#getFAQsData").html("");
                $("#getFAQsData").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }

    function getFAQsList(id) {
        var limit = 5;
        $.ajax({
            type: "GET",
            dataType: "html",
            url: "../../../controller/customerSupport/hod/faqs.php",
            data: {
                id: id
            },
            success: function (data) {
                $("#getFAQsData").html("");
                $("#getFAQsData").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }


</script>