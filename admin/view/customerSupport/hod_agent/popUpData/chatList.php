<?php
session_start();
//include "../layout/header.php";

$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");



$tickectId    = $_GET['ticket_id'];
$ticketStatus = $_GET['status'];
$i            = 1;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
?>
<div class="" id = "message-user-details">
    <div class="chat-container bg-light-purple" id="userMessage">
        <div class="p-10" id="chatComments">
        </div>
    </div>
</div>
<input type="hidden" id="ticket-details" value ="<?php echo  $getTicketByID['ticket_id'];?>">
<input type="hidden" id="old_request_status" value="<?= $ticketStatus;?>">
<?php 

    $agentUserType = getTicketViewerAuth($_SESSION['admin_id'], $connAdmin);
    if (noError($agentUserType)) {
        $agentUserType = $agentUserType['errMsg'];
    } else {
        $agentUserType = 0;   
    }
    $escalationStatus = getTicketEscalationStatus($tickectId, $connKeywords);
    if (noError($escalationStatus)) {
        $escalationStatus = $escalationStatus['errMsg'];
    } else {
        $escalationStatus = 0;   
    }
    $TicketViewIsHod = getTicketViewerIsHod($_SESSION['admin_id'], $connAdmin);
    if (noError($TicketViewIsHod)) {
        $TicketViewIsHod = $TicketViewIsHod['errMsg'];
    } else {
        $TicketViewIsHod = 0;   
    }

    if ($ticketStatus != 'closed' && ($agentUserType > 0 || $escalationStatus > 0) && $TicketViewIsHod <= 0) {

?>
        <div class="chat-input position-relative">
            <textarea  name="shout_message" id="shout_message" class="autoExpand"></textarea>
            <i class="fa fa-paper-plane-o chatSubmit" id="submitForChat" aria-hidden="true" ajax_call = "true" ></i>
        </div>
<?php
    }
?>
<script>
    // To create new comment/message on press of enter key
//    $('#shout_message').keypress(function(e) {
//      if (e.which == 13) {
//        $('#submitForChat').click();
//      }
//    });

    $("#shout_message").keypress(function(e){
        if (e.keyCode == 13 && !e.shiftKey)
        {
            $('#submitForChat').click();
        }
    });
    // To create new comment/message on press icone to enter
    $("#submitForChat").click(function(){
        var comments=$("#shout_message").val();
        var lastmsgs=$("#message-user-details").find(".chat_msg_div").last().attr("id");
        if(typeof lastmsgs!= "undefined") {
            lastmsgs = lastmsgs.split("_");
            var lastMSGid = lastmsgs[1];
        }else {
            var lastMSGid = 0;
        }
        var userType="agent";
        var ticketID='<?= $tickectId; ?>';
        var emailAgent='<?= $_SESSION["admin_id"]; ?>';
        var ajaxCall = $('#submitForChat').attr('ajax_call');
        var commentCheck = $.trim(comments);

        if (ajaxCall == "true" && commentCheck != "") {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "popUpData/chatSubmitController.php",
                data: {
                    comments: comments, userType: userType,emailAgent:emailAgent,ticketID:ticketID,lastMSGid:lastMSGid
                },
                beforeSend: function(){
                    $('#submitForChat').attr('ajax_call','false');
                },
                success: function (data) {
                    var responseMSG = $('<div />').html(data);
                    var responseDivMSG = responseMSG.find('#MessageErrorCode').text();
                    if (responseDivMSG == 12) {
                        var msg = "Ticket Is Closed";
                        showErrorMsg(msg,12);
                    } else if (responseDivMSG == 13) {
                        var msg = "Ticket Is Revoked By HOD..!";
                        showErrorMsg(msg,13);
                    } else {
                        var todayDate = todayDateData();
                        var dateId = $('#userMessage').find('#'+todayDate).length;
                        if (dateId == 1) {
                            var response = $('<div />').html(data);
                            var responseDiv = response.find('#'+todayDate);
                            $(responseDiv).remove();
                        } else {
                            var response =data;
                        }
                        $("#chatComments").append(response);
                        var elem = document.getElementById('userMessage');
                        $("#shout_message").val('');
                        elem.scrollTop = elem.scrollHeight;
                        var lastStatus=$("#old_request_status").val();
                        var newStatus=$("#new_request_status").val();

                        if(lastStatus!=newStatus) {
                            update_request_status(ticketID);//diffrent  behaviour for agent and hod
                            $("#old_request_status").val(newStatus);
                        }
                        var currentTimeInterval = $('#ticketTimerValue').text();
                        if (currentTimeInterval == "00:00:00" && ticketTimer == '') {
                            ticketTimer = setInterval(startTime, 1000);
                        }
                    }
                },
                complete: function(data){
                $('#submitForChat').attr('ajax_call','true');
              },
                error: function () {
                    console.log("fail");
                }

            });
        }
    });

    function showErrorMsg(msg,code) {
        bootbox.confirm({
            message: "<h4>"+msg+"</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    console.log("reload page");
                    $('.modal').modal('hide');
                    getNewTickets();
                } else {
                    console.log("no selected");
                }
            }
        });
    }

    function todayDateData() {
        var now = new Date();
        var day = ("0" + now.getDate()).slice(-2);
        var month = ("0" + (now.getMonth() + 1)).slice(-2);
        var today = (day)+ "-" + (month) + "-" +now.getFullYear();
        return today;
    }



    $(document).ready(function(){
        $("#chatComments").load("popUpData/chatSubmitController.php?ticket_id=<?= $tickectId; ?>");
//        var elem = document.getElementById('userMessage');
//        elem.scrollTop = elem.scrollHeight;
        $(".chat-container").animate({scrollTop: 9999},500);
    });

    //$('#supprt_ticket_modal').on('shown.bs.modal', function () {

            var updateChatWindows = setInterval(updateChatWindows, <?= $chatRefreshTime; ?>);// milliseconds
            function updateChatWindows() {
           var ststus=$(".chat_msg_div").is(":visible");
           if(ststus==true) {
                updateChatWindow();
            }
        }
    ///});



    function updateChatWindow() {
        //var lastMSGid=$("#lastMsgIDHidden").val();//issue
        var lastmsgs=$("#message-user-details").find(".chat_msg_div").last().attr("id");
        if(typeof lastmsgs!= "undefined") {
            lastmsgs = lastmsgs.split("_");
            var lastMSGid = lastmsgs[1];
        }else {
            var lastMSGid = 0;
        }
        var ajaxCall = $('#submitForChat').attr('ajax_call');
        var ticketID='<?= $tickectId; ?>';
        var lstMsg=checklastmsg(ticketID);
        if(lastMSGid!=lstMsg && ajaxCall == "true") {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "popUpData/chatSubmitController.php",
                data: {
                   lastMSGid:lastMSGid,ticketID:ticketID
                },
                beforeSend: function(){

                },
                success: function (data) {              
                        if (data != "") {
                            var todayDate = todayDateData();
                            var dateId = $('#userMessage').find('#'+todayDate).length;
                            if (dateId == 1) {
                                var response = $('<div />').html(data);
                                var responseDiv = response.find('#'+todayDate);
                                $(responseDiv).remove();
                            } else {
                                var response =data;
                            }
                            $("#chatComments").append(response);
                            var elem = document.getElementById('userMessage');
                            elem.scrollTop = elem.scrollHeight;
                        } else {
                            console.log("nothing to update");
                        }                    
                },
                error: function () {
                    console.log("fail");
                }
            });
        }else {
            console.log("nothing to update");
        }
    }

    function checklastmsg(ticketID)
    {
        var lastmsg="";
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/customerSupport/hod/check_last_msg.php",
            async:false,
            data: {
                ticketID:ticketID,operation:"checkMsg"
            },
            success: function (data) {
                lastmsg=data["errMsg"];
            }
        });
        return lastmsg;
    }

    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });

</script>