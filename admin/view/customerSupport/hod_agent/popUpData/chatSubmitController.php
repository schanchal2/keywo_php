<?php
session_start();
//include "../layout/header.php";

$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");
require_once("{$docrootpath}/helpers/stringHelper.php");
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$commentText=isset($_POST["comments"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["comments"])):"";
$userType=isset($_POST["userType"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["userType"])):"";
$emailID=isset($_POST["emailAgent"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["emailAgent"])):"";
$tickectId=isset($_POST["ticketID"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"])):"";
$lastMSGid=isset($_POST["lastMSGid"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["lastMSGid"])):"";
if (empty($tickectId)) {
    $tickectId=isset($_GET['ticket_id'])?cleanQueryParameter($connKeywords, cleanXSS($_GET['ticket_id'])):"";
}

if($commentText!="") {
    $result = updateCommentText($commentText, $emailID, $tickectId, $userType, $connKeywords);

    if (noError($result)) {
        $startTimer = setTicketTimerStart($tickectId, $connKeywords);
    } else if ($result['errCode'] == 12) {
?>
        <div id = "MessageErrorCode" style = "display:none;">12</div>
<?php
        exit;
    } else if ($result['errCode'] == 13 ) {
?>
        <div id = "MessageErrorCode" style = "display:none;">13</div>
<?php
        exit;
    }
}


if(isset($lastMSGid))
{
    $getComment=getCommentText($tickectId, $connKeywords, $lastMSGid)["errMsg"][0];
    if(!empty($getComment))
    {
    $endmsgId=end($getComment)["id"];

    $i = 1;
    for($k= 0; $k<count($getComment); $k++) {
        $currentDateTime = $getComment[$k]['msg_time'];
        $newDateTime = uDateTime('h:i A', $currentDateTime);
        $date2 = uDateTime("Y-m-d H:i:s",date("Y-m-d H:i:s"));
        $dateForMonthConv1 = uDateTime('d-m-Y', $currentDateTime);
        $dateForYesterday = uDateTime('d-m-Y',date('d-m-Y', strtotime("-1 day")));
        $dateForMonthConv2 = uDateTime('d-m-Y',date("d-m-Y"));
        $j = 1;
        if ($dateForMonthConv1 == $dateForMonthConv2) {
            $displayDate = "Today";
            $j + 1;
        } elseif ($dateForMonthConv1 == $dateForYesterday) {
            $displayDate = "Yesterday";
        } else {
            $displayDate = $dateForMonthConv1;
        }

        $dateDis = $dateForMonthConv1;
        $prevDate = uDateTime('d-m-Y', $getComment[$k - 1]['msg_time']);
        ?>

        <?php if ($prevDate != $dateForMonthConv1) { ?>
            <div class="text-center" id="<?php echo $dateForMonthConv1; ?>">
                <div class="chat-date bg-light-purple">
                    <?php echo $displayDate; ?>
                </div>
            </div>
        <?php } ?>

        <!-- left chat ends -->

        <!-- right chat begins -->
        <?php if ($getComment[$k]['user_type'] == "agent") { ?>
            <div class="p-y-10 chat-right clearfix">
                <div class="p-10 arrow_box right-arrow bg-white pull-right border-radius-10  wrd-wrp_brk-wrd">
                    <div class="chat_msg_div" id="agent_<?php echo $getComment[$k]['id']; ?>">
                        <?php echo $getComment[$k]['msg_content']; ?>
                    </div>
                    <div class="text-right text-grayscale-80 chat-time">
                        <span class="">  <?php echo $newDateTime; ?></span>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if ($getComment[$k]['user_type'] == "customer") { ?>
            <div class="p-y-10 chat-left clearfix">
                <div class="p-10 arrow_box left-arrow bg-lightBlue1 pull-left border-radius-10  wrd-wrp_brk-wrd">
                    <div class="chat_msg_div" id="customer_<?php echo $getComment[$k]['id']; ?>">
                        <?php echo $getComment[$k]['msg_content']; ?>
                    </div>
                    <div class="text-right text-grayscale-80 chat-time">
                        <span class="">  <?php echo $newDateTime; ?></span>
                    </div>
                </div>
            </div>



        <?php } ?>


        <?php
    }
    ?>

    <?php
}
}

?>


