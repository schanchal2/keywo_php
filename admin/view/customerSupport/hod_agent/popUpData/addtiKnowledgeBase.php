<?php
session_start();
//include "../layout/header.php";

$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$tickectId           = $_GET['ticket_id'];
$getCategory          = getAllCategory($connKeywords);

if(noError($getCategory)){
    $getCategory                              = $getCategory["errMsg"];

}

?>
<div style="margin: 50px">
<form>
    <div class="form-group">
        <label for="categosry_faq">Category:</label>
        <select class="form-control" id="categosry_faq">
            <?php
            foreach ($getCategory as $value) {
                echo $value["name"];
                echo "<option data-live-search=\"true\" data-tokens='" . $value['name'] . "' id='" . $value['id'] . "' value='" . $value['name'] . "'>" . ucwords($value['name']) . "</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group">
        <label for="question_faq">Question:</label>
        <textarea class="form-control noresize" rows="3" id="question_faq"></textarea>
    </div>
    <div class="form-group">
        <label for="answer_faq">Answer:</label>
        <textarea class="form-control noresize" rows="3" id="answer_faq"></textarea>
    </div>
    <button type="button" id="submitFAQs" class="btn btn-primary m-b-10 pull-right" onclick="submitCatModel()" >Submit</button>
</form>

    <span id="errorsDanger" style="color:red"></span>
    <span id="errorsSuccess" style="color:green"></span>
</div>

<script>
    function submitCatModel() {
        $("#errorsDanger").html("");
        $("#errorsSuccess").html("");
        var category_faq     = ($("#categosry_faq").val());
        var question_faq = ($("#question_faq").val());
        var answer_faq   = ($("#answer_faq").val());
        if(answer_faq && question_faq) {
            operateLadda("submitFAQs","start");
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../../controller/customerSupport/hod/FAQsSubmit.php",
                data: {
                    category_faq: category_faq,
                    question_faq: question_faq,
                    answer_faq: answer_faq
                },
                success: function (data) {
                    operateLadda("submitFAQs","stop");
                    console.log(data['errCode']);
                    if (data['errCode'] == -1) {
                        $("#errorsSuccess").html("FAQ Added successfully");
                        //$("#categosry_faq").val("");
                        $("#question_faq").val("");
                        $("#answer_faq").val("");

                    }
                },
                error: function (data) {
                    console.log(data['errCode']);

                }
            });
        }else{

            $("#errorsDanger").html("Please Enter All Field");

        }
    }
</script>
