<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");


require_once("{$docrootpath}helpers/stringHelper.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


$ticket_id=$_POST["ticket_id"];

$getTicket          = getTicketDetailsByTicketId($ticket_id,$connKeywords);

if(noError($getTicket)){
    $getTicket = $getTicket["errMsg"];
    
}
$tk_request_status=$getTicket["request_status"];

$resultCountAdmin=checkAdminGroup($_SESSION["agent_id"], $connAdmin)["errMsg"][0]["count"];

?>

<form>
    <div class="modal-content QueryTicket-content">
    <?php //printArr($getTicket); ?>
    
        <div class="modal-header QueryTicket-header">
            <div class="row">
                <div class="col-xs-3">
                    <div id = "popUpChatStatus" class="QueryTicket-TicketID pull-left" ticketStatus = "<?php echo $getTicket['request_status']; ?>">
                        Ticket ID :
                        <span>  <?= $getTicket["ticket_id"]; ?></span>
                    </div>


                </div>
                <div class="col-xs-3 ">
                    <div class="QueryTicket-UserEmailID">
                        Ticket Time:
                    <?php
                        $getTicketResponseStart = getTicketTimerStart($getTicket["ticket_id"], $connKeywords);
                        if (noError($getTicketResponseStart)) {
                            $getTicketResponseStart = $getTicketResponseStart['errMsg'][0];
                            $startDate = strtotime($getTicketResponseStart['response_start']);
                            $closeDate = strtotime($getTicketResponseStart['closed_on']);

                            if ($startDate == '') {
                                $timerValue = '00:00:00';
                            } else {
                                if ($closeDate == '-62170005200') {
                                    $currentDate = date('Y-m-d h:i:s');
                                    $timerValue = strtotime($currentDate) - $startDate;
                                    $timerValue = gmdate("H:i:s", $timerValue);                                
                                } else {                   
                                    $timerValue = $closeDate - $startDate;
                                    $timerValue = gmdate("H:i:s", $timerValue);
                                }
                            }
                        }
                    ?>

                        <span id = "ticketTimerValue" class="text-darkblue"><?= $timerValue; ?></span>


                    </div>
                </div>
                <div class="col-xs-4">

                        User e-mail ID : <span class="text-blue"><?= $getTicket['reg_email'] ; ?></span>
                </div>
                <div class="col-xs-2">

                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    &nbsp;     <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-minus"></i> </button>-->

                </div>
            </div>

        </div>

        <div class="modal-body QueryTicket-body">
            <div class="row">
                <div class="col-xs-10">
                    <div>
                        <!-- Tab panes -->
                        <div class="tab-content QueryTicket-QA">
                            <div role="tabpanel" class="tab-pane active" id="chatData">

                                <div  class="resetTableDatapopup">
                                    <div class="card social-card m-b-15 ">
                                        <div class="col-xs-5">
                                            <div class="row">
                                                <div class="form-group clearfix">
                                                    
                                                    <div class="col-sm-12">
                                                        
                                                    <?php
                                                        $ticketRequestType = $getTicket['request_type'];
                                                        if (!empty($getTicket['keyword_purchased'])) {
                                                            $keyWords = ltrim($getTicket['keyword_purchased'],'[');
                                                            $keyWords = rtrim($keyWords,']');
                                                        }

                                                        if (!empty($getTicket['paypal_address']) && empty($getTicket['bitcoin_address']) && empty($getTicket['transaction_hash'])) {
                                                            $payMethod = 'PayPal';
                                                            $payAddress = $getTicket['paypal_address'];
                                                        } elseif (empty($getTicket['paypal_address']) && !empty($getTicket['bitcoin_address']) && empty($getTicket['transaction_hash'])) {
                                                            $payMethod = 'BitGo';
                                                            $payAddress = $getTicket['bitcoin_address'];
                                                        } elseif (empty($getTicket['paypal_address']) && empty($getTicket['bitcoin_address']) && !empty($getTicket['transaction_hash'])) {
                                                            $payMethod = 'Wallet';
                                                            $payAddress = $getTicket['transaction_hash'];
                                                        }
                                                        switch ($ticketRequestType) {
                                                        
                                                            case 'error in purchasing keyword':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";
                                                                $textToDesplay .=  "Keywords Purchased : ". $keyWords ."\n\n";
                                                                $textToDesplay .= "Payment date : ". $getTicket['date_time'] ."\n\n";
                                                                $textToDesplay .= "Payment mode  : ". $payMethod ."\n\n";
                                                                $textToDesplay .= "Payment Address  : ". $payAddress ."\n\n";
                                                                $textToDesplay .= "Amount : ". $getTicket['amount_paid'] ."\n\n";
                                                                break;
                                                            case 'trading related issues':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";
                                                                $textToDesplay .= "Keywords : ". $keyWords ."\n\n";
                                                                $textToDesplay .= "Trade Type : ". $getTicket['trade_type'] ."\n\n";
                                                                $textToDesplay .= "Received Email : ". $getTicket['isrecivedTrade_email'] ."\n\n";
                                                                $textToDesplay .= "Amount : ". $getTicket['amount_paid'] ."\n\n";
                                                                break;
                                                            case 'issue with it purchase':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";
                                                                $textToDesplay .= "Deposited Amount : ". $getTicket['amount_paid'] ."\n\n";
                                                                $textToDesplay .=  "Purchase Date : ". $getTicket['date_time'] ."\n\n";

                                                                if (!empty($getTicket['paypal_address']) && empty($getTicket['bitcoin_address'])) {
                                                                    $payMethod = 'PayPal';
                                                                    $payAddress = $getTicket['paypal_address'];
                                                                } elseif (empty($getTicket['paypal_address']) && !empty($getTicket['bitcoin_address'])) {
                                                                    $payMethod = 'BiyGo';
                                                                    $payAddress = $getTicket['bitcoin_address'];
                                                                }
                                                                $textToDesplay .= "Payment mode  : ". $payMethod ."\n\n";
                                                                $textToDesplay .= "Payment Address  : ". $payAddress ."\n\n";
                                                                if (!empty($getTicket['transaction_hash'])) {
                                                                    $textToDesplay .= "Transaction Has : ". $getTicket['transaction_hash'] ."\n\n";
                                                                }
                                                                break;
                                                            case 'issues with it cashout':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";
                                                                $textToDesplay .=  "Keywords Purchased : ". $keyWords ."\n\n";  
                                                                $textToDesplay .= "Amount : ". $getTicket['amount_paid'] ."\n\n";   
                                                                $textToDesplay .= "Payment mode  : ". $payMethod ."\n\n";
                                                                $textToDesplay .= "Payment Address  : ". $payAddress ."\n\n";      
                                                                break;
                                                            case 'issues with sending itd':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";       
                                                                $textToDesplay .= "Registered Email : ". $getTicket['reg_email'] ."\n\n"; 
                                                                $textToDesplay .= "Amount : ". $getTicket['amount_paid'] ."\n\n";   
                                                                $textToDesplay .=  "Send Date : ". $getTicket['date_time'] ."\n\n";       
                                                                $textToDesplay .= "Receiver Email : ". $getTicket['rec_sender_email'] ."\n\n";
                                                                break;
                                                            case 'issues with receiving itd':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";       
                                                                $textToDesplay .= "Registered Email : ". $getTicket['reg_email'] ."\n\n"; 
                                                                $textToDesplay .= "Amount : ". $getTicket['amount_paid'] ."\n\n";   
                                                                $textToDesplay .=  "Send Date : ". $getTicket['date_time'] ."\n\n";       
                                                                $textToDesplay .= "Receiver Email : ". $getTicket['rec_sender_email'] ."\n\n";
                                                                break;
                                                            case 'other_general':
                                                                $textToDesplay = "\nQuery : ".$getTicket['comments']."\n\n";  
                                                                break;
                                                        }
                                                    ?>
                                                        <textarea class="form-control QueryTicket-UserQuery" name="QueryTicket-UserQuery" disabled="disabled">
                                                            <?php
                                                                echo cleanDisplayParameter($connKeywords, $textToDesplay);
                                                            ?>    
                                                        </textarea>
                                                        
                                                        <div class="textareaInfo"><span><?php $dateData= explode(" ",uDateTime("Y-m-d h:i A",$getTicket['requestTime'])) ;
                                                            echo $dateData[0];
                                                            ?></span> <span class="text-blue"><?php echo $dateData[1]." ".$dateData[2];?> </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                                $agentUserType = getTicketViewerAuth($_SESSION['admin_id'], $connAdmin);
                                                if (noError($agentUserType)) {
                                                    $agentUserType = $agentUserType['errMsg'];
                                                } else {
                                                    $agentUserType = 0;   
                                                }
                                                $escalationStatus = getTicketEscalationStatus($getTicket["ticket_id"], $connKeywords);
                                                if (noError($escalationStatus)) {
                                                    $escalationStatus = $escalationStatus['errMsg'];
                                                } else {
                                                    $escalationStatus = 0;   
                                                }
                                                $TicketViewIsHod = getTicketViewerIsHod($_SESSION['admin_id'], $connAdmin);
                                                if (noError($TicketViewIsHod)) {
                                                    $TicketViewIsHod = $TicketViewIsHod['errMsg'];
                                                } else {
                                                    $TicketViewIsHod = 0;   
                                                }
                                                if ($tk_request_status !== 'closed' && ($agentUserType > 0 || $escalationStatus > 0) && $TicketViewIsHod <= 0) {
                                            ?>
                                                    <div class="row">
                                                        <div class="form-group vertical-center">
                                                            <label class="col-sm-5 col-sm-offset-2 text-right control-label">Send as status:</label>
                                                            <div class="col-sm-5">
                                                                <div class="row">
                                                                    <label class="dropdownOptions m-x-15">
                                                                        <select id="new_request_status" class="form-control">


                                                                            <option value="open" <?php if(strtolower($tk_request_status)=="open"){ echo 'selected'; } ?>>Open</option>
                                                                            <option value="pending" <?php if(strtolower($tk_request_status)=="pending"){ echo 'selected'; } ?>>Pending</option>
                                                                            <option value="closed" <?php if(strtolower($tk_request_status)=="closed"){ echo 'selected'; } ?>>Closed</option>

                                                                        </select>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            <?php
                                                }
                                            ?>
                                        </div>
                                        <div id="FilteredChat" class="col-xs-7">
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="FAQ">
                                <div id="faqsFiltered" class="resetTableDatapopup">

                                </div>
                            </div>
                                <div role="tabpanel" class="tab-pane" id="CannedResponse">
                                    <div id="addToKnb" class="resetTableDatapopup">

                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="CreatServiceRequest">
                                    <div id="cr_service_req" class="resetTableDatapopup">

                                    </div>
                                </div>
                                <!--<div role="tabpanel" class="tab-pane" id="RaiseIternalTicket">
                                    <div id="raiseIT" class="resetTableDatapopup">
                                    </div>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs  nav-stacked QueryTicket-QA " role="tablist">
                            <li role="presentation" class="active"><a href="#chatData" aria-controls="Query" role="tab" data-toggle="tab">Chat</a></li>
                            <li role="presentation" class=""><a href="#FAQ" aria-controls="FAQ" role="tab" data-toggle="tab">Knowledge Based</a></li>
                            <li role="presentation"><a href="#CannedResponse" aria-controls="CannedResponse" role="tab" data-toggle="tab">Add To Knowledge Based</a></li>
                            <!--<li role="presentation">
                                <a href="#CreatServiceRequest" aria-controls="CreatServiceRequest" role="tab" data-toggle="tab">
                                    Service Request</a>
                            </li>-->
                            <!--<li role="presentation"><a href="#RaiseIternalTicket" aria-controls="RaiseIternalTicket" role="tab" data-toggle="tab">Raise Iternal Ticket</a></li>-->

                        </ul>
                    </div>
                </div>
            </div>
            <div class="modal-footer QueryTicket-footer">
                <div class="row">
                    <div class="col-xs-<?php if($resultCountAdmin>0){echo 7;}else{echo 9; } ?>">
                        <div class="QueryTicket-Tagging">
                            <div class="row">

                                <div class="form-group ">
                                    <label for="" class="col-sm-1 control-label"><span>Tag: </span></label>
                                    <div class="col-sm-4">
                                        <input type="email" value='<?= ($getTicket['request_type']); ?>'
                                               class="form-control" id="inputEmail3" placeholder="" readonly disabled>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php


                    if($resultCountAdmin>0) {

                        if (basename($_SERVER['HTTP_REFERER']) != "cshod.php") {
                            if ($tk_request_status != "closed"){
                                ?>
                                <div class="col-xs-5">
                                <button type="button" onclick="escalateTicketToHod('<?= $getTicket["ticket_id"]; ?>')"
                            class="btn btn-primary bg-darkBlue pull-right closeTicketFaq">Escalate to HOD
                            </button>
                            </div>
                            <?php
                        }
                    }
                    }
                    ?>
                    <div class="col-xs-1">
                      <div class="QueryTicket-folloUpdate">
                            <!--Follo up Date : <span> 22/12/2017 </span>&nbsp; <i class="fa fa-calendar"></i>-->
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>

</form>


<script>

//    $( "#closeTicketModalData" ).click(function() {
//        $('#supprt_ticket_modal').modal('hide');
//        $('body').removeClass('modal-open');
//        $('.modal-backdrop').remove();
//    });
    var ticketTimer = '';
    $(document).ready(function(){
        $("button").click(function(){
            $("p").clone().appendTo("body");
        });

        $("#FilteredChat").html("");
        $("#FilteredChat").load("popUpData/chatList.php?ticket_id=<?php echo $ticket_id; ?>&status=<?php echo $tk_request_status; ?>");

        var tk_status='<?= $tk_request_status; ?>';
        if(tk_status=="new")
        {
            makeTicketOpen('<?php echo $ticket_id; ?>');
        }
       
 

    });


    function checkTime(i) {
        i = 1 + i;  // add zero in front of numbers < 10
        if (i < 10){
            i = "0"+i;
        }
        return i;
    }

    function startTime() {
        var currentTime = $('#ticketTimerValue').text();
        var timer = currentTime.split(':');
        var h = parseInt(timer[0]);
        var m = parseInt(timer[1]);
        var s = parseInt(timer[2]);
        // console.log('hMS : '+h+' & '+m+' & '+s);
        if (s < 60) {
            s = checkTime(s);  
            if (m < 10){
                m = "0"+m;
            }
            if (h < 10){
                h = "0"+h;
            }          
        } else if (s == 60) {
            s = "00";
            if (m < 60) {
                m = checkTime(m);
                if (h < 10){
                    h = "0"+h;
                }                
            } else if (m == 60) {
                m = "00";
                h = checkTime(h);
                if (s < 10){
                    s = "0"+s;
                }
            }
        }
        var currentTimeString = h + ":" + m + ":" + s;
        $("#ticketTimerValue").text(currentTimeString);      
    }

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab
        $("#FilteredChat").html("");
        switch (target)
        {
            case "#FAQ":
                $("#faqsFiltered").load("popUpData/faqsFiltered.php?ticket_id=<?php echo $ticket_id; ?>");
                break;

            case "#chatData":
                $("#FilteredChat").load("popUpData/chatList.php?ticket_id=<?php echo $ticket_id; ?>&status=<?php echo $tk_request_status; ?>");
                break;

            case "#RaiseIternalTicket":
                $("#raiseIT").load("popUpData/raiseInternalTickets.php?ticket_id=<?php echo $ticket_id; ?>");
                break;

            case "#CannedResponse":
                $("#addToKnb").load("popUpData/addtiKnowledgeBase.php?ticket_id=<?php echo $ticket_id; ?>");
                break;

            case "#CreatServiceRequest":
                $("#cr_service_req").load("popUpData/serviceRequest.php?ticket_id=<?php echo $ticket_id; ?>");
                break;

        }
    });

function makeTicketOpen(ticketID)
{
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controller/customerSupport/hod/crud_support_ticket.php",
        data: {
            newStatus: "open",
            ticketID: ticketID,
            operation:"changeTicketStatus"
        },
        beforeSend: function () {

        },
        success: function (data) {
            $("#last_request_status").val(data["errMsg"]);
        },
        error: function () {
            console.log("fail");
        }

    });

}



</script>
