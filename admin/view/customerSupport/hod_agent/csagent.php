<?php include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/keywords/common_keyword.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/customer/customerModel.php");
require_once "../../../model/geolocation/geoLocation.php";
?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Customer Support</h1><span>Query Ticket-Agents</span><!--default new filter-->
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
             <div class="row">
                <div class="col-xs-12">
                    <div class="table--lightBlue2">
                        <table class="table text-center table-responsive m-b-10">
                            <thead>
                                <tr>
                                    <th>Support Queries</th>
                                </tr>
                            </thead>
                        </table>
                    </div><!--UserList closed-->
                    <div class="table--blue">
                        <div id="agentTicketAnalyticsData">

                        </div>
                    </div>
                </div>
            </div>
            <div class="row m-t-15">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">


                            <div class="form-group">
                                <label class="sr-only" for="">Ticket ID</label>
                                <input type="text" onkeyup="getNewTickets()" class="form-control" id="ticketID" placeholder="Ticket ID">
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="">Username</label>
                                <input type="email" onkeyup="getNewTickets()" class="form-control" id="userEmail" placeholder="User Email">
                            </div>




                            <div class="input-group styled-select">
                                <?php

                                $result=getAllCountry($connSearch)["errMsg"];

                                ?>
                                <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select">

                                    <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                    <?php
                                    foreach($result as $value){
                                        echo $value["name"];
                                        echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                    }
                                    ?>

                                </select>

                            </div>



                            <div class="input-group styled-select pull-right">
                                <select id="ticketStatus" class="col-145">
                                    <option value="new" selected>New</option>
                                    <option value="open">Open</option>
                                    <option value="pending">Pending</option>
                                    <option value="closed">Closed</option>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                            </div>

                            </br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div id="newRegisteredUsers">
                                        <div id="getNewTicketsData">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </main>

    <!--modal to display editing data-->
    <div class="modal QueryTicket fade" id="supprt_ticket_modal"  aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog QueryTicket-dialog">
            <div id="popup_modal_updated_data">

            </div>
        </div>
    </div>

    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script>
        $("#countryToSearch").chosen();
        // To open Model for edit post
        function showModalChat(ticket_id) {
            $.ajax({
                type: 'POST',
                url: '../../customerSupport/hod_agent/popUpChat.php',
                dataType: 'html',
                data:{ticket_id:ticket_id},
                success: function(data) {
                    $('#popup_modal_updated_data').html("");
                    $('#popup_modal_updated_data').html(data);
                    $('#supprt_ticket_modal').modal('show');
                },
                error: function(data) {
                    console.log('Error in post Update');
                }
            });
        }

        $( document ).ready(function() {
            getNewTickets();
            getAgentAnalyticsCS('<?= $_SESSION["agent_id"]; ?>');
        });



        function getNewTickets()
        {
            var ticketID=($("#ticketID").val());
            var userEmailId=($("#userEmail").val());
            var assignedAgent="";
            var country=($("#countryToSearch").val());
            var ticketStatus=($("#ticketStatus").val());

            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=5;
            }

            var creationTime="";


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/customerSupport/agent/agent_tickets.php",
                data: {
                    limit:limit,userEmailId:userEmailId,ticketID:ticketID,assignedAgent:assignedAgent,country:country,ticketStatus:ticketStatus,creationTime:creationTime
                },
                success: function (data) {
                    $("#getNewTicketsData").html("");
                    $("#getNewTicketsData").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });

        }


        function getNewTicketsPage(pageNo,userEmailId,ticketID,assignedAgent,country,ticketStatus,creationTime,limit)
        {

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/customerSupport/agent/agent_tickets.php",
                data: {
                    limit:limit,userEmailId:userEmailId,ticketID:ticketID,page:pageNo,assignedAgent:assignedAgent,country:country,ticketStatus:ticketStatus,creationTime:creationTime
                },
                success: function (data) {
                    $("#getNewTicketsData").html("");
                    $("#getNewTicketsData").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });

        }



        $("#countryToSearch").change(function(){
            getNewTickets();
        });

        $("#ticketStatus").change(function(){
            getNewTickets();
        });

        function escalateTicketToHod(ticket)
        {
            bootbox.confirm({
                message: "<h4>Are you sure to escalate this ticket to HOD?</h4>",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    if(result==true)
                    {

                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "../../../controller/customerSupport/hod/crud_support_ticket.php",
                            data: {
                                ticketID: ticket,
                                operation:"escalateTicket"
                            },
                            beforeSend: function () {

                            },
                            success: function (data) {
                                bootbox.alert("<h4>"+data["errMsg"]+"</h4>", function () {
                                    getNewTickets();
                                    getAgentAnalyticsCS('<?= $_SESSION["agent_id"];?>');
                                    $("#supprt_ticket_modal").modal("hide");
                                });
                            },
                            error: function () {
                                console.log("fail");
                            }

                        });
                    }else {
                        console.log("no selected");
                    }
                }
            });
        }


        function getAgentAnalyticsCS(agentid)
        {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/customerSupport/hod/agent_support_ticket_analytics.php",
                data: {
                    agent_id: agentid,
                },
                beforeSend: function () {

                },
                success: function (data) {
                    $("#agentTicketAnalyticsData").html("");
                    $("#agentTicketAnalyticsData").html(data);
                },
                error: function () {
                    console.log("fail");
                }

            });

        }

        function update_request_status(ticketID)
        {
            var lastStatus=$("#old_request_status").val();
            var newStatus=$("#new_request_status").val();

            if(lastStatus!=newStatus) {
                $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: "../../../controller/customerSupport/hod/crud_support_ticket.php",
                    data: {
                        newStatus: newStatus,
                        ticketID: ticketID,
                        operation:"changeTicketStatus"
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        getAgentAnalyticsCS('<?= $_SESSION["agent_id"]; ?>');
                        $("#last_request_status").val(data["errMsg"]);
                        if(newStatus=="closed")
                        {
                            $("#ticketStatus").val(newStatus).change();
                            $("#supprt_ticket_modal").modal("hide");
                            //getNewTickets();

                        }
                    },
                    error: function () {
                        console.log("fail");
                    }

                });
            }
        }


        $("#supprt_ticket_modal").on('show.bs.modal', function() {
            var ticketStatus = $("#popUpChatStatus").attr('ticketStatus');
            if (ticketStatus != 'closed'){
                ticketStatus = '';
                var currentTimeInterval = $('#ticketTimerValue').text();
                if (currentTimeInterval != "00:00:00" && ticketTimer == '') {
                    ticketTimer = setInterval(startTime, 1000);
                }
            }
        });

        $("#supprt_ticket_modal").on('hide.bs.modal', function() {
            var ticketStatus = $("#popUpChatStatus").attr('ticketStatus');
            if (ticketStatus != 'closed'){
                if (ticketTimer != '') {
                    ticketStatus = '';
                    clearInterval(ticketTimer);
                }
            }
        }); 
    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>