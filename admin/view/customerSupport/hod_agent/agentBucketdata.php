<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once "../../../model/geolocation/geoLocation.php";

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


if (isset($_GET["tab_type"])) {
  $tab_type = $_GET["tab_type"];
    switch ($tab_type) {
        case "all_agent_bucket":
            $ticketStatus = "";
            $escalated_to_hod = "";
            break;
        case "escalated_to_hod":
            $ticketStatus = "open";
            $escalated_to_hod = 1;
            break;
        case "epending_hod":
            $ticketStatus = "pending";
            $escalated_to_hod = 1;
            break;
        case "closed_hod":
            $ticketStatus = "closed";
            $escalated_to_hod = 1;
            break;
    }

} else {
    $tab_type="all_agent_bucket";
    $ticketStatus = "";
    $escalated_to_hod = "";
}

?>

<div class="row m-t-15">
    <div class="col-lg-12">
        <div id="filter" class="sign-upverification">
            <form action="" method="POST" class="form-inline" role="form" name="filter">


                <div class="form-group">
                    <label class="sr-only" for="">Ticket ID</label>
                    <input type="text" onkeyup="getNewTickets()" class="form-control" id="ticketID"
                           placeholder="Ticket ID">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="">Username</label>
                    <input type="email" onkeyup="getNewTickets()" class="form-control" id="userEmail"
                           placeholder="User Email">
                </div>

                <div class="form-group">
                    <label class="sr-only" for="">Assign To</label>
                    <input type="email" onkeyup="getNewTickets()" class="form-control" id="assignedAgent"
                           placeholder="Assign To Agent">
                </div>


                <div class="input-group styled-select">
                    <?php

                    $result = getAllCountry($connSearch)["errMsg"];

                    ?>
                    <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select">

                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                        <?php
                        foreach ($result as $value) {
                            echo $value["name"];
                            echo "<option data-live-search=\"true\" data-tokens='" . $value['name'] . "' id='" . $value['id'] . "' value='" . $value['name'] . "'>" . $value['name'] . "</option>";
                        }
                        ?>

                    </select>

                </div>



                <input type="hidden" value="<?= $ticketStatus; ?>" id="ticketStatus">

                <?php
                if($tab_type=="all_agent_bucket") {
                    ?>
                    <button type="button" name="Reassign" value="Reassign" class="btn btn-primary pull-right" onclick = "reasignTicketModal();">Reassign
                    </button>
                    <?php
                }
                ?>
                </br>
                <div class="row">
                    <div class="col-xs-12">
                        <div id="newRegisteredUsers">
                            <div id="getNewTicketsData">
                            </div>
                        </div>
                    </div>
                </div>

            </form>

        </div>

    </div>
</div>
<script>
    $("#countryToSearch").chosen();
    // To open Model for edit post
    function showModalChat(ticket_id) {
        $.ajax({
            type: 'POST',
            url: '../../customerSupport/hod_agent/popUpChat.php',
            dataType: 'html',
            data: {ticket_id: ticket_id},
            success: function (data) {
                $('#popup_modal_updated_data').html("");
                $('#popup_modal_updated_data').html(data);
                $('#supprt_ticket_modal').modal('show');
            },
            error: function (data) {
                console.log('Error in post Update');
            }
        });
    }

    $(document).ready(function () {
        getNewTickets("<?= $escalated_to_hod; ?>");
    });


    function getNewTickets(escalation_status) {
        var ticketID = ($("#ticketID").val());
        var userEmailId = ($("#userEmail").val());
        var assignedAgent = ($("#assignedAgent").val());
        var country = ($("#countryToSearch").val());
        var ticketStatus = ($("#ticketStatus").val());

        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 5;
        }

        var creationTime = "";


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/customerSupport/hod/hod_tickets.php",
            data: {
                limit: limit,
                userEmailId: userEmailId,
                ticketID: ticketID,
                assignedAgent: assignedAgent,
                country: country,
                ticketStatus: ticketStatus,
                creationTime: creationTime,
                escalation_status: escalation_status
            },
            success: function (data) {
                $("#getNewTicketsData").html("");
                $("#getNewTicketsData").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });

    }


    function getNewTicketsPage(pageNo, userEmailId, ticketID, assignedAgent, country, ticketStatus, creationTime, limit, escalation_status) {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/customerSupport/hod/hod_tickets.php",
            data: {
                limit: limit,
                userEmailId: userEmailId,
                ticketID: ticketID,
                page: pageNo,
                assignedAgent: assignedAgent,
                country: country,
                ticketStatus: ticketStatus,
                creationTime: creationTime,
                escalation_status: escalation_status
            },
            success: function (data) {
                $("#getNewTicketsData").html("");
                $("#getNewTicketsData").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }


    $("#countryToSearch").change(function () {
        getNewTickets();
    });

    function update_request_status(ticketID)
    {
        var lastStatus=$("#old_request_status").val();
        var newStatus=$("#new_request_status").val();

        if(lastStatus!=newStatus) {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/customerSupport/hod/crud_support_ticket.php",
                data: {
                    newStatus: newStatus,
                    ticketID: ticketID,
                    operation:"changeTicketStatus"
                },
                beforeSend: function () {

                },
                success: function (data) {
                    $("#last_request_status").val(data["errMsg"]);
                    if(newStatus=="closed")
                    {
                        $("#supprt_ticket_modal").modal("hide");
                        getNewTickets("<?= $escalated_to_hod; ?>");
                    }
                },
                error: function () {
                    console.log("fail");
                }

            });
        }
    }

</script>