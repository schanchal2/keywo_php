<?php
session_start();
 include "../../../layout/header.php"; 

 ?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Customer Support</h1><span>Query Ticket-HOD</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            
            <style>
    .bdrR
    {
       border-right:  solid 1px white;
    }
    #filteredTabs .active .bg-darkblue {
        background-color: #35ccf1 !important;
    }

    #filteredTabs{
        margin-top: 15px;
    }


    #filter form[name="filter"] .countryToSearch button{
        background: none;
        border: 1px solid #ccc;
        text-indent: 1em;
        color: #b2b2b2;
        height: 34px;
        width: 268px;
        display: table-cell;
        -webkit-writing-mode: horizontal-tb;
        background: transparent;
        -webkit-appearance: none;
        -moz-appearance: none; }

    .user_checkbox,
    .checkboxStyle {
        display: none; }

    .checkboxStyle  + label,
    .user_checkbox + label {
        padding: 9px;
        display: inline-block;
        position: relative;
        margin-top: 7px; }

    .checkboxStyle  + label {
        padding-bottom: 0; }

    .user_checkbox + label:after,
    .checkboxStyle  + label:after {
        content: "\f096";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }

    .checkboxStyle + label:after {
        color: #fff; }

    .user_checkbox:checked + label:after,
    .checkboxStyle:checked + label:after {
        content: "\f14a";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }

    .checkboxStyle:checked + label:after {
        color: #fff; }
</style>

<div id="filteredTabs" class="nav-tabs-custom ">
    <div class="btn-group btn-group-justified " role="group" aria-label="...">
        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active bdrR"><a class="bg-darkblue text-white" href="#AgentBucket" aria-controls="AgentBucket" role="tab" data-toggle="tab">Agent Bucket</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#EscalationHODTickets" aria-controls="EscalationHODTickets" role="tab" data-toggle="tab">Escalation[HOD Tickets]</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#PendingBucketHOD" aria-controls="PendingBucketHOD" role="tab" data-toggle="tab">Pending Bucket-HOD</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#ClosedHOD" aria-controls="ClosedHOD" role="tab" data-toggle="tab">Closed(HOD)</a></li>
        </ul>
    </div>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="AgentBucket">

                <div id="newTopSearchUsersFilter">

                </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="EscalationHODTickets">

                <div id="newTopKeywordUsersFilter">

                </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="PendingBucketHOD">

                <div id="newTopAppUsersFilter">

                </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="SearchAffEarners">

                <div id="newTopSearchAffUsersFilter">

                </div>

        </div>
        <div role="tabpanel" class="tab-pane" id="KeywordAffEarners">

                <div id="newTopKeywordAffUsersFilter">

                </div>

        </div>
    </div>
</div>

<div id="userRefferalAnalatics">
    <div class="row" style="margin-top: 15px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="topPostEarnersnewDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>

                     <div class="input-group styled-select">
                        <select id="" style="width:125px">
                            <option value="" disabled selected>Ticket ID</option>
                            <option value="text"> Text</option>
                            <option value="display"> Display</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                     </div>

                     <div class="input-group styled-select">
                        <select id="" style="width:145px">
                            <option value="" disabled selected>Query Type</option>
                            <option value="text"> Text</option>
                            <option value="display"> Display</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                     </div>

                     <div class="input-group styled-select">
                        <select id="" style="width:145px">
                            <option value="" disabled selected>User Name</option>
                            <option value="text"> Text</option>
                            <option value="display"> Display</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                     </div>

                     <div class="input-group styled-select">
                       
                        <select id="countryToSearch" style="width: 210px;">
                            <option value="" disabled selected>Country</option>
                            <option value="india" >India</option>
                            <option value="usa" >USA</option>
                            <option value="uk">UK</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>

                    <div class="input-group styled-select">
                        <select id="" style="width:145px">
                            <option value="" disabled selected>Assign To</option>
                            <option value="text"> Text</option>
                            <option value="display"> Display</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                     </div>

                     <div class="input-group styled-select">
                        <select id="" style="width:145px">
                            <option value="" disabled selected>New</option>
                            <option value="old">old</option>
                            <option value="existing">Existing</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                     </div>

                     <button type="button" name="Reassign" value="Reassign" class="btn btn-primary">Reassign</button>
</br></br>
                </form>
                     <div class="row">
                         <div class="col-xs-12">
                             <div id="userList" class="m-t-15">
                                 <table class="table text-center table-responsive">
                                     <thead>
                                         <tr>
                                            <th><input id="select_all" name="" type="checkbox">
                                                <label for="select_all" class="fa"></label>
                                            </th>
                                            <th>Ticket ID</th>
                                            <th>Timestamp</th>
                                            <th>Query Type</th>
                                            <th>Username</th>
                                            <th>Country</th>
                                            <th>No. of Intractions</th>
                                            <th>Assigned(Name)</th>
                                            <th>Assigned Timestamp</th>
                                            <th>User Live Status</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                        <td>
                                            <input class="user_checkbox" id="user_checkbox" name="user_checkbox[]" value="" type="checkbox">
                                            <label for="user_checkbox" class="fa"></label>
                                         </td>
                                         <td>KT-01</td>
                                         <td></td>
                                         <td>Lerem ipsum</td>
                                         <td>Lerem ipsum</td>
                                         <td>India</td>
                                         <td>15</td>
                                         <td>Lerem CS</td>
                                         <td></td>
                                         <td><span class="fa fa-circle userOffline"></span></td>
                                     </tbody>
                                 </table>
                             </div>
                         </div>
                     </div>




        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>

    <script type="text/javascript">

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>