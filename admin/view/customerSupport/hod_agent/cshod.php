<?php
session_start();
include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/keywords/common_keyword.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/customer/customerModel.php");


?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Customer Support</h1><span>Query Ticket-HOD</span>
                <!--default all agent bucket -->
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div id="filteredTabs" class="nav-tabs-custom ">
                <div class="btn-group btn-group-justified " role="group" aria-label="...">
                    <ul class="nav nav-tabs nav-justified" role="tablist">
                        <li role="presentation" class="active bdrR"><a class=" text-white" href="#AgentBucket" aria-controls="AgentBucket" role="tab" data-toggle="tab">All Agent Bucket</a></li>
                        <li role="presentation" class="bdrR"><a class=" text-white" href="#EscalationHODTickets" aria-controls="EscalationHODTickets" role="tab" data-toggle="tab">Escalation[ Open HOD Tickets]</a></li>
                        <li role="presentation" class="bdrR"><a class=" text-white" href="#PendingBucketHOD" aria-controls="PendingBucketHOD" role="tab" data-toggle="tab">Pending Bucket-HOD</a></li>
                        <li role="presentation" class="bdrR"><a class=" text-white" href="#ClosedHOD" aria-controls="ClosedHOD" role="tab" data-toggle="tab">Closed(HOD)</a></li>
                    </ul>
                </div>
                <div class="row">
                    <div id="errors" class="col-xs-12 m-t-10"></div> 
                </div> 
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="AgentBucket">
                        <div id="agentBucketsData" class="resetTableData">
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="EscalationHODTickets">
                        <div id="esclatedHodBucketsData" class="resetTableData">
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="PendingBucketHOD">
                        <div id="pendingHodBucketsData" class="resetTableData">
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="ClosedHOD">
                        <div id="cloasedHodBucketsData" class="resetTableData">
                        </div>
                    </div>
                </div>
            </div>
    </main>
    <!--modal to display editing data-->
    <div class="modal QueryTicket fade" id="supprt_ticket_modal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog QueryTicket-dialog">
            <div id="popup_modal_updated_data">
            </div>
        </div>
    </div>
    <!--modal to display editing data-->
    <div class="modal QueryTicket fade" id="" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog QueryTicket-dialog">
            <div>
                <div id="Reasign_data_append">
                    <!-- <form>
                    <div class="form-group"> -->
                    <label class="sr-only" for="">Username</label>
                    <input type="email" onkeyup="" class="" id="userEmail" placeholder="Agent Email">
                    <!-- </div>
                </form> -->
                </div>
            </div>
        </div>
    </div>
    <!--modal to display editing data-->
    <div class="modal madal--1 fade" id="ReasignTicket" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="width: 870px;">


            <div class="modal-content">
            <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Assign to agent</h4>
                </div>

          <!--       <div class="clearfix closebutton-absolute text-right">
                    <i class="fa fa-times close-button close-dialog" data-dismiss="modal" style="font-size: 25px; color: #ddd;"></i>
                </div> -->


                <div class="modal-body" style="">
                    <form>
                        <div class="form-group">
                            <label class="sr-only" for="">Username</label>
                            <input type="email" onkeyup="loadEmailDetailsOnPopUp();" class="form-control" id="agentEmailId" placeholder="Agent Email">
                        </div>
                    </form>
                    <div id="ReasignTicketEmailData">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $("#errors").html('');
        $(".resetTableData").html("");
        $("#agentBucketsData").load("agentBucketdata.php");
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        $("#errors").html('');
        var target = $(e.target).attr("href"); // activated tab
        $(".resetTableData").html("");
        switch (target) {
            case "#AgentBucket":
                $("#agentBucketsData").html(showLoadingDiv()).load("agentBucketdata.php?tab_type=all_agent_bucket");
                break;

            case "#EscalationHODTickets":
                $("#esclatedHodBucketsData").html(showLoadingDiv()).load("agentBucketdata.php?tab_type=escalated_to_hod");
                break;

            case "#PendingBucketHOD":
                $("#pendingHodBucketsData").html(showLoadingDiv()).load("agentBucketdata.php?tab_type=epending_hod");
                break;

            case "#ClosedHOD":
                $("#cloasedHodBucketsData").html(showLoadingDiv()).load("agentBucketdata.php?tab_type=closed_hod");
                break;

        }
    });

    function getAllTicketIds() {
        var tags = document.getElementsByName('user_checkbox[]');
        var tickets = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {
                tickets.push(tags[i].value);
            }
        }
        return tickets;
    }

    function reasignTicketModal() {
        var tickets = getAllTicketIds();
        if (tickets.length > 0) {
            $('#ReasignTicket').modal('show');
            loadEmailDetailsOnPopUp('0');
        }
    }

    function loadEmailDetailsOnPopUp(pageNo) {
        $("#errors").html('');
        var email = $('#agentEmailId').val();
        var limit = ($("#LimitedResultPopup").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 10;
        }

        if (((typeof pageNo) == "undefined") || pageNo == "") {
            var pageNo = $('#hiddenpagePopup').val();
        }

        var agentsTicketArray = new Array();  

        $("#getNewTicketsData .highlight .assigned_to_agent").each(function() {
            if($.inArray($( this ).text(), agentsTicketArray) === -1) agentsTicketArray.push($( this ).text());
        }); 
        console.log("agentsTicketArray :" + agentsTicketArray)
        // pageNo = 1;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/customerSupport/hod/reassignTicketData.php",
            data: {
                limit:             limit,
                email:             email,
                page:              pageNo,
                agentsTicketArray: agentsTicketArray
            },
            success: function(data) {
                $("#ReasignTicketEmailData").html("");
                $("#ReasignTicketEmailData").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });
    }

    $("#ReasignTicket").on('hide.bs.modal', function() {
        $('#agentEmailId').val('');
        $('#ReasignTicketEmailData').html('');
    });




    function reassignTicketsToAgent(agentId) {
        $("#errors").html('');
        var tickets = getAllTicketIds();

        if (tickets.length > 0) {
            var agentEmail = $('#tcktAssignedTo' + agentId).attr('agent_email');
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../../controller/customerSupport/hod/assignTicketToAgent.php",
                async: false,
                data: {
                    tickets: tickets,
                    agentEmail: agentEmail,
                    agentId: agentId
                },
                success: function(data) {
                    if (data.errCode == -1) {
                        $("#errors").html('');
                        $("#errors").html(bsAlert("success","Ticket Re-assigned Successfully..!"));
                        $('#ReasignTicket').modal('hide');
                        $("#agentBucketsData").load("agentBucketdata.php?tab_type=all_agent_bucket");
                    } else {
                        $("#errors").html('');
                        $("#errors").html(bsAlert("danger","Ticket Re-assigned Failed..!"));
                    }
                },
                beforeSend: function(data) {
                    $('.tckt_assigning_btn').css('pointer-event', 'none');
                    $('.tckt_assigning_btn').removeClass('btn-primary');
                    $('.tckt_assigning_btn').addClass('btn-danger');
                    $('#tcktAssignedTo' + agentId).removeClass('btn-danger');
                    $('#tcktAssignedTo' + agentId).addClass('btn-success');
                },
                complete: function(data) {
                    $('.tckt_assigning_btn').css('pointer-event', 'cursor');
                    $('.tckt_assigning_btn').removeClass('btn-success');
                    $('.tckt_assigning_btn').removeClass('btn-danger');
                    $('.tckt_assigning_btn').addClass('btn-primary');
                },
                error: function() {
                    console.log("fail");
                    alert('failed');
                }
            });
        }
    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
