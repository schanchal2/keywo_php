<?php
include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Knowledge Based</h1><span></span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="row">
                <div class="col-xs-2">
                     <div class="input-group">
                        <button class="form-control bg-blue border-none text-white">Log in</button>
                            <span class="btn input-group-addon bg-lightblue text-white"><i class="fa fa-pencil"></i></span>
                    </div>
                </div>
                 <div class="col-xs-2">
                    <div class="input-group">
                        <button class="form-control bg-blue border-none text-white">Search</button>
                            <span class="btn input-group-addon bg-lightblue text-white"><i class="fa fa-pencil white"></i></span>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="input-group">
                        <button class="form-control bg-blue border-none text-white">Wallet</button>
                            <span class="btn input-group-addon bg-lightblue text-white"><i class="fa fa-pencil"></i></span>
                    </div>
                </div>
                <div class="col-xs-2">
                    <div class="input-group">
                        <button class="form-control bg-blue border-none text-white">Social</button>
                            <span class="btn input-group-addon bg-lightblue text-white"><i class="fa fa-pencil"></i></span>
                    </div>
                </div>

                <div class="col-xs-2">
                    <div class="input-group">
                        <button class="form-control bg-blue border-none text-white">Miscellaneous</button>
                            <span class="btn input-group-addon bg-lightblue text-white"><i class="fa fa-pencil"></i></span>
                    </div>
                </div>


                <div class="col-xs-1 col-xs-offset-1">
                    <button class="btn btn-block bg-blue border-none text-white right">Add</button>
                </div>
            </div>
            <div class="m-t-15"></div>
            <div class="row">
                <div class="form-group">
                    <div class="col-xs-3 col-xs-offset-6">
                        <select class="text-blue form-control border-blue">
                            <option value="" selected class="text-blue">Log in</option>
                            <option value="" ></option>
                            <option value="" ></option>
                            <option value=""></option>
                        </select>
                    </div>
                    <div class="col-xs-3">
                        <select class="text-blue form-control border-blue">
                            <option value="" selected class="text-blue">Sub-categories</option>
                            <option value="" ></option>
                            <option value="" ></option>
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>
            
   
            <div class="row m-t-20">
                <div class="col-xs-12">
                    <div class="table--lightBlue2">
                        <table class="table text-center table-responsive">
                            <thead>
                                <tr>
                                    <th class="width10">Sr. No</th>
                                    <th class="width25">Queries</th>
                                     <th class="width25">Responses</th>
                                     <th class="width20">Email Id</th>
                                     <th class="width20 ">Action Button</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td >
                                    <div class="scrollbar" id="scroll">
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text. Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>

                                <tr>
                                    <td>1</td>
                                    <td >
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>
                                       <div class="noresize text-left scrollBar">Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.Lorem Ipsum is not simply random text.Countrary to popular belief,Lorem Ipsum is not simply random text.</div>
                                    </td>
                                    <td>abc@gmail.com</td>
                                    <td>
                                        <i class="fa fa-pencil" aria-hidden="true"></i> &nbsp;&nbsp;
                                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                                    </td>
                                </tr>












                                  
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          


    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script type="text/javascript">


    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>