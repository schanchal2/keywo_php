<?php
include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/customer/customerModel.php");
$connSearch = createDBConnection("dbkeywords");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];
$getCategory = getAllCategory($connSearch);
if (noError($getCategory)) {
    $getCategory = $getCategory['errMsg'];
} else {
    $getCategory = "Error To Load";
}

?>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Knowledge Based</h1>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="row ">
                <!-- <div class="col-xs-12"> -->
                <div class="">
                    <div class="pull-right col" style="z-index: 1;">
                        <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#addFAQ" id="addNewUser">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
                <!-- </div> -->
                <!--     <a class="btn btn-success pull-right" id="addNewUser" data-toggle="modal" data-target="#addFAQ" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">
                    Add New Record <span class="fa fa-plus-circle"></span>
                </a> -->
                <?php //printArr($getCategory);?>
                <div class="pull-right ">
                    <label class="dropdownOptions">
                        <select id="knowledgeFaqStatus" class="form-control p-r-20" onchange="LoadFAQList();">
                            <option value="1" selected class="">Approved</option>
                            <option value="0" class="">Pending</option>
                        </select>
                    </label>
                </div>
                <div class="pull-right m-r-15 ">
                    <label class="dropdownOptions">
                        <select id="knowledgeCategory" class="form-control" onchange="LoadFAQList();">
                            <?php 
                            foreach ($getCategory as $key => $value) {                                
                        ?>
                            <option value="<?php echo $value['name'];?>" <?php if ($key==0 ){?>selected
                                <?php }?> class="">
                                <?php echo ucwords($value['name']);?>
                            </option>
                            <?php
                            }
                        ?>
                        </select>
                    </label>
                </div>
            </div>
            <div class="m-t-15"></div>
            <div class="m-t-20"></div>
            <div class="row">
                <div class="col-xs-12">
                    <div id="faq-append-div" class="table--lightBlue2">
                    </div>
                </div>
            </div>
    </main>
    </div>
    <!--modal to display editing data-->
    <div class="modal madal--1 fade" id="addFAQ" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" style="font-weight: bold"> Add New Recored</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <form class="clearfix">
                            <div class="form-group">
                                <input type="hidden" name="faq-id" id="faq-id" value="">
                                <label for="categosry_faq">Category:</label>
                                <select class="form-control" id="categosry_faq">
                                    <?php
                                    foreach ($getCategory as $value) {
                                        echo "<option data-live-search=\"true\" data-tokens='" . $value['name'] . "' id='" . $value['id'] . "' value='" . $value['name'] . "'>" . ucwords($value['name']) . "</option>";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="question_faq">Question:</label>
                                <textarea class="form-control noresize" rows="3" name="question_faq" id="question_faq"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="answer_faq">Answer:</label>
                                <textarea class="form-control noresize" rows="3" name="answer_faq" id="answer_faq"></textarea>
                            </div>
                            <div class="form-group " id="add_faq_error" style="color:red;"></div>
                            <button id="FAQSubmit" type="button" class="btn btn-sm btn-success border-default-border ajaxhide pull-right" onclick="submitCatModel();">Update</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script type="text/javascript">
    $(document).ready(function() {
        LoadFAQList();
    });

    $("#addFAQ").on('hide.bs.modal', function() {
        $('#faq-id').val('');
        $('#question_faq').val('');
        $('#answer_faq').val('');
        $('#add_faq_error').text('');
    });

    function LoadFAQList(category) {
        if (!category) {
            var category = $('#knowledgeCategory').val();
        }
        var faqStatus = $('#knowledgeFaqStatus').val();
        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 10;
        }

        $.ajax({
            type: "POST",
            dataType: "HTML",
            url: "../../../controller/customerSupport/faq/knowledge_Faq_table.php",
            data: {
                category: category,
                limit: limit,
                faqStatus: faqStatus
            },
            success: function(data) {
                $('#faq-append-div').html(data);
            },
            beforeSend: function(data) {

            },
            complete: function(data) {

            },
            error: function(data) {
                alert('Failed');
            }

        });
    }

    function getFaqPages(pageNo, category, limit) {
        var faqStatus = $('#knowledgeFaqStatus').val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/customerSupport/faq/knowledge_Faq_table.php",
            data: {
                limit: limit,
                category: category,
                page: pageNo,
                faqStatus: faqStatus
            },
            success: function(data) {
                $("#faq-append-div").html("");
                $("#faq-append-div").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });
    }

    function submitCatModel() {
        $('#add_faq_error').text('');
        var id = $("#addFAQ #faq-id").val();
        var category_faq = $("#addFAQ #categosry_faq").val();
        var question_faq = $("#addFAQ #question_faq").val();
        var answer_faq = $("#addFAQ #answer_faq").val();
        if (category_faq != '' & question_faq != '') {
            operateLadda("FAQSubmit","start");
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../../controller/customerSupport/hod/FAQsSubmit.php",
                data: {
                    category_faq: category_faq,
                    question_faq: question_faq,
                    answer_faq: answer_faq,
                    id: id
                },
                success: function(data) {
                    console.log(data);
                    $("#addFAQ").modal("hide");
                    $('#knowledgeCategory').val(category_faq);
                    if (id) {
                        loadKnowledgePageAfterAction()
                    } else {
                        LoadFAQList(category_faq);
                    }
                    operateLadda("FAQSubmit","stop");
                },
                beforeSend: function(data) {
                    $('#addFAQ #FAQSubmit').css('pointer-events', 'none');
                },
                complete: function(data) {
                    $('#addFAQ #FAQSubmit').css('pointer-events', 'auto');
                },
                error: function(data) {
                    alert('Failed');
                }

            });
        } else {
            $('#add_faq_error').text('All Fields Are Mandetory...!');
        }
    }

    function deleteFaq(id) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "../../../controller/customerSupport/faq/deleteFaq.php",
            data: {
                id: id
            },
            success: function(data) {
                if (data.errCode == -1) {
                    loadKnowledgePageAfterAction();
                } else {
                    alert('Error In Delete');
                }
            },
            beforeSend: function(data) {

            },
            complete: function(data) {

            },
            error: function(data) {
                alert('Failed');
            }
        });
    }

    function loadKnowledgePageAfterAction() {
        var pageNo = $('#hiddenpage').val();
        var limit = ($("#LimitedResult").val());
        var category = $('#knowledgeCategory').val();
        getFaqPages(pageNo, category, limit);
    }
  
    function EditFaq(id, category, que, ans) {
        var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}
         que = krDencodeEntities(Base64.decode(que));
         ans = krDencodeEntities(Base64.decode(ans));
        $('#faq-id').val(id);
        $('#categosry_faq').val(category);
        $('#question_faq').val(que);
        $('#answer_faq').val(ans);
        $("#addFAQ").modal("show");
    }

    function approveFaq(id) {
        $.ajax({
            type: "POST",
            dataType: "JSON",
            url: "../../../controller/customerSupport/faq/updateFaqStatus.php",
            data: {
                id: id
            },
            success: function(data) {
                if (data.errCode == -1) {
                    loadKnowledgePageAfterAction();
                } else {
                    alert('Error In Delete');
                }
            },
            beforeSend: function(data) {

            },
            complete: function(data) {

            },
            error: function(data) {
                alert('Failed');
            }
        });
    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
