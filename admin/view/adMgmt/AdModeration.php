<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Ad Management</h1><span>Ad-Moderation</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <div class="row">
                    <div class="col-md-12">
                        <div id="userRefferalAnalatics">
                            <input type="text" hidden>
                            <table class="table  text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center text-white">Ad's Created(Nos.)</th>
                                        <th class="text-center text-white">Approved(Nos.)</th>
                                        <th class="text-center text-white">Rejected(Nos.)</th>
                                        <th class="text-center text-white"><i class="fa fa-calendar" id="Admgnt_DateRange"></i></font></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="Ads_Created">15</td>
                                        <td id="Approved">20</td>
                                        <td id="Rejected">15</td>
                                        <td id="Admgnt_DateRange"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">
                            <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i></span>
                                            <?php
                                            $date=date("Y-m-d");
                                            ?>
                                            <input style="width: 180px" type="text" id="addModerationSpentDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                            
                            

                            <div class="input-group">
                                <input style="width:110px";type="text" class="form-control" id="userId" placeholder="User ID">
                                <span class="input-group-addon" onclick="getAdModeration()"><i class="fa fa-search"></i></span>
                            </div>

                             <div class="input-group">
                                <input style="width:135px"; type="text" class="form-control" id="campaignName" placeholder="Campaign Name">
                                <span class="input-group-addon" onclick="getAdModeration()"><i class="fa fa-search"></i></span>
                            </div>

                             <div class="input-group">
                                <input style="width:120px"; type="text" class="form-control" id="actionBy" placeholder="Actioned By">
                                <span class="input-group-addon" onclick="getAdModeration()"><i class="fa fa-search"></i></span>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
            <div id="moderationDetailsAjax">

            </div>

        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $( document ).ready(function() {
            getAdModeration();
        });

        function getAdModeration()
        {

            var refferaldatevalue=($("#addModerationSpentDate").val());
            var datedata=refferaldatevalue.split("to");
            var fromdate=(datedata[0]).trim();
            var todate=(datedata[1]).trim();

            var userId=($("#userId").val());
            var campaignName=($("#campaignName").val());

            var actionBy=($("#actionBy").val());


            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/adMgmt/adModertion.php",
                data: {
                    fromdate: fromdate, todate: todate,limit:limit,userId:userId,campaignName:campaignName,listtype:"first",actionBy:actionBy
                },
                success: function (data) {
                    $("#moderationDetailsAjax").html("");
                    $("#moderationDetailsAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#addModerationSpentDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function(event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function(event, obj) {

            getAdModeration();
        });


        $("#LimitedResult").change(function(){
            getAdModeration();
        });






    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>