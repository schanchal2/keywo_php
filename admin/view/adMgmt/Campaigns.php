<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Ad Management</h1><span>Campaigns</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
                <div class="row">
                    <div class="col-md-12">
                        <div id="userRefferalAnalatics">
                            <input type="text" hidden>
                            <table class="table  text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center text-white">Ad-Compaigns(Nos.)</th>
                                        <th class="text-center text-white">Total Active(Nos.)</th>
                                        <th class="text-center text-white">Total Paused(Nos.)</th>
                                        <th class="text-center text-white">Total Archived(Nos.)</th>
                                        <th class="text-center text-white"><i class="fa fa-calendar" id="campaign_DateRange"></i></font></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="Ad_Compaigns">15</td>
                                        <td id="Total_act">15</td>
                                        <td id="Total_Paused">15</td>
                                        <td id="Total_Archived">20</td>
                                        <td id="campaign_DateRange"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">
                            <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i></span>
                                            <?php
                                            $date=date("Y-m-d");
                                            ?>
                                            <input style="width: 130px" type="text" id="addCampSpentDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                            </div>



                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="campaginID" class="form-control" value='' placeholder="Campagin ID">



                            </div>

                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="campaginName" class="form-control" value='' placeholder="Campagin Name">



                            </div>

                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="adPoster" class="form-control" value='' placeholder="Ad-Poster">



                            </div>


                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="bidStatergy" class="form-control" value='' placeholder="Bid Statergy">


                                <span class="btn input-group-addon" type="submit" onclick="getAdcampagian()"><i class="fa fa-search"></i>  </span>


                            </div>



                            <div class="input-group styled-select">
                                <select id="" style="width:115px">
                                    <option value="" disabled selected> Ad-Type</option>
                                    <option value="text"> Text</option>
                                    <option value="display"> Display</option>
                                    <option value=""> All</option>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                            </div>

                            <div class="input-group styled-select pull-right">
                                <select id="adStatus" style="width: 120px";>
                                    <option value="" disabled selected>Status</option>
                                    <option value="Paused"> Paused</option>
                                    <option value="Active"> Active</option>
                                    <option value="Rejected"> Rejected</option>
                                    <option value=""> All</option>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div id="campagianDetailsAjax">
                
            </div>

        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $( document ).ready(function() {
            getAdcampagian();
        });

        function getAdcampagian()
        {

            var refferaldatevalue=($("#addCampSpentDate").val());
            var datedata=refferaldatevalue.split("to");
            var fromdate=(datedata[0]).trim();
            var todate=(datedata[1]).trim();

            var adType=($("#adType").val());
            var adStatus=($("#adStatus").val());

            var campaginID=($("#campaginID").val());
            var campaginName=($("#campaginName").val());
            var adPoster=($("#adPoster").val());
            var bidStatergy=($("#bidStatergy").val());

            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/adMgmt/adCampagins.php",
                data: {
                    fromdate: fromdate, todate: todate,limit:limit,adStatus:adStatus,adType:adType,listtype:"first",campaginID:campaginID,campaginName:campaginName,adPoster:adPoster,bidStatergy:bidStatergy
                },
                success: function (data) {
                    $("#campagianDetailsAjax").html("");
                    $("#campagianDetailsAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#addCampSpentDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function(event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function(event, obj) {

            getAdcampagian();
        });


        $("#LimitedResult").change(function(){
            getAdcampagian();
        });
        $("#adType").change(function(){
            getAdcampagian();
        });

        $("#adStatus").change(function(){
            getAdcampagian();
        });





    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>