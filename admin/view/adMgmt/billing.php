<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Ad Management</h1><span>Billing</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <div class="row">
                <div class="col-xs-12">
                    <div id="userRefferalAnalatics">

                            <table class="table  text-center table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center text-white">Invoice Generated(Nos.)</th>
                                        <th class="text-center text-white">Total Amt.</th>
                                        <th class="text-center text-white"><i class="fa fa-calendar" id="Invoice_Generated_DateRange"></i></font></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td id="Invoice_Generated(Nos.)">400</td>
                                        <td id="Total_Amt">5 <?= $adminCurrency; ?></td>
                                        <td id="Invoice_Generated_DateRange"></td>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        
            <div class="row">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">
                            <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i></span>
                                            <?php
                                            $date=date("Y-m-d");
                                            ?>
                                            <input style="width: 190px" type="text" id="addCampSpentDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>



                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="userID" class="form-control" value='' placeholder="User ID">

                                <span class="btn input-group-addon" type="submit" onclick="getAdBilling()"><i class="fa fa-search"></i>  </span>

                            </div>

                            <div class="input-group styled-select" >

                                <input style="width: 110px;z-index: 1" type="text" id="InvoiceNO" class="form-control" value='' placeholder="Invoice No">

                                <span class="btn input-group-addon" type="submit" onclick="getAdBilling()"><i class="fa fa-search"></i>  </span>

                            </div>
                            

                        </form>
                    </div>
                </div>
            </div>


         <div id="billingDetailsAjax">

         </div>
    </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script>
        $( document ).ready(function() {
            getAdBilling();
        });

        function getAdBilling()
        {

            var refferaldatevalue=($("#addCampSpentDate").val());
            var datedata=refferaldatevalue.split("to");
            var fromdate=(datedata[0]).trim();
            var todate=(datedata[1]).trim();

            var userID=($("#userID").val());
            var InvoiceNO=($("#InvoiceNO").val());

            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/adMgmt/adBilling.php",
                data: {
                    fromdate: fromdate, todate: todate,limit:limit,InvoiceNO:InvoiceNO,userID:userID,listtype:"first"
                },
                success: function (data) {
                    $("#billingDetailsAjax").html("");
                    $("#billingDetailsAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#addCampSpentDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function(event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function(event, obj) {

            getAdBilling();
        });


        $("#LimitedResult").change(function(){
            getAdBilling();
        });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>