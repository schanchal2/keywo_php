<?php
session_start();

require_once "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
    require_once("../../model/cms/landingContent_model.php");
/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
    ?>
    <link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Manage Users</h1><span>Universal Users</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <br/>
            <div class="content-wrapper">
                <div id="errors"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <?php
                        if((in_array("write", $myp))) {
                        ?>
                                <div class="box-header">
                                    <div class="row " style="float:right">
                                        <form class="form-inline">
                                               <a href="javascript:;" class="btn btn-success" id="addNewUser" onclick="addNewUser()" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">Add
                                        New User <span
                                            class="fa fa-plus-circle"></span></a>
                                            <div class="btn-group pull-right" style="margin-bottom: 5px;margin-right: 15px;background: #0299be;">
                                                <button type="button" style="border:none;" class="btn bg-blue dropdown-toggle text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-user "></i> Account Actions <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu drop-down">
                                                    <li><a href="javascript:;" onclick="changeSpecialStatusUserBulk('delete');"><i class="fa fa-trash"></i>
                                                    Delete</a></li>
                                                </ul>
                                            </div>
                                        <!--     <div class="pull-right p-r-15" style="z-index: 1;">
                                                <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" onclick="addNewUser()" id="addNewUser">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                </button>
                                            </div> -->
                                        </form>
                                    </div>
                                </div>
                                <?php } ?>
                                <!-- /.box-header -->
                                <div class="box-body">
                                    <div id="specialUserAjax">
                                    </div>
                                </div>
                                <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
            </div>
        </div>
        <div id="specialUser">
        </div>
        <div id="errDialog">
        </div>
    </main>
    </div>
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script>
    $("#specialUser").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Add",
        buttons: {
            "AddUser": function(e) {

            }

        }

    });

    $("#errDialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Error !",
        width: 350,
        height: 180,
        buttons: {

        }
    });


    function addNewUser() {

        var html = '<div class="form-group has-feedback"> <input type="text" class="form-control" id="addnewspecailuser" name="addnewspecailuser" placeholder="Enter Special User Email ID"> <span class="glyphicon glyphicon-user form-control-feedback"></span> </div>';

        $("#specialUser").html(html);
        $("#specialUser").dialog('option', 'title', "Add New Special User");

        var buttons = $('#specialUser').dialog('option', 'buttons');
        buttons.AddUser = function() {
            //updateFeesAjax(idtofileddynamic,idelementtoupdate,fieldname);

            addNewSpecialUser();
        };

        $('#specialUser').dialog('option', 'buttons', buttons);
        $("#specialUser").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');

    }

    function addNewSpecialUser() {
        var specialUser = document.getElementById("addnewspecailuser").value;
        var pageno = $("#hiddenpagespecial").val();

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/manageUser/CRUDSpecialUser.php",
            data: {
                specialUser: specialUser,
                type: "addnewuser"
            },
            beforeSend: function() {
                $('#loadng-image').show();
            },
            success: function(user) {

                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {

                    getUserSpecialListPagination(pageno, 10);
                    $("#specialUser").dialog('close');

                    $("#errors").html(bsAlert("success", "Added special users Success!"));
                } else {
                    $("#errDialog").html("<br/><br/><center style='color:red'><h4>" + msg + "<h4></h4></center>");
                    $("#errDialog").dialog('open');
                    // $("#errors").html(bsAlert("danger","Error Adding special users!"));
                }
                $('#loadng-image').hide();

            },
            error: function() {
                console.log("Failed to Update Data.");

            }
        });

    }


    $(document).ready(function() {
        getUserListSpecial();
    });

    function getUserListSpecial() {

        var limit = 10;

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/manageUser/getUserListSpecial.php",
            data: {
                limit: limit,
                listtype: 'first'
            },
            beforeSend: function() {
                $('#loadng-image').show();
            },
            success: function(data) {

                $("#specialUserAjax").html("");
                $("#specialUserAjax").html(data);
                $('#loadng-image').hide();

            },
            error: function() {
                alert("fail");
            }
        });

    }

    function getUserSpecialListPagination(pageno, limit) {


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/manageUser/getUserListSpecial.php",
            data: {
                limit: limit,
                listtype: 'pagination',
                pageno: pageno
            },
            beforeSend: function() {
                $('#loadng-image').show();
            },
            success: function(data) {

                $("#specialUserAjax").html("");
                $("#specialUserAjax").html(data);
                $('#loadng-image').hide();

            },
            error: function() {
                alert("fail");
            }
        });
    }


    function changeSpecialStatusUserBulk(status) {

        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var emails = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring = (tags[i].value).trim();
                var splitmystring = mystring.split(",");

                var useremail = splitmystring[0];

                emails.push(useremail);

            }

        }


        if (emails.length === 0) {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {});
            return false;
        }

        bootbox.confirm({
            message: "<h4>Are you sure to Delete Universal users from keywo?</h4>",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result) {
                if (result == true) {
                    var pageno = $("#hiddenpagespecial").val();

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "../../controller/manageUser/manageSpecialUserBulkAct.php",
                        data: {
                            email: emails,
                            status: status
                        },
                        beforeSend: function() {
                            $('#loadng-image').show();
                        },
                        success: function(user) {

                            if (user["errCode"]) {
                                if (user["errCode"] == "-1") {

                                    getUserSpecialListPagination(pageno, 10);
                                    $('#loadng-image').hide();
                                    $("#errors").html(bsAlert("success", user["errMsg"]));
                                } else if (user["errCode"] == "4") {


                                    getUserSpecialListPagination(pageno, 10);
                                    $('#loadng-image').hide();


                                    $("#errors").html(bsAlert("success", user["errMsg"]));
                                } else {
                                    $("#errors").html(bsAlert("danger", "Error Deleting special users!"));
                                }

                            }

                        },
                        error: function() {
                            console.log("Failed to delete user");
                        }
                    });
                } else {
                    console.log("no selected");
                }
            }
        });


    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
