<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>
<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild">
    <thead>
    <tr>
        <th> Campaign ID</th>
        <th> Amount Utilised</th>
        <th> Start Date </th>
        <th> Expiry Date</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            AB-123
        </td>
        <td>0.000023 <?= $adminCurrency; ?></td>
        <td>01.08.2016</td>
        <td>01.10.2016</td>
    </tr>
    <tr>
        <td>
            AB-123
        </td>
        <td>0.000023 <?= $adminCurrency; ?></td>
        <td>01.08.2016</td>
        <td>01.10.2016</td>
    </tr>
    </tbody>
</table>
    </div>