<?php
session_start();
require_once("../../../config/config.php");
require_once("../../../config/db_config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/keywords/common_keyword.php");
require_once("{$docRootAdmin}/helpers/stringHelper.php");
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$buyer_id=base64_decode($_GET["tid"]);
$keywords=getMyKeywordDetails($connKeywords, $buyer_id);
$keywordsData=json_decode($keywords["errMsg"]["transaction_details"],true);

?>

<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild" id="myKeywordsListTable">
    <thead>
    <tr>
        <th> Keyword</th>
        <th> Purchase Price</th>
       <!-- <th> Discount Amount </th>-->
        <th> Your Ask</th>
        <th> Highest Bid Amount </th>
        <th> Purchase Mode </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($keywordsData as $value) {

        $individualKeywords=getKeywordOwnershipDetails($connKeywords, $value["keyword"])["errMsg"];
        if(count($individualKeywords)!=0)
        {
            $kwdAsk=$individualKeywords["ask_price"];
            $highestAsk=$individualKeywords["highest_bid_amount"];
            $purchaseBy=$individualKeywords["payment_mode"];
        }

        ?>
        <tr>
            <td>
                <?= cleanDisplayParameter($connKeywords,$value["keyword"]); ?>
            </td>
            <td><?= number_format($value["kwd_price"],6); ?></td>
            <!-- <td>0.000023 ITD</td>-->
            <td><?= number_format($kwdAsk,6); ?></td>
            <td><?= number_format($highestAsk,6); ?></td>
            <td><?= $purchaseBy; ?></td>
        </tr>
        <?php
    }
?>

    </tbody>
</table>
    </div>
<script>
    $('#myKeywordsListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>