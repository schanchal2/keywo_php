<?php
session_start();
require_once("../../../config/config.php");
require_once("../../../config/db_config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/manageUser/manageUser.php");
require_once("{$docRootAdmin}/helpers/stringHelper.php");
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$buyer_id=base64_decode($_GET["tid"]);
$supportData=getLastQueryTickets($buyer_id,100,$connKeywords)["errMsg"];

?>

<div id="debitPanel">
    <table class="table text-center table-responsive tblwhite-lastchild" id="myQueryListTable">
    <thead>
    <tr>
        <th style="width:10%"> Date/Timestamp</th>
        <th> Ticket ID</th>
        <th> Query Type </th>
        <th> Status </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($supportData as $value) {
        $time=explode(" ",$value["requestTime"]);
        ?>

        <tr>
            <td>
                <span class="date"> <?= $time[0]; ?> </span>
                <time> <?= $time[1]; ?></time>
            </td>
            <td><?= $value["ticket_id"]; ?></td>
            <td> <?= $value["request_type"]; ?></td>
            <td> <?= ucfirst($value["request_status"]); ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
    </div>
<script>
    $('#myQueryListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>