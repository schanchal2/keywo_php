<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/manageUser/manageUser.php";
include "../../../model/wallet/transactions.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$result=getRefferedUsers($userEmail);
if(noError($result))
{
    $my_referral_id=$result["errMsg"]["my_referral_id"];
    $result=$result["errMsg"]["users"];

}else
{
    $result=array();
}

?>

<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild" id="referralDetailsData">
    <thead>
    <tr>
        <th> Your Referral Code </th>
        <th> Referee's Email ID</th>
        <th> Referee's Referral Code </th>
        <th> Referee's Signup Date </th>
        <th> Total Social Affiliate </th>
        <th> Total Keyword Affiliate </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($result as $data) {
        $time=$data["referred_user_signup_time"];
        ?>
        <tr>
            <td>
                <?= $my_referral_id; ?>
            </td>
            <td> <?= $data["referred_user_email"]; ?></td>
            <td> <?= $data["referred_user_referral_code"]; ?></td>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <td><?= number_format($data["social_referral_earnings"],6); ?> </td>
            <td><?= number_format($data["affiliate_earnings"],6); ?> </td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
    </div>
<script>
    $('#referralDetailsData').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
    </script>