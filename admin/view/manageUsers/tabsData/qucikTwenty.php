<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/manageUser/manageUser.php";
include "../../../model/wallet/transactions.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$user_id=getUserInfo($userEmail, $walletURLIP . 'api/v3/', "user_id")["errMsg"]["_id"];
$transaction_toquery="search_earning,ST_Earnings,post_viewer_earning,search_referral_earnings,keyword_ownership_earning,affiliate_earnings,social_content_sharer_earnings,social_content_creator_earnings,social_content_view_earnings,search_affiliate_earnings";
$result=getTransactionsWallet($userEmail,$user_id,"100",$transaction_toquery,"nin","");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];
}

?>
<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild" id="myTransactionsListTable">
    <thead>
    <tr>
        <th> Date/Timestamp</th>
        <th> Transaction Type</th>
        <th> Transaction Details </th>
        <th> Amount </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {
        if($transaction['sender']== $userEmail){
            $type = ($transaction['type'] == "transfer")? "Sent": $transaction['type'] ;
            $senderDesc = sprintf($descriptionArray['sender_desc'], $transaction['amount'], 'ITD', $transaction['receiver']);

            $description = $senderDesc;
        }elseif($transaction['sender'] != $userEmail){
            $type = ($transaction['type'] == "transfer")? "Received": $transaction['type'] ;
            $receiverDesc = sprintf($descriptionArray['reciver_desc'], $transaction['amount'], 'ITD', $transaction['sender']);
            $description = $receiverDesc;
        }else{
            $type = $transaction['type'];

        }
        $type=ucwords(str_replace("_"," ",$type));
        $time=$transaction["time"];
        ?>
    <tr>
        <td>
            <span class="date"> <?= date("d-m-Y",($time / 1000)); ?> </span>
            <time> <?= date("h:i:s",($time / 1000)); ?></time>
        </td>
        <td><?= $type; ?></td>
        <td> <?= $description; ?> </td>
        <td><?= number_format($transaction["amount"],6); ?> </td>
    </tr>
    <?php

    }
    ?>

    </tbody>
</table>
        </div>

<script>
    $('#myTransactionsListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>