<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

$usere_id = base64_decode(trim($_GET["tid"]));
?>
<style>
   /* .LogAct {

        display: block;
    }

    #debitPanel tr:last-child {
        background: #fff;

    }

    #debitPanel tr:last-child td {

        color: #333;
    }

     #debitPanel th {
         background: rgb(142, 145, 138);
         text-align: center;
         padding: 3px;
     }

    #debitPanel th:not(:last-child) {
        border-right: solid 1px #255e8f;
    }
    #debitPanel th {
        background: rgb(142, 145, 138);
        text-align: center;
        padding: 3px;
    }

    #debitPanel tr:last-child td {

        color: #333;
    }

    #userDebitDetails td:not(:last-child), #debitPanel td:not(:last-child) {
        border-right: solid 1px #8e918a;
    }

    #userDebitDetails td, #debitPanel td {
        vertical-align: middle;
        border-bottom: solid 1px #8e918a;
        border-top: none;
        text-align: left;
    }

    #userDebitDetails tr:last-child td, #debitPanel tr:last-child td {
        border-bottom: solid 1px #8e918a;
    }

    #userDebitDetails table, #debitPanel table {
        border: solid 1px #8e918a;
    }

    .pagination li:first-child a, .pagination li:last-child a {
        color: #8e918a;
        background: #d9edf8;
    }
    .pagination > li > a:hover, .pagination > li > a:focus, .pagination > li > span:hover, .pagination > li > span:focus {
        z-index: 2;
        color: #8e918a;
        background-color: #eeeeee;
        border-color: #ddd;
    }*/

</style>

<input id="userEid" value="<?php echo $usere_id; ?>" hidden>
<div id="smartTable">
    <div class="wrapper">
        <div class="row">
            <div class="col-md-12">
                <!-- Custom Tabs -->
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs" style="background: #35ccf1;">
                        <li class="active"><a href="#loginLogs" data-toggle="tab">Login</a></li>
                        <li><a href="#DepositLogs" data-toggle="tab"><?= $adminCurrency; ?> Purchase</a></li>
                        <li><a href="#TradingLogs" data-toggle="tab">Trading</a></li>
                        <li><a href="#withdrawalLogs" data-toggle="tab">Cashout</a></li>
                        <li><a href="#forgotPasswdLogs" data-toggle="tab">Forgot Password </a></li>
                        <li><a href="#buyKeywordLogs" data-toggle="tab">Buy Keyword</a></li>
                    </ul>
                    <div class="tab-content" style="border: solid 1px rgba(0, 188, 212, 0.39);padding:10px;">
                        <div class="tab-pane active" id="loginLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="loginformexport" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="loginLogsDateRangePiker"
                                                                   name="loginLogsDateRangePiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="login" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readLoginLogs()"></button>
                                                    </div>


                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="logintextSearch" type="text"
                                                               placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="dataofLoginLogs">
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="tab-pane" id="DepositLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="itdPurchaseformexport" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="itdPurchaseLogsDateRangePiker"
                                                                   name="itdPurchaseLogsDateRangePiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="trade" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readitdPurchaseLogs()"></button>
                                                    </div>

                                                    <div class="input-group styled-select">
                                                        <select id="itdPurchase_by">
                                                            <option value="itdPurchase_by_paypal"> Paypal</option>
                                                            <option value="itdPurchase_by_bitgo"> Bitgo</option>
                                                        </select>
                                                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                                    </div>




                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="itdPurchasetextSearch" type="text"
                                                               placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="dataofitdPurchaseLogs">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="TradingLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="tradeformexport" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="tradeLogsDateRangePiker"
                                                                   name="tradeLogsDateRangePiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="trade" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readTradeLogs()"></button>
                                                    </div>

                                                    <div class="input-group styled-select">
                                                        <select id="trade_status">
                                                            <option value="accept_bid"> Accept Bid</option>
                                                            <option value="accept_ask"> Accept Ask (Buy Now)</option>
                                                        </select>
                                                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                                    </div>



                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="tradetextSearch" type="text"
                                                               placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="dataoftradeLogs">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="withdrawalLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="cashoutformexport" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="cashoutLogsDateRangePiker"
                                                                   name="cashoutLogsDateRangePiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="trade" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readcashoutLogs()"></button>
                                                    </div>

                                                    <div class="input-group styled-select">
                                                        <select id="cashout_process">
                                                            <option value="cashout_submitted"> Cashout Request Submitted</option>
                                                            <option value="cashout_processed"> Cashout Request Processed</option>
                                                        </select>
                                                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                                    </div>




                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="cashouttextSearch" type="text"
                                                               placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="dataofcashoutLogs">
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="tab-pane" id="forgotPasswdLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="forgotpasswordlogform" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="forgotpassworddatepiker"
                                                                   name="forgotpassworddatepiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="login" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readforgotpasswordLogs()"></button>
                                                    </div>


                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="forgotpasstextSearch"
                                                               type="text" placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="forgotPasswordLogs">
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                        <div class="tab-pane" id="buyKeywordLogs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                        <form class="form-inline" name="buyKwdformexport" method="post"
                                              action="javascript:;">
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar"></i>
                                                            </div>
                                                            <input type="text" placeholder="Select Date Range"
                                                                   class="form-control" id="buyKwdLogsDateRangePiker"
                                                                   name="buyKwdLogsDateRangePiker"
                                                                   value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                   autocomplete="off" size="23">
                                                        </div><!-- /.input group -->
                                                    </div><!-- /.form group -->

                                                    <input type="text" value="trade" name="type" hidden="">
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-default fa fa-search"
                                                                onclick="readBuyKeywordLogs()"></button>
                                                    </div>

                                                    <div class="input-group styled-select">
                                                        <select id="buyKwd_by">
                                                            <option value="buy_by_paypal"> Paypal</option>
                                                            <option value="buy_by_wallet"> Wallet</option>
                                                            <option value="accept_bitgo"> Bitgo</option>
                                                        </select>
                                                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                                    </div>




                                                </div>
                                                <div class="form-group col-sm-3" style="float:right">
                                                    <div class="">
                                                        <label>Filter:</label>
                                                        <input class="form-control" id="buyKwdtextSearch" type="text"
                                                               placeholder="Search Logs">
                                                    </div>
                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div id="dataofbuyKwdLogs">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- nav-tabs-custom -->
            </div>
        </div>
    </div>
</div>

<script>

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab
        switch (target) {
            case "#forgotPasswdLogs":
                readforgotpasswordLogs();
                break;

            case "#TradingLogs":
                readTradeLogs();
                break;

            case "#buyKeywordLogs":
                readBuyKeywordLogs();
                break;

            case "#loginLogs":
                readLoginLogs();
                break;

            case "#DepositLogs":
                readitdPurchaseLogs();
                break;

            case "#withdrawalLogs":
                readcashoutLogs();
                break;
        }
    });

    $(document).ready(function () {
        readLoginLogs();
    });

    var todaysdate = new Date();
    var threeMonthsAgo = (3).months().ago();

    /****************************************** login ****************************************/
    $('#loginLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });

    function readLoginLogs() {


        var datecapctured = $("#loginLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReadLoginLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofLoginLogs").html("");
                $("#dataofLoginLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /****************************************** login ****************************************/


    /****************************************** forgot password ****************************************/
    $('#forgotpassworddatepiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);


    });

    function readforgotpasswordLogs() {

        var forgotpassworddatepiker = $("#forgotpassworddatepiker").val();
        var datedata = forgotpassworddatepiker.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReadForgotPasswordLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#forgotPasswordLogs").html("");
                $("#forgotPasswordLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /****************************************** forgot password ****************************************/


    /****************************************** trade ****************************************/
    $('#tradeLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);


    });

    function readTradeLogs() {

        var tradeLogsDateRangePiker = $("#tradeLogsDateRangePiker").val();
        var datedata = tradeLogsDateRangePiker.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        var trade_status = $("#trade_status").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReadTradeLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail,trade_status:trade_status
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataoftradeLogs").html("");
                $("#dataoftradeLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /****************************************** trade ****************************************/


    /****************************************** buy Keyword ****************************************/
    $('#buyKwdLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);


    });

    function readBuyKeywordLogs() {

        var buyKwdLogsDateRangePiker = $("#buyKwdLogsDateRangePiker").val();
        var datedata = buyKwdLogsDateRangePiker.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        var buyKwd_by = $("#buyKwd_by").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReadbuyKwdLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail,buyKwd_by:buyKwd_by
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofbuyKwdLogs").html("");
                $("#dataofbuyKwdLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /****************************************** buy Keyword ****************************************/



    /****************************************** ITD PURCHASE ****************************************/
    $('#itdPurchaseLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);


    });

    function readitdPurchaseLogs() {

        var itdPurchaseLogsDateRangePiker = $("#itdPurchaseLogsDateRangePiker").val();
        var datedata = itdPurchaseLogsDateRangePiker.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        var itdPurchase_by = $("#itdPurchase_by").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReaditdPurchaseLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail,itdPurchase_by:itdPurchase_by
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofitdPurchaseLogs").html("");
                $("#dataofitdPurchaseLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /****************************************** ITD PURCHASE  ****************************************/




    /****************************************** cashout ****************************************/
    $('#cashoutLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);


    });

    function readcashoutLogs() {

        var cashoutLogsDateRangePiker = $("#cashoutLogsDateRangePiker").val();
        var datedata = cashoutLogsDateRangePiker.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        var userEmail = $("#userEid").val();
        var cashout_process = $("#cashout_process").val();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/logReaders/ReadcashoutLogs.php",
            data: {
                fromdate: fromdate, todate: todate, userEmail: userEmail,cashout_process:cashout_process
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofcashoutLogs").html("");
                $("#dataofcashoutLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /****************************************** cashout ****************************************/
</script>