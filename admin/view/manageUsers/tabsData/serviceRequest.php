<?php
session_start();
require_once("../../../config/config.php");
require_once("../../../config/db_config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/serviceRequest/serviceRequestModel.php");
require_once("{$docRootAdmin}/helpers/stringHelper.php");
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$buyer_id=base64_decode($_GET["tid"]);
$srData=userLastServiceRequests($connKeywords,"data", $buyer_id);
$srData=$srData["data"];


?>

<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild" id="mysrQueryListTable">
    <thead>
    <tr>
        <th> Date/Timestamp</th>
        <th> SR-Number</th>
        <th> SR Type </th>
        <th> Raised By (Agent)</th>
        <th> Actioned By (Agent) </th>
        <th> Status </th>
    </tr>
    </thead>
    <?php
    foreach($srData as $value) {
        $time=explode(" ",$value["request_date"]);
        ?>

        <tr>
            <td>
                <span class="date"> <?= $time[0]; ?> </span>
                <time> <?= $time[1]; ?></time>
            </td>
            <td><?= $value["request_no"]; ?></td>
            <td> <?= $value["request_type"]; ?></td>
            <td> <?= $value["raised_by"]; ?></td>
            <td> <?php if($value["request_status_by"]!="")
                {
                  echo $value["request_status_by"];
                }else
                {
                    echo "NA";
                }?></td>
            <td> <?= ucfirst($value["request_status"]); ?></td>
        </tr>
        <?php
    }
    ?>
    </tbody>
</table>
</div>
<script>
    $('#mysrQueryListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>