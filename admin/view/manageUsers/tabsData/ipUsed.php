<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/cryptoHelper.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
include "../../../model/manageUser/manageUser.php";
$usere_id=base64_decode(trim($_GET["tid"]));
$result=getLastUsedIP($usere_id);
$result=array_reverse($result["errMsg"]["last_used_ip"]);

?>

<div id="debitPanel">
<table class="table text-center table-responsive tblwhite-lastchild" id="myIpUsedListTable">
    <thead>
    <tr>
        <th> Last Used IP</th>
        <th> Status</th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($result as $result) {
        ?>
        <tr>
            <td>
                <?php echo $result["ip"]; ?>
            </td>
            <td><?php
                $activeStatus = $result["status"];
                if ($activeStatus == "0") {
                    echo "<span class=\"text-danger\">Blocked</span>";
                }

                if ($activeStatus == "1") {
                    echo "<span class=\"text-success\">Active</span>";
                }

                ?></td>
        </tr>

        <?php
    }

    ?>
    </tbody>
</table>
    </div>

<script>
    $('#myIpUsedListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>