<?php
session_start();
require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/cryptoHelper.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/manageUser/manageUser.php";



$usere_id=base64_decode(trim($_GET["tid"]));
$result=getUserStatus($usere_id)["errMsg"];
$sendFunds=$result["send"];
$depositFunds=$result["deposit"];
$itdPurchase=$result["purchase_itd"];
$receiveFunds=$result["receive"];
$blockedBidsFunds=$result["blocked_for_bids"];
$salesFunds=$result["sales"];
$cashbackFunds=$result["cashback"];//
$affiliateEarningFunds=$result["affiliate_earning"];
$approvedWithdrawalsFunds=$result["approved_withdrawals"];//cashout=withdrawal
$blockedForPendingWithdrawalsFunds=$result["blocked_for_pending_withdrawals"];
$tradeFeesFunds=$result["trade_fees"];//
$purchasesFunds=$result["purchases"];
$searchEarningFunds=$result["search_earning"];
$total_kwd_incomeFunds=$result["total_kwd_income"];
$total_app_incomeFunds=$result["total_app_income"];
$search_affiliate_earningsFunds=$result["search_affiliate_earnings"];
$renewal_feesFunds=$result["renewal_fees"];//
$maintenance_feesFunds=$result["maintenance_fees"];//
$total_unsold_kwd_refundFunds=$result["total_unsold_kwd_refund"];//
$post_earningFunds=$result["post_earning"];
$post_kwd_earningFunds=$result["post_kwd_earning"];
$post_share_earningFunds=$result["post_share_earning"];
$post_view_earnings=$result["social_content_view_earnings"];
$cashout_fees=$result["cashout_fees"];

$adPurchase=0;
$transactionFees=$maintenance_feesFunds+$renewal_feesFunds+$tradeFeesFunds+$cashout_fees;
$socialandSearchEarning=$post_view_earnings+$searchEarningFunds;

$debit=$approvedWithdrawalsFunds+$sendFunds+$transactionFees+$purchasesFunds+$adPurchase+$blockedBidsFunds+$blockedForPendingWithdrawalsFunds;
$credit=$itdPurchase+$receiveFunds+$salesFunds+$total_kwd_incomeFunds+$search_affiliate_earningsFunds+$affiliateEarningFunds+$socialandSearchEarning+$post_earningFunds+$post_kwd_earningFunds+$post_share_earningFunds;
?>
<div id="userDebitDetails" class="claearfix">
    <table class="table text-center table-responsive">
        <thead>
        <tr>
            <th> Debit </th>
            <th> Amount </th>
            <th> Creadit </th>
            <th> Amount </th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/approved_cashouts.php?tid=<?php echo base64_encode($usere_id); ?>">Approved Cashouts</a></td>
            <td> <?php echo number_format($approvedWithdrawalsFunds,6);?> <?= $adminCurrency; ?></td>
            <td><a href="#" data-value="manageUserWallet/credit/itd_purchase.php?tid=<?php echo base64_encode($usere_id); ?>"> <?= $adminCurrency; ?> Purches (Deposit) </a></td>
            <td> <?php echo number_format($itdPurchase,6);?> <?= $adminCurrency; ?></td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/send_funds.php?tid=<?php echo base64_encode($usere_id); ?>"> Send Funds</a></td>
            <td> <?php echo number_format($sendFunds,6);?> <?= $adminCurrency; ?> </td>
            <td><a href="#" data-value="manageUserWallet/credit/received_funds.php?tid=<?php echo base64_encode($usere_id); ?>"> Recieved Funds </a></td>
            <td> <?php echo number_format($receiveFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/kwd_purchase.php?tid=<?php echo base64_encode($usere_id); ?>"> Keyword Purchase</a></td>
            <td> <?php echo number_format($purchasesFunds,6);?> <?= $adminCurrency; ?> </td>
            <td><a href="#" data-value="manageUserWallet/credit/kwd_trading.php?tid=<?php echo base64_encode($usere_id); ?>"> Keyword Trading (Sales)</a></td>
            <td> <?php echo number_format($salesFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/transaction_fees.php?tid=<?php echo base64_encode($usere_id); ?>"> Transaction Fees </a></td>
            <td> <?php echo number_format($transactionFees,6);?> <?= $adminCurrency; ?> </td>
            <td><a href="#" data-value="manageUserWallet/credit/kwd_income.php?tid=<?php echo base64_encode($usere_id); ?>"> Keyword Income</a></td>
            <td> <?php echo number_format($total_kwd_incomeFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/pending_cashout.php?tid=<?php echo base64_encode($usere_id); ?>">Pending Cashouts</a></td>
            <td> <?php echo number_format($blockedForPendingWithdrawalsFunds,6);?> <?= $adminCurrency; ?> </td>


            <td><a href="javascript:;" data-value="javascript:;" class="text-darkblue"> REFERRAL :-</a></td>
            <td> &nbsp; </td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/blocked_bid.php?tid=<?php echo base64_encode($usere_id); ?>"> Blocked for Bids </a></td>
            <td> <?php echo number_format($blockedBidsFunds,6);?> <?= $adminCurrency; ?> </td>
            <td><a href="#" data-value="manageUserWallet/credit/kwd_refferal.php?tid=<?php echo base64_encode($usere_id); ?>"> Keyword Referral</a></td>
            <td> <?php echo number_format($affiliateEarningFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>
        <tr>
            <td> <a href="#" data-value="manageUserWallet/debit/add_purchase.php?tid=<?php echo base64_encode($usere_id); ?>"></a></td>
            <td> <?php //echo number_format($adPurchase,6);?> </td>
            <td><a href="#" data-value="manageUserWallet/credit/search_refferal.php?tid=<?php echo base64_encode($usere_id); ?>"> Social Referral</a></td>
            <td> <?php echo number_format($search_affiliate_earningsFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>

        <tr>
            <td> &nbsp; </td>
            <td> &nbsp; </td>
            <td>
                <a href="javascript:;" data-value="javascript:;" class="text-darkblue"> <b> Social :-</b> </a>
            </td>
            <td> </td>
        </tr>
        <tr>
            <td> &nbsp; </td>
            <td> &nbsp; </td>
            <td><a href="#" data-value="manageUserWallet/credit/post_earning.php?tid=<?php echo base64_encode($usere_id); ?>"> Post Earning</a></td>
            <td> <?php echo number_format($post_earningFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>
       
        <tr>
            <td> &nbsp; </td>
            <td> &nbsp; </td>
            <td><a href="#" data-value="manageUserWallet/credit/postShareEarning.php?tid=<?php echo base64_encode($usere_id); ?>"> Post Share Earnings </a></td>
            <td> <?php echo number_format($post_share_earningFunds,6);?> <?= $adminCurrency; ?> </td>
        </tr>

        <tr>
            <td> <b> TOTAL </b></td>
            <td> &nbsp;<?php echo number_format($debit,6);?> <?= $adminCurrency; ?> </td>
            <td>
                <a href="javascript:;" data-value="javascript:;"> <b> TOTAL </b></a>
            </td>
            <td> &nbsp;<?php echo number_format($credit,6);?> <?= $adminCurrency; ?> </td>
        </tr>
        </tbody>
    </table>
</div>
<div id="debitPanel">
</div>
<script>
    $('#userDebitDetails a[data-value]').on('click', function(event) {
        event.preventDefault();
        console.log($(this).data('value'));
        $(this).parent().parent().parent().find('td').removeClass('active');
        $(this).parent().addClass('active').next().addClass('active');
        var sPageName = $(this).data('value');
        $.get(sPageName, function(data) {
            $("#debitPanel").html(data);
           // document.body.scrollTop = document.body.scrollHeight;
            $('body').animate({
                scrollTop: document.body.scrollHeight
            }, 500);
        });
    });
</script>