<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
$userEmail=trim($_GET["tid"]);
?>
<div id="debitPanel">
    <divwrapper">
    <div class="row">
        <div class="col-md-12">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs" style="background: #35ccf1;">
                    <li class="active"><a href="#TradeFees" data-toggle="tab">Trade</a></li>
                    <li><a href="#CashoutFees" data-toggle="tab">Cashout</a></li>
                    <li><a href="#RenewalFees" data-toggle="tab">Renewal</a></li>
                </ul>
                <div class="tab-content" style="border: solid 1px #bdeefa;padding:10px;">
                    <div class="tab-pane active" id="TradeFees">
                        <div class="row">
                            <div class="col-xs-12">
                                <div id="tradeFeesData" class="resetTableData">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="tab-pane" id="CashoutFees">
                        <div class="row">
                            <div class="col-xs-12">

                                <div id="cashoutFeesData" class="resetTableData">
                                </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                    <div class="tab-pane" id="RenewalFees">
                        <div class="row">
                            <div class="col-xs-12">
                              <div id="renewalFeesData" class="resetTableData">
                              </div>
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- nav-tabs-custom -->
        </div>
    </div>
</div>
</div>

<script>
    $( document ).ready(function() {
        $(".resetTableData").html("");
        $("#tradeFeesData").load("manageUserWallet/debit/fees/trade.php?tid=<?php echo $userEmail; ?>");
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab
        $(".resetTableData").html("");
        switch (target)
        {
            case "#TradeFees":
                $("#tradeFeesData").load("manageUserWallet/debit/fees/trade.php?tid=<?php echo $userEmail; ?>");
                break;

            case "#CashoutFees":
                $("#cashoutFeesData").load("manageUserWallet/debit/fees/cashout.php?tid=<?php echo $userEmail; ?>");
                break;


            case "#RenewalFees":
                $("#renewalFeesData").load("manageUserWallet/debit/fees/renewal.php?tid=<?php echo $userEmail; ?>");
                break;

        }
    });

</script>
