<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
$userEmail=base64_decode(trim($_GET["tid"]));
require_once('../../../../model/tracking/keywordwise/keywordSearchInteraction.php');
require_once('../../../../model/keywords/common_keyword.php');
require_once('../../../../helpers/stringHelper.php');
//end other

//printArr($_POST);
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$result=getMyLastBids($userEmail, $connKeywords);
$result=$result["errMsg"];
?>
<table class="table text-center table-responsive tblwhite-lastchild" id="blockedForBidTables">
    <thead>
    <tr>
        <th> Date/Timestamp</th>
        <th> Bid Placed Against Keyword</th>
        <th> Present Owner</th>
        <th> Bid Amount (A) </th>
        <th> Transaction Fees (B)</th>
        <th> Total Blocked Amt (A + B)</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {
        $time=explode(" ",$transaction["bid_time"]);
        $keyword=$transaction["keyword"];
        $tnx_fees=explode("~~",$transaction["bid_transaction_id"])[4];
        $amount=$transaction["bid_price"];
        $total=$amount+$tnx_fees;
        $Owner=getKeywordOwnershipDetails($connKeywords, $keyword)["errMsg"]["buyer_id"];


        ?>
        <tr>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <td><?= $keyword; ?></td>
            <td><?= $Owner; ?></td>
            <td> <?= number_format($amount,6); ?> </td>
            <td> <?= number_format($tnx_fees,6); ?> </td>
            <td> <?= number_format($total,6); ?> </td>
        </tr>
        <?php

    }
    ?>

    </tbody>
</table>

<script>
    $('#blockedForBidTables').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
