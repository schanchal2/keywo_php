<?php
session_start();
require_once("../../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../../../model/manageUser/manageUser.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$user_id=getUserInfo($userEmail, $walletURLIP . 'api/v3/', "user_id")["errMsg"]["_id"];
$transaction_toquery="trade";
$result=getTransactionsWallet($userEmail,$user_id,"100",$transaction_toquery,"in","");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];
}

?>
<table class="table text-center table-responsive tblwhite-lastchild" id="TableKwdPurchasesAmount">
    <thead>
    <tr>
        <th> Date/Timestamp </th>
        <th> Keyword Purchased </th>
        <th> Seller </th>
        <!--<th> Country</th>-->
        <th> Trade Price</th>
        <th> Transaction Fees</th>
        <th> Total</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {
        $time=$transaction["time"];
        $amount=$transaction["amount"];
        $trade_fees=explode("~~",$transaction["meta_details"]["highest_bid"])[4];
        $total=$amount+$trade_fees;
        ?>
        <tr>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <td><?= $transaction["meta_details"]["keyword"]; ?></td>
            <td><?= $transaction["payment_mode"]; ?></td>
            <td> <?= number_format($amount,6); ?> </td>
            <td> <?= number_format($trade_fees,6); ?> </td>
            <td> <?= number_format($total,6); ?> </td>
        </tr>
        <?php

    }
    ?>

    </tbody>
</table>

<script>
    $('#TableKwdPurchasesAmount').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>