<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../../model/manageUser/manageUser.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$user_id=getUserInfo($userEmail, $walletURLIP . 'api/v3/', "user_id")["errMsg"]["_id"];
$transaction_toquery="cashout";
$result=getTransactionsWallet($userEmail,$user_id,"100",$transaction_toquery,"in","pending");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];
}

?>

<table class="table text-center table-responsive tblwhite-lastchild" id="TablePendingWithdrawal">
    <thead>
    <tr>
        <th> Timestamp </th>
        <!-- <th> Cashout ID </th>-->
        <th> <?= $adminCurrency; ?> Account </th>
        <th> Amount </th>
        <th> Transaction Fees </th>
        <th> Transferred Amt </th>
        <th> Status </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {
        $type=ucwords(str_replace("_"," ",$type));
        $time=$transaction["time"];
        $amount=$transaction["amount"];
        $fees=$transaction["amount"];
        $transfered=$amount-$fees;
        $status=ucfirst($transaction["transaction_status"]);
        ?>
        <tr>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <!--<td></td>-->
            <td><?= $transaction["sender"]; ?></td>
            <td> <?= number_format($amount,6); ?> </td>
            <td><?= number_format($fees,6); ?> </td>
            <td><?= $transfered; ?></td>
            <td><?= $status; ?></td>
        </tr>
        <?php

    }
    ?>

    </tbody>
</table>

<script>
    $('#TablePendingWithdrawal').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>