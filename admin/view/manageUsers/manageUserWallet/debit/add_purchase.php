<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>
<table class="table text-center table-responsive">
    <thead>
    <tr>
        <th> Invoice Date </th>
        <th> Invoice No. </th>
        <th> Total Cost (<?= $adminCurrency; ?>)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>
            <span class="date"> 01-11-2015 </span>
        </td>
        <td> inv-123</td>
        <td> 2525 <?= $adminCurrency; ?> </td>
    </tr>
    <tr>

        <td>  </td>
        <td> </td>
        <td> 2525 <?= $adminCurrency; ?> </td>

    </tr>

    </tbody>
</table>