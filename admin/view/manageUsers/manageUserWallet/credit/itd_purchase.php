<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../../model/manageUser/manageUser.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$user_id=getUserInfo($userEmail, $walletURLIP . 'api/v3/', "user_id")["errMsg"]["_id"];
$transaction_toquery="purchase_itd";
$result=getTransactionsWallet($userEmail,$user_id,"100",$transaction_toquery,"in","");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];
}

?>

<table class="table text-center table-responsive tblwhite-lastchild" id="itdPurchaseTable">
    <thead>
    <tr>
        <th> Date/Timestamp</th>
        <th><?= $adminCurrency; ?> Purchase ID</th>
        <th> Amt in Deposited Currency</th>
        <th> Amt Converted in ITD </th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {


        $amount_in_purchase_currency=$transaction["meta_details"]["purchase_amount_in_sgd"];
        $amount=$transaction["amount"];
        $time=$transaction["time"];
        ?>
        <tr>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <!--<td></td>-->
            <td><?= $transaction["meta_details"]["buyer_id"]; ?></td>
            <td> <?= number_format($amount_in_purchase_currency,6); ?> </td>
            <td><?= number_format($amount,6); ?> </td>
        </tr>
        <?php

    }
    ?>

    </tbody>
</table>

<script>
    $('#itdPurchaseTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
