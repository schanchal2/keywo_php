<?php
session_start();
require_once("../../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../../model/manageUser/manageUser.php";
$userEmail=base64_decode(trim($_GET["tid"]));
$user_id=getUserInfo($userEmail, $walletURLIP . 'api/v3/', "user_id")["errMsg"]["_id"];
$transaction_toquery="total_kwd_income";
$result=getTransactionsWallet($userEmail,$user_id,"100",$transaction_toquery,"in","");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];
}


?>
<table class="table text-center table-responsive" id="kwd_income_aff_earnings_datas">
    <thead>
    <tr>
        <th> Date/Timestamp</th>
        <th>Keyword</th>
        <th>Amount <?= $adminCurrency; ?></th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($result as $transaction)
    {
        $time=$transaction["time"];
        ?>
        <tr>
            <td>
                <span class="date"> <?= uDateTime("d-m-Y",$time); ?> </span>
                <time> <?= uDateTime("h:i:s",$time); ?></time>
            </td>
            <td><?= $transaction["meta_details"]["keyword"]; ?></td>
            <td> <?= number_format($transaction["amount"],6); ?> </td>
        </tr>
        <?php

    }
    ?>

    </tbody>
</table>

<script>
    $('#kwd_income_aff_earnings_datas').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });
</script>