<?php
include "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
session_start();
require_once("../../model/manageUser/manageUser.php");
require_once("../../model/keywords/keywordsAnalytics.php");
require_once "../../model/keywords/common_keyword.php";

/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/

$email = $_SESSION["admin_id"];
$keyword_sold = getTotalKeyword_Sold($connKeywords);
$keyword_traded = getTotalTradedKeywords($connKeywords);


$keywoStat = getKeywoStat($connKeywords);
$keywoStat = $keywoStat["errMsg"][0];


$totalActiveUsers = $keywoStat["active_users"];
$totalBlockedUsers = $keywoStat["blocked_users"];
$totalDeActiveUsers = $keywoStat["unverified_users"];

$totalUsers = $keywoStat["registered_users"];

$kwdStat=getKeywordStat($connKeywords)["errMsg"];
$TotalkwdTraded=$kwdStat["total_kwd_traded"];
?>

<main>
<div class="container-fluid">



    <div class="title clearfix">
        <h1 class="pull-left">Manage Users</h1><span>UAM</span>
        <form action="" method="POST" class="form-inline pull-right" role="form" name="">
        </form>
    </div>



    <div id="userAnalitics" class="clearfix">
        <table class="table text-center">
            <!-- <thead>
                   <tr>
                       <th></th>
                   </tr>
               </thead> -->
            <tbody>
            <tr>
                <td colspan="3"> Total Users : <?php echo $totalUsers; ?></td>
                <td rowspan="2"> Total keywords Sold</td>
                <td rowspan="2"> Total keywords Traded </td>
            </tr>
            <tr>
                <td>Active</td>
                <td> De-Active</td>
                <td> Blocked</td>
            </tr>
            <tr>
                <td id="ActiveUserAjax"><?php echo $totalActiveUsers; ?> </td>
                <td><?php echo $totalDeActiveUsers; ?></td>
                <td id="BlockedUserAjax"><?php echo $totalBlockedUsers; ?></td>
                <td><?php echo $keyword_sold["errMsg"]["total_sold_kwd"]; ?></td>
                <td><?php echo $TotalkwdTraded; ?></td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="errors"></div>
    <div class="row">
        <div class="col-lg-12">
            <div id="filter">
                <form action="#" method="POST" class="form-inline" role="form" name="filter">
                    <div class="form-group">
                        <label class="sr-only" for="">Search by Email</label>
                        <input type="email" class="form-control" id="useremail" placeholder="Search by Email">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="">First Name</label>
                        <input type="email" class="form-control" id="userfname" placeholder="First Name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="">Last Name</label>
                        <input type="email" class="form-control" id="userlname" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="">IP Adress</label>
                        <input type="email" class="form-control" id="useripaddress" placeholder="IP Adress">
                    </div>
                    <button type="button" onclick="getUserList()" class="btn btn-primary"><i class="fa fa-search"></i></button>
                    <?php
                    if((in_array("write", $myp))) {
                    ?>
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-default dropdown-toggle bg-red text-white" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-user "></i> Account Actions <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu drop-down">
                            <li><a href="javascript:;"
                                   onclick="changeStatusUserBulk('2');"><i class="fa fa-ban fa-fw"></i> Block</a>
                            </li>
                            <li><a href="javascript:;"
                                   onclick="changeStatusUserBulk('1');"><i class="fa fa-unlock fa-fw"></i> Un-Block</a>
                            </li>

                        </ul>
                    </div>
                    <?php } ?>
                </form>
                <!-- Single button -->
            </div>
        </div>
    </div>
    <div id="userDataList">
</div>
</div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script>

    $( document ).ready(function() {
        getUserList();
    });

    $("#useremail,#userfname,#userlname,#useripaddress").keyup(function(){
        getUserList();
    });
    function getUserList()
    {

        var useremail=$("#useremail").val();
        var userfname=$("#userfname").val();
        var userlname=$("#userlname").val();
        var useripaddress=$("#useripaddress").val();

        var limit=8;

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/manageUser/getUserList.php",
            data: {
                useremail: useremail, userfname: userfname,userlname:userlname,useripaddress:useripaddress,limit:limit,listtype:'first'
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                $("#userDataList").html("");
                $("#userDataList").html(data);
                $('#loadng-image').hide();

            },
            error: function () {
                alert("fail");
            }
        });

    }
    function getUserListPagination(pageno,email,column,order,fname,lname,userip)
    {
        var limit=8;

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/manageUser/getUserList.php",
            beforeSend: function(){
                $('#loadng-image').show();
            },
            data: {
                useremail: email, userfname: fname,userlname:lname,useripaddress:userip,limit:limit,listtype:'pagination',pageno:pageno,column:column,order:order
            },
            success: function (data) {

                $("#userDataList").html("");
                $("#userDataList").html(data);
                $('#loadng-image').hide();

            },
            error: function () {
                alert("fail");
            }
        });
    }




    function changeStatusUserBulk(status) {


        var resonToblock = "";
        var serviceReqID = "";
        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var emails = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring = (tags[i].value).trim();
                var splitmystring = mystring.split(",");

                var useremail = splitmystring[0]; //email now changed to id
                var userstatus = splitmystring[1];
                var resetid = splitmystring[2];
                var userid = splitmystring[3];
                if (status !== userstatus) {
                    if (userstatus == 2 || userstatus == 1) {
                        emails.push(useremail + ":" + userid + ":" + userstatus);
                    } else {
                        $("#" + resetid).trigger("click");
                        console.log("no need to change status");
                    }

                } else {
                    $("#" + resetid).trigger("click");
                    console.log("no need to change status");
                }
            }

        }

        if (status == 2) {
            if (emails.length > 0) {

                bootbox.confirm({
                    message: "<h4>Are you sure to change user active status?</h4>",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                       if(result==true)
                       {
                           bootbox.confirm({
                               title: "Enter Following details to block Users.",
                               message: '<form><div class="form-group"> <label for="reason">Reason to Block:</label><textarea maxlength="350" type="reason" class="form-control" id="reason"></textarea> </div> <div class="form-group"> <label for="srid">Service Request ID:</label> <input type="srid" class="form-control" id="srid"> </div></form>',
                               callback: function (result) {
                                   resonToblock=$("#reason").val();
                                   serviceReqID=$("#srid").val();
                                   changeStatus(emails,status,resonToblock,serviceReqID)
                               }
                           });


                       }else {
                           console.log("no selected");
                       }
                    }
                });
            }

        }else {
            if (emails.length > 0) {
                bootbox.confirm({
                    message: "<h4>Are you sure to change user active status?</h4>",
                    buttons: {
                        confirm: {
                            label: 'Yes',
                            className: 'btn-success'
                        },
                        cancel: {
                            label: 'No',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result == true) {
                            changeStatus(emails, status, resonToblock, serviceReqID);
                        } else {
                            console.log("no selected");
                        }
                    }
                });
            }
        }
    }

        function changeStatus(emails,status,resonToblock,serviceReqID)
        {
            var pageno= $("#hiddenpage").val();

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../controller/manageUser/manageUserBulkAct.php",
                data: {
                    email: emails, status: status,resonToblock:resonToblock,serviceReqID:serviceReqID
                },
                beforeSend: function(){
                   // $('#loadng-image').show();
                },
                success: function (user) {

                    if (user["errCode"]) {
                        if (user["errCode"] == "-1") {

                            var useremail=$("#useremail").val();
                            var userfname=$("#userfname").val();
                            var userlname=$("#userlname").val();
                            var useripaddress=$("#useripaddress").val();


                            getUserListPagination(pageno,useremail,"","",userfname,userlname,useripaddress);
                            getLatestUMAnalytics();
                            $('#loadng-image').hide();
                            $("#errors").html(bsAlert("success",user["errMsg"]));


                        }
                        else {


                            getUserListPagination(pageno,useremail,"","",userfname,userlname,useripaddress);
                            getLatestUMAnalytics();

                            $("#errors").html(bsAlert("danger",user["errMsg"]));
                            $('#loadng-image').hide();

                        }

                    }

                },
                error: function () {
                    console.log("Failed to change user status");
                }
            });
        }


    function getLatestUMAnalytics()
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/manageUser/getLatestAnalytics.php",
            data: {
                 type: "afterBlockUnblockUser"
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (user) {

                if (user["errCode"]) {
                    if (user["errCode"] == "-1") {
                        var active=user["errMsg"]["active_users"];
                        var blocked=user["errMsg"]["blocked_users"];

                        $("#BlockedUserAjax").html("");
                        $("#BlockedUserAjax").html(blocked);
                        $("#ActiveUserAjax").html("");
                        $("#ActiveUserAjax").html(active);
                        $('#loadng-image').hide();
                    }
                    else {
                        console.log("Failed to change analytics");
                    }

                }

            },
            error: function () {
                console.log("Failed to get analytics");
            }
        });
    }




</script>

<?php include "{$docRootAdmin}view/layout/footer.php" ?>