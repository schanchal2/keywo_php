<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}/model/keywords/common_keyword.php");
require_once "{$docRootAdmin}model/acl/checkAccess.php";
require_once("{$docRootAdmin}model/serviceRequest/serviceRequestModel.php");

/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
?>
    <link rel="stylesheet"
          href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">
    <style>
        #toTop {
            position: fixed;
            bottom: 10px;
            right: 10px;
            cursor: pointer;
            display: none;
        }
    </style>
    <main class="">
        <?php
        include "../../model/manageUser/manageUser.php";
        $usere_id = base64_decode(trim($_GET["tid"]));
        $result = getUserSummary($usere_id);
        $result = $result["errMsg"];
        $username = $result["first_name"] . " " . $result["last_name"];
        $registeredTime = uDateTime("m-d-Y",$result["creationTime"]);
        $activeStatus = $result["active"];
        $trend_preference = (int)$result["trend_preference"];
        if ($trend_preference == 1) {
            $trend_preferencedisplay = "Default";
        } else if ($trend_preference == 2) {
            $trend_preferencedisplay = "2FA";
        }
        $country = $result["country"];
        $last_used_ip = count($result["last_used_ip"]);


        $keywords = getMyKeywordDetails($connKeywords, $usere_id);
        $keywordsDataCount = count(json_decode($keywords["errMsg"]["transaction_details"], true));

        $queryTicketsCount=getQueryTicketsCount($usere_id,$connKeywords)["errMsg"][0]["count"];

        $srDatacounr=userLastServiceRequests($connKeywords,"count", $usere_id)["data"][0]["count"];

        ?>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Manage Users</h1><span>UAM</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <div id="userList" class="m-t-15">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Sign-Up Date</th>
                        <th>Country</th>
                        <th>Balance</th>
                        <th>Account Status</th>
                        <th>Authentication</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <div>
                            <td>
                                <a href="#">
                                    <?php echo $username; ?>
                                </a>
                            </td>
                            <td>
                                <?php echo $usere_id; ?>
                            </td>
                            <td>
                                <?php echo $registeredTime; ?>
                            </td>
                            <td>
                                <?php echo $country; ?>
                            </td>
                            <td>
                                <?php echo getUsersBalance($usere_id)["errMsg"]; ?> <?= $adminCurrency; ?>
                            </td>
                            <td class="text-success">
                                <?php
                                if ($activeStatus == "0") {
                                    echo "<span class=\"text-danger\">Deactive</span>";
                                }

                                if ($activeStatus == "1") {
                                    echo "<span class=\"text-success\">Active</span>";
                                }

                                if ($activeStatus == "2") {
                                    echo "<span style=\"color:black\">Blocked</span>";
                                }
                                ?>
                            </td>

                            <td> <?php echo $trend_preferencedisplay; ?></td>
                        </div>
                    </tr>
                    </tbody>
                </table>
            </div>
            <ul id="manageUserTabs" class="nav nav-pills nav-justified clearfix" role="tablist">
                <li role="presentation" class="no-badge"><a href="javascript:;" data-value="tabsData/qucikTwenty.php?tid=<?php echo base64_encode($usere_id); ?>">Quick
                        Overview</a></li>
                <li role="presentation" class="no-badge"><a href="javascript:;" id="walletDataLoad"
                                           data-value="manageUserWallet/manageUserWallet.php?tid=<?php echo base64_encode($usere_id); ?>">
                        Wallet</a></li>
                <li role="presentation" class="no-badge"><a href="javascript:;" data-value="tabsData/referral.php?tid=<?php echo base64_encode($usere_id); ?>">Referral <!--<span
                                class="badge">3</span>--></a></li>
                <li role="presentation"><a href="javascript:;" data-value="tabsData/queryTicket.php?tid=<?php echo base64_encode($usere_id); ?>">Query Ticket <span
                                class="badge"><?= $queryTicketsCount; ?></span> </a></li>
                <li role="presentation"><a href="javascript:;" data-value="tabsData/serviceRequest.php?tid=<?php echo base64_encode($usere_id); ?>"> Service Request
                        <span class="badge"><?= $srDatacounr; ?></span></a></li>
                <li role="presentation"><a href="javascript:;"
                                           data-value="tabsData/kwdDetails.php?tid=<?php echo base64_encode($usere_id); ?>">Keyword
                        <span class="badge"><?= $keywordsDataCount; ?></span></a></li>
                <li role="presentation"><a href="javascript:;"
                                           data-value="tabsData/logsData.php?tid=<?php echo base64_encode($usere_id); ?>">Log
                        Files </a></li>
                <li role="presentation" class="badge"><a href="javascript:;"
                                                            data-value="tabsData/ipUsed.php?tid=<?php echo base64_encode($usere_id); ?>">IP's
                        Used <span class="badge"><?= $last_used_ip; ?></span></a></li>
               <!-- <li role="presentation" class="no-badge"><a href="javascript:;" data-value="tabsData/addCampagian.php">Ad
                        - Campaigns </a></li>-->
            </ul>
            <div id="manageUserTabPanel">
            </div>

        </div>
    </main>
    </div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php"; ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script type="text/javascript">
        // in main panal > manage user page
        $('#manageUserTabs a[data-value]').on('click', function (event) {
            event.preventDefault();
            $(this).addClass('active');

            var sPageName = $(this).data('value');
            $.get(sPageName, function (data) {
                $("#manageUserTabPanel").html(data);
            }).done(function () {

            });
        });


        $(document).ready(function () {
            $('#walletDataLoad').click();
            $('#sidebarChanger').click();

            $('body').append('<div id="toTop" class="btn btn-info" style="background-color: #027a98;border-color: #0b0c0c;" ><span class="fa fa-angle-up"></span> Back to Top</div>');
            $(window).scroll(function () {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });
            $('#toTop').click(function () {
                $("html, body").animate({scrollTop: 0}, 600);
                return false;
            });
        });
    </script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>