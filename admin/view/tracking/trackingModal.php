<?php
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("{$docRootAdmin}model/tracking/tracklist/tracklist.php");
$resultCategory=getAllTracklistCategory($connAdmin);
$resultCategory=$resultCategory["errMsg"];

?>


<div class="modal fade" id="addTotracklistmodalBulk" tabindex="-1" role="dialog"
     aria-labelledby="addTotracklistmodalBulk" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">

    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                    <button type="button" style="opacity: 1;font-size: xx-large;color: white;"class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="addTotracklistmodalLabelBulk"
                        style="color:white;font-weight: bold">Add To Tracklist (Bulk)</h4>
                </div>
                <div class="modal-body">
                    <form>
                    <div class="register-box-body" id="addTotracklistmodalbodyBulk">
                        <div class="title clearfix" style="border-bottom: none">


                                <p style="font-size: x-small;">
                                    NOTE: While adding subject to tracklist.If Subject is already added in tracklist then it will not added from current bulk add request.
                                </p>

                            <br/>
                            <!--<div class="form-inline pull-right" role="form" name="">
                                <label class="dropdownOptions">
                                    <select id="tracklistOptionsBulk" style="height: 33px;width: 360px;" class="form-control">
                                        <option value="" selected disabled>Select Folder</option>
                                        <?php

//                                        foreach($resultCategory as $value)
//                                        {
//                                            echo $value["cat_name"];
//                                            echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_id']."'>".$value['cat_name']."</option>";
//
//                                        }
                                        ?>
                                    </select>
                                </label>
                            </div>-->
                        </div>

                        <!--<div id="showSubTracklistBulk">

                        </div>-->

                        <div class="row" style="margin-top:20px">
                            <div class="col-lg-2">
                                <h5>Notes:</h5>
                            </div>
                            <div class="col-lg-10">

                                    <div class="form-group">
                                        <textarea class="form-control" maxlength="450" id="tracklistTextareaDataBulk" rows="4" cols="50" id="comment"></textarea>
                                    </div>

                            </div>
                        </div>

                        <div class="row" style="">
                            <div class="col-lg-4 ">
                            </div>
                            <div class=" pull-right" style="margin: 0px;padding: 0px">
                                <div class="col-lg-4 ">
                                    <button type="button" id="AddtoTracklistBTN-Bulk"style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default" onclick="addinTracklistBulk()">Add to Tracklist</button>
                                </div>

                                <!--<div class="col-lg-4">
                                    <button type="button" style="padding: 6px 12px;border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default">Cancel</button>
                                </div>-->
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>



<div class="modal fade" id="addTotracklistmodal" tabindex="-1" role="dialog"
     aria-labelledby="addTotracklistmodal" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                    <button type="button" style="opacity: 1;font-size: xx-large;color: white;"class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="addTotracklistmodalLabel"
                        style="color:white;font-weight: bold">Add To Tracklist </h4>
                </div>
                <div class="modal-body">
                    <form>
                    <div class="register-box-body" id="addTotracklistmodalbody">
                        <!--<div class="title clearfix" style="border-bottom: none">
                            <div class="form-inline pull-right" role="form" name="">
                                <label class="dropdownOptions">
                                    <select id="tracklistOptions" style="height: 33px;width: 360px;" class="form-control">
                                        <option value="" selected disabled>Select Folder</option>
                                        <?php

                                      /*  foreach($resultCategory as $value)
                                        {
                                            echo $value["cat_name"];
                                            echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_id']."'>".$value['cat_name']."</option>";

                                        }*/
                                        ?>
                                    </select>
                                </label>
                            </div>
                        </div>

                        <div id="showSubTracklist">

                        </div>-->

                        <div class="row" style="margin-top:20px">
                            <div class="col-lg-2">
                                <h5>Notes:</h5>
                            </div>
                            <div class="col-lg-10">

                                    <div class="form-group">
                                        <textarea class="form-control" maxlength="450" id="tracklistTextareaData" rows="4" cols="50" id="comment"></textarea>
                                    </div>

                            </div>
                        </div>

                        <div class="row" style="">
                            <div class="col-lg-4 ">
                            </div>
                            <div class=" pull-right" style="margin: 0px;padding: 0px">
                                <div class="col-lg-4 ">
                                    <button type="button" style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;"  id="btnAddToTracklist" class="btn btn-default" onclick="addinTracklist()">Add to Tracklist</button>
                                </div>

                                <!--<div class="col-lg-4">
                                    <button type="button" style="padding: 6px 12px;border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default">Cancel</button>
                                </div>-->
                            </div>
                        </div>
                    </div>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="moveUpdatetracklistmodal" tabindex="-1" role="dialog" aria-labelledby="moveUpdatetracklistmodal" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                    <button type="button" style="opacity: 1;font-size: xx-large;color: white;"class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="moveUpdatetracklistLabel" style="color:white;font-weight: bold">Edit Tracklist Data</h4>
                </div>
                <div class="modal-body">
                    <div id="redError" style="color:red;padding-bottom: 7px"></div>
                    <form>
                        <div class="register-box-body" id="moveUpdatetracklistBody">
                            <!-- <div class="clearfix" style="border-bottom: none"> -->
                                 <div class="row m-b-20">
                                    <div class="col-xs-10 col-xs-offset-2">
                                    <label class="dropdownOptions">
                                        <select id="tracklistOptionsnew" class="form-control" >
                                            <option value="" selected disabled>Select Folder</option>
                                            <?php

                                            foreach($resultCategory as $value)
                                            {
                                                echo $value["cat_name"];
                                                echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_id']."'>".$value['cat_name']."</option>";

                                            }
                                            ?>
                                        </select>
                                    </label>
                                    </div>
                                </div>
                            <!-- </div> -->
                            <div id="showSubTracklistedit">
                            </div>
                            <div class="row" style="margin-top:20px">
                                <div class="col-lg-2">
                                    <h5>Notes:</h5>
                                </div>
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        <textarea class="form-control" maxlength="450" id="tracklistTextareaDataEdit" rows="4" cols="50" id="comment"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="">
                                <div class="col-lg-4 ">
                                </div>
                                <div class=" pull-right" style="margin: 0px;padding: 0px">
                                    <div class="col-lg-4 ">
                                        <button type="button" style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default" onclick="updateTracklistSingle()">Update</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="moveUpdatetracklistmodalbulk" tabindex="-1" role="dialog"
     aria-labelledby="moveUpdatetracklistmodalbulk" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">
    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                    <button type="button" style="opacity: 1;font-size: xx-large;color: white;"class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="moveUpdatetracklistLabelbulk"
                        style="color:white;font-weight: bold">Move/Edit/Delete Tracklist Data</h4>
                </div>
                <div class="modal-body">


                    <ul class="nav nav nav-pills" style="background: #97e5f8;">
                        <li class="active"><a data-toggle="pill" href="#move">Move</a></li>
                        <li><a data-toggle="pill" href="#delete">Delete</a></li>
                    </ul>

                    <div class="tab-content">

                        <div id="move" class="tab-pane fade in active"><br/>
                            <form>
                            <div class="register-box-body" id="moveUpdatetracklistBodybulk">
                                <div class="title clearfix" style="border-bottom: none">
                                    <div class="form-inline pull-right" role="form" name="">
                                        <label class="dropdownOptions">
                                            <select id="tracklistOptionsnewbulk" style="height: 33px;width: 360px;" class="form-control">
                                                <option value="" selected disabled>Select Folder</option>
                                                <?php

                                                foreach($resultCategory as $value)
                                                {
                                                    echo $value["cat_name"];
                                                    echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_id']."'>".$value['cat_name']."</option>";

                                                }
                                                ?>
                                            </select>
                                        </label>
                                    </div>
                                </div>

                                <div id="showSubTracklisteditbulk">

                                </div>

                                <div class="row" style="margin-top:20px" hidden>
                                    <div class="col-lg-2">
                                        <h5>Notes:</h5>
                                    </div>
                                    <div class="col-lg-10">

                                            <div class="form-group">
                                                <textarea class="form-control" maxlength="450" id="tracklistTextareaDataEditbulk" rows="4" cols="50" id="comment"></textarea>
                                            </div>

                                    </div>
                                </div>

                                <div class="row" style="">
                                    <div class="col-lg-4 ">
                                    </div>
                                    <div class=" pull-right" style="margin: 0px;padding: 0px">
                                        <div class="col-lg-4 ">
                                            <button type="button" style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default" onclick="updateMoveTracklistBulkGlobal()">Move</button>
                                        </div>

                                    </div>

                                </div>

                            </div>
                                </form>
                        </div>
                        <div id="delete" class="tab-pane fade"><br/>
                            <div class="row" style="">
                                <div class="col-lg-4 ">
                                </div>
                                <div class=" pull-right" style="margin: 0px;padding: 0px">
                                    <div class="col-lg-4 ">
                                        <button type="button" style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default" onclick="deletemyTracklistBulk()">Delete</button>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>

    var adminroot="<?php echo $adminRoot; ?>";
    //var subtracklistOptions='<div class="title clearfix" style="border-bottom: none"> <form action="" method="POST" class="form-inline pull-right" role="form" name=""> <label class="dropdownOptions"> <select id="subtracklistOptions" style="height: 33px;width: 360px;" class="form-control" data-placeholder="Choose a Sub-Category..." class="chosen-select"> <option value="general">General</option> <option value="intraction">Intraction</option> <option value="userwise">Userwise</option> <option value="keywordwise">Keywordwise</option> <option value="postwise">Postwise</option> <option value="adwise">Ad-wise</option> </select> </label> </form> </div>';
    $('#tracklistOptions').on('change', function() {
        var mycategoryid=$('#tracklistOptions').val();
       getSubCategoryByMainCategoryID(mycategoryid,adminroot,"single");
        $("#redError").html("");
    });

    $('#tracklistOptionsBulk').on('change', function() {
        var mycategoryid=$('#tracklistOptionsBulk').val();
        getSubCategoryByMainCategoryID(mycategoryid,adminroot,"bulk");
        $("#redError").html("");
    });


    $('#tracklistOptionsnew').on('change', function() {
        var mycategoryid=$('#tracklistOptionsnew').val();
        getSubCategoryByMainCategoryID(mycategoryid,adminroot,"editOne");
        $("#redError").html("");
    });

    $('#tracklistOptionsnewbulk').on('change', function() {
        var mycategoryid=$('#tracklistOptionsnewbulk').val();
        getSubCategoryByMainCategoryID(mycategoryid,adminroot,"editBulk");
        $("#redError").html("");
    });




    function getSubCategoryByMainCategoryID(catID,adminroot,status)
    {
        var cttype;

        if(status=="single")
        {
            cttype="singleSelect";
        }
        if(status=="bulk")
        {
            cttype="multiSelect";
        }

        if(status=="editOne")
        {
            cttype="editOne";
        }

        if(status=="editBulk")
        {
            cttype="editBulk";
        }

       $.ajax({
            type: "POST",
            dataType: "html",
            url: adminroot+"controller/tracking/tracklist/getAllCategory.php",
            data: {
                categoryType: cttype, categoryID: catID
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();

                if(status=="single")
                {
                    $("#showSubTracklist").html("");
                    $("#showSubTracklist").html(data);
                }

                if(status=="bulk")
                {
                    $("#showSubTracklistBulk").html("");
                    $("#showSubTracklistBulk").html(data);
                }

                if(status=="editOne")
                {
                    $("#showSubTracklistedit").html("");
                    $("#showSubTracklistedit").html(data);
                }

                if(status=="editBulk")
                {
                    $("#showSubTracklisteditbulk").html("");
                    $("#showSubTracklisteditbulk").html(data);
                }

            },
            error: function () {
                alert("fail");
            }
        });

    }



    function addUserToTracklist()
    {
        var categoryID=$("#tracklistOptions").val();
        var SubcategoryID=$("#subtracklistOptions").val();
        var SubjectOwner=$("#finalsubjectownermodal").val();
        var Subject=$("#finalsubjectmodal").val();
        var notes=$("#tracklistTextareaData").val();

        var subListData= $("#subtracklistOptions").find('option:selected').text();

        if(categoryID=="" || categoryID==null)
        {
            bootbox.alert("<h4>Please Select Category</h4>", function() {
            });
            return false;
        }

        if(SubcategoryID=="" || SubcategoryID==null)
        {

            bootbox.alert("<h4>Please Select Sub Category</h4>", function() {
            });
            return false;
        }

        var status;

        operateLadda("btnAddToTracklist","start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: adminroot+"controller/tracking/tracklist/addToTracklist.php",
            data: {
                categoryID: categoryID, SubcategoryID: SubcategoryID,SubjectOwner:SubjectOwner,Subject:Subject,subListData:subListData,notes:notes,processType:"single"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                operateLadda("btnAddToTracklist","stop");
                $('#loadng-image').hide();
                status=data;
            },
            error: function () {
               console.log("Error in adding to tracklist");
            }
        });

        return status;

    }

    function addUserToTracklistBulk()
    {

        /******************************* ***************************************/

        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var emails = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var useremail=splitmystring[0]; //email now changed to id
                var subject=splitmystring[1];

                var pushData=useremail+':'+subject;
                emails.push(pushData);

            }

        }


        if(emails.length === 0)
        {
            $("#addTotracklistmodalBulk .close").click();
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }

        /******************************* ***************************************/


        var categoryID=$("#tracklistOptionsBulk").val();
        var SubcategoryID=$("#subtracklistOptionsbulk").val();
        var notes=$("#tracklistTextareaDataBulk").val();

        var subListData= $("#subtracklistOptionsbulk").find('option:selected').text();


      /*  if(categoryID=="" || categoryID==null)
        {
            bootbox.alert("<h4>Please Select Folder</h4>", function() {
            });
            return false;
        }

        if(SubcategoryID=="" || SubcategoryID==null)
        {

            bootbox.alert("<h4>Please Select Sub Folder</h4>", function() {
            });
            return false;
        }*/



        var status;
        operateLadda("AddtoTracklistBTN-Bulk","start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: adminroot+"controller/tracking/tracklist/addToTracklist.php",
            data: {
                categoryID: categoryID, SubcategoryID: SubcategoryID,SubjectOwner:"",Subject:"",subListData:subListData,notes:notes,subjectNowner:emails,processType:"bulk"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                operateLadda("AddtoTracklistBTN-Bulk","stop");
                $('#loadng-image').hide();

                status=data;
            },
            error: function () {
                console.log("Error in adding to tracklist");
            }
        });

        return status;

    }

    function addElementToModal(SubjectOwner,Subject)
    {
       //var subject = [];
      //subject.push({"SubjectOwner" : SubjectOwner});
      // subject.push({"Subject" : Subject});
        $("#finalsubjectownermodal").remove();
        $("#finalsubjectmodal").remove();

       var subjectOwnInput="<input type='text' id='finalsubjectownermodal' value='"+SubjectOwner+"' hidden>";
       var subjectInput="<input type='text' id='finalsubjectmodal' value='"+Subject+"' hidden>";
       var data=subjectOwnInput+subjectInput;
        $("#addTotracklistmodal").append(data);



    }




    function deleteTracklist(catid,subcatidn,adminid,Subject,subOwner){
        var status;
        /*bootbox.confirm({
            title: 'Delete Tracklist!!',
            message: "Are you sure delete tracklist !",
            buttons: {
                'cancel': {
                    label: 'Cancel',
                    className: 'btn-default'
                },
                'confirm': {
                    label: 'Continue',
                    className: 'btn-success pull-right'
                }
            },
            callback: function(result) {

                if (result) {*/

                    $.ajax({

                        type: "POST",
                        dataType:"json",
                        url: adminroot+"controller/tracking/tracklist/crudTracklist.php",
                        data: {
                            catid: catid,subcatidn:subcatidn,adminid:adminid,Subject:Subject,subOwner:subOwner,crudType:"delete"
                        },
                        async: false,
                        beforeSend: function(){
                            $('#loadng-image').show();
                        },
                        success: function(user) {
                            $('#loadng-image').hide();
                           status=user;
                            //status.push(user);


                        },
                        error: function() {

                            console.log("Failed to  delete Tracklist");
                        }
                    });

                /*}

            }


        });*/


        return status;

    }


    function deleteTracklistBulk()
    {
        /******************************* ***************************************/

        var status;
        var tags = document.getElementsByName('user_checkbox[]');
        var subjectids = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var subjectid=splitmystring[0];
                var adminid=splitmystring[1];

                var mydata=subjectid+":"+adminid;
                subjectids.push(mydata);

            }

        }

        if(subjectids.length === 0)
        {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }


        $.ajax({

            type: "POST",
            dataType:"json",
            url: adminroot+"controller/tracking/tracklist/crudTracklist.php",
            data: {
                subjectids: subjectids,crudType:"bulkdelete"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function(user) {

                status=user;
                $('#loadng-image').hide();

            },
            error: function() {

                console.log("Failed to  delete Tracklist");
            }
        });
        return status;

    }


    /************************** update tracklist single ******************/

    function updateMoveTracklist(subjectid,catid,subcatid,subject_owner,subjecttomove,noteold)
    {
        $("#subjectIDeditModal").remove();
        $("#catIDeditModal").remove();
        $("#subcatIDeditModal").remove();
        $("#subject_ownerTLIST").remove();
        $("#subjecttomoveTLIST").remove();

        var subjectID="<input type='text' id='subjectIDeditModal' value='"+subjectid+"' hidden>";
        var catids="<input type='text' id='catIDeditModal' value='"+catid+"' hidden>";
        var subcatids="<input type='text' id='subcatIDeditModal' value='"+subcatid+"' hidden>";
        var subject_ownerTLIST="<input type='text' id='subject_ownerTLIST' value='"+subject_owner+"' hidden>";
        var subjecttomoveTLIST="<input type='text' id='subjecttomoveTLIST' value='"+subjecttomove+"' hidden>";
        var noteoldTLIST="<input type='text' id='noteoldTLIST' value='"+noteold+"' hidden>";
        var data=subjectID+catids+subcatids+subject_ownerTLIST+subjecttomoveTLIST+noteoldTLIST;
        $("#moveUpdatetracklistmodal").append(data);


    }


    function UpdateFromTracklist()
    {
        var status;
        var subjectToEdit= $("#subjectIDeditModal").val();

        var catidold=$("#catIDeditModal").val();
        var subcatidold=$("#subcatIDeditModal").val();


        var catidnew=$("#tracklistOptionsnew").val();
        var subcatidnew=$("#subtracklistOptionsEditOne").val();
        var subject_owner=$("#subject_ownerTLIST").val();
        var subjecttomove=$("#subjecttomoveTLIST").val();
        var noteold=$("#noteoldTLIST").val();

        if(catidold==catidnew && subcatidold==subcatidnew)
        {
            bootbox.alert("<h4>Please Change Folder Or Subfolder.</h4>", function() {
            });

            return false;
        }


        if(catidnew==null)
        {
            $("#redError").html("");
            $("#redError").html("Please select category");
            return false;
        }

        if(subcatidnew==null)
        {
            $("#redError").html("");
            $("#redError").html("Please select sub-category");
            return false;
        }



        var Note=$("#tracklistTextareaDataEdit").val();

        if(Note=="")
        {
            $("#redError").html("");
            $("#redError").html("Pleas add note details.");
            return false;
        }

        $.ajax({

            type: "POST",
            dataType:"json",
            url: adminroot+"controller/tracking/tracklist/crudTracklist.php",
            data: {
                subjectToEdit: subjectToEdit,catidnew:catidnew,subcatidnew:subcatidnew,Note:Note,subject_owner:subject_owner,subjecttomove:subjecttomove,catidold:catidold,subcatidold:subcatidold,noteold:noteold,crudType:"editSingle"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function(user) {
                $('#loadng-image').hide();
                status=user;

            },
            error: function() {

                console.log("Failed to  Edit Tracklist");
            }
        });
        return status;

    }
    /************************** update tracklist single ******************/




    function updateMoveTracklistBulk() //Bulk
    {
        /******************************* ***************************************/

        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var subjectids = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var subjectid=splitmystring[0]; //email now changed to id
                var subject_owner=splitmystring[3]; //email now changed to id
                var subject=splitmystring[4]; //email now changed to id
                var cat_id=splitmystring[5]; //email now changed to id
                var sub_cat_id=splitmystring[6]; //email now changed to id
                var noteold=splitmystring[7]; //email now changed to id

                subjectids.push(subjectid+":"+subject_owner+":"+subject+":"+cat_id+":"+sub_cat_id+":"+noteold);

            }

        }

        if(subjectids.length === 0)
        {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }

        var categoryID=$("#tracklistOptionsnewbulk").val();
        var SubcategoryID=$("#tracklistOptionsnewbulksub").val();
        var notes=$("#tracklistTextareaDataEditbulk").val();

        var subListData= $("#tracklistOptionsnewbulksub").find('option:selected').text();

        if(categoryID=="" || categoryID==null)
        {
            bootbox.alert("<h4>Please Select Folder</h4>", function() {
            });
            return false;
        }

        if(SubcategoryID=="" || SubcategoryID==null)
        {

            bootbox.alert("<h4>Please Select Sub Folder</h4>", function() {
            });
            return false;
        }



        var Note=$("#tracklistTextareaDataEditbulk").val();

        $.ajax({

            type: "POST",
            dataType:"json",
            url: adminroot+"controller/tracking/tracklist/crudTracklist.php",
            data: {
                subjectToEdit: subjectids,categoryID:categoryID,SubcategoryID:SubcategoryID,Note:Note,crudType:"editBulkMove"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function(user) {
                $('#loadng-image').hide();

                status=user;

            },
            error: function() {

                console.log("Failed to  Edit Tracklist");
            }
        });
        return status;
        /******************************* ***************************************/
    }


    $('.modal').on('hidden.bs.modal', function(){
        $(this).find('form')[0].reset();
    });


    function moveUpdatetracklistmodalbulkcheck()
    {
        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var subjectids = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var subjectid=splitmystring[0]; //email now changed to id

                subjectids.push(subjectid);

            }

        }


        if(subjectids.length === 0)
        {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }

        $('#moveUpdatetracklistmodalbulk').modal('show');
    }

    function addTotracklistmodalBulkCheck()
    {
        var status = status;
        var tags = document.getElementsByName('user_checkbox[]');
        var emails = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var useremail=splitmystring[0]; //email now changed to id
                var subject=splitmystring[1];

                var pushData=useremail+':'+subject;
                emails.push(pushData);

            }

        }


        if(emails.length === 0)
        {
            $("#addTotracklistmodalBulk .close").click();
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }
        $('#addTotracklistmodalBulk').modal('show');
    }

    /* //for removing duplicate code
     function getAlladdTotracklistmodalBulkElement()
     {

     var status = status;
     var tags = document.getElementsByName('user_checkbox[]');
     var emails = new Array();
     for (var i = 0; i < tags.length; ++i) {
     if (tags[i].checked) {

     // emails.push(tags[i].value);
     var mystring=(tags[i].value).trim();
     var splitmystring=mystring.split(",");

     var useremail=splitmystring[0]; //email now changed to id
     var subject=splitmystring[1];

     var pushData=useremail+':'+subject;
     emails.push(pushData);

     }

     }


     if(emails.length === 0)
     {
     $("#addTotracklistmodalBulk .close").click();
     bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
     });
     return false;
     }else
     {
     return true;
     }
     }
     function addTotracklistmodalBulkCheck()
     {
     var status=getAlladdTotracklistmodalBulkElement();

     if(status==true)
     {
     $('#addTotracklistmodalBulk').modal('show');
     }

     }
     */



    $('#moveUpdatetracklistmodal').on('show.bs.modal', function (e) {
        $("#redError").html("");
    });

    $("#tracklistTextareaDataEdit").keyup(function(){
        $("#redError").html("");
    });
</script>