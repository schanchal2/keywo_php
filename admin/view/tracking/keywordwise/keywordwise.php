<?php
session_start();

include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">
<main>
    <div class="container-fluid">



        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Keywordwise</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                <label class="dropdownOptions">
                    <select id="callingPageName" class="form-control">
                        <option value="KeywordInteractionSearch" selected>Keyword Interaction (Search)</option>
                        <option value="KeywordEarnings">Keyword Earnings (#bitcoins)</option>
                        <option value="KeywordAverageEarnings">Keyword Average Earnings</option>
                        <option value="KeywordsDueForRenewal">Keywords Due For Renewal</option>
                        <option value="Bids">Keywords Bid</option>
                        <option value="KeywordTrade">Keyword Trade</option>
                        <option value="KeywordSubscribed">Keyword Subscribed (sold)</option>
                        <option value="TradingFees">Trading Fees</option>
                        <option value="RenewalFees">Renewal Fees</option>
                        <!--<option value="Refund">Refund (Cancel Subscribtion)</option>
                        <option value="TradeReversal">Trade Reversal</option>-->
                    </select>
                </label>
            </form>
        </div>
        <div id="callPageAjaxData">

        </div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>

<?php include "../trackingModal.php" ?>
<script>


    $('#callingPageName').on('change', function() {
        var pagename=this.value;

        switchTarget(pagename);
    });

    function switchTarget(pagename) {
        switch (pagename) {
            case "KeywordInteractionSearch":
                $("#callPageAjaxData").load("KeywordInteractionSearch.php");
                break;

            case "KeywordEarnings":
                $("#callPageAjaxData").load("KeywordEarnings.php");
                break;

            case "KeywordTrade":
                $("#callPageAjaxData").load("KeywordTrade.php");
                break;

            case "KeywordSubscribed":
                $("#callPageAjaxData").load("KeywordSubscribed.php");
                break;

            case "Bids":
                $("#callPageAjaxData").load("Bids.php");
                break;

            case "Refund":
                $("#callPageAjaxData").load("Refund.php");
                break;

            case "TradeReversal":
                $("#callPageAjaxData").load("TradeReversal.php");
                break;

            case "KeywordAverageEarnings":
                $("#callPageAjaxData").load("KeywordAverageEarnings.php");
                break;

            case "TradingFees":
                $("#callPageAjaxData").load("TradingFees.php");
                break;

            case "RenewalFees":
                $("#callPageAjaxData").load("RenewalFees.php");
                break;

            case "KeywordsDueForRenewal":
                $("#callPageAjaxData").load("KeywordsDueForRenewal.php");
                break;
        }
    }


    $(document).ready(function () {
        <?php
        if(isset($_GET["target"]))
        {
        ?>
        var triggerName = "<?php echo $_GET["target"]; ?>";

        switchTarget(triggerName);
        $("#callingPageName").find('option[value="' + triggerName + '"]').prop("selected", true);
        <?php
        }else
        {
        ?>
        $("#callPageAjaxData").load("KeywordInteractionSearch.php");
        <?php
        }
        ?>
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
