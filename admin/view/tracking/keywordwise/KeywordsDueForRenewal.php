<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<div id="userRefferalAnalatics">

    <div class="row">
        <div class="col-lg-10">
<div id="keywordDueCountData">

</div>
        </div>
        <div id="errors" style="margin-bottom: 10px"></div>
        <div class="col-lg-2">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">


                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="kwdExpiry" class="form-control" style="    width: 100px;">
                                <option value="" disabled selected> Expiry</option>
                                <option value="1_day"="">1 Day</option>
                                <option value="1_week">2 Week</option>
                                <option value="1_month">1 Month</option>
                                <option value="2_month">2 Month</option>
                                <option value="3_month">3 Month</option>
                            </select>
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div id="dueRenewalkwdajaxList">

</div>

<!--- 1- Keywords -->
<!--- 11- Keywords Due For Renewal -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="11">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="11">
<script>
    $( document ).ready(function() {
        dueRenewalkwdList(1,"3 month","7");
    });

    function dueRenewalkwdList(pageno,expiryTime,limitData)
    {
        var limit,expiry;
        if(expiryTime!="")
        {
             expiry=expiryTime;
        }else{
             expiry=($("#kwdExpiry").val());
        }



        if(limitData!="")
        {
            limit=limitData;
        }else {
             limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=7;
            }
        }

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/dueRenewalkwdList.php",
            data: {
                limit:limit,expiry:expiry,page: pageno
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                keywordDueAnalytics(expiry);
                $("#dueRenewalkwdajaxList").html("");
                $("#dueRenewalkwdajaxList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    $("#LimitedResult").change(function(){

        var page=$("#hiddenpage").val();
        dueRenewalkwdList(page,"","");
    });
    $("#kwdExpiry").change(function(){
        var page=$("#hiddenpage").val();
        dueRenewalkwdList(page,"","");
    });


    function keywordDueAnalytics(expiry)
    {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getKeywordDueAnalytics.php",
            data: {
                   expiry:expiry
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#keywordDueCountData").html("");
                $("#keywordDueCountData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                dueRenewalkwdList(1,"3 month","7");
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                dueRenewalkwdList(1,"3 month","7");
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>

