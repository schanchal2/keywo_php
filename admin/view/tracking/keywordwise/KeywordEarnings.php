<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

        <style>
            #userRefferalAnalatics table th 
            {
               border-bottom: 0;
               background: #027a98;
               border-top: solid 1px #bdeefa;
               color:white;
               text-align: center;
            }
        </style>

    <div id="userRefferalAnalatics">
       <!-- Ajax call for keyword analytics-->
    </div>
<div id="errors" style="margin-bottom: 10px"></div>
    <div class="row">
            <div class="col-lg-12">

                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                            $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="keywoirdEarningsDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                        </div>
                        <div class="input-group styled-select" style="z-index: 1;">
                            <?php

                            $result=getAllCountry($connSearch)["errMsg"];

                            ?>
                            <select id="countryToSearch" class="countryToSearch">
                                <option value="" disabled selected>Country</option>
                                <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                <?php
                                foreach($result as $value){
                                    echo $value["name"];
                                    echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                }
                                ?>

                            </select>
                        </div>

                        <div class="input-group styled-select" >
                            <select id="DeviceData" style="width: 95px">
                                <option value="" disabled selected> Device</option>
                                <option value="web"> Web</option>
                                <option value="mobile"> Mobile</option>
                                <option value=""> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                        </div>
                    </form>
                    
                </div>
            </div>
    </div><!--Row Closed-->

<div id="keywordEarningsListAjax">

</div>

<!--- 1- Keyword -->
<!--- 2- Keyword Earnings (#bitcoins) -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="2">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="2">
<script>


    $('#keywoirdEarningsDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#keywoirdEarningsDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#keywoirdEarningsDate').on('drop', function(event) {
        event.preventDefault();
    });

    $( document ).ready(function() {
        getKeywordDataPagination();
        getKeywordDataAnalytics();
    });

    function getKeywordDataPagination()
    {

        var refferaldatevalue=($("#keywoirdEarningsDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();


        var country=($("#countryToSearch").val());
        var DeviceData=($("#DeviceData").val());


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/keywordEarningsList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,DeviceData:DeviceData
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#keywordEarningsListAjax").html("");
                $("#keywordEarningsListAjax").html(data);
                getKeywordDataAnalytics();
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();


    $('#keywoirdEarningsDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {
        getKeywordDataPagination();

    });



    $("#countryToSearch").change(function(){
        getKeywordDataPagination();
    });



    $("#DeviceData").change(function(){
        getKeywordDataPagination();
    });


    $("#countryToSearch").chosen();

    function getKeywordDataAnalytics()
    {

        var refferaldatevalue=($("#keywoirdEarningsDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();
        var country=($("#countryToSearch").val());
        var DeviceData=($("#DeviceData").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/keywordEarningsAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate, country:country, DeviceData:DeviceData
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#userRefferalAnalatics").html("");
                $("#userRefferalAnalatics").html(data);

            },
            error: function () {
                alert("fail");
            }
        });

    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordDataPagination();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordDataPagination();
                $('#addTotracklistmodalBulk').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>
