<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }
</style>
<div id="userRefferalAnalatics">

    <div class="row">
        <div class="col-lg-12">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th>Keyword Traded</th>
                    <th>Keyword Traded Amt</th>
                    <th>Txn Fees</th>
                    <th>Total Amt</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id=""> 555 </td>
                    <td id=""> 1025 </td>
                    <td id=""> 1025 </td>
                    <td id=""> 1025 </td>
                </tr>
                </tbody>
            </table>
        </div>


    </div><div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="tradeReverseDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>

                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 120px;z-index: 1" type="text" id="keywordsrearch" class="form-control" value='' placeholder="Keyword">

                        <span class="btn input-group-addon" type="submit" onclick="getTradeReversalList()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 110px;z-index: 1" type="text" id="userEmail" class="form-control" value='' placeholder="Email">

                        <span class="btn input-group-addon" type="submit" onclick="getTradeReversalList()"><i class="fa fa-search"></i>  </span>

                    </div>
                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>
<div id="tradeReverseajaxList">

</div>


<!--- 1- Keyword -->
<!--- 7- Trade Reversal -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="7">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="7">
<script>
    $( document ).ready(function() {
        getTradeReversalList();
    });

    function getTradeReversalList()
    {

        var refferaldatevalue=($("#tradeReverseDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var userEmail=($("#userEmail").val());
        var keywordsrearch=($("#keywordsrearch").val());


        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getTradeReversalList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,limit:limit,userEmail:userEmail,listtype:"first",keywordsrearch:keywordsrearch
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#tradeReverseajaxList").html("");
                $("#tradeReverseajaxList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#tradeReverseDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#tradeReverseDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#tradeReverseDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#tradeReverseDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getTradeReversalList();
    });

    $("#countryToSearch").change(function(){
        getTradeReversalList();
    });


    $("#LimitedResult").change(function(){
        getTradeReversalList();
    });

    $("#paymentMode").change(function(){
        getTradeReversalList();
    });


    $("#countryToSearch").chosen();


</script>

