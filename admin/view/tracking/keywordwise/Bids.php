<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }

</style>
<div id="userRefferalAnalatics">

    <div class="row">
        <div class="col-lg-9">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th>Total No. of Bid Palced</th>
                    <th>Total Blocked for Bid Amt</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="total_placed_bid"> 0 </td>
                    <td id="total_placed_bid_amountdata"> 0 </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div class="col-lg-3">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="bidDataDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>



                </form>
            </div>
        </div>


    </div>
</div>
<div id="errors"></div>
<div id="bidDataAjax">

</div>


<!--- 1- Keywords -->
<!--- 5- Keyword Bid -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="5">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="5">
<script>
    $( document ).ready(function() {
        getBids("bid_price");
    });

    function getBids(sort)
    {

        var refferaldatevalue=($("#bidDataDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getBids.php",
            data: {
                fromdate: fromdate, todate: todate,sortBy:sort
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#bidDataAjax").html("");
                $("#bidDataAjax").html(data);

                getBidsAnalytics(fromdate,todate,sort);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#bidDataDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#bidDataDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#bidDataDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#bidDataDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getBids("bid_price");
    });

    $("#countryToSearch").change(function(){
        getBids("bid_price");
    });

    $("#countryToSearch").chosen();



    function getSortedData(sortType)
    {
        getBids(sortType);
    }



    function getBidsAnalytics(fromdate,todate,sort)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/kwd_bid_analytics.php",
            data: {
                fromdate: fromdate, todate: todate,sortBy:sort
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                data=JSON.parse(data);
                if(data["errCode"]==-1)
                {

                    $("#total_placed_bid_amountdata").html("");
                    $("#total_placed_bid").html("");

                    $("#total_placed_bid_amountdata").html(data["errMsg"]["bid_price"]+" <?= $adminCurrency; ?>");
                    $("#total_placed_bid").html(data["errMsg"]["bid_count"]);
                }
            },
            error: function () {
                alert("fail");
            }
        });

    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBids("bid_price");
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBids("bid_price");
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

