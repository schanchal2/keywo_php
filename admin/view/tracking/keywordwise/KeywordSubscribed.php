<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }
</style>
<div id="userRefferalAnalatics">

  <div id="kwdTradeAnalytics">

  </div><div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="keywordsubscribersdate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>

                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>

                    <!--<div class="input-group styled-select" >

                        <input style="width: 120px;z-index: 1" type="text" id="keywordsrearch" class="form-control" value='' placeholder="Keyword">

                        <span class="btn input-group-addon" type="submit" onclick="getKeywordSubscribers()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 110px;z-index: 1" type="text" id="userEmail" class="form-control" value='' placeholder="Email">

                        <span class="btn input-group-addon" type="submit" onclick="getKeywordSubscribers()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 95px;z-index: 1" type="text" id="orderID" class="form-control" value='' placeholder="Order ID">

                        <span class="btn input-group-addon" type="submit" onclick="getKeywordSubscribers()"><i class="fa fa-search"></i>  </span>

                    </div>-->
                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="paymentMode" class="form-control" style="    width: 130px;">
                                <option value="" disabled selected> Payment Mode</option>
                                <option value="ITD"=""><?= $adminCurrency; ?></option>
                                <option value="bitcoin">Bitcoin</option>
                                <option value="">All</option>
                            </select>
                        </label>
                    </div>
                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>
<div id="keywordsubscribersAjax">

</div>

<!--- 1- Keyword -->
<!--- 4- Keyword Subscribed (sold) -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="4">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="4">
<script>
    $( document ).ready(function() {
        getKeywordSubscribers();
    });

    $('#keywordsubscribersdate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#keywordsubscribersdate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#keywordsubscribersdate').on('drop', function(event) {
        event.preventDefault();
    });

    function getKeywordSubscribers()
    {

        var refferaldatevalue=($("#keywordsubscribersdate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var orderID=($("#orderID").val());
        var userEmail=($("#userEmail").val());
        var keywordsrearch=($("#keywordsrearch").val());
        var paymentMode=($("#paymentMode").val());


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getKeywordSubscribers.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,orderID:orderID,userEmail:userEmail,keywordsrearch:keywordsrearch,paymentMode:paymentMode
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                kwdSubAnalytics(fromdate,todate,country,orderID,userEmail,keywordsrearch,paymentMode);
                $("#keywordsubscribersAjax").html("");
                $("#keywordsubscribersAjax").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#keywordsubscribersdate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getKeywordSubscribers();
    });

    $("#countryToSearch").change(function(){
        getKeywordSubscribers();
    });

    $("#paymentMode").change(function(){
        getKeywordSubscribers();
    });

    $("#countryToSearch").chosen();



    function kwdSubAnalytics(fromdate,todate,country,orderID,userEmail,keywordsrearch,paymentMode)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getKeywordSubAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,orderID:orderID,userEmail:userEmail,keywordsrearch:keywordsrearch,paymentMode:paymentMode
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#kwdTradeAnalytics").html("");
                $("#kwdTradeAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordSubscribers();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordSubscribers();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

