<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }
</style>
<div id="userRefferalAnalatics">

    <div class="row">
        <div class="col-lg-12">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th style="width:50%" class="bg-lightBlue" rowspan="2">Total Nos. of Canceled Subscription Transactions</th>
                    <th style="width:50%" class="bg-lightBlue" colspan="2">Total Amount</th>
                </tr>
                <tr>
                    <th>Paypal</th>
                    <th>BTC</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id=""> 15 </td>
                    <td id=""> 5 </td>
                    <td id=""> 5 </td>
                </tr>
                </tbody>
            </table>
        </div>


    </div><div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="refundUserDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>

                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 120px;z-index: 1" type="text" id="keywordsrearch" class="form-control" value='' placeholder="Keyword">

                        <span class="btn input-group-addon" type="submit" onclick="getRefundUserList()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 110px;z-index: 1" type="text" id="userEmail" class="form-control" value='' placeholder="Email">

                        <span class="btn input-group-addon" type="submit" onclick="getRefundUserList()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="paymentMode" class="form-control" style="    width: 130px;">
                                <option value="" disabled selected> Payment Mode</option>
                                <option value="paypal"="">Paypal</option>
                                <option value="wallet">Wallet</option>
                                <option value="bitcoin">Bitcoin</option>
                                <option value="all">All</option>
                            </select>
                        </label>
                    </div>
                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>
<div id="refundUSerListAjax">

</div>


<!--- 1- Keyword -->
<!--- 6- Refund (Cancel Subscribtion) -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="6">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="6">
<script>
    $( document ).ready(function() {
        getRefundUserList();
    });

    function getRefundUserList()
    {

        var refferaldatevalue=($("#refundUserDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var userEmail=($("#userEmail").val());
        var keywordsrearch=($("#keywordsrearch").val());
        var paymentMode=($("#paymentMode").val());

        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getRefundUserList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,limit:limit,userEmail:userEmail,listtype:"first",keywordsrearch:keywordsrearch,paymentMode:paymentMode
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#refundUSerListAjax").html("");
                $("#refundUSerListAjax").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#refundUserDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#refundUserDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#refundUserDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#refundUserDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getRefundUserList();
    });

    $("#countryToSearch").change(function(){
        getRefundUserList();
    });


    $("#LimitedResult").change(function(){
        getRefundUserList();
    });

    $("#paymentMode").change(function(){
        getRefundUserList();
    });

    $("#countryToSearch").chosen();


</script>

