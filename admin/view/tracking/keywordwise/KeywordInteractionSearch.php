<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

        <style>
            #wrapper
            {
                margin-top: 10px;
            }
        </style>
<div id="errors" style="margin-top: 10px"></div>
        <div id="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">
                            <div class="input-group">
                                <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                <?php
                                $date=date("Y-m-d");
                                ?>
                                <input style="width: 190px" type="text" id="keywordIntractionsearchDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                            </div>
                            <div class="input-group styled-select" style="z-index: 1;">
                                <?php

                                $result=getAllCountry($connSearch)["errMsg"];

                                ?>
                                <select id="countryToSearch" class="countryToSearch">
                                    <option value="" disabled selected>Country</option>
                                    <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                    <?php
                                    foreach($result as $value){
                                        echo $value["name"];
                                        echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                            
                            <div class="input-group styled-select pull-right">
                                <select id="searchType" style="width: 145px">
                                    <option value="" disabled selected>Search Type</option>
                                    <option value="Qualified"> Qualified</option>
                                    <option value="UnQualified"> Un-Qualified</option>
                                    <option value=""> All</option>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div><!--Row Closed-->
        </div>
<div id="getKwdIntractAjaxData">

</div>
<!--- 1- Keyword -->
<!--- 1- Keyword Interaction (Search) -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="1">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="1">
<script>
    $( document ).ready(function() {

        kwdIntractionSerchList();
    });

    function kwdIntractionSerchList()
    {

        var refferaldatevalue=($("#keywordIntractionsearchDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();


        var country=($("#countryToSearch").val());
        var searchType=($("#searchType").val());



        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/kwdIntractionSerchList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,listtype:"first",searchType:searchType
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#getKwdIntractAjaxData").html("");
                $("#getKwdIntractAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#keywordIntractionsearchDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#keywordIntractionsearchDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#keywordIntractionsearchDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();


    $('#keywordIntractionsearchDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {
        kwdIntractionSerchList();
    });



    $("#countryToSearch").change(function(){
        kwdIntractionSerchList();
    });


    $("#searchType").change(function(){
        kwdIntractionSerchList();
    });


    $("#countryToSearch").chosen();



    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                kwdIntractionSerchList();
                $('#addTotracklistmodal').modal('hide');
        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                kwdIntractionSerchList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

