<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }
</style>
<div id="userRefferalAnalatics">
    <!-- Ajax call for keyword average earners analytics-->
</div>
<div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="averageEarnersDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>


                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>

<div id="averageEarnersAjax">

</div>

<!--- 1- Keyword -->
<!--- 8- Keyword Average Earnings -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="8">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="8">
<script>
    $( document ).ready(function() {
        var pageno = $("#hiddenpagemetatag").val();
        getKeywordAverageEarners(pageno);
        getKeywordAverageEarnersAnalytics();
    });

    function getKeywordAverageEarners(pageno)
    {
        var refferaldatevalue=($("#averageEarnersDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getKeywordAverageEarners.php",
            data: {
                page:pageno, fromdate: fromdate, todate: todate,limit:limit,listtype:"first"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#averageEarnersAjax").html("");
                $("#averageEarnersAjax").html(data);
                getKeywordAverageEarnersAnalytics();
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#averageEarnersDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {
        var pageno = $("#hiddenpagemetatag").val();
        getKeywordAverageEarners(pageno);
    });

    $("#LimitedResult").change(function(){
        var pageno = $("#hiddenpagemetatag").val();
        getKeywordAverageEarners(pageno);
    });

    function getKeywordAverageEarnersAnalytics()
    {
        var refferaldatevalue=($("#averageEarnersDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/keywordAverageEarnersAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#userRefferalAnalatics").html("");
                $("#userRefferalAnalatics").html(data);

            },
            error: function () {
                alert("fail");
            }
        });

    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordAverageEarnersAnalytics();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getKeywordAverageEarnersAnalytics();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>

