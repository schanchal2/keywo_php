<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

<style>

    #filter form[name="filter"] .btn-group button[data-id="countryToSearch"] {
        width:167px;
    }
</style>
<div id="userRefferalAnalatics">

    <div class="row">

        <div id="tradeFeesAnalytics">

        </div>


    </div><div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="getTradingFeesDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>



                   <!-- <div class="input-group styled-select" >

                        <input style="width: 120px;z-index: 1" type="text" id="keywordsrearch" class="form-control" value='' placeholder="Keyword">

                        <span class="btn input-group-addon" type="submit" onclick="getTradingFeesList()"><i class="fa fa-search"></i>  </span>

                    </div>-->


                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>
<div id="getTradingFeesAjaxList">

</div>


<!--- 1- Keyword -->
<!--- 9- Trading Fees -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="1">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="1">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="9">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="9">

<script>
    $( document ).ready(function() {
        getTradingFeesList();
    });

    $('#getTradingFeesDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#getTradingFeesDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#getTradingFeesDate').on('drop', function(event) {
        event.preventDefault();
    });

    function getTradingFeesList()
    {

        var refferaldatevalue=($("#getTradingFeesDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var keywordsrearch=($("#keywordsrearch").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getTradingFeesList.php",
            data: {
                fromdate: fromdate, todate: todate,keywordsrearch:keywordsrearch
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                tradeFeesAnalytics(fromdate,todate,keywordsrearch);
                $("#getTradingFeesAjaxList").html("");
                $("#getTradingFeesAjaxList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#getTradingFeesDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getTradingFeesList();
    });


    function tradeFeesAnalytics(fromdate,todate,keywordsrearch)
    {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/keywordwise/getTradingFeesAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,keywordsrearch:keywordsrearch
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#tradeFeesAnalytics").html("");
                $("#tradeFeesAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getTradingFeesList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getTradingFeesList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

