<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<div id="userRefferalAnalatics"><!--css are on this name-->
    <input type="text" hidden>
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>All</th>
            <th>Audio</th>
            <th>Video</th>
            <th>Blog</th>
            <th>Image</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id=""> 0 </td>
            <td id=""> 0 </td>
            <td id=""> 0 </td>
            <td id=""> 0 </td>
            <td id=""> 0 </td>

        </tr>
        </tbody>
    </table>
</div>
<div class="row">


    <div class="col-lg-12">
        <div id="filter" class="sign-upverification">
            <form action="" method="POST" class="form-inline" role="form" name="filter">
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                    <?php
                    $date=date("Y-m-d");
                    ?>
                    <input style="width: 190px" type="text" id="getBlockedPostDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                </div>

                <div class="form-inline pull-right">
                    <label class="dropdownOptions">
                        <select id="postBlockReasonType" class="form-control" style="    width: 100px;">
                            <option value="" disabled selected> Reason</option>
                            <option value="Religious"="">Religious</option>
                            <option value="Spam">Spam</option>
                            <option value="Nudity">Nudity</option>
                            <option value="all">All</option>
                        </select>
                    </label>
                </div>

                <div class="form-inline pull-right" style="margin-right: 10px;">
                    <label class="dropdownOptions">
                        <select id="postType" class="form-control" style="    width: 100px;">
                            <option value="" disabled selected> Post</option>
                            <option value="blog"="">Blog</option>
                            <option value="TopPostEarnings">Video</option>
                            <option value="TopPostViewEarnings">Image</option>
                            <option value="TopPostShareEarnings">Audio</option>
                            <option value="all">All</option>
                        </select>
                    </label>
                </div>
            </form>
        </div>

        <!-- Single button -->
    </div>
</div>
</div>
<div id="getBlockedPostAjaxData">


</div>


<!--- 5- General -->
<!--- 39- Blocked Post -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="5">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="5">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="39">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="39">
<script>

    $('#getBlockedPostDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#getBlockedPostDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#getBlockedPostDate').on('drop', function(event) {
        event.preventDefault();
    });
    $( document ).ready(function() {
        getBlockedPostByDate();
    });
    var todaysdate = new Date();
     todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#getBlockedPostDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function(event, obj) {
    }).bind('datepicker-change', function(event, obj) {

        var newdate = (obj.value);
        getBlockedPostByDate();
    });


    function getBlockedPostByDate()
    {
        var blogdatadate=($("#getBlockedPostDate").val());
        var datedata=blogdatadate.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var postBlockReasonType=($("#postBlockReasonType").val());
        var postType=($("#postType").val());

        var limit=($("#LimitedResults").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/BlockedPosts.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,listtype:"first",postType:postType,postBlockReasonType:postBlockReasonType
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#getBlockedPostAjaxData").html("");
                $("#getBlockedPostAjaxData").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }


    $("#postBlockReasonType").change(function(){
        getBlockedPostByDate();
    });


    $("#postType").change(function(){
        getBlockedPostByDate();
    });


</script>

