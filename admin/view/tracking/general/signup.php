<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/manageUser/userManagement_analytics.php";
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];


?>
        <div id="userRefferalAnalatics">
            <input type="text" hidden>
            <table class="table  text-center table-responsive">
                <thead>
                    <tr>
                        <th>Signed-Up With Referral Code (NoS.)</th>
                        <th>Users Sign-up Without Referral Code </th>
                        <th>Un-Verified Users (Nos.)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td id="Total_UserBy_referral"> 0 </td>
                        <td id="UsersRegisteredWithoutRefferal"> 0 </td>
                        <td id="TotalUnverifiedUsers"> 0 </td>
                    </tr>
                </tbody>
            </table>
        </div>
<div id="errors"></div>
        <div class="row">
            <div class="col-lg-12">
                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <input type="text" id="userListDateRange" class="form-control" value='<?php
                            $date=date("Y-m-d");
                            echo $date." to ".$date; ?>' placeholder="Select Date Range">
                        </div>
                        <div class="input-group styled-select">
                            <?php

                            $result=getAllCountry($connSearch)["errMsg"];

                            ?>
                            <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select">

                                <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                    <?php
                                foreach($result as $value){
                                    echo $value["name"];
                                    echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                }
                                ?>

                                </select>
                              
                        </div>
                        <div class="input-group styled-select">
                            <!-- <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span> -->
                            <!-- <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> -->
                            <select id="userGender">
                                <option value="" disabled selected> Gender</option>
                                <option value="male"> Male</option>
                                <option value="female"> Female</option>
                                <option value="other"> Other</option>
                                <option value=""> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                        </div>
                        <div class="input-group styled-select">
                            <select id="UserStatus">
                                <option value="" disabled selected>Status</option>
                                <option value="1"> Verified</option>
                                <option value="0"> Un-Verified</option>
                                <option value="2"> Blocked</option>
                                <option value=""> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                        </div>
                    </form>
                    <!-- Single button -->
                </div>
            </div>
        </div>
        <div id="newRegisteredUsersList">

        </div>

<!--- 5- General -->
<!--- 35- Signup -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="5">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="5">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="35">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="35">
<script>

    $( document ).ready(function() {
        getUserAnalyticsByDate();
        getUserListByDate();
    });


    $('#userListDateRange').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#userListDateRange').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#userListDateRange').on('drop', function(event) {
        event.preventDefault();
    });
    function getUserListByDate()
    {

        var refferaldatevalue=($("#userListDateRange").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var gender=($("#userGender").val());
        var status=($("#UserStatus").val());

        var limit=($("#LimitedResults").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/getUserListSignup.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender,status:status,limit:limit,listtype:"first"
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#newRegisteredUsersList").html("");
                $("#newRegisteredUsersList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    function getUserAnalyticsByDate()
    {

        var refferaldatevalue=$("#userListDateRange").val();
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var gender=($("#userGender").val());

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/general/getUserAnalyticsSignup.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                if (data["errCode"]) {
                    if (data["errCode"] == "-1") {
                        var Total_UserBy_referral=data["errMsg"]["Total_UserBy_referral"];
                        var UsersRegisteredWithoutRefferal=data["errMsg"]["UsersRegisteredWithoutRefferal"];
                        var TotalUnverifiedUsers=data["errMsg"]["TotalUnverifiedUsers"];

                        if(Total_UserBy_referral==null)
                        {
                            Total_UserBy_referral=0;
                        }

                        if(UsersRegisteredWithoutRefferal==null)
                        {
                            UsersRegisteredWithoutRefferal=0;
                        }

                        if(TotalUnverifiedUsers==null)
                        {
                            TotalUnverifiedUsers=0;
                        }
                        $("#Total_UserBy_referral").text("");
                        $("#Total_UserBy_referral").text(Total_UserBy_referral);
                        $("#UsersRegisteredWithoutRefferal").text("");
                        $("#UsersRegisteredWithoutRefferal").text(UsersRegisteredWithoutRefferal);
                        $("#TotalUnverifiedUsers").text("");
                        $("#TotalUnverifiedUsers").text(TotalUnverifiedUsers);
                        $('#loadng-image').hide();
                    }
                    else {
                        console.log("failed to fetch analytics");
                    }

                }


            },
            error: function () {
                alert("fail");
            }
        });

    }

var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
var threeMonthsAgo = (3).months().ago();




    $('#userListDateRange').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {
        getUserListByDate();
        var newdate = (obj.value);
        //$("#refferaldatevalue").html("");
      //  $("#refferaldatevalue").html(newdate);

        getUserAnalyticsByDate();
    });

    $("#countryToSearch").change(function(){
        getUserListByDate();
        getUserAnalyticsByDate();
    });

    $("#userGender").change(function(){
        getUserListByDate();
        getUserAnalyticsByDate();
    });

    $("#UserStatus").change(function(){
        getUserListByDate();
        getUserAnalyticsByDate();
    });

    $("#LimitedResults").change(function(){
        getUserListByDate();
        getUserAnalyticsByDate();
    });

    $("#countryToSearch").chosen();


    function getUserListPaginationByDate(pageno,fromdate,todate,country,gender,status,limit)
    {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/getUserListSignup.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,country:country,gender:gender,listtype:'pagination',pageno:pageno,status:status
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#newRegisteredUsersList").html("");
                $("#newRegisteredUsersList").html(data);

            },
            error: function () {
                console.log("fail");
            }

        });
    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {
            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getUserListByDate();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {

            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getUserListByDate();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }
    /************************ add to tracklist **********************/



</script>

