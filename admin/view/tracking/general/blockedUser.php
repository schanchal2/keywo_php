<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .table > thead > tr > th {
        padding: 5px 0;
    }

    .table > thead > tr > td{
        padding: 5px 0;
    }
</style>
<div id="userRefferalAnalatics">


    <div class="row" style="">
        <div class="col-lg-4">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th>Blocked Users Numbers</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="blockedUsersNos"> 0 </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="">
        <div id="errors"></div>
    </div>
    <div class="row">
        <div class="col-lg-12" style="margin-bottom: 10px">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="getUserblockeddate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>
                    <div class="input-group styled-select">
                        <select id="userGender" style="width: 105px">
                            <option value="" disabled selected> Gender</option>
                            <option value="male"> Male</option>
                            <option value="female"> Female</option>
                            <option value="other"> Other</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>
                </form> </div></div>


    </div>


</div>
<div id="getUserblockedajax">

</div>


<!--- 5- General -->
<!--- 38- Blocked User List -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="5">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="5">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="38">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="38">

<script>

    $('#getUserblockeddate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#getUserblockeddate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#getUserblockeddate').on('drop', function(event) {
        event.preventDefault();
    });

    $( document ).ready(function() {
        getBlockedUsersList();
    });

    function getBlockedUsersList()
    {

        var refferaldatevalue=($("#getUserblockeddate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var gender=($("#userGender").val());
        var country=($("#countryToSearch").val());

        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/getBlockedUsers.php",
            data: {
                fromdate: fromdate, todate: todate,gender:gender,limit:limit,listtype:"first",country:country
            },
            success: function (data) {
                getUserAnalyticsByDate(fromdate,todate,country,gender);
                $("#getUserblockedajax").html("");
                $("#getUserblockedajax").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#getUserblockeddate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getBlockedUsersList();
    });

    $("#countryToSearch").change(function(){
        getBlockedUsersList();
    });

    $("#userGender").change(function(){
        getBlockedUsersList();
    });

    $("#LimitedResults").change(function(){
        getBlockedUsersList();
    });

    $("#countryToSearch").chosen();

    function getUserListPaginationByDate(pageno,fromdate,todate,country,gender,limit)
    {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/getBlockedUsers.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,country:country,gender:gender,listtype:'pagination',pageno:pageno
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#getUserblockedajax").html("");
                $("#getUserblockedajax").html(data);

            },
            error: function () {
                console.log("fail");
            }

        });
    }


    function getUserAnalyticsByDate(fromdate,todate,country,gender)
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/general/getUserAnalyticsBlocked.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                if (data["errCode"]) {
                    if (data["errCode"] == "-1") {
                        $("#blockedUsersNos").html("");
                        $("#blockedUsersNos").html(data["errMsg"]);
                        $('#loadng-image').hide();
                    }
                    else {
                        console.log("failed to fetch analytics");
                    }

                }


            },
            error: function () {
                console.log("fail");
            }
        });

    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlockedUsersList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlockedUsersList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

