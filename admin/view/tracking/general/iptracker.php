<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>


<div id="wrapper" style="margin-top:10px;">
    <div class="row">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                              $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="ipTrackerdate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                       </div>



                    <div class="input-group styled-select">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                        <!-- <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>-->
                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 130px;z-index: 1" type="text" id="userNameid" class="form-control" value='' placeholder="User Name">

                        <span class="btn input-group-addon" type="submit" onclick="getipTrackerData()"><i class="fa fa-search"></i>  </span>

                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 130px;z-index: 1" type="text" id="ipaddressid" class="form-control" value='' placeholder="IP Address">

                        <span class="btn input-group-addon" type="submit" onclick="getipTrackerData()"><i class="fa fa-search"></i>  </span>

                    </div>
                            <div class="input-group styled-select pull-right">
                                <select id="UserStatus" style="width: 145px";>
                                    <option value="" disabled selected>Active</option>
                                    <option value="1"> Verified</option>
                                    <option value="0"> Un-Verified</option>
                                    <option value="2"> Blocked</option>
                                    <option value=""> All</option>
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                            </div>
                </form>
                        
            </div>
        </div>
    </div><!--Row Closed-->
</div>
<div id="ipTrackerAjaxData">

</div>
<script>


    $( document ).ready(function() {
        getipTrackerData();
    });
    var todaysdate = new Date();
    var threeMonthsAgo = (3).months().ago();

    $('#ipTrackerdate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function(event, obj) {
    }).bind('datepicker-change', function(event, obj) {

        var newdate = (obj.value);
        getipTrackerData();
    });


    function getipTrackerData()
    {
        var blogdatadate=($("#ipTrackerdate").val());
        var datedata=blogdatadate.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var UserStatus=($("#UserStatus").val());
        var ipaddress=($("#ipaddressid").val());
        var userName=($("#userNameid").val());

        var limit=($("#LimitedResults").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/general/getIPTrackerList.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,listtype:"first",ipaddress:ipaddress,userName:userName,UserStatus:UserStatus
            },
            success: function (data) {
                $("#ipTrackerAjaxData").html("");
                $("#ipTrackerAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    $("#countryToSearch").change(function(){
        getipTrackerData();
    });

    $("#countryToSearch").chosen();

    $("#UserStatus").change(function(){
        getipTrackerData();
    });

</script>
