<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
?>

    <div id="domainTrackerAjax" style="padding-top: 25px">
        <div id="userList" class="m-t-15"><!--due to global id css design id is same-->
            <table class="table  text-center table-responsive" id="domianTrackerTable">
                <thead>
                <tr>

                    <th> Domain Name </th>
                    <th> Total Reg. User</th>
                    <th> Verified</th>
                    <th> Un-Verified</th>
                </tr>

                </thead>
                <tbody>

                </tbody>

            </table>
        </div>
    </div>

<script>
    $( document ).ready(function() {
        $('#domianTrackerTable').DataTable({
            "bInfo": true,
            "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
            "ajax": "../../../controller/tracking/general/getUserRegisterbyDomain.php",
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": false,
            "info": true,
            "autoWidth": true
        });
    });

</script>

