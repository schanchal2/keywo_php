<?php
session_start();

include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet"
      href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">

<main>
    <div class="container-fluid">


        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">General</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                <label class="dropdownOptions">
                    <select id="callingPageName" class="form-control">
                        <option value="SIGNUP" selected="">Signup</option>
                        <!--<option value="IPTRACKER">IP Tracker</option>-->
                        <option value="DOMIANEmail">Domain (Email)</option>
                        <option value="BLOCKEDUSER">Blocked Users List</option>
                        <!--<option value="BLOCKEPOST">Blocked Post 	</option>-->
                    </select>
                </label>
            </form>
        </div>
        <div id="callPageAjaxData">

        </div>

    </div>
</main>

</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>

<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php include "../trackingModal.php" ?>

<script>


    $('#callingPageName').on('change', function () {
        var pagename = this.value;
        switchTarget(pagename);
    });

    function switchTarget(pagename) {
        switch (pagename) {
            case "SIGNUP":
                $("#callPageAjaxData").html(showLoadingDiv()).load("signup.php");
                break;

            case "IPTRACKER":
                $("#callPageAjaxData").html(showLoadingDiv()).load("iptracker.php");
                break;

            case "DOMIANEmail":
                $("#callPageAjaxData").html(showLoadingDiv()).load("domaintrack.php");
                break;

            case "BLOCKEDUSER":
                $("#callPageAjaxData").html(showLoadingDiv()).load("blockedUser.php");
                break;

            case "BLOCKEPOST":
                $("#callPageAjaxData").html(showLoadingDiv()).load("blockedPost.php");
                break;


        }
    }
    $(document).ready(function () {
        <?php
        if(isset($_GET["target"]))
        {
        ?>
        var triggerName = "<?php echo $_GET["target"]; ?>";

        switchTarget(triggerName);
        $("#callingPageName").find('option[value="' + triggerName + '"]').prop("selected", true);
        <?php
        }else
        {
        ?>
        $("#callPageAjaxData").html(showLoadingDiv()).load("signup.php");
        <?php
        }
        ?>
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
