<?php
session_start();

include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
 include "../../../model/geolocation/geoLocation.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet"><main>
    <div class="container-fluid">



        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Ad-Wise</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                <label class="dropdownOptions">
                    <select id="callingPageName" class="form-control">
                        <option value="TopAdCampaignSpent" selected="">Top Ad-Campaign Spent</option>
                        <option value="TopperformingAdCampaigns">Top performing Ad-Campaigns (CPC,CPM, CPL)	</option>
                    </select>
                </label>
            </form>
        </div>
        <div id="callPageAjaxData">

        </div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>


<script>


    $('#callingPageName').on('change', function() {
        var pagename=this.value;

        switch(pagename)
        {
            case "TopAdCampaignSpent":
                $("#callPageAjaxData").load("TopAdCampaignSpent.php");
                break;

            case "TopperformingAdCampaigns":
                $("#callPageAjaxData").load("TopperformingAdCampaigns.php");
                break;

        }
    });
    $( document ).ready(function() {
        $("#callPageAjaxData").load("TopAdCampaignSpent.php");
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
