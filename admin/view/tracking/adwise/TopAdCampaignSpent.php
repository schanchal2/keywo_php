<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

        <style>
        #userRefferalAnalatics table th 
        {
           border-bottom: 0;
           background: #027a98;
           border-top: solid 1px #bdeefa;
           color:white;
           text-align: center;
        }

 

 
        </style>

<div id="userRefferalAnalatics">
    <input type="text" hidden>
        <table class="table  text-center table-responsive">
            <thead>
                <tr>
                    <th>Ad-Campaign in Network(Nos.)</font></th>
                    <th>Revenue Received(Amt.)</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="Ad-Campaign in Network">400</td>
                    <td id="Revenue Received">2525IT$</td>
                </tr>
            </tbody>
        </table>
</div>

 <div class="row">
            <div class="col-lg-12">

                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                            $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="addCampSpentDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                        </div>



                        <div class="input-group styled-select" style="z-index: 1;">
                            <?php

                            $result=getAllCountry($connSearch)["errMsg"];

                            ?>
                            <select id="countryToSearch" class="countryToSearch">
                                <option value="" disabled selected>Country</option>
                                <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                <?php
                                foreach($result as $value){
                                    echo $value["name"];
                                    echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                }
                                ?>

                            </select>
                        </div>


                        <div class="form-inline pull-right">
                            <label class="dropdownOptions">
                                <select id="adCampaignType" class="form-control" style="    width: 140px;">
                                    <option value="" disabled selected> Campaign-Type</option>
                                    <option value="search"> Search</option>
                                    <option value="social"> Social</option>
                                    <option value="widget"> Widget</option>
                                    <option value="promotePost"> Promote Post</option>
                                    <option value=""> All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                    
                </div>
            </div>
    </div><!--Row Closed-->

<div id="getAddspentAjaxData">

</div>

<script>
    $( document ).ready(function() {
        getAddSpentList();
    });

    function getAddSpentList()
    {

        var refferaldatevalue=($("#addCampSpentDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();


        var country=($("#countryToSearch").val());
        var adCampaignType=($("#adCampaignType").val());


        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/ads/getAddSpentList.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,country:country,listtype:"first",adCampaignType:adCampaignType
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#getAddspentAjaxData").html("");
                $("#getAddspentAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    var threeMonthsAgo = (3).months().ago();

    $('#addCampSpentDate').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getAddSpentList();
    });



    $("#countryToSearch").change(function(){
        getAddSpentList();
    });


    $("#LimitedResult").change(function(){
        getAddSpentList();
    });

    $("#adCampaignType").change(function(){
        getAddSpentList();
    });

    $("#countryToSearch").chosen();



</script>