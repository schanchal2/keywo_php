<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

        <style>
        #userRefferalAnalatics table th 
        {
           border-bottom: 0;
           background: #027a98;
           border-top: solid 1px #bdeefa;
           color:white;
           text-align: center;
        }

        .styled-select select
         {
      	    color: #35ccf1;
      	    margin-inline-start: 13px;
}
         }

 		</style>

	<div class="row">
		<div class="col-md-4">
			<div id="userRefferalAnalatics">
			    <input type="text" hidden>
			        <table class="table  text-center table-responsive">
			            <thead>
			                <tr>
			                    <th>Ad-Campaign in Network(Nos.)</font></th>
			                </tr>
			            </thead>
			            <tbody>
			                <tr>
			                    <td id="Ad-Campaign in Network">400</td>
			                </tr>
			            </tbody>
			        </table>
			</div>
    	</div>
    </div>

    <div class="row">
            <div class="col-lg-12">
				<div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                            $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="addCampTopDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                        </div>


                        <div class="input-group styled-select" >
                            <select id="conversionData" style="width: 150px">
                                <option value="" disabled selected> Conversion</option>
                                <option value="c1"> Conversion 1</option>
                                <option value="c2"> Conversion 2</option>
                                <option value="all"> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i> </span>

                        </div>

                        <div class="form-inline pull-right">
                            <label class="dropdownOptions">
                                <select id="adCampaignType" class="form-control" style="    width: 140px;">
                                    <option value="" disabled selected> Campaign-Type</option>
                                    <option value="search"> Search</option>
                                    <option value="social"> Social</option>
                                    <option value="widget"> Widget</option>
                                    <option value="promotePost"> Promote Post</option>
                                    <option value=""> All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                    
                </div>
            </div>
    </div><!--Row Closed-->



<div id="getAddTopAjaxData">

</div>

<script>
    $( document ).ready(function() {
        topAdCampList();
    });

    function topAdCampList()
    {

        var refferaldatevalue=($("#addCampTopDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();


        var conversionData=($("#conversionData").val());
        var adCampaignType=($("#adCampaignType").val());


        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/ads/topPerformingAdCamp.php",
            data: {
                fromdate: fromdate, todate: todate,limit:limit,conversionData:conversionData,listtype:"first",adCampaignType:adCampaignType
            },
            success: function (data) {
                $("#getAddTopAjaxData").html("");
                $("#getAddTopAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    var todaysdate = new Date();
    var threeMonthsAgo = (3).months().ago();

    $('#addCampTopDate').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        topAdCampList();
    });
    


    $("#LimitedResult").change(function(){
        topAdCampList();
    });

    $("#adCampaignType").change(function(){
        topAdCampList();
    });

    $("#conversionData").change(function(){
        topAdCampList();
    });
    



</script>