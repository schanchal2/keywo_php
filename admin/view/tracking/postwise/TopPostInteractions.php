<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<div id="errors" style="margin-top: 10px"></div>
        <div class="row" id="userRefferalAnalatics">
            <div class="col-lg-12">
                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                            $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="topPostIntractionsDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                            <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                        </div>
                        <div class="input-group styled-select" style="z-index: 1;">
                            <?php

                            $result=getAllCountry($connSearch)["errMsg"];

                            ?>
                            <select id="countryToSearch">
                                    <option value="" disabled selected>Country</option>
                                <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                    <?php
                                foreach($result as $value){
                                    echo $value["name"];
                                    echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                }
                                ?>

                                </select>
                               <!-- <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>-->
                        </div>
                        <div class="input-group styled-select">
                            <!-- <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span> -->
                            <!-- <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> -->
                            <select id="userGender" style="width: 105px">
                                <option value="" disabled selected> Gender</option>
                                <option value="male"> Male</option>
                                <option value="female"> Female</option>
                                <option value="other"> Other</option>
                                <option value=""> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                        </div>
                        <div class="input-group styled-select" >
                            <select id="DeviceData" style="width: 95px">
                                <option value="" disabled selected> Device</option>
                                <option value="web"> Web</option>
                                <option value="mobile"> Mobile</option>
                                <option value=""> All</option>
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                        </div>
                        <div class="form-inline pull-right">
                            <label class="dropdownOptions">
                                <select id="postType" class="form-control" style="    width: 100px;">
                                    <option value="" disabled selected> Post</option>
                                    <option value="blog">Blog</option>
                                    <option value="video">Video</option>
                                    <option value="image">Image</option>
                                    <option value="audio">Audio</option>
                                    <option value="status">Status</option>
                                    <option value="">All</option>
                                </select>
                            </label>
                        </div>
                    </form>


                    <!-- Single button -->
                </div>
            </div>
        </div>
        <div id="postIntractionsAjaxData">


        </div>

<!--- 3- Post -->
<!--- 29- Top Post Interactions -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="3">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="3">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="29">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="29">
<script>

    $( document ).ready(function() {
        getBlogTrackListByDate();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#topPostIntractionsDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#topPostIntractionsDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#topPostIntractionsDate').on('drop', function(event) {
        event.preventDefault();
    });

    $('#topPostIntractionsDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function(event, obj) {
        /* This event will be triggered when first date is selected */
        console.log('first-date-selected', obj);
        // obj will be something like this:
        // {
        //      date1: (Date object of the earlier date)
        // }
    }).bind('datepicker-change', function(event, obj) {

        var newdate = (obj.value);


       getBlogTrackListByDate();

        /* This event will be triggered when second date is selected */
        console.log('change', obj);
        // obj will be something like this:
        // {
        //      date1: (Date object of the earlier date),
        //      date2: (Date object of the later date),
        //      value: "2013-06-05 to 2013-06-07"
        // }
    })
        .bind('datepicker-apply', function(event, obj) {
            /* This event will be triggered when user clicks on the apply button */
            console.log('apply', obj);
        })
        .bind('datepicker-close', function() {
            /* This event will be triggered before date range picker close animation */
            console.log('before close');
        })
        .bind('datepicker-closed', function() {
            /* This event will be triggered after date range picker close animation */
            console.log('after close');
        })
        .bind('datepicker-open', function() {
            /* This event will be triggered before date range picker open animation */
            console.log('before open');
        })
        .bind('datepicker-opened', function() {
            /* This event will be triggered after date range picker open animation */
            console.log('after open');
        });



    $("#countryToSearch").chosen();

    function getBlogTrackListByDate()
    {
        var blogdatadate=($("#topPostIntractionsDate").val());
        var datedata=blogdatadate.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var device=($("#DeviceData").val());
        var postType=($("#postType").val());
        var gender=($("#userGender").val());

        var limit=($("#LimitedResults").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/postwise/topPostIntractions.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender,limit:limit,listtype:"first",postType:postType,device:device
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#postIntractionsAjaxData").html("");
                $("#postIntractionsAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    $("#userGender").change(function(){
        getBlogTrackListByDate();
    });

    $("#countryToSearch").change(function(){
        getBlogTrackListByDate();
    });

    $("#postType").change(function(){
        getBlogTrackListByDate();
    });

    $("#DeviceData").change(function(){
        getBlogTrackListByDate();
    });


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlogTrackListByDate();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlogTrackListByDate();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

