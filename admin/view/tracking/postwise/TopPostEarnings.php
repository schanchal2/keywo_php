<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<div id="userRefferalAnalatics"><!--css are on this name-->
    <input type="text" hidden>
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>No. of Post</th>
            <th>Post Liked (No.) </th>
            <th>Post Share (No.)</th>
            <th>Post View Earnings</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="total_no_post"> 0 </td>
            <td id="total_post_liked"> 0 </td>
            <td id="total_post_shared"> 0 </td>
            <td id="total_post_view_earnings"> 0 </td>

        </tr>
        </tbody>
    </table>
</div>
<div id="errors" style="margin-bottom: 10px"></div>
<div class="row">


    <div class="col-lg-12">
        <div id="filter" class="sign-upverification">
            <form action="" method="POST" class="form-inline" role="form" name="filter">
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                    <?php
                    $date=date("Y-m-d");
                    ?>
                    <input style="width: 190px" type="text" id="topPostEarningsDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                </div>
                <div class="input-group styled-select" style="z-index: 1;">
                    <?php

                    $result=getAllCountry($connSearch)["errMsg"];

                    ?>
                    <select id="countryToSearch">
                        <option value="" disabled selected>Country</option>
                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                        <?php
                        foreach($result as $value){
                            echo $value["name"];
                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                        }
                        ?>

                    </select>
                    <!-- <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>-->
                </div>
                <div class="input-group styled-select">
                    <!-- <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span> -->
                    <!-- <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> -->
                    <select id="userGender" style="width: 105px">
                        <option value="" disabled selected> Gender</option>
                        <option value="male"> Male</option>
                        <option value="female"> Female</option>
                        <option value="other"> Other</option>
                        <option value=""> All</option>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                </div>
                <div class="input-group styled-select" >
                    <select id="DeviceData" style="width: 95px">
                        <option value="" disabled selected> Device</option>
                        <option value="web"> Web</option>
                        <option value="mobile"> Mobile</option>
                        <option value=""> All</option>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                </div>

                <div class="form-inline pull-right">
                    <label class="dropdownOptions">
                        <select id="postType" class="form-control" style="    width: 100px;">
                            <option value="" disabled selected> Post</option>
                            <option value="blog">Blog</option>
                            <option value="video">Video</option>
                            <option value="image">Image</option>
                            <option value="audio">Audio</option>
                            <option value="status">Status</option>
                            <option value="">All</option>
                        </select>
                    </label>
                </div>
                </div>
            </form>
</div>

            <!-- Single button -->
        </div>
    </div>
</div>
<div id="postEarningAjaxData">


</div>

<!--- 3- Post -->
<!--- 30- Top Post Earnings -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="3">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="3">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="30">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="30">
<script>


    $( document ).ready(function() {
        getPostEarningsByDate();
        getPostViewAnalyticsByDate();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();


    function getPostViewAnalyticsByDate(fromdate,todate,country,gender,device,postType)
    {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/postwise/getPostAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender,device:device,postType:postType
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                if(data["errCode"]=="-1")
                {
                    var analytics=data["errMsg"];


                    $("#total_no_post").html("");
                    $("#total_no_post").html(analytics.total_no_of_post);
                    $("#total_post_liked").html("");
                    $("#total_post_liked").html(analytics.total_likes);
                    $("#total_post_shared").html("");
                    $("#total_post_shared").html(analytics.total_shares);
                    $("#total_post_view_earnings").html("");
                    $("#total_post_view_earnings").html(analytics.total_post_earnings);
                }
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /*************************************************** section for table **********************************************/
    $('#topPostEarningsDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#topPostEarningsDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#topPostEarningsDate').on('drop', function(event) {
        event.preventDefault();
    });
    $('#topPostEarningsDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function(event, obj) {

    }).bind('datepicker-change', function(event, obj) {

        var newdate = (obj.value);
        getPostEarningsByDate();
        console.log('change', obj);
    });



    $("#countryToSearch").chosen();

    function getPostEarningsByDate()
    {
        var blogdatadate=($("#topPostEarningsDate").val());
        var datedata=blogdatadate.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var device=($("#DeviceData").val());

        var gender=($("#userGender").val());
        var postType=($("#postType").val());



        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/postwise/topPostEarnings.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,gender:gender,device:device,postType:postType
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                getPostViewAnalyticsByDate(fromdate,todate,country,gender,device,postType);
                $("#postEarningAjaxData").html("");
                $("#postEarningAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    $("#userGender").change(function(){
        getPostEarningsByDate();
    });

    $("#countryToSearch").change(function(){
        getPostEarningsByDate();
    });

    $("#postType").change(function(){
        getPostEarningsByDate();
    });

    $("#DeviceData").change(function(){
        getPostEarningsByDate();
    });




    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getPostEarningsByDate();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getPostEarningsByDate();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>

