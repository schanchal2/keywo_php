<?php
session_start();

include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Userwise</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                <label class="dropdownOptions">
                    <select id="callingPageName" class="form-control">
                        <!--<option value="topSearchEarners" selected="">Top Search Earners</option>-->
                        <option value="topSocialEarners">Top Social Earners</option>
                        <!--<option value="topTotalSerchPlusSocial">Top Total Earnings (Search + Social)</option>-->
                        <option value="highestItdPurchase">Highest <?= $adminCurrency; ?> Purchased</option>
                        <option value="highestTotalItdPurchaseAmt">Highest Total <?= $adminCurrency; ?> Purchased Amt</option>
                        <option value="highestCashout">Highest Cashouts</option>
                        <option value="highestTotalCashoutAmt">Highest Total Cashout Amt</option>
                        <option value="TopReferrer">Top Referrer</option>
                        <option value="TopSent">Top Sent</option>
                        <option value="TopTotalSent">Top Total Sent</option>
                        <option value="TopReceived">Top Received</option>
                        <option value="TopTotalReceived">Top Total Received</option>
                        <option value="BlockedForBids">Blocked for Bids</option>
                        <option value="CashoutFees">Cashout fees</option>
                        <option value="HighestWalletBalance">Highest Wallet Balance</option>
                        <!--<option value="TopUnUtilisedAdBalances">Top Un-Utilised Ad Balances</option>
                        <option value="TopAdSpenders">Top Ad-Spenders</option>-->
                    </select>
                </label>
            </form>
        </div>

        <div id="callPageAjaxData">

        </div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>

<?php include "../trackingModal.php" ?>
<script>


    $('#callingPageName').on('change', function() {
        var pagename=this.value;

        switchTarget(pagename);
    });
    function switchTarget(pagename) {
        switch (pagename) {
            case "topSearchEarners":
                $("#callPageAjaxData").load("topSearchEarners.php");
                break;

            case "topSocialEarners":
                $("#callPageAjaxData").load("topSocialEarners.php");
                break;

            case "topTotalSerchPlusSocial":
                $("#callPageAjaxData").load("topTotalSerchPlusSocial.php");
                break;

            case "highestItdPurchase":
                $("#callPageAjaxData").load("highestItdPurchase.php");
                break;

            case "highestTotalItdPurchaseAmt":
                $("#callPageAjaxData").load("highestTotalItdPurchaseAmt.php");
                break;

            case "highestCashout":
                $("#callPageAjaxData").load("highestCashout.php");
                break;

            case "highestTotalCashoutAmt":
                $("#callPageAjaxData").load("highestTotalCashoutAmt.php");
                break;

            case "BlockedForBids":
                $("#callPageAjaxData").load("BlockedForBids.php");
                break;

            case "TopReferrer":
                $("#callPageAjaxData").load("TopReferrer.php");
                break;

            case "TopSent":
                $("#callPageAjaxData").load("TopSent.php");
                break;

            case "TopTotalSent":
                $("#callPageAjaxData").load("TopTotalSent.php");
                break;

            case "TopReceived":
                $("#callPageAjaxData").load("TopReceived.php");
                break;

            case "TopTotalReceived":
                $("#callPageAjaxData").load("TopTotalReceived.php");
                break;

            case "Cashback":
                $("#callPageAjaxData").load("Cashback.php");
                break;

            case "TopAdSpenders":
                $("#callPageAjaxData").load("TopAdSpenders.php");
                break;

            case "CashoutFees":
                $("#callPageAjaxData").load("CashoutFees.php");
                break;

            case "TransferFees":
                $("#callPageAjaxData").load("TransferFees.php");
                break;

            case "HighestWalletBalance":
                $("#callPageAjaxData").load("HighestWalletBalance.php");
                break;

            case "TopUnUtilisedAdBalances":
                $("#callPageAjaxData").load("TopUnUtilisedAdBalances.php");
                break;


        }
    }

    $(document).ready(function () {
        <?php
        if(isset($_GET["target"]))
        {
        ?>
        var triggerName = "<?php echo $_GET["target"]; ?>";

        switchTarget(triggerName);
        $("#callingPageName").find('option[value="' + triggerName + '"]').prop("selected", true);
        <?php
        }else
        {
        ?>
        $("#callPageAjaxData").load("topSocialEarners.php");
        <?php
        }
        ?>
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
