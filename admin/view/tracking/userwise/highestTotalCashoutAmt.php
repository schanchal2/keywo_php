<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .table > thead > tr > th {
        padding: 5px 0;
    }

    .table > thead > tr > td{
        padding: 5px 0;
    }


    #newRegisteredUsers{
        margin-top: 0px;
    }

</style>
<div id="userRefferalAnalatics">

    <div class="row">
        <div id="highestcashAnalytics">

        </div>


    </div>    <div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 15px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="highetsITDPurchaseDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>

                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="cashoutStatus" class="form-control" style="    width: 100px;">
                                <option value="" disabled selected> Status</option>
                                <option value="1"="">Approved</option>
                                <option value="2">Rejected</option>
                                <option value="0">Pending</option>
                                <option value="all">All</option>
                            </select>
                        </label>
                    </div>


                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>

</div>
<div id="highestITDPurchaseUserList">

</div>


<!--- 2- User -->
<!--- 18- Highest Total Cashout  -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="18">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="18">
<script>
    $( document ).ready(function() {
        highetsCashoutUserList();
    });

    function highetsCashoutUserList()
    {

        var refferaldatevalue=($("#highetsITDPurchaseDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var cashoutStatus=($("#cashoutStatus").val());


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/highestCashoutUsersList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,status:cashoutStatus,orderedBy:"transaction_count"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                highestcashAnalytics(fromdate,todate,country);
                $("#highestITDPurchaseUserList").html("");
                $("#highestITDPurchaseUserList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#highetsITDPurchaseDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#highetsITDPurchaseDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#highetsITDPurchaseDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#highetsITDPurchaseDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        highetsCashoutUserList();
    });

    $("#countryToSearch").change(function(){
        highetsCashoutUserList();
    });

    $("#cashoutStatus").change(function(){
        highetsCashoutUserList();
    });


    $("#countryToSearch").chosen();

    function highestcashAnalytics(fromdate,todate,country)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/highestCashoutUsersAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#highestcashAnalytics").html("");
                $("#highestcashAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                highetsCashoutUserList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                highetsCashoutUserList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>

