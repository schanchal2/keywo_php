<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .table > thead > tr > th {
        padding: 5px 0;
    }

    .table > thead > tr > td{
        padding: 5px 0;
    }


    #newRegisteredUsers{
        margin-top: 0px;
    }

</style>
<div id="userRefferalAnalatics">


    <div class="row" style="">

        <div id="walletBalanceAnalytics">


        </div>

        <div id="errors" style="margin-bottom: 10px"></div>
        <div class="col-lg-12" style="margin-bottom: 10px">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="highestWalletbalDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>
                    <div class="input-group styled-select">
                        <select id="userGender" style="width: 105px">
                            <option value="" disabled selected> Gender</option>
                            <option value="male"> Male</option>
                            <option value="female"> Female</option>
                            <option value="other"> Other</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>

                    <div class="input-group styled-select">
                        <select id="accountLevel" style="width: 120px">
                            <option value="" disabled selected> KYC Level</option>
                            <option value="kyc_1"> Level 1</option>
                            <option value="kyc_2"> Level 2</option>
                            <option value="kyc_3"> Level 3</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>


                    <div class="form-inline  pull-right">
                        <label class="dropdownOptions">
                            <select id="accountStatus" class="form-control" style="    width: 115px;">
                                <option value="" disabled selected> AC Status</option>
                                <option value="0">De-Active</option>
                                <option value="1">Active</option>
                                <option value="2">Blocked</option>
                                <option value="">All</option>
                            </select>
                        </label>
                    </div>
                </form> </div></div>


    </div>


</div>
<div id="getHighestBalnceuserAjax">

</div>


<!--- 2- User -->
<!--- 26- Highest Wallet Balance -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="26">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="26">
<script>
    $( document ).ready(function() {
        getHighestBalnceWalletUserList();
    });

    function getHighestBalnceWalletUserList()
    {

        var refferaldatevalue=($("#highestWalletbalDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var gender=($("#userGender").val());
        var AClevel=($("#accountLevel").val());
        var ACStatus=($("#accountStatus").val());
        var country=($("#countryToSearch").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/getHighestBalnce.php",
            data: {
                fromdate: fromdate, todate: todate,gender:gender,AClevel:AClevel,ACStatus:ACStatus,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                balanceAnalytics(fromdate,todate,gender,AClevel,ACStatus,country);
                $("#getHighestBalnceuserAjax").html("");
                $("#getHighestBalnceuserAjax").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#highestWalletbalDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#highestWalletbalDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#highestWalletbalDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#highestWalletbalDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getHighestBalnceWalletUserList();
    });

    $("#countryToSearch").change(function(){
        getHighestBalnceWalletUserList();
    });

    $("#userGender").change(function(){
        getHighestBalnceWalletUserList();
    });

    $("#accountLevel").change(function(){
        getHighestBalnceWalletUserList();
    });

    $("#accountStatus").change(function(){
        getHighestBalnceWalletUserList();
    });


    $("#countryToSearch").chosen();


    function balanceAnalytics(fromdate,todate,gender,AClevel,ACStatus,country)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/balanceAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,gender:gender,AClevel:AClevel,ACStatus:ACStatus,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#walletBalanceAnalytics").html("");
                $("#walletBalanceAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getHighestBalnceWalletUserList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getHighestBalnceWalletUserList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/

</script>

