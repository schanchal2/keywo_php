<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>    <div id="errors" style="margin-bottom: 10px"></div>
<div id="userRefferalAnalatics">
    <div class="row" style="margin-top: 15px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="topPostEarnersnewDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>
                    <div class="input-group styled-select">
                        <select id="userGenderpostEarners" style="width: 105px">
                            <option value="" disabled selected> Gender</option>
                            <option value="male"> Male</option>
                            <option value="female"> Female</option>
                            <option value="other"> Other</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>
                    <div class="input-group styled-select" >
                        <select id="DeviceDatapostEarners" style="width: 95px">
                            <option value="" disabled selected> Device</option>
                            <option value="web"> Web</option>
                            <option value="mobile"> Mobile</option>
                            <option value="all"> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                    </div>

                    <!--<div class="form-inline  pull-right">
                        <label class="dropdownOptions">
                            <select id="mediaType" class="form-control" style="    width: 100px;">
                                <option value="" disabled selected> Media</option>
                                <option value="blog">Search</option>
                                <option value="TopPostEarnings">Social</option>
                                <option value="all">All</option>
                            </select>
                        </label>
                    </div>-->
                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>

<div id="newAllTopTotalEarnersSS">

</div>

<!--- 2- User -->
<!--- 14- Top Total Earnings (Search + Social) -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="14">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="14">

<script>
    var type="TopTotalEarnersSearchSocial";//serah+social eraner type
    $( document ).ready(function() {
        gettopSocialSearchUserListByDate(type);
    });

    function gettopSocialSearchUserListByDate(type)
    {

        var refferaldatevalue=($("#topPostEarnersnewDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var gender=($("#userGenderpostEarners").val());
        var device=($("#DeviceDatapostEarners").val());
       // var mediaType=($("#mediaType").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/filtersTopSearchSocail/totalSearchPlusSocialUsersList.php",
            data: {
                earningType:type,fromdate: fromdate, todate: todate,country:country,gender:gender,device:device
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#newAllTopTotalEarnersSS").html("");
                $("#newAllTopTotalEarnersSS").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#topPostEarnersnewDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#topPostEarnersnewDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#topPostEarnersnewDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#topPostEarnersnewDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        gettopSocialSearchUserListByDate(type);
    });

    $("#countryToSearch").change(function(){
        gettopSocialSearchUserListByDate(type);
    });

    $("#userGenderpostEarners").change(function(){
        gettopSocialSearchUserListByDate(type);
    });

    $("#DeviceDatapostEarners").change(function(){
        gettopSocialSearchUserListByDate(type);
    });

    $("#mediaType").change(function(){
        gettopSocialSearchUserListByDate(type);
    });

    $("#countryToSearch").chosen();

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                gettopSocialSearchUserListByDate(type);
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                gettopSocialSearchUserListByDate(type);
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

