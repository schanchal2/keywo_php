<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .table > thead > tr > th {
        padding: 5px 0;
    }

    .table > thead > tr > td{
        padding: 5px 0;
    }


    #newRegisteredUsers{
        margin-top: 0px;
    }

</style>
<div id="userRefferalAnalatics">

 <div id="cashoutFeesAnalytics">

 </div>
    <div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="cashoutFeesDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>

                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>

</div>
<div id="cashoutfeesajaxlist">

</div>



<!--- 2- User -->
<!--- 25- Cashout fees -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="25">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="25">
<script>
    $( document ).ready(function() {
        CashoutFeesUserList();
    });

    function CashoutFeesUserList()
    {

        var refferaldatevalue=($("#cashoutFeesDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/CashoutFeesUsersList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                cashoutFeesAnalytics(fromdate,todate,country);
                $("#cashoutfeesajaxlist").html("");
                $("#cashoutfeesajaxlist").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#cashoutFeesDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#cashoutFeesDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#cashoutFeesDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#cashoutFeesDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        CashoutFeesUserList();
    });

    $("#countryToSearch").change(function(){
        CashoutFeesUserList();
    });


    $("#countryToSearch").chosen();


    function cashoutFeesAnalytics(fromdate,todate,country)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/CashoutFeesUsersAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#cashoutFeesAnalytics").html("");
                $("#cashoutFeesAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                CashoutFeesUserList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                CashoutFeesUserList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

