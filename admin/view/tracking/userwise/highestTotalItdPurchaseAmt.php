<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .table > thead > tr > th {
        padding: 5px 0;
    }

    .table > thead > tr > td{
        padding: 5px 0;
    }


    #newRegisteredUsers{
        margin-top: 0px;
    }

</style>
<div id="userRefferalAnalatics">

    <div class="row">
        <div id="itd_purchase_analytics">

        </div>


    </div>
    <div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 15px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="highetsITDPurchaseDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>
                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>
                   <!-- <div class="input-group styled-select">
                        <select id="paymentMode" style="width: 150px">
                            <option value="" disabled selected> Payment Mode</option>
                            <option value="male"> Paypal</option>
                            <option value="female"> Wallet</option>
                            <option value="other"> Bitcoin</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>

                    <div class="input-group styled-select" >

                        <input style="width: 130px;z-index: 1" type="text" id="userID" class="form-control" value='' placeholder="User Id">

                        <span class="btn input-group-addon" type="submit" onclick="highestITDPurchaseUsersList()"><i class="fa fa-search"></i>  </span>

                    </div>-->

                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="postType" class="form-control" style="    width: 100px;">
                                <option value="" disabled selected> Export</option>
                                <option value="blog"="">PDF</option>
                                <option value="TopPostEarnings">EXCEL</option>
                                <option value="TopPostViewEarnings">CSV</option>
                            </select>
                        </label>
                    </div>


                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>

</div>
<div id="highestITDPurchaseUserList">

</div>


<!--- 2- User -->
<!--- 16- Highest Total ITD Purchased -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="16">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="16">
<script>
    $( document ).ready(function() {
        highestITDPurchaseUsersList();
    });

    function highestITDPurchaseUsersList()
    {

        var refferaldatevalue=($("#highetsITDPurchaseDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var paymentMode=($("#paymentMode").val());
        var userID=($("#userID").val());

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/highestITDPurchaseUsersList.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,userID:userID,paymentMode:paymentMode,orderedBy:"transaction_count"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                itd_purchase_analytics(fromdate,todate,country);
                $("#highestITDPurchaseUserList").html("");
                $("#highestITDPurchaseUserList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#highetsITDPurchaseDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#highetsITDPurchaseDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#highetsITDPurchaseDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#highetsITDPurchaseDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        highestITDPurchaseUsersList();
    });

    $("#countryToSearch").change(function(){
        highestITDPurchaseUsersList();
    });

    $("#paymentMode").change(function(){
        highestITDPurchaseUsersList();
    });

    $("#countryToSearch").chosen();

    function itd_purchase_analytics(fromdate,todate,country)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/wallet/itd_purchaseNtransfer_analytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,display:"itd_purchase",transaction_type:"purchase_itd"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#itd_purchase_analytics").html("");
                $("#itd_purchase_analytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }

    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                highestITDPurchaseUsersList();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                highestITDPurchaseUsersList();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

