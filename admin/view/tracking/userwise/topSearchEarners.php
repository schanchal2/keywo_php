<?php
session_start();
require_once("../../../config/config.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>

    <div id="filteredTabs" class="nav-tabs-custom ">
        <div class="btn-group btn-group-justified " role="group" aria-label="...">
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active bdrR"><a class="text-white" href="#SearchEarners" aria-controls="SearchEarners" role="tab" data-toggle="tab">Search Earners</a></li>
                <li role="presentation" class="bdrR"><a class="text-white" href="#KeywordEarners" aria-controls="KeywordEarners" role="tab" data-toggle="tab">Keyword Earners</a></li>
                <li role="presentation" class="bdrR"><a class="text-white" href="#AppEarners" aria-controls="AppEarners" role="tab" data-toggle="tab">App Earners</a></li>
                <li role="presentation" class="bdrR"><a class="text-white" href="#SearchAffEarners" aria-controls="SearchAffEarners" role="tab" data-toggle="tab">Search Affiliate Earners</a></li>
                <li role="presentation" class="bdrR"><a class="text-white" href="#KeywordAffEarners" aria-controls="KeywordAffEarners" role="tab" data-toggle="tab">Keyword Affiliate Earners</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="SearchEarners">
                <div id="newTopSearchUsersFilter" class="resetTableData">
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="KeywordEarners">
                <div id="newTopKeywordUsersFilter" class="resetTableData">
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="AppEarners">
                <div id="newTopAppUsersFilter" class="resetTableData">
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="SearchAffEarners">
                <div id="newTopSearchAffUsersFilter" class="resetTableData">
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="KeywordAffEarners">
                <div id="newTopKeywordAffUsersFilter" class="resetTableData">
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        $(".resetTableData").html("");
        $("#newTopSearchUsersFilter").load("filtersTopSearch/topEarners.php?type=search_earning");
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        var target = $(e.target).attr("href"); // activated tab
        $(".resetTableData").html("");
        switch (target) {
            case "#SearchEarners":
                $("#newTopSearchUsersFilter").load("filtersTopSearch/topEarners.php?type=search_earning");
                break;

            case "#KeywordEarners":
                $("#newTopKeywordUsersFilter").load("filtersTopSearch/topEarners.php?type=keyword_ownership_earning_search");
                break;

            case "#AppEarners":
                $("#newTopAppUsersFilter").load("filtersTopSearch/topEarners.php?type=app_earning");
                break;

            case "#SearchAffEarners":
                $("#newTopSearchAffUsersFilter").load("filtersTopSearch/topEarners.php?type=search_referral_earnings");
                break;

            case "#KeywordAffEarners":
                $("#newTopKeywordAffUsersFilter").load("filtersTopSearch/topEarners.php?type=keyword_affiliate_earning");
                break;

        }
    });
    </script>
