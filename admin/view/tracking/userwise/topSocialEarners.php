<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<style>
    .bdrR
    {
        border-right:  solid 1px white;
    }
    #filteredTabs .active .bg-darkblue {
        background-color: #35ccf1 !important;
    }

    #filteredTabs{
        margin-top: 15px;
    }


    #filter form[name="filter"] .countryToSearch button{
        background: none;
        border: 1px solid #ccc;
        text-indent: 1em;
        color: #b2b2b2;
        height: 34px;
        width: 268px;
        display: table-cell;
        -webkit-writing-mode: horizontal-tb;
        background: transparent;
        -webkit-appearance: none;
        -moz-appearance: none; }

    .user_checkbox,
    .checkboxStyle {
        display: none; }

    .checkboxStyle  + label,
    .user_checkbox + label {
        padding: 9px;
        display: inline-block;
        position: relative;
        margin-top: 7px; }

    .checkboxStyle  + label {
        padding-bottom: 0; }

    .user_checkbox + label:after,
    .checkboxStyle  + label:after {
        content: "\f096";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }

    .checkboxStyle + label:after {
        color: #fff; }

    .user_checkbox:checked + label:after,
    .checkboxStyle:checked + label:after {
        content: "\f14a";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }

    .checkboxStyle:checked + label:after {
        color: #fff; }
</style>

<div id="filteredTabs" class="nav-tabs-custom ">
    <div class="btn-group btn-group-justified " role="group" aria-label="...">


        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active bdrR"><a class="bg-darkblue text-white" href="#postViewEarnings" aria-controls="postViewEarnings" role="tab" data-toggle="tab">Post View Earners</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#postEarners" aria-controls="postEarners" role="tab" data-toggle="tab">Post Earners</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#keywordViewEarnings" aria-controls="keywordViewEarnings" role="tab" data-toggle="tab">Keyword View Earnings</a></li>
            <li role="presentation" class="bdrR"><a class="bg-darkblue text-white" href="#socialReshareEarnings" aria-controls="socialReshareEarnings" role="tab" data-toggle="tab">Social Re-Share Earnings</a></li>
        </ul>
    </div>

        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="postViewEarnings">

                    <div id="newToppostViewEarningsFilter" class="resetTableData">

                    </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="postEarners">

                    <div id="newToppostEarnersFilter" class="resetTableData">

                    </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="keywordViewEarnings">

                    <div id="newTopkeywordViewEarningsFilter" class="resetTableData">

                    </div>

            </div>
            <div role="tabpanel" class="tab-pane" id="socialReshareEarnings">

                    <div id="newTopsocialReshareEarningsFilter" class="resetTableData">

                    </div>

            </div>
        </div>

</div>



<script>
    $( document ).ready(function() {
        $(".resetTableData").html("");
        $("#newToppostViewEarningsFilter").load("filtersTopSocial/topSocialEarners.php?type=post_view_earnings");
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab
        switch (target)
        {
            case "#postViewEarnings":
                $(".resetTableData").html("");
                $("#newToppostViewEarningsFilter").load("filtersTopSocial/topSocialEarners.php?type=post_view_earnings");
                break;

            case "#postEarners":
                $(".resetTableData").html("");
                $("#newToppostEarnersFilter").load("filtersTopSocial/topSocialEarners.php?type=post_creator_earning");
                break;

            case "#keywordViewEarnings":
                $(".resetTableData").html("");
                $("#newTopkeywordViewEarningsFilter").load("filtersTopSocial/topSocialEarners.php?type=keyword_ownership_earning_social");
                break;

            case "#socialReshareEarnings":
                $(".resetTableData").html("");
                $("#newTopsocialReshareEarningsFilter").load("filtersTopSocial/topSocialEarners.php?type=post_sharer_earning");
                break;


        }
    });


</script>

