<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<div id="userRefferalAnalatics"><!--css are on this name-->
    <input type="text" hidden>
   <div id="blockedBidAnalytics">
   </div>

</div>
<div id="errors" style="margin-bottom: 10px"></div>
<div class="row">


    <div class="col-lg-12">
        <div id="filter" class="sign-upverification">
            <form action="" method="POST" class="form-inline" role="form" name="filter">
                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                    <?php
                    $date=date("Y-m-d");
                    ?>
                    <input style="width: 190px" type="text" id="blockedBidsUserDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                </div>
                <!--<div class="input-group styled-select" style="z-index: 1;">
                    <?php

                   // $result=getAllCountry($connSearch)["errMsg"];

                    ?>
                    <select id="countryToSearch" class="countryToSearch">
                        <option value="" disabled selected>Country</option>
                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                        <?php
                      /*  foreach($result as $value){
                            echo $value["name"];
                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                        }*/
                        ?>

                    </select>
                    <!-- <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>-->
                <!--</div>-->
        </div>
        </form>
    </div>

    <!-- Single button -->
</div>
</div>
</div>
<div id="blockedbidsUserAjaxData">


</div>


<!--- 2- User -->
<!--- 19- Blocked for Bids -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="19">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="19">
<script>

    $('#blockedBidsUserDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#blockedBidsUserDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#blockedBidsUserDate').on('drop', function(event) {
        event.preventDefault();
    });
    $( document ).ready(function() {
        getBlockedBidUserListByDate();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#blockedBidsUserDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,
    }).bind('datepicker-first-date-selected', function(event, obj) {
    }).bind('datepicker-change', function(event, obj) {
        var newdate = (obj.value);
        getBlockedBidUserListByDate();

    });



    $("#countryToSearch").chosen();

    function getBlockedBidUserListByDate()
    {
        var blogdatadate=($("#blockedBidsUserDate").val());
        var datedata=blogdatadate.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/trading/blockedForBidsUsers.php",
            data: {
                fromdate: fromdate, todate: todate,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                blockedBidAnalytics(fromdate,todate,country);
                $("#blockedbidsUserAjaxData").html("");
                $("#blockedbidsUserAjaxData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }


    $("#countryToSearch").change(function(){
        getBlockedBidUserListByDate();
    });



    function blockedBidAnalytics(fromdate,todate,country)
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/trading/blockedForBidsUsersAnalytics.php",
            data: {
                fromdate: fromdate, todate: todate,country:country
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#blockedBidAnalytics").html("");
                $("#blockedBidAnalytics").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }


    /************************ add to tracklist **********************/

    function addinTracklist() {
        var status=addUserToTracklist();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlockedBidUserListByDate();
                $('#addTotracklistmodal').modal('hide');

        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodal').modal('hide');
        }

    }


    function addinTracklistBulk() {
        var status=addUserToTracklistBulk();

        if(status["errCode"]=="-1")
        {

            $("#errors").html(bsAlert("success","Subject Added to Tracklist!"));
                getBlockedBidUserListByDate();
                $('#addTotracklistmodalBulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#addTotracklistmodalBulk').modal('hide');
        }

    }
    /************************ add to tracklist **********************/
</script>

