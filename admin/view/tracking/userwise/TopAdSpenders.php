<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<div id="userRefferalAnalatics">
    <div id="errors" style="margin-bottom: 10px"></div>
    <div class="row" style="margin-top: 0px;margin-bottom: 15px;">
        <div class="col-lg-12">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">
                    <div class="input-group">
                        <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                        <?php
                        $date=date("Y-m-d");
                        ?>
                        <input style="width: 190px" type="text" id="topadSpentDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    </div>


                    <div class="input-group styled-select" style="z-index: 1;">
                        <?php

                        $result=getAllCountry($connSearch)["errMsg"];

                        ?>
                        <select id="countryToSearch" class="countryToSearch">
                            <option value="" disabled selected>Country</option>
                            <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                            <?php
                            foreach($result as $value){
                                echo $value["name"];
                                echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                            }
                            ?>

                        </select>
                    </div>


                    <div class="input-group styled-select">
                        <select id="CampaignType" style="width: 150px">
                            <option value="" disabled selected> Campaign-Type</option>
                            <option value="search"> Search</option>
                            <option value="social"> Social</option>
                            <option value="widget"> Widget</option>
                            <option value="promotePost"> Promote Post</option>
                            <option value=""> All</option>
                        </select>
                        <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                    </div>

                </form>
            </div>
        </div>

        <!-- Single button -->
    </div>
</div>
<div id="topcampaginsUserListbyAjx">

</div>


<!--- 2- User -->
<!--- 27- Top Ad-Spenders -->
<input type="hidden" id="tracklistOptions" name="tracklistOptions" value="2">
<input type="hidden" id="tracklistOptionsBulk" name="tracklistOptionsBulk" value="2">
<input type="hidden" id="subtracklistOptions" name="subtracklistOptions" value="27">
<input type="hidden" id="subtracklistOptionsbulk" name="subtracklistOptionsbulk" value="27">

<script>
    $( document ).ready(function() {
        getTopAsSpentUsers();
    });

    function getTopAsSpentUsers()
    {

        var refferaldatevalue=($("#topadSpentDate").val());
        var datedata=refferaldatevalue.split("to");
        var fromdate=(datedata[0]).trim();
        var todate=(datedata[1]).trim();

        var country=($("#countryToSearch").val());
        var CampaignType=($("#CampaignType").val());
        var userID=($("#userID").val());


        var limit=($("#LimitedResult").val());
        if(((typeof limit)=="undefined") || limit=="")
        {
            limit=10;
        }


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/userwise/ads/getTopAsSpentUsers.php",
            data: {
                fromdate: fromdate, todate: todate,country:country,limit:limit,CampaignType:CampaignType,listtype:"first"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#topcampaginsUserListbyAjx").html("");
                $("#topcampaginsUserListbyAjx").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

    $('#topadSpentDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#topadSpentDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#topadSpentDate').on('drop', function(event) {
        event.preventDefault();
    });
    var todaysdate = new Date();
    todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
    var threeMonthsAgo = (3).months().ago();

    $('#topadSpentDate').dateRangePicker({
        autoClose: true,
        endDate: moment().startOf('day').toDate(),
        maxDays: 90,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {

        getTopAsSpentUsers();
    });

    $("#countryToSearch").change(function(){
        getTopAsSpentUsers();
    });

    $("#CampaignType").change(function(){
        getTopAsSpentUsers();
    });

    $("#LimitedResult").change(function(){
        getTopAsSpentUsers();
    });


    $("#countryToSearch").chosen();


</script>

