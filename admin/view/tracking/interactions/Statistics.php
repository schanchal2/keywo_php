<?php

require_once("../../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../config/db_config.php");
require_once("../../../helpers/coreFunctions.php");
require_once("../../../helpers/deviceHelper.php");
require_once("../../../helpers/date_helpers.php");
require_once("../../../helpers/arrayHelper.php");
require_once("../../../helpers/stringHelper.php");
require_once("../../../core/errorMap.php");
require_once "../../../model/geolocation/geoLocation.php";
require_once "../../../model/app/app_model.php";


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<html>
<head>
    <style>
        .col-md-12
        {
            margin-top: 15px;
        }



        .table-responsive {
            overflow-x: auto;
            min-height: 0.01%;
        }




        .container
        {
            margin-left: -10px;
        }


        audio, canvas, progress, video {
            display: inline-block;
            vertical-align: baseline;
            width: 400px;
            height: 200px;

        }



    </style>
</head>
<body>
<div class="row">
    <div class="col-md-12">
        <div id="filter" class="sign-upverification" style="margin-bottom:18px">
            <form action="" method="post" class=form-inline role="form" name="filter">

                <div class="input-group">
                    <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                    <?php
                    $date=date("Y-m-d");
                    ?>
                    <input style="width: 190px" type="text" id="intractionSearchDateRange" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                    <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                </div>
                <div class="input-group styled-select" style="z-index: 1;">
                    <?php

                    $result=getAllCountry($connSearch)["errMsg"];

                    ?>
                    <select id="countryToSearch" style="">
                        <option value="" disabled selected>Country</option>
                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                        <?php
                        foreach($result as $value){
                            echo $value["name"];
                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                        }
                        ?>

                    </select>
                    <!-- <span class="input-group-addon"><i class="fa fa-angle-down"></i> </span>-->
                </div>
                <div class="input-group styled-select">
                    <!-- <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span> -->
                    <!-- <input type="text" class="form-control" aria-label="Amount (to the nearest dollar)"> -->
                    <select id="userGender" style="width: 105px">
                        <option value="" disabled selected> Gender</option>
                        <option value="male"> Male</option>
                        <option value="female"> Female</option>
                        <option value="other"> Other</option>
                        <option value=""> All</option>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i> </span>
                </div>
                <div class="input-group styled-select" >
                    <select id="DeviceData" style="width: 95px">
                        <option value="" disabled selected> Device</option>
                        <option value="web"> Web</option>
                        <option value="mobile"> Mobile</option>
                        <option value=""> All</option>
                    </select>
                    <span class="input-group-addon"><i class="fa fa-angle-down"></i> </span>

                </div>
        </div>

        <div class="container-fluid">
            <div class=" col-lg-8 col-lg-offset-2  bg-lightGray1" style="border: 1px solid rgb(227, 227, 227);">
                <div id="chartContainer" style="height: 280px;"></div>
            </div>
        </div>
        <div id="userList" class="m-t-15">
            <table class="table text-center table-responsive">
                <thead>
                <tr>
                    <th>Qualified</th>
                    <th>Unqualified</th>
                    <!--<th>Anonymous</th>-->
                    <th>Total No.of Intractions</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td id="monthQualified">0</td>
                    <td id="monthunQualified">0</td>
                    <!--<td>44444</td>-->
                    <td id="monthTotal">0</td>
                </tr>
                </tbody>
            </table>
        </div>



        <script type="text/javascript">

            $('#intractionSearchDateRange').keydown(function() {
                //code to not allow any changes to be made to input field
                return false;
            });
            $('#intractionSearchDateRange').bind("cut copy paste",function(e) {
                e.preventDefault();
            });

            $('#intractionSearchDateRange').on('drop', function(event) {
                event.preventDefault();
            });


            $( document ).ready(function() {


                getIntractionAnalyticsByDate();
            });

            $("#userGender").change(function(){
                getIntractionAnalyticsByDate();
            });

            $("#DeviceData").change(function(){
                getIntractionAnalyticsByDate();
            });


            function getIntractionAnalyticsByDate()
            {

                var refferaldatevalue=$("#intractionSearchDateRange").val();
                var datedata=refferaldatevalue.split("to");
                var fromdate=(datedata[0]).trim();
                var todate=(datedata[1]).trim();

                var country=($("#countryToSearch").val());
                var gender=($("#userGender").val());
                var device=($("#DeviceData").val());


                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "../../../controller/tracking/intraction/intractionAnalytics.php",
                    data: {
                        fromdate: fromdate, todate: todate,country:country,gender:gender,device:device,resultFor:"combined"
                    },
                    beforeSend: function(){
                        $('#loadng-image').show();
                    },
                    success: function (data) {

                        if (data["errCode"]) {
                            if (data["errCode"] == "-1") {


                                var qualified=data["errMsg"]["qualified"];
                                var unqualified=data["errMsg"]["unqualified"];
                                var total=data["errMsg"]["total"];
                                $("#chartContainer").html("");

                                loadBarChart(qualified,unqualified,total);


                                $("#monthQualified").html("");
                                $("#monthQualified").html(qualified);

                                $("#monthunQualified").html("");
                                $("#monthunQualified").html(unqualified);

                                $("#monthTotal").html("");
                                $("#monthTotal").html(total);
                                $('#loadng-image').hide();
                            }
                            else {
                                console.log("failed to fetch analytics");
                            }

                        }


                    },
                    error: function () {
                        alert("fail");
                    }
                });

            }

            var todaysdate = new Date();
            todaysdate=todaysdate.setDate(todaysdate.getDate() - 1);
            var threeMonthsAgo = (3).months().ago();




            $('#intractionSearchDateRange').dateRangePicker({
                autoClose: true,
                endDate: moment().startOf('day').toDate(),
                maxDays: 90,
                time: {
                    enabled: false
                }
            }).bind('datepicker-first-date-selected', function(event, obj) {
                var date1 = obj.date1;
            }).bind('datepicker-change', function(event, obj) {
                var newdate = (obj.value);
                getIntractionAnalyticsByDate();
            });


            $("#countryToSearch").chosen();

            $("#countryToSearch").change(function(){
                getIntractionAnalyticsByDate();
            });


            function loadBarChart(qualified,unqualified,total) {
                Morris.Bar({
                    barSizeRatio: 0.23,
                    element: 'chartContainer',
                    data: [
                        {y: 'Qualified', a:  qualified },
                        {y: 'Unqualified', a:  unqualified },
                        {y: 'Total Intractions', a:  total }
                    ],
                    xkey: 'y',
                    ykeys: ['a'],
                    barColors: ['#0299be'],
                    backgroundColor: '#ccc',
                    labelColor: '#060',
                    labels: ['Count'],
                    hideHover: 'auto',
                    //resize: false,
                    gridEnabled: true,
                    gridIntegers: true,
                    yLabelFormat: function(y){
                        return y != Math.round(y)?'':y;
                    },

                });
            }
        </script>

        <body>
</html>

