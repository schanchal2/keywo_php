<?php
session_start();

include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/morris/morris.css" rel="stylesheet">


  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
<main>
    <div class="container-fluid">



        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Interactions</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                <label class="dropdownOptions">
                    <select id="callingPageName" class="form-control">
                        <option value="SearchStatistics" selected="">Search Statistics</option>
                        <option value="SocialStatistics">Social Statistics (View)</option>
                        <option value="Statistics">Statistics(Search+Social)</option>
                    </select>
                </label>
            </form>
        </div>
        <div id="callPageAjaxData">

        </div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/morris/morris.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>

<script>


    $('#callingPageName').on('change', function() {
        var pagename=this.value;

        switch(pagename)
        {
            case "SearchStatistics":
                $("#callPageAjaxData").load("SearchStatistics.php");
                break;

            case "SocialStatistics":
                $("#callPageAjaxData").load("SocialStatistics.php");
                break;

            case "Statistics":
                $("#callPageAjaxData").load("Statistics.php");
                break;
        }
    });
    $( document ).ready(function() {
        $("#callPageAjaxData").load("SearchStatistics.php");
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
