
<?php
session_start();

include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php";
?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Tracklist</span>
        </div>
        <div id="callPageAjaxData">
            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th>Agent Name</th>
                                <th>Total Entries Added</th>
                                <th>Total Entries Deleted</th>
                                <th>Total Tracklist Created</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td id=""> Dinesh Ghule </td>
                                    <td id=""> 1025 </td>
                                    <td id=""> 100 </td>
                                    <td id=""> 58 </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>

<script>



    $( document ).ready(function() {

    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
