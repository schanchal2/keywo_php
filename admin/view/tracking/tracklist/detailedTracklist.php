<?php
session_start();

include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
include "../../../model/geolocation/geoLocation.php";
include "../../../model/tracking/tracklist/tracklist.php";
$listtype = cleanQueryParameter($connAdmin, cleanXSS($_GET["listType"]));

$resultCategory=getAllTracklistCategory($connAdmin);
$resultCategory=$resultCategory["errMsg"];

$totalTracklist=getTotalTracklist($_SESSION["agent_id"],$connAdmin)["errMsg"];
//$totalSubFolders=getTotalSubFolders("1",$connAdmin)["errMsg"];
?>

<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Tracklist</span>
        </div>
        <div id="callPageAjaxData">
            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-6">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th>Total Entries</th>
                                <!--<th>Total Sub Folders Added</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="myTotalTracklists"> <?php echo $totalTracklist;?></td>
                                <!--<td id="totalSubFoldersAdded"> <?php //echo $totalSubFolders;?></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-6">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="post" class="form-inline pull-right" role="form" name="filter">
                                <div class="input-group styled-select pull-right" style="">

                                    <select id="tracklistType" style="width: 160px;">
                                        <?php

                                        foreach($resultCategory as $value)
                                        {
                                            echo $value["cat_name"];
                                            if($listtype==$value["cat_name"])
                                            {
                                                echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_name']."' selected>".$value['cat_name']."</option>";

                                            }else{
                                                echo "<option data-live-search=\"true\" data-tokens='".$value['cat_name']."' id='category-".$value['cat_id']."' value='".$value['cat_name']."'>".$value['cat_name']."</option>";

                                            }

                                        }
                                        ?>
                                    </select>

                                    <span class="input-group-addon"><i class="fa fa-folder-open-o"
                                                                       aria-hidden="true"></i> </span>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
<div id="errors"></div>

        <div id="loadTracklist">

        </div>
    </div>
</main>



<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<?php include "../trackingModal.php" ?>
<script>


    $(document).ready(function () {
       // $('#sidebarChanger').click();
        getMyTracklistPagination(1,"");
    });





    $("#LimitedResult").change(function () {
        getMyTracklistPagination(1,"");
    });

    $("#tracklistType").change(function () {
        var type = $("#tracklistType").val();
        getMyTracklistPagination(1,type)
    });

    $("#tracklistType").click(function () {
        clearAlertError();
    });


    function getMyTracklistPagination(pageno,type) {

        if (type == "") {
            var tracklistType = "<?php echo $listtype; ?>";
        } else {
            var tracklistType = type;
        }
        var limit = 10;
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../../controller/tracking/tracklist/getTracklistData.php",
            data: {
                page: pageno,limit: limit,tracklistType:tracklistType
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $("#loadTracklist").html("");
                $("#loadTracklist").html(data);
                $('#loadng-image').hide();
            },
            error: function () {
                alert("fail");
            }
        });
    }


    function getTotalTracklist() {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/tracklist/tracklistAnalytics.php",
            data: {
             analyticsType:"totalTracklist"
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                $("#myTotalTracklists").text("");
                $("#myTotalTracklists").text(data["errMsg"]);
                $('#loadng-image').hide();
            },
            error: function () {
                cosole.log("Error Get Analytics Total Tracklist.");
            }
        });
    }


    function deleteTracklistData(catid,subcatidn,adminid,Subject,subOwner)
    {

        var pagno=$("#hiddenpageTracklist").val();
        var tltype=$("#hiddenpageTracklistType").val();
        var status= deleteTracklist(catid,subcatidn,adminid,Subject,subOwner);

        if(status) {

            if(status["errCode"]=="-1")
            {
                $("#errors").html(bsAlert("success","Subjects Deleted From Tracklist"));
                    getTotalTracklist();
                    getMyTracklistPagination(pagno,tltype);

            }
            else if(status["errCode"]==65)
            {
                $("#errors").html(bsAlert("danger",status['errMsg']));
            }
        }


    }

    function deletemyTracklistBulk()
    {

        var tltype=$("#hiddenpageTracklistType").val();
        var status= deleteTracklistBulk();

        if(status) {

            if(status["errCode"]=="-1")
            {
                $("#errors").html(bsAlert("success","Subjects Deleted From Tracklist"));

                    getMyTracklistPagination(1,tltype);
                    getTotalTracklist();
                    $('#moveUpdatetracklistmodalbulk').modal('hide');

            }
            else if(status["errCode"]==65)
            {
                $("#errors").html(bsAlert("danger",status['errMsg']));
                $('#moveUpdatetracklistmodalbulk').modal('hide');
            }
        }

    }
    /***************************  Move Update Tracklist **************************/

  function updateTracklistSingle()
    {
        var pagno=$("#hiddenpageTracklist").val();
        var tltype=$("#hiddenpageTracklistType").val();

        var status= UpdateFromTracklist();
        if(status["errCode"]=="-1")
        {


                $("#errors").html(bsAlert("success","Tracklist Updated Successfully"));
                getTotalTracklist();
                getMyTracklistPagination(1,status["cat_name"]);

                var newstat=status["cat_name"];

                $("#tracklistType").val(newstat).change();
                $('#moveUpdatetracklistmodal').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#moveUpdatetracklistmodal').modal('hide');
        }
    }



    function updateMoveTracklistBulkGlobal()
    {

        var tltype=$("#hiddenpageTracklistType").val();

        var status= updateMoveTracklistBulk();
        if(status["errCode"]=="-1")
        {
            $("#errors").html(bsAlert("success","Tracklist Updated Successfully"));
                getTotalTracklist();
                getMyTracklistPagination(1,status["cat_name"]);

                var newstat=status["cat_name"];

                $("#tracklistType").val(newstat).change();
                $('#moveUpdatetracklistmodalbulk').modal('hide');


        }
        else if(status["errCode"]==65)
        {
            $("#errors").html(bsAlert("danger",status['errMsg']));
            $('#moveUpdatetracklistmodalbulk').modal('hide');
        }
    }


    /***************************  Move Update Tracklist **************************/

</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
