<?php
session_start();

include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../../model/tracking/tracklist/tracklist.php");
include "../../../model/geolocation/geoLocation.php";

$CatResult=getAllTracklistCategory($connAdmin);
$CatResult=$CatResult["errMsg"];
$totalTracklist=getTotalTracklist($_SESSION["agent_id"],$connAdmin)["errMsg"];
//$totalSubFolders=getTotalSubFolders("1",$connAdmin)["errMsg"];
?>

<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<style>
    .hiddenRow {
        padding: 0 !important;
    }

    .user_checkboxnew,
    .checkboxStyle {
        display: none; }

    .checkboxStyle  + label,
    .user_checkboxnew + label {
        padding: 7px;
        display: inline-block;
        position: relative;
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .checkboxStyle  + label {
        padding-bottom: 0; }

    .user_checkboxnew + label:after,
    .checkboxStyle  + label:after {
        content: "\f096";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }

    .checkboxStyle + label:after {
        color: #fff; }

    .user_checkboxnew:checked + label:after,
    .checkboxStyle:checked + label:after {
        content: "\f14a";
        font-size: 14px;
        position: absolute;
        top: 0px;
        left: 3px;
        color: #027a98; }


</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Tracking & Monitoring</h1><span id="pageTitleData">Tracklist Summary</span>
        </div>
        <div id="callPageAjaxData">
            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-6">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th>Total Entries</th>
                                <!--<th>Total Sub Folders Added</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id="myTotalTracklists"> <?php echo $totalTracklist;?> </td>
                                <!--<td id="totalSubFoldersAdded"> <?php //echo $totalSubFolders;?>  </td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--<div class="col-lg-2 col-lg-offset-2 pull-right">
                        <button type="button" data-toggle="modal" id="checkmodalopenAccordian" data-target="#addNewCatModal" class="btn btn-default" style="color: #fff;background-color: #0299be;border-color: #0299be;">Add New Folder</button>
                    </div>-->

                  <div class="col-lg-2 col-lg-offset-2 pull-right">
                      <a class="btn btn-default" style="color: #fff;background-color: #0299be;border-color: #0299be;" href="detailedTracklist.php?listType=Keyword" target="_blank">Go To Tracklist </a>
                  </div>


                </div>

            </div>
        </div>

        <div id="userList" class="m-t-15"><!--due to global id css design id is same-->
            <table id="MainCategoryList" class="table  text-center table-responsive" style="border-collapse:collapse;">



            <thead>
            <tr>
                <!--<th>
                    <input id="categoryListCBox" type="checkbox" class="checkboxStyle">
                    <label for="categoryListCBox" class="fa"></label>
                </th>-->
                <th> Folder Name</th>
                <th>
                <span id="actionsIdmain">Total Entries</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span id="functionsIDmain"><i onclick="deleteFoldersBulk()" class="" aria-hidden="true"></i></span><!-- class removed  fa fa-trash-o fa-lg btn-->
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($CatResult as $Category)
            {
                $result=getTotalTracklistAddedInCategory($_SESSION["admin_id"],$Category["cat_name"],$connAdmin);
                $resultCount=$result["errMsg"];
                ?>
                <tr id="deleteIDtr-<?php echo $Category["cat_id"];  ?>" data-toggle="collapse" data-target="#<?php echo $Category["cat_id"];  ?>" class="accordion-toggle">
                    <!--<td style="width:10px;text-align: center">
                        <?php


                           /*if($Category["cat_id"]==1 || $Category["cat_id"]==2 ||$Category["cat_id"]==3 ||$Category["cat_id"]==4 ||$Category["cat_id"]==5 )
                           {

                           }else
                           {
                               */?>
                               <input class="user_checkboxnew" id="user_checkboxnew<?php /*echo ($Category["cat_id"]); */?>"
                                      type="checkbox" name="user_checkboxnew[]"
                                      value="<?php /*echo $Category["cat_id"] . "," . $Category["updated_by"] . ",user_checkboxnew" . ($Category["cat_id"]); */?>">
                               <label for="user_checkboxnew<?php /*echo ($Category["cat_id"]); */?>" class="fa"></label>


                              <?php
/*                           }*/

                        ?>

                    </td> -->
                    <td><a href="detailedTracklist.php?listType=<?php echo $Category["cat_name"];  ?>" target="_blank"><?php echo $Category["cat_name"];  ?> </a></td>
                    <td id="categoryAnalytics-<?php echo $Category["cat_id"];  ?>"><?php echo $resultCount  ?></td>
                </tr>

                <tr>
                    <td colspan="2" class="hiddenRow ">
                        <div class="accordian-body collapse  panel-collapsecdcd" id="<?php echo $Category["cat_id"];  ?>">

                        <div id="subCategoryAnalytics-<?php echo $Category["cat_id"];  ?>">

                        </div>
                        </div>
                    </td>
                </tr>
            <?php
            }
            ?>


            </tbody>
        </table>
        </div>
    </div>
</main>
<div class="modal fade" id="addNewCatModal" tabindex="-1" role="dialog"
     aria-labelledby="addNewCatModal" aria-hidden="true" data-backdrop="static"
     data-keyboard="false">

    <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center" style="width:30%">
            <div class="modal-content">
                <div class="modal-header" style="background: #027a98;padding: 10px;height: 47px;">
                    <button type="button" style="opacity: 1;font-size: xx-large;color: white;"class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                    <h4 class="modal-title" id="addNewCatModalLabel"
                        style="color:white;font-weight: bold">Add New Folder</h4>
                </div>
                <div class="modal-body">
                    <form name="addNewCatMain" id="addNewCatMain">
                        <div class="register-box-body" id="addNewCatModalBody">
                            <div class="title clearfix" style="border-bottom: none">
                                <div class="" role="form" name="">
                                    <label class="dropdownOptions">
                                      <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-folder-open-o"></i></span>
                                                <input maxlength="35"
                                                       onKeydown="Javascript: if(event.keyCode == 13){
                                                        event.preventDefault();
                                                        }if (event.keyCode==13)
                                                            addNewCategory('<?php echo $_SESSION["admin_id"]; ?>');"
                                                       autocomplete="off" id="folderNameID" type="text" class="form-control" name="folderNameID" placeholder="Folder Name">
                                            </div>
                                    </label>
                                </div>
                            </div>

                            <div class="row" style="">
                                <div class="col-lg-4 ">
                                </div>
                                <div class=" pull-right" style="margin: 0px;padding: 0px">
                                    <div class="col-lg-4 ">
                                        <button type="button"  style="padding: 6px 12px; border-color: rgb(242, 234, 234);background-color: #e7f1f2;" class="btn btn-default" onclick="addNewCategory('<?php echo $_SESSION["admin_id"]; ?>')">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>

<script>



    var lastajax=[];
    $('.accordian-body').on('show.bs.collapse', function () {
        $("#"+lastajax[0]).html("");
        lastajax.pop();
        var thisid= $(this).val( $(this).attr("id") );

        var ids=(thisid[0]).getAttribute("id");
        lastajax.push("subCategoryAnalytics-"+ids);
        $("#subCategoryAnalytics-"+ids).load("../../../controller/tracking/tracklist/subCatAnalytics.php?id="+ids);

        $(this).closest("table")
            .find(".collapse.in")
            .not(this)
            .collapse('toggle');
    })



    $(document).ready(function () {

    });


    function deleteSubFoldersBulk()
    {
        /******************************* ***************************************/

        var status;
        var tags = document.getElementsByName('user_checkbox[]');
        var subjectids = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var subcatID=splitmystring[0];
                var adminid=splitmystring[1];

                var mydata=subcatID+":"+adminid;
                subjectids.push(mydata);

            }

        }

        if(subjectids.length === 0)
        {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }


        $.ajax({

            type: "POST",
            dataType:"json",
            url: "../../../controller/tracking/tracklist/crudTracklist.php",
            data: {
                subCategoryandIDAgentID: subjectids,crudType:"bulkdeleteSubCategory"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },

            success: function(user) {

                if(user["errCode"]==-1)
                {

                    bootbox.alert("<h4>Sub Folders Deleted Successfully.</h4>", function() {
                        var str=window.lastajax[0];
                        str=str.split("-");
                        $("#"+window.lastajax[0]).html("");
                        $("#"+window.lastajax[0]).load("../../../controller/tracking/tracklist/subCatAnalytics.php?id="+str[1]);
                        getTotalTracklist();
                        totalTracklistbyid(str[1]);
                        totalSubFoldersAnalytics();
                    });
                    $('#loadng-image').hide();
                }else
                {
                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function() {
                    });
                    $('#loadng-image').hide();
                }



            },
            error: function() {

                console.log("Failed to  delete Tracklist");
            }
        });


    }

    function getTotalTracklist() {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/tracklist/tracklistAnalytics.php",
            data: {
                analyticsType:"totalTracklist"
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                $("#myTotalTracklists").text("");
                $("#myTotalTracklists").text(data["errMsg"]);
                $('#loadng-image').hide();
            },
            error: function () {
                cosole.log("Error Get Analytics Total Tracklist.");
            }
        });
    }

    function totalTracklistbyid(cat_id) {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/tracklist/tracklistAnalytics.php",
            data: {
                analyticsType:"totalTracklistbyid",cat_id:cat_id
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                $("#categoryAnalytics-"+cat_id).text("");
                $("#categoryAnalytics-"+cat_id).text(data["errMsg"]);
                $('#loadng-image').hide();
            },
            error: function () {
                cosole.log("Error Get Analytics Total Tracklist.");
            }
        });
    }


    function totalSubFoldersAnalytics() {

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/tracklist/tracklistAnalytics.php",
            data: {
                analyticsType:"totalSubFoldersAdded"
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                $("#totalSubFoldersAdded").text("");
                $("#totalSubFoldersAdded").text(data["errMsg"]);
                $('#loadng-image').hide();
            },
            error: function () {
                cosole.log("Error Get Analytics Total Sub Folders.");
            }
        });
    }

    $(function() {
        $('#folderNameID').on('keypress', function(e) {
            if (e.which == 32)
                return false;
        });
    });

    $('#folderNameID').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#folderNameID').on('drop', function(event) {
        event.preventDefault();
    });


    function addNewCategory(adminEmail)
    {


        var catName=$("#folderNameID").val();

        if(catName=="")
        {
//            bootbox.alert("<h4>Please Enter Name for new Category!</h4>", function() {
//
//            });

            return false;
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../../controller/tracking/tracklist/tracklistAnalytics.php",
            data: {
                analyticsType:"addNewMainCategory",adminEmail:adminEmail,catName:catName
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {

                if(data["errCode"]==-1)
                {
                    var mydata='<td style="width:10px;text-align: center"> <input class="user_checkboxnew" id="user_checkboxnew'+data['catID']+'" type="checkbox" name="user_checkboxnew[]" value="'+data['catID']+','+adminEmail+',user_checkboxnew'+data['catID']+'"> <label for="user_checkboxnew'+data['catID']+'" class="fa"></label> </td>';
                    $('#userList tbody').append('<tr id="deleteIDtr-'+data['catID']+'" data-toggle="collapse" data-target="#'+data['catID']+'" class="accordion-toggle">'+mydata+'<td><a href="detailedTracklist.php?listType=General" target="_blank">'+catName+' </a></td><td>0</td></tr><tr> <td colspan="3" class="hiddenRow"> <div class="accordian-body collapse" id="'+data['catID']+'"> <div id="subCategoryAnalytics-'+data['catID']+'"> </div> </div> </td> </tr>');
                    $("#addNewCatModal").modal("hide");
                    //location.reload();
                    $('#loadng-image').hide();
                    $('#addNewCatMain').trigger("reset");
                }else {

                    bootbox.alert("<h4>"+data['errMsg']+"</h4>", function() {

                    });
                    $('#loadng-image').hide();
                }

            },
            error: function () {
                cosole.log("Error Get Analytics Total Sub Folders.");
            }
        });

    }


    $(function () {
        var _last_selected = null, checkboxes = $("#userList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#categoryListCBox').on('click', function () {
            if (this.checked) {
                $('.user_checkboxnew').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkboxnew').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkboxnew').on('click', function () {
            if ($('.user_checkboxnew:checked').length == $('.user_checkboxnew').length) {
                $('#categoryListCBox').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#user_checkboxnew').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


    function deleteFoldersBulk()
    {
        /******************************* ***************************************/

        var status;
        var tags = document.getElementsByName('user_checkboxnew[]');
        var subjectids = new Array();
        for (var i = 0; i < tags.length; ++i) {
            if (tags[i].checked) {

                // emails.push(tags[i].value);
                var mystring=(tags[i].value).trim();
                var splitmystring=mystring.split(",");

                var catID=splitmystring[0];
                var adminEmail=splitmystring[1];

                var mydata=catID+":"+adminEmail;
                subjectids.push(mydata);

            }

        }

        if(subjectids.length === 0)
        {
            bootbox.alert("<h4>Please Select Checkbox.</h4>", function() {
            });
            return false;
        }


        $.ajax({

            type: "POST",
            dataType:"json",
            url: "../../../controller/tracking/tracklist/crudTracklist.php",
            data: {
                catIDAdminEmail: subjectids,crudType:"bulkdeleteCategory"
            },
            async: false,
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function(user) {

                if(user["errCode"]==-1)
                {

                    bootbox.alert("<h4>Folders Deleted Successfully.</h4>", function() {
                     getTotalTracklist();
                        totalSubFoldersAnalytics();



                        for (i = 0; i < subjectids.length; i++) {
                            var splitmystring=subjectids[i].split(":");
                            var delid="deleteIDtr-"+splitmystring[0];
                            $("#"+delid).closest('tr').remove();

                        }
                        $('#loadng-image').hide();
                    });
                }else
                {
                    bootbox.alert("<h4>"+user["errMsg"]+"</h4>", function() {
                    });
                    $('#loadng-image').hide();
                }



            },
            error: function() {

                console.log("Failed to  delete Folders");
            }
        });


    }

    $("#checkmodalopenAccordian").click(function(){
        $('.panel-collapse,.in')
            .collapse('hide');
    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
