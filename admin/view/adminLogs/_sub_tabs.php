<div class="row m-b-15">
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-4">
                <ul class="nav nav-pills nav-justified nav-pills--1" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#overview" role="tab" data-toggle="tab">Overview </a>
                    </li>
                    <li role="presentation">
                        <a href="#activity_log" role="tab" data-toggle="tab">Activity Log</a>
                    </li>
                </ul>
            </div>
            <div class="col-xs-8">
                <div class="row">
                    <!--     <div class="col-xs-4">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <input type="text" id="userListDateRange" class="form-control" value="2017-05-02">
                        </div>
                    </div> -->
                    <div class="col-xs-4 pull-right p-l-0">
                        <label class="dropdownOptions">
                            <select class="form-control">
                                <option value="USD" selected>Customer support</option>
                                <option value="SGD">SGD</option>
                                <option value="BTC">BTC</option>
                                <option value="ITD">ITD</option>
                            </select>
                        </label>
                    </div>
                    <div class="col-xs-4 pull-right p-l-0">
                        <label class="dropdownOptions">
                            <select class="form-control">
                                <option value="USD" selected>HOD (Tom)</option>
                                <option value="SGD">SGD</option>
                                <option value="BTC">BTC</option>
                                <option value="ITD">ITD</option>
                            </select>
                        </label>
                    </div>
                    <div class="input-group pull-right col-xs-4 p-x-15 p-b-15">
                        <span class="form-control">1 March 2014</span>
                        <span class="input-group-addon"> <i class="fa fa-folder-open"></i> </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="tab-content p-t-15">
                    <div role="tabpanel" class="tab-pane active" id="overview">
                        <div class="table--lightBlue2 table-responsive">
                            <?php include '_qt.php' ?>
                        </div>
                        <div class="table--lightBlue2 table-responsive">
                            <?php include '_sr.php' ?>
                        </div>
                        <div class="table--lightBlue2 table-responsive">
                            <?php include '_it.php' ?>
                        </div>
                        <!--  -->
                        <div class="table--blue table-responsive">
                            <?php include '_qt.php' ?>
                        </div>
                        <div class="table--blue table-responsive">
                            <?php include '_sr.php' ?>
                        </div>
                        <div class="table--blue table-responsive">
                            <?php include '_it.php' ?>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="activity_log">
                        <div class="table--lightBlue2">
                            <?php //include '_activitylog_1.php' ?>
                            <?php include '_activitylog_2.php' ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
