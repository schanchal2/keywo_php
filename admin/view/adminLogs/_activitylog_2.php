
<div class="row">
    <div class="col-xs-12">
        <table class="table text-center">
            <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Timestamp</th>
                    <th>Activity performed </th>
                    <th>Module</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td class="timestamp">
                        <span class="date "> 08-03-2017 </span>
                        <time>10:12:12</time>
                    </td>
                    <td>Log in</td>
                    <td>Customer support</td>
                </tr>
                <tr>
                    <td>2</td>
                    <td class="timestamp">
                        <span class="date "> 08-03-2017 </span>
                        <time>10:12:12</time>
                    </td>
                    <td>Log out</td>
                    <td>Customer support</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>


