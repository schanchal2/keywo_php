<?php include "../layout/header.php";
checkGroupAccess();
?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/css/datatable_custom.css">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Logs</h1>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div class="row m-t-15  ">
            <div class="col-sm-12">
                <div role="tabpanel">
                    <ul class="nav nav-pills nav-justified nav-pills--1" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#customer_support" role="tab" data-toggle="tab">Access Control </a>
                        </li>
                       <!-- <li role="presentation">
                            <a href="#communication" role="tab" data-toggle="tab">Communication</a>
                        </li>-->
                    </ul>
                    <div class="tab-content p-t-15">
                        <div role="tabpanel" class="tab-pane active" id="customer_support">
                            <div class="wrapper">
                                <div class="row">
                                    <div class="col-md-12">
                                        <!-- Custom Tabs -->
                                        <div class="nav-tabs-custom">
                                            <ul class="nav nav-tabs" style="background: #35ccf1;">
                                                <li class="active"><a href="#loginLogs" data-toggle="tab">Login</a></li>
                                                <li><a href="#addNewAgent" data-toggle="tab">Add Agent</a></li>
                                                <li><a href="#deleteAgent" data-toggle="tab">Delete Agent</a></li>
                                                <li><a href="#addGroup" data-toggle="tab">Add Group</a></li>
                                                <li><a href="#deleteGroup" data-toggle="tab">Delete Group</a></li>
                                                <li><a href="#addIp" data-toggle="tab">Add IP</a></li>
                                                <li><a href="#deleteIp" data-toggle="tab">Delete IP</a></li>
                                            </ul>
                                            <div class="tab-content" style="border: solid 1px #bdeefa;padding:10px;">
                                                <div class="tab-pane active" id="loginLogs">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="loginformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="loginLogsDateRangePiker"
                                                                                           name="loginLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readLoginLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="logintextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofLoginLogs">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="addNewAgent">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="addNewAgentformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="addNewAgentLogsDateRangePiker"
                                                                                           name="addNewAgentLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readaddNewAgentLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="addNewAgenttextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofaddNewAgentLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="deleteAgent">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="deleteAgentformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="deleteAgentLogsDateRangePiker"
                                                                                           name="deleteAgentLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readdeleteAgentLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="deleteAgenttextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofdeleteAgentLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="addGroup">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="addGroupformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="addGroupLogsDateRangePiker"
                                                                                           name="addGroupLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readaddGroupLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="addGrouptextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofaddGroupLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="deleteGroup">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="deleteGroupformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="deleteGroupLogsDateRangePiker"
                                                                                           name="deleteGroupLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readdeleteGroupLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="deleteGrouptextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofdeleteGroupLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="addIp">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="addIpformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="addIpLogsDateRangePiker"
                                                                                           name="addIpLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readaddIpLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="addIptextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofaddIpLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="deleteIp">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="callout callout"
                                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                                <form class="form-inline" name="deleteIpformexport" method="post"
                                                                      action="javascript:;">
                                                                    <div class="row">
                                                                        <div class="col-sm-9">
                                                                            <div class="form-group">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">
                                                                                        <i class="fa fa-calendar"></i>
                                                                                    </div>
                                                                                    <input type="text" placeholder="Select Date Range"
                                                                                           class="form-control"
                                                                                           id="deleteIpLogsDateRangePiker"
                                                                                           name="deleteIpLogsDateRangePiker"
                                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                                           autocomplete="off" size="23">
                                                                                </div><!-- /.input group -->
                                                                            </div><!-- /.form group -->

                                                                            <input type="text" value="login" name="type" hidden="">
                                                                            <div class="form-group">
                                                                                <button type="submit"
                                                                                        class="btn btn-default fa fa-search"
                                                                                        onclick="readdeleteIpLogs()"></button>
                                                                            </div>


                                                                        </div>
                                                                        <div class="form-group col-sm-3" style="float:right">
                                                                            <div class="">
                                                                                <label>Filter:</label>
                                                                                <input class="form-control" id="deleteIptextSearch"
                                                                                       type="text" placeholder="Search Logs">
                                                                            </div>
                                                                        </div>
                                                                    </div>


                                                                </form>
                                                            </div>

                                                            <div id="dataofdeleteIpLogs">
                                                            </div>
                                                        </div>
                                                        <!-- /.col -->
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.tab-content -->
                                        </div>
                                        <!-- nav-tabs-custom -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="communication">
                          asd
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 

         -->
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>

<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");// activated tab

        switch (target) {
            case "#loginLogs":
                readLoginLogs();
                break;

            case "#addNewAgent":
                readaddNewAgentLogs();
                break;
            case "#deleteAgent":
                readdeleteAgentLogs();
                break;

            case "#addGroup":
                readaddGroupLogs();
                break;

            case "#deleteGroup":
                readdeleteGroupLogs();
                break;

            case "#addIp":
                readaddIpLogs();
                break;

            case "#deleteIp":
                readdeleteIpLogs();
                break;
        }
    });

    $(document).ready(function () {
        readLoginLogs();
    });

    var todaysdate = new Date();
    var threeMonthsAgo = (3).months().ago();

    /*********************************** login *************************************/
    $('#loginLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#loginLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#loginLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#loginLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readLoginLogs() {

        var datecapctured = $("#loginLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/ReadLoginLogs.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofLoginLogs").html("");
                $("#dataofLoginLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** login *************************************/



    /*********************************** add agent *************************************/
    $('#addNewAgentLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#addNewAgentLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#addNewAgentLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#addNewAgentLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readaddNewAgentLogs() {

        var datecapctured = $("#addNewAgentLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/ReadAddAgentsLogs.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofaddNewAgentLogs").html("");
                $("#dataofaddNewAgentLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** add agent *************************************/


    /*********************************** delete agent *************************************/
    $('#deleteAgentLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#deleteAgentLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#deleteAgentLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#deleteAgentLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readdeleteAgentLogs() {

        var datecapctured = $("#deleteAgentLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/deleteAgentLog.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofdeleteAgentLogs").html("");
                $("#dataofdeleteAgentLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** delete agent *************************************/

    /*********************************** addGroup agent *************************************/
    $('#addGroupLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#addGroupLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#addGroupLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#addGroupLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readaddGroupLogs() {

        var datecapctured = $("#addGroupLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/addGroup.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofaddGroupLogs").html("");
                $("#dataofaddGroupLogs").html(data);

            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** addGroup agent *************************************/


    /*********************************** deleteGroup agent *************************************/
    $('#deleteGroupLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#deleteGroupLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#deleteGroupLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#deleteGroupLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readdeleteGroupLogs() {

        var datecapctured = $("#deleteGroupLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/deleteGroup.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofdeleteGroupLogs").html("");
                $("#dataofdeleteGroupLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** delete agent *************************************/


    /*********************************** addIp agent *************************************/
    $('#addIpLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#addIpLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#addIpLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#addIpLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readaddIpLogs() {

        var datecapctured = $("#addIpLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/addIp.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofaddIpLogs").html("");
                $("#dataofaddIpLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** delete agent *************************************/

    /*********************************** deleteIp agent *************************************/
    $('#deleteIpLogsDateRangePiker').dateRangePicker({
        autoClose: true,
        startDate: threeMonthsAgo,
        endDate: todaysdate,
    }).bind('datepicker-first-date-selected', function (event, obj) {
    }).bind('datepicker-change', function (event, obj) {
        var newdate = (obj.value);

    });


    $('#deleteIpLogsDateRangePiker').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#deleteIpLogsDateRangePiker').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#deleteIpLogsDateRangePiker').on('drop', function(event) {
        event.preventDefault();
    });

    function readdeleteIpLogs() {

        var datecapctured = $("#deleteIpLogsDateRangePiker").val();
        var datedata = datecapctured.split("to");
        var fromdate = (datedata[0]).trim();
        var todate = (datedata[1]).trim();

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/adllogReaders/deleteIp.php",
            data: {
                fromdate: fromdate, todate: todate
            },
            beforeSend: function(){
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                $("#dataofdeleteIpLogs").html("");
                $("#dataofdeleteIpLogs").html(data);
            },
            error: function () {
                alert("fail");
            }
        });
    }
    /*********************************** delete agent *************************************/
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
