<?php include "../layout/header.php"; ?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Admin Logs</h1>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div class="row m-t-15  ">
            <div class="col-sm-12">
                <div role="tabpanel">
                    <ul class="nav nav-pills nav-justified nav-pills--1" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#customer_support" role="tab" data-toggle="tab">Customer support </a>
                        </li>
                        <li role="presentation">
                            <a href="#andm" role="tab" data-toggle="tab">T & M</a>
                        </li>
                        <li role="presentation">
                            <a href="#accounts" role="tab" data-toggle="tab">Accounts</a>
                        </li>
                        <li role="presentation">
                            <a href="#hr" role="tab" data-toggle="tab">HR</a>
                        </li>
                        <li role="presentation">
                            <a href="#uam" role="tab" data-toggle="tab">UAM</a>
                        </li>
                        <li role="presentation">
                            <a href="#mm" role="tab" data-toggle="tab">MM</a>
                        </li>
                        <li role="presentation">
                            <a href="#communication" role="tab" data-toggle="tab">Communication</a>
                        </li>
                    </ul>
                    <div class="tab-content p-t-15">
                        <div role="tabpanel" class="tab-pane active" id="customer_support">sgdfgd
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="andm">dfgdgd
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="accounts">dfgddfgg
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="hr">
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="uam">
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="mm">
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="communication">
                            <?php include '_sub_tabs.php'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 

         -->
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
var todaysdate = new Date();
 $('#userListDateRange').dateRangePicker({
        autoClose: true,
        startDate: todaysdate,
        endDate: todaysdate,

        //format: 'DD.MM.YYYY HH:mm',
        time: {
            enabled: false
        }
    }).bind('datepicker-first-date-selected', function(event, obj) {
        var date1 = obj.date1;
    }).bind('datepicker-change', function(event, obj) {
        getUserListByDate();
        var newdate = (obj.value);
    })
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
