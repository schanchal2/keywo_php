<?php
session_start();
include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../model/spamPost/spam_post_model.php');
checkGroupAccess();
$result = getCategoryandLimit();
$allCategories = $result["errMsg"];

?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix without-subtitle">
                <h1 class="pull-left">Report Post</h1><span>Post Reports</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="col-lg-12">
                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                            <?php
                            $date=date("Y-m-d");
                            ?>
                            <input style="width: 190px" type="text" id="getBlockedPostDate" class="form-control" value='<?php echo $date." to ".$date; ?>' placeholder="Select Date Range">
                            <!-- <span class="input-group-addon"><i class="fa fa-search"></i></span>-->
                        </div>

                        <div class="form-inline pull-right">
                            <label class="dropdownOptions">
                                <select id="postBlockReasonType" class="form-control" style="    width: 100px;">
                                    <option value="" disabled selected> Reason</option>
                                    <?php
                                    foreach($allCategories as $key=>$reason)
                                    {
                                        echo "<option value='{$key}'>".ucfirst($key)."</option>";
                                    }
                                    ?>
                                    <option value=''>All</option>
                                </select>
                            </label>
                        </div>

                        <div class="form-inline pull-right" style="margin-right: 10px;">
                            <label class="dropdownOptions">
                                <select id="postType" class="form-control" style="    width: 100px;">
                                    <option value="" disabled selected> Post</option>
                                    <option value="blog">Blog</option>
                                    <option value="video">Video</option>
                                    <option value="image">Image</option>
                                    <option value="audio">Audio</option>
                                    <option value="status">Status</option>
                                    <option value="">All</option>
                                </select>
                            </label>
                        </div>
                    </form>
                </div>

                <!-- Single button -->
                <div id="getReportedPostAjaxData">

                </div>
            </div>


        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script>


        $( document ).ready(function() {
           getReportedPostByDate();
        });
        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#getBlockedPostDate').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function(event, obj) {
        }).bind('datepicker-change', function(event, obj) {

            var newdate = (obj.value);
            getReportedPostByDate();
        });


        function getReportedPostByDate()
        {
            var blogdatadate=($("#getBlockedPostDate").val());
            var datedata=blogdatadate.split("to");
            var fromdate=(datedata[0]).trim();
            var todate=(datedata[1]).trim();

            var postBlockReasonType=($("#postBlockReasonType").val());
            var postType=($("#postType").val());

            var limit=($("#LimitedResults").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/spamPost/ReportedPosts.php",
                data: {
                    fromdate: fromdate, todate: todate,limit:limit,listtype:"first",postType:postType,postBlockReasonType:postBlockReasonType
                },
                success: function (data) {
                    $("#getReportedPostAjaxData").html("");
                    $("#getReportedPostAjaxData").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }


        $("#postBlockReasonType").change(function(){
            getReportedPostByDate();
        });


        $("#postType").change(function(){
            getReportedPostByDate();
        });


        function getUserListPaginationByDate(pageno,fromdate,todate,postType,postBlockReasonType,limit)
        {

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/spamPost/ReportedPosts.php",
                data: {
                    fromdate: fromdate, todate: todate,limit:limit,postType:postType,postBlockReasonType:postBlockReasonType,listtype:'pagination',pageno:pageno
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#getReportedPostAjaxData").html("");
                    $("#getReportedPostAjaxData").html(data);

                },
                error: function () {
                    alert("fail");
                }

            });
        }


    </script>

<?php include "{$docRootAdmin}view/layout/footer.php" ?>