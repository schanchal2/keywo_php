<?php
session_start();
require_once "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

/*********************** get user Permissions ********************/
$myp = mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>
    <link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables_themeroller.css">
    <link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.css">
    <style>
    .dataTables_filter {
        display: none;
    }
    
    .sorting_desc:after,
    .sorting:after,
    .sorting_asc:after {
        content: "" !important;
    }
    
    .dataTables_wrapper .dataTables_paginate .paginate_button {
        padding: 0em !important;
    }
    /*table.dataTable thead .sorting{
        background-image: url();
    }

    table.dataTable thead .sorting_desc {
        background-image: url();
    }

    table.dataTable thead .sorting_asc {
        background-image: url();
    }*/
    
    .dtTableHeadPadding {
        padding: 5px !important;
    }
    </style>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Report Post</h1><span>Report Post Categories Limit</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <div class="content-wrapper">
                <div id="errors" class="m-t-10"></div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                            if((in_array("write", $myp))) {
                            ?>
                            <div class="pull-right" style="z-index: 1;">
                                <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#addNewCatModal" id="addNewUser">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </button>
                            </div>
                            <?php } ?>
                    </div>
                    <div class="col-xs-12">
                        <div id="ajaxResponse">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div class="modal madal--1 fade" id="addNewCatModal" tabindex="-1" role="dialog" aria-labelledby="addNewCatModal" aria-hidden="true" data-backdrop="static" data-keyboard="false" style="">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Add new Spam Category</h4>
                </div>
                <div class="modal-body">
                    <div class="register-box-body">
                        <div id="rederror" style="">
                            <div id="errmsg" style="padding: 0px;color:red;"></div>
                            <br/>
                        </div>
                        <form id="addstaff" name="addstaff" action="javascript:;" data-parsley-validate="" class="form-horizontal form-label-left">
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Category Name<span class="">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="catnameAdd" name="catnameAdd" class="date-picker form-control col-md-7 col-xs-12" maxlength="200">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Report Limit<span class="">*</span>
                                </label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="number" id="catReportLimitAdd" name="catReportLimitAdd" class="date-picker form-control col-md-7 col-xs-12" min='0' maxlength="50">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-4">
                                    <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel
                                    </button>
                                    <button type="submit" id="submitnewcat" class="btn btn-success ajaxhide" onclick="submitCategorydata()">Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.form-box -->
            </div>
        </div>
    </div>
    <div id="feesValues"></div>
    <div id="errDialog"></div>
    </div>
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
    $(document).ready(function() {
        getCategoryLimitdata();
    });


    $("#feesValues").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Edit",
        buttons: {
            "Update": function(e) {

            }

        }

    });

    $("#errDialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        title: "Error !",
        width: 350,
        height: 180,
        buttons: {

        }
    });

    function updateCategoryLimit(title, idelementtoupdate, idtofileddynamic, placeholders) {

        var html = '<div class="form-group has-feedback"> <input type="number"  min="0" class="form-control" id="' + idtofileddynamic + "input" + '" name="' + idtofileddynamic + "input" + '" placeholder="' + placeholders + '"> <span class="glyphicon glyphicon-bitcoin form-control-feedback"></span> </div>';

        $("#feesValues").html(html);
        $("#feesValues").dialog('option', 'title', title);

        var buttons = $('#feesValues').dialog('option', 'buttons');
        buttons.Update = function() {
            updateLimitAjax(idtofileddynamic, idelementtoupdate);
        };

        $('#feesValues').dialog('option', 'buttons', buttons);
        $("#feesValues").dialog('open');
        var buttons = $('.ui-dialog-buttonset').children('button').removeClass().addClass('btn btn-primary');
    }

    function updateLimitAjax(idtofileddynamic, idelementtoupdate) {

        var category_limit = document.getElementById(idtofileddynamic + "input").value;

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/spamPost/spamPostCRUD.php",
            data: {
                category_name: idelementtoupdate,
                category_limit: category_limit,
                action: "update"
            },
            success: function(user) {

                var msg = user["errMsg"];
                if (user["errCode"] == "-1") {
                    if (idelementtoupdate) {
                        $("#" + idtofileddynamic).html("");
                        $("#" + idtofileddynamic).html(category_limit);
                        $("#feesValues").dialog('close');
                    } else {
                        var error = user["errMsg"];
                        $("#errDialog").html("<br/><br/><center style='color:red'><h4>" + error + "</h4></center>");
                        $("#errDialog").dialog('open');
                    }
                }

            },
            error: function() {
                console.log("Failed to Update Data.");

            }
        });

    }



    function submitCategorydata() {
        var category_name = $("#catnameAdd").val();
        var category_limit = $("#catReportLimitAdd").val();
        operateLadda("submitnewcat", "start");
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/spamPost/spamPostCRUD.php",
            data: {
                category_name: category_name,
                category_limit: category_limit,
                action: "add"
            },
            success: function(user) {
                operateLadda("submitnewcat", "stop");
                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {

                    $('#addNewCatModal').modal("hide");
                    getCategoryLimitdata();
                    $("#errors").html(bsAlert("success", "Category Added Successfully!"));
                } else if (user["errCode"] == "5") {
                    $("#errmsg").html("");
                    $("#errmsg").html(msg);
                }
            },
            error: function() {
                console.log("Failed to Update Data.");

            }
        });

    }



    function getCategoryLimitdata() {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/spamPost/spamPostCRUD.php",
            data: {
                category_name: "",
                category_limit: "",
                action: "display"
            },
            success: function(user) {

                $("#ajaxResponse").html("");
                $("#ajaxResponse").html(user);
            },
            error: function() {
                console.log("Failed to get Data.");

            }
        });

    }

    function deleteCategory(category_name, category_limit) {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/spamPost/spamPostCRUD.php",
            data: {
                category_name: category_name,
                category_limit: category_limit,
                action: "remove"
            },
            success: function(user) {

                var msg = user["errMsg"];

                if (user["errCode"] == "-1") {
                    getCategoryLimitdata();
                    $("#errors").html(bsAlert("success", "Category Deleted Successfully!"));


                } else {

                    $("#errors").html(bsAlert("danger", "Error in deleting category ,please try again!"));
                }
            },
            error: function() {
                console.log("Failed to Update Data.");

            }
        });
    }
    </script>
    <?php include "{$docRootAdmin}view/layout/footer.php" ?>
