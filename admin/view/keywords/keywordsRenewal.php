
<?php

include "../layout/header.php";
checkGroupAccess();
require_once("../../config/config.php");
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../config/db_config.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../helpers/deviceHelper.php");
require_once("../../helpers/date_helpers.php");
require_once("../../helpers/arrayHelper.php");
require_once("../../helpers/stringHelper.php");
require_once("../../core/errorMap.php");
require_once "../../model/geolocation/geoLocation.php";

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : $connSearch["errMsg"];

?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<style>
    table td.firstColumn { display: none; }
    table th.firstColumn { display: none; }

    table td.lastColumn { display: none; }
    table th.lastColumn { display: none; }
</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Keywords</h1><span>Keywords Renewal</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>

<div id="userRefferalAnalatics">

    <div class="row">
        <div class="col-lg-10">
            <div id="keywordDueCountData">

            </div>
        </div>

        <div class="col-lg-2">
            <div id="filter" class="sign-upverification">
                <form action="" method="POST" class="form-inline" role="form" name="filter">


                    <div class="form-inline pull-right">
                        <label class="dropdownOptions">
                            <select id="kwdExpiry" class="form-control" style="    width: 100px;">
                                <option value="" disabled selected> Expiry</option>
                                <option value="1_day"="">1 Day</option>
                                <option value="1_week">2 Week</option>
                                <option value="1_month">1 Month</option>
                                <option value="2_month">2 Month</option>
                                <option value="3_month">3 Month</option>
                            </select>
                        </label>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<div id="dueRenewalkwdajaxList">

</div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script>
    $( document ).ready(function() {
        dueRenewalkwdList(1,"3 month","7");
    });

    function dueRenewalkwdList(pageno,expiryTime,limitData)
    {
        var limit,expiry;
        if(expiryTime!="")
        {
            expiry=expiryTime;
        }else{
            expiry=($("#kwdExpiry").val());
        }



        if(limitData!="")
        {
            limit=limitData;
        }else {
            limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=7;
            }
        }

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/tracking/keywordwise/dueRenewalkwdList.php",
            data: {
                limit:limit,expiry:expiry,page: pageno
            },
            success: function (data) {
                keywordDueAnalytics(expiry);
                $("#dueRenewalkwdajaxList").html("");
                $("#dueRenewalkwdajaxList").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }


    $("#LimitedResult").change(function(){

        var page=$("#hiddenpage").val();
        dueRenewalkwdList(page,"","");
    });
    $("#kwdExpiry").change(function(){
        var page=$("#hiddenpage").val();
        dueRenewalkwdList(page,"","");
    });


    function keywordDueAnalytics(expiry)
    {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/tracking/keywordwise/getKeywordDueAnalytics.php",
            data: {
                expiry:expiry
            },
            success: function (data) {
                $("#keywordDueCountData").html("");
                $("#keywordDueCountData").html(data);
            },
            error: function () {
                alert("fail");
            }
        });

    }

</script>




<?php include "{$docRootAdmin}view/layout/footer.php" ?>
