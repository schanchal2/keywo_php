<?php include "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";

require_once "../../model/keywords/common_keyword.php";

$kwdStat=getKeywordStat($connKeywords)["errMsg"];
$totalKwdSoldbyPaypal=$kwdStat["total_kwd_sold_by_paypal"];
$totalKwdSoldbyBtc=$kwdStat["total_kwd_sold_by_btc"];
$totalKwdSoldbyITD=$kwdStat["total_kwd_sold_by_itd"];
$totalKwdSale=$totalKwdSoldbyPaypal+$totalKwdSoldbyBtc+$totalKwdSoldbyITD;

$totalKwdSoldAmount=$kwdStat["total_keyword_sold_amount"];
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Keywords</h1><span>Keywords Sold</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="" style="width:20%">Keyword Sold By Paypal</th>
                                <th class="" style="width:20%">Keyword Sold By Bitcoin</th>
                                <th class="" style="width:20%">Keyword Sold By <?= $adminCurrency; ?></th>
                                <th class="" style="width:20%">Total Sale</th>
                                <th class="" style="width:20%">Total Sale Amount </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> <?= $totalKwdSoldbyPaypal; ?> </td>
                                <td id=""> <?= $totalKwdSoldbyBtc; ?> </td>
                                <td id=""> <?= $totalKwdSoldbyITD; ?> </td>
                                <td id=""> <?= $totalKwdSale; ?> </td>
                                <td id=""> <?= number_format($totalKwdSoldAmount,6); ?> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">

                                <div class="input-group styled-select" >

                                    <input style="width: 226px;" type="text" id="userEmailId" class="form-control" value='' onkeyup="getKeywordsSoldList()" placeholder="Search by keyword owner email">



                                </div>
                                <div class="input-group styled-select" >

                                    <input style="width: 330px;" type="text" id="keywordID" class="form-control" value='' onkeyup="getKeywordsSoldList()" placeholder="Search keyword(s) by seprated space ex.(aa bb)">



                                </div>

                                <!--<div class="form-inline pull-right" style="margin-right: 10px;">
                                    <label class="dropdownOptions">
                                        <select id="paymentType" class="form-control" style="    width: 145px;">
                                            <option value="" disabled selected> Payment Modes</option>
                                            <option value="Bitcoin"="">Bitcoin</option>
                                            <option value="Wallet">Wallet</option>
                                            <option value="Paypal">Paypal</option>
                                            <option value="all">All</option>
                                        </select>
                                    </label>
                                </div>-->

                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="getKeywordsAjaxData">
            </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script>
        $( document ).ready(function() {
            getKeywordsSoldList();
        });

        function getKeywordsSoldList()
        {

            var userEmailId=($("#userEmailId").val());
            var keyword=($("#keywordID").val());
            //var paymentType=($("#paymentType").val());
            var limit=($("#LimitedResult").val());
            if(((typeof limit)=="undefined") || limit=="")
            {
                limit=10;
            }


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/keywords/keywordsSold.php",
                data: {
                    limit:limit,userEmailId:userEmailId,keyword:keyword //paymentType:paymentType
                },
                success: function (data) {
                    $("#getKeywordsAjaxData").html("");
                    $("#getKeywordsAjaxData").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        $('#getKeywordsDate').dateRangePicker({
            autoClose: true,
            startDate: threeMonthsAgo,
            endDate: todaysdate,

            //format: 'DD.MM.YYYY HH:mm',
            time: {
                enabled: false
            }
        }).bind('datepicker-first-date-selected', function(event, obj) {
            var date1 = obj.date1;
        }).bind('datepicker-change', function(event, obj) {

            getKeywordsSoldList();
        });


        $("#LimitedResult").change(function(){
            getKeywordsSoldList();
        });

        /*$("#paymentType").change(function(){
            getKeywordsSoldList();
        });*/


        function kwdSoldListPages(pageNo,searchquery,owner,limit)
        {

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/keywords/keywordsSold.php",
                data: {
                    limit:limit,userEmailId:owner,keyword:searchquery,page:pageNo //paymentType:paymentType
                },
                success: function (data) {
                    $("#getKeywordsAjaxData").html("");
                    $("#getKeywordsAjaxData").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }



    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>