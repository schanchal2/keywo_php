<?php
include "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";
require_once "../../model/manageUser/manageUser.php";
$keywoStat = getKeywoStat($connKeywords);
$keywoStat = $keywoStat["errMsg"][0];
$registered_users=$keywoStat["registered_users"];
$users_with_refferal=$keywoStat["users_with_refferal"];

?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <style>
        .table > thead > tr > th {
            padding: 5px 0;
        }

        .table > thead > tr > td{
            padding: 5px 0;
        }


        #newRegisteredUsers{
            margin-top: 0px;
        }

    </style>
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Manage Users</h1><span>Referral</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th class="" >Total No. of Registered Users</th>
                                <th class="" >Total Signed-Up By Using Referral Code</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> <?= $registered_users; ?> </td>
                                <td id=""> <?= $users_with_refferal; ?> </td>

                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>
                <!--<div class="row">
                    <div class="col-lg-12">
                        <table class="table  text-center table-responsive">
                            <thead>
                            <tr>
                                <th colspan="2" class="" >Social Affiliate Earnings</th>
                                <th colspan="2" class="" >Keyword Affiliate Earnings</th>


                            </tr>
                            <tr>
                                <th class="bg-lightBlue">Txn. No</th>
                                <th class="bg-lightBlue">Total Amt</th>
                                <th class="bg-lightBlue">Txn. No</th>
                                <th class="bg-lightBlue">Total Amt</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td id=""> 0 </td>
                                <td id=""> 0 </td>
                                <td id=""> 0 </td>
                                <td id=""> 0 </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>


                </div>-->
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline" role="form" name="filter">
                                <div class="input-group styled-select" >

                                    <input style="width: 130px;z-index: 1" type="text" id="userEmailId" onKeyup="getRefferalUserList()" class="form-control" value='' placeholder="Email ID">

                                 </span>

                                </div>

                                <div class="input-group styled-select" >

                                    <input style="width: 130px;z-index: 1" type="text" onKeyup="getRefferalUserList()" id="userRefferalId" class="form-control" value='' placeholder="Referral Code"></span>

                                </div>

                                <div class="input-group styled-select">
                                    <?php

                                    $result=getAllCountry($connSearch)["errMsg"];

                                    ?>
                                    <select id="countryToSearch" data-placeholder="Choose a Country..." class="chosen-select">

                                        <option data-live-search=\"true\" data-tokens='All' id='All' value=''>All</option>
                                        <?php
                                        foreach($result as $value){
                                            echo $value["name"];
                                            echo "<option data-live-search=\"true\" data-tokens='".$value['name']."' id='".$value['id']."' value='".$value['name']."'>".$value['name']."</option>";
                                        }
                                        ?>

                                    </select>

                                </div>


                            </form>
                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="refferalUserAjax">
        </div>
    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <script>
        $( document ).ready(function() {
            getRefferalUserList();
        });

        function getRefferalUserList()
        {

            var userRefferalId=($("#userRefferalId").val());
            var userID=($("#userEmailId").val());
            var country=($("#countryToSearch").val());
            var limit=10;

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/referral/referralUsersList.php",
                data: {
                    userID:userID,userRefferalId:userRefferalId,pageno:1,country:country,limit:limit
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#refferalUserAjax").html("");
                    $("#refferalUserAjax").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });

        }



        $("#countryToSearch").chosen();


        $("#countryToSearch").change(function(){
            getRefferalUserList()
        });

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>