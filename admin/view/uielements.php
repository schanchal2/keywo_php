<?php include "layout/header.php"; ?>
<link rel="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/plugins/prism/prism.css">
<main>
    <div class="container-fluid">
        <?php include "{$docRootAdmin}view/layout/titlebar.php" ?>
        <!--==============================
        =            checkbox            =
        ===============================-->
    
        <h3>Checkbox</h3>
        <pre>
<script type="text/plain" style="display: block">
    <div class="checkbox checkbox--1">
        <label>
            <input type="checkbox" value=""> Label
            <i class="fa fa-checkbox--1"> </i>
        </label>
    </div>
</script>
        </pre>
        
        <div class="checkbox checkbox--1">
            <label>
                <input type="checkbox" value=""> Label
                <i class="fa fa-checkbox--1"> </i>
            </label>
        </div>
        <!--====  End of checkbox  ====-->
        <!--==============================================
        =            background color classes            =
        ===============================================-->
        <h2>Background color classes</h2>
        <ol style="line-height:1.5">
            <!--            <li class="bg-black"> bg-black</li>-->
            <!--            <li class="bg-gray"> bg-gray</li>-->
            <!--            <li class="bg-lightGray1"> bg-lightGray1</li>-->
            <!--            <li class="bg-lightGray"> bg-lightGray</li>-->
            <!--            <li class="bg-blue"> bg-blue</li>-->
            <!--            <li class="bg-darkblue"> bg-darkblue</li>-->
            <!--            <li class="bg-lightBlue"> bg-lightBlue</li>-->
            <!--            <li class="bg-lightBlue1"> bg-lightBlue1</li>-->
            <!--            <li class="bg-lightBlue2"> bg-lightBlue2</li>-->
            <!--            <li class="bg-red"> bg-red</li>-->
            <!--            <li class="bg-yellow"> bg-yellow</li>-->
            <!--            <li class="bg-green"> bg-green</li>-->
            <!--            <li class="bg-brand-success"> bg-brand-success</li>-->
            <!--            <li class="bg-white"> bg-white</li>-->
        </ol>
        <!--====  End of background color classes  ====-->
        <!--===================================
        =            styled select            =
        ====================================-->
        <div id="">
            <h2>Styled select tag</h2>
            <div class="form-group">
                <label class="col-sm-3 control-label">Select Currency for Slab</label>
                <div class="col-sm-9">
                    <div class="modal-form">
                        <label class="dropdownOptions">
                            <select class="form-control">
                                <option value="USD" selected>USD</option>
                                <option value="SGD">SGD</option>
                                <option value="BTC">BTC</option>
                                <option value="ITD">ITD</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <!--====  End of styled select  ====-->
        <!--==========================  
        =            tabs            =
        ===========================-->
        <h2>Styled tabs</h2>
        <div id="">
            <ul class="nav nav-tabs tabs-styled" role="tablist">
                <li role="presentation" class="active"><a href="#tab1" aria-controls="" role="tab" data-toggle="tab">Tab 1</a></li>
                <li role="presentation"><a href="#tab2" aria-controls="SlabsList" role="tab" data-toggle="tab">Tab 2</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="tab1">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <b>tab1</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="tab2">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <b>tab2</b> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====  End of tabs  ====-->
        <!--================================================================
        =         Ad-Management page code : CS1A    Sub module : Query Ticket Design
                       http://pasteboard.co/tsZKpLBOi.jpg  pop-up            =
        =================================================================-->
        <div class="row">
            <a class="btn btn-primary" data-toggle="modal" href='#modal-id'>Trigger modal</a>
            <!-- 


         -->
            <div class="modal QueryTicket fade" id="modal-id">
                <div class="modal-dialog QueryTicket-dialog">
                    <form>
                        <div class="modal-content QueryTicket-content">
                            <div class="modal-header QueryTicket-header">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="QueryTicket-TicketID">
                                            Ticket ID :
                                            <span>  QT - 123456</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <div class="QueryTicket-UserEmailID">
                                            User e-mail ID :
                                            <span class="text-blue"> asd@sdf.com </span>
                                            <time>00.00.00</time>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                                        &nbsp;
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-minus"></i> </button>
                                    </div>
                                </div>
                                <!-- <h4 class="modal-title QueryTicket-title">Modal title</h4> -->
                            </div>
                            <!-- 


                         -->
                            <div class="modal-body QueryTicket-body">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="row">
                                            <div class="form-group clearfix">
                                                <label for="QueryTicket-UserQuery" class="col-sm-2 control-label">User Query</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control QueryTicket-UserQuery" name="QueryTicket-UserQuery" disabled="disabled" rows="6"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque sequi unde quo, temporibus voluptate. Unde! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque sequi unde quo, temporibus voluptate. Unde! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque sequi unde quo, temporibus voluptate. Unde! </textarea>
                                                    <div class="textareaInfo"><span>21st Oct, 2015</span> <span class="text-blue">20:10 am </span></div>
                                                    <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> -->
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="divider "></div>
                                            </div>
                                            <div class="form-group clearfix">
                                                <label for="QueryTicket-ReplyQuery" class="col-sm-2 control-label">Reply Query</label>
                                                <div class="col-sm-10">
                                                    <textarea class="form-control QueryTicket-ReplyQuery" name="QueryTicket-ReplyQuery" autofocus onkeyup="countChar(this,1000, '#QueryTicket-ReplyQuery-info' );" rows="10"></textarea>
                                                    <!-- <input type="email" class="form-control" id="inputEmail3" placeholder="Email"> -->
                                                    <div class="textareaInfo" id="QueryTicket-ReplyQuery-info">1000</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-2">
                                                    <button type="button" class="btn btn-primary bg-lightBlue ReplyWithFAQs">Reply with FAQs</button>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="QueryTicket-SendAs"><span class="text-blue"> Send as </span>
                                                        <label class="dropdownOptions pull-right">
                                                            <select class="selectpicker">
                                                                <option>Open</option>
                                                                <option>Pendding</option>
                                                                <option>Close</option>
                                                            </select>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <div>
                                            <!-- Tab panes -->
                                            <div class="tab-content QueryTicket-QA">
                                                <div role="tabpanel" class="tab-pane active" id="FAQ">
                                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingOne">
                                                                <h4 class="panel-title">
                                                                
                                                                 
                                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne " aria-expanded="true" aria-controls="collapseOne">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 

                                              Collapsible Group Item #1
                                            
                                              Collapsible Group Item #1
                                            
                                              Collapsible Group Item #1
                                            
                                              Collapsible Group Item #1
                                            </a>
                                          

                                           </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                                <h4 class="panel-title">
                                                                
                                      


                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i>   Collapsible Group Item #2</a>
                                           

                                           </h4>
                                                            </div>
                                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="headingThree">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading1">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="false" aria-controls="collapse1">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading2">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading3">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading4">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading" role="tab" id="heading5">
                                                                <h4 class="panel-title">
                                                                
                                     
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i> 
                                              Collapsible Group Item #3
                                            </a>
                                           </h4>
                                                            </div>
                                                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                                                <div class="panel-body">
                                                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div role="tabpanel" class="tab-pane" id="CannedResponse"> profile profile </div>
                                                <div role="tabpanel" class="tab-pane" id="CreatServiceRequest"> messages messages </div>
                                                <div role="tabpanel" class="tab-pane" id="RaiseIternalTicket"> settings settings </div>
                                                <div role="tabpanel" class="tab-pane" id="Reset2FA"> Reset2FAReset2FAReset2FA </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs  nav-stacked QueryTicket-QA " role="tablist">
                                            <li role="presentation" class="active"><a href="#FAQ" aria-controls="FAQ" role="tab" data-toggle="tab">FAQ</a></li>
                                            <li role="presentation"><a href="#CannedResponse" aria-controls="CannedResponse" role="tab" data-toggle="tab">Canned Response</a></li>
                                            <li role="presentation">
                                                <a href="#CreatServiceRequest" aria-controls="CreatServiceRequest" role="tab" data-toggle="tab">
                                                     Service Request</a>
                                            </li>
                                            <li role="presentation"><a href="#RaiseIternalTicket" aria-controls="RaiseIternalTicket" role="tab" data-toggle="tab">Raise Iternal Ticket</a></li>
                                            <li role="presentation"><a href="#Reset2FA" aria-controls="Reset2FA" role="tab" data-toggle="tab">Reset 2FA</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer QueryTicket-footer">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <div class="QueryTicket-Tagging">
                                            <div class="row">
                                                <div class="form-group ">
                                                    <label for="" class="col-sm-1 control-label"><span>Tagging:</span></label>
                                                    <div class="col-sm-4">
                                                        <input type="email" class="form-control" id="inputEmail3" placeholder="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="QueryTicket-folloUpdate">
                                            Follo up Date : <span> 22/12/2017 </span>&nbsp; <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button> -->
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    <!--====  End of http://pasteboard.co/tsZKpLBOi.jpg  pop-up  ====-->
    </div>
    <!-- 




     -->
    <h2>   Input collected from other branches need to check</h2>
    <div class="col-lg-12 filter">
        <form class="form-inline m-t-10">
            <div class="form-group p-r-10">
                <label class="text-Gray f-sz15">Sort By : </label>
                <div class="input-group">
                    <div class="Sort-by">
                        <label class="dropdownOptions pull-right">
                            <select class="selectpicker">
                                <option value="">All</option>
                                <option value="sent">Sent</option>
                                <option value="received">Received</option>
                                <option value="paymentrequest">Request</option>
                                <option value="cashout">Cashout</option>
                                <option value="purchase">Purchase</option>
                            </select>
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="text-Gray f-sz15">Date : </label>
                <div class="input-group text-Blue dateRange">
                    <input type="text" class="form-control text-Blue" value="10/9/2017-12/9/2017" placeholder="" id="datepicker" name="daterange">
                    <span class="input-group-addon  bg-white text-Blue"> <i class="fa fa-calendar" id=""></i> </span>
                </div>
            </div>
            <div class="form-group">
                <div class="input-group ">
                    <button type="button" class="btn bg-Lightest-Blue text-White p-x-10" data-toggle="modal" data-target="#DownloadTransactionHistory"> <i class="fa fa-download"></i> </button>
                </div>
            </div>
            <div class="form-group pull-right">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search">
                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">  <i class="fa fa-search text-Gray"></i></button>
                                </span>
                </div>
            </div>
        </form>
    </div>
    <!-- 







                 -->
    <h4>styled-select   </h4>
    <h5>not to use</h5>
    <div class="input-group styled-select pull-right">
        <select id="requestStatus" style="width: 120px" ;>
            <option value="" disabled>Status</option>
            <option value="Approved"> Approved</option>
            <option value="Rejected"> Rejected</option>
            <option value="Pending" selected> Pending</option>
            <option value="Hold"> Hold</option>
            <!-- <option value=""> All</option>-->
        </select>
        <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
    </div>
    <!--  -->
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-horizontal p-x-10">
                        <div class="form-group m-b-0">
                            <div class="col-sm-12">
                                <div class="m-x-10">
                                    <label for="exampleInputEmail1">Current Display Currency : <span class="text-Blue">USD</span></label>
                                    <div class="input-group">
                                        <div class="Sort-by materialize">
                                            <label class="dropdownOptions pull-right">
                                                <select class="selectpicker">
                                                    <option disabled="" selected="">USD</option>
                                                    <option>ITD</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class=" p-10 clearfix ">
                        <button type="button" class="btn pull-right  btn-trading-dark"> &nbsp; Cancel &nbsp; </button>
                        <button type="button" class="btn  pull-right  m-r-10 btn-trading-wid-auto "> &nbsp; Save Changes &nbsp; </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script type="text/javascript">
jQuery(document).ready(function($) {

});
</script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/prism/prism.js"></script>
<script>
$(document).ready(function() {
  
});
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
