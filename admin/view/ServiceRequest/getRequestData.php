<?php
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once "../../core/errorMap.php";
require_once "../../model/geolocation/geoLocation.php";
require_once "../../model/serviceRequest/serviceRequestModel.php";

$pageBy=$_POST["type"]; 

$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$urlPageName = $_POST["url"];

?>
    <div class="modal madal--1 fade" id="raise_sr_model" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="width: 920px">
            <div class="modal-content">
                <div class="modal-header ModalHeaderBackground">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold">Service Request Details [Raised By : <span class="raised_by"></span> ]</h4>
                </div>
                <!--  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Service Request Details [Raised By : <span class="raised_by"></span> ]</h4>
                </div> -->
                <form class="" id="ITDPurchaseCreadit" enctype="multipart/form-data">
                    <div class="modal-body clearfix">
                        <div class="p-x-10 border-bottom">
                            <div class="row">
                                <div class="col-xs-3 m-y-5 l-h17">Date :
                                    <?php echo date('d.m.Y'); ?>
                                </div>
                                <div class="col">
                                    <div class="p-5">
                                        <span class="text-darkblue f-wt8 f-sz21 l-h10 request_no"></span>
                                    </div>
                                </div>
                                <div class="col-xs-6 pull-right">
                                    <div class="row">
                                        <div class="col-xs-6  m-y-5 l-h17 text-right">Service Request Type :</div>
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="sr_type" id="" placeholder="" value="" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-x-10 p-t-20">
                            <div class="row">
                                <div id="callPageAjaxData" class="col-xs-12"></div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    var rootUrl = '<?php echo $rootUrl;?>';
    $('#raise_sr_model').on('show.bs.modal', function(event) {

        var urlpageName = '<?php echo $urlPageName; ?>';
        var button = $(event.relatedTarget) // Button that triggered the modal
        var sr_type = button.data('sr_type') // Extract info from data-* attributes
        var request_no = button.data('request_no')
        var raised_by = button.data('raised_by') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('input[name="sr_type"]').val(sr_type);
        // modal.find('input[name="request_no"]').val(request_no);
        modal.find('.request_no').text(request_no);
        modal.find('.raised_by').text(raised_by);

        switch (sr_type) {
            case "domain_block":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "domain_unblock":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "ip_block":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "ip_unblock":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "itd_purchase_debit":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "itd_purchase_credit":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "kyc_update":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "user_account_block":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;

            case "user_account_unblock":
                $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/get_ticket_data.php?type=<?= $pageBy; ?>&reqType=" + sr_type + "&requestNo=" + request_no + "&urlPageName=" + urlpageName);
                break;
        }
    })
    </script>
    <div class="modal madal--1 fade" id="escalation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="" style="color:white;font-weight: bold">Give Reason For Escalation<span class="raised_by"></span> </h4>
                </div>
                <div class="testing"></div>
                <div class="modal-body">
                    <div class="form-group m-0">
                        <textarea class="form-control noresize" maxlength="1000" rows="3" id="escalateReason" rows="5.5" placeholder="Enter Your Comments Here"></textarea>
                    </div>
                </div>
                <div class="modal-footer border-none">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="resetModel">Close</button>
                    <button type="button" id="escalateSubmit" class="btn btn-default" onclick="escalateServiceRequest()">Submit</button>
                </div>
                <div id="escalateData">
                </div>
            </div>
        </div>
    </div>
    <div class="modal  madal--1  fade" id="reject" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"> <i class="fa fa-close"></i></button>
                    <h4 class="modal-title">Give Reason For Reject</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group m-0">
                        <textarea class="form-control noresize" maxlength="1000" rows="3" id="rejectReason" rows="5.5" placeholder="Enter Your Comments Here"></textarea>
                    </div>
                </div>
                <div class="modal-footer border-none">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" id="rejectionSubmit" class="btn btn-default" onclick="rejectServiceRequest()">Submit</button>
                </div>
                <div id="escalateData">
                </div>
            </div>
        </div>
    </div>
    <div class="modal  madal--1 fade" id="approve" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                    <h4 class="modal-title">Are you Sure, You Want to Approve this Request</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
                    <button type="button" id="approveRequest" class="btn btn-success" onclick="approveServiceRequest()">Yes</button>
                </div>
                <div id="escalateData">
                </div>
            </div>
        </div>
    </div>
