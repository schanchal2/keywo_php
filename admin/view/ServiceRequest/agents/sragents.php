<?php include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

$pageBy="agent";
?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Service Request</h1><span>Agents</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <div id="userRefferalAnalatics" style="margin-top: 0px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div id="reportRequestAnalytics">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="filter" class="sign-upverification">
                    <form action="" method="POST" class="form-inline" role="form" name="filter">
                        <div class="input-group">
                         <!--    <div class="pull-right" style="z-index: 1;">
                                <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#raise_sr_model" id="raise_sr_modal_open">
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </button>
                            </div> -->
                            
                            <a href="javascript:;" data-toggle="modal" data-target="#raise_sr_model" class="btn btn-success m-b-5 m-r-15" id="raise_sr_modal_open" style="background: #0299be;">
                                    Raise SR <span class="fa fa-plus-circle"></span></a>
                        </div>
                        <div class="input-group">
                            <input style="width:120px" ; name="srNumber" type="text" onkeyup="getAgentsData()" class="form-control" id="srNumber" placeholder="SR Number">
                        </div>
                        <div class="input-group">
                            <input style="width:135px" ; name="srType" type="text" onkeyup="getAgentsData()" class="form-control" value='' id="srType" placeholder="SR Type">
                        </div>
                        <div class="input-group">
                            <input style="width:135px" ; name="username" type="text" onkeyup="getAgentsData()" class="form-control" id="username" placeholder="Username">
                        </div>
                        <div class="input-group styled-select pull-right">
                            <select id="requestStatus" style="width: 120px" ;>
                                <option value="" disabled>Status</option>
                                <option value="approved"> Approved</option>
                                <option value="rejected"> Rejected</option>
                                <option value="pending" selected> Pending</option>
                                <!-- <option value=""> All</option>-->
                            </select>
                            <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                        </div>
                    </form>
                </div>
            </div>
        </div>

         <div id="errors" style="margin-top:10px"></div>
        <div id="AgentsAjaxdata">

        </div>
</main>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap-validator/dist/validator.js"></script>
<div id="callPageAgentAjaxDataModal">
</div>
<script>
function pageName(type) {
    $.ajax({
        type: 'POST',
        url: '../my_raise_sr_modal.php',
        dataType: 'html',
        data: {
            type: type
        },
        async: true,
        success: function(data) {
            $("#callPageAgentAjaxDataModal").html("");
            $("#callPageAgentAjaxDataModal").html(data);

        }
    });
}

$(document).ready(function() {
    pageName("<?= $pageBy; ?>");
    getAgentsData();
    getagentAnalytics();
});

function getAgentsData() {

    var userName = ($("#username").val());
    var raisedBy = ($("#raisedBY").val());
    var holdBy = ($("#HoldBY").val());
    var srNo = ($("#srNumber").val());
    var srtype = ($("#srType").val());
    var requestStatus = ($("#requestStatus").val());

    var limit = ($("#LimitedResultData").val());
    if (((typeof limit) == "undefined") || limit == "") {
        limit = 5;
    }

    var creationTime = "";


    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controller/serviceRequest/agents/pending.php",
        data: {
            limit: limit,
            userName: userName,
            raisedBy: raisedBy,
            holdBy: holdBy,
            srNo: srNo,
            srType: srtype,
            requestStatus: requestStatus,
            type: "agent"
        },
        success: function(data) {
            $("#AgentsAjaxdata").html("");
            $("#AgentsAjaxdata").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

}


$("#requestStatus").change(function() {
    getAgentsData();
});

function getNewAgentData(pageNo, timestamp, srType, userName, srNo, raisedBy, priority, holdBy, requestStatus, agentIDS, limit) {

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controller/serviceRequest/agents/pending.php",
        data: {
            limit: limit,
            timestamp: timestamp,
            srType: srType,
            userName: userName,
            raisedBy: raisedBy,
            priority: priority,
            srNo: srNo,
            holdBy: holdBy,
            page: pageNo,
            requestStatus: requestStatus,
            //agentIDS     : "agent",
            type: "agent"
        },
        success: function(data) {
            $("#AgentsAjaxdata").html("");
            $("#AgentsAjaxdata").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });

}

function getagentAnalytics() {
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../../controller/serviceRequest/agents/reportRequestAnalytics.php",
        data: {
            type: "agent"
        },
        success: function(data) {
            $("#reportRequestAnalytics").html("");
            $("#reportRequestAnalytics").html(data);
        },
        error: function() {
            console.log("fail");
        }
    });
}
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
