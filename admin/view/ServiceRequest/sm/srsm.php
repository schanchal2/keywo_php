<?php include "../../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");

$pageBy="sm";
$urlPageName = basename($_SERVER['PHP_SELF']);
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/chosen/custom_chosen.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Service Request</h1><span>SM</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>
            <div class="row">
                <div class="col-md-12">
                    <div id="userRefferalAnalatics" style="margin-top: 0px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="reportRequestSMAnalytics">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="row">
                <div class="col-lg-12">
                    <div id="filter" class="sign-upverification">
                        <form action="" method="POST" class="form-inline" role="form" name="filter">

                            <div class="input-group">
                                <input style="width:110px";type="text" onkeyup="getMMData()" class="form-control" id="srNumber" placeholder="SR Number">

                            </div>

                            <div class="input-group">
                                <input style="width:110px"; type="text" onkeyup="getMMData()" class="form-control" id="srType" placeholder="SR Type">

                            </div>

                            <div class="input-group">
                                <input style="width:110px"; type="text" onkeyup="getMMData()" class="form-control" id="username" placeholder="Username">

                            </div>

                            <div class="input-group">
                                <input style="width:90px"; type="text" onkeyup="getMMData()" class="form-control" id="raisedBY" placeholder="Raised By">

                            </div>

                            <div class="input-group styled-select pull-right">
                                <select id="requestStatus" style="width: 110px";>
                                    <option value="" disabled>Status</option>
                                    <option value="approved"> Approved</option>
                                    <option value="rejected"> Rejected</option>
                                    <option value="pending" selected> Pending</option>
                                    <!-- <option value=""> All</option>-->
                                </select>
                                <span class="input-group-addon"><i class="fa fa-angle-down"></i></span>
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div id="errors" style="margin-top:10px"></div>
            <div id="mmAjaxdata"></div>
            <div id="modelAjaxdata"></div>

    </main>

    </div>

    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/js/bootstrap-select-min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.jquery.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/chosen/chosen.proto.min.js"></script>
    <div id="callPageAjaxDataModal">

    </div>
    <script>

        function pageName(type)
        {
            var urlPageName = '<?php echo $urlPageName; ?>';
            $.ajax({
                type: 'POST',
                url: '../getRequestData.php',
                dataType: 'html',
                data: {
                    type   : type,
                    url    : urlPageName
                },
                async: true,
                success: function (data) {
                    $("#callPageAjaxDataModal").html("");
                    $("#callPageAjaxDataModal").html(data);
                }
            });

        }

        $( document ).ready(function() {
            pageName("<?= $pageBy; ?>");
            getMMData();
            getMMAnalytics();
        });

        function getMMData()
        {

            var userName      = ($("#username").val());
            var raisedBy      = ($("#raisedBY").val());
            var srNo          = ($("#srNumber").val());
            var srtype        = ($("#srType").val());
            var requestStatus = ($("#requestStatus").val());

            var limit = ($("#LimitedResultData").val());
            if (((typeof limit) == "undefined") || limit == "") {
                limit = 5;
            }

            var creationTime = "";


            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/serviceRequest/mm/pending.php",
                data: {
                    limit         : limit,
                    userName      : userName,
                    raisedBy      : raisedBy,
                    srNo          : srNo,
                    srType        : srtype,
                    requestStatus : requestStatus,
                    agentIDS      : "escalate_to_sm",
                    type          :"escalate_to_sm",
                    types         :"sm"
                },
                success: function (data) {
                    $("#mmAjaxdata").html("");
                    $("#mmAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
        }

        $("#requestStatus").change(function(){
            getMMData();
        });

        function getNewMMData(pageNo, timestamp,srType,userName, srNo, raisedBy, requestStatus,agentIDS,limit) {

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/serviceRequest/mm/pending.php",
                data: {
                    limit         : limit,
                    timestamp     : timestamp,
                    srType        : srType,
                    userName      : userName,
                    raisedBy      : raisedBy,
                    srNo          : srNo,
                    page          : pageNo,
                    requestStatus : requestStatus,
                    agentIDS      : "",
                    agentIDS      : "escalate_to_sm",
                    type          : "escalate_to_sm",
                    types         :"sm"
                },
                success: function (data) {
                    $("#mmAjaxdata").html("");
                    $("#mmAjaxdata").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
        }

        function getMMAnalytics()
        {
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controller/serviceRequest/mm/reportMMAnalytics.php",
                data: {
                    type    :"sm",
                    hold_by :"escalate_to_sm"
                },
                success: function (data) {
                    $("#reportRequestSMAnalytics").html("");
                    $("#reportRequestSMAnalytics").html(data);
                },
                error: function () {
                    console.log("fail");
                }
            });
        }

        function approveServiceRequest() {
            var tags = document.getElementsByName('user_checkbox[]');
            var tickets = new Array();
            for (var i = 0; i < tags.length; ++i) {
                if (tags[i].checked) {
                    tickets.push(tags[i].value);
                }
            }
            operateLadda("approveRequest","start");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../../controller/serviceRequest/forms/addServiceQueryController.php",
                data: {
                    tickets     : tickets,
                    escalate    : "escalate_to_sm",
                    reqStatusBy : "sm",
                    escalateby  : "mm",
                    status      : "approved"
                },
                success: function (data) {
                    operateLadda("approveRequest","stop");

                    var errorCodes=data["errCode"].toString();
                    switch(errorCodes)
                    {
                        case "71":
                        $('#approve').modal('hide');
                        $("#errors").html(bsAlert("danger","This ticket is already Approved! Please try another one")); 
                        getMMData();
                        getMMAnalytics();
                        break;

                        case "-1":
                        $('#approve').modal('hide'); 
                        $("#errors").html(bsAlert("success","Request Approved!"));
                        getMMData();
                        getMMAnalytics();
                        break;

                        default:
                        $("#errors").html(bsAlert("danger","Error in Approving Requesting please try again."));
                        break;
                    }
                },
                error: function () {
                    console.log("fail");
                }
            });

            console.log(tickets);
        }

        function rejectServiceRequest() {
            var rejectResaon = document.getElementById("rejectReason").value; //alert(escalateResaon);
            var tags = document.getElementsByName('user_checkbox[]');

            var tickets = new Array();
            for (var i = 0; i < tags.length; ++i) {
                if (tags[i].checked) {
                    tickets.push(tags[i].value);
                }
            }

            if(rejectResaon == '')
            {
                $('#reject').modal('hide');
                $("#errors").html(bsAlert("danger","Please Enter Comment Before Submit"));
                getMMData();
                getMMAnalytics();
            }
            operateLadda("rejectionSubmit","start");
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "../../../controller/serviceRequest/forms/addServiceQueryController.php",
                data: {
                    comments    : rejectResaon,
                    escalate    : "escalate_to_sm",
                    status      : "rejected",
                    reqStatusBy : "sm",
                    tickets     : tickets
                },
                success: function (data) {
                    operateLadda("rejectionSubmit","stop");

                    var errorCodes=data["errCode"].toString();
                    switch(errorCodes)
                    {
                        case "71":
                        $('#reject').modal('hide');
                        $("#errors").html(bsAlert("danger","This ticket is already rejected! Please try another one")); 
                        getMMData();
                        getMMAnalytics();
                        break;

                        case "-1":
                        $('#reject').modal('hide'); 
                        $("#errors").html(bsAlert("success","Your Request Ticket Rejected successfully!"));
                        getMMData();
                        getMMAnalytics();
                        break;

                        default:
                        $("#errors").html(bsAlert("danger","Error in Rejecting your Requesting please try again."));
                        break;
                    }
                },
                error: function () {
                    console.log("fail");
                }
            });

            console.log(tickets);

        }

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>