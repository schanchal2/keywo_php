<?php
include "../../layout/header.php"; ?>
<main>
<div class="container-fluid">
    <div class="title clearfix">
        <h1 class="pull-left">Manage Users</h1>
        <span>Keyword Trade Reverse</span>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="form-horizontal" id="ITDPurchaseCreadit">
                <fieldset class="bg-lightGray1">
                    <div class="form-group">
                        <label for="inputServiceRequestType" class="col-sm-offset-9 col-sm-1 control-label">Dated : </label>
                        <div class="col-sm-2">
                            <!-- <div class="row"> -->
                            <div class="input--styled--date">
                                <input type="text" class="form-control" id="inputServiceRequestType1" placeholder="03.05.17 [Auto]">
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <label for="inputServiceRequestType" class="col-sm-3 control-label">Service Request Type:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" id="inputServiceRequestType" placeholder="Keyword Trade Reverse">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputServiceRequestNum" class="col-sm-3 control-label">Service Request No.:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputServiceRequestNum" placeholder="[auto]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Query Ticket ID:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputQueryTicketID" placeholder="[auto / manual if not picked through auto mode ]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-3 control-label">Username:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputUsername" placeholder="[auto]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputKeyword" class="col-sm-3 control-label">Keyword:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputKeyword" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTransactionId" class="col-sm-3 control-label">Transaction ID:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputTransactionId" placeholder="[manual](effect against txn id)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="input--styled">
                        <div class="form-group">
                            <label for="inputTransactionId" class="col-sm-3 control-label">From (username):</label>
                            <div class="col-sm-3 p-0" style="border-bottom:1px solid #bebebe">
                                <input type="text" class="form-control" id="inputTransactionId" placeholder="[manual](effect against txn id)">
                            </div>
                            <label for="inputTransactionId" class="col-xs-2 control-label">To(username): </label>
                            <div class="col-sm-4 p-0" style="border-bottom:1px solid #bebebe">
                                <input type="text" class="form-control" id="inputTransactionId" placeholder="[manual]">
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputReversalReason" class="col-sm-3 control-label">Reversal Reason:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputReversalReason" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">System Checked:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <a href="" class="userCheckedList">Checklist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label">Agent Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputAgentComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUAMComments" class="col-sm-3 control-label">UAM Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="inputUAMComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
             <div class="row">
                <div class="col-xs-12">
                    <div class="table--lightBlue2">
                        <table class="table text-center table-responsive">
                            <thead>
                                <tr>
                                    <th>Raised By</th>
                                    <th>UAM Approved </th>
                                    <th>MM Approved</th>
                                    <th> SM Approved </th>
                                    <th> Priority</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <span class="text-darkblue font-sm">Tom (CS)</span>
                                        <br>
                                        <span class="font-sm">(TAT 3 Working Days)</span>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-default">
                                        <i class="fa fa-pause fa-lg text-lightGray"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-check fa-lg text-darkblue"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-times fa-lg text-lightGray"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-default">
                                        <i class="fa fa-pause fa-lg text-lightGray"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-check fa-lg text-darkblue"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-times fa-lg text-lightGray"></i>
                                        </button>
                                    </td>
                                    <td> </td>
                                    <td class="text-lightGray text-center font-lg"> high/normal/low </td>
                                </tr>
                                <tr >
                                    <td  colspan="2" class="text-center bg-lightBlue border-none">
                                        <span class="text-white f-sz16 ">Closed By : </span>
                                        <span class="text-darkBlue f-sz16">Tom </span>
                                    </td>
                                    <td class="text-center bg-lightBlue border-none">
                                        <span class="text-white f-sz16">Closed On : </span>
                                        <span class="text-lightGray f-sz16">[manual]</span>
                                    </td>
                                    <td colspan="2" class="bg-lightBlue border-none text-left">
                                        <button class="btn btn-default btn-xs text-darkblue ViewComment"> View Comment</button>
                                        <button class="btn btn-xs doc-btn AttachDoc text-white m-l-10"> Attach Doc.</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="border-bottom">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>