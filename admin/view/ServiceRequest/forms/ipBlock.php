<?php

session_start();
require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../core/errorMap.php');
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$pageTypes = $_GET["type"];

$getMxID = selectmaxservicerequest($conn);
if(noError($getMxID))
{
    $getMxID = $getMxID["errMsg"];
    $getLastId = $getMxID["lastid"];
}

$getQueryListDetail =  getQueryListDetail($conn,$pageTypes); // printArr($getQueryListDetail);
if(noError($getQueryListDetail))
{
    $getQueryListDetail = $getQueryListDetail["errMsg"];
}



?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="form-horizontal" id="ITDPurchaseCreadit">
                <fieldset class="">
                    <!-- <div class="form-group"> -->
<!--                         <label for="inputServiceRequestNum" class="col-sm-3 control-label">Service Request No.:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                         <?php
                                            // if(!empty($getLastId))
                                            // {
                                                // $getLastId = $getLastId+1;
                                            // }
                                            // else
                                            // {
                                                // $getLastId = 1;
                                            // }
                                            ?>
                                            <input type="text" class="form-control" value="<?php //echo " SR- ".$getLastId; ?>" placeholder="[auto]" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
 -->                    <!-- </div> -->
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Query Ticket ID:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <select class="col-sm-12" id="ipblockTicketID">
                                        <option value="Select TicketID">Select TicketID</option>
                                        <?php
                                            foreach($getQueryListDetail as $getQueryListDetail1)
                                            {
                                                $ticket_id  = $getQueryListDetail1["ticket_id"];

                                                ?>
                                            <option value="<?php echo $ticket_id; ?>">
                                                <?php echo $ticket_id; ?>
                                            </option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-3 control-label">Username:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">

                                            <input type="hidden" class="form-control" id="iprequestFrom" value=""/>
                                            <input type="text" class="form-control" id="ipblockUsername" value="" disabled/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputDomain" class="col-sm-3 control-label">IP Address:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="ipaddress" placeholder="e.g. Requested By Customer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputReasontoBlock" class="col-sm-3 control-label">Reason To Block:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="ipblockResaon" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-3 control-label">From (Username):</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="fromUsername" placeholder="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputReversalReason" class="col-sm-3 control-label">Reversal Reason:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="ipblockReversalReason" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">System Checked:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <a href="" class="userCheckedList">Checklist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <!-- <div class="col-sm-12"> -->
                                    <span class="col-sm-1 control-label text-lightGray text-left">Action</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control" id="ipActionComment" placeholder="Debit by admin">
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <span class="col-sm-1 control-label text-lightGray text-left">Amount</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control allownumericwithoutdecimal" id="ipActionAmount" placeholder="[Debit amt {manual}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label">Agent Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="ipblockAgentComment" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUAMComments" class="col-sm-3 control-label">UAM Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="ipblockUAMComment" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Priority:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <select class="col-sm-12" id="reqPriority">
                                        <option value="high">High</option>
                                        <option value="medium">Medium</option>
                                        <option value="low">Low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onclick="return ipBlockModule()">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <script>
    var rootUrl = '<?php echo $rootUrl;?>';

    function ipBlockModule() {
        var type           = document.getElementById("iprequestFrom").value;
        var requestType    = document.getElementById("ipblockRequestType").value;
        var ticketID       = document.getElementById("ipblockTicketID").value;
        var userName       = document.getElementById("ipblockUsername").value;
        var ipaddress      = document.getElementById("ipaddress").value;
        var blockResaon    = document.getElementById("ipblockResaon").value;
        var fromUsername   = document.getElementById("fromUsername").value;
        var ReversalReason = document.getElementById("ipblockReversalReason").value;
        var actionComment  = document.getElementById("ipActionComment").value;
        var actionAmt      = document.getElementById("ipActionAmount").value;
        var agentComment   = document.getElementById("ipblockAgentComment").value;
        var uamComment     = document.getElementById("ipblockUAMComment").value;
        var priority       = $('#reqPriority').find(":selected").text();

        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/serviceRequest/forms/addServiceQueryController.php',
            dataType: 'html',
            data: {
                type: type,
                requestType: requestType,
                ticketID: ticketID,
                userName: userName,
                ipaddress: ipaddress,
                blockResaon: blockResaon,
                fromUsername: fromUsername,
                ReversalReason: ReversalReason,
                actionComment: actionComment,
                actionAmt: actionAmt,
                agentComment: agentComment,
                uamComment: uamComment,
                priority: priority

            },
            async: true,
            success: function(data) {

                alert("Your IP Block ServiceRequest Ticket is successfully created");
                $('.modal-content').hide();
                $('.modal-backdrop').hide();
                $('#ITDPurchaseCreadit').hide();
                window.location.reload();

            }
        });
    }

    $("#ipblockTicketID").change(function() {

        var id = $('#ipblockTicketID').find(":selected").text();
        var type = '<?php echo $pageTypes; ?>';

        $.ajax({
            type: 'POST',
            url: '../forms/ipBlock.php',
            dataType: 'html',
            data: {
                id: id,
                type: type
            },
            async: true,
            success: function(data) {
                $('#ITDPurchaseCreadit').html(data);
            }
        });
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });
    </script>
