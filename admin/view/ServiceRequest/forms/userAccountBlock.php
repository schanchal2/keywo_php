<?php

session_start();
require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../core/errorMap.php');
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}
$pageTypes = $_GET["type"];
$getMxID = selectmaxservicerequest($conn);
if(noError($getMxID))
{
    $getMxID = $getMxID["errMsg"];
    $getLastId = $getMxID["lastid"];
}

$getQueryListDetail =  getQueryListDetail($conn,$pageTypes); // printArr($getQueryListDetail);
if(noError($getQueryListDetail))
{
    $getQueryListDetail = $getQueryListDetail["errMsg"];
}

?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="form-horizontal" id="ITDPurchaseCreadit">
                <fieldset class="">
                    <!-- <div class="form-group"> -->
<!--                         <label for="inputServiceRequestNum" class="col-sm-3 control-label">Service Request No.:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <?php
                                            // if(!empty($getLastId))
                                            // {
                                                // $getLastId = $getLastId+1;
                                            // }
                                            // else
                                            // {
                                                // $getLastId = 1;
                                            // }
                                            ?>
                                            <input type="text" class="form-control" value="<?php //echo " SR- ".$getLastId; ?>" placeholder="[Auto]" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    <!-- </div> -->
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Query Ticket ID:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <select class="col-sm-12" id="accBlockTicketID">
                                        <option value="Select TicketID">Select TicketID</option>
                                        <?php
                                            foreach($getQueryListDetail as $getQueryListDetail1)
                                            {
                                                $ticket_id  = $getQueryListDetail1["ticket_id"];

                                                ?>
                                            <option value="<?php echo $ticket_id; ?>">
                                                <?php echo $ticket_id; ?>
                                            </option>
                                            <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-3 control-label">Username:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">

                                            <input type="hidden" class="form-control" id="iprequestFrom" value=""/>
                                            <input type="text" class="form-control" id="accBlockUsername" value="" disabled/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputServiceRequestNum" class="col-sm-3 control-label">Reason To Block</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="accBlockReason" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">System Checked:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <a href="" class="userCheckedList">Checklist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <!-- <div class="col-sm-12"> -->
                                    <span class="col-sm-1 control-label text-lightGray text-left">Action</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control" id="accBlockActionComment" placeholder="Debit by admin">
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <span class="col-sm-1 control-label text-lightGray text-left">Amount</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control allownumericwithoutdecimal" id="accBlockActionAmount" placeholder="[Debit amt {manual}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label">Agent Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="accBlockAgentComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUAMComments" class="col-sm-3 control-label">UAM Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="accBlockUAMComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Priority:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <select class="col-sm-12" id="reqPriority">
                                        <option value="high">High</option>
                                        <option value="medium">Medium</option>
                                        <option value="low">Low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onclick="return userAccBlock()">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <script>
    var rootUrl = '<?php echo $rootUrl;?>';

    function userAccBlock() {
        var type          = document.getElementById("iprequestFrom").value;
        var requestType   = document.getElementById("accBlockRequestType").value;
        var ticketID      = document.getElementById("accBlockTicketID").value;
        var userName      = document.getElementById("accBlockUsername").value;
        var blockResaon   = document.getElementById("accBlockReason").value;
        var actionComment = document.getElementById("accBlockActionComment").value;
        var actionAmt     = document.getElementById("accBlockActionAmount").value;
        var agentComment  = document.getElementById("accBlockAgentComments").value;
        var uamComment    = document.getElementById("accBlockUAMComments").value;
        var priority      = $('#reqPriority').find(":selected").text();

        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/serviceRequest/forms/addServiceQueryController.php',
            dataType: 'html',
            data: {
                type: type,
                requestType: requestType,
                ticketID: ticketID,
                userName: userName,
                blockResaon: blockResaon,
                actionComment: actionComment,
                actionAmt: actionAmt,
                agentComment: agentComment,
                uamComment: uamComment,
                priority: priority

            },
            async: true,
            success: function(data) {

                alert("Your UserAccount Block ServiceRequest Ticket is successfully created");
                $('.modal-content').hide();
                $('.modal-backdrop').hide();
                $('#ITDPurchaseCreadit').hide();
                window.location.reload();

            }
        });
    }

    $("#accBlockTicketID").change(function() {

        var id = $('#accBlockTicketID').find(":selected").text();
        var type = '<?php echo $pageTypes; ?>';

        $.ajax({
            type: 'POST',
            url: '../forms/userAccountBlock.php',
            dataType: 'html',
            data: {
                id: id,
                type: type
            },
            async: true,
            success: function(data) {
                $('#ITDPurchaseCreadit').html(data);
            }
        });
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });
    </script>
