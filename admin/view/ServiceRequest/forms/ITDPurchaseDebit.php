<?php

session_start();
require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../core/errorMap.php');
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$pageTypes = $_GET["type"];

$getMxID = selectmaxservicerequest($conn);
if(noError($getMxID))
{
    $getMxID = $getMxID["errMsg"];
    $getLastId = $getMxID["lastid"];

}

$getQueryListDetail =  getQueryListDetail($conn,$pageTypes); // printArr($getQueryListDetail);
if(noError($getQueryListDetail))
{
    $getQueryListDetail = $getQueryListDetail["errMsg"];
}


?>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="form-horizontal" id="ITDPurchaseCreadit" name="myForm">
                <fieldset class="">
                    <!-- <div class="form-group"> -->
<!--                         <label for="inputServiceRequestNum" class="col-sm-3 control-label">Service Request No.:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                            <?php
                                            // if(!empty($getLastId))
                                            // {
                                                // $getLastId = $getLastId+1;
                                            // }
                                            // else
                                            // {
                                                // $getLastId = 1;
                                            // }
                                            ?>
                                            <input type="text" class="form-control" id="debitQueryTicketID" value="<?php //echo " SR- ".$getLastId; ?>" placeholder="[Auto]" disabled/>
                                    </div>
                                </div>
                            </div>
                        </div>
 -->                    <!-- </div> -->
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Query Ticket ID:</label>
                        <div class="col-sm-3">
                            <!-- <div class="row"> -->
                                <div class="input--styled">
                                <!-- <div class="col-xs-12"> -->
                                    <select class="form-control border-none no-shadow" id="ticketID">
                                        <option value="Select TicketID" disabled>Select TicketID</option>
                                        <?php
                                            foreach($getQueryListDetail as $getQueryListDetail1)
                                            {
                                            $ticket_id  = $getQueryListDetail1["ticket_id"];

                                                ?>
                                            <option value="<?php echo $ticket_id; ?>">
                                                <?php echo $ticket_id; ?>
                                            </option>
                                            <?php } ?>
                                    </select>
                                <!-- </div> -->
                                </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">Txn ID's:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="debitTxnID" placeholder="[manual] (effect against ITD Purchase ID)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">System Checked:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <a href="" class="userCheckedList">Checklist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUsername" class="col-sm-3 control-label">Username:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">

                                            <input type="hidden" class="form-control" id="requestTypeFrom" value=""/>
                                            <input type="text" class="form-control" id="debitUsername" value="" disabled/>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <!-- <div class="col-sm-12"> -->
                                    <span class="col-sm-1 control-label text-lightGray text-left">Action</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control" id="debitActionComment" placeholder="Debit by admin">
                                    </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <span class="col-sm-1 control-label text-lightGray text-left">Amount</span>
                                    <div class="col-sm-9 col-sm-offset-2">
                                        <input type="text" class="form-control allownumericwithoutdecimal" id="debitActionAmount" placeholder="[Debit amt {manual}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label">Agent Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="debitAgentComment" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUAMComments" class="col-sm-3 control-label">UAM Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" id="debitUAMComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Priority:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="input--styled">
                                    <select class="col-sm-12" id="reqPriority">
                                        <option value="high">High</option>
                                        <option value="medium">Medium</option>
                                        <option value="low">Low</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-default" onclick="itdPurchaseDebit()">Submit</button>
                </div>
            </form>
        </div>
    </div>
    <script>
    var rootUrl = '<?php echo $rootUrl;?>';

    function itdPurchaseDebit() {
        var type          = document.getElementById("requestTypeFrom").value;
        var requestType   = document.getElementById("debitServiceRequestType").value;
        var ticketID      = document.getElementById("ticketID").value;
        var userName      = document.getElementById("debitUsername").value;
        var txnID         = document.getElementById("debitTxnID").value;
        var actionComment = document.getElementById("debitActionComment").value;
        var actionAmt     = document.getElementById("debitActionAmount").value;
        var agentComment  = document.getElementById("debitAgentComment").value;
        var uamComment    = document.getElementById("debitUAMComments").value;
        var priority      = $('#reqPriority').find(":selected").text();

        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/serviceRequest/forms/addServiceQueryController.php',
            dataType: 'html',
            data: {
                type: type,
                requestType: requestType,
                ticketID: ticketID,
                userName: userName,
                txnID: txnID,
                actionComment: actionComment,
                actionAmt: actionAmt,
                agentComment: agentComment,
                uamComment: uamComment,
                priority: priority

            },
            async: true,
            success: function(data) {

                alert("Your ITD Purchase(Debit) ServiceRequest Ticket is successfully created");
                $('.modal-content').hide();
                $('.modal-backdrop').hide();
                $('#ITDPurchaseCreadit').hide();
                window.location.reload();

            }
        });
    }

    $("#ticketID").change(function() {

        var id = $('#ticketID').find(":selected").text();
        var type = '<?php echo $pageTypes; ?>';

        $.ajax({
            type: 'POST',
            url: '../forms/ITDPurchaseDebit.php',
            dataType: 'html',
            data: {
                id: id,
                type: type
            },
            async: true,
            success: function(data) {
                $('#ITDPurchaseCreadit').html(data);
            }
        });
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });
    </script>
