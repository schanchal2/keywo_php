<?php

session_start();
require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../core/errorMap.php');
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}
$pageTypes = $_GET["type"];
$getMxID = selectmaxservicerequest($conn);
if(noError($getMxID))
{
    $getMxID = $getMxID["errMsg"];
    $getLastId = $getMxID["lastid"];
}

$getQueryListDetail =  getQueryListDetail($conn,$pageTypes); // printArr($getQueryListDetail);
if(noError($getQueryListDetail))
{
    $getQueryListDetail = $getQueryListDetail["errMsg"];
}

?>
    <div class="row">
        <div class="col-xs-4">
            <div class="form-group">
                <label for="inputQueryTicketID" class="control-label">Query Ticket ID:</label>
                <div class="">
                    <label class="dropdownOptions">
                        <select class="form-control" id="kycTicketID">
                            <option value="Select TicketID">Select TicketID</option>
                            <?php
                                    foreach($getQueryListDetail as $getQueryListDetail1) {
                                        $ticket_id  = $getQueryListDetail1["ticket_id"]; 
                                    ?>
                            <option value="<?php echo $ticket_id; ?>">
                                <?php echo $ticket_id; ?>
                            </option>
                            <?php } ?>
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <label for="inputQueryTicketID" class="control-label">Priority:</label>
                <div class="">
                    <label class="dropdownOptions">
                        <select class="form-control" id="reqPriority">
                            <option value="high">High</option>
                            <option value="medium">Medium</option>
                            <option value="low">Low</option>
                        </select>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
                <label for="inputUsername" class="control-label">Username:</label>
                <input type="text" class="form-control" id="kycUsername" value="adfsffsf@sf.df<?php echo $reg_email; ?>" disabled/>
            </div>
        </div>
    </div>
    <div class="row border-bottom  border-top m-t-20 p-t-10">
        <div class="col-xs-6  p-t-10"><span class="h4">Documents</span></div>
        <div class="col-xs-6">
            <div class="form-group m-b-10">
                <div class="row">
                    <label for="checklist" class="col-sm-4 control-label text-right l-h17 m-y-5">Uploaded on :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="kycDocDate" placeholder="[Date]">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row p-t-20 KYC-userDocuments">
                <?php 
                /*=================================================
                =            in case of disabled state            =
                =================================================*/
                ?>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-default"> PAN Card
                            <input type="checkbox" value="" disabled> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-success"> Passport
                            <input type="checkbox" value="" disabled checked> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <?php 
                /*=====  End of in case of disabled state  ======*/
                ?>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-default"> Aadhaar Card
                            <input type="checkbox" value=""> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-default"> Voters Identity Card
                            <input type="checkbox" value=""> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-default"> Driving License
                            <input type="checkbox" value=""> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2">
                        <div class="alert alert-default"> Bank Passbook
                            <input type="checkbox" value=""> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <section class="row">
        <div class="col-xs-12">
            <div class="row border-bottom border-top m-t-20 p-y-15">
                <div class="col-xs-12"><span class="h4">Checklist</span></div>
            </div>
            <div class="form-group p-t-20">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              1.Users account status (Active/DeActive)
                            </a>
                            <!--=========================================
                            =            if already checked add this            =
                            ==========================================-->
                            <i class="fa fa-check-square text-green pull-right"></i>    
                            <!--====  End of if already checked add this  ====-->
                        </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <!--==================================================
                                =            if already checked add this             =
                                ===================================================-->
                                <div class="sr-checklist__commentbox--others m-b-10">
                                    <div class="sr-checklist__commentbox__heading"><b>Name of checker</b></div>
                                    <!--" commentbox__text "is optional,  -->
                                    <div class="sr-checklist__commentbox__text">comment by checkerAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                                <!--====  End of if already checked add this   ====-->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              2.Account Balance
                            </a>
                            <i class="fa fa-check-square text-green pull-right "></i>    
                        </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <!--==================================================
                                =            if already checked add this             =
                                ===================================================-->
                                <div class="sr-checklist__commentbox--others m-b-10">
                                    <div class="sr-checklist__commentbox__heading"><b>Name of checker</b></div>
                                    <!--" commentbox__text "is optional,  -->
                                    <!-- <div class="sr-checklist__commentbox__text">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div> -->
                                </div>
                                <!--====  End of if already checked add this   ====-->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              3.Deposit transaction history
                            </a>
                        </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="checkbox checkbox--1">
                                    <label class="to-enable--sr-checklist__commentbox">
                                        <input type="checkbox" value=""> Have you checked this?
                                        <i class="fa fa-checkbox--1 text-green"> </i>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="kycagentComment" class="control-label"><b>Level 3 Comments</b></label>
                                    <textarea name="kycagentComment" id="kycagentComment" class="form-control noresize" rows="3" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                              4.Mode of Payment (Paypal, Bitcoins)
                            </a>
                        </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                            <div class="panel-body">
                                <div class="checkbox checkbox--1">
                                    <label class="to-enable--sr-checklist__commentbox">
                                        <input type="checkbox" value=""> Have you checked this?
                                        <i class="fa fa-checkbox--1 text-green"> </i>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="kycagentComment" class="control-label"><b>Level 3 Comments</b></label>
                                    <textarea name="kycagentComment" id="kycagentComment" class="form-control noresize" rows="3" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row border-bottom border-top m-t-20 p-y-15">
        <div class="col-xs-12"><span class="h4">Comments </span></div>
    </div>
    <div class="row p-t-20">
        <div class="col-xs-12">
            <div class="sr-main__commentbox--others m-b-10">
                <div class="sr-main__commentbox__heading"><b>Name of checker</b></div>
                <!--" commentbox__text "is optional,  -->
                <div class="sr-main__commentbox__text">comment by checkerAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div>
            </div>
            <div class="form-group">
                <label for="kycagentComment" class="control-label"><b>Agent Comments</b></label>
                <textarea name="kycagentComment" id="kycagentComment" class="form-control noresize m-t-5" rows="3"></textarea>
            </div>
        </div>
    </div>
    <form class="" id="ITDPurchaseCreadit">
        <!--    <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-default" onclick="return kycUpdate()">Submit</button>
        </div> -->
    </form>
    <script>
    var rootUrl = '<?php echo $rootUrl;?>';

    function kycUpdate() {
        var type          = document.getElementById("iprequestFrom").value;
        var requestType   = document.getElementById("kycRequestType").value;
        var ticketID      = document.getElementById("kycTicketID").value;
        var userName      = document.getElementById("kycUsername").value;
        var kycDocDate    = document.getElementById("kycDocDate").value;
        var kycDocUpload  = document.getElementById("kycDocUpload").value;
        var actionComment = document.getElementById("kycActionComment").value;
        var actionAmt     = document.getElementById("kycActionAmount").value;
        var agentComment  = document.getElementById("kycagentComment").value;
        var userComment   = document.getElementById("kycuserComment").value;
        var priority      = $('#reqPriority').find(":selected").text();

        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/serviceRequest/forms/addServiceQueryController.php',
            dataType: 'html',
            data: {
                type: type,
                requestType: requestType,
                ticketID: ticketID,
                userName: userName,
                kycDocDate: kycDocDate,
                kycDocUpload: kycDocUpload,
                actionComment: actionComment,
                actionAmt: actionAmt,
                agentComment: agentComment,
                userComment: userComment,
                priority: priority

            },
            async: true,
            success: function(data) {

                alert("Your KYC Document ServiceRequest Ticket is successfully created");
                $('.modal-content').hide();
                $('.modal-backdrop').hide();
                $('#ITDPurchaseCreadit').hide();
                window.location.reload();

            }
        });
    }

    $(document).ready(function() {
        var today = new Date();
        $('.datepicker').datepicker({
            format: 'mm-dd-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function(ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function() {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

    });

    $("#kycTicketID").change(function() {

        var id = $('#kycTicketID').find(":selected").text();
        var type = '<?php echo $pageTypes; ?>';

        $.ajax({
            type: 'POST',
            url: '../forms/kycUpdate.php',
            dataType: 'html',
            data: {
                id: id,
                type: type
            },
            async: true,
            success: function(data) {
                $('#ITDPurchaseCreadit').html(data);
            }
        });
    });

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });


    $('.checkbox--2').on('click', function(event) {
        // event.preventDefault();
        // console.log($(this).find('input').prop('checked'));
        console.log($(this).find('.alert').hasClass('alert-pre'));
        if ($(this).find('input').prop('checked')) {
            $(this).find('.alert').removeClass('alert-default').addClass('alert-success')
        } else {
            $(this).find('.alert').removeClass('alert-success').addClass('alert-default')
        }
    });
    $('.to-enable--sr-checklist__commentbox').on('click', function(event) {
        // event.preventDefault();
        /* Act on the event */
        // console.log($(this).find('.alert').hasClass('alert-pre'));
        if ($(this).find('input').prop('checked')) {
            // console.log("yes checked", $(this).parents(".panel-body"));
            $(this).parents(".panel-body").find('textarea').removeAttr('disabled');
        } else {
            console.log("not checked");
            $(this).parents(".panel-body").find('textarea').attr('disabled', 'disabled');
        }
    });
    </script>
