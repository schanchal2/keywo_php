<?php
include "../../layout/header.php"; ?>
<main>
<div class="container-fluid">
    <div class="title clearfix">
        <h1 class="pull-left">Manage Users</h1>
        <span>ITD Transfer Reverse</span>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <form class="form-horizontal" id="ITDPurchaseCreadit">
                <fieldset class="bg-lightGray1">
                    <div class="form-group">
                        <label for="inputServiceRequestType" class="col-sm-offset-9 col-sm-1 control-label">Dated : </label>
                        <div class="col-sm-2">
                            <!-- <div class="row"> -->
                           <div class="input--styled--date">
                                <input type="text" class="form-control" id="inputServiceRequestType1" placeholder="03.05.17 [Auto]">
                            </div>
                            <!-- </div> -->
                        </div>
                    </div>
                    <!--  -->
                    <div class="form-group">
                        <label for="inputServiceRequestType" class="col-sm-3 control-label">Service Request Type:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputServiceRequestType" placeholder="ITD Transfer Reverse">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputServiceRequestNum" class="col-sm-3 control-label">Service Request No.:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputServiceRequestNum" placeholder="[auto]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="col-sm-3 control-label">Query Ticket ID:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputQueryTicketID" placeholder="[auto / manual if not picked through auto mode ]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group">
                            <label for="inputTransactionId" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-3 p-0" >
                                <input type="text" class="form-control input-bottom-line left-line" id="inputKeyword" placeholder="[auto]">
                            </div>
                            <label for="inputTransactionId" class="col-xs-2 control-label">Transfer ID </label>
                            <div class="col-sm-4 p-0" >
                                <input type="text" class="form-control input-bottom-line" id="inputTransactionId" placeholder="">
                            </div>
                        </div>
                    </div>

                     <div class="">
                        <div class="form-group">
                            <label for="inputTransactionId" class="col-sm-3 control-label">Transfered From(Sender)</label>
                            <div class="col-sm-3 p-0" >
                                <input type="text" class="form-control input-bottom-line left-line" id="inputKeyword" placeholder="[auto]">
                            </div>
                            <label for="inputTransactionId" class="col-xs-2 control-label">Transfered Amt </label>
                            <div class="col-sm-4 p-0" >
                                <input type="text" class="form-control input-bottom-line " id="inputTransactionId" placeholder="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="inputpurchaseDT" class="col-sm-3 control-label">Transferred to (Receiver)</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class=" bottom-line">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputpurchaseDT" placeholder="[Manual](type email id)">

                                    </div>
                                </div>
                                 <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputpurchaseDT" placeholder="[Manual](type email id)">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 

                    
                    <div class="form-group">
                        <label for="inputTransactionId" class="col-sm-3 control-label">Reason for Reversal:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputTransactionId" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputTxnID" class="col-sm-3 control-label">System Checked:</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0 input-bottom-line">
                                        <a href="" class="userCheckedList  left-line p-l-10>Checklist</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputAgentComments" class="col-sm-3 control-label">Agent Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputAgentComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputUAMComments" class="col-sm-3 control-label">UAM Comments</label>
                        <div class="col-sm-9">
                            <div class="row">
                                <div class="">
                                    <div class="col-sm-12 p-0">
                                        <input type="text" class="form-control input-bottom-line left-line" id="inputUAMComments" placeholder="[manual]">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
             <div class="row">
                <div class="col-xs-12">
                    <div class="table--lightBlue2">
                        <table class="table text-center table-responsive">
                            <thead>
                                <tr>
                                    <th>Raised By</th>
                                    <th>UAM Approved </th>
                                    <th>MM Approved</th>
                                    <th> SM Approved </th>
                                    <th> Priority</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <span class="text-darkblue font-sm">Tom (CS)</span>
                                        <br>
                                        <span class="font-sm">(TAT 3 Working Days)</span>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-default">
                                        <i class="fa fa-pause fa-lg text-lightGray"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-check fa-lg text-darkblue"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-times fa-lg text-lightGray"></i>
                                        </button>
                                    </td>
                                    <td class="text-center">
                                        <button class="btn btn-default">
                                        <i class="fa fa-pause fa-lg text-lightGray"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-check fa-lg text-darkblue"></i>
                                        </button>
                                        <button class="btn btn-default">
                                        <i class="fa fa-times fa-lg text-lightGray"></i>
                                        </button>
                                    </td>
                                    <td> </td>
                                    <td class="text-lightGray text-center font-lg"> high/normal/low </td>
                                </tr>
                                <tr >
                                    <td  colspan="2" class="text-center bg-lightBlue border-none">
                                        <span class="text-white f-sz16 ">Closed By : </span>
                                        <span class="text-darkBlue f-sz16">Tom </span>
                                    </td>
                                    <td class="text-center bg-lightBlue border-none">
                                        <span class="text-white f-sz16">Closed On : </span>
                                        <span class="text-lightGray f-sz16">[manual]</span>
                                    </td>
                                    <td colspan="2" class="bg-lightBlue border-none text-left">
                                        <button class="btn btn-default btn-xs text-darkblue ViewComment"> View Comment</button>
                                        <button class="btn btn-xs doc-btn AttachDoc text-white m-l-10"> Attach Doc.</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" class="border-bottom">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</main>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>