<?php

session_start();
require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../core/errorMap.php');
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

/**
 * To decide whether request is for "block" or "unblock" 
 * To set label Text
 */
$reqType =$_GET["reqType"];
$requestNo = $_GET["requestNo"]; 
$block1 =  explode("_", $reqType);
if ($block1[(count($block1)-1)] == "block") {
    $blockPlacehoder= "block";
}else{
    $blockPlacehoder= "unblock";
}
/**********************************************************************/
/**
 * To display form elements Based on request type
 * 
 */
    $basic = false;/*  */$checklist = false;/*  */$reson = false; /*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */

switch ($reqType) {
    case 'domain_block':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= false; /**/ $domain=true; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    case 'domain_unblock':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= false; /**/ $domain=true; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    case 'ip_block':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= true; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    case 'ip_unblock':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= true; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    case 'itd_purchase_debit':
    $basic = true;/*  */$checklist = false;/*  */$reson = false;/*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=true;  /*     */ $documnets = false;/*        */
    break;

    case 'itd_purchase_credit':
    $basic = true;/*  */$checklist = false;/*  */$reson = false;/*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=true;  /*     */ $documnets = false;/*        */
    break;

    case 'user_account_block':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    case 'kyc_update':
    $basic = true;/*  */$checklist = false;/*  */$reson = false;/*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = true;/*        */
    break;

    case 'user_account_unblock':
    $basic = true;/*  */$checklist = false;/*  */$reson = true; /*   */ $ip= false; /**/ $domain=false; /*   */$transactionId=false;  /*     */ $documnets = false;/*        */
    break;

    default:
    break;
}
/**********************************************************************/

    $getTicketdata = getServiceRequestInfo($conn,$requestNo);
    if(noError($getTicketdata))
    {
        $getData            = $getTicketdata["data"][0]; 
        $ipAddress          = $getData["ip_address"]; 
        $reasonBlockUnblock = $getData["block_reason"];
        $agentComment       = $getData["agent_comment"];
        $txnID              = $getData["txn_id"];
        $ipDomain           = $getData["ip_domain"];
        $dueDate            = $getData["duedate"];
        $kycDocDate         = $getData["kyc_doc_date"];
        $kycDoc             = $getData["kyc_doc"];
        $hodComment         = $getData["hod_comment"];
        $mmComment          = $getData["mm_comment"];
        $ticketNo           = $getData["ticket_id"];
        $userName           = $getData["username"];
        $priority           = $getData["req_priority"];

        $kycData = json_decode($kycDoc, true);
    }

    $url = $_GET["urlPageName"];

 ?>
  <?php if ($basic ): ?>
    <div class="row">
        <div class="p-x-15">
            <div class="row">
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="control-label"><b>Query Ticket ID</b></label>
                        <input type="text" class="form-control" name="requestNo" id="request_no" placeholder="" value="<?php echo $ticketNo; ?>" readonly="">
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="inputUsername" class="control-label"><b>Email ID</b></label>
                        <input type="text" class="form-control" name="" id="" placeholder="" value="<?php echo $userName; ?>" readonly />
                    </div>
                </div>
                <div class="col-xs-4">
                    <div class="form-group">
                        <label for="inputQueryTicketID" class="control-label"><b>Priority</b></label>
                        <input type="text" class="form-control" name="" id="" placeholder="" value="<?php echo $priority; ?>" readonly />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
    <?php if ($ip): ?>
    <!-- <div class="p-t-20">     -->
        <div class="row">
            <div class="col-xs-12">
                <div class="form-group m-b-10">
                    <label for="ipaddress" class="control-label"><b>IP</b></label>
                    <input type="text" class="form-control" name="ipaddress" id="ipaddress" value="<?php echo $ipAddress; ?>" disabled />
                </div>
            </div>
        </div>
    <!-- </div> -->
    <?php endif ?>
    <?php if ($domain): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group m-b-10">
                <label for="domainName" class="control-label"><b>Domain</b></label>
                <input type="text" name="domainName" class="form-control" id="domainName" value="<?php echo $ipDomain; ?>" disabled />
            </div>
        </div>
    </div>
    <?php endif ?>
    <?php if ($transactionId): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group m-b-10">
                <label for="txnID" class="control-label"><b>Transaction ID </b></label>
                <input type="text" class="form-control" name="txnID" id="txnID" value="<?php echo $txnID; ?>" disabled />
            </div>
        </div>
    </div>
    <?php endif ?>
    <?php if ($reson): ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group m-b-10">
                <label for="reason" class="control-label"><b>Reason to <?= $blockPlacehoder ?> </b></label>
                <input type="text" class="form-control" name="reason" id="reason" value="<?php echo $reasonBlockUnblock; ?>" disabled />
            </div>
        </div>
    </div>
    <?php endif ?>
    <?php if ($documnets): ?>
    <div class="row border-bottom  border-top m-t-20 p-t-10">
        <div class="col-xs-6"><span class="h4">Documents</span></div>
        <div class="col-xs-6">
            <div class="form-group m-b-10">
                <div class="row">
                    <label for="kycDocDate" class="col-sm-4 control-label text-right l-h17 m-y-5">Uploaded on :</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" name="kycDocDate" value="<?php echo $kycDocDate; ?>" id="kycDocDate" placeholder="[Date]" disabled>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="row p-t-20 KYC-userDocuments">
                <?php 
                /*=================================================
                =            in case of disabled state            =
                =================================================*/
                ?>
                <div class="col-xs-4">
                    <label class="checkbox--2" for="panCard">
                    <?php if(in_array("panCard", $kycData)) { $success = "success"; $panCard = "checked"; } else { $success = "default"; $panCard = ""; } ?>
                        <div class="alert alert-<?php echo $success; ?>"> PAN Card
                            <input type="checkbox" value="" id="panCard" name="panCard" disabled <?php  echo $panCard; ?>> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                <?php if(in_array("passport", $kycData)) { $success = "success"; $passport = "checked"; } else { $success = "default"; $passport = ""; } ?>
                    <label class="checkbox--2" for="Passport">
                        <div class="alert alert-<?php echo $success; ?>"> Passport
                            <input type="checkbox" value="" id="Passport" name="Passport" disabled <?php echo $passport; ?>> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <?php 
                /*=====  End of in case of disabled state  ======*/
                ?>
                <div class="col-xs-4">
                    <label class="checkbox--2" for="aadhaarcard">
                    <?php if(in_array("aadhaarcard", $kycData)) { $success = "success"; $aadhaarcard = "checked"; } else { $success = "default"; $aadhaarcard = ""; } ?>
                        <div class="alert alert-<?php echo $success; ?>"> Aadhaar Card
                            <input type="checkbox" value="" id="aadhaarcard" name="aadhaarcard" disabled <?php echo $aadhaarcard; ?>> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2" for="voters_identity_card">
                    <?php if(in_array("voters_identity_card", $kycData)) { $success = "success"; $votersId = "checked"; } else { $success = "default"; $votersId = ""; } ?>
                        <div class="alert alert-<?php echo $success; ?>"> Voters Identity Card
                            <input type="checkbox" id="voters_identity_card" name="voters_identity_card" value="" disabled <?php echo $votersId; ?> > <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2" for="driving_license">
                    <?php if(in_array("driving_license", $kycData)) { $success = "success"; $drivingLicense = "checked"; } else { $success = "default"; $drivingLicense = ""; } ?>
                        <div class="alert alert-<?php echo $success; ?>"> Driving License
                            <input type="checkbox" value="" name="driving_license" id="driving_license" disabled <?php echo $drivingLicense; ?>> <i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
                <div class="col-xs-4">
                    <label class="checkbox--2" for="bank_passbook">
                    <?php if(in_array("bank_passbook", $kycData)) { $success = "success"; $bankPassbook = "checked"; } else { $success = "default"; $bankPassbook = ""; } ?>
                        <div class="alert alert-<?php echo $success; ?>"> Bank Passbook
                            <input type="checkbox" value="bank_passbook" name="bank_passbook" id="bank_passbook" disabled <?php echo $bankPassbook; ?>><i class="fa fa-checkbox--2"> </i> </div>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
    <?php if ($checklist): ?>
    <section class="row">
        <div class="col-xs-12">
            <div class="row border-bottom border-top m-t-20 p-y-15">
                <div class="col-xs-12"><span class="h4">Checklist</span></div>
            </div>
            <div class="form-group p-t-20">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              1.Users account status (Active/DeActive)
                            </a>
                            <!--=========================================
                            =            if already checked add this            =
                            ==========================================-->
                            <i class="fa fa-check-square text-green pull-right"></i>    
                            <!--====  End of if already checked add this  ====-->
                        </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <!--==================================================
                                =            if already checked add this             =
                                ===================================================-->
                                <div class="sr-checklist__commentbox--others m-b-10">
                                    <div class="sr-checklist__commentbox__heading"><b>Name of checker</b></div>
                                    <!--" commentbox__text "is optional,  -->
                                    <div class="sr-checklist__commentbox__text">comment by checkerAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                                <!--====  End of if already checked add this   ====-->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              2.Account Balance
                            </a>
                            <i class="fa fa-check-square text-green pull-right "></i>    
                        </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <!--==================================================
                                =            if already checked add this             =
                                ===================================================-->
                                <div class="sr-checklist__commentbox--others m-b-10">
                                    <div class="sr-checklist__commentbox__heading"><b>Name of checker</b></div>
                                    <!--" commentbox__text "is optional,  -->
                                    <!-- <div class="sr-checklist__commentbox__text">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div> -->
                                </div>
                                <!--====  End of if already checked add this   ====-->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              3.Deposit transaction history
                            </a>
                        </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                <div class="checkbox checkbox--1">
                                    <label class="to-enable--sr-checklist__commentbox">
                                        <input type="checkbox" value=""> Have you checked this?
                                        <i class="fa fa-checkbox--1 text-green"> </i>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label"><b>Level 3 Comments</b></label>
                                    <textarea name="" id="" class="form-control noresize" rows="3" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                            <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                              4.Mode of Payment (Paypal, Bitcoins)
                            </a>
                        </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                            <div class="panel-body">
                                <div class="checkbox checkbox--1">
                                    <label class="to-enable--sr-checklist__commentbox">
                                        <input type="checkbox" value=""> Have you checked this?
                                        <i class="fa fa-checkbox--1 text-green"> </i>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="" class="control-label"><b>Level 3 Comments</b></label>
                                    <textarea name="" id="" class="form-control noresize" rows="3" disabled></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php endif ?>
    <?php if ($basic): ?>
   <!--  <div class="row border-bottom border-top m-t-20 p-y-15">
        <div class="col-xs-12"><span class="h4">Comments </span></div>
    </div> -->
    <div class="row">
        <div class="col-xs-12">
            <!-- <div class="sr-main__commentbox--others m-b-10"> -->
                <!-- <div class="sr-main__commentbox__heading"><b>Name of checker</b></div> -->
                <!-- " commentbox__text "is optional,   -->
              <!--   <div class="sr-main__commentbox__text">comment by checkerAnim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                </div> -->
            <!-- </div> -->
            <div class="form-group">
                <label for="kycagentComment" class="control-label"><b>Agent Comments</b></label>
                <textarea name="kycagentComment" id="kycagentComment" value="<?php echo $agentComment; ?>" class="form-control noresize m-t-5" rows="3" disabled><?php echo $agentComment; ?></textarea>
            </div>
            <?php if($url == 'srmm.php' || $url == 'srsm.php'){ ?>
            <div class="form-group">
                <label for="kycagentComment" class="control-label"><b>HOD Comments</b></label>
                <textarea name="kychodComment" id="kychodComment" value="" class="form-control noresize m-t-5" rows="3" disabled><?php echo $hodComment; ?></textarea>
            </div> <?php } if($url == 'srsm.php') {?>
            <div class="form-group">
                <label for="kycagentComment" class="control-label"><b>MM Comments</b></label>
                <textarea name="kycmmComment" id="kycmmComment" value="" class="form-control noresize m-t-5" rows="3" disabled><?php echo $mmComment; ?></textarea>
            </div> <?php } ?>
        </div>
    </div>
    <?php endif ?>

    <script>

    $(document).ready(function() {
        var today = new Date();
        $('.datepicker').datepicker({
            format: 'mm-dd-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function(ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function() {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });

    $('.checkbox--2').on('click', function(event) {
        // event.preventDefault();
        // console.log($(this).find('input').prop('checked'));
        console.log($(this).find('.alert').hasClass('alert-pre'));
        if ($(this).find('input').prop('checked')) {
            $(this).find('.alert').removeClass('alert-default').addClass('alert-success')
        } else {
            $(this).find('.alert').removeClass('alert-success').addClass('alert-default')
        }
    });
    $('.to-enable--sr-checklist__commentbox').on('click', function(event) {
        // event.preventDefault();
        /* Act on the event */
        // console.log($(this).find('.alert').hasClass('alert-pre'));
        if ($(this).find('input').prop('checked')) {
            // console.log("yes checked", $(this).parents(".panel-body"));
            $(this).parents(".panel-body").find('textarea').removeAttr('disabled');
        } else {
            console.log("not checked");
            $(this).parents(".panel-body").find('textarea').attr('disabled', 'disabled');
        }
    });
    </script>
