<div class="modal fade" id="reject" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Give Reason For Reject</h4> <br>

                <div class="form-group">
                    <textarea class="form-control noresize" maxlength="1000" rows="3" id="rejectReason" rows="5.5" placeholder="Enter Your Comments Here"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onclick="rejectServiceRequest()">Submit</button>
            </div>

            <div id="escalateData">

            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="escalation" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Give Reason For Escalation</h4> <br>

                <div class="form-group">
                    <textarea class="form-control noresize" maxlength="1000" rows="3" id="escalateReason" rows="5.5" placeholder="Enter Your Comments Here"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-default" onclick="escalateServiceRequest()">Submit</button>
            </div>

            <div id="escalateData">

            </div>
        </div>

    </div>
</div>