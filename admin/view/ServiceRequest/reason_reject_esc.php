
<?php

$status = $_POST["status"];
?>
<div class="modal fade" id="raise_reason_model" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Give Reason For <?php echo ucfirst($status); ?></h4>
            </div>

            <div class="form-group">
                <textarea class="form-control noresize" maxlength="1000" rows="3" id="commentsReason" rows="5.5" placeholder="Enter Your Comments Here"></textarea>
            </div>

        </div>
    </div>
</div>
