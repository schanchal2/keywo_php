<?php
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once "../../core/errorMap.php";
require_once "../../model/geolocation/geoLocation.php";
require_once "../../model/serviceRequest/serviceRequestModel.php";


$pageBy = $_POST["type"];

$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$pageTypes="";
$getID =  getQueryListDetail($connKeywords,$pageTypes);
if (noError($getID)) {
    $errMsg = $getID["errMsg"];
}

?>
    <div class="modal madal--1 fade" id="raise_sr_model" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="width: 920px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                    <h4 class="modal-title" id="myModalLabel" style="color:white;font-weight: bold"> Raise Service Request</h4>
                </div>
                <form class="" id="ITDPurchaseCreadit" enctype="multipart/form-data" data-toggle="validator" role="form">
                    <div class="modal-body clearfix">
                        <div class="p-x-10 p-b-10 border-bottom">
                            <div class="row">
                                <div class="col-xs-3 m-y-5 l-h17">Date :
                                    <?php echo date('d.m.Y'); ?>
                                </div>
                                <?php
                            /*==================================================================
                            =            to be showen at the time of editing ticket            =
                            ==================================================================*/
                            ?>
                                    <!-- <div class="col">
                                <div class="p-5">Service Request No.:
                                    <span class="text-darkblue f-wt8 f-sz21 l-h10">
                                        <?php
                            // if(!empty($getLastId)) {
                            // $getLastId = $getLastId+1;
                            // } else {
                            // $getLastId = 1;
                            // }
                            ?>
                                        <?php // echo " SR- ".$getLastId; ?>
                                    </span>
                                </div>
                            </div> -->
                                    <?php
                            /*=====  End of to be showen at the time of editing ticket  ======*/
                            ?>
                                        <div class="col-xs-3 col-xs-offset-2 m-y-5 l-h17 text-right">Service Request Type :</div>
                                        <div class="col-xs-4">
                                            <label class="dropdownOptions">
                                                <select id="callingPageName" name="callingPageName" class="form-control">
                                                    <option value="" disabled="" selected> Select Service Request Type</option>
                                                    <option value="domain_block">Domain Block</option>
                                                    <option value="domain_unblock">Domain Unblock</option>
                                                    <option value="itd_purchase_debit">ITD Purchase (Debit)</option>
                                                    <option value="itd_purchase_credit">ITD Purchase (credit)</option>
                                                    <option value="ip_block">IP Block</option>
                                                    <option value="ip_unblock">IP Unblock</option>
                                                    <option value="kyc_update">KYC Update</option>
                                                    <option value="user_account_block">User Account Block</option>
                                                    <option value="user_account_unblock">User Account Unblock</option>
                                                    <option value="other">other</option>
                                                </select>
                                            </label>
                                        </div>
                            </div>
                        </div>
                        <?php # "Query Ticket ID" "Username"  "Priority"  ?>
                        <div class="p-x-10 p-t-10 border-bottom testing">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="inputQueryTicketID" class="control-label"><b>Query Ticket ID</b></label>
                                        <label class="dropdownOptions">
                                            <div class="select--1">
                                                <select class="select--1 chosen-select form-control" name="kycTicketID" id="kycTicketID" disabled>
                                                    <option value="" disabled selected>Select TicketID</option>
                                                    <?php
                                            foreach ($errMsg as $getdata) {
                                                $getTicketID = $getdata["ticket_id"];
                                                ?>
                                                        <option value="<?php echo $getTicketID; ?>">
                                                            <?php echo $getTicketID; ?>
                                                        </option>
                                                        <?php } ?>
                                                        <option value="other">Other</option>
                                                </select>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="kycUsername" class="control-label"><b>Email ID</b></label>
                                        <input type="email" class="form-control" name="kycUsername" id="kycUsername" placeholder="placehoder email" value="<?php //echo $getEmail; ?>" readonly/>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="form-group">
                                        <label for="reqPriority" class="control-label"><b>Priority</b></label>
                                        <div class="">
                                            <label class="dropdownOptions">
                                                <select class="form-control" name="reqPriority" id="reqPriority" disabled>
                                                    <option value="high">High</option>
                                                    <option value="medium">Medium</option>
                                                    <option value="low">Low</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="p-x-10 p-t-20">
                            <div class="row">
                                <div id="callPageAjaxData" class="col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-default" id="submitSr" onclick="kycUpdate()" disabled>Submit</button>
                        <!-- <button type="submit" class="btn btn-default" id="" onclick="">Submit</button> -->
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    var rootUrl = '<?php echo $rootUrl;?>';
    $('#raise_sr_model').on('hidden.bs.modal', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('#submitSr').attr('disabled', 'disabled');
        resetSelectTag("#kycTicketID");
        $("#callPageAjaxData").html("");
        $("#kycTicketID").val('').trigger("chosen:updated");
    });


    // $("#kycTicketID").change(function() {
    // var ticket = $(this).val();
    // $.ajax({
    //     type: 'POST',
    //     url: rootUrl + 'admin/controller/serviceRequest/forms/getUserData.php',
    //     dataType: 'json',
    //     data: {
    //         ticket_id: ticket
    //     },
    //     async: true,
    //     success: function(data) {
    //         if (data["errCode"] == "-1") {
    //             //console.log(data["errMsg"]);
    //             $("#kycUsername").val(data["errMsg"][0]["reg_email"]);
    //         }
    //     }
    // });
    // });

    function kycUpdate() {
// console.log(e);
        // var passport = document.forms[0];
        // var aadhaarcard = document.forms[0];
        // var panCard = document.forms[0];
        // var voters_identity_card = document.forms[0];
        // var driving_license = document.forms[0];
        // var bank_passbook = document.forms[0];


        //console.log(rootUrl);

        var myForm = $('#ITDPurchaseCreadit')[0];
        var formData1 = new FormData(myForm);

        operateLadda("submitSr", "start");
        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/serviceRequest/forms/addServiceQueryController.php',
            dataType: 'json',
            data: formData1,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            async: true,
            success: function(data) {
                //alert("Your KYC Document ServiceRequest Ticket is successfully created");
                operateLadda("submitSr", "stop");

                if (data["errCode"] == "-1") {
                    $('#ITDPurchaseCreadit')[0].reset();
                    $('#raise_sr_model').modal('hide');
                    $("#errors").html(bsAlert("success", "Your ServiceRequest Ticket Created successfully!"));
                    getAgentsData();
                    getagentAnalytics();

                } else {
                    $("#errors").html(bsAlert("danger", "Error in Creating ServiceRequest, please try again."));
                }

            }
        });
    }

    /////////////////////////////////////////////////////////////
    // to enable submit button on SRtype  an ticket selection  //
    /////////////////////////////////////////////////////////////
    function enableSubmit(argument) {
        // if (($("#callingPageName").val() !== null) && ($("#kycTicketID").val() !== null)) {
        if (($("#callingPageName").val() !== null) && ($('#kycTicketID').find(":selected").val() !== null)) {
            // console.log('$("#callingPageName").val() : ', $("#callingPageName").val());
            // console.log('$("#kycTicketID").val() : ', $("#kycTicketID").val());
            if (true) {
                $('#submitSr').removeProp('disabled');
            }
        }
    }

    function srValidator() {
        // console.log($('#ITDPurchaseCreadit'));
        // $('body').addClass('class_name')
        // $('#ITDPurchaseCreadit').validator('destroy');
        $('#ITDPurchaseCreadit').validator();

    }


    var pagename;
    $(document).ready(function() {
        $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + $('#callingPageName').val());
        $('#callingPageName').on('change', function() {
            $('#callPageAjaxData').html('');
            var callingPageName = $("#callingPageName").val();
            if (callingPageName !== null) {
                // console.log(this.value);
                pagename = this.value;
                if (callingPageName == "other") {
                    resetSelectTag("#kycTicketID", "other");
                    $('#kycUsername').removeAttr('readonly');
                    $('#reqPriority').removeAttr('disabled');
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=other", function(argument) {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                        enableSubmit();
                    });
                    $('#kycTicketID').removeAttr('disabled');



                } else {
                    resetSelectTag("#kycTicketID");
                    $('#kycTicketID').removeAttr('disabled');
                    // $("#kycTicketID").chosen();
                    $("#kycTicketID").val('').trigger("chosen:updated");
                    $('#kycUsername').val('').prop('readonly', 'readonly');
                    $('#reqPriority').prop('disabled', 'disabled')
                }

            }
            $('#submitSr').prop('disabled', 'disabled');

        });

        $('#kycTicketID').on('change', function() {
            var kycTicketID = $('#kycTicketID').find(":selected").val();

            $.ajax({
                type: 'POST',
                url: rootUrl + 'admin/controller/serviceRequest/forms/getUserData.php',
                dataType: 'json',
                data: {
                    ticket_id: kycTicketID
                },
                async: true,
                success: function(data) {
                    if (data["errCode"] == "-1") {
                        //console.log(data["errMsg"]);
                        $("#kycUsername").val(data["errMsg"][0]["reg_email"]);
                    }
                }
            });


            // 
            // console.log("kycTicketID bfor :", kycTicketID.trim());
            resetSelectTag("#kycTicketID", kycTicketID.trim());
            kycTicketID = $('#kycTicketID').find(":selected").val();
            // console.log("kycTicketID aftr :", kycTicketID.trim());

            $('#reqPriority').removeAttr('disabled');

            if (this.value == "other") {
                $('#kycUsername').removeAttr('readonly');
            } else {
                $('#kycUsername').prop('readonly', 'readonly');
            }
            console.log("enableSubmit()")
            enableSubmit();
            console.log("END enableSubmit()")


            $("#callPageAjaxData").html("");
            // $('#ITDPurchaseCreadit').validator(); 

            switch (pagename) {
                case "domain_block":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "domain_unblock":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "ip_block":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "ip_unblock":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "itd_purchase_debit":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "itd_purchase_credit":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "kyc_update":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "user_account_block":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "user_account_unblock":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;

                case "other":
                    $("#callPageAjaxData").html(showLoadingDiv()).load("../forms/my_sr_modal_body.php?type=<?= $pageBy; ?>&reqType=" + pagename + "&ticketID=" + kycTicketID, function() {
                        $('#ITDPurchaseCreadit').validator('destroy');
                        $('#ITDPurchaseCreadit').validator();
                    });
                    break;
            }
        });
    });

    // function enableSubmit(argument) {
    //     if (($("#reject").val() !== null)) {
    //         $('#submitReject').removeAttr('disabled');
    //     }
    // }
    </script>
