<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once "../../model/geolocation/geoLocation.php";
checkGroupAccess();
?>

    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">

    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Report Bug</h1><span>User Generated</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="reportBugAnalytics">

                        </div>

                    </div>


                </div>

                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline pull-right" role="form" name="filter">

                                <div class="input-group styled-select" >

                                    <input style="width: 110px;z-index: 1" onkeyup="getNewBugs()" type="text" id="BUGID" class="form-control" value='' placeholder="Bug ID">



                                </div>

                                <div class="input-group styled-select" >
                                    <select id="bugStatus" style="width: 130px">
                                        <option value="" disabled selected>Status</option>
                                        <option value="open" selected>open</option>
                                        <option value="closed">closed</option>
                                    </select>
                                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                </div>

                            </form>

                        </div>
                    </div>

                    <!-- Single button -->
                </div>

            </div>
            <div id="errors"></div>
            <div id="reportBugUserReportAjax">

            </div>
        </div>
    </main>

<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php"; ?>

<script>


    $(document).ready(function () {
        getNewBugs();
        getBugsAnalytics();
    });


    function getNewBugs() {
        var BUGID = ($("#BUGID").val());
        var bugStatus = ($("#bugStatus").val());

        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 5;
        }

        var creationTime = "";


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugInternalreport.php",
            data: {
                limit: limit,
                BUGID: BUGID,
                bugStatus: bugStatus,
                bugType:"user_report"
            },
            success: function (data) {
                $("#reportBugUserReportAjax").html("");
                $("#reportBugUserReportAjax").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });

    }


    function getNewBugsPages(pageNo, BUGID, bugStatus,limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugInternalreport.php",
            data: {
                limit: limit,
                BUGID: BUGID,
                bugStatus: bugStatus,
                page: pageNo,
                bugType:"user_report"
            },
            success: function (data) {
                $("#reportBugUserReportAjax").html("");
                $("#reportBugUserReportAjax").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });

    }

    $("#bugStatus").change(function () {
        getNewBugs();
    });


    function getBugsAnalytics()
    {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugAnalytics.php",
            data: {
                bugType:"user_report"
            },
            success: function (data) {
                $("#reportBugAnalytics").html("");
                $("#reportBugAnalytics").html(data);
            },
            error: function () {
                console.log("fail");
            }
        });
    }


    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);

    function closeReportModel(bug_id,bug_type)
    {
        $.ajax({
            type: 'POST',
            url: rootUrl+'admin/controller/reportBug/reportUserBugController.php',
            dataType: 'json',
            data: {
                bug_id          : bug_id,
                bug_type  : bug_type

            },
            async: true,
            success: function (data) {

                if(data["errCode"]=="-1")
                {
                    $("#errors").html(bsAlert("success","Bug status changed successfully."));
                    getNewBugs();
                    getBugsAnalytics();

                }else
                {
                    $("#errors").html(bsAlert("danger","Error in updating bug status please try again."));

                }

            }
        });
    }

</script>
