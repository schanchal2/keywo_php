<?php

include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
/*********************** get user Permissions ********************/
$myp=mypermissions($largest);
checkGroupAccess();
/*********************** get user Permissions ********************/
?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/bootstrap/css/bootstrap-select-min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Report Bug</h1><span>Internal Report</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>
            <div id="userRefferalAnalatics">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="reportBugAnalytics">
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-bottom: 15px;">
                    <div class="col-lg-12">
                        <div id="filter" class="sign-upverification">
                            <form action="" method="POST" class="form-inline pull-right" role="form" name="filter">
                                <div class="input-group styled-select">
                                    <input style="width: 110px;z-index: 1" onkeyup="getNewBugs()" type="text" id="BUGID" class="form-control" value='' placeholder="Bug ID">
                                </div>
                                <div class="input-group styled-select">
                                    <select id="bugStatus" style="width: 130px">
                                        <option value="" disabled>Status</option>
                                        <option value="open" selected>open</option>
                                        <option value="closed">closed</option>
                                    </select>
                                    <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>
                                </div>
                                <?php
                                if((in_array("write", $myp))) {
                                ?>
                                    <div class="input-group styled-select" style="z-index: 1;">
                                        <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" data-toggle="modal" data-target="#addNewInternal">
                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                        </button>
                                    </div>
                                    <?php } ?>
                            </form>
                        </div>
                    </div>
                    <!-- Single button -->
                </div>
            </div>
            <div id="errors"></div>
            <div id="reportBugUserReportAjax">
            </div>
        </div>
    </main>
    <div id="addNewInternal" class="modal madal--1 fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" id="modelDialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    <h5 class="modal-title">Report A Bug </h5>
                </div>
                <form id="bug_report_internal" name="bug_report_internal">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <input type="hidden" id="internalReport" value="internal_report">
                                    <label for="bug_description">Add Description &nbsp&nbsp:<i class="fa fa-asterisk fsz-8 text-red    mandatory-field"></i>
                                    </label>
                                    <textarea id="bug_description" class="form-control noresize" maxlength="1000" rows="3" id="bug_desc" name="bug_desc" placeholder="Enter Your Comments Here"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 file-in">
                                <div class="form-group file-in__pseudo__form-group">
                                    <!-- <input class="form-control file-in-input hidden" type="file" name="files[]" multiple="multiple" /> -->
                                    <input class="form-control file-in-input hidden" type="file" name="screenShots[]" />
                                    <i class="fa fa-close hidden"> </i>
                                    <span id="helpBlock1" class="help-block hidden">Only gif,png, jpg, jpeg are supported </span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="modalError" style="color:red;margin-left: 15px"></div>
                        </div>
                    </div>
                    <div class="modal-footer border-none">
                        <button type="reset" class="btn btn-md btn-primary" id="reset" data-dismiss="modal">Close</button>
                        <button type="button" id="submit_internalBugs_btn" class="btn btn-success ajaxhide" disabled onclick="internalReportModel()">Submit
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
    <?php include "{$docRootAdmin}view/layout/transperent_footer.php"; ?>
    <script>
    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);

    function internalReportModel() {
        var formData = new FormData($("#bug_report_internal")[0]);
        formData.append("bug_type", "internal_report");

        var bug_desc = $("#bug_desc").val();

        if (bug_desc < 50) {
            $("#modalError").html("Bug description should be greater than 50 characters.");
            return false;
        }

        if (bug_desc > 650) {
            $("#modalError").html("Bug description should be less than 650 characters.");
            return false;
        }
        operateLadda("submit_internalBugs_btn", "start");
        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/reportBug/reportUserBugController.php',
            dataType: 'json',
            data: formData,
            async: true,
            cache: false,
            processData: false,
            contentType: false,
            success: function(data) {
                operateLadda("submit_internalBugs_btn", "stop");
                if (data["errCode"] == "-1") {
                    $("#addNewInternal").modal("hide");
                    $("#errors").html(bsAlert("success", "Your Bug is successfully added!"));
                    getNewBugs();
                    getBugsAnalytics();

                } else {
                    $("#errors").html(bsAlert("danger", "Error in adding bug please try again."));
                }
            }
        });
    }


    $(document).ready(function() {
        getNewBugs();
        getBugsAnalytics();
    });


    function getNewBugs() {

        var BUGID = ($("#BUGID").val());
        var bugStatus = ($("#bugStatus").val());

        var limit = ($("#LimitedResult").val());
        if (((typeof limit) == "undefined") || limit == "") {
            limit = 5;
        }

        var creationTime = "";


        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugInternalreport.php",
            data: {
                limit: limit,
                BUGID: BUGID,
                bugStatus: bugStatus,
                bugType: "internal_report"
            },
            success: function(data) {
                $("#reportBugUserReportAjax").html("");
                $("#reportBugUserReportAjax").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }


    function getNewBugsPages(pageNo, BUGID, bugStatus, limit) {

        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugInternalreport.php",
            data: {
                limit: limit,
                BUGID: BUGID,
                bugStatus: bugStatus,
                page: pageNo,
                bugType: "internal_report"
            },
            success: function(data) {
                $("#reportBugUserReportAjax").html("");
                $("#reportBugUserReportAjax").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }

    $("#bugStatus").change(function() {
        getNewBugs();
    });


    function getBugsAnalytics() {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/reportBug/reportBugAnalytics.php",
            data: {
                bugType: "internal_report"
            },
            success: function(data) {
                $("#reportBugAnalytics").html("");
                $("#reportBugAnalytics").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });
    }


    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);

    function closeReportModel(bug_id, bug_type) {
        $.ajax({
            type: 'POST',
            url: rootUrl + 'admin/controller/reportBug/reportUserBugController.php',
            dataType: 'json',
            data: {
                bug_id: bug_id,
                bug_type: bug_type //bug type

            },
            async: true,
            success: function(data) {

                var errorCodes = data["errCode"].toString();
                switch (errorCodes) {
                    case "71":
                        $("#errors").html(bsAlert("danger", "This Bugs is already closed! Please try another one"));
                        getNewBugs();
                        getBugsAnalytics();
                        break;

                    case "-1":
                        $("#errors").html(bsAlert("success", "Bug status changed successfully."));
                        getNewBugs();
                        getBugsAnalytics();
                        break;

                    default:
                        $("#errors").html(bsAlert("danger", "Error in updating bug status please try again."));
                        break;
                }

            }
        });
    }

    var $uploader = $('.file-in__pseudo__form-group').clone();
    $uploader.appendTo('.file-in')
        .removeClass('file-in__pseudo__form-group')
        .find('input').removeClass('hidden').addClass('visible');

    function totalFiles() {
        return $('.file-in .form-group').length - 1;
    }

    function addFileUploade() {
        $('.file-in__pseudo__form-group')
            .clone()
            .appendTo('.file-in')
            .removeClass('file-in__pseudo__form-group')
            .find('input').removeClass('hidden').addClass('visible');
    }
    $('.file-in').on('click', '.fa', function(event) {
        if ($(this).parents(".file-in").find('input.visible').length == 0) {
            addFileUploade();
        }
        $(this).parent(".form-group ").remove();
    });
    $('.file-in').on('change', '.file-in-input', function(event) {
        $total_files = totalFiles();
        if ($total_files <= 3) {
            $(this).parent().removeClass('has-warning').find('.help-block').addClass('hidden')

            var fileName = '';
            fileName = event.target.value.split('\\').pop();

            var extn = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
            if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                $(this).addClass('hidden').removeClass('visible').parent().append(fileName);
                $(this).parent(".form-group").find('.fa').removeClass('hidden');
                if ($total_files < 3) {
                    addFileUploade();
                }
            } else {
                // $(this).parent().append( $("p", {className:"text-danger", innerText: "Only gif,png, jpg, jpeg are supported"}) );
                // var err=$("p", {className:"text-danger"}) ;
                // err.appendTo($(this).parent());
                // $(this).parent().append( );
                // $(this).remove();
                $(this).parent().addClass('has-warning').find('.help-block').removeClass('hidden');
                $(this).val(function() {
                    return this.defaultValue;
                });



            }


        }

    });
    $("#addNewInternal").on('hide.bs.modal', function() {
        // $('#modalError').html('');
        // $('#bug_desc').html('');
        $('#bug_report_internal').find('textarea, input').val('');
        // $('.file-in__pseudo__form-group').nextAll(".form-group").remove();
        // $('.file-in__pseudo__form-group').nextUntil(".file-in .form-group:last-child").remove();
        $(".file-in .form-group:not('.file-in__pseudo__form-group')").remove();
        $('.file-in__pseudo__form-group').clone()
            .appendTo('.file-in')
            .removeClass('file-in__pseudo__form-group')
            .find('input').removeClass('hidden').addClass('visible');

    });

    $('#bug_description').on('input', function(event) {
        event.preventDefault();
        /* Act on the event */
        console.log(this.value.trim().length);
        if (this.value == " ")
            this.value = ""
        if (this.value.trim().length > 0) {
            $('#submit_internalBugs_btn').removeProp('disabled')
        } else {
            $('#submit_internalBugs_btn').prop('disabled', 'disabled')
        }
    });
    </script>
