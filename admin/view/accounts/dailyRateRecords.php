<?php include "../layout/header.php";
checkGroupAccess();
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
?>




<!-- <link rel="stylesheet" property="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
<link rel="stylesheet" property="stylesheet" href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/lightcolor/blue/jtable.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $adminRoot; ?>frontend_libraries/jtable/themes/custom.css" rel="stylesheet" type="text/css" />
<style>

    div.jtable-main-container table.jtable tbody > tr > td .jtable-delete-command-button {
        background: url(../../frontend_libraries/img/deletess.png) no-repeat;
        width: 16px;
        height: 16px;
        opacity: 0.85;
    }


</style>
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Accounts</h1><span>Daily Exchange Rate Records</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <br/>
        <div id="dailyRatesRecordsTable" STYLE="" class=""></div>

    </div>
</main>
</div>
<!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/jtable/jquery.jtable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        //Prepare jTable
        $('#dailyRatesRecordsTable').jtable({
            title: 'Current Rate Record',
            paging: true,
            pageSize: 15,
            sorting: true,
            actions: {
                listAction: '../../controller/account/calling_pageCurrencyRecordsActions.php?action=list'
            },
            fields: {
                id: {
                    title: 'ID',
                    width: '20%',
                    key: true,
                    create: false,
                    edit: false
                },
                timestamp: {
                    title: 'Timestamp',
                    width: '20%',
                    create: false,
                    edit: false

                },
                usd_rates: {
                    title: 'USD Rates',
                    width: '20%',
                    create: false,
                    edit: false

                },
                sgd_rates: {
                    title: 'SGD Rates',
                    width: '20%',
                    create: false,
                    edit: false

                }

            },


        });

        //Load person list from server
        $('#dailyRatesRecordsTable').jtable('load');

    });
</script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
