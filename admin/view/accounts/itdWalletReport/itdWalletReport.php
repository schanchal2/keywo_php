<?php include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");?>
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
  <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">Accounts</h1><span>Internate Dollar Wallet Report</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

        </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script>
        $( document ).ready(function() {

        });

    </script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>