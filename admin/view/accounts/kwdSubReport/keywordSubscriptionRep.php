<?php include "../../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");?>
<link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
<link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
<main>
    <div class="container-fluid">
        <div class="title clearfix">
            <h1 class="pull-left">Accounts</h1><span>Keyword Subscription Report</span>
            <form action="" method="POST" class="form-inline pull-right" role="form" name="">
            </form>
        </div>
        <div class="row m-t-15  ">
            <div class="col-sm-12">
                <div role="tabpanel">
                    <ul class="nav nav-pills nav-justified nav-pills--1" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#income" role="tab" data-toggle="tab"> Income </a>
                        </li>
                        <li role="presentation">
                            <a href="#expenditure" role="tab" data-toggle="tab"> Expenditure </a>
                        </li>
                        <li role="presentation">
                            <a href="#liability" role="tab" data-toggle="tab"> Liability </a>
                        </li>
                        <li role="presentation">
                            <a href="#audit_purpose" role="tab" data-toggle="tab"> Audit Purpose </a>
                        </li>
                    </ul>
                    <div class="tab-content p-t-15">
                        <div role="tabpanel" class="tab-pane active" id="income">
                            <?php include '_filter.php'; ?>
                            <div class="row">
                                <div class="col-xs-12 m-t-15">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pull-right">
                                                <button type="button" class="btn  bg-blue text-white"> <i class="fa fa-angle-left"></i> </button>
                                                <button type="button" class="btn  bg-blue text-white"> <i class="fa fa-angle-right"></i> </button>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>KEYWORD SUBSCRIPTION REPORT (INCOME)</h3>
                                    <?php include 'income/keyword_subscription_report.php'; ?>
                                    <?php include 'income/keyword_subscription_report_1.php'; ?>
                                    <h3>Keywo FEES (INCOME)</h3>
                                    <?php include 'income/keywo_fees.php'; ?>
                                    <?php include 'income/keywo_fees_1.php'; ?>
                                    <h3>KEYWORDS RENEWAL FEES (INCOME)</h3>
                                    <?php include 'income/keywords_renewal_fees.php'; ?>
                                    <?php include 'income/keywords_renewal_fees_1.php'; ?>
                                    <h3>KEYWORDS TRADING FEES (INCOME)</h3>
                                    <?php include 'income/keywords_trading_fees.php'; ?>
                                    <?php include 'income/keywords_trading_fees_1.php'; ?>
                                    <h3>ADS SPACE SOLD ON KEYWO (INCOME)</h3>
                                    <?php include 'income/ads_space_sold_on_keywo.php'; ?>
                                    <?php include 'income/ads_space_sold_on_keywo_1.php'; ?>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="expenditure">
                            <?php include '_filter.php'; ?>
                            <div class="row">
                                <div class="col-xs-12 m-t-15">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="pull-right">
                                                <button type="button" class="btn  bg-blue text-white"> <i class="fa fa-angle-left"></i> </button>
                                                <button type="button" class="btn  bg-blue text-white"> <i class="fa fa-angle-right"></i> </button>
                                            </div>
                                        </div>
                                    </div>
                                    <h3>SEARCH USERS PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/search_users_payout.php'; ?>
                                    <?php include 'expenditure/search_users_payout_1.php'; ?>
                                    <h3>APP DEVELOPERS PAYOUT REPORT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/app_developers_payout_report.php'; ?>
                                    <?php include 'expenditure/app_developers_payout_report_1.php'; ?>
                                    <h3>KEYWORD OWNERS PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/keyword_owners_payout.php'; ?>
                                    <?php include 'expenditure/keyword_owners_payout_1.php'; ?>
                                    <h3>SOCIAL POST REFERRAL PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/social_post_referral_payout.php'; ?>
                                    <?php include 'expenditure/social_post_referral_payout_1.php'; ?>
                                    <h3>KEYWORD REFERRAL PAYOUT (EXPENDITURE)</h3>
                                    <?php  include 'expenditure/keyword_referral_payout.php'; ?>
                                    <?php  include 'expenditure/keyword_referral_payout_1.php'; ?>
                                    <h3>USER REFERRAL PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/user_referral_payout.php'; ?>
                                    <?php include 'expenditure/user_referral_payout_1.php'; ?>
                                    <h3>FIRST PURCHASE PAYOUT (EXPENDITURE) </h3>
                                    <?php include 'expenditure/first_purchase_payout.php'; ?>
                                    <?php include 'expenditure/first_purchase_payout_1.php'; ?>
                                    <h3>SOCIAL POST PAYOUT (EXPENDITURE) </h3>
                                    <?php include 'expenditure/social_post_payout.php'; ?>
                                    <?php include 'expenditure/social_post_payout_1.php'; ?>
                                    <h3>SOCIAL CLICK PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/social_click_payout.php'; ?>
                                    <?php include 'expenditure/social_click_payout_1.php'; ?>
                                    <h3>ADS PUBLISHED ON 3RD PARTY SITES PAYOUT (EXPENDITURE)</h3>
                                    <?php include 'expenditure/ads_published_on_3rd_party_sites_payout.php'; ?>
                                    <?php include 'expenditure/ads_published_on_3rd_party_sites_payout_1.php'; ?>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="liability">
                            <?php include '_filter.php'; ?> liability
                        </div>
                        <div role="tabpanel" class="tab-pane" id="audit_purpose">
                            <?php include '_filter.php'; ?> audit_purpose
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<div class="modal  madal--1 fade madal--monthpicker" id="DownloadTransactionHistory">
    <div class="modal-dialog">
        <div class="modal-content border-all">
            <div class="modal-header bg-color-Light-Grey text-color-White">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close f-sz15"></i> </button>
                <h4 class="modal-title f-sz16">Transaction History</h4>
            </div>
            <div class="modal-body p-0">
                <div id='InlineMenu'></div>
            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-block text-white bg-blue" data-dismiss="modal"> <i class="fa fa-download"></i> Download Report</button>
            </div>
        </div>
    </div>
</div>
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
<script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
<!--====================================
=            month selector            =
=====================================-->
<script type="text/javascript" src="<?php echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo $adminRoot; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"> -->
<!--====  End of month selector  ====-->
<!--===================================
=            onpage script            =
====================================-->
<script type="text/javascript">
var todaysdate = new Date();
$('#userListDateRange').dateRangePicker({
    autoClose: true,
    startDate: todaysdate,
    endDate: todaysdate,

    //format: 'DD.MM.YYYY HH:mm',
    time: {
        enabled: false
    }
}).bind('datepicker-first-date-selected', function(event, obj) {
    var date1 = obj.date1;
}).bind('datepicker-change', function(event, obj) {
    getUserListByDate();
    var newdate = (obj.value);
})
</script>
<script type="text/javascript">
// $('#DownloadTransactionHistory').modal('show');
$('#DownloadTransactionHistory').on('shown.bs.modal', function(e) {

    $("#InlineMenu").MonthPicker({
        SelectedMonth: '04/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        }
    });
})
</script>
<!--====  End of onpage script  ====-->
<script type="text/javascript"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>
