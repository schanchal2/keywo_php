<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th>Date </th>
                <th>No. of Purchases </th>
                <th>No of Users Avg. </th>
                <th>Amt Per Payout </th>
                <th>Total Amt. (ITD) </th>
                <th>Total Amt. (SGD) </th>
                <th>Total Amt. (USD)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">
                    <a href="#">12 Nov 2015</a>
                </td>
                <td class="text-right">150</td>
                <td class="text-right">15</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
            </tr>
        </tbody>
    </table>
</div>
