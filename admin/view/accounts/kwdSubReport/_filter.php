<div class="row">
    <div class="col-xs-2">
        <div class="input-group">
            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
            <input type="text" id="userListDateRange" class="form-control" value="2017-05-02">
        </div>
    </div>
    <div class="col-xs-2 pull-right p-l-0">
        <label class="dropdownOptions">
            <select class="form-control">
                <option value="USD" selected="">Keyword Renewal Fees</option>
                <option value="SGD">SGD</option>
                <option value="BTC">BTC</option>
                <option value="ITD">ITD</option>
            </select>
        </label>
    </div>
    <div class="col-xs-2 pull-right p-l-0 text-right">
        <div class="p-10"> EXG RATE :<span class="text-lightBlue"> 1IT$=00SG$</span></div>
    </div>
    <!-- <div class="col-xs-1 pull-right p-l-0"> -->
        <button type="button" class="btn pull-right bg-lightBlue text-white" data-toggle="modal" href='#DownloadTransactionHistory'>
            <i class="fa fa-download"></i>
        </button>
    <!-- </div> -->
</div>
