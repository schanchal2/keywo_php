<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th> Date </th>
                <th> Serial No. </th>
                <th> Ads Details </th>
                <th> Transaction Id </th>
                <th> Rate per Ads </th>
                <th> No. of Impression </th>
                <th> Discount </th>
                <th> Total Amt. (ITD) </th>
                <th> Total Amt. (SGD)</th>
            </tr>
        </thead>
        <tbody>
             <tr>
                <td class="timestamp text-center">
                    <span class="date">12-12-12</span>
                    <time>12:12:12</time>
                </td>
                <td class="text-right">1</td>
                <td class="text-right"> Lorem ipsum.</td>
                <td class="text-right">WD - 01</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
            </tr>
        </tbody>
    </table>
</div>
