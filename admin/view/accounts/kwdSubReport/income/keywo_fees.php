<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>No. of Qualified Searches</th>
                <th>No. of Users</th>
                <th>No. of Keywords Searched</th>
                <th>Avg. Payout per Search</th>
                <th>Total Amt (ITD)</th>
                <th>Total Amt (SGD)</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">
                    <a href="#">12 Nov 2015</a>
                </td>
                <td class="text-right">150</td>
                <td class="text-right">15</td>
                <td class="text-right">10</td>
                <td class="text-right">0000</td>
                <td class="text-right">2525</td>
                <td class="text-right">2525</td>
            </tr>
        </tbody>
    </table>
</div>
