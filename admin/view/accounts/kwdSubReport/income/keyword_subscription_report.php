<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th> Date </th>
                <th> Keywords Sold (Nos.) </th>
                <th> Users (Nos.) </th>
                <th> Rate per Keyword ITD </th>
                <th> Discount </th>
                <th> Total Amt (ITD) </th>
                <th> Total Amt (SGD) </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center">
                    <a href="#">12 Nov 2015</a>
                </td>
                <td class="text-right">1</td>
                <td class="text-right">15</td>
                <td class="text-right">2525</td>
                <td class="text-right">xxx</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
            </tr>
        </tbody>
    </table>
</div>
