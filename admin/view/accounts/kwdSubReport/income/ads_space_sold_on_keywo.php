<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th>Date </th>
                <th>No. of Ads. Sold </th>
                <th>No. of Advertisers </th>
                <th>Avg Rate per Ads (ITD) </th>
                <th>No. of Impressions </th>
                <th>Discount </th>
                <th>Total Amt (ITD) </th>
                <th>Total Amt (SGD)</th>
            </tr>
        </thead>
        <tbody>
           <tr>
                <td class="text-center">
                    <a href="#">12 Nov 2015</a>
                </td>
                <td class="text-right">150</td>
                <td class="text-right">15</td>
                <td class="text-right">2525</td>
                <td class="text-right">2525</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
            </tr>
        </tbody>
    </table>
</div>
