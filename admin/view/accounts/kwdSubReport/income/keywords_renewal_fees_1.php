<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Serial No.</th>
                <th>Keyword</th>
                <th>Transaction Id</th>
                <th>Amt. Per Keyword</th>
                <th>Total Amt. (ITD)</th>
                <th>Total Amt. (SGD)</th>
                <th>Total Amt. (USD)</th>
                <th>User ID</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="timestamp text-center">
                    <span class="date">12-12-12</span>
                    <time>12:12:12</time>
                </td>
                <td class="text-right">1</td>
                <td class="text-right">Bitcoin</td>
                <td class="text-right">WD-10</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right"> <a href="#"> asd@adsd.com</a></td>
            </tr>
        </tbody>
    </table>
</div>
