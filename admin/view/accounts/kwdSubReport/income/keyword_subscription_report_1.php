<div class="table-responsive table--lightBlue2">
    <table class="table">
        <thead>
            <tr>
                <th>Date</th>
                <th>Serial No.</th>
                <th>Keyword Subscribed</th>
                <th>Transaction Id</th>
                <th>Rate per Keyword</th>
                <th>Discount</th>
                <th>Total Amt. ITD</th>
                <th>Total Amt. SGD</th>
                <th>Total Amt. USD</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="timestamp text-center">
                    <span class="date">12-12-12</span>
                    <time>12:12:12</time>
                </td>
                <td class="text-right">1</td>
                <td class="text-right">15</td>
                <td class="text-right">WD - 01</td>
                <td class="text-right">0000</td>
                <td class="text-right">xxx</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
                <td class="text-right">0000</td>
            </tr>
        </tbody>
    </table>
</div>
