<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
checkGroupAccess();
?>

    <link rel="stylesheet"
          href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">System Logs</h1><span>User Management</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" style="background: #35ccf1;">
                                <li class="active"><a href="#loginLogs" data-toggle="tab">Login</a></li>
                                <li><a href="#SignupLogs" data-toggle="tab">Signup</a></li>
                                <li><a href="#VerifyUserLogs" data-toggle="tab">Verify User</a></li>
                                <li><a href="#ResendActivation" data-toggle="tab">Resend Activation</a></li>
                                <li><a href="#forgotPasswdLogs" data-toggle="tab">Forgot Password </a></li>
                               <li><a href="#ChangePassword" data-toggle="tab">Change Password </a></li>
                            </ul>
                            <div class="tab-content" style="border: solid 1px #bdeefa;padding:10px;">
                                <div class="tab-pane active" id="loginLogs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="loginformexport" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="loginLogsDateRangePiker"
                                                                           name="loginLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readLoginLogs()"></button>
                                                            </div>

                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control" id="logintextSearch"
                                                                       type="text" placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofLoginLogs">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="SignupLogs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="signupformexport" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="signupLogsDateRangePiker"
                                                                           name="signupLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readSignupLogs()"></button>
                                                            </div>


                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control" id="signuptextSearch"
                                                                       type="text" placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofSignupLogs">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="VerifyUserLogs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="VerifyUserformexport" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="VerifyUserLogsDateRangePiker"
                                                                           name="VerifyUserLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readVerifyUserLogs()"></button>
                                                            </div>


                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control" id="VerifyUsertextSearch"
                                                                       type="text" placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofVerifyUserLogs">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="ResendActivation">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="ResendActivationformexport"
                                                      method="post" action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="ResendActivationLogsDateRangePiker"
                                                                           name="ResendActivationLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readResendActivationLogs()"></button>
                                                            </div>


                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control"
                                                                       id="ResendActivationtextSearch" type="text"
                                                                       placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofResendActivationLogs">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="forgotPasswdLogs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="forgotpasswordlogform" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="forgotpassworddatepiker"
                                                                           name="forgotpassworddatepiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readforgotpasswordLogs()"></button>
                                                            </div>


                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control" id="forgotpasstextSearch"
                                                                       type="text" placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="forgotPasswordLogs">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                                <div class="tab-pane" id="ChangePassword">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout"
                                                 style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="ChangePasswordformexport" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control"
                                                                           id="ChangePasswordLogsDateRangePiker"
                                                                           name="ChangePasswordLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="login" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit"
                                                                        class="btn btn-default fa fa-search"
                                                                        onclick="readChangePasswordLogs()"></button>
                                                            </div>


                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control"
                                                                       id="ChangePasswordtextSearch" type="text"
                                                                       placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofChangePasswordLogs">
                                            </div>
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                </div>
            </div>
        </div>

    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script>

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");// activated tab

            switch (target) {
                case "#loginLogs":
                    readLoginLogs();
                    break;

                case "#SignupLogs":
                    readSignupLogs();
                    break;

                case "#VerifyUserLogs":
                    readVerifyUserLogs();
                    break;

                case "#ResendActivation":
                    readResendActivationLogs();
                    break;

                case "#forgotPasswdLogs":
                    readforgotpasswordLogs();
                    break;

                case "#ChangePassword":
                    readChangePasswordLogs();
                    break;

            }
        });

        $(document).ready(function () {
            readLoginLogs();
        });

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();

        /*********************************** login *************************************/
        $('#loginLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);
        });


        $('#loginLogsDateRangePiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#loginLogsDateRangePiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#loginLogsDateRangePiker').on('drop', function(event) {
            event.preventDefault();
        });

        function readLoginLogs() {

            var datecapctured = $("#loginLogsDateRangePiker").val();
            var datedata = datecapctured.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadLoginLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofLoginLogs").html("");
                    $("#dataofLoginLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }
        /*********************************** login *************************************/


        /*********************************** signup *************************************/
        $('#signupLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);

        });
        $('#signupLogsDateRangePiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#signupLogsDateRangePiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#signupLogsDateRangePiker').on('drop', function(event) {
            event.preventDefault();
        });
        function readSignupLogs() {

            var datecapctured = $("#signupLogsDateRangePiker").val();
            var datedata = datecapctured.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadSignupLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofSignupLogs").html("");
                    $("#dataofSignupLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }

        /*********************************** signup *************************************/

        /*********************************** verify user *************************************/
        $('#VerifyUserLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);

        });
        $('#VerifyUserLogsDateRangePiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#VerifyUserLogsDateRangePiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#VerifyUserLogsDateRangePiker').on('drop', function(event) {
            event.preventDefault();
        });
        function readVerifyUserLogs() {

            var datecapctured = $("#VerifyUserLogsDateRangePiker").val();
            var datedata = datecapctured.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadVerifyUserLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofVerifyUserLogs").html("");
                    $("#dataofVerifyUserLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }
        /*********************************** verify user *************************************/

        /*********************************** resend activation *************************************/
        $('#ResendActivationLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);

        });
        $('#ResendActivationLogsDateRangePiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#ResendActivationLogsDateRangePiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#ResendActivationLogsDateRangePiker').on('drop', function(event) {
            event.preventDefault();
        });
        function readResendActivationLogs() {

            var datecapctured = $("#ResendActivationLogsDateRangePiker").val();
            var datedata = datecapctured.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadResendActivationLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofResendActivationLogs").html("");
                    $("#dataofResendActivationLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }

        /*********************************** resend activation *************************************/


        /*********************************** change password  *************************************/
        $('#ChangePasswordLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);

        });
        $('#ChangePasswordLogsDateRangePiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#ChangePasswordLogsDateRangePiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#ChangePasswordLogsDateRangePiker').on('drop', function(event) {
            event.preventDefault();
        });
        function readChangePasswordLogs() {

            var datecapctured = $("#ChangePasswordLogsDateRangePiker").val();
            var datedata = datecapctured.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadChangePasswordLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofChangePasswordLogs").html("");
                    $("#dataofChangePasswordLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }


        /*********************************** change password  *************************************/


        /*********************************** forgot password  *************************************/
        $('#forgotpassworddatepiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);


        });
        $('#forgotpassworddatepiker').keydown(function() {
            //code to not allow any changes to be made to input field
            return false;
        });
        $('#forgotpassworddatepiker').bind("cut copy paste",function(e) {
            e.preventDefault();
        });

        $('#forgotpassworddatepiker').on('drop', function(event) {
            event.preventDefault();
        });

        function readforgotpasswordLogs() {

            var forgotpassworddatepiker = $("#forgotpassworddatepiker").val();
            var datedata = forgotpassworddatepiker.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = "";
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReadForgotPasswordLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#forgotPasswordLogs").html("");
                    $("#forgotPasswordLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }

        /*********************************** forgot password  *************************************/
    </script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>