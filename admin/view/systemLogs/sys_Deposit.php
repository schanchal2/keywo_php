<?php include "../layout/header.php";
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
checkGroupAccess();
?>
    <style>
        .LogAct {

            display: block;
        }

        #debitPanel tr:last-child {
            background: #fff;
            /*color: #333;*/
        }

        #debitPanel tr:last-child td {
            /*background: #fff;*/
            color: #333;
        }

    </style>
    <link rel="stylesheet"
          href="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.css">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/dp-range/daterangepicker.min.css" rel="stylesheet">
    <link href="<?php echo $adminRoot; ?>frontend_libraries/jquery/jquery-ui.min.css" rel="stylesheet">
    <main>
        <div class="container-fluid">
            <div class="title clearfix">
                <h1 class="pull-left">System Logs</h1><span><?= $adminCurrency; ?> Purchase</span>
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                </form>
            </div>

            <br/>

            <div class="wrapper">
                <div class="row">
                    <div class="col-md-12">
                        <!-- Custom Tabs -->
                        <div class="nav-tabs-custom">
                            <ul class="nav nav-tabs" style="background: #35ccf1;">
                                <li class="active"><a href="#DepositLogs" data-toggle="tab"><?= $adminCurrency; ?> Purchase Logs</a></li>
                            </ul>
                            <div class="tab-content" style="border: solid 1px #bdeefa;padding:10px;">
                                <div class="tab-pane active" id="DepositLogs">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="callout callout" style="background-color: #D9EDF8;padding: 5px;">
                                                <form class="form-inline" name="itdPurchaseformexport" method="post"
                                                      action="javascript:;">
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                    <input type="text" placeholder="Select Date Range"
                                                                           class="form-control" id="itdPurchaseLogsDateRangePiker"
                                                                           name="itdPurchaseLogsDateRangePiker"
                                                                           value="<?php echo date("Y-m-d") . " to " . date("Y-m-d"); ?>"
                                                                           autocomplete="off" size="23">
                                                                </div><!-- /.input group -->
                                                            </div><!-- /.form group -->

                                                            <input type="text" value="trade" name="type" hidden="">
                                                            <div class="form-group">
                                                                <button type="submit" class="btn btn-default fa fa-search"
                                                                        onclick="readitdPurchaseLogs()"></button>
                                                            </div>

                                                            <div class="input-group styled-select">
                                                                <select id="itdPurchase_by">
                                                                    <option value="itdPurchase_by_paypal"> Paypal</option>
                                                                    <option value="itdPurchase_by_bitgo"> Bitgo</option>
                                                                </select>
                                                                <span class="input-group-addon"><i class="fa fa-angle-down"></i>  </span>

                                                            </div>




                                                        </div>
                                                        <div class="form-group col-sm-3" style="float:right">
                                                            <div class="">
                                                                <label>Filter:</label>
                                                                <input class="form-control" id="itdPurchasetextSearch" type="text"
                                                                       placeholder="Search Logs">
                                                            </div>
                                                        </div>
                                                    </div>


                                                </form>
                                            </div>

                                            <div id="dataofitdPurchaseLogs">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-content -->
                        </div>
                        <!-- nav-tabs-custom -->
                    </div>
                </div>
            </div>
        </div>

    </main>
    </div>
    <!-- praposed to remove from here and place in "footer.php"   -->
<?php include "{$docRootAdmin}view/layout/transperent_footer.php" ?>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/moment.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/dp-range/date.js"></script>
    <script>

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href");// activated tab

            switch (target) {
                case "#DepositLogs":
                    readitdPurchaseLogs();
                    break;
            }
        });

        $(document).ready(function () {
            readitdPurchaseLogs();
        });

        var todaysdate = new Date();
        var threeMonthsAgo = (3).months().ago();
        /****************************************** ITD PURCHASE ****************************************/
        $('#itdPurchaseLogsDateRangePiker').dateRangePicker({
            autoClose: true,
            endDate: moment().startOf('day').toDate(),
            maxDays: 90,
        }).bind('datepicker-first-date-selected', function (event, obj) {
        }).bind('datepicker-change', function (event, obj) {
            var newdate = (obj.value);


        });

        function readitdPurchaseLogs() {

            var itdPurchaseLogsDateRangePiker = $("#itdPurchaseLogsDateRangePiker").val();
            var datedata = itdPurchaseLogsDateRangePiker.split("to");
            var fromdate = (datedata[0]).trim();
            var todate = (datedata[1]).trim();

            var userEmail = $("#userEid").val();
            var itdPurchase_by = $("#itdPurchase_by").val();
            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../controller/logReaders/ReaditdPurchaseLogs.php",
                data: {
                    fromdate: fromdate, todate: todate, userEmail: userEmail,itdPurchase_by:itdPurchase_by
                },
                beforeSend: function(){
                    $('#loadng-image').show();
                },
                success: function (data) {
                    $('#loadng-image').hide();
                    $("#dataofitdPurchaseLogs").html("");
                    $("#dataofitdPurchaseLogs").html(data);
                },
                error: function () {
                    alert("fail");
                }
            });
        }

        /****************************************** ITD PURCHASE  ****************************************/
    </script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo $adminRoot; ?>frontend_libraries/plugins/datatables/dataTables.bootstrap.min.js"></script>
<?php include "{$docRootAdmin}view/layout/footer.php" ?>