jQuery(document).ready(function($) {
    $(document).ajaxSuccess(function(event, request, settings) {
        // $("#msg").append("<li>Successful Request!</li>");

        equalizeSidebar();

    });

    /*===========================================================================
    =            to change sidebar menu dropdown icon on interaction            =
    ===========================================================================*/
    if (!($('.sidbarWrapper').hasClass('in'))) {
        $('#sidebar').on('shown.bs.collapse', function() {
            var list = $(this).find('> li');
            // console.log(list);
            list.each(function(index, el) {
                // console.log(el);
                if (!($(el).find('a').hasClass('collapsed'))) {
                    // console.log(el);
                    $(el).find('i.fa-angle-right').toggleClass('fa-angle-down').toggleClass('fa-angle-right');
                }
            });
        });
        $('#sidebar').on('hidden.bs.collapse', function() {
            // console.log(this); // #sidebar
            var list = $(this).find('> li');
            // console.log(list);
            list.each(function(index, el) {
                // console.log(el);
                if (($(el).find('a').hasClass('collapsed'))) {
                    // console.log(el);
                    $(el).find('i.fa-angle-down').toggleClass('fa-angle-down').toggleClass('fa-angle-right');
                }
            });
        });
    }

    /*=====  End of to change sidebar menu dropdown icon on interaction  ======*/

    /*======================================
    =            hiding sidebar            =
    ======================================*/
    $('.sidebar-toggler').on('click', function(event) {
        //event.preventDefault();
        $('.sidbarWrapper').toggleClass('in');
        $('main').toggleClass('in');
        console.log(this);
        $(this).find('.fa-inverse').toggleClass('fa-angle-double-left').toggleClass('fa-angle-double-right');

        /*======================================================================
        =            to dissable dropdown when only icon is visible            =
        ======================================================================*/
        // console.log();
        // if($('.sidbarWrapper').hasClass('in')){
        //     console.log('Class "in" Present');
        //     //$('.sidbarWrapper div').attr('data-toggle', 'collapse');

        // }else{
        //     console.log('Class "in" Removed');
        //     //$(el).attr('data-toggle', '');
        // }

        if (!($('.sidbarWrapper').hasClass('in'))) {
            // when we open
            $('.sidbarWrapper a[role]').each(function(index, el) {
                // $(el).data( { "toggle": "aValue" } );
                $(el).attr('data-toggle', 'collapse');



                // console.log(index + $(el).data("toggle"));
                // console.log($(el));
                // console.log(index + $(el).data("toggle"));
                $(el).find('span').find('ul').remove();
            });
        } else {
            // when we close
            $('.sidbarWrapper div[role="tabpanel"]').removeClass('in'); //will collaps open side submenu
            $('.sidbarWrapper .panel>a').each(function(index, el) {
                // $(el).data( { "toggle": "aValue" } );
                $(el).attr('data-toggle', '');
                // console.log(index + $(el).data("toggle"));
                // $(el).find('span').append($(el).next().find('ul'));
                $(el).next().find('ul').clone().appendTo($(el).find('span'));
            });
        }

        /*=====  End of to dissable dropdown when only icon is visible  ======*/

    });

    /*=====  End of hiding sidebar  ======*/




    // $('#sidebar a[data-value]').on('click', function(event) {
    //     event.preventDefault();
    //     /* Act on the event */
    //     // console.log($(this).data('value'));
    //     var sPageName = $(this).data('value');
    //     // $("main").html("sdgsduhsdhdghdhdjhghdjdjhfgdgdugdgh");
    //
    //
    //     $.get(sPageName, function(data) {
    //         $("main").html(data);
    //         // alert("Load was performed.");
    //     });
    //
    // });





    /*===================================================================
    =            //to equalize height of sidebar with main .            =
    ===================================================================*/
    equalizeSidebar();
    $(window).resize(function(event) {
        /* Act on the event */
        equalizeSidebar();

    });

    function equalizeSidebar() {

        // var navbarHeight = $('nav.navbar-default').height();
        // console.log(navbarHeight);

        $("aside").height($("main").height());


        // console.log( $("main").height() - navbarHeight  );
        // console.log("equalizeSidebar()");
    }
    /*=====  End of //to equalize height of sidebar with main .  ======*/




    /*==================================================
    =            to add 'active' class to $("#userDebitDetails table td ")             =
    ==================================================*/

    $('#CurrencySlab').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        // console.log("changed Currency");
        // console.log($(this).val());
        $('#inputPriceIn').attr('placeholder', 'Enter price for Slab in ' + $(this).val());
        // console.log( $("lable[for='inputPriceIn']"));
        $("[for='inputPriceIn']").text('Price of Slab in ' + $(this).val());
    });


    /*===========================================================
    =            to change pacehoder with user input            =
    ===========================================================*/



    /*=====  End of to change pacehoder with user input  ======*/



    //  $('#userDebitDetails a[data-value]').on('click', function(event) {
    //     event.preventDefault();
    //     console.log($(this).data('value'));
    //     $(this).parent().addClass('active').next().addClass('active');
    //     var sPageName = $(this).data('value');
    //     $.get(sPageName, function(data) {
    //         $("#debitPanel").html(data);
    //     });
    // });


    /*=====  End of to add 'active' class to $("#userDebitDetails table td ")   ======*/

    /*==========================================================
    =            http://pasteboard.co/tsZKpLBOi.jpg            =
    ==========================================================*/

    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });




    /*=====  End of http://pasteboard.co/tsZKpLBOi.jpg  ======*/




});
/*========================================================
=            to count characters in text area            =
========================================================*/

function countChar(val, charLenght, indicator) {
    // console.log(val);
    // console.log(charLenght);
    // console.log(indicator);
    var len = val.value.length;
    if (len > charLenght) {
        val.value = val.value.substring(0, charLenght);
    } else {
        // $('#charNum').text(10 - len);
        $(indicator).text(charLenght - len);
    }
};

/*=====  End of to count characters in text area  ======*/

/*==========================================
=            reset select input            =
==========================================*/
/**
 * will reset select tag 
 * @param  {[string]} selectTagId [css selector as an argument]
 */
function resetSelectTag(selectTagId, resetTO) {
    $(selectTagId + ' :disabled').removeAttr('disabled');
    $(selectTagId + ' :selected').removeAttr('selected');
    // console.log("resetTO :",resetTO, ", typeof resetTO :", typeof resetTO )
    if ((resetTO != undefined ) || (resetTO == " ")) {
        // console.log("is undefined")
         $(selectTagId + ' [value="' + resetTO+'"]').prop('selected', 'selected')
    }
    $(selectTagId + ' :selected').attr('disabled', 'disabled');
}
/*=====  End of reset select input  ======*/


function showGIFDataDOTload() {
    var host = window.location.href;
    var hostnew = host.split("admin/");
    var showGIF = "<div><img src='" + hostnew[0] + "admin/frontend_libraries/img/loading.gif'></img></div>";
    return showGIF;
}

function showLoadingDiv() {
    var showdiv = '<div id="loadng-image"><center><div style="background: lightgoldenrodyellow;height:40px;width:200px;text-align: center;padding:2px;border: solid 2px yellow;    position: fixed;top: 76px;left: 50%;"> <h5><b>Please wait Loading...</b></h5> </div></center> </div>';
    return showdiv;
}

$("#hit_admin_settings").click(function() {
    $("#hit_settings").trigger("click");
});

function bsAlert(type, msg) {
    return '<div class="alert alert-' + type + '"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + msg + '</div>';
}

function clearAlertError()
{
    $("#errors") .html("");
}
