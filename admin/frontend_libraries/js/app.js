function previewImage(fileId, imageHolderId, cssThumbClsss) {

    $("#" + imageHolderId).html("");
    /*var file= $("#"+fileId+"")[0];
     //Get count of selected files
     var countFiles = file.files.length;
     var imgPath = file.value;
     var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
     var image_holder = $("#"+imageHolderId+"");
     image_holder.empty();
     if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
     if (typeof(FileReader) != "undefined") {
     //loop for each file selected for uploaded.
     for (var i = 0; i < countFiles; i++)
     {
     var reader = new FileReader();
     reader.onload = function(e) {
     $("<img />", {
     "src": e.target.result,
     "class": cssThumbClsss
     }).appendTo(image_holder);
     }
     image_holder.show();
     reader.readAsDataURL(file.files[i]);
     }
     } else {
     alert("This browser does not support FileReader.");
     }
     } else {
     alert("Pls select only images");
     }*/


    var fileUpload = $("#" + fileId + "")[0];
    var image_holder = $("#" + imageHolderId + "");
    //Check whether the file is valid Image.
    var countFiles = fileUpload.files.length;
    var imgPath = fileUpload.value;
    var extn = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
        //Check whether HTML5 is supported.
        if (typeof (fileUpload.files) != "undefined") {
            //Initiate the FileReader object.
            var reader = new FileReader();
            //Read the contents of Image File.
            reader.readAsDataURL(fileUpload.files[0]);
            reader.onload = function (e) {
                //Initiate the JavaScript Image object.
                var image = new Image();
                //Set the Base64 string return from FileReader as source.
                image.src = e.target.result;
                image.onload = function () {
                    //Determine the Height and Width.
                    var height = this.height;
                    var width = this.width;
                    if (height > 420) {
                        //alert("Height and Width must not exceed 377px.");
                        $("#errMsgFailedc").html("");
                        $("#errMsgSuccessc").html("");
                        $("#errMsgFailedc").html("Height and Width must not exceed 420px.");

                        resetFields(imageHolderId);
                        return false;
                    } else if (width > 320) {
                        //alert("Width must not exceed 296px.");
                        $("#errMsgFailedc").html("");
                        $("#errMsgSuccessc").html("");
                        $("#errMsgFailedc").html("Width must not exceed 320px.");
                        resetFields(imageHolderId);
                        return false;
                    } else if (height < 420) {
                        //alert("Height and Width must not less than 377px.");
                        $("#errMsgFailedc").html("");
                        $("#errMsgSuccessc").html("");
                        $("#errMsgFailedc").html("Height and Width must not less than 420px.");
                        resetFields(imageHolderId);
                        return false;
                    } else if (width < 320) {
                        //alert("Width must not less than 296px.");
                        $("#errMsgFailedc").html("");
                        $("#errMsgSuccessc").html("");
                        $("#errMsgFailedc").html("Width must not less than 320px.");
                        resetFields(imageHolderId);
                        return false;
                    }
                    // alert("Uploaded image has valid Height and Width.");
                    $("#errMsgSuccessc").html("");
                    $("#errMsgFailedc").html("");
                    $("#errMsgSuccessc").html("Its a Valid Image.");
                    $("<img />", {
                        "src": e.target.result,
                        "class": cssThumbClsss
                    }).appendTo(image_holder);
                    return true;
                };
            }
        } else {
            resetFields(imageHolderId);
            alert("This browser does not support HTML5.");
            return false;
        }
    } else {
        resetFields(imageHolderId);
        alert("There was an upload error.Make sure to upload a JPG,JPEG or PNG file and try again");
        return false;
    }
}

function resetFields(imageHolderId) {
    if (imageHolderId == "consumerImageHolder") {
        $("#consumerImage").val("");
    } else {
        $("#uploaderImage").val("");
    }
}