function validateLoginDetails(url) {

    var email = document.forms["loginform"]["username"].value;
    var passhash = document.forms["loginform"]["password"].value;

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/login_controller.php",
        data: {
            username: email, password: passhash
        },
        success: function (user) {

            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errorid").innerHTML = msg;

                }
                else {
                    window.location.href = url;
                }
            }

        },
        error: function () {
            alert("Failed to Login please try again.");
        }
    });

}


function resetAdminPassword() {
    var emails = document.forms["loginform1"]["username"].value;
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            email: emails, type: "resetpassword"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg").innerHTML = msg;
                    document.getElementById("errmsgsuc").innerHTML = "";


                }
                else {
                    document.getElementById("errmsg").innerHTML = "";
                    document.getElementById("errmsgsuc").innerHTML = msg;
                    document.getElementById("loginform1").reset();
                    $(".ajaxhide").attr('disabled', false);

                    $("#editnewadmindataadminrights").modal("hide");


                }
            }

        },
        error: function () {
            alert("Failed to Reset Password please try again.");
        }
    });

}


$(".cleanLabel").on('click', function () {
    $("#errmsgsuc").html("");
});