function changeProfilePic() {
    $("#changenewprofile").click();
}

$(document).ready(function () {
    $('#changenewprofile').change(function () {
        $('#form_uploadpic').trigger('submit');
    });
});

function popupCenter(url, title, w, h) {
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

// Encode/decode htmlentities
function krEncodeEntities(s){
    return $("<div/>").text(s).html();
}
function krDencodeEntities(s){
    return $("<div/>").html(s).text();
}
