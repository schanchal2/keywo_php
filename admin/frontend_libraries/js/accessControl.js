/*

 Created By:Dinesh Ghule
 Dependent Module :Admin Access Control
 Usages of this Utility :

 This JS is used for

 Interaction between Front End to ../controller


 */

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href");// activated tab
});

$(function () {
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');

    $('.nav-tabs a').click(function (e) {
        $(this).tab('show');
        var scrollmem = $('body').scrollTop() || $('html').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });
});

function popupCenter(url, title, w, h) {

    /*

     open center Dialog
     url-url of page to open in dialog
     title-title of dialog
     w-eidth of dialog
     h-height of dialog

     */
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=yes, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function submitUserdata() {

    var email = document.forms["addstaff"]["email"].value;
    var fname = document.forms["addstaff"]["fname"].value;
    var lname = document.forms["addstaff"]["lname"].value;
    // var group= document.forms["addstaff"]["group"].value;
    var tags = document.getElementsByName('groups[]');

    var permissions = new Array();
    for (var i = 0; i < tags.length; ++i) {
        //alert(tags[i].value);
        if (tags[i].checked) {
            permissions.push(tags[i].value);
        }

    }


    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            email: email, fname: fname, lname: lname, group: permissions, type: "addnew"
        },

        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg4").innerHTML = msg;
                    document.getElementById("errmsgsuc4").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false);
                }
                else {

                    document.getElementById("errmsg4").innerHTML = "";
                    document.getElementById("errmsgsuc4").innerHTML = msg;
                    document.getElementById("addstaff").reset();

                    location.reload();

                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to add new admin</h4>", function () {

            });

        }
    });

}

$('#basicModal').on('hidden.bs.modal', function () {
    document.getElementById("addstaff").reset();


})
$('#basicModal2').on('hidden.bs.modal', function () {
    document.getElementById("addgroup").reset();

})
$('#basicModal3').on('hidden.bs.modal', function () {
    document.getElementById("addip").reset();

})
function deleteAdmin(email) {


    bootbox.confirm({
        title: 'Delete User!!',
        message: "Are you sure delete user " + email + "!!",
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default'
            },
            'confirm': {
                label: 'Continue',
                className: 'btn-success pull-right'
            }
        },
        callback: function (result) {

            if (result) {
                $.ajax({

                    type: "POST",
                    dataType: "json",
                    url: "../../controller/acl/admin_controller.php",
                    data: {
                        email: email, type: "deleteadmin"
                    },
                    success: function (user) {

                        if (user["errCode"]) {
                            var msg = user["errMsg"];

                            if (user["errCode"] == "5") {
                                var red = user["returl"];

                                window.location = red;
                            }
                            if (user["errCode"] != "-1") {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {

                                });
                            }
                            else {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {
                                    location.reload();
                                });
                            }
                        }

                    },
                    error: function () {

                        bootbox.alert("<h4>Failed to  delete admin</h4>", function () {

                        });

                    }
                });

            }
        }
    });


}

function deleteip(address, id) {


    bootbox.confirm({
        title: 'Delete IP Address!!',
        message: "Are you sure delete IP Address: " + address + "!!",
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default'
            },
            'confirm': {
                label: 'Continue',
                className: 'btn-success pull-right'
            }
        },
        callback: function (result) {

            if (result) {
                $.ajax({

                    type: "POST",
                    dataType: "json",
                    url: "../../controller/acl/admin_controller.php",
                    data: {
                        id: id, type: "deleteip"
                    },
                    success: function (user) {

                        if (user["errCode"]) {
                            var msg = user["errMsg"];
                            if (user["errCode"] != "-1") {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {

                                });
                            }
                            else {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {
                                    location.reload();
                                    window.location.hash = "#manage_ip";
                                });
                            }
                        }

                    },
                    error: function () {

                        bootbox.alert("<h4>Failed to  delete ip address</h4>", function () {

                        });

                    }
                });

            }
        }
    });


}


function editAdmin(email, msg) {


    $.ajax({
        type: 'POST',
        url: '../../controller/acl/admin_controller.php',
        dataType: 'html',
        data: {
            email: email, type: 'editadmin'
        },
        success: function (data) {

            // alert(data);
            $("#editnewadmindata").html('');
            $("#editnewadmindata").html(data);
            if (msg != undefined) {
                document.getElementById("errmsg1").innerHTML = "";
                document.getElementById("errmsgsuc1").innerHTML = msg;
            }

        }
    });
    return false;
}

function editGroup(id) {
    $.ajax({
        type: 'POST',
        url: '../../controller/acl/admin_controller.php',
        dataType: 'html',
        data: {
            groupid: id, type: 'editgroup'
        },
        success: function (data) {

            //alert(data);
            $("#editnewadmindatagroup").html('');
            $("#editnewadmindatagroup").html(data);
        }
    });
    return false;
}


function changeRights(email, msg) {
    $.ajax({
        type: 'POST',
        url: '../../controller/acl/admin_controller.php',
        dataType: 'html',
        data: {
            email: email, type: 'changeRights'
        },
        success: function (data) {

            // alert(data);
            $("#editnewadmindatarights").html('');
            $("#editnewadmindatarights").html(data);
            if (msg !== undefined) {
                document.getElementById("errmsg1").innerHTML = "";
                document.getElementById("errmsgsuc1").innerHTML = msg;
            }
        }
    });
    return false;
}

function submitUserdataupdate(email) {

    var fname = document.forms["addstaff1"]["fname"].value;
    var lname = document.forms["addstaff1"]["lname"].value;
    var tags = document.getElementsByName('groups[]');

    var permissions = new Array();

    for (var i = 0; i < tags.length; ++i) {
        //alert(tags[i].value);
        if (tags[i].checked) {
            permissions.push(tags[i].value);
        }

    }


    var tags1 = document.getElementsByName('permsissions[]');

    var permissions1 = new Array();

    for (var i = 0; i < tags1.length; ++i) {
        //alert(tags[i].value);
        if (tags1[i].checked) {
            permissions1.push(tags1[i].value);
        }

    }


    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            email: email,
            fname: fname,
            lname: lname,
            group: permissions,
            userpermission: permissions1,
            type: "updateuser"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg1").innerHTML = msg;
                    document.getElementById("errmsgsuc1").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false); // disable button
                }
                else {

                    document.getElementById("errmsg1").innerHTML = "";
                    document.getElementById("errmsgsuc1").innerHTML = msg;
                    document.getElementById("addstaff1").reset();
                    // document.getElementById("useradmionburron").disabled = false;
                    editAdmin(email, msg);

                    location.reload();
                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to update admin information</h4>", function () {

            });

        }
    });

}

function submitUserdatagroup() {

    var groupname = document.forms["addgroup"]["groupname"].value;
    // var modulename= document.forms["addgroup"]["modulename"].value;
    var tags = document.getElementsByName('permsissions[]');

    function getModules() {
        var select1 = document.getElementById("modulename");
        var selected1 = [];
        for (var i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) selected1.push(select1.options[i].value);
        }
        return selected1;
    }

    function getSubModules() {
        var select1 = document.getElementById("submodulename");
        if (select1 != null) {
            var selected1 = [];
            for (var i = 0; i < select1.length; i++) {
                if (select1.options[i].selected) selected1.push(select1.options[i].value);
            }
            return selected1;
        }
    }

    var modulename = getModules();
    var submodulename = getSubModules();
    //alert(modulename);


    var permissions = new Array();
    for (var i = 0; i < tags.length; ++i) {
        //alert(tags[i].value);
        if (tags[i].checked) {
            permissions.push(tags[i].value);
        }

    }
    //alert(permissions);
    $.ajax({

        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            groupname: groupname,
            modulename: modulename,
            submodulename: submodulename,
            permissions: permissions,
            type: "addgroup"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },

        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg11").innerHTML = msg;
                    document.getElementById("errmsgsuc11").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false); // disable button
                }
                else {
                    document.getElementById("errmsg11").innerHTML = "";
                    document.getElementById("errmsgsuc11").innerHTML = msg;
                    document.getElementById("addgroup").reset();
                    // document.getElementById("useradmionburron").disabled = false;
                    // location.reload();
                    location.reload();
                    window.location.hash = "#manage_group";
                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to add new group</h4>", function () {

            });

        }
    });

}


function deleteGroup(id) {


    bootbox.confirm({
        title: 'Delete Group!!',
        message: "Are you sure delete group with ID " + id + "!!" +
        "<br/><span style='color:red'>Admin Associated with this group can't able to use this group further!!</span>",
        buttons: {
            'cancel': {
                label: 'Cancel',
                className: 'btn-default'
            },
            'confirm': {
                label: 'Continue',
                className: 'btn-success pull-right'
            }
        },
        callback: function (result) {

            if (result) {
                $.ajax({

                    type: "POST",
                    dataType: "json",
                    url: "../../controller/acl/admin_controller.php",
                    data: {
                        groupid: id, type: "deletegroup"
                    },
                    success: function (user) {

                        if (user["errCode"]) {
                            var msg = user["errMsg"];
                            if (user["errCode"] != "-1") {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {

                                });
                            }
                            else {
                                bootbox.alert("<h4>" + user["errMsg"] + "</h4>", function () {
                                    location.reload();
                                    window.location.hash = "#manage_group";
                                });
                            }
                        }

                    },
                    error: function () {

                        bootbox.alert("<h4>Failed to delete group</h4>", function () {

                        });

                    }
                });

            }
        }
    });


}


function submitUserRights(email) {


    var tags1 = document.getElementsByName('permsissions[]');

    var permissions1 = new Array();

    for (var i = 0; i < tags1.length; ++i) {
        //alert(tags[i].value);
        if (tags1[i].checked) {
            permissions1.push(tags1[i].value);
        }

    }
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            email: email, userpermission: permissions1, type: "editrights"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg1").innerHTML = msg;
                    document.getElementById("errmsgsuc1").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false); // disable button
                }
                else {
                    document.getElementById("errmsg1").innerHTML = "";
                    document.getElementById("errmsgsuc1").innerHTML = msg;
                    changeRights(email, msg);

                    location.reload();

                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to edit  admin right</h4>", function () {

            });

        }
    });

}


function submiteditedGroup(id) {


    var groupname = document.forms["editedgroup"]["groupname"].value;
    // var modulename= document.forms["addgroup"]["modulename"].value;
    var tags = document.getElementsByName('permsissionsedit[]');

    function getModules() {
        var select1 = document.getElementById("modulenameedit");
        var selected1 = [];
        for (var i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) selected1.push(select1.options[i].value);
        }
        return selected1;
    }

    function getSubModules() {


        var select1 = document.getElementById("submodulename");

        var selected1 = [];

        for (var i = 0; i < select1.length; i++) {
            if (select1.options[i].selected) selected1.push(select1.options[i].value);
        }

        return selected1;
    }

    var modulename = getModules();
    var submodulename = getSubModules();


    var permissions = new Array();
    for (var i = 0; i < tags.length; ++i) {
        //alert(tags[i].value);
        if (tags[i].checked) {
            permissions.push(tags[i].value);
        }

    }

    $.ajax({

        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            groupid: id,
            groupname: groupname,
            submodulename: submodulename,
            modulename: modulename,
            permissions: permissions,
            type: "editgroupnew"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg112").innerHTML = msg;
                    document.getElementById("errmsgsuc112").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false); // disable button
                }
                else {
                    document.getElementById("errmsg112").innerHTML = "";
                    document.getElementById("errmsgsuc112").innerHTML = msg;
                    location.reload();
                    window.location.hash = "#manage_group";

                    // document.getElementById("useradmionburron").disabled = false;
                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to edit group</h4>", function () {

            });

        }
    });

}


$('#modulename').on('change', function () {

    var optionSelected = $("option:selected", this);
    var len = optionSelected.length;
    var i;
    var allmodules = [];
    for (i = 0; i < len; i++) {
        var data = optionSelected[i].value;
        allmodules.push(data);
    }

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/acl/admin_controller.php",
        data: {
            allmodules: allmodules, type: "getsubmodules"
        },
        success: function (user) {

            $("#submodulesdata").html('');
            $("#submodulesdata").html(user);

        },
        error: function () {

            bootbox.alert("<h4>Failed to get Submodules</h4>", function () {

            });

        }
    });
});


$('#select_all').on('click', function () {
    if (this.checked) {
        $('.user_checkbox').each(function () {
            this.checked = true;
            $(this).closest('tr').toggleClass("highlight", this.checked);
        });
    } else {
        $('.user_checkbox').each(function () {
            this.checked = false;
            $(this).closest('tr').toggleClass("highlight", this.checked);
        });
    }
});

$('.user_checkbox').on('click', function () {
    if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
        $('#select_all').prop('checked', true);
        $(this).closest('tr').toggleClass("highlight", this.checked);
    } else {
        $('#select_all').prop('checked', false);
        $(this).closest('tr').toggleClass("highlight", this.checked);
    }
});





function submitaipddress() {
    var ipaddress = document.forms["addip"]["ipname"].value;
    //lert(ipaddress);

    $.ajax({
        type: "POST",
        dataType: "json",
        url: "../../controller/acl/admin_controller.php",
        data: {
            ipaddress: ipaddress, type: "addnewip"
        },
        beforeSend: function () {

            $(".ajaxhide").attr('disabled', true); // disable button
        },
        success: function (user) {


            if (user["errCode"]) {
                var msg = user["errMsg"];
                if (user["errCode"] != "-1") {
                    document.getElementById("errmsg").innerHTML = msg;
                    document.getElementById("errmsgsuc").innerHTML = "";
                    $(".ajaxhide").attr('disabled', false); // disable button
                }
                else {
                    document.getElementById("errmsg").innerHTML = "";
                    document.getElementById("errmsgsuc").innerHTML = msg;
                    document.getElementById("addip").reset();
                    // document.getElementById("useradmionburron").disabled = false;
                    location.reload();
                    window.location.hash = "#manage_ip";
                }
            }

        },
        error: function () {

            bootbox.alert("<h4>Failed to add new admin</h4>", function () {

            });

        }
    });

}


function editsubmodule() {

    var optionSelected = $("option:selected", "#modulenameedit");
    var len = optionSelected.length;
    var i;
    var allmodules = [];
    for (i = 0; i < len; i++) {
        var data = optionSelected[i].value;
        allmodules.push(data);
    }

    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/acl/admin_controller.php",
        data: {
            allmodules: allmodules, type: "getsubmodules"
        },
        success: function (user) {

            $("#submodulesdatanew").html('');
            $("#submodulesdatanew").html(user);

        },
        error: function () {

            bootbox.alert("<h4>Failed to get Submodules</h4>", function () {

            });

        }
    });
}

$(".cleanLabel").on('click', function () {
    $("#errmsg4").html("");
    $("#errmsgsuc4").html("");
    $("#errmsg1").html("");
    $("#errmsgsuc1").html("");
    $("#errmsg11").html("");
    $("#errmsgsuc11").html("");
    $("#errmsg112").html("");
    $("#errmsgsuc112").html("");
    $("#errmsg").html("");
    $("#errmsgsuc").html("");
});
$(function () {
    $('#IPAddressdata').DataTable({
        "bInfo": true,
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
    });


    $('#groupsAData').DataTable({
        "bInfo": true,
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": true,
        "autoWidth": true,
    });
});