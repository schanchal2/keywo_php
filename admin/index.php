<?php
session_start();

//start config
require_once('config/config.php');
require_once('config/db_config.php');
//end config

//start helper
require_once('helpers/coreFunctions.php');
require_once('helpers/arrayHelper.php');
//end helper

//other
require_once('core/errorMap.php');
require_once('model/acl/acl_model.php');
//end other

global $adminRoot;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$adminEmail=strtolower($_SESSION["admin_id"]);

$data=getAdminData($adminEmail, $connAdmin);
$dataEmail=strtolower($data["errMsg"]["admin_email"]);

if ($adminEmail==$dataEmail) {
    $url=$adminView."dashboard/dashboard.php";
    header("Location:{$url}");
}else
{
    $url=$adminView."acl/index.php";
    header("Location:{$url}");
}

?>