<?php
$rootUrl = "http://192.168.2.56/keywo/"; //Static
$docRoot = $_SERVER['DOCUMENT_ROOT'] . "/keywo/";
if($_SERVER['DOCUMENT_ROOT']==""){$docRoot= $_SERVER["PWD"]."/";}//CMD

$docRootAdmin = "{$docRoot}admin/";

$blanks    = array('', ' ', "", " ");
$mode      = "development"; //Static
$errorMode = "development"; // For error reporting log

/* for wallet */
$walletPrivateKey = "24eb6b033f8d23556a79880e05ed99289b71cc35b66f2567e0078da07ea1397051b901b5b93960cd4d24d6e23268ca0472f7fb41c7af1dd319f1f6397ecee088";
$walletPublicKey = "8b428ac0a0ae1be15a6e75d69fbc15a9129909ed261a1aeb4d1e087592659daa";


$walletURLIP = "http://192.168.2.134:5011/"; //Static
// $walletURLIP = "http://192.168.2.180:5600/"; //Static
//$walletURLIP = "http://192.168.2.122:5000/"; //Static

 $walletURLIPnotification = "http://192.168.2.134:4101/"; //Static
// $walletURLIPnotification = "http://192.168.2.180:4600/"; //Static
//$walletURLIPnotification = "http://192.168.2.122:4000/"; //Static


$walletURL                      = "{$walletURLIP}api/v3/";
$walletURLnotificationServer    = "{$walletURLIPnotification}api/notify/v2/"; //Static
$mongoServerSocial              = "{$walletURLIPnotification}social/v1/"; //Static
$NotificationURL                = $walletURLIPnotification."api/notify/";
$walletURLnotification          = "{$walletURLIPnotification}stnotify/bypreferences/";
$preference_code_Notification_2 = 2;
/* end for wallet */

/* for admin section */
$adminRoot = "{$rootUrl}admin/";
$adminView = "{$adminRoot}view/";
$category_Notification_admin_support = "admin_support";

$inactive = 1200000;
$checkSession = 30000;
$ApachePhpPort = 80;
$adminTimezone="Asia/Kolkata";

/* end for admin section */


/***************Log Path Array *****************/
$logPath["admin"]           = $docRootAdmin . "logs/acl/";
$logPath["newsArchive"]     = $docRootAdmin . "logs/cronLogs/cronNews";
$logPath["analyticsCron"]   = $docRootAdmin."logs/cronLogs/analyticsCron/";
$logPath["userManagement"]  = $docRoot."logs/UserMgmt/";
$logPath["social"]          = $docRoot."logs/social/";
$logPath["keywordPurchase"] = $docRoot."logs/keyword/purchase/";
$logPath["keywordBid"]      = $docRoot."logs/keyword/bid/";
$logPath["wallet"]          = $docRoot."logs/wallet/";
$logPath["security"]        = $docRoot."logs/security/";
/***************************************/

$rootUrlImages = "{$rootUrl}images/";
$rootUrlJs = "{$rootUrl}js/";
$rootUrlCss = "{$rootUrl}css/";

/* for emails */
$paymentEmail = "payment@keywo.com";
$noReplyEmail = "noreply@keywo.com";
/* end for emails */

/* special keywo users*/
$communityPoolUser   = "communitypool@keywo.com";
$keywoUser           = "keywo@keywo.com";
$appDeveloperUser    = "appdeveloper@keywo.com";
$unownedKwdOwnerUser = "unownedkwdower@keywo.com";
$maintenanceUser     = "maintenance@keywo.com";
$defaultAdmin        = "admin@keywo.com";
/* end */

/********** for support admin**************/
$adminCurrency     = "Credit";
$chatRefreshTime   = 10000;
$agentsGroupCS     = "csagent";
$hodGroupCS        = "cshod";
$superAdminGroupCS = "SuperAdmin";

$coagent           = "coagent";
$cohod             = "cohod";
$comm              = "comm";
$cosm              = "cosm";
/********** for support admin**************/


/************kyc*****************************/
$kycUploadDirectoy = "{$rootUrl}kyc_docs/";
/************kyc*****************************/

/****************************************************/
define("MAILGUN_API_KEY", "key-2b8f2419e616db09b1297ba51d7cc770");
define("MAILGUN_DOMAIN", "searchtrade.com");
define("DIR_ASSETS_EMAIL_TEMPLATES", "{$docRootAdmin}email_template/");
/****************************************************/

/***********************************************************************************/
$sendersNews=array("admin@keywo.com"=>"Keywo Admin","support@keywo.com"=>"Keywo Support","payments@keywo.com"=>"Keywo Payments");
/***********************************************************************************/

/******************************** admin groups defined********************************/
$adminsDefined=array("SuperAdmin","csagent","cshod","sragent","srhod","cohod","coagent","MM","SM","TrackingMonitoring");
/**************************************************************************************/




if ($errorMode != "production") {
    ini_set('display_errors', 1);
    error_reporting(E_ALL);
}else
{
    ini_set('display_errors', 0);
    error_reporting(0);
}

if ( date_default_timezone_set( "UTC" ) != TRUE)
{
    $now = new DateTime();
    $now->setTimezone(new DateTimeZone('UTC'));
}

?>

