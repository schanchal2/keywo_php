<?php


$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/helpers/stringHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/manageUser/userManagement_analytics.php");
require_once("$docrootpath/model/tracking/intraction/intraction_analytics.php");
require_once("$docrootpath/model/keywords/analytics/daily_keyword_analytics.php");
require_once("$docrootpath/model/tracking/userwise/common.php");

$dbkeywords = createDBConnection("dbkeywords");
noError($dbkeywords) ? $dbkeywords = $dbkeywords["connection"] : $error = $dbkeywords["errMsg"];

$dbsearch = createDBConnection("dbsearch");
noError($dbsearch) ? $dbsearch = $dbsearch["connection"] : $error = $dbsearch["errMsg"];

if ($_GET) {
    $current_year = $_GET["year"];
} else {
    $current_year = date("Y", strtotime("+1 year"));
}

for ($i = 1; $i <= 12; $i++) {
    if (strlen($i) == 1) {
        $i = "0" . $i;
    }

    createTable_keywo_UAM_analytics($current_year, $i, $dbsearch);
    createTable_keywo_Referral_analytics($current_year, $i, $dbsearch);
    createTable_keywo_intraction_analytics($current_year, $i, $dbsearch);
    createTable_keywo_userEarning_analytics($current_year, $i, $dbsearch);
    createTable_daily_keyword_earnings($current_year, $i, $dbkeywords);
//    createTable_daily_TOP1000_Referrer($current_year, $i,"Referrer", $dbsearch);
//
//    $EarningType=explode(",","post_view_earnings,post_creator_earning,keyword_ownership_earning_search,keyword_ownership_earning_social,search_earning,app_earning,post_sharer_earning,keyword_affiliate_earning");
//
//    foreach($EarningType as $earning)
//    {
//        createTable_daily_TOP1000_UsersEarnings($current_year, $i, $earning, $dbsearch);
//    }
//
//    createTable_daily_TOP1000_UsersEarnings($current_year, $i, "TopTotalEarnersSearchSocial", $dbsearch);
}


?>
