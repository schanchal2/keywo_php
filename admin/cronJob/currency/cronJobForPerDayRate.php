<?php
$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/model/currency/currency_rate_model.php");


$year = date("Y");
$month = date("m");
$date = date("d");

$logDir = $docrootpath."logs/cronLogs/cronNews/".$year . "/" . $month . "/" . $date . "/";
if (!is_dir($logDir)) {
    mkdir($logDir, 0777, true);
}


$URL = $logDir . "/every_day_price_list.txt";
$method = (file_exists($URL)) ? 'a' : 'w+';
$myfile = fopen($URL, $method) or die("Unable to open file!");
$txt = "***********************************************************************************\n\n";
$txt .= "Cron DATE and Time \t" . date('Y-m-d H:i:s') . "\n\n";


$connKeywords = createDBConnection("dbkeywords");
if (noError($connKeywords)) {
    $connKeywords = $connKeywords["connection"];
    $txt .= "Database Connection established\n\n";

    $returnArr = array();

    $rateforusd = getCurrencyRate("usd");
    $txt .= "USD Rates Today:" . $rateforusd . "\n\n";

    $rateforsgd = getCurrencyRate("sgd");
    $txt .= "SGD Rates Today:" . $rateforsgd . "\n\n";

    $result = insertRatesRecords($rateforusd, $rateforsgd, $connKeywords);

    if (noError($result)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Rate Record Updated successs!";
        //echo json_encode($returnArr);
        $txt .= "Rate Record Updated successs!\n\n";
        echo "Rate Record Updated successs!";

    } else {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Record not updated error in transaction!";
        //echo json_encode($returnArr);
        $txt .= "Record not updated error in transaction!\n\n";
    }
} else {
    $txt .= "Database connection failure Keywords Database!\n\n";

}

$txt .= "***********************************************************************************\n\n\n";
fwrite($myfile, $txt);
fclose($myfile);

?>
