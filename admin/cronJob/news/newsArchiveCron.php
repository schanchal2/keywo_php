<?php

$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

//start config
require_once("{$docrootpath}config/config.php");

require_once("{$docrootpath}config/db_config.php");

require_once("{$docrootpath}helpers/coreFunctions.php");

require_once("{$docrootpath}helpers/email_helpers.php");
require_once("{$docrootpath}helpers/arrayHelper.php");

require_once("{$docrootpath}core/errorMap.php");
//To load RSS feeds.


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$logPath = $docrootpath . "logs/cronLogs/cronNews";
if ($connSearch) {

    $updateLog = updateLog($connSearch, $logPath);


    if (noError($updateLog)) {
        echo "Updating channels successfull.";
        printArr($updateLog["errMsg"]);

    } else {
        echo "Error in updating channels.";
        printArr($updateLog["errMsg"]);
    }
}

//Create or update log file.
//Create week wise tables for archived news records.
//Send mail if error occurs while archiving news records.
function updateLog($connSearch, $logPath)
{
    $extraArg = array();
    $returnArr = array();
    $month = date("MY");
    $logData = "";
    $logDir = "{$logPath}" . $month;
    if (!is_dir($logDir)) {
        mkdir($logDir, 0777, true);
    }
    $file = fopen("" . $logDir . "/" . date("d") . " log.txt", "a");
    $time1 = date("Y-m-d H:i:s");
    $logData .= "\n*********************************************************************************************\n";
    $logData .= "******************************CRON RUN TIME:" . $time1 . "******************************\n\n";

    $result = createTables($connSearch);
    if (noError($result)) {
        echo "Archiving news items successful.";
        $logData .= $result["logData"];
        $returnArr = setErrorStack($returnArr, -1, "Archiving news items successful", $extraArg);
    } else {
        echo "Error in archiving news records.";
        $message = "errMsg:" . $result['errMsg'] . " errCode:" . $result['errCode'] . "\n";
        //sendNewsMail($message);
        $logData .= $result["logData"];
        $returnArr = setErrorStack($returnArr, 59, null, $extraArg);
    }

    $logData .= "\n*********************************************************************************************\n";
    $logData .= "**************************************** *** END **** ***************************************\n\n ";
    fwrite($file, $logData);
    fclose($file);
    return $returnArr;
}

//Create week wise tables for archived news records.
//Move old news into into archive items and images tables.
//Delete old records from items and images tables.
function createTables($connSearch)
{
    $extraArg = array();
    $returnArr = array();
    $now = time();
    $daysAgo = 5;
    $sql = "Select items.*,images.image_id,images.url,images.`type`,images.width,images.height from images right join items ON images.item_id = items.item_id  Where (SELECT DATEDIFF( now(), items.publish_date)) > $daysAgo ";
    $resultQuery = runQuery($sql, $connSearch);

    $logData = "Checking old recoreds from items and images table completed. \n";

    $newsresult1 = array();
    while ($news = mysqli_fetch_assoc($resultQuery["dbResource"])) {
        array_push($newsresult1, $news);
    }
    foreach ($newsresult1 as $newsresult) {

        $publishDate = strtotime($newsresult["publish_date"]);
        $datediff = $now - $publishDate;
        $dayDiff = floor($datediff / (60 * 60 * 24));
        //if news record is older than 2 months.
        if ($dayDiff > 60) {
            continue;
        };
        $publishWeek = date("Y_m_W", strtotime($newsresult["publish_date"]));
        $news_archive_items = "news_archive_items_$publishWeek";
        $news_archive_images = "news_archive_images_$publishWeek";
        $sql = "SELECT 1 FROM $news_archive_items LIMIT 1";
        $resultQuery = runQuery($sql, $connSearch);


        if ($resultQuery["errCode"] == 3) {

            $sql = "CREATE TABLE $news_archive_items (
				item_id INT(11) NOT NULL,
				channel_id INT(11) NOT NULL,
				title VARCHAR(256) NULL DEFAULT NULL,
				description TEXT NULL,
				link VARCHAR(256) NULL DEFAULT NULL,
				source VARCHAR(256) NULL DEFAULT NULL,
				publish_date TIMESTAMP NULL DEFAULT NULL,
				PRIMARY KEY (item_id)
				) ENGINE=MyISAM ";

            $resultQuery = runQuery($sql, $connSearch);


            if (noError($resultQuery)) {

                $sql = "CREATE TABLE $news_archive_images (
					image_id INT(50) NOT NULL,
					channel_id INT(50) NULL DEFAULT NULL,
					item_id INT(50) NULL DEFAULT NULL,
					url TEXT NOT NULL,
					type VARCHAR(256) NULL DEFAULT NULL,
					width INT(50) NULL DEFAULT NULL,
					height INT(50) NULL DEFAULT NULL,
					PRIMARY KEY (image_id)
					)";

                $resultQuery = runQuery($sql, $connSearch);

                if (noError($resultQuery)) {

                    $resultQuery = moveData($news_archive_items, $news_archive_images, $newsresult, $connSearch);

                    if (noError($resultQuery)) {
                        $result = $resultQuery["errMsg"];
                        $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
                        //Row moved to archive table;
                    } else {
                        $result = $resultQuery["errMsg"];
                        $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
                    }

                } else {
                    $result = $resultQuery["errMsg"];
                    $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
                }
            } else {
                $result = $resultQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
            }
        } elseif ($resultQuery["errCode"] == -1) {

            $resultQuery = moveData($news_archive_items, $news_archive_images, $newsresult, $connSearch);

            if (noError($resultQuery)) {
                $result = $resultQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
                //Row moved to archive table;
            } else {
                $result = $resultQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
            }

        } else {
            $result = $resultQuery["errMsg"];
            $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
        }

    }

    if (empty($newsresult1)) {
        $logData .= "No any old news records found. \n";
        $result = $resultQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
        $returnArr["logData"] = $logData;

    } elseif (!$returnArr || noError($returnArr)) {
        $logData .= "Created archive items and images tables. \n";
        $logData .= "News items moved to archive tables. \n";
        $sql = "Delete images from images inner join items ON images.item_id = items.item_id  Where (SELECT DATEDIFF( now(), items.publish_date)) > $daysAgo";
        $resultQuery = runQuery($sql, $connSearch);
        if (noError($resultQuery)) {
            $sql = "Delete from items WHERE (SELECT DATEDIFF( now(), publish_date )) > $daysAgo";
            $resultQuery = runQuery($sql, $connSearch);
            if (noError($resultQuery)) {
                $logData .= "Deleted old records from items and images tables. \n";
                $result = $resultQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
                $returnArr["logData"] = $logData;
            } else {
                $logData .= "Error in deleting old records from images table. \n";
                $result = $resultQuery["errMsg"];
                $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
                $returnArr["logData"] = $logData;
            }
        } else {
            $logData .= "Error in deleting old records from items table. \n";
            $result = $resultQuery["errMsg"];
            $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
            $returnArr["logData"] = $logData;
        }
    } else {
        $logData .= "Error in moving old records from items and images tables. \n";
        $result = $resultQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
        $returnArr["logData"] = $logData;
    }

    return $returnArr;
}

//Move old news into into archive items and images tables.
function moveData($news_archive_items, $news_archive_images, $newsresult, $connSearch)
{
    $extraArg = array();
    $returnArr = array();

    $itemId = $newsresult['item_id'];
    $channelId = $newsresult['channel_id'];
    $itemTitle = strip_tags($newsresult['title']);
    $itemDescription = strip_tags($newsresult['description']);
    $itemLink = $newsresult['link'];
    $itemSource = $newsresult['source'];
    $itemUpdatedate = $newsresult['publish_date'];
    $sql = "Select item_id from $news_archive_items where title = '" . addslashes($itemTitle) . "'";
    $resultQuery = runQuery($sql, $connSearch);
    $checkItemId = mysqli_fetch_array($resultQuery["dbResource"]);

    if (empty($checkItemId['item_id'])) {

        $sql = "INSERT INTO $news_archive_items(item_id,channel_id, title, description, link, source, publish_date) VALUES ('$itemId','$channelId', '" . addslashes($itemTitle) . "', '" . addslashes($itemDescription) . "', '$itemLink', '$itemSource', '$itemUpdatedate')";
        $resultQuery = runQuery($sql, $connSearch);

        if (noError($resultQuery)) {

            if ($newsresult['image_id']) {

                $imageId = $newsresult['image_id'];
                $channelId = $newsresult['channel_id'];
                $itemId = $newsresult['item_id'];
                $url = $newsresult['url'];
                $type = $newsresult['type'];
                $width = $newsresult['width'];
                $height = $newsresult['height'];
                $resultQuery = runQuery($sql, $connSearch);
                $newsresult = mysqli_fetch_array($resultQuery["dbResource"]);
                $sql = "INSERT INTO $news_archive_images VALUES ('$imageId','$channelId', '$itemId', '$url', '$type', '$width','$height')";
                $resultQuery = runQuery($sql, $connSearch);

                if (noError($resultQuery)) {
                    $result = $resultQuery["errMsg"];
                    $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
                } else {

                    $result = $resultQuery["errMsg"];
                    $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
                }

            }
        } else {

            $result = $resultQuery["errMsg"];
            $returnArr = setErrorStack($returnArr, 59, $result, $extraArg);
        }
    } else {

        $result = $resultQuery["errMsg"];
        $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
    }

    return $returnArr;
}

//To send mail if any error occurs.
function sendNewsMails($message)
{
    //$to = "searchtradedeveloper@gmail.com";
    $to = "searchtradedeveloper@gmail.com";
    $subject = "Error in archiving news data.";
    //$message="test";
    //$result = Send_Mail($to,'',$subject,$message);
    global $noReplyEmail;
    $result = sendMailGunBatchEmail($to, $message, $subject, "", 0, $noReplyEmail);
}

?>

