<?php

$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

//start config
require_once("{$docrootpath}config/config.php");

require_once("{$docrootpath}config/db_config.php");

require_once("{$docrootpath}helpers/coreFunctions.php");

require_once("{$docrootpath}core/errorMap.php");

//require_once("{$docrootpath}helpers/email_helpers.php");

require_once("{$docrootpath}helpers/arrayHelper.php");

//To load RSS feeds.
error_reporting(E_ALL);

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$logPath = $docrootpath . "logs/cronLogs/cronNews";
if ($connSearch) {

    $updateLog = updateLog($connSearch, $logPath);
//    printArr($updateLog);
    if (noError($updateLog)) {
        echo "Updating channels successfull.";
        printArr($updateLog);
    } else {
        echo "Error in updating channels.";
        printArr($updateLog);
    }
}

//Create or update log file.
function updateLog($connSearch, $logPath)
{
    $logData = "";
    $extraArg = array();
    $returnArr = array();

    $month = date("MY");
    $logDir = "{$logPath}" . $month;
    if (!is_dir($logDir)) {
        mkdir($logDir, 0777, true);
    }

    $file = fopen("" . $logDir . "/" . date("d") . " log.txt", "a");
    $time1 = date("Y-m-d H:i:s");
    $logData .= "\n*********************************************************************************************\n";
    $logData .= "******************************CRON RUN TIME:" . $time1 . "******************************\n\n";
    $checkUpdate = checkUpdate($connSearch);
//    printArr($checkUpdate);

    $extraArg['file_url'] = $checkUpdate['data']['file_url'];
    $extraArg['rss_url'] = $checkUpdate['data']['rss_url'];
    $extraArg['log_data'] = $checkUpdate['logData'];

    if (noError($checkUpdate)) {
        $logData .= $checkUpdate["logData"];
        $logData .= "\n*********************************************************************************************\n";
        $logData .= "**************************************** *** END **** ***************************************\n\n ";
        fwrite($file, $logData);
        fclose($file);
        $returnArr = setErrorStack($returnArr, -1, $checkUpdate["errMsg"], $extraArg);
    } else {
        //sendNewsMail($checkUpdate["errMsg"]);
        $returnArr = setErrorStack($returnArr, 51, $checkUpdate["errMsg"], $extraArg);
    }
    return $returnArr;
}

//To update RSS feeds by checking last update time.
function checkUpdate($connSearch)
{
    $extraArg = array();
    $returnArr = array();
    $getChannels = getChannels($connSearch);
//    printArr($getChannels);
    $returnArr["logData"] = $getChannels["logData"];
    $extraArg['data'] = $getChannels["errMsg"];
    if (noError($getChannels)) {
        if (!empty($getChannels["errMsg"])) {
            $channel = $getChannels["errMsg"];
            $category = $channel['category'];
            $fileURL = $channel['file_url'];

            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $fileURL);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $output = curl_exec($ch);
            curl_close($ch);
            $output = (array)json_decode($output, true);
            if (noError($output)) {
                $returnArr = setErrorStack($returnArr, -1, $output["errMsg"], $extraArg);
                $returnArr["logData"] .= "Updating channel " . $category . " is successfull. \n";
            } else {
                $returnArr = setErrorStack($returnArr, 51, $output["errMsg"], $extraArg);
                $returnArr["logData"] .= "Error in updating channel " . $category . " \n";
            }

        } else {
            $data = "All RSS feeds are up to date.";
            $returnArr = setErrorStack($returnArr, -1, $data, $extraArg);
            $returnArr["logData"] .= "All RSS feeds are up to date.\n";
        }
    } else {
        $returnArr = setErrorStack($returnArr, 52, $getChannels["errMsg"], $extraArg);
        $returnArr["logData"] .= $getChannels["logData"];
    }
    return $returnArr;
}

//To fetch out dated channels.
function getChannels($connSearch)
{
    $extraArg = array();
    $returnArr = array();
    $sql = "SELECT * FROM channels WHERE (SELECT HOUR(TIMEDIFF(NOW(),last_update))) >= update_frequency";
    //$sql = "select * from channels where category ='yahoo top stories'";
    $resultQuery = runQuery($sql, $connSearch);
//    printArr($resultQuery);
    if (noError($resultQuery)) {
        $result = array();
        $result = mysqli_fetch_array($resultQuery["dbResource"]);
        $returnArr = setErrorStack($returnArr, -1, $result, $extraArg);
        $returnArr["logData"] = "Checking channels table is successfull.\n";
    } else {
        $returnArr = setErrorStack($returnArr, 53, null, $extraArg);
        $returnArr["logData"] = "Error in checking channels table. \n";
    }
    return $returnArr;
}


//To send mail if any error occurs.
/*function sendNewsMail($message){
    $to = "searchtradedeveloper@gmail.com";
    $subject = "Error in loading news";
    $result = send_Mail($to,'',$subject,$message);
}*/

//To send mail if any error occurs.
function sendNewsMails($message)
{
    //$to = "searchtradedeveloper@gmail.com";
    $to = "searchtradedeveloper@gmail.com";
    $subject = "Error in archiving news data.";
    //$message="test";
    //$result = Send_Mail($to,'',$subject,$message);
    global $noReplyEmail;

    $result = sendMailGunBatchEmail($to, $message, $subject, $recipientVars, 0, $noReplyEmail);

}


?>