<?php

//$docRootnew=explode("admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];
$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/tracking/intraction/intraction_analytics.php");
require_once("$docrootpath/model/keywords/analytics/daily_keyword_analytics.php");
require_once("$docrootpath/model/keywords/common_keyword.php");
require_once("$docrootpath/model/keywords/analytics/keyword_stat_analytics.php");


//dbconnection for search db
$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

function readXMLCron($scanDirPath, $connDemo)
{
    $userDate = date("Y-m-d", strtotime('-1 day'));
    if (file_exists($scanDirPath)) {
        $filesInpath = scandir($scanDirPath);

        foreach ($filesInpath as $files) {
            if ($files !== "." && $files !== "..") {
                $filesString = strtolower(substr($files, 10));
                $URL = $scanDirPath . "/" . $files;
                libxml_use_internal_errors(true);
                $xml = simplexml_load_file($URL);
                if (!$xml) {
                    global $docrootpath;
                    $mypath = $docrootpath . "logs/cronLogs/analyticsCron/";
                    $URLtowrite = $scanDirPath . "/" . $files . "\n";
                    writeTextLogs($mypath, "collect_kwd_aff_analytics_Failed", $URLtowrite);
                    continue;
                }
                $dom = new DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($xml->asXML());
                $xml = new SimpleXMLElement($dom->saveXML());
                //printArr($xml);
                if ($xml == false) {

                    echo "Failed loading XML: ";
                    foreach (libxml_get_errors() as $error) {
                        echo "<br>", $error->message;
                    }
                } else {
                    foreach ($xml->activity as $xmlData) {

                        $user['trackingDate'] = date("d-m-Y", strtotime($xmlData->attributes()->timestamp));
                        $user['browser'] = $xmlData->attributes()->browser;
                        $user['userIp'] = $xmlData->attributes()->userIp;
                        $user['device'] = $xmlData->attributes()->device;
                        $user['country'] = $xmlData->attributes()->country;
                        $user['city'] = $xmlData->attributes()->city;
                        $user['gender'] = $xmlData->attributes()->gender;
                        $user['username'] = $xmlData->attributes()->username;

                        switch ($filesString) {

                            case "paybywallet.xml":

                                $response = json_decode(urldecode($xmlData->response->attributes()->response), true);
                                $errorCode = $response["errCode"];


                                if ($errorCode == "-1") {

                                    $keywordBasket = explode(" ", $xmlData->keywordBasket);

                                    $kwdcount = 0;
                                    foreach ($keywordBasket as $keyword) {

                                        $keywordmd5 = md5($keyword);
                                        $kwddata = "kwywordPurchase{$keywordmd5}";

                                        $kwdArray = $xmlData->$kwddata;
                                        $buyerRefferedBy = "buyerRefferedBy{$keywordmd5}";
                                        $buyerRefferedBy = trim($kwdArray->$buyerRefferedBy);
                                        $affiliateEarnings = "affiliateEarnings{$keywordmd5}";
                                        $affiliateEarnings = $kwdArray->$affiliateEarnings;


                                        $intraction_type_main = "keyword_affiliate_earning";
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social",
                                            $user['country'], $user['device'], $user['browser'], $user['gender'], "", "qualified", "",
                                            $affiliateEarnings, $buyerRefferedBy, $userDate, $user['username'], $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                    }

                                }


                                break;

                            case "paybypaypal.xml":

                                $errorCode = $xmlData->response->attributes()->errCode;


                                if ($errorCode == "-1") {

                                    $keywordBasket = explode(" ", $xmlData->keywordBasket);

                                    $kwdcount = 0;
                                    foreach ($keywordBasket as $keyword) {

                                        $keywordmd5 = md5($keyword);
                                        $kwddata = "kwywordPurchase{$keywordmd5}";

                                        $kwdArray = $xmlData->$kwddata;
                                        $buyerRefferedBy = "buyerRefferedBy{$keywordmd5}";
                                        $buyerRefferedBy = trim($kwdArray->$buyerRefferedBy);
                                        $affiliateEarnings = "affiliateEarnings{$keywordmd5}";
                                        $affiliateEarnings = $kwdArray->$affiliateEarnings;


                                        $intraction_type_main = "keyword_affiliate_earning";
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social",
                                            $user['country'], $user['device'], $user['browser'], $user['gender'], "", "qualified", "",
                                            $affiliateEarnings, $buyerRefferedBy, $userDate, $user['username'], $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                    }

                                }


                                break;


                        }
                    }

                }

            }
        }

    }
}

$yesterdayDate = explode("-", date("d-m-Y", strtotime('0 day')));

$date = $yesterdayDate["0"];
$month = $yesterdayDate["1"];
$year = $yesterdayDate["2"];

global $logPath;

//$logPathnew = $logPath["userManagement"];
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/logs/keyword/purchase/";
$scanDirPath = $docrootpath . $year . "/" . $month . "/" . $date;
$data = readXMLCron($scanDirPath, $connDemo);
?>