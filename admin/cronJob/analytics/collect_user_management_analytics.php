<?php

//$docRootnew=explode("admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];
$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/manageUser/userManagement_analytics.php");

//dbconnection for search db
$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

function readXMLCron($scanDirPath, $connDemo)
{
    $userDate = date("Y-m-d", strtotime('-1 day'));
    if (file_exists($scanDirPath)) {
        $filesInpath = scandir($scanDirPath);

        foreach ($filesInpath as $files) {
            if ($files !== "." && $files !== "..") {
                $filesString = strtolower(substr($files, 10));
                $URL = $scanDirPath . "/" . $files;
                libxml_use_internal_errors(true);
                $xml = simplexml_load_file($URL);
                if (!$xml) {
                    global $docrootpath;
                    $mypath = $docrootpath . "logs/cronLogs/analyticsCron/";
                    $URLtowrite = $scanDirPath . "/" . $files . "\n";
                    writeTextLogs($mypath, "collectUserManagementAnalytics_Failed", $URLtowrite);
                    continue;
                }
                $dom = new DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($xml->asXML());
                $xml = new SimpleXMLElement($dom->saveXML());
                if ($xml == false) {

                    echo "Failed loading XML: ";
                    foreach (libxml_get_errors() as $error) {
                        echo "<br>", $error->message;
                    }
                } else {
                    foreach ($xml->activity as $xmlData) {

                        $user['trackingDate'] = date("d-m-Y", strtotime($xmlData->attributes()->timestamp));
                        $user['browser'] = $xmlData->attributes()->browser;
                        $user['userIp'] = $xmlData->attributes()->userIp;
                        $user['device'] = $xmlData->attributes()->device;
                        $user['country'] = $xmlData->attributes()->country;
                        $user['state'] = $xmlData->attributes()->state;
                        $user['city'] = $xmlData->attributes()->city;
                        $user['gender'] = $xmlData->attributes()->gender;
                        $user['location'] = $user['country'];
                        $user['errorCode'] = $xmlData->response->attributes()->errCode;

                        switch ($filesString) {

                            case "user_login.xml":
                                if ($user['errorCode'] == "-1") {
                                    $data = insert_user_activity("user_loginCount", $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                    printArr($data);
                                }

                                break;


                            case "forget_password.xml":

                                if ($user['errorCode'] == "-1") {
                                    $data = insert_user_activity("user_forget_password_count", $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                    printArr($data);
                                }
                                break;

                            case "reset_password.xml":

                                if ($user['errorCode'] == "-1") {
                                    $data = insert_user_activity("user_forget_passSucc_count", $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                    printArr($data);
                                } else {
                                    $data = insert_user_activity("user_forget_passFailed_count", $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                    printArr($data);
                                }

                                break;

                            case "modeswitch.xml":

                                $user['switchMode'] = $xmlData->response->attributes()->fieldname;

                                if ($user['errorCode'] == "-1") {
                                    $data = insert_user_activity(trim($user['switchMode']), $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                    printArr($data);
                                }
                                break;
                        }
                    }

                }

            }
        }

    }
}

$yesterdayDate = explode("-", date("d-m-Y", strtotime('-1 day')));

$date = $yesterdayDate["0"];
$month = $yesterdayDate["1"];
$year = $yesterdayDate["2"];

global $logPath;

//$logPathnew = $logPath["userManagement"];
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/logs/UserMgmt/";
$scanDirPath = $docrootpath . $year . "/" . $month . "/" . $date;
$data = readXMLCron($scanDirPath, $connDemo);
?>