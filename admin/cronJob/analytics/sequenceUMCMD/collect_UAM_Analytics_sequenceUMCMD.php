<?php
error_reporting(0);


$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/helpers/stringHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/manageUser/userManagement_analytics.php");


$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

if(!empty($_GET))
{
    $userDate = cleanQueryParameter($connDemo, cleanXSS(($_GET["date"])));

    $filename =  cleanQueryParameter($connDemo, cleanXSS(($_GET["filename"])));

    $fieldsArray = array(
        'date' => urlencode($userDate),
        'filename' => urlencode($filename)
    );



    $url = $adminRoot."cronJob/analytics/sequenceUMCMD/usersignup.php";
    $result= internalCurlRequestPost($url,$fieldsArray);

    $url = $adminRoot."cronJob/analytics/sequenceUMCMD/verifyUser.php";
    $result= internalCurlRequestPost($url,$fieldsArray);

    $url = $adminRoot."cronJob/analytics/sequenceUMCMD/blockUser.php";
    $result= internalCurlRequestPost($url,$fieldsArray);

    $url = $adminRoot."cronJob/analytics/sequenceUMCMD/unblockUser.php";
    $result= internalCurlRequestPost($url,$fieldsArray);


}else{
    echo "<h1>Please Enter File Name and Date";
}






/*
 *Command :-
 * php collect_user_management_analytics_CMD.php 23 01 2017 04_OClock_ulogin.xml  CMD
 *
 * collect_user_management_analytics_CMD.php?date=15/03/2017&filename=11_OClock_UserSignup.xml  //GET
 *
 * parameters are : date month year file name with hour o clock prefix
 */
?>