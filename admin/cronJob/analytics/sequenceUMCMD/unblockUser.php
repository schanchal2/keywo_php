<?php
error_reporting(0);
$docRootnew=explode("/admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/helpers/stringHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/manageUser/userManagement_analytics.php");


$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];



function readXMLCron($scanDirPath, $filename,$userDate, $connDemo)
{
    if (file_exists($scanDirPath)) {
        $filesInpath = scandir($scanDirPath);

        if (in_array($filename, $filesInpath)) {

            $filesString = strtolower(substr($filename, 10));
            $URL = $scanDirPath . "/" . $filename;
            libxml_use_internal_errors(true);
            $xml = simplexml_load_file($URL);
            if (!$xml) {
                global $docrootpath;
                $mypath = $docrootpath . "logs/cronLogs/analyticsCron/";
                $URLtowrite = $scanDirPath . "/" . $filename . "\n";
                writeTextLogs($mypath, "collectUserManagementAnalytics_Failed", $URLtowrite);
            }
            $dom = new DOMDocument('1.0');
            $dom->preserveWhiteSpace = false;
            $dom->formatOutput = true;
            $dom->loadXML($xml->asXML());
            $xml = new SimpleXMLElement($dom->saveXML());
            if ($xml == false) {

                echo "Failed loading XML: ";
                foreach (libxml_get_errors() as $error) {
                    echo "<br>", $error->message;
                }
            } else {
                foreach ($xml->activity as $xmlData) {

                    $user['trackingDate'] = date("d-m-Y", strtotime($xmlData->attributes()->timestamp));
                    $user['browser'] = $xmlData->attributes()->browser;
                    $user['userIp'] = $xmlData->attributes()->userIp;
                    $user['device'] = $xmlData->attributes()->device;
                    $user['country'] = $xmlData->attributes()->country;
                    $user['state'] = $xmlData->attributes()->state;
                    $user['city'] = $xmlData->attributes()->city;
                    $user['gender'] = $xmlData->attributes()->gender;
                    $user['location'] = $user['country'];
                    $user['errorCode'] = $xmlData->response->attributes()->errCode;

                    switch ($filesString) {

                        case "un-blockedusersbyadmin.xml":

                            $user['switchMode'] = $xmlData->response->attributes()->fieldname;

                            if ($user['errorCode'] == "-1") {
                                $data = insert_user_activity('total_unblocked_userstoday', $user['country'], $user['device'], $user['browser'], $user['gender'], $userDate, $connDemo);
                                printArr($data);
                            }
                            break;

                    }
                }

            }
        }
    }
}


if(!empty($_POST))
{
    $userDate = cleanQueryParameter($connDemo, cleanXSS(($_POST["date"])));
    $dmy=explode("/",$userDate);
    $date = $dmy["0"];
    $month = $dmy["1"];
    $year = $dmy["2"];

    $filename =  cleanQueryParameter($connDemo, cleanXSS(($_POST["filename"])));
    $userDates =$year."-".$month."-".$date;
}


$docRootnew=explode("admin/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];
global $logPath;
$logPathnew = $docRootnew."logs/UserMgmt/";
$scanDirPath = $logPathnew . $year . "/" . $month . "/" . $date;
$data = readXMLCron($scanDirPath, $filename,$userDates,$connDemo);


/*
 *Command :-
 * php collect_user_management_analytics_CMD.php 23 01 2017 04_OClock_ulogin.xml  CMD
 *
 * collect_user_management_analytics_CMD.php?date=15/03/2017&filename=11_OClock_UserSignup.xml  //GET
 *
 * parameters are : date month year file name with hour o clock prefix
 */
?>