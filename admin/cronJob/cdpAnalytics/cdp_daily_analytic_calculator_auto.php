<?php

$docRootnew = explode("/admin/", $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0] . "/";
$docrootpath = $docRootnew . "admin/";


require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/helpers/stringHelper.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/tracking/intraction/intraction_analytics.php");
require_once("$docrootpath/model/keywords/analytics/daily_keyword_analytics.php");
require_once("$docrootpath/model/keywords/common_keyword.php");
require_once("$docrootpath/model/keywords/analytics/keyword_stat_analytics.php");

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$conndbkeywords = createDBConnection("dbkeywords");
noError($conndbkeywords) ? $conndbkeywords = $conndbkeywords["connection"] : $error = $conndbkeywords["errMsg"];


function readXMLCron($scanDirPath, $connDemo, $conndbkeywords)
{
    $userDate = date("Y-m-d", strtotime('-1 day'));

    if (file_exists($scanDirPath)) {
        $filesInpath = scandir($scanDirPath);


        foreach ($filesInpath as $files) {

            if ($files !== "." && $files !== "..") {
                $filesString = strtolower(substr($files, 10));
                $URL = $scanDirPath . "/" . $files;
                libxml_use_internal_errors(true);
                $xml = simplexml_load_file($URL);
                //printArr($xml);

                if (!$xml) {
                    global $docrootpath;
                    $mypath = $docrootpath . "logs/cronLogs/analyticsCron/";
                    $URLtowrite = $scanDirPath . "/" . $files . "\n";
                    writeTextLogs($mypath, "collectIntractionAnalytics_Failed", $URLtowrite);
                    continue;
                }

                if ($filesString != "three_user_cdp.xml") {
                    $filesString = "singleUser";
                }

                $dom = new DOMDocument('1.0');
                $dom->preserveWhiteSpace = false;
                $dom->formatOutput = true;
                $dom->loadXML($xml->asXML());
                $xml = new SimpleXMLElement($dom->saveXML());


                if ($xml == false) {

                    echo "Failed loading XML: ";
                    foreach (libxml_get_errors() as $error) {
                        echo "<br>", $error->message;
                    }
                } else {
                    foreach ($xml->activity as $xmlData) {

                        switch ($filesString) {

                            case "singleUser":


                                $errCode = $xmlData->response->attributes()->errCode;

                                if ($errCode == -1) {


                                    $email = $xmlData->attributes()->email;
                                    $browser = $xmlData->attributes()->browser;
                                    $device = $xmlData->attributes()->device;
                                    $country = $xmlData->attributes()->country;
                                    $gender = $xmlData->attributes()->gender;
                                    $username = $xmlData->attributes()->username;

                                    $intraction_type = $xmlData->app->attributes()->intractionMode;
                                    $sub_type_qualification = strtolower($xmlData->intraction->attributes()->qualificationType);//qualified/unqualified
                                    $intraction_earnings = $xmlData->intraction->attributes()->intractionPayout;

                                    if ($intraction_type == "search") {
                                        $post_type = "";
                                        $appid = $xmlData->app->attributes()->app_id;

                                        $intraction_type_main = $xmlData->app->attributes()->intraction_type;
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "1");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }

                                        $result = insert_intraction_activity($intraction_type, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $userDate, $connDemo);
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }

                                        if ($xmlData->referrerUser->attributes()->email != "") { //for search Referral
                                            $email = $xmlData->referrerUser->attributes()->email;
                                            $intraction_earnings = $xmlData->referrerUser->attributes()->payoutGiven;
                                            $username = $xmlData->referrerUser->attributes()->username;
                                            $gender = $xmlData->referrerUser->attributes()->gender;
                                            $intraction_type_main = $xmlData->referrerUser->attributes()->referral_type;

                                            $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                            if (noError($result)) {
                                                echo "success analytics updated <br/>";
                                            } else {
                                                printArr($result);
                                            }

                                        }


                                        if ($xmlData->search_keyword != "") {
                                            $keywordsgroup = explode(" ", $xmlData->search_keyword);
                                            foreach ($keywordsgroup as $keyword) {

                                                $keywordResult = getKeywordOwnershipDetails($conndbkeywords, $keyword);
                                                $keywordResult = $keywordResult["errMsg"];
                                                if (!empty($keywordResult)) {
                                                    $keywordOwner = $keywordResult["buyer_id"];
                                                } else {
                                                    global $unownedKwdOwnerUser;
                                                    $keywordOwner = $unownedKwdOwnerUser;
                                                }

                                                $kwdOwnerInfo = "gender";
                                                global $walletURLIP;

                                                $kwdOwnerUserInfo = getUserInfo($keywordOwner, $walletURLIP . 'api/v3/', $kwdOwnerInfo);
                                                $kwdOwnerUserInfo = $kwdOwnerUserInfo["errMsg"];
                                                $kwdOwnergender = $kwdOwnerUserInfo["gender"];
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/
                                                $resultKeyword = insert_keywordEarning_activity("search", "total_interaction_count", $country, $device, $browser, $kwdOwnergender, $appid, $keyword, $sub_type_qualification, $post_type, $intraction_earnings, $keywordOwner, $userDate, $conndbkeywords, "1");

                                                if (noError($resultKeyword)) {

                                                    $result = insert_top1000_keywordEarning_interaction($keyword, $intraction_earnings, "1", $conndbkeywords);
                                                    echo "success keyword Search analytics updated <br/>";
                                                } else {
                                                    printArr($resultKeyword);
                                                }
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/


                                                /*********************************** keyword stat ************************************/
                                                insert_keyword_statistics("keyword_lifeTime_intractions", "0", $conndbkeywords);
                                                /*********************************** keyword stat ************************************/


                                            }
                                        }

                                    }
                                    if ($intraction_type == "social") {

                                        $appid = "";
                                        $post_type = $xmlData->app->attributes()->post_type;
                                        $post_id = $xmlData->app->attributes()->post_id;

                                        $intraction_type_main = $xmlData->app->attributes()->intraction_type;
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "1");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }

                                        $result = insert_intraction_activity($intraction_type, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $userDate, $connDemo);
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }


                                        if ($xmlData->search_keyword != "") {
                                            $keywordsgroup = explode(" ", $xmlData->search_keyword);
                                            foreach ($keywordsgroup as $keyword) {

                                                $keywordResult = getKeywordOwnershipDetails($conndbkeywords, $keyword);
                                                $keywordResult = $keywordResult["errMsg"];
                                                if (!empty($keywordResult)) {
                                                    $keywordOwner = $keywordResult["buyer_id"];
                                                } else {
                                                    global $unownedKwdOwnerUser;
                                                    $keywordOwner = $unownedKwdOwnerUser;
                                                }

                                                $kwdOwnerInfo = "gender";
                                                global $walletURLIP;

                                                $kwdOwnerUserInfo = getUserInfo($keywordOwner, $walletURLIP . 'api/v3/', $kwdOwnerInfo);
                                                $kwdOwnerUserInfo = $kwdOwnerUserInfo["errMsg"];
                                                $kwdOwnergender = $kwdOwnerUserInfo["gender"];
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection social++++++++++++++++++++++++++++++++++++++*/
                                                $resultKeyword = insert_keywordEarning_activity("social", "total_interaction_count", $country, $device, $browser, $kwdOwnergender, $appid, $keyword, $sub_type_qualification, $post_type, $intraction_earnings, $keywordOwner, $userDate, $conndbkeywords, "1");

                                                if (noError($resultKeyword)) {
                                                    $result = insert_top1000_keywordEarning_interaction($keyword, $intraction_earnings, "1", $conndbkeywords);
                                                    echo "success keyword social analytics updated <br/>";
                                                } else {
                                                    printArr($resultKeyword);
                                                }
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection social++++++++++++++++++++++++++++++++++++++*/

                                                /*********************************** keyword stat ************************************/
                                                insert_keyword_statistics("keyword_lifeTime_intractions", "0", $conndbkeywords);
                                                /*********************************** keyword stat ************************************/
                                            }
                                        }
                                    }


                                }


                                break;


                            case "three_user_cdp.xml":

                                $errCode = $xmlData->response->attributes()->errCode;

                                if ($errCode == -1) {


                                    $country = $xmlData->request->attributes()->country;
                                    $device = $xmlData->request->attributes()->device;
                                    $browser = $xmlData->request->attributes()->browser;
                                    $intraction_type = strtolower($xmlData->request->attributes()->mode); //search/social
                                    $sub_type_qualification = strtolower($xmlData->request->attributes()->interaction_type);//qualified/unqualified


                                    if ($intraction_type == "search") {
                                        $post_type = "";
                                        $appid = $xmlData->request->attributes()->content_id;


                                        /***************************************** for Keywo User********************************************/

                                        $email = $xmlData->keywoUser->attributes()->user;
                                        $intraction_type_main = $xmlData->keywoUser->attributes()->interaction_type;
                                        $intraction_earnings = $xmlData->keywoUser->attributes()->amount_given_to_user;
                                        $gender = $xmlData->keywoUser->attributes()->keywo_gender;
                                        $usernamejson = json_decode($xmlData->keywoUser->attributes()->kwresponse, true);
                                        $username = isset($usernamejson["first_name"]) ? $usernamejson["first_name"] : "" . isset($usernamejson["last_name"]) ? $usernamejson["last_name"] : "";
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                        /***************************************** for Keywo User********************************************/

                                        /************************************* for app Developer User*************************************/
                                        $email = $xmlData->appDeveloper->attributes()->user;
                                        $intraction_type_main = $xmlData->appDeveloper->attributes()->interaction_type;
                                        $intraction_earnings = $xmlData->appDeveloper->attributes()->amount_given_to_user;
                                        $gender = $xmlData->appDeveloper->attributes()->app_dev_gender;
                                        $username = $xmlData->appDeveloper->attributes()->username;
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                        /************************************* for app Developer User*************************************/


                                        /************************************* for keyword owner User*************************************/

                                        $keywordsgroup = explode(" ", $xmlData->kwywordOwnership->keywords);
                                        foreach ($keywordsgroup as $keyword) {
                                            if ($keyword != "") {
                                                $keywordTag = "kwywordOwnership" . md5($keyword);
                                                $email = $xmlData->$keywordTag->attributes()->user;
                                                $intraction_type_main = $xmlData->$keywordTag->attributes()->interaction_type . "_search";
                                                $intraction_earnings = $xmlData->$keywordTag->attributes()->amount_given_to_user;
                                                $gender = $xmlData->$keywordTag->attributes()->kwd_owner_gender;
                                                $username = $xmlData->$keywordTag->attributes()->username;
                                                $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                                if (noError($result)) {
                                                    echo "success analytics updated <br/>";
                                                } else {
                                                    printArr($result);
                                                }
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/
                                                $resultKeyword = insert_keywordEarning_activity("search", "total_interaction_count", $country, $device, $browser, $gender, $appid, $keyword, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $conndbkeywords, "0");

                                                if (noError($resultKeyword)) {
                                                    $result = insert_top1000_keywordEarning_interaction($keyword, $intraction_earnings, "0", $conndbkeywords);
                                                    echo "success keyword analytics updated <br/>";
                                                } else {
                                                    printArr($resultKeyword);
                                                }
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/


                                                /*********************************** keyword stat ************************************/
                                                insert_keyword_statistics("keyword_lifeTime_earnings", $intraction_earnings, $conndbkeywords);
                                                /*********************************** keyword stat ************************************/


                                            }


                                        }
                                        /************************************* for keyword owner User*************************************/

                                    }

                                    if ($intraction_type == "social") {
                                        $appid = "";
                                        $post_type = $xmlData->request->attributes()->post_type;


                                        /***************************************** for Keywo User********************************************/

                                        $email = $xmlData->keywoUser->attributes()->user;
                                        $intraction_type_main = $xmlData->keywoUser->attributes()->interaction_type;
                                        $intraction_earnings = $xmlData->keywoUser->attributes()->amount_given_to_user;
                                        $gender = $xmlData->keywoUser->attributes()->keywo_gender;
                                        $usernamejson = json_decode($xmlData->keywoUser->attributes()->kwresponse, true);
                                        $username = isset($usernamejson["first_name"]) ? $usernamejson["first_name"] : "" . isset($usernamejson["last_name"]) ? $usernamejson["last_name"] : "";
                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                        /***************************************** for Keywo User********************************************/


                                        /************************************* for contentCreator and shearer User*************************************/
                                        $email = $xmlData->contentCreator->attributes()->user;
                                        $intraction_type_main = $xmlData->contentCreator->attributes()->interaction_type;
                                        $intraction_earnings = $xmlData->contentCreator->attributes()->amount_given_to_user;
                                        $gender = $xmlData->contentCreator->attributes()->creator_gender;
                                        $usernamejson = json_decode($xmlData->contentCreator->attributes()->response_creator, true);
                                        $username = isset($usernamejson["first_name"]) ? $usernamejson["first_name"] : "" . isset($usernamejson["last_name"]) ? $usernamejson["last_name"] : "";

                                        $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                        if (noError($result)) {
                                            echo "success analytics updated <br/>";
                                        } else {
                                            printArr($result);
                                        }
                                        /*********************************************************/
                                        if ($xmlData->contentCreator->attributes()->sharer_user != "") {
                                            $email = $xmlData->contentCreator->attributes()->sharer_user;
                                            $intraction_type_main = $xmlData->contentCreator->attributes()->interaction_type_share;
                                            $intraction_earnings = $xmlData->contentCreator->attributes()->amount_given_to_sharer_user;
                                            $gender = $xmlData->contentCreator->attributes()->content_sharer_gender;
                                            $usernamejson = json_decode($xmlData->contentCreator->attributes()->response_sharer, true);
                                            $username = isset($usernamejson["first_name"]) ? $usernamejson["first_name"] : "" . isset($usernamejson["last_name"]) ? $usernamejson["last_name"] : "";
                                            $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                            if (noError($result)) {
                                                echo "success analytics updated <br/>";
                                            } else {
                                                printArr($result);
                                            }
                                        }
                                        /************************************* for contentCreator and shearer User*************************************/

                                        /************************************* for keyword owner User*************************************/

                                        $keywordsgroup = explode(" ", $xmlData->kwywordOwnership->keywords);
                                        foreach ($keywordsgroup as $keyword) {
                                            if ($keyword != "") {
                                                $keywordTag = "kwywordOwnership" . md5($keyword);
                                                $email = $xmlData->$keywordTag->attributes()->user;
                                                $intraction_type_main = $xmlData->$keywordTag->attributes()->interaction_type . "_social";
                                                $intraction_earnings = $xmlData->$keywordTag->attributes()->amount_given_to_user;
                                                $gender = $xmlData->$keywordTag->attributes()->kwd_owner_gender;
                                                $username = $xmlData->$keywordTag->attributes()->username;
                                                $result = insert_intractionEarning_activity($intraction_type_main, "no_of_searchORview_social", $country, $device, $browser, $gender, $appid, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $username, $connDemo, "0");
                                                if (noError($result)) {
                                                    echo "success analytics updated <br/>";
                                                } else {
                                                    printArr($result);
                                                }

                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/
                                                $resultKeyword = insert_keywordEarning_activity("social", "total_interaction_count", $country, $device, $browser, $gender, $appid, $keyword, $sub_type_qualification, $post_type, $intraction_earnings, $email, $userDate, $conndbkeywords, "0");

                                                if (noError($resultKeyword)) {
                                                    $result = insert_top1000_keywordEarning_interaction($keyword, $intraction_earnings, "0", $conndbkeywords);
                                                    echo "success keyword analytics updated <br/>";
                                                } else {
                                                    printArr($resultKeyword);
                                                }
                                                /*+++++++++++++++++++++++++++++++++++++++++++ Keyword Analytics collection search++++++++++++++++++++++++++++++++++++++*/


                                                /*********************************** keyword stat ************************************/
                                                insert_keyword_statistics("keyword_lifeTime_earnings", $intraction_earnings, $conndbkeywords);
                                                /*********************************** keyword stat ************************************/

                                            }
                                        }
                                        /************************************* for keyword owner User*************************************/


                                    }


                                } else {
                                    echo "XML file data is not valid! Activity failed by user. <br/>";
                                }


                                break;

                        }
                    }

                }

            }
        }

    }
}

$yesterdayDate = explode("-", date("d-m-Y", strtotime('-1 day')));

$date = $yesterdayDate["0"];
$month = $yesterdayDate["1"];
$year = $yesterdayDate["2"];

global $logPath;

//$logPathnew = $logPath["userManagement"];
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/logs/coinDistribution/";
$scanDirPath = $docrootpath . $year . "/" . $month . "/" . $date;
$data = readXMLCron($scanDirPath, $connDemo, $conndbkeywords);
?>