<?php

$docRootnew = explode("/admin/", $_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0] . "/";
$docrootpath = $docRootnew . "admin/";



require_once("$docrootpath/config/config.php");
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/model/tracking/userwise/common.php");

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$userDate = date("Y-m-d", strtotime('-1 day'));
$EarningType=explode(",","post_view_earnings,post_creator_earning,keyword_ownership_earning_search,keyword_ownership_earning_social,search_earning,app_earning,post_sharer_earning,keyword_affiliate_earning");

foreach($EarningType as $earning)
{
    $result=getDayWiseQuery($userDate,$earning,$connDemo);
    printArr($result);
}

$result=getDayWiseQueryTopToatalEarnersSS($userDate, $connDemo);
printArr($result);
?>