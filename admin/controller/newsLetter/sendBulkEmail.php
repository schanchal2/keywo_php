<?php
session_start();
ob_start();
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config
error_reporting(0);
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/email_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other
require_once("../../model/newsletter/newsLetter.php");
ignore_user_abort(true);
ini_set('max_execution_time', 0);//0 until finish

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();

$newsLetterContent = cleanQueryParameter($connKeywords, $_POST["newsLetterContent"]);
$subject = cleanQueryParameter($connKeywords, cleanXSS($_POST["subject"]));
$usersType= cleanQueryParameter($connKeywords, cleanXSS($_POST["usersType"]));
$sender= cleanQueryParameter($connKeywords, cleanXSS($_POST["sender"]));
$gender= "";
$country= "";
$city= "";
$status= 0;

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    if ($usersType == "") {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Please Select Users Type !!";
        echo(json_encode($returnArr));
        exit;
    }



    if ($sender == "") {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Please Select Sender !!";
        echo(json_encode($returnArr));
        exit;
    }

    if ($subject == "") {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Please Enter Newsletter Subject!!";
        echo(json_encode($returnArr));
        exit;
    }

    if ($newsLetterContent == "") {
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Please Enter Newsletter Content!!";
        echo(json_encode($returnArr));
        exit;
    }

    $result=addNewsletterToQueue($newsLetterContent,$subject,$usersType,$sender,$gender,$country,$city,$status,$connKeywords);
    if(noError($result))
    {
        global $adminRoot;
        $url="{$adminRoot}controller/newsLetter/check_newsletter_queue.php";
        // runBackgroundProcess($url,$ApachePhpPort);
        $data=array();
        backgroundProcessPost($url,$data);
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $result["errMsg"];
    }else{
        $returnArr["errCode"] = $result["errCode"];
        $returnArr["errMsg"] = $result["errMsg"];
    }
}

echo json_encode($returnArr);


//sendBackgroundNewsletter($url);


?>
