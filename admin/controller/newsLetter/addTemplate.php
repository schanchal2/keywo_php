<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/template/templateModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once "../../model/geolocation/geoLocation.php";

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$offset = 0;
$limit =5;
$id = $_GET['id'];
if(!empty($id)){
    $getAllTemplateRequest = getAllTemplateRequest($offset, $limit,$id,$connKeywords)["errMsg"];
}

?>
    <div class="row">
        <div class="">
            <div class="col-lg-12">
                <form class="form-horizontal p-t-15" name="createNewsletter">
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Subject:</label>
                        <div class="col-sm-8">
                            <input type="hidden" id="tempID" placeholder="" value="<?= $getAllTemplateRequest['id']; ?>">
                            <input type="text" class="form-control" id="subjectPop" placeholder="" value="<?= $getAllTemplateRequest['subject']; ?>">
                        </div>
                    </div>
                    <div class="form-group clearfix">
                        <label for="createNewsletter-message" class="col-sm-2 control-label">Message :
                        </label>
                        <div class="col-sm-8">
                            <textarea class="form-control createNewsletter-message" name="createNewsletter-message_template" id="createNewsletter-message_template_pop" rows="6" onkeyup="countChar(this,1000, '#createNewsletter-recieverDetails-info' );">
                                <?= htmlspecialchars_decode($getAllTemplateRequest['content']); ?>
                            </textarea>
                            <div class="textareaInfo" id="createNewsletter-recieverDetails-info">
                                1000
                            </div>
                        </div>
                    </div>
                    <div class="form-group ">
                        <label for="inputPassword3" class="col-sm-2 control-label"></label>
                        <div class="col-sm-8">
                            <div id="add_template_error" style="color:red;">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-8">
                            <div class="pull-right ">
                                <button type="button" id="templateSubmitPop" onclick="submitTemplatePopUp();" class="btn rounded-corner bg-darkblue text-white btn-default">
                                    Save as Template
                                </button>
                                <!--       <button type="submit"
                                    class="btn rounded-corner bg-darkblue text-white btn-default">
                                Save
                                as Draft
                            </button>
                            <button type="submit" class="btn rounded-corner bg-darkblue text-white btn-default">
                                Send
                            </button>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
    CKEDITOR.replace('createNewsletter-message_template');

    function submitTemplatePopUp() {
        var tempID = $("#tempID").val();
        var newsLetter = $("#addTempalateList .cke_wysiwyg_frame").contents().find("body").html();
        var subject = $("#subjectPop").val();
        $("#templateSubmitPop").css('pointer-events', 'none');
        if (newsLetter != '' & subject != '') {
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: "../../controller/newsLetter/templateSubmit.php",
                data: {
                    tempID: tempID,
                    newsLetter: newsLetter,
                    subject: subject
                },
                success: function(data) {

                    if (data["errCode"] == -1) {
                        $("#addTempalateList").modal('hide');
                        $("#ManageTemplate").load("../../controller/newsLetter/template.php");
                        $("#errors").html(bsAlert("success", "Template Saved Successfully."));
                    } else {
                        $("#errors").html(bsAlert("danger", "There was error in adding new template please try again."));
                    }
                },
                beforeSend: function(data) {

                },
                complete: function(data) {

                },
                error: function(data) {
                    //                    alert('Failed');
                }

            });
        } else {
            $('#add_template_error').text('All Fields Are Mandetory...!');
        }
    }
    </script>
