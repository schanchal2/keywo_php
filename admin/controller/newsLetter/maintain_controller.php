<?php
session_start();

//start config
require_once('../../config/config.php');

require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/newsletter/maintain_model.php');
//end other


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();

$type = cleanQueryParameter($connKeywords, cleanXSS($_POST["operation_type"]));

if(isset($_POST["maintain_description"]))
{
    $maintain_description = cleanQueryParameter($connKeywords, cleanXSS($_POST["maintain_description"]));
}else
{
    $maintain_description = "";
}


if(isset($_POST["maintain_id"]))
{
    $maintain_id = cleanQueryParameter($connKeywords, cleanXSS($_POST["maintain_id"]));
}else
{
    $maintain_id = "";
}


if(isset($_POST["maintain_status"]))
{
    $maintain_status = cleanQueryParameter($connKeywords, cleanXSS($_POST["maintain_status"]));
}else
{
    $maintain_status = "";
}


$email=$_SESSION["admin_id"];

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "add_maintain":

            if ($maintain_description=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please add description!";;
                echo(json_encode($returnArr));
                exit;
            }

            $result=addNewMaintainMsg($maintain_description,$email,$connKeywords);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in adding Maintenance Message! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;
        case "delete_maintain":

            $result=deleteMsg($maintain_id,$connKeywords);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Deleting Maintenance Mode Details! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;

        case "edit_messages":

           $bulletinData=getMsgtoEdit($maintain_id,$connKeywords)["errMsg"][0];

            ?>

            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="edit_bulletin_description">Description:</label>
                <div class="col-xs-10">
                            <textarea class="form-control bg-lightgray noresize scrollBar" rows="3"
                                      id="edit_bulletin_description">
                                <?= $bulletinData['message']; ?>
                            </textarea>
                </div>
            </div>
            <input type="hidden" id="hiddenBulletinId" value="<?= $bulletinData['id']; ?>">
            <?php
            break;

        case "change_maintainence_status":

            if($maintain_status==1)
            {
                $result=checkOpenMessage($maintain_status,$connKeywords);
                $count=$result["errMsg"][0]["count"];
                if($count>=1)
                {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = "You can not set two messages at a time ! Please close old one and try Again.";
                    echo json_encode($returnArr, true);
                    exit;
                }
            }
            $result=setMaintainStatus($maintain_id, $maintain_status,$connKeywords);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Changing Maintenance Mode Status ! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;

        case "update_maintain_data":

            if ($maintain_description=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please add description!";;
                echo(json_encode($returnArr));
                exit;
            }


            $result=updateMaintainsg($maintain_id,$maintain_description,$email,$connKeywords);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Maintenance  Details Data ! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;
    }


} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>