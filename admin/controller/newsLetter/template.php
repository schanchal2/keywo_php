<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');

//end helper

//other
require_once('../../core/errorMap.php');
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
if((in_array("write", $myp))) { ?>
    <div class="row ">
        <div class="col-xs-12">
            <div id="how_does_on_window" class="pull-right">
                <!-- <a class="btn btn-success" id="addNewUser" onclick="addTemplate();" style="margin-bottom: 15px;margin-right: 15px;background: #0299be;">
            Add New Record <span class="fa fa-plus-circle"></span>
            </a> -->
                <div class="input-group styled-select" style="z-index: 1;">
                    <button type="button" class="btn bg-darkblue text-white f-sz22 l-h9 p-5" onclick="addTemplate();" id="addNewUser">
                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="row">
        <div class="col-xs-12">
            <div id="newRegisteredUsers">
                <div id="getAllTemplates">
                </div>
            </div>
        </div>
    </div>
    <script>
    $(document).ready(function() {
        getAllTemplates();
    });

    function getAllTemplates() {
        $.ajax({
            type: "POST",
            dataType: "html",
            url: "../../controller/newsLetter/templateList.php",
            data: {},
            success: function(data) {
                $("#getAllTemplates").html("");
                $("#getAllTemplates").html(data);
            },
            error: function() {
                console.log("fail");
            }
        });

    }
    </script>
