<?php

session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/coreFunctions.php');

$DBConnection =new DBConnection();
$credential=$DBConnection->getDatabaseCredential("dbkeywords");

$table = 'newsletter_queue';

$primaryKey = 'id';

$columns = array(
    array( 'db' => 'created','dt' => 0),
    array( 'db' => 'category',  'dt' => 1 ),
    array( 'db' => 'sender',   'dt' => 2 ),
    array( 'db' => 'subject', 'dt' => 3 ),
    array( 'db' => 'content',   'dt' => 4 ),
    array( 'db' => 'no_of_user_sent',   'dt' => 5 ),
    array( 'db' => 'status',   'dt' => 6 ),
);

$sql_details = array(
    'user' => $credential["user"],
    'pass' => $credential["password"],
    'db'   => $credential["database"],
    'host' => $credential["host"],
    'port' => $credential["port"]
);


require( '../../backend_libraries/dataTable/ssp.class.php' );

$myData= json_encode(
    SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);
$myDataArray=json_decode($myData,true);
$myDatanew=$myDataArray["data"];

$result = array();
$result1 = array();

foreach ($myDatanew as $value) {

    $mydate = uDateTime("m-d-Y h:i:s", $value[0]);
    $datetime = explode(" ", $mydate);
    $result[0] = "<span class='date'>".$datetime[0]."</span><time>".$datetime[1]."</time>";
    $result[1] = $value[1];
    $result[2] = $value[2];
    $result[3] = $value[3];
    $result[4] = "<button class='btn btn-primary p-y-5' onclick=viewContent('".base64_encode($value[4])."') >View</button>";
    $result[5] = $value[5];
    $result[6] = $value[6];

    array_push($result1, $result);
}
$myDataArray['data'] = $result1;

echo json_encode($myDataArray);
?>



