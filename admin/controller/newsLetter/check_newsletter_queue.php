<?php
$docRootnew=explode("/admin/", dirname(__FILE__))[0]."/";
$docrootpath=$docRootnew."admin/";

require_once("$docrootpath/config/config.php");
//error_reporting(0);
require_once("$docrootpath/helpers/arrayHelper.php");
require_once("$docrootpath/helpers/stringHelper.php");
require_once("$docrootpath/config/db_config.php");
require_once("$docrootpath/helpers/coreFunctions.php");
require_once("$docrootpath/core/errorMap.php");
require_once("$docrootpath/helpers/cronHelper.php");
require_once("$docrootpath/helpers/email_helpers.php");
require_once("$docrootpath/model/newsletter/newsLetter.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();

$allNewslettrs=getNewsletterFromQueue($connKeywords);
if(noError($allNewslettrs))
{
    $allNewslettrs=$allNewslettrs["errMsg"];
    if(count($allNewslettrs)>0) {
        foreach ($allNewslettrs as $allNewslettrsData) {

            $status = $allNewslettrsData["category"];
            $content = $allNewslettrsData["content"];

            $id = $allNewslettrsData["id"];
            $emailSubject = $allNewslettrsData["subject"];
            $from = $allNewslettrsData["sender"];
            $limit = 1000;
            $time = 0; //start time
            $count = 1;
            $usersCount = 0;

            if ($status!="keyword_owners") {
                switch ($status) {
                    case "all_users":
                        $status = "";
                        break;

                    case "active_users":
                        $status = 1;
                        break;

                    case "inactive_users":
                        $status = 0;
                        break;

                    case "blocked_users":
                        $status = 2;
                        break;
                }
                sendNewsletterInBatch($time, $limit, $status, $from, $emailSubject, $content, $count, $usersCount, $connKeywords, $id);
            }else{
                $kWid=0;
                sendNewsletterInBatchKeywordUsers($kWid,$limit,$from,$emailSubject,$content,$count,$usersCount,$connKeywords,$id);
            }
        }
    }else{
        echo "No newsletter added in the queue.<br/>";
    }


}else
{
printArr($allNewslettrs);
}
?>