<?php

session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/coreFunctions.php');

$DBConnection =new DBConnection();
$credential=$DBConnection->getDatabaseCredential("dbkeywords");

if($_GET["status"]!="")
{
    $where="status={$_GET["status"]}";
}else
{
    $where="1=1";
}
$table = 'maintenance_mode';

$primaryKey = 'id';

$columns = array(
    array( 'db' => 'last_updated_on','dt' => 0),
    array( 'db' => 'message',  'dt' => 1 ),
    array( 'db' => 'status',   'dt' => 2 ),
    array( 'db' => 'id','dt' => 3),
);

$sql_details = array(
    'user' => $credential["user"],
    'pass' => $credential["password"],
    'db'   => $credential["database"],
    'host' => $credential["host"],
    'port' => $credential["port"]
);


require( '../../backend_libraries/dataTable/ssp.class.php' );
$data= json_encode(
    SSP::complex( $_GET, $sql_details, $table, $primaryKey, $columns ,$where)
);

$mydata=json_decode($data,true);
$mydatas=$mydata["data"];

$resultnew = array();
$result1 = array();

foreach($mydatas as $result)
{
    $msg_id=$result["3"];
    $status=$result['2'];

    $statusnew ="<script>    
            $(function() {
                $('.msg_switch_on_of').bootstrapToggle();
            });
        </script>";

    if ($status == "1") {
        $btn1="";
        $btn2="";
    $status="<input type='checkbox'  data-toggle='toggle' onchange=changeMaintainanceStatus('switch_". $msg_id."','".$status."') id='switch_".$msg_id."' class='msg_switch_on_of' data-size='mini' checked>";

}
if ($status == "0") {

    $btn1="<div class='btn btn_custom' onclick=editMaintain('{$msg_id}','{$status}') style='padding:0px'> <i class='fa fa-pencil fa-lg'></i></div> ";
    $btn2="<div class='btn btn_custom' onclick=setMaintainStatus('{$msg_id}','{$status}','delete_maintain') style='margin-right:3px'> <i class='fa fa-trash-o fa-lg'></i></div> ";

    $status="<input type='checkbox'  data-toggle='toggle' onchange=changeMaintainanceStatus('switch_". $msg_id."','".$status."') id='switch_".$msg_id."' class='msg_switch_on_of' data-size='mini'>";

}
    $status=$btn1.$btn2.$status.$statusnew;


                        $mydate = uDateTime("m-d-Y h:i:s", $result[0]);
                        $datetime = explode(" ", $mydate);



$resultnew["0"]="<span class='date'>".$datetime[0]."</span><time>".$datetime[1]."</time>";
    $resultnew["1"]=$result[1];
    $resultnew["2"]=$status;
    $resultnew["3"]=$msg_id;

    array_push($result1, $resultnew);
}

$mydata["data"]=$result1;

//printArr($mydata);
echo json_encode($mydata);

?>