<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other
require_once("../../model/template/templateModel.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();
if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {
    $tempID                    = cleanQueryParameter($connKeywords, cleanXSS($_POST["id"]));
    $deleteFaqById = deleteTemplateData($tempID, $connKeywords);
    if (noError($deleteFaqById)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] =$deleteFaqById['errMsg'];
    } else {
        $returnArr["errCode"] = 41;
        $returnArr["errMsg"] = $deleteFaqById['errMsg'];
    }
}

echo json_encode($returnArr,true);
?>
