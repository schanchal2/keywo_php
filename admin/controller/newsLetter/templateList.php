<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/template/templateModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once "../../model/geolocation/geoLocation.php";

/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$offset = 0;
$limit =5;
$getAllTemplateRequest = getAllTemplateRequest($offset, $limit,$tickedUserEmail,$connKeywords)["errMsg"];
//printArr($getAllTemplateRequest);
?>
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive" id="">
            <table class="table table-hover text-center newsletterTable" id="email_templates_table">
                <thead>
                    <tr>
                        <th class="dtTableHeadPadding no-sort ">Subject</th>
                        <th class="p-t-10 dtTableHeadPadding no-sort "> Date</th>
                        <?php
                        if((in_array("write", $myp))) {
                        ?>
                        <th class="dtTableHeadPadding no-sort "> Action</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach ($getAllTemplateRequest as $key=>$value) {
                ?>
                <tr>
                    <td><?= strlen($value['subject'])<=5 ? $value['subject'] : substr($value['subject'],0,5).'...';  ?></td>
                    <td>
                        <?php
                        $mydate = uDateTime("m-d-Y h:i:s", $value['created']);
                        $datetime = explode(" ", $mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>
                    <?php
                    if((in_array("write", $myp))) {
                    ?>

                    <td style="width:20%;text-align: center">
                        <span class="btn"><i onclick="addTemplate('<?= $key; ?>');" class="fa fa-pencil fa-lg" ></i> </span>
                        <span class="btn"><i onclick="deleteTemplate('<?= $key; ?>');" class="fa fa-trash-o fa-lg"></i> </span>
                    </td>
                    <?php } ?>
                </tr>
                <?php
                 }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $('#email_templates_table').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]

    });
</script>