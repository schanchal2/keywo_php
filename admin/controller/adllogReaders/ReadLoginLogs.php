<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../helpers/arrayHelper.php");
require_once("../../config/db_config.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../core/errorMap.php");
require_once("../../helpers/cronHelper.php");
require_once("../../model/logReader/readLogsUM.php");

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$fromdateinput = $_POST["fromdate"];
$todateinput = $_POST["todate"];

$filenameread = "_OClock_AdminLogin.xml";

global $logPath;
global $adminRoot;

$docrootpath = $logPath["admin"];
$finalrootUrl = $adminRoot . "logs/acl/";
$emailuser="";
$data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

?>


<div id="smartTable">
<table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left" id="loginlogTablein">
    <thead style="">
    <tr>
        <th style="width:20%;color:white;">Activity</th>
        <th style="width:60%;color:white;">Steps</th>
        <th style="width:20%;color:white;">Response</th>

    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($data as $xmlData) {
        ?>
        <tr style="color:<?php
        if(($xmlData->response->attributes()->errCode) == -1)
        {
            echo "green !important";
        }else
        {
            echo "red !important;";
        }
        ?>">
            <td style="text-align: left;">
                <span class="LogAct dtTableHeadPadding no-sort">Email:<?php echo $xmlData->attributes()->email; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                <span class="LogAct dtTableHeadPadding no-sort">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
            </td>
            <td style="text-align: left;">
                <?php
                if($xmlData->step!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step.'</span><br/>';
                }

                if($xmlData->step1!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step1.'</span><br/>';
                }

                if($xmlData->step2!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step2.'</span><br/>';
                }
                if($xmlData->step3!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step3.'</span><br/>';
                }

                if($xmlData->step4!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step4.'</span><br/>';
                }

                if($xmlData->step5!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step5.'</span><br/>';
                }

                if($xmlData->step6!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step6.'</span><br/>';
                }

                if($xmlData->step7!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step7.'</span><br/>';
                }

                if($xmlData->step8!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step8.'</span><br/>';
                }

                if($xmlData->step9!="")
                {
                    echo '<span class="LogAct">'.$xmlData->step9.'</span><br/>';
                }


        ?>
    </td>
    <td style="text-align: left;">
        <span class="LogAct">Error Code:<?php echo $xmlData->response->attributes()->errCode; ?></span><br/>
        <span class="LogAct">Error Message:<?php echo $xmlData->response->attributes()->errMsg; ?></span><br/>
    </td>
</tr>
    <?php
    }
    ?>

    </tbody>
</table>
</div>

<script>

    var table=$('#loginlogTablein').DataTable({
        "bInfo": true,
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": false,
        "info": true,
        "autoWidth": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]
    });

    $('#logintextSearch').keyup(function () {
        table.search( this.value ).draw();
    } );
</script>
