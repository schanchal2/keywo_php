<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

//Get page number from Ajax
if(isset($_POST["page"])){
    $page_number = (int) filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
}else{
    $page_number = 1; //if there's no page number, set it to 1
}
if($page_number==0)
{
    $page_number=1;
}
$item_per_page=5;
$get_total_rows=getMetaTagDataCount($connSearch)["errMsg"];

$total_pages = ceil($get_total_rows/$item_per_page);

//position of records
$page_position = (($page_number-1) * $item_per_page);
$lastpage = ceil($total_pages);


$results=getMetaTagData($connSearch, $page_position,$item_per_page);
$results=$results["errMsg"];
$count=count($results);

if($count==0)
{
    $page_number = $page_number-1; //if there's no page number, set it to 1
$item_per_page=5;
$get_total_rows=getMetaTagDataCount($connSearch)["errMsg"];

$total_pages = ceil($get_total_rows/$item_per_page);

//position of records
$page_position = (($page_number-1) * $item_per_page);
$lastpage = ceil($total_pages);

    $results=getMetaTagData($connSearch, $page_position,$item_per_page);
    $results=$results["errMsg"];
}

?>
<table id="userList" class="m-t-15 table table-bordered table-hover">
    <thead>
    <tr>
        <th>Sr.</th>
        <th>Page Name</th>
        <th>Page Title</th>
        <th>Meta Name</th>
        <th>Meta Content</th>
        <th>Actions</th>
    </tr>

    </thead>
    <tbody>
    <?php
    $count = ($page_number - 1) * $item_per_page;

    if($get_total_rows==0)
    {
        ?>
        <tr>
            <td style="text-align: center" colspan="6">No Data Found</td>
        </tr>
        <?php
    }else {
        foreach ($results as $value) {
            $count = $count + 1;
            print("<tr>");
            print("<td>" . $count . "</td>");
            print("<td>" . $value['page_name'] . "</td>");
            print("<td>" . $value['page_title'] . "</td>");
            print("<td>" . $value['meta_name'] . "</td>");
            print("<td>" . $value['meta_content'] . "</td>");
            print("<td> <a href=\"#\" class=\"\" onclick='editMetaData({$value['id']})' id=\"editMeta\" data-toggle=\"modal\" data-target=\"#editContent\"><span class=\"btn btn-xs btn-primary fa fa-edit\"></span></a>&nbsp;<a href=\"#\" id=\"deleteMeta\" onclick='deleteMetaData({$value['id']})' class=\"\"><span class=\"btn btn-xs btn-danger fa fa-close\"></span> </a></td>");
            print("</tr>");
        }
    }
    ?>
    </tbody>

    </tfoot>
</table>

<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage,$page_number);

function getPaginationData($lastpage,$pageno)
{
    echo '<input type="text" id="hiddenpagemetatag" name="hiddenpagemetatag" value="'.$pageno.'" hidden>';
   echo '<div style="" class="box-footer clearfix">';
    echo'<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


                if ($pageno > 1) {

                    $pagenum=1;
                    print('<li><a href="#"onclick=getMetaTagPagination("'.$pagenum.'")>&laquo;</a></li>');
                }

                if ($pageno > 1) {
                    $pagenumber=$pageno - 1;
                    print('<li><a href="#" onclick=getMetaTagPagination("'.$pagenumber.'")>Previous</a></li>');
                }

                if ($pageno == 1) {
                    $startLoop = 1;
                    $endLoop = ($lastpage < 5) ? $lastpage : 5;
                } else if ($pageno == $lastpage) {
                    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                    $endLoop = $lastpage;
                } else {
                    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
                }

                for ($i = $startLoop; $i <= $endLoop; $i++) {
                    if ($i == $pageno) {
                        print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
                    } else {
                        $pagenumber=$i;
                        print('<li><a href="#" onclick=getMetaTagPagination("'.$pagenumber.'")>' . $i . '</a></li>');
                    }
                }
                if ($pageno < $lastpage) {
                    $pagenumber=$pageno + 1;
                    print('<li><a href="#" onclick=getMetaTagPagination("'.$pagenumber.'")>Next</a></li>');

                }

                if ($pageno != $lastpage) {
                    print('<li><a href="#" onclick=getMetaTagPagination("'.$lastpage.'")>&raquo;</a></li>');
                }


echo '</ul>';
    echo '</div>';
}

function getMetaTagDataCount($connSearch) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as count_meta_tags FROM meta_tags;";
    $result = runQuery($query, $connSearch);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["count_meta_tags"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getMetaTagData($connSearch, $page_position,$item_per_page)
{

    $returnArr = array();

    $query = "SELECT * FROM meta_tags ORDER BY timestamp DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}
?>





