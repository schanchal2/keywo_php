<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/cms/landingContent_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$returnArr = array();
$type = $_POST["type"];
$contentTitle = cleanQueryParameter($connSearch, cleanXSS($_POST["contentTitle"]));
$contentVideoURL = cleanQueryParameter($connSearch, cleanXSS($_POST["contentVideoURL"]));
$contentShortDesc = trim(cleanQueryParameter($connSearch, cleanXSS($_POST["contentShortDesc"])));
$contentLongDesc = trim(cleanQueryParameter($connSearch, cleanXSS($_POST["contentLongDesc"])));
$tablename = cleanQueryParameter($connSearch, cleanXSS($_POST["table"]));
if($tablename=="cms_howToEarn")
{
    $contentType = "How to Earn?";
}else if($tablename=="cms_keywoFeatures"){
    $contentType = "Keywo Features";
}

$pageTitle = cleanQueryParameter($connSearch, cleanXSS($_POST["contentTitlePage"]));
$bannerImageName = cleanQueryParameter($connSearch, cleanXSS($_POST["contentBannerImage"]));


if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "insert":
            if (isset($_POST)) {

                if ($tablename == "cms_keywoFeatures") {
                    $cms_FeatureCount = selectTableRowCount("cms_keywoFeatures", $connSearch);
                    $cms_keywoFeatures = $cms_FeatureCount["errMsg"][0]["count"];

                    if ($cms_keywoFeatures == 10) {
                        $returnArr["errCode"] = "41";
                        $returnArr["errMsg"] = "You reached maximum Upload Limit!";
                        echo json_encode($returnArr, true);
                        exit;
                    }

                } else if ($tablename == "cms_howToEarn") {
                    $cms_howToEarn = selectTableRowCount("cms_howToEarn", $connSearch);
                    $cms_howToEarn = $cms_howToEarn["errMsg"][0]["count"];

                    if ($cms_howToEarn == 10) {
                        $returnArr["errCode"] = "41";
                        $returnArr["errMsg"] = "You reached maximum Upload Limit!";
                        echo json_encode($returnArr, true);
                        exit;
                    }
                }


                if ($contentTitle == "") {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Please add content title!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if (strlen($contentTitle) > 20) {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Only 20 characters allowed in content Title!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if ($contentVideoURL == "") {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Please add content video URL!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if ($contentShortDesc == "") {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Please add content Short Description!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if (strlen($contentShortDesc) > 50) {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Only 50 characters allowed in content short description!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if ($contentLongDesc == "") {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Please add content Long Description!";
                    echo json_encode($returnArr, true);
                    exit;
                }

                if (strlen($contentLongDesc) > 400) {
                    $returnArr["errCode"] = "101";
                    $returnArr["errMsg"] = "Only 400 characters allowed in content long description!";
                    echo json_encode($returnArr, true);
                    exit;
                }


                $result = addUpdateFeatureVideo($tablename, $contentTitle, $contentVideoURL, $contentShortDesc, $contentLongDesc, $contentType, $contentId, $connSearch);

                if ($result[errCode] == -1) {
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Data Updated Successfully";
                    echo json_encode($returnArr, true);

                } else {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = $result[errMsg];
                    echo json_encode($returnArr, true);
                }

            }
            break;
        case "delete":
            $contentId = $_POST["id"];
            $result = deleteFeatureVideo($tablename, $contentId, $connSearch);
            if ($result[errCode] == -1) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Deleted Successfully";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }
            break;
        case "edit":
            $contentId = $_POST["id"];
            $result = getVideoCMSData($tablename, $contentId, $connSearch);
            if ($result[errCode] == -1) {

                ?>
                <form id="editContentFormFeature" enctype="multipart/form-data" method="post"
                      name="editContentFormFeature" class="form-horizontal form-label-left">


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label">Features<span class="">*</span>
                                        </label>
                                        <div class="">

                                            <select class="form-control col-md-7 col-xs-12" name="contentType"
                                                    id="contentType" disabled>
                                                <option
                                                    value="How to Earn?" <?php if (($result['errMsg']['feature_type']) == "How to Earn?") {
                                                    echo "selected";
                                                }; ?> >How to Earn?
                                                </option>
                                                <option
                                                    value="Keywo Features" <?php if (($result['errMsg']['feature_type']) == "Keywo Features") {
                                                    echo "selected";
                                                }; ?> >Keywo Features
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                </div>
                                <div class="col-sm-4">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Content Title<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="text" value="<?php echo $result['errMsg']['content_title']; ?>"
                                                   class="form-control col-md-7 col-xs-12" name="contentTitle"
                                                   id="contentTitle">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Content Video URL<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="text"
                                                   value="<?php echo $result['errMsg']['content_video_link']; ?>"
                                                   class="form-control col-md-7 col-xs-12" name="contentVideoURL"
                                                   id="contentVideoURL">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Short Description<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <textarea class="form-control col-md-7 col-xs-12 noresize" style="text-align: left"
                                                      name="contentShortDesc" id="contentShortDesc">
                                                <?php echo trim($result['errMsg']['short_description']); ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Long Description<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <textarea class="form-control col-md-7 col-xs-12 noresize" style="text-align: left"
                                                      name="contentLongDesc" id="contentLongDesc">
                                            <?php echo trim($result['errMsg']['long_description']); ?>
                                            </textarea></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                            <?php
                            if ($tablename == "cms_keywoFeatures") {
                                $dataFn = "UpdatedFeatureKeywo";
                            } else {
                                $dataFn = "UpdatedFeature";
                            }
                            ?>
                            <button type="button" onclick="<?php echo $dataFn; ?>(<?php echo $contentId; ?>)"
                                    class="btn btn-success">Submit
                            </button>
                        </div>
                    </div>

                </form>
                <?php
            }
            break;
        case "update":
            $contentId = $_POST["id"];


            if ($contentTitle == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please add content title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (strlen($contentTitle) > 20) {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Only 20 characters allowed in content Title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if ($contentVideoURL == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please add content video URL title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if ($contentShortDesc == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please add content Short Description!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (strlen($contentShortDesc) > 50) {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Only 50 characters allowed in content short description!";
                echo json_encode($returnArr, true);
                exit;
            }

            if ($contentLongDesc == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please add content Long Description!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (strlen($contentLongDesc) > 400) {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Only 400 characters allowed in content long description!";
                echo json_encode($returnArr, true);
                exit;
            }


            $result = addUpdateFeatureVideo($tablename, $contentTitle, $contentVideoURL, $contentShortDesc, $contentLongDesc, $contentType, $contentId, $connSearch);

            if ($result[errCode] == -1) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Updated Successfully";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }
            break;
        case "editBanner":
            $contentId = $_POST["id"];
            $data = getBannerTitle($connSearch);
            if ($data[errCode] == -1) {

                $data = $data["errMsg"][0];
                $page_title = $data["page_title"];
                $banner_image = $data["banner_image"];

                ?>
                <form id="editContentFormTitle" enctype="multipart/form-data" method="post" name="editContentFormTitle"
                      class="form-horizontal form-label-left">


                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Page Title<span class="">*</span>
                                </label>
                                <div class="">
                                    <input type="text" value="<?php echo $page_title; ?>"
                                           class="form-control col-md-7 col-xs-12" name="contentTitlePage"
                                           id="contentTitlePage">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label">Banner Title<span class="">*</span>
                                </label>
                                <div class="">
                                    <input type="text" value="<?php echo $banner_image; ?>"
                                           class="form-control col-md-7 col-xs-12" name="contentBannerImage"
                                           id="contentBannerImage">
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                            <button type="button" onclick="UpdatedFeatureData(<?php echo $contentId; ?>)"
                                    class="btn btn-success">Submit
                            </button>
                        </div>
                    </div>

                </form>

                <?php
            }
            break;
        case "updateBanner":
            $contentId = $_POST["id"];


            if ($pageTitle == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please add Page title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if ($bannerImageName == "") {
                $returnArr["errCode"] = "101";
                $returnArr["errMsg"] = "Please Add Banner Title!";
                echo json_encode($returnArr, true);
                exit;
            }


            $result = UpdateBannerTitle($tablename, $pageTitle, $bannerImageName, $contentId, $connSearch);

            if ($result[errCode] == -1) {


                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Updated Successfully";
                $returnArr["pageTitle"] = $pageTitle;
                $returnArr["pageBanner"] = $bannerImageName;
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }
            break;
    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>
