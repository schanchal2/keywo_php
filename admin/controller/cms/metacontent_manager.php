<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/cms/meta_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$returnArr = array();
$type = $_POST["type"];

$pageName = cleanQueryParameter($connSearch, cleanXSS($_POST["pageName"]));
$pageTitle = cleanQueryParameter($connSearch, cleanXSS($_POST["pageTitle"]));
$metaName = cleanQueryParameter($connSearch, cleanXSS($_POST["metaName"]));
$metaContent = cleanQueryParameter($connSearch, cleanXSS(trim($_POST["metaContent"])));
$contentId = cleanQueryParameter($connSearch, cleanXSS($_POST["id"]));


if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "insert":


            if (!$pageName) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Name!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$pageTitle) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$metaName) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Meta Name!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$metaContent) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Meta Content!";
                echo json_encode($returnArr, true);
                exit;
            }

            $result = addUpdateMetatag($metaId, $pageName, $pageTitle, $metaName, $metaContent, $connSearch);
            if ($result[errCode] == -1) {

                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Updated Successfully";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }

            break;
        case "delete":
            $contentId = $_POST["id"];
            $result = removeMetatag($contentId, $connSearch);
            if ($result[errCode] == -1) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Deleted Successfully";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }
            break;

        case "edit":
            $contentId = $_POST["id"];
            $result = getMetaContent($contentId, $connSearch);

            if ($result[errCode] == -1) {

                ?>
                <form id="addContentFormMeta" enctype="multipart/form-data" method="post" name="addContentFormMeta"
                      class="form-horizontal form-label-left">

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Page Name<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="text" value="<?php echo $result["errMsg"]["page_name"]; ?>"
                                                   class="form-control col-md-7 col-xs-12" name="pageName"
                                                   id="pageName">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Page Title<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="text" value="<?php echo $result["errMsg"]["page_title"]; ?>"
                                                   class="form-control col-md-7 col-xs-12" name="pageTitle"
                                                   id="pageTitle">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Meta Name<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="text" value="<?php echo $result["errMsg"]["meta_name"]; ?>"
                                                   class="form-control col-md-7 col-xs-12" name="metaName"
                                                   id="metaName">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Meta Content<span class="">*</span>
                                        </label>
                                        <div class="">
                        <textarea class="form-control col-md-7 col-xs-12" name="metaContent" id="metaContent">
                            <?php echo $result["errMsg"]["meta_content"]; ?>
                          </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                            <button type="button" onclick="UpdatedFeature(<?php echo $result["errMsg"]["id"]; ?>)"
                                    class="btn btn-success">Submit
                            </button>
                        </div>
                    </div>

                </form>

                <?php
            }
            break;

        case "update":
            $contentId = $_POST["id"];

            if (!$pageName) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Name!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$pageTitle) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Title!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$metaName) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Meta Name!";
                echo json_encode($returnArr, true);
                exit;
            }

            if (!$metaContent) {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Please type Page Meta Content!";
                echo json_encode($returnArr, true);
                exit;
            }


            $result = addUpdateMetatag($contentId, $pageName, $pageTitle, $metaName, $metaContent, $connSearch);

            if ($result[errCode] == -1) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Data Updated Successfully";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result[errMsg];
                echo json_encode($returnArr, true);
            }
            break;
    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>