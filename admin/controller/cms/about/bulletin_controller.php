<?php
session_start();

//start config
require_once('../../../config/config.php');

require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/cms/about_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


$returnArr = array();


if(isset($_POST["operation_type"]))
{
    $type = cleanQueryParameter($connSearch, cleanXSS($_POST["operation_type"]));
}else{
    $type="";
}

if(isset($_POST["bulletin_description"]))
{
    $bulletin_description = cleanQueryParameter($connSearch, cleanXSS($_POST["bulletin_description"]));
}else{
    $bulletin_description="";
}


if(isset($_POST["bulletin_title"]))
{
    $bulletin_title = cleanQueryParameter($connSearch, cleanXSS($_POST["bulletin_title"]));
}else{
    $bulletin_title="";
}

if(isset($_POST["bulletin_id"]))
{
    $bulletin_id = cleanQueryParameter($connSearch, cleanXSS($_POST["bulletin_id"]));
}else{
    $bulletin_id="";
}

if(isset($_POST["bulletin_status"]))
{
    $bulletin_status = cleanQueryParameter($connSearch, cleanXSS($_POST["bulletin_status"]));
}else{
    $bulletin_status="";
}


$email=$_SESSION["admin_id"];

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "add_bulletin":
            $result=addBulletin($bulletin_description,$bulletin_title,$email);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in adding Announcement! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;
        case "delete_bulletin":

            $result=setBulletinStatus($bulletin_id, $bulletin_status);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Delete Announcement! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;

        case "edit_bulletin":

            $bulletinData= getBulletin("", "all", "data", "0", "0",$bulletin_id)["errMsg"][0];

            ?>
            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="edit_bulletin_title">Title:</label>
                <div class="col-xs-10">
                    <input type="text" id="edit_bulletin_title" value="<?= $bulletinData['title']; ?>" class="form-control bg-lightgray">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-xs-2 col-form-label" for="edit_bulletin_description">Description:</label>
                <div class="col-xs-10">
                            <textarea class="form-control bg-lightgray noresize scrollBar" rows="3"
                                       id="edit_bulletin_description">
                                <?= $bulletinData['announcement_text']; ?>
                            </textarea>
                </div>
            </div>
            <input type="hidden" id="hiddenBulletinId" value="<?= $bulletinData['_id']; ?>">
            <?php
            break;

        case "change_bulletin_status":
            $result=setBulletinStatus($bulletin_id, $bulletin_status);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Changing Announcement Status ! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;

        case "update_bulletin_data":
            $result=updateBulletin($bulletin_description,$bulletin_title,$email,$bulletin_id);
            if(noError($result))
            {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$result["errMsg"];

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Error in Updating Announcement Data ! Please Try Again.";

            }
            echo json_encode($returnArr, true);
            break;
    }


} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>