<?php
session_start();
//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');

//start model
require_once('../../../model/cms/about_model.php');

/*********************** get user Permissions ********************/
require_once "{$docRootAdmin}model/acl/checkAccess.php";
$myp=mypermissions($largest);
//printArr($myp);
/*********************** get user Permissions ********************/


if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    $connAdmin = createDBConnection("acl");
    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

    $bulletin_status = cleanQueryParameter($connAdmin, cleanXSS($_POST['bulletin_status']));
    $limit = cleanQueryParameter($connAdmin, cleanXSS($_POST['limit']));
    $email = cleanQueryParameter($connAdmin, cleanXSS($_POST['email']));

    $listtype = cleanQueryParameter($connAdmin, cleanXSS($_POST['listtype']));

    if ($listtype != "first") {
        $pageno = cleanQueryParameter($connAdmin, cleanXSS($_POST['pageno']));

        if ($pageno == "") {
            $pageno = 1;
        }

        $resultsPerPage = $limit;
        $start = ($pageno - 1) * $resultsPerPage;

        $bulletinCount = getBulletin($email, $bulletin_status, "count", "0", "0", "");
        $bulletinCount = $bulletinCount["errMsg"];

        $row_countw = $bulletinCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }

        if (!isset($pageno) || ($pageno == 1)) {
            $start = '0';
        } else {
            $start = $start;
        }

        $result = getBulletin($email, $bulletin_status, "data", $skip, $limit, "");
        $result = $result["errMsg"];
        if ($result == "No Bulletins Found") {
            $pageno = $pageno - 1; //if there's no page number, set it to 1

            if ($pageno <= 1) {
                $pageno = 1;
                $pn = $pageno - 1;
                $skip = $pn * $limit;
            } else if ($pageno > 1) {//$pageno>$lastpage
                //$pageno=$lastpage;
                $pn = $pageno - 1;
                $skip = $pn * $limit;
            }

            if (!isset($pageno) || ($pageno == 1)) {
                $start = '0';
            } else {
                $start = $start;
            }

            $result = getBulletin($email, $bulletin_status, "data", $skip, $limit, "");
            $result = $result["errMsg"];

        }

        if ($result=="No Bulletins Found") {
            $error = trim(strtolower($result));
        }
        if ($error == "no bulletins found") {
            ?>

            <table class="table text-center table-responsive table-bordered listHeader">
                <thead>
                <tr>
                    <th class="">Modified Date</th>
                    <th class="">Title</th>
                    <th class="">Description</th>
                    <th class="">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="4">
                        <?php

                            echo "No Result Found";

                        ?>

                    </td>
                </tr>
                </tbody>
            </table>

            <?php
            exit;
        }
    } else {


        $skip = 0;
        $bulletinCount = getBulletin($email, $bulletin_status, "count", "0", "0", "");
        $bulletinCount = $bulletinCount["errMsg"];

        $result = getBulletin($email, $bulletin_status, "data", $skip, $limit, "");
        $result = $result["errMsg"];

        if ($result=="No Bulletins Found") {
            $error = trim(strtolower($result));
        }
        if ($error == "no bulletins found" || $result=="Error in curl execution") {
            ?>

            <table class="table text-center table-responsive table-bordered listHeader">
                <thead>
                <tr>
                    <th class="">Modified Date</th>
                    <th class="">Title</th>
                    <th class="">Description</th>
                    <th class="">Action</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td colspan="4">
                        <?php

                            echo "No Result Found";

                        ?>

                    </td>
                </tr>
                </tbody>
            </table>

            <?php
            exit;
        }
        $resultsPerPage = $limit;

        $row_countw = $bulletinCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }
    }

    ?>

    <table class="table text-center table-responsive table-bordered listHeader">
        <thead>
        <tr>
            <th class="">Modified Date</th>
            <th class="">Title</th>
            <th class="">Description</th>
            <?php
            if((in_array("write", $myp))) {
            ?>
            <th class="">Action</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php

        foreach ($result as $result) {
            $bulletin_id = $result["_id"];
            ?>
            <tr>
                <td class="largeDate" style="width:12%;"><?= date("d-m-Y", $result["created_at"] / 1000); ?></td>
                <td class="purpule-blue" style="width:30%;"><?= $result["title"]; ?></td>
                <td class="purpule-blue" style="width:38%;"><?= $result["announcement_text"]; ?></td>
                <?php
                if((in_array("write", $myp))) {
                ?>
                <td style="width:20%;">
                    <?php
                    if ($result["status"] != "1") {
                        ?>
                        <div class="btn btn_custom" onclick="editBulletin('<?= $bulletin_id; ?>')" style="padding:0px">
                            <i
                                    class="fa fa-pencil fa-lg" aria-hidden="true" data-target="#edit_anouncement"
                                    data-toggle="modal"></i></div> &nbsp;&nbsp;
                        <div class="btn btn_custom"
                             onclick="setBulletinStatus('<?= $bulletin_id; ?>','delete','delete_bulletin')"
                             style="margin-right:3px"><i class="fa fa-trash-o fa-lg" aria-hidden="true"></i></div>

                        <?php
                    }
                    if ($result["status"] == "1") {
                        ?>
                            <input type="checkbox" checked data-toggle="toggle" onchange="changeBulletinStatus('switch_<?= $bulletin_id; ?>','<?= $result['status']; ?>')" id="switch_<?= $bulletin_id; ?>" class="bulletin_switch_on_of" data-size="mini">
                        <?php
                    }
                    if ($result["status"] == "2") {
                        ?>
                            <input type="checkbox" data-toggle="toggle" onchange="changeBulletinStatus('switch_<?= $bulletin_id; ?>','<?= $result['status']; ?>')" id="switch_<?= $bulletin_id; ?>" class="bulletin_switch_on_of" data-size="mini">

                        <?php
                    }


                    ?>
                </td>
                <?php } ?>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>

    <input type="text" id="hiddenpage" name="hiddenpage" value="<?php echo $pageno; ?>" hidden>
    <div style="" class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">

            <?php
            if ($pageno > 1) {

                $pagenum = 1;
                print('<li><a href="#"onclick=getBulletinListPagination("' . $pagenum . '","' . $bulletin_status . '","' . $limit . '")>&laquo;</a></li>');
            }

            if ($pageno > 1) {
                $pagenumber = $pageno - 1;
                print('<li><a href="#" onclick=getBulletinListPagination("' . $pagenumber . '","' . $bulletin_status . '","' . $limit . '")>Previous</a></li>');
            }

            if ($pageno == 1) {
                $startLoop = 1;
                $endLoop = ($lastpage < 5) ? $lastpage : 5;
            } else if ($pageno == $lastpage) {
                $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                $endLoop = $lastpage;
            } else {
                $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
            }

            for ($i = $startLoop; $i <= $endLoop; $i++) {
                if ($i == $pageno) {
                    print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
                } else {
                    $pagenumber = $i;
                    print('<li><a href="#" onclick=getBulletinListPagination("' . $pagenumber . '","' . $bulletin_status . '","' . $limit . '")>' . $i . '</a></li>');
                }
            }
            if ($pageno < $lastpage) {
                $pagenumber = $pageno + 1;
                print('<li><a href="#" onclick=getBulletinListPagination("' . $pagenumber . '","' . $bulletin_status . '","' . $limit . '")>Next</a></li>');

            }

            if ($pageno != $lastpage) {
                print('<li><a href="#" onclick=getBulletinListPagination("' . $lastpage . '","' . $bulletin_status . '","' . $limit . '")>&raquo;</a></li>');
            }
            ?>

        </ul>
    </div>

    <?php

} else {
    $url = $adminView . "acl/index.php";
    echo '<script>window.location.replace("' . $url . '");</script>';
}

?>
<script>

    $(function() {
        $('.bulletin_switch_on_of').bootstrapToggle();
    })

   function changeBulletinStatus(id,bulletin_status) {

       var bulletin_id = id.replace("switch_", "");

       if (bulletin_status == 2) {
           bulletin_status = "enabled";
       } else if(bulletin_status == 1) {
           bulletin_status = "disabled";
       }

       $.ajax({
           type: "POST",
           dataType: "json",
           url: "../../controller/cms/about/bulletin_controller.php",
           data: {
               bulletin_id: bulletin_id,
               operation_type: "change_bulletin_status",
               bulletin_status: bulletin_status
           },
           beforeSend: function () {
               $('#loadng-image').show();
           },
           success: function (data) {
               $('#loadng-image').hide();
               if (data["errCode"] != -1) {
                   $("#errors").html(bsAlert("danger", "There was error in changing announcement status."));

               } else {
                   loadBulletinState();
                   $("#errors").html(bsAlert("success", "Successfully changed announcement status."));

               }


           },
           error: function () {
               console.log("fail");
           }

       });

   }
</script>


                               