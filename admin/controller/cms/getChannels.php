<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


$channel = cleanQueryParameter($connSearch, cleanXSS($_POST["channel"]));
//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = 10;
$get_total_rows = getAllChannelsCount($connSearch,$channel)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllChannelsData($connSearch, $page_position, $item_per_page,$channel);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = 10;
    $get_total_rows = getAllChannelsCount($connSearch,$channel)["errMsg"];

    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllChannelsData($connSearch, $page_position, $item_per_page,$channel);
    $results = $results["errMsg"];


}


?>
<div id="userList" class="m-t-15">
    <table id="" class="table table-bordered table-hover">
        <thead>
        <thead style="background-color:#3c8dbc; height:10%;">
        <tr class="headings" style="background-color:#3c8dbc; color:white;">

            <th class="column-title" style="width:20%;display: table-cell;">Name </th>
            <th class="column-title" style="width:15%;display: table-cell;">Category </th>

            <th class="column-title" style="width:15%;display: table-cell;">Last Update</th>
            <th class="column-title" style="width:15%;display: table-cell; " >Update Frequency </th>
            <th class="column-title" style="width:10%;display: table-cell;">Country</th>
            <th class="column-title" style="width:10%;display: table-cell;">Action</th>

        </tr>

        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="6">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
                $data1 = explode(".", end(explode("/", ($value["file_url"]))));
                print("<tr>");
                print("<td>" . $data1[0] . "</td>");
                print("<td>" . $value['category'] . "</td>");
                print("<td>" . $value['last_update'] . "</td>");
                print("<td>" . $value['update_frequency'] . "</td>");
                print("<td>" . $value['country'] . "</td>");
                print("<td><a href=\"#\" class=\"btn btn-xs btn-primary\" id=\"editi\" onclick=\"editChannel('".$value['channel_id']."')\" data-toggle=\"modal\" data-target=\"#myModal\"><span class=\"btn btn-xs btn-primary fa fa-edit\"></span> </a> &nbsp;<a href=\"#\" id=\"deletei\" class=\"btn btn-xs btn-danger\" onclick=\"deleteChannel('".$value['channel_id']."')\"><span class=\"btn btn-xs btn-danger fa fa-close\"></span> </a></td>");
                print("</tr>");
            }
        }
        ?>
        </tbody>

        </tfoot>
    </table>
</div>
<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage, $page_number,$channel);

function getPaginationData($lastpage, $pageno,$channel)
{
    echo '<input type="text" id="hiddenagentpage" name="hiddenagentpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getAllChannels("' . $pagenum . '","' . $channel . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getAllChannels("' . $pagenumber . '","' . $channel . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getAllChannels("' . $pagenumber . '","' . $channel . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getAllChannels("' . $pagenumber . '","' . $channel . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getAllChannels("' . $lastpage . '","' . $channel . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

function getAllChannelsCount($connSearch,$channel) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as Count FROM channels";

    if($channel!="")
    {
        $query.=" where file_url LIKE '%{$channel}%'";
    }
    $result = runQuery($query, $connSearch);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["Count"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getAllChannelsData($connSearch, $page_position, $item_per_page,$channel)
{

    $returnArr = array();

    $query = "SELECT * FROM channels";

    if($channel!="")
    {
        $query.=" where file_url LIKE '%{$channel}%'";
    }

    $query.=" ORDER BY channel_id DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connSearch);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

?>


