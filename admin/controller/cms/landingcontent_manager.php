<?php
session_start();

//start config
require_once('../../config/config.php');

require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/cms/landingContent_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


$returnArr = array();
$type = $_POST["type"];
if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "insert":
            if (isset($_FILES)) {

                $data = selectTableRowCount("cms_slider", $connSearch);
                $countSliderold = $data["errMsg"][0]["count"];

                if ($countSliderold == 10) {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = "You reached maximum Upload Limit!";
                    echo json_encode($returnArr, true);
                    exit;
                }
                $consumerImageName = $_FILES["consumerImage"]["name"];
                $consumerImageTempName = $_FILES["consumerImage"]["tmp_name"];

                if (!$consumerImageTempName) {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = "Please select consumer image file to Upload!";
                    echo json_encode($returnArr, true);
                    exit;
                }


                $consumerImageType = pathinfo($consumerImageName, PATHINFO_EXTENSION);

                $uploaderImageName = $_FILES["uploaderImage"]["name"];
                $uploaderImageTempName = $_FILES["uploaderImage"]["tmp_name"];

                if (!$uploaderImageTempName) {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = "Please select Uploader image file to Upload!";
                    echo json_encode($returnArr, true);
                    exit;
                }

//                $contentorder = $_POST[contentorder];
//                if (!$contentorder) {
//                    $returnArr["errCode"] = "41";
//                    $returnArr["errMsg"] = "Please select order for Images";
//                    echo json_encode($returnArr, true);
//                    exit;
//                }

                $uploaderImageType = pathinfo($uploaderImageName, PATHINFO_EXTENSION);


                $target_dir = "../../../images/landingPage/consumerUploader/";

                $milliseconds = round(microtime(true) * 1000);

                $consumerImageNameNew = "consumerImage-" . $milliseconds .".". $consumerImageType;
                $uploaderImageNameNew = "uploaderImage-" . $milliseconds .".". $uploaderImageType;

                $consumerImageNameNewdb =$rootUrl."images/landingPage/consumerUploader/consumerImage-" . $milliseconds .".". $consumerImageType;
                $uploaderImageNameNewdb =$rootUrl."images/landingPage/consumerUploader/uploaderImage-" . $milliseconds .".". $uploaderImageType;

                $contentId="undefined";
                $result = addUpdateConsumerUploaderImage($consumerImageNameNewdb, $uploaderImageNameNewdb, "NULL", $contentId, $connSearch);
                if ($result[errCode] == -1) {
                    move_uploaded_file($consumerImageTempName, $target_dir . $consumerImageNameNew);
                    move_uploaded_file($uploaderImageTempName, $target_dir . $uploaderImageNameNew);

                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Data Updated Successfully";
                    echo json_encode($returnArr, true);

                } else {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = $result[errMsg];
                    echo json_encode($returnArr, true);
                }

            }
            break;
        case "delete":

            $data = selectTableRowCount("cms_slider", $connSearch);
            $countSliderold = $data["errMsg"][0]["count"];

            if ($countSliderold == 1) {

                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "You can not delete All Images!";
                echo json_encode($returnArr, true);
            } else {
                $contentId = $_POST["id"];
                $result = getConsumerUploaderImage($contentId, $connSearch);
                $conImage = $result["errMsg"]["consumer_image"];
                $upImage = $result["errMsg"]["producer_image"];
                $target_dir = "../../../images/landingPage/consumerUploader/";

                $result = deleteConsumerUploaderImage($contentId, $connSearch);
                if ($result[errCode] == -1) {

                    unlink($target_dir . $conImage);
                    unlink($target_dir . $upImage);
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Data Deleted Successfully";
                    echo json_encode($returnArr, true);

                } else {
                    $returnArr["errCode"] = "41";
                    $returnArr["errMsg"] = $result[errMsg];
                    echo json_encode($returnArr, true);
                }
            }

            break;

        case "edit":
            $contentId = $_POST["id"];
            $result = getConsumerUploaderImage($contentId, $connSearch);
            if ($result[errCode] == -1) {

                ?>
                <form id="editContentForm" enctype="multipart/form-data" method="post" name="editContentForm"
                      class="form-horizontal form-label-left">

                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Consumer Image<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="file" class="form-control col-md-7 col-xs-12"
                                                   name="consumerImageEdit" id="consumerImageEdit">
                                            <div id="consumerImageHolderEdit" class="thumb-image">
                                                <img
                                                    src="../../../images/landingPage/consumerUploader/<?php echo $result['errMsg']['consumer_image']; ?>"></img>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                </div>
                                <div class="col-sm-5">
                                    <div class="form-group">
                                        <label class="control-label">Uploader Image<span class="">*</span>
                                        </label>
                                        <div class="">
                                            <input type="file" class="form-control col-md-7 col-xs-12"
                                                   name="uploaderImageEdit" id="uploaderImageEdit">
                                            <div id="uploaderImageHolderEdit" class="thumb-image">
                                                <img
                                                    src="../../../images/landingPage/consumerUploader<?php echo $result['errMsg']['producer_image']; ?>"></img>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-sm-12 text-right">
                            <button type="reset" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                            <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>

                </form>

                <?php
            }
            break;

        case "sliderCount":
            $data = selectTableRowCount("cms_slider", $connSearch);
            $countSlider = $data["errMsg"][0]["count"] + 1;
            $countSliderold = $data["errMsg"][0]["count"];

            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = $countSliderold;
            echo json_encode($returnArr, true);
            break;

        case "cms_howToEarnCount":
            $cms_howToEarn = selectTableRowCount("cms_howToEarn", $connSearch);
            $cms_howToEarn = $cms_howToEarn["errMsg"][0]["count"];

            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = $cms_howToEarn;
            echo json_encode($returnArr, true);
            break;

        case "cms_keywoFeatures":
            $cms_FeatureCount = selectTableRowCount("cms_keywoFeatures", $connSearch);
            $cms_keywoFeatures = $cms_FeatureCount["errMsg"][0]["count"];

            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = $cms_keywoFeatures;
            echo json_encode($returnArr, true);
            break;
    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>