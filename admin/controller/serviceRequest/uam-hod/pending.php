<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../model/serviceRequest/serviceRequestModel.php');
require_once('../../../core/errorMap.php');

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$srNo           = cleanQueryParameter($connKeywords, cleanXSS($_POST["srNo"]));
$limit          = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$timestamp      = cleanQueryParameter($connKeywords, cleanXSS($_POST["timestamp"]));
$srType         = cleanQueryParameter($connKeywords, cleanXSS($_POST["srType"]));
$userName       = cleanQueryParameter($connKeywords, cleanXSS($_POST["userName"]));
$raisedBy       = cleanQueryParameter($connKeywords, cleanXSS($_SESSION["admin_id"]));
$priority       = cleanQueryParameter($connKeywords, cleanXSS($_POST["priority"]));
$holdBy         = cleanQueryParameter($connKeywords, cleanXSS($_POST["holdBy"]));
$type           = cleanQueryParameter($connKeywords, cleanXSS($_POST["type"]));
$requestStatus  = cleanQueryParameter($connKeywords, cleanXSS($_POST["requestStatus"]));

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllRequestCount($connKeywords,$timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllRequestData($page_position, $limit, $connKeywords, $timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllRequestCount($connKeywords,$timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllRequestData($page_position, $limit, $connKeywords, $timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus);
    $results = $results["errMsg"];

}

?>

<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive">
        <thead>
        <tr>
            <th><input id="select_all" name="" type="checkbox"><label for="select_all" class="fa"></label></th>
            <th>Timestamp</th>
            <th>SR Number</th>
            <th>SR Type</th>
            <th>Username</th>
            <th>Raised By</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th>Hold By</th>

        </tr>
        </thead>

        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="10">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
//            printArr($value);
                ?>
                <tr>
                    <td><input class="user_checkbox" id="user_checkbox" name="user_checkbox[]" value="" type="checkbox">
                        <label for="user_checkbox" class="fa"></label>
                    </td>
                    <td>
                        <?php
                        $mydate = uDateTime("m-d-Y h:i:s", $value['request_date']);
                        $datetime = explode(" ", $mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>

                    </td>
                    <td><?php echo $value['request_no'] ?></td>
                    <td><?php echo $value['request_type'] ?></td>
                    <td><?php echo $value['username'] ?></td>
                    <td><?php echo $value['raised_by'] ?></td>
                    <td><?php echo $value['req_priority'] ?></td>
                    <td><?php echo $value[''] ?></td>
                    <td><?php echo $value['hold_by'] ?></td>

                </tr>
            <?php }}?>
        </tbody>
    </table>

    <?php

    getPaginationData($lastpage, $page_number, $limit,$timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus);
    function getPaginationData($lastpage, $pageno, $limit,$timestamp,$srNo,$srType,$userName,$raisedBy,$priority,$holdBy,$type,$requestStatus)
    {

        ?>
        <span class="pull-left recordCountsShow-styled-select"> Show
<select id="LimitedResultData">
<option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
<option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
<option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
<option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
<option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
<option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
</select>
            <button type="button" class="btn text-white border-none btn-success width">Approve</button>
            <button type="button" class="btn text-white border-none btn-warning width">Hold</button>
            <button type="button" class="btn text-white border-none btn-danger width">Reject</button>
</span>


        <?php
        echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
        echo '<div style="" class="box-footer clearfix">';
        echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


        if ($pageno > 1) {

            $pagenum = 1;
            print('<li><a href="#"onclick=getNewUAMHODData("' . $pagenum . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$priority.'","'.$holdBy.'","'.$requestStatus.'","' . $limit . '","' . $type . '")>&laquo;</a></li>');
        }

        if ($pageno > 1) {
            $pagenumber = $pageno - 1;
            print('<li><a href="#" onclick=getNewUAMHODData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$priority.'","'.$holdBy.'","'.$requestStatus.'","' . $limit . '","' . $type . '")>Previous</a></li>');
        }

        if ($pageno == 1) {
            $startLoop = 1;
            $endLoop = ($lastpage < 5) ? $lastpage : 5;
        } else if ($pageno == $lastpage) {
            $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
            $endLoop = $lastpage;
        } else {
            $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
            $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
        }

        for ($i = $startLoop; $i <= $endLoop; $i++) {
            if ($i == $pageno) {
                print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
            } else {
                $pagenumber = $i;
                print('<li><a href="#" onclick=getNewUAMHODData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$priority.'","'.$holdBy.'","'.$requestStatus.'","' . $limit . '","' . $type . '")>' . $i . '</a></li>');
            }
        }
        if ($pageno < $lastpage) {
            $pagenumber = $pageno + 1;
            print('<li><a href="#" onclick=getNewUAMHODData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$priority.'","'.$holdBy.'","'.$requestStatus.'","' . $limit . '","' . $type . '")>Next</a></li>');

        }

        if ($pageno != $lastpage) {
            print('<li><a href="#" onclick=getNewUAMHODData("' . $lastpage . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$priority.'","'.$holdBy.'","'.$requestStatus.'","' . $limit . '","' . $type . '")>&raquo;</a></li>');
        }


        echo '</ul>';
        echo '</div>';
    }

    ?>



    <script>
        $("#LimitedResultData").change(function(){
            getUAMHODData();
        });


    </script>