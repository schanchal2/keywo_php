<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/serviceRequest/serviceRequestModel.php');

//end other

//printArr($_POST);


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$userName  = $_SESSION["admin_name"];
$email     = $_SESSION["admin_id"];
$type      = cleanQueryParameter($connKeywords, cleanXSS($_POST["type"]));
$hold_by   = cleanQueryParameter($connKeywords, cleanXSS($_POST["hold_by"]));

$result = getRequestMMAnalytics($hold_by,$type,$connKeywords)["errMsg"][0];

?>


<table class="table  text-center table-responsive">
    <thead>
    <tr>
        <th class="text-center text-white">MM</th>
        <th class="text-center text-white">SR Received</th>
        <th class="text-center text-white">Hold/Pending</th>
        <th class="text-center text-white">Approved</th>
        <th class="text-center text-white">Rejected</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td id=""> <?= $userName; ?></td>
        <td id=""> <?= isset($result["total_count"])?$result["total_count"]:0; ?></td>
        <td id=""> <?= isset($result["total_open"])?$result["total_open"]:0; ?></td>
        <td id=""> <?= isset($result["total_resolved"])?$result["total_resolved"]:0; ?></td>
        <td id=""> <?= isset($result["rejected"])?$result["rejected"]:0; ?></td>

    </tr>
    </tbody>
</table>
