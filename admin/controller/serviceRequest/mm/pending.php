<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../model/serviceRequest/serviceRequestModel.php');
require_once('../../../core/errorMap.php');

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$srNo           = cleanQueryParameter($connKeywords, cleanXSS($_POST["srNo"]));
$limit          = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$timestamp      = cleanQueryParameter($connKeywords, cleanXSS($_POST["timestamp"]));
$srType         = cleanQueryParameter($connKeywords, cleanXSS($_POST["srType"]));
$userName       = cleanQueryParameter($connKeywords, cleanXSS($_POST["userName"]));
$raisedBy       = cleanQueryParameter($connKeywords, cleanXSS($_POST["raisedBy"]));
$type           = cleanQueryParameter($connKeywords, cleanXSS($_POST["type"]));
$requestStatus  = cleanQueryParameter($connKeywords, cleanXSS($_POST["requestStatus"]));
$agentIDS       = cleanQueryParameter($connKeywords, cleanXSS($_POST["agentIDS"]));
$types          = cleanQueryParameter($connKeywords, cleanXSS($_POST["types"]));
$escalateBy     = cleanQueryParameter($connKeywords, cleanXSS($_POST["escalateBy"]));

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllmmsmRequestCount($connKeywords,$timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllmmsmRequestData($page_position, $limit, $connKeywords, $timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllmmsmRequestCount($connKeywords,$timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllmmsmRequestData($page_position, $limit, $connKeywords, $timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$escalateBy);
    $results = $results["errMsg"];

}

?>

<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive">
        <thead>
        <tr>
            <?php if($types == 'mm' || $types == 'sm') { ?>
                <th>
                    <input id="select_all" type="checkbox" name="">
                    <label for="select_all" class="fa"></label>
                </th>
            <?php } ?>
            <th>Timestamp</th>
            <th>SR Number</th>
            <th>SR Type</th>
            <th>Username</th>
            <th>Due Date</th>
            <th>Priority</th>
            <?php if($types == 'mm' || $types == 'sm') { ?>
                <th>Raised By</th>
            <?php } ?>

        </tr>
        </thead>

        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="10">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
//            printArr($value);
                ?>
                <tr>
                    <?php if($types == 'mm' || $types == 'sm') { ?>
                        <td>
                            <input class="user_checkbox" id="user_checkbox<?php echo $value["request_no"]; ?>"
                                   type="checkbox" name="user_checkbox[]"
                                   value="<?php echo $value["request_no"]; ?>">
                            <label for="user_checkbox<?php echo $value["request_no"]; ?>" class="fa"></label>
                        </td>
                    <?php } ?>
                    <td>

                        <?php
                        $mydate = uDateTime("m-d-Y h:i:s", $value['request_date']);
                        $datetime = explode(" ", $mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>


                        </td>
                    <td><a href="javascript:;" data-toggle="modal" id="raise_sr_modal_open" data-target="#raise_sr_model" data-raiseBy="<?php echo $value['raised_by'] ?>" data-sr_type="<?php echo $value['request_type']?>"  data-raised_by="<?php echo $value['raised_by']?>" data-request_no="<?php echo $value['request_no'] ?>" ><?php echo $value['request_no'] ?> </a></td>
                    <td><?php echo $value['request_type'] ?></td>
                    <td><?php echo $value['username'] ?></td>
                    <td><?php echo $value[''] ?></td>
                    <td><?php echo $value['req_priority'] ?></td>
                    <?php if($types == 'mm' || $types == 'sm') { ?>
                        <td><?php echo $value['raised_by'] ?></td>
                    <?php } ?>

                </tr>
            <?php }}?>
        </tbody>
    </table>

    <?php

    getPaginationData($lastpage, $page_number, $limit,$timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$types,$get_total_rows);
    function getPaginationData($lastpage, $pageno, $limit,$timestamp,$srNo,$srType,$userName,$raisedBy,$type,$requestStatus,$agentIDS,$types,$get_total_rows)
    {

        ?>
        <span class="pull-left recordCountsShow-styled-select"> Show
<select id="LimitedResultData">
<option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
<option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
<option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
<option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
<option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
<option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
</select>
<?php if($get_total_rows == 0){ }else{ ?>
    <button href="javascript:;" data-toggle="modal" data-target="#approve" class="btn text-white border-none btn-success width approved" disabled="disabled">Approve </button>
    <?php if($types == 'mm') { ?>
    <button href="javascript:;" data-toggle="modal" data-target="#escalation" class="btn text-white border-none btn-warning width escalation" disabled="disabled">Escalate </button><?php } ?>
    <button href="javascript:;" data-toggle="modal" data-target="#reject" class="btn text-white border-none btn-danger width rejects" disabled="disabled">Reject </button> <?php }?>
            
</span>


        <?php
        echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
        echo '<div style="" class="box-footer clearfix">';
        echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


        if ($pageno > 1) {

            $pagenum = 1;
            print('<li><a href="#"onclick=getNewMMData("' . $pagenum . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$requestStatus.'","'.$agentIDS.'","' . $limit . '","' . $type . '")>&laquo;</a></li>');
        }

        if ($pageno > 1) {
            $pagenumber = $pageno - 1;
            print('<li><a href="#" onclick=getNewMMData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$requestStatus.'","'.$agentIDS.'","' . $limit . '","' . $type . '")>Previous</a></li>');
        }

        if ($pageno == 1) {
            $startLoop = 1;
            $endLoop = ($lastpage < 5) ? $lastpage : 5;
        } else if ($pageno == $lastpage) {
            $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
            $endLoop = $lastpage;
        } else {
            $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
            $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
        }

        for ($i = $startLoop; $i <= $endLoop; $i++) {
            if ($i == $pageno) {
                print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
            } else {
                $pagenumber = $i;
                print('<li><a href="#" onclick=getNewMMData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$requestStatus.'","'.$agentIDS.'","' . $limit . '","' . $type . '")>' . $i . '</a></li>');
            }
        }
        if ($pageno < $lastpage) {
            $pagenumber = $pageno + 1;
            print('<li><a href="#" onclick=getNewMMData("' . $pagenumber . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$requestStatus.'","'.$agentIDS.'","' . $limit . '","' . $type . '")>Next</a></li>');

        }

        if ($pageno != $lastpage) {
            print('<li><a href="#" onclick=getNewMMData("' . $lastpage . '","' . $timestamp . '","'.$srNo.'","' . $srType . '","'.$userName.'","'.$raisedBy.'","'.$requestStatus.'","'.$agentIDS.'","' . $limit . '","' . $type . '")>&raquo;</a></li>');
        }


        echo '</ul>';
        echo '</div>';
    }

    ?>



    <script>


        $(function () {
            var _last_selected = null, checkboxes = $("#userDataList :checkbox");
            checkboxes.click(function (e) {
                var ix = checkboxes.index(this), checked = this.checked;


                if (e.shiftKey && ix != _last_selected) {
                    checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                        .each(function () {
                            this.checked = checked

                        });
                    _last_selected = null;

                } else {
                    _last_selected = ix

                }
            })
        });

        $(document).ready(function () {
            $('#select_all').on('click', function () {
                if (this.checked) {
                    $('.user_checkbox').each(function () {
                        this.checked = true;
                        $(this).closest('tr').toggleClass("highlight", this.checked);
                    });
                } else {
                    $('.user_checkbox').each(function () {
                        this.checked = false;
                        $(this).closest('tr').toggleClass("highlight", this.checked);
                    });
                }
            });

            $('.user_checkbox').on('click', function () {
                if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                    $('#select_all').prop('checked', true);
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                } else {
                    $('#select_all').prop('checked', false);
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                }
            });
        });

        $(function() {
            $(".user_checkbox").click(function(){
                $('.rejects').prop('disabled',$('input.user_checkbox:checked').length == 0);
                $('.approved').prop('disabled',$('input.user_checkbox:checked').length == 0);
                $('.escalation').prop('disabled',$('input.user_checkbox:checked').length == 0);            
            });
        });

        $(function(){
        $("#select_all").click(function(){
            $('.rejects').prop('disabled',$('input#select_all:checked').length == 0);
            $('.escalation').prop('disabled',$('input#select_all:checked').length == 0);
            $('.approved').prop('disabled',$('input#select_all:checked').length == 0);
           });
        });


        $("#LimitedResultData").change(function(){
            getMMData();
        });

    </script>
