<?php

/**
 **********************************************************************************
 *                  addServiceQueryController.php
 * ********************************************************************************
 *      This controller is used for Raising/Generating ServiceRequest Tickets.
 */

session_start();

require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once "../../../helpers/arrayHelper.php";
require_once "../../../helpers/stringHelper.php";
require_once "../../../helpers/coreFunctions.php";
require_once "../../../core/errorMap.php";
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$sessionEmail   = $_SESSION["admin_id"];
$pageName        = isset($_POST["callingPageName"])?cleanQueryParameter($conn, cleanXSS($_POST["callingPageName"])):"";
$kycTicketID     = isset($_POST["kycTicketID"])?cleanQueryParameter($conn, cleanXSS($_POST["kycTicketID"])):"";
$kycUsername     = isset($_POST["kycUsername"])?cleanQueryParameter($conn, cleanXSS($_POST["kycUsername"])):"";
$reqPriority     = isset($_POST["reqPriority"])?cleanQueryParameter($conn, cleanXSS($_POST["reqPriority"])):"";
$domainName      = isset($_POST["domainName"])?cleanQueryParameter($conn, cleanXSS($_POST["domainName"])):"";
$reason          = isset($_POST["reason"])?cleanQueryParameter($conn, cleanXSS($_POST["reason"])):"";
$kycagentComment = isset($_POST["kycagentComment"])?cleanQueryParameter($conn, cleanXSS($_POST["kycagentComment"])):"";
$txnID           = isset($_POST["txnID"])?cleanQueryParameter($conn, cleanXSS($_POST["txnID"])):"";
$ipaddress       = isset($_POST["ipaddress"])?cleanQueryParameter($conn, cleanXSS($_POST["ipaddress"])):"";

$kycDocDate           = isset($_POST["kycDocDate"])?cleanQueryParameter($conn, cleanXSS($_POST["kycDocDate"])):"";

if(isset($kycDocDate)) {
    $aadhaarcard = isset($_POST["aadhaarcard"]) ? cleanQueryParameter($conn, cleanXSS($_POST["aadhaarcard"])) : "";
    $voters_identity_card = isset($_POST["voters_identity_card"]) ? cleanQueryParameter($conn, cleanXSS($_POST["voters_identity_card"])) : "";
    $driving_license = isset($_POST["driving_license"]) ? cleanQueryParameter($conn, cleanXSS($_POST["driving_license"])) : "";
    $bank_passbook = isset($_POST["bank_passbook"]) ? cleanQueryParameter($conn, cleanXSS($_POST["bank_passbook"])) : "";
    $panCard = isset($_POST["panCard"]) ? cleanQueryParameter($conn, cleanXSS($_POST["panCard"])) : "";
    $passport = isset($_POST["Passport"]) ? cleanQueryParameter($conn, cleanXSS($_POST["Passport"])) : "";
    $kycData=array();
    $kycData["voters_identity_card"]=$voters_identity_card;
    $kycData["driving_license"]=$driving_license;
    $kycData["bank_passbook"]=$bank_passbook;
    $kycData["panCard"]=$panCard;
    $kycData["Passport"]=$passport;
    $kycData["aadhaarcard"]=$aadhaarcard;

    if(isset($kycData)) {
        $kycDoc = json_encode($kycData);
    }
}else{
    $kycDoc="";
}

if(isset($_POST["tickets"]))
{
    $tickets        = $_POST["tickets"];
}else{
    $tickets="";
}

if(isset($_POST["status"]))
{
    $request_status= $_POST["status"];
}else{
    $request_status="";
}

if(isset($_POST["escalate"]))
{
    $escalate        = $_POST["escalate"];
}else{
    $escalate="";
}
if(isset($_POST["comments"]))
{
    $comments        = $_POST["comments"];
}else{
    $comments="";
}

if(isset($_POST["reqStatusBy"]))
{
    $reqStatusBy        = $_POST["reqStatusBy"];
}else{
    $reqStatusBy="";
}


if(isset($_POST["escalateby"]))
{
    $escalateby        = $_POST["escalateby"];
}else{
    $escalateby="";
}


if(isset($_POST["raisedBy"]))
{
    $raisedBy        = $_POST["raisedBy"];
}else{
    $raisedBy="";
}

if(isset($_POST["hod_comments"]))
{
    $hod_comments        = $_POST["hod_comments"];
}else{
    $hod_comments="";
}

if(isset($_POST["mm_comments"]))
{
    $mm_comments        = $_POST["mm_comments"];
}
else{
    $mm_comments="";
}



$queryList   = array(

    "callingPageName" => $pageName,
    "kycTicketID"     => $kycTicketID,
    "kycUsername"     => $kycUsername,
    "reqPriority"     => $reqPriority,
    "domainName"      => $domainName,
    "reason"          => $reason,
    "kycagentComment" => $kycagentComment,
    "txnID"           => $txnID,
    "ipaddress"       => $ipaddress
);

$requestList = array(

    "status"         => $request_status,
    "escalate"       => $escalate,
    "comments"       => $comments,
    "reqStatusBy"    => $reqStatusBy,
    "escalateby"     => $escalateby,
    "raisedBy"       => $raisedBy,
    "hod_comments"   => $hod_comments,
    "mm_comments"    => $mm_comments
);

$updateServiceRequestList = updateServiceRequestList($tickets,$requestList,$sessionEmail,$queryList,$kycDoc,$kycDocDate,$conn);

echo json_encode($updateServiceRequestList,true);

?>