<?php

/**
 **********************************************************************************
 *                  addServiceQueryController.php
 * ********************************************************************************
 *      This controller is used for Raising/Generating ServiceRequest Tickets.
 */

session_start();

require_once "../../../config/config.php";
require_once "../../../config/db_config.php";
require_once "../../../helpers/arrayHelper.php";
require_once "../../../helpers/stringHelper.php";
require_once "../../../helpers/coreFunctions.php";
require_once "../../../core/errorMap.php";
require_once "../../../model/serviceRequest/serviceRequestModel.php";

$conn = createDBConnection('dbkeywords');
if (noError($conn)) {
    $conn = $conn["connection"];
} else {
    print_r("Database Error");
}

$sessionEmail = $_SESSION["admin_id"];

$ticket_id = $_POST["ticket_id"];

$returnArr = array();
$getEmail = getUserEmail($ticket_id, $conn);
if (noError($getEmail)) {

    $getEmail=$getEmail["errMsg"];
    $returnArr['errCode'] = -1;
    $returnArr['errMsg'] = $getEmail;
} else {
    $returnArr['errCode'] = 5;
    $returnArr['errMsg'] = "Failed to get Data";
}
echo json_encode($returnArr, true);

?>