<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/email_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

global $inactive;
$email=$_SESSION["admin_id"];


if (isset($_SESSION["timeout"])) {
    $sessionTTL = time() - $_SESSION["timeout"];
    if ($sessionTTL > $inactive) {

        session_destroy();

        if($email!="")
        {
            updateLogoutTime($email,$connAdmin);
        }
        $data="-1";

    }else
    {
        if($email!="")
        {
            updateLastActivityTime($_SESSION["admin_id"],$connAdmin);
        }
        $data= "Updated Time";
    }
}


echo $data;

?>