<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
//end other


$connAcl = createDBConnection("acl");
noError($connAcl) ? $connAcl = $connAcl["connection"] : checkMode($connAcl["errMsg"]);

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = 10;
$get_total_rows = getGroupListCount($connAcl)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getGroupListData($connAcl, $page_position, $item_per_page);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = 5;
    $get_total_rows = getGroupListCount($connAcl)["errMsg"];

    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getGroupListData($connAcl, $page_position, $item_per_page);
    $results = $results["errMsg"];


}

?>
<table id="userList" class="m-t-15 table table-bordered table-hover">
    <thead>
    <tr>
        <!--<th class='text-center' width="5%">
            <input id="select_allgroup" class="checkboxStyle" type="checkbox" name="">
            <label for="select_allgroup" class="fa"></label>
        </th>-->
        <th> Name</th>
        <th> Rights</th>
        <th> Modules</th>
        <th> Sub-Modules</th>
        <th> Actions</th>
    </tr>

    </thead>
    <tbody>
    <?php
    $count = ($page_number - 1) * $item_per_page;

    if ($get_total_rows == 0) {
        ?>
        <tr>
            <td style="text-align: center" colspan="6">No Data Found</td>
        </tr>
        <?php
    } else {
        foreach ($results as $value) {
            $group_name = $value["group_name"];
            $group_id = $value["group_id"];
            $count = $count + 1;
            print("<tr>");
            //print("<td class='text-center'><input class=\"user_checkboxgroup user_checkbox\" id=\"user_checkboxgroup" . md5($group_id) . "\" type=\"checkbox\" name=\"user_checkboxgroup[]\" value=" . $group_id . "><label for=\"user_checkboxgroup" . md5($group_id) . "\" class=\"fa\"></label></td>");
            print("<td>" . $group_name . "</td>");
            print("<td>");


            $dt = str_split($value["group_rights"]);
            foreach ($dt as $newdt) {
                if ($newdt == 1) {
                    echo "Read<br/>";
                }

                if ($newdt == 2) {
                    echo "Write<br/>";

                }


            }


            print("</td>");
            $resultnew = json_decode($value["right_on_module"], true);
            $count = 0;
            print("<td>");
            foreach ($resultnew as $groupsd) {
                $count++;
                echo $count . "]" . $groupsd . "<br/>";
            }
            print("</td>");
            print("<td>");
            $resultdsubModules = explode(",", $value["right_on_submodule"]);


            $datas = getPageList($resultdsubModules, $connAcl);
            $count = 0;
            if (strtolower($group_name) == "superadmin") {
                echo "All";

            } else {
                foreach ($datas as $nd) {
                    echo ($count + 1) . "] " . $nd . "<br/>";
                    $count++;
                }
            }
            print("</td>");
            if(in_array($group_name,$adminsDefined)) {
                if (strtolower($group_name) == "superadmin") {
                    print("<td class=\"text-center\">NA</td>");
                }else {
                    print("<td class=\"text-center\"> <a href=\"javascript:;\" onclick=\"editGroup('" . $group_id . "');\" data-toggle=\"modal\" data-target=\"#editnewadmindataadmingroup\" class=\"btn btn-xs btn-primary fa fa-edit cleanLabel\"></a></td>");
                }
                }else{
                print("<td class=\"text-center\"> <a href=\"javascript:;\" onclick=\"editGroup('" . $group_id . "');\" data-toggle=\"modal\" data-target=\"#editnewadmindataadmingroup\" class=\"btn btn-xs btn-primary fa fa-edit cleanLabel\"></a>&nbsp;<a href=\"#\" onclick=\"deleteGroup('" . $group_id . "')\" id=\"\" class=\"\"><span class=\"btn btn-xs btn-danger fa fa-close\"></span> </a></td>");
            }
            print("</tr>");
        }
    }
    ?>
    </tbody>

    </tfoot>
</table>

<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getGroupListDataPagination($lastpage, $page_number);

function getGroupListDataPagination($lastpage, $pageno)
{
    echo '<input type="text" id="hiddenagentpagegroup" name="hiddenagentpagegroup" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getGroupListDataPagination("' . $pagenum . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getGroupListDataPagination("' . $pagenumber . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getGroupListDataPagination("' . $pagenumber . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getGroupListDataPagination("' . $pagenumber . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getGroupListDataPagination("' . $lastpage . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

function getGroupListCount($connAcl) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as groupsCount FROM groups;";
    $result = runQuery($query, $connAcl);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["groupsCount"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getGroupListData($connAcl, $page_position, $item_per_page)
{

    $returnArr = array();

    $query = "SELECT * FROM groups ORDER BY group_id DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connAcl);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

?>


<script>

    $(document).ready(function () {
        $('#select_allgroup').on('click', function () {
            if (this.checked) {
                $('.user_checkboxgroup').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkboxgroup').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkboxgroup').on('click', function () {
            if ($('.user_checkboxgroup:checked').length == $('.user_checkboxgroup').length) {
                $('#select_allgroup').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_allgroup').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>