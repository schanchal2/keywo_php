<?php
session_start();
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


$user = strtolower(cleanQueryParameter($connAdmin, cleanXSS(($_POST["username"]))));
$pass = cleanQueryParameter($connAdmin, cleanXSS($_POST["password"]));

$returnArr = array();
//for xml writing essential
$xmlProcessor = new xmlProcessor();
$xmlfilename = "AdminLogin.xml";
$logStorePathAdmin = $logPath["admin"];
$xmlArray = initializeXMLLog($user);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
// xml essential


$xml_data['step']["data"] = 'Start Admin Login';

$returnArr = array();


if ($connAdmin) {
    $xml_data['step1']["data"] = '1.Successfull establising database connection';
} else {
    $xml_data['step1']["data"] = '1. Failed to  establising database connection';
}


$pass = sha1Md5DualEncryption($pass);
$role = cleanQueryParameter($connAdmin, cleanXSS(($_POST["role"])));

if ($_POST["password"] != "") {
    $yes = "Yes";
} else {
    $yes = "No";
}

$xml_data['step2']["data"] = "2. Get Parameters for Login 1]User Email-: " . $user . " 2]Password-: " . $yes;

if (empty($_POST["username"])) {
    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "Please Enter Email !!";
    $xml_data['step3']["data"] = " Check is email entered -: Email is not entered Exit.";

    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/index.php");
    exit;
} else {
    if (!filter_var($user, FILTER_VALIDATE_EMAIL)) {
        $returnArr["errCode"] = "1";
        $returnArr["errMsg"] = "Please enter valid email!!";

        $xml_data['step4']["data"] = "4. Check is email valid -: Email is not valid Exit.";
        $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
        $_SESSION["loginError"]=$returnArr["errMsg"];
        header("location:../../view/acl/index.php");
        exit;
    }
}

if (empty($_POST["password"])) {
    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "Please Enter Password!!";

    $xml_data['step5']["data"] = "5. Check is password blank -: Password is Blank Exit.";
    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/index.php");
    exit;
}


$xml_data['step3']["data"] = "3. Email not balnk proceed to login";
$xml_data['step4']["data"] = "4. Email is valid proceed to login";
$xml_data['step5']["data"] = "5. Password not balnk proceed to login";

$adminStatus = "1";
$result = ValidateAdminLogin($user, $pass, $adminStatus, $connAdmin);

if ($result["errCode"] == "-1") {
    $xml_data['step6']["data"] = "6. Run validate login function :success.";

} else {
    $xml_data['step6']["data"] = "6. Run validate login function :Failed.";
}


$newdata = $result["errMsg"];
$newemail = $newdata["admin_email"];
$newepass = $newdata["admin_password_hash"];
$newrole = $newdata["admin_rolecode"];
$name = ucfirst($newdata["admin_first_name"]) . " " . ucfirst($newdata["admin_last_name"]);
$profilepic = $newdata["admin_profile_pic"];
$adminStatus = $newdata["admin_status"];
$agentID = $newdata["admin_id"];
$groups = $newdata["group_id"];


$xml_data['step7']["data"] = "7. Admin Name: " . $name . " Before Authentication.";

if($adminStatus=="")
{
    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "Invalid Username and Password!!";

    $xml_data['step8']["data"] = "8. Invalid Username and Password";
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/index.php");
    exit;
}

if ($adminStatus != 1 && $adminStatus == 0) {
    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "You have not activated your account.Please Activate by link send to your email address!";
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/index.php");
    exit;
}
if (($user == $newemail) && ($pass == $newepass)) {

    $xml_data['step8']["data"] = "8. Admin Name: " . $name . " After Authentication Success.";
    updateLoginTime($newemail,$connAdmin);
    updateLastActivityTime($newemail,$connAdmin);
    $_SESSION['user'] = "admin";
    $_SESSION['admin'] = 1;
    $_SESSION['admin_name'] = $name;
    $_SESSION["admin_id"] = $newemail;
    $_SESSION["admin_profile_pic"] = $profilepic;
    $_SESSION["agent_id"] = $agentID;
    $_SESSION["timeout"] = time();
    $_SESSION["timezone"] = $adminTimezone;
    //$_SESSION["mygroups"] = $groups;

    $xml_data['step9']["data"] = "9. Admin Session Started Success Redirect to Dashboard.";

    /******************** for logs user analytics ****************/
    $responseArr["errCode"]="-1";
    $responseArr["errMsg"]="Admin Session Started Success Redirect to Dashboard";
    $xml_data['response']["data"] = "";
    $xml_data['response']["attributes"] = $responseArr;
    /******************** for logs user analytics ****************/

    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

    $returnArr["errCode"] = "-1";
    $returnArr["errMsg"] = "dashboard.php";


    header("location:../../view/dashboard/dashboard.php");

} else {

    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "Invalid Username and Password!!";

    $xml_data['step8']["data"] = "8. Invalid Username and Password";
    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/index.php");
}


?>
































