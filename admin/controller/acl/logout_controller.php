<?php
session_start();
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$adminView = "../../index.php";
$email=$_SESSION["admin_id"];
session_destroy();

if($email!="")
{
    updateLogoutTime($email,$connAdmin);
}
print("<script>");
print("window.location='" . $adminView . "'");
print("</script>");

exit;

?>
