<?php
session_start();
//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other


$connAcl = createDBConnection("acl");
noError($connAcl) ? $connAcl = $connAcl["connection"] : checkMode($connAcl["errMsg"]);

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = 5;
$get_total_rows = getKeywoInfoDataCount($connAcl)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getKeywoInfoData($connAcl, $page_position, $item_per_page);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = 5;
    $get_total_rows = getKeywoInfoDataCount($connAcl)["errMsg"];

    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getKeywoInfoData($connAcl, $page_position, $item_per_page);
    $results = $results["errMsg"];


}

?>
<div id="userList" class="m-t-15">
    <table id="" class="table table-bordered table-hover">
        <thead>
        <tr>
            <!--<th class='text-center' width="5%">
                <input id="select_allstaff" class="checkboxStyle" type="checkbox" name="">
                <label for="select_allstaff" class="fa"></label>
            </th>-->
            <th> Name</th>
            <th> Email ID</th>
            <th> Role</th>
            <th> Live Status</th>
            <th> Last Login</th>
            <th> Last Logout</th>
            <th> Rights</th>
            <th> Actions</th>
        </tr>

        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="6">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
                $userEmail = $value["admin_email"];
                $count = $count + 1;
                print("<tr>");
                //print("<td class='text-center'><input class=\"user_checkboxStaff user_checkbox\" id=\"user_checkboxStaff" . md5($value['admin_email']) . "\" type=\"checkbox\" name=\"user_checkboxStaff[]\" value=" . $value['admin_email'] . "><label for=\"user_checkboxStaff" . md5($value['admin_email']) . "\" class=\"fa\"></label></td>");
                print("<td>" . $value['admin_first_name'] . " " . $value['admin_last_name'] . "</td>");
                print("<td>" . $value['admin_email'] . "</td>");
                print("<td>" . $value['group_id'] . "</td>");
                $last_logout_time = strtotime($value['last_logout_time']);

                $lastActiveTime = strtotime($value['last_activity_time']);
                $nowtime= time();

                if($last_logout_time>$lastActiveTime)
                {
                    print("<td class='text-center' style=''><span class=\"fa fa-circle userOffline\"></span></td>");
                }else
                {

                   // if (($nowtime-$lastActiveTime ) < (15 * 60))
                   $diffrence=(int)round(abs($nowtime - $lastActiveTime) / 60,2);
                    if($diffrence<20)
                    {
                        print("<td class='text-center' style=''><span class=\"fa fa-circle userOnline\"></span></td>");
                    }elseif($diffrence>20){
                        print("<td class='text-center' style=''><span class=\"fa fa-circle userOffline\"></span></td>");
                    }
                }
                
                $last_login_time = strtotime($value['last_login_time']);
                print("<td class='text-center'><span class='date'> " . uDateTime("d-m-Y", $value['last_login_time']) . " </span><time> " . uDateTime("h:i:s", $value['last_login_time']) . "</time></td>");

                print("<td class='text-center'><span class='date'> " . uDateTime("d-m-Y", $value['last_logout_time']) . " </span><time> " . uDateTime("h:i:s", $value['last_logout_time']) . "</time></td>");

                echo "<td>";

                if ($userEmail == "malvik@searchtrade.com" || $userEmail == "admin@keywo.com") {
                    echo "View<br/>Edit";
                }else{
                $dt = str_split($value["rights"]);
                if ($value["rights"] == "") {
                    echo "NA&nbsp;&nbsp";
                }
                foreach ($dt as $newdt) {
                    if ($newdt == 1) {
                        echo "View<br/>";
                    }

                    if ($newdt == 2) {
                        echo "Edit<br/>";

                    }
                }



                    ?>

                    <a href="javascript:;"
                       onclick="changeRights('<?php echo cleanDisplayParameter($connAcl, $value["admin_email"]); ?>');"
                       data-toggle="modal"
                       data-target="#editnewadmindataadminrights"> <i
                                style="color:red;width:3.285714em;"
                                class="fa fa-fw fa-edit cleanLabel">Edit</i></a>
                    <?php
                }


                echo "</td>";

                if($userEmail =="admin@keywo.com" || $userEmail=="malvik@searchtrade.com" || $userEmail == $_SESSION["admin_id"])
                {
                    print("<td class='text-center'> NA</td>");

                }else{
                    print("<td> <a href=\"javascript:;\" onclick=\"editAdmin('" . $userEmail . "');\" data-toggle=\"modal\" data-target=\"#editnewadmindataadmin\" class=\"btn btn-xs btn-primary fa fa-edit cleanLabel\"></a>&nbsp;<a href=\"#\" onclick=\"deleteAdmin('" . $userEmail . "')\" id=\"deleteidatakeywo\" class=\"\"><span class=\"btn btn-xs btn-danger fa fa-close\"></span> </a></td>");

                }

               print("</tr>");
            }
        }
        ?>
        </tbody>

        </tfoot>
    </table>
</div>
<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage, $page_number);

function getPaginationData($lastpage, $pageno)
{
    echo '<input type="text" id="hiddenagentpage" name="hiddenagentpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getAgentListsAcl("' . $pagenum . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getAgentListsAcl("' . $pagenumber . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getAgentListsAcl("' . $pagenumber . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getAgentListsAcl("' . $pagenumber . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getAgentListsAcl("' . $lastpage . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

function getKeywoInfoDataCount($connAcl) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as admin_userCount FROM admin_user;";
    $result = runQuery($query, $connAcl);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["admin_userCount"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getKeywoInfoData($connAcl, $page_position, $item_per_page)
{

    $returnArr = array();

    $query = "SELECT * FROM admin_user ORDER BY admin_id DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connAcl);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}

?>


<script>

    $(document).ready(function () {
        $('#select_allstaff').on('click', function () {
            if (this.checked) {
                $('.user_checkboxStaff').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkboxStaff').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkboxStaff').on('click', function () {
            if ($('.user_checkboxStaff:checked').length == $('.user_checkboxStaff').length) {
                $('#select_allstaff').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_allstaff').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>