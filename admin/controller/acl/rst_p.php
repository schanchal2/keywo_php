<?php
session_start();
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/email_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
require_once('../../model/geolocation/geoLocation.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
//end other

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["admin"];
global $noReplyEmail;
global $adminRoot;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$xmlArray = initializeXMLLog($_SESSION["admin_id"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
// xml essential


$email = strtolower(cleanQueryParameter($connAdmin, cleanXSS($_POST["email"])));

/*
     if type=="resetpassword"
     This Controller run following function

    changePassword($email,md5($token),$connAdmin);

    which have 3 parameters
    1]$email -  Email for user we are Sending Password
    2]$token - Token for user Password  withb md5 hashed
    3]Connection for admin database


    it's resend new password and update password hash in admin_user table.

 */


if ($_POST["email"] == "") {
    $returnArr["errCode"] = "2";
    $returnArr["errMsg"] = "Email is mandatory !!";
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/rsp.php");
    exit;
} else {
    if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
        $returnArr["errCode"] = "2";
        $returnArr["errMsg"] = "Please enter valid email!!";
        $_SESSION["loginError"]=$returnArr["errMsg"];
        header("location:../../view/acl/rsp.php");
        exit;
    }
}

$result = isAdminExists($email, $connAdmin);

$result = $result["errMsg"];
$result = $result["admin_email"];
if ($result != $email) {
    $returnArr["errCode"] = "2";
    $returnArr["errMsg"] = "Email is not registered !!";
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/rsp.php");
    exit;
}


$token1 = generateRandomString($length = 30);
$token = $token1;
$token = sha1Md5DualEncryption($token);
$result = changePassword($email, $token, $connAdmin);

if ($result["errCode"] == -1) {

    $result = getAdminData($email, $connAdmin);
    //printArr($result);
    $result = $result["errMsg"];
    $admin_id = $result["admin_id"];
    $admin_email = $result["admin_email"];
    $fname = $result["admin_first_name"];
    $lname = $result["admin_last_name"];

    $to = $admin_email;
    $username = $fname . " " . $lname;//'.$randonnumber.'
    $emailSubject = "Your New Password ";
    $content = '<br/>Your Keywo Admin account password has been updated.</p>

<p>Your password is: <span style="font-weight:bold;">' . $token1 . '</span></p><p>Please change your password after login!!';

    $parameters["name"]=$username;
    $parameters["content"]=$content;
    $sendMail=sendEmail($to, $noReplyEmail, $emailSubject,"admin_default.html",$parameters);

    $returnArr["errCode"] = -1;
    $returnArr["errMsg"] = "Admin User password request submitted please check your email for further process !!";

    $_SESSION["loginErrorSuccess"]=$returnArr["errMsg"];
    header("location:../../view/acl/rsp.php");
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In resetting password !!";
    $_SESSION["loginError"]=$returnArr["errMsg"];
    header("location:../../view/acl/rsp.php");
}
?>








