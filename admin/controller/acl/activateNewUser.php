<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/email_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
require_once('../../model/geolocation/geoLocation.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
//end other

//for xml writing essential
$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["admin"];
global $noReplyEmail;
global $adminRoot;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


$datastring = $_SERVER['QUERY_STRING'];
$queryStringForTransaction = decodeURLparameter($datastring);
$id = $queryStringForTransaction["id"];
$email = strtolower($queryStringForTransaction["email"]);
$token = $queryStringForTransaction["token"];


$result = getAdminData($email, $connAdmin);
$datae = $result["errMsg"];
$emailtomatch = $datae["admin_email"];
$idtomatch = $datae["admin_id"];
$tokentomatch = $datae["auth_token"];


if (isset($email) && isset($id) && isset($token)) {

    if (($email == $emailtomatch) && ($id == $idtomatch) && ($token == $tokentomatch)) {
        $token = generateRandomString($length = 35);

        $result = updateToken($email, $token, $connAdmin);

        $result = getAdminData($email, $connAdmin);
        $result = $result["errMsg"];
        $fname = utf8_decode($result["admin_first_name"]);
        $lname = utf8_decode($result["admin_last_name"]);
        $hash = generateRandomString();

        activateUser($email, $hash, $connAdmin);

        $to = $email;
        $username = $fname . " " . $lname;//'.$randonnumber.'
        $emailSubject = "Your Account is Activated on SearchTrade Admin Section ";
        $content = '<br/>Your SearchTrade Admin account has been Activated.</p>

<p>Your Password is: <span style="font-weight:bold;">' . $hash . '</span></p><p>Please change your password after first login!!';


        $parameters["name"]=$username;
        $parameters["content"]=$content;
        $sendMail=sendEmail($to, $noReplyEmail, $emailSubject,"admin_default.html",$parameters);

        header("Location:" . $adminView . "index.php?ermsg=Your Account is activated.Check your email for password.");
    } else {
        header("Location:" . $adminView . "index.php?ermsg=Invalid Token you are not valid user!!");
    }
} else {

    header("Location:" . $adminView . "index.php?ermsg=Invalid Token you are not valid user!!");
}


?>

