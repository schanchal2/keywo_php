<?php

session_start();

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

//start config
    require_once('../../config/config.php');
    require_once('../../config/db_config.php');
//end config

//start helper
    require_once('../../helpers/arrayHelper.php');
    require_once('../../helpers/stringHelper.php');
    require_once('../../helpers/date_helpers.php');
    require_once('../../helpers/cryptoHelper.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/deviceHelper.php');
    require_once('../../helpers/image_helpers.php');
//end helper

//other
    require_once('../../core/errorMap.php');
    require_once('../../model/acl/acl_model.php');
//end other

    $connAdmin = createDBConnection("acl");
    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


    global $adminView;

    $adminEmail = $_SESSION["admin_id"];

    if (isset($_FILES['image'])) {
        $errors = array();
        //$file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_tmp = $_FILES['image']['tmp_name'];

        $file_type = $_FILES['image']['type'];
        $file_ext = strtolower(end(explode('.', $_FILES['image']['name'])));
        $expensions = array("jpeg", "jpg", "png", "svg");
        $data = generateRandomString();

        $file_name = $adminEmail . "_" . $data . "." . $file_ext;

        if (in_array($file_ext, $expensions) === false) {
            $errors[] = "Extension not allowed, please choose a JPEG or PNG file.";
        }

        if ($file_size > 2097152) {
            $errors[] = 'Image File size must be excately 2 MB';
        }

        if (empty($errors) == true) {


            $target_dir = "../../uploads/adminProfilePics/";
            move_uploaded_file($file_tmp, $target_dir . $file_name);
            changeAdminProfilePic($adminEmail, $file_name, $connAdmin);

            $target_file = "../../uploads/adminProfilePics/{$file_name}";
            $resized_file = "../../uploads/adminProfilePics/$file_name";
            $wmax = 160;
            $hmax = 160;
            image_resizer($target_file, $resized_file, $wmax, $hmax, $file_ext);

        } else {
            //print_r($errors);

            foreach ($errors as $err) {
                $error = $err;

            }

            $_SESSION["errorsInProPic"] = $error;
        }
    }
    $link = $adminView . "dashboard/dashboard.php";
    print("<script>");
    print('window.location.href = "' . $link . '";');
    print("</script>");
    exit;
} else {
    print("<script>");
    print('window.location.href = "' . $adminView . '";');
    print("</script>");
}


?>
