<?php
session_start();
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/cryptoHelper.php');
require_once('../../helpers/email_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/acl/acl_model.php');
require_once('../../model/geolocation/geoLocation.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
//end other


//for xml writing essential
$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["admin"];
global $noReplyEmail;
global $adminRoot;

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$xmlArray = initializeXMLLog($_SESSION["admin_id"]);
$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];
// xml essential

$fname = cleanQueryParameter($connAdmin, cleanXSS($_POST["fname"]));
$lname = cleanQueryParameter($connAdmin, cleanXSS($_POST["lname"]));
$email = strtolower(cleanQueryParameter($connAdmin, cleanXSS($_POST["email"])));
$group = $_POST["group"];
$oldp = cleanQueryParameter($connAdmin, cleanXSS($_POST["oldp"]));
$newp = cleanQueryParameter($connAdmin, cleanXSS($_POST["newp"]));

$rnewp = cleanQueryParameter($connAdmin, cleanXSS($_POST["rnewp"]));
$type = cleanQueryParameter($connAdmin, cleanXSS($_POST["type"]));
$ipaddress = cleanQueryParameter($connAdmin, cleanXSS($_POST["ipaddress"]));
$ipOwner = cleanQueryParameter($connAdmin, cleanXSS($_POST["ipOwner"]));
$allmodules = $_POST["allmodules"];
$id = cleanQueryParameter($connAdmin, cleanXSS($_POST["id"]));
$groupname = cleanQueryParameter($connAdmin, cleanXSS($_POST["groupname"]));
$modulename = $_POST["modulename"];
$submodulename = $_POST["submodulename"];

$modulename = json_encode($modulename, true);
$permissions = $_POST["permissions"];
$userpermission = $_POST["userpermission"];
$groupid = cleanQueryParameter($connAdmin, cleanXSS($_POST["groupid"]));
$permissions = implode("", $permissions);

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {


    switch ($type) {
        case "addnewip":


            $xmlfilename = "AddNewIPAccess.xml";

            $xml_data['step1']["data"] = 'Add New IP in Admin Section';

            if ($_POST["ipaddress"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = " ip address is Required !!";

                $xml_data['step2']["data"] = 'ip address is Required!! -error!!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = 'ip address is Required!! success!!';
            }

            if ($_POST["ipOwner"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = " IP Owner Name is Required !!";

                $xml_data['step2']["data"] = 'IP Owner Name is Required!! -error!!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = 'IP Owner Name is Required!! success!!';
            }




            if (filter_var($ipaddress, FILTER_VALIDATE_IP) === false) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = " Please add Valid IP address!!";

                $xml_data['step3']["data"] = 'IP is not Valid!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step3']["data"] = 'IP is not In-Valid!';
            }

            $checkip = getipaddress($ipaddress, $connAdmin);
            $data = $checkip["errMsg"];
            $ip = $data["ip_address"];
            if ($ipaddress == $ip) {

                $returnArr["errCode"] = -8;
                $returnArr["errMsg"] = " new IP address added alrady present in database !!";

                $xml_data['step4']["data"] = 'new IP address added alrady present in database !! --YES!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
                exit;

            } else {
                $xml_data['step4']["data"] = 'new IP address added alrady present in database !! --NO!';
            }


            $result = addipaddress($ipaddress,$ipOwner, $connAdmin);
            if ($result["errCode"] == -1) {

                $_SESSION['ipaccess'] = "IP address added to system success !!";
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = " new IP address added sucessfully !!";


                $xml_data['step5']["data"] = 'new IP address added sucessfully !!';
                $xml_data['ipOwner']["data"] =$ipaddress;
                $xml_data['ipAddress']["data"] = $ipOwner;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]=$returnArr["errMsg"];
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin Error in ip address adding !!";

                $xml_data['step5']["data"] = 'new IP address adding failed !!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }


            break;
        case "addgroup":
            $xmlfilename = "AddNewGroup.xml";

            $xml_data['step1']["data"] = 'Add New Group in Admin Section!!';

            if ($_POST["groupname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Group Name is Required !!";

                $xml_data['step2']["data"] = 'Admin User Group Name is Required!!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = 'Admin User Group Name is Required!!';
            }


            $result = checkgroupadded($groupname, $connAdmin);
            $result = $result["errMsg"];
            $resultname = $result["group_name"];

            if (strtolower($resultname) == strtolower($groupname)) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "This Group Allready Added !!";

                $xml_data['step3']["data"] = 'This Group Allready Added !!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = 'This Group Allready not Added  !!';
            }

            if ($_POST["permissions"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin Group Permissions are required !!";

                $xml_data['step4']["data"] = 'Admin Group Permissions are required -error!!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step4']["data"] = 'Admin Group Permissions added';
            }


            if ($_POST["modulename"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please Select Module on you want to add this group!!";

                $xml_data['step5']["data"] = 'Please Select Module on you want to add this group!! -error!!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step5']["data"] = 'Module to add group Selected!!';
            }

            $xml_data['groupName']["data"] = $groupname;


            $checkmodule = json_decode($modulename, true);

            foreach ($checkmodule as $value) {
                if ($value == "") {
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Please Select Module on you want to add this group!!";
                    echo(json_encode($returnArr));
                    exit;
                }

            }


            if (empty($submodulename)) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please Select at list one submodule !!";

                $xml_data['step6']["data"] = 'Please Select submodule on you want to add this group!! -error!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step6']["data"] = 'Submodules selected to add in group!!';
            }


            foreach ($submodulename as $value) {
                if ($value == "") {
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Please Select Sub-Module on you want to add this group!!";
                    echo(json_encode($returnArr));
                    exit;
                }

            }
            $result = submitUserdatagroup($submodulename, $groupname, $modulename, $permissions, $connAdmin);


            if ($result["errCode"] == -1) {
                $_SESSION['greenerrorgroup'] = "Admin group added to system success !!";
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin new Group added sucessfully !!";

                $xml_data['step7']["data"] = 'run submitUserdatagroup function !! Admin new Group added sucessfully !!';

                $xml_data['groupName']["data"] = $groupname;
                $xml_data['modules']["data"] = implode(",",json_decode($modulename,true));
                $xml_data['submodules']["data"] = implode(",",$submodulename);
                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]=$returnArr["errMsg"];
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/

                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin Error in Adding Group !!";

                $xml_data['step8']["data"] = 'run submitUserdatagroup function !! Admin new Group adding failed !!';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }


            break;
        case "chngpass1":

            if ($_POST["oldp"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "old password is mandatory  !!";
                echo(json_encode($returnArr));
                exit;
            }


            $result = getAdminData($email, $connAdmin);
            $data = $result["errMsg"];
            $passwordhash = $data["admin_password_hash"];

            $oldpassword = sha1Md5DualEncryption($oldp);
            if ($passwordhash != $oldpassword) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "This is not old password !!";
                echo(json_encode($returnArr));
                exit;
            }

            break;
        case "chngpass":


            if ($_POST["oldp"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "old password is mandatory  !!";
                echo(json_encode($returnArr));
                exit;
            }


            $result = getAdminData($email, $connAdmin);
            $data = $result["errMsg"];
            $passwordhash = $data["admin_password_hash"];

            $oldpassword = sha1Md5DualEncryption($oldp);
            if ($passwordhash != $oldpassword) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "This is not old password !!";
                echo(json_encode($returnArr));
                exit;
            }

            if (strlen($_POST["newp"]) < 10) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "New Password should be greater than 10 character !!";
                echo(json_encode($returnArr));
                exit;
            }

            if (strlen($_POST["rnewp"]) < 10) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Renew Password should be greater than 10 character !!";
                echo(json_encode($returnArr));
                exit;
            }


            if ($_POST["newp"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "new password is mandatory !!";
                echo(json_encode($returnArr));
                exit;
            }
            if ($_POST["rnewp"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please re-enter password!!";
                echo(json_encode($returnArr));
                exit;
            }
            if ($newp != $rnewp) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "password are not matching!!";
                echo(json_encode($returnArr));
                exit;
            }


            $newp = sha1Md5DualEncryption($newp);
            $result = changePassword($email, $newp, $connAdmin);
            if ($result["errCode"] == -1) {
                session_destroy();
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Password change sucessfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In change password !!";
                echo(json_encode($returnArr));
            }


            break;
        case "deleteadmin":

            $xmlfilename = "deleteAdminUser.xml";

            $xml_data['step1']["data"] = 'Start delete admin';

            $xml_data['step1']["data"] = "user to delete: {$email}";

            $result = deleteAdmin($email, $connAdmin);

            if ($result["errCode"] == -1) {

                if ($email == $_SESSION["admin_id"]) {
                    session_start();
                    session_destroy();

                    $returnArr["errCode"] = 5;
                    $returnArr["errMsg"] = "Admin User Deleted !!";
                    $returnArr["returl"] = "index.php";

                } else {
                    $returnArr["errCode"] = -1;
                    $returnArr["errMsg"] = "Admin User Deleted !!";
                }

                $xml_data['step3']["data"] = "Admin User Deleted Success!";

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]="Admin User Deleted Success";
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));

            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In user Delition !!";

                $xml_data['step3']["data"] = "Admin User Deleting Failed!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }

            break;
        case "editrights":


            $rights = implode("", $userpermission);


            $result = editAdminrights($email, $rights, $connAdmin);

            if ($result["errCode"] == -1) {
                $_SESSION['greenerror'] = "Admin Rights Changes Success !!";
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin User rights changed !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In rights change !!";
                echo(json_encode($returnArr));
            }

            break;
        case "updateuser":
            $xmlfilename = "editAdminInfo.xml";

            $xml_data['step1']["data"] = "Start edit Information!";

            $group = implode(" ", $group);

            if ($_POST["fname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Fisrt Name is Required !!";

                $xml_data['step2']["data"] = "Admin User First Name blank!";

                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = "Admin User First Name Filled!";
            }

            if ($_POST["lname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Last Name is Required !!";

                $xml_data['step3']["data"] = "Admin User Last Name blank!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step3']["data"] = "Admin User Last Name Filled!";
            }
            if ($_POST["email"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Email is Required !!";

                $xml_data['step4']["data"] = "Admin User Email is blank!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $returnArr["errCode"] = "2";
                    $returnArr["errMsg"] = "Please enter valid email!!";

                    $xml_data['step4']["data"] = "Admin User Email is invalid!";
                    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                    echo(json_encode($returnArr));
                    exit;
                }
            }

            if ($_POST["group"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Group is Required !!";

                $xml_data['step5']["data"] = "Admin User Group is blank!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step5']["data"] = "Admin User Group is filled!";
            }


            $result = updateuser($email, $fname, $lname, $group, $connAdmin);
            if ($result["errCode"] == -1) {
                $_SESSION['greenerror'] = "Admin user Updated to system success !!";
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin Profile updated sucessfully !!";

                $xml_data['step6']["data"] = "Admin Profile updated sucessfully !!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                if (isset($group)) {

                    $datatt = getAdminData($email, $connAdmin);
                    $datatt = $datatt["errMsg"];
                    $datatt = $datatt["group_id"];

                    $permsiion = array();
                    $resultsss = explode(" ", $datatt);
                    foreach ($resultsss as $res) {

                        $result = selectGroups($res, $connAdmin);
                        $resultnew = $result["errMsg"];
                        $rightss = $resultnew["group_rights"];
                        array_push($permsiion, $rightss);

                    }

                    $largest = max($permsiion);

                    setUserPermissions($email, $largest, $connAdmin);
                }

                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin profile not updated sucessfully !!";

                $xml_data['step6']["data"] = "Admin Profile not updated !!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }


            break;
        case "deletegroup":

            $xmlfilename = "deleteGroup.xml";

            $xml_data['step1']["data"] = "Start delete Group!!";

            $mygrouptodelete = getGroupName($groupid, $connAdmin);
            $mygrouptodelete = $mygrouptodelete["errMsg"][0];
            $mygrouptodelete = $mygrouptodelete["group_name"];

            $xml_data['step2']["data"] = "Group To Delete :{$mygrouptodelete}";

            /*

                     if type=="deletegroup"
                     This Controller run following function

                    deleteGroup($groupid,$connAdmin);

                    which have 2 parameters
                    1]GroupId - Group ID which we are deleting
                    2]Connection for admin database


                    it's Delete group from  groups table.


             */

            $result = deleteGroup($groupid, $connAdmin);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin Group Deleted !!";

                $xml_data['step3']["data"] = "Admin Group Deleted!";

                $xml_data['groupName']["data"] = $mygrouptodelete;

                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]=$returnArr["errMsg"];
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/

                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Group Delition !!";

                $xml_data['step3']["data"] = "Error In Group Delition !";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }

            break;
        case "editadmin":


            /*
                     if type=="editadmin"
                     This Controller run following function

                    Returns HTML View For Editeting User Details

             */

            $result = getAdminData($email, $connAdmin);
            $datae = $result["errMsg"];
            $oldemail = $datae["admin_email"];
            $oldfname = $datae["admin_first_name"];
            $oldlname = $datae["admin_last_name"];
            $oldrolecode = $datae["group_id"];


            if ($result["errCode"] == -1) {
                ?>

                <div id="rederror1" style="">
                    <div id="errmsg1" style="padding: 0px;color:red;"></div>
                    <br/>
                </div>

                <div id="greenerror1" style="">
                    <div id="errmsgsuc1" style="padding: 0px;color:green;"></div>
                    <br/>
                </div>
                <div class="register-box-body">
                    <form id="addstaff1" name="addstaff1" action="javascript:;" data-parsley-validate
                          class="form-horizontal form-label-left">

                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label p-r-0">First Name* :</label>
                            <div class="col-sm-9">
                                <input type="text" name="fname" maxlength="50"
                                       onkeyup="editadminfname()" class="form-control" value="<?php echo $oldfname; ?>" placeholder="" required="">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="col-sm-3 control-label p-r-0">Last Name* :</label>
                            <div class="col-sm-9">
                                <input type="text" name="lname" maxlength="50"
                                       onkeyup="editadminlname()" class="form-control" value="<?php echo $oldlname; ?>" placeholder="" required="">
                            </div>
                        </div>

                        <div class="modal-footer">
                            <div class="form-group m-b-0">
                                <label for="" class="col-sm-3 control-label p-r-0 p-t-0">Groups :</label>
                                <div class="col-sm-9">


                                    <div class="checkbox checkbox--1 p-t-0 clearfix">
                                    <?php
                                    $result = explode(" ", $oldrolecode);
                                    $datas = $result;


                                    $result = getAllRoles($connAdmin);
                                    $data = $result["errMsg"];


                                    foreach ($data as $value) {

                                        if (in_array($value['group_name'], $datas)) {
                                            ?>
                                        <label class="p-r-10 pull-left">
                                            <input type="checkbox" id="groups" name="groups[]"
                                                   value="<?php echo $value['group_name']; ?>" checked>
                                           <?php echo (strtolower($value["group_name"])); ?>
                                            <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                            <?php
                                        } else {
                                            ?>
                                            <label class="p-r-10 pull-left">
                                            <input type="checkbox" id="groups" name="groups[]"
                                                   value="<?php echo $value['group_name']; ?>">
                                           <?php echo (strtolower($value["group_name"])); ?>
                                                <i class="fa fa-checkbox--1"> </i>
                                            </label>
                                            <?php

                                        }


                                    }
                                    ?>

                                </div>
                            </div>
                            </div>
                            <div style="margin-top:15px">
                                <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Close</button>
                                <button type="submit" id="submit_usr_edit_lda" onclick="submitUserdataupdate('<?php echo $oldemail; ?>')" data-style="expand-right" class="btn btn-sm btn-success border-default-border ajaxhide">Save changes</button>
                            </div>
                        </div>






                    </form>
                </div>


                </div><!-- /.form-box -->
                <?php


            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Editing !!";
                echo(json_encode($returnArr));
            }

            break;
        case "editgroup":

            /*

                     if type=="editgroup"
                     This Controller run following function

                    Returns HTML View for group being Edited


             */

            $result = getGroup($groupid, $connAdmin);
            $datae = $result["errMsg"];

            $group_name = $datae["group_name"];
            $gpid = $datae["group_id"];
            $group_rights = $datae["group_rights"];
            $right_on_module = json_decode($datae["right_on_module"], true);
            $right_on_submodule = explode(",", $datae["right_on_submodule"]);
            $datanews = getPageList($right_on_submodule, $connAdmin);

            if ($result["errCode"] == -1) {
                ?>

                <div id="rederror112" style="">
                    <div id="errmsg112" style="padding: 0px;color:red;"></div>

                </div>

                <div id="greenerror112" style="">
                    <div id="errmsgsuc112" style="padding: 0px;color:green;"></div>

                </div>

                <div class="x_content">
                    <br/>
                    <form id="editedgroup" name="editedgroup" action="javascript:;" data-parsley-validate
                          class="form-horizontal form-label-left">

                        <div class="form-group">
                            <label for="" class="col col-135 control-label p-r-0">Group Name * :</label>
                            <div class="col col-355">

                                <input id="selectdate" value="<?php echo $group_name; ?>"
                                       class="date-picker form-control col-md-7 col-xs-12" maxlength="50"
                                       name="groupname" onkeyup="AllowAlphabetlgroupnameedit()" type="text">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="" class="col col-135 control-label p-r-0 p-l-0">Permision group * :</label>
                            <div class="col col-355">


                                    <?php


                                    $old = str_split($group_rights);

                                    $dts = str_split("12");


                                    ?>


                                                <?php


                                                foreach ($dts as $dt) {

                                                    if (in_array($dt, $old)) {

                                                        ?>
                                <div class="checkbox checkbox--1" style="">
                                    <label>
                                                        <input type="checkbox" id="permsissionsedit"
                                                               name="permsissionsedit[]"
                                                               value="<?php echo $dt; ?>" checked>
                                                     <?php

                                                                    if ($dt == 1) {

                                                                        echo "View";
                                                                    }

                                                                    if ($dt == 2) {

                                                                        echo "Edit";
                                                                    }

                                                                    if ($dt == 4) {

                                                                        echo "Execute";
                                                                    }


                                                                    ?>
                                        <i class="fa fa-checkbox--1"> </i>
                                    </label>
                                </div>
                                                        <?php
                                                    } else {
                                                        ?>
                                        <div class="checkbox checkbox--1" style="">
                                            <label>
                                                        <input type="checkbox" id="permsissionsedit"
                                                               name="permsissionsedit[]"
                                                               value="<?php echo $dt; ?>">
                                                        <?php

                                                                    if ($dt == 1) {

                                                                        echo "View";
                                                                    }

                                                                    if ($dt == 2) {

                                                                        echo "Edit";
                                                                    }

                                                                    if ($dt == 4) {

                                                                        echo "Execute";
                                                                    }

                                                                    ?>
                                                <i class="fa fa-checkbox--1"> </i>
                                            </label>
                                        </div>
                                                        <?php

                                                    }


                                                }

                                                ?>




                                </div>
                            </div>

                        <div class="form-group">
                            <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on module :</label>
                            <div class="col col-355">
                                <div class="select--1">
                                    <?php
                                    $result = getallmodules($connAdmin);
                                    $resultnew = $result["errMsg"];
                                    // printArr($result);

                                    ?>
                                    <select class="chosen-select" id="modulenameedit" name="modulenameedit"
                                            onchange="editsubmodule()" multiple>
                                        <?php
                                        $allmodules = array();

                                        foreach ($resultnew as $value) {

                                            if (in_array($value["module_name"], $right_on_module)) {
                                                echo "<option selected>" . $value["module_name"] . "</option>";
                                                array_push($allmodules, $value["module_name"]);
                                            } else {

                                                echo "<option>" . $value["module_name"] . "</option>";

                                            }

                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on submodule :</label>
                            <div class="col col-355">
                                <div class="select--1">
                                    <?php

                                    $submodule = $allmodules;
                                    $data = getsubmodules($submodule, $connAdmin);


                                    ?>
                                    <select class="chosen-select" id="submodulenameeditdata" name="submodulename" multiple>
                                        <?php

                                        foreach ($data as $value) {
                                            if (in_array($value, $datanews)) {

                                                echo "<option selected>" . $value . "</option>";
                                            } else {
                                                echo "<option>" . $value . "</option>";

                                            }
                                        }
                                        ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Cancel</button>
                            <button type="submit" id="submit_user_grp_edit" onclick="submiteditedGroup('<?php echo $gpid; ?>')" class="btn btn-sm btn-success border-default-border" ajaxhide>Submit</button>
                        </div>



                    </form>
                </div>

                <script>
                    $("#editnewadmindataadmingroup  #modulenameedit").chosen();
                    $("#editnewadmindataadmingroup #submodulenameeditdata").chosen();
                </script>
                <?php


            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Editing !!";
                echo(json_encode($returnArr));
            }

            break;
        case "changeRights":


            /*
                     if type=="changeRights"
                     This Controller run following function

                    Its Returns HTML View for changing group permission particular User.

            */

            $datatt = getAdminData($email, $connAdmin);


            if ($datatt["errCode"] == -1) {
                ?>

                <div id="rederror1" style="">
                    <div id="errmsg1" style="padding: 0px;color:red;"></div>
                    <br/>
                </div>

                <div id="greenerror1" style="">
                    <div id="errmsgsuc1" style="padding: 0px;color:green;"></div>
                    <br/>
                </div>
                <div class="register-box-body">
                    <form id="editrights" name="editrights" action="javascript:;" data-parsley-validate
                          class="form-horizontal form-label-left">


                        <?php


                        $datatt = $datatt["errMsg"];
                        $datatt = $datatt["rights"];

                        $old = str_split($datatt);

                        $dts = str_split("12");


                        ?>

                        <div class="row" style="padding:15px">

                            <div class="form-group">
                                <label for="" class="col col-135 control-label p-r-0 p-l-0">Edit Agent Access :</label>
                                <div class="col col-355">
                                    <?php

                                    foreach ($dts as $dt) {

                                        if (in_array($dt, $old)) {

                                            ?>
                                    <div class="checkbox checkbox--1" style="">
                                        <label>
                                            <input type="checkbox" id="permsissions" name="permsissions[]"
                                                   value="<?php echo $dt; ?>" checked>
                                            <?php

                                                        if ($dt == 1) {

                                                            echo "View";
                                                        }

                                                        if ($dt == 2) {

                                                            echo "Edit";
                                                        }

                                                        if ($dt == 4) {

                                                            echo "Execute";
                                                        }


                                                        ?>   <i class="fa fa-checkbox--1"> </i>
                                        </label>
                                            <?php
                                        } else {
                                            ?>
                                        <div class="checkbox checkbox--1" style="">
                                            <label>
                                            <input type="checkbox" id="permsissions" name="permsissions[]"
                                                   value="<?php echo $dt; ?>">
                                          <?php

                                                        if ($dt == 1) {

                                                            echo "View";
                                                        }

                                                        if ($dt == 2) {

                                                            echo "Edit";
                                                        }

                                                        if ($dt == 4) {

                                                            echo "Execute";
                                                        }

                                                        ?>   <i class="fa fa-checkbox--1"> </i>
                                                </label>
                                            <?php

                                        }


                                    }

                                    ?>


                        </div>



                            <div class="modal-footer">
                                <button type="reset" class="btn btn-sm bg-transparent border-default-border" data-dismiss="modal">Cancel</button>
                                <button type="submit" id="submitusersGrpDta" onclick="submitUserRights('<?php echo $email; ?>')" class="btn btn-sm btn-success border-default-border" ajaxhide>Submit</button>
                            </div>



                    </form>
                </div>


                </div><!-- /.form-box -->
                <?php


            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Editing !!";
                echo(json_encode($returnArr));
            }

            break;
        case "deleteip":

            $xmlfilename = "deleteIPfromACL.xml";
            $xml_data['step1']["data"] = "Start delete IP !";

            $mygrouptodelete = getipbyID($id, $connAdmin);
            $ipinfo = $mygrouptodelete["errMsg"][0];
            $mygrouptodelete = $ipinfo["ip_address"];
            $ip_owner = $ipinfo["ip_owner"];

            $xml_data['step2']["data"] = "IP to Delete :{$mygrouptodelete}";


            /*
                 if type=="deleteip"
                 This Controller run following function

                deleteip($id,$connAdmin);

                which have 7 parameters
                1]IpAddress - IP address we want to delete
                7]Connection for admin database


                it's Delete IP address from IP_access TAble.
                */
            $result = deleteip($id, $connAdmin);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "ip address Deleted !!";

                $xml_data['step3']["data"] = "ip address Deleted !";

                $xml_data['ipOwner']["data"] =$ip_owner;
                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]=$returnArr["errMsg"];
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In ip Delition !!";

                $xml_data['step3']["data"] = "Error In ip Delition!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }

            break;
        case "getsubmodules":


            /*
                 if type=="deleteip"
                 This Controller run following function

                 Returns HTML View of All Modules.
                */

            $submodule = $allmodules;
            $data = getsubmodules($submodule, $connAdmin);


            ?>


                <label for="" class="col col-135 control-label p-r-0 p-t-0">Permision on </br> Sub-Module :</label>
                <div class="col col-355">
                    <div class="select--1">
                        <select class="chosen-select" id="submodulename" name="submodulename" multiple>
                            <?php

                            foreach ($data as $value) {
                                echo "<option>" . cleanDisplayParameter($connAdmin, $value) . "</option>";
                            }
                            ?>

                        </select>
                    </div>
                </div>

            <script>
                $('#submodulename').chosen();
            </script>
            <?php


            break;
        case "editgroupnew":
            $xmlfilename = "editGroup.xml";

            $xml_data['step1']["data"] = "Start edit Group!";

            /*
                 if type=="editgroupnew"
                 This Controller run following function

                editUserdatagroup($submodulename,$groupid,$groupname,$modulename,$permissions,$connAdmin);

                which have 7 parameters
                1]$submodulename -  Submodules in group
                2]$groupid - Groupid we are editing
                3]$groupname - GroupNAme we are editing
                4]$modulename - ModuleNAme we are editing
                5]$permissions - Permission on group
                6]Connection for admin database


                it's Edit old group with new data in  TAble group.

             */
            if ($_POST["groupname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Group Name is Required !!";

                $xml_data['step2']["data"] = "Admin User Group Name field balnk!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step2']["data"] = "Admin User Group Name field filled!  Group Name to edit :{$_POST['groupname']}";
            }

            if ($_POST["permissions"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin Group Permissions are required !!";

                $xml_data['step3']["data"] = "Admin Group Permissions field balnk!";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step3']["data"] = "Admin Group Permissions field filled";
            }


            if ($_POST["modulename"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please Select Module on you want to add this group!!";

                $xml_data['step4']["data"] = "Modules field balnk";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step4']["data"] = "Modules field filled";
            }


            $checkmodule = json_decode($modulename, true);

            foreach ($checkmodule as $value) {
                if ($value == "") {
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Please Select Module on you want to add this group!!";
                    echo(json_encode($returnArr));
                    exit;
                }

            }


            if (empty($submodulename)) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please Select at list one submodule !!";

                $xml_data['step5']["data"] = "sub-Modules field balnk";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step5']["data"] = "Sub-Modules field filled";
            }


            foreach ($submodulename as $value) {
                if ($value == "") {
                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Please Select Sub-Module on you want to add this group!!";
                    echo(json_encode($returnArr));
                    exit;
                }

            }

            $result = editUserdatagroup($submodulename, $groupid, $groupname, $modulename, $permissions, $connAdmin);
            if ($result["errCode"] == -1) {
                $_SESSION['greenerrorgroup'] = "Group Edited SUccess !!";

                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin Group edited sucessfully !!";

                $xml_data['step6']["data"] = "Admin Group edited sucessfully ";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = " Error in editing Group !!";
                $xml_data['step6']["data"] = "Error in editing Group ";
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            }


            break;
        case "updatemyprofile":


            $firstname = cleanQueryParameter($connAdmin, cleanXSS($_POST["fisrtname"]));
            $lastname = cleanQueryParameter($connAdmin, cleanXSS($_POST["lastname"]));
            $mobileno = cleanQueryParameter($connAdmin, cleanXSS($_POST["mobileno"]));
            $country = cleanQueryParameter($connAdmin, cleanXSS($_POST["country"]));
            $selectState = cleanQueryParameter($connAdmin, cleanXSS($_POST["selectState"]));
            $citySelect = cleanQueryParameter($connAdmin, cleanXSS($_POST["citySelect"]));
            $address = cleanQueryParameter($connAdmin, cleanXSS($_POST["address"]));
            $zip = cleanQueryParameter($connAdmin, cleanXSS($_POST["zip"]));
            $gender = cleanQueryParameter($connAdmin, cleanXSS($_POST["gender"]));




            if ($firstname=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "First name can not be empty!";;
                echo(json_encode($returnArr));
                exit;
            }

            if (strlen($firstname)<3) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "First Name should be greater than 3 characters!";;
                echo(json_encode($returnArr));
                exit;
            }

            if ($lastname=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Last name can not be empty!";;
                echo(json_encode($returnArr));
                exit;
            }

            if (strlen($lastname)<3) {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Last Name should be greater than 3 characters!";;
                echo(json_encode($returnArr));
                exit;
            }

            if ($gender=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Please select gender!";;
                echo(json_encode($returnArr));
                exit;
            }

            if ($country=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Country can not be Blank!";;
                echo(json_encode($returnArr));
                exit;
            }

            if ($selectState=="" || $selectState=="selectState") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "State can not be Blank!";;
                echo(json_encode($returnArr));
                exit;
            }


            if (!preg_match('/[^0-9]/', $mobileno) && strlen($mobileno) == 10) {
            }else
            {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Mobile number is not valid!";;
                echo(json_encode($returnArr));
                exit;
            }


            if ($zip=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Zip Code can not be Blank!";;
                echo(json_encode($returnArr));
                exit;
            }else if(!preg_match('/^[a-zA-Z0-9\s-]+$/', trim($zip)))
            {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Zip Code is invalid please enter valid zip!";;
                echo(json_encode($returnArr));
                exit;
            }

            if ($address=="") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Address can not be Blank!";;
                echo(json_encode($returnArr));
                exit;
            }

            $res = getCountryCityandState($selectState, $country, $citySelect, $connDemo);
            $res = $res["errMsg"];
            $country = $res["country"];
            $selectState = $res["states"];
            $citySelect = $res["cities"];

            $result = editAdminInfoProfilenew($email, $firstname, $lastname, $mobileno, $country, $selectState, $citySelect, $address, $zip, $gender, $connAdmin);


            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Profile Updated Success !!!!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin Error in Updating Profile !!";
                echo(json_encode($returnArr));
            }

            break;
        case "addnew":
            $xmlfilename = "AddNewAdmin.xml";

            $xml_data['step1']["data"] = 'Add New User in Admin Section';

            $xml_data['step2']["data"] = "Check PArametersFirst Name: " . $_POST["fname"] . " ,Last Name: " . $_POST["lname"] . " ,Email: " . $_POST["email"];


            if ($_POST["fname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Fisrt Name is Required !!";

                echo(json_encode($returnArr));
                exit;
            }

            if ($_POST["lname"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Last Name is Required !!";
                echo(json_encode($returnArr));
                exit;
            }

            if ($_POST["email"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Email is Required !!";
                echo(json_encode($returnArr));
                exit;
            } else {
                if (!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL)) {
                    $returnArr["errCode"] = "2";
                    $returnArr["errMsg"] = "Please enter valid email!!";
                    echo(json_encode($returnArr));
                    exit;
                }


            }

            $result = getAdminData($email, $connAdmin);

            $data = $result["errMsg"];
            $admnValid = $data["admin_email"];
            if (($_POST["email"] == $admnValid)) {
                $returnArr["errCode"] = "2";
                $returnArr["errMsg"] = "User Allready Registered!!";

                $xml_data['step3']["data"] = 'User Allready Registered';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {
                $xml_data['step3']["data"] = 'User Allready not Registered';
            }

            if ($_POST["group"] == "") {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Admin User Group is Required !!";

                $xml_data['step4']["data"] = 'Admin User Group is Required';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
                exit;
            } else {

                $xml_data['step4']["data"] = 'Admin User Group is selected for this user';
            }


            $group = implode(" ", $group);

            $randonnumber = generateRandomString();

            $token = generateRandomString($length = 30);
            $result = addNewAdmin($email, $fname, $lname, $randonnumber, $group, $token, $connAdmin);

            $xml_data['step5']["data"] = 'Run addNewAdmin()';

            if ($result["errCode"] == "-1") {

                $xml_data['step6']["data"] = 'Run addNewAdmin() success';

                $result = getAdminData($email, $connAdmin);

                $_SESSION['greenerror'] = "Admin user added to system success !!";
                //printArr($result);
                $result = $result["errMsg"];
                $admin_id = $result["admin_id"];
                $admin_email = $result["admin_email"];

                $url = $adminRoot . 'controller/acl/activateNewUser.php?id=' . urlencode($admin_id) . '&email=' . urlencode($admin_email) . '&token=' . urlencode($token);
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Admin User Added Successfully !!";
                $to = $email;
                $username = $fname . " " . $lname;//'.$randonnumber.'
                $emailSubject = "Your have added on Keywo Admin Section ";
                $content = '<br/>Your Keywo Admin account has been created.</p>
<p> Your Activation Link is : <span style="font-weight:bold;"><a href="' . $url . '">Activate Click Here</a></span> </p>
<p>Please change your password after first login!!';


                $parameters["name"]=$username;
                $parameters["content"]=$content;
                $sendMail=sendEmail($to, $noReplyEmail, $emailSubject,"admin_default.html",$parameters);

                $xml_data['step7']["data"] = 'Sent Notification Email to User';

                $datatt = getAdminData($email, $connAdmin);
                $datatt = $datatt["errMsg"];
                $datatt = $datatt["group_id"];

                $permsiion = array();
                $resultsss = explode(" ", $datatt);
                foreach ($resultsss as $res) {

                    $result = selectGroups($res, $connAdmin);
                    $resultnew = $result["errMsg"];
                    $rightss = $resultnew["group_rights"];
                    array_push($permsiion, $rightss);

                }


                $largest = max($permsiion);

                $perresult = setUserPermissions($email, $largest, $connAdmin);


                if ($perresult["errCode"] == -1) {
                    $xml_data['step8']["data"] = 'Set User Permission Success!';
                } else {
                    $xml_data['step8']["data"] = 'Set User Permission failed!';
                }
                /******************** for logs user analytics ****************/
                $responseArr["errCode"]="-1";
                $responseArr["errMsg"]="Admin Agent Added Success";
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;
                /******************** for logs user analytics ****************/


                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                echo(json_encode($returnArr));
            } else {
                $xml_data['step6']["data"] = 'Run addNewAdmin() failed';
                $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                echo(json_encode($returnArr));
            }


            break;

    }


} else {

        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "INVALID DETAILS!!";
        echo(json_encode($returnArr));
}
?>








