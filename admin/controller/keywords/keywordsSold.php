<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/keywords/common_keyword.php');

//end other

//printArr($_POST);


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$owner = cleanQueryParameter($connKeywords, cleanXSS($_POST["userEmailId"]));
$searchQuery =cleanQueryParameter($connKeywords, cleanXSS($_POST["keyword"]));


//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllRecentlySoldKeywordsCount($connKeywords, $searchQuery, $owner)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllRecentlySoldKeywords($page_position, $limit, $connKeywords, $searchQuery, $owner);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllRecentlySoldKeywordsCount($connKeywords, $searchQuery, $owner)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllRecentlySoldKeywords($page_position, $limit, $connKeywords, $searchQuery, $owner);
    $results = $results["errMsg"];
}


?>
<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive">
        <thead>
        <tr>

            <th style="width:13%"> Purchase Timestamp</th>
            <th> Keyword</th>
            <!--<th> Order ID (ITD)</th>-->
            <th> Keyword Price</th>
            <th> Owned By</th>
            <th> Purchase By</th>
        </tr>

        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="6">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
                $count = $count + 1;
                ?>
                <tr>

                    <td>
                        <?php
                        $dataDate=explode(" ",uDateTime("d-m-Y h:i:s",$value["purchase_timestamp"]));
                        ?>
                        <span class="date"> <?php echo $dataDate[0]; ?> </span>
                        <time> <?php echo $dataDate[1]; ?></time>
                    </td>
                    <td><?php echo "#".cleanDisplayParameter($connKeywords,$value["keyword"]); ?> </td>
                    <!--<td>od-555</td>-->
                    <td><?php echo $value["keyword_price"]; ?> <?= $adminCurrency; ?></td>
                    <td><?php echo $value["buyer_id"]; ?></td>
                    <td>

                        <?php

                        $dataWallet=json_decode($value["transaction_details"],true);

                        foreach($dataWallet as $datavalue)
                        {


                            if($value["keyword"]==$datavalue["keyword"])
                            {
                                echo $datavalue["payment_mode"];
                                break;
                            }
                        }


                        ?>


                    </td>
                </tr>
            <?php }
        } ?>
        </tbody>

        </tfoot>
    </table>
</div>
<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage, $page_number, $limit,$searchQuery,$owner);
function getPaginationData($lastpage, $pageno, $limit, $searchQuery,$owner)
{


    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=kwdSoldListPages("' . $pagenum . '","' . $searchQuery . '","' . $owner . '","' . $limit . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=kwdSoldListPages("' . $pagenumber . '","' . $searchQuery . '","' . $owner . '","' . $limit . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=kwdSoldListPages("' . $pagenumber . '","' . $searchQuery . '","' . $owner . '","' . $limit . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=kwdSoldListPages("' . $pagenumber . '","' . $searchQuery . '","' . $owner . '","' . $limit . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=kwdSoldListPages("' . $lastpage . '","' . $searchQuery . '","' . $owner . '","' . $limit . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

?>


<script>

    $("#LimitedResults").change(function () {

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>