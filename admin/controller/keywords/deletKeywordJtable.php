<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
//end other

require_once('../../model/keywords/deleteJtableKwd.php');

$connKwd = createDBConnection("dbkeywords");
noError($connKwd) ? $connKwd = $connKwd["connection"] : checkMode($connKwd["errMsg"]);

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$returnArr = array();



$kwdIddelete = explode(",",$_POST["id"]);
$type = $_POST["type"];


if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($type) {
        case "suggestedDelete":

            foreach ($kwdIddelete as $kwdid) {
                deleteSuggestedKwd($kwdid, $connKwd);
            }
            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = "Data Updated Successfully";
            echo json_encode($returnArr, true);

            break;

        case "blockedDelete":

            foreach ($kwdIddelete as $kwdid) {
                deleteBlockedKwd($kwdid, $connKwd);
            }
            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = "Data Updated Successfully";
            echo json_encode($returnArr, true);

            break;

        case "blockedDomain":

            foreach ($kwdIddelete as $kwdid) {
                deleteBlockedDomain($kwdid, $connDemo);
            }
            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = "Data Updated Successfully";
            echo json_encode($returnArr, true);

            break;

        case "BlockedIP":

            foreach ($kwdIddelete as $id) {
                $ip_address=getBlcokedIP($id, $connKwd)["errMsg"]["ip_address"];
                $result=deleteBlockedIP($id, $connKwd);
                if(noError($result))
                {
                    global $docRoot;
                    $banip = "Deny from {$ip_address}\n";
                    $file=$docRoot.".htaccess";
                    $resultdata=file_get_contents($file);
                    $bodycont = str_replace($banip,"",$resultdata);
                   // file_put_contents($file ,$bodycont);
                }
            }
            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = "Data Updated Successfully";
            echo json_encode($returnArr, true);

            break;



    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>