<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
require_once('../../helpers/email_helpers.php');
require_once('../../model/keywords/common_keyword.php');
//end helper

//other
require_once('../../core/errorMap.php');

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$result=getMyKeywordDetails($connKeywords, "dineshghule321@gmail.com");
$result=json_decode($result["errMsg"]["transaction_details"],true);
printArr($result);


