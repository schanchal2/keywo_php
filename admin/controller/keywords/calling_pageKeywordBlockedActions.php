<?php
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../core/errorMap.php');
require_once('../../helpers/stringHelper.php');
$connAdmin = createDBConnection("dbkeywords");
$conn = $connAdmin["connection"];

try {

    if (!$conn) {
        die("connection object not created: " . mysqli_error($conn));
    }
    //Getting records (listAction)
    if ($_GET["action"] == "list") {
        //Get record count
        $result = mysqli_query($conn, "SELECT COUNT(*) AS RecordCount FROM blocked_keywords;");
        $row = mysqli_fetch_array($result);
        $recordCount = $row['RecordCount'];

        //Get records from database
        $result = mysqli_query($conn, "SELECT * FROM blocked_keywords ORDER BY id desc LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");

        while ($row = mysqli_fetch_array($result)) {
            $rows[] = $row;
        }

        $result = array();
        $result1 = array();

        foreach ($rows as $value) {

            $result["0"] = $value["0"];
            $result["id"] = $value["id"];
            $result["1"] = cleanDisplayParameter($conn,$value["1"]);
            $result["keyword"] = cleanDisplayParameter($conn,$value["keyword"]);
            array_push($result1, $result);
        }


        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $result1;


        print json_encode($jTableResult);
    } //Creating a new record (createAction)
    else if ($_GET["action"] == "create") {
        $keyw = strtolower(cleanQueryParameter($conn, cleanXSS($_POST["addKeyword"])));

      /*  if (preg_match('/[<>]/', $_POST["keyword"])) {
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Special Characters not allowed!";
            print json_encode($jTableResult);
            exit;
        }*/
        if (empty($keyw)) {
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Please enter keyword empty field not allowed!";
            print json_encode($jTableResult);
            exit;
        }

        if (preg_match('/\s/', $keyw)) {
            $jTableResult = array();
            $jTableResult['Message'] = "Space not allowed in Keyword. \n Submit one keyword at a time.";
            print json_encode($jTableResult);

        } else {
            $result1 = mysqli_query($conn, "SELECT keyword FROM blocked_keywords where keyword='" . $keyw . "'");
            $row = mysqli_fetch_array($result1);
            $nme = $row['keyword'];
            if (strlen($_POST["keyword"]) <= 50) {
            if ($keyw != $nme) {
                $result2 = mysqli_query($conn, "SELECT keyword FROM suggested_keywords where keyword='" . $keyw . "'");
                $row = mysqli_fetch_array($result2);
                $nmee = $row['keyword'];
                if ($keyw != $nmee) {
                    //Insert record into database
                    $result = mysqli_query($conn, "INSERT INTO blocked_keywords(keyword) VALUES('" . $keyw . "')");

                    $result = mysqli_query($conn, "SELECT * FROM blocked_keywords WHERE id = LAST_INSERT_ID();");
                    $row = mysqli_fetch_array($result);

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    $jTableResult['Record'] = $row;
                    print json_encode($jTableResult);
                } else {
                    $jTableResult = array();
                    $jTableResult['Message'] = "Keyword Allready Present in suggested keywords you can not block this keyword.";
                    print json_encode($jTableResult);
                }
            } else {
                $jTableResult = array();
                $jTableResult['Message'] = "Keyword Allready Present OR Blocked.";
                print json_encode($jTableResult);
            }
            }else{
                //Return result to jTable
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "Keyword Name must be less than 50 characters";
                print json_encode($jTableResult);
            }
        }

    } //Updating a record (updateAction)
    else if ($_GET["action"] == "update") {

        $keyw = strtolower(cleanQueryParameter($conn, cleanXSS($_POST["Editkeyword"])));

       /* if (preg_match('/[<>]/', $_POST["keyword"])) {
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Special Characters not allowed!";
            print json_encode($jTableResult);
            exit;
        }*/

        if (empty($keyw)) {
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Please enter keyword empty field not allowed!";
            print json_encode($jTableResult);
            exit;
        }


        if (preg_match('/\s/', $keyw)) {
            $jTableResult = array();
            $jTableResult['Message'] = "Space not allowed in Keyword. \n Submit one keyword at a time.";
            print json_encode($jTableResult);

        } else {
            $result1 = mysqli_query($conn, "SELECT keyword FROM blocked_keywords where keyword='" . $keyw . "'");
            $row = mysqli_fetch_array($result1);
            $nme = $row['keyword'];
            if (strlen($_POST["keyword"]) <= 50) {
            if ($keyw != $nme) {
                $result2 = mysqli_query($conn, "SELECT keyword FROM suggested_keywords where keyword='" . $keyw . "'");
                $row = mysqli_fetch_array($result2);
                $nmee = $row['keyword'];
                if ($keyw != $nmee) {
                    //Update record in database
                    $result = mysqli_query($conn, "UPDATE blocked_keywords SET keyword = '" . $keyw . "' WHERE id = " . $_POST["id"] . ";");

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    print json_encode($jTableResult);
                } else {
                    $jTableResult = array();
                    $jTableResult['Message'] = "Keyword Allready Present in suggested keywords you can not block this keyword.";
                    print json_encode($jTableResult);
                }
            } else {
                $jTableResult = array();
                $jTableResult['Message'] = "Keyword Allready Present OR Blocked.";
                print json_encode($jTableResult);
            }}else{
                //Return result to jTable
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "Keyword Name must be less than 50 characters";
                print json_encode($jTableResult);
            }
        }
    } //Deleting a record (deleteAction)
    else if ($_GET["action"] == "delete") {
        //Delete from database
        $result = mysqli_query($conn, "DELETE FROM blocked_keywords WHERE id = " . $_POST["id"] . ";");

        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        print json_encode($jTableResult);
    }

    //Close database connection

} catch (Exception $ex) {
    //Return error message
    $jTableResult = array();
    $jTableResult['Result'] = "ERROR";
    $jTableResult['Message'] = $ex->getMessage();
    print json_encode($jTableResult);
}

?>
