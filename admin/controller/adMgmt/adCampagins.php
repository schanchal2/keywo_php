<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');

//end other

//printArr($_POST);
?>

<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive">
        <thead>
        <tr>
            <th><input id="select_all" name="" type="checkbox"><label for="select_all" class="fa"></label></th>
            <th>Campaign ID</th>
            <th>Ad-Poster(Username)</th>
            <th>Campaign Name</th>
            <th>Campaign Type</th>
            <th>User Status</th>
            <th>Start Date</th>
            <th>Expiry date</th>
            <th>Bid Stategy</th>
            <th>Expenditure Till Date</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td><input class="user_checkbox" id="user_checkbox" name="user_checkbox[]" value="" type="checkbox">
                <label for="user_checkbox" class="fa"></label>
            </td>
            <td>Ad-idea01</td>
            <td>demo@keywo.com</td>
            <td>idea-Pumpkin Ad</td>
            <td>Text</td>
            <td>Active</td>
            <td>20-02-17</td>
            <td>21-02-17</td>
            <td>CPM</td>
            <td>100</td>
        </tr>
        </tbody>
    </table>
</div>

<script>

    $("#LimitedResults").change(function(){

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>