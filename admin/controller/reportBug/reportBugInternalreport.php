<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/bugReport/bugReportModel.php');

//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp=mypermissions($largest);
//printArr($myp);
/*********************** get user Permissions ********************/
//printArr($_POST);


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$BUGID = cleanQueryParameter($connKeywords, cleanXSS($_POST["BUGID"]));
$bugStatus =cleanQueryParameter($connKeywords, cleanXSS($_POST["bugStatus"]));
$bugType =cleanQueryParameter($connKeywords, cleanXSS($_POST["bugType"]));

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllBugsCount($connKeywords,$BUGID,$bugStatus,$bugType)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllBugsData($page_position, $limit, $connKeywords, $BUGID,$bugStatus,$bugType);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllBugsCount($connKeywords,$BUGID,$bugStatus,$bugType)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllBugsData($page_position, $limit, $connKeywords, $BUGID,$bugStatus,$bugType);
    $results = $results["errMsg"];
}

?>
    <div id="userList" class="m-t-15 table-responsive ">
        <table class="table  text-center reloadInternal">
            <thead>
                <tr>
                    <th> Bug ID </th>
                    <th> Timestamp </th>
                    <th style="max-width: 200px;" width="200px"> Subject </th>
                    <th > Bug Raised by</th>
                    <th> Screenshot</th>
                    <?php if($bugStatus == "open"){
                            if((in_array("write", $myp))) { ?>
                    <th> Action </th>
                    <?php }} ?>
                </tr>
            </thead>
            <tbody>
                <?php $count = ($page_number - 1) * $item_per_page;
                if ($get_total_rows == 0) { ?>
                <tr>
                    <?php if((in_array("write", $myp))) { ?>
                    <td style="text-align: center" colspan="6">No Data Found</td>
                    <?php } else { ?>
                    <td style="text-align: center" colspan="5">No Data Found</td>
                    <?php } ?>
                </tr>
                <?php } else {
                    foreach ($results as $value) { ?>
                <tr>
                    <td>
                        <?= $value['bug_id'] ?>
                    </td>
                    <td>

                        <?php
                        $mydate = uDateTime("m-d-Y h:i:s", $value['bug_createdAt']);
                        $datetime = explode(" ", $mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>

                    </td>
                    <td class="col-355  wrd-wrp_brk-wrd">
                        <?php echo $value['bug_desc'] ?>
                    </td>
                    <td>
                        <?php echo $value['bug_rasied_by'] ?>
                    </td>
                    <td>
                        <?php $imPath= json_decode($value['imagePath'],true);
                        if(!empty($imPath)) {
                            $count = 0;
                            foreach ($imPath as $path) {
                                $count = $count + 1;
                                $pathfull = $adminRoot . $path;
                                // echo "<span onclick=showScreenshot('" . $pathfull . "')>Screen:" . $count . "</span><br/>";
                                ?>
                        <span class="label label-default m-r-5" data-toggle="modal" href='#Screenshot' data-path="<?=$pathfull?>" role="button"> <?= $count ?> </span>
                        <?php }
                        } else { echo "NA"; } ?>
                    </td>
                    <?php if($bugStatus == "open") {
                        $bug_id = $value['bug_id'];
                        if((in_array("write", $myp))) { ?>
                    <td>
                        <!-- <input type="button" value="Close bugs" class="btn btn-block bg-blue border-none" onclick="closeReportModel('<?php //echo $bug_id; ?>','<?php //echo $bugType; ?>')"> -->
                        <button type="button" class="btn bg-darkblue text-white p-5" onclick="closeReportModel('<?php echo $bug_id; ?>','<?php echo $bugType; ?>')">Close</button>
                    </td>
                    <?php } } ?>
                </tr>
                <?php } } ?>
            </tbody>
        </table>
    </div>
    <?php

getPaginationData($lastpage, $page_number, $limit,$BUGID,$bugStatus,$bugType);
function getPaginationData($lastpage, $pageno, $limit,$BUGID,$bugStatus,$bugType)
{

    ?>
        <span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResult">
                    <option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
                    <option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
                    <option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
                    <option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
                    <option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
        <?php
    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getNewBugsPages("' . $pagenum . '","' . $BUGID . '","' . $bugStatus . '","' . $limit . '","' . $bugType . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getNewBugsPages("' . $pagenumber . '","' . $BUGID . '","' . $bugStatus . '","' . $limit . '","' . $bugType . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getNewBugsPages("' . $pagenumber . '","' . $BUGID . '","' . $bugStatus . '","' . $limit . '","' . $bugType . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getNewBugsPages("' . $pagenumber . '","' . $BUGID . '","' . $bugStatus . '","' . $limit . '","' . $bugType . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getNewBugsPages("' . $lastpage . '","' . $BUGID . '","' . $bugStatus . '","' . $limit . '","' . $bugType . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}



?>
            <div class="modal madal--1  fade" id="Screenshot" data-backdrop="static"  data-keyboard="false" >
                <div class="modal-dialog" style="width: 920px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> <i class="fa fa-close"></i> </button>
                            <h4 class="modal-title">Screenshot</h4>
                        </div>
                        <div class="modal-body">
                            <img src="" class="img-responsive">
                        </div>
                        <!--  <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary">Save changes</button>
                        </div> -->
                    </div>
                             </div>
            </div>
            <script>
            $("#LimitedResult").change(function() {
                getNewBugs();
            });

            // function showScreenshot(path) {
            //     alert(path);
            // }
            $('#Screenshot').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var imgpath = button.data('path') // Extract info from data-* attributes
                    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                modal.find('img').attr('src', imgpath);
                
            })
            </script>
