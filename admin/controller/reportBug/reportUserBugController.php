<?php

/**
 **********************************************************************************
 *                  reportUserBug.php
 * ********************************************************************************
 *      This controller is used for Raising/Generating InternalBugs Request.
 */

session_start();
//start config
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../core/errorMap.php";
require_once "../../model/bugReport/bugReportModel.php";


$conn = createDBConnection('dbkeywords');
if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$email       = $_SESSION["admin_id"];
if(isset($_POST["bug_id"])) {
    $bug_id = $_POST["bug_id"];
}else{
    $bug_id="";
}
if(isset($_POST["bug_desc"]))
{
    $bug_desc    = $_POST["bug_desc"];
}else{
    $bug_desc="";
}

if(isset($_POST["bug_type"]))
{
    $requestType    = $_POST["bug_type"];
}else{
    $requestType="";
}

$data = array(
    'email'    => $email,
    'bug_type' => $requestType,
    'bug_desc' => $bug_desc
);

// printArr($_POST);
// printArr($data);


if($requestType == "internal_report") {

    if (isset($_FILES["screenShots"]) && !empty($_FILES["screenShots"])){
        $count=0;
        $imgPath=array();
        foreach ($_FILES["screenShots"]["tmp_name"] as $key => $tmp_name) {
            if ($_FILES["screenShots"]["tmp_name"][$key]!="") {
                $temp = $_FILES["screenShots"]["tmp_name"][$key];
                $name = $_FILES["screenShots"]["name"][$key];
                $type = explode(".",$name)[1];
                $size = $_FILES["screenShots"]["size"][$key];
                $count=$count+1;
                $filePath="images/bug/".base64_encode($email)."_".strtotime(date("d-m-Y h:i:sa")).$count.".".$type;
                move_uploaded_file($temp,$docRootAdmin.$filePath);
                array_push($imgPath,$filePath);
            }
        }
    }
    if($_FILES) {
        $data["imagePath"] = json_encode($imgPath);
    }

    $updateBugReportData = updateBugData($bug_id, $data, $conn);
}
else if($requestType == "user_report")
{
    $updateBugReportData = updateBugData($bug_id,$data,$conn);
}

echo json_encode($updateBugReportData);
?>

