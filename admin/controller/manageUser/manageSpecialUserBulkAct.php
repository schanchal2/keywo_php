<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start model
require_once('../../model/manageUser/manageUserSpecail.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$connKeyword = createDBConnection("dbkeywords");
noError($connKeyword) ? $connKeyword = $connKeyword["connection"] : checkMode($connKeyword["errMsg"]);
$returnArr = array();

$email = array();
$email = $_POST['email'];

$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["userManagement"];

$status = cleanQueryParameter($connAdmin, cleanXSS($_POST['status']));

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $xmlArray = initializeXMLLog($_SESSION["admin_id"]);
    $xml_data['request']["data"] = '';
    $xml_data['request']["attribute"] = $xmlArray["request"];

    if (!empty($email)) {

        switch ($status) {
            case "delete":

                if (isset($_POST["status"]) && !empty($_POST["status"])) {

                    $xmlfilename = "DeleteSpecialUser.xml";

                    foreach ($email as $value) {

                        $result = deleteSpecialUser($value);

                        if($result["errCode"]=="-1")
                        {

                            $returnArrXml["errCode"] = -1;
                            $returnArrXml["errMsg"] = "User Blocked Successfully !!";
                            $returnArrXml["DeletedUser"] = $value;
                            //$xml_data['response']["data"] = "";
                            //$xml_data['response']["attributes"] = $returnArrXml;
                           // $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                        }
                    }

                    if ($result["errCode"] == "-1") {
                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"] = "User Deleted Successfully !!";
                    } else {
                        $returnArr["errCode"] = 4;
                        $returnArr["errMsg"] = $result["errMsg"];
                    }


                } else {

                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = "Error In getting status users";

                }


                break;


        }
    } else {
        $returnArr["errCode"] = 4;
        $returnArr["errMsg"] = "Plese select checkbox to do Action!!";

    }
    echo json_encode($returnArr);

} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}
?>