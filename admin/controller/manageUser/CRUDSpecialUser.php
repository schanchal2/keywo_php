<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');


//start model
require_once('../../model/manageUser/manageUser.php');
require_once('../../model/manageUser/manageUserSpecail.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');

$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();

$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["userManagement"];

$specialUserEmail = cleanQueryParameter($connAdmin, cleanXSS($_POST['specialUser']));
$type = cleanQueryParameter($connAdmin, cleanXSS($_POST['type']));

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    if ($specialUserEmail!="") {

        switch($type)
        {
            case "addnewuser":

                $specialUser=getDataForSpecialUserCheck($specialUserEmail);

                if(noError($specialUser))
                {

                    $specialUser=$specialUser["errMsg"];
                    $specialUserid=$specialUser["user_id"];
                    $active=$specialUser["active"];

                    if($active!=1)
                    {
                        $returnArr["errCode"] ="NACTIVE";
                        $returnArr["errMsg"] = "User Account is not Active!";
                        echo json_encode($returnArr);
                        exit;
                    }
                    $specialUseraccount_handle=$specialUser["account_handle"];

                    $result=addNewSpecialUser($specialUserid, $specialUseraccount_handle,$specialUserEmail);
                    if(noError($result))
                    {
                        $returnArr["errCode"] = "-1";
                        $returnArr["errMsg"] = "Specail User Added Success";
                    }else{
                        $returnArr["errCode"] = $result["errCode"];
                        $returnArr["errMsg"] = $result["errMsg"];
                    }

                }else{
                    $returnArr["errCode"] = $specialUser["errCode"];
                    $returnArr["errMsg"] = $specialUser["errMsg"];
                }
                echo json_encode($returnArr);
                break;

        }

    } else {
        $returnArr["errCode"] = 4;
        $returnArr["errMsg"] = "Please Add Special User";
        echo json_encode($returnArr);
    }


} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}
?>