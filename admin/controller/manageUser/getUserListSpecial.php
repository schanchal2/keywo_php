<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start model
require_once('../../model/manageUser/manageUserSpecail.php');

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    $connAdmin = createDBConnection("acl");
    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

    $limit = cleanQueryParameter($connAdmin, cleanXSS($_POST['limit']));

    $listtype = cleanQueryParameter($connAdmin, cleanXSS($_POST['listtype']));

    if ($listtype != "first") {

        $pageno = cleanQueryParameter($connAdmin, cleanXSS($_POST['pageno']));

        if ($pageno == "") {
            $pageno = 1;
        }

        $resultsPerPage = $limit;
        $start = ($pageno - 1) * $resultsPerPage;

        $userListCount = getUsersListSpecial(0,1,"");
        $userListCount = $userListCount["errMsg"];

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }

        if (!isset($pageno) || ($pageno == 1)) {
            $start = '0';
        } else {
            $start = $start;
        }

        $flag = 2;
        $result = getUsersListSpecial($skip,$flag,$resultsPerPage);
        $result = $result["errMsg"];

        if($result=="No Result" || $result=="Error in curl execution" || $result=="")
        {
            ?>
            <div id="userList" class="m-t-15">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <th>
                            <input id="select_all" type="checkbox" name="">
                        </th>
                        <th>Use Email</th>
                        <th>Account Handle </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="3">
                            <?php
                            echo "No Result Found";
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            exit;
        }

    }else
    {

        $skip = 0;

        $userListCount = getUsersListSpecial($skip,1,"");
        $userListCount = $userListCount["errMsg"];


        $flag = 2;
        $result = getUsersListSpecial($skip,$flag,$limit);
        $result = $result["errMsg"];

        if($result=="No Result" || $result=="Error in curl execution" || $result=="")
        {

                ?>
                <div id="userList" class="m-t-15">
                    <table class="table text-center table-responsive">
                        <thead>
                        <tr>
                            <th>
                                <input id="select_all" type="checkbox" name="">
                            </th>
                            <th>Use Email</th>
                            <th>Account Handle </th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3">
                                <?php
                                    echo "No Result Found";
                                ?>

                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <?php
                exit;
        }


        $resultsPerPage = $limit;

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }
    }


    $result=array_reverse($result)
    ?>
    <div id="userList" class="m-t-15">
        <table class="table text-center table-responsive">
            <thead>
            <tr>
                <th>
                    <input id="select_all" type="checkbox" name="">
                    <label for="select_all" class="fa"></label>
                </th>
                <th>Use Email</th>
                <th>Account Handle </th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($result as $result) {
                ?>
                <tr>
                    <td>
                        <input class="user_checkbox" id="user_checkbox<?php echo md5($result["email"]); ?>"
                               type="checkbox" name="user_checkbox[]"
                               value="<?php echo $result["email"]; ?>">
                        <label for="user_checkbox<?php echo md5($result["email"]); ?>" class="fa"></label>
                    </td>

                    <td> <?php echo $result["email"]; ?></td>
                    <td> <?php echo $result["account_handle"]; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <input type="text" id="hiddenpagespecial" name="hiddenpagespecial" value="<?php echo $pageno; ?>" hidden>
    <div style="" class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">

            <?php
            if ($pageno > 1) {

                $pagenum = 1;
                print('<li><a href="#"onclick=getUserSpecialListPagination("' . $pagenum . '","' . $resultsPerPage . '")>&laquo;</a></li>');
            }

            if ($pageno > 1) {
                $pagenumber = $pageno - 1;
                print('<li><a href="#" onclick=getUserSpecialListPagination("' . $pagenumber . '","' . $resultsPerPage . '")>Previous</a></li>');
            }

            if ($pageno == 1) {
                $startLoop = 1;
                $endLoop = ($lastpage < 5) ? $lastpage : 5;
            } else if ($pageno == $lastpage) {
                $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                $endLoop = $lastpage;
            } else {
                $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
            }

            for ($i = $startLoop; $i <= $endLoop; $i++) {
                if ($i == $pageno) {
                    print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
                } else {
                    $pagenumber = $i;
                    print('<li><a href="#" onclick=getUserSpecialListPagination("' . $pagenumber . '","' . $resultsPerPage . '")>' . $i . '</a></li>');
                }
            }
            if ($pageno < $lastpage) {
                $pagenumber = $pageno + 1;
                print('<li><a href="#" onclick=getUserSpecialListPagination("' . $pagenumber . '","' . $resultsPerPage . '")>Next</a></li>');

            }

            if ($pageno != $lastpage) {
                print('<li><a href="#" onclick=getUserSpecialListPagination("' . $lastpage . '","' . $resultsPerPage . '")>&raquo;</a></li>');
            }
            ?>

        </ul>
    </div>

    <?php

} else {
    $url = $adminView . "acl/index.php";
    echo '<script>window.location.replace("' . $url . '");</script>';
}

?>
<script>
    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>
