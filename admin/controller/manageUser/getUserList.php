    <?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start model
require_once('../../model/manageUser/manageUser.php');
if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    $connAdmin = createDBConnection("acl");
    noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

    $useremail = cleanQueryParameter($connAdmin, cleanXSS($_POST['useremail']));
    $userfname = cleanQueryParameter($connAdmin, cleanXSS($_POST['userfname']));
    $userlname = cleanQueryParameter($connAdmin, cleanXSS($_POST['userlname']));
    $useripaddress = cleanQueryParameter($connAdmin, cleanXSS($_POST['useripaddress']));
    $limit = cleanQueryParameter($connAdmin, cleanXSS($_POST['limit']));

    $listtype = cleanQueryParameter($connAdmin, cleanXSS($_POST['listtype']));

    if ($listtype != "first") {
        $order = cleanQueryParameter($connAdmin, cleanXSS($_POST['order']));
        $column = cleanQueryParameter($connAdmin, cleanXSS($_POST['column']));
        $pageno = cleanQueryParameter($connAdmin, cleanXSS($_POST['pageno']));

        if ($pageno == "") {
            $pageno = 1;
        }
        if ($column == "") {
            $column = "email";
        }

        if ($order == "") {
            $order = -1;
        }


        $resultsPerPage = $limit;
        $start = ($pageno - 1) * $resultsPerPage;

        $userListCount = getUsersList($useremail, $userfname, $userlname, "", "", "", 1, $useripaddress, "");
        $userListCount = $userListCount["errMsg"];

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }

        if (!isset($pageno) || ($pageno == 1)) {
            $start = '0';
        } else {
            $start = $start;
        }

        $flag = 2;
        $result = getUsersList($useremail, $userfname, $userlname, $skip, $order, $column, $flag, $useripaddress, $limit);
        $result = $result["errMsg"];


        if ($result["result"]) {
            $error = trim(strtolower($result["result"]));
        } else if ($result["errMsg"]) {
            $error = trim(strtolower($result["errMsg"]));
        }
        if ($result["code"] == "ECONNREFUSED") {
            $error = "ECONNREFUSED";
        }
        if ($error == "no results" || $error == "error in curl execution" || $error == "ECONNREFUSED" || $error=="e") {
            ?>
            <div id="userList" class="m-t-15">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <th>
                            <input id="select_all" type="checkbox" name=""><label for="select_all" class="fa"></label></th>
                        <th>Username</th>
                        <th>Email</th>
                        <th class="col-120">Sign-Up Date</th>
                        <th>Country</th>
                        <th>Balance</th>
                        <th>Account Status</th>
                        <th>Authentication</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8">
                            <?php
                            if ($error == "error in curl execution") {
                                echo "Error getting data from server";
                            } elseif ($error == "ECONNREFUSED") {
                                echo "Sever Connection Refused!";
                            } else {
                                echo "No Result Found";
                            }
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            exit;
        }
    } else {

        $order = -1;
        $skip = 0;
        $column = "email";

        $userListCount = getUsersList($useremail, $userfname, $userlname, "", "", "", 1, $useripaddress, "");
        $userListCount = $userListCount["errMsg"];


        $flag = 2;
        $result = getUsersList($useremail, $userfname, $userlname, $skip, $order, $column, $flag, $useripaddress, $limit);
        $result = $result["errMsg"];

        if ($result["result"]) {
            $error = trim(strtolower($result["result"]));
        }
        if ($result["errMsg"]) {
           $error = trim(strtolower($result));
        }

        if ($result["code"] == "ECONNREFUSED") {
            $error = "ECONNREFUSED";
        }
        if ($error == "no results" || $error == "error in curl execution" || $error == "ECONNREFUSED" || $error=="e") {
            ?>
            <div id="userList" class="m-t-15">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <th>
                            <input id="select_all" type="checkbox" name=""><label for="select_all" class="fa"></label></th>
                        <th>Username</th>
                        <th>Email</th>
                        <th class="col-120">Sign-Up Date</th>
                        <th>Country</th>
                        <th>Balance</th>
                        <th>Account Status</th>
                        <th>Authentication</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="8">
                            <?php
                            if ($error == "error in curl execution") {
                                echo "Error getting data from server";
                            } elseif ($error == "ECONNREFUSED") {
                                echo "Sever Connection Refused!";
                            } else {
                                echo "No Result Found";
                            }
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            exit;
        }
        $resultsPerPage = $limit;

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }
    }


    ?>
    <div id="userList" class="m-t-15">
        <table class="table text-center table-responsive">
            <thead>
            <tr>
                <th>
                    <input id="select_all" type="checkbox" name="">
                    <label for="select_all" class="fa"></label>
                </th>

                <th>Username</th>
                <th>Email</th>
                <th class="col-120">Sign-Up Date</th>
                <th>Country</th>
                <th>Balance <?= ucfirst($adminCurrency); ?></th>
                <th>Account Status</th>
                <th>Authentication</th>
            </tr>
            </thead>
            <tbody>
            <?php

            foreach ($result as $result) {
                ?>
                <tr>
                    <td>
                        <input class="user_checkbox" id="user_checkbox<?php echo md5($result["email"]); ?>"
                               type="checkbox" name="user_checkbox[]"
                               value="<?php echo $result["email"] . "," . $result["active"] . ",user_checkbox" . md5($result["email"]).",".$result["_id"] ?>">
                        <label for="user_checkbox<?php echo md5($result["email"]); ?>" class="fa"></label>
                    </td>
                    <td class="wrd-wrp_brk-wrd col-210">
                        <a target="_blank" href="<?php echo $adminView; ?>manageUsers/manageUser.php?tid=<?php echo base64_encode($result["email"]); ?>"><?php echo $result["first_name"] . " " . $result["last_name"]; ?></a>
                    </td>
                    <td class="wrd-wrp_brk-wrd col-210" > <?php echo $result["email"]; ?></td>
                    <td> <?php echo uDateTime("m-d-Y",$result["creationTime"]); ?></td>
                    <td> <?php echo $result["country"]; ?></td>
                    <td class="wrd-wrp_brk-wrd col-210"> <?php
                        $dataprice= getUsersBalance($result["email"])["errMsg"];
                        if($dataprice=="Error in curl execution")
                        {
                            echo "Error Connecting Wallet";
                        }else{
                            echo $dataprice;
                        }
                        ?> </td>
                    <td> <?php
                        $activeStatus = $result["active"];
                        if ($activeStatus == "0") {
                            echo "<span style=\"color:orangered\" >Deactive</span>";
                        }

                        if ($activeStatus == "1") {
                            echo "<span style=\"color:green\">Active</span>";
                        }

                        if ($activeStatus == "2") {
                            echo "<span style=\"color:red\">Blocked</span>";
                        }
                        ?>

                    </td>
                    <td> <?php
                        $trend_preference = $result["trend_preference"];
                        if ($trend_preference == 1) {
                            echo "Default";
                            $pref = "User Name Password (Default)";
                        } else if ($trend_preference == 2) {
                            echo "2FA";
                            $pref = "Two Factor Authentication (2FA)";
                        } else {
                            echo "NA";
                            $pref = "Not Set";
                        }
                        ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
    <input type="text" id="hiddenpage" name="hiddenpage" value="<?php echo $pageno; ?>" hidden>
    <div style="" class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">

            <?php
            if ($pageno > 1) {

                $pagenum = 1;
                print('<li><a href="#"onclick=getUserListPagination("' . $pagenum . '","' . $useremail . '","' . $column . '","' . $order . '","' . $userfname . '","' . $userlname . '","' . $useripaddress . '")>&laquo;</a></li>');
            }

            if ($pageno > 1) {
                $pagenumber = $pageno - 1;
                print('<li><a href="#" onclick=getUserListPagination("' . $pagenumber . '","' . $useremail . '","' . $column . '","' . $order . '","' . $userfname . '","' . $userlname . '","' . $useripaddress . '")>Previous</a></li>');
            }

            if ($pageno == 1) {
                $startLoop = 1;
                $endLoop = ($lastpage < 5) ? $lastpage : 5;
            } else if ($pageno == $lastpage) {
                $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                $endLoop = $lastpage;
            } else {
                $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
            }

            for ($i = $startLoop; $i <= $endLoop; $i++) {
                if ($i == $pageno) {
                    print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
                } else {
                    $pagenumber = $i;
                    print('<li><a href="#" onclick=getUserListPagination("' . $pagenumber . '","' . $useremail . '","' . $column . '","' . $order . '","' . $userfname . '","' . $userlname . '","' . $useripaddress . '")>' . $i . '</a></li>');
                }
            }
            if ($pageno < $lastpage) {
                $pagenumber = $pageno + 1;
                print('<li><a href="#" onclick=getUserListPagination("' . $pagenumber . '","' . $useremail . '","' . $column . '","' . $order . '","' . $userfname . '","' . $userlname . '","' . $useripaddress . '")>Next</a></li>');

            }

            if ($pageno != $lastpage) {
                print('<li><a href="#" onclick=getUserListPagination("' . $lastpage . '","' . $useremail . '","' . $column . '","' . $order . '","' . $userfname . '","' . $userlname . '","' . $useripaddress . '")>&raquo;</a></li>');
            }
            ?>

        </ul>
    </div>

    <?php

} else {
    $url = $adminView . "acl/index.php";
    echo '<script>window.location.replace("' . $url . '");</script>';
}

?>
<script>
    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    });

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>
