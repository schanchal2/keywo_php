<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start model
require_once('../../model/manageUser/manageUser.php');
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $type = cleanQueryParameter($connKeywords, cleanXSS(($_POST["type"])));

    switch($type)
    {

        case "afterBlockUnblockUser":
            $keywoStat=getKeywoStat($connKeywords);
            $keywoStat = $keywoStat["errMsg"][0];

            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] = $keywoStat;
            break;

    }
}else{
   // echo '<script>window.location.replace("view/acl/index.php");</script>';

    $returnArr["errCode"] = "1";
    $returnArr["errMsg"] = "Invalid Token!!";
}

echo(json_encode($returnArr));