<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');

//start model
require_once('../../model/manageUser/manageUser.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../../models/analytics/userRegistration_analytics.php";
$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$connKeyword = createDBConnection("dbkeywords");
noError($connKeyword) ? $connKeyword = $connKeyword["connection"] : checkMode($connKeyword["errMsg"]);
$returnArr = array();

$email = array();
$email = $_POST['email'];

$xmlProcessor = new xmlProcessor();
$logStorePathAdmin = $logPath["userManagement"];

$status = cleanQueryParameter($connAdmin, cleanXSS($_POST['status']));
$resonToblock = cleanQueryParameter($connAdmin, cleanXSS($_POST['resonToblock']));
$serviceReqID = cleanQueryParameter($connAdmin, cleanXSS($_POST['serviceReqID']));

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $xmlArray = initializeXMLLog($_SESSION["admin_id"]);
    $xml_data['request']["data"] = '';
    $xml_data['request']["attribute"] = $xmlArray["request"];

    if (!empty($email)) {

        switch ($status) {
            case "2":

                if (isset($_POST["status"]) && !empty($_POST["status"])) {

                    $xmlfilename = "BlockedUsersbyAdmin.xml";

                    foreach ($email as $value) {

                        $value=explode(":",$value);
                        $useremail=$value[0];
                        $userid=$value[1];
                        $currentStatus=$value[2];

                        $result = setUserStatusWallet($useremail, $status);
                        if($result["errCode"]=="-1")
                        {

                            $blockDetailsWallet=setUserBlockStatusWallet($useremail, $resonToblock,$_SESSION["admin_id"],$serviceReqID);

                            if($blockDetailsWallet["errCode"]=="-1") {
                                $result = setUserStatusKeywo($userid, $status);
                                if ($result["errCode"] == -1) {
                                    $blockDetailsKeywo = setUserBlockStatusKeywo($userid, $resonToblock, $_SESSION["admin_id"], $serviceReqID);

                                    if($blockDetailsKeywo["errCode"]=="-1") {
                                        if ($currentStatus != 2) {
                                            insert_user_statistics("blocked_users_block", $connKeyword);
                                            $blockedUser = $result["errMsg"]["email"];

                                            $returnArrXml["errCode"] = -1;
                                            $returnArrXml["errMsg"] = "User Blocked Successfully !!";
                                            $returnArrXml["blockedId"] = $blockedUser;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $returnArrXml;
                                            $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);

                                        }

                                        $returnArr["errCode"] = -1;
                                        $returnArr["errMsg"] = "User Blocked Successfully !!";
                                    }else{
                                        $returnArr["errCode"] = 3;
                                        $returnArr["errMsg"] = "Error Blcoking Users Account try again.";
                                    }


                                }
                            }

                    }
                    }


                } else {

                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = "Error In getting status users";

                }


                break;
            case "1":

                if (isset($_POST["status"]) && !empty($_POST["status"])) {

                    $xmlfilename = "un-BlockedUsersbyAdmin.xml";

                    foreach ($email as $value) {

                        $value=explode(":",$value);
                        $useremail=$value[0];
                        $userid=$value[1];
                        $currentStatus=$value[2];

                        $result = setUserStatusWallet($useremail, $status);

                        if($result["errCode"]=="-1") {
                            $result=setUserStatusKeywo($userid, $status);

                            if($result["errCode"]==-1) {
                                if($currentStatus!=1) {
                                    insert_user_statistics("blocked_users_unblock", $connKeyword);
                                    $unblockedUser = $result["errMsg"]["email"];
                                    $returnArrXml["errCode"] = -1;
                                    $returnArrXml["errMsg"] = "User Un_blocked Successfully !!";
                                    $returnArrXml["unblockedUser"] = $unblockedUser;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $returnArrXml;
                                    $xmlProcessor->writeXML($xmlfilename, $logStorePathAdmin, $xml_data, $xmlArray["activity"]);
                                }

                                $returnArr["errCode"] = -1;
                                $returnArr["errMsg"] = "User activated Successfully !!";
                            }else{
                                $returnArr["errCode"] = 3;
                                $returnArr["errMsg"] = "Error activating Users.";
                            }
                        }
                    }


                } else {

                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = "Error In getting status users";

                }


                break;
            case "zero":

                if (isset($_POST["status"]) && !empty($_POST["status"])) {

                    $status = "0";
                    foreach ($email as $value) {


                        $value=explode(":",$value);
                        $useremail=$value[0];
                        $userid=$value[1];
                        $currentStatus=$value[2];

                        $result = setUserStatusWallet($useremail, $status);

                        if($result["errCode"]=="-1") {
                            $result = setUserStatusKeywo($userid, $status);
                            $finalErrorCode=$result["errCode"];

                            $returnArr["errCode"] = -1;
                            $returnArr["errMsg"] = "User De-Activated Successfully !!";
                        }else{
                            $returnArr["errCode"] = 3;
                            $returnArr["errMsg"] = "Error De-Activating Users.";
                        }
                    }


                } else {

                    $returnArr["errCode"] = 3;
                    $returnArr["errMsg"] = "Error In getting status users";

                }


                break;
        }
    } else {
        $returnArr["errCode"] = 4;
        $returnArr["errMsg"] = "Plese select checkbox to do Action!!";

    }
    echo json_encode($returnArr);

} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}
?>