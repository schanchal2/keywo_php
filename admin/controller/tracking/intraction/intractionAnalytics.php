<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/intraction/intraction_analytics.php');
//end other




$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$intractionType = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["resultFor"])));


$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$country = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["country"])));
$device = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["device"])));
$app = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["app"])));
$postType = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["postType"])));
$browser="";

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {



switch($intractionType)
{
    case "search":

        $postType="";

        $qualified=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"qualified",$postType,$intractionType,$connDemo)["errMsg"][0];
        $qualifiedEarning=(double) $qualified["intraction_earning"];
        $qualifiedCount=(int) $qualified["no_of_searchORview_social"];

        $unqualifiedresult=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"unqualified",$postType,$intractionType,$connDemo);
        $unqualified=$unqualifiedresult["errMsg"][0];
        $unqualifiedEarning=(double) $unqualified["intraction_earning"];
        $unqualifiedCount=(int) $unqualified["no_of_searchORview_social"];


        $totalCount=(int) ($qualifiedCount+$unqualifiedCount);
        //$totalEarning=$qualifiedEarning+$unqualifiedEarning;


        $counts=array();
        $counts["qualified"]=$qualifiedCount;
        $counts["unqualified"]=$unqualifiedCount;
        $counts["total"]=$totalCount;

        if ($unqualifiedresult["errCode"] == "-1") {
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $counts;
        } else {
            $returnArr["errCode"] = 3;
            $returnArr["errMsg"] = "Error getting Intraction Analytics.";
        }

        break;


    case "social":


        $qualified=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"qualified",$postType,$intractionType,$connDemo)["errMsg"][0];
        $qualifiedEarning=(double) $qualified["intraction_earning"];
        $qualifiedCount=(int) $qualified["no_of_searchORview_social"];

        $unqualifiedresult=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"unqualified",$postType,$intractionType,$connDemo);
        $unqualified=$unqualifiedresult["errMsg"][0];
        $unqualifiedEarning=(double) $unqualified["intraction_earning"];
        $unqualifiedCount=(int) $unqualified["no_of_searchORview_social"];


        $totalCount=(int) ($qualifiedCount+$unqualifiedCount);
        //$totalEarning=$qualifiedEarning+$unqualifiedEarning;


        $counts=array();
        $counts["qualified"]=$qualifiedCount;
        $counts["unqualified"]=$unqualifiedCount;
        $counts["total"]=$totalCount;

        if ($unqualifiedresult["errCode"] == "-1") {
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $counts;
        } else {
            $returnArr["errCode"] = 3;
            $returnArr["errMsg"] = "Error getting Intraction Analytics.";
        }

        break;

    case "combined":
        $app="";
        $postType="";
        $intractionType="";
        $qualified=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"qualified",$postType,$intractionType,$connDemo)["errMsg"][0];
        $qualifiedEarning=(double) $qualified["intraction_earning"];
        $qualifiedCount=(int) $qualified["no_of_searchORview_social"];

        $unqualifiedresult=getIntractionAnalytics($fromdate,$todate,$device,$browser,$gender,$country,$app,"unqualified",$postType,$intractionType,$connDemo);
        $unqualified=$unqualifiedresult["errMsg"][0];
        $unqualifiedEarning=(double) $unqualified["intraction_earning"];
        $unqualifiedCount=(int) $unqualified["no_of_searchORview_social"];


        $totalCount=(int) ($qualifiedCount+$unqualifiedCount);
        //$totalEarning=$qualifiedEarning+$unqualifiedEarning;


        $counts=array();
        $counts["qualified"]=$qualifiedCount;
        $counts["unqualified"]=$unqualifiedCount;
        $counts["total"]=$totalCount;

        if ($unqualifiedresult["errCode"] == "-1") {
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $counts;
        } else {
            $returnArr["errCode"] = 3;
            $returnArr["errMsg"] = "Error getting Intraction Analytics.";
        }

        break;
}






} else {

        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "INVALID TOKEN!!";

}
echo(json_encode($returnArr));
?>








