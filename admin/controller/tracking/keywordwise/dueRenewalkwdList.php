
<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other

/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$expiry = str_replace("_"," ",cleanQueryParameter($connKeywords, cleanXSS($_POST["expiry"])));


if($expiry=="")
{
    $expiry="3 month";
}
//Get page number from Ajax
if(isset($_POST["page"])){
    $page_number = (int) filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
}else{
    $page_number = 1; //if there's no page number, set it to 1
}
if($page_number==0)
{
    $page_number=1;
}
$item_per_page=$limit;
$get_total_rows=getExpiredKeywordDataCount($connKeywords,$expiry)["errMsg"];

$total_pages = ceil($get_total_rows/$item_per_page);

//position of records
$page_position = (($page_number-1) * $item_per_page);
$lastpage = ceil($total_pages);


$results=getExpiredKeywordData($connKeywords, $page_position,$item_per_page,$expiry);
$results=$results["errMsg"];
$count=count($results);

if($count==0)
{
    $page_number = $page_number-1; //if there's no page number, set it to 1
    $item_per_page=$limit;
    $get_total_rows=getExpiredKeywordDataCount($connKeywords,$expiry)["errMsg"];

    $total_pages = ceil($get_total_rows/$item_per_page);

//position of records
    $page_position = (($page_number-1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results=getExpiredKeywordData($connKeywords, $page_position,$item_per_page,$expiry);
    $results=$results["errMsg"];
}

?>
<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive">
    <thead>
    <tr>
        <th class="firstColumn">
            <input id="select_all" type="checkbox" name="">
            <label for="select_all" class="fa"></label>
        </th>
        <th> Keyword </th>
        <th> Current Owner</th>
        <th style="width: 20%"> Last Traded/Purchase Date </th>
        <th style="width: 20%"> Expiry Date </th>
        <?php
        if ((in_array("write", $myp))) {

        ?>
        <th class="no-sort dtTableHeadPadding lastColumn" style="width:7%;text-align: center"> &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>
<?php } ?>
    </tr>

    </thead>
    <tbody>
    <?php
    $count = ($page_number - 1) * $item_per_page;

    if($get_total_rows==0)
    {
        ?>
        <tr>
            <?php
            if ((in_array("write", $myp))) {

            ?>
            <td style="text-align: center" colspan="6">No Data Found</td>
           <?php }
           else{
?> <td style="text-align: center" colspan="5">No Data Found</td>
            <?php
           }
           ?>
        </tr>
        <?php
    }else {
        foreach ($results as $value) {
            $count = $count + 1;
            ?>
            <tr>
                <td style="width:7%;padding: 5px;" class="firstColumn">
                    <input class="user_checkbox" id="user_checkbox<?php echo md5($value["buyer_id"]); ?>"
                           type="checkbox" name="user_checkbox[]"
                           value="<?php echo $value["buyer_id"]/*subject owner*/ . "," . $value["keyword"] /*subject*/ . ",user_checkbox" . md5($value["buyer_id"]); ?>">
                    <label for="user_checkbox<?php echo md5($value["buyer_id"]); ?>" class="fa"></label>
                </td>
                <td><?php echo $value['keyword']; ?></td>
                <td><?php echo $value['buyer_id']; ?></td>

                <td>
                    <span class="largeDate"> <?php echo uDateTime("d-m-Y", $value['purchase_timestamp']); ?> </span>
                    <!--                <time> 12:05:26</time>-->
                </td>
                <td>
                    <span class="largeDate"> <?php echo uDateTime("d-m-Y", $value['ownership_expiry_time']); ?> </span>
                    <!--                <time> 12:05:26</time>-->
                </td>
            <?php
            if ((in_array("write", $myp))) {

                ?>

                <td style="width:7%;text-align: center" class="lastColumn"><a href="javascript:;" data-toggle="modal"
                                                           data-target="#addTotracklistmodal"><i
                            class="fa fa-plus-square "
                            onclick=addElementToModal('<?php echo $value["buyer_id"]; ?>','<?php echo $value["keyword"]; ?>')
                            data-value="<?php echo $value["buyer_id"]; ?>"> </i></a></td>
                <?php } ?>
            </tr>
        <?php }
    }?>
    </tbody>

    </tfoot>
</table>
</div>
<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage,$page_number,$limit,$expiry);
function getPaginationData($lastpage,$pageno,$limit,$expiry)
{

    $expiry = str_replace(" ","_",$expiry);
    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="'.$pageno.'" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo'<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum=1;
        print('<li><a href="#"onclick=dueRenewalkwdList("'.$pagenum.'","'.$expiry.'","'.$limit.'")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber=$pageno - 1;
        print('<li><a href="#" onclick=dueRenewalkwdList("'.$pagenumber.'","'.$expiry.'","'.$limit.'")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber=$i;
            print('<li><a href="#" onclick=dueRenewalkwdList("'.$pagenumber.'","'.$expiry.'","'.$limit.'")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber=$pageno + 1;
        print('<li><a href="#" onclick=dueRenewalkwdList("'.$pagenumber.'","'.$expiry.'","'.$limit.'")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=dueRenewalkwdList("'.$lastpage.'","'.$expiry.'","'.$limit.'")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

function getExpiredKeywordDataCount($connKeywords,$expiry) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    // set session sql_mode='STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';
    $query = "SELECT COUNT(*) as count FROM view_keyword_due_for_renewal WHERE ownership_expiry_time BETWEEN now() AND now() + INTERVAL {$expiry};";
    $result = runQuery($query, $connKeywords);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["count"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }

    return $returnArr;
}


function getExpiredKeywordData($connKeywords, $page_position,$item_per_page,$expiry)
{

    $returnArr = array();

     $query = "	SELECT buyer_id,keyword,purchase_timestamp,ownership_expiry_time FROM view_keyword_due_for_renewal WHERE ownership_expiry_time BETWEEN now() AND now() + INTERVAL {$expiry} ORDER BY ownership_expiry_time DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connKeywords);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}
?>


<script>

    $("#LimitedResults").change(function(){

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>