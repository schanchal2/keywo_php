<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');

//end other

//printArr($_POST);
?>

<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
<table class="table  text-center table-responsive">
    <thead>
    <tr>
        <th>
            <input id="select_all" type="checkbox" name="">
            <label for="select_all" class="fa"></label>
        </th>
        <th> Purchase Timestamp </th>
        <th> Cancelled Timestamp </th>
        <th> Keyword </th>
        <th> Order ID (ITD) </th>
        <th> Kwd Price (ITD)</th>
        <th> Purchased By </th>
        <th> Country </th>
        <th> Payment Mode </th>
        <th> SR-Number </th>
    </tr>

    </thead>
    <tbody>

    <tr>
        <td>
            <input class="user_checkbox" id="user_checkbox"
                   type="checkbox" name="user_checkbox[]"
                   value="">
            <label for="user_checkbox" class="fa"></label>
        </td>
        <td>
            <span class="date"> 01-11-2015 </span>
            <time> 12:05:26</time>
        </td>
        <td>
            <span class="date"> 01-11-2015 </span>
            <time> 12:05:26</time>
        </td>
        <td>#India</td>
        <td>ITD-444</td>
        <td>10 ITD</td>
        <td>dg@grr.la</td>
        <td>India</td>
        <td>Paypal</td>
        <td>SR-1012</td>

    </tr>


    </tbody>

    </table>
</div>


<script>

    $("#LimitedResults").change(function(){

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>