<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/kwd_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$payment_mode = cleanQueryParameter($connDemo, cleanXSS($_POST["paymentMode"]));

//printArr($_POST);

/*****************************************************************************/
$apiName="getsoldkeyword";
$flag="2";
$transaction_type="";
/*****************************************************************************/

$result=keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device,$apiName,$flag,$payment_mode,$transaction_type);

if(noError($result))
{
    $result=$result["errMsg"]["batched_container"][0];

}

?>
<div class="row">
    <div class="col-lg-12">
        <table class="table  text-center table-responsive">
            <thead>
            <tr>
                <th>Keyword Sold By Paypal</th>
                <th>Keyword Sold By Bitcoin</th>
                <th>Total Sale</th>
                <th>Total Sale Amount (ITD)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id=""> <?= isset($result["keywords_soldby_paypal"])?$result["keywords_soldby_paypal"]:0; ?> </td>
                <td id=""> <?= isset($result["keywords_soldby_bitcoin"])?$result["keywords_soldby_bitcoin"]:0; ?> </td>
                <td id=""> <?= isset($result["total_keyword_sale"])?$result["total_keyword_sale"]:0; ?> </td>
                <td id=""> <?= isset($result["total_sale_amount"])?$result["total_sale_amount"]:0; ?> </td>
            </tr>
            </tbody>
        </table>
    </div>


</div>