<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/keywordSearchInteraction.php');
//end other

/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/

//printArr($_POST);
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$fromdate    = cleanQueryParameter($connKeywords, cleanXSS($_POST['fromdate']));
$todate      = cleanQueryParameter($connKeywords, cleanXSS($_POST['todate']));
$sortBy     = cleanQueryParameter($connKeywords, cleanXSS($_POST['sortBy']));
$bidstatus=0;
$result=getKeywordBidInDateRange($fromdate, $todate,$bidstatus,$sortBy, $connKeywords);
$result=$result["errMsg"];

?>
<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive" id="bidKwdListTable">
        <thead>
        <tr>
            <th class="no-sort dtTableHeadPadding" style="width:7%;"><input id="select_all" name="" type="checkbox"><label for="select_all" class="fa"></label></th>
            <th class="no-sort dtTableHeadPadding"> Keyword </th>
            <th class="no-sort dtTableHeadPadding" data-toggle="tooltip" data-placement="top" title="Sort" style="cursor:pointer" onclick="getSortedData('bid_count')"> Nos. of Bid Received </th>
            <th class="no-sort dtTableHeadPadding" data-toggle="tooltip" data-placement="top" title="Sort" style="cursor:pointer" onclick="getSortedData('bid_price')"> Bid Amount </th>
            <?php
            if ((in_array("write", $myp))) {

            ?>
            <th class="no-sort dtTableHeadPadding" style="width:7%;text-align: center"> &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>
<?php } ?>
        </tr>

        </thead>
        <tbody>
        <?php



        foreach($result as $key => $value) {?>
            <tr>
                <td style="width:7%;padding: 5px;">
                    <input class="user_checkbox" id="user_checkbox<?php echo md5($value["kwd_owner_email"]); ?>"
                           type="checkbox" name="user_checkbox[]"
                           value="<?php echo $value["kwd_owner_email"]/*subject owner*/ . "," . $value["keyword"] /*subject*/. ",user_checkbox" . md5($value["kwd_owner_email"]); ?>">
                    <label for="user_checkbox<?php echo md5($value["kwd_owner_email"]); ?>" class="fa"></label>
                </td>
                <td><?php echo $value['keyword']; ?></td>
                <td><?php echo $value['bid_count']; ?></td>
                <td><?php echo number_format($value['bid_price'],6)." ITD"; ?></td>
                <?php
                if ((in_array("write", $myp))) {

                ?>
                <td style="width:7%;text-align: center"><a href="javascript:;" data-toggle="modal" data-target="#addTotracklistmodal"><i class="fa fa-plus-square " onclick=addElementToModal('<?php echo $value["kwd_owner_email"]; ?>','<?php echo $value["keyword"]; ?>') data-value="<?php echo $value["kwd_owner_email"]; ?>"> </i></a></td>
           <?php } ?>
            </tr>
        <?php } ?>

        </tbody>

        </tfoot>
    </table>
</div>

<script>

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $('#bidKwdListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });

</script>