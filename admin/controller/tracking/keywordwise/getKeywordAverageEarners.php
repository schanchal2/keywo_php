<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/keywordAverageEarners.php');
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/

$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords['connection'] : checkMode($connKeywords['errMsg']);

$fromdate    = cleanQueryParameter($connKeywords, cleanXSS($_POST['fromdate']));
$todate      = cleanQueryParameter($connKeywords, cleanXSS($_POST['todate']));


$results = getKeywordAverageEarnersData($fromdate, $todate, "0", "1000", $connKeywords);
$results = $results['errMsg'];

?>

<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive" id="searchUserListTable">
    <thead>
    <tr>
        <th class="no-sort dtTableHeadPadding" style="width:7%;"><input id="select_all" name="" type="checkbox"><label for="select_all" class="fa"></label></th>
        <th class="no-sort dtTableHeadPadding" style="width:7%;">Keyword</th>
        <th class="no-sort dtTableHeadPadding">Keyword Earnings</th>
        <th class="no-sort dtTableHeadPadding">Nos. of View+Searches (Qualified)</th>
        <th class="no-sort dtTableHeadPadding">Avg. Earing (Per Interaction) </th>
    </tr>

    </thead>
    <tbody>
    <?php foreach($results as $key => $value){?>
    <tr>
        <td>
            <input class="user_checkbox" id="user_checkbox"
                   type="checkbox" name="user_checkbox[]"
                   value="">
            <label for="user_checkbox" class="fa"></label>
        </td>
        <td><?php echo $value['keyword']; ?></td>
        <td><?php echo $value['total_interaction_earning']; ?></td>
        <td><?php echo $value['total_interaction_count']; ?></td>
        <td>
            <?php
                if($value['total_interaction_count'] != 0){
                    $avg_earning = ($value['total_interaction_earning']/$value['total_interaction_count']);
                }else {
                    $avg_earning = $value['total_interaction_earning'];
                }
                    echo $avg_earning;
            ?>

        </td>
    </tr>
    <?php } ?>

    </tbody>

    </table>
</div>


<script>

    $("#LimitedResults").change(function(){

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $('#searchUserListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });

</script>