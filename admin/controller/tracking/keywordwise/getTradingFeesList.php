<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/kwd_analytics.php');

//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));


//printArr($_POST);

/*****************************************************************************/
$apiName="gettradingkeyword";
$payment_mode="";
$flag="1";
$transaction_type="trade_fees";
/*****************************************************************************/

$result=keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device,$apiName,$flag,$payment_mode,$transaction_type);


if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];

}


?>
<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive" id="searchUserListTable">
        <thead>
        <tr>
            <th class="dtTableHeadPadding no-sort ">
                <input id="select_all" type="checkbox" name=""><label for="select_all" class="fa"></label>
            </th>
            <th class="dtTableHeadPadding no-sort "> Keyword </th>
            <th class="dtTableHeadPadding no-sort "> Keyword Owner</th>
            <th class="dtTableHeadPadding no-sort "> Keyword Traded Txns.</th>
            <th class="dtTableHeadPadding no-sort "> Total Trading Amt </th>
            <th class="dtTableHeadPadding no-sort "> Trading Fees </th>
            <?php
            if ((in_array("write", $myp))) {

            ?>
            <th class="dtTableHeadPadding no-sort "> Tracklist &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>
        <?php } ?>
        </tr>
        </thead>
        <tbody>

        <?php

        foreach ($result as $value) {

            echo "<tr>";
            ?>

            <td>
                <input class="user_checkbox" id="user_checkbox<?php echo md5($value["buyer_email"]); ?>"
                       type="checkbox" name="user_checkbox[]"
                       value="<?php echo $value["buyer_email"]/*subject owner*/ . "," . $value["keyword"] /*subject*/. ",user_checkbox" . md5($value["buyer_email"]); ?>">
                <label for="user_checkbox<?php echo md5($value["email"]); ?>" class="fa"></label>
            </td>
            <?php
            echo '<td>' . $value['keyword'] . '</td>';
            echo '<td>' . $value['keyword_owner'] . '</td>';
            echo '<td>' . $value['keyword_transaction_count'] . '</td>';
            echo '<td>' . number_format($value['total_keyword_tradeamount'],6) . '</td>';
            echo '<td>' . number_format($value['total_tradingfees'],6) . '</td>';

                if ((in_array("write", $myp))) {

                    ?>
                    <td><a href="javascript:;" data-toggle="modal" data-target="#addTotracklistmodal"><i
                                    class="fa fa-plus-square "
                                    onclick=addElementToModal('<?php echo $value["buyer_email"]; ?>','<?php echo $value["keyword"]; ?>')
                                    data-value="<?php echo $value["buyer_email"]; ?>"> </i></a></td>
                    <?php
                }
            echo "</tr>";

        }
        ?>


        </tbody>

    </table>
</div>


<script>

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $('#searchUserListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]
    });
</script>

