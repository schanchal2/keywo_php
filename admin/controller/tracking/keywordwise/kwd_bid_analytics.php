<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/keywordSearchInteraction.php');
//end other



$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$fromdate    = cleanQueryParameter($connKeywords, cleanXSS($_POST['fromdate']));
$todate      = cleanQueryParameter($connKeywords, cleanXSS($_POST['todate']));
$sortBy     = cleanQueryParameter($connKeywords, cleanXSS($_POST['sortBy']));
$bidstatus=0;


if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $result=getKeywordBidInDateRangeCount($fromdate, $todate,$bidstatus,$sortBy, $connKeywords);
   if(noError($result))
   {
       $result=$result["errMsg"][0];

     if($result["bid_price"]=="")
     {
         $result["bid_price"]=0;
     }
       $returnArr["errCode"] = -1;
       $returnArr["errMsg"] = $result;
   }else{
       $returnArr["errCode"] = 2;
       $returnArr["errMsg"] = "Error in finding analytics for Keyword Bid.";
   }
} else {

        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "INVALID TOKEN!!";

}
echo(json_encode($returnArr));
?>








