<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/kwd_analytics.php');

//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$payment_mode = cleanQueryParameter($connDemo, cleanXSS($_POST["paymentMode"]));

//printArr($_POST);

/*****************************************************************************/
$apiName="getsoldkeyword";
$flag="1";
$transaction_type="";
/*****************************************************************************/

$result=keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device,$apiName,$flag,$payment_mode,$transaction_type);

if(noError($result))
{
    $result=$result["errMsg"]["batched_container"];

}
?>
<div id="userList" class="m-t-15">
<table class="table text-center table-responsive" id="searchUserListTable">
    <thead>
    <tr>
        <th class="dtTableHeadPadding no-sort ">
            <input id="select_all" type="checkbox" name=""><label for="select_all" class="fa"></label>
        </th>
        <th class="dtTableHeadPadding no-sort "> Timestamp </th>
        <th class="dtTableHeadPadding no-sort "> Keyword </th>
        <!--<th class="dtTableHeadPadding no-sort "> Order ID (ITD) </th>-->
        <th class="dtTableHeadPadding no-sort "> Kwd Price (ITD)</th>
        <th class="dtTableHeadPadding no-sort "> Purchased By </th>
        <!--<th class="dtTableHeadPadding no-sort "> 2FA </th>-->
        <th class="dtTableHeadPadding no-sort "> Country </th>
        <th class="dtTableHeadPadding no-sort "> Payment Mode </th>
        <?php
        if ((in_array("write", $myp))) {

        ?>
        <th class="dtTableHeadPadding no-sort "> Tracklist &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>
    <?php } ?>
    </tr>
    </thead>
    <tbody>

    <?php

    foreach ($result as $value) {

        echo "<tr>";
        ?>

        <td>
            <input class="user_checkbox" id="user_checkbox<?php echo md5($value["email"]); ?>"
                   type="checkbox" name="user_checkbox[]"
                   value="<?php echo $value["email"]/*subject owner*/ . "," . $value["keyword"] /*subject*/. ",user_checkbox" . md5($value["email"]); ?>">
            <label for="user_checkbox<?php echo md5($value["email"]); ?>" class="fa"></label>
        </td>
        <td class="timestamp">
        <span class="date"> <?php echo uDateTime("d-m-Y", $value["time_stamp"]); ?></span>
        <time> <?php echo uDateTime("h:i:s", $value["time_stamp"]); ?></time>
        </td>
        <?php
        echo '<td>' . $value['keyword'] . '</td>';
        //echo '<td>' . $value['order_id'] . '</td>';
        echo '<td>' . number_format($value['keyword_price'],6) . '</td>';
        echo '<td>' . $value['email'] . '</td>';
        //echo '<td>' . $value['2fa'] . '</td>';
        echo '<td>' . $value['country'] . '</td>';
        echo '<td>' . $value['payment_mode'] . '</td>';

                if ((in_array("write", $myp))) {

                    ?>
                    <td><a href="javascript:;" data-toggle="modal" data-target="#addTotracklistmodal"><i
                                    class="fa fa-plus-square "
                                    onclick=addElementToModal('<?php echo $value["email"]; ?>','<?php echo $value["keyword"]; ?>')
                                    data-value="<?php echo $value["email"]; ?>"> </i></a></td>
                    <?php
                }
        echo "</tr>";

    }
    ?>


    </tbody>

</table>
</div>


<script>

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $('#searchUserListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]
    });
</script>
