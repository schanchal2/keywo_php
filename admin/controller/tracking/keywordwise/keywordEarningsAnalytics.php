<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/keywordEarnings.php');
require_once('../../../model/tracking/keywordwise/kwd_analytics.php');
//end other


$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords['connection'] : checkMode($connKeywords['errMsg']);

$fromdate = cleanQueryParameter($connKeywords, cleanXSS($_POST['fromdate']));
$todate   = cleanQueryParameter($connKeywords, cleanXSS($_POST['todate']));
$country  = cleanQueryParameter($connKeywords, cleanXSS($_POST['country']));
$device   = cleanQueryParameter($connKeywords, cleanXSS($_POST['DeviceData']));

$results=getKeywordEarningAnalytics($fromdate, $todate, $device, $country, $connKeywords);
$results=$results['errMsg'];


/*****************************************************************************/
$apiName="getsoldkeyword";
$flag="2";
$transaction_type="";
/*****************************************************************************/


$resultkwd=keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device,$apiName,$flag,$payment_mode,$transaction_type);

if(noError($resultkwd))
{
    $resultkwd=$resultkwd["errMsg"]["batched_container"][0];

}
?>
<table class="table  text-center table-responsive">
    <thead>
        <tr>
            <th>Keywords Owned(Nos.)</th>
            <th>Keywords Searches(Nos.)</th>
            <th>Post Keywords Views(Nos.)</th>
            <th>Keywords Earning(Amt)</th>
        </tr>
    </thead>
    <tbody>
    <tr>
        <td id="Ad_Campaign_in_Network"><?= isset($resultkwd["total_keyword_sale"])?$resultkwd["total_keyword_sale"]:0; ?> </td>
        <td id="Keywords_searches"><?php echo ($results['total_searchInteraction_count'] != '') ?  $results['total_searchInteraction_count'] : '0'; ?></td>
        <td id="Post_Keywords_Views"><?php echo ($results['total_socialInteraction_count'] != '') ?  $results['total_socialInteraction_count'] : '0'; ?></td>
        <td id="Keywords_Earning"><?php echo ($results['total_interaction_earning'] != '') ?  $results['total_interaction_earning'] : '0'; ?></td>
    </tr>
    </tbody>
</table>
