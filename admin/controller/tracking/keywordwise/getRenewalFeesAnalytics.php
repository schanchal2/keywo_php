<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/kwd_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));


//printArr($_POST);

/*****************************************************************************/
$apiName="getrenewalkeyword";
$payment_mode="";
$flag="2";
$transaction_type="renewal_fees";
/*****************************************************************************/

$result=keyword_Top_1000_Analytics($fromdate, $todate, $country, $gender, $device,$apiName,$flag,$payment_mode,$transaction_type);


if(noError($result))
{
    $result=$result["errMsg"]["batched_container"]["0"];

}

?>
<div class="col-lg-12">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>Keyword Renewed (Nos.)</th>
            <th>Renewal Fees Collected</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id=""> <?= isset($result["total_keyword_renewalcount"])?$result["total_keyword_renewalcount"]:0; ?> </td>
            <td id=""> <?= isset($result["total_renewalfees"])?number_format($result["total_renewalfees"],6):0; ?></td>
        </tr>
        </tbody>
    </table>
</div>


