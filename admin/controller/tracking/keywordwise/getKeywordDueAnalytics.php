<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/keywords/kwdDueAnalytics.php');
//end other

$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords['connection'] : checkMode($connKeywords['errMsg']);

$expiry = cleanQueryParameter($connKeywords, cleanXSS($_POST['expiry']));
?>
<table class="table  text-center table-responsive">
    <thead>
    <tr>

        <th>Keywords Due for Renewal</th>
        <th>Total Keywords Expired</th>

    </tr>
    </thead>
    <tbody>
    <tr>

        <td id=""> <?php echo keywordDueForRenewal($connKeywords,$expiry)["errMsg"]; ?> </td>
        <td id=""> <?php echo keywordsExpired($connKeywords)["errMsg"]; ?> </td>
    </tr>
    </tbody>
</table>