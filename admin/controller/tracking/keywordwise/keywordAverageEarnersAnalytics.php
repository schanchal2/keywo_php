<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/keywordwise/keywordAverageEarners.php');
//end other

$connKeywords = createDBConnection('dbkeywords');
noError($connKeywords) ? $connKeywords = $connKeywords['connection'] : checkMode($connKeywords['errMsg']);

$fromdate = cleanQueryParameter($connKeywords, cleanXSS($_POST['fromdate']));
$todate   = cleanQueryParameter($connKeywords, cleanXSS($_POST['todate']));;

$results=getKeywordAverageEarnersAnalytics($fromdate, $todate, $connKeywords);
$results=$results['errMsg'];
foreach ($results as $key => $value){
   $count   +=  $value['total_interaction_count'];
   $earning +=  $value['total_interaction_earning'];
}
?>
<div class="row">
    <div class="col-lg-12">
        <table class="table  text-center table-responsive">
            <thead>
            <tr>
                <th>Nos. of Owned Keywords </th>
                <th>Nos. of View+Searches</th>
                <th>Keyword Earnings</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id=""> 0 </td>
                <td id=""><?php echo ($count == '') ? '0' : $count;?></td>
                <td id=""><?php echo ($earning == '') ? '0' : $earning; ?></td>
            </tr>
            </tbody>
        </table>
    </div>


</div>