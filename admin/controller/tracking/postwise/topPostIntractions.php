<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/postwise/postwise_analytics.php');
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);


$fromdate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]))));
$todate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]))));
$postType = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["postType"])));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["device"])));
$result=getPostInteractions($fromdate,$todate,$postType,$gender,$device,$country);
$result=$result["errMsg"];

?>
<div id="userList" class="m-t-15">
    <table class="table text-center table-responsive" id="searchUserListTable">
        <thead>
        <tr>
            <th rowspan="2" class="dtTableHeadPadding no-sort">
                <input id="select_all" type="checkbox" name="">
                <label for="select_all" class="fa"></label>
            </th>
            <th rowspan="2" class="dtTableHeadPadding no-sort"> Post ID </th>
            <th rowspan="2" class="dtTableHeadPadding no-sort"> Post Type</th>
            <th  colspan="4" class="dtTableHeadPadding no-sort"> Total No. of Intractions </th>
            <?php
            if ((in_array("write", $myp))) {

            ?>
            <th rowspan="2" class="dtTableHeadPadding no-sort"> Tracklist &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>
       <?php } ?>
        </tr>
        <tr>

            <th class="bg-darkblue dtTableHeadPadding no-sort">Likes </th>
            <th class="bg-darkblue dtTableHeadPadding no-sort"> Comments </th>
            <th class="bg-darkblue dtTableHeadPadding no-sort"> Share </th>
            <th class="bg-darkblue dtTableHeadPadding no-sort"> Earning </th>

        </tr>


        </thead>
        <tbody>

        <?php

        foreach ($result as $value) {

            $email=$value["user_ref"]["email"];

            echo "<tr>";
            ?>

            <td>
                <input class="user_checkbox" id="user_checkbox<?php echo md5($email); ?>"
                       type="checkbox" name="user_checkbox[]"
                       value="<?php echo $email/*subject owner*/ . "," . $value["_id"] /*subject*/. ",user_checkbox" . md5($email); ?>">
                <label for="user_checkbox<?php echo md5($email); ?>" class="fa"></label>
            </td>
            <?php
            echo '<td>' . $value['_id'] . '</td>';
            echo '<td>' . $value['post_type'] . '</td>';
            echo '<td>' . $value['like_count'] . '</td>';
            echo '<td>' . $value['comment_count'] . '</td>';
            echo '<td>' . $value['share_count'] . '</td>';
            echo '<td>' . $value['post_earnings'].' '.$adminCurrency.'</td>';

                if ((in_array("write", $myp))) {

                    ?>
                    <td><a href="javascript:;" data-toggle="modal" data-target="#addTotracklistmodal"><i
                                    class="fa fa-plus-square "
                                    onclick=addElementToModal('<?php echo $email; ?>','<?php echo $value["_id"]; ?>')
                                    data-value="<?php echo $email; ?>"> </i></a></td>
                    <?php
                }
            echo "</tr>";

        }
        ?>


        </tbody>

    </table>
</div>


<script>

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $('#searchUserListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]

    });
</script>