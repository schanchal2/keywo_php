<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/postwise/postwise_analytics.php');
//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);


$fromdate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]))));
$todate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]))));
$postType = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["postType"])));
$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["gender"])));
$device = strtolower(cleanQueryParameter($connDemo, cleanXSS($_POST["device"])));



if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $result=getTopPostEarnings($fromdate,$todate,$postType,$gender,$device,$country,"2");

    if ($result["errCode"] == "-1") {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = $result["errMsg"];
    } else {
        $returnArr["errCode"] = 3;
        $returnArr["errMsg"] = "Error getting Analytics Post.";
    }


} else {

    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "INVALID TOKEN!!";

}
echo(json_encode($returnArr));
?>








