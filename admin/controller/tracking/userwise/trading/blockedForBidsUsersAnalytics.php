<?php
session_start();

//start config
require_once('../../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../../config/db_config.php');
//end config

//start helper
require_once('../../../../helpers/coreFunctions.php');
require_once('../../../../helpers/arrayHelper.php');
require_once('../../../../helpers/stringHelper.php');
require_once('../../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../../core/errorMap.php');
require_once('../../../../model/tracking/keywordwise/kwd_analytics.php');

//end other
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$fromdate = cleanQueryParameter($connKeywords, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connKeywords, cleanXSS($_POST["todate"]));
$country="";
$device="";
$gender="";
;

$result=getBlockedForBidUsersAnalytics($fromdate, $todate, $device, $country, $gender, $connKeywords);
$result=$result["errMsg"];

?>

<table class="table  text-center table-responsive">
    <thead>
    <tr>
        <th>User Participating in Bid (Nos.)</th>
        <th>Bid Placed (Nos.) </th>
        <th>Total Blocked Amount</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td id="users_in_bid">  <?= isset($result["user_participated_in_bid"])?$result["user_participated_in_bid"]:0; ?> </td>
        <td id="total_bid_placed">  <?= isset($result["bids_placed"])?$result["bids_placed"]:0; ?> </td>
        <td id="BlockedAmountinBid">  <?= isset($result["total_blocked_amount"])?$result["total_blocked_amount"]:0; ?> </td>
    </tr>
    </tbody>
</table>
