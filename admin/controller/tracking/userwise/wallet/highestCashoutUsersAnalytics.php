<?php
session_start();

//start config
require_once('../../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../../config/db_config.php');
//end config

//start helper
require_once('../../../../helpers/coreFunctions.php');
require_once('../../../../helpers/arrayHelper.php');
require_once('../../../../helpers/stringHelper.php');
require_once('../../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../../core/errorMap.php');
require_once('../../../../model/tracking/userwise/wallet_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = cleanQueryParameter($connDemo, cleanXSS($_POST["userGender"]));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$status = cleanQueryParameter($connDemo, cleanXSS($_POST["status"]));



$result=count_cashout_analytics($fromdate, $todate, $country, $gender, $device,"cashout");
if(noError($result))
{
    $result=$result["errMsg"]["batched_container"]["0"];

}


?>
<div class="col-lg-12">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th colspan="2" class="bg-lightBlue" >Approved Cashout</th>
            <th colspan="2" class="bg-lightBlue" >Pending Cashout</th>
            <th colspan="2" class="bg-lightBlue" >Rejected Cashout</th>
        </tr>
        <tr>
            <th>Txn. No</th>
            <th>Txn. Amt</th>
            <th>Txn. No</th>
            <th>Txn. Amt</th>
            <th>Txn. No</th>
            <th>Txn. Amt</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="approvedCashoutTraCount"> <?= isset($result["total_approved_transactions"])?$result["total_approved_transactions"]:0; ?> </td>
            <td id="approvedCashoutTraAmt"> <?= isset($result["total_approved_amount"])?number_format($result["total_approved_amount"],6):0; ?> </td>
            <td id="pendingCashoutTraCount"> <?= isset($result["total_pending_transactions"])?$result["total_pending_transactions"]:0; ?> </td>
            <td id="pendingCashoutTraAmt"> <?= isset($result["total_pending_amount"])?number_format($result["total_pending_amount"],6):0; ?> </td>
            <td id="rejectedCashoutTraCount"> <?= isset($result["total_rejected_transactions"])?$result["total_rejected_transactions"]:0; ?></td>
            <td id="rejectedCashoutTraAmt"> <?= isset($result["total_rejected_amount"])?number_format($result["total_rejected_amount"],6):0; ?></td>
        </tr>
        </tbody>
    </table>
</div>
