<?php
session_start();

//start config
require_once('../../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../../config/db_config.php');
//end config

//start helper
require_once('../../../../helpers/coreFunctions.php');
require_once('../../../../helpers/arrayHelper.php');
require_once('../../../../helpers/stringHelper.php');
require_once('../../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../../core/errorMap.php');
require_once('../../../../model/tracking/userwise/wallet_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = cleanQueryParameter($connDemo, cleanXSS($_POST["userGender"]));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$status = cleanQueryParameter($connDemo, cleanXSS($_POST["status"]));

/**************************************************************************/
$orderBy = "amount";
$groupBy ="sender";
$sort_type ="";
$transaction_type ="cashout_fees";
$api_name ="cashout_fees";
$flag="2";
/**************************************************************************/

$result=highestWalletAnalytics($fromdate,$todate,$country,$gender,$device,$groupBy,$orderBy,$sort_type,$transaction_type,$api_name,$flag);

if(noError($result))
{
    $result=$result["errMsg"]["batched_container"]["0"];

}
?>

<div class="row">
    <div class="col-lg-12">
        <table class="table  text-center table-responsive">
            <thead>
            <tr>
                <th>Cashout Approved</th>
                <th>Cashout Amount</th>
                <th>Cashout Fees</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td id="approvedCashotCount"> <?= isset($result["total_cashout_approved"])?$result["total_cashout_approved"]:0; ?></td>
                <td id="approvedCashotAmountAnalytics"> <?= isset($result["total_cashout_pending"])?$result["total_cashout_pending"]:0; ?> </td>
                <td id="approvedCashotfeesAnalytics"> <?= isset($result["total_count"])?$result["total_count"]:0; ?> </td>
            </tr>
            </tbody>
        </table>
    </div>


</div>
