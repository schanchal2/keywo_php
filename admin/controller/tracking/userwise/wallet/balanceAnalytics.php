<?php
session_start();

//start config
require_once('../../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../../config/db_config.php');
//end config

//start helper
require_once('../../../../helpers/coreFunctions.php');
require_once('../../../../helpers/arrayHelper.php');
require_once('../../../../helpers/stringHelper.php');
require_once('../../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../../core/errorMap.php');
require_once('../../../../model/tracking/userwise/wallet_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = cleanQueryParameter($connDemo, cleanXSS($_POST["gender"]));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$ACStatus = cleanQueryParameter($connDemo, cleanXSS($_POST["ACStatus"]));
$AClevel= cleanQueryParameter($connDemo, cleanXSS($_POST["AClevel"]));
$ApiName="highestBalAnalytics";
$result=getHighestWalletBalanceUser($fromdate, $todate, $country, $gender, $device,$AClevel, $ACStatus,$ApiName);
$result=$result["errMsg"][0];


?>
<div class="col-lg-4">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>Registered A/C</th>
            <th>Wallet Balance</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td id="registeredAccAnalytics"> <?= isset($result["total_registered_account"])?$result["total_registered_account"]:0; ?> </td>
            <td id="walletBalAnalytics"> <?= isset($result["total_balance_registeredUser"])?$result["total_balance_registeredUser"]:0; ?> </td>
        </tr>
        </tbody>
    </table>
</div>

<div class="col-lg-8">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>Status of Account</th>
            <th>Total Nos.</th>
            <th>Wallet Balance</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td> Active </td>
            <td> <?= isset($result["total_active_account"])?$result["total_active_account"]:0; ?> </td>
            <td> <?= isset($result["total_balance_activeUser"])?$result["total_balance_activeUser"]:0; ?> </td>
        </tr>
        <tr>
            <td> De-Active </td>
            <td> <?= isset($result["total_deactive_account"])?$result["total_deactive_account"]:0; ?> </td>
            <td> <?= isset($result["total_balance_inactiveUser"])?$result["total_balance_inactiveUser"]:0;?> </td>
        </tr>
        <tr>
            <td> Blocked </td>
            <td> <?= isset($result["total_blocked_account"])?$result["total_blocked_account"]:0; ?> </td>
            <td> <?= isset($result["total_balance_blockedUser"])?$result["total_balance_blockedUser"]:0;?> </td>
        </tr>
        </tbody>
    </table>
</div>