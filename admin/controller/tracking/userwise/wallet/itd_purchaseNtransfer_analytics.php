<?php
session_start();

//start config
require_once('../../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../../config/db_config.php');
//end config

//start helper
require_once('../../../../helpers/coreFunctions.php');
require_once('../../../../helpers/arrayHelper.php');
require_once('../../../../helpers/stringHelper.php');
require_once('../../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../../core/errorMap.php');
require_once('../../../../model/tracking/userwise/wallet_analytics.php');

//end other

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$country = cleanQueryParameter($connDemo, cleanXSS($_POST["country"]));
$gender = cleanQueryParameter($connDemo, cleanXSS($_POST["userGender"]));
$device = cleanQueryParameter($connDemo, cleanXSS($_POST["device"]));
$fromdate = cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]));
$todate = cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]));
$status = cleanQueryParameter($connDemo, cleanXSS($_POST["status"]));
$transaction_type = cleanQueryParameter($connDemo, cleanXSS($_POST["transaction_type"]));
$display = cleanQueryParameter($connDemo, cleanXSS($_POST["display"]));


$result = idt_purchaseNtransfer_analytics($fromdate, $todate, $country, $gender, $device, $transaction_type);

if (noError($result)) {
    $result = $result["errMsg"]["batched_container"]["0"];

}
?>

<?php
switch ($display)
{
case "itd_purchase":
?>

<div class="col-lg-12">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th>Total ITD Purchase Txns.</th>
            <th>Total ITD Purchase Amt</th>
        </tr>
        </thead>
        <?php
        break;

        case "transfer":
        ?>
        <div class="col-lg-12">
            <table class="table  text-center table-responsive">
                <thead>
                <tr>
                    <th>Total No. of Sent Txns.</th>
                    <th>Total Sent Amt</th>
                </tr>
                </thead>
                <?php
                break;

                case "received":
                ?>
                <div class="col-lg-12">
                    <table class="table  text-center table-responsive">
                        <thead>
                        <tr>
                            <th>Total No. of Received Txns.</th>
                            <th>Total Received Amt</th>
                        </tr>
                        </thead>

                        <?php
                        break;

                        }
                        ?>

                        <tbody>
                        <tr>
                            <td id="total_deposit_txn">  <?= isset($result["total_transactions"]) ? $result["total_transactions"] : 0; ?> </td>
                            <td id="total_deposit_amt">  <?= isset($result["total_amount"]) ? number_format($result["total_amount"], 6) : 0; ?> </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

