<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/tracklist/tracklist.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();
$categoryType = $_POST["categoryType"];
$categoryID = $_POST["categoryID"];


if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($categoryType) {

        case "singleSelect":
            $resultSubCat = getCategoryByID($categoryID, $connAdmin);
            $resultSubCat = $resultSubCat["errMsg"];


            ?>
            <div class="title clearfix" style="border-bottom: none">
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                    <label class="dropdownOptions">
                        <select id="subtracklistOptions" style="height: 33px;width: 360px;" class="form-control">
                            <option value="" selected disabled>Select Sub-Folder</option>
                            <?php

                            foreach ($resultSubCat as $value) {
                                echo $value["sub_cat_name"];
                                echo "<option data-live-search=\"true\" data-tokens='" . $value['sub_cat_name'] . "' id='category-" . $value['sub_cat_name'] . "' value='" . $value['sub_cat_id'] . "'>" . $value['sub_cat_name'] . "</option>";

                            }
                            ?>
                        </select>
                    </label>
                </form>
            </div>
            <script>
                var select, chosen;
                select = $("#subtracklistOptions");

                select.chosen({ no_results_text: 'Press Enter to add new entry:' });

                chosen = select.data('chosen');
                chosen.dropdown.find('input').on('keyup', function(e)//single
                    //chosen.search_field.on('keyup', function(e) //multiple
                {
                    if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0)
                    {
                        var option = $("<option>").val(this.value).text(this.value);
                        select.prepend(option);
                        select.find(option).prop('selected', true)
                        select.trigger("chosen:updated");
                    }
                });


                $('#subtracklistOptions_chosen').children().bind("cut copy paste",function(e) {
                    e.preventDefault();
                });

                $('#subtracklistOptions_chosen').children().on('drop', function(event) {
                    event.preventDefault();
                });
                $("#subtracklistOptions_chosen").children().children().children().attr("maxlength", "35");
                $("#subtracklistOptions_chosen").children().children().children().attr("autocomplete", "off");

                $( "#subtracklistOptions_chosen" ).keydown(function() {
                    if(event.keyCode == 13){
                        event.preventDefault();
                    }
                });
            </script>
            <?php
            break;
        case "editOne":
            $resultSubCat = getCategoryByID($categoryID, $connAdmin);
            $resultSubCat = $resultSubCat["errMsg"];

            ?>
            <div class="clearfix" style="border-bottom: none">
                <form action="" method="POST" class="" role="form" name="">
                    <!-- <label class="dropdownOptions"> -->
                        <!-- <select id="subtracklistOptionsEditOne" style="height: 33px;width: 360px;" class="form-control"> -->
                            <!-- <option value="" selected disabled>Select Sub-Folder</option> -->
                            <?php

                            // foreach ($resultSubCat as $value) {
                            //     echo $value["sub_cat_name"];
                            //     echo "<option data-live-search=\"true\" data-tokens='" . $value['sub_cat_name'] . "' id='category-" . $value['sub_cat_name'] . "' value='" . $value['sub_cat_id'] . "'>" . $value['sub_cat_name'] . "</option>";

                            // }
                            ?>
                        <!-- </select> -->
                    <!-- </label> -->
                    <!-- 
                     -->
                    <div class="row">
                        <div class="col-xs-10 col-xs-offset-2">
                            <div class="select--1">
                                <select class="select--1 chosen-select" id="subtracklistOptionsEditOne" data-placeholder="Select Sub-Folder...">
                                <!-- <option value="" selected disabled>Select Sub-Folder</option> -->
                                <?php
                                    foreach ($resultSubCat as $value) {
                                        echo $value["sub_cat_name"];
                                        echo "<option data-live-search=\"true\" data-tokens='" . $value['sub_cat_name'] . "' id='category-" . $value['sub_cat_name'] . "' value='" . $value['sub_cat_id'] . "'>" . $value['sub_cat_name'] . "</option>";

                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <script>
                var select, chosen;
                select = $("#subtracklistOptionsEditOne");
                select.chosen({ });

                chosen = select.data('chosen');


            </script>
            <?php
            break;

        case "multiSelect":
            $resultSubCat = getCategoryByID($categoryID, $connAdmin);
            $resultSubCat = $resultSubCat["errMsg"];

            ?>
            <div class="title clearfix" style="border-bottom: none">
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                    <label class="dropdownOptions">
                        <select id="subtracklistOptionsbulk" style="height: 33px;width: 360px;" class="form-control">
                            <option value="" selected disabled>Select Sub-Folder</option>
                            <?php

                            foreach ($resultSubCat as $value) {
                                echo $value["sub_cat_name"];
                                echo "<option data-live-search=\"true\" data-tokens='" . $value['sub_cat_name'] . "' id='category-" . $value['sub_cat_name'] . "' value='" . $value['sub_cat_id'] . "'>" . $value['sub_cat_name'] . "</option>";

                            }
                            ?>
                        </select>
                    </label>
                </form>
            </div>
            <script>
                var select, chosen;
                select = $("#subtracklistOptionsbulk");

                select.chosen({ no_results_text: 'Press Enter to add new entry:' });

                chosen = select.data('chosen');
                chosen.dropdown.find('input').on('keyup', function(e)//single
                    //chosen.search_field.on('keyup', function(e) //multiple
                {
                    if (e.which == 13 && chosen.dropdown.find('li.no-results').length > 0)
                    {
                        var option = $("<option>").val(this.value).text(this.value);
                        select.prepend(option);
                        select.find(option).prop('selected', true)
                        select.trigger("chosen:updated");
                    }
                });


                $('#subtracklistOptionsbulk_chosen').children().bind("cut copy paste",function(e) {
                    e.preventDefault();
                });

                $('#subtracklistOptionsbulk_chosen').children().on('drop', function(event) {
                    event.preventDefault();
                });
                $("#subtracklistOptionsbulk_chosen").children().children().children().attr("maxlength", "35");
                $("#subtracklistOptionsbulk_chosen").children().children().children().attr("autocomplete", "off");

                $( "#subtracklistOptionsbulk_chosen" ).keydown(function() {
                    if(event.keyCode == 13){
                        event.preventDefault();
                    }
                });
            </script>
            <?php
            break;

        case "editBulk":
            $resultSubCat = getCategoryByID($categoryID, $connAdmin);
            $resultSubCat = $resultSubCat["errMsg"];

            ?>
            <div class="title clearfix" style="border-bottom: none">
                <form action="" method="POST" class="form-inline pull-right" role="form" name="">
                    <label class="dropdownOptions">
                        <select id="tracklistOptionsnewbulksub" style="height: 33px;width: 360px;" class="form-control">
                            <option value="" selected disabled>Select Sub-Folder</option>
                            <?php

                            foreach ($resultSubCat as $value) {
                                echo $value["sub_cat_name"];
                                echo "<option data-live-search=\"true\" data-tokens='" . $value['sub_cat_name'] . "' id='category-" . $value['sub_cat_name'] . "' value='" . $value['sub_cat_id'] . "'>" . $value['sub_cat_name'] . "</option>";

                            }
                            ?>
                        </select>
                    </label>
                </form>
            </div>
            <script>
                var select, chosen;
                select = $("#tracklistOptionsnewbulksub");
                select.chosen({ });

                chosen = select.data('chosen');

            </script>
            <?php
            break;

    }

}


?>
