<?php
session_start();
//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
require_once('../../../helpers/cryptoHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other

/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);


//Get page number from Ajax
if(isset($_POST["page"])){
    $page_number = (int) filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if(!is_numeric($page_number)){die('Invalid page number!');} //incase of invalid page number
}else{
    $page_number = 1; //if there's no page number, set it to 1
}
if($page_number==0)
{
    $page_number=1;
}
$item_per_page=$_POST["limit"];
$tracklistType=$_POST["tracklistType"];

$get_total_rows=getMyTracklistDataCount($connAdmin,$tracklistType)["errMsg"];

$total_pages = ceil($get_total_rows/$item_per_page);

//position of records
$page_position = (($page_number-1) * $item_per_page);
$lastpage = ceil($total_pages);


$results=getMyTracklistData($connAdmin, $page_position,$item_per_page,$tracklistType);
$results=$results["errMsg"];
$count=count($results);

if($count==0)
{
    $page_number = $page_number-1; //if there's no page number, set it to 1
    $item_per_page=$_POST["limit"];
    $get_total_rows=getMyTracklistDataCount($connAdmin,$tracklistType)["errMsg"];

    $total_pages = ceil($get_total_rows/$item_per_page);

//position of records
    $page_position = (($page_number-1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results=getMyTracklistData($connAdmin, $page_position,$item_per_page,$tracklistType);
    $results=$results["errMsg"];
}


?>
<div id="userList" class="m-t-15">
<table id="" class="table text-center table-responsive">
    <thead>
    <tr>
        <th>
            <input id="select_all" type="checkbox" name="">
            <label for="select_all" class="fa"></label>
        </th>
        <th> Subject </th>
        <th> Folder </th>
        <th> Sub Folder </th>
        <th> Notes</th>
        <?php
        if ((in_array("write", $myp))) {

        ?>
        <th style="width:10%;text-align: center">
            <!--<span id="actionsId">Action</span><span id="functionsID" class="btn"><i  onclick="moveUpdatetracklistmodalbulkcheck()" class="fa fa-pencil-square-o fa-lg" aria-hidden="true"></i></span>-->

            <span id="actionsId">Action</span><span id="functionsID" class="btn"><i  onclick="deletemyTracklistBulk()" class="fa fa-trash-o" aria-hidden="true"></i></span>
        </th>
        <?php } ?>
    </tr>

    </thead>
    <tbody>
    <?php
    $count = ($page_number - 1) * $item_per_page;

    if($get_total_rows==0)
    {
        ?>
        <tr>
            <?php
            if ((in_array("write", $myp))) {

                ?>
                <td style="text-align: center" colspan="6">No Data Found</td>
                <?php
            }else
            {
                ?>
                <td style="text-align: center" colspan="5">No Data Found</td>
            <?php
            }
            ?>
        </tr>
        <?php
    }else {
        foreach ($results as $value) {
            $count = $count + 1;
            print("<tr>");
            ?>
            <td style="width:10px;text-align: center">
                <input class="user_checkbox" id="user_checkbox<?php echo md5($value["subject_id"]); ?>"
                       type="checkbox" name="user_checkbox[]"
                       value="<?php echo $value["subject_id"] . "," . $value["admin_id"] . ",user_checkbox" . md5($value["subject_id"]).",".$value["subject_owner"].",".$value["subject"].",".$value["cat_id"].",".$value["sub_cat_id"].",".$value["note"]; ?>">
                <label for="user_checkbox<?php echo md5($value["subject_id"]); ?>" class="fa"></label>
            </td>
    <?php


            print("<td style=\"text-align: center\"><a href='".$adminView."manageUsers/manageUser.php?tid=".base64_encode($value["subject_owner"])."' target='_blank'>" . $value['subject'] . "</a></td>");
            print("<td style=\"text-align: center\">" . $value['cat_name'] . "</td>");
            print("<td style=\"text-align: center\">" . $value['sub_cat_name'] . "</td>");
            print("<td style=\" width:30%;word-wrap: break-word;text-align: center\">");
            echo html_entity_decode(wordwrap(($value["note"]),30,"<br>",true))."</td>";

            if ((in_array("write", $myp))) {

                ?>

                <td style="width:20%;text-align: center">
                    <span class="btn" style=""><i
                                onclick="updateMoveTracklist('<?php echo $value['subject_id']; ?>','<?php echo $value['cat_id']; ?>','<?php echo $value['sub_cat_id']; ?>','<?php echo $value['subject_owner']; ?>','<?php echo $value['subject']; ?>','<?php echo $value['note']; ?>')"
                                class="fa fa-pencil" aria-hidden="true" data-toggle="modal"
                                data-target="#moveUpdatetracklistmodal"></i> </span>

                    <span class="btn"><i
                                onclick=deleteTracklistData('<?php echo $value['cat_id']; ?>','<?php echo $value['sub_cat_id']; ?>','<?php echo $value['admin_id']; ?>','<?php echo $value['subject']; ?>','<?php echo $value['subject_owner']; ?>')
                                class="fa fa-trash-o" aria-hidden="true"></i> </span>
                </td>
                <?php
            }
            print("</tr>");
        }
    }
    ?>
    </tbody>
</table>
</div>
<?php
//echo paginate_function($item_per_page, $page_number, $get_total_rows, $total_pages);
getPaginationData($lastpage,$page_number,$tracklistType);

function getPaginationData($lastpage,$pageno,$tracklistType)
{
    echo '<input type="text" id="hiddenpageTracklist" name="hiddenpageTracklist" value="'.$pageno.'" hidden>';
    echo '<input type="text" id="hiddenpageTracklistType" name="hiddenpageTracklistType" value="'.$tracklistType.'" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo'<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum=1;
        print('<li><a href="#"onclick=getMyTracklistPagination("'.$pagenum.'","'.$tracklistType.'")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber=$pageno - 1;
        print('<li><a href="#" onclick=getMyTracklistPagination("'.$pagenumber.'","'.$tracklistType.'")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber=$i;
            print('<li><a href="#" onclick=getMyTracklistPagination("'.$pagenumber.'","'.$tracklistType.'")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber=$pageno + 1;
        print('<li><a href="#" onclick=getMyTracklistPagination("'.$pagenumber.'","'.$tracklistType.'")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getMyTracklistPagination("'.$lastpage.'","'.$tracklistType.'")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

function getMyTracklistDataCount($connAdmin,$tracklistType) //get table rows count
{
    $extraArg = array();
    $returnArr = array();

    $query="SELECT count(*) as count_my_tracklist from (SELECT mt.subject, mt.note, mt.created_on,mt.updated_on,mt.updated_by,sc.sub_cat_name, ct.cat_name,ad.admin_email FROM my_tracklist AS mt INNER JOIN sub_categories AS sc ON sc.sub_cat_id = mt.sub_cat_id INNER JOIN category AS ct ON ct.cat_id = mt.cat_id INNER JOIN admin_user AS ad ON ad.admin_id = mt.admin_id GROUP BY mt.subject_id) as data where admin_email='".$_SESSION['admin_id']."'";

    if($tracklistType!="")
    {
       $query.=" and cat_name='".$tracklistType."'";
    }
    $result = runQuery($query, $connAdmin);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"])) {
            $res[] = $row;
        }
        $returnArr = setErrorStack($returnArr, -1, $res[0]["count_my_tracklist"], $extraArg);
    } else {
        $returnArr = setErrorStack($returnArr, 47, null, $extraArg);
    }
    return $returnArr;
}


function getMyTracklistData($connAdmin, $page_position,$item_per_page,$tracklistType)
{

    $returnArr = array();

    $query="SELECT * from (SELECT mt.subject,mt.subject_id, mt.cat_id,mt.sub_cat_id,mt.note,mt.subject_owner, mt.created_on,mt.updated_on,mt.updated_by,sc.sub_cat_name, ct.cat_name,ad.admin_email,ad.admin_id FROM my_tracklist AS mt INNER JOIN sub_categories AS sc ON sc.sub_cat_id = mt.sub_cat_id INNER JOIN category AS ct ON ct.cat_id = mt.cat_id INNER JOIN admin_user AS ad ON ad.admin_id = mt.admin_id GROUP BY mt.subject_id) as data where admin_email='".$_SESSION['admin_id']."'";

    if($tracklistType!="")
    {
        $query.=" and cat_name='".$tracklistType."'";
    }


    $query.=" ORDER BY created_on DESC LIMIT $page_position, $item_per_page";

    $result = runQuery($query, $connAdmin);

    if (noError($result)) {
        $res = array();

        while ($row = mysqli_fetch_assoc($result["dbResource"]))

            array_push($res, $row);

        $returnArr["errCode"][-1] = -1;

        $returnArr["errMsg"] = $res;

    } else {

        $returnArr["errCode"][5] = 5;

        $returnArr["errMsg"] = $result["errMsg"];

    }

    return $returnArr;

}
?>


<script>
    $(function () {
        var _last_selected = null, checkboxes = $("#userList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {

        var row = $('#userList tbody tr:first td:first').text();
        if(row=="No Data Found")
        {
            $("#functionsID").hide();
            $("#actionsId").show();
        }else {
            $("#functionsID").show();
            $("#actionsId").hide();
        }

        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>






