<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/tracklist/tracklist.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();

$categoryID = cleanQueryParameter($connAdmin, cleanXSS($_GET["id"]));

$categorySub=getCategoryByIDAdminID($categoryID,"1",$connAdmin)["errMsg"];


?>

<center> <table id="subcategorylist" class="table  text-center table-responsive border-lightBlue2" style="margin-top:10px;width:60%;">
        <thead>
        <tr>
          <!--  <th class="bg-lightBlue2 border-lightBlue2 text-darkblue">
                <input id="select_all" type="checkbox" name="">
                <label for="select_all" class="fa"></label>
            </th>-->
            <th class="bg-lightBlue2 border-lightBlue2 text-darkblue"> Sub Folder Name</th>
            <th class="bg-lightBlue2 border-lightBlue2 text-darkblue"> <span id="actionsId">Total Entries</span>&nbsp;&nbsp;&nbsp;&nbsp;
                <span id="functionsID"><i onclick="deleteSubFoldersBulk()" class="" aria-hidden="true"></i></span><!-- removed class fa fa-trash-o fa-lg btn -->
            </th>
        </tr>
        </thead>
        <tbody>


        <?php

        if(count($categorySub)>0) {
            foreach ($categorySub as $sub) {
                ?>
                <tr>
                    <!--<td class="border-lightBlue2" style="width:10px;text-align: center">
                        <input class="user_checkbox" id="user_checkbox<?php echo $sub["sub_cat_id"]; ?>"
                               type="checkbox" name="user_checkbox[]"
                               value="<?php //echo $sub["sub_cat_id"] . "," . $sub["admin_id"] . ",user_checkbox" . $sub["sub_cat_id"]; ?>">
                        <label for="user_checkbox<?php //echo $sub["sub_cat_id"]; ?>" class="fa"></label>
                    </td>-->
                    <td class="border-lightBlue2"> <?php echo $sub["sub_cat_name"]; ?> </td>
                    <td class="border-lightBlue2">
                        <?php
                        echo $result=getTotalTracklistAddedInSubCategory($_SESSION["admin_id"],$sub["sub_cat_name"],$connAdmin)["errMsg"];
                        ?></td>
                </tr>
                <?php
            }
        }else
        {
           ?>
            <tr>
                <td colspan="3" class="border-lightBlue2">Sub Folder Not Added</td>
            </tr>
        <?php
        }
        ?>

        </tbody>
    </table>
</center>
<script>



    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {

        var row = $('#subcategorylist tbody tr:first td:first').text();
        if (row == "Sub Folder Not Added") {
            $("#functionsID").hide();
            //$("#actionsId").show();
        } else {
            $("#functionsID").show();
            // $("#actionsId").hide();
        }

        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });
</script>

