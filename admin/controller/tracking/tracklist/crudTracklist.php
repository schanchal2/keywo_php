<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/tracklist/tracklist.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();



$crudType= cleanQueryParameter($connAdmin, cleanXSS($_POST["crudType"]));

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch($crudType)
    {
        case "delete":

            $categoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["catid"]));
            $SubcategoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["subcatidn"]));
            $adminid= cleanQueryParameter($connAdmin, cleanXSS($_POST["adminid"]));
            $Subject= cleanQueryParameter($connAdmin, cleanXSS($_POST["Subject"]));
            $subOwner= cleanQueryParameter($connAdmin, cleanXSS($_POST["subOwner"]));

            if($adminid == $_SESSION["agent_id"]) {
                $result=deleteTracklist($adminid,$categoryID,$SubcategoryID,$Subject,$subOwner, $connAdmin);
                if(noError($result))
                {
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Tracklist Deleted Success !";
                }else{
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error Deleting Tracklist, Please try again.";
                }
            }else{

                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Error Deleting User in Tracklist ! You Not allowed to delete this entry.";
            }

            break;

        case "editSingle":
            $subjectToEdit= cleanQueryParameter($connAdmin, cleanXSS($_POST["subjectToEdit"]));
            $categoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["catidnew"]));
            $catidold= cleanQueryParameter($connAdmin, cleanXSS($_POST["catidold"]));
            $subcatidnew= cleanQueryParameter($connAdmin, cleanXSS($_POST["subcatidnew"]));
            $notenew= cleanQueryParameter($connAdmin, cleanXSS($_POST["Note"]));
            $subject_owner= cleanQueryParameter($connAdmin, cleanXSS($_POST["subject_owner"]));
            $subjecttomove= cleanQueryParameter($connAdmin, cleanXSS($_POST["subjecttomove"]));
            $subcatidold= cleanQueryParameter($connAdmin, cleanXSS($_POST["subcatidold"]));

            $sub_cat_namenew=getSubCategoryByID($categoryID,$subcatidnew, $connAdmin)["errMsg"][0]["sub_cat_name"];
           // $sub_cat_nameold=getSubCategoryByID($catidold, $connAdmin)["errMsg"][0]["sub_cat_name"];



            //$mydata=checkTracklistEntryExistance($categoryID,$subcatidnew,$subjecttomove,$subject_owner,$connAdmin);
            $mydata=checkTracklistEntryExistanceAll($categoryID,$sub_cat_namenew,$subjecttomove,$subject_owner,$connAdmin);
            $mydata=$mydata["errMsg"];


            if(count($mydata)>=1)
            {

                deleteTracklist($_SESSION["agent_id"],$catidold,$subcatidold,$subjecttomove,$subject_owner, $connAdmin);
                updateNote($categoryID,$subcatidold,$subjecttomove,$subject_owner,$notenew,$connAdmin);
                $cat_nameedited=getMainCategoryByID($categoryID, $connAdmin)["errMsg"][0]["cat_name"];
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Tracklist Edited Success !";
                $returnArr["cat_name"] =$cat_nameedited;
                echo(json_encode($returnArr));
                die;
            }

            $result=updateTracklist($subjectToEdit,$categoryID,$subcatidnew,$notenew,$connAdmin);
            if(noError($result))
            {
                $cat_nameedited=getMainCategoryByID($categoryID, $connAdmin)["errMsg"][0]["cat_name"];
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Tracklist Edited Success !";
                $returnArr["cat_name"] =$cat_nameedited;
            }else{
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = " Error Editing Tracklist, Please try again.";
            }

            break;


        case "editBulkMove":
            $mydata= $_POST["subjectToEdit"];


            $categoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["categoryID"]));
            $SubcategoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["SubcategoryID"]));
            $Note= cleanQueryParameter($connAdmin, cleanXSS($_POST["Note"]));

            foreach($mydata as $mydatanew) {

                $mydatanewExplode=explode(":",$mydatanew);
                $mydatanew=$mydatanewExplode[0];
                $subject_owner=$mydatanewExplode[1];
                $subjecttomove=$mydatanewExplode[2];
                $cat_idold=$mydatanewExplode[3];
                $subcatidold=$mydatanewExplode[4];
                $NoteOLD=$mydatanewExplode[5];

                $sub_cat_namenew=getSubCategoryByID($categoryID,$SubcategoryID, $connAdmin)["errMsg"][0]["sub_cat_name"];
                //$sub_cat_nameold=getSubCategoryByID($cat_idold, $connAdmin)["errMsg"][0]["sub_cat_name"];

                //$mydata=checkTracklistEntryExistance($categoryID,$subcatidnew,$subjecttomove,$subject_owner,$connAdmin);
                $mydata=checkTracklistEntryExistanceAll($categoryID,$sub_cat_namenew,$subjecttomove,$subject_owner,$connAdmin);
                $mydata=$mydata["errMsg"];





                if(count($mydata)>=1)
                {

                    deleteTracklist($_SESSION["agent_id"],$cat_idold,$subcatidold,$subjecttomove,$subject_owner, $connAdmin);
                   // updateNote($categoryID,$subcatidold,$subjecttomove,$subject_owner,$mydatanew, $connAdmin);

                    $cat_nameedited=getMainCategoryByID($categoryID, $connAdmin)["errMsg"][0]["cat_name"];
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Tracklist Edited Success !";
                    $returnArr["cat_name"] =$cat_nameedited;
                    continue;
                }
                $result=updateTracklist($mydatanew,$categoryID,$SubcategoryID,$NoteOLD,$connAdmin);
                if(noError($result))
                {
                    $cat_nameedited=getMainCategoryByID($categoryID, $connAdmin)["errMsg"][0]["cat_name"];
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "Tracklist Edited Success !";
                    $returnArr["cat_name"] =$cat_nameedited;
                }else{
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error Editing Tracklist, Please try again.";
                }



            }



            break;


        case "bulkdelete":
            $mydata= $_POST["subjectids"];


            foreach($mydata as $mydatanew) {
                $newdata = explode(":", $mydatanew);
                $Subjectid = $newdata[0];
                $adminid = $newdata[1];

                if ($adminid == $_SESSION["agent_id"]) {
                    $result = deleteTracklistBulk($adminid, $Subjectid, $connAdmin);

                    if (noError($result)) {
                        $returnArr["errCode"] = "-1";
                        $returnArr["errMsg"] = "Tracklist Deleted Success !";
                    } else {
                        $returnArr["errCode"] = "65";
                        $returnArr["errMsg"] = "Error Deleting Tracklist, Please try again.";
                    }
                } else {
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error Deleting User in Tracklist ! You Not allowed to delete this entry.";
                }
            }



            break;

        case "bulkdeleteSubCategory":
            $mydata= $_POST["subCategoryandIDAgentID"];


            foreach($mydata as $mydatanew) {
                $newdata = explode(":", $mydatanew);

                $subcatID = $newdata[0];
                $adminid = $newdata[1];

                if ($adminid == $_SESSION["agent_id"]) {
                    $result=deleteTracklistbySubcategory($adminid,$subcatID,$connAdmin);
                    if (noError($result)) {
                        $resultnew = deleteSubCategoryBulk($adminid, $subcatID, $connAdmin);
                        $returnArr["errCode"] = "-1";
                        $returnArr["errMsg"] = "Subcategory Deleted Success !";
                    } else {
                        $returnArr["errCode"] = "65";
                        $returnArr["errMsg"] = "Error Deleting Subcategoryc, Please try again.";
                    }
                } else {
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error Deleting Subcategory ! You Not allowed to delete this entry.";
                }
            }



            break;


        case "bulkdeleteCategory":
            $mydata= $_POST["catIDAdminEmail"];


            foreach($mydata as $mydatanew) {
                $newdata = explode(":", $mydatanew);

                $catID = $newdata[0];
                $adminEmail = $newdata[1];

                if ($adminEmail == $_SESSION["admin_id"]) {
                    $result=deleteTracklistbyCatID($catID,$connAdmin);
                    if (noError($result)) {
                        $result=deleteSubCategoryByCatID($catID, $connAdmin);
                        if(noError($result))
                        {
                            $resultnew = deleteCategoryByCatIDUpdatedBY($catID,$adminEmail, $connAdmin);
                            if(noError($resultnew))
                            {
                                $returnArr["errCode"] = "-1";
                                $returnArr["errMsg"] = "Category Deleted Successfully !";
                            }else{
                                $returnArr["errCode"] = "65";
                                $returnArr["errMsg"] = "Error Deleting Category, Please try again.";
                            }
                        }else{
                            $returnArr["errCode"] = "65";
                            $returnArr["errMsg"] = "Error Deleting Subcategory, Please try again.";
                        }
                    } else {
                        $returnArr["errCode"] = "65";
                        $returnArr["errMsg"] = "Error Deleting Tracklist, Please try again.";
                    }
                } else {
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error Deleting Subcategory ! You Not allowed to delete this entry.";
                }
            }



            break;

    }



    echo(json_encode($returnArr));


} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}


?>





