<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/tracklist/tracklist.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();

$categoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["categoryID"]));
$SubcategoryID= cleanQueryParameter($connAdmin, cleanXSS($_POST["SubcategoryID"]));
$subListData= cleanQueryParameter($connAdmin, cleanXSS($_POST["subListData"]));

$SubjectOwner= cleanQueryParameter($connAdmin, cleanXSS($_POST["SubjectOwner"]));
$Subject= cleanQueryParameter($connAdmin, cleanXSS($_POST["Subject"]));

$notes= cleanQueryParameter($connAdmin, cleanXSS($_POST["notes"]));
$processType= cleanQueryParameter($connAdmin, cleanXSS($_POST["processType"]));




if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {




    switch($processType)
    {
        case "single":

            $totalTracklist=getTotalTracklist($_SESSION["agent_id"],$connAdmin)["errMsg"];
            if($totalTracklist<=99)
            {
            $mydata=checkTracklistEntryExistance($categoryID,$SubcategoryID,$Subject,$SubjectOwner,$connAdmin);
            $mydata=$mydata["errMsg"];
     

            if(count($mydata)>=1)
            {
                $admin_id=$mydata[0]["admin_id"];
                $agent_id=$_SESSION["agent_id"];
                if($admin_id == $agent_id)
                {
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "User Already Added in your Tracklist!";
                }else
                {
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "User Already Added by other agent in Tracklist!";
                }

                echo(json_encode($returnArr));
                exit;
            }

            $subjectType=getMainCategoryByID($categoryID, $connAdmin);
            $subjectType=$subjectType["errMsg"][0]["cat_name"];

            $agentID=$_SESSION["agent_id"];
            $updatedBy=$_SESSION["admin_id"];
            if($subListData==$SubcategoryID)
            {
                $result=addSubcategory($agentID,$categoryID, $subListData,$updatedBy,$connAdmin);
                if(noError($result))
                {
                    $resultdata=getSubCategoryByName($SubcategoryID, $connAdmin);
                    $SubcategoryID=$resultdata["errMsg"][0]["sub_cat_id"];
                    $result=addTracklist($categoryID, $SubcategoryID,$agentID,$Subject,$SubjectOwner,$notes,$subjectType,$updatedBy,$connAdmin);

                    if(noError($result))
                    {
                        $returnArr["errCode"] = "-1";
                        $returnArr["errMsg"] = "User Added to Tracklist";
                    }else{
                        $returnArr["errCode"] = "65";
                        $returnArr["errMsg"] = "Error adding User in Tracklist,Please try again";
                    }
                }

            }else{
                $result=addTracklist($categoryID, $SubcategoryID,$agentID,$Subject,$SubjectOwner,$notes,$subjectType,$updatedBy,$connAdmin);

                if(noError($result))
                {
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "User Added to Tracklist";
                }else{
                    $returnArr["errCode"] = "65";
                    $returnArr["errMsg"] = "Error adding User in Tracklist,Please try again";
                }
            }
            }else{
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "You Have added total 100 tracklist.Please remove old entries then add again.";
            }
            echo(json_encode($returnArr));
            break;

        case "bulk":

            $subjectNowner=$_POST["subjectNowner"];


            $subjectType=getMainCategoryByID($categoryID, $connAdmin);
            $subjectType=$subjectType["errMsg"][0]["cat_name"];

            $totalTracklistold=getTotalTracklist($_SESSION["agent_id"],$connAdmin)["errMsg"];
             $totalTracklist=$totalTracklistold+(count($subjectNowner));
            $totalTracklisttoremove=$totalTracklist-100;


            if($totalTracklist>=100) {
                $returnArr["errCode"] = "65";

                if($totalTracklistold==100)
                {
                    $returnArr["errMsg"] = "You Have added total 100 tracklist.Please remove old entries then add again.";

                }else{

                    //$totalTracklisttoremove=$totalTracklisttoremove-25;
                    $returnArr["errMsg"] = "Please remove old entries then add again OR Remove {$totalTracklisttoremove} entries from list to proceed";

                }

                echo(json_encode($returnArr));
                exit;
            }
            foreach($subjectNowner as $value)
            {
                $mixdata=explode(":",$value);
                $SubjectOwner=$mixdata[0];
                $Subject=$mixdata[1];

                $agentID=$_SESSION["agent_id"];
                $updatedBy=$_SESSION["admin_id"];

                $mydata=checkTracklistEntryExistance($categoryID,$SubcategoryID,$Subject,$SubjectOwner,$connAdmin);
                $mydata=$mydata["errMsg"];

                if(count($mydata)>=1)
                {
                    $returnArr["errCode"] = "-1";
                    $returnArr["errMsg"] = "User Added to Tracklist";
                    continue;
                }


                if($subListData==$SubcategoryID)
                {
                    $result=addSubcategory($agentID,$categoryID, $subListData,$updatedBy,$connAdmin);
                    if(noError($result))
                    {
                        $resultdata=getSubCategoryByName($SubcategoryID, $connAdmin);
                        $SubcategoryID=$resultdata["errMsg"][0]["sub_cat_id"];
                        $result=addTracklist($categoryID, $SubcategoryID,$agentID,$Subject,$SubjectOwner,$notes,$subjectType,$updatedBy,$connAdmin);

                        if(noError($result))
                        {
                            $returnArr["errCode"] = "-1";
                            $returnArr["errMsg"] = "User Added to Tracklist";
                        }else{
                            $returnArr["errCode"] = "65";
                            $returnArr["errMsg"] = "Error adding User in Tracklist";
                        }
                    }

                }else{
                    $result=addTracklist($categoryID, $SubcategoryID,$agentID,$Subject,$SubjectOwner,$notes,$subjectType,$updatedBy,$connAdmin);

                    if(noError($result))
                    {
                        $returnArr["errCode"] = "-1";
                        $returnArr["errMsg"] = "User Added to Tracklist";
                    }else{
                        $returnArr["errCode"] = "65";
                        $returnArr["errMsg"] = "Error adding User in Tracklist,Please try again";
                    }
                }


            }
            echo(json_encode($returnArr));
            break;
    }

} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}


?>
