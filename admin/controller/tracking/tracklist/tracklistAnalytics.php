<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/tracking/tracklist/tracklist.php');
//end other


$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();

$analyticsType= cleanQueryParameter($connAdmin, cleanXSS($_POST["analyticsType"]));

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch($analyticsType){

        case "totalTracklist":
            $totalTracklist=getTotalTracklist($_SESSION["agent_id"],$connAdmin);
            if (noError($totalTracklist)) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = $totalTracklist["errMsg"];
            } else {
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Error getting Tracklist analytics";
            }
            break;

        case "totalTracklistbyid":
            $catid= cleanQueryParameter($connAdmin, cleanXSS($_POST["cat_id"]));
            $totalTracklist=getTotalTracklistByCatIDAdminID($_SESSION["agent_id"],$catid,$connAdmin);
            if (noError($totalTracklist)) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = $totalTracklist["errMsg"];
            } else {
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Error getting Tracklist analytics";
            }
            break;

        case "totalSubFoldersAdded":

            $totalSubFolders=getTotalSubFolders("1",$connAdmin);
            if (noError($totalSubFolders)) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = $totalSubFolders["errMsg"];
            } else {
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Error getting Tracklist analytics";
            }
            break;

        case "addNewMainCategory":

            $adminEmail= cleanQueryParameter($connAdmin, cleanXSS($_POST["adminEmail"]));
            $catName= strtolower(cleanQueryParameter($connAdmin, cleanXSS($_POST["catName"])));

            $catNamenew=getMainCategoryByName($catName, $connAdmin);
            $catNamenew=strtolower($catNamenew["errMsg"][0]["cat_name"]);
            if($catName==$catNamenew)
            {
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Category Name Allready Present.";
                echo(json_encode($returnArr));
                exit;
            }
            $result=addCategory($catName,$adminEmail,$connAdmin);
            if (noError($result)) {
                $newresult=getCategoryIDbyName($catName, $connAdmin)["errMsg"][0]["cat_id"];
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] = "Category Added Successfully";
                $returnArr["catID"] =$newresult;
            } else {
                $returnArr["errCode"] = "65";
                $returnArr["errMsg"] = "Error getting Tracklist analytics";
            }
            break;


    }




    echo(json_encode($returnArr));


} else {
    $returnArr["errCode"] = "101";
    $returnArr["errMsg"] = "You are Not Authorised User!!! Bad Luck !!!";

    echo(json_encode($returnArr));
}


?>
