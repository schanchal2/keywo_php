
<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/tracking/postwise/postwise_analytics.php');
//end other




$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);


$fromdate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["fromdate"]))));
$todate = date("m/d/Y",strtotime(cleanQueryParameter($connDemo, cleanXSS($_POST["todate"]))));
$limitData = (int)cleanQueryParameter($connDemo, cleanXSS($_POST["limit"]));
$postType= cleanQueryParameter($connDemo, cleanXSS($_POST["postType"]));
if($postType=="")
{
    $postType="all";
}
$reportCat = cleanQueryParameter($connDemo, cleanXSS($_POST["postBlockReasonType"]));

$listtype = cleanQueryParameter($connDemo, cleanXSS($_POST["listtype"]));


if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    if ($listtype != "first") {

        $pageno = cleanQueryParameter($connDemo, cleanXSS($_POST['pageno']));

        if ($pageno == "") {
            $pageno = 1;
        }


        $resultsPerPage = $limitData;
        $start = ($pageno - 1) * $resultsPerPage;

        $userListCount = getReportedPost($fromdate,$todate,$reportCat,$postType,"1","0","0")["errMsg"];

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limitData;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limitData;
        }

        if (!isset($pageno) || ($pageno == 1)) {
            $start = '0';
        } else {
            $start = $start;
        }

        $result = getReportedPost($fromdate,$todate,$reportCat,$postType,"2",$skip,$resultsPerPage);
        $result = $result["errMsg"];

        if ($result["result"]) {
            $error = trim(strtolower($result["result"]));
        } else if ($result["errMsg"]) {
            $error = trim(strtolower($result["errMsg"]));
        }
        if ($result["code"] == "ECONNREFUSED") {
            $error = "ECONNREFUSED";
        }
        if ($error == "no result" || $error == "error in curl execution" || $error == "ECONNREFUSED") {
            ?>
            <div id="newRegisteredUsers">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <!--<th>
                            <input id="select_all" type="checkbox" name="">
                            <label for="select_all" class="fa"></label>
                        </th>-->
                        <th> Post Author </th>
                        <th> Reported By </th>
                        <th> Timestamp </th>
                        <th> Reason </th>
                        <th> Author Country </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="7">
                            <?php
                            if ($error == "error in curl execution") {
                                echo "Error getting data from server";
                            } elseif ($error == "ECONNREFUSED") {
                                echo "Sever Connection Refused!";
                            } else {
                                echo "No Result Found";
                            }
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            exit;
        }
    }else{
        $order = -1;
        $skip = 0;


        $userListCount = getReportedPost($fromdate,$todate,$reportCat,$postType,"1","0","0")["errMsg"];

        $skip=0;
        $limit=$limitData;
        $result = getReportedPost($fromdate,$todate,$reportCat,$postType,"2",$skip,$limit);
        $result = $result["errMsg"];

        if ($result["result"]) {
            $error = trim(strtolower($result["result"]));
        }
        if ($result["errMsg"]) {
            $error = trim(strtolower($result));
        }

        if ($result["code"] == "ECONNREFUSED") {
            $error = "ECONNREFUSED";
        }
        if ($error == "no result" || $error == "error in curl execution" || $error == "ECONNREFUSED") {
            ?>
            <div id="newRegisteredUsers">
                <table class="table text-center table-responsive">
                    <thead>
                    <tr>
                        <!--<th>
                            <input id="select_all" type="checkbox" name="">
                            <label for="select_all" class="fa"></label>
                        </th>-->
                        <th> Post Author </th>
                        <th> Reported By </th>
                        <th> Timestamp </th>
                        <th> Reason </th>
                        <th> Author Country </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="7">
                            <?php
                            if ($error == "error in curl execution") {
                                echo "Error getting data from server";
                            } elseif ($error == "ECONNREFUSED") {
                                echo "Sever Connection Refused!";
                            } else {
                                echo "No Result Found";
                            }
                            ?>

                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <?php
            exit;
        }
        $resultsPerPage = $limit;

        $row_countw = $userListCount;
        $lastpage = $row_countw / $resultsPerPage;
        $lastpage = ceil($lastpage);

        if ($pageno <= 1) {
            $pageno = 1;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        } else if ($pageno > 1) {//$pageno>$lastpage
            //$pageno=$lastpage;
            $pn = $pageno - 1;
            $skip = $pn * $limit;
        }
    }


    ?>

    <div id="newRegisteredUsers">
        <table class="table  text-center table-responsive">
            <thead>
            <tr>
               <!-- <th>
                    <input id="select_all" type="checkbox" name="">
                    <label for="select_all" class="fa"></label>
                </th>-->
                <th> Post Author </th>
                <th> Reported By </th>
                <th> Timestamp </th>
                <th> Reason </th>
                <th> Author Country </th>
                <!--<th> Tracklist &nbsp;<a href="javascript:;" onclick="addTotracklistmodalBulkCheck()"><i class="fa fa-plus-square fa-lg" data-value=" "> </i></a> </th>-->
            </tr>
            </thead>
            <tbody>
            <?php
            foreach($result as $datavalue) {
                $email=$datavalue["post_id"]["user_ref"]["email"];
                $country=$datavalue["post_id"]["meta"]["country"];
                $postID=$datavalue["post_id"]["_id"]
                ?>
                <tr>
                    <!--<td>
                        <input class="user_checkbox" id="user_checkbox<?php echo md5($email); ?>"
                               type="checkbox" name="user_checkbox[]"
                               value="<?php //echo $email/*subject owner*/ . "," . $postID /*subject*/. ",user_checkbox" . md5($email); ?>">
                        <label for="user_checkbox<?php //echo md5($email); ?>" class="fa"></label>
                    </td>-->
                    <td> <?php echo $email; ?></td>
                    <td> <?php echo $datavalue["reporter_ref"]["email"]; ?></td>
                    <td>
                        <?php
                        $mydate= uDateTime("m-d-Y h:i:s",$datavalue["created_at"]);
                        $datetime=explode(" ",$mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>
                    </td>
                    <td> <?php echo $datavalue["report_text"]; ?></td>
                    <td> <?php echo $country; ?></td>
                   <!-- <td><a href="javascript:;" data-toggle="modal" data-target="#addTotracklistmodal"><i class="fa fa-plus-square " onclick=addElementToModal('<?php //echo $email; ?>','<?php //echo $email; ?>') data-value="<?php //echo $email; ?>"> </i></a></td>-->
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <!-- <span class="pull-left recordCountsShow"> Show <input type="text" name=""></span> -->
        <span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResults">
                    <option value="10" <?php if($limitData==10){ echo "selected"; } ?>> 10 </option>
                    <option value="20" <?php if($limitData==20){ echo "selected"; } ?>> 20 </option>
                    <option value="30" <?php if($limitData==30){ echo "selected"; } ?>> 30 </option>
                    <option value="40" <?php if($limitData==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limitData==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
        <input type="text" id="hiddenpage" name="hiddenpage" value="<?php echo $pageno; ?>" hidden>
        <div style="" class="box-footer clearfix">
            <ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">

                <?php
                if ($pageno > 1) {

                    $pagenum = 1;
                    print('<li><a href="#"onclick=getUserListPaginationByDate("' . $pagenum . '","' . $fromdate . '","' . $todate . '","' . $postType . '","' . $reportCat . '","' . $limitData . '")>&laquo;</a></li>');
                }

                if ($pageno > 1) {
                    $pagenumber = $pageno - 1;
                    print('<li><a href="#" onclick=getUserListPaginationByDate("' . $pagenumber . '","' . $fromdate . '","' . $todate . '","' . $postType . '","' . $reportCat . '","' . $limitData . '")>Previous</a></li>');
                }

                if ($pageno == 1) {
                    $startLoop = 1;
                    $endLoop = ($lastpage < 5) ? $lastpage : 5;
                } else if ($pageno == $lastpage) {
                    $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                    $endLoop = $lastpage;
                } else {
                    $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                    $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
                }

                for ($i = $startLoop; $i <= $endLoop; $i++) {
                    if ($i == $pageno) {
                        print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
                    } else {
                        $pagenumber = $i;
                        print('<li><a href="#" onclick=getUserListPaginationByDate("' . $pagenumber . '","' . $fromdate . '","' . $todate . '","' . $postType . '","' . $reportCat . '","' . $limitData . '")>' . $i . '</a></li>');
                    }
                }
                if ($pageno < $lastpage) {
                    $pagenumber = $pageno + 1;
                    print('<li><a href="#" onclick=getUserListPaginationByDate("' . $pagenumber . '","' . $fromdate . '","' . $todate . '","' . $postType . '","' . $reportCat . '","' . $limitData . '")>Next</a></li>');

                }

                if ($pageno != $lastpage) {
                    print('<li><a href="#" onclick=getUserListPaginationByDate("' . $lastpage . '","' . $fromdate . '","' . $todate . '","' . $postType . '","' . $reportCat . '","' . $limitData . '")>&raquo;</a></li>');
                }
                ?>

            </ul>
        </div>
    </div>

    <?php
} else {

    $url=$adminView."acl/index.php";
    echo '<script>window.location.replace("'.$url.'");</script>';

}

?>

<script>

    $("#LimitedResults").change(function(){
        getReportedPostByDate();
    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {

        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>











