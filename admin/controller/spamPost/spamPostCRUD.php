<!--==========================================================
=            controller/spamPost/spamPostCRUD.php            =
===========================================================-->
<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/spamPost/spam_post_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$category = cleanQueryParameter($connSearch, cleanXSS($_POST["category_name"]));
$limit = cleanQueryParameter($connSearch, cleanXSS($_POST["category_limit"]));
$action = cleanQueryParameter($connSearch, cleanXSS($_POST["action"]));

/*********************** get user Permissions ********************/
$docrootpath=$docRootAdmin;
require_once "{$docRootAdmin}model/acl/checkAccess.php";
$myp=mypermissions($largest);
//printArr($myp);
/*********************** get user Permissions ********************/

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    switch ($action) {
        case 'add':

            if($category=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Category Name Required!";
                echo(json_encode($returnArr));
                exit;
            }

            if($limit=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Limit is Required!";
                echo(json_encode($returnArr));
                exit;
            }
            $result = addNewCategoryforSpamPost($category, $limit, "update");
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Category Added Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Adding Category !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'update':

            $result = addNewCategoryforSpamPost($category, $limit, "update");

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Category Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Category !!";
                echo(json_encode($returnArr));
            }
            break;
        case 'remove':

            $result = addNewCategoryforSpamPost($category, $limit, "remove");

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Category Deleted Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Deleting Category !!";
                echo(json_encode($returnArr));
            }
            break;
        case 'display':
            $result = getCategoryandLimit();
            $allCategories = $result["errMsg"];
            ?>
            <div id="userList" class="m-t-10">
                <table class="table text-center table-responsive" id="spamPostTable">
                    <thead>
                    <tr>
                        <th class="no-sort dtTableHeadPadding"> Sr.No.</th>
                        <th class="no-sort dtTableHeadPadding"> Category</th>
                        <th class="no-sort dtTableHeadPadding"> Limit</th>
                        <?php
                        if((in_array("write", $myp))) {
                        ?>
                        <th class="no-sort dtTableHeadPadding"> Action</th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    $count = 0;

                    if(($allCategories!="Error in curl execution")) {
                        if (($allCategories != "Wrong Category")) {
                            foreach ($allCategories as $key => $categoryData) {

                                $count = $count + 1;
                                ?>
                                <tr>
                                    <td> <?php echo $count; ?> </td>
                                    <td>  <?php echo $key; ?>  </td>
                                    <td>
                                        <span id="<?php echo $categoryData . "DataToUpdate"; ?>"><?php echo $categoryData; ?></span>
                                    </td>
                                <?php
                                if((in_array("write", $myp))) {
                                    ?>
                                    <td>
                                        <button type="button" class="btn btn-default"
                                                onclick="updateCategoryLimit('Edit Category Report Limit','<?php echo $key; ?>','<?php echo $categoryData . "DataToUpdate"; ?>','Enter Category Report Limit')">
                                            <i class="fa fa-pencil"></i></button>

                                        <button type="button" class="btn btn-default"
                                                onclick="deleteCategory('<?php echo $key; ?>','<?php echo $categoryData; ?>')">
                                            <i class="fa fa-trash-o"></i></button>
                                    </td>
                                    <?php } ?>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>

                    </tbody>
                </table>
            </div>
            <script>
                $('#spamPostTable').DataTable({
                    "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
                    "ordering": false,
                    columnDefs: [{
                        orderable: false,
                        targets: "no-sort"
                    }]

                });
            </script>
            <?php
            break;
    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Updating Category !!";
    echo(json_encode($returnArr));
} 
?>
<!--====  End of controller/spamPost/spamPostCRUD.php  ====-->
