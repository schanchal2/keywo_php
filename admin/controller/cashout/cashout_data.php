<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/cashout/cashout_model.php');

//end other


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit             = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$tablename         = cleanQueryParameter($connKeywords, cleanXSS($_POST["tablename"]));
$userEmailId       = cleanQueryParameter($connKeywords, cleanXSS($_POST["userEmailId"]));
$userWalletAddress = cleanQueryParameter($connKeywords, cleanXSS($_POST["userWalletAddress"]));
$requestStatus     = cleanQueryParameter($connKeywords, cleanXSS($_POST["cashoutReqStatus"]));
$countryToSearch   = cleanQueryParameter($connKeywords, cleanXSS($_POST["countryToSearch"]));
$payment_mode      = cleanQueryParameter($connKeywords, cleanXSS($_POST["payment_mode"]));
$adminEmail        = $_SESSION["admin_id"];

if ($requestStatus == "") {
    $actionEmail       = $_SESSION['admin_id'];
} else {
    $actionEmail       = "";
}

// $basePage=basename($_SERVER["HTTP_REFERER"]);
// if($basePage!="couamAgent.php")
// {
//     $actionEmail       = $_SESSION['admin_id'];
// }else{
//     $actionEmail="";
// }


$agentEmail        = '';

//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}

$item_per_page = $limit;
$get_total_rows = getAllCashoutCount($connKeywords, $tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode,$adminEmail ,$actionEmail, $agentEmail)["errMsg"];
$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllCashoutData($page_position, $limit, $connKeywords, $tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode,$adminEmail ,$actionEmail, $agentEmail);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllCashoutCount($connKeywords, $tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode,$adminEmail ,$actionEmail, $agentEmail)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);


    $results = getAllCashoutData($page_position, $limit, $connKeywords, $tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode,$adminEmail ,$actionEmail, $agentEmail);
    $results = $results["errMsg"];
}

//printArr($results);
?>
<div id="userList" class="m-t-15">
    <table class="table  text-center table-responsive reloadInternal">
        <thead>
        <tr>
            <th rowspan="2">
                <input id="select_all" type="checkbox" name=""><label for="select_all" class="fa"></label>
            </th>
            <th rowspan="2">Timestamp</th>
            <th rowspan="2">User Email</th>
            <th rowspan="2">Cashout Amount </th>
            <th rowspan="2">Cashout Fees </th>
            <th rowspan="2">Country</th>
            <th rowspan="2">Payment Mode</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="8">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $value) {
                // printArr($value);
                $country=json_decode($value['meta_details'],true)["country"];
                ?>
                <tr>
                    <td>
                        <input class="user_checkbox" id="user_checkbox<?php echo $value["id"]; ?>"
                               type="checkbox" name="user_checkbox[]"
                               value="<?php echo $value["id"]; ?>" requestDate = "<?php echo $value['request_date']; ?>">
                        <label for="user_checkbox<?php echo $value["id"]; ?>" class="fa"></label>
                    </td>
                    <td>
                        <?php
                        $datetime=explode(" ",uDateTime("d-m-Y h:i:s",$value["request_date"]));
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo explode(".",$datetime[1])[0]; ?> </time>
                    </td>
                    <td><?php echo $value['user_email'] ?></td>
                    <td><?php echo $value['payment_amount'] ?></td>
                    <td><?php echo $value['cashout_fees'] ?></td>
                    <td><?php echo $country; ?></td>
                    <td><?php echo $value['payment_mode'] ?></td>

                </tr>
                <?php
            }
        }?>
        </tbody>
    </table>
</div>
<?php


getPaginationData($lastpage, $page_number, $limit,$tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode);
function getPaginationData($lastpage, $pageno, $limit,$tablename,$userEmailId,$userWalletAddress,$requestStatus,$countryToSearch,$payment_mode)
{

    ?>
    <span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResult">
                    <option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
                    <option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
                    <option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
                    <option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
                    <option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
    <?php
    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getCashoutRequestsPages("' . $pagenum . '","' . $limit . '","' . $tablename . '","' . $userEmailId . '","' . $userWalletAddress . '","' . $requestStatus . '","' . $countryToSearch . '","' . $payment_mode . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getCashoutRequestsPages("' . $pagenumber . '","' . $limit . '","' . $tablename . '","' . $userEmailId . '","' . $userWalletAddress . '","' . $requestStatus . '","' . $countryToSearch . '","' . $payment_mode . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getCashoutRequestsPages("' . $pagenumber . '","' . $limit . '","' . $tablename . '","' . $userEmailId . '","' . $userWalletAddress . '","' . $requestStatus . '","' . $countryToSearch . '","' . $payment_mode . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getCashoutRequestsPages("' . $pagenumber . '","' . $limit . '","' . $tablename . '","' . $userEmailId . '","' . $userWalletAddress . '","' . $requestStatus . '","' . $countryToSearch . '","' . $payment_mode . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getCashoutRequestsPages("' . $lastpage . '","' . $limit . '","' . $tablename . '","' . $userEmailId . '","' . $userWalletAddress . '","' . $requestStatus . '","' . $countryToSearch . '","' . $payment_mode . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

?>


<script>
    $("#LimitedResult").change(function(){
        getCashoutRequests();
    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    });

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


</script>
