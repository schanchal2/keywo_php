<?php
	session_start();

	//start config
	require_once('../../config/config.php');
	require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
	require_once('../../config/db_config.php');
	//end config

	//start helper
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/arrayHelper.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/date_helpers.php');
	//end helper

	//other
	require_once('../../core/errorMap.php');
	require_once('../../model/cashout/cashout_model.php');
	require_once('../../model/settings/fees_model.php');
	// require_once('../../model/wallet/walletCashoutModel.php');

	//end other

	$connKeywords = createDBConnection("dbkeywords");
	noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

	$userAction     = cleanQueryParameter($connKeywords, cleanXSS($_POST['action']));
	$Usertype       = cleanQueryParameter($connKeywords, cleanXSS($_POST['adminUserType']));
	$actionMessage  = cleanQueryParameter($connKeywords, cleanXSS($_POST['comment']));
	$requestIds     = json_decode($_POST['cashoutIds']);
	$returnArr      = array();
	

	if ($Usertype == "{$coagent}" || $Usertype == "{$cosm}") {	
		if ($userAction == 'Hold') {

			if ($Usertype == "{$coagent}") {
				$updatingStatus = "hold_by_uam";
			} else if ($Usertype == "{$cosm}") {
				$updatingStatus = "hold_by_sm";
			}

		} else if ($userAction == 'Reject') {

			if ($Usertype == "{$coagent}") {
			$updatingStatus = "rejected_by_uam";
			} else if ($Usertype == "{$cosm}") {
				$updatingStatus = "rejected_by_sm";
			}

		} else if ($userAction == 'Approve') {

			if ($Usertype == "{$coagent}") {
			$updatingStatus = "pending_for_hod_approval";
			} else if($Usertype == "{$cosm}") {
				$updatingStatus = "approved_tbc";
			}
			$actionMessage = '';

		}	
		
		foreach ($requestIds as $key => $value) {
			$givenData = strtotime($value->requestTime);
			$tableName = "queue_processor_".date("m",$givenData)."_".$year=date("Y",$givenData);
			$RequestedId = $value->id;
			$actionPerformedBy = $_SESSION['admin_id'];
			$requestStatusCheck = CheckCashoutRequestStatus($RequestedId, $tableName, $connKeywords);
			if (noError($requestStatusCheck)) {
				$setRequestUpdate = updateCashoutRequest($RequestedId, $tableName, $updatingStatus, $actionPerformedBy, $actionMessage, $connKeywords);				
				if (noError($setRequestUpdate)) {
                    if ($userAction=="Reject") {
                        $resultReject = rejectManualCashoutRequest($RequestedId,$tableName,$connKeywords);
                        if (noError($resultReject)) {
                            $returnArr['errCode'] = -1;
                            $returnArr['errMsg'] = "Updated Successfully";
                        } else {
                            $returnArr['errCode'] = $resultReject['errCode'];
                            $returnArr['errMsg'] = $resultReject['errMsg'];
                        }
                    } else {
                        $returnArr['errCode'] = -1;
                        $returnArr['errMsg'] = "Updated Successfully";
                    }	              	
				} else {
					$returnArr['errCode'] = $setRequestUpdate['errCode'];
					$returnArr['errMsg'] = $setRequestUpdate['errMsg'];
				}		
			} else if ($requestStatusCheck['errCode'] == 2){
				$returnArr['errCode'] = 2;
	            $returnArr['errMsg'] = "Request Allready Rejected";
			} else {
				$returnArr['errCode'] = $requestStatusCheck['errCode'];
	            $returnArr['errMsg'] = "failed to update";
			}
		}
	} else {
		$GetwidrawalLimit = getAdminSettingsFromKeywordAdmin($connKeywords);
		if ($GetwidrawalLimit['errCode']['-1'] == -1) {
			$mmDeptAmtLmit = $GetwidrawalLimit['errMsg']['cashout_manual_mode_amount_mmDepartment'];
			$smDeptAmtLmit = $GetwidrawalLimit['errMsg']['cashout_manual_mode_amount_smDepartment'];

			foreach ($requestIds as $key => $value) {
				$givenData = strtotime($value->requestTime);
				$tableName = "queue_processor_".date("m",$givenData)."_".$year=date("Y",$givenData);
				$RequestedId = $value->id;	

				
				if ($userAction == 'Hold') {

					if ($Usertype == "{$cohod}"){							
						$updatingStatus = "hold_by_hod";							
					} else if ($Usertype == "{$comm}") {							
						$updatingStatus = "hold_by_mm";							
					}

				} else if ($userAction == 'Reject') {

					if ($Usertype == "{$cohod}"){							
						$updatingStatus = "rejected_by_hod";							
					} else if ($Usertype == "{$comm}") {							
						$updatingStatus = "rejected_by_mm";							
					}

				} else if ($userAction == 'Approve') {

					$getCashoutRequest = GetCashOutDetail($RequestedId, $tableName, $connKeywords);
					$getCashoutRequest = $getCashoutRequest['errMsg'][0]['payment_amount'];

					if ($Usertype == "{$cohod}"){
						if ($getCashoutRequest < $mmDeptAmtLmit) {
							$updatingStatus = "approved_tbc";
						} else {
							$updatingStatus = "pending_for_mm_approval";
						}
					} else if ($Usertype == "{$comm}") {
						if ($getCashoutRequest < $smDeptAmtLmit) {
							$updatingStatus = "approved_tbc";
						} else {
							$updatingStatus = "pending_for_sm_approval";
						}
					}
					$actionMessage = '';

				}
				$actionPerformedBy = $_SESSION['admin_id'];			
				$requestStatusCheck = CheckCashoutRequestStatus($RequestedId, $tableName, $connKeywords);		
				$setRequestUpdate = updateCashoutRequest($RequestedId, $tableName, $updatingStatus, $actionPerformedBy, $actionMessage, $connKeywords);
				if (noError($requestStatusCheck)) {
					if (noError($setRequestUpdate)) {

					    if ($userAction=="Reject") {
	                        $resultReject=rejectManualCashoutRequest($RequestedId,$tableName,$connKeywords);
	                        if (noError($resultReject)) {
	                            $returnArr['errCode'] = -1;
	                            $returnArr['errMsg'] = "Updated Successfully";
	                        } else {
	                            $returnArr['errCode'] = $resultReject['errCode'];
	                            $returnArr['errMsg'] = $resultReject['errMsg'];
	                        }
	                   } else {
	                        $returnArr['errCode'] = -1;
	                        $returnArr['errMsg'] = "Updated Successfully";
	                    }

					} else {
						$returnArr['errCode'] = $setRequestUpdate['errCode'];
						$returnArr['errMsg'] = $setRequestUpdate['errMsg'];
					}	
				} else if ($requestStatusCheck['errCode'] == 2){
					$returnArr['errCode'] = 2;
		            $returnArr['errMsg'] = "Request Allready Rejected";
				} else {
					$returnArr['errCode'] = $requestStatusCheck['errCode'];
		            $returnArr['errMsg'] = "failed to update";
				}					
			}
		} else {
			$returnArr['errCode'] = 45;
			$returnArr['errMsg'] = "Parameters are blank .";
		}	
	}


		

	echo json_encode($returnArr,true);
?>