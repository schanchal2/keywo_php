<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/cashout/cashout_model.php');

//end other

// printArr($_POST);


$connKeywords = createDBConnection("acl");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$email = cleanQueryParameter($connKeywords, strtolower(cleanXSS($_POST["email"])));
$cashoutAgentEmail = $_POST["cashoutAgentsArray"];
if (count($cashoutAgentEmail) == 1){
    $cashoutAdminArray = $cashoutAgentEmail;
} else {
    $cashoutAdminArray = array();
}



//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllCashoutAgentEmailDataCount($connKeywords,$email,$cashoutAdminArray)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllCashoutAgentEmailData($page_position, $limit, $connKeywords, $email, $cashoutAdminArray);
$results = $results["errMsg"];
// printArr($results);
// foreach ($results as $key => $value) {
//     echo "<br>".$value['group_id'];
// }
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllCashoutAgentEmailDataCount($connKeywords,$email, $cashoutAdminArray)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllCashoutAgentEmailData($page_position, $limit, $connKeywords, $email, $cashoutAdminArray);
    $results = $results["errMsg"];
}


?>
<div id="userList" class="m-t-15">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            
            <th class="f-sz16">First Name</th>
            <th class="f-sz16">Last Name</th>
            <th class="f-sz16">Email</th>
            <th class="f-sz16">Select</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="10">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $allTickeData) {
                // printArr($allTickeData);
                // if ()
                ?>
                <tr>
                    
                    <td><?php echo ucwords($allTickeData['admin_first_name']); ?></td>
                    <td><?php echo ucwords($allTickeData['admin_last_name']); ?></td>
                    <td><?php echo $allTickeData['admin_email']; ?></td>
                    <td><input type = "button" id = "tcktAssignedTo<?php echo $allTickeData["admin_id"];?>" class = "btn btn-primary tckt_assigning_btn" onclick = "reassignCashReqToAgent('<?php echo $allTickeData["admin_id"];?>');" agent_email = "<?php echo $allTickeData["admin_email"];?>" value = "Assign"></td>                    
                </tr>
                <?php
            }
        }?>
        </tbody>
    </table>
</div>
<?php

getPaginationData($lastpage, $page_number, $limit,$email);
function getPaginationData($lastpage, $pageno, $limit,$email)
{

?>
<span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResultPopup">
                    <option  value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
                    <option  value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
                    <option  value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
                    <option  value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
                    <option  value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
<?php
    echo '<input type="text" id="hiddenpagePopup" name="hiddenpagePopup" value="' . $pageno . '" hidden>';
    echo '<div class="box-footer clearfix" style = "margin-bottom:20px; !important">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick="loadEmailDetailsOnPopUp(\'' . $pagenum . '\')">&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick="loadEmailDetailsOnPopUp(\'' . $pagenumber . '\')">Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick="loadEmailDetailsOnPopUp(\'' . $pagenumber . '\')">' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick="loadEmailDetailsOnPopUp(\'' . $pagenumber . '\')">Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick="loadEmailDetailsOnPopUp(\'' . $lastpage . '\')">&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

?>


<script>

     $("#LimitedResultPopup").change(function(){
       loadEmailDetailsOnPopUp("1");
    });
</script>