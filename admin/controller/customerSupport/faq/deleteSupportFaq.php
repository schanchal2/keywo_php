<?php
	session_start();

	//start config
	require_once('../../../config/config.php');
	require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
	require_once('../../../config/db_config.php');
	//end config

	//start helper
	require_once('../../../helpers/coreFunctions.php');
	require_once('../../../helpers/arrayHelper.php');
	require_once('../../../helpers/stringHelper.php');
	require_once('../../../helpers/date_helpers.php');
	//end helper

	//other
	require_once('../../../core/errorMap.php');
	require_once('../../../model/customer/customerModel.php');

	//end other

	$id = cleanXSS($_POST['id']);
	$returnArr = array();
	$connKeywords = createDBConnection("dbkeywords");
	noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

	$deleteFaqById = deleteSupportFaqData($id, $connKeywords);

	if (noError($deleteFaqById)) {
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] =$deleteFaqById['errMsg'];
    } else {
        $returnArr["errCode"] = 41;
        $returnArr["errMsg"] = $deleteFaqById['errMsg'];
    }
echo json_encode($returnArr, true);

?>