<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/customer/customerModel.php');

//end other

//printArr($_POST);


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$category = cleanQueryParameter($connKeywords, cleanXSS($_POST["category"]));



//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAllSupportfaqsDataCount($connKeywords,$category)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAllSupportfaqsData($page_position, $limit, $connKeywords, $category);
$results = $results["errMsg"];
// printArr($results);
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAllSupportfaqsDataCount($connKeywords,$category)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAllSupportfaqsData($page_position, $limit, $connKeywords, $category);
    $results = $results["errMsg"];
}


?>
<div id="userList" class="m-t-15">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th class="f-sz16">Question</th>
            <th class="f-sz16">Answer</th>
            <th class="f-sz16">Video</th>
            <th class="f-sz16">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="10">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $allTickeData) {
                // printArr($allTickeData);
                ?>
                <tr>
                    <td><?php echo $allTickeData['question']; ?></td>
                    <td><?php echo $allTickeData['answer']; ?></td>
                    <td> 
                        <iframe src="<?php echo $allTickeData['video_link']; ?>" width="200" height="100"></iframe> 
                    </td>
                    <td>
                         <a onclick = "EditSupportFaq('<?php echo $allTickeData["id"]; ?>','<?php echo $allTickeData["question"]; ?>','<?php echo $allTickeData["answer"]; ?>','<?php echo $allTickeData["video_link"]; ?>');">Edit</a><br>
                         <a onclick = "deleteFaq(<?php echo $allTickeData['id']; ?>);">Delete</a>
                    </td>
                </tr>
                <?php
            }
        }?>
        </tbody>
    </table>
</div>
<?php

getPaginationData($lastpage, $page_number, $limit,$category);
function getPaginationData($lastpage, $pageno, $limit,$category)
{

?>
<span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResult">
                    <option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
                    <option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
                    <option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
                    <option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
                    <option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
<?php
    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
    echo '<div class="box-footer clearfix" style = "margin-bottom:20px; !important">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick="getSupportFaqPages(\'' . $pagenum . '\',\'' . $category . '\',' . $limit . '\')">&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick="getSupportFaqPages(\'' . $pagenumber . '\',\'' . $category . '\',\'' . $limit . '\')">Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick="getSupportFaqPages(\'' . $pagenumber . '\',\'' . $category . '\',\'' . $limit . '\')">' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick="getSupportFaqPages(\'' . $pagenumber . '\',\'' . $category . '\',\'' . $limit . '\')">Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick="getSupportFaqPages(\'' . $lastpage . '\',\'' . $category . '\',\'' . $limit . '\')">&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

?>


<script>

    $("#LimitedResult").change(function(){
        LoadSupportFAQList('how does one');
    });
</script>