<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
require_once('../../../model/customer/common_cust.php');

//end other

//printArr($_POST);

$agent_id=$_SESSION["agent_id"];
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$ticketId = cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"]));
$tickedUserEmail =cleanQueryParameter($connKeywords, cleanXSS($_POST["userEmailId"]));
$ticketAgent =cleanQueryParameter($connKeywords, cleanXSS($_POST["assignedAgent"]));
$country =strtolower(cleanQueryParameter($connKeywords, cleanXSS($_POST["country"])));
$ticketStatus =cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketStatus"]));
$creationTime =cleanQueryParameter($connKeywords, cleanXSS($_POST["creationTime"]));


//Get page number from Ajax
if (isset($_POST["page"])) {
    $page_number = (int)filter_var($_POST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH); //filter number
    if (!is_numeric($page_number)) {
        die('Invalid page number!');
    } //incase of invalid page number
} else {
    $page_number = 1; //if there's no page number, set it to 1
}
if ($page_number == 0) {
    $page_number = 1;
}
$item_per_page = $limit;
$get_total_rows = getAgentServiceRequestCount($connKeywords, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id)["errMsg"];

$total_pages = ceil($get_total_rows / $item_per_page);

//position of records
$page_position = (string)(($page_number - 1) * $item_per_page);
$lastpage = ceil($total_pages);


$results = getAgentServiceRequest($page_position, $limit, $connKeywords, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id);
$results = $results["errMsg"];
$count = count($results);

if ($count == 0) {
    $page_number = $page_number - 1; //if there's no page number, set it to 1
    $item_per_page = $limit;
    $get_total_rows = getAgentServiceRequestCount($connKeywords, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id)["errMsg"];
    $total_pages = ceil($get_total_rows / $item_per_page);

//position of records
    $page_position = (($page_number - 1) * $item_per_page);
    $lastpage = ceil($total_pages);

    $results = getAgentServiceRequest($page_position, $limit, $connKeywords, $ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus, $creationTime,$agent_id);
    $results = $results["errMsg"];
}


?>
<div id="newRegisteredUsersList">
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th class="p-b-15">Ticket ID</th>
            <th class="p-b-15">Timestamp</th>
            <th class="p-b-15">Query Type</th>
            <th class="p-b-15">User Email</th>
            <th class="p-b-15">Country</th>
            <th>User Live Status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = ($page_number - 1) * $item_per_page;

        if ($get_total_rows == 0) {
            ?>
            <tr>
                <td style="text-align: center" colspan="10">No Data Found</td>
            </tr>
            <?php
        } else {
            foreach ($results as $allTickeData) {
                ?>
                <tr>
                    <td class="purple-blue" data-toggle="modal"
                        onclick="showModalChat('<?php echo $allTickeData['ticket_id'] ?>')"><?php echo $allTickeData['ticket_id'] ?></td>
                    <td>
                        <?php
                        $mydate = uDateTime("m-d-Y h:i:s", $allTickeData["requestTime"]);
                        $datetime = explode(" ", $mydate);
                        ?>
                        <span class="date"> <?php echo $datetime[0]; ?> </span>
                        <time> <?php echo $datetime[1]; ?> </time>

                    </td>
                    <td><?php echo $allTickeData['request_type'] ?></td>
                    <td><?php echo $allTickeData['reg_email'] ?></td>
                    <td><?php echo $allTickeData['user_country'] ?></td>
                    <td>
                        <?php
                        $result=getUserLiveStatus($allTickeData['reg_email'])["errMsg"];
                        if($result=="true")
                        {
                            $color="userOnline";
                        }else
                        {
                            $color="userOffline";
                        }

                        ?><span class="fa fa-circle <?= $color;?>"></span></td>
                </tr>
                <?php
            }
        }?>
        </tbody>
    </table>
</div>
<?php

getPaginationData($lastpage, $page_number, $limit,$ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus,$creationTime);
function getPaginationData($lastpage, $pageno, $limit,$ticketId,$tickedUserEmail,$ticketAgent,$country,$ticketStatus,$creationTime)
{

?>
<span class="pull-left recordCountsShow-styled-select"> Show
            <select id="LimitedResult">
                    <option value="5" <?php if($limit==5){ echo "selected"; } ?>> 05 </option>
                    <option value="10" <?php if($limit==10){ echo "selected"; } ?>> 10 </option>
                    <option value="20" <?php if($limit==20){ echo "selected"; } ?>> 20 </option>
                    <option value="30" <?php if($limit==30){ echo "selected"; } ?>> 30 </option>
                    <option value="40" <?php if($limit==40){ echo "selected"; } ?>> 40 </option>
                    <option value="50" <?php if($limit==50){ echo "selected"; } ?>> 50 </option>
                </select>
            </span>
<?php
    echo '<input type="text" id="hiddenpage" name="hiddenpage" value="' . $pageno . '" hidden>';
    echo '<div style="" class="box-footer clearfix">';
    echo '<ul class="pagination pagination-sm no-margin pull-right" style="font-weight: bold">';


    if ($pageno > 1) {

        $pagenum = 1;
        print('<li><a href="#"onclick=getNewTicketsPage("' . $pagenum . '","' . $tickedUserEmail . '","' . $ticketId . '","' . $ticketAgent . '","' . $country . '","' . $ticketStatus . '","' . $creationTime . '","' . $limit . '")>&laquo;</a></li>');
    }

    if ($pageno > 1) {
        $pagenumber = $pageno - 1;
        print('<li><a href="#" onclick=getNewTicketsPage("' . $pagenumber . '","' . $tickedUserEmail . '","' . $ticketId . '","' . $ticketAgent . '","' . $country . '","' . $ticketStatus . '","' . $creationTime . '","' . $limit . '")>Previous</a></li>');
    }

    if ($pageno == 1) {
        $startLoop = 1;
        $endLoop = ($lastpage < 5) ? $lastpage : 5;
    } else if ($pageno == $lastpage) {
        $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
        $endLoop = $lastpage;
    } else {
        $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
        $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
    }

    for ($i = $startLoop; $i <= $endLoop; $i++) {
        if ($i == $pageno) {
            print('   <li class = "active"><a href = "#">' . $pageno . '</a></li>');
        } else {
            $pagenumber = $i;
            print('<li><a href="#" onclick=getNewTicketsPage("' . $pagenumber . '","' . $tickedUserEmail . '","' . $ticketId . '","' . $ticketAgent . '","' . $country . '","' . $ticketStatus . '","' . $creationTime . '","' . $limit . '")>' . $i . '</a></li>');
        }
    }
    if ($pageno < $lastpage) {
        $pagenumber = $pageno + 1;
        print('<li><a href="#" onclick=getNewTicketsPage("' . $pagenumber . '","' . $tickedUserEmail . '","' . $ticketId . '","' . $ticketAgent . '","' . $country . '","' . $ticketStatus . '","' . $creationTime . '","' . $limit . '")>Next</a></li>');

    }

    if ($pageno != $lastpage) {
        print('<li><a href="#" onclick=getNewTicketsPage("' . $lastpage . '","' . $tickedUserEmail . '","' . $ticketId . '","' . $ticketAgent . '","' . $country . '","' . $ticketStatus . '","' . $creationTime . '","' . $limit . '")>&raquo;</a></li>');
    }


    echo '</ul>';
    echo '</div>';
}

?>


<script>

    $("#LimitedResults").change(function () {

    });

    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });

    $("#LimitedResult").change(function(){
        getNewTickets();
    });
</script>