<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other
require_once("../../../model/customer/customerModel.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$returnArr = array();

$operation= cleanQueryParameter($connKeywords, cleanXSS($_POST["operation"]));

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($operation) {
        case "checkMsg":
            $ticketID= isset($_POST["ticketID"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"])):"";
            $result =$getComment=getCommentText($ticketID, $connKeywords, "");
            if ($result["errCode"] == -1) {
                $getComment=$getComment["errMsg"][0];
                $endmsgId=end($getComment)["id"];
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$endmsgId;
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result["errMsg"];
                echo json_encode($returnArr, true);
            }
            break;
    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>
