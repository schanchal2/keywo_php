<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other
require_once("../../../model/customer/customerModel.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();
if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {
            $id                  = cleanQueryParameter($connKeywords, cleanXSS($_POST["id"]));
            $category_faq        = cleanQueryParameter($connKeywords, cleanXSS($_POST["category_faq"]));
            $question_faq        = cleanQueryParameter($connKeywords, cleanXSS($_POST["question_faq"]));
            $answer_faq          = cleanQueryParameter($connKeywords, cleanXSS($_POST["answer_faq"]));
            $video_link_faq      = cleanQueryParameter($connKeywords, cleanXSS($_POST["video_link_faq"]));

            $result = $setFAQs = setRightSideFAQs($id, $category_faq,$question_faq,$answer_faq, $video_link_faq, $connKeywords);
            // printArr($result);
            if ($result["errCode"] == -1) {
                if(isset($getComment["errMsg"]))
                {
                    $getComment=$getComment["errMsg"];
                    $endmsgId=end($getComment)["id"];
                }else
                {
                    $endmsgId="";
                }

                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$endmsgId;
            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = $result["errMsg"];
            }
}

echo json_encode($returnArr);
?>
