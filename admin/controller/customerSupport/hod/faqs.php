<?php
session_start();
$docrootpath = __DIR__;
$docrootpath = explode('/admin', $docrootpath);
$docrootpath = $docrootpath[0] . "/admin/";

require_once("{$docrootpath}/model/keywords/common_keyword.php");
require_once("{$docrootpath}/model/acl/SessionCheck.php");
require_once("{$docrootpath}/model/customer/customerModel.php");

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$faqKey = $_POST["faqKey"];
$start = 0;
$limit = 10;
$id = 1;

//printArr($countFAQs);
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $start=($id-1)*$limit;
}

$allFAQs = getAllFAQs($faqKey,$start,$limit,$connKeywords);
if(noError($allFAQs)){
    $allFAQs                              = $allFAQs["errMsg"];
}

$countFAQs = countFAQs($connKeywords);
if(noError($countFAQs)){
    $countFAQs                              = $countFAQs["errMsg"];
}

// get No of pages to show
$total=ceil($countFAQs['total']/$limit);

if(count($allFAQs)>0) {

    ?>


    <?php foreach ($allFAQs as $faqDetails) { // printArr($faqDetails); ?>
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id=" <?php echo $faqDetails['id']; ?>">
                <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                       href="#collapse<?php echo $faqDetails['id']; ?>" aria-expanded="false"
                       aria-controls="collapse<?php echo $faqDetails['id']; ?>">
                        <i class="fa fa-plus-circle"> </i> <i class="fa fa-minus-circle"> </i>
                        <?php echo $faqDetails['ques_title']; ?>
                    </a>
                </h4>
            </div>
            <div id="collapse<?php echo $faqDetails['id']; ?>" class="panel-collapse collapse" role="tabpanel"
                 aria-labelledby=" <?php echo $faqDetails['id']; ?>">
                <div class="panel-body" id="ansText<?php echo $faqDetails['id']; ?>">
                    <?php echo $faqDetails['ans']; ?>
                </div>
            </div>

        </div>
    <?php }
}else
{
    ?>
<div class="panel panel-default">
    <div class="text-center">Sorry! No Data Found.</div>
</div>
<?php
}

if(count($allFAQs)>0) {
    ?>

    <div class="p-5 bg-darkblue clearfix">

    <div class="pull-right">
        <?php
        if ($id > 1) {
            //echo "<a href='#' onclick='getFAQsList(".($id-1). ")' class='button'>PREVIOUS</a>";
            echo '<button type="button" onclick="getFAQsList(\'' . ($id - 1) . '\')" class="btn  bg-blue text-white prevNextBtn"> <i class="fa fa-angle-left"></i> </button> ';
        } else {
            echo '<button type="button" class="btn  bg-blue text-white prevNextBtn" disabled> <i class="fa fa-angle-left"></i> </button>';
        }


        if ($id != $total && count($allFAQs)> $limit-1) {
            //  echo "<a href='#' onclick='getFAQsList(".($id+1). ")' class='button'>NEXT</a>";
            echo '<button type="button" onclick="getFAQsList(\'' . ($id + 1) . '\')" class="btn  bg-blue text-white prevNextBtn"> <i class="fa fa-angle-right"></i> </button>';
        } else {
            echo '<button type="button" class="btn  bg-blue text-white prevNextBtn" disabled> <i class="fa fa-angle-right"></i> </button>';
        }

        ?>
    </div>
    <?php
}
?>
