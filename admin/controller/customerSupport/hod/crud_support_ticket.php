<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other
require_once("../../../model/customer/common_cust.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);


$returnArr = array();

$operation= cleanQueryParameter($connKeywords, cleanXSS($_POST["operation"]));

if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {

    switch ($operation) {
        case "changeTicketStatus":
            $ticketID= cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"]));
            $newStatus= cleanQueryParameter($connKeywords, cleanXSS($_POST["newStatus"]));
            $result =changeTicketStatus($ticketID,$newStatus,$connKeywords);
            if ($result['errCode'] == -1) {
                $updatedStatus=$newStatus;
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] =$updatedStatus;
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Failed To Update Status";
                echo json_encode($returnArr, true);
            }
            break;

        case "escalateTicket":
            $ticketID= cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"]));

            $result =escalateTicketToHod($ticketID,$connKeywords);
            if ($result['errCode'] == -1) {
                // $updatedStatus=$newStatus;
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"] ="Success ticket Escalated to HOD for processing.";
                echo json_encode($returnArr, true);

            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"] = "Failed To Update Status";
                echo json_encode($returnArr, true);
            }
            break;
    }

} else {
    $returnArr["errCode"] = "42";
    $returnArr["errMsg"] = "You Are not authorised user!!";
    echo json_encode($returnArr, true);
}


?>
