<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other
require_once("../../../model/customer/customerModel.php");
require_once("../../../model/acl/acl_model.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$returnArr = array();
if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {
    $id = cleanQueryParameter($connKeywords, cleanXSS($_POST["id"]));
    $category_faq = cleanQueryParameter($connKeywords, cleanXSS($_POST["category_faq"]));
    $question_faq = cleanQueryParameter($connKeywords, cleanXSS($_POST["question_faq"]));
    $answer_faq   = cleanQueryParameter($connKeywords, cleanXSS($_POST["answer_faq"]));
    $adminUserGroup = getAdminGroup($connAdmin);
    if (noError($adminUserGroup)) {
        $adminUserGroup = $adminUserGroup['errMsg']['group_id'];
        if(strlen(strstr(strtolower($adminUserGroup), $agentsGroupCS)) >0 || strlen(strstr(strtolower($adminUserGroup), $coagent)) >0) {
            $createdBy = $_SESSION['admin_id'];
            $approvedBy = '';
            $status = 0;            
        } else {
            $createdBy = $_SESSION['admin_id'];
            $approvedBy = $_SESSION['admin_id'];
            $status = 1;
        }
        $result       = $setFAQs = setFAQs($id, $category_faq,$question_faq,$answer_faq, $createdBy, $approvedBy,$status, $connKeywords);
        if ($result[errCode] == -1) {
            $getComment=$getComment["errMsg"];
            $endmsgId=end($getComment)["id"];
            $returnArr["errCode"] = "-1";
            $returnArr["errMsg"] =$endmsgId;
        } else {
            $returnArr["errCode"] = "41";
            $returnArr["errMsg"] = $result["errMsg"];
        }
    } else {
        $returnArr["errCode"] = "41";
        $returnArr["errMsg"] = "Error in Adding FAQ";
    }

    
}

echo json_encode($returnArr);
?>
