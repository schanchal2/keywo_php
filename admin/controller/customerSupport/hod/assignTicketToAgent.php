<?php
    session_start();

    //start config
    require_once('../../../config/config.php');
    require_once('../../../config/db_config.php');
    //end config

    //start helper
    require_once('../../../helpers/coreFunctions.php');
    require_once('../../../helpers/deviceHelper.php');
    require_once('../../../helpers/arrayHelper.php');
    require_once('../../../helpers/stringHelper.php');
    require_once('../../../helpers/date_helpers.php');
    //end helper

    //other
    require_once('../../../core/errorMap.php');
    //end other
    require_once("../../../model/customer/customerModel.php");

    $connKeywords = createDBConnection("dbkeywords");
    noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

    $returnArr = array();
    if (isset($_SESSION["admin_name"]) && isset($_SESSION["admin_id"])) {
        $agentId    = isset($_POST["agentId"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["agentId"])):"";
        $tickets    = $_POST["tickets"];
        $agentEmail = isset($_POST["agentEmail"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["agentEmail"])):"";
        $assignedBy = $_SESSION['admin_id'];
        
        foreach ($tickets as $key => $value) {
            $singleTicket    = cleanQueryParameter($connKeywords, cleanXSS($value));
            $result = $setFAQs = assignTicketToAgent($singleTicket, $agentEmail, $agentId, $assignedBy, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = "-1";
                $returnArr["errMsg"]  = $result['errMsg'];
            } else {
                $returnArr["errCode"] = "41";
                $returnArr["errMsg"]  = $result['errMsg'];
                break;
            }
        }
    }

    echo json_encode($returnArr);
?>