<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/deviceHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/date_helpers.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other
require_once("../../../model/customer/common_cust.php");

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$returnArr = array();

$agent_id= cleanQueryParameter($connKeywords, cleanXSS($_POST["agent_id"]));
$result=getAgentTicketAnalytics($agent_id,$connKeywords)["errMsg"]["0"];

?>
<table class="table  text-center table-responsive m-0">
    <thead>
    <tr>
        <th class="text-center text-white">New</th>
        <th class="text-center text-white">Open</th>
        <th class="text-center text-white">Pending</th>
        <th class="text-center text-white">Escalated</th>
        <th class="text-center text-white">Closed</th>
        <th class="text-center text-white">Total Queries</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?= isset($result["new"])?$result["new"]:0; ?></td>
        <td><?= isset($result["open"])?$result["open"]:0; ?></td>
        <td><?= isset($result["pending"])?$result["pending"]:0; ?></td>
        <td><?= isset($result["escalated"])?$result["escalated"]:0; ?></td>
        <td><?= isset($result["closed"])?$result["closed"]:0; ?></td>
        <td><?= isset($result["total_queries"])?$result["total_queries"]:0; ?></td>
    </tr>
    </tbody>
</table>


