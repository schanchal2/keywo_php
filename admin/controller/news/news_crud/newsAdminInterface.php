<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/coreFunctions.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other


$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);

$fileName = mysqli_real_escape_string($connDemo, trim(urldecode($_POST['fileName'])));
$string = str_replace(' ', '', trim($fileName)); // Replaces all spaces with hyphens.
$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.	
$string = preg_replace('/-+/', '', $string);

//Create rss file name
$checkfile = explode(".", $fileName);
if (end($checkfile) != "php") {
    $fileName = $string . ".php";
} else {
    $fileName = $string;
}

$rssUrl = mysqli_real_escape_string($connDemo, trim($_POST['rssUrl']));
$category = mysqli_real_escape_string($connDemo, trim($_POST['category']));
$frequency = mysqli_real_escape_string($connDemo, trim($_POST['frequency']));
$source = mysqli_real_escape_string($connDemo, trim($_POST['source']));
$countrye = mysqli_real_escape_string($connDemo, trim($_POST['country']));


if($rssUrl=="" || $category=="" || $frequency=="" || $source=="" || $countrye=="" || $fileName=="")
{
    $returnArr["errCode"] = 1;
    $returnArr["errMsg"] = "All Fields are required!";

    echo (json_encode($returnArr));
    exit;
}
/*
1.Add news RSS in channels table.
2.Creates news rss.php file. 
3.Add news items in items and images table.
*/
function addChannels($fileName, $rssUrl, $category, $frequency, $source, $countrye, $connDemo)
{
    global $rootUrl, $docRootAdmin;

    $fileDir = $docRootAdmin . "controller/news/collection";

    $channelLastUpdate = date('Y-m-d H:i:s');

    if (!is_dir($fileDir)) {
        mkdir($fileDir, 0777, true);
    }

    $file = fopen($fileDir . "/" . $fileName, "w+");
    $fileData = '<?php require_once(\'../../../model/news/common.php\'); 
				$rssInfo[\'rss\'] = "' . $rssUrl . '"; 
				$rssInfo[\'file_url\'] = "https://". $_SERVER[\'SERVER_NAME\'] . $_SERVER[\'REQUEST_URI\']; 
				$rssInfo[\'category\'] = "' . $category . '";
				$rssInfo[\'updateFrequency\'] = ' . $frequency . '; 
				$rssInfo[\'source\'] = "' . $source . '"; 
				$rssInfo[\'country\'] = "' . $countrye . '"; 
				callRSS($rssInfo); 
				?>';
    $fileData;
    fwrite($file, $fileData);
    fclose($file);

    //chmod($fileDir . "/" . $fileName, 0777);

    global $adminRoot;
     $fileURL = $adminRoot . "controller/news/collection/" . $fileName;


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $fileURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($ch);
    curl_close($ch);
    $output = (array)json_decode($output, true);


    if (noError($output)) {
        $sql = 'UPDATE channels SET country="' . $countrye . '",source="' . $source . '" WHERE rss_url ="' . $rssUrl . '" ';
        $resultQuery = mysqli_query($connDemo, $sql);

        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Adding new channel " . $category . " is successfull.";

        echo (json_encode($returnArr));
    } else {

        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error in adding channel " . $category . "";

        echo (json_encode($returnArr));
    }

    return $returnArr;
}

$data = addChannels($fileName, $rssUrl, $category, $frequency, $source, $countrye, $connDemo);


?>