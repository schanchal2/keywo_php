<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/coreFunctions.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$rssUrl = mysqli_real_escape_string($connSearch, trim($_REQUEST['rssUrl']));
$fileName = mysqli_real_escape_string($connSearch, trim($_REQUEST['fileName']));
$ext=".php";
if ($fileName) {
    $fileDir = $docRootAdmin . "controller/news/collection/" . $fileName.$ext;
    if (file_exists($fileDir)) {
        print "<span style=\"color:red;\">Channel already exists !!</span>";
        print "1";
    } else {
        print "<span style=\"color:green;\">Channel is available !!</span>";
        print "0";
    }
}

if ($rssUrl) {
    $data = mysqli_query($connSearch, "SELECT * FROM channels where rss_url ='$rssUrl'");
    if (mysqli_num_rows($data) > 0) {
        print "<span style=\"color:red;\">Channel already exists !!</span>";
        print "1";
    } else {
        print "<span style=\"color:green;\">Channel is available !!</span>";
        print "0";
    }
}


?>