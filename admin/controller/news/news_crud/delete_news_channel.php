<?php
session_start();

//start config
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
//end config

//start helper
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/coreFunctions.php');
//end helper

//other
require_once('../../../core/errorMap.php');
//end other


$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);
$returnArr = array();


if ($_POST['id']) {

    $id = mysqli_real_escape_string($connDemo, trim($_POST['id']));
    $sql = "SELECT file_url FROM channels where channel_id = '{$id}'";
    $resultQuery = runQuery($sql, $connDemo);
    $fileArr = mysqli_fetch_array($resultQuery["dbResource"]);
    $fileUrl = explode("/", $fileArr["file_url"]);

    global $docRootAdmin;
    $fileDir = $docRootAdmin . "controller/news/collection/" . end($fileUrl);

    $delete = "DELETE FROM channels WHERE channel_id='{$id}'";
    $getChannelsResult = runQuery($delete, $connDemo);

    if (noError($getChannelsResult)) {
        $deleteItems = "DELETE FROM items WHERE channel_id='{$id}'";
        $getItemsResult = runQuery($deleteItems, $connDemo);
        $deleteImages = "DELETE FROM images WHERE channel_id='{$id}'";
        $getImagesResult = runQuery($deleteImages, $connDemo);
        unlink($fileDir);
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Channel Deleted successfully!!";
    } else {
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error deleting channel";
    }

    print(json_encode($returnArr));
}
?>