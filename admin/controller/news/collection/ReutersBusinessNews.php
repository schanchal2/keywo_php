<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://feeds.reuters.com/reuters/INbusinessNews";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "Business";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "Reuters Business News";
$rssInfo['country'] = "India";
callRSS($rssInfo);
?>