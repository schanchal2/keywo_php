<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://rss.nytimes.com/services/xml/rss/nyt/InternationalHome.xml";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "nyt international home";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "NYtimes";
$rssInfo['country'] = "International";
callRSS($rssInfo);
?>