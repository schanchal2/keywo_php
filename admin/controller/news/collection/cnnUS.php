<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://rss.cnn.com/rss/edition_us.rss";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "cnn us";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "CNN US";
$rssInfo['country'] = "United States of America";
callRSS($rssInfo);
?>