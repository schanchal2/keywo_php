<?php
require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://rss.nytimes.com/services/xml/rss/nyt/World.xml";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "nyt world";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = 'The New York Times';
callRSS($rssInfo);
?>