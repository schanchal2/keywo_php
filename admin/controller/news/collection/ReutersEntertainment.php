<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://feeds.reuters.com/reuters/INentertainmentNews";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "Entertainment";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "Reuters Entertainment";
$rssInfo['country'] = "India";
callRSS($rssInfo);
?>