<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://feeds.reuters.com/reuters/INsouthAsiaNews";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "News";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "Reuters South Asia";
$rssInfo['country'] = "India";
callRSS($rssInfo);
?>