<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://www.forbes.com/feeds/popstories.xml";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "news";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "Forbes";
$rssInfo['country'] = "International";
callRSS($rssInfo);
?>