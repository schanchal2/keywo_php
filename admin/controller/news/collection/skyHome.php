<?php
require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://feeds.skynews.com/feeds/rss/home.xml";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "sky home";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = 'Sky News';
callRSS($rssInfo);
?>