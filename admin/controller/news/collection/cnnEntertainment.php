<?php
require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://rss.cnn.com/rss/edition_entertainment.rss";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "cnn entertainment";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = 'CNN News';
callRSS($rssInfo);
?>