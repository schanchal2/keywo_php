<?php require_once('../../../model/news/common.php');
$rssInfo['rss'] = "http://feeds.reuters.com/reuters/INtopNews";
$rssInfo['file_url'] = "https://" . $collectionNewsServerName . $_SERVER['REQUEST_URI'];
$rssInfo['category'] = "General";
$rssInfo['updateFrequency'] = 2;
$rssInfo['source'] = "Reuters Top News";
$rssInfo['country'] = "India";
callRSS($rssInfo);
?>