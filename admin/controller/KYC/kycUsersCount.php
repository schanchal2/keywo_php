<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once "../../model/kyc/kyc_model.php";
require_once("../../model/manageUser/manageUser.php");


//end other
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$keywoStat = getKeywoStat($connKeywords);
if (noError($keywoStat)) {
    $keywoStat = $keywoStat["errMsg"][0];
    $returnArr['errCode'] = -1;
    $returnArr['errMsg'] = $keywoStat;
} else {
    $returnArr['errCode'] = 1;
    $returnArr['errMsg'] = $keywoStat['errMsg'];
}
echo json_encode($returnArr);
?>