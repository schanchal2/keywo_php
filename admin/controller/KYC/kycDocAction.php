<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once "../../model/kyc/kyc_model.php";
require_once "../../model/settings/kyc_slab.php";
require_once "../../../models/analytics/userRegistration_analytics.php";//user side model


//end other
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$docDetails = json_decode(base64_decode($_POST['docDetail']));
$actionType = cleanXSS($_POST['type']);
$returnArr = array();
$email = $docDetails->email;
$docId = $docDetails->_id;
$docNo = $docDetails->docNo;
$setAction = changeKYCdocStatus($email, $docId, $docNo, $actionType);

if (noError($setAction)) {
    $setActionData = $setAction["errMsg"];
    if ($setActionData["kyc_level_msg"] == "level_3_changed") {
        insert_user_statistics("kyc_level_3_users", $connKeywords);

        /************************* update user kyc level 3 ************************************************/

        $userFields = "no_of_qualified_interactions_pending,user_id,first_name,last_name,mobile_number";
        $getUserDataDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userFields);
        if (noError($getUserDataDetails)) {
            $UserPendinGinteractionCount = $getUserDataDetails['errMsg']['no_of_qualified_interactions_pending'];
            $userId = $getUserDataDetails['errMsg']['user_id'];
            $firstName = $getUserDataDetails['errMsg']['first_name'];
            $lastName = $getUserDataDetails['errMsg']['last_name'];
            $mobileNumber = $getUserDataDetails['errMsg']['mobile_number'];
            $smsText="";
            $updateKYCLevelNotify = setUserNotifyInfo($userId, 'kyc_level', 'kyc_3');

            if (noError($updateKYCLevelNotify)) {
                $kycStablDetails = getKYCSlabByLevel($level = "3", $connSearch);
                if (noError($kycStablDetails)) {
                    $kyclevel3cnt = $kycStablDetails['errMsg'][0]["intraction"];
                    $level2data = getKYCSlabByLevel($level = "2", $connSearch);
                    if (noError($level2data)) {
                        $level2dataCount = $level2data['errMsg'][0]["intraction"];

                        $increaseCount = $level2dataCount - $UserPendinGinteractionCount;
                        $increaseCount = $kyclevel3cnt - $increaseCount;

                        $updateKYCLevelNotify = setUserNotifyInfo($userId, 'qualified_interact_pending', $increaseCount);

                        $mailSubject = 'You have successfully verified KYC Level-3 in Keywo';
                        $emailBody = '<br>Dear User,<br> Yor KYC document are verified you have got KYC level 3.<br>';
                        $notificationBody = "You have successfully verified KYC Level-3 in Keywo.";

                        $preferenceCode = 2;
                        $category = "wallet";
                        $sendMail = sendNotification($email,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category);

                        $returnArr['errCode'] = -1;
                        $returnArr['errMsg'] = "KYC level 3 has been updated successfully";
                    } else {
                        $returnArr['errCode'] = 4;
                        $returnArr['errMsg'] = "Failed to upgrade KYC level 3";
                    }
                } else {
                    $returnArr['errCode'] = 4;
                    $returnArr['errMsg'] = "Failed to upgrade KYC level 3";
                }
            } else {
                $returnArr['errCode'] = 4;
                $returnArr['errMsg'] = "Failed to upgrade KYC level 3";
            }
        }

        /************************* update user kyc level 3 ************************************************/
    }

    $returnArr['errCode'] = -1;
    $returnArr['errMsg'] = $setAction['errMsg'];
} else {
    $returnArr['errCode'] = 1;
    $returnArr['errMsg'] = $setAction['errMsg'];
}
echo json_encode($returnArr);
?>