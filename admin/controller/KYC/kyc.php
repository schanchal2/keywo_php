<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once "../../model/kyc/kyc_model.php";


//end other
$connAdmin = createDBConnection("acl");
noError($connAdmin) ? $connAdmin = $connAdmin["connection"] : checkMode($connAdmin["errMsg"]);

$userEmail = cleanQueryParameter($connAdmin, cleanXSS($_POST["UserEmail"]));
$result=getLevel3KYCusersToVerify($userEmail);
$result=$result["errMsg"];
?>

<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive" id="kycUsersListTable">
        <thead>
        <tr>
            <th rowspan="2" class="dtTableHeadPadding no-sort ">
                <input id="select_all" type="checkbox" name="">
                <label for="select_all" class="fa"></label>
            </th>
            <th rowspan="2" class="dtTableHeadPadding no-sort "> Email </th>
            <th rowspan="2" class="dtTableHeadPadding no-sort "> P3 </th>
            <th colspan="8"  class="bg-darkblue "> Document Links</th>
            <th rowspan="2" class="dtTableHeadPadding no-sort "> Action</th>
        </tr>
        <tr>
            <th class="dtTableHeadPadding no-sort "> Doc-1</th>
            <th class="dtTableHeadPadding no-sort "> Status</th>
            <th class="dtTableHeadPadding no-sort "> Doc-2</th>
            <th class="dtTableHeadPadding no-sort "> Status</th>
            <th class="dtTableHeadPadding no-sort "> Doc-3</th>
            <th class="dtTableHeadPadding no-sort "> Status</th>
            <th class="dtTableHeadPadding no-sort "> Doc-4</th>
            <th class="dtTableHeadPadding no-sort "> Status</th>
        </tr>


        </thead>
        <tbody>
        <?php
        foreach($result as $value) {

            ?>
            <tr>
                <td>
                    <input class="user_checkbox" id="user_checkbox"
                           type="checkbox" name="user_checkbox[]"
                           value="">
                    <label for="user_checkbox" class="fa"></label>
                </td>
                <td>
                    <?= $value["email"]; 

                    $value['kyc_3_doctype_1']["email"]=$value["email"];//value of email aded in array
                    $value['kyc_3_doctype_2']["email"]=$value["email"];//value of email aded in array
                    $value['kyc_3_doctype_3']["email"]=$value["email"];//value of email aded in array
                    $value['kyc_3_doctype_4']["email"]=$value["email"];//value of email aded in array

                    $value['kyc_3_doctype_1']["docNo"]=1;//value of Doc No aded in array
                    $value['kyc_3_doctype_2']["docNo"]=2;//value of Doc No aded in array
                    $value['kyc_3_doctype_3']["docNo"]=3;//value of Doc No aded in array
                    $value['kyc_3_doctype_4']["docNo"]=4;//value of Doc No aded in array
                    ?>
                </td>
                <td><?php
                    if(strtolower($value["kyc_3_doctype_1"]["status"])=="pending" || strtolower($value["kyc_3_doctype_2"]["status"])=="pending" || strtolower($value["kyc_3_doctype_3"]["status"])=="pending"|| strtolower($value["kyc_3_doctype_4"]["status"])=="pending")
                    {
                        echo "Pending";
                    }else
                    {
                        echo "Rejected";
                    }
                    ?>
                        
                </td>
                <td>
                    <a <?= isset($value["kyc_3_doctype_1"]["path"])?" onclick = 'kycDocDisplay(".json_encode($value['kyc_3_doctype_1'],true).");'":""; ?> ><?= isset($value["kyc_3_doctype_1"]["path"])?"Link":"NA"; ?></a>
                </td>
                <td>
                    <?= isset($value["kyc_3_doctype_1"]["status"])?ucfirst($value["kyc_3_doctype_1"]["status"]):"NA"; ?>
                </td>
                <td>
                    <a <?= isset($value["kyc_3_doctype_1"]["path"])?"onclick = 'kycDocDisplay(".json_encode($value['kyc_3_doctype_2'],true).");'":""; ?> ><?= isset($value["kyc_3_doctype_2"]["path"])?"Link":"NA"; ?></a>
                </td>
                <td>
                    <?= isset($value["kyc_3_doctype_2"]["status"])?ucfirst($value["kyc_3_doctype_2"]["status"]):"NA"; ?>
                </td>
                <td>
                    <a <?= isset($value["kyc_3_doctype_1"]["path"])?"onclick = 'kycDocDisplay(".json_encode($value['kyc_3_doctype_3'],true).");'":""; ?> ><?= isset($value["kyc_3_doctype_3"]["path"])?"Link":"NA"; ?></a>
                </td>
                <td>
                    <?= isset($value["kyc_3_doctype_3"]["status"])?ucfirst($value["kyc_3_doctype_3"]["status"]):"NA"; ?>
                </td>
                <td>
                    <a <?= isset($value["kyc_3_doctype_1"]["path"])?"onclick = 'kycDocDisplay(".json_encode($value['kyc_3_doctype_4'],true).");'":""; ?> ><?= isset($value["kyc_3_doctype_4"]["path"])?"Link":"NA"; ?></a>
                </td>
                <td>
                    <?= isset($value["kyc_3_doctype_4"]["status"])?ucfirst($value["kyc_3_doctype_4"]["status"]):"NA"; ?>
                </td>
                <td>

                </td>

            </tr>
            <?php
        }
        ?>

        </tbody>

    </table>
</div>


<script>


    $(function () {
        var _last_selected = null, checkboxes = $("#userDataList :checkbox");
        checkboxes.click(function (e) {
            var ix = checkboxes.index(this), checked = this.checked;


            if (e.shiftKey && ix != _last_selected) {
                checkboxes.slice(Math.min(_last_selected, ix), Math.max(_last_selected, ix))
                    .each(function () {
                        this.checked = checked

                    });
                _last_selected = null;

            } else {
                _last_selected = ix

            }
        })
    })

    $(document).ready(function () {
        $('#select_all').on('click', function () {
            if (this.checked) {
                $('.user_checkbox').each(function () {
                    this.checked = true;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            } else {
                $('.user_checkbox').each(function () {
                    this.checked = false;
                    $(this).closest('tr').toggleClass("highlight", this.checked);
                });
            }
        });

        $('.user_checkbox').on('click', function () {
            if ($('.user_checkbox:checked').length == $('.user_checkbox').length) {
                $('#select_all').prop('checked', true);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            } else {
                $('#select_all').prop('checked', false);
                $(this).closest('tr').toggleClass("highlight", this.checked);
            }
        });
    });


    $('#kycUsersListTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]
    });

</script>