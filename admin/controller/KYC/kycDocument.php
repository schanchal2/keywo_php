<?php
	session_start();

	//start config
	require_once('../../config/config.php');
	require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
	require_once('../../config/db_config.php');
	//end config

	//start helper
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/arrayHelper.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/date_helpers.php');
	//end helper

	//other
	require_once('../../core/errorMap.php');
	require_once "../../model/kyc/kyc_model.php";

    /*********************** get user Permissions ********************/
    $docrootpath = $docRootAdmin;
    require_once "{$docrootpath}model/acl/checkAccess.php";
    $myp = mypermissions($largest);
    /*********************** get user Permissions ********************/
	//end other

	$kycData        = $_POST['docDetail'];
	//$kycDocPath     = $kycUploadDirectoy.end(explode("/",$kycData['path']));
	$kycDocPath     =  $kycUploadDirectoy.basename($kycData['path']);
	$docPathExplode = pathinfo($kycDocPath);
	$kycDocExt      = $docPathExplode['extension'];
	$kycDocExt      = strtolower($kycDocExt);

	if ($kycDocExt == 'pdf' || $kycDocExt == 'doc' || $kycDocExt == 'docx' || $kycDocExt == 'dif') {
		$kycDataDisplay = '<iframe width="854" height="480" src="'.$kycDocPath.'" frameborder="0" allowfullscreen></iframe>';
	} else if ($kycDocExt == 'png' || $kycDocExt == 'jpg' || $kycDocExt == 'jpeg') {
		$kycDataDisplay = '<img class = "img-responsive" src = "'.$kycDocPath.'" >';
	}

?>

<div class="">
	<div class = "p-b-10">
		<?php echo $kycDataDisplay; ?>
		<!-- <a href="<?php echo $kycDocPath; ?>" target = "_blank">This IS link</a> -->
	</div>
	<div id="errMsg-kyc" style="display:none; color: red;"></div>
    <?php
    if((in_array("write",$myp))) {

    ?>
	<div class = "text-right">
		<button class="btn btn-success kyc-action" type="button" name="kycAccept" id="kycAccept_btn" value="" onclick="actionOnKycDoc('<?php echo base64_encode(json_encode($kycData, true)); ?>', 'approved');">Approve</button>
		<button class="btn btn-danger kyc-action" type="button" name="kycReject" id="kycReject_btn" value="" onclick="actionOnKycDoc('<?php echo base64_encode(json_encode($kycData, true)); ?>', 'rejected');">Reject</button>
	</div>
    <?php } ?>
</div>