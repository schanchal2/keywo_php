<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../helpers/arrayHelper.php");
require_once("../../config/db_config.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../core/errorMap.php");
require_once("../../helpers/cronHelper.php");
require_once("../../model/logReader/readLogsUM.php");

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$fromdateinput = $_POST["fromdate"];
$todateinput = $_POST["todate"];
if($_POST["userEmail"]!="") {
    $emailuser = $_POST["userEmail"];
}else{
    $emailuser ="";
}
$buyKwd_by = $_POST["buyKwd_by"];

global $logPath;
global $rootUrl;

$docrootpath = $logPath["keywordPurchase"];
$finalrootUrl = $rootUrl . "logs/keyword/purchase/";

if($buyKwd_by=="buy_by_wallet") {
    $filenameread = "_OClock_payByWallet.xml";

    $data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

    ?>

    <div id="smartTable">
        <table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left"
               id="keywordPurchaselogTablein">
            <thead style="">
            <tr>
                <th class="no-sort" style="width:30%;color:white;">Activity</th>
                <th class="no-sort" style="width:70%;color:white;">Steps</th>


            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $xmlData) {

               $keywordsArray= explode(" ",$xmlData->keywordBasket);
                ?>
                <tr>
                    <td style="text-align: left;">
                        <?php
                        if(isset($xmlData->attributes()->email))
                        {

                        }else{

                        }
                        ?>

                        <span class="LogAct">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                        <span class="LogAct">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                        <span class="LogAct">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                        <span class="LogAct">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                        <span class="LogAct">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                        <span class="LogAct">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
                        <span class="LogAct">Keywords:<?php echo $xmlData->keywordBasket; ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <?php

                        if ($xmlData->step != "") {
                            echo '<span class="LogAct">' . $xmlData->step . '</span><br/>';
                        }

                        if ($xmlData->step1 != "") {
                            echo '<span class="LogAct">' . $xmlData->step1 . '</span><br/>';
                        }

                        if ($xmlData->step2 != "") {
                            echo '<span class="LogAct">' . $xmlData->step2 . '</span><br/>';
                        }
                        if ($xmlData->step3 != "") {
                            echo '<span class="LogAct">' . $xmlData->step3 . '</span><br/>';
                        }

                        if ($xmlData->step4 != "") {
                            echo '<span class="LogAct">' . $xmlData->step4 . '</span><br/>';
                        }
                        if ($xmlData->step5 != "") {
                            echo '<span class="LogAct">' . $xmlData->step5 . '</span><br/>';
                        }
                        if ($xmlData->step6 != "") {
                            echo '<span class="LogAct">' . $xmlData->step6 . '</span><br/>';
                        }
                        if ($xmlData->step7 != "") {
                            echo '<span class="LogAct">' . $xmlData->step7 . '</span><br/>';
                        }
                        if ($xmlData->step8 != "") {
                            echo '<span class="LogAct">' . $xmlData->step8 . '</span><br/>';
                        }
                        if ($xmlData->step9 != "") {
                            echo '<span class="LogAct">' . $xmlData->step9 . '</span><br/>';
                        }

                        if ($xmlData->step10 != "") {
                            echo '<span class="LogAct">' . $xmlData->step10 . '</span><br/>';
                        }

                        if ($xmlData->step11 != "") {
                            echo '<span class="LogAct">' . $xmlData->step11 . '</span><br/>';
                        }

                        if ($xmlData->step22 != "") {
                            echo '<span class="LogAct">' . $xmlData->step12 . '</span><br/>';
                        }
                        if ($xmlData->step13 != "") {
                            echo '<span class="LogAct">' . $xmlData->step13 . '</span><br/>';
                        }

                        if ($xmlData->step14 != "") {
                            echo '<span class="LogAct">' . $xmlData->step14 . '</span><br/>';
                        }

                        foreach($keywordsArray as $keyword)
                        {
                            $keywordmd5="kwywordPurchase".md5($keyword);
                            $keywordmd5=(array) $xmlData->$keywordmd5;
                            $errCode=(string) $keywordmd5["errCode"];
                           // $errCode=$keywordmd5["errMsg"];
                            unset($keywordmd5["errCode"]);
                            unset($keywordmd5["errMsg"]);
                            if($errCode=="-1")
                            {
                                $color="color:green";
                            }else{
                                $color="color:red";
                            }
                            foreach($keywordmd5 as $key=>$values)
                            {
                                if($key=="step1")
                                {
                                    echo "<hr/><br/>";
                                }
                                echo '<span class="LogAct" style="'.$color.'">' . $values. '</span><br/>';
                            }
                        }


                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
    <?php
}

if($buyKwd_by=="buy_by_paypal")
{
    $filenameread = "_OClock_payByPaypal.xml";

    $data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

    ?>

    <div id="smartTable">
        <table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left"
               id="keywordPurchaselogTablein">
            <thead style="">
            <tr>
                <th style="width:20%;color:white;">Activity</th>
                <th style="width:70%;color:white;">Steps</th>


            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $xmlData) {

                $keywordsArray= explode(" ",$xmlData->keywordBasket);
                ?>
                <tr>
                    <td style="text-align: left;">
                        <span class="LogAct">Email:<?php echo $xmlData->attributes()->email; ?></span><br/>
                        <span class="LogAct">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                        <span class="LogAct">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                        <span class="LogAct">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                        <span class="LogAct">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                        <span class="LogAct">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                        <span class="LogAct">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
                        <span class="LogAct">Keywords:<?php echo $xmlData->keywordBasket; ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <?php

                        if ($xmlData->step != "") {
                            echo '<span class="LogAct">' . $xmlData->step . '</span><br/>';
                        }

                        if ($xmlData->step1 != "") {
                            echo '<span class="LogAct">' . $xmlData->step1 . '</span><br/>';
                        }

                        if ($xmlData->step2 != "") {
                            echo '<span class="LogAct">' . $xmlData->step2 . '</span><br/>';
                        }
                        if ($xmlData->step3 != "") {
                            echo '<span class="LogAct">' . $xmlData->step3 . '</span><br/>';
                        }

                        if ($xmlData->step4 != "") {
                            $data=explode("payment_mode",$xmlData->step4);
                            echo '<span class="LogAct">' . $data[0] . '</span><br/>';
                            echo '<span class="LogAct">' . $data[1] . '</span><br/>';
                        }
                        if ($xmlData->step5 != "") {
                            echo '<span class="LogAct">' . $xmlData->step5 . '</span><br/>';
                        }
                        if ($xmlData->step6 != "") {
                            echo '<span class="LogAct">' . $xmlData->step6 . '</span><br/>';
                        }
                        if ($xmlData->step7 != "") {
                            echo '<span class="LogAct">' . $xmlData->step7 . '</span><br/>';
                        }

                        foreach($keywordsArray as $keyword)
                        {
                            $keywordmd5="kwywordPurchase".md5($keyword);
                            $keywordmd5=(array) $xmlData->$keywordmd5;
                            $errCode=(string) $keywordmd5["errCode"];
                            // $errCode=$keywordmd5["errMsg"];
                            unset($keywordmd5["errCode"]);
                            unset($keywordmd5["errMsg"]);
                            if($errCode=="-1")
                            {
                                $color="color:green";
                            }else{
                                $color="color:red";
                            }
                            foreach($keywordmd5 as $key=>$values)
                            {
                                if($key=="step1")
                                {
                                    echo "<hr/><br/>";
                                }
                                echo '<span class="LogAct" style="'.$color.'">' . $values. '</span><br/>';
                            }
                        }


                        ?>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
    <?php
}



if($buyKwd_by=="accept_bitgo")
{
    $filenameread = "_OClock_buy_now.xml";

    $data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

    ?>

    <div id="debitPanel">
        <table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left"
               id="keywordPurchaselogTablein">
            <thead style="">
            <tr>
                <th class="no-sort" style="width:20%;color:white;">Activity</th>
                <th class="no-sort" style="width:35%;color:white;">Steps</th>
                <th class="no-sort" style="width:25%;color:white;">Request</th>
                <th class="no-sort" style="width:20%;color:white;">Response</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $xmlData) {
                ?>
                <tr style="color:<?php
                if (($xmlData->response->attributes()->errCode) == -1) {
                    echo "green !important";
                } else {
                    echo "red !important;";
                }
                ?>">
                    <td style="text-align: left;">
                        <span class="LogAct">Email:<?php echo $xmlData->attributes()->email; ?></span><br/>
                        <span class="LogAct">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                        <span class="LogAct">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                        <span class="LogAct">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                        <span class="LogAct">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                        <span class="LogAct">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                        <span class="LogAct">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <?php
                        if ($xmlData->step != "") {
                            echo '<span class="LogAct">' . $xmlData->step . '</span><br/>';
                        }

                        if ($xmlData->step1 != "") {
                            echo '<span class="LogAct">' . $xmlData->step1 . '</span><br/>';
                        }

                        if ($xmlData->step2 != "") {
                            echo '<span class="LogAct">' . $xmlData->step2 . '</span><br/>';
                        }
                        if ($xmlData->step3 != "") {
                            echo '<span class="LogAct">' . $xmlData->step3 . '</span><br/>';
                        }

                        if ($xmlData->step4 != "") {
                            echo '<span class="LogAct">' . $xmlData->step4 . '</span>';
                        }


                        ?>
                    </td>
                    <td style="text-align: left;">
                        <span class="LogAct">Credential:<?php printArr(json_decode($xmlData->credential,true)); ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <span class="LogAct">Error Code:<?php echo $xmlData->response->attributes()->errCode; ?></span><br/>
                        <span class="LogAct">Error Message:<?php echo $xmlData->response->attributes()->errMsg; ?></span><br/>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
    <?php
}
?>

<script>


    var table= $('#keywordPurchaselogTablein').DataTable({
       "bInfo": true,
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]
    });
    $('#buyKwdtextSearch').keyup(function () {
        table.search( this.value ).draw();
    } );
</script>

