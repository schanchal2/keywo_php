<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once("../../helpers/arrayHelper.php");
require_once("../../config/db_config.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../core/errorMap.php");
require_once("../../helpers/cronHelper.php");
require_once("../../model/logReader/readLogsUM.php");

$connDemo = createDBConnection("dbsearch");
noError($connDemo) ? $connDemo = $connDemo["connection"] : $error = $connDemo["errMsg"];

$fromdateinput = $_POST["fromdate"];
$todateinput = $_POST["todate"];
if($_POST["userEmail"]!="") {
    $emailuser = $_POST["userEmail"];
}else{
    $emailuser ="";
}
$trade_status = $_POST["trade_status"];

if($trade_status=="accept_bid") {
    $filenameread = "_OClock_accept_bid.xml";

    global $logPath;
    global $rootUrl;

    $docrootpath = $logPath["keywordBid"];
    $finalrootUrl = $rootUrl . "logs/keyword/bid/";

    $data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

    ?>

    <div id="smartTable">
        <table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left"
               id="tradelogTablein">
            <thead style="">
            <tr>
                <th class="no-sort" style="width:20%;color:white;">Activity</th>
                <th class="no-sort" style="width:60%;color:white;">Steps</th>
                <th class="no-sort" style="width:20%;color:white;">Response</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $xmlData) {
                ?>
                <tr style="color:<?php
                if (($xmlData->response->attributes()->errCode) == "-1") {
                    echo "green !important";
                } else {
                    echo "red !important;";
                }
                ?>">
                    <td style="text-align: left;">
                        <?php
                        if(isset($xmlData->attributes()->email))
                        {
                            ?>
                            <span class="LogAct">Email:<?php echo $xmlData->attributes()->email; ?></span><br/>
                            <?php
                        }else{
                            ?>
                            <span class="LogAct">Account Handle:<?php echo $xmlData->attributes()->account_handle; ?></span><br/>
                            <?php
                        }
                        ?>
                        <span class="LogAct">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                        <span class="LogAct">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                        <span class="LogAct">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                        <span class="LogAct">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                        <span class="LogAct">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                        <span class="LogAct">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <?php
                        if ($xmlData->step != "") {
                            echo '<span class="LogAct">' . $xmlData->step . '</span><br/>';
                        }

                        if ($xmlData->step1 != "") {
                            echo '<span class="LogAct">' . $xmlData->step1 . '</span><br/>';
                        }

                        if ($xmlData->step2 != "") {
                            echo '<span class="LogAct">' . $xmlData->step2 . '</span><br/>';
                        }
                        if ($xmlData->step3 != "") {
                            echo '<span class="LogAct">' . $xmlData->step3 . '</span><br/>';
                        }

                        if ($xmlData->step4 != "") {
                            echo '<span class="LogAct">' . $xmlData->step4 . '</span><br/>';
                        }
                        if ($xmlData->step5 != "") {
                            echo '<span class="LogAct">' . $xmlData->step5 . '</span><br/>';
                        }
                        if ($xmlData->step6 != "") {
                            echo '<span class="LogAct">' . $xmlData->step6 . '</span><br/>';
                        }
                        if ($xmlData->step7 != "") {
                            echo '<span class="LogAct">' . $xmlData->step7 . '</span><br/>';
                        }
                        if ($xmlData->step8 != "") {
                            echo '<span class="LogAct">' . $xmlData->step8 . '</span><br/>';
                        }
                        if ($xmlData->step9 != "") {
                            echo '<span class="LogAct">' . $xmlData->step9 . '</span><br/>';
                        }

                        if ($xmlData->step10 != "") {
                            echo '<span class="LogAct">' . $xmlData->step10 . '</span><br/>';
                        }

                        if ($xmlData->step11 != "") {
                            echo '<span class="LogAct">' . $xmlData->step11 . '</span><br/>';
                        }

                        if ($xmlData->step22 != "") {
                            echo '<span class="LogAct">' . $xmlData->step12 . '</span><br/>';
                        }
                        if ($xmlData->step13 != "") {
                            echo '<span class="LogAct">' . $xmlData->step13 . '</span><br/>';
                        }

                        if ($xmlData->step14 != "") {
                            echo '<span class="LogAct">' . $xmlData->step14 . '</span><br/>';
                        }
                        if ($xmlData->step15 != "") {
                            echo '<span class="LogAct">' . $xmlData->step15 . '</span><br/>';
                        }
                        if ($xmlData->step16 != "") {
                            echo '<span class="LogAct">' . $xmlData->step16 . '</span><br/>';
                        }
                        if ($xmlData->step17 != "") {
                            echo '<span class="LogAct">' . $xmlData->step17 . '</span><br/>';
                        }
                        if ($xmlData->step18 != "") {
                            echo '<span class="LogAct">' . $xmlData->step18 . '</span><br/>';
                        }
                        if ($xmlData->step19 != "") {
                            echo '<span class="LogAct">' . $xmlData->step19 . '</span><br/>';
                        }

                        if ($xmlData->step20 != "") {
                            echo '<span class="LogAct">' . $xmlData->step20 . '</span><br/>';
                        }

                        if ($xmlData->step21 != "") {
                            echo '<span class="LogAct">' . $xmlData->step21 . '</span><br/>';
                        }

                        if ($xmlData->step22 != "") {
                            echo '<span class="LogAct">' . $xmlData->step22 . '</span><br/>';
                        }
                        if ($xmlData->step23 != "") {
                            echo '<span class="LogAct">' . $xmlData->step23 . '</span><br/>';
                        }

                        if ($xmlData->step24 != "") {
                            echo '<span class="LogAct">' . $xmlData->step24 . '</span><br/>';
                        }
                        if ($xmlData->step25 != "") {
                            echo '<span class="LogAct">' . $xmlData->step25 . '</span><br/>';
                        }
                        if ($xmlData->step26 != "") {
                            echo '<span class="LogAct">' . $xmlData->step26 . '</span><br/>';
                        }
                        if ($xmlData->step27 != "") {
                            echo '<span class="LogAct">' . $xmlData->step27 . '</span><br/>';
                        }
                        if ($xmlData->step28 != "") {
                            echo '<span class="LogAct">' . $xmlData->step28 . '</span><br/>';
                        }
                        if ($xmlData->step29 != "") {
                            echo '<span class="LogAct">' . $xmlData->step29 . '</span><br/>';
                        }

                        if ($xmlData->step30 != "") {
                            echo '<span class="LogAct">' . $xmlData->step30 . '</span><br/>';
                        }

                        if ($xmlData->step31 != "") {
                            echo '<span class="LogAct">' . $xmlData->step31 . '</span><br/>';
                        }

                        if ($xmlData->step32 != "") {
                            echo '<span class="LogAct">' . $xmlData->step32 . '</span><br/>';
                        }
                        if ($xmlData->step33 != "") {
                            echo '<span class="LogAct">' . $xmlData->step33 . '</span><br/>';
                        }

                        if ($xmlData->step34 != "") {
                            echo '<span class="LogAct">' . $xmlData->step34 . '</span><br/>';
                        }
                        if ($xmlData->step35 != "") {
                            echo '<span class="LogAct">' . $xmlData->step35 . '</span><br/>';
                        }
                        if ($xmlData->step36 != "") {
                            echo '<span class="LogAct">' . $xmlData->step36 . '</span><br/>';
                        }
                        if ($xmlData->step37 != "") {
                            echo '<span class="LogAct">' . $xmlData->step37 . '</span><br/>';
                        }

                        if ($xmlData->step38 != "") {
                            echo '<span class="LogAct">' . $xmlData->step38 . '</span><br/>';
                        }

                        if ($xmlData->step39 != "") {
                            echo '<span class="LogAct">' . $xmlData->step39 . '</span><br/>';
                        }

                        ?>
                    </td>
                    <td style="text-align: left;">
                        <span class="LogAct">Error Code:<?php echo $xmlData->response->attributes()->errCode; ?></span><br/>
                        <span class="LogAct">Error Message:<?php echo $xmlData->response->attributes()->errMsg; ?></span><br/>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
    <?php
}else
{
    $filenameread = "_OClock_buy_now.xml";

    global $logPath;
    global $rootUrl;

    $docrootpath = $logPath["keywordPurchase"];
    $finalrootUrl = $rootUrl . "logs/keyword/purchase/";

    $data = getxmlRangeDataTime($fromdateinput, $todateinput, $filenameread, $emailuser, $docrootpath, $finalrootUrl);

    ?>

    <div id="smartTable">
        <table class="table table-bordered mytable table-hover tblwhite-lastchild" style="text-align:left"
               id="tradelogTablein">
            <thead style="">
            <tr>
                <th class="no-sort" style="width:20%;color:white;">Activity</th>
                <th class="no-sort" style="width:60%;color:white;">Steps</th>
                <th class="no-sort" style="width:20%;color:white;">Response</th>

            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($data as $xmlData) {
                ?>
                <tr style="color:<?php
                if (($xmlData->response->attributes()->errCode) == "-1") {
                    echo "green !important";
                } else {
                    echo "red !important;";
                }
                ?>">
                    <td style="text-align: left;">
                        <?php
                        if(isset($xmlData->attributes()->email))
                        {
                            ?>
                            <span class="LogAct">Email:<?php echo $xmlData->attributes()->email; ?></span><br/>
                            <?php
                        }else{
                            ?>
                            <span class="LogAct">Account Handle:<?php echo $xmlData->attributes()->account_handle; ?></span><br/>
                            <?php
                        }
                        ?>
                        <span class="LogAct">Time:<?php echo $xmlData->attributes()->timestamp; ?></span><br/>
                        <span class="LogAct">Browser:<?php echo $xmlData->attributes()->browser; ?></span><br/>
                        <span class="LogAct">User IP:<?php echo $xmlData->attributes()->userIp; ?></span><br/>
                        <span class="LogAct">Device:<?php echo $xmlData->attributes()->device; ?></span><br/>
                        <span class="LogAct">Country:<?php echo $xmlData->attributes()->country; ?></span><br/>
                        <span class="LogAct">City:<?php echo $xmlData->attributes()->city; ?></span><br/>
                    </td>
                    <td style="text-align: left;">
                        <?php
                        if ($xmlData->step != "") {
                            echo '<span class="LogAct">' . $xmlData->step . '</span><br/>';
                        }

                        if ($xmlData->step1 != "") {
                            echo '<span class="LogAct">' . $xmlData->step1 . '</span><br/>';
                        }

                        if ($xmlData->step2 != "") {
                            echo '<span class="LogAct">' . $xmlData->step2 . '</span><br/>';
                        }
                        if ($xmlData->step3 != "") {
                            echo '<span class="LogAct">' . $xmlData->step3 . '</span><br/>';
                        }

                        if ($xmlData->step4 != "") {
                            echo '<span class="LogAct">' . $xmlData->step4 . '</span><br/>';
                        }
                        if ($xmlData->step5 != "") {
                            echo '<span class="LogAct">' . $xmlData->step5 . '</span><br/>';
                        }
                        if ($xmlData->step6 != "") {
                            echo '<span class="LogAct">' . $xmlData->step6 . '</span><br/>';
                        }
                        if ($xmlData->step7 != "") {
                            echo '<span class="LogAct">' . $xmlData->step7 . '</span><br/>';
                        }
                        if ($xmlData->step8 != "") {
                            echo '<span class="LogAct">' . $xmlData->step8 . '</span><br/>';
                        }
                        if ($xmlData->step9 != "") {
                            echo '<span class="LogAct">' . $xmlData->step9 . '</span><br/>';
                        }

                        if ($xmlData->step10 != "") {
                            echo '<span class="LogAct">' . $xmlData->step10 . '</span><br/>';
                        }

                        if ($xmlData->step11 != "") {
                            echo '<span class="LogAct">' . $xmlData->step11 . '</span><br/>';
                        }

                        if ($xmlData->step22 != "") {
                            echo '<span class="LogAct">' . $xmlData->step12 . '</span><br/>';
                        }
                        if ($xmlData->step13 != "") {
                            echo '<span class="LogAct">' . $xmlData->step13 . '</span><br/>';
                        }

                        if ($xmlData->step14 != "") {
                            echo '<span class="LogAct">' . $xmlData->step14 . '</span><br/>';
                        }
                        if ($xmlData->step15 != "") {
                            echo '<span class="LogAct">' . $xmlData->step15 . '</span><br/>';
                        }
                        if ($xmlData->step16 != "") {
                            echo '<span class="LogAct">' . $xmlData->step16 . '</span><br/>';
                        }
                        if ($xmlData->step17 != "") {
                            echo '<span class="LogAct">' . $xmlData->step17 . '</span><br/>';
                        }
                        if ($xmlData->step18 != "") {
                            echo '<span class="LogAct">' . $xmlData->step18 . '</span><br/>';
                        }
                        if ($xmlData->step19 != "") {
                            echo '<span class="LogAct">' . $xmlData->step19 . '</span><br/>';
                        }

                        if ($xmlData->step20 != "") {
                            echo '<span class="LogAct">' . $xmlData->step20 . '</span><br/>';
                        }

                        if ($xmlData->step21 != "") {
                            echo '<span class="LogAct">' . $xmlData->step21 . '</span><br/>';
                        }

                        if ($xmlData->step22 != "") {
                            echo '<span class="LogAct">' . $xmlData->step22 . '</span><br/>';
                        }
                        if ($xmlData->step23 != "") {
                            echo '<span class="LogAct">' . $xmlData->step23 . '</span><br/>';
                        }

                        if ($xmlData->step24 != "") {
                            echo '<span class="LogAct">' . $xmlData->step24 . '</span><br/>';
                        }
                        if ($xmlData->step25 != "") {
                            echo '<span class="LogAct">' . $xmlData->step25 . '</span><br/>';
                        }
                        if ($xmlData->step26 != "") {
                            echo '<span class="LogAct">' . $xmlData->step26 . '</span><br/>';
                        }
                        if ($xmlData->step27 != "") {
                            echo '<span class="LogAct">' . $xmlData->step27 . '</span><br/>';
                        }
                        if ($xmlData->step28 != "") {
                            echo '<span class="LogAct">' . $xmlData->step28 . '</span><br/>';
                        }
                        if ($xmlData->step29 != "") {
                            echo '<span class="LogAct">' . $xmlData->step29 . '</span><br/>';
                        }

                        if ($xmlData->step30 != "") {
                            echo '<span class="LogAct">' . $xmlData->step30 . '</span><br/>';
                        }

                        if ($xmlData->step31 != "") {
                            echo '<span class="LogAct">' . $xmlData->step31 . '</span><br/>';
                        }

                        if ($xmlData->step32 != "") {
                            echo '<span class="LogAct">' . $xmlData->step32 . '</span><br/>';
                        }
                        if ($xmlData->step33 != "") {
                            echo '<span class="LogAct">' . $xmlData->step33 . '</span><br/>';
                        }

                        if ($xmlData->step34 != "") {
                            echo '<span class="LogAct">' . $xmlData->step34 . '</span><br/>';
                        }
                        if ($xmlData->step35 != "") {
                            echo '<span class="LogAct">' . $xmlData->step35 . '</span><br/>';
                        }
                        if ($xmlData->step36 != "") {
                            echo '<span class="LogAct">' . $xmlData->step36 . '</span><br/>';
                        }


                        ?>
                    </td>
                    <td style="text-align: left;">
                        <span class="LogAct">Error Code:<?php echo $xmlData->response->attributes()->errCode; ?></span><br/>
                        <span class="LogAct">Error Message:<?php echo $xmlData->response->attributes()->errMsg; ?></span><br/>
                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div>
    <?php
}
?>

<script>

    var table=$('#tradelogTablein').DataTable({
        "bInfo": true,
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [],
        "columnDefs": [{
            orderable: false,
            targets: "no-sort",
        }]
    });

    $('#tradetextSearch').keyup(function () {
        table.search( this.value ).draw();
    } );
</script>

