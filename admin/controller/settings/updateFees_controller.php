<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/fees_model.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

 $fieldtoupdate = $_POST["fieldToUpdate"];
$fieldValue = $_POST["updateValue"];

if ($fieldValue == "" || $fieldValue == "NaN") {
    $returnArr["errCode"] = "INVALID";
    $returnArr["errMsg"] = "Please Enter Valid Data!!";
    echo(json_encode($returnArr));
    exit;
}
if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    switch ($fieldtoupdate) {
        case 'currentpayout':
            $result = addUpdateKeywoSearchFees("current_max_payout", $fieldValue, $connSearch);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Fees Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'maxiplimit':
            $result = addUpdateKeywoSearchFees("IPMaxLimit", $fieldValue, $connSearch);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Fees Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'defaultApp':
            $result = addUpdateKeywoSearchFees("default_app_id", $fieldValue, $connSearch);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "App Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'defaultMode':
            $result = addUpdateKeywoSearchFees("default_system_mode", $fieldValue, $connSearch);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Mode Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'searchaffnewtype':
            $result = addUpdateKeywoKeywordsFees("search_affiliate_percent", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;
        case 'tradeFees':
            $result = addUpdateKeywoKeywordsFees("trading_commision_percent", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'withdrawalFees':
            $result = addUpdateKeywoKeywordsFees("withdrawal_fees", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'TransferFees':
            $result = addUpdateKeywoKeywordsFees("transferFees", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'RenewalFees':
            $result = addUpdateKeywoKeywordsFees("kwd_renewal_fees_per_year", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'kwdReferralFees':
            $result = addUpdateKeywoKeywordsFees("affiliate_earning_percent", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'firstBuyCashback':
            $result = addUpdateKeywoKeywordsFees("first_buy_percent", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'minimumWithdrawalFees':
            $result = addUpdateKeywoKeywordsFees("minimum_withdrawal_amount", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'stMaintainanceFees':
            $result = addUpdateKeywoKeywordsFees("searchtrade_maintenance_percent", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'first_confirmation_minutes':
            $result = addUpdateKeywoKeywordsFees("first_confirmation_minutes", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'final_confirmation_minutes':
            $result = addUpdateKeywoKeywordsFees("final_confirmation_minutes", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'payment_confirmation_numbers':
            $result = addUpdateKeywoKeywordsFees("payment_confirmation_numbers", $fieldValue, $connKeywords);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'blockedPostThreshold':

            $result = setBlockUserThreshold($fieldValue);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Threshold !!";
                echo(json_encode($returnArr));
            }
            break;


        case 'cashout_automode_amount_limit':
            $result = addUpdateKeywoKeywordsFees("cashout_automode_amount_limit", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'cashout_manual_mode_amount_uamDepartment':
            $result = addUpdateKeywoKeywordsFees("cashout_manual_mode_amount_uamDepartment", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;


        case 'cashout_manual_mode_amount_mmDepartment':
            $result = addUpdateKeywoKeywordsFees("cashout_manual_mode_amount_mmDepartment", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'cashout_manual_mode_amount_smDepartment':
            $result = addUpdateKeywoKeywordsFees("cashout_manual_mode_amount_smDepartment", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'cashout_packing_list_duration':
            $result = addUpdateKeywoKeywordsFees("cashout_packing_list_duration", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'post_sharePercentage':
            $result = addUpdateKeywoKeywordsFees("post_share_percent", $fieldValue, $connKeywords);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Fees !!";
                echo(json_encode($returnArr));
            }
            break;



    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Updating Fees !!";
    echo(json_encode($returnArr));
}
