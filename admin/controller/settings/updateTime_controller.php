<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/activeTimeSocial.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);


$activeTime = cleanQueryParameter($connSearch, cleanXSS($_POST['activeTime']));
$idleTime = cleanQueryParameter($connSearch, cleanXSS($_POST['idleTime']));


if ($activeTime == "") {
    $returnArr["errCode"] = "INVALID";
    $returnArr["errMsg"] = "Please Enter Active Time!";
    echo(json_encode($returnArr));
    exit;
}

if ($idleTime == "") {
    $returnArr["errCode"] = "INVALID";
    $returnArr["errMsg"] = "Please Enter Idle Time!";
    echo(json_encode($returnArr));
    exit;
}


if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    $result=setActiveTimeSocial($activeTime,$idleTime);
    if(noError($result))
    {
        $result=getActiveTimeSocial();
        $returnArr["errCode"] = "-1";
        $returnArr["errMsg"] =$result["errMsg"];
    }else{
        $returnArr["errCode"] = $result["errCode"];
        $returnArr["errMsg"] =$result["errMsg"];
    }
    echo(json_encode($returnArr));
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Updating Fees !!";
    echo(json_encode($returnArr));
}
