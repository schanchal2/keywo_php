<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/fees_model.php');
//end other


$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$field=cleanQueryParameter($connKeywords, cleanXSS($_POST["field"]));
$status=cleanQueryParameter($connKeywords, cleanXSS($_POST["status"]));
$operationType=cleanQueryParameter($connKeywords, cleanXSS($_POST["operationType"]));

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    switch ($operationType) {
        case 'update':

            $result=two_faOnOff($field,$status,$connKeywords);
           if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Status Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating Status !!";
                echo(json_encode($returnArr));
            }
            break;

    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Updating KYC Slab !!";
    echo(json_encode($returnArr));
}
