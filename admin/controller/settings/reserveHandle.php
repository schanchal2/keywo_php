<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/reserveHandleModel.php');
//end other

/*********************** get user Permissions ********************/
$docrootpath=$docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp=mypermissions($largest);
//printArr($myp);
/*********************** get user Permissions ********************/


$result=getHandlesList("reserved")["errMsg"];

if($result["failed"]=="handle not found") {
    unset($result["failed"]);
}



?>

<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive"  id="reserveHandleTable">
        <thead>
        <tr>
            <th class="no-sort dtTableHeadPadding"> Handle ID</th>
            <?php
            if((in_array("write",$myp))) {
                ?>
            <th class="no-sort dtTableHeadPadding"> Assigne to Email</th>

                <th class="no-sort dtTableHeadPadding"> Action</th>
                <?php
            }
            ?>
        </tr>

        </thead>
        <tbody>

        <?php



            foreach ($result as $data) {
                ?>
                <tr>
                    <td><?php echo "@" . $data["handle"]; ?></td>
                    <?php
                    if((in_array("write",$myp))) {
                        ?>
                    <td>
                        <span style="color:#d2d2d2">Click to assign to user &nbsp;</span>
                        <button type="button" onclick="addHandletoProcess('<?php echo $data["handle"]; ?>')"
                                class="btn btn-default" data-toggle="modal" data-target="#assignHandleModal">
                            <i style="color:#0299be" class="fa fa-plus-circle fa-lg" aria-hidden="true"></i></button>
                    </td>

                        <td>
                            <button type="button" onclick="editHandletoProcess('<?php echo $data["handle"]; ?>')"
                                    class="btn btn-default"
                                    data-toggle="modal" data-target="#editHandleModal">
                                <i class="fa fa-pencil"></i></button>

                            <button type="button" class="btn btn-default"
                                    onclick="deleteHandleData('<?php echo $data["handle"]; ?>','reserved')">
                                <i class="fa fa-trash-o"></i></button>
                        </td>
                        <?php
                    }
                    ?>


                </tr>
                <?php
            }

        ?>


        </tbody>

    </table>
</div>





<script>

    $('#reserveHandleTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": false,
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });




</script>
