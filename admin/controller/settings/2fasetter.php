<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/fees_model.php');
//end other
//end other
/*********************** get user Permissions ********************/
$docrootpath = $docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp = mypermissions($largest);
/*********************** get user Permissions ********************/
$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
$result = get2fA_status($connKeywords)["errMsg"];
?>

<table class="table text-center table-responsive table-bordered">
    <thead style="height:50px;width:70%">
    <tr>
        <th style="text-align:left;width:70%">List Of Activity Wherein Security Prompt is Applicable</th>
        <?php
        if ((in_array("write", $myp))) {
            ?>
            <th style="text-align:left;width:30%">Change Mode (ON/OFF)</th>
        <?php } else {
            ?>
            <th style="text-align:left;width:30%">Status</th>
            <?php
        } ?>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Place Bid</td>

        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['place_bid'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('place_bid','<?= $result['place_bid']; ?>')"
                                              id="place_bid" class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['place_bid'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Edit Bid</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['edit_bid'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('edit_bid','<?= $result['edit_bid']; ?>')" id="edit_bid"
                                              class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['edit_bid'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Cancel Bid</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['cancel_bid'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('cancel_bid','<?= $result['cancel_bid']; ?>')"
                                              id="cancel_bid" class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['cancel_bid'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Buy Now</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['buy_now'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('buy_now','<?= $result['buy_now']; ?>')" id="buy_now"
                                              class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['buy_now'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Set Ask</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['set_ask'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('set_ask','<?= $result['set_ask']; ?>')" id="set_ask"
                                              class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['set_ask'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Edit Ask</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['edit_ask'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('edit_ask','<?= $result['edit_ask']; ?>')" id="edit_ask"
                                              class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['edit_ask'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Cancel/Delete Ask</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['cancel_ask'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('cancel_ask','<?= $result['cancel_ask']; ?>')"
                                              id="cancel_ask" class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['cancel_ask'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Accept Bid</td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['accept_bid'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('accept_bid','<?= $result['accept_bid']; ?>')"
                                              id="accept_bid" class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['accept_bid'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Send <?= $adminCurrency; ?></td>
        <?php
        if ((in_array("write", $myp))) {
        ?>
        <td style="text-align: center"><input type="checkbox" <?php if ($result['send_itd'] == 1) {
                echo "checked";
            } ?> data-toggle="toggle" onchange="change2faStatus('send_itd','<?= $result['send_itd']; ?>')" id="send_itd"
                                              class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['send_itd'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    <tr>
        <td>Sell <?= $adminCurrency; ?>/ Cashouts[USD/Bitcoin]</td>
        <?php
        if ((in_array("write", $myp))) {
            ?>
            <td style="text-align: center"><input type="checkbox" <?php if ($result['cashout'] == 1) {
                    echo "checked";
                } ?> data-toggle="toggle" onchange="change2faStatus('cashout','<?= $result['cashout']; ?>')"
                                                  id="cashout"
                                                  class="tfa_on_of" data-size="mini"></td>
        <?php } else {

            if ($result['cashout'] == 1) {
                $stat = "On";

            } else {
                $stat = "Off";

            }


            ?>
            <td style="text-align: center">
                <?php echo $stat; ?>
            </td>
            <?php
        }
        ?>
    </tr>

    </tbody>
</table>
<script>
    $(function () {
        $('.tfa_on_of').bootstrapToggle();
    });


    function change2faStatus(field, status) {

        if (status == 0) {
            status = 1;
        } else if (status == 1) {
            status = 0;
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "../../controller/settings/2faSetter_crud.php",
            data: {
                field: field,
                status: status,
                operationType: "update"
            },
            beforeSend: function () {
                $('#loadng-image').show();
            },
            success: function (data) {
                $('#loadng-image').hide();
                get2faAuthSetter();

            },
            error: function () {
                console.log("fail");
            }

        });

    }
</script>