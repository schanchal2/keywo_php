<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/reserveHandleModel.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

if(isset($_POST["userName"]))
{
    $userName = strtolower(cleanQueryParameter($connSearch, cleanXSS($_POST["userName"])));
}else{
    $userName = "";
}

if(isset($_POST["userEmail"]))
{
    $userEmail = strtolower(cleanQueryParameter($connSearch, cleanXSS($_POST["userEmail"])));
}else{
    $userEmail = "";
}

if(isset($_POST["handleID"]))
{
    $handleID = strtolower(cleanQueryParameter($connSearch, cleanXSS($_POST["handleID"])));
}else{
    $handleID = "";
}


$action = cleanQueryParameter($connSearch, cleanXSS($_POST["action"]));

if(isset($_POST["oldhandleID"]))
{
    $oldhandleID= strtolower(cleanQueryParameter($connSearch, cleanXSS($_POST["oldhandleID"])));
}else{
    $oldhandleID = "";
}




if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    switch ($action) {
        case 'assign':


            if($userName=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Name Required!";
                echo(json_encode($returnArr));
                exit;
            }

            if(!ctype_alnum($userName))
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Name is not Valid!";
                echo(json_encode($returnArr));
                exit;
            }


            if($userEmail=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Email Address is Required!";
                echo(json_encode($returnArr));
                exit;
            }else {
                if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
                    $returnArr["errCode"] = "5";
                    $returnArr["errMsg"] = "Please enter valid email!!";
                    echo(json_encode($returnArr));
                    exit;
                }


            }
            $result=assignReservedHandle($handleID,$userName,$userEmail);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Handle Assigned Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Assigning Handle !!";
                echo(json_encode($returnArr));
            }
            break;

        case 'add':


            if($handleID=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Handle ID is Required!";
                echo(json_encode($returnArr));
                exit;
            }


            if(strlen($handleID)<6)
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Minimum Handle length should be more than 6 character!";
                echo(json_encode($returnArr));
                exit;
            }

            $result=addReservedHandle($handleID);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Handle Added Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = $result["errMsg"];
                echo(json_encode($returnArr));
            }
            break;


        case 'delete':


            if($handleID=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Handle ID is Required!";
                echo(json_encode($returnArr));
                exit;
            }
           // delete handle code
            $result=deleteReservedHandle($handleID);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Handle Deleted Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = $result["errMsg"];
                echo(json_encode($returnArr));
            }
            break;


        case 'editHandleName':


            if($handleID=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Handle ID is Required!";
                echo(json_encode($returnArr));
                exit;
            }

            if(strlen($handleID)<6)
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Minimum Handle length should be more than 6 character!";
                echo(json_encode($returnArr));
                exit;
            }

            // edit non assigned  handle code
            $result=editReservedHandle("2",$oldhandleID,"","",$handleID);

            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Handle Named Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = $result["errMsg"];
                echo(json_encode($returnArr));
            }
            break;


        case 'editHandleData':


            if($handleID=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "Handle ID is Required!";
                echo(json_encode($returnArr));
                exit;
            }


            if($userName=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Name Required!";
                echo(json_encode($returnArr));
                exit;
            }

            if(!ctype_alnum($userName))
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Name is not Valid!";
                echo(json_encode($returnArr));
                exit;
            }


            if($userEmail=="")
            {
                $returnArr["errCode"] = 5;
                $returnArr["errMsg"] = "User Email Address is Required!";
                echo(json_encode($returnArr));
                exit;
            }else {
                if (!filter_var($userEmail, FILTER_VALIDATE_EMAIL)) {
                    $returnArr["errCode"] = "5";
                    $returnArr["errMsg"] = "Please enter valid email!!";
                    echo(json_encode($returnArr));
                    exit;
                }


            }
            // edit assigned  handle code
            $result=editReservedHandle("1",$handleID,$userName,$userEmail,"");
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "Handle Data Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = $result["errMsg"];
                echo(json_encode($returnArr));
            }
            break;


    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Unauthorised User !!";
    echo(json_encode($returnArr));
}
