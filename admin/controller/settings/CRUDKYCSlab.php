<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/kyc_slab.php');
//end other


$connSearch = createDBConnection("dbsearch");
noError($connSearch) ? $connSearch = $connSearch["connection"] : checkMode($connSearch["errMsg"]);

$operationType=cleanQueryParameter($connSearch, cleanXSS($_POST["operationType"]));

$slabId=isset($_POST["slabId"])?cleanQueryParameter($connSearch, cleanXSS($_POST["slabId"])):"";
$sendLimit=isset($_POST["sendLimit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["sendLimit"])):"";
$receiveLimit=isset($_POST["receiveLimit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["receiveLimit"])):"";
$withdrawalLimit=isset($_POST["withdrawalLimit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["withdrawalLimit"])):"";
$intractionLimit=isset($_POST["intractionLimit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["intractionLimit"])):"";
$depositLimit=isset($_POST["depositLimit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["depositLimit"])):"";
$yearly_send_fund_limit=isset($_POST["yearly_send_fund_limit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["yearly_send_fund_limit"])):"";
$yearly_withdrawal_limit=isset($_POST["yearly_withdrawal_limit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["yearly_withdrawal_limit"])):"";
$yearly_income_limit=isset($_POST["yearly_income_limit"])?cleanQueryParameter($connSearch, cleanXSS($_POST["yearly_income_limit"])):"";
$transaction_fees=isset($_POST["transaction_fees"])?cleanQueryParameter($connSearch, cleanXSS($_POST["transaction_fees"])):"";




if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    switch ($operationType) {
        case 'update':

            $result=updateKycSlabLevel($slabId,$sendLimit,$receiveLimit,$withdrawalLimit,$intractionLimit,$depositLimit,$yearly_send_fund_limit,$yearly_withdrawal_limit,$yearly_income_limit,$transaction_fees,$connSearch);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = "KYC Slab Updated Successfully !!";
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In Updating KYC Slab !!";
                echo(json_encode($returnArr));
            }
            break;


        case 'getSlabbyId':

            $result=getKYCSlabSettingAdminbyID($slabId,$connSearch);
            if ($result["errCode"] == -1) {
                $returnArr["errCode"] = -1;
                $returnArr["errMsg"] = $result["errMsg"];
                echo(json_encode($returnArr));
            } else {
                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Error In getting KYC Slab !!";
                echo(json_encode($returnArr));
            }
            break;



    }
} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Updating KYC Slab !!";
    echo(json_encode($returnArr));
}
