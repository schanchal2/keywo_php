<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/keywordslabModel.php');
require_once('../../model/currency/currency_rate_model.php');
//end other



$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);

$result=get_all_slab($connKeywords);
$result=$result["errMsg"];
$resultCnt=count($result);
$resultdata=get_currentActiveSlab($connKeywords);
$currentSlab=$resultdata["errMsg"]["id"];

$currentUsdRate=getCurrencyRate("usd");//usd=itd

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

?>

    <div class="" id="slabListTable">
        <table class="table text-center table-responsive table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th> Start Limit </th>
                <th> End Limit </th>
                <th> Price (<?= $adminCurrency; ?>)</th>
                <th> Price (BTC @CR)</th>
                <th> Keywords Sold </th>
                <th> Status </th>
            </tr>
            </thead>
            <tbody>
            <?php
            if($resultCnt<=0)
            {
                ?>
                <tr>
                <th colspan="7" style="background: white;" class="text-center">No Slab List Generated Yet</th>
                </tr>

                <?php
            }else{
            $count=0;
            foreach($result as $value) {

                ?>
                <tr class="<?php

                if($currentSlab==$value["id"])
                {
                    echo "current-slab";
                }

                ?>">
                    <td><?php echo $count=$count+1; ?></td>
                    <td><?php echo $value["start_limit"]; ?></td>
                    <td><?php echo $value["end_limit"]; ?></td>
                    <td><?php echo number_format($value["ITD_Price"], 2); ?> </td>
                    <td><?php echo number_format(getAmountInCurrentBtcRate($value["ITD_Price"],$currentUsdRate),8); ?> </td>
                    <td><?php echo $value["kwd_sold"]; ?></td>
                    <td style="width: 18%;"><?php echo $value["kwd_status"]; ?></td>

                </tr>
                <?php
            }
            }
                    ?>
            </tbody>
        </table>
    </div>
<?php

} else {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Error In Connection !!";
    echo(json_encode($returnArr));
}


