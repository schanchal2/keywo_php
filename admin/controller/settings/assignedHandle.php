<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/reserveHandleModel.php');

//end other
/*********************** get user Permissions ********************/
$docrootpath=$docRootAdmin;
require_once "{$docrootpath}model/acl/checkAccess.php";
$myp=mypermissions($largest);
//printArr($myp);
/*********************** get user Permissions ********************/

$result=getHandlesList("assigned")["errMsg"];
if($result["failed"]=="handle not found") {
    unset($result["failed"]);
}
?>

<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive"  id="assignedHandleTable">
        <thead>
        <tr>
            <th class="no-sort dtTableHeadPadding"> Name </th>
            <th class="no-sort dtTableHeadPadding"> Handle ID</th>
            <th class="no-sort dtTableHeadPadding"> Assigned to</th>
            <?php
            if((in_array("write",$myp))) {
                ?>
                <th class="no-sort dtTableHeadPadding"> Action</th>
                <?php
            }
            ?>
        </tr>

        </thead>
        <tbody>
        <?php

        foreach($result as $data) {
            ?>
            <tr>
                <td><?php echo $data["username"]; ?></td>
                <td>@<?php echo $data["handle"]; ?></td>
                <td>
                    <?php echo $data["useremail"]; ?>
                </td>
                <?php
                if((in_array("write",$myp))) {
                    ?>
                    <td>
                        <button type="button"
                                onclick="editAssignedHandletoProcess('<?php echo $data["handle"]; ?>','<?php echo $data["username"]; ?>','<?php echo $data["useremail"]; ?>')"
                                class="btn btn-default"
                                data-toggle="modal" data-target="#editHandleModalassigned">
                            <i class="fa fa-pencil"></i></button>

                        <button type="button" class="btn btn-default"
                                onclick="deleteHandleData('<?php echo $data["handle"]; ?>','assigned')">
                            <i class="fa fa-trash-o"></i></button>
                    </td>
                    <?php
                }
                ?>


            </tr>
            <?php
        }
        ?>


        </tbody>

    </table>
</div>


<script>

    $('#assignedHandleTable').DataTable({
        "sDom": 'rt<"bottom"flp><"bottom"i><"clear">',
        "ordering": false,
        columnDefs: [{
            orderable: false,
            targets: "no-sort"
        }]

    });



</script>
