<?php
session_start();

//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once('../../model/settings/keywordslabModel.php');
//end other


global $adminRoot;

$dbkeywords = createDBConnection("dbkeywords");
noError($dbkeywords) ? $dbkeywords = $dbkeywords["connection"] : checkMode($dbkeywords["errMsg"]);

$limitstart = cleanQueryParameter($dbkeywords, cleanXSS($_POST['limitstart']));
$limitend = cleanQueryParameter($dbkeywords, cleanXSS($_POST['limitend']));
$pricebtc = cleanQueryParameter($dbkeywords, cleanXSS($_POST['btc']));
$priceusd = cleanQueryParameter($dbkeywords, cleanXSS($_POST['usd']));
$priceitd = cleanQueryParameter($dbkeywords, cleanXSS($_POST['itd']));


if ($_POST['limitstart'] == '') {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Enter Start Limit.";
    echo(json_encode($returnArr));
    exit;
}


if ($_POST['limitend'] == '') {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Enter End Limit.";
    echo(json_encode($returnArr));
    exit;
}

if ($pricebtc != '') {
    $price = $pricebtc;
    $priceType = "btc";
} elseif ($priceusd != '') {
    $price = $priceusd;
    $priceType = "usd";
} elseif($priceitd != ''){
    $price = $priceitd;
    $priceType = "itd";
}else
    {
    $returnArr["errCode"] = 2;
    $returnArr["errMsg"] = "Price is Mandatory Field";
    echo(json_encode($returnArr));
    exit;

}


if ($priceType == "btc") {

    $result = get_first_purchase_slabs_MAX_ID($dbkeywords);


    $id = $result["errMsg"]["id"];

    if (noError($result)) {

        $result = get_last_first_purchase_slabs_from_MAX_ID($id, $dbkeywords);
        $start_limit_old = $result["errMsg"]["start_limit"];
        $end_limit_old = $result["errMsg"]["end_limit"];
        $newstartlimit = $end_limit_old + 1;
        $price_old = $result["errMsg"]["BTC_price"];
        $pricenew = $price_old - 0.01;

        if ($limitstart == $newstartlimit) {

            if ($limitend > $limitstart) {


                $query = "INSERT INTO first_purchase_slabs(start_limit,end_limit,BTC_price,kwd_status,kwd_sold,sold_voucher,USD_price,ITD_Price) VALUES('" . $limitstart . "','" . $limitend . "','" . $price . "','open',0,0,0,0);";
                $result = runQuery($query, $dbkeywords);


                if (noError($result)) {


                    $result = get_first_purchase_slabs_MAX_ID($dbkeywords);
                    $currentActiveSlab = $result["errMsg"]["id"];


                    if (noError($result)) {

                        $noOfVouchers = $limitend - $limitstart;
                        $noOfVouchers = $noOfVouchers + 1;

                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"] = "Slab Generated Successful.";

                    } else {

                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"] = "Error in getting slab maximum ID.";
                    }

                } else {

                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Error in inserting slab.";
                }

            } else {

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Enter proper end limit it should be greater than current start limit.";
            }
        } else {

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Enter valid Start limit it should be greater than old end limit.";
        }

    } else {


        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Error in getting slab maximum ID.";
    }


}

if ($priceType == "usd") {

    $result = get_first_purchase_slabs_MAX_ID($dbkeywords);


    $id = $result["errMsg"]["id"];

    if (noError($result)) {

        $result = get_last_first_purchase_slabs_from_MAX_ID($id, $dbkeywords);
        $start_limit_old = $result["errMsg"]["start_limit"];
        $end_limit_old = $result["errMsg"]["end_limit"];
        $newstartlimit = $end_limit_old + 1;
        $price_old = $result["errMsg"]["USD_price"];
        $pricenew = $price_old - 1;

        if ($limitstart == $newstartlimit) {

            if ($limitend > $limitstart) {


                $query = "INSERT INTO first_purchase_slabs(start_limit,end_limit,BTC_price,kwd_status,kwd_sold,sold_voucher,USD_price,ITD_Price) VALUES('" . $limitstart . "','" . $limitend . "',0,'open',0,0,'" . $price . "',0);";
                $result = runQuery($query, $dbkeywords);


                if (noError($result)) {


                    $result = get_first_purchase_slabs_MAX_ID($dbkeywords);
                    $currentActiveSlab = $result["errMsg"]["id"];


                    if (noError($result)) {

                        $noOfVouchers = $limitend - $limitstart;
                        $noOfVouchers = $noOfVouchers + 1;


                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"] = "Slab Generated Successful.";

                    } else {

                        $returnArr["errCode"] = 2;
                        $returnArr["errMsg"] = "Error in getting slab maximum ID.";
                    }

                } else {

                    $returnArr["errCode"] = 2;
                    $returnArr["errMsg"] = "Error in inserting slab.";
                }

            } else {

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Enter proper end limit it should be greater than current start limit.";
            }
        } else {

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Enter valid Start limit it should be greater than old end limit.";
        }

    } else {


        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Error in getting slab maximum ID.";
    }


}


if ($priceType == "itd") {

    $result = get_first_purchase_slabs_MAX_ID($dbkeywords);


    $id = $result["errMsg"]["id"];

    if (noError($result)) {

        $result = get_last_first_purchase_slabs_from_MAX_ID($id, $dbkeywords);
        $start_limit_old = $result["errMsg"]["start_limit"];
        $end_limit_old = $result["errMsg"]["end_limit"];
        $newstartlimit = $end_limit_old + 1;
        $price_old = $result["errMsg"]["ITD_Price"];
        $pricenew = $price_old - 1;

        if ($limitstart == $newstartlimit) {

            if ($limitend > $limitstart) {


       if($pricenew==$price) {
           $query = "INSERT INTO first_purchase_slabs(start_limit,end_limit,BTC_price,kwd_status,kwd_sold,sold_voucher,USD_price,ITD_Price) VALUES('" . $limitstart . "','" . $limitend . "',0,'open',0,0,0,'" . $price . "');";
           $result = runQuery($query, $dbkeywords);


           if (noError($result)) {


               $result = get_first_purchase_slabs_MAX_ID($dbkeywords);
               $currentActiveSlab = $result["errMsg"]["id"];


               if (noError($result)) {

                   $noOfVouchers = $limitend - $limitstart;
                   $noOfVouchers = $noOfVouchers + 1;


                   $returnArr["errCode"] = -1;
                   $returnArr["errMsg"] = "Slab Generated Successful.";

               } else {

                   $returnArr["errCode"] = 2;
                   $returnArr["errMsg"] = "Error in getting slab maximum ID.";
               }

           } else {

               $returnArr["errCode"] = 2;
               $returnArr["errMsg"] = "Error in inserting slab.";
           }
       }else{
           $returnArr["errCode"] = 2;
           $returnArr["errMsg"] = "Price Should be less than previous slab.(new_slabprice=old_slabprice-1)";
       }

            } else {

                $returnArr["errCode"] = 2;
                $returnArr["errMsg"] = "Enter proper end limit it should be greater than current start limit.";
            }
        } else {

            $returnArr["errCode"] = 2;
            $returnArr["errMsg"] = "Enter valid Start limit it should be greater than old end limit.";
        }

    } else {


        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = "Error in getting slab maximum ID.";
    }


}


echo(json_encode($returnArr));


?>
  