<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
require_once('../../core/errorMap.php');
require_once('../../helpers/stringHelper.php');
$connAdmin = createDBConnection("dbkeywords");
$con = $connAdmin["connection"];

try {

    if (!$con) {
        die("connection object not created: " . mysqli_error($con));
    }

    //Getting records (listAction)

    if ($_GET["action"] == "list") {
        //Get record count
        $result = mysqli_query($con, "SELECT COUNT(*) AS RecordCount FROM blocked_ip;");
        $row = mysqli_fetch_array($result);
        $recordCount = $row['RecordCount'];

        //Get records from database
        $result = mysqli_query($con, "SELECT * FROM blocked_ip ORDER BY id desc LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
        while ($row = mysqli_fetch_array($result)) {
            $rows[] = $row;
        }
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $rows;
        print json_encode($jTableResult);
    } //Creating a new record (createAction)
    else if ($_GET["action"] == "create") {

        $ip_address= strtolower(cleanQueryParameter($con, cleanXSS($_POST["addip"])));
        $reason_blocked= strtolower(cleanQueryParameter($con, cleanXSS($_POST["addreason"])));


        $result = mysqli_query($con, "SELECT * FROM blocked_ip WHERE ip_address = '" . $ip_address . "';");
        $count = mysqli_num_rows($result);

        if (trim($ip_address) != "") {
                if ($count < 1) {

                    if (filter_var($ip_address, FILTER_VALIDATE_IP) === false) {
                        $jTableResult = array();
                        $jTableResult['Result'] = "ERROR";
                        $jTableResult['Message'] = "This IP Address not valid!";
                        print json_encode($jTableResult);
                        exit;
                    }

                    if ($reason_blocked=="") {
                        $jTableResult = array();
                        $jTableResult['Result'] = "ERROR";
                        $jTableResult['Message'] = "Please add reason to block IP address.";
                        print json_encode($jTableResult);
                        exit;
                    }

                    //Insert record into database
                    $result = mysqli_query($con, "INSERT INTO blocked_ip(ip_address,reason_blocked) VALUES('" .$ip_address . "','" .$reason_blocked . "')");
                     if($result)
                     {
                         global $docRoot;
                         $banip = "Deny from {$ip_address}\n";
                         $file=$docRoot.".htaccess";
                        // file_put_contents($file ,$banip, FILE_APPEND);
                     }
                    //Get last inserted record (to return to jTable)
                    //$last_id = mysqli_insert_id($conn);
                    $result = mysqli_query($con, "SELECT * FROM blocked_ip WHERE id = LAST_INSERT_ID();");
                    $row = mysqli_fetch_array($result);

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    $jTableResult['Record'] = $row;
                    print json_encode($jTableResult);

                } else {
                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "ERROR";
                    $jTableResult['Message'] = "IP Address already blocked .";
                    print json_encode($jTableResult);


                }
        } else {
            //Return result to jTable
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Blank IP Address not allowed";
            print json_encode($jTableResult);

        }


    } //Updating a record (updateAction)
    else if ($_GET["action"] == "update") {


        $ip_address= strtolower(cleanQueryParameter($con, cleanXSS($_POST["Editip"])));
        $reason_blocked= strtolower(cleanQueryParameter($con, cleanXSS($_POST["Editreason"])));

        $result = mysqli_query($con, "SELECT * FROM blocked_ip WHERE ip_address = '" . $ip_address . "';");

        $count = mysqli_num_rows($result);
        if (trim($ip_address) != "") {
            if (filter_var($ip_address, FILTER_VALIDATE_IP) === false) {
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "This IP Address not valid!";
                print json_encode($jTableResult);
                exit;
            }

            if ($reason_blocked=="") {
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "Please add reason to block IP Address.";
                print json_encode($jTableResult);
                exit;
            }

                if ($count < 1) {
                    //Update record in database
                    $result = mysqli_query($con, "UPDATE blocked_ip SET ip_address = '" . $ip_address . "',reason_blocked = '" . $reason_blocked . "' where id='" . $_POST["id"] . "';");

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    print json_encode($jTableResult);

                } else {
                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "ERROR";
                    $jTableResult['Message'] = "IP Address already Blocked";
                    print json_encode($jTableResult);


                }

        } else {
            //Return result to jTable
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Blank IP Address not allowed";
            print json_encode($jTableResult);

        }

    } //Deleting a record (deleteAction)
    else if ($_GET["action"] == "delete") {

        //Delete from database
        $result = mysqli_query($con, "SELECT * FROM blocked_ip WHERE id = '" . $_POST["id"] . "';");
        $data=mysqli_fetch_assoc($result);
        $ip_address=$data["ip_address"];
        if($ip_address=="")
        {
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Error Deleting IP Address.";
            print json_encode($jTableResult);
            exit;
        }

        $result = mysqli_query($con, "DELETE FROM blocked_ip WHERE id = " . $_POST["id"] . ";");
        if($result)
        {
            global $docRoot;
            $banip = "Deny from {$ip_address}\n";
            $file=$docRoot.".htaccess";
            $resultdata=file_get_contents($file);
            $bodycont = str_replace($banip,"",$resultdata);
           // file_put_contents($file ,$bodycont);
        }
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        print json_encode($jTableResult);
    }

    //Close database connection
    mysqli_close($con);

}
catch (Exception $ex) {
    //Return error message
    $jTableResult = array();
    $jTableResult['Result'] = "ERROR";
    $jTableResult['Message'] = $ex->getMessage();
    print json_encode($jTableResult);
}
?>