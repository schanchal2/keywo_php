<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
require_once('../../core/errorMap.php');
require_once('../../helpers/stringHelper.php');
$connAdmin = createDBConnection("dbsearch");
$con = $connAdmin["connection"];

try {

    if (!$con) {
        die("connection object not created: " . mysqli_error($con));
    }

    //Getting records (listAction)

    if ($_GET["action"] == "list") {
        //Get record count
        $result = mysqli_query($con, "SELECT COUNT(*) AS RecordCount FROM blocked_domains;");
        $row = mysqli_fetch_array($result);
        $recordCount = $row['RecordCount'];

        //Get records from database
        $result = mysqli_query($con, "SELECT * FROM blocked_domains ORDER BY id desc LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
        while ($row = mysqli_fetch_array($result)) {
            $rows[] = $row;
        }
        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $rows;
        print json_encode($jTableResult);
    } //Creating a new record (createAction)
    else if ($_GET["action"] == "create") {


        $domaintoBlock = strtolower(cleanQueryParameter($con, cleanXSS($_POST["addDomain"])));

        $result = mysqli_query($con, "SELECT * FROM blocked_domains WHERE domain_name = '" . $domaintoBlock . "';");
        $count = mysqli_num_rows($result);

        if (trim($domaintoBlock) != "") {
            if (strlen($_POST["addDomain"]) <= 50) {
                if ($count < 1) {

                    //Insert record into database
                    $result = mysqli_query($con, "INSERT INTO blocked_domains(domain_name) VALUES('" .$domaintoBlock . "')");
                    //Get last inserted record (to return to jTable)
                    //$last_id = mysqli_insert_id($conn);
                    $result = mysqli_query($con, "SELECT * FROM blocked_domains WHERE id = LAST_INSERT_ID();");
                    $row = mysqli_fetch_array($result);

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    $jTableResult['Record'] = $row;
                    print json_encode($jTableResult);

                } else {
                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "ERROR";
                    $jTableResult['Message'] = "Domain Name already present in database";
                    print json_encode($jTableResult);


                }
            } else {
                //Return result to jTable
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "Domain Name must be less than 50 characters";
                print json_encode($jTableResult);
            }
        } else {
            //Return result to jTable
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Blank Domian Name not allowed";
            print json_encode($jTableResult);

        }


    } //Updating a record (updateAction)
    else if ($_GET["action"] == "update") {


        $domaintoBlock = strtolower(cleanQueryParameter($con, cleanXSS($_POST["EditDomain"])));
        //echo $query="SELECT * FROM blocked_domains WHERE domain_name = {$_POST["domain_name"]};";
        $result = mysqli_query($con, "SELECT * FROM blocked_domains WHERE domain_name = '" . $domaintoBlock . "';");

        $count = mysqli_num_rows($result);
        if (trim($domaintoBlock) != "") {
            if (strlen($_POST["EditDomain"]) <= 50) {
                if ($count < 1) {
                    //Update record in database
                    $result = mysqli_query($con, "UPDATE blocked_domains SET domain_name = '" . $domaintoBlock . "' where id='" . $_POST["id"] . "';");

                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "OK";
                    print json_encode($jTableResult);

                } else {
                    //Return result to jTable
                    $jTableResult = array();
                    $jTableResult['Result'] = "ERROR";
                    $jTableResult['Message'] = "Domain Name already present in database";
                    print json_encode($jTableResult);


                }
            } else {
                //Return result to jTable
                $jTableResult = array();
                $jTableResult['Result'] = "ERROR";
                $jTableResult['Message'] = "Domain Name must be less than 50 characters";
                print json_encode($jTableResult);
            }
        } else {
            //Return result to jTable
            $jTableResult = array();
            $jTableResult['Result'] = "ERROR";
            $jTableResult['Message'] = "Blank Domian Name not allowed";
            print json_encode($jTableResult);

        }

    } //Deleting a record (deleteAction)
    else if ($_GET["action"] == "delete") {
        //Delete from database
        $result = mysqli_query($con, "DELETE FROM blocked_domains WHERE id = " . $_POST["id"] . ";");

        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        print json_encode($jTableResult);
    }

    //Close database connection
    mysqli_close($con);

} catch (Exception $ex) {
    //Return error message
    $jTableResult = array();
    $jTableResult['Result'] = "ERROR";
    $jTableResult['Message'] = $ex->getMessage();
    print json_encode($jTableResult);
}
?>