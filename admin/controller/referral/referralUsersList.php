<?php
session_start();

//start config
require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
//end config

//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/date_helpers.php');
//end helper

//other
require_once('../../core/errorMap.php');
require_once "../../model/manageUser/manageUser.php";

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
//end other
/************************ input *************************************************************/
$email = cleanQueryParameter($connKeywords, cleanXSS($_POST["userID"]));
$my_referral_id = cleanQueryParameter($connKeywords, cleanXSS($_POST["userRefferalId"]));
$pageno = cleanQueryParameter($connKeywords, cleanXSS($_POST["pageno"]));
$country = cleanQueryParameter($connKeywords, cleanXSS($_POST["country"]));
$limit = cleanQueryParameter($connKeywords, cleanXSS($_POST["limit"]));
$flag = "2";
/************************ input *************************************************************/

$refferalData = getReferralUsersList($email, $my_referral_id, $pageno, $flag, $limit,$country);
$refferalData = $refferalData["errMsg"];
//printArr($refferalData);

$refferalDataCount = getReferralUsersList($email, $my_referral_id, 0, 1, 0,$country)["errMsg"]["recordcount"];

$lastPage=ceil($refferalDataCount/$limit);
?>


<div id="userList" class="m-t-15"><!--due to global id css design id is same-->
    <table class="table  text-center table-responsive">
        <thead>
        <tr>
            <th> Referrer</th>
            <th> Referral Code</th>
            <th> Country (Referrer)</th>
            <th> Total Referee's Count</th>
            <th> Total Referrers Earnings</th>
        </tr>

        </thead>
        <tbody>

        <?php
        if (count($refferalData) != 0) {
        foreach ($refferalData as $value) {
            ?>
            <tr>
                <td><?= $value["email"]; ?></td>
                <td><?= $value["my_referral_id"]; ?></td>
                <td><?= $value["country"]; ?></td>
                <td><?= $value["total_referred_users"]; ?></td>
                <td><?= $value["total_referred_earnings"]; ?></td>

            </tr>

            <?php
        }
        ?>
        </tbody>
    </table>
        <div class="input-group pull-right">

            <?php
            if($pageno==1) {
                ?>
                <button type="button" class="btn  bg-blue text-white m-r-5" disabled><i class="fa fa-angle-left"></i>
                </button>
                <?php
            }else
            {
                ?>
                <button type="button" class="btn  bg-blue text-white m-r-5"
                        onclick="getPreviousRecord('<?= $pageno; ?>','<?= $email; ?>','<?= $my_referral_id; ?>','<?= $country; ?>','<?= $limit; ?>')"><i class="fa fa-angle-left"></i>
                </button>
            <?php
            }
            if($pageno==$lastPage) {
                ?>
                <button type="button" disabled class="btn  bg-blue text-white"><i
                            class="fa fa-angle-right"></i></button>
                <?php
            }else
                {
                    ?>
                    <button type="button" onclick="getNextRecord('<?= $pageno; ?>','<?= $email; ?>','<?= $my_referral_id; ?>','<?= $country; ?>','<?= $limit; ?>')" class="btn  bg-blue text-white"><i
                                class="fa fa-angle-right"></i></button>
            <?php
                }
            ?>
        </div>
    <?php
    } else {
        ?>
        <tr>
            <td colspan="5">No Records Found</td>
        </tr>
        </tbody>

        </table>
        <?php
    }

    ?>

</div>


<script>
function getNextRecord(currentPage,email,refId,country,limit)
{
    pageno=parseInt(currentPage) + 1;
    callAjax(email,refId,pageno,country,limit);
}

function getPreviousRecord(currentPage,email,refId,country,limit)
{
    pageno=parseInt(currentPage) - 1;
    callAjax(email,refId,pageno,country,limit);

}

function callAjax(email,refId,pageno,country,limit)
{
    $.ajax({
        type: "POST",
        dataType: "html",
        url: "../../controller/referral/referralUsersList.php",
        data: {
            userID:email,userRefferalId:refId,pageno:pageno,country:country,limit:limit
        },
        beforeSend: function(){
            $('#loadng-image').show();
        },
        success: function (data) {
            $('#loadng-image').hide();
            $("#refferalUserAjax").html("");
            $("#refferalUserAjax").html(data);
        },
        error: function () {
            alert("fail");
        }
    });
}

</script>