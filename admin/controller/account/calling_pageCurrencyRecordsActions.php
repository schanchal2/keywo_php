<?php

require_once('../../config/config.php');
require_once("{$docRootAdmin}/model/acl/SessionCheck.php");
require_once('../../config/db_config.php');
require_once('../../core/errorMap.php');
require_once('../../helpers/stringHelper.php');
$connAdmin = createDBConnection("dbkeywords");
$conn = $connAdmin["connection"];

try {

    if (!$conn) {
        die("connection object not created: " . mysqli_error($con));
    }

    if ($_GET["action"] == "list") {
        //Get record count
        $result = mysqli_query($conn,"SELECT COUNT(*) AS RecordCount FROM currentRatesRecord;");
        $row = mysqli_fetch_array($result);
        $recordCount = $row['RecordCount'];

        //Get records from database
        $result = mysqli_query($conn,"SELECT * FROM currentRatesRecord ORDER BY id desc LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";");
        //$result = "SELECT * FROM industry_verticals ORDER BY name LIMIT " . $_GET["jtStartIndex"] . "," . $_GET["jtPageSize"] . ";";
        //echo $result;
        //Add all records to an array
        //$rows = array();
        while ($row = mysqli_fetch_array($result)) {
            $rows[] = $row;
        }

        //Return result to jTable
        $jTableResult = array();
        $jTableResult['Result'] = "OK";
        $jTableResult['TotalRecordCount'] = $recordCount;
        $jTableResult['Records'] = $rows;
        print json_encode($jTableResult);
    }


} catch (Exception $ex) {
    //Return error message
    $jTableResult = array();
    $jTableResult['Result'] = "ERROR";
    $jTableResult['Message'] = $ex->getMessage();
    print json_encode($jTableResult);
}

?>
