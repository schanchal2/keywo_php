<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');

//end helper

//other
require_once('../../core/errorMap.php');
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../model/geolocation/geoLocation.php');

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {
    //require_once("../config/config.php");
    //require_once("../model/utilities.php");
    //require_once("../model/wallet_utilities_admin.php");

    $country = $_POST['countryid'];

    /* $connDemo = createDbConnection($servername, $username, $password, $dbname);
     if (noError($connDemo)) {
         $connDemo = $connDemo["errMsg"];
     } else {
         printArr("Search Database Error.............");
     }*/
    $connDemo = createDBConnection("dbsearch");
    noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);
    $returnArr = array();

    $states = getAllCitiesinState($country, $connDemo);
    $states = $states["errMsg"];

    echo '<option value="citySelect" disabled>Select City</option>';

    foreach ($states as $states) {
        echo "<option value='" . $states['id'] . "'>" . $states['name'] . "</option>";
    }
}

?>
<script>

           //$("#citySelect").chosen();
</script>
