<?php
session_start();
//start helper
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');

//end helper

//other
require_once('../../core/errorMap.php');
//start config
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../model/geolocation/geoLocation.php');

if ($_SESSION['user'] == "admin" && $_SESSION['admin'] == 1) {

    $country = $_POST['countryid'];


    $connDemo = createDBConnection("dbsearch");
    noError($connDemo) ? $connDemo = $connDemo["connection"] : checkMode($connDemo["errMsg"]);
    $returnArr = array();

   // $states = getAllCitiesinState($country, $connDemo);
    $result = getCountryCode($country, $connDemo);
    $result = $result["errMsg"][0];

  $returnArr["errCode"]="-1";
  $returnArr["errMsg"]=$result;
}
else{
    $returnArr["errCode"]="1";
    $returnArr["errMsg"]="Unauthorised user!";

}
echo json_encode($returnArr,true);
?>
