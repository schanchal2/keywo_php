<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 28/11/16
 * Time: 7:03 PM
 */


function cleanQueryParameter($conn,$string) {

    $string = trim($string);
    $string = addslashes($string);
    $string = mysqli_real_escape_string($conn,$string);

    return $string;
}

function cleanXSS($string)
{
    return htmlspecialchars($string,ENT_QUOTES,'UTF-8');
}
function cleanDisplayParameter($conn, $string)
{

    /*$string = stripslashes($string);
    $string = sanitize_data($string, ENT_QUOTES | ENT_HTML5);*/
    $string=htmlspecialchars_decode($string);
    $string=html_entity_decode($string,ENT_QUOTES);
    $string = sanitize_data($string);
    $string = mysqli_real_escape_string($conn, $string);

    return $string;
}

function sanitize_data($input_data) {
    return htmlentities(stripslashes($input_data), ENT_QUOTES | ENT_HTML5);
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function formatNumberToSort
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   formatNumberToSort()
*   Purpose       :   This function convert number to sort display views (1000 = 1K, 1000000 = 1M etc).
*   Arguments     :   1.(int) $number, 2.(int) $precision.
*   Returns       :   (string) Return formatted string.
*/


function formatNumberToSort($num, $precision)
{
    $suffix = '';
    $devBy = '';

    /*if(is_float($num)){
        $num = round($num);
    }*/

    if ($num <= 999) {
        // 0 - 900
        $suffix = '';
    } else if ($num < 900000) {
        // 0.9k-850k
        $devBy = 1000;
        $suffix = 'K';
    } else if ($num < 900000000) {
        // 0.9m-850m
        $devBy = 1000000;
        $suffix = 'M';
    } else if ($num < 900000000000) {
        // 0.9b-850b
        $devBy = 1000000000;
        $suffix = 'B';
    } else {
        // 0.9t+
        $devBy = 9999999999999;
        $suffix = 'T';
    }

    if(!empty($suffix)){
        $num_format = number_format((float)($num/$devBy),$precision);
    }else{
        $num_format = number_format((float)$num,$precision);
    }

   // echo $num_format;

//    if ( $precision > 0 ) {
//        $dotzero = '.' . str_repeat( '0', $precision );
//        $num_format = str_replace( $dotzero, '', $num_format );
//    }


    return "{$num_format}{$suffix}";
}


function decodeURLparameter($datastring)
{
    $decodedquery = urldecode($datastring);
    parse_str($decodedquery, $queryStringForTransaction);

    return $queryStringForTransaction;
}

function decodeRequestParameter($string)
{
    $reqParam = trim(urldecode($string));
    $reqParam = utf8_decode($reqParam);

    return $reqParam;

}

?>