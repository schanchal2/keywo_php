<?php



/**
 * function name: folderPresenceCheck();
 * parameters: $dir(string)
 * purpose: to Check folder Existance and if not then create it
 */
function folderPresenceCheck ($dir) {
  $returnArr = array();
  $extraArg = array();

   $dir = rtrim($dir,'/');
   if (!file_exists($dir)) {
       $makeDirectory = @mkdir($dir, 0777, true);
       if($makeDirectory) {
           $errMsg = "Successfully Created Folder";
           $returnArr = setErrorStack($returnArr, -1, $errMsg,$extraArg);
       } else {
           $returnArr = setErrorStack($returnArr, 43,NULL,$extraArg);
       }
   } else {
       $errMsg = "Folder Already Exist";
       $returnArr = setErrorStack($returnArr, -1, $errMsg,$extraArg);
   }
    return $returnArr;
}




/**
 * function name: uploadImage();
 * parameters: $file(array),$fileWidth(number),$fileHeight(number),$fileType(string)
 * purpose: to upload images
 */
function uploadImage($file,$fileWidth,$fileHeight,$fileType,$targetFile){
    $returnArr = array();
    $extraArg = array();
    $check = getimagesize($file["tmp_name"]);
    if($check !== false) {
        if(!file_exists($targetFile)){
            if(move_uploaded_file($file["tmp_name"], $targetFile)){
                $errMsg = "Successfully uploaded file ".$file["name"];
                $returnArr = setErrorStack($returnArr, -1, $errMsg,$extraArg);
            }else{
                $returnArr = setErrorStack($returnArr, 11,NULL,$extraArg);
            }
        }else{
            $returnArr = setErrorStack($returnArr, 8,NULL,$extraArg);
        }

    }else{
        $returnArr = setErrorStack($returnArr, 10,NULL,$extraArg);
    }

    return $returnArr;
}




?>
