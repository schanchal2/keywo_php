<?php

function DateToMilliSeconds($date)
{
    return strtotime($date) * 1000;
}


function MilliSecondsToDate($millisecond)
{
    return date("d-m-Y h:i:s A",($millisecond/1000));
}
