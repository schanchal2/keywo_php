<?php

/*
 function checkForSession
 Purpose: To check if user cookie and session is active
 Accepts: db conn
 returns: standard format error or success array
 */
function checkForSession($conn){

    if (isset($_COOKIE["keywo_ver_1"]) && !empty($_COOKIE["keywo_ver_1"])) {

        $email = $_COOKIE["keywo_ver_1"];

        global $walletURLIP,$userRequiredFields;

        //check for User details
        $userDetails = getUserCartDetails($email, $conn);

        if(noError($userDetails)){
            $user_email = $userDetails["errMsg"]["user_email"];
            $session_id = $userDetails["errMsg"]["session_id"];

            //get user details
            $userFields = $userRequiredFields . ",_id,account_handle,system_mode,gender,no_of_qualified_searches_pending";
            $userInfo = getUserInfo($email, $walletURLIP . 'api/v3/', $userFields);
            if(noError($userInfo) ){
                $userInfo = $userInfo['errMsg'];
                if (isset($user_email) && !empty($user_email)) {
                    //valid user
                    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
                        //session is set
                        //allow login
                        session_start();
                        $_SESSION["email"] = $email;
                        $_SESSION["id"] = $userInfo["_id"];
                        $_SESSION["first_name"] = $userInfo["first_name"];
                        $_SESSION["account_handle"] = $userInfo["account_handle"];
                        $_SESSION["last_name"] = $userInfo["last_name"];
                        $_SESSION["system_mode"] = $userInfo["system_mode"];
                        $_SESSION["gender"] = $userInfo["gender"];
                        $_SESSION["pending_qualified_search"] = $userInfo["no_of_qualified_searches_pending"];

                        //update new session_start_time and session_id
                        $result = updateSession($email, $conn, $time = "", session_id());

                    } else {
                        //session is not set
                        if (isset($session_id) && !empty($session_id)) {
                            //session id is present
                            //force User to login
                            session_start();
                            $_SESSION["email"] = $email;
                            $_SESSION["id"] = $userInfo["_id"];
                            $_SESSION["first_name"] = $userInfo["first_name"];
                            $_SESSION["account_handle"] = $userInfo["account_handle"];
                            $_SESSION["last_name"] = $userInfo["last_name"];
                            $_SESSION["system_mode"] = $userInfo["system_mode"];
                            $_SESSION["gender"] = $userInfo["gender"];
                            $_SESSION["pending_qualified_search"] = $userInfo["no_of_qualified_searches_pending"];

                            //update new session_start_time and session_id
                            $result = updateSession($email, $conn, $time = "", session_id());

                        } else {

                            //User has already logged Out
                            //do nothing
                        }
                    }

                } else {

                    //Destroy Invalid cookie
                    $val = setcookie("keywo_ver_1", $email, time() - 3600, "/", "keywo.com", 0);

                }

            }else{
                //Error in get user info
            }

        }else{
            //Error in buyer details
        }

    } else {

        //Cookie Doses'nt exists
        if (isset($_SESSION["email"])) {
            //login allow login with session
        } else {
            //Session and Cookie both doesn't exists
            //session_destroy();
        }

    }

}


/*
 function updateSession
 Purpose: To update session info in the user table
 Accepts: username, db conn, session start time, session id
 returns: standard format error or success array
 */
function updateSession($username, $conn, $time="", $sessId){

    $returnArr = array();
    //create and run the update query
    $query = sprintf("UPDATE presale_users SET session_start_time=NOW(), session_id='%s' WHERE user_email='%s'", $sessId, $username);
    //echo $query;
    $result = runQuery($query, $conn);

    //return error / success array
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]="Session Updated";
    } else {
        $returnArr["errCode"]=2;
        $returnArr["errMsg"]="Session Error: Could not update session: ".$result["errMsg"];
    }

    return $returnArr;
}

/*
 function DeleteSession
 Purpose: To delete session info in the user table
 Accepts: username, db conn, session start time, session id
 returns: standard format error or success array
 */
function deleteSession($username, $conn){

    $returnArr = array();
    //create and run the update query
    $query = "UPDATE presale_users SET session_start_time=NULL, session_id=NULL WHERE user_email='".$username."'";
    //echo $query;
    $result = runQuery($query, $conn);

    //return error / success array
    if(noError($result)){
        $returnArr["errCode"]=-1;
        $returnArr["errMsg"]="Session Updated";
    } else {
        $returnArr["errCode"]=2;
        $returnArr["errMsg"]="Session Error: Could not delete session: ".$result["errMsg"];
    }

    return $returnArr;
}

?>