<?php

/*
 *  function    :	startTransaction
 *  Purpose     : 	To start CRUD operation on database but not commit
 *  Arguments	: 	i) connection object
 *  Returns     : 	Return message with error or success stack in array
*/
function startTransaction($conn)
{
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_autocommit($conn, FALSE);
    if (!$result) {
        $extraArg['transError']="Could not start transaction: " . mysqli_error($conn);
        $returnArr = setErrorStack($returnArr,4,null,$extraArg);
    } else {
        $errMsg  = "Transaction started";
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}

/*
 *  function    :	commitTransaction
 *  Purpose     : 	To apply previously CRUD operation on database
 *  Arguments	: 	i) connection object
 *  Returns     : 	Return message with error or success stack in array
*/
function commitTransaction($conn)
{
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_commit($conn);
    if (!$result) {
        $extraArg['transError']="Could not commit transaction: " . mysqli_error($conn);
        $returnArr = setErrorStack($returnArr,5,null,$extraArg);
    } else {
        $errMsg = "Transaction committed";
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}

/*
 *  function    :	rollbackTransaction
 *  Purpose     : 	To revert previously CRUD operation on database before commit
 *  Arguments	: 	i) connection object
 *  Returns     : 	Return message with error or success in array
*/
function rollbackTransaction($conn)
{
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_rollback($conn);
    if (!$result) {
        $extraArg['transError']="Could not rollback transaction: " . mysqli_error($conn);
        $returnArr = setErrorStack($returnArr,6,null,$extraArg);
    } else {
        $errMsg = "Transaction rolled back";
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}

/*
 *  function    :	runTransactionedQuery
 *  Purpose     : 	To explicitly make CRUD operation on mySql database
 *  Arguments	: 	i) connection object    ii) mysql query
 *  Returns     : 	Return error stack on success or error
*/
function runTransactionedQuery($conn,$query)
{
    $returnArr = array();
    $extraArg = array();
    $startTransaction = startTransaction($conn);
    if (noError($startTransaction)) {
        $result = runQuery($conn, $query);
        if (noError($result)) {
            commitTransaction($conn);
            $errMsg ="Query Transaction successfully commited.";
            $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
        } else {
            rollbackTransaction($conn);
            $extraArg['transError']="Query Transaction Failure. DB state rolled back. " . $result["errMsg"];
            $returnArr = setErrorStack($returnArr,7,null,$extraArg);
        }
    }

    return $returnArr;
}


?>