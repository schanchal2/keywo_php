<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 28/11/16
 * Time: 7:02 PM
 */

function printArr($array)
{
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function array_push_assoc($oldArr,$newKey,$newval)
{
    return array_merge($oldArr, array("{$newKey}" => "{$newval}"));
}

function my_array_search($array, $key, $value)
{
    $results = array();

    if (is_object($array)) {
        $array = (array) $array;
    }

    if (is_array($array)) {
        if (isset($array[$key]) && $array[$key] == $value) {
            $results[] = $array;
        }
        foreach ($array as $subarray) {
            $results = array_merge($results, my_array_search($subarray, $key, $value));
        }
    }
    return $results;
}

function array_is_unique($array) {
    return array_unique($array) == $array;
}

?>
