<?php
/*
 * Function	    :	updateGeoTrendData()
 * Purpose	    :	Update keyword search detail into geo trends database.
 * Arguments	  : (string) $searchQuery, (integer) $appId, (string) $originIp
*/

function updateGeoTrendData($searchQuery, $appId, $originIp){

    	$retArray = array();
    	global $rootUrl;

    	$geoURL = $rootUrl."geotrends/controllers/processKey.php";

      // initialize curl
       $ch = curl_init();

    	curl_setopt($ch, CURLOPT_URL, $geoURL);
    	curl_setopt($ch, CURLOPT_HEADER, false);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    	curl_setopt($ch, CURLOPT_POST, true);
    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_POSTFIELDS, "keyword=".$searchQuery."&appId=".$appId."&requestorIP=".$originIp);

    	$result = curl_exec($ch);
    	$resultJson = json_decode($result,true);

      if(noError($resultJson)){
          $extraArgs["count"] = $resultJson["count"];
          $errMsg = 'Error: '.$resultJson["errStack"];
          $retArray = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
      }else{
          $errMsg = 'Error: '.$resultJson["errMsg"];
          $errcode = $resultJson["errCode"];
          $retArray = setErrorStack($returnArr, $errcode, $errMsg, $extraArgs);
      }

    	return $retArray;
}
?>
