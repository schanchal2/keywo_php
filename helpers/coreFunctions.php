<?php
/*
*-----------------------------------------------------------------------------------------------------------
*    CORE FUNCTIONS
*-----------------------------------------------------------------------------------------------------------
*
*   Description : This file contains all the core function required to call on all controllers and
*                 models files.
*/

 /*
 *-----------------------------------------------------------------------------------------------------------
 *    Function curlRequest()
 *-----------------------------------------------------------------------------------------------------------
 *   Function Name :    curlRequest()
 *   Purpose       :    Make a curl request to given api to wallet and handle related errors
 *   Arguments     :   (array) $curlParamArr($apiName, $apiText, $curlPostFields, $log)
  *                    (array) $headerArr (x-ts : value, x-cnonce : value)
 *   Returns       :   Return response about curl request
 */


 function curlRequest($curlParamArr, $headerArr){

     global  $walletPrivateKey;
     $retArray = array();
  //   printArr($curlParamArr);

     /*
       @ other curl request function are not sending $curlParamArr['requestUrl'] parameter that's why if Else condition is written

       @ After when all other function will send $curlParamArr['requestUrl'] parameter then if else statement must delete And $apiURL = $curlParamArr['requestUrl']; must execute.

     */
     $apiURL = '';
     if (isset($curlParamArr['requestUrl']) || !empty($curlParamArr['requestUrl'])) {
       $apiURL = $curlParamArr['requestUrl'];
     } /*else {
       $apiURL = $walletURL;
     }  */

     /* Check required fields in array */
     if(count($curlParamArr) > 0){
         /* Check required fields is empty */
         if(isset($curlParamArr['apiName']) && !empty($curlParamArr['apiName'])){
           if(isset($apiURL) && !empty($apiURL)){
             /* Check required fields is empty */
             if(isset($curlParamArr['apiText']) && !empty($curlParamArr['apiText'])){
                 /* Check required fields is empty */
                 if(isset($curlParamArr['curlPostFields']) && !empty($curlParamArr['curlPostFields'])){

                     if(isset($curlParamArr['curlType']) && !empty($curlParamArr['curlType'])){

                         /* Initialize curl */
                         $ch = curl_init();

                         /* Check error in curl initialization */
                         if($ch != false){
                             /* Create signature */
                             $apiText = $curlParamArr['apiText'];
                             $signature = hash_hmac('sha512', $apiText, $walletPrivateKey);

                             /* Add signature in post fields*/

                              $postFields = "{$curlParamArr['curlPostFields']}&signature={$signature}";

                             /* Set curl options */
                             curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}");

                             if(!empty($headerArr)){
                                 curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArr);
                             }
                             if($curlParamArr['curlType'] == 'GET')
                             {
                                 curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}?{$postFields}");
                                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                             }else if($curlParamArr['curlType'] == 'POST')
                             {

                               if(isset($curlParamArr['curlHttpHeader']) && !empty($curlParamArr['curlHttpHeader'])){
                                   $curlParamArr['curlPostFields']['signature'] = $signature;
                                   $postFields = json_encode($curlParamArr['curlPostFields']);
                                   curl_setopt($ch, CURLOPT_HTTPHEADER, array( $curlParamArr['curlHttpHeader']));

                               }


                                 curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                                // curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $curlParamArr['curlType']);
                                 curl_setopt($ch, CURLOPT_POST, true);
                                 curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                 curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);

                             }else if($curlParamArr['curlType'] == 'PUT')
                             {
                               //  $postFields = "{$curlParamArr['curlPostFields']}";
                              if(isset($curlParamArr['curlHttpHeader']) && !empty($curlParamArr['curlHttpHeader'])){
                                   $curlParamArr['curlPostFields']['signature'] = $signature;
                                   $postFields = json_encode($curlParamArr['curlPostFields']);
                                   curl_setopt($ch, CURLOPT_HTTPHEADER, array( $curlParamArr['curlHttpHeader']));

                               }

                                 //curl_setopt($ch, CURLOPT_URL, "{$apiURL}{$curlParamArr['apiName']}");
                                 curl_setopt($ch, CURLOPT_CUSTOMREQUEST,$curlParamArr['curlType']);
                                 curl_setopt($ch, CURLOPT_POST, true);
                                 curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                 curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);

                             }else if($curlParamArr['curlType'] == 'DELETE')
                             {
                                 curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $curlParamArr['curlType']);
                                 curl_setopt($ch, CURLOPT_POST, true);
                                 curl_setopt($ch, CURLOPT_HEADER, FALSE);
                                 curl_setopt($ch, CURLOPT_POSTFIELDS,$postFields);
                             }

                             curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                             /* Execute curl */
                             $result = curl_exec($ch);
                             curl_close($ch);
                             if($result != false){
                                 $resultArr = json_decode($result, true);
                                 /* get curl response */
                                 $errCode   = $resultArr["errCode"];
                                 $errMsg    = $resultArr["errMsg"];
                                 if($errCode == -1){
                                     $retArray['errCode'] = -1;
                                     $retArray['errMsg'] = $errMsg;
                                 }else{
                                     $retArray['errCode'] = $errCode;
                                     $retArray['errMsg'] = $errMsg;
                                 }
                             }else{
                                 $retArray['errCode'] = 27;
                                 $retArray['errMsg'] = 'Error in curl execution';
                             }
                         }else{
                             $retArray['errCode'] = 26;
                             $retArray['errMsg'] = 'Error in curl initialization';
                         }

                     }else{
                         $retArray['errCode'] = 25;
                         $retArray['errMsg'] = 'Curl type field is missing or empty';
                     }

                 }else {
                     $retArray['errCode'] = 24;
                     $retArray['errMsg'] = 'Post field is missing or empty';
                 }
             }else {
                 $retArray['errCode'] = 23;
                 $retArray['errMsg'] = 'Api text is missing or empty';
             }
           } else {

             $retArray['errCode'] = 22;
                 $retArray['errMsg'] = 'Wrong Api server';
           }

         }else {
             $retArray['errCode'] = 21;
             $retArray['errMsg'] = 'Api name is missing or empty';
         }

     } else {
         $retArray['errCode'] = 20;
         $retArray['errMsg'] = 'Parameters are blank';
     }

     return $retArray;
 }
/*
*-----------------------------------------------------------------------------------------------------------
*    Function runQuery
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   runQuery()
*   Purpose       :   To execute the sql queries.
*   Arguments     :   (string)$query, (string)$conn
*   Returns       :   Array that contains errCode and errMsg having success or failure message.
*/

function runQuery($query, $conn){
    $returnArr = array();
    $extraArg = array();
    $result = mysqli_query($conn, $query);
    if(!$result){
        $extraArg['query']   = $query;
        $returnArr = setErrorStack($returnArr,3,null,$extraArg);
    }else{
        $extraArg['dbResource'] = $result;
        $errMsg = 'Query Successful';
        $returnArr = setErrorStack($returnArr,-1,$errMsg,$extraArg);
    }

    return $returnArr;
}



/*
*-----------------------------------------------------------------------------------------------------------
*    Function noError
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   noError()
*   Purpose       :   This function valdiate the condition, if array contains errCode = -1 then true
*                      else false.
*   Arguments     :   (array)$resArray
*   Returns       :   boolean (true or false)
*/

function noError($resArray) {
    $noError = false;
    if($resArray["errCode"] == -1){
        $noError = true;
    }
    return $noError;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getEncryptedNonce()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getEncryptedNonce()
*   Purpose       :   creating cipher object using Rijndael encryption algorithm with Cipher-block chaining
*                     (CBC) as mode of AES encryption Here using 128 bit Rijndael encryption
*   Arguments     :   (string)$nonce_aes_mode_key, (string)$nonce_secret
*   Returns       :   Nonce cipher text in encrypted form.
*/

function getEncryptedNonce(){

        global $nonce_aes_mode_key, $nonce_secret;

        $returnArr = array();
        /*
        creating cipher object using Rijndael encryption algorithm with Cipher-block chaining (CBC) as mode of AES encryption
        Here I have chosen 128 bit Rijndael encryption
        */
        $cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

        /*
        for 256 bit AES encryption key size should be of 32 bytes (256 bits)
        for 128 bit AES encryption key size should be of 16 bytes (128 bits)
        here i am doing 256-bit AES encryption
        choose a strong key
        */
        $key = $nonce_aes_mode_key;

        /*
        for 128 bit Rijndael encryption, initialization vector (iv) size should be 16 bytes
        for 256 bit Rijndael encryption, initialization vector (iv) size should be 32 bytes
        here I have chosen 128 bit Rijndael encryption, so $iv size is 16 bytes
        */
        $initVector = getVectorCounter();

        if(noError($initVector)){

              $initVector = $initVector["errMsg"]["counter"];

              $nonceSecret = $nonce_secret;
              mcrypt_generic_init($cipher, $key, $initVector);
              // PHP pads with NULL bytes if $plainText is not a multiple of the block size
              $getNonce = mcrypt_generic($cipher, $nonceSecret);
              mcrypt_generic_deinit($cipher);
              /*
              $nonceBinToHex stores encrypted text in hex
              we will be decrypting data stored in $cipherHexText256 from node js
              */
              $nonceBinToHex = bin2hex($getNonce);

              /*
              echoing $nonceBinToHex (copy the output)
              */
              $returnArr["errCode"] = -1;
              $returnArr["errMsg"] = $nonceBinToHex;
        }else{
              $returnArr["errCode"] = $initVector["errCode"];
              $returnArr["errMsg"] = $initVector["errMsg"];
        }

      return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function generateSignatureWithNonce
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   generateSignatureWithNonce()
*   Purpose       :   Append current timestamp in signature.
*   Arguments     :   (string) (apiText)
*/


function generateSignatureWithNonce($apiTextVal){
    if(!empty($apiTextVal)){
        $get_nonce = getEncryptedNonce();
        if(noError($get_nonce)){
            $nonce_value = $get_nonce["errMsg"];
            $timestamp = round(microtime(true) *1000);
            $api_text = "{$apiTextVal}&ts={$timestamp}";
            /*$post_fields = "{$reqParam['curlPostFields']}&nonce={$nonce_value}&timstamp={$timestamp}";*/

            $retArray["errCode"] = -1;
            $retArray["errMsg"]["apiText"] = $api_text;
            $retArray["errMsg"]["timestamp"] = $timestamp;
            $retArray["errMsg"]["cnonce"] = $nonce_value;
        }else{
            $retArray['errCode'] = $get_nonce['errCode'];
            $retArray['errMsg'] = $get_nonce["errMsg"];;
        }
    }else{
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = "Mandatory fields not found";
    }

    return $retArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function getVectorCounter
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getVectorCounter()
*   Purpose       :   Generate vector counter to create encoded cipher text.
*   Arguments     :   none
*/

function getVectorCounter(){
      global $docRoot;
      $returnArr = array();

      // nonce directory
      $nonceDir = $docRoot."controllers/nonce";
      if(is_dir($nonceDir)){
            // nonce json file that stores current timestamp and next request vector counter.
            $nonceCounterJson = $nonceDir."/nonceCounter.json";

            if(file_exists($nonceCounterJson)){
                  // if json exist then read the current vector counter
                  $fp = fopen($nonceCounterJson, "r");
                  $readJson = fread($fp, filesize($nonceCounterJson));
                  fclose($fp);
                  $readJson = json_decode($readJson, true);
                  $currentCounter = $readJson["vector_count"];

                  $returnArr["errCode"] = -1;
                  $returnArr["errMsg"]["counter"] = $currentCounter;
                  $returnArr["errMsg"]["counter_json_path"] = $nonceCounterJson;
            }else{
                  $returnArr["errCode"] = 2;
                  $returnArr["errMsg"] = "Error: ".$nonceCounterJson." not exist.";
            }
      }else{
            $returnArr["errCode"] = 1;
            $returnArr["errMsg"] = "Error: ".$nonceDir." not exist.";
      }

      return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function incrementVectorCounter
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   incrementVectorCounter()
*   Purpose       :   This function is used to increment vector counter by 1 and write into nonceCounter.json
*                     file.
*   Arguments     :   none
*/
function incrementVectorCounter(){

      $returnArr = array();
      $getVectorCounter = getVectorCounter();

      if(noError($getVectorCounter)){
           $getVectorCounter       = $getVectorCounter["errMsg"];
           $currentCounter         = (double) $getVectorCounter["counter"];
           $nonceCounterJsonPath   = $getVectorCounter["counter_json_path"];

          if($currentCounter < 1000000000000000){
                $nextCounter =$currentCounter +1;
                $nextCounter = sprintf("%'.016d\n", $nextCounter);
                $nextCounter = trim(str_replace("\n",'', $nextCounter));
          }else{
                $nextCounter = $currentCounter+1;
          }

          // After getting the current vector counter prepare for the next vector counter and write
          // the nonce json file.

          $fp1 = fopen($nonceCounterJsonPath, "w");
          $currentTime = time();
          $arr = array("time" => $currentTime, "vector_count" => $nextCounter);
          $arr = json_encode($arr);
          fwrite($fp1, $arr);
          fclose($fp1);

          $returnArr["errCode"] = -1;
          $returnArr["errMsg"] = "Successfully counter increases by 1";
      }else{
          $returnArr["errCode"] = 1;
          $returnArr["errMsg"] = $getVectorCounter["errMsg"];
      }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function createVectorCounterArchieveCron()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   createVectorCounterArchieveCron()
*   Purpose       :   This is a cron file, which is used to take backup of nonceCounterJson file which
*                     contains the vector counter current timestamp and current vector count.
*   Arguments     :   none
*/

function createVectorCounterArchieveCron(){
    $returnArr = array();
    global $docRoot;

    $nonceDir = $docRoot."controllers/nonce";
    if(is_dir($nonceDir)){
        $nonceCounterJson = $nonceDir."/nonceCounter.json";
        if(file_exists($nonceCounterJson)){
              // If file exist then .
              // 1. create nonceCounter.zip archieve file which stores the nonceCounter.json file.
              // 2. create a json back up file with name nonceCounter.json.bak.
              $fileBackUp = fileIO($nonceDir, $nonceCounterJson);
              if($fileBackUp["errCode"] == -1){
                  $returnArr["errCode"] = -1;
                  $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
              }else{
                  $returnArr["errCode"] = 1;
                  $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
              }
        }else{
              // if file not exist then
              // 1. call API to get current vector counter.
              // 2. create a nonceCounter.json file with current time and vector counter get from API.
              // 3. create nonceCounter.zip archieve file which stores the nonceCounter.json file.
              // 4. create a json back up file with name nonceCounter.json.bak.

              // get API response here
              $apiResult = '{"errCode":"-1","errMsg":"0000000000000048"}';
              // end API response here
              $apiResult = json_decode($apiResult, true);
              if($apiResult["errCode"] == -1) {
                    // get current timestamp.
                    $currentTime = time();
                    // get vector counter from API result.
                    $vector_counter = $apiResult["errMsg"];

                    $arr = array("time" => $currentTime, "vector_count" => $vector_counter);
                    // json encode the data.
                    $arr = json_encode($arr);

                    // create a file pointer
                    $filePointer = fopen($nonceCounterJson, "w");
                    // write into the file i.e. $nonceCounterJson.
                    fwrite($filePointer, $arr);
                    // close the file pointer
                    fclose($filePointer);
                    // give full permission to the json file.
                    chmod($nonceCounterJson, 0777);

                    $fileBackUp = fileIO($nonceDir, $nonceCounterJson);
                    if($fileBackUp["errCode"] == -1){
                        $returnArr["errCode"] = -1;
                        $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
                    }else{
                        $returnArr["errCode"] = 1;
                        $returnArr["errMsg"] = $fileBackUp["ErrMsg"];
                    }
            }else{
                  $returnArr["errCode"] = 1;
                  $returnArr["errMsg"] = "Error: Unable to get API response.";
            }
        }
    }else{
      $returnArr["errCode"] = 1;
      $returnArr["errMsg"] = "Error: ". $nonceDir ." not found";
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*    Function fileIO()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   fileIO()
*   Purpose       :   To create archeve and nonceCounter.json backup file.
*   Arguments     :   (string)$nonceDir, (string)$nonceCounterJson
*/

function fileIO($nonceDir, $nonceCounterJson){
      $returnArr = array();

      // If file exist then create or orverwrite the existing json file and create new json file with current time.
      $zip = new ZipArchive;
      $URL = $nonceDir."/nonceCounter.zip";

      if ($zip->open($URL, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE ) === TRUE ) {
          $URL = $nonceDir."/nonceCounter.json";
          $zip->addFile($URL, 'nonceCounter.json');
          $zip->close();

          // create a back up file i.e. nonceCounter.json.bak, if incase by mistake json file get deleted.

          // open nonce counter json file.
          $fp = fopen($nonceCounterJson, "r+");
          if($fp){
            // read the json file.
            $fpRead = fread($fp, filesize($nonceCounterJson));
            // close the file pointer ($fp).
            fclose($fp);

            // declear json backup variable
            $nonceBackUp = $nonceDir."/nonceCounter.json.bak";

            // create a new file pointer that open new file IO.
            $newFP = fopen($nonceBackUp, "w");
            if($newFP){
                  // write content into json backup file that  end extension with '.bak' i.e. nonceCounter.josn.bk.
                  fwrite($newFP, $fpRead);

                  // close file pointer
                  fclose($newFP);

                  // change mode permission to read, write and execute
                  chmod($nonceBackUp, 0777);

                  $returnArr["errCode"] = -1;
                  $returnArr["errMsg"] = "Successfully create json backup and archieve file.";
            }else{
                  $returnArr["errCode"] = 3;
                  $returnArr["errMsg"] = "Error: Unable to open file pointer for " .$nonceBackUp. " file.";
            }
          }else{
              $returnArr["errCode"] = 2;
              $returnArr["errMsg"] = "Error: Unable to open file pointer for ". $nonceCounterJson ." file.";
          }
      }else{
          $returnArr["errCode"] = 1;
          $returnArr["errMsg"] = "Error: Unable to create archive file.";
      }

      return $returnArr;
}


/*
*-----------------------------------------------------------------------------------------------------------
*   Function getCurrencyExchangeRate()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getCurrencyExchangeRate()
*   Purpose       :   This function returns the exchange rate of respective currency which is the
*                 :   conversion of IT$ amount i.e. (USD).
*   Arguments     :   (string)$conn, (array) $currencyExchangeRate
*   Returns       :   (array) exchange rate of USD, BTC, SGD, UPC.
*/

function getCurrencyExchangeRate($currencyExchangeArr, $searchDbConn){

    $returnArr = array();
    $currencyRateArr = array();
    $extraArgs = array();
    $fromCurrCode = "USD";

    if(count($currencyExchangeArr) > 0){
        if(isset($searchDbConn) && !empty($searchDbConn)) {
            foreach($currencyExchangeArr as $key => $toCurrCode){

                $fromCurrRate = getCurrentRate($fromCurrCode, $searchDbConn);
                if(noError($fromCurrRate)){
                    $fromCurrRate = $fromCurrRate["errMsg"]["current_price"];

                    $toCurrRate = getCurrentRate($toCurrCode, $searchDbConn);
                    if(noError($toCurrRate)){

                        $toCurrRate = $toCurrRate["errMsg"]["current_price"];
                        /*if($toCurrCode == "BTC"){
                            $amount = $fromCurrRate/$toCurrRate;
                        }else{
                            $amount = (float)$toCurrRate;
                        }*/
                        $amount = (float)$toCurrRate;
                        //  $currKey = "IT$ - ".$toCurrCode;
                        $toCurrCode = strtolower($toCurrCode);
                        $currencyRateArr[$toCurrCode] = $amount;
                        $extraArgs["exchange_rate"] = $currencyRateArr;

                        $errMsg = "Successfully fetch currency rate for ".$toCurrCode;
                        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
                    }else{
                        $errMsg = "Error: unable to get from currency rate";
                        $returnArr = setErrorStack($returnArr, 4, $errMsg, $extraArgs);
                    }
                }else{
                    $errMsg = "Error: unable to get from currency rate";
                    $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
                }
            }
        }else{
            $errMsg = "Error: missing database connection";
            $returnArr = setErrorStack($returnArr, 2, $errMsg, $extraArgs);
        }
    }else{
        $errMsg = "Error: empty currency code array";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;

}


//to calculate users available balance.
function getUserBalance($email){
  $extraArg       = array();
  $returnArr      = array();
  $myAvailableBalance = 0;
  global $walletURLIP,$userRequiredFields;
  // get user details with available balance if login
  $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,purchase_itd";

  $balanceArray = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);

  if (noError($balanceArray)) {

    if ($balanceArray['errCode'] == -1) {
      $myAvailableBalance = $balanceArray['errMsg']['deposit'] + $balanceArray['errMsg']['sales'] +
			$balanceArray['errMsg']['cashback'] + $balanceArray['errMsg']['affiliate_earning'] + $balanceArray['errMsg']['purchase_itd'] +
			$balanceArray['errMsg']['total_kwd_income'] + $balanceArray['errMsg']['search_earning'] +
			$balanceArray['errMsg']['social_content_view_earnings'] +
            $balanceArray['errMsg']['search_affiliate_earnings'] +
            $balanceArray['errMsg']['social_content_sharer_earnings'] +
            $balanceArray['errMsg']['social_content_creator_earnings'] +
            $balanceArray['errMsg']['total_app_income'] - $balanceArray['errMsg']['blocked_for_pending_withdrawals'] -
            $balanceArray['errMsg']['blocked_for_bids'] - $balanceArray['errMsg']['approved_withdrawals'] -
            $balanceArray['errMsg']['trade_fees'] -	$balanceArray['errMsg']['purchases'] -
            $balanceArray['errMsg']['renewal_fees'];
    }

    if($myAvailableBalance <= 0){
      $myAvailableBalance =   0.00000;
    }

    $errMsg    = ['total_available_balance' => $myAvailableBalance];
    $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArg);

    if ($balanceArray['errCode'] == 27) {
      $returnArr = $balanceArray;
    }

  }

  return $returnArr;
}

//to send SMS
function sendSMS($msg='',$mobile=''){

    define('SEND_SMS_URL', 'http://onlinesms.in/api/sendValidSMSdataUrl.php?');
    define('SMS_URL_LOGIN','9320027660');
    define('SMS_URL_PWD','tagepuguz');
    define('SMS_URL_SNDRID','OPTINS');

    $ch   = curl_init();
    $url  = SEND_SMS_URL;
    $login  = SMS_URL_LOGIN;
    $pwd  = SMS_URL_PWD;
    $sndID  = SMS_URL_SNDRID;
    $message  = urlencode($msg);
    $urlnew = $url."login=".$login."&pword=".$pwd."&msg=".$message."&senderid=".$sndID."&mobnum=".$mobile;
    curl_setopt($ch, CURLOPT_URL, $urlnew);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "login=".$login."&pword=".$pwd."&msg=".$message."&senderid=".$sndID."&mobnum=".$mobile);
    $buffer = curl_exec($ch);

    curl_close($ch);
    return $buffer;
}

//
function generateOTPnumber($length = 9, $add_dashes = false, $available_sets = 'luds')
{
    $sets = array();
    if(strpos($available_sets, 'l') !== false)
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
    if(strpos($available_sets, 'u') !== false)
        $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
    if(strpos($available_sets, 'd') !== false)
        $sets[] = '23456789';
    if(strpos($available_sets, 's') !== false)
        $sets[] = '!#@$%&';


    $all = '';
    $password = '';
    foreach($sets as $set)
    {
        $password .= $set[array_rand(str_split($set))];
        $all .= $set;
    }

    $all = str_split($all);
    for($i = 0; $i < $length - count($sets); $i++)
        $password .= $all[array_rand($all)];

    $password = str_shuffle($password);

    if(!$add_dashes)
        return $password;

    $dash_len = floor(sqrt($length));
    $dash_str = '';
    while(strlen($password) > $dash_len)
    {
        $dash_str .= substr($password, 0, $dash_len) . '-';
        $password = substr($password, $dash_len);
    }
    $dash_str .= $password;
    return $dash_str;
}

function getExchangeRates($currCode){
    global $rootUrl;
    $returnArray = array();
    $bitPayResponse = file_get_contents("{$rootUrl}controllers/convertCurrency.php?curCode={$currCode}");
    $bitPayResponseArr = json_decode($bitPayResponse, true);
    if(noError($bitPayResponseArr))
    {
        $returnArray["errCode"] = -1;
        $returnArray["currRate"] = $bitPayResponseArr["currRate"];

    }else{
        $returnArray["errCode"] = 2;
        $returnArray["errMsg"] = "Error: Fetching current exchange rate.";
    }

    return $returnArray;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getCurrentRate()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getCurrentRate()
*   Purpose       :   This function returns the exchange rate of respective currency.
*   Arguments     :   (string)$conn, (array) $currencyExchangeRate
*   Returns       :   (array) exchange rate of respective currency like USD or BTC or SGD or UPC.
*/

function getCurrentRate($currency, $conn){

    $returnArr = array();

    $query = "SELECT current_price from currency_value where shortforms ='".$currency."'";
    $currResult = runQuery($query, $conn);
    if(noError($currResult)){
        while($row = mysqli_fetch_assoc($currResult["dbResource"])){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $row;
        }
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = $currResult["errMsg"];
    }

    return $returnArr;
}


function initializeXMLLog($userEmail){

    $retArray = array();
    $deviceType = "web";
    $userIp = getClientIP();
    $data = getLocationUserFromIP($userIp);

    // check whether request parameter is email or account handle, if account handle then accordingly make the
    // activity attributes

    if(preg_match("/^[^@]{1,64}@[^@]{1,255}$/", $userEmail)){
        $activity = "email";
    } else{
        $activity = "account_handle";
    }

    $activity_attribute=array();
    $activity_attribute[$activity]=$userEmail;
    $activity_attribute['timestamp']=uDateTime("Y-m-d h:i:s A",date("Y-m-d h:i:s A"));
    $activity_attribute['browser']=getBrowserName();
    $activity_attribute['userIp']=$data["userIP"];
    $activity_attribute['device']=$deviceType;
    $activity_attribute['country']=$data["country"];
    $activity_attribute['state']=$data["state"];
    $activity_attribute['city']=$data["city"];
    $activity_attribute['gender']=isset($_SESSION["gender"])?$_SESSION["gender"]:"";
    $activity_attribute['username']=isset($_SESSION["first_name"])?$_SESSION["first_name"]:"".isset($_SESSION["last_name"])?$_SESSION["last_name"]:"";

    $request_attribute=array();

    $retArray = array('activity' => $activity_attribute, 'request' => $request_attribute);

    return $retArray;
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function getTotalKeywordSold
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getTotalKeywordSold()
*   Purpose       :   This function returns Total Keyword sold.
*
*   Arguments     :   (obj)$conn connection object of respective Database where table exists.
*   Returns       :   (int) Total Total Keyword sold.
*/

function getTotalKeywordSold($conn) {
      $returnArr = array();
      $extraArgs = array();
      $query = "select sum(kwd_sold) as total_keyword_sold from first_purchase_slabs";

      $execQuery = runQuery($query, $conn);

      if(noError($execQuery)){
          $cmsRecords = array();
          while($row = mysqli_fetch_assoc($execQuery["dbResource"])){
                $cmsRecords[] = $row;
          }

          $errMsg = "Successfully fetch Total Keyword Sold";
          $extraArgs["data"] = $cmsRecords;
          $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

      }else{
          $errMsg = "Error getting Total Keyword Sold ";
          $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
      }

      return $returnArr;
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function getUserInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getUserInfo()
*   Purpose       :   Get user balance details from wallet database by making an API request.
*   Arguments     :   1.(string) $email, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

function getUserInfo($email, $requestUrl, $userRequiredFields)
{

  global  $walletPublicKey, $mode;
  $retArray = array();
  $headers   = array();

  /* create signature */
  $apiText      = "email={$email}&fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
  $postFields   = "email=" . $email . "&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
  // $requestUrl    = strtolower($requestUrl);
  $apiName      = 'user/userdetails';
  $curl_type    = 'GET';


  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

  /* Check mode type from config (Nonce active or not)*/
  if($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if(noError($nonce_result)) {
      $nonce_result        = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg']  = $nonce_result['errMsg'];
      $headers[]           = "x-ts: {$nonce_result['timestamp']}";
      $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg']  = $nonce_result['errMsg'];
    }
  }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}


/**
*   Function Name :   getUserInfoByHandle()
*   Purpose       :   Get user balance details from wallet database by making an API request.
*   Arguments     :   1.(string) $email, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

function getUserInfoByHandle($account_handle, $requestUrl, $userRequiredFields)
{

  global  $walletPublicKey, $mode;
  $retArray = array();
  $header   = array();

  /* create signature */
  $apiText      = "fieldnames={$userRequiredFields}&publicKey={$walletPublicKey}";
  $postFields   = "account_handle=" . $account_handle . "&fieldnames=" . $userRequiredFields . "&publicKey=" . urlencode($walletPublicKey);
  $apiName      = 'user/userdetails';
  $curl_type    = 'GET';


  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

  /* Check mode type from config (Nonce active or not)*/
  if($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if(noError($nonce_result)) {
      $nonce_result        = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      $headers[]           = "x-ts: {$nonce_result['timestamp']}";
      $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg']  = $nonce_result['errMsg'];
    }
  }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function getUserWorkInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getUserWorkInfo()
*   Purpose       :   Get user work details from wallet database by making an API request.
*   Arguments     :   1.(integer) $userId, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/
function getUserWorkInfo($userId, $requestUrl) {
  global  $walletPublicKey, $mode;
  $retArray = array();
  $header   = array();

  /* create signature */
  $apiText      = "user_id={$userId}&publicKey={$walletPublicKey}";
  $postFields   = "publicKey=" . urlencode($walletPublicKey);
  $apiName      = 'user/' . $userId . '/eduworkinfo';
  $curl_type    = 'GET';
  $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

  /* Check mode type from config (Nonce active or not)*/
  if($mode == 'production') {
    $nonce_result = generateSignatureWithNonce($apiText);
    if(noError($nonce_result)) {
      $nonce_result        = $nonce_result['errMsg'];
      $retArray['errCode'] = -1;
      //$retArray['errMsg']  = $nonce_result['errMsg'];
      $headers[]           = "x-ts: {$nonce_result['timestamp']}";
      $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
    } else {
      $retArray['errCode'] = 2;
      $retArray['errMsg']  = $nonce_result['errMsg'];
    }
  }

  /* Making curl request */
  $respArray = curlRequest($curlReqParam, $headers);
//print_r($respArray);
  if (noError($respArray)) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = -1;
    $retArray['errMsg']  = $respArray['errMsg'];
    $retArray['mode']    = $mode;

  } elseif ($respArray["errCode"] != 73) {
    if ($mode == 'production') {
      $nonce_counter = incrementVectorCounter();
    }
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  } else {
    $retArray['errCode'] = $respArray['errCode'];
    $retArray['errMsg']  = $respArray['errMsg'];
  }

  return $retArray;
}


function setUserNotifyInfo($id, $fieldType, $fieldValue) {

  global $walletPublicKey, $walletPrivateKey, $walletURLIPnotification;
  $extraArgs = array();

  $ch              = curl_init();
  $returnArr       = array();
  $NotificationURL = $walletURLIPnotification."api/notify/v2/user/";
  //check if field value is "short_desc" than api text changes. So if..
  if ($fieldType == "short_desc") {
    $apiText         = "user_id={$id}&publicKey={$walletPublicKey}";
  } else {
   $apiText         = "user_id={$id}&{$fieldType}={$fieldValue}&publicKey={$walletPublicKey}";
  }

  $signature       = hash_hmac('sha512', $apiText, $walletPrivateKey);
  $api_request_url = $NotificationURL.$id.'/usermetadata?publicKey=' . $walletPublicKey . '&signature=' . $signature;
  // $postFields      = array($fieldType => $fieldValue);
  $postFields      = $fieldType . "=" . $fieldValue;

  curl_setopt($ch, CURLOPT_URL, $api_request_url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
  // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postFields));
  curl_setopt($ch, CURLOPT_POSTFIELDS, $postFields);

  $result     = curl_exec($ch);
  $resultJson = json_decode($result,true);
  // print_r($result);die();
  //get curl response
  $errCode = $resultJson["errCode"];
  $errMsg  = $resultJson["errMsg"];
  //printArr($result);die();
  if ($errCode == -1) {
    $returnArr["errCode"] = -1;
    $returnArr["errMsg"]  = $errMsg;
    $returnArr            = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

   } else {
     //error message
     $returnArr["errCode"] = $errCode;
     $returnArr["errMsg"]  = $errMsg;
     $returnArr            = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
   }
   return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function searchKeySanitizer()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   searchKeySanitizer()
*   Purpose       :   This function sanitize the search query and remove extra spaces from query (left, right,
*                 :    and in between ).
*   Arguments     :   (string) $keyword
*   Returns       :   (float) current_payout
*/

function searchKeySanitizer($keyword){

  $keywordsArr = explode(" ", $keyword);

  //looping through keywordsArr to remove extra spaces.
  $searchkey = "";
  foreach($keywordsArr as $key => $keyword){
    if($keyword!=""){
      $searchkey.=$keyword." ";
    }
  }

  $array2 = array();
  foreach($keywordsArr as $array1){
    $string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.

    //$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

    $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.

    array_push($array2, $string);
  }

  $searchData = implode(" ", $array2);

  // sanitizing search query from tags, extra spaces and special characters.
//	$searchData = strip_tags(trim($searchData));
  $searchData = preg_replace("/\s\s([\s]+)?/", " ", $searchData);

  return $searchData;
}

/*
 * Function	    :	getAdminSettingsFromSearchAdmin()
 * Purpose	    :	To retrieve admin setting details form scoin97_db database from admin_settings table.
 * Arguments	  : (string)$conn.
 * Returns	    :	Returns all data from admin setting table in array format.
*/
function getSettingsFromSearchAdmin($conn){
  $returnArr = array();
    $extraArgs = array();

  $query = "SELECT * FROM admin_setting ORDER BY timestamp DESC LIMIT 1";
  $result = runQuery($query, $conn);
  if (noError($result)) {
    $settingRes = array();

    while ($row = mysqli_fetch_assoc($result["dbResource"]))
         $settingRes = $row;

        $errMsg = "Successfully : fetched admin setting details.";
        $extraArgs["data"] = $settingRes;
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);
  } else {
        $errMsg = "Error : Fetching admin setting details.";
        $returnArr = setErrorStack($returnArr, 3, $errMsg, $extraArgs);
  }

  return $returnArr;
}

function getAdminSettingsFromKeywordAdmin($connkeyword){
    $returnArr = array();
    $extraArgs = array();

    $query = "SELECT * FROM admin_settings ORDER BY update_timestamp DESC LIMIT 1";
    $result = runQuery($query, $connkeyword);
    if (noError($result)) {
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;

        $extraArgs["data"] = $res;
        $errMsg = "Successfully fetch admin setting table details.";
        $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

    } else {
        $errMsg = "Error : Fetching keyword admin setting details.";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }

    return $returnArr;
}

/*
*-----------------------------------------------------------------------------------------------------------
*   Function getSearchType()
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getSearchType()
*   Purpose       :   This function returns the users search type either qualified or unqualified search.
*   Arguments     :   (string)$email
*   Returns       :   (array) Array( [errCode] => -1 Array( [search_type] => Qualified / Unqualified ))
*/

function getSearchType($email, $userRequiredFields){
    global $walletURLIPnotification;
    $returnArr = array();
    $response = array();

    $requestUrl = $walletURLIPnotification."api/notify/v2/";
    $getUserDetails = getUserInfo($email, $requestUrl, $userRequiredFields);

    if (noError($getUserDetails)) {
        // getting user info details
        $getUserDetails = $getUserDetails["errMsg"];
        if (isset($getUserDetails["user_id"])) {

            $userId = $getUserDetails["user_id"];
        } else {
            $userId = "";
        }

        $conn = createDBConnection("dbsearch");
        if(noError($conn)) {
            $conn = $conn["connection"];

            /* Get kyc level from db search */
            $getKycLevels = getKYCSlabByLevel($conn);
            if (noError($getKycLevels)) {
                $getKycLevels = $getKycLevels['errMsg'];
                $KycLevelsParam = array();

                for ($i = 0; $i < count($getKycLevels) - 1; $i++) {
                    $KycLevelsParam[$i]['kyc_level'] = $getKycLevels[$i]['kyc_level_name'];
                    $KycLevelsParam[$i]['interaction'] = $getKycLevels[$i]['intraction'];
                }

                $numberOfQualifiedSearches = $getUserDetails["no_of_qualified_interactions_pending"];
                $numberOfSearchesInLastHour = $getUserDetails["no_of_searches_in_last_hour"];
                $lastHourSearchTime = $getUserDetails["last_hour_search_time"];
                if(isset($getUserDetails["kyc_current_level"])){
                    $userKycLevel = $getUserDetails["kyc_current_level"];
                }else{
                    $userKycLevel = "";
                }

                $profilePic = $getUserDetails["profile_pic"];

                /* Set user qualifiedInteractionLimit */
                $qualifiedInteractionLimit = '';
                if($userKycLevel == 'kyc_1'){
                    $qualifiedInteractionLimit = $KycLevelsParam[0]['interaction'];
                }else if($userKycLevel == 'kyc_2'){
                    $qualifiedInteractionLimit = $KycLevelsParam[1]['interaction'];
                }else if($userKycLevel == 'kyc_3'){
                    $qualifiedInteractionLimit = $KycLevelsParam[2]['interaction'];
                }

                if(isset($getUserDetails["no_of_qualified_searches_pending"])){
                    $no_of_qualified_searches_pending  = $getUserDetails["no_of_qualified_searches_pending"];
                }else{
                    $no_of_qualified_searches_pending  = "";
                }
                if ($numberOfQualifiedSearches <= $qualifiedInteractionLimit && $numberOfQualifiedSearches > 0) {

                    $sec = round($lastHourSearchTime / 1000); //Convert miliseconds to seconds
                    $diff = time() - $sec; //Calculate time difference in seconds
                    if ($diff < 3600) {
                        if ($numberOfSearchesInLastHour < 10) {

                            $userSearchType = "Qualified";
                        } elseif ($numberOfSearchesInLastHour >= 10) {
                            $userSearchType = "Unqualified";
                        }
                    } elseif ($diff >= 3600) {
                        $userSearchType = "Qualified";
                    }
                } elseif ($no_of_qualified_searches_pending == 0) {
                    $userSearchType = "Unqualified";
                }

                $response["user_search_type"] = $userSearchType;
                $response["user_id"] = $userId;
                $response["no_of_qualified_interactions_pending"] = $numberOfQualifiedSearches;
                $response["no_of_searches_in_last_hour"] = $numberOfSearchesInLastHour;
                $response["last_hour_search_time"] = $lastHourSearchTime;
                $response["user_kyc_level"] = $userKycLevel;
                $response["qualified_interaction_limit"] = $qualifiedInteractionLimit;
                $response["profile_pic"] = $profilePic;

                $errMsg = "Success";
                $extraArgs["data"] = $response;
                $returnArr = setErrorStack($returnArr, -1, $errMsg, $extraArgs);

            }else{
                $errMsg = "Error: Unable to get kyc level";
                $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
            }

        }else{
            $errMsg = "Error: Unable to connect databse";
            $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
        }

    } else {
        $errMsg = "Error: Unable to get user search type";
        $returnArr = setErrorStack($returnArr, 1, $errMsg, $extraArgs);
    }


    return $returnArr;
}




/**
* To send email or insert notification
* To send email and notification both set preferenceCode 2
* To send insert only notification keep  preferenceCode blank
*/
function sendNotification($to,$mailSubject,$emailBody,$firstName,$lastName,$userId,$smsText,$mobileNumber,$notificationBody,$preferenceCode,$category){
  global $walletPublicKey, $mode, $walletURL, $NotificationURL;
  global $descriptionArray;
  $retArray = array();
  $headers = array();

  if(isset($to) && isset($mailSubject) && isset($userId) && isset($preferenceCode)){
    $noteArr = array();
    $noteArr['note'] = urlencode(utf8_encode($note));

    $postFields = array(
      "mail_to" => $to,
      "mail_subject" =>  utf8_encode($mailSubject),
      "email_body" => utf8_encode($emailBody),
      "first_name" => $firstName,
      "last_name" => $lastName,
      "user_id" => $userId,
      "smsText" => utf8_encode($smsText),
      "mobileNumber" => $mobileNumber,
      "notification_body" => utf8_encode($notificationBody),
      "preference_code" => $preferenceCode,
      "category" => $category,
      "publicKey" =>  urlencode($walletPublicKey)
    );

    $mailSubject = $mailSubject;
    //create signature
    $apiText = "mail_to={$to}&mail_subject={$mailSubject}&user_id={$userId}&preference_code={$preferenceCode}&publicKey={$walletPublicKey}";
    $postFields = $postFields;
    $apiName = "v2/inbox/user";
    $requestUrl = "{$NotificationURL}";
    $curl_type = 'POST';
    $content_type = "content-type: application/json";
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type, 'curlHttpHeader' => $content_type);

    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg'] = $respArray['errMsg'];
        $retArray['mode'] = $mode;

    } else if ($respArray["ergenerateSignatureWithNoncerCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg'] = $respArray['errMsg'];
    }
  }else{
    $retArray['errCode'] = 7;
      $retArray['errMsg'] = "Mandatory feild not found";
  }

  return $retArray;
}

function getCurrentKYCLimits($searchConn,$email){
  global $walletURLIP,$userRequiredFields;
  $returnArr = array();
  $requiredFields = $userRequiredFields . ",kyc_current_level,mobile_number,security_preference";

  $userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $requiredFields);

  if(noError($userDetails)){
    $kyc_current_level = $userDetails['errMsg']['kyc_current_level'];
    $kyc_current_level = str_replace("kyc_","Level-",$kyc_current_level);
    $query = "SELECT * FROM kyc_levels WHERE kyc_level_name='{$kyc_current_level}'";
    $queryResult = runQuery($query,$searchConn);
    if(noError($queryResult)){
      $rows = mysqli_fetch_array($queryResult['dbResource']);
      if(!empty($rows)){
        $returnArr['errMsg'] = $rows;
        $returnArr['errCode'] = -1;
      }
    }else{
      $returnArr['errMsg'] = "Failed";
      $returnArr['errCode'] = 2;
    }
  }else{
    $returnArr['errMsg'] = "Failed";
    $returnArr['errCode'] = 2;
  }
  return $returnArr;
}

function internalCurlRequestPost($url,$fieldscount,$fields_string)
{
    $strCookie = 'PHPSESSID=' . $_COOKIE['PHPSESSID'] . '; path=/';
    session_write_close();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, $fieldscount);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
    curl_setopt( $ch, CURLOPT_COOKIE, $strCookie );
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function makePostUrlForCurl($fieldsArray)
{
    $fields_string="";
    foreach($fieldsArray as $key=>$value) {
        $fields_string .= $key.'='.$value.'&';
    }
    return rtrim($fields_string, '&');
}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function updateUserProfile
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getEducationWorkInfo()
*   Purpose       :   Get user balance details from wallet database by making an API request.
*   Arguments     :   1.(string) $email, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

//function updateUserProfile($user_id, $requestUrl, $userdata,$profile, $flag, $info, $skills, $mode)
//{
//
//    printArr(func_get_args());
//    global  $walletPublicKey, $mode, $NotificationURL;
//    $retArray = array();
//    $header   = array();
//    $userdata=json_encode($userdata);
//
//    echo $userdata;
//    $flag=json_encode($flag);
//
//    /* create signature */
//    if($flag == 0){
//        $apiText = "user_id={$user_id}&publicKey={$walletPublicKey}";
//        $postFields   = $userdata."&publicKey=".urlencode($walletPublicKey);
//    }
//    if($info == "work"){
//
    //        $apiText = "user_id={$user_id}&info={$info}&publicKey={$walletPublicKey}";
    //        //printArr($userdata);
    //        $postFields   = "companyInfo=".$userdata."&skills=".$skills."&publicKey=".urlencode($walletPublicKey);
    //        // echo "postFields "; printArr($postFields);
//    }
//
//    else if($info == "education"){
//
//        $apiText = "user_id={$user_id}&info={$info}&publicKey={$walletPublicKey}";
//        $postFields   = "school_info=".$userdata."&university_details=".$flag."&university=".$mode."&activity=".$skills."&publicKey=".urlencode($walletPublicKey);
//        echo "postFields "; printArr($postFields);
//    }
//
//    echo $apiName      = "user/{$user_id}/{$profile}";
//    $curl_type    = 'PUT';
//
//
//    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);
//
//    /* Check mode type from config (Nonce active or not)*/
//    if($mode == 'production') {
//        $nonce_result = generateSignatureWithNonce($apiText);
//        if(noError($nonce_result)) {
//            $nonce_result        = $nonce_result['errMsg'];
//            $retArray['errCode'] = -1;
//            //$retArray['errMsg']  = $nonce_result['errMsg'];
//            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
//            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
//        } else {
//            $retArray['errCode'] = 2;
//            $retArray['errMsg']  = $nonce_result['errMsg'];
//        }
//    }
//
//    /* Making curl request */
//    $respArray = curlRequest($curlReqParam, $headers);
//    printArr($respArray);
//    if (noError($respArray)) {
//        if ($mode == 'production') {
//            $nonce_counter = incrementVectorCounter();
//        }
//        $retArray['errCode'] = -1;
//        $retArray['errMsg']  = $respArray['errMsg'];
//        $retArray['mode']    = $mode;
//
//    } elseif ($respArray["errCode"] != 73) {
//        if ($mode == 'production') {
//            $nonce_counter = incrementVectorCounter();
//        }
//        $retArray['errCode'] = $respArray['errCode'];
//        $retArray['errMsg']  = $respArray['errMsg'];
//    } else {
//        $retArray['errCode'] = $respArray['errCode'];
//        $retArray['errMsg']  = $respArray['errMsg'];
//    }
//
//    return $retArray;
//}


/*
*-----------------------------------------------------------------------------------------------------------
*    Function getEducationWorkInfo
*-----------------------------------------------------------------------------------------------------------
*   Function Name :   getEducationWorkInfo()
*   Purpose       :   Get user balance details from wallet database by making an API request.
*   Arguments     :   1.(string) $email, 2.(string) $user_required_fields.
*   Returns       :   (array) $retArray User info array according to required fileds passed.
*/

function getEducationWorkInfo($user_id)
{
    global  $walletPublicKey, $mode, $NotificationURL;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText      = "user_id={$user_id}&publicKey={$walletPublicKey}";
    $postFields   = $apiText;
    $requestUrl    = $NotificationURL;
    $apiName      = "v2/user/{$user_id}/eduworkinfo";
    $curl_type    = 'GET';

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg']  = $nonce_result['errMsg'];
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);
    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}

function  updateUserProfile($userId, $apiTextReq, $requestType, $apiURLEndPoint, $requestParameter){
    global  $walletPublicKey, $mode, $NotificationURL;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText      = "{$apiTextReq}&publicKey={$walletPublicKey}";
    $postFields   = $requestParameter."&publicKey=".urlencode($walletPublicKey);
    $requestUrl    = $NotificationURL;
    $apiName      = 'v2/user/'.$userId.'/'.$apiURLEndPoint;
    $curl_type    = $requestType;

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;

}


function setUserDetails($userId, $apiTextReq, $requestType, $apiURLEndPoint, $requestParameter){
    global  $walletPublicKey, $mode, $walletURL;
    $retArray = array();
    $header   = array();

    /* create signature */
    $apiText      = "{$apiTextReq}&publicKey={$walletPublicKey}";
    $postFields   = $requestParameter."&publicKey=".urlencode($walletPublicKey);
    $requestUrl    = $walletURL;
    $apiName      = "user/{$userId}/{$apiURLEndPoint}";
    $curl_type    = $requestType;

    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curl_type);

    /* Check mode type from config (Nonce active or not)*/
    if($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if(noError($nonce_result)) {
            $nonce_result        = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            $headers[]           = "x-ts: {$nonce_result['timestamp']}";
            $headers[]           = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg']  = $nonce_result['errMsg'];
        }
    }

    /* Making curl request */
    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } elseif ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }

    return $retArray;
}


function uploadProfileImage($accountHandle, $imgName){

    global $docRoot;
    $returnArr = array();

    $imgUploadPath = $docRoot.'images/social/users/'.$accountHandle;
    $profilePicName = $accountHandle.'_'.$imgName;

    if(!is_dir($imgUploadPath)){
        $imgUploadPath = $imgUploadPath."/profile/";
        mkdir($imgUploadPath, 0777, true);
    }else{
        $imgUploadPath = $imgUploadPath."/profile/";
      if(!is_dir($imgUploadPath)){
          mkdir($imgUploadPath, 0777, true);
      }
    }

    $targetUploadDir = $imgUploadPath.$profilePicName;

    $uploadImg =  move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $targetUploadDir);
    if($uploadImg == 1){
        $returnArr["errCode"] = -1;
        $returnArr["errMsg"] = "Profile image upload successfully";
    }else{
        $returnArr["errCode"] = 1;
        $returnArr["errMsg"] = "Error: Uploading profile pic";
    }

    return $returnArr;
}


function updateFirstTimeUserStatus($conn,$email, $id, $page,$type=null,$acctHandle=null){
    $retArray = array();
    $query = '';
    $userNotExist = true;

    if($page == 'profile'){
        $checkUser = is_record_exist($conn, 'first_time_user', $email);
        if(noError($checkUser)){
            $recCount = mysqli_num_rows($checkUser['errMsg']);
            if($recCount > 0){
                $userNotExist = false;
            }else{
                if($type == 'ftUser_old'){
                    $query = "insert into first_time_user(`user_id`,`user_email`,`account_handle`,`timestamp`) VALUES ('".$id."','".$email."','".$acctHandle."',now()); ";
                }else{
                    $query = "insert into first_time_user(`user_id`,`user_email`,`timestamp`) VALUES ('".$id."','".$email."',now()); ";
                }
            }

        }else{
            $retArray["errCode"] = 21;
            $retArray["errMsg"]  = "Error getting user existence.";
        }

    }else if($page == 'follow'){
        $query = "update first_time_user SET `user_follow` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }else if($page == 'claim'){
        $query = "update first_time_user SET `kwd_claim` = 1 , `ftue_status` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }else if($page == 'like'){
        $query = "update first_time_user SET `like_status` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }else if($page == 'create'){
        $query = "update first_time_user SET `post_status` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }else if($page == 'share'){
        $query = "update first_time_user SET `share_post` = 1 , `ftue_status` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }else if($page == 'share_skip'){
        $query = "update first_time_user SET `ftue_status` = 1 where `user_email` = '".$email."' order by timestamp desc limit 1; ";
    }

    if($userNotExist){
        $result = runQuery($query, $conn);
        //printArr($result);
        if(noError($result)){
            $retArray["errCode"] = -1;
            $retArray["errMsg"]  = "Success: update first time user status";
        }else{
            $retArray["errCode"] = 11;
            $retArray["errMsg"]  = "Error: update first time user status";
        }
    }else{
        $retArray["errCode"] = 101;
        $retArray["errMsg"]  = "already updated";
    }

    return $retArray;
}

function getFirstTimeUserStatus($conn,$email,$id){
    $retArray = array();
    $query = "select * from first_time_user where `user_id` = '".$id."' and user_email = '".$email."' order by timestamp desc limit 1; ";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $retArray["errCode"] = -1;
        $retArray["errMsg"]  = $result['dbResource'];
    }else{
        $retArray["errCode"] = 11;
        $retArray["errMsg"]  = "Error: getting first time user status";
    }
    return $retArray;
}

function is_record_exist($conn, $table, $email){
    $retArray = array();

    $query = "select * from $table where user_email = '".$email."' order by timestamp desc limit 1; ";
    $result = runQuery($query, $conn);
    if(noError($result)){
        $retArray["errCode"] = -1;
        $retArray["errMsg"]  = $result['dbResource'];
    }else{
        $retArray["errCode"] = 11;
        $retArray["errMsg"]  = "Error: getting first time user status";
    }
    return $retArray;
}


function postWalletDataToNotification($postParameter) {
  // printArr($postParameter);
  global $mode, $NotificationURL, $walletPublicKey;
  $retArray     = array();

// [user_id] => 627cd06e791aa592ce32c60ef7b862174b9a6b01
//     [email] => d10@grr.la
//     [first_name] => sudeep
//     [last_name] => makwanad10
//     [ref_email] => 
//     [my_referral_id] => 4vgt5
//     [refered_code] => 
//     [gender] => male
//     [creationTime] => 1495180968772
//     [country] => Afghanistan
//     [city] => 'Ali Khayl
//     [client_IP] => ::1
//     [last_used_ip] => 
//     [active] => 
//     [account_handle] => ddddd10
//     [user_last_active_time] => 
//     [notify_options_fk_key] => 
  
// first_name:first_name,
// last_name:last_name,
// last_used_ip:[{ip:client_IP,status:1}],
// active:1,
// account_handle:account_handle,
// user_last_active_time:creationTime,


  // if (isset($userId) && !empty($userId) && isset($category) && !empty($category)) {
    $headers      = array();
    $dataNow      = round(microtime(true) * 1000);
    //create signature
    $apiText      = 'user_id=' . $postParameter['user_id'] . '&email=' . $postParameter['email'] . '&ref_email=' . $postParameter['ref_email'] . '&my_referral_id=' . $postParameter['my_referral_id'] . '&refered_code=' . $postParameter['refered_code'] . '&client_ip=' . $postParameter['client_IP'] . '&gender=' . $postParameter['gender'] . '&country=' . $postParameter['country'] . '&city=' . $postParameter['city'] . '&creationTime=' . $postParameter['creationTime'] . '&publicKey=' . $walletPublicKey;

    $postFields   = 'user_id=' . $postParameter['user_id'] . '&email=' . $postParameter['email'] . '&ref_email=' . $postParameter['ref_email'] . '&my_referral_id=' . $postParameter['my_referral_id'] . '&refered_code=' . $postParameter['refered_code'] . '&client_ip=' . $postParameter['client_IP'] . '&gender=' . $postParameter['gender'] . '&country=' . $postParameter['country'] . '&city=' . $postParameter['city'] . '&creationTime=' . $postParameter['creationTime'] . '&first_name=' . $postParameter['first_name'] . '&last_name=' . $postParameter['last_name'] . '&last_used_ip=' . $postParameter['last_used_ip'] . '&active=' . $postParameter['active'] . '&account_handle=' . $postParameter['account_handle'] . '&user_last_active_time=' . $postParameter['user_last_active_time'] . '&publicKey=' . $walletPublicKey;
    $apiName      = 'v2/user/insertUser';
    $requestUrl   = "{$NotificationURL}";
    $curlType     = 'POST';
    $curlReqParam = array('apiName' => $apiName, 'apiText' => $apiText, 'requestUrl' => $requestUrl, 'curlPostFields' => $postFields, 'curlType' => $curlType);
    //print_r($curlReqParam);die;
    if ($mode == 'production') {
        $nonce_result = generateSignatureWithNonce($apiText);
        if (noError($nonce_result)) {
            $nonce_result = $nonce_result['errMsg'];
            $retArray['errCode'] = -1;
            //$retArray['errMsg'] = $nonce_result['errMsg'];
            $headers[] = "x-ts: {$nonce_result['timestamp']}";
            $headers[] = "x-cnonce: {$nonce_result['cnonce']}";
        } else {
            $retArray['errCode'] = 2;
            $retArray['errMsg'] = $nonce_result['errMsg'];
        }
    }

    $respArray = curlRequest($curlReqParam, $headers);

    if (noError($respArray)) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = -1;
        $retArray['errMsg']  = $respArray['errMsg'];
        $retArray['mode']    = $mode;

    } else if ($respArray["errCode"] != 73) {
        if ($mode == 'production') {
            $nonce_counter = incrementVectorCounter();
        }
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    } else {
        $retArray['errCode'] = $respArray['errCode'];
        $retArray['errMsg']  = $respArray['errMsg'];
    }
  // } else {
  //     $retArray["errCode"] = 2;
  //     $retArray["errMsg"]  = "All fields are mandatory";
  // }

  return $retArray;
}

function getFirstTimeUserType($userCreationTime){
    global $keywoLaunchDate;

    $launchDate = strtotime($keywoLaunchDate) * 1000;

    $userType = '';

    if($userCreationTime < $launchDate){
        $userType = 'ftUser_old';
    }else{
        $userType = 'ftUser_new';
    }

    return $userType;
}

function getMaintainenceMsg($conn){

    $returnArr = array();
    $query = "SELECT message from maintenance_mode where status =1 order by last_updated_by DESC limit 1";
    $currResult = runQuery($query, $conn);
    if(noError($currResult)){
        while($row = mysqli_fetch_assoc($currResult["dbResource"])){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $row;
        }
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = $currResult["errMsg"];
    }

    return $returnArr;
}

function checkCount($conn){

    $returnArr = array();
    $query = "SELECT count(*) as count from maintenance_mode where status =1 order by last_updated_by DESC limit 1";
    $currResult = runQuery($query, $conn);
    if(noError($currResult)){
        while($row = mysqli_fetch_assoc($currResult["dbResource"])){
            $returnArr["errCode"] = -1;
            $returnArr["errMsg"] = $row;
        }
    }else{
        $returnArr["errCode"] = 2;
        $returnArr["errMsg"] = $currResult["errMsg"];
    }

    return $returnArr;
}


/**
 * @param $date
 * @param $timezone
 * @param $format
 * @return false|string
 */
function uDateTime($format,$date)
{


    if ($format == "") {
        $format = "d-m-Y h:i:s A";
    }

    if ($date == "") {
        $date = date($format);
    }
    $timezone=$_SESSION["timezone"];

    if ($timezone == "") {
        $timezone = "UTC";
    }

    $dateTimeZonec = new DateTimeZone($timezone);
    $dateTimeZone = new DateTime("now", $dateTimeZonec);
    $timeOffset = $dateTimeZonec->getOffset($dateTimeZone);
    if (strpos($date, '-') !== false || strpos($date, '/') !== false) {
        $offset_time = strtotime($date) + $timeOffset;
    } else {
        $offset_time = strtotime(date($format, $date / 1000)) + $timeOffset;
    }
    $finalDate = date($format, $offset_time);
    return $finalDate;
}
?>
