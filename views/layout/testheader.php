
<?php

if(!isset($_SESSION))
{
    session_start();
}
header('Content-Type: text/html; charset=utf-8');
echo "<!DOCTYPE html>";

// $docrootpath=$_SERVER["DOCUMENT_ROOT"]."/keywo_php/";

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

// Include dependent files
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}models/search/searchResultModel.php");
require_once("{$docrootpath}models/header/headerModel.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}models/wallet/walletKYCModel.php");
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");

// if user verify its account which is already activated then show below error messages.
error_reporting(0);
$errMsg = '';
if (isset($_POST) && !empty($_POST)) {
    if ($_POST["errCode"] == 37 || $_POST["errCode"] == 999) {
        $errMsg = "Account is already activated";
    }
}


$kwdConn = createDbConnection('dbkeywords');
if (noError($kwdConn)) {
    $kwdConn = $kwdConn["connection"];
} else {
    print("Error: Database connection");
    exit;
}

$getRemoteIP = getClientIP();

$connSearch = createDbConnection('dbsearch');
if (noError($connSearch)) {
    $connSearch = $connSearch["connection"];
} else {
    print("Error: Database connection");
    exit;
}


$email = $_SESSION["email"]; //echo $email;
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $email = $_SESSION["email"];
    $cilentSessionId = session_id();
    $loginStatus = 1;
    $userFteStatusFlag = false;

    $getUserFteStatus = getFirstTimeUserStatus($kwdConn,$email,$_SESSION['id']);
    if (noError($getUserFteStatus)) {
        $userFteStatusCount = mysqli_num_rows($getUserFteStatus["errMsg"]);
        if ($userFteStatusCount > 0) {
            $row = mysqli_fetch_assoc($getUserFteStatus["errMsg"]);
            $userFteStatusRes = $row['ftue_status'];
            $_SESSION['userFteStatusFlag'] = $userFteStatusRes;
            if (!empty($userFteStatusRes) && $userFteStatusRes != 0) {
                $userFteStatusFlag = true;
            } else {
                $userFteStatusFlag = false;
            }
        } else {
            $userFteStatusFlag = false;
        }
        $userFteStatus = $res['ftue_status'];
    } else {
        $userFteStatusFlag = false;
    }






    if ($userFteStatusFlag == false) {
        $userRequiredFields = "_id,first_name,last_name,email,password,creationTime,salt,first_buy_status,account_handle,my_referral_id,ref_email,refered_code,gender,country,city,client_IP,active";
        $getUserWalletData = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
        if (noError($getUserWalletData)) {
            $getUserWalletData = $getUserWalletData['errMsg'];
            $postParameters = array(
                "user_id"               => "{$getUserWalletData['_id']}",
                "email"                 => "{$getUserWalletData['email']}",
                "first_name"            => "{$getUserWalletData['first_name']}",
                "last_name"             => "{$getUserWalletData['last_name']}",
                "ref_email"             => "{$getUserWalletData['ref_email']}",
                "my_referral_id"        => "{$getUserWalletData['my_referral_id']}",
                "refered_code"          => "{$getUserWalletData['refered_code']}",
                "gender"                => "{$getUserWalletData['gender']}",
                "creationTime"          => "{$getUserWalletData['creationTime']}",
                "country"               => "{$getUserWalletData['country']}",
                "city"                  => "{$getUserWalletData['city']}",
                "client_IP"             => "{$getUserWalletData['client_IP']}",
                "last_used_ip"          => "",
                "active"                => "",
                "account_handle"        => "{$getUserWalletData['account_handle']}",
                "user_last_active_time" => "",
                "notify_options_fk_key" => "",
            );
            $postWalletDataToNotification = postWalletDataToNotification($postParameters);
            // printArr($postWalletDataToNotification);
        }
    }

    // get user details with available balance if login

    $userAvailableBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,kyc_current_level,mobile_number,security_preference,social_affiliate_earnings";
    $getUserDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userAvailableBalance);

    if (noError($getUserDetails)) {
        $_SESSION['security_preference'] = $getUserDetails['errMsg']['security_preference'];
        if(!isset($getUserDetails['errMsg']['security_preference'])){
            $_SESSION['security_preference'] = 1;
        }
        $userCurrencyPreference = $getUserDetails['errMsg']['currencyPreference'];
        $kyc_current_level = $getUserDetails['errMsg']['kyc_current_level'];
        $mobile_number = $getUserDetails['errMsg']['mobile_number'];
        $getUserBalance = calculateUserBalance($getUserDetails);
        if (noError($getUserBalance)) {
            $userBalance = $getUserBalance['errMsg']['total_available_balance'];
        } else {
            $userBalance = "Error";
        }
    } else {
        $userBalance = "Error";
    }

    /* Get user pending interaction, System Mode, Unread Notification Count */

    $userPendingInteraction = "no_of_qualified_interactions_pending,kyc_current_level,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic,trend_preference";
    $getUserInteractionDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userPendingInteraction);

    if (noError($getUserInteractionDetails)) {

        /* User Pending Interaction Details */
        $UserPendinGinteractionCount = $getUserInteractionDetails['errMsg']['no_of_qualified_interactions_pending'];

        /* 2FA preference */
        $securityPreference = $getUserInteractionDetails['errMsg']['trend_preference'];

        /* User System Mode Code */
//        $userSystemMode = $getUserInteractionDetails['er  rMsg']['system_mode'];
        $userSystemMode = 2;
        //$userCurrencyPreference = $getUserInteractionDetails['errMsg']['currencyPreference'];

        /* set user currency in session */

        if(empty($userCurrencyPreference)){
            $_SESSION['CurrPreference'] = 'USD';
        }else{

            if ($userCurrencyPreference == 'USD/US Dollar') {
                $setCurrPreference = explode('/', $userCurrencyPreference);
                $_SESSION['CurrPreference'] = $setCurrPreference[0];
                /* For testing */

                $_SESSION['CurrPreference'] = 'INR';
            } else {
                $_SESSION['CurrPreference'] = $userCurrencyPreference;
            }
        }

        if (!isset($_SESSION['systemMode']) && empty($_SESSION['systemMode'])) {
            $_SESSION['systemMode'] = $userSystemMode;
        }

        if ($userSystemMode == 1) {
            $userDefaulsSearchAppDetail = getUserDefaultAppDetails($email, $connSearch);
            if (noError($userDefaulsSearchAppDetail)) {
                $userDefaulsSearchAppDetail = $userDefaulsSearchAppDetail['errMsg'];
                $systemModeRedirection = $rootUrlSearch . $userDefaulsSearchAppDetail['appLandingUrl'];
            }
        } else if ($userSystemMode == 2) {
            $systemModeRedirection = $rootUrl . "views/social/index.php";
        }


        /*   // To get System Default mode on signup process
           if (empty($userSystemMode)) {
               $getUserSystemMode = getSettingsFromSearchAdmin($connSearch);
               if(noError($getUserSystemMode)) {
                   $getUserSystemMode = $getUserSystemMode['data'];
                   $userSystemMode = $getUserSystemMode['default_system_mode'];



                   if ($userSystemMode == 1) {
                       $userDefaulsSearchAppDetail = getUserDefaultAppDetails($email, $connSearch);
                       if (noError($userDefaulsSearchAppDetail)) {
                           $userDefaulsSearchAppDetail = $userDefaulsSearchAppDetail['errMsg'];
                           $systemModeRedirection = $rootUrlSearch . $userDefaulsSearchAppDetail['appLandingUrl'];
                       }
                   } else if ($userSystemMode == 2) {
                       $systemModeRedirection = $rootUrl . "views/social/index.php";
                   }
               }
           }*/

        /* Unread Notification Detail */
        $notifyCount = $getUserInteractionDetails['errMsg']['unread_notification_count'];

        /* Counting No of Unread Notification to display */
        if ($notifyCount > 5) {
            $notificationCount = "5+";
            $notifyCallCount = $notifyCount;
        } else if ($notifyCount > 0 && $notifyCount <= 5) {
            $notificationCount = $notifyCount;
            $notifyCallCount = $notifyCount;
        } else {
            $notificationCount = 0;
            $notifyCallCount = 0;
        }

        // get IP count
        $getRemoteIPCount = getIPInfo($getRemoteIP);
        if(noError($getRemoteIPCount)){
            $getRemoteIPCount = $getRemoteIPCount["errMsg"]["ip_count"];
        }else{
            print('Error: Fetching ip details');
            exit;
        }

        // get admin setting details from search database
        $getAdminSetting = getSettingsFromSearchAdmin($connSearch);
        if(noError($getAdminSetting)){
            $getAdminSetting = $getAdminSetting['data'];
            $ipMaxLimit = $getAdminSetting['IPMaxLimit'];
        }else{
            print('Error: Fetching settings details');
            exit;
        }

        // get user search type
        $getSearchType = getSearchType($email, $userPendingInteraction);
        if (noError($getSearchType)) {
            $getSearchType = $getSearchType["data"];
            $userSearchType = $getSearchType["user_search_type"];

            if ($userSearchType == "Qualified" && $getRemoteIPCount <= $ipMaxLimit) {
                $_SESSION["likeIconDisableStatus"] = 0;
                $pendingInteractionStyle           = "color:rgb(9, 250, 9)";
            } else {
                $_SESSION["likeIconDisableStatus"] = 1;
                $pendingInteractionStyle           = "color: red";
            }

        } else {
            print('Error: Fetching user interation type');
            exit;
        }
    } else {
        $UserPendinGinteractionCount = "Error";
        $notificationCount = 'ERROR';
    }
} else {
    $loginStatus = 0;
}

/* Get user cart count for display on cart icon */
$userCartCount = '';
$userCartDetails = getUserCartDetails($email, $kwdConn);
if (noError($userCartDetails)) {
    $user_cart = $userCartDetails["errMsg"]["user_cart"];
    $user_cart = json_decode($user_cart, TRUE);
    $userCartCount = COUNT($user_cart);
} else {
    $retArray["errCode"] = 2;
    $retArray["errMsg"] = "Error getting cart details";
}

?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="<?php echo $rootUrlImages; ?>Keywo_favicon.ico" type="image/x-icon">
    <title>KEYWO</title>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app.css?<?php echo date('l jS \of F Y h:i:s A');  ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_keyword.css?<?php echo date('l jS \of F Y h:i:s A');  ?>" type="text/css" />
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css?<?php echo date('l jS \of F Y h:i:s A');  ?>" type="text/css" />
    <!--=======================================
      =            date range picker            =
      ========================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo $rootUrl; ?>frontend_libraries/dp-range/daterangepicker.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $rootUrl; ?>frontend_libraries/star-rate-lib/css/star-rating.min.css" />
    <!--====  End of date range picker  ====-->
    <script src="<?php echo $rootUrl; ?>frontend_libraries/jquery/jquery-1.12.4.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo $rootUrl; ?>frontend_libraries/tzDetector/jstz.js"></script>
</head>
<?php

// get currency exchange rate of usd, sgd and btc
$exchangeRateArray = $exchageCurrencyCode;
if($_SESSION['CurrPreference'] != "")
{
    $usercurrPrefrence=strtoupper($_SESSION['CurrPreference']);
}else{
    $usercurrPrefrence= "USD";
}

array_push($exchangeRateArray,$usercurrPrefrence);
$getExchangeRate = getCurrencyExchangeRate($exchangeRateArray, $connSearch);

if (noError($getExchangeRate)) {
    $getExchangeRate = $getExchangeRate["exchange_rate"];
    $currentExchangeRate = json_encode($getExchangeRate);

    echo "<script> var exchangeRate = " . json_encode($getExchangeRate) . "; </script>";
    echo "<script> var userCurrPref = '" . strtoupper($_SESSION['CurrPreference']) . "'; </script>";

} else {
    print("Error: Fetching current exchange rate.");
    exit;
}

$msgCount=checkCount($kwdConn)["errMsg"]["count"];
if($msgCount>0)
{
    $setpixel="style='margin-top:30px'";
}else
{
    $setpixel="style='margin-top:0px'";

}

?>
<body class="bg-concrete" <?= $setpixel; ?>>
<?php if ($loginStatus == 1) { ?>
    <!-- =============== After Login Header =============== -->
    <header id="login-header-start" class="login_header" user-email="<?php echo $email; ?>">
        <?php
        require_once "{$docrootpath}views/maintainance/maintainence.php";
        ?>
        <div class="container-fluid row-10">
            <div class="row">
                <div class="col-xs-4">
                    <!-- <a onclick="redirectDefaultApp();" class="header-main-logo innerT pull-left" style="display:block;width:117px;">
                        <img src='<?php /*echo $rootUrl; */?>images/keywo_logo.png' alt="keywo logo" style="height:28px;"/></a>-->

                    <a href="<?php echo $rootUrl ?>" class="header-main-logo innerT pull-left" style="display:block;width:117px;">
                        <img src='<?php echo $rootUrl; ?>images/logo.svg' alt="keywo logo" style="height:28px;"/></a>
                    <?php
                    $url = $_SERVER['REQUEST_URI'];
                    if (strpos($url, 'search_result') !== false) {
                        ?>
                        <form id="my_form" class="header-form" action="<?php echo $page; ?>" role="search" onsubmit="return validateEmpty(this);">
                            <div class="pull-left innerML search-box-header-main">
                                <div class="input-group half innerMT forgot-email-icons search-box-header">
                                    <input type="text" id="search_box" name="q" value="<?php echo $keywords; ?>" class="form-control forgot-email-input autoSuggest" placeholder="Enter search keywords" required>
                                    <input type="submit" class="search-btn-header" value="" />
                                    <!-- <span class="glyphicon glyphicon-search" style="color:#0b628f"
                                      aria-hidden="true"></span> -->
                                </div>
                            </div>
                        </form>
                    <?php } ?>
                </div>
                <div class="col-xs-3">
                    <div class="half innerT text-center">
                        <h4 class="text-white">
                            Balance : <span id="available-balance"
                                            onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                            origPrice="<?php echo number_format("{$userBalance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$userBalance}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$userBalance}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                            <!--<span>IT$</span>-->
                            <span class="innerL" id="pending-interaction"
                                  style="<?php echo $pendingInteractionStyle; ?>"><?php echo $UserPendinGinteractionCount; ?></span>
                        </h4>
                    </div>
                </div>
                <div class="col-xs-5 padding-right-none">
                    <div class="half innerT">
                        <ul class="list-inline nav navbar-nav logged_in_header pull-right">
                            <span id="modSwitchId" style="display:none;" value="<?php echo $userSystemMode; ?>"><?php echo $systemModeRedirection; ?></span>
                            <!--  <li class="social-mode-button">
                                <label class="text-white innerT innerR" style="vertical-align:top;font-weight:500;">Social Mode
                                </label>
                                <label class="switch half innerMT">
                                    <input type="checkbox" <?php //if ($userSystemMode==1 ) { echo "checked"; } ?> onclick="changeUserMode('<?php echo $rootUrl; ?>');">
                                    <div class="slider round"></div>
                                </label>
                                <span id="modSwitchId" style="display:none;" value="<?php //echo $userSystemMode; ?>"><?php //echo $systemModeRedirection; ?></span>
                                <span id="analiticPageUrl" style="display:none;" value="<?php //echo $_SERVER['REQUEST_URI'] ?>"><?php //echo $_SESSION['systemMode']; ?></span>
                            </li> -->
                            <?php if ($userFteStatusFlag){ ?>
                                <li class="notification-header" id="demo">
                                    <a href="#" class="header_notification_icon notification-header" data-toggle="tooltip" title="Notifications" data-placement="bottom" data-rootUrltoload="<?php echo $rootUrl; ?>" data-userid="<?php echo $_SESSION["id"]; ?>" data-notifycount="<?php echo $notifyCallCount; ?>" data-category="<?php echo 'all'; ?>">
                                        <img alt="" src="<?php echo $rootUrlImages; ?>header/notification.png"
                                             class="notification-header">
                                    </a>
                                    <?php if ($notifyCount > 0) { ?>
                                        <span class="badge badge-danger">

                                <?php echo $notificationCount; ?>
                                </span>
                                    <?php } ?>
                                    <!-- =================== Top 5 Notifications =================== -->
                                    <div class="notification-container" id="notify-drop">
                                        <div id="notifyDetails"></div>
                                    </div>
                                    <!-- notification-container -->
                                </li>


                                <li class="cart-header">
                                    <input type="hidden" value="<?php echo $userCartCount; ?>" id="cart-count" />
                                    <a href="#" class="header_cart_icon cart-header cartCount" data-toggle="tooltip"
                                       data-placement="bottom"
                                       title="Keyword Cart">
                                        <img alt="" class="cart-header" onclick="openCart('<?php echo $email; ?>');"
                                             src="<?php echo $rootUrlImages; ?>header/cart.png">

                                        <?php if ($userCartCount > 0) { ?>
                                            <span class="badge badge-blue" id="cartTotalCount"> <?php echo $userCartCount; ?></span>
                                        <?php } ?>
                                    </a>
                                </li>
                                <li class="keyword-search-header">
                                    <a href="#" class="header_keywordsearch_icon keyword-search-header" data-toggle="tooltip" data-placement="bottom" title="Keyword Search">
                                        <img alt="" class="keyword-search-header"
                                             src="<?php echo $rootUrlImages; ?>header/keywordsearch.png">
                                    </a>
                                </li>
                                <!-- <li class="manage-apps-header">
                                <a href="#" class="header_manage_apps manage-apps-header" data-toggle="tooltip" data-placement="bottom" title="Choose your Search Engine" onclick="loadSearchApp('<?php echo $_SESSION["email"]; ?>','<?php echo $rootUrl; ?>');">
                                    <img alt="" class="manage-apps-header"
                                         src="<?php echo $rootUrlImages; ?>header/appmarket.png">
                                </a>
                            </li> -->
                                <li class="main-menu-header">
                                    <a href="#" class="header_main_menu main-menu-header" data-toggle="tooltip" data-placement="bottom" title="Main Menu">
                                        <img alt="" class="main-menu-header"P
                                             src="<?php echo $rootUrlImages; ?>header/menubar.png">
                                    </a>
                                </li>
                            <?php } else { ?>
                                <li class="main-menu-header">
                                    <a href="<?php echo $rootUrl; ?>views/user/logout.php" >
                                    <span>
                                        <i title="Logout" data-toggle="tooltip" data-original-title = "Logout" data-placement="bottom" class="fa fa-sign-out static-menu-icons" aria-hidden="true" style = "color:white !important"></i>
                                    </span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div id="notification-bell"></div>
    </header>
    <!-- =================== Cart =================== -->
    <div class="cart-menu" style="display:none; <?= $panelPixel; ?>" >

    </div>
    <!-- cart-menu -->
    <!-- =================== Keyword Search =================== -->
    <?php

    if($_GET["status"] != "H"){
        $searchQueryFinal    = optimizeSearchQuery(cleanXSS($_GET["q"])); //echo $searchQueryFinal;
    }else{
        $searchQueryFinal    = optimizeSearchQuery(urldecode($_GET["q"]));
    }

//    $searchQueryFinal   = str_replace(",", " ", $searchQueryFinal); //echo $searchQueryFinal;

    if(!empty($searchQueryFinal)){
        $searchQueryFinalKeyword    = explode(" ",$searchQueryFinal); //print_r($searchQueryFinalKeyword);
    }else{
        $searchQueryFinalKeyword = array();
    }


    // echo "get";
    ?>

    <div class="keyword-marketplace keyword-search-container" style="display:none;">
        <div class="col-xs-12 padding-none keyword-search-performance" style="height:90%;">
            <!-- analytics search starts here -->
            <!-- mobile-width starts here -->
            <div class="mobile-width">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- search bar starts here -->
                        <div class="row">
                            <div class="col-md-12">
                                <div id="custom-search-input">
                                    <div class="input-group col-md-12 padding-left-none padding-right-none">
                                        <form id="performance_search_form" method="GET">
                                            <input type="text" class="search-query form-control inner-3x innerR ellipses xssValidation" id="performance_search" data-type="1" name="q" placeholder="keyword" value="<?php echo $searchQueryFinal;?>">
                                            <span class="input-group-btn">
                                                    <button class="btn btn-danger" onclick="searchResultPerformance();" type="button">
                                                    <span class=" glyphicon glyphicon-search"></span>
                                                    </button>
                                                    </span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if(!isset($_GET["q"]) && $searchQueryFinal == '')
                        {
                            ?>
                            <div class="performance">
                                <h5> <center>
                                        No Search Keywords</center>
                                </h5>
                            </div>

                        <?php }
                        else
                        {
                        // echo "Data is available";

                        ?>

                        <!-- search bar ends here -->
                        <!-- analytics search row starts here -->
                        <div class="performance">
                            <?php

                            if(!empty($searchQueryFinalKeyword)){
                                $pageUrl = "views/keywords/analytics/keyword_analytics.php";
                                foreach($searchQueryFinalKeyword as $keyNo => $keyword) {

                                    $checkForRevenewAvailability = getRevenueDetailsByKeyword($kwdConn,$keyword);
                                    if(noError($checkForRevenewAvailability)){

                                        $checkForRevenewAvailability = $checkForRevenewAvailability["data"][0];
                                        $totalPaidSearch = $checkForRevenewAvailability["user_kwd_search_count"]; //echo "Paid search ".$totalPaidSearch."<br />";
                                        $totalKeywordEarning = $checkForRevenewAvailability["user_kwd_ownership_earnings"]; //echo "Earning ".$totalKeywordEarning;

                                        $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$kwdConn); //printArr($checkForKeywordAvailability);

                                        //printArr($checkForKeywordAvailability);
                                        if(noError($checkForKeywordAvailability)){

                                            $availabilityFlag = $checkForKeywordAvailability['errMsg'];
                                            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                                            $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
                                            $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
                                            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                                            $CartStatus = $checkForKeywordAvailability['status'];
                                            $activeBids = $checkForKeywordAvailability["active_bids"];

                                            if(isset($totalKeywordEarning) && !empty($totalKeywordEarning)){
                                                $totalKeywordEarning = $totalKeywordEarning;
                                            }else{
                                                $totalKeywordEarning = 0;
                                            }

                                            if(isset($totalPaidSearch) && !empty($totalPaidSearch)){
                                                $totalPaidSearch = $totalPaidSearch;
                                            }else{
                                                $totalPaidSearch = 0;
                                            }

                                            $pageUrl = "views/keywords/analytics/keyword_analytics.php";

                                            if($availabilityFlag == 'keyword_available'){


                                                ?>

                                                <div class="row">
                                                    <div class="col-xs-12 padding-left-none padding-right-none">
                                                        <div class="text-left">

                                                            <div class="row grey-block">
                                                                <div class="col-md-7  padding-left-none padding-right-none">
                                                                    <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                </div>
                                                                <div class="col-md-5  padding-left-none padding-right-none">
                                                                    <span class="more-info-marketp pull-right innerAll"><a href='<?php echo $rootUrl.$pageUrl.$keyword; ?>'>more info</a></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Interaction :</label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo $totalPaidSearch; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Earning : </label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                            origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </div>
                                                    <div class="col-xs-12 text-right">&nbsp;
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <?php if(in_array($keyword, $user_cart)){ ?>
                                                            <input value="Remove" type="button" id="hdr_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $cilentSessionId; ?>', '<?php echo $rootUrl; ?>', 'hdr')" />
                                                        <?php }else{ ?>
                                                            <input value="Add To Cart" id="hdr_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-trading-dark button-text" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $cilentSessionId; ?>', '<?php echo $rootUrl; ?>', 'hdr')" />
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-xs-12">&nbsp;</div>
                                                </div>
                                            <?php }elseif($availabilityFlag == 'keyword_not_available'){

                                                $activeBids = json_decode($activeBids, true);
                                                foreach($activeBids as $key => $bidValue){
                                                    $bidValue = explode('~~', $bidValue);
                                                    $bidderEmail[] = $bidValue[1];

                                                }
                                                if(in_array($email, $bidderEmail)){
                                                    $bidStatus = true;
                                                }else{
                                                    $bidStatus = false;
                                                }
                                                ?>

                                                <div class="row">
                                                    <div class="col-xs-12 padding-left-none padding-right-none">
                                                        <div class="text-left">
                                                            <div class="row grey-block">
                                                                <div class="col-md-7  padding-left-none padding-right-none">
                                                                    <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                </div>
                                                                <div class="col-md-5  padding-left-none padding-right-none">
                                                                    <span class="more-info-marketp pull-right innerAll"><a href='<?php echo $rootUrl.$pageUrl.$keyword; ?>'>more info</a></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Interaction :</label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo $totalPaidSearch; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Earning : </label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                            origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Asking Price :</label>

                                                        <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                origPrice="<?php echo "{$kwdAskPrice} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                        <?php }else {?>
                                                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                        <?php } ?>


                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Current Bid : </label>
                                                        <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                origPrice="<?php echo "{$highestBidAmtForKwd} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                        <?php }else {?>
                                                            <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                        <?php } ?>


                                                    </div>

                                                    <div class="col-xs-12 text-right">&nbsp;
                                                    </div>
                                                    <div class="pull-left col-xs-6 col-md-12">
                                                        <?php if($email == $kwdOwnerId){
                                                            if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>

                                                                <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Ask</button>&nbsp;

                                                                <?php
                                                            }else{
                                                                // set ask
                                                                ?>

                                                                <button class="btn-trading"  value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Set Ask</button>&nbsp;

                                                                <?php
                                                            }

                                                            if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                                ?>

                                                                <button class="btn-trading-dark" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept" >Accept Bid</button>&nbsp;

                                                                <?php
                                                            }
                                                        }else{

                                                            if($CartStatus == "sold"){

                                                                if(empty($activeBids)){
                                                                    ?>

                                                                    <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>&nbsp;

                                                                    <?php
                                                                }else{
                                                                    if($bidStatus){
                                                                        ?>
                                                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Edit Bid</button>&nbsp;

                                                                        <?php
                                                                    }else{
                                                                        ?>

                                                                        <button class="btn-trading" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);" >Place Bid</button>&nbsp;

                                                                        <?php
                                                                    }
                                                                }

                                                                if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                                    ?>
                                                                    <button class="btn-trading-dark"  value="<?php echo $keyword;  ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>', <?php  echo $kwdAskPrice; ?>);"  >Buy Now</button>&nbsp;
                                                                    <?php
                                                                }

                                                            }
                                                        }

                                                        ?>

                                                    </div>
                                                    <div class="col-xs-12">&nbsp;</div>

                                                </div>
                                            <?php }elseif($availabilityFlag == "keyword_blocked"){ ?>

                                                <div class="row">
                                                    <div class="col-xs-12 padding-left-none padding-right-none">
                                                        <div class="text-left">

                                                            <div class="row grey-block">
                                                                <div class="col-md-7  padding-left-none padding-right-none">
                                                                    <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                                </div>
                                                                <div class="col-md-5  padding-left-none padding-right-none">
                                                                    <span class="more-info-marketp pull-right innerAll"><a href='<?php echo $rootUrl.$pageUrl.$keyword; ?>'>more info</a></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Interaction :</label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch} ".$keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <label class="keyword-grey pull-left">Earning : </label>
                                                        <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                            origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;Not Available </span>
                                                    </div>
                                                    <div class="col-xs-12">&nbsp;</div>
                                                </div>

                                            <?php } elseif(empty($searchQueryFinalKeyword)){ ?>
                                                <div>
                                                    <label>No search keywords</label>
                                                </div>
                                            <?php }} elseif($checkForKeywordAvailability["errCode"] == 4){ ?>
                                            <div class="row">
                                                <div class="col-xs-12 padding-left-none padding-right-none">
                                                    <div class="text-left">

                                                        <div class="row grey-block">
                                                            <div class="col-md-7  padding-left-none padding-right-none">
                                                                <h5 class="text-blue keyword-name ellipses margin-bottom-none"><a class="display-in-block-txt-blk" onclick="dialogsbox('<?php echo urlencode($keyword); ?>');" title="<?php echo "#".$keyword; ?>" data-toggle="tooltip" data-placement="bottom" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">#<?php echo $keyword; ?></a></h5>
                                                            </div>
                                                            <div class="col-md-5  padding-left-none padding-right-none">
                                                                <span class="more-info-marketp pull-right innerAll"><a href='<?php echo $rootUrl.$pageUrl.$keyword; ?>'>more info</a></span>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="keyword-grey pull-left">Interaction :</label>
                                                    <span class="keyword-grey-span pull-right">&nbsp;<a href="#" title="<?php  echo "{$totalPaidSearch} ".$keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalPaidSearch}", 0); ?></a></span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <label class="keyword-grey pull-left">Earning : </label>
                                                    <span class="keyword-grey-span pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                        origPrice="<?php echo "{$totalKeywordEarning} {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalKeywordEarning}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalKeywordEarning}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                </div>
                                                <div class="col-xs-12">
                                                    <span class="keyword-grey-span pull-r-marketplace">&nbsp;Not Available </span>
                                                </div>
                                                <div class="col-xs-12">&nbsp;</div>
                                            </div>
                                        <?php }}}}} ?>

                        </div>
                    </div>
                </div>
            </div>



            <div class="col-xs-12 goto-marketplace padding-right-none padding-left-none innerTB half bg-light-purple goto-market-menu" style=" position:absolute; bottom:10%;  ">
                <!-- go to marketplace -->
                <div class="wid-50 pull-left">
                    <i class="fa fa-angle-double-left" aria-hidden="true"></i>
                </div>
                <div class="">
                    <a href="<?php echo $rootUrl; ?>views/keywords/marketplace/keyword_search.php">Go to marketplace</a>
                </div>

                <!-- <div class="col-md-3">
                            <div class="text-left bglightest-grey">
                                <h5 class="text-blue keyword-name text-center inner-2x innerML"><i class="fa fa-angle-double-left pull-left half" aria-hidden="true"></i>
                                </h5>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="text-left bglightest-grey inner-3x innerMT innerML padding-left-none">

                                <span class="goto-marketplace"><a href="<?php echo $rootUrl; ?>views/keywords/marketplace/keyword_search.php">Go to marketplace</a></span>

                            </div>
                        </div> -->


                <!-- go to marketplace -->
            </div>

        </div>


    </div>



    <!-- =================== User Static MEnu =================== -->
    <!-- right side panel 1 -->
    <nav class="navbar navbar-default sidebar" id="static-user-menu" role="navigation"
         style="border:none;display:none;overflow:visible; <?= $panelPixel; ?>">
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <!-- <div class="arrow-up">
            </div> -->
            <ul class="nav navbar-nav">
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/social/user_social/myprofile.php" class="clearfix">
                  <span class="pull-left">
                    <i class="fa fa-user static-menu-icons" aria-hidden="true"></i>
                  </span>
                        <span class="ellipses-general ellipses pull-left">
                    <?php echo $_SESSION['first_name'] . " " . $_SESSION['last_name']; ?>
                  </span>
                        <span>
                    <i class="fa fa-pencil pull-right margin-right-none half innerMT" aria-hidden="true"></i>
                  </span>
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/keywords/user_dashboard/dashboard.php">
                                <span>
                    <i class="fa fa-dashboard static-menu-icons" aria-hidden="true"></i>
                  </span> My Earnings
                    </a>
                </li>
                <!--<li class="text-capitalize">
                    <a href="<?php /*echo $rootUrl; */?>views/keywords/user_dashboard/trading_history.php">
                                <span>
                    <i class="fa fa-dashboard static-menu-icons" aria-hidden="true"></i>
                  </span> Trading History
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php /*echo $rootUrl; */?>views/keywords/user_dashboard/active_trade.php">
                                <span>
                    <i class="fa fa-dashboard static-menu-icons" aria-hidden="true"></i>
                  </span> Active Trade
                    </a>
                </li>-->
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/wallet/index.php">
                        <img src="<?php echo $rootUrlImages ?>rightpanel-img/wallet.png" class="static-menu-icons"/>
                        My Wallet
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/keywords/marketplace/keyword_search.php">
                                <span>
                    <i class="fa fa-line-chart static-menu-icons" aria-hidden="true"></i>
                  </span> Keyword Market
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/communityPool/keywoStats.php">
                        <img src="<?php echo $rootUrlImages ?>rightpanel-img/keywo_stats.png"
                             class="static-menu-icons"/>
                        Keywo Stats
                    </a>
                </li>
                <!--  <li class="text-capitalize">
                    <a href="<?php /*echo $rootUrl; */?>views/apps/apps.php">
                                <span>
                    <i class="fa fa-th static-menu-icons" aria-hidden="true"></i>
                  </span> App Market
                    </a>
                </li>-->
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/social/index.php">
                                <span>
                    <i class="fa fa-th static-menu-icons" aria-hidden="true"></i>
                  </span> Social
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/referral_zone/referralZone.php">
                        <img src="<?php echo $rootUrlImages ?>rightpanel-img/referral_zone.png"
                             class="static-menu-icons"/>
                        Referral Zone
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="#">
                                <span>
                    <i class="fa fa-bullhorn static-menu-icons" aria-hidden="true"></i>
                  </span> AD Manager
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/about/main.php">
                                <span>
                    <i class="fa fa-info-circle static-menu-icons" aria-hidden="true"></i>
                  </span> About Us
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/support/main.php">
                                <span>
                    <i class="fa fa-question-circle static-menu-icons" aria-hidden="true"></i>
                  </span> Help and Support
                    </a>
                </li>
                <li class="text-capitalize">
                    <a href="<?php echo $rootUrl; ?>views/user/logout.php">
                                <span>
                    <i class="fa fa-sign-out static-menu-icons" aria-hidden="true"></i>
                  </span> Logout
                    </a>
                </li>
            </ul>
        </div>
    </nav>
    <!-- =================== Manage Apps =================== -->
    <nav class="navbar navbar-default sidebar" id="manage-app-menu" role="navigation" style="display:none;overflow:visible;">
        <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <div class="app-menu" id="appMenu"></div>
        </div>
    </nav>
    <!-- right side panel 2-->
<?php } else { ?>
    <!-- =============== before login header =============== -->
    <header class="prelogin_header">
        <?php
        require_once "{$docrootpath}views/maintainance/maintainence.php";
        ?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-6">
                    <div>
                        <a href="<?php echo $rootUrl . "views/prelogin/ "; ?>" class="">
                            <img src="<?php echo $rootUrlImages; ?>logo.svg" class=" inner-2x innerMT"
                                 alt="keywo logo"/>
                        </a>
                    </div>
                </div>
                <!-- =============== login form =============== -->
                <div class="col-xs-6">
                    <div class="user_login_form">
                        <form class="pull-right" id="prelogin-form">
                            <div class="row">
                                <div class="col-xs-10 padding-right-none">
                                    <div class="half innerT forgot-Password-font text-right">
                                        <a href="<?php echo $rootUrl . 'views/user/forgotPassword.php'; ?>" class="text-white">Forgot
                                            Password?</a>
                                    </div>
                                </div>
                                <div class="col-xs-2"></div>
                            </div>
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="">
                                        <input type="email" class="form-control inner-3x innerR" id="form_prelogin_login_username" placeholder="Email / Handle">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-xs-5 padding-right-none">
                                    <div class="">
                                        <input type="password" class="restrictPasswordSpace form-control inner-3x innerR" id="form_prelogin_login_password" placeholder="Password">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <div class="col-xs-2">
                                    <input type="submit" value="" id="loginLink" />
                                </div>
                                <!-- col-xs-2 -->
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="msgSuccessError " id="errMsg" style="color:#6ee8fe;">
                                        <?php echo $errMsg; ?>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div id="resend-main-div" class="forgot-Password-font" style="visibility:hidden;">
                                        <a href="<?php echo $rootUrl . 'views/user/resendActivationLink.php'; ?>" class="text-white inner-4x innerL">Resend Activation</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
    </header>


<?php } ?>
<script src="<?php echo $rootUrl; ?>js/marketplace.js"></script>



<!-- tooltip jquery code starts here -->
<script>
    $(document).ready(function(){
// <!-- js for cart close -->
        $(document).mouseup(function (e)
        {
            // console.log(e)
            var container = $(".cart-menu, .keyword-search-container");
            // var container = $(".keyword-search-container");

            if (!container.is(e.target) // if the target of the click isn't the container...
                && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.hide();
            }
        });

// <!-- js for cart close -->
        // $('[data-toggle="tooltip"]').tooltip({
        //     trigger:'hover'
        // });

    });
    var rootUrlForToast = "<?php echo $rootUrl; ?>";
</script>
<?php include("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php"); ?>
<!-- </body> -->
<!-- tooltip jquery code ends here -->
