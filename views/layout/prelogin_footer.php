<footer class="prelogin_footer bg-concrete inner-3x innerT">
		<div class="container">
			<ul class="list-inline text-center margin-bottom-none">
				<li class="footer-li"><a href="javascript:void(0);">©Keywo.com Pte Ltd </a></li>
				<li class="footer-li"><a href="../about/privacy_policy.php" target="_blank">Privacy Policy</a></li>
				<li class="footer-li"><a href="../about/terms_of_use.php" target="_blank">Terms and Conditions</a></li>
				<li class="footer-li"><a href="../about/main.php">About Us</a></li>
				<li class="footer-li"><a href="#">FAQs</a></li>
				<!-- <li class="footer-li"><a href="#">Download App</a></li> -->
				<li class="footer-li"><a href="#">Help & Support</a></li>
			</ul>
		</div>
	</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<!--<script src="--><?php //echo $rootUrlJs; ?><!--jquery-ui.js"></script>-->
<script src="<?php echo $rootUrl; ?>frontend_libraries/jquery/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $rootUrl; ?>bower_components/chosen/chosen.jquery.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/toast/toastr.min.js"></script>
<script src='<?php echo $rootUrl; ?>frontend_libraries/google_recaptcha.js'></script>
<script src="<?php echo $rootUrlJs; ?>common.js"></script>
<script src="<?php echo $rootUrlJs; ?>landing.js"> </script>
<script src="<?php echo $rootUrlJs; ?>auth.js"></script>
<script>
	var rootUrl = "<?php echo $rootUrl; ?>";
	var keywoDefaultCurrency = "<?php echo $keywoDefaultCurrencyName; ?>";

	$(document).ready(function(){
		$('#myCarousel').carousel({
		    interval: 3000
		});
		//$('.chosen-select-deselect').chosen();
		$(".chosen-select-deselect").chosen({
	    enable_split_word_search: false,
	    width: "100%"
	  });
		$(".chosen-search").keyup(function(){
			if($(".chosen-results").children().hasClass("no-results")){
				$(".no-results").text("No results match");
			}
		});

	});


</script>
</body>
</html>
