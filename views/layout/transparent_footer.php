<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--=============================================================================================
=            jQuery                                                                             =
==============================================================================================-->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<script src="<?php echo $rootUrl; ?>frontend_libraries/jquery/jquery-ui.js"></script>
<script src="<?php echo $rootUrl; ?>bower_components/chosen/chosen.jquery.js"></script>     
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap-select/dist/js/bootstrap-select.min.js"></script>      
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/toast/toastr.min.js"></script>
<script src='<?php echo $rootUrl; ?>frontend_libraries/google_recaptcha.js'></script>
<script src="<?php echo $rootUrl; ?>js/auth.js"></script>
<script src="<?php echo $rootUrl; ?>js/common.js"></script>
<script src="<?php echo $rootUrl; ?>js/header.js"></script>
<!--  PLUGIN 1:  dp-range -->
<script src="<?php echo $rootUrl; ?>frontend_libraries/dp-range/moment.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/dp-range/jquery.daterangepicker.min.js"></script>
 <!-- END PLUGIN 1:  dp-range -->
<!--  PLUGIN 2:  bootstrap.daterangepicker -->
<script type="text/javascript" src="<?php echo $rootUrl; ?>frontend_libraries/dp-range/moment.min.js"></script> <!-- comented while checking admanager uI -->
<script type="text/javascript" src="<?php echo $rootUrl; ?>frontend_libraries/datepicker/daterangepicker.js"></script> <!-- comented while checking admanager uI -->
<!--  END PLUGIN 2:  bootstrap.daterangepicker -->
<!--====  End of date range picker  ====-->
<!--





 -->
 <script type="text/javascript" src="<?php echo $rootUrl; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script type="text/javascript" src="<?php echo $rootUrl; ?>frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>

<script type="text/javascript" src="<?php echo $rootUrl; ?>frontend_libraries/star-rate-lib/js/star-rating.min.js
"></script>

<script>
var rootUrl = "<?php echo $rootUrl; ?>";
var keywoDefaultCurrency = "<?php echo $keywoDefaultCurrencyName; ?>";

</script>

<script src="<?php  echo $rootUrl; ?>js/app.js"></script> 

<!-- tooltip jquery code starts here -->
<script>
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();

$(".chosen-select-deselect").chosen({
        enable_split_word_search: false,
        width: "100%"
    });
    $(".chosen-search").keyup(function() {
        if ($(".chosen-results").children().hasClass("no-results")) {
            $(".no-results").text("No results match");
        }
    });
     $(".currencySelect ").selectmenu();
    updateUserTimezone(rootUrl);
});
</script>
<!-- tooltip jquery code ends here -->
