
<footer class="prelogin_footer bg-concrete inner-3x innerT postFooter">
	<div class="container">
		<ul class="list-inline text-center margin-bottom-none">
			<li class="footer-li"><a href="javascript:void(0);">©keywo.com Pvt Ltd</a></li>
			<li class="footer-li"><a href="#">Privacy Policy</a></li>
			<li class="footer-li"><a href="#">Terms and Conditions</a></li>
			<li class="footer-li"><a href="#">About Us</a></li>
			<li class="footer-li"><a href="#">FAQs</a></li>
			<li class="footer-li"><a href="#">Download App</a></li>
			<li class="footer-li"><a href="#">Help</a></li>
			<li class="footer-li"><a href="#">Support</a></li>
		</ul>
	</div>
</footer>


<script src="<?php echo $rootUrl; ?>frontend_libraries/jquery/jquery-ui.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script src="<?php echo $rootUrl; ?>bower_components/chosen/chosen.jquery.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/toast/toastr.min.js"></script>
<script src='<?php echo $rootUrl; ?>frontend_libraries/google_recaptcha.js'></script>
<script src="<?php echo $rootUrlJs; ?>auth.js"></script>
<script src="<?php echo $rootUrlJs; ?>header.js"></script>
<script src="<?php echo $rootUrlJs; ?>app.js"></script>
<script>
	var rootUrl = "<?php echo $rootUrl; ?>";
	var keywoDefaultCurrency = "<?php echo $keywoDefaultCurrencyName; ?>";
	$(document).ready(function(){
		$(".add").click(function() {
       $('<div class="half innerMT"><input type="text"><span class="glyphicon glyphicon-remove rem-btn"></span></div>').appendTo(".contents");
    });
        updateUserTimezone(rootUrl);
	});
	$('.contents').on('click', '.rem-btn', function() {
	  $(this).parent("div").remove();
	});

</script>
</body>
</html>
