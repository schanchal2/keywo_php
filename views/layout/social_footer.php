<?php
  $file = basename($_SERVER['SCRIPT_NAME'], ".php");
?>
<script>
    var socialFileUrl = "<?php echo 'social/' . $file; ?>";
    var maxCharCount = '<?php echo $validateCharCountOnCommentBox; ?>';
    var maxCharReportCount = '<?php echo $validateCharCountOnReportCommentBox; ?>';
    var rootUrl = "<?php echo $rootUrl; ?>";
    var keywoDefaultCurrency = "<?php echo $keywoDefaultCurrencyName; ?>";
</script>
<!--====  End of dropdown to select  ====-->
<script src="<?php echo $rootUrl; ?>frontend_libraries/jquery/jquery-ui.js"></script>
<script src="<?php echo $rootUrl; ?>bower_components/chosen/chosen.jquery.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap-editable/js/bootstrap-editable.min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrap-wysiwyg-master/bootstrap-wysiwyg.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/bootstrapvalidator/js/bootstrapValidator.min.js"></script>
<!--<script src="--><?php //echo $rootUrl; ?><!--frontend_libraries/jquery-validation-1.16.0/dist/jquery.validate.min.js"></script>-->
<script src="<?php echo $rootUrl; ?>frontend_libraries/caret/jquery.caret.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/atwho/jquery.atwho.min.js"></script>
<!-- <script src="<?php // echo $rootUrl; ?>frontend_libraries/jquery.cropbox.js"></script> -->
<script src="<?php echo $rootUrlJs; ?>social_module.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/toast/toastr.min.js"></script>
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBfUpTddoLqdJPqyClo90y7RPGzHfhgAnE"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/gmaps.js"></script>
<!-- <script src="<?php //echo $rootUrl; ?>frontend_libraries/topmsg.js"></script> -->
<script src="<?php echo $rootUrlJs; ?>auth.js"></script>
<script src="<?php echo $rootUrlJs; ?>common.js"></script>
<script src="<?php echo $rootUrlJs; ?>header.js"></script>

<!-- tooltip jquery code starts here -->
<script>
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();
    updateUserTimezone(rootUrl);
});
</script>
<!-- tooltip jquery code ends here -->
