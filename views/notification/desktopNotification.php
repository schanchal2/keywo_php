


<?php
// Include require files
    session_start();
    require_once('../../config/config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/stringHelper.php');
    require_once('../../models/header/headerModel.php');

    error_reporting(0);
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
        $id        = $_SESSION["id"];//cleanXSS($_POST['user_id']);
        $email = $_SESSION["email"];
        $returnArr = array();

        /* Get Notification Details*/
        $getUserUnreadCount = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', 'unread_notification_count');
        if (noError($getUserUnreadCount)) {
            $notificationUnreadCount = $getUserUnreadCount['errMsg']['unread_notification_count'];
            if ($notificationUnreadCount > 0) {
                if ($notificationUnreadCount < 5) {
                    $callNotify = $notificationUnreadCount;
                } else {
                    $callNotify = 5;
                }
                $notificationDetails     = getNotificationDetail($id, 'all', $callNotify, NULL, NULL, NULL);
                // print_r($notificationDetails);
                if (noError($notificationDetails)) {
                    // $unreadNotifyCount   = $notificationDetails['errMsg']['pending_notify_readcount'];
                    // if (empty($unreadNotifyCount)) {
                    //     $unreadNotifyCount = 0;
                    // }
                    $notificationDetails = $notificationDetails['errMsg']['batched_container'];
                    $notifyCount = count($notificationDetails);

                    $returnArr['errCode'] = -1;
                    $returnArr['errMsg'] = $notificationDetails;
                } else {
                    $returnArr['errCode'] = 1;
                    $returnArr['errMsg'] = 'Fail To Load Notification';
                }
            } else {
                $returnArr['errCode'] = 3;
                $returnArr['errMsg'] = 'No Notification';
            }
        } else {
            $returnArr['errCode'] = 2;
            $returnArr['errMsg'] = 'Error in API';
        }
        
    } else {
        $returnArr['errCode'] = 100;
        $returnArr['errMsg'] = 'Please Login';
    }

    echo json_encode($returnArr);

?>