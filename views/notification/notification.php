<?php
session_start();
// Include require files
    require_once('../../config/config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/stringHelper.php');
    require_once('../../models/header/headerModel.php');
    error_reporting(0);
    $id                      = cleanXSS($_POST['user_id']);
    $category                = cleanXSS($_POST['category']);
    $notifyCount             = cleanXSS($_POST['notifyCount']);

/* Get Notification Details*/
if ($notifyCount > 5) {
    $notifyCallCount   = 5;
} else if ($notifyCount > 0 && $notifyCount <= 5) {
    $notifyCallCount   = $notifyCount;
} else {
    $notifyCallCount   = 0;
}
if ($notifyCallCount != 0) {
    $notificationDetails     = getNotificationDetail($id, $category, $notifyCallCount, NULL, NULL, NULL);
    if (noError($notificationDetails)) {
        $unreadNotifyCount   = $notificationDetails['errMsg']['pending_notify_readcount'];
        if (empty($unreadNotifyCount)) {
            $unreadNotifyCount = 0;
        }
        $notificationDetails = $notificationDetails['errMsg']['batched_container'];
        $notifyCount = count($notificationDetails);
    } else {
      $notifyCount   = 'Error';
    }
} else {
    $notificationDetails = "";
    $unreadNotifyCount = 0;
    $notifyCount = 0;
}
// print_r($notificationDetails);
?>
    <div class="panel margin-none border-none row-10">
        <!-- Default panel contents -->
        <div class="panel-heading border-bottom">
            <div class="row">
                <div class="col-xs-6">
                    Notification <span class="text-color-orange">(<?php echo $unreadNotifyCount; ?>)</span>
                </div>
                <div class="col-xs-6">
                    <div class="pull-right  "><a href="<?php echo $rootUrl; ?>views/social/allNotification.php" target="blank" title="Notifications"><i class="fa fa-inbox"></i></a></div>
                </div>
            </div>
        </div>
        <div class="panel-body padding-none ">
            <ul class="list-unstyled row">
                <?php if ($notifyCount <= 0) { ?>
                <li class="col-xs-12">
                    <div class="row innerLR">
                        <div class="col-xs-12 no-notify">
                            <div class="notification__text innerT half">
                               None so far!
                            </div>
                        </div>
                    </div>
                    <!--<div class="notification__time text-right innerB half"> 10 min ago</div>-->
                </li>
                <?php } else {
                    foreach ($notificationDetails as $key => $value) { ?>
                <li class="col-xs-12">
                    <div class="row innerLR">
                        <div class="col-xs-12">
                            <div class="notification__text innerT half ">
                                <?php echo $value['notification_body']; ?>
                            </div>
                            <div class="notification__time text-right innerB half text-grayscale-6">
                                <?php

                                   $mil = $value['created_at'];
                                    $seconds = $mil / 1000;
                                    $created_at         =  uDateTime("h:i A",$value['created_at'])." - ".uDateTime("d M Y",$value['created_at']);



                                   echo $created_at;
                                    ?>
                            </div>
                        </div>
                    </div>
                </li>
                <?php
                    } // End of For Loop
                  } //End of If condition
                ?>
            </ul>
        </div>
        <?php if ($unreadNotifyCount > 5) { ?>
        <div class="panel-footer text-center bg-white padding-none">
            <a class="btn" href="<?php echo $rootUrl; ?>views/social/allNotification.php" target="blank">View all</a>
        </div>
        <?php } ?>
    </div>
    <script type="text/javascript">
    // function ellipsizeTextBox() {
    //     $.each($(".notification__text a"), function(index, val) {
    //         /* iterate through array or object */
    //         var el = $(this);
    //         var wordArray = el.innerHTML.split(' ');
    //         while (el.scrollHeight > el.offsetHeight) {
    //             wordArray.pop();
    //             el.innerHTML = wordArray.join(' ') + '...';
    //         }
    //     });
    // }
    // ellipsizeTextBox();
    </script>
