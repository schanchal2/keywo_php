


<?php
// Include require files
    require_once('../../config/config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/stringHelper.php');
    require_once('../../models/header/headerModel.php');

    error_reporting(0);
$id                      = cleanXSS($_POST['user_id']);
$category                = cleanXSS($_POST['category']);
$notifyCount             = cleanXSS($_POST['notifyCount']);

/* Get Notification Details*/
if ($notifyCount > 5) {
    $notifyCallCount   = 5;
} else if ($notifyCount > 0 && $notifyCount <= 5) {
    $notifyCallCount   = $notifyCount;
} else {
    $notifyCallCount   = 0;
}
if ($notifyCallCount != 0) {
    $notificationDetails     = getNotificationDetail($id, $category, $notifyCallCount, NULL, NULL, NULL);
    if (noError($notificationDetails)) {
        $unreadNotifyCount   = $notificationDetails['errMsg']['pending_notify_readcount'];
        if (empty($unreadNotifyCount)) {
            $unreadNotifyCount = 0;
        }
        $notificationDetails = $notificationDetails['errMsg']['batched_container'];
        $notifyCount = count($notificationDetails);
    } else {
      $notifyCount   = 'Error';
    }
} else {
    $notificationDetails = "";
    $unreadNotifyCount = 0;
    $notifyCount = 0;
}
// print_r($notificationDetails);
?>
    <div class="panel margin-none border-none">
        <!-- Default panel contents -->
        <div class="panel-heading border-bottom">
            <div class="row">
                <div class="col-xs-6">
                    Notification <span class="text-color-orange">(14)</span>
                </div>
                <div class="col-xs-6">
                    <div class="pull-right"> Mark all as read <i class="fa fa-gear"></i></div>
                </div>
            </div>
        </div>
        <div class="panel-body padding-none ">
            <ul class="list-unstyled">
                <?php for ($i=0; $i < 2 ; $i++) { ?>
                <li class="innerLR">
                    <div class="notification__text innerT half ">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, tempore?
                    </div>
                    <div class="notification__time text-right innerB half"> 10 min ago</div>
                </li>
                <?php  } ?>
                <li class="innerLR bg-color-grayscale-be">
                    <div class="notification__text innerT half">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, tempore?
                    </div>
                    <div class="notification__time text-right innerB half"> 10 min ago</div>
                </li>
                <?php for ($i=0; $i < 2 ; $i++) { ?>
                <li class="innerLR innerT">
                    <div class="notification__text innerT half">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores, tempore?
                    </div>
                    <div class="notification__time text-right innerB half"> 10 min ago</div>
                </li>
                <?php  } ?>
            </ul>
        </div>
        <div class="panel-footer text-center bg-white padding-none">
            <button class="btn btn-link" href="#" role="button">View all</button>
        </div>
    </div>
