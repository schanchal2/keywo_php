<?php
// Include require files
	require_once('../../config/config.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../models/header/headerModel.php');

	error_reporting(0);
$id       				 = cleanXSS($_POST['user_id']);
$category 				 = cleanXSS($_POST['category']);
$notifyCount 			 = cleanXSS($_POST['notifyCount']);

/* Get Notification Details*/
if ($notifyCount > 5) {
    $notifyCallCount   = 5;
} else if ($notifyCount > 0 && $notifyCount <= 5) {
    $notifyCallCount   = $notifyCount;
} else {
    $notifyCallCount   = 0;
}
if ($notifyCallCount != 0) {
	$notificationDetails 	 = getNotificationDetail($id, $category, $notifyCallCount, NULL, NULL, NULL);
    if (noError($notificationDetails)) {
    	$unreadNotifyCount 	 = $notificationDetails['errMsg']['pending_notify_readcount'];
    	if (empty($unreadNotifyCount)) {
    		$unreadNotifyCount = 0;
    	}
		$notificationDetails = $notificationDetails['errMsg']['batched_container'];
		$notifyCount = count($notificationDetails);
    } else {
      $notifyCount 	 = 'Error';
    }
} else {
	$notificationDetails = "";
	$unreadNotifyCount = 0;
	$notifyCount = 0;
}
// print_r($notificationDetails);
?>
    <div class="notif-triangle">
        <div class="triangle-up"></div>
    </div>
    <!-- notif-triangle -->
    <div class="notif-content">
        <div class="pull-left">
            <strong>Unread notifications ( <?php echo $unreadNotifyCount; ?> ) </strong>
        </div>
        <!--notif-content  -->
        <div class="pull-right">
            <a href="#">
		<img src='<?php echo "$rootUrlImages"?>notification_settings.png' style="width: 17px;cursor: pointer;" data-toggle="tooltip" title="" data-container="body" data-placement="bottom" data-original-title="Notification Setting">
	</a>
        </div>
    </div>
    <div class="notif-list-main">
        <?php if ($notifyCount <= 0) { ?>
        <div class="col-xs-12 notif-list-content" style="">No Notifications</div>
        <?php } else { foreach ($notificationDetails as $key => $value) { ?>
        <div class="notif-content">
            <a href="#">
                <?php echo $value['notification_body']; ?>
            </a>
            <span class="notif-detail pull-right">
	<!--Need to Integrate-->
		<?php
			$creation = $value['created_at'];
			$dt = $creation/1000;
			echo date("d m, Y h:i:s A", $dt);
		?>
	</span>
        </div>
        <?php }	} ?>
        <!-- notif-content -->
    </div>
    <?php if ($unreadNotifyCount > 5) { ?>
    <!-- notif-list-main -->
    <div class="notif-view-more col-xs-12">
        <a href="#" class="text-blue text-center innerAll bg-white center-block">
            <strong><a href="../../notification/notification_inbox.php">View More</a></strong>
        </a>
    </div>
    <?php } ?>
