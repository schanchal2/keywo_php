<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container row-10 padding-none">
            <div class="row">
                <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                        <!-- 
							/**
								- left user info card is missing from design
								- if added then set next div "inner-8x" class to "inner-2x"
							 */
                        -->
                        <div class="card left-panel-modules inner-8x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Notification </h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a href="#"> All</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a href="#"> General</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a href="#"> Search</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a class="accordion-toggle side-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnetwo"> Social</a>
                                        <div id="collapseOnetwo" class="panel-collapse collapse">
                                                <a href="#" class="border-bottom">1</a>
                                                <a href="#" class="border-bottom">2</a>
                                                <a href="#" class="border-bottom">3</a>
                                                <a href="#" class="border-bottom">4</a>
                                                <a href="#">5</a>
                                        </div>
                                    </div>
                                     <div class="social-user-setting-name border-bottom">
                                        <a class="accordion-toggle side-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnethree"> Keyword</a>
                                        <div id="collapseOnethree" class="panel-collapse collapse">
                                                <a href="#" class="border-bottom">1</a>
                                                <a href="#" class="border-bottom">2</a>
                                                <a href="#" class="border-bottom">3</a>
                                                <a href="#" class="border-bottom">4</a>
                                                <a href="#">5</a>
                                        </div>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a href="#"> Wallet</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a href="#"> Referral</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include '_sidebar_activity_log.php'; ?>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
                <div id="activity__log">
                    <div class="col-xs-9">
                        <div class="social-center-panel">
                            <div class="row">
                                <div class="col-xs-12 innerB">
                                    <div class="profile-heading ">
                                        <h4 class="text-color-Text-Primary-Blue margin-none f-sz20">Notification</h4>
                                        <h5 class=" margin-none f-sz15"></h5>
                                        <div class="pull-right">
                                            <form class="clearfix margin-none">
                                                <div class="pull-left search-box-comments">
                                                    <div class="input-group">
                                                        <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="">
                                                        <input type="submit" class="search-btn-header" value="">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <!--  -->
                                <div class="h4 innerL">Today</div>
                                <!--  -->
                                <ul class="list-unstyled innerMT notification__list">
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-thumbs-o-up fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#"> Byrne Farnandes </a> like <a class="" href="#">Ravi Nandwani's </a> post.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:15pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-thumbs-o-down fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#">  You </a> dislike <a class="" href="#">  Ravi Nandwani's </a> post.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:15pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/notification_wallet.png">
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#">  You </a> have received 25IT$ frm <a class="" href="#">  Byrne Farnandes. </a></p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:15pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
												  		<i class="fa fa-circle fa-stack-2x"></i>
												  		<i class="fa  fa-comments-o fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#">  Byrne Farnandes </a> Commented on your post.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:05pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-group fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#">  Byrne rom  </a> used your code as Referral code</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:05pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
														<i class="fa fa-circle fa-stack-2x"></i>
														<i class="fa fa-search fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#"> You </a> had searched <a class="" href="#"> #Home </a> keyword.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:05pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
													<img class="media-object--notification" src="<?=$rootUrlImages ?>icons/notification_keywo.png">
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"><a class="" href="#">  You </a> had successfully changed your password.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:05pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
													<img class="media-object--notification" src="<?=$rootUrlImages ?>icons/keywordsearch.png">
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#"> You </a> had purchase <a class="" href="#"> #home </a> keyword.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:05pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="h4  innerT innerL ">Yesterday</div>
                                <ul class="list-unstyled innerMT notification__list">
                                    <?php for ($i=0; $i <5 ; $i++) { ?>
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
													  <i class="fa fa-circle fa-stack-2x"></i>
													  <i class="fa fa-thumbs-o-up fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#"> Byrne Farnandes </a> like <a class="" href="#">Ravi Nandwani's </a> post.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:15pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php  } ?>
                                </ul>
                                <div class="h4  innerT innerL ">20 <sup>th</sup> Septembr, 2017</div>
                                <ul class="list-unstyled innerMT notification__list">
                                    <?php for ($i=0; $i <5 ; $i++) { ?>
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a class="f-sz13" href="#">
                                                    <span class="fa-stack fa-lg">
													  <i class="fa fa-circle fa-stack-2x"></i>
													  <i class="fa fa-thumbs-o-up fa-stack-1x text-white"></i>
													</span>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="row">
                                                            <div class="col-xs-7">
                                                                <p class="ellipses innerTB half margin-none"> <a class="" href="#"> Byrne Farnandes </a> like <a class="" href="#">Ravi Nandwani's </a> post.</p>
                                                            </div>
                                                            <div class="col-xs-5">
                                                                <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <p class="half innerTB margin-none pull-right text-color-grayscale-be">2:15pm</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php  } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- social-center-panel  -->
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <script>
        $(document).on('click','.side-toggle', function (e) {
                 //$('.accordion-heading i').toggleClass(' ');
                 $(e.target).parent('.social-user-setting-name').toggleClass('accordion-opened');
            });
            
            // $(document).on('click','.side-toggle', function (e) {
            //     $(this).find('.social-user-setting-name').not($(e.target)).removeClass('accordion-opened');
            //     //$('.accordion-heading i').toggleClass('fa-chevron-right fa-chevron-down');
            // });
    </script>
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../layout/social_footer.php'); ?>
