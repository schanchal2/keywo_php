<?php
error_reporting(0);
/*
 function getCurrUser
 Purpose: To get the current logged in user from the session array
 Accepts: db conn
 returns: username
 */
function getCurrUser(){
	if(sizeOf($_SESSION)>0)
	return $_SESSION['user'];
}
/*
 function updateSession
 Purpose: To update session info in the user table
 Accepts: username, db conn, session start time, session id
 returns: standard format error or success array
 */
function updateSession($username, $conn, $time="", $sessId){

	$returnArr = array();
	//create and run the update query
	$query = sprintf("UPDATE presale_users SET session_start_time=NOW(), session_id='%s' WHERE user_email='%s'", $sessId, $username);
	//echo $query;
	$result = runQuery($query, $conn);

	//return error / success array
	if(noError($result)){
		$returnArr["errCode"]=array("-1"=>-1);
		$returnArr["errMsg"]="Session Updated";
	} else {
		$returnArr["errCode"]=array("2"=>2);
		$returnArr["errMsg"]="Session Error: Could not update session: ".$result["errMsg"];
	}

	return $returnArr;
}

/*
 function checkSession
 Purpose: To check if session is active
 Accepts: db conn
 returns: standard format error or success array
 */
function checkSession($conn){


	global $sessionTimeout, $blanks;

	$returnArr = array();

	if(isset($_SESSION['user']) && !(in_array($_SESSION['user'], $blanks))){

		$sessionTimeQuery = sprintf("SELECT session_start_time FROM user_info WHERE username='%s'", $_SESSION['user']);
		//print_r($sessionTimeQuery); die;
		$result = runQuery($sessionTimeQuery, $conn);

		$result = mysqli_fetch_array($result["dbResource"]);
		$time = $result['session_start_time'];

		$currentTime = date("Y-m-d H:i:s", time());

		$diff = (strtotime($currentTime)-strtotime($time));

		if($diff < ($sessionTimeout*60)){
			$returnArr["errCode"]=array("-1"=>-1);
			$returnArr["errMsg"]="Session Active";
		} else{
			$returnArr["errCode"][2]=2;
			$returnArr["errMsg"]="Session timedout";
		}
	} else {
		session_destroy();
		$returnArr["errCode"][2]=2;
		$returnArr["errMsg"]="Session Inctive";
	}

	return $returnArr;
}

/*
 function checkSession
 Purpose: To check if session is active
 Accepts: db conn
 returns: standard format error or success array
 */
function checkSession_searchcoin($conn){

	global $sessionTimeout, $blanks;

	$returnArr = array();

	if(isset($_SESSION['user']) && !(in_array($_SESSION['user'], $blanks))){

		$sessionTimeQuery = sprintf("SELECT session_start_time FROM sc_user WHERE email='%s'", $_SESSION['email']);
		$result = runQuery($sessionTimeQuery, $conn);

		$result = mysqli_fetch_array($result["dbResource"]);
		$time = $result['session_start_time'];

		$currentTime = date("Y-m-d H:i:s", time());

		$diff = (strtotime($currentTime)-strtotime($time));

		if($diff < ($sessionTimeout*60)){
			$returnArr["errCode"]=array("-1"=>-1);
			$returnArr["errMsg"]="Session Active";
		} else{
			$returnArr["errCode"][2]=2;
			$returnArr["errMsg"]="Session timedout";
		}
	} else {
		session_destroy();
		$returnArr["errCode"][2]=2;
		$returnArr["errMsg"]="Session Inctive";
	}

	return $returnArr;
}

/*
 function checkForSession
 Purpose: To check if user cookie and session is active
 Accepts: db conn
 returns: standard format error or success array
 */
/* function checkForSession($conn){
	
	

}
 */
 function checkForSession($conn){
    
    if (isset($_COOKIE["search_trade_ver_1"])) {
        
      
        //echo 1;
        $email = $_COOKIE["search_trade_ver_1"];
        
        //Cookie Exists
        
        //check for User details
        $userDetails = getBuyerDetails($email, $conn);
		$user_email = $userDetails["errMsg"]["user_email"];
        $session_id = $userDetails["errMsg"]["session_id"];
        $firstname  = $userDetails["errMsg"]["firstname"];
        $lastname   = $userDetails["errMsg"]["lastname"];
		
		//get user wallet state
		$userState = getUserStateFromWallet($email);
		$userState = json_decode($userState,TRUE);
		$firstname = $userState["errMsg"]["first_name"];   
        
        if (isset($user_email)) {
            //echo 2;
            //valid user
            if (isset($_SESSION["email"])) {
                //session is set
                //allow login
                
                
            } else {
				//echo 3;
                
                //session is not set
                if (isset($session_id)) {
                    
                  // echo 4;
                    
                    //session id is present
                    //force User to login
                    session_start();
                    $_SESSION["email"]     = $user_email;
                    $_SESSION["firstname"] = $firstname;
                    
                    
                    //update new session_start_time and session_id
                    $result = updateSession($user_email, $conn, $time = "", session_id());
                    
                } else {
					
					//echo 5;
                    
                    //User has already logged Out
                    //do nothing
                }
            }
            
        } else {
            
           // echo 6;
            //Destroy Invalid cookie
            $val = setcookie("search_trade_ver_1", $email, time() - 3600, "/", "searchtrade.com", 0);
            
        }
        
    } else {
        
        //Cookie Doses'nt exists
        //echo 7;
        
        if (isset($_SESSION["email"])) {
            //login allow login with session
        } else {
            //Session and Cookie both doesn't exists
            //do nothing
            //session_destroy();
        }
        
    }
    
}

/*
 function DeleteSession
 Purpose: To delete session info in the user table
 Accepts: username, db conn, session start time, session id
 returns: standard format error or success array
 */
function deleteSession($username, $conn){

	$returnArr = array();
	//create and run the update query
	$query = "UPDATE presale_users SET session_start_time=NULL, session_id=NULL WHERE user_email='".$username."'";  
	//echo $query;
	$result = runQuery($query, $conn);

	//return error / success array
	if(noError($result)){
		$returnArr["errCode"]=array("-1"=>-1);
		$returnArr["errMsg"]="Session Updated";
	} else {
		$returnArr["errCode"]=array("2"=>2);
		$returnArr["errMsg"]="Session Error: Could not delete session: ".$result["errMsg"];
	}

	return $returnArr;
}

?>