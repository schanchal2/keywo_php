<?php

//echo "Hello";
session_start();
include('../layout/header.php');

include('../layout/transparent_footer.php');
error_reporting(0);

$email = $_SESSION["email"];

//Validating User LoggedIn and LoggedOUt status
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    $login_status = 1;
}else{
    $login_status = 0;
}

if(isset($_SESSION["email"]))
{

    $user_id             = $_POST['user_id'];
    //$category             = $_POST['category'];
    $require             = $userRequiredFields . ",user_id";
    $requestUrl          = $walletURLIP.'api/v3/';
    $getNotificationData = getUserInfo($email, $requestUrl, $require);

    /* Get Notification Details*/

    $id  = $getNotificationData['errMsg']['_id'];
    $category = 'all';
    $notifyCount ='';

    $notificationDetails     = getNotificationDetail($id,$category,$notifyCount, $cursor_value,$offset);

    if (noError($notificationDetails)) {

        // $arrCount = count($notificationDetails["errMsg"]["batched_container"])-1; //echo $arrCount;
        $fwd_cursor = $notificationDetails["errMsg"]["fwd_cursor"]; //echo $fwd_cursor;
        $bkwd_cursor = $notificationDetails["errMsg"]["bkwd_cursor"]; //echo $bkwd_cursor;
        $fwd_cursor_offset_from = $notificationDetails["errMsg"]["fwd_cursor_offset_from"]; //echo $fwd_cursor_offset_from;
        $bkwd_cursor_offset_from = $notificationDetails["errMsg"]["bkwd_cursor_offset_from"]; //echo $bkwd_cursor_offset_from;

    } else {
        $notifyCount   = 'Error';
    }
//        echo '<pre>';
//        print_r($notificationDetails);
//        echo '</pre>';

    ?>

    <script>

        var backwords_offset;
        var forwords_offset;


        function getDataPageToken(cursor,offset,flag){
            var user_email = "<?php echo $email;?>"; //alert(user_email);
            var user_id = "<?php echo $user_id ?>"; //alert(user_id);
            var catgory = $( ".selectCat option:selected" ).val(); //alert(catgory);
            //alert(catgory);
            var offsets ;
            var cursors;
            var value;
            if(flag=="0"){
                offsets = offset;
                value  = cursor;
                cursors = "fwd_cursor";
            }
            else if(flag=="1"){
                offsets = offset;
                value = cursor;
                cursors = "bkwd_cursor";
            }
//            alert('cursor'+cursor+', offset'+offset+', catgory'+catgory+', ')
            $.ajax({
                    type: "POST",
                    dataType: "html",
                    url: "../../controllers/notification/getNotificationList.php",
                    data: {
                            id: user_id, cursor : cursors, value:cursor,offset_from: offsets,category: catgory }
                    ,
                    success:function(dataupdate){
//            console.log('dataupdate', dataupdate)
                        jQuery(".tableData").html(dataupdate);
                        var forwords = $('#fwd_cursor').val()
                        var backwords= $('#bkwd_cursor').val()
                        forwords_offset= $('#fwd_cursor_offset_from').val()
                        backwords_offset= $('#bkwd_cursor_offset_from').val()
//                        alert('forwords'+forwords+'forwords_offset'+forwords_offset)
                        if (forwords == "" || forwords == null){
                            jQuery(".forword").attr('disabled','disabled');

                        }
                        else if(backwords == "" || backwords == null) {
                            $('#forword-pointer').attr('onclick',"getDataPageToken('"+forwords+"','"+forwords_offset+"','0');")
                        }
                        else{
                            //alert('forword'+forwords+forwords_offset)
                            jQuery(".forword").removeAttr("disabled");
                            $('#forword-pointer').attr('onclick',"getDataPageToken('"+forwords+"','"+forwords_offset+"','-1');")
                        }
                        if (backwords == "" || backwords == null){
                            jQuery(".backword").attr('disabled','disabled');
                        }else{
                            //alert('backword'+backwords+backwords_offset)
                            jQuery(".backword").removeAttr("disabled");
                            $('#backword-pointer').attr('onclick',"getDataPageToken('"+backwords+"','"+backwords_offset+"','1');")

                        }
                    }
                    ,
                    error: function () {
                        alert(error);
                    }
                }
            );
        }


        jQuery(document).ready(function(){
            var category = $('.selectCat').val();

            //window.localStorage.setItem("category", category);

            var notificationDetails = "<?php echo $arrCount; ?>";
            var forwords = "<?php echo $fwd_cursor; ?>";
            var backwords= "<?php echo $bkwd_cursor; ?>";

//        alert('forwords'+forwords+'backwords'+backwords)

            if(notificationDetails < 0){
                jQuery(".mailinbox").hide();
                jQuery(".msgStatus").html("No Notifications");
                jQuery(".msgStatus").show();
                jQuery(".forword").attr('disabled','disabled');
                jQuery(".backword").attr('disabled','disabled');
            }

            if (forwords == '' || forwords == null){
                jQuery(".forword").attr('disabled','disabled');
            }
            else{
                jQuery(".forword").removeAttr("disabled");
            }
            if (backwords == "" || backwords == null){
                jQuery(".backword").attr('disabled','disabled');
            }
            else{
                jQuery(".backword").removeAttr("disabled");
            }

            // check all checkboxes in table
            if(jQuery('.checkall').length > 0) {
                jQuery('.checkall').click(function(){

                        var parentTable = jQuery(this).parents('table');
                        var ch = parentTable.find('.checkbox');
                        if(jQuery(this).is(':checked')) {
                            //check all rows in table
                            ch.each(function(){
                                    jQuery(this).prop('checked',true);
                                    jQuery(this).parent().addClass('checked');
                                    //used for the custom checkbox style
                                    jQuery(this).parents('tr').addClass('selected');
                                    // to highlight row as selected
                                }
                            );
                        }
                        else {
                            //uncheck all rows in table
                            ch.each(function(){
                                    jQuery(this).prop('checked',false);
                                    jQuery(this).parent().removeClass('checked');
                                    //used for the custom checkbox style
                                    jQuery(this).parents('tr').removeClass('selected');
                                }
                            );
                        }
                    }
                );
            }
            if(jQuery('.mailinbox').length > 0) {
                // star
                //  jQuery('.msgstar').click(function(){
                //  if(jQuery(this).hasClass('starred'))
                //  jQuery(this).removeClass('starred');
                //  else
                //  jQuery(this).addClass('starred');
                //});
                //add class selected to table row when checked
                jQuery('.mailinbox tbody input:checkbox').click(function(){
                        if(jQuery(this).is(':checked'))
                            jQuery(this).parents('tr').addClass('selected');
                        else
                            jQuery(this).parents('tr').removeClass('selected');
                    }
                );
                // trash
                if(jQuery('.delete').length > 0) {
                    //alert(jQuery('.delete').length);
                    jQuery('.delete').click(function(ev){
                        var chkdata = jQuery(".checkbox");
                        var category = $( ".selectCat option:selected" ).val();
                        window.localStorage.setItem("category", category);
                        if(!$(".checkbox").is(':checked')){
                            alert('No selected message');
                        } else{


                            var c = false;
                            var cn = 0;
                            var o = new Array();
                            var _id = "";
                            var jsonObj = [];
                            jQuery('.mailinbox .checkbox').each(function(){
                                if(jQuery(this).is(':checked')) {
                                    c = true;
                                    o[cn] = jQuery(this);
                                    cn++;
                                    var col_name = jQuery(this).attr("flag");
                                    var statuschk = jQuery(this).attr("status");
                                    var status;
                                    if(statuschk == "1"){
                                        status = true;
                                    }
                                    else{
                                        status = false;
                                    }
                                    _id = jQuery(this).val();
                                    item = {}
                                    item ["_id"] = _id;
                                    item ["collection_name"] = col_name;
                                    item ["status"] = status;
                                    jsonObj.push(item);
                                }
                            });

                            var doc_deldata = JSON.stringify(jsonObj);
                            var user_email = "<?php echo $email;?>";
                            var user_id = "<?php echo $id; ?>";
                            var doc_id = doc_deldata;
                            //console.log('docid '+doc_id);
                            //return;
                            //console.log('user id '+user_id);
                            //console.log('user email '+user_email);
                            $.ajax({
                                    //alert("Chanchal");
                                    type: "POST",
                                    url: "../../controllers/notification/deleteNotification.php",
                                    data: {
                                        uid: (user_id),doc_id: (doc_id),email: (user_email) },
                                    success:function(dataupdate){

                                        window.location.replace("notification_inbox.php");

                                    }
                                    ,
                                    error: function () {
                                        alert(error);
                                    }
                                }
                            );
                            if(!c) {
                                alert('No selected message');
                                ev.preventDefault();
                                return false;
                            }
                        }
                    });
                }

            }



            // for select category

            jQuery(".selectCat").click(function(){

                var setCategory = jQuery(this).val();

                //  window.localStorage.setItem("category", setCategory);
                var setcat = setCategory;
                var user_email = "<?php echo $email;?>";
                var user_id = "<?php echo $user_id ?>";

                //alert(''+setcat+''+user_email+''+user_id)
                $.ajax({
                        type: "POST",
                        dataType: "html",
                        url: "../../controllers/notification/getNotificationList.php",
                        data: {
                            id: user_id,category: setcat,email: user_email},

                        success:function(dataGet){
                            // alert(dataGet);


                            jQuery(".tableData").html(dataGet);

                            var forwords = $('#fwd_cursor').val()
                            var backwords= $('#bkwd_cursor').val()
                            forwords_offset= $('#fwd_cursor_offset_from').val()
                            backwords_offset= $('#bkwd_cursor_offset_from').val()
//                        alert('forwords'+forwords+'backwords'+backwords)
//                            alert(forwords+forwords_offset);

                            if (forwords == "" || forwords == null){
                                jQuery("#forword-pointer").attr('disabled','disabled');

                            }
//
                            else{
                                //alert('forword'+forwords+forwords_offset)
                                alert('hereh');
                                jQuery("#forword-pointer").removeAttr("disabled");
                                $('#forword-pointer').attr('onclick',"getDataPageToken('"+forwords+"','"+forwords_offset+"','0');")
                            }
                            if (backwords == "" || backwords == null){
                                jQuery("#backword-pointer").attr('disabled','disabled');
                            }else{
//                                alert('backword'+backwords+backwords_offset)
                                jQuery("#backword-pointer").removeAttr("disabled");
                                $('#backword-pointer').attr('onclick',"getDataPageToken('"+backwords+"','"+backwords_offset+"','1');")

                            }

//
                        },
                        error: function () {
                            alert(error);
                        }
                    }
                );
            });

        });
        document.getElementById('download').click();


    </script>


    <main>
        <div class="container">
            <div id="mail" class="personalDetailsMainDiv inner-2x innerMAll innerAll bg-white">
                <div class="col-xs-12 innerMTB">
                    <select data-toggle="tooltip" title="" data-container="body" data-placement="top" data-original-title="Category" class="btn selectCat" style="float: left;background: white;border: 1px solid #ddd;border-radius: 0;margin-left: 0;">
                        <option value="all">All
                        </option>
                        <option value="ask">Ask
                        </option>
                        <option value="bid">Bid
                        </option>
                        <option value="buy">Buy
                        </option>
                        <option value="deposit">Deposit
                        </option>
                        <option value="withdrawal">Withdrawal
                        </option>
                    </select>

                    <button data-toggle="tooltip" title="" data-container="body" data-placement="top" data-original-title="Delete your notification"  class="btn btn-default icon delete">Delete
                    </button>
                    <div style="float:right">
                        <button data-toggle="tooltip" title="" data-container="body" data-placement="bottom" data-original-title="Newer"  class="btn btn-default ico forword paginationValue" onclick= getDataPageToken('<?php echo $fwd_cursor; ?>','<?php echo  $fwd_cursor_offset_from;?>',"0"); id="forword-pointer" >
                            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/arrow-left-01-128.png" style="width: 13px;margin: 3px;">
                        </button>
                        <button data-toggle="tooltip" title="" data-container="body" data-placement="bottom" data-original-title="Older"  class="btn btn-default ico backword paginationValue" onclick= getDataPageToken('<?php echo $bkwd_cursor; ?>','<?php echo $bkwd_cursor_offset_from;?>',"1"); id="backword-pointer" >
                            <img src="https://cdn3.iconfinder.com/data/icons/faticons/32/arrow-left-01-128.png" style="width: 13px;margin: 3px;-ms-transform: rotate(7deg);-webkit-transform: rotate(7deg);transform: rotate(181deg);">
                        </button>
                    </div>
                </div>

                <table class="table table-bordered mailinbox" style="border-collapse:collapse; margin-top:10px;" border="0" cellpadding="5" cellspacing="3" >
                    <thead>
                    <tr>
                        <th class="head1 aligncenter" style="width:5%"><input type="checkbox" class="checkall" value=""></th>
                        <th class="head0" style="width:10%">Subject</th>
                        <th class="head1" style="width:55%">Description</th>
                        <th class="head0 attachement" style="width:6%">Attach</th>
                        <th class="head1" style="text-align:center;width:20%;">Date</th>
                    </tr>
                    </thead>
                    <tbody class="tableData">
                    <?php
                    if(noError($notificationDetails)){
                        $notificationDetails = $notificationDetails["errMsg"]['batched_container'];
                        //printArr($notificationDetails);
                        $xx = 0;
                        foreach($notificationDetails as $key => $value){
                            ?>
                            <tr class="<?php if($value['read'] != 1){ $status = 0; ?> unread  <?php  }else{ $status = 1; } ?>" >
                                <td class="aligncenter">
                                    <input type="checkbox" style="text-align: center ;margin: auto ;zoom: 1.2" status="<?php echo $status; ?>" flag="<?php echo $value['archive_boundary']; ?>" class="checkbox" value="<?php echo $value['_id']; ?>" name="" />
                                </td>
                                <!--<td class="star"><a class="msgstar"></a></td>-->
                                <td >
                                    <div style="text-transform: capitalize;"><?php echo $value['category']?></div>
                                </td>
                                <td>
                                    <?php if($value['category'] == "bid" ) { ?>
                                        <a href="<?php echo $dashbordUrl; ?>#activebid" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body']);?>
                                        </a>
                                    <?php } else if($value['category'] == "withdrawal"){ ?>
                                        <a href="<?php echo $presaleRootURL; ?>MyWallet.php#withdrawalRequest" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body']); ?>
                                        </a>
                                    <?php  } else if($value['category'] == "buy" || $value['category'] == "ask" || $value['category'] == "sell"){ ?>
                                        <a href="<?php echo $dashbordUrl; ?>#mykeyword" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body'])?>
                                        </a>
                                    <?php }else if($value['category'] == "2FA Setting"){ ?>
                                        <a href="<?php echo $rootUrl; ?>stsecurity.php" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body']); ?>
                                        </a>
                                    <?php  }else if($value['category'] == "2FA Recovery"){ ?>
                                        <a href="<?php echo $rootUrl; ?>stsecurity.php" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body']); ?>
                                        </a>
                                    <?php  }else if($value['category'] == "deposit"){ ?>
                                        <a href="<?php echo $presaleRootURL; ?>MyWallet.php#deposit" class="title" id="<?php echo $value['_id']; ?>" tocken_flag="<?php echo $value['archive_boundary']?>" statusRU="<?php echo $value['read']?>">
                                            <?php echo rawurldecode($value['notification_body']); ?>
                                        </a>


                                    <?php }  ?>
                                </td>
                                <td class="attachment" style="">
                                    <?php
                                    if($value['link'] != ''){
                                        $a = explode("keywords", $value['link']);
                                        $b = "keywords".$a[1];
                                    }
                                    ?>
                                    <a href="<?php echo $b;?>"
                                        <?php if($value['link'] == ''){?> hidden
                                        <?php } ?> download id='download' class='attachment' >
                                        <img src="../../images/attachment.png" alt="" />
                                    </a>
                                </td>
                                <td class="date">
                                    <?php
                                    $time = $value['created_at'];
                                    $original_timestamp = $time;
                                    $timestamp = (int) substr($original_timestamp,0,-3);
                                    echo date('d m, Y g:i:s A' ,$timestamp);
                                    ?>
                                </td>
                            </tr>
                            <?php
                            $xx++; }
                    }else{
                        print("Error: Unable to fetch notification");
                        exit;
                    } ?>
                    </tbody>
                </table>
                <!--?php// } else { ?-->
                <!--  <div class="col-md-12" style="text-align:center;"> Unable to found your notification list </div>-->
                <!--?php// } ?-->
            </div>
        </div>
        <!--col-xs-12-->
    </main>
<?php }


?>



