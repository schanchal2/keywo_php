<?php
  session_start();

  //check for session
  if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

    $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <style>
    .highcharts-contextbutton {
        display: none!important;
    }
    
    .canvasjs-chart-credit {
        display: none;
    }
    </style>
    <!-- container starts here -->
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3">
                <div class="myearnings-left-panel">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll border-all">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="user-name">
                                    Vishal Gupta
                                </div>
                                <div class="user-earnings-text">
                                    Earning
                                </div>
                                <h4 class="text-blue margin-none">
                                 2343.43 <?php echo $keywoDefaultCurrencyName; ?>
                          </h4>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                    <!-- my-info-card  -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Serach</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content consumption -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Upload</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content upload -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Owned</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Active Trade</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Trading history</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Keyword -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Interaction</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Commission</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Referral -->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <h5 class="text-blue keyword-name margin-top-none">My Earning</h5>
                <!-- row starts here -->
                <div class="row">
                    <div class="col-md-12 bg-white border-all">
                        <div class="row">
                            <div class="col-md-6 border-right half  innerLR innerT">
                                <div class="row">
                                    <div class="col-md-5">
                                        <!-- graph div starts here -->
                                        <div id="container"></div>
                                        <!-- graph div ends here -->
                                    </div>
                                    <div class="col-md-7">
                                        <div id="my-earning-unordered-list">
                                            <span class="keyword-grey-span f-sz16">Earning Type</span>
                                            <ul class="padding-left-none margin-none l-h18">
                                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Content Consumption</span></li>
                                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Content Upload</span></li>
                                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Keyword</span></li>
                                                <li><i class="fa fa-stop half innerMR innerMT" aria-hidden="true"></i><span>Referral</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 half  innerLR innerT">
                                <div class="row">
                                    <div class="col-md-6 text-center keyword-marketplace">
                                        <!-- added keyword-marketplace class for text style -->
                                        <div id="my-earning-unordered-list">
                                            <!-- added my-earning-unordered-list class for  ul li css -->
                                            <span class="keyword-grey-span f-sz16">Earnings</span>
                                            <ul class="padding-left-none half innerMT margin-bottom-none">
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-md-6 text-center keyword-marketplace">
                                        <!-- added keyword-marketplace class for text style -->
                                        <div id="my-earning-unordered-list">
                                            <!-- added my-earning-unordered-list class for  ul li css -->
                                            <span class="keyword-grey-span f-sz16">Interactions</span>
                                            <ul class="padding-left-none half innerMT margin-bottom-none">
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546</a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546</a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546</a></label>
                                                </li>
                                                <li>
                                                    <label class="text-black ellipses margin-bottom-none"><a href="#" class="display-in-block-txt-blk" title="2,540,1352654654654164161646546" data-toggle="tooltip" data-placement="bottom">2,540,1352654654654164161646546</a></label>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row ends here -->
                <!-- row starts here -->
                <div class="row inner-2x innerMT">
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-left-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll">
                            <span class="keyword-grey-span innerL half f-sz16">Content Consumption</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none l-h16">
                            <div class="comn-lft-rght-cont">
                                <label>Blog</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Video</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Image</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Audio</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Search</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="darkest-blue-bg innerAll">
                            <div id="chartContainer1" style="height:57px; width: 100%;">
                            </div>
                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-right-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll">
                            <span class="keyword-grey-span innerL half f-sz16">Content Upload</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none l-h16">
                            <div class="comn-lft-rght-cont">
                                <label>Blog</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Video</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Image</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Audio</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>App</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="dark-blue-bg innerAll">
                            <div id="chartContainer2" style="height:57px; width: 100%;">
                            </div>
                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                </div>
                <!-- row ends here -->
                <!-- row starts here -->
                <div class="row inner-2x innerMT">
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-left-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll">
                            <span class="keyword-grey-span innerL half f-sz16 l-h16">Keyword</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none">
                            <div class="comn-lft-rght-cont">
                                <label>Owned</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Active Trade</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Trading History</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="light-blue-bg innerAll">
                            <div id="chartContainer3" style="height:57px; width: 100%;">
                            </div>
                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                    <!-- col-md-6 starts here -->
                    <div class="col-md-6 padding-right-none">
                        <!-- title starts here -->
                        <div class="bg-white border-all half innerAll ">
                            <span class="keyword-grey-span innerL half f-sz16 l-h16">Referral</span>
                        </div>
                        <!-- title ends here -->
                        <!-- list starts here -->
                        <div class="bg-white border-all innerAll border-top-none">
                            <div class="comn-lft-rght-cont">
                                <label>Interaction</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Commission</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                            <div class="comn-lft-rght-cont">
                                <label>Post Share</label>
                                <label class="pull-right ellipses-market-trade-span text-right"><a href="#" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- list ends here -->
                        <!-- graph starts here -->
                        <div class="lightest-blue-bg innerAll">
                            <div id="chartContainer4" style="height:57px; width: 100%;">
                            </div>
                        </div>
                        <!-- graph ends here -->
                    </div>
                    <!-- col-md-6 ends here -->
                </div>
                <!-- row ends here -->
                <!-- social-all-status-tabs -->
            </div>
            <!-- col-xs-6 -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- graphs starts here -->
        <script src="<?php echo $rootUrl ;?>frontend_libraries/graph/highcharts_myearnings.js"></script>
        <script src="<?php echo $rootUrl ;?>frontend_libraries/graph/exporting.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script> -->
        <script src="<?php echo $rootUrl ;?>frontend_libraries/graph/canvasjs.min.js"></script>
        <?php
   include("../layout/transparent_footer.php");
   ?>
            <script>
            $(function() {
                Highcharts.setOptions({
                    colors: ['#5cc0f2', '#009eec', '#006496', '#004161']
                });
                // Create the chart
                chart = new Highcharts.Chart({
                    chart: {
                        renderTo: 'container',
                        type: 'pie'
                    },
                    title: {
                        text: ' '
                    },
                    yAxis: {
                        title: {
                            text: 'Total percent market share'
                        }
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                //  stroke-width:'0',
                                //  stroke:"#ff0000",
                                color: '#ffffff',
                                distance: -18
                            },
                            shadow: false
                        }
                    },
                    tooltip: {
                        // enabled: false,
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.y + ' %';
                        }
                    },
                    series: [{
                        type: 'pie',
                        name: 'Earning Type',
                        innerSize: '44%',
                        //  startAt:'-10',
                        size: '140%',
                        data: [

                            ['20%', 20],
                            ['15%', 15],
                            ['35%', 35],
                            ['30%', 30],

                            {
                                name: 'Proprietary or Undetectable',
                                y: 0.2,
                                dataLabels: {
                                    enabled: false
                                }
                            }
                        ]
                    }]
                });
            });
            </script>
            <!-- graph js ends here -->
            <!-- blank line graph starts here -->
            <script type="text/javascript">
            window.onload = function() {
                var chart = new CanvasJS.Chart("chartContainer1", {
                    backgroundColor: "transparent",
                    animationEnabled: true,

                    credits: {
                        enabled: false,
                        text: 'Hello'
                    },
                    axisX: {

                        lineColor: 'transparent',
                        tickLength: 0,
                        gridColor: "Silver",
                        tickColor: "silver",
                        valueFormatString: " "
                    },
                    toolTip: {
                        enabled: false,
                        shared: true
                    },
                    theme: "theme",
                    axisY: {
                        lineColor: 'transparent',
                        tickLength: 0,
                        visible: false,
                        gridColor: "Silver",
                        tickColor: "silver",
                        valueFormatString: " "
                    },
                    legend: {
                        verticalAlign: "center",
                        horizontalAlign: "right"
                    },
                    data: [{
                            shadow: true,
                            type: "line",
                            showInLegend: false,
                            lineThickness: 1,
                            name: " ",
                            markerType: "circle",
                            color: "#ffffff",
                            dataPoints: [{
                                x: new Date(2010, 0, 3),
                                y: 650
                            }, {
                                x: new Date(2010, 0, 5),
                                y: 700
                            }, {
                                x: new Date(2010, 0, 7),
                                y: 710
                            }, {
                                x: new Date(2010, 0, 9),
                                y: 658
                            }, {
                                x: new Date(2010, 0, 11),
                                y: 734
                            }, {
                                x: new Date(2010, 0, 13),
                                y: 963
                            }, {
                                x: new Date(2010, 0, 15),
                                y: 847
                            }, {
                                x: new Date(2010, 0, 17),
                                y: 853
                            }, {
                                x: new Date(2010, 0, 19),
                                y: 869
                            }, {
                                x: new Date(2010, 0, 21),
                                y: 943
                            }, {
                                x: new Date(2010, 0, 23),
                                y: 970
                            }]
                        },
                    ],
                    legend: {
                        cursor: "pointer",
                        itemclick: function(e) {
                            if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                                e.dataSeries.visible = false;
                            } else {
                                e.dataSeries.visible = false;
                            }
                            chart.render();
                        }
                    }
                });
                chart.render();
            }
            </script>
            <!-- blank line graph ends here -->
