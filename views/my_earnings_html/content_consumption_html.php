<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container innerB">
            <div class="col-xs-3">
                <div class="myearnings-left-panel">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll border-all">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="user-name">
                                    Vishal Gupta
                                </div>
                                <div class="user-earnings-text">
                                    Earning
                                </div>
                                <h4 class="text-blue margin-none">
                                2343.43 <?php echo $keywoDefaultCurrencyName; ?>
                            </h4>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                    <!-- my-info-card  -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Serach</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content consumption -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content upload -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Owned</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Active Trade</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Trading history</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Keyword -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Interaction</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Commission</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Referral -->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#lowest" class="l-h10" data-toggle="tab">Lowest</a></li>
                            <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>
                        </ul>
                        <div class="earnings-day inner-2x innerMT innerML-10">
                            Today
                        </div>
                        <div id="myTabContent" class="tab-content pull-right width-100-percent inner-1x innerMT">
                            <div class="tab-pane active in" id="lowest">
                                <!-- content starts here -->
                                <div class="" id="content_consumption">
                                    <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-light-grey"></i>
                                                                <i class="fa fa-file-text text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-red"></i>
                                                                <i class="fa fa-play-circle text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-sky-blue"></i>
                                                                <i class="fa fa-image text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-orange"></i>
                                                                <i class="fa fa-soundcloud text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-dark-green"></i>
                                                                <i class="fa fa-search text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane fade" id="highest">
                                <!-- content starts here -->
                                <div class="" id="content_consumption">
                                    <ul class="card social-card earnings-list all-box-shadow padding-none clearfix" style="list-style:none;">
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-light-grey"></i>
                                                                <i class="fa fa-file-text text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-red"></i>
                                                                <i class="fa fa-play-circle text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="clearfix border-bottom padding-none">
                                            <div class="col-xs-12 innerAll">
                                                <div class="col-xs-9 padding-none">
                                                    <div class="padding-left-none">
                                                        <div class="innerMR pull-left">
                                                            <div class="image-circle-container">
                                                                <i class="fa fa-circle text-sky-blue"></i>
                                                                <i class="fa fa-image text-white"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="innerMT padding-none earnings-text">
                                                        <span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                                                    </div>
                                                </div>
                                                <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="253.345 <?php echo $keywoDefaultCurrencyName; ?>">253.345 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <!-- content ends here -->
                            </div>
                        </div>
                    </div>
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data">
                <div class="clearfix">
                    <div class="col-xs-12 padding-none earnings-selectlist">
                        <div class="input-group width-100-percent">
                            <div class="Sort-by">
                                <label class="dropdownOptions btn-block margin-none">
                                    <select class="selectpicker">
                                        <option value="All time">All time</option>
                                        <option value="No time">No time</option>
                                        <option value="High time">High time</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dropdown -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Blog</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix ">
                                <div class="col-xs-8 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369 <?php echo $keywoDefaultCurrencyName; ?>
                                </a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Blog -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Video</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix ">
                                <div class="col-xs-8 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none ">
                                <div class="col-xs-8 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369 <?php echo $keywoDefaultCurrencyName; ?>
                                </a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Video -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Image</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix">
                                <div class="col-xs-8 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369 <?php echo $keywoDefaultCurrencyName; ?>
                                </a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Image -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Audio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix">
                                <div class="col-xs-8 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369 <?php echo $keywoDefaultCurrencyName; ?>
                                </a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Audio -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Search</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix">
                                <div class="col-xs-8 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369
                                </a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">103321152162145446416545616369 <?php echo $keywoDefaultCurrencyName; ?>
                                </a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Search -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <select name="" class="earnings-select border-all half innerAll">
                                    <option>Content Consumption</option>
                                    <option>Content Upload</option>
                                    <option>Keyword</option>
                                    <option>Referral</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
                            <i class="fa fa-download text-white"></i>
                        </span>
                            <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
                        </div>
                    </div>
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <?php
   include("../layout/transparent_footer.php");
   ?>
            <script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
            <script type="text/javascript">
            $("#calendar").MonthPicker({
                SelectedMonth: '04/' + new Date().getFullYear(),
                OnAfterChooseMonth: function(selectedDate) {
                    // Do something with selected JavaScript date.
                    // console.log(selectedDate);
                },
                i18n: {
                    months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                }
            });
            </script>
