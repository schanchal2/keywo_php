<?php
  session_start();

  //check for session
  if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

    $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container">
            <div class="col-xs-3">
                <div class="myearnings-left-panel">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll border-all">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="user-name">
                                    Vishal Gupta
                                </div>
                                <div class="user-earnings-text">
                                    Earning
                                </div>
                                <h4 class="text-blue margin-none">
                                  2343.43 <?php echo $keywoDefaultCurrencyName; ?>
                                </h4>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                    <!-- my-info-card  -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Serach</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content consumption -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Upload</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content upload -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Owned</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Active Trade</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Trading history</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Keyword -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Interaction</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Commission</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Referral -->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <!-- social-all-status-tabs -->
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#lowest" class="l-h10 text-color-App-Primary" data-toggle="tab">Lowest</a></li>
                            <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content pull-right width-100-percent innerT inner-2x">
                            <div class="tab-pane active in" id="lowest">
                                <!-- content starts here -->
                                <div class="pagination-content text-center">
                                    <!-- pagination head starts here -->
                                    <div class="pagination-head half innerAll padding-key-market-data">
                                        <div class="row">
                                            <div class="col-xs-2 text-white innerMT innerMR pull-left text-left">keyword</div>
                                            <div class="col-xs-2 text-white text-center inner-1x innerMR ">Sold
                                                <br>(Price)</div>
                                            <div class="col-xs-2 text-white text-center inner-1x innerMR ">Purchase
                                                <br>(Price)</div>
                                            <div class="col-xs-2 text-white text-center innerMLR innerMT">Earning</div>
                                            <div class="col-xs-2 text-white pull-right text-right innerMT padding-left-none">Profit / Loss</div>
                                        </div>
                                    </div>
                                    <!-- pagination head ends here -->
                                    <!-- pagination body starts here -->
                                    <ul class="border-all">
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none">
                                                        <a href="#" class="text-color-App-Success display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none">
                                                        <a href="#" class="error-color display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="1,985 <?php echo $keywoDefaultCurrencyName; ?>">1,985 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none">
                                                        <a href="#" class="text-color-App-Success display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none">
                                                        <a href="#" class="error-color display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="1,985 <?php echo $keywoDefaultCurrencyName; ?>">1,985 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                    </ul>
                                    <!-- pagination body ends here -->
                                </div>
                                <!-- content ends here -->
                            </div>
                            <div class="tab-pane fade" id="highest">
                                <!-- content starts here -->
                                <div class="pagination-content text-center">
                                    <!-- pagination head starts here -->
                                    <div class="pagination-head half innerAll padding-key-market-data">
                                        <div class="row">
                                            <div class="col-xs-2 text-white innerMT innerMR pull-left text-left">keyword</div>
                                            <div class="col-xs-2 text-white text-center inner-1x innerMR ">Sold
                                                <br>(Price)</div>
                                            <div class="col-xs-2 text-white text-center inner-1x innerMR ">Purchase
                                                <br>(Price)</div>
                                            <div class="col-xs-2 text-white text-center innerMLR innerMT">Earning</div>
                                            <div class="col-xs-2 text-white pull-right text-right innerMT padding-left-none">Profit / Loss</div>
                                        </div>
                                    </div>
                                    <!-- pagination head ends here -->
                                    <!-- pagination body starts here -->
                                    <ul class="border-all">
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll padding-bottom-none" id="trading_history">
                                            <!-- row starts here   -->
                                            <div class="row padding-none half innerAll">
                                                <div class="col-md-2 col-xs-2 text-white innerMR pull-left text-left padding-bottom-none">
                                                    <span class="txt-blue ellipses text-left"><a href="#" class="display-in-block-txt-blk text-color-Username-Link" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#Party">#Party</a></span>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center inner-1x innerMR ">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white text-center innerMLR">
                                                    <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">8,458 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                                <div class="col-xs-2 text-white pull-right text-right padding-left-none">
                                                    <label class="text-black text-right ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="8,458<?php echo $keywoDefaultCurrencyName; ?>">2,054 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                    </ul>
                                    <!-- pagination body ends here -->
                                </div>
                                <!-- content ends here -->
                            </div>
                        </div>
                    </div>
                    <!-- tabs html ends here -->
                </div>
            </div>
            <!-- col-xs-6 -->
            <!-- col-md-3 starts here -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data">
                <div class="clearfix">
                    <div class="col-xs-12 padding-none earnings-selectlist">
                        <div class="input-group width-100-percent">
                            <div class="Sort-by">
                                <label class="dropdownOptions btn-block margin-none">
                                    <select class="selectpicker">
                                        <option value="All time">All time</option>
                                        <option value="No time">No time</option>
                                        <option value="High time">High time</option>
                                    </select>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Dropdown -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Trading History</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module" id="my-earnings-trading">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-8 text-left">
                                    <span>Keyword Sold</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="800 <?php echo $keywoDefaultCurrencyName; ?>">800 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none  border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Keyword Purchase</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="800 <?php echo $keywoDefaultCurrencyName; ?>">800 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Keyword Earning</span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="800 <?php echo $keywoDefaultCurrencyName; ?>">800 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none border-top-none">
                                <div class="col-xs-8 text-left">
                                    <span>Net <span class="text-color-App-Success float-left">Profit</span> / <span class="error-color float-left">Loss</span></span>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none">
                                        <a href="#" class="text-color-App-Success display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="800 <?php echo $keywoDefaultCurrencyName; ?>">800 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Interaction -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <select name="" class="earnings-select border-all half innerAll">
                                    <option>Content Consumption</option>
                                    <option>Content Upload</option>
                                    <option>Keyword</option>
                                    <option>Referral</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
              <i class="fa fa-download text-white"></i>
            </span>
                            <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
                        </div>
                    </div>
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-md-3 ends here -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <?php
   include("../layout/transparent_footer.php");
   ?>
            <script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
            <script type="text/javascript">
            $("#calendar").MonthPicker({
                SelectedMonth: '04/' + new Date().getFullYear(),
                OnAfterChooseMonth: function(selectedDate) {
                    // Do something with selected JavaScript date.
                    // console.log(selectedDate);
                },
                 i18n:{months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']}
            });
            </script>
