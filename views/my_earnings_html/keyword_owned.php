<?php
  session_start();

  //check for session
  if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

    $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container">
            <div class="col-xs-3">
                <div class="myearnings-left-panel">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll border-all">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="user-name">
                                    Vishal Gupta
                                </div>
                                <div class="user-earnings-text">
                                    Earning
                                </div>
                                <h4 class="text-blue margin-none">
                2343.43 <?php echo $keywoDefaultCurrencyName; ?>
              </h4>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                    <!-- my-info-card  -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Serach</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content consumption -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Upload</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content upload -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Owned</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Active Trade</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Trading history</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Keyword -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Interaction</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Commission</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Referral -->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <!-- social-all-status-tabs -->
                <div class="col-xs-12 earnings-container padding-none inner-2x keyword-marketplace keyword-marketplace-data keymarketplace-data keyword-markt-popup-common" id="active_trade-common">
                    <!-- tabs html starts here -->
                    <div class="well active-trade-unordered padding-none">
                        <ul class="nav nav-tabs pull-right">
                            <li class="active"><a href="#lowest" class="l-h10" data-toggle="tab">Lowest</a></li>
                            <li class="padding-right-none"><a href="#highest" class="l-h10" data-toggle="tab">Highest</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content pull-right width-100-percent innerT inner-2x">
                            <div class="tab-pane active in" id="lowest">
                                <!-- content starts here -->
                                <div class="pagination-content text-center">
                                    <!-- pagination head starts here -->
                                    <div class="pagination-head half innerAll padding-key-market-data">
                                        <div class="row">
                                            <div class="col-xs-2 text-white">keyword</div>
                                            <div class="col-xs-3 text-white text-center">Interaction</div>
                                            <div class="col-xs-2 text-white text-center">Earning</div>
                                            <div class="col-xs-2 text-white text-center">Ask</div>
                                            <div class="col-xs-3 text-white text-center pull-right padding-right-none">Action</div>
                                        </div>
                                    </div>
                                    <!-- pagination head ends here -->
                                    <!-- pagination body starts here -->
                                    <ul class="border-all" id="trading_history">
                                        <!-- li starts here -->
                                        <li class="half innerAll">
                                            <!-- row starts here   -->
                                            <div class="row half innerAll padding-left-none padding-right-none">
                                                <div class="col-md-12 padding-left-none padding-right-none" style="height:25px;">
                                                    <div class="row">
                                                        <div class="col-md-9 half innerL padding-right-none">
                                                            <div class="col-md-3 text-left"><span class=" ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span></div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200000000</a></label>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,50000000000 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="0.00 <?php echo $keywoDefaultCurrencyName; ?> ">0.00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="col-xs-12 text-right">
                                                                <input value=" Set Ask " type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 half innerL padding-right-none">
                                                    <div class="row">
                                                        <div class="col-md-9 padding-left-none padding-right-none innerMT">
                                                            <div class="col-md-12 text-left">
                                                                <p class="float-left-k margin-bottom-none">Your <span class="txt-blue ellipses width-keyword-owned"><a href="#" class="text-color-Username-Link display-in-keywrd-owned" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span> keyword will expires within <span class="error-color">8 days.</div></p>
                                                                </div>
                                                                <div class="col-md-3">
                                                                  <div class="col-xs-12 text-right innerMT">
                                                                         <input value="  Renew " type="button" class="btn-trading-wid-auto-green" data-toggle="modal" data-target="#">
                                                                  </div>
                                                                </div>
                                                              </div>
                                                            </div>
                                                          </div>
                                                    <!-- row ends here -->

                                                        </li>
                                                    <!-- li ends here -->


                                                    <!-- li starts here -->
                                                    <li class="half innerAll">
                                                    <!-- row starts here   -->
                                                      <div class="row half innerAll padding-left-none padding-right-none">
                                                        <div class="col-md-2">
                                                                 <span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span>
                                                            </div>
                                                            <div class="col-md-3 text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200</a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row pull-right">
                                                                    <div class="col-xs-12 text-right">
                                                                        <input value="Edit Ask" type="button" class="btn-trading-wid-auto-dark" data-toggle="modal" data-target="#">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll">
                                            <!-- row starts here   -->
                                            <div class="row half innerAll padding-left-none padding-right-none">
                                                <div class="col-md-12 padding-left-none padding-right-none" style="height:25px;">
                                                    <div class="row">
                                                        <div class="col-md-9 half innerL padding-right-none">
                                                            <div class="col-md-3 text-left"><span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span></div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200000000</a></label>
                                                            </div>
                                                            <div class="col-md-4 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,50000000000 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                                    <a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title=" "></a>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="col-xs-12 text-right">
                                                                <input value=" Set Ask " type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 half innerL padding-right-none">
                                                    <div class="row">
                                                        <div class="col-md-9 padding-left-none padding-right-none innerMT">
                                                            <div class="col-md-12 text-left">
                                                                <p class="float-left-k margin-bottom-none">Your <span class="txt-blue ellipses width-keyword-owned"><a href="#" class="text-color-Username-Link display-in-keywrd-owned" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span> keyword will expires within <span class="error-color">8 days.</div></p>
                </div>
                <div class="col-md-3">
                  <div class="col-xs-12 text-right innerMT">
                         <input value="  Renew " type="button" class="btn-trading-wid-auto-green" data-toggle="modal" data-target="#">
                  </div>
                </div>
              </div>
            </div>
          </div>
    <!-- row ends here -->
        </li>
    <!-- li ends here -->



    <!-- li starts here -->
    <li class="half innerAll">
    <!-- row starts here   -->
      <div class="row half innerAll padding-left-none padding-right-none">
        <div class="col-md-2">
          <span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span>
                                                            </div>
                                                            <div class="col-md-3 text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200</a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row pull-right">
                                                                    <div class="col-xs-12 text-right">
                                                                        <input value="Edit Ask" type="button" class="btn-trading-wid-auto-dark" data-toggle="modal" data-target="#">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                        <!-- li starts here -->
                                        <li class="half innerAll">
                                            <!-- row starts here   -->
                                            <div class="row half innerAll padding-left-none padding-right-none">
                                                <div class="col-md-12 padding-left-none padding-right-none" style="height:25px;">
                                                    <div class="row">
                                                        <div class="col-md-9 half innerL padding-right-none">
                                                            <div class="col-md-3 text-left"><span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span></div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200000000</a></label>
                                                            </div>
                                                            <div class="col-md-4 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,50000000000 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-3 text-center">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                                    <a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title=" "></a>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="col-xs-12 text-right">
                                                                <input value=" Set Ask " type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 half innerL padding-right-none">
                                                    <div class="row">
                                                        <div class="col-md-9 padding-left-none padding-right-none innerMT">
                                                            <div class="col-md-12 text-left">
                                                                <p class="float-left-k margin-bottom-none">Your <span class="txt-blue ellipses width-keyword-owned"><a href="#" class="text-color-Username-Link display-in-keywrd-owned" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span> keyword will expires within <span class="error-color">8 days.</div></p>
                </div>
                <div class="col-md-3">
                  <div class="col-xs-12 text-right innerMT">
                         <input value="  Renew " type="button" class="btn-trading-wid-auto-green" data-toggle="modal" data-target="#">
                  </div>
                </div>
              </div>
            </div>
          </div>
    <!-- row ends here -->
        </li>

    <!-- li ends here -->


    <!-- li starts here -->
    <li class="half innerAll">
    <!-- row starts here   -->
      <div class="row half innerAll padding-left-none padding-right-none">
        <div class="col-md-2">
          <span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span>
                                                            </div>
                                                            <div class="col-md-3 text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200</a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                            </div>
                                                            <div class="col-md-2  text-black">
                                                                <label class="text-black ellipses margin-bottom-none margin-top-none">
                                                                    <a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title=" "> </a>
                                                                </label>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="row pull-right">
                                                                    <div class="col-xs-12 text-right">
                                                                        <input value="Edit Ask" type="button" class="btn-trading-wid-auto-dark" data-toggle="modal" data-target="#">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- row ends here -->
                                        </li>
                                        <!-- li ends here -->
                                    </ul>
                                    <!-- pagination body ends here -->
                                    </div>
                                    <!-- content ends here -->
                                    </div>
                                    <div class="tab-pane fade" id="highest">
                                        <!-- content starts here -->
                                        <div class="pagination-content text-center">
                                            <!-- pagination head starts here -->
                                            <div class="pagination-head half innerAll padding-key-market-data">
                                                <div class="row">
                                                    <div class="col-xs-2 text-white">keyword</div>
                                                    <div class="col-xs-3 text-white text-center">Interaction</div>
                                                    <div class="col-xs-2 text-white text-center">Earning</div>
                                                    <div class="col-xs-2 text-white text-center">Ask</div>
                                                    <div class="col-xs-3 text-white text-center pull-right padding-right-none">Action</div>
                                                </div>
                                            </div>
                                            <!-- pagination head ends here -->
                                            <!-- pagination body starts here -->
                                            <ul class="border-all" id="trading_history">
                                                <!-- li starts here -->
                                                <li class="half innerAll">
                                                    <!-- row starts here   -->
                                                    <div class="row half innerAll padding-left-none padding-right-none">
                                                        <div class="col-md-2">
                                                            <span class="txt-blue ellipses text-left"><a href="#" class="text-color-Username-Link display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="PartyPartyPartyParty">#PartyPartyPartyParty</a></span>
                                                        </div>
                                                        <div class="col-md-3 text-black">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">200</a></label>
                                                        </div>
                                                        <div class="col-md-2  text-black">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                        </div>
                                                        <div class="col-md-2  text-black">
                                                            <label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="row pull-right">
                                                                <div class="col-xs-12 text-right">
                                                                    <input value=" Set Ask " type="button" class="btn-trading-wid-auto" data-toggle="modal" data-target="#">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </li>
                                                <!-- li ends here -->
                                            </ul>
                                            <!-- pagination body ends here -->
                                        </div>
                                        <!-- content ends here -->
                                    </div>
                                    </div>
                                    </div>
                                    <!-- tabs html ends here -->
                                    </div>
                                    </div>
                                    <!-- col-xs-6 -->
                                    <!-- col-md-3 starts here -->
                                    <div class="col-xs-3 keyword-marketplace-data keymarketplace-data">
                                        <div class="clearfix">
                                            <div class="col-xs-12 padding-none earnings-selectlist">
                                                <div class="input-group width-100-percent">
                                                    <div class="Sort-by">
                                                        <label class="dropdownOptions btn-block margin-none">
                                                            <select class="selectpicker">
                                                                <option value="All time">All time</option>
                                                                <option value="No time">No time</option>
                                                                <option value="High time">High time</option>
                                                            </select>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Dropdown -->
                                        <div class="card right-panel-modules inner-2x innerMT">
                                            <div class="bg-light-gray right-panel-modules-head">
                                                <div class="row margin-none">
                                                    <div class="col-xs-12">
                                                        <h4 class="margin-none half innerTB">Keyword Owned</h4>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="earnings-right-module">
                                                <div class="margin-none  border-all">
                                                    <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                                        <div class="col-xs-6 text-left">
                                                            <span>Total Keyword</span>
                                                        </div>
                                                        <div class="col-xs-6 text-right">
                                                            <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                                        <div class="col-xs-6 text-left">
                                                            <span>Interaction</span>
                                                        </div>
                                                        <div class="col-xs-6 text-right">
                                                            <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                                        </div>
                                                    </div>
                                                    <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                                        <div class="col-xs-6 text-left">
                                                            <span>Earning</span>
                                                        </div>
                                                        <div class="col-xs-6 text-right">
                                                            <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#55555555555555 <?php echo $keywoDefaultCurrencyName; ?>">#55555555555555 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Interaction -->
                                        <div class="card right-panel-modules inner-2x innerMT">
                                            <div class="bg-light-gray right-panel-modules-head">
                                                <div class="row margin-none">
                                                    <div class="col-xs-12 padding-none">
                                                        <select name="" class="earnings-select border-all half innerAll">
                                                            <option>Content Consumption</option>
                                                            <option>Content Upload</option>
                                                            <option>Referral</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="earnings-right-module">
                                                <div id='calendar'></div>
                                                <div class="text-blue innerTB text-center download-btn">
                                                    <span class="download-icon">              <i class="fa fa-download text-white"></i>          </span>
                                                    <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Calendar -->
                                    </div>
                                    <!-- col-md-3 ends here -->
                                    <!-- popup code starts here -->
                                    <div id="keyword-popup-set-ask" class="modal fade in keyword-popup" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content row">
                                                <div class="modal-header custom-modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">×</button>
                                                    <h4 class="modal-title">Set Ask</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div>
                                                        <div class="">
                                                            <div class="row">
                                                                <div class="col-md-12 innerMB">
                                                                    <span class="keyword-grey-span pull-r-marketplace">#keyword</span>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="text-left">
                                                                        <span class="span-keyword-market pull-left">Your Ask : </span>
                                                                        <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;Not set</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="text-left pull-right">
                                                                        <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                                        <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="text-left">
                                                                        <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                                        <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <div class="text-left pull-right">
                                                                        <span class="span-keyword-market pull-left">INR : &nbsp;</span>
                                                                        <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-md-6 innerMB">
                                                                    <div class="text-left">
                                                                        <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6 innerMB">
                                                                    <div class="text-left pull-right">
                                                                        <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                                                        <input value="Set Ask" type="button" class="btn-trading-wid-auto innerMTB">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- popup ends here -->
                                    </div>
                                    <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <?php
   include("../layout/transparent_footer.php");
   ?>
            <script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
            <script type="text/javascript">
            $("#calendar").MonthPicker({
                SelectedMonth: '04/' + new Date().getFullYear(),
                OnAfterChooseMonth: function(selectedDate) {
                    // Do something with selected JavaScript date.
                    // console.log(selectedDate);
                },
                i18n: {
                    months: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUNE', 'JULY', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC']
                }
            });
            </script>
