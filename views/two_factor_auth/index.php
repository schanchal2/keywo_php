<?php
session_start();
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}
$docrootpath = "../../"; 

include("../layout/header.php");
$securityPreference = $_SESSION['security_preference'];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<br>
<br>
<br>
<br>

<?php if($securityPreference == 2){ 
  $_SESSION['security_type'] = "Google Authentication";
  ?>
<!-- googleAuthenticator Modal -->
<div id="googleAuthenticator" class=" " role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header innerLR innerT padding-bottom-none">
      <h4 class="text-blue">Google Authenticator Verification</h4>
      <!-- <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span> -->
    </div>
    <div class="modal-body auth-box">
      <div class="text-center">
        <div class="authenticator-text innerMB">
          Enter your OTP (one time password) sent on your authenticator app<i class="fa fa-asterisk error-color"></i>
        </div>
        <div class="form-group">
          <input type="text" name="" id="auth-code" class="form-control bg-light-purple" value="">
        </div>
        <div class="">
          <div class="">
            <a onclick="cantAccessPhone();" class="text-sky-blue">Can't access any of your phone?</a>
          </div>
          <h5 class="err-msg" style="color: red;"></h5>
          <div class="clearfix btn-bottom">
            <div class="reset btn btn-social-wid-auto btn-xs innerMAll pull-left">
              Reset
            </div>
            <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" id="google-auth-btn" onclick="validateGoogleAuth();">
              Submit
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php }else if($securityPreference == 1){ 
$_SESSION['security_type'] = "Account password";
  ?>
<div id="googleAuthenticator" class=" " role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header innerLR innerT padding-bottom-none">
      <h4 class="text-blue">Password Verification</h4>
      <!-- <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span> -->
    </div>
    <div class="modal-body auth-box">
      <div class="text-center">
        <div class="authenticator-text innerMB">
          Enter your account password<i class="fa fa-asterisk error-color"></i>
        </div>
        <div class="form-group">
          <input type="password" name="" id="password-code" class="form-control bg-light-purple" value="">
        </div>
        <div class="">
          <div class="">
            <!-- <a href="#" class="text-sky-blue">Forgot your password?</a> -->
          </div>
          <h5 class="err-msg" style="color: red;"></h5>
          <div class="clearfix btn-bottom">
            <div class="reset btn btn-social-wid-auto btn-xs innerMAll pull-left">
              Reset
            </div>
            <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" id="pass-btn" onclick="verfyPassword();">
              Submit
            </div>

             <!-- <input type="submit" class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" id="pass-btn" onclick="verfyPassword();" value="Submit" />-->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<?php }else if($securityPreference == 3){ 
$_SESSION['security_type'] = "Transaction pin";
  ?>
<div id="googleAuthenticator" class=" " role="dialog">
<div class="modal-dialog">
  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header innerLR innerT padding-bottom-none">
      <h4 class="text-blue">Transaction Pin Verification</h4>
      <!-- <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span> -->
    </div>
    <div class="modal-body auth-box">
      <div class="text-center">
        <div class="authenticator-text innerMB">
          Enter your transaction pin which you set in security settings<i class="fa fa-asterisk error-color"></i>
        </div>
        <div class="form-group">
          <input type="password" name="" id="trans-pin" class="form-control bg-light-purple" value="">
        </div>
        <div class="">
          <div class="">
            <!-- <a href="#" class="text-sky-blue">Forgot your transaction pin?</a> -->
          </div>
          <h5 class="err-msg" style="color: red;"></h5>
          <div class="clearfix btn-bottom">
            <div class="reset btn btn-social-wid-auto btn-xs innerMAll pull-left">
              Reset
            </div>
            <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" id="trans-auth-btn" onclick="validateTransPin();">
              Submit
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
 


<?php } ?>

<!-- cantAccessPhone Modal -->
  <div id="cantAccessPhone" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="form-group access-options">
              <div class="radio radio-primary">
                  <input type="radio" checked="checked" id="accessBtn1" name="accessBtn" value="">
                  <label for="accessBtn1" class="radioLabel">
                      I already have the one time backup verification code
                  </label>
              </div>
              <div class="radio radio-primary">
                  <input type="radio" id="accessBtn2" name="accessBtn" value="">
                  <label for="accessBtn2" class="radioLabel">
                      I don't have phone or backup verification code
                  </label>
              </div>
            </div>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll pull-left" onclick="openCantAccessModal();">
                  Proceed
                </div>
                <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" onclick="cancel();">
                  Cancel
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- backupCodeVerification Modal -->
  <div id="backupCodeVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="" >
            <div class="text-center innerMB">
              <div class="backup-text border-bottom half innerB innerMB">
                Backup Code Verification
              </div>
            </div>
            <div class="">
              <p class="margin-top-none">The backup code is used to access your account in case of emergencies like misplaced or lost phone or if you have problems receiving code.</p>
            </div>
            <div class="">
              <div class="backup-verification-text innerMB half">
                Enter Backup Verification code<i class="fa fa-asterisk error-color"></i>
              </div>
              <div class="form-group">
                <input type="text" name="" id="backup-code" class="form-control bg-light-purple" value="">
              </div>
            </div>
            <h5 class="err-msg" style="color: red;"></h5>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll pull-left" id="backup-btn" onclick="backupCodeVerification()">
                  Verify code
                </div>
                <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right" onclick="cancel();">
                  Cancel
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- signInVerification Modal -->
  <div id="signInVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="innerMB">
              <div class="signin-text border-bottom half innerB">
                Sign-in Verification Code
              </div>
            </div>
            <div class="">
              <p class="margin-top-none">If you can't access your phone in addition to the backup verification code<br>
                Contact to <a href="#">support@keywo.com</a>
              </p>
            </div>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll" onclick="window.location.href='<?php echo $rootUrl; ?>views/social/index.php'">
                  Back to Home
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript">
  var redirect_url = '<?php echo $_SESSION['redirect_url']; ?>';
  var changePass = '<?php echo $_SESSION['changePass']; ?>'
	$(".reset").click(function(){
	    $(".form-control").val('');
      $(".err-msg").text(''); 
	});

	function cantAccessPhone(){
		$('#cantAccessPhone').modal('show');
	}
	function openCantAccessModal() {
		$('#cantAccessPhone').modal('hide');
		var modal_to_open = '';
		if ($("#accessBtn1").is(':checked')) {
            $('#backupCodeVerification').modal('show');
        }else if($("#accessBtn2").is(':checked')){
        	$('#signInVerification').modal('show');
        }
	}

  $('.auth-box').keydown(function (event) {
    var securityPreference = '<?php echo $securityPreference; ?>';
      var keypressed = event.keyCode || event.which;
      if (keypressed == 13) {
        if(securityPreference == 2){
          validateGoogleAuth();
        }else if(securityPreference == 1){
          verfyPassword();
        }else{
          validateTransPin();
        }
      }
  });


  function validateGoogleAuth(){
    $(".err-msg").text('');
    $("#google-auth-btn").text('Validating...'); 
    var auth_code = $("#auth-code").val();

    if(auth_code.trim().length == 0){
      $(".err-msg").text("Please enter authentication code");   
      $('#google-auth-btn').text('Submit');   
    }else{
        $('#google-auth-btn').prop('onclick',null).off('click');
      $.ajax({
              type: 'POST',
              dataType: 'json',
              url: rootUrl+"controllers/wallet/verify2FAcodeController.php",
              data: {
                  auth_code: encodeURIComponent(auth_code)
              },

              success: function(data){
                 console.log(data);
                  if(data.errCode == -1){
                      $(".err-msg").css("color","green");
                      $(".err-msg").text(data.errMsg);
                      window.location.href=redirect_url;

                  }else{
                      $(".err-msg").text(data.errMsg);
                      $('#google-auth-btn').attr('onClick','validateGoogleAuth()');
                  }
                  $('#google-auth-btn').text('Submit');
              }

          });
    }
  }

  function backupCodeVerification(){
    $(".err-msg").text('');
    $("#backup-btn").text('Verifying...'); 
    var backup_code = $("#backup-code").val();

    if(backup_code.trim().length == 0){
      $(".err-msg").text("Please enter authentication code");   
      $('#backup-btn').text('Verify Code');   
    }else{
        $('#backup-btn').prop('onclick',null).off('click');
      $.ajax({
          type: 'POST',
          dataType: 'json',
          url: rootUrl+"controllers/wallet/verify2FABackupCodeController.php",
          data: {
              backup_code: encodeURIComponent(backup_code)
          },

          success: function(data){
             console.log(data);
              if(data.errCode == -1){
                  $(".err-msg").css("color","green");
                  $(".err-msg").text(data.errMsg);
                  window.location.href=redirect_url;

              }else{
                  $(".err-msg").text(data.errMsg);
                  $('#backup-btn').attr('onClick','backupCodeVerification()');
              }
              $('#backup-btn').text('Verify Code');
          }

      });
    }
  }

  function verfyPassword(){
    $(".err-msg").text('');
    $("#pass-btn").text('Validating...');
    var password = $("#password-code").val();

    if(password.trim().length == 0){
      $(".err-msg").text("Please enter account password");   
      $('#pass-btn').text('Submit');
    }else{
        $('#pass-btn').prop('onclick',null).off('click');
      $.ajax({
              type: 'POST',
              dataType: 'json',
              url: rootUrl+"controllers/wallet/verifyPasswordController.php",
              data: {
                  password: encodeURIComponent(password)
              },

              success: function(data){
              console.log(data);
              if(data.errCode == -1 && changePass== "Change Password"){
                  callChangePassword();
              }else if(data.errCode == -1){
                  $(".err-msg").css("color","green");
                  $(".err-msg").text(data.errMsg);
                  window.location.href=redirect_url;

              }else{
                  $('#pass-btn').attr('onClick','verfyPassword()');
                  $(".err-msg").text(data.errMsg);
              }
                  $('#pass-btn').text('Submit');
              }

          });
    }
  }

  function validateTransPin(){
    $(".err-msg").text('');
    $("#trans-auth-btn").text('Validating...'); 
    var trans_pin = $("#trans-pin").val();

    if(trans_pin.trim().length == 0){
      $(".err-msg").text("Please enter transaction pin");   
      $('#trans-auth-btn').text('Submit');   
    }else{
        $("#trans-auth-btn").prop('onclick',null).off('click');
      $.ajax({
              type: 'POST',
              dataType: 'json',
              url: rootUrl+"controllers/wallet/verifyTransPinController.php",
              data: {
                  trans_pin: encodeURIComponent(trans_pin)
              },

              success: function(data){
                 console.log(data);
                  if(data.errCode == -1){
                    $(".err-msg").css("color","green");
                      $(".err-msg").text(data.errMsg);
                      window.location.href=redirect_url;

                  }else{
                      $(".err-msg").text(data.errMsg);
                      $('#trans-auth-btn').attr('onClick','validateTransPin()');
                  }
                  $('#trans-auth-btn').text('Submit');
              }

          });
    }
  }


  ///------------------------------------to Change Password------------------------------------------------
  function callChangePassword(){
      $.ajax({
          url:'../../controllers/user/changePasswordController.php',
          type: 'POST',
          dataType:'json',
          success: function(data){
              if (data.errCode == -1) {
                  showToast("success",'Password Changed');
                  setTimeout(function(){
                      window.location.href = rootUrl + 'views/user/logout.php';
                  }, 5000);
              }
              else {
                  showToast("failed",'User action Failed');
              }
          },
          error: function(data) {
              showToast("failed",'Error in Connection');
          }
      });
  }

</script>