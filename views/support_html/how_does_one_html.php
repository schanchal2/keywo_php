<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_support.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
  <main class="inner-6x innerT">
 <div class="container">
     <div class="row">
         <div class="col-xs-3">
             <div class="list-group boxshadow general__sidecol">
                 <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover">FAQs</a>
                 <a href="#" class="list-group-item half innerTB bg-white">General</a>
                 <a href="#" class="list-group-item half innerTB">Keywords</a>
                 <a href="#" class="list-group-item half innerTB">Search</a>
                 <a href="#" class="list-group-item half innerTB">Social</a>
                 <a href="#" class="list-group-item half innerTB">Wallet</a>
                 <a href="#" class="list-group-item half innerTB">Referal Program</a>
             </div>

             <div class="list-group boxshadow general__sidecol">
                 <a href="#" class="list-group-item text-black bg-light-gray-color half innerTB nohover">Support Center</a>
                 <a href="#" class="list-group-item half innerTB">Create Ticket </a>
                 <a href="#" class="list-group-item half innerTB">Active Ticket</a>
                 <a href="#" class="list-group-item half innerTB">Support History</a>
             </div>
         </div>

         <div class="col-xs-6 card social-card border-all">
            <div class="row border-bottom">
            <div class="col-xs-12">
                    <div class="text-blue header-title innerTB half">How Does One
                        <span class="text-black-light"> Buy IT$</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 padding-none">
                    <div class="img-wrapper innerMT inner-2x">
                        
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-contain text-justify">
                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
                    </div>
                </div>
            </div>
            
            
            
         </div>
         
         <div class="col-xs-3">
             <div class="list-group boxshadow general__sidecol">
                 <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover no-radius text-white">How does one</a>
                 <a href="#" class="list-group-item half innerTB sidemenu-active "><span class="text-blue ">Buy IT$</span></a>
                 <a href="#" class="list-group-item half innerTB">Cashout IT$</a>
                 <a href="#" class="list-group-item half innerTB">Send / Receive IT$</a>
                 <a href="#" class="list-group-item half innerTB">Make a Search</a>
                 <a href="#" class="list-group-item half innerTB">Buy a Keyword</a>
                 <a href="#" class="list-group-item half innerTB">Trade a Keyword</a>
                 <a href="#" class="list-group-item half innerTB">Follow a Keyword</a>
                 <a href="#" class="list-group-item half innerTB">Follow a People</a>
                 <a href="#" class="list-group-item half innerTB">Follow a Content</a>
                 <a href="#" class="list-group-item half innerTB">Post Content</a>
                 <a href="#" class="list-group-item half innerTB">Share Content</a>
                 <a href="#" class="list-group-item half innerTB">Refer a Friend</a>
                 <a href="#" class="list-group-item half innerTB">Track Earnings</a>
             </div>
         </div>
     </div>
 </div>


  </main>
  <?php
  } else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
  }
  ?>
  <?php include('../layout/social_footer.php'); ?>
  <script type="text/javascript">
    function toggleIcon(e) {
         $(e.target)
         .prev('.panel-heading')
         .find(".gap")
         .toggleClass('fa-plus-circle fa-minus-circle').parent().parent().toggleClass('text-black text-blue');
    }   
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
  </script>