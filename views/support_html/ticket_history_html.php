<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_support.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
  <main class="inner-6x innerT">
 <div class="container">
     <div class="row">
         <div class="col-xs-3">
             <div class="list-group boxshadow general__sidecol">
                 <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover">FAQs</a>
                 <a href="#" class="list-group-item half innerTB bg-white">General</a>
                 <a href="#" class="list-group-item half innerTB">Keywords</a>
                 <a href="#" class="list-group-item half innerTB">Seach</a>
                 <a href="#" class="list-group-item half innerTB">Social</a>
                 <a href="#" class="list-group-item half innerTB">Wallet</a>
                 <a href="#" class="list-group-item half innerTB">Referal Program</a>
             </div>
             <div class="list-group boxshadow general__sidecol">
                 <a href="#" class="list-group-item text-black bg-light-gray-color half innerTB nohover">Support Center</a>
                 <a href="#" class="list-group-item half innerTB">Create Ticket </a>
                 <a href="#" class="list-group-item half innerTB">Active Ticket</a>
                 <a href="#" class="list-group-item half innerTB sidemenu-active"><span class="text-blue">Support History</span></a>
             </div>
         </div>

         <div class="col-xs-6 card social-card border-all padding-none">
              <div class="border-bottom">
            <div class="row">
              <div class="col-xs-12">
                <h4 class="margin-none innerAll text-blue ">Support Center</h4>
              </div></div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h5 class="innerTB half">Support History - Choose an option</h5>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-responsive rating-table">
                        <thead class="text-blue">
                        <tr>
                            <th>Ticket Type</th>
                            <th class="text-center">Opened</th>
                            <th class="text-center">Closed</th>
                            <th class="text-center">Rating</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a class="text-social-gray" data-toggle="modal" data-target="#keyword-ticket-history">Error in Purchasing the Keyword</a></td>
                                <td>12/01/2015</td>
                                <td>25/02/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>General Queries</td>
                                <td>08/03/2015</td>
                                <td>28/03/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>Issue with Billing and Payment</td>
                                <td>25/05/2015</td>
                                <td>05/07/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>Issue with sendingb IT$</td>
                                <td>08/08/2015</td>
                                <td>08/09/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>Issue with IT$ Purchase</td>
                                <td>12/10/2015</td>
                                <td>19/11/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </td>
                            </tr>

                            <tr>
                                <td>Issue with IT$ Cashout</td>
                                <td>21/11/2015</td>
                                <td>28/12/2015</td>
                                <td class="innerMT text-center text-yellow rating">
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                    <i class="fa fa-star-o"></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
         
        <div class="col-xs-3">
            <div class="list-group boxshadow general__sidecol">
                <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover no-radius text-white">How does one</a>
                <a href="#" class="list-group-item half innerTB">Buy IT$</span></a>
                <a href="#" class="list-group-item half innerTB">Cashout IT$</a>
                <a href="#" class="list-group-item half innerTB">Send / Receive IT$</a>
                <a href="#" class="list-group-item half innerTB">Make a Search</a>
                <a href="#" class="list-group-item half innerTB">Buy a Keyword</a>
                <a href="#" class="list-group-item half innerTB">Trade a Keyword</a>
                <a href="#" class="list-group-item half innerTB">Follow a Keyword</a>
                <a href="#" class="list-group-item half innerTB">Follow a People</a>
                <a href="#" class="list-group-item half innerTB">Follow a Content</a>
                <a href="#" class="list-group-item half innerTB">Post Content</a>
                <a href="#" class="list-group-item half innerTB">Share Content</a>
                <a href="#" class="list-group-item half innerTB">Refer a Friend</a>
                <a href="#" class="list-group-item half innerTB">Track Earnings</a>
            </div>
         </div>
     </div>
 </div>

  <!-- popup content -->
 <div id="keyword-ticket-history" class="modal fade keyword-popup  keyword-markt-popup-common" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <h4 class="margin-none innerAll text-blue ">Ticket Type <span class="text-social-gray text-small-ticket">Error in purchasing keyword</span></h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                       <div class="row">          
          <div class="col-xs-12">
            <div class="text-blue ticket-id text-right">
                <strong>Ticket ID : 25085</strong>
            </div>
            <div class="innerB innerMB">
              <div class="chat-container bg-light-purple">
                <div class="innerMLR">
                  <!-- right chat begins -->
                  <div class="innerTB chat-right clearfix">
                    <div class="innerTB arrow_box right-arrow bg-white pull-right">
                      <div class="">
                        oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye kakeoye kakeoye kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- right chat box ends -->
                  <!-- left chat begins -->
                  <div class="innerTB chat-left clearfix">
                    <div class="innerTB arrow_box left-arrow chatbox-other-user pull-left">
                      <div class="">
                        bol kabol kakebol kakebol kakebol kakekebbol kakebol kakebol kakebol kakebol kakebol kakebol kakeol kakebol kakebol kakebol kakebol kakebol kakebol kakebol kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- left chat ends -->
                  <div class="text-center">
                    <div class="chat-date bg-light-purple">
                      21st oct, 2017
                    </div>
                  </div>
                  <!-- right chat begins -->
                  <div class="innerTB chat-right clearfix">
                    <div class="innerTB arrow_box right-arrow bg-white pull-right">
                      <div class="">
                        oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye kakeoye kakeoye kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- right chat box ends -->
                  <!-- right chat begins -->
                  <div class="innerTB chat-right clearfix">
                    <div class="innerTB arrow_box right-arrow bg-white pull-right">
                      <div class="">
                        oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye kakeoye kakeoye kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- right chat box ends -->
                  <!-- left chat begins -->
                  <div class="innerTB chat-left clearfix">
                    <div class="innerTB arrow_box left-arrow chatbox-other-user pull-left">
                      <div class="">
                        bol kabol kakebol kakebol kakebol kakekebbol kakebol kakebol kakebol kakebol kakebol kakebol kakeol kakebol kakebol kakebol kakebol kakebol kakebol kakebol kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- left chat ends -->
                  <!-- right chat begins -->
                  <div class="innerTB chat-right clearfix">
                    <div class="innerTB arrow_box right-arrow bg-white pull-right">
                      <div class="">
                        oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye oye kakeoye kakeoye kakeoye kakeoye kakeoye kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- right chat box ends -->
                  <!-- left chat begins -->
                  <div class="innerTB chat-left clearfix">
                    <div class="innerTB arrow_box left-arrow chatbox-other-user pull-left">
                      <div class="">
                        bol kabol kakebol kakebol kakebol kakekebbol kakebol kakebol kakebol kakebol kakebol kakebol kakeol kakebol kakebol kakebol kakebol kakebol kakebol kakebol kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- left chat ends -->
                  <!-- left chat begins -->
                  <div class="innerTB chat-left clearfix">
                    <div class="innerTB arrow_box left-arrow chatbox-other-user pull-left">
                      <div class="">
                        bol kabol kakebol kakebol kakebol kakekebbol kakebol kakebol kakebol kakebol kakebol kakebol kakeol kakebol kakebol kakebol kakebol kakebol kakebol kakebol kake
                      </div>
                      <div class="text-right text-grayscale-80 chat-time">
                        <span class="">12:36 am</span>
                      </div>
                    </div>
                  </div>
                  <!-- left chat ends -->
                </div>
              </div>
            </div>
        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
      <!-- popup content -->
  </main>
  <?php
  } else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
  }
  ?>
  <?php include('../layout/social_footer.php'); ?>
  <script type="text/javascript">
    function toggleIcon(e) {
         $(e.target)
         .prev('.panel-heading')
         .find(".gap")
         .toggleClass('fa-plus-circle fa-minus-circle').parent().parent().toggleClass('text-black text-blue');
    }   
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
  </script>