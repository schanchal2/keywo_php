<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_support.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
  <main class="inner-6x innerT">
  <div class="container">
    <div class="row">
      <div class="col-xs-3">
        <div class="list-group boxshadow general__sidecol">
          <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover">FAQs</a>
          <a href="#" class="list-group-item sidemenu-active half innerTB sidemenu-active"><span class="text-blue">General</span></a>
          <a href="#" class="list-group-item half innerTB">Keywords</a>
          <a href="#" class="list-group-item half innerTB">Search</a>
          <a href="#" class="list-group-item half innerTB">Social</a>
          <a href="#" class="list-group-item half innerTB">Wallet</a>
          <a href="#" class="list-group-item half innerTB">Referal Program</a>
        </div>
        <div class="list-group boxshadow general__sidecol">
          <a href="#" class="list-group-item text-black bg-light-gray-color half innerTB nohover">Support Center</a>
          <a href="#" class="list-group-item half innerTB">Create Ticket </a>
          <a href="#" class="list-group-item half innerTB">Active Ticket</a>
          <a href="#" class="list-group-item half innerTB">Support History</a>
        </div>
      </div>
      <div class="col-xs-6 padding-none faq-wrapper display-block bg-white border-all">
        <!-- <div class="border-all"> -->
        <div class="col-xs-12 border-bottom heading">
          <h4 class="margin-none innerAll text-blue ">FAQs</h4>
          <span>General</span>
        </div>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h5 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                Is Wordpress available in my Language?
              </a>
              </h5>
            </div>
            <div id="collapseOne" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch etc.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                How is Wordpress related to other blogging applications?
              </a>
              </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading bg-white nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                Why choose wordpress?
              </a>
              </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading bg-white nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                Why choose wordpress?
              </a>
              </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseSeven" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseEight">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseEight" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseNine">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseNine" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
          <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading nospace">
              <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
              <h4 class="panel-title">
              <a class="accordion-toggle change" data-toggle="collapse" data-parent="#accordion" href="#collapseTen">
                What is the difference between wordpress.org and wordpress.com?
              </a>
              </h4>
            </div>
            <div id="collapseTen" class="panel-collapse collapse innerL inner-2x">
              <div class="panel-body border-none padding-top-none panel_margin">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
              </div>
            </div>
          </div>
        </div>
        <!-- </div> -->
      </div>
      <div class="col-xs-3">
        <div class="list-group boxshadow general__sidecol">
          <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover no-radius text-white">How does one</a>
          <a href="#" class="list-group-item half innerTB">Buy IT$</a>
          <a href="#" class="list-group-item half innerTB">Cashout IT$</a>
          <a href="#" class="list-group-item half innerTB">Send / Receive IT$</a>
          <a href="#" class="list-group-item half innerTB">Make a Search</a>
          <a href="#" class="list-group-item half innerTB">Buy a Keyword</a>
          <a href="#" class="list-group-item half innerTB">Trade a Keyword</a>
          <a href="#" class="list-group-item half innerTB">Follow a Keyword</a>
          <a href="#" class="list-group-item half innerTB">Follow a People</a>
          <a href="#" class="list-group-item half innerTB">Follow a Content</a>
          <a href="#" class="list-group-item half innerTB">Post Content</a>
          <a href="#" class="list-group-item half innerTB">Share Content</a>
          <a href="#" class="list-group-item half innerTB">Refer a Friend</a>
          <a href="#" class="list-group-item half innerTB">Track Earnings</a>
        </div>
      </div>
    </div>
  </div>
  </main>
  <?php
  } else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
  }
  ?>
  <?php include('../layout/social_footer.php'); ?>
  <script type="text/javascript">
  function toggleIcon(e) {
  $(e.target)
  .prev('.panel-heading')
  .find(".gap")
  .toggleClass('fa-plus-circle fa-minus-circle').toggleClass('text-black text-blue').parent().parent().toggleClass('text-black text-blue');
  }
  $('.panel-group').on('hidden.bs.collapse', toggleIcon);
  $('.panel-group').on('shown.bs.collapse', toggleIcon);
  </script>