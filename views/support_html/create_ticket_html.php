<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_support.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
  <main class="inner-7x innerT">
  <div class="container">
    <div class="row">
      <div class="col-xs-3">
          <div class="list-group boxshadow general__sidecol">
              <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover">FAQs</a>
              <a href="#" class="list-group-item sidemenu-active half innerTB">General</span></a>
              <a href="#" class="list-group-item half innerTB">Keywords</a>
              <a href="#" class="list-group-item half innerTB">Search</a>
              <a href="#" class="list-group-item half innerTB">Social</a>
              <a href="#" class="list-group-item half innerTB">Wallet</a>
              <a href="#" class="list-group-item half innerTB">Referal Program</a>
          </div>
          <div class="list-group boxshadow general__sidecol">
              <a href="#" class="list-group-item text-black bg-light-gray-color half innerTB nohover">Support Center</a>
              <a href="#" class="list-group-item half innerTB sidemenu-active"><span class="text-blue">Create Ticket</span></a>
              <a href="#" class="list-group-item half innerTB">Active Ticket</a>
              <a href="#" class="list-group-item half innerTB">Support History</a>
          </div>
      </div><!-- col-md-3 left panel closed-->
      <div class="col-xs-6 innerMB inner-2x">
        <div class="row card social-card innerB inner-7x">
          <div class="col-xs-12 border-bottom heading">
            <h4 class="margin-none innerAll text-blue ">Create Ticket</h4>
          </div>
          <div class="col-xs-12">
            <div class="text-center innerMT">
              <div class="text-blue">
                How can we help you today?
              </div>
              <p>
                Would you wish to contact us for any reason, this is the place to do so.<br>
                Please submit your details, share your comments with us & we will get back to you as soon as possible.
                We'd like to hear from you.
              </p>
            </div>
          </div>
          <div class="col-xs-12">
            <ul class="innerL text-blue issue-list">
              <li>
                KEYWORD MINING
                <ul class="innerL inner-2x">
                  <li>
                    <span data-toggle="modal" data-target="#supportForm">Error in purchasing keyword</span>
                  </li>
                  <li>
                    Trading related issue (Bid, Buy, Ask, Sell)
                  </li>
                </ul>
              </li>
              <li>
                WALLET (DEPOSIT & WITHDRAWAL)
                <ul class="innerL inner-2x">
                  <li>
                    Issue with IT$ purchase
                  </li>
                  <li>
                    Issue with IT$ cashout
                  </li>
                  <li>
                    Issue with sending IT$
                  </li>
                  <li>
                    Issue with receiving IT$
                  </li>
                </ul>
              </li>
              <li>
                AD PROGRAM
                <ul class="innerL inner-2x">
                  <li>
                    Issue with billing and payment
                  </li>
                  <li>
                    Issue with campaign
                  </li>
                </ul>
              </li>
              <li>
                OTHERS
                <ul class="innerL inner-2x">
                  <li>
                    General Queries
                  </li>
                  <li>
                    Feedback / Suggestions
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-3">
          <div class="list-group boxshadow general__sidecol">
              <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover no-radius text-white">How does one</a>
              <a href="#" class="list-group-item half innerTB">Buy IT$</a>
              <a href="#" class="list-group-item half innerTB">Cashout IT$</a>
              <a href="#" class="list-group-item half innerTB">Send / Receive IT$</a>
              <a href="#" class="list-group-item half innerTB">Make a Search</a>
              <a href="#" class="list-group-item half innerTB">Buy a Keyword</a>
              <a href="#" class="list-group-item half innerTB">Trade a Keyword</a>
              <a href="#" class="list-group-item half innerTB">Follow a Keyword</a>
              <a href="#" class="list-group-item half innerTB">Follow a People</a>
              <a href="#" class="list-group-item half innerTB">Follow a Content</a>
              <a href="#" class="list-group-item half innerTB">Post Content</a>
              <a href="#" class="list-group-item half innerTB">Share Content</a>
              <a href="#" class="list-group-item half innerTB">Refer a Friend</a>
              <a href="#" class="list-group-item half innerTB">Track Earnings</a>
          </div>
      </div><!-- col-md-3 right panel closed-->
    </div>
  </main>
<!-- Modal -->
<div id="supportForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-title text-blue">
            <div>
              Error in purchasing keyword
            </div>
      </div>
      </div>
      <form>
        <div class="modal-body padding-bottom-none">
          <div class="clearfix innerMB">
            <div class="pull-left innerMT">
              <div class="support-form-label">Keyword purchased &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="pull-right">
              <input type="text" class="form-control kwd-purchase-input"/>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="pull-left half innerMT">
              <div class="support-form-label">Payment date & time &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="pull-right">
              <input type="text" class="form-control"/>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="pull-left half innerMT">
              <div class="support-form-label">Payment mode &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="pull-right">
              <select>
                <option value="Wallet">Wallet</option>
                <option value="BitGo">BitGo</option>
                <option value="Paypal">Paypal</option>
              </select>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="pull-left half innerMT">
              <div class="support-form-label">Amount in IT$ &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="pull-right">
              <input type="text" class="form-control"/>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="pull-left half innerMT">
              <div class="support-form-label">Transaction hash of payment &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="pull-right">
              <input type="text" class="form-control"/>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="half innerMTB">
              <div class="support-form-label">BTC address through payment transferred &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="">
              <input type="text" class="form-control btc-address-input"/>
            </div>
          </div>
          <div class="clearfix innerMB">
            <div class="half innerMTB">
              <div class="support-form-label">Comment &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
            </div>
            <div class="">
              <textarea type="text" class="form-control" rows="3"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer innerT innerMB">
          <div class="pull-right text-blue">
            <div class="btn btn-xs reset-btn">
              RESET
            </div>
            <div class="btn btn-xs submit-btn">
              SUBMIT
            </div>
            <!-- <input type="button" value="RESET"/>
            <input type="button" value="SUBMIT"/> -->
          </div>
        </div>
      </form>


    </div>

  </div>
</div>
  <?php
  } else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
  }
  ?>
  <?php include('../layout/social_footer.php'); ?>
