<?php

/* Search Landing Page */
session_start();

//include required files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../models/search/searchResultModel.php');
require_once('../../models/search/searchLandingModel.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/sessionHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');

error_reporting(0);
// connecting to keywords Database
$kwdConn = createDbConnection('dbkeywords');

if(noError($kwdConn)){
  $kwdConn = $kwdConn["connection"];
}else{
  print("Keyword Database Error");
  exit();
}



//  session existence
checkForSession($kwdConn);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
  $email = $_SESSION["email"];
  $login_status = 1;
}else{
  $login_status = 0;
}


// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
	
  $connSearch = $connSearch["connection"];
  // fetching app detail from search database in sc_app_details.
  	if($login_status == 1){
		// // getting logged in user's default app id
    $searchAppId = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/','default_search_appId');
		
		// print_r($searchAppId);die();
		
		if(noError($searchAppId )){
			$searchAppId = $searchAppId["errMsg"]['default_search_appId'];
			if(!empty($searchAppId)){
				// getting app details of user's default app.
				$appDetails = getAppDetails($connSearch, $searchAppId);
				
				if(noError($appDetails)){
					$appDetails    = $appDetails["errMsg"][0];
					$appName       = $appDetails["app_name"];
					$appId         = $appDetails["app_id"];
					$appLogo       = $appDetails["app_logo"];
					$appURL        = $appDetails["searchresult_url"];
					$appSearchLogo = $appDetails["searchengine_images"];
					$landingPage   = $appDetails["landingPage_url"];
				} else {
					print("Error: ".$appDetails["errMsg"]);
					exit;
				}
			}
		}else{
			print("Error: ".$searchAppId["errMsg"]);
			exit;
		}
	}


	// if default app id is blank, get admin set default app
	if(empty($searchAppId)){
		//getting default search app as set by admin.
		$appDetails = getDefaultAppId($connSearch);
		if(noError($appDetails)){
			$appName       = $appDetails["errMsg"][0]["app_name"];
			$appURL        = $appDetails["errMsg"][0]["searchresult_url"];
			$appId         = $appDetails["errMsg"][0]["app_id"];
			$appLogo       = $appDetails["errMsg"][0]["searchengine_images"];
			$appSearchLogo = $appDetails["errMsg"][0]["searchengine_images"];
			$landingPage   = $appDetails["errMsg"][0]["landingPage_url"];
			
		}else{
			print("Error Fetching default search engine");
			exit;
		}
	}
}else{
  print("Error: Database connection");
  exit;
}

//include header file


?>
<main>
  <div class="container">
  <!-- Call Landing page here-->
  <div class="loadLandingPage"></div>
  <div class="row">
    <?php
        include('../search/appFooter.php');
    ?>
  </div>
  <div class="row">
    <div class="col-xs-4">
      <div class="card">
        <h5 class="innerAll text-center text-blue margin-bottom-none">
          <b>REFER AND EARN</b>
        </h5>
        <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray">
          Refer your friend to searchtrade and earn income when they search or buy keywords
        </p>
        <div class="row">
          <div class="col-xs-6">
            <h5 class="text-red text-center">
              <b class="search-landing-buttons">REFER NOW</b>
            </h5>
          </div>
          <div class="col-xs-6">
            <h5 class="text-center">
              <b class="search-landing-buttons">LEARN MORE</b>
            </h5>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="card">
        <h5 class="innerAll text-center text-blue margin-bottom-none">
          <b>INVEST IN KEYWORDS</b>
        </h5>
        <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray">
          Buy search keywords and earn residual income everytime word you own is searched
        </p>
        <div class="row">
          <div class="col-xs-6">
            <h5 class="text-red text-center">
              <b class="search-landing-buttons">BUY NOW</b>
            </h5>
          </div>
          <div class="col-xs-6">
            <h5 class="text-center">
              <b class="search-landing-buttons">LEARN MORE</b>
            </h5>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xs-4">
      <div class="card">
        <h5 class="innerAll text-center text-blue margin-bottom-none">
          <b>ANDROID APP</b>
        </h5>
        <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray" >
          Download Android app today and experiance searchtrade on your mobile
        </p>
        <div class="row">
          <div class="col-xs-12">
            <h5 class="text-center">
              <b class="search-landing-buttons">DOWNLOAD NOW</b>
            </h5>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>
<?php //include '../layout/after_login_footer.php' ?>
<!-- Script for JavaScript -->
<script type="text/javascript">

  $(document).ready(function() {

    var rootUrl     	=  '<?php echo $rootUrl; ?>';
    var loginStatus     = '<?php echo $login_status; ?>';
    var appId 			= '<?php echo $appId; ?>';
    var searchRootUrl	= '<?php echo $rootUrlSearch.strtolower(ucwords($appName))."/"; ?>';
    var landing 		= '<?php echo $landingPage; ?>';
    var appLogo 		= '<?php echo $appLogo;  ?>';
    var appName 		= '<?php echo $appName; ?>';
    var appURL 		    = '<?php echo $appURL; ?>';
    var systemSwitchMode = $('#modSwitchId').attr('value');
	if (systemSwitchMode != 2) {
	  var newUrl2 = searchRootUrl;
	  //redirect to new page
	  window.history.pushState('page23', 'search3', newUrl2);
	}
    
    searchLandingAjax(loginStatus, searchRootUrl, landing, appId,  rootUrl, appLogo, appName, appURL);
  });


</script>
<script src="<?php echo $rootUrl;?>js/search.js"></script>

<h6 style="margin-top: 80px"><center>DEMO <span style="color:red;"> Search </span>Landing Page</center></h6>
