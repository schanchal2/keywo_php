<?php

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");



?>
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="model_header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close "></i></button>
                <div class="model-title text-blue">
                    <h4>Trading Related Issues</h4>
                </div>
            </div>

            <div class="row">
                <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
            </div>
            
            <form id="resetFormTrade">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="inputKeyword">Keyword<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="hidden" class="form-control" id="requesttype" name="requesttype" value="trading related issues"/>
<!--                                <input type="text" id="fullname" name="fullname" required class="textbox" placeholder="Lastname Firstname Middlename"  pattern="^\S+$">-->
                                <input type="text" class="form-control" placeholder="Enter Single Keyword Traded" name="keyword" id="keyword" pattern="^\S+$">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="selectTradeType">Select Trade Type<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <select class="form-control" name="tradeType" id="tradeType">
                                    <option disabled selected value="">Trade Type</option>
                                    <option value="bid">Bid</option>
                                    <option value="buy">Buy</option>
                                    <option value="ask">Ask</option>
                                    <option value="sell">Sell</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="inputAmount">Trade Amount in <?php echo $keywoDefaultCurrencyName; ?><i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="text" class="form-control" id="tradeAmount" name="tradeAmount" onkeypress="return isNumberKey(event)" placeholder="Enter Amount Here">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="control-label">
                                <label>Received Notification Email about Trade<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            </div>
                            <div class="control-label" name="recNotification" id="recNotification">
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" value="yes">Yes
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" value="No">No
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="comments">Comments &nbsp;&nbsp;:<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i>
                            </label>
                            <textarea class="form-control textAreaSize" maxlength="1000" rows="3" id="comments" name="comments" rows="5.5" placeholder="Enter Your Comments Here, Maximum Limit is 1000"></textarea>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-xs-8">
                            <div class="modal-footer text-center">
                                <button type="reset"  class="btn btn-md btn-primary reserAllSupportForms" id="reset">Reset</button>
                                <button type="button" class="btn btn-md btn-primary" onclick="submitTradingModel();">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
<script>

    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);


    function submitTradingModel()
    {
        var requestType     = document.getElementById("requesttype").value;
        var keywordName     = (document.getElementById("keyword").value).trim();
        var tradeType       = ($('#tradeType').find(":selected").text()).trim();
        var tradeAmount     = (document.getElementById("tradeAmount").value).trim();
        var recNotification = $('input[name=optradio]:checked', '#recNotification').val();

        if (tradeType == 'Bid') {
            var transactionAddress = 'bid';
        } else if (tradeType == 'Buy') {
            var transactionAddress = 'buy';
        } else if (tradeType == 'Ask') {
            var transactionAddress = 'ask';
        } else if (tradeType == 'Sell') {
            var transactionAddress = 'sell';
        } else {
            var transactionAddress = '';
        }

        if(recNotification==undefined)
        {
            recNotification="";
        }

        var comment         = (document.getElementById("comments").value).trim();


        if(keywordName.match(' ')){
            $(".modalErrorData").html("Please Enter Single Keyword!");
            //alert('Please Enter Single Keyword!');
            return false;
        }


        if ( keywordName == "" || tradeType == "" || tradeAmount == "" || recNotification == "" || comment == "" || transactionAddress == "") {
            $(".modalErrorData").html("All fields are Mandatory");
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: rootUrl+'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType     : requestType,
                    keywordName     : keywordName,
                    tradeType       : tradeType,
                    amountITD       : tradeAmount,
                    recNotification : recNotification,
                    comment         : comment

                },
                async: true,
                success: function (data) {

                    $('.modal-backdrop').hide();
                    $('#tradingIssueForm').hide();
                    blockCreateTicket();

                    var msg=data["errMsg"];
                    showToast("success", msg);

                    $("#main_container").load("active_ticket.php");
                }
            });

        }

    }

function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}    

</script>