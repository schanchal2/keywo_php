<?php

// echo "Testing"; die;
session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");

$email = $_SESSION["email"];

?>

<div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
        <div class="model_header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close "></i></button>
            <div class="model-title text-blue">
                <h4>General Queries / Feedback</h4>
            </div>
        </div>

        <div class="row">
            <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
        </div>

        <form id="resetFormGeneral">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div>
                            <p>Please give brief description.</p>
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control" id="requesttypeFeedback" value="other_general">
                            <label for="inputKeyword">Registred Email ID &nbsp&nbsp: <i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="email" class="form-control" id="email" placeholder="Enter your Email Id" value="<?php echo $email; ?>" disabled/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label for="inputAmount">Comments &nbsp&nbsp:<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i>
                        </label>
                        <textarea class="form-control textAreaSize" rows="3" id="commentsFeedback" rows="5.5" placeholder="Enter Your Comments Here, Maximum Limit is 1000" maxlength="1000"></textarea>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-xs-8">
                        <div class="modal-footer text-center">
                            <button type="reset" class="btn btn-md btn-primary reserAllSupportForms" id="reset">Reset</button>
                            <button type="button" class="btn btn-md btn-primary" id="submit" onclick="submitFeedBackModel();">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);



    function submitFeedBackModel()
    {
       // alert("inserted in function");
        var requesttypeFeedback  = (document.getElementById("requesttypeFeedback").value).trim(); //alert(requesttypeFeedback);
        var commentsFeedback     = (document.getElementById("commentsFeedback").value).trim(); //alert(commentsFeedback);

        if (requesttypeFeedback == "" || commentsFeedback == "") {
            $(".modalErrorData").html("All fields are Mandatory");
        } else {
            $.ajax({
                type: 'POST',
                url: rootUrl+'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType     : requesttypeFeedback,
                    comment         : commentsFeedback

                },
                async: true,
                success: function (data) {

                    $('.modal-backdrop').hide();
                    $('#generalQuery').hide();
                    blockCreateTicket();

                    var msg=data["errMsg"];
                    showToast("success", msg);


                    $("#main_container").load("active_ticket.php");

                }
            });

        }
    }

</script>

