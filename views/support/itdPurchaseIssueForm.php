﻿<?php

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");


?>


<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="model_header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close "></i></button>
            <div class="model-title text-blue">
                <h4>Issue with <?= $keywoDefaultCurrencyName; ?> Purchase</h4>
            </div>
        </div>

        <div class="row">
            <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
        </div>

        <form id="resetFormpurchase">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputKeyword">Deposite Amount in <?php echo $keywoDefaultCurrencyName; ?> &nbsp&nbsp: <i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="hidden" class="form-control" id="type" value="issue with it purchase">
                            <input type="text" class="form-control allownumericwithoutdecimal" id="depositeAmt" placeholder="Enter Deposite Amount">
                        </div>
                    </div>

                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputKeyword">Date of Purchase &nbsp&nbsp: <i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="text" class="form-control datepicker" id="creditdepositeDate" name="depositeDate" placeholder="Select Date"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="selectTradeType">Purchase Payment Method<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <select class="form-control" required id="purchaseType" onchange="ShowHideDivPurchase()">
                                <option disabled selected value="">Cashout Type</option>
                                <option value="Paypal">Paypal</option>
                                <option value="BitGo">BitGo</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputKeyword">Transaction Hash &nbsp&nbsp:</i></label>
                            <input type="text" class="form-control" id="transaction" placeholder="Enter Transaction Has">
                        </div>
                    </div>
                </div>

                <div class="col-xs-6">

                    <div class="form-group" id="dvPassport2" style="display: none">
                        <label for="inputAmount">BitGo Address<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                        <input type="text" class="form-control" id="btcPurchaseAddress" placeholder="Enter BitGo Address">
                    </div>

                    <div class="form-group" id="dvPassport1" style="display: none">
                        <label for="inputAmount">Paypal Address<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                        <input type="text" class="form-control" id="paypalPurchaseaddress" placeholder="Enter Paypal Address">
                    </div>

                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <label for="commentsPurchase">Comments &nbsp&nbsp:<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i>
                        </label>
                        <textarea class="form-control textAreaSize" rows="3" id="commentsPurchase" name="commentsPurchase" rows="5.5" placeholder="Enter Your Comments Here, Maximum Limit is 1000" maxlength="1000"></textarea>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-xs-8">
                        <div class="modal-footer text-center">
                            <button type="reset"  class="btn btn-md btn-primary reserAllSupportForms" id="reset">Reset</button>
                            <button type="button" class="btn btn-md btn-primary" id="submit" onclick="submitPurchaseModel();">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>
    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);

    function ShowHideDivPurchase() {
        //alert("inserted in function");
        var purchaseType = document.getElementById("purchaseType");
        var dvPassport2 = document.getElementById("dvPassport2");
        var dvPassport1 = document.getElementById("dvPassport1");
        dvPassport2.style.display = purchaseType.value == "BitGo" ? "block" : "none";
        dvPassport1.style.display = purchaseType.value == "Paypal" ? "block" : "none";
    }

    $('#depositeDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#depositeDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#depositeDate').on('drop', function(event) {
        event.preventDefault();
    });

    function submitPurchaseModel()
    {
        var type                  = (document.getElementById("type").value).trim();
        var purchaseType          = (document.getElementById("purchaseType").value).trim();
        var depositeAmt           = (document.getElementById("depositeAmt").value).trim();
        var depositeDate          = (document.getElementById("creditdepositeDate").value).trim();
        var btcPurchaseAddress    = (document.getElementById("btcPurchaseAddress").value).trim();
        var paypalPurchaseaddress = (document.getElementById("paypalPurchaseaddress").value).trim();
        var transaction           = (document.getElementById("transaction").value).trim();
        var commentsPurchase      = (document.getElementById("commentsPurchase").value).trim();

        if (purchaseType == 'BitGo') {
            var transactionAddress = btcPurchaseAddress;
        } else if (purchaseType == 'Paypal') {
            var transactionAddress = paypalPurchaseaddress;
        } else {
            var transactionAddress = '';
        }

        if (purchaseType == "" || depositeAmt == "" || commentsPurchase == "" || depositeDate == "" || transactionAddress == "") {
            $(".modalErrorData").html("All fields are Mandatory");
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: rootUrl+'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType     : type,
                    payment         : depositeDate,
                    amountITD       : depositeAmt,
                    BTCaddress      : btcPurchaseAddress,
                    paypal          : paypalPurchaseaddress,
                    hashPayment     : transaction,
                    comment         : commentsPurchase

                },
                async: true,
                success: function (data) {

                    $('.modal-backdrop').hide();
                    $('#itPurchaseIssueForm').hide();
                    blockCreateTicket();

                    var msg=data["errMsg"];
                    showToast("success", msg);

                    $("#main_container").load("active_ticket.php");

                }
            });
        }
    }

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });

    $(document).ready(function () {
       // alert("inserted in ITD purchase");

        var today = new Date();
        $('#creditdepositeDate').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

        $('#creditdepositeDate').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

    });


</script>
