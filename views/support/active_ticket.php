<?php
session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../models/customer/customerModel.php');

$dbkeywordDbConn = createDBConnection("dbkeywords");
$login_status = '';
if(noError($dbkeywordDbConn)){
    $dbkeywordDbConn = $dbkeywordDbConn["connection"];
}
$email           = $_SESSION['email'];

$allTickeDetails = getRequestTicketDetails($email,$dbkeywordDbConn);

if(noError($allTickeDetails)){
    if(count($allTickeDetails["errMsg"])>0) {
        $allTickeDetails = $allTickeDetails["errMsg"][0];
        $ticketID = $allTickeDetails["ticket_id"];
    }else{
        $ticketID="";
    }
}
if($ticketID !="")
{
?>

<div id = "NoActiveTicketFound" activeValue = "1"  class="">
    <div class="row card social-card innerMB inner-2x">
        <div class="col-xs-12 border-bottom heading">
            <h4 class="margin-none innerAll text-blue ">Ticket Type</h4>
            <span><?php echo $allTickeDetails['request_type']; ?></span>
            <span data-toggle="modal" data-target="#tickit-close-confirmation" class="close-ticket innerMT pull-right text-color-grayscale-80" onclick = "closingTicketConfirm('<?php echo $ticketID; ?>');"><i class="fa fa-close"></i></span>
        </div>
        <div class="col-xs-12">
            <div class="text-blue ticket-id text-right">
                <strong>Ticket ID :<?php echo $ticketID; ?></strong>
            </div>

            <input type="hidden" id="ticket-details" value ="<?php echo $ticketID;?>">


            <!-- <div class="text-center">
              <div class="chat-date bg-light-purple">
                21st oct, 2017
              </div>
            </div> -->
            <div class="innerB innerMB border-bottom" id ="message-user-details">

            </div>

            <form id="message-post" name="message-post" class="innerAll padding-bottom-none padding-top-none" >
                <div class="chat-input position-relative">
                    <textarea class="" rows="1" name="shout_message" id="shout_message"></textarea>
                    <div class="button__chat--div" id="createNewComment" ajax_call = "true">
                        <div class="button__chat--box">
                            <i class="fa fa-send text-blue" style="font-size:14px;"></i>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<script>



   // $(document).ready(function(){
        $("#message-user-details").load("chatList.php?ticket_id=<?php echo $ticketID; ?>");
       // var elem = document.getElementById('userMessage');
        //elem.scrollTop = elem.scrollHeight;
   // });

        function todayDateData() {
          var now = new Date();
          var day = ("0" + now.getDate()).slice(-2);
          var month = ("0" + (now.getMonth() + 1)).slice(-2);
          var today = (day)+ "-" + (month) + "-" +now.getFullYear();
          return today;
        }

//    $('#shout_message').keypress(function(e) {
//      if (e.which == 13) {
//        $('#createNewComment').click();
//      }
//    });

   $("#shout_message").keypress(function(e){
       if (e.keyCode == 13 && !e.shiftKey)
       {
           $('#createNewComment').click();
       }
   });

    $('#createNewComment').on('click',function(e) {
        var comments=$("#shout_message").val();
        var lastmsgs=$("#message-user-details").find(".chat_msg_div").last().attr("id");
        if(typeof lastmsgs!= "undefined") {
            lastmsgs = lastmsgs.split("_");
            var lastMSGid = lastmsgs[1];
        }else {
            var lastMSGid = 0;
        }
        var userType="customer";
        var ticketID='<?php echo $ticketID; ?>';
        var emailAgent='<?php echo $email; ?>';
        var ajaxCall = $('#createNewComment').attr('ajax_call');
        var commentCheck = $.trim(comments);

        if (ajaxCall == "true" && commentCheck != "") {

          $.ajax({
              type: "POST",
              dataType: "html",
              url: "chatList.php",
              data: {
                  comments: comments, userType: userType,emailAgent:emailAgent,ticketID:ticketID,lastMSGid:lastMSGid
              },
              beforeSend: function(data){
                $('#createNewComment').attr('ajax_call','false');
              },
              success: function (data) {
                var todayDate = todayDateData();
                var dateId = $('#message-user-details').find('#'+todayDate).length;
                if (dateId == 1) {
                  var response = $('<div />').html(data);
                  var responseDiv = response.find('#'+todayDate);
                  $(responseDiv).remove();
                } else {
                  var response =data;
                }
                var ticketStatus = $(response).find('#userMessage').attr('ticketStatus');
                if (ticketStatus == 54) {
                  showToast("success","Your Ticket Is Expired<br>Please Create New",20000);
                  loadActiveTicket();
                } else {
                  $("#userMessage").append(response);
                  var elem = document.getElementById('userMessage');
                  $("#shout_message").val('');
                  elem.scrollTop = elem.scrollHeight;
                }   
              },
              complete: function(data){
                $('#createNewComment').attr('ajax_call','true');
              },
              error: function () {
                  console.log("fail");
              }

          });
        }
    });



       var updateChatWindows = setInterval(updateChatWindows, <?php echo $chatRefreshTime; ?>);// milliseconds
       function updateChatWindows() {
           var ststus=$(".chat_msg_div").is(":visible");
           if(ststus==true) {
               updateChatWindow();
           }
       }


      

      function updateChatWindow() {
        var lastmsgs=$("#message-user-details").find(".chat_msg_div").last().attr("id");

        if(typeof lastmsgs!= "undefined") {
          lastmsgs = lastmsgs.split("_");
          var lastMSGid = lastmsgs[1];
        }else {
          var lastMSGid = 0;
        }
        var ticketID='<?php echo $ticketID; ?>';
        var ajaxCall = $('#createNewComment').attr('ajax_call');
        var lstMsg=checklastmsg(ticketID);
        if(lastMSGid != lstMsg && ajaxCall == "true") {
          $.ajax({
            type: "POST",
            dataType: "html",
            url: "chatList.php",
            data: {
              lastMSGid:lastMSGid,ticketID:ticketID
            },
            beforeSend: function(){

            },
            success: function (data) {

              if (data != "") {
                var todayDate = todayDateData();
                var dateId = $('#message-user-details').find('#'+todayDate).length;
                if (dateId == 1) {
                  var response = $('<div />').html(data);
                  var responseDiv = response.find('#'+todayDate);
                  $(responseDiv).remove();
                } else {
                  var response =data;
                }
                $("#userMessage").append(response);
                var elem = document.getElementById('userMessage');
                elem.scrollTop = elem.scrollHeight;

              } else {
                console.log("nothing to update");
              }
            },
            error: function () {
              console.log("fail");
            }
          });
        }else {
          console.log("nothing to update");
        }
      }

   function checklastmsg(ticketID)
   {
       var lastmsg="";
       $.ajax({
           type: "POST",
           dataType: "json",
           url: "../../controllers/customer/check_last_msg.php",
           async:false,
           data: {
               ticketID:ticketID,operation:"checkMsg"
           },
           success: function (data) {
               lastmsg=data["errMsg"];
           }
       });
       return lastmsg;
   }

</script>
<?php
}else
{

?>
  <div id = "NoActiveTicketFound" activeValue = "0" class="clearfix">
    <div class="row card social-card innerMB inner-2x">
    <div  style = "margin:100px 50px 100px 50px; ">
      <center>
      <h3>
        No Active Ticket 
        </h3>
        <a id="genTicketLink">Click Here if You have Any Query To Generate Ticket.</a>
      </center>
      </div>
        
    </div>
</div>

<script>
$("#genTicketLink").click(function(){
  createTicket();
});
</script>

<?php
}

?>