<?php
session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../models/customer/customerModel.php');
require_once('../../models/support/queryListModel.php');
if (!isset($_SESSION["email"]) && empty($_SESSION["email"])) {
    echo "email Not set";
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $rootUrl . "';\", 000);");
    print("</script>");
    die;
}
$dbkeywordDbConn = createDBConnection("dbkeywords");
if (noError($dbkeywordDbConn)) {
    $dbkeywordDbConn = $dbkeywordDbConn["connection"];
}
$category = $_GET['category'];

$how_do_one = array();
$how_do_one["follow_people"]["title"] = "Follow People";
$how_do_one["follow_people"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["follow_people"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";

$how_do_one["follow_keyword"]["title"] = "Follow a Keyword";
$how_do_one["follow_keyword"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["follow_keyword"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";


$how_do_one["trade_keyword"]["title"] = "Trade a Keyword";
$how_do_one["trade_keyword"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["trade_keyword"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";


$how_do_one["buy_keyword"]["title"] = "Buy a Keyword";
$how_do_one["buy_keyword"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["buy_keyword"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";


$how_do_one["send_receive_itd"]["title"] = "Send / Receive {$keywoDefaultCurrencyName}";
$how_do_one["send_receive_itd"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["send_receive_itd"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";


$how_do_one["cashout_itd"]["title"] = "Cashout {$keywoDefaultCurrencyName}";
$how_do_one["cashout_itd"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["cashout_itd"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";

$how_do_one["post_content"]["title"] = "Post Content";
$how_do_one["post_content"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["post_content"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";


$how_do_one["share_content"]["title"] = "Share Content";
$how_do_one["share_content"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["share_content"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";

$how_do_one["refer_friend"]["title"] = "Refer a Friend";
$how_do_one["refer_friend"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["refer_friend"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";

$how_do_one["track_earnings"]["title"] = "Track Earnings";
$how_do_one["track_earnings"]["source"] = "https://www.youtube.com/embed/ZazfNLIvpiw";
$how_do_one["track_earnings"]["desc"] = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                        Lorem Ipsum hasbeen theindustry's standard dummy text ever since the 1500s,
                                        when an unknown printer took a galley of type and scrambled it to make a 
                                        type specimen book. It has survived not only five centuries, but also the 
                                        leap into electronic typesetting, remaining essentially unchanged. It was 
                                        popularised in the 1960s with the release of Letraset sheets containing 
                                        Lorem Ipsum passages, and more recently with desktop publishing software 
                                        like Aldus PageMaker including versions of Lorem Ipsum.";

if (array_key_exists($category,$how_do_one))
{
        ?>
        <div class="bg-white border-all clearfix innerB">
            <!-- <div class="row border-bottom"> -->
                <div class="col-xs-12 border-bottom">
                    <h4 class="margin-none innerAll text-blue ">How Does One
                        <span class="text-black-light f-sz14"> <?= $how_do_one["$category"]["title"]; ?></span>
                    </h4>
                </div>
            <!-- </div> -->

            <!-- <div class="row"> -->
                <div class="col-xs-12 padding-none">
                    <div class="img-wrapper innerMT inner-2x">
                       <iframe class="border-none" src="<?= $how_do_one["$category"]["source"]; ?>" width="100%" height="100%"></iframe>
                       <!-- <img src="<?= $how_do_one["$category"]["source"]; ?>" alt=" <?= $how_do_one["$category"]["title"]; ?>" width="100%" height="100%"> -->
                    </div>
                </div>
            <!-- </div> -->
            <!-- <div class="row"> -->
                <div class="col-xs-12">
                    <div class="text-contain text-justify">
                        <p>
                            <?= $how_do_one["$category"]["desc"]; ?>
                        </p>
                    </div>
                </div>
            <!-- </div> -->
        </div>
        <?php

}


?>


