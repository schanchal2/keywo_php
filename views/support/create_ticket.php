<?php
require_once "../../config/config.php";
?>

<div class="clearfix innerMB inner-2x">
    <div class="row card social-card innerB inner-7x">
        <div class="col-xs-12 border-bottom heading">
            <h4 class="margin-none innerAll text-blue ">Create Ticket</h4>
        </div>
        <div class="col-xs-12">
            <div class="text-center innerMT">
                <div class="text-blue">
                    How can we help you today?
                </div>
                <p>
                    Would you wish to contact us for any reason, this is the place to do so.<br>
                    Please submit your details, share your comments with us & we will get back to you as soon as possible.
                    We'd like to hear from you.
                </p>
            </div>
        </div>
        <div class="col-xs-12">
            <ul class="innerL text-blue issue-list">
                <li>
                    KEYWORD MINING
                    <ul class="innerL inner-2x">
                        <li>
                            <span data-toggle="modal" data-target="#supportForm">Error in purchasing keyword</span>
                        </li>
                        <li>
                            <span data-toggle="modal" data-target="#tradingIssueForm">Trading related issue (Bid, Buy, Ask, Sell)</span>
                        </li>
                    </ul>
                </li>
                <li>
                    WALLET (DEPOSIT & WITHDRAWAL)
                    <ul class="innerL inner-2x">
                        <li>
                            <span data-toggle="modal" data-target="#itPurchaseIssueForm">Issue with <?= $keywoDefaultCurrencyName; ?> purchase</span>
                        </li>
                        <li>
                           <span data-toggle="modal" data-target="#issueItCashout">Issue with <?= $keywoDefaultCurrencyName; ?> cashout</span>
                        </li>
                        <li>
                             <span data-toggle="modal" data-target="#issueItSending">Issue with sending <?= $keywoDefaultCurrencyName; ?>
                        </li>
                        <li>
                             <span data-toggle="modal" data-target="#issueItReceiving">Issue with receiving <?= $keywoDefaultCurrencyName; ?>
                        </li>
                    </ul>
                </li>
<!--                <li>-->
<!--                    AD PROGRAM-->
<!--                    <ul class="innerL inner-2x">-->
<!--                        <li>-->
<!--                            Issue with billing and payment-->
<!--                        </li>-->
<!--                        <li>-->
<!--                            Issue with campaign-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </li>-->
                <li>
                    OTHERS
                    <ul class="innerL inner-2x">
                        <li>
                            <span data-toggle="modal" data-target="#generalQuery">General Queries / Feedback
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){


        $('#tradingIssueForm').load("tradingIssueForm.php");
        $('#itPurchaseIssueForm').load("itdPurchaseIssueForm.php");
        $('#issueItCashout').load("issueItdCashout.php");
        $('#issueItSending').load("issueItdSending.php");
        $('#issueItReceiving').load("issueItdReceiving.php");
        $('#generalQuery').load("generalQuery.php");
    });


</script>