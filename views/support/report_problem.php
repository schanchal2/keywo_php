<?php
session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../models/customer/customerModel.php');
require_once('../../models/support/queryListModel.php');
if (!isset($_SESSION["email"]) && empty($_SESSION["email"])) {
    echo "email Not set";
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$dbkeywordDbConn = createDBConnection("dbkeywords");
if(noError($dbkeywordDbConn)){
    $dbkeywordDbConn = $dbkeywordDbConn["connection"];
}
$category = $_GET['category'];
if ($category == 'referal') {
    $category = 'referal program';
} else {
    $category = $category;
}

$getAllFaq = getAllfaqsSupportData('0', '9999999', $dbkeywordDbConn, $category);
if (noError($getAllFaq)){
    $getAllFaq = $getAllFaq['errMsg'];
    ?>
    <div class="bg-white border-all clearfix innerB">
        <div class="col-xs-12 border-bottom heading">
            <h4 class="margin-none innerAll text-blue ">Report a problem / bug</h4>
            <span><?php //echo ucwords($category); ?></span>
        </div>
        <div class="">
            <form name="form_bug_report" id="form_bug_report">
                <div class="col-xs-12">
                    <div class="form-group innerMT">
                        <input type="hidden" id="internalReport" value="internal_report">
                        <label for="inputAmount">Add Description :<i class="fa fa-asterisk fsz-8 text-red    mandatory-field"></i>
                        </label>
                        <textarea class="form-control noresize" maxlength="1000" rows="6" id="bug_desc" name="bug_desc" placeholder="Enter Your Comments Here"></textarea>
                    </div>
                </div>
                <div class="col-xs-12">
                    <span class="">Add screenshots</span>
                </div>
                <div class="col-xs-12 file-in">
                    <div class="form-group file-in__pseudo__form-group">
                        <input class="form-control file-in-input hidden" type="file" name="screenShots[]">
                        <i class="fa fa-close hidden"> </i>
                        <span id="helpBlock1" class="help-block hidden">Only gif,png, jpg, jpeg are supported </span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group ">
                        <span id="bg_announcement_danger" class="text-danger"></span>
                        <span id="bg_announcement_success" class="text-success"></span>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="form-group ">
                        <button type="button" onclick="submitBug()" class="btn btn-primary pull-right" disabled id="btn_submit_bug">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <?php
} else {
    ?>
        <div class="clearfix padding-none faq-wrapper display-block bg-white border-all">
            <!-- <div class="border-all"> -->
            <div class="col-xs-12 border-bottom heading">
                <center>
                    <h4 class="margin-none innerAll text-blue">Something Went Wrong...!</h4></center>
            </div>
        </div>
        <?php
}

?>
        <script type="text/javascript">
        var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);

        var $uploader = $('.file-in__pseudo__form-group').clone();
        $uploader.appendTo('.file-in')
            .removeClass('file-in__pseudo__form-group')
            .find('input').removeClass('hidden').addClass('visible');

        function totalFiles() {
            return $('.file-in .form-group').length - 1;
        }

        function addFileUploade() {
            $('.file-in__pseudo__form-group')
                .clone()
                .appendTo('.file-in')
                .removeClass('file-in__pseudo__form-group')
                .find('input').removeClass('hidden').addClass('visible');
        }
        $('.file-in').on('click', '.fa', function(event) {
            if ($(this).parents(".file-in").find('input.visible').length == 0) {
                addFileUploade();
            }
            $(this).parent(".form-group ").remove();
        });
        $('.file-in').on('change', '.file-in-input', function(event) {
            $total_files = totalFiles();
            if ($total_files <= 3) {
                $(this).parent().removeClass('has-warning').find('.help-block').addClass('hidden')
                var fileName = '';
                fileName = event.target.value.split('\\').pop();
                var extn = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
                if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg") {
                    $(this).addClass('hidden').removeClass('visible').parent().append(fileName);
                    $(this).parent(".form-group").find('.fa').removeClass('hidden');
                    if ($total_files < 3) {
                        addFileUploade();
                    }
                } else {
                    // $(this).parent().append( $("p", {className:"text-danger", innerText: "Only gif,png, jpg, jpeg are supported"}) );
                    // var err=$("p", {className:"text-danger"}) ;
                    // err.appendTo($(this).parent());
                    // $(this).parent().append( );
                    // $(this).remove();
                    $(this).parent().addClass('has-warning').find('.help-block').removeClass('hidden');
                    $(this).val(function() {
                        return this.defaultValue;
                    });
                }
            }
        });

        // $('#bug_desc').bind('input propertychange', function(event) {
        $('#bug_desc').on('input', function(event) {
            event.preventDefault();
            /* Act on the event */
            console.log(this.value.trim().length);
            if (this.value.trim().length > 0) {
                $('#btn_submit_bug').removeProp('disabled');

            } else {
                $('#btn_submit_bug').prop('disabled', 'disabled');

            }

        });

        function submitBug() {
            $("#bg_announcement_success").text('');
            $("#bg_announcement_danger").text('');

            var bug_desc = $("#bug_desc").val().length;
            if (bug_desc < 50) {
                $("#bg_announcement_danger").text("Bug description should be greater than 50 characters.");
                return false;
            }

            if (bug_desc > 650) {
                $("#bg_announcement_danger").text("Bug description should be less than 650 characters.");
                return false;
            }


            var formData = new FormData($("#form_bug_report")[0]);
            formData.append("bug_type", "user_report");
            $.ajax({
                type: 'POST',
                url: rootUrl + 'controllers/support/reportUserBugController.php',
                dataType: 'json',
                data: formData,
                async: true,
                cache: false,
                processData: false,
                contentType: false,
                success: function(data) {
                    if (data["errCode"] == "-1") {
                        $('#form_bug_report').find('textarea, input').val('');
                        $(".file-in .form-group:not('.file-in__pseudo__form-group')").remove();
                        $('.file-in__pseudo__form-group').clone()
                            .appendTo('.file-in')
                            .removeClass('file-in__pseudo__form-group')
                            .find('input').removeClass('hidden').addClass('visible');
                        $("#bg_announcement_success").text("Thank You.Your Bug Details submitted to keywo.");
                    } else {
                        $("#bg_announcement_danger").text('Oops! something went wrong while creating ticket please try again.');
                    }
                }
            });
        }
        </script>