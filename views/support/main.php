﻿<?php

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");

require_once("{$docrootpath}models/customer/customerModel.php");


//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    include("../layout/header.php");
    $email      = $_SESSION["email"];

    $conn = createDBConnection('dbkeywords'); //printArr($conn);

    if(noError($conn)){
        $conn = $conn["connection"];
    }else{
        print_r("Database Error");
    }
    $supportTicket = getLastCount($email, $conn)["errMsg"]["max(id)"];

    // printArr($supportTicket); die();
    ?>
    <link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_support.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="inner-6x innerT" id="currentPageFunction" functionality="">
        <div class="container row-10">
            <div class="row">
                <div class="col-xs-3">
                    <div class="list-group general__sidecol">
                        <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover">FAQs</a>
                        <a class="list-group-item sidemenu-active half innerTB support_side_menu sidemenu-active" id="general_window"><span class="text-blue">General</span></a>
                        <a id="keyword_window" href="#" class="list-group-item half innerTB support_side_menu">Keywords</a>
                        <!--<a id="search_window" href="#" class="list-group-item half innerTB support_side_menu">Search</a>-->
                        <a id="social_window" href="#" class="list-group-item half innerTB support_side_menu">Social</a>
                        <a id="wallet_window" href="#" class="list-group-item half innerTB support_side_menu">Wallet</a>
                        <a id="referal_window" href="#" class="list-group-item half innerTB support_side_menu">Referal Program</a>
                    </div>
                    <div class="list-group general__sidecol">
                        <a class="list-group-item text-black bg-light-gray-color half innerTB nohover">Support Center</a>
                        <a class="list-group-item half innerTB" id="create_ticket_link" onclick="<?php if (empty($supportTicket)) {?>createTicket();<?php }?>" style="<?php if (!empty($supportTicket)) {?>cursor: not-allowed; opacity: 0.6;<?php }?>">Create Ticket </a>
                        <a class="list-group-item half innerTB" id="active_ticket_link" onclick="loadActiveTicket();">Active Ticket</a>
                        <a class="list-group-item half innerTB" id="support_history_link">Support History</a>
                        <a class="list-group-item half innerTB  reportProblem "> Report a problem / bug </a>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div id="main_container">
                        <div class="row card social-card innerMB inner-2x">
                            <div style="margin:100px 50px 100px 50px; ">
                                <center>
                                    <h3>        Loading...        </h3>
                                    <h1><i class="fa fa-spinner fa-spin"></i></h1>
                                </center>
                            </div>
                        </div>
                        <div class="card">
                        </div>
                    </div>
                </div>
                <div id="ticketHistoryScroll" offset="0" data-count="0" ajax-call="true" data-end="false" style = "..."> </div>
                <div id="faqScroll" offset="0" data-count="0" ajax-call="true" data-end="false" data-type = "general" style = "..."></div>
                <div class="col-xs-3">
                    <div class="list-group general__sidecol">
                        <a href="#" class="list-group-item bg-light-gray-color half innerTB nohover no-radius text-white">How does one</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('follow_people')">Follow People</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('follow_keyword')">Follow  Keyword</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('trade_keyword')">Trade  Keyword</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('buy_keyword')">Buy  Keyword</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('send_receive_itd')">Send / Receive <?= $keywoDefaultCurrencyName; ?></a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('cashout_itd')">Cashout <?= $keywoDefaultCurrencyName; ?></a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('post_content')">Post Content</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('share_content')">Share Content</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('refer_friend')">Refer Friend</a>
                        <a href="#" class="list-group-item half innerTB" id="how_to_buy_itd" onclick="loadRightFAQ('track_earnings')">Track Earnings</a>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <div id="supportForm" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" id="modelDialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-title text-blue">
                        <div>
                            Error in purchasing keyword
                        </div>
                    </div>
                </div>
                <div class="row">
                    <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
                </div>
                <form id="resetFormkwd" name="myForm">
                    <div class="modal-body padding-bottom-none">
                        <div class="clearfix innerMB">
                            <div class="pull-left innerMT">
                                <div class="support-form-label">Keyword purchased &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
                            </div>
                            <div class="pull-right">
                                <input type="hidden" class="form-control kwd-purchase-input" id="requestTypePurchasing" name="requestTypePurchasing" value="error in purchasing keyword" />
                            </div>
                            <div class="pull-right">
                                <input type="text" class="form-control kwd-purchase-input" id="keywordName" name="keywordName" />
                            </div>
                        </div>
                        <div class="clearfix innerMB">
                            <div class="pull-left half innerMT">
                                <div class="support-form-label">Payment date &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
                            </div>
                            <div class="pull-right">
                                <input type="text" class="form-control datepicker" id="payment" name="payment" />
                            </div>
                        </div>
                        <div class="clearfix innerMB">
                            <div class="pull-left half innerMT">
                                <div class="support-form-label">Payment mode &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
                            </div>
                            <div class="pull-right">
                                <select name="paymentMode" required id="ddlPassport" onchange="ShowHideDiv()" />
                                <option>Choose</option>
                                <option value="Wallet">Wallet</option>
                                <option value="BitGo">BitGo</option>
                                <option value="Paypal">Paypal</option>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix innerMB">
                            <div class="pull-left half innerMT">
                                <div class="support-form-label">Amount in <?= $keywoDefaultCurrencyName; ?> &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
                            </div>
                            <div class="pull-right">
                                <input type="text" class="form-control checkCrdAmount" id="amountitd" name="amountitd" />
                            </div>

                        </div>
                        <div class="clearfix innerMB" id="dvPassport" style="display: none">
                            <div class="pull-left half innerMT">
                                <div class="support-form-label">Transaction hash of payment &nbsp&nbsp:</i>
                                </div>
                            </div>
                            <div class="pull-right">
                                <input type="text" class="form-control" id="hashPayment" name="hashPayment" />
                            </div>
                        </div>
                        <div class="clearfix innerMB" id="dv2Passport" style="display: none">
                            <div class="half innerMTB">
                                <div class="support-form-label">Paypal address through payment transferred &nbsp&nbsp:</i>
                                </div>
                            </div>
                            <div class="">
                                <input type="text" class="form-control btc-address-input" id="paypal" name="paypal" />
                            </div>
                        </div>
                        <div class="clearfix innerMB" id="dv1Passport" style="display: none">
                            <div class="half innerMTB">
                                <div class="support-form-label">BTC address through payment transferred &nbsp&nbsp:</i>
                                </div>
                            </div>
                            <div class="">
                                <input type="text" class="form-control btc-address-input" id="BTCaddress" name="BTCaddress" />
                            </div>
                        </div>
                        <div class="clearfix innerMB">
                            <div class="half innerMTB">
                                <div class="support-form-label">Comment &nbsp&nbsp: <i class="fa fa-asterisk text-red mandatory-field"></i></div>
                            </div>
                            <div class="">
                                <textarea type="text" class="form-control" rows="3" id="comment" name="comment" placeholder="Enter Your Comments Here, Maximum Limit is 1000" maxlength="1000"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer innerT innerMB">
                        <div class="modal-footer text-center">
                            <button type="reset"  class="btn btn-md btn-primary reserAllSupportForms" id="resetKwdBtn" >Reset</button>
                            <button type="button" class="btn btn-md btn-primary" id="submit" onclick="submitModel()">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="tradingIssueForm" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="itPurchaseIssueForm" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="issueItCashout" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="issueItSending" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="issueItReceiving" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="generalQuery" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <div id="feedback" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    </div>
    <!-- Modal -->
    <div id="ticketCloseModal" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                </div>
                <div class="modal-body">
                    <div class="innerAll">
                        <div class="clearfix">
                            <div class="pull-left support-img innerMR">
                                <img class="img-responsive img-circle" src="<?php echo $rootUrlImages?>customerHelpDesk.png" />
                            </div>
                            <div class="pull-left text-center support-text">
                                We would like to listen you!!!
                            </div>
                        </div>
                        <div class="innerMT text-center">
                            How would you rate our customer service?
                        </div>
                        <div class="innerMTB text-center text-yellow rating">
                            <input id="input-id" name="input-id" type="number" class="rating hidden">
                        </div>
                        <div class="clearfix">
                            <div id="starButton" class="pull-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="tickit-close-confirmation" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <!-- Modal content-->
                <div class="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-blue">Delete</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to Close This Ticket ?
                    </div>
                    <div id="ticktCloseAppendDiv">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php }else{
    header("Location: $rootUrl./views/prelogin/index.php");
} ?>
    <?php include('../layout/transparent_footer.php'); ?>
    <script type="text/javascript">

        $('.checkCrdAmount').keypress(function(eve) {
            if ((eve.which != 46 || $(this).val().indexOf('.') != -1) && (eve.which < 48 || eve.which > 57) || (eve.which == 46 && $(this).caret().start == 0) ) {
                eve.preventDefault();
            }

            // this part is when left part of number is deleted and leaves a . in the leftmost position. For example, 33.25, then 33 is deleted
            $('.checkCrdAmount').keyup(function(eve) {
                if($(this).val().indexOf('.') == 0) {    $(this).val($(this).val().substring(1));
                }
            });
        });

    $('#issueItReceiving,#supportForm,#tradingIssueForm,#itPurchaseIssueForm,#issueItCashout,#issueItSending,#generalQuery').on('show.bs.modal', function(e) {
        $(".modalErrorData").text("");
        resetModal('reserAllSupportForms');
    });

    $('#supportForm').on('hide.bs.modal', function(e) {
        $("#requestTypePurchasing").val('');
        $("#keywordName").val('');
        $("#ddlPassport").val('Choose');
        $("#dvPassport").hide();
        $("#dv2Passport").hide(); 
        $("#dv1Passport").hide();         
        $("#payment").val('');
        $("#amountitd").val('');
        $("#hashPayment").val('');
        $("#BTCaddress").val('');
        $("#paypal").val('');
        $("#comment").val('');
    });
    var $inp = $('#input-id');

    $inp.rating({
        min: 0,
        max: 5,
        step: 1,
        size: 'xs',
        showClear: false,
        'showCaption': false
    });

    $(".support_side_menu").click(function(e) {
        $(".support_side_menu").removeClass('sidemenu-active');
        var selectedId = $(this).attr('id');
        switch (selectedId) {
            case 'general_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','general');
                $("#general_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'general');
                loadGeneralQuestions('general');
                break;
            case 'keyword_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','keywords');
                $("#keyword_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'keywords');
                loadGeneralQuestions('keywords');
                break;
            case 'referal_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','referal');
                $("#referal_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'referal');
                loadGeneralQuestions('referal');
                break;
            case 'search_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','search');
                $("#search_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'search');
                loadGeneralQuestions('search');
                break;
            case 'social_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','social');
                $("#social_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'social');
                loadGeneralQuestions('social');
                break;
            case 'wallet_window':
                faqInitialize();
                $('#faqScroll').attr('data-type','wallet');
                $("#wallet_window").addClass('sidemenu-active');
                $("#currentPageFunction").attr('functionality', 'wallet');
                loadGeneralQuestions('wallet');
                break;
        }
    })

    $('.reportProblem').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $("#currentPageFunction").attr('functionality', 'reportBug');
        $("#main_container").load("report_problem.php");


    });
    function faqInitialize() {
        // offset="0" data-count="0" ajax-call="true" data-end="false" data-type = "general"
        $("#faqScroll").attr('offset', '0');
        $("#faqScroll").attr('data-count', '0');
        $("#faqScroll").attr('ajax-call', 'true');
        $("#faqScroll").attr('data-end', 'false');
    }

    function loadGeneralQuestions(category) {
        var dataCount  = parseInt($("#faqScroll").attr("data-count"));
        var dataOffset = $("#faqScroll").attr("offset");
        var ajaxCall   = $("#faqScroll").attr("ajax-call");
        var dataEnd    = $("#faqScroll").attr("data-end");
        if (ajaxCall   == "true" && dataEnd == "false") {
            $.ajax({
                type: 'GET',
                url: 'general.php',
                dataType: 'HTML',
                data: {
                    category:  category,
                    dataCount: dataCount,
                    offset:    dataOffset
                },
                async: true,
                success: function(data) {
                    if (dataOffset == 0) {
                        $("#main_container").html(data);
                    } else {
                        $("#main_container").append(data);
                    }

                    var NextOffset = $('#faqData-' + dataCount).attr('nextOffset');
                    var dataEmpty = $('#faqData-' + dataCount).attr('dataEmpty');

                    $('#faqScroll').attr('offset', NextOffset);
                    $('#faqScroll').attr('data-end', dataEmpty);
                    $('#faqScroll').attr('data-count', parseInt(dataCount) + 1);
                },
                beforeSend: function(data) {
                    $('#faqScroll').attr('ajax-call', 'false');
                },
                complete: function(data) {
                    $('#faqScroll').attr('ajax-call', 'true');
                },
                error: function(data) {
                    console.log('failed');
                }
            });
        }
        return;
    }



    function closingTicketConfirm(tcktId) {
        var callFunction = 'onclick = "closeTicket(\'' + tcktId + '\')"';
        var htmlCode = '<div class="modal-footer border-none"><button type="button" class="btn btn-social-wid-auto yes-remove-block-btn" data-dismiss="modal" ' + callFunction + '>Yes</button><button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button></div>';
        $('#tickit-close-confirmation #ticktCloseAppendDiv').html('');
        $('#tickit-close-confirmation #ticktCloseAppendDiv').html(htmlCode);
    }

    function blockCreateTicket() {
        $('#create_ticket_link').attr('onclick', '');
        $('#create_ticket_link').css('cursor', 'not-allowed');
        $('#create_ticket_link').css('opacity', '0.6');
        return true;
    }

    function createTicket() {
        $("#main_container").load("create_ticket.php");
        $("#currentPageFunction").attr('functionality', 'createTckt');
    }

    function loadActiveTicket() {
        $("#currentPageFunction").attr('functionality', 'activeTckt');
        $("#main_container").load("active_ticket.php", function(response, status, xhr) {
            if (status == "success") {
                var findingDiv = $('#NoActiveTicketFound').attr('activeValue');
                if (findingDiv == '0') {
                    $('#create_ticket_link').attr('onclick', 'createTicket();');
                    $('#create_ticket_link').css('cursor', 'pointer');
                    $('#create_ticket_link').css('opacity', '');
                }
            }
        });
    }



    function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".gap")
            .toggleClass('fa-plus-circle fa-minus-circle').toggleClass('text-black text-blue').parent().parent().toggleClass('text-black text-blue');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);


    $("#support_history_link").click(function() {
        $("#currentPageFunction").attr('functionality', 'tcktHistory');
        $("#ticketHistoryScroll").attr("data-count", '0');
        $("#ticketHistoryScroll").attr("offset", '0');
        $("#ticketHistoryScroll").attr("ajax-call", 'true');
        $("#ticketHistoryScroll").attr("data-end", 'false');
        loadTicketHistory();
    });

    function loadTicketHistory() {
        var dataCount = parseInt($("#ticketHistoryScroll").attr("data-count"));
        var dataOffset = $("#ticketHistoryScroll").attr("offset");
        var ajaxCall = $("#ticketHistoryScroll").attr("ajax-call");
        var dataEnd = $("#ticketHistoryScroll").attr("data-end");
        if (ajaxCall == "true" && dataEnd == "false") {
            $.ajax({
                type: 'GET',
                url: 'ticket_history.php',
                dataType: 'HTML',
                data: {
                    offset: dataOffset,
                    dataCount: dataCount
                },
                async: true,
                success: function(data) {
                    if (dataOffset == 0) {
                        $("#main_container").html(data);
                    } else {
                        $("#main_container").append(data);
                    }

                    var NextOffset = $('#ticketPagination-' + dataCount).attr('nextOffset');
                    var dataEmpty = $('#ticketPagination-' + dataCount).attr('dataEmpty');

                    $('#ticketHistoryScroll').attr('offset', NextOffset);
                    $('#ticketHistoryScroll').attr('data-end', dataEmpty);
                    $('#ticketHistoryScroll').attr('data-count', parseInt(dataCount) + 1);
                },
                beforeSend: function(data) {
                    $('#ticketHistoryScroll').attr('ajax-call', 'false');
                },
                complete: function(data) {
                    $('#ticketHistoryScroll').attr('ajax-call', 'true');
                },
                error: function(data) {
                    console.log('failed');
                }
            });
        }

    }

    $(document).ready(function() {
        // $("#main_container").load("general.php?category=general");
        loadGeneralQuestions('general');
    });

    $("#how_to_buy_itd").click(function() {
        // $("#main_container").load("how_does_one.php");
    });

    function resetModal(id) {
        //document.getElementById(id).reset();
        $("."+id).trigger('click');

    }

    $('#payment').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#payment').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#payment').on('drop', function(event) {
        event.preventDefault();
    });

    //submitModel
    var rootUrl = '<?php echo $rootUrl; ?>';

    function submitModel() {
        //alert("Inserted");
        var requestType = document.getElementById("requestTypePurchasing").value;
        var keywordName = (document.getElementById("keywordName").value).trim();
        var payment     = (document.getElementById("payment").value).trim();
        var amountITD   = (document.getElementById("amountitd").value).trim();
        var hashPayment = document.getElementById("hashPayment").value;
        var BTCaddress  = document.getElementById("BTCaddress").value;
        var paypal      = document.getElementById("paypal").value;
        var comment     = (document.getElementById("comment").value).trim();

        var transactionType = $("#ddlPassport").val();
        var transactionData = '';



        if (transactionType == 'Wallet' && hashPayment != '') {
            transactionData = 'Wallet';
        } else if (transactionType == 'BitGo' && BTCaddress != '') {
            transactionData = 'BitGo';
        } else if (transactionType == 'Paypal' && paypal != '') {
            transactionData = 'Paypal';
        }


        if (keywordName == "" || payment == "" || amountITD == "" || comment == "" || transactionData == "" ) {
            $(".modalErrorData").html("All fields are Mandatory");
            return false;
        }else {
            $.ajax({
                type: 'POST',
                url: rootUrl + 'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType: requestType,
                    keywordName: keywordName,
                    payment: payment,
                    amountITD: amountITD,
                    hashPayment: hashPayment,
                    BTCaddress: BTCaddress,
                    paypal: paypal,
                    comment: comment

                },
                async: true,
                success: function(data) {

                    if(data["errCode"] == "-1")
                    {

                        resetModal('resetFormkwd');

                        $('.modal-backdrop').hide();
                        $('#supportForm').hide();
                        blockCreateTicket();
                        var msg = data["errMsg"];
                        showToast("success", msg);
                        $("#main_container").load("active_ticket.php");

                    }
                    else
                    {
                        console.log("fail in uploading data");
                    }

                }
            });
        }
    }


    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });

    function ShowHideDiv() {
        var ddlPassport = document.getElementById("ddlPassport");
        var dvPassport = document.getElementById("dvPassport");
        var dv1Passport = document.getElementById("dv1Passport");
        var dv2Passport = document.getElementById("dv2Passport");
        dvPassport.style.display = ddlPassport.value == "Wallet" ? "block" : "none";
        dv1Passport.style.display = ddlPassport.value == "BitGo" ? "block" : "none";
        dv2Passport.style.display = ddlPassport.value == "Paypal" ? "block" : "none";
    }


    // allow till current date selection.
    $(document).ready(function() {
        var today = new Date();
        $('.datepicker').datepicker({
            format: 'mm-dd-yyyy',
            autoclose: true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function(ev) {
            $(this).datepicker('hide');
        });

        $('.datepicker').keyup(function() {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });

    });

    function closeTicket(tcktId) {
        $.ajax({
            type: 'POST',
            url: rootUrl + 'controllers/support/closeActiveTicket.php',
            dataType: 'JSON',
            data: {
                ticketId: tcktId
            },
            async: true,
            success: function(data) {
                if (data.errCode == -1) {
                    $("#main_container").load("ticket_history.php");
                    $('#create_ticket_link').attr('onclick', 'createTicket();');
                    $('#create_ticket_link').css('cursor', 'pointer');
                    $('#create_ticket_link').css('opacity', '');
                    showStarReview(tcktId);
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else {
                    showToast("failed", data.errMsg);
                }
            },
            error: function(data) {
                showToast("failed", 'Error in Connection');
                alert('Failed');
            }
        });
    }

    function showStarReview(tcktId) {
        $('#input-id').rating('reset');
        var starButton = '<div onclick = "setStarReview(\'' + tcktId + '\');" class="btn btn-info btn-xs continue-btn">Continue</div>';
        $('#ticketCloseModal #starButton').html(starButton);
        $('#ticketCloseModal').modal('show');
    }

    function setStarReview(tcktId) {
        var starCount = parseInt($('#input-id').val());
        $.ajax({
            type: 'POST',
            url: rootUrl + 'controllers/support/setCustomerReview.php',
            dataType: 'JSON',
            data: {
                ticketId: tcktId,
                starCount: starCount
            },
            async: true,
            success: function(data) {
                if (data.errCode == -1) {
                    $('#ticketCloseModal').modal('hide');
                    $("#main_container").load("ticket_history.php");
                } else if (data.errCode == 100) {
                    window.location.href = rootUrl;
                } else {
                    showToast("failed", data.errMsg);
                }
            },
            error: function(data) {
                showToast("failed", 'Error in Connection');
                alert('Failed');
            }
        });
    }


    function loadRightFAQ(category) {

        $("#currentPageFunction").attr('functionality', 'faq-' + category);
        $("#main_container").load("how_does_one.php?category=" + category);
    }

    /*
 For endless scrolling
 */
    var bottom = $(document).height() - $(window).height();
    var successflag = 0;
    $(document).scroll(function() {
        var win = $(window);
        // Each time the user scrolls
        win.scroll(function() {
            // End of the document reached?
            if ($(document).height() - win.height() == win.scrollTop()) {
                // Do the stuff
                if (successflag == 0) {
                    //do stuff here
                    successflag = 1;
                    var scrollingType = $('#currentPageFunction').attr('functionality');
                    console.log(scrollingType);                    
                    // if (scrollingType == 'tcktHistory') {
                    //     loadTicketHistory();
                    // } else if (scrollingType == 'keywords') {
                    //     loadGeneralQuestions('keywords');
                    // }

                    switch (scrollingType) {
                        case 'tcktHistory':
                            loadTicketHistory();
                            break;
                        case 'general':
                            loadGeneralQuestions('general');
                            break;
                        case 'keywords':
                            loadGeneralQuestions('keywords');
                            break;
                        case 'referal':
                            loadGeneralQuestions('referal');
                            break;
                        case 'search':
                            loadGeneralQuestions('search');
                            break;
                        case 'social':
                            loadGeneralQuestions('social');
                            break;
                        case 'wallet':
                            loadGeneralQuestions('wallet');
                            break;
                    }


                }
            } else {
                successflag = 0;
            }
        });
    });

    
    
    </script>
