<?php

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");



?>


<div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="model_header">
                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close "></i></button>
                <div class="model-title text-blue">
                    <h4>Issues with <?= $keywoDefaultCurrencyName; ?> Cashout</h4>
                </div>
            </div>

            <div class="row">
                <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
            </div>

            <form id="cashoutResetForm">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="inputKeyword">Keyword<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="hidden" class="form-control" id="requestTypeCashout" value="issues with it cashout">
                                <input type="text" class="form-control" id="keywordCashoutname" placeholder="Enter Keyword Traded">
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="selectTradeType">Cashout Payment Method<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <select class="form-control" required id="cashoutType" onchange="ShowHideDivCashout()">
                                    <option disabled selected value="">Cashout Type</option>
                                    <option value="Paypal">Paypal</option>
                                    <option value="BitGo">BitGo</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="form-group">
                                <label for="inputAmount">Cashout Amount in <?php echo $keywoDefaultCurrencyName; ?><i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="text" class="form-control allownumericwithoutdecimal" id="amount" placeholder="Enter Amount Here">
                            </div>
                        </div>

                        <div class="col-xs-6">

                            <div class="form-group" id="btcaddress" style="display: none">
                                <label for="inputAmount">BitGo Address<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="text" class="form-control" id="cashoutbtcaddress" placeholder="Enter BitGo Address">
                            </div>

                            <div class="form-group" id="paypaladdress" style="display: none">
                                <label for="inputAmount">Paypal Address<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                                <input type="text" class="form-control" id="cashoutpaypaladdress" placeholder="Enter Paypal Address">
                            </div>

                        </div>



                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="commentsCashout">Comments &nbsp;&nbsp;:<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i>
                            </label>
                            <textarea class="form-control textAreaSize" rows="3" id="commentsCashout" rows="5.5" placeholder="Enter Your Comments Here, Maximum Limit is 1000" maxlength="1000"></textarea>
                        </div>
                    </div>
                    <div class="row m-t-10">
                        <div class="col-xs-8">
                            <div class="modal-footer text-center">
                                <button type="reset"  class="btn btn-md btn-primary reserAllSupportForms" id="reset">Reset</button>
                                <button type="button" class="btn btn-md btn-primary" id="submit" onclick="submitModelCashout()">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

<script>

    var rootUrl = '<?php echo $rootUrl; ?>'; //alert(rootUrl);


    function submitModelCashout()
    {
        var requestTypeCashout  = (document.getElementById("requestTypeCashout").value).trim();
        var keywordCashoutname  = (document.getElementById("keywordCashoutname").value).trim();
        var amount              = (document.getElementById("amount").value).trim();
        var btcaddress          = (document.getElementById("cashoutbtcaddress").value).trim();
        var paypaladdress       = (document.getElementById("cashoutpaypaladdress").value).trim();
        var commentsCashout     = (document.getElementById("commentsCashout").value).trim();
        var cashoutType     = (document.getElementById("cashoutType").value).trim();

        if(keywordCashoutname.match(' ')){
            $(".modalErrorData").html("Please Enter Single Keyword!");
            return false;
        }

        if(cashoutType=="Paypal")
        {
            var finalAddress=paypaladdress;
        }else if(cashoutType=="BitGo"){
            var finalAddress=btcaddress;
        }

        if (requestTypeCashout == "" || keywordCashoutname== "" || amount == "" || commentsCashout == "" || cashoutType =="" || finalAddress=="") {
            $(".modalErrorData").html("All fields are Mandatory");
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: rootUrl+'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType : requestTypeCashout,
                    keywordName : keywordCashoutname,
                    amountITD   : amount,
                    BTCaddress  : btcaddress,
                    paypal      : paypaladdress,
                    comment     : commentsCashout

                },
                async: true,
                success: function (data) {

                    $('.modal-backdrop').hide();
                    $('#issueItCashout').hide();
                    blockCreateTicket();

                    var msg=data["errMsg"];
                    showToast("success", msg);

                    $("#main_container").load("active_ticket.php");


                }
            });

        }
    }

    function ShowHideDivCashout() {
//        alert("inserted in function");
        var cashoutType   = document.getElementById("cashoutType");
        var btcaddress    = document.getElementById("btcaddress");
        var paypaladdress = document.getElementById("paypaladdress");
        btcaddress.style.display = cashoutType.value == "BitGo" ? "block" : "none";
        paypaladdress.style.display = cashoutType.value == "Paypal" ? "block" : "none";
    }

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });

</script>