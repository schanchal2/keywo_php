<?php
session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../models/customer/customerModel.php');
require_once('../../models/support/queryListModel.php');
if (!isset($_SESSION["email"]) && empty($_SESSION["email"])) {
    echo "email Not set";
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
$dbkeywordDbConn = createDBConnection("dbkeywords");
if(noError($dbkeywordDbConn)){
    $dbkeywordDbConn = $dbkeywordDbConn["connection"];
}
$category = cleanXSS($_GET['category']);
$dataCount = cleanXSS($_GET['dataCount']);
$offset = cleanXSS($_GET['offset']);
$limit = 21;
$nextOffset = 0;
if ($category == 'referal') {
    $category = 'referal program';
} else {
    $category = $category;
}

$getAllFaq = getAllfaqsSupportData($offset, $limit, $dbkeywordDbConn, $category);

if (count($getAllFaq['errMsg']) < $limit) {
    $dataEmpty = "true";
} else {
    $dataEmpty = "false";
}

if (noError($getAllFaq)){
    $getAllFaq = $getAllFaq['errMsg'];
?>
<div class= "clearfix padding-none faq-wrapper display-block bg-white">
    <!-- <div class="border-all"> -->
    <?php if ($dataCount == 0) {?>
    <div class="col-xs-12 border-bottom heading">
        <h4 class="margin-none innerAll text-blue ">FAQs</h4>
        <span><?php echo ucwords($category); ?></span>
    </div>
    <?php } ?>
    <div class="panel-group" id="accordion" style = " margin-bottom: 0px; ">
    <?php 
        foreach ($getAllFaq as $key => $value) {              
            if ($key < $limit-1) {
                // printArr($key);
                // if (!empty($value['ans'])) {
    ?>
        <div class="panel panel-default innerR panel_margin" id="panel-group">
            <div class="panel-heading">
                <i class="fa fa-plus-circle gap text-black innerMR " aria-hidden="true"></i>
                <h5 class="panel-title">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?php echo $key.$dataCount; ?>">
                        <?php echo $value['ques_title']; ?>
                    </a>
                </h5>
            </div>
            <div id="collapseOne<?php echo $key.$dataCount; ?>" class="panel-collapse collapse innerL inner-2x">
                <div class="panel-body border-none padding-top-none panel_margin">
                    <?php echo $value['ans']; ?>
                    
                </div>
            </div>
        </div>
    <?php
                // }
        $nextOffset = $nextOffset + 1;
            }
        }

    ?>
        
    </div>
    <!-- </div> -->
</div>

<?php
    } else {
?>

<div class="clearfix padding-none faq-wrapper display-block bg-white border-all">
    <!-- <div class="border-all"> -->
    <div class="col-xs-12 border-bottom heading">
        <center><h4 class="margin-none innerAll text-blue ">Something Went Wrong...!</h4></center>
    </div>
</div>

<?php
    }

?>

<?php 
    $lastOffsetValue = $offset + $nextOffset;
?>

<div id = "faqData-<?php echo $dataCount; ?>" nextOffset = "<?php echo $lastOffsetValue; ?>" dataEmpty = "<?php echo $dataEmpty; ?>" ></div>

