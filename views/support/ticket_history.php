<?php
    session_start();

    /* Add Global variables */
    $docrootpath = __DIR__;
    $docrootpath = explode('/views', $docrootpath);
    $docrootpath = $docrootpath[0] . "/";
    /* Add DB Management Files */
    require_once "{$docrootpath}config/config.php";
    require_once "{$docrootpath}config/db_config.php";
    /* Add Model */
    require_once("{$docrootpath}helpers/deviceHelper.php");
    require_once "{$docrootpath}helpers/arrayHelper.php";
    require_once "{$docrootpath}helpers/stringHelper.php";
    require_once "{$docrootpath}helpers/errorMap.php";
    require_once "{$docrootpath}helpers/coreFunctions.php";
    require_once("{$docrootpath}models/support/queryListModel.php");
    if (!isset($_SESSION["email"]) && empty($_SESSION["email"])) {
        echo "email Not set";
        print("<script>");
        print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
        print("</script>");
        die;
    }

    $conn = createDBConnection('dbkeywords');

    if(noError($conn)){
        $conn = $conn["connection"];
    }else{
        print_r("Database Error");
        die();
    }
    $dataCount = cleanXSS($_GET['dataCount']);
    $offsetValue = cleanXSS($_GET['offset']);
    $email = $_SESSION['email'];
    $limit = 21;
    $lastTicket = "";
    $getSupportHistory = getSupportHistory($email, $offsetValue, $limit, $conn);    
    if (count($getSupportHistory['errMsg']) < $limit) {
        $dataEmpty = "true";
    } else {
        $dataEmpty = "false";
    }
    
?>

<div class="clearfix bg-white padding-none ">
<?php if ($offsetValue == 0) { ?>
    <div class="border-bottom ">
        <div class="row">
            <div class="col-xs-12">
                <h4 class="margin-none innerAll text-blue ">Support Center</h4>
            </div></div>
    </div>
<?php }?>
    <!--<div class="row">
        <div class="col-xs-12">
            <h5 class="innerTB half">Support History - Choose an option</h5>
        </div>
    </div>-->
    <?php

        if (noError($getSupportHistory)) {

            $getSupportHistory = $getSupportHistory['errMsg']; 
    ?>
    <div class="row">
        <div class="col-xs-12">
        <?php
            if (count($getSupportHistory) > 0) {
        ?>
            <table class="table table-responsive rating-table margin-none">
            <?php if ($offsetValue == 0) { ?>
                <thead class="text-blue">
                
                <tr>
                    <!--<th>ID</th>-->
                    <th>Ticket Type</th>
                    <th class="text-center">Opened</th>
                    <th class="text-center">Closed</th>
                    <th class="text-center">Rating</th>
                </tr>
                
                </thead>
                <?php }?>
                <tbody>

                <?php
                    foreach ($getSupportHistory as $key => $value) {
                        if ($key < $limit-1) {
                ?>
                <tr>
                    <!--<td><?php //echo ucwords($value['id']); ?></td>-->
                    <td><?php echo ucwords($value['request_type']); ?></td>
                    <td><?php echo date('d/m/y', strtotime($value['requestTime'])); ?></td>
                    <td><?php echo date('d/m/y', strtotime($value['closed_on'])); ?></td>
                    <td class="innerMT text-center text-yellow rating">
                    <?php 
                        for($i = 0; $i < $value['ratings_by_user'] && $i < 5; $i++) { 
                    ?>
                        <i class="fa fa-star"></i>
                    <?php
                        } 

                        for ($j = $i; $j < 5; $j++) {
                    ?>
                        <i class="fa fa-star-o"></i>
                    <?php
                        }
                    ?>                        
                    </td>
                </tr>
                <?php 
                    $lastTicket = $value['id'];     
                        }              
                    } // End of foreach loop
                ?>                
                </tbody>
            </table>
        <?php
            } else {
                echo "<div style = 'margin:20px;'><center>No History Found</center></div>";
            }
        ?>

        </div>
    </div>
    <?php
        } else {
    ?>
    <div>
        <center>Something Went Wrong..!</center>
        <br>
        <center>Please Try Again Later</center>
    </div>
    <?php
        }
    ?>
</div>

<div id = "ticketPagination-<?php echo $dataCount; ?>" nextOffset = "<?php echo $lastTicket; ?>" dataEmpty = "<?php echo $dataEmpty; ?>" ></div>