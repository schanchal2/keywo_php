<?php
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../models/customer/customerModel.php');

$connKeywords = createDBConnection("dbkeywords");
noError($connKeywords) ? $connKeywords = $connKeywords["connection"] : checkMode($connKeywords["errMsg"]);
// printArr("POST");
// printArr($_POST);
// printArr("_GET");
// printArr($_GET);

$commentText = isset($_POST["comments"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["comments"])):"";
$userType    = isset($_POST["userType"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["userType"])):"";
$emailID     = isset($_POST["emailAgent"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["emailAgent"])):"";
$tickectId   = isset($_POST["ticketID"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["ticketID"])):"";
$lastMSGid   = isset($_POST["lastMSGid"])?cleanQueryParameter($connKeywords, cleanXSS($_POST["lastMSGid"])):"";
if (empty($tickectId)) {
  $tickectId = isset($_GET['ticket_id'])?cleanQueryParameter($connKeywords, cleanXSS($_GET['ticket_id'])):"";
}


// echo "<br>LAST MSG: ".$lastMSGid;

if($commentText!="") {
    $result = updateCommentText($commentText, $emailID, $tickectId, $userType, $connKeywords);
}


if(isset($lastMSGid))
{
// echo "entered<br>";
$getComment=getCommentText($tickectId, $connKeywords, $lastMSGid)["errMsg"][0];

// printArr($getComment);

 ?>
<div class="chat-container bg-light-purple" id="userMessage" ticketStatus = "<?php echo isset($result['errCode'])?$result['errCode']:"";?>">
  <div class="innerMLR">
   <?php
    if(!empty($getComment))
    {
    $endmsgId=end($getComment)["id"];
    $dateDisplayFlag = false;

    $i = 1;
        for($k= 0; $k<count($getComment); $k++){


          $currentDateTime = $getComment[$k]['msg_time'];
          $newDateTime = uDateTime('h:i A', $currentDateTime);
          $date2=uDateTime("Y-m-d H:i:s",date("Y-m-d H:i:s"));
          $dateForMonthConv1= uDateTime('d-m-Y',$currentDateTime);
          $dateForYesterday = uDateTime("d-m-Y",date('d-m-Y',strtotime("-1 day")));
          $dateForMonthConv2= uDateTime("d-m-Y",date("d-m-Y"));
          $j = 1;


          if($dateForMonthConv1 == $dateForMonthConv2 ){
            $displayDate = "Today";
            $j+1;
          }elseif($dateForMonthConv1 == $dateForYesterday){
            $displayDate = "Yesterday";
          }else{
              $displayDate = $dateForMonthConv1;
          }

          $dateDis = $dateForMonthConv1;
          // echo "<br>".$k;
          $off     = $k-1;
          if($off  == -1)
          {
              $off=1;
          }


          $prevDate = uDateTime('d-m-Y',$getComment[$off]['msg_time']);


      ?>
    <!-- right chat begins -->
    <?php //if($prevDate != $dateForMonthConv1){ ?>
      <?php 
        if($dateDisplayFlag == false){ 
          $dateDisplayFlag = true;
      ?>
    <div class="text-center" id= "<?php echo $dateForMonthConv1; ?>">
      <div class="chat-date bg-light-purple">
      <?php echo $displayDate; ?>
      </div>
    </div>
    <?php } ?>
    <?php //} ?>

   <?php  if($getComment[$k]['user_type'] == "customer") { ?>
      <div class="innerTB chat-right clearfix">
        <div class="innerTB arrow_box right-arrow bg-white pull-right">
          <div class="chat_msg_div" id="customer_<?php echo $getComment[$k]['id']; ?>">
              <?php echo $getComment[$k]['msg_content'];  ?>
            </div>
          <div class="text-right text-grayscale-80 chat-time">
            <span class="">  <?php echo $newDateTime;  ?></span>
          </div>
        </div>
      </div>
    <?php } ?>


    <!-- right chat box ends -->
    <!-- left chat begins -->
    <?php    if($getComment[$k]['user_type'] == "agent") { ?>
    <div class="innerTB chat-left clearfix">
      <div class="innerTB arrow_box left-arrow chatbox-other-user pull-left">
        <div class="chat_msg_div" id="agent_<?php echo $getComment[$k]['id']; ?>">
             <?php echo $getComment[$k]['msg_content'];  ?>
       </div>
        <div class="text-right text-grayscale-80 chat-time">
          <span class="">  <?php echo $newDateTime;  ?></span>
        </div>
      </div>
    </div>
    <?php }
$i++;
  }
  }
  ?>
  </div>
</div>

    <?php

}

?>
