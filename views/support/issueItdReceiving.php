<?php

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
/* Add DB Management Files */
require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
/* Add Model */
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once("{$docrootpath}models/support/queryListModel.php");

$email = $_SESSION["email"];

?>


<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="model_header">
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close "></i></button>
            <div class="model-title text-blue">
                <h4>Issues with Receiving <?= $keywoDefaultCurrencyName; ?></h4>
            </div>
        </div>

        <div class="row">
            <span class="modalErrorData" style="color:red;margin-left:20px;"></span>
        </div>

        <form id="resetFormReceive">
            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label for="inputKeyword">Registerd Email Id<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="hidden" class="form-control" id="requestTypeReceiving" value="issues with receiving itd">
                            <input type="text" class="form-control" id="regRecEmail" value="<?php echo $email; ?>" disabled/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputKeyword">Amount to received/Amount to receive in <?php echo $keywoDefaultCurrencyName; ?><i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="text" class="form-control allownumericwithoutdecimal" id="amountRec" placeholder="Enter Amount">
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputKeyword">Date of Request<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="text" class="form-control datepickerReceiver" id="recDate" placeholder="Select Date">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="inputAmount">Sender Email address<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i></label>
                            <input type="text" class="form-control" id="senderEmail" placeholder="Enter Sender EmailID">
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <label for="commentsReceiver">Comments &nbsp;&nbsp;:<i class="fa fa-asterisk fsz-8 text-red mandatory-field"></i>
                        </label>
                        <textarea class="form-control textAreaSize" rows="3" id="commentsReceiver" name="commentsReceiver" rows="5.5" placeholder="Enter Your Comments Here, Maximum Limit is 1000" maxlength="1000"></textarea>
                    </div>
                </div>
                <div class="row m-t-10">
                    <div class="col-xs-8">
                        <div class="modal-footer text-center">
                            <button type="reset"  class="btn btn-md btn-primary reserAllSupportForms" id="reset">Reset</button>
                            <button type="button" class="btn btn-md btn-primary" id="submit" onclick="submitModelReceiving()">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<script>

    $('#recDate').keydown(function() {
        //code to not allow any changes to be made to input field
        return false;
    });
    $('#recDate').bind("cut copy paste",function(e) {
        e.preventDefault();
    });

    $('#recDate').on('drop', function(event) {
        event.preventDefault();
    });

    function submitModelReceiving()
    {
        var requestType      = (document.getElementById("requestTypeReceiving").value).trim();
        var amountRec        = (document.getElementById("amountRec").value).trim();
        var recDate          = (document.getElementById("recDate").value).trim();
        var senderEmail      = (document.getElementById("senderEmail").value).trim();
        var commentsReceiver = (document.getElementById("commentsReceiver").value).trim();

        

        if (requestType == "" || amountRec== "" || senderEmail == "" || commentsReceiver == "") {
            $(".modalErrorData").html("All fields are Mandatory");
            return false;
        }

        var atpos = senderEmail.indexOf("@");
        var senderEmail = senderEmail.lastIndexOf(".");

            if (atpos<1 || senderEmail<atpos+2 || senderEmail+2>=senderEmail.length) {
                $(".modalErrorData").html("Not a valid e-mail address");
                return false;


        } else {
            $.ajax({
                type: 'POST',
                url: rootUrl+'controllers/support/addQueryController.php',
                dataType: 'json',
                data: {
                    requestType : requestType,
                    amountITD   : amountRec,
                    payment     : recDate,
                    recEmail    : senderEmail,
                    comment     : commentsReceiver

                },
                async: true,
                success: function (data) {

                    $('.modal-backdrop').hide();
                    $('#issueItReceiving').hide();
                    blockCreateTicket();

                    var msg=data["errMsg"];
                    showToast("success", msg);

                    $("#main_container").load("active_ticket.php");

                }
            });

        }
    }

    $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
        var that = this;

        //paste event
        if (event.type === "paste") {
            setTimeout(function() {
                $(that).val($(that).val().replace(/[^\d].+/, ""));
            }, 100);
        } else {

            if (event.which < 48 || event.which > 57) {
                event.preventDefault();
            } else {
                $(this).val($(this).val().replace(/[^\d].+/, ""));
            }
        }

    });

    $(document).ready(function () {
        var today = new Date();
        $('.datepickerReceiver').datepicker({
            format: 'mm-dd-yyyy',
            autoclose:true,
            endDate: "today",
            maxDate: today
        }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });


        $('.datepickerReceiver').keyup(function () {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9^-]/g, '');
            }
        });
    });

</script>