<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT keyword-markt-popup-common ADP7">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="innerB clearfix">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Manage Creatives</h4>
                            <!-- <input value="Add Balance" type="file" class="btn-trading-wid-auto innerMT pull-right" > -->
                            <div class="file-upload col pull-right innerMR">
                                Upload Files
                                <input type="file" class="hide_file">
                            </div>
                        </div>
                        <!-- profile heading -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="bg-white innerAll">
                            <h4 class="text-color-Text-Primary-Blue margin-none innerTB leadCorrect-b">Image Ads</h4>
                            <div class="row innerAll half">
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                                <div class="col-xs-6 half innerAll"> <img src="https://placeholdit.imgix.net/~text?w=350&h=150" class="img-thumbnail innerAll no-radius"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
