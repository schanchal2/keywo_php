<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
<main class="inner-7x innerT ADP4">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-6">
                <div class="row">
                    <div class="innerB clearfix">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Campaign Analytics</h4>
                        </div>
                        <!-- profile heading -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card bg-White border-all innerAll padding-bottom-none innerMB f-sz15 scoreboard">
                            <div class="filter">
                                <!-- select structure copied from "views/keyword_html/analytics/keyword_analytics.php" -->
                                <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                    <option value="All" selected="">Date</option>
                                    <option value="">option1</option>
                                </select>
                                <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                    <option value="All" selected="">Country</option>
                                    <option value="">option1</option>
                                </select>
                                <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                    <option value="All" selected="">Device</option>
                                    <option value="">option1</option>
                                </select>
                                <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                    <option value="All" selected="">Os</option>
                                    <option value="">option1</option>
                                </select>
                                <select type="text" width="100%" value="" placeholder="" class="half innerAll graph-input-wid">
                                    <option value="All" selected="">Gender</option>
                                    <option value="">option1</option>
                                </select>
                            </div>
                            <div id="graph"></div>
                            <hr class="border-bottom">
                            <div class="scoreboard__heading f-sz16 text-color-Text-Primary-Blue clearfix">
                                <div class="scoreboard__impressions_heading pull-left text-center">IMPRESSIONS </div>
                                <div class="scoreboard__clicks_heading pull-left text-center">CLICKS</div>
                            </div>
                            <div class="scoreboard__subtitle text-color-Text-Primary-Blue f-sz16"> Country</div>
                            <ul class="scoreboard__list   clearfix innerML">
                                <li>
                                    <div class="col scoreboard__key padding-left-none">India </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none">Luxembourg </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none">Singapore </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                            </ul>
                            <div class="scoreboard__subtitle text-color-Text-Primary-Blue f-sz16"> Device</div>
                            <ul class="scoreboard__list   clearfix innerML">
                                <li>
                                    <div class="col scoreboard__key padding-left-none">Samsung Galaxy S7 </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none">LG G5 </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none">OnePlus 3 </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                            </ul>
                            <div class="scoreboard__subtitle text-color-Text-Primary-Blue f-sz16"> Operating System (OS)</div>
                            <ul class="scoreboard__list   clearfix innerML">
                                <li>
                                    <div class="col scoreboard__key padding-left-none"> Microsoft Windows 7 </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none"> Windows 8 </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none"> Ubuntu </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                            </ul>
                            <div class="scoreboard__subtitle text-color-Text-Primary-Blue f-sz16"> Gender</div>
                            <ul class="scoreboard__list   clearfix innerML">
                                <li>
                                    <div class="col scoreboard__key padding-left-none">Male </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                                <li>
                                    <div class="col scoreboard__key padding-left-none">Female </div>
                                    <div class="col scoreboard__impressions text-center">1200 </div>
                                    <div class="col scoreboard__clicks text-center">50</div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="card left-panel-modules">
                    <div class="bg-light-gray left-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4>Active Campaigns</h4>
                            </div>
                        </div>
                    </div>
                    <div class="settings-preference-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name border-bottom">
                                <a href="#"><span class="text-color-Text-Primary-Blue"> All</span> </a>
                            </div>
                            <div class="social-user-setting-name border-bottom">
                                <a href="#"><span class="text-color-Text-Primary-Blue"> Campaign - 1_ </span> #27080</a>
                            </div>
                            <div class="social-user-setting-name border-bottom">
                                <a href="#"><span class="text-color-Text-Primary-Blue"> Campaign - 2 _</span> #25080</a>
                            </div>
                            <div class="social-user-setting-name border-bottom">
                                <a href="#"> <span class="text-color-Text-Primary-Blue">Campaign - 13_ </span>#25090</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<script src="<?php echo $rootUrl; ?>frontend_libraries/raphael/raphael-min.js"></script>
<script src="<?php echo $rootUrl; ?>frontend_libraries/morris/morris.js"></script>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
Morris.Line({
    element: 'graph',
    data: [{
            y: '15',
            a: 10
        }, {
            y: '30',
            a: 20
        }, {
            y: '45',
            a: 30
        }, {
            y: '60',
            a: 40
        }, {
            y: '75',
            a: 50
        }, {
            y: '90',
            a: 20
        }, {
            y: '105',
            a: 40
        }, {
            y: '120',
            a: 30
        }, {
            y: '135',
            a: 40
        }, {
            y: '150',
            a: 50
        }




    ],
    xkey: 'y',
    parseTime: false,
    ykeys: ['a'],
    labels: ['Count', 'Days'],
    lineColors: ['green']
});
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
