<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT keyword-markt-popup-common ADP3">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="innerB clearfix">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Manage Campaign</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table text-center manage_campaign__table border-all table-striped">
                                <thead class="bg-light-gray text-color-White">
                                    <tr>
                                        <th class="f-wt1 text-left">Campaigns</th>
                                        <th class="f-wt1 text-center">Status</th>
                                        <th class="f-wt1 text-right">Budget</th>
                                        <th class="f-wt1 text-right">Impressions <span class="btn-block">(CPM)</span> </th>
                                        <th class="f-wt1 text-right">Clicks <span class="btn-block">(CPC)</span> </th>
                                        <th class="f-wt1 text-center">Analytics </th>
                                        <th class="f-wt1 text-center">Action </th>
                                    </tr>
                                </thead>
                                <tbody class="bg-color-White">
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td><span class="text-color-App-Success">Active </span></td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-link" disabled="">
                                                <i class="f-sz18 fa fa-play-circle text-color-App-Success "></i>
                                            </button>
                                            <button type="button" class="btn btn-link ">
                                                <i class="f-sz18 fa fa-pause-circle text-light-orange"></i>
                                            </button>
                                            <button type="button" class="btn btn-link ">
                                                <i class="f-sz18 fa fa-stop-circle text-red"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td><span class="text-red">Inactive </span></td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="f-sz18 fa fa-play-circle text-color-App-Success "></i>
                                            </button>
                                            <button type="button" class="btn btn-link " disabled="">
                                                <i class="f-sz18 fa fa-pause-circle text-light-orange"></i>
                                            </button>
                                            <button type="button" class="btn btn-link ">
                                                <i class="f-sz18 fa fa-stop-circle text-red"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td><span class="text-red">Draft </span></td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="f-sz18 fa fa-play-circle text-color-App-Success "></i>
                                            </button>
                                            <button type="button" class="btn btn-link ">
                                                <i class="f-sz18 fa fa-pause-circle text-light-orange"></i>
                                            </button>
                                            <button type="button" class="btn btn-link ">
                                                <i class="f-sz18 fa fa-stop-circle text-red"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td><span class="text-red">Incomplet </span></td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-link">
                                                <i class="fa  fa-pencil text-color-grayscale-4"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
