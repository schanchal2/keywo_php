<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT ADP5">
    <div>
        <div class="container row-10 text-color-grayscale-4 padding-none">
            <div class="row">
                <div class="col-xs-3">
                    <?php include("components/left_sidebar.php"); ?>
                </div>
                <div class="col-xs-9">
                    <div class="row">
                        <div class="innerB clearfix">
                            <div class="profile-heading">
                                <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Billing</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table class="table text-center billing__table border-all table-striped">
                                    <thead class="bg-light-gray text-color-White">
                                        <tr>
                                            <th class="f-wt1 text-center">Date</th>
                                            <th class="f-wt1 text-center">Payment mode</th>
                                            <th class="f-wt1 text-center">Amount</th>
                                            <th class="f-wt1 text-center">Invoice</th>
                                        </tr>
                                    </thead>
                                    <tbody class="bg-color-White">
                                        <tr>
                                            <td>
                                                <sapn class="text-color-Text-Primary-Blue">29th Jan, 2015</sapn>
                                            </td>
                                            <td>PayPal</td>
                                            <td>010.05 $</td>
                                            <td>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#invoice">
                                                    Preview
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <sapn class="text-color-Text-Primary-Blue">29th Jan, 2015</sapn>
                                            </td>
                                            <td>PayPal</td>
                                            <td>000.05 $</td>
                                            <td>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#invoice">
                                                    Preview
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <sapn class="text-color-Text-Primary-Blue">29th Jan, 2015</sapn>
                                            </td>
                                            <td>PayPal</td>
                                            <td>150.05 $</td>
                                            <td>
                                                <button type="button" class="btn btn-link" data-toggle="modal" data-target="#invoice">
                                                    Preview
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--=============================
        =            Pop-ups            =
        ==============================-->
        <div class="modal fade madal--1 row-10" id="invoice" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
            <!-- keyword-popup -->
            <div class="modal-dialog billing-invoice--modal-dialog innerMT inner-9x" role="document">
                <div class="modal-content no-radius ">
                    <div class="modal-header border-none padding-bottom-none">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                       <!--  <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="">Select Post From your Timeline</h4> -->
                    </div>
                    <div class="modal-body innerAll">
                        <div class="billing-invoice--modal-body ">
                            <div class="billing-invoice__head innerB">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <img class="billing-invoice__logo innerB" src="<?php echo $rootUrl; ?>images/keywo_logo_blue.png">
                                        <div class="text-color-Text-Primary-Blue">Searchtrade.com Pte. Ltd.</div>
                                        <address>
                                            13-00 Far East Finance Building
                                            <br> Robinson Road,
                                            <br> Singapore 048545
                                        </address>
                                        <span class="text-color-Text-Primary-Blue">Email Id:</span><span>asdf@sdg.com</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="billing-invoice__transaction-details clearfix border-top border-bottom  border-color-Text-Primary-Blue "> 
                               
                                    <div class="col-xs-12">
                                        <div class="billing-invoice__subhead">
                                            <div class="row">
                                                <div class="col-xs-4 innerT">
                                                    <div class="billing-invoice__recievers-details">
                                                        <div class="billing-invoice__recievers__name">LOREM LIPSUM</div>
                                                        <div class="billing-invoice__recievers__email">lorem@gmail.com</div>
                                                        <div class="billing-invoice__recievers__account-handel-name">@lorem</div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-offset-6 col-xs-2 padding-none ">
                                                    <div class="billing-invoice__datebox text-center bg-color-Light-Blue text-color-White innerAll ">
                                                        <span class="btn-block f-sz15">Invoice Date</span>
                                                        <sapn class="f-sz14">Jan 08, 2005</sapn>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="billing-invoice__transaction-details__table-head text-color-Text-Primary-Blue">
                                            <div class="row ">
                                                <div class="col-xs-4 ">Description
                                                </div>
                                                <div class="col-xs-2 col-xs-offset-2 text-right"><?php echo $keywoDefaultCurrencyName; ?>
                                                </div>
                                                <div class="col-xs-2 text-right">BTC
                                                </div>
                                                <div class="col-xs-2 text-right">USD
                                                </div>
                                            </div>
                                        </div>
                                        <div class="billing-invoice__transaction-details__table-body border-top innerT half">
                                            <div class="row">
                                                <div class="col-xs-4 text-color-Text-Primary-Blue">You have purchased 100 <?php echo $keywoDefaultCurrencyName; ?> by paypal
                                                </div>
                                                <div class="col-xs-2 col-xs-offset-2 text-right">100
                                                </div>
                                                <div class="col-xs-2 text-right">0.400
                                                </div>
                                                <div class="col-xs-2 text-right">100
                                                </div>
                                            </div>
                                        </div>
                                        <div class="billing-invoice__transaction-details__table-footer border-top innerT half">
                                            <div class="row">
                                                <div class="col-xs-10 text-color-Text-Primary-Blue text-right">Total :
                                                </div>
                                                <div class="col-xs-2 text-right">100
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <h4 class="innerLR text-color-Text-Primary-Blue">Thank you !</h4>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer innerAll border-none padding-top-none text-center">
                        <div class="text-center">
                            <input value="Print" id="print-billing-invoice--modal-body" type="button" class="btn-trading-wid-auto innerMTB innerMR">
                            <input value="Download" type="button" class="btn-trading-wid-auto innerMTB innerMR btn-trading-dark">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====  End of Pop-ups  ====-->
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
$(function() {

    // $('#print-billing-invoice--modal-body').on('click', function(event) {
    //     event.preventDefault();
    //     /* Act on the event */
    //     $('.billing-invoice--modal-body').print();
    //     // window.print();
    // });
    // 
    // 
    // 
    // 
    //////
    //  //
    //////
    $('#print-billing-invoice--modal-body').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        printDiv();

    });

    function printDiv() {

        var divToPrint = $('.billing-invoice--modal-body');
        console.log(divToPrint);

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint[0].innerHTML + '</body></html>');

        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }




});
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
