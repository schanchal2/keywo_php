<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerTB ADP1">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-9">
                <div class="row banner">
                    <div class="col-xs-12">
                        <div id="carousel-id" class="carousel bg-color-White border-all slide " data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <div class="col-xs-7">
                                        <h4 class="f-sz16 text-color-Username-Link">Finibus Bonorum et Malorum</h4>
                                        <p class="f-sz15">Lorem ipsum dolor sit amet, consectetur adipiscing elilIt, sed eiusmod tempor incididunt ut labore dolore magna aliqua. Ut ad minim veniam, qus nostrud exercitation ullamco labors nisi quip ex ea commodo consequat.
                                            <br>
                                            <br> Excepteur sint occaecat cupidatat non proident, sunt in culpa ad minim veniam, qus nostrud exercitation.
                                        </p>
                                    </div>
                                    <div class="col-xs-5 padding-right-none">
                                        <img class="img-responsive innerT" src="https://placeholdit.imgix.net/~text?w=262&h=158">
                                    </div>
                                </div>
                                <div class="item ">
                                    <div class="col-xs-7">
                                        <h4 class="f-sz16 text-color-Username-Link">Finibus Bonorum et Malorum</h4>
                                        <p class="f-sz15">Lorem ipsum dolor sit amet, consectetur adipiscing elilIt, sed eiusmod tempor incididunt ut labore dolore magna aliqua. Ut ad minim veniam, qus nostrud exercitation ullamco labors nisi quip ex ea commodo consequat.
                                            <br>
                                            <br> Excepteur sint occaecat cupidatat non proident, sunt in culpa ad minim veniam, qus nostrud exercitation.
                                        </p>
                                    </div>
                                    <div class="col-xs-5 padding-right-none">
                                        <img class="img-responsive innerT" src="https://placeholdit.imgix.net/~text?w=262&h=158">
                                    </div>
                                </div>
                            </div>
                            <a class="left carousel-control  opacity-10  text-grayscale-80  text-left" href="#carousel-id" data-slide="prev">
                                <i class="fa position-absolute fa-chevron-left f-sz18"></i>
                            </a>
                            <a class="right carousel-control opacity-10  text-grayscale-80  text-right" href="#carousel-id" data-slide="next">
                                <i class="fa position-absolute fa-chevron-right f-sz18"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr class="margin-bottom-none border-color-grayscale-be">
                <div class="row ad_option">
                    <div class="col-xs-12">
                        <h4 class="f-sz18 text-color-Username-Link innerL l-h10 innerT">AD OPTION</h4></div>
                    <div class="col-xs-4 socail">
                        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15">
                            <div class="panel-heading no-radius text-center text-color-Username-Link f-sz16 border-bottom l-h10 ">
                                <p class="leadCorrect-b"> Social</p>
                            </div>
                            <div class="panel-body innerAll">
                                <ul class="list-unstyled leadCorrect-t leadCorrect-b">
                                    <li class="">Text Banner Left</li>
                                    <li class="">Fedd Image Banner</li>
                                    <li class="">Prommoted Post</li>
                                    <li class="">Image Banner Left</li>
                                </ul>
                            </div>
                            <div class="panel-footer no-radius bg-White clearfix innerAll l-h10 border-none">
                                <a href="#" class="leadCorrect-b pull-left" data-toggle="modal" data-target="#knowMore" data-whatever="Social"> Know more </a>
                                <a href="#" class="leadCorrect-b pull-right"> Create </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 socail">
                        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15">
                            <div class="panel-heading no-radius text-center text-color-Username-Link f-sz16 border-bottom l-h10 ">
                                <p class="leadCorrect-b"> Search </p>
                            </div>
                            <div class="panel-body innerAll">
                                <ul class="list-unstyled leadCorrect-t leadCorrect-b">
                                    <li class="">Text Banner </li>
                                    <li class="">Image Banner</li>
                                    <li class="">Search Result Top</li>
                                    <li class="">&nbsp;</li>
                                </ul>
                            </div>
                            <div class="panel-footer no-radius bg-White clearfix innerAll l-h10 border-none">
                                <a href="#" class="leadCorrect-b pull-left" data-toggle="modal" data-target="#knowMore" data-whatever="Search"> Know more </a>
                                <a href="#" class="leadCorrect-b pull-right"> Create </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 socail">
                        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15">
                            <div class="panel-heading no-radius text-center text-color-Username-Link f-sz16 border-bottom l-h10 ">
                                <p class="leadCorrect-b"> Widgets </p>
                            </div>
                            <div class="panel-body innerAll">
                                <ul class="list-unstyled leadCorrect-t leadCorrect-b">
                                    <li class="">Logo Sponsership</li>
                                    <li class="">&nbsp;</li>
                                    <li class="">&nbsp;</li>
                                    <li class="">&nbsp;</li>
                                </ul>
                            </div>
                            <div class="panel-footer no-radius bg-White clearfix innerAll l-h10 border-none">
                                <a href="#" class="leadCorrect-b pull-left" data-toggle="modal" data-target="#knowMore" data-whatever="Widgets"> Know more </a>
                                <a href="#" class="leadCorrect-b pull-right"> Create </a>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="margin-bottom-none border-color-grayscale-be">
                <div class="row stats">
                    <div class="col-xs-12">
                        <h4 class="f-sz18 text-color-Username-Link innerL l-h10 innerT">STATS</h4></div>
                    <div class="col-xs-3">
                        <!--==================================================================================================
                    =            block picked from localhost/keywo_php/views/keyword_html/marketplace/            =
                    /**
                     *  Modifications Made:
                     *  font size helper classes added
                     *
                     */
                    ===================================================================================================-->
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key-dark f-sz24">48522</div>
                            <div class="keyword-num-status light-blue-col   f-sz14">Total Registered Users</div>
                        </div>
                        <!--====  End of block picked from localhost/keywo_php/views/keyword_html/marketplace/  ====-->
                    </div>
                    <div class="col-xs-3">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key f-sz24">48522</div>
                            <div class="keyword-num-status f-sz14">Interaction Delivered</div>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key-dark f-sz24">48522</div>
                            <div class="keyword-num-status light-blue-col f-sz14">Impressions Delivered</div>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="keyword-info">
                            <div class="keyword-info-num bg-blue-key f-sz24">48522</div>
                            <div class="keyword-num-status f-sz14">Clicks Delivered</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade madal--1" id="knowMore" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content no-radius ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="knowMoreLabel">New message</h4>
                </div>
                <div class="modal-body">
                    <div class="embed-responsive embed-responsive-16by9">
                        <!-- <iframe class="embed-responsive-item" src="..."></iframe> -->
                        <iframe width="" height="204" src="https://player.vimeo.com/video/20602479" frameborder="0" allowfullscreen="" class="embed-responsive-item"></iframe>
                    </div>
                    <p class="margin-none innerT">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
$('.carousel').carousel();

$('#knowMore').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var recipient = button.data('whatever');
    var modal = $(this);
    modal.find('.modal-title').text(recipient);
})
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
<!--===========================
=            Notes            =
============================-->
<!--/**
    TODO:
    - $(".keyword-num-status") text transform set to capitalize
 */
-->
<!--====  End of Notes  ====-->
<?php 
/**
 * 19-4-17
 *     - removed class ".keyword-markt-popup-common" from main tag 
 *     - removed class ".keyword-popup" from .modal
 *     - removed class ".custom-modal-header" from .modal-header 
 * 20-4-17
 *     -
 *
 *
 * 
 */


 ?>
