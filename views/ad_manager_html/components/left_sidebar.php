<!--==========================================
=            took from Earning UI            =
/**
 *
 * Modifications Made:
 * added $("input.btn-trading-wid-auto")
 * $(".my-info-card").removeClass("social-card card").addClass("bg-white")
 *
 */
===========================================-->
<div class="myearnings-main-container">
    <div class="bg-white my-info-card">
        <div class="clearfix innerAll border-all">
            <div class="my-info-img pull-left innerMR">
                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
            </div>
            <!-- my-picture  -->
            <div class="my-info-detail pull-left">
                <div class="leadCorrect-t user-name">Vishal Gupta</div>
                <div class="inner-2x innerT user-earnings-text">Ad Account Balance : </div>
                <h4 class="leadCorrect-b margin-none text-blue">2343.43 IT$</h4>
                <input value="Add Balance" type="button" class="btn-trading-wid-auto innerMT pull-right" data-toggle="modal" data-target="#keyword-popup-set-ask">
            </div>
        </div>
        <!-- my-info-detail  -->
    </div>
</div>
<!--====  End of took from Earning UI  ====-->
<!--=================================================
=            Block picked from social UI            =
/**
 *
 * Modifications Made:
 *
 */
==================================================-->
<div class="card left-panel-modules inner-2x innerMT ">
    <div class="bg-light-gray left-panel-modules-head">
        <div class="row margin-none">
            <div class="col-xs-12">
                <h4>Ad Manager</h4>
            </div>
        </div>
    </div>
    <div class="settings-preference-module">
        <div class="margin-none">
            <div class="social-user-setting-name border-bottom">
                <a href="Create_Edit_Campaign_Page/Create_Edit_Campaign_Page.php"> Create Comapaing </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Manage_Campaign_Page.php"> Manage Comapaing </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Archived.php"> Archived </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Campaign_Analytics_Page.php"> Anylytics </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Manage_Creatives.php"> Manage Creatives </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Billing_Page.php"> Billings </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="FAQs.php"> FAQs </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a href="Terms_of_Use.php"> Terms of Use </a>
            </div>
        </div>
    </div>
</div>
<!--====  End of Block picked from social UI  ====-->
