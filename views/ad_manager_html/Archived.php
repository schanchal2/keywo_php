<?php 
include("../layout/header.php");
?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT ADP6">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="innerB clearfix">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Archived</h4>
                        </div>
                        <!-- profile heading -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table text-center archived__table border-all table-striped">
                                <thead class="bg-light-gray text-color-White">
                                    <tr>
                                        <th class="f-wt1 text-left">Campaigns</th>
                                        <th class="f-wt1 text-center">Status</th>
                                        <th class="f-wt1 text-right">Budget</th>
                                        <th class="f-wt1 text-right">Impressions<span class="btn-block">(CPM)</span></th>
                                        <th class="f-wt1 text-right">Clicks<span class="btn-block">(CPC)</span></th>
                                        <th class="f-wt1 text-center">Analytics</th>
                                        <th class="f-wt1 text-center">Duplicate</th>
                                    </tr>
                                </thead>
                                <tbody class="bg-color-White">
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td>Archived </td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none text-color-App-Success" data-toggle="modal" data-target="#duplicate-popup">
                                                <i class="fa fa-file"></i><i class="fa fa-file"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td>Archived </td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none text-color-App-Success" data-toggle="modal" data-target="#duplicate-popup">
                                                <i class="fa fa-file"></i><i class="fa fa-file"></i>
                                            </button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">
                                            <sapn class="text-color-Text-Primary-Blue">Campaign - 1</sapn><span class="btn-block text-left text-color-grayscale-6"> #25085</span>
                                        </td>
                                        <td>Archived </td>
                                        <td class="text-right">0.05 $</td>
                                        <td class="text-right">54000</td>
                                        <td class="text-right">1029</td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-line-chart text-color-Text-Primary-Blue">   </i>
                                            </button>
                                        </td>
                                        <td>
                                            <button type="button" class="btn  btn-link outline-none text-color-App-Success" data-toggle="modal" data-target="#duplicate-popup">
                                                <i class="fa fa-file"></i><i class="fa fa-file"></i>
                                            </button>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<div class="modal fade madal--1" id="duplicate-popup" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
    <div class="modal-dialog innerMT inner-9x" role="document">
        <div class="modal-content no-radius ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="knowMoreLabel">Duplicate Campaign</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body innerAll half">
                    <div class="pramote_post--modal-body">
                        <div class="panel-body innerAll padding-bottom-none half">
                            <div class="margin-bottom-none filter clearfix margin-none">
                                <label for="inputPassword3" class="col-sm-4 margin-none padding-left-none l-h10 innerT text-left text-grayscale-6 f-wt1">New Campaign Name : </label>
                                <div class="col-sm-8  padding-none">
                                    <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" placeholder="">
                                </div>
                            </div>
                            <p class="innerT half innerMTB">Create Duplicate copy of<span class="text-color-Username-Link"> Campaign - 1 </span>details to create new campaign ? </p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer innerAll border-none padding-top-none">
                    <div class="text-center">
                        <input value="Continue" type="button" class="btn-trading-wid-auto innerMTB innerMR" data-dismiss="modal">
                        <input value="Cancel" type="button" class="btn-trading-dark btn-trading-wid-auto innerMTB" data-dismiss="modal">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--====  End of Pop-ups  ====-->
<?php 
include("../layout/transparent_footer.php");
?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
<!-- /** 
 
     TODO:
     - 1. Outline of button can be hidden
     - 
 
  */-->
