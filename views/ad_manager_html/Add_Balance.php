<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT ADP11">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-9">
                <div class="row">
                    <div class="innerB clearfix">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20 col">Add Balance </h4>
                        </div>
                        <!-- profile heading -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="card bg-White border-all   padding-bottom-none innerMB f-sz15">
                            <div class="row">
                                <div class="col-xs-12 innerMTB inner-2x">
                                    <form class="form-horizontal">
                                        <!-- 
                                     -->
                                        <div class="form-group margin-bottom-none filter clearfix margin-none">
                                            <label for="inputPassword3" class="col-sm-2 margin-none l-h10 innerT text-left text-grayscale-6 f-wt1">Enter Amount : </label>
                                            <div class="col-sm-4  padding-left-none">
                                                <div class="input-group text-grayscale-4">
                                                    <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="" placeholder="">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"><?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4  padding-right-none">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="" type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6">USD</span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 text-right keyword-checkout">
                                                <input value="Proceed" type="button" class="btn-pay-now f-sz18 l-h12 margin-none">
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 

                         -->
                    <div class="col-xs-12">
                        <div class="bg-color-Text-Primary-Blue text-white innerMT">
                            <div class="row innerAll">
                                <div class="col-md-7">
                                    <div class="current-w1">
                                        <h5 class="f-sz16">Current Wallet Balance is : 300 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your payment amount : 400 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="text-right inner-2x innerMT keyword-checkout">
                                        <input value="Pay Now" type="button" class="btn-pay-now f-sz16 l-h12 margin-none">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <p class="f-sz16 text-dark-red l-h10 innerTB leadCorrect-b">Condition : If Wallet balance is greater than payment ammount</p>
                    </div>
                    <div class="col-xs-12">
                        <!--============================================
                        =            picked from wallet UI ( that also picked from somewhere)            =
                        /**
                         *
                         * Modifications Made: 
                         * "Purchase ITD with"  section upgraded as it's appearance has been changed (bug) in Wallet UI after Testing. 
                         * $("current-wallet-balance.innerMT.inner-2x").addClass("bg-color-Text-Primary-Blue text-white").removeClass("current-wallet-balance inner-2x") [so now no ned to append "app_wallet.css"]
                         */
                        =============================================-->
                        <div class="bg-color-Text-Primary-Blue text-white innerMT">
                            <div class="row innerAll">
                                <div class="col-md-7">
                                    <div class="current-w1">
                                        <h5 class="f-sz16">Current Wallet Balance is : 300 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your payment amount : 400 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Please Purchase 100 <?php echo $keywoDefaultCurrencyName; ?> to settle payment now</h5>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <h5 class="f-sz16">Purchase <?php echo $keywoDefaultCurrencyName; ?> with<i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="radio radio-primary radio-div margin-top-none">
                                                <input type="radio" name="gender" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label for="Bitcoin" class="f-sz16">
                                                    Bitcoin
                                                </label>
                                            </div>
                                            <span class="pull-right f-sz16">0.45682021 BTC</span>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="radio radio-primary radio-div margin-top-none">
                                                <input type="radio" name="gender" id="PayPal" value="PayPal">
                                                <label for="PayPal" class="f-sz16">
                                                    PayPal
                                                </label>
                                            </div>
                                            <span class="pull-right f-sz16">100.00 USD</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center innerMB keyword-checkout">
                                    <input value="Proceed to Purchase" type="button" class="btn-pay-now f-sz16 l-h12">
                                </div>
                            </div>
                        </div>
                        <!--====  End of picked from wallet UI ( that also picked frome somewhwre)  ====-->
                    </div>
                    <div class="col-xs-12">
                        <p class="f-sz16 text-dark-red l-h10 innerTB leadCorrect-b">Condition : If Wallet balance is lesser than payment ammount</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
$('.filter input').on('focus', function(event) {
    event.preventDefault();
    $(this).parent().addClass('input-is-focused');
});
$('.filter input').on('focusout', function(event) {
    event.preventDefault();
    $(this).parent().removeClass('input-is-focused');
});
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
