<?php include("../../layout/header.php");?>
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<main class="inner-7x innerT ADP2">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <div class="col-xs-3">
                <?php include("../components/left_sidebar.php"); ?>
            </div>
            <div class="col-xs-6">
                <?php 
                include("components/create_campaigns.php" );
                 ?>
                <div id="select_campaign_type">
                    <?php 
                // include("components/select_campaign_type.php");
                ?>
                </div>
                <div id="choose_ad_placement">
                    <?php 
                // include("components/choose_ad_placement.php");
                 ?>
                </div>
                <div id="campaign_targeting">
                    <?php 
                // include("components/campaign_targeting.php");
                 ?>
                </div>
                <div id="create_ad">
                    <?php 
                // include("components/create_ad.php"); 
                 ?>
                </div>
                <div id="ad_budget">
                    <?php 
                // include("components/ad_budget.php");
                 ?>
                </div>
                <div id="setup_campaign_schedule">
                    <?php 
                // include("components/setup_campaign_schedule.php");
                 ?>
                </div>
            </div>
            <div class="col-xs-3">
                <?php include("components/right_sidebar.php"); ?>
            </div>
        </div>
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script> -->
<!-- <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script> -->
<script type="text/javascript">
/*=====================================================
=            For presentationdemonstration purpose only            =
=====================================================*/
jQuery(document).ready(function($) {
    /*----------------------------------------------------------------*/
    /**
     *
     * highlights border of selected input
     *
     */
    $('.input-group input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.input-group input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    /*----------------------------------------------------------------*/
    $('#submit-campaigns_name').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        $.ajax({
            type: 'POST',
            url: 'components/select_campaign_type.php',
            dataType: 'html',
            success: function(data) {
                // console.log(data);
                $('#select_campaign_type').html(data);
                $('#submit-select_campaign_type').on('click', function(event) {
                    event.preventDefault();
                    // console.log($('input[name="options"]:checked').val());
                    /* Act on the event */
                    if ($('#select_campaign_type input[name="options"]:checked').length != 0) {
                        $.ajax({
                            type: 'POST',
                            url: 'components/choose_ad_placement.php',
                            dataType: 'html',
                            data: {
                                'campaign_type': $('input[name="options"]:checked').val(),
                            },
                            success: function(data) {
                                // console.log(data);
                                $('#choose_ad_placement').html(data);
                                $('#submit-choose_ad_placement').on('click', function(event) {
                                    event.preventDefault();
                                    /* Act on the event */
                                    $.ajax({
                                        type: 'POST',
                                        url: 'components/campaign_targeting.php',
                                        dataType: 'html',
                                        success: function(data) {
                                            // console.log(data);
                                            $('#campaign_targeting').html(data);
                                            $('#submit-campaign_targeting').on('click', function(event) {
                                                event.preventDefault();
                                                /* Act on the event */
                                                $.ajax({
                                                    type: 'POST',
                                                    url: 'components/create_ad.php',
                                                    dataType: 'html',
                                                    success: function(data) {
                                                        // console.log(data);
                                                        $('#create_ad').html(data);
                                                        $('#submit-create_ad').on('click', function(event) {
                                                            event.preventDefault();
                                                            /* Act on the event */
                                                            $.ajax({
                                                                type: 'POST',
                                                                url: 'components/ad_budget.php',
                                                                dataType: 'html',
                                                                success: function(data) {
                                                                    // console.log(data);
                                                                    $('#ad_budget').html(data);
                                                                    $('#submit-ad_budget').on('click', function(event) {
                                                                        event.preventDefault();
                                                                        /* Act on the event */
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: 'components/setup_campaign_schedule.php',
                                                                            dataType: 'html',
                                                                            success: function(data) {
                                                                                // console.log(data);
                                                                                $('<div id="setup_campaign_s"></div>chedule').html(data);
                                                                                // $('#submit-setup_campaign_schedule').on('click', function(event) {
                                                                                //     event.preventDefault();
                                                                                //     /* Act on the event */

                                                                                // });
                                                                            }
                                                                        });
                                                                    });
                                                                }
                                                            });
                                                        });
                                                    }
                                                });
                                            });
                                        }
                                    });
                                });
                            }
                        });
                    }
                });
            }
        });
    });
});

/*=====  End of For demonstration purpose only  ======*/
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
