<!--=============================================
=            include : create_ad.php            =
==============================================-->
<?php 
$adCotentType="image" ; 

switch ($adCotentType) {
    case 'image':
    $panelHeading    = "Ad";
        break;
    
    case 'text':
    $panelHeading    = "Text / Banner";
        break;

    default:
    $panelHeading    = "Ad";
        break;
}
 ?>
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15 create_ad-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Create
                    <?php echo $panelHeading  ?>
                </p>
            </div>
            <form onSubmit="return false;" method="POST" class="form-horizontal  " role="form">
                <div class="panel-body innerAll">
                    <p class="f-sz14"><span class="text-color-Username-Link">Ad Format :</span> Choose the Images, Blog and video you'd like to use in your ads</p>
                    <!-- 
                    -->
                    <?php if ($adCotentType=="image"): ?>
                    <h5>Recommended</h5>
                    <div class="row">
                        <div class="col-xs-6 text-color-grayscale-6">
                            <i class="fa fa-circle f-sz10 text-color-Username-Link"></i> Image size : 1200 x 628 pixels
                        </div>
                        <div class="col-xs-6 text-color-grayscale-6">
                            <i class="fa fa-circle f-sz10 text-color-Username-Link"></i> Video Format : .MOV or .MP4 files
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 text-color-grayscale-6">
                            <i class="fa fa-circle f-sz10 text-color-Username-Link"></i> Image ratio : 1.91:1
                        </div>
                        <div class="col-xs-6 text-color-grayscale-6">
                            <i class="fa fa-circle f-sz10 text-color-Username-Link"></i> 60 minutes max (2.3 GB)
                        </div>
                    </div>
                    <div class="row innerT clearfix">
                        <label for="" class="col-sm-4 control-label  f-wt1  text-right">Select Banner :</label>
                        <div class="col-sm-8">
                            <div class="border-all clearfix border-color-Username-Link ">
                                <input type="file" name="" id="file-1" class="inputfile" data-multiple-caption="{count} files selected" multiple="">
                                <label for="file-1" class="pull-right bg-color-Username-Link text-color-White margin-none halfwidth-100-percent innerLR">
                                    <span class="pull-left bg-color-White text-color text-color-grayscale-4 inputfile_fileName ellipses-market-trade-span "> &nbsp;</span> <i class="fa fa-folder-open innerL innerAll half l-h14"></i> <span class="pull-right innerLR innerAll half"> Browse</span></label>
                            </div>
                        </div>
                    </div>
                    <div class="row innerT clearfix">
                        <div class="col-sm-8 col-sm-offset-4">
                            <a class="btn create_ad-upload_creatives-button text-color-White no-radius padding-none btn-block l-h20" data-toggle="modal" href="#upload_creatives"> <i class="fa fa-plus fa-fw bg-color-White innerAll half pull-left l-h14"> </i> Upload Creatives</a>
                            <div class="row innerT">
                                <div class="col-xs-4">
                                    <div class="inputfile-preview border-all innerAll half">
                                        <img src="https://placeholdit.imgix.net/~text?w=208&h=109" class="boxshadow img-responsive"><i class="fa fa-close text-color-White bg-color-Username-Link f-sz10 "></i>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="inputfile-preview border-all innerAll half">
                                        <img src="https://placeholdit.imgix.net/~text?w=208&h=109" class="boxshadow img-responsive"><i class="fa fa-close text-color-White bg-color-Username-Link f-sz10 "></i>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="inputfile-preview border-all innerAll half">
                                        <img src="https://placeholdit.imgix.net/~text?w=208&h=109" class="boxshadow img-responsive"><i class="fa fa-close text-color-White bg-color-Username-Link f-sz10 "></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- 
                     -->
                    <?php elseif($adCotentType=="text"): ?>
                    <div class="form-group margin-bottom-none filter clearfix margin-none">
                        <label for="inputPassword3" class="col-sm-4 margin-none padding-left-none l-h10 innerT control-label text-grayscale-6 f-wt1">Title :</label>
                        <div class="col-sm-8  padding-none">
                            <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" placeholder="Example Pizza - Great Deals">
                        </div>
                    </div>
                    <div class="form-group margin-bottom-none filter clearfix margin-none">
                        <label for="inputPassword3" class="col-sm-4 margin-none padding-left-none l-h10 innerT control-label text-grayscale-6 f-wt1">URL : </label>
                        <div class="col-sm-8  padding-none">
                            <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" placeholder="www.example.com/deals">
                        </div>
                    </div>
                    <div class="form-group margin-bottom-none filter clearfix margin-none">
                        <label for="inputPassword3" class="col-sm-4 margin-none padding-left-none l-h10 innerT control-label text-grayscale-6 f-wt1">Description : </label>
                        <div class="col-sm-8 padding-none">
                            <textarea name="" id="textarea" class="form-control " rows="3" required="required" placeholder="Free Breadsticks with Large Pizza"></textarea>
                        </div>
                    </div>
                    <?php endif ?>
                    <div class="no-radius bg-White clearfix innerMB  padding-top-none l-h10 border-none">
                        <input value="Preview" type="button" class="btn-trading-wid-auto innerMT pull-right" data-toggle="modal" data-target="#keyword-popup-set-ask">
                    </div>
                    <div class="bg-color-Lightest-grey innerAll text-center">
                        <img src="https://placeholdit.imgix.net/~text?w=208&h=109" class="boxshadow">
                    </div>
                </div>
                <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                    <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto innerMT pull-right" id="submit-create_ad">
                </div>
            </form>
        </div>
    </div>
</div>
<!--============================
=            pop up            =
=============================-->
<div class="modal fade  madal--1" id="upload_creatives" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content no-radius ">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="knowMoreLabel">Upload Image</h4>
            </div>
            <div class="modal-body innerAll half">
                <p class="innerLR innerT half margin-none">Select Photos from Manage Creatives</p>
                <!-- 


                -->
                <div id="carousel-id" class="carousel bg-color-White  slide carousel--in-popup carousel--image-select" data-ride="carousel">
                    <div class="carousel-inner">
                        <?php 
                        $totalNumbersOfImages = 23;
                        if ($totalNumbersOfImages % 9 !=0) {
                            $totalNumbersOfSlides= intval($totalNumbersOfImages / 9 )+1; 
                        }
                        else {
                            $totalNumbersOfSlides= intval($totalNumbersOfImages / 9 );
                             }
                        for ($i=0 ,$k=$totalNumbersOfImages; $i < $totalNumbersOfSlides; $i++) { ?>
                        <div class="item <?php if($i==0){echo 'active';} ?>">
                            <?php for ($j=0; $j < 9 && $k>0; $j++,$k--){ ?>
                            <div class="col-xs-4 innerAll half l-h0">
                                <input type="checkbox" name="image_<?php echo $i.$j?>" id="image_<?php echo $i.$j?>">
                                <label for="image_<?php echo $i.$j?>">
                                    <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=150&h=70">
                                </label>
                            </div>
                            <?php }?>
                        </div>
                        <?php } ?>
                    </div>
                    <a class="left carousel-control" href="#carousel-id" data-slide="prev">
                        <i class="fa fa-chevron-left f-sz18"></i>
                    </a>
                    <a class="right carousel-control" href="#carousel-id" data-slide="next">
                        <i class="fa fa-chevron-right f-sz18"></i>
                    </a>
                </div>
                <!-- 


                             -->
            </div>
            <div class="modal-footer innerAll border-none padding-top-none">
                <!-- <button type="button" class="btn bg-color-App-Primary text-color-White" ">Close</button> -->
                <input value="Upload" type="button" class="btn-trading-wid-auto innerMTB" data-dismiss="modal">
            </div>
        </div>
    </div>
</div>
<!--====  End of pop up  ====-->
<script type="text/javascript">
/*
    By Osvaldas Valutis, www.osvaldas.info
    Available for use under the MIT License
*/

'use strict';

;
(function(document, window, index) {
    var inputs = document.querySelectorAll('.inputfile');
    Array.prototype.forEach.call(inputs, function(input) {
        var label = input.nextElementSibling,
            labelVal = label.innerHTML;

        input.addEventListener('change', function(e) {
            var fileName = '';
            if (this.files && this.files.length > 1)
                fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
            else
                fileName = e.target.value
                .split('\\').pop();

            if (fileName)
                label.querySelector('span').innerHTML = fileName;
            else
                label.innerHTML = labelVal;
        });

        // Firefox bug fix
        input.addEventListener('focus', function() {
            input.classList.add('has-focus');
        });
        input.addEventListener('blur', function() {
            input.classList.remove('has-focus');
        });
    });
}(document, window, 0));
</script>
<!-- /** 
 
     TODO:
     - 1. file input js and styling.  
     - 2. Inconstancy   in modal width
     - 
 
  */-->
<!--====  End of include : create_ad.php  ====-->
