<!--===========================================================
=            include : setup_campaign_schedule.php            =
============================================================-->
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15 setup_campaign_schedule-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Setup Campaign Schedule </p>
            </div>
            <div class="panel-body innerAll">
                <form action="" method="POST" class="form-horizontal row" role="form">
                    <div class="innerT clearfix">
                        <label for="" class="col-xs-4 control-label text-color-Username-Link f-wt1">Schedule :</label>
                        <div class="col-xs-8">
                            <div class="radio">
                                <input type="radio" name="schedule" id="schedule-today" value="schedule-today" checked>
                                <label class="text-color-grayscale-6 f-sz15" for="schedule-today"> Run my ad set continuously starting today</label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="schedule" id="schedule-later" value="schedule-later">
                                <label class="text-color-grayscale-6 f-sz15" for="schedule-later"> Set a start and end date</label>
                            </div>
                        </div>
                    </div>
                    <div class="start_and_end_date" style="display: none">
                        <div class="innerT innerLR clearfix">
                            <!-- <label class="text-Gray f-sz15">Set a start and end date for Ad Campaign </label> -->
                            <p> Set a start and end date for Ad Campaign </p>
                        </div>
                        <div class="clearfix text-color-grayscale-6">
                            <label for="" class="col-xs-4 control-label f-wt1 text-right">Time zone :</label>
                            <div class="col-sm-8">
                                <label for="" class="control-label f-wt1 ">kolkata </label>
                            </div>
                        </div>
                        <div class="innerT clearfix">
                            <label for="" class="col-xs-4 control-label text-color-Username-Link f-wt1 text-right">Satrt :</label>
                            <div class="col-xs-8 Setup_Campaign_Schedule-date">
                                <div class="input-group text-Blue dateRange">
                                    <span class="input-group-addon  bg-white text-Blue no-radius"> <i class="fa fa-calendar" id=""></i> </span>
                                    <input type="text" class="form-control text-Blue padding-left-none" value="May 88, 8888" placeholder="" id="date-satrt" name="date-satrt">
                                </div>
                            </div>
                            <div class="col Setup_Campaign_Schedule-time padding-left-none">
                                <div class="input-group text-Blue dateRange">
                                    <span class="input-group-addon  bg-white text-Blue no-radius"> <i class="fa fa-clock-o" id=""></i> </span>
                                    <input type="text" class="form-control text-Blue padding-left-none" value="88:88 AM" placeholder="" id="time-start" name="time-start">
                                </div>
                            </div>
                        </div>
                        <div class="innerT clearfix">
                            <label for="" class="col-xs-4 control-label text-color-Username-Link f-wt1 text-right">End :</label>
                            <div class="col-xs-8 Setup_Campaign_Schedule-date">
                                <div class="input-group text-Blue dateRange">
                                    <span class="input-group-addon  bg-white text-Blue no-radius"> <i class="fa fa-calendar" id=""></i> </span>
                                    <input type="text" class="form-control text-Blue padding-left-none" value="May 88, 8888" placeholder="" id="date-end" name="date-end">
                                </div>
                            </div>
                            <div class="col Setup_Campaign_Schedule-time padding-left-none">
                                <div class="input-group text-Blue dateRange">
                                    <span class="input-group-addon  bg-white text-Blue no-radius"> <i class="fa fa-clock-o" id=""></i> </span>
                                    <input type="text" class="form-control text-Blue padding-left-none" value="88:88 AM" placeholder="" id="time-end" name="time-end">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                <input value="Save Draft" type="button" class="btn-trading-wid-auto innerMT pull-right btn-trading-dark">
                <input value="Start Campaign" type="button" class="btn-trading-wid-auto innerMT innerMR pull-right">
            </div>
        </div>
    </div>
</div>
<!--====  End of include : setup_campaign_schedule.php  ====-->
<script type="text/javascript">
$(function() {
    /**
     *
     * Extra options should bedisplayed only if user selects to set 
     * start and end date
     *
     */
    // console.log($('#schedule-later').prop('checked'));
    $('input[name="schedule"]').on('change', function(event) {
        event.preventDefault();
        /* Act on the event */
        $('.start_and_end_date').fadeToggle("slow", "linear");
    });
  
});
</script>
