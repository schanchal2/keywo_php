<div class="card left-panel-modules audience_details_summary">
    <div class="bg-light-gray left-panel-modules-head">
        <div class="row margin-none">
            <div class="col-xs-12">
                <h4>Audience Details Summary</h4>
            </div>
        </div>
    </div>
    <div>
        <div class="summery innerLR ">
            <div class="h5  text-color-Username-Link"> 20% Completed</div>
            <div class="progress no-radius border-all">
                <div class="progress-bar bg-color-App-Success  " role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%;">
                    20%
                </div>
            </div>
            <h5 class="text-uppercase text-color-Username-Link f-wt6">Campaign _1 :</h5>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none">campaign Type</h5>
            <ul class="innerML innerL">
                <li>
                    Social Ads
                </li>
            </ul>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none">Ad Type</h5>
            <ul class="innerML innerL">
                <li>
                    Feed Image
                </li>
            </ul>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none">Campaign Targeted</h5>
            <sapn class="text-color-Username-Link"> Location :</sapn>
            <ul class="innerML innerL">
                <li> Mumbai, India</li>
                <li> Delhi, India</li>
                <li> Guntur, India</li>
            </ul>
            <sapn class="text-color-Username-Link "> Interest :</sapn>
            <ul class="innerML innerL">
                <li> Advertising</li>
                <li> Business & Industry</li>
                <li> Food & Drink</li>
            </ul>
            <div>
                <sapn class="text-color-Username-Link"> Gender :</sapn> All</div>
            <sapn class="text-color-Username-Link"> Keyword :</sapn>
            <ul class="innerML innerL">
                <li> #Sun</li>
                <li> #Sgu</li>
            </ul>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none ">Ad Budget</h5>
            <ul class="innerML innerL">
                <li>
                    <sapn class="text-color-Username-Link">Budget :</sapn> Daily Budget
                </li>
                <li>
                    <span class="text-color-Username-Link">Amount :</span> 100.00 $
                </li>
                <li>
                    <span class="text-color-Username-Link">Bid strategy :</span> Run asCPM
                </li>
            </ul>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none">Schedule</h5>
            <ul class="innerML innerL">
                <li> Set a Start and End Date</li>
                <ul class="innerML innerL">
                    <li>
                        <sapn class="text-color-Username-Link">Start Date :</sapn> 15th Jun,2016</li>
                    <li>
                        <sapn class="text-color-Username-Link"> End Date :</sapn> 16 Oct,2016</li>
                </ul>
            </ul>
        </div>
        <hr class="margin-none">
        <div class="summery innerLR innerB">
            <h5 class="text-uppercase text-color-Username-Link margin-bottom-none">Estimated Daily reach</h5>
            <span class="text-color-grayscale-be"> 15,000 - 40,000 people on keywo</span>
            <div class="daily_reach bg-color-Darkest-Gray innerMTB">
                <span style="width: 30%" class="bg-color-Username-Link"></span>
            </div>
            <div class="clearfix"><span class="text-color-grayscale-be pull-right">of 63,000,000</span></div>
            <small class="clearfix">This is only an estimate.Numbers shown are based on the average performance of ads targeted to your selected audience.</small>
        </div>
    </div>
</div>
