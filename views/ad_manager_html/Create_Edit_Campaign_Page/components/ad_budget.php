<!--=============================================
=            include : ad_budget.php            =
==============================================-->
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15 ad_budget-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Ad Budget </p>
            </div>
            <div class="panel-body innerAll">
                <p>Define how much you’d like to spend, & when you’d like your ads to appear.</p>
                <form action="" method="POST" class="form-horizontal row " role="form">
                    <div class="clearfix text-color-grayscale-6">
                        <label for="" class="col-xs-4 control-label f-wt1 text-right">Budget :</label>
                        <div class="col-sm-8">
                            <label for="" class="control-label f-wt1 ">100.00 <?php echo $keywoDefaultCurrencyName; ?> </label>
                        </div>
                    </div>
                    <div class="innerT clearfix">
                        <label for="" class="col-xs-4 control-label text-color-Username-Link f-wt1  text-right">Bid Startegy :</label>
                        <div class="col-sm-8">
                            <label for="" class="control-label f-wt1">Choose How you’d like to set bids for your ads. </label>
                            <div class="radio">
                                <input type="radio" name="bid-strategy" id="bid-strategy-CPC" value="CPC">
                                <label class="text-color-grayscale-6 f-sz15" for="bid-strategy-CPC"> Run as CPC</label>
                            </div>
                            <div class="radio">
                                <input type="radio" name="bid-strategy" id="bid-strategy-CPM" value="CPM">
                                <label class="text-color-grayscale-6 f-sz15" for="bid-strategy-CPM"> Run as CPM</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto innerMT pull-right" id="submit-ad_budget" >
            </div>
        </div>
    </div>
</div>
<!--====  End of include : ad_budget.php  ====-->
