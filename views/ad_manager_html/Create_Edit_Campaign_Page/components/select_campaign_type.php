<!--========================================================
=            include : select_campaign_type.php            =
=========================================================-->
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15 select_campaign_type-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Select Campaign Type : <span class="text-color-grayscale-be f-sz16">Choose any One Campaign Type</span></p>
            </div>
            <form class="form-horizontal">
                <div class="panel-body">
                    <div class="btn-group row" data-toggle="buttons">
                        <div class="col-xs-6 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <input type="radio" name="options" id="option1" value="social_ads"> <span class="f-sz16 select_campaign_type__type-text l-h14">SOCIAL ADS</span>
                                <i class="fa select_campaign_type__icon fa-check-circle f-sz20 pull-right "></i>
                            </label>
                        </div>
                        <div class="col-xs-6 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <input type="radio" name="options" id="option2" value="search_ads"> <span class="f-sz16 select_campaign_type__type-text l-h14">SEARCH ADS</span>
                                <i class="fa select_campaign_type__icon fa-check-circle f-sz20 pull-right "></i>
                            </label>
                        </div>
                        <div class="col-xs-6 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <input type="radio" name="options" id="option3" value="widget_sponsorship"> <span class="f-sz16 select_campaign_type__type-text l-h14">WIDGET SPONSORSHIP</span>
                                <i class="fa select_campaign_type__icon fa-check-circle f-sz20 pull-right "></i>
                            </label>
                        </div>
                        <div class="col-xs-6 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <input type="radio" name="options" id="option4" value="promote_post"> <span class="f-sz16 select_campaign_type__type-text l-h14">PROMOTE POST</span>
                                <i class="fa select_campaign_type__icon fa-check-circle f-sz20 pull-right "></i>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                    <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto  pull-right" id="submit-select_campaign_type">
                </div>
            </form>
        </div>
    </div>
</div>
<!--====  End of include : select_campaign_type.php  ====-->
