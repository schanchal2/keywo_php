<!--=======================================================
=            include : choose_ad_placement.php            =
========================================================-->
<?php 
$campaign_type = $_REQUEST["campaign_type"];
switch ($campaign_type) {
    case 'social_ads':
    $panelHeading    = "Social Ad";
    $panelSubHeading = "Choose Ad Placement";
    $navPill_1       = "Feed Image";
    $navPill_2       = "Left Column Image";
    $navPill_3       = "Left Column Text";
        break;
    
    case 'search_ads':
    $panelHeading    = "Search Ad";
    $panelSubHeading = "Choose Ad Placement";
    $navPill_1       = "Search Result Text";
    $navPill_2       = "Right Column Image";
    $navPill_3       = "Right Column Text";
        break;

    case 'widget_sponsorship':
    $panelHeading    = "Widget Sponsorship";
    $panelSubHeading = "Choose a Widget";
        break;

    case 'promote_post':
    $panelHeading    = " Promote Post";
    $panelSubHeading = "Choose a Post";
        break;

    default:
        # code...
        break;
}
 ?>
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15 choose_ad_pacement-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT">
                    <?php echo $panelHeading ?> : <span class="text-color-grayscale-be f-sz16"> <?php echo $panelSubHeading ?></span></p>
            </div>
            <div class="panel-body innerAll">
                <?php if ($campaign_type=="social_ads" || $campaign_type=="search_ads"  ): ?>
                <!-- Nav tabs -->
                <ul class="nav nav-pills nav-justified choose_ad_pacement-panel__nav  f-sz15" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#feed_image" aria-controls="home" role="tab" data-toggle="tab" class="no-radius">
                            <?php echo $navPill_1  ?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#Left_Column_Image" aria-controls="profile" role="tab" data-toggle="tab" class="no-radius">
                            <?php echo $navPill_2 ?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#Left_Column_Text" aria-controls="messages" role="tab" data-toggle="tab" class="no-radius">
                            <?php echo $navPill_3 ?>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content innerT half">
                    <div role="tabpanel" class="tab-pane active" id="feed_image">
                        <img src="https://placeholdit.imgix.net/~text?w=470&h=200">
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Left_Column_Image">
                        <img src="https://placeholdit.imgix.net/~text?w=470&h=200">
                    </div>
                    <div role="tabpanel" class="tab-pane" id="Left_Column_Text">
                        <img src="https://placeholdit.imgix.net/~text?w=470&h=200">
                    </div>
                </div>
                <?php endif ?>
                <?php if ($campaign_type=="widget_sponsorship" || $campaign_type=="promote_post"): ?>
                <img src="https://placeholdit.imgix.net/~text?w=470&h=200">
                <?php endif ?>
                <?php if ($campaign_type=="widget_sponsorship"): ?>
                <p class="innerT margin-none"> Select Widgets you want to Sponsor -</p>
                <div class="innerAll half">
                    <div class="btn-group row widget-radio_option" data-toggle="buttons">
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <span class="pull-left widget-icon"> <i class="fa fa-clock-o f-sz20"></i></span>
                                <input type="radio" name="options" id="option1" value="social_ads"> <span class="pull-left f-sz14 widge-name innerL">TimeZone </span>
                            </label>
                        </div>
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 active ">
                                <span class="pull-left widget-icon"> <i class="fa fa-dollar f-sz20"></i></span>
                                <input type="radio" name="options" id="option2" value="search_ads"> <span class="pull-left f-sz14 widge-name innerL">Currency</span>
                            </label>
                        </div>
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <span class="pull-left widget-icon"> <i class="fa fa-btc f-sz20"></i></span>
                                <input type="radio" name="options" id="option3" value="widget_sponsorship"> <span class="pull-left f-sz14 widge-name innerL">Crypto Currency</span>
                            </label>
                        </div>
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <span class="pull-left widget-icon"> <i class="fa fa-weather_forecast f-sz20"> &nbsp;</i></span>
                                <input type="radio" name="options" id="option4" value="promote_post"> <span class="pull-left f-sz14 widge-name innerL">Weather Forecast</span>
                            </label>
                        </div>
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <span class="pull-left widget-icon"> <i class="fa fa-unit_converter f-sz20">&nbsp;</i></span>
                                <input type="radio" name="options" id="option5" value="promote_post"> <span class="pull-left f-sz14 widge-name innerL">Unit Converter</span>
                            </label>
                        </div>
                        <div class="col-xs-4 innerAll half">
                            <label class="btn btn-primary no-radius l-h10 ">
                                <span class="pull-left widget-icon"> <i class="fa fa-calculator f-sz20"></i></span>
                                <input type="radio" name="options" id="option6" value="promote_post"> <span class="pull-left f-sz14 widge-name innerL">Calculator</span>
                            </label>
                        </div>
                    </div>
                </div>
                <?php endif ?>
                <!-- START promote_post -->
                <?php if ($campaign_type=="promote_post"): ?>
                <div class="no-radius bg-White clearfix innerMB padding-top-none l-h10 border-none">
                    <input value="Select Post" type="button" class="btn-trading-wid-auto innerMT pull-right" data-toggle="modal" data-target="#select_post_to_promot">
                </div>
                <div class="bg-color-Lightest-grey innerAll text-center">
                    <img src="https://placeholdit.imgix.net/~text?w=208&h=109" class="boxshadow">
                </div>
                <?php endif ?>
                <!-- END promote_post -->
            </div>
            <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto innerMT pull-right" id="submit-choose_ad_placement">
            </div>
        </div>
    </div>
</div>
<!--===========================
=            popup            =
============================-->
<div class="modal fade madal--1" id="select_post_to_promot" tabindex="-1" role="dialog" aria-labelledby="">
    <div class="modal-dialog innerMT inner-9x" role="document">
        <div class="modal-content no-radius ">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="">Select Post From your Timeline</h4>
            </div>
            <div class="modal-body innerAll half">
                <div class="pramote_post--modal-body">
                    <div role="tabpanel" class="innerLR innerT half margin-none">
                        <!-- Nav tabs -->
                        <div class="row">
                            <form action="" class="form-inline">
                                <div class="filter">
                                    <div class="col-xs-8">
                                        <ul class="nav nav-pills nav-justified pramote_post--nav l-h10 in innerTB f-sz15" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#all" aria-controls="all" role="tab" data-toggle="tab">ALL</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#status" aria-controls="status" role="tab" data-toggle="tab">STATUS</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#blog" aria-controls="blog" role="tab" data-toggle="tab">BLOG   </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#video" aria-controls="video" role="tab" data-toggle="tab">VIDEO   </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#image" aria-controls="image" role="tab" data-toggle="tab">IMAGE </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#audio" aria-controls="audio" role="tab" data-toggle="tab">AUDIO</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="pull-right margin-none ">
                                            <div class="input-group pramote_post--modal-body__searchbox">
                                                <input type="text" class="form-control f-sz14  text-grayscale-be" placeholder="Search">
                                                <span class="input-group-btn ">
                                                    <button class="btn btn-default pramote_post--modal-body__searchbox-button" type="button ">  <i class="fa fa-search text-grayscale-6 "></i></button>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content innerMT">
                            <div role="tabpanel" class="tab-pane active" id="all"> all</div>
                            <div role="tabpanel" class="tab-pane" id="status"> status</div>
                            <div role="tabpanel" class="tab-pane" id="blog"> blog</div>
                            <div role="tabpanel" class="tab-pane" id="video"> video</div>
                            <div role="tabpanel" class="tab-pane" id="image"> image</div>
                            <div role="tabpanel" class="tab-pane" id="audio"> audio</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--====  End of popup  ====-->
<!--====  End of include : choose_ad_placement.php  ====-->
<script type="text/javascript">
loadPramotPostList("#all");
$('.pramote_post--modal-body a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
        // console.log("_**************" + e.target);
        // console.log("_**************" + e.relatedTarget);
        // console.log(e);
        // console.log(this.hash);
    loadPramotPostList(this.hash);
});

function loadPramotPostList(tabPaneId) {
    // body...
    $(tabPaneId).load('../pramot_post_list.php', {
            postType: tabPaneId.substr(1)
        },
        function() {
            /* Stuff to do after the page is loaded */
        });
}
</script>
