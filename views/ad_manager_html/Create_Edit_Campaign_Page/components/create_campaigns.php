<!--====================================================
=            include : create_campaigns.php            =
=====================================================-->
<div class="row">
    <div class="col-xs-12 innerB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Create Campaigns </p>
            </div>
            <form class="form-horizontal innerT">
                <div class="panel-body innerAll">
                    <!-- <form class="form-horizontal innerT"> -->
                    <!-- 
                                     -->
                    <div class="form-group margin-bottom-none filter clearfix margin-none">
                        <label for="inputPassword3" class="col-sm-4 margin-none padding-left-none l-h10 innerT text-left text-grayscale-6 f-wt1">Enter Campaign Name : </label>
                        <div class="col-sm-8  padding-none">
                            <!-- <div class="input-group text-grayscale-4"> -->
                            <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" placeholder="">
                            <!--  <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"><?php //echo $keywoDefaultCurrencyName; ?> </span> -->
                            <!-- </div> -->
                        </div>
                    </div>
                    <!-- 
                                     -->
                    <!--  </form> -->
                </div>
                <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                    <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto pull-right" id="submit-campaigns_name">
                </div>
            </form>
        </div>
    </div>
</div>
<!--====  End of include : create_campaigns.php  ====-->
