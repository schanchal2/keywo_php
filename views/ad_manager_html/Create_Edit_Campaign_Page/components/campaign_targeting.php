<!--======================================================
=            include : campaign_targeting.php            =
=======================================================-->
<div class="row">
    <div class="col-xs-12 innerTB">
        <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz14 campaign_targeting-panel">
            <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                <p class="leadCorrect-b innerT"> Campaign Targeting</p>
            </div>
            <div class="panel-body innerAll">
                <form onSubmit="return false;" method="POST" class="form-horizontal row" role="form">
                    <!-- add location -->
                    <p class="f-sz14 col">Tell us about the people you'd most like to connect with everyone in this location.</p>
                    <div class="col-xs-12 add-location">
                        <div class="row">
                            <div class="col-xs-6">
                            <div class="force-caret-icon ">
                                <input type="text" class="form-control" id="add-location-input" placeholder=""></div>
                            </div>
                            <div class="col-xs-6">
                                <button class="btn btn-default bg-color-Lightest-Blue text-white no-radius border-none" type="submit" id="add-location-button"><i class="fa fa-plus"></i> Add location</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6">
                                <ul class="list-unstyled add-location-list">
                                    <li class="border-bottom" style="display: none"> <i class="fa fa-map-marker text-color-Lightest-Blue"></i> <span></span>, <span class="text-color-Lightest-Blue"></span> <i class="fa fa-close pull-right text-color-grayscale-be"></i> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- add interest -->
                    <p class="f-sz14 col"> Detailed Targeting INCLUDE people who match atleast ONE of the following Interest.
                    </p>
                    <div class="col-xs-12 add-interest">
                        <span>select the interest</span>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="force-caret-icon ">
                                    <input type="text" class="form-control" id="add-interest-input" placeholder="">
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <button class="btn btn-default bg-color-Lightest-Blue text-white no-radius border-none" type="submit" id="add-interest-button"><i class="fa fa-plus"></i> Add interest</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                            </div>
                            <div class="col-xs-6">
                                <ul class="list-unstyled add-interest-list">
                                    <li class="border-bottom" style="display: none"> <span>interest</span> <i class="fa fa-close pull-right text-color-grayscale-be"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- gender select -->
                    <div class="innerT clearfix">
                        <label for="" class="col-sm-4 control-label  f-wt1  text-right">Gender :</label>
                        <div class="col-sm-8">
                            <div class="btn-group campaign_targeting-panel__btn-group row" data-toggle="buttons">
                                <div class="col padding-none">
                                    <label class="btn btn-primary no-radius active l-h12 f-sz14 ">
                                        <input class="campaign_targeting-panel__radio-input" type="radio" name="options" id="option1" autocomplete="off" checked> <span>All</span>
                                    </label>
                                </div>
                                <div class="col padding-right-none">
                                    <label class="btn btn-primary no-radius l-h12 f-sz14 ">
                                        <input class="campaign_targeting-panel__radio-input" type="radio" name="options" id="option2" autocomplete="off"> <span>Male</span>
                                    </label>
                                </div>
                                <div class="col padding-right-none">
                                    <label class="btn btn-primary no-radius l-h12 f-sz14 ">
                                        <input class="campaign_targeting-panel__radio-input" type="radio" name="options" id="option3" autocomplete="off"> <span>Female</span>
                                    </label>
                                </div>
                                <div class="col padding-right-none">
                                    <label class="btn btn-primary no-radius l-h12 f-sz14 ">
                                        <input class="campaign_targeting-panel__radio-input" type="radio" name="options" id="option3" autocomplete="off"> <span>Unknown</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="innerT clearfix">
                        <label for="" class="col-sm-4 control-label f-wt1 text-right">Keyword / Hash Tag :</label>
                        <div class="col-sm-8 padding-left-none">
                            <div id="tags" class="tag-text border-all clearfix" data-id="selected-keywords">
                                <input type="text" class="tag-input" placeholder="" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="panel-footer no-radius bg-White clearfix innerAll innerMB padding-top-none l-h10 border-none">
                <input value="Save &amp; Continue" type="button" class="btn-trading-wid-auto innerMT pull-right" id="submit-campaign_targeting">
            </div>
        </div>
    </div>
</div>
<style type="text/css">
</style>
<script type="text/javascript">
jQuery(document).ready(function($) {



    $(".tag-input").focus(function() {
        console.log("zilsu");
        $(".tag-text").css({
            border: "1px solid #719ECE",
            boxShadow: "0 0 10px #719ECE"
        });
    });
    $(".tag-input").blur(function() {
        $(".tag-text").css({
            border: "1px solid #ccc",
            boxShadow: "none"
        });
    });
    /*===============================================
    =            script for keyword tags            =
    ===============================================*/
    /**
     *
     * copied from social_module.js with help of Palak.
     *
     */
    $(".tag-text input").on({
        keyup: function(ev) {
            // if: comma|enter (delimit more keyCodes with | pipe)
            if (/(188|13|32)/.test(ev.which)) {
                //var txt= this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
                var txt = this.value; //allow all characters
                // txt = txt.replace(",","");
                txt = txt.replace(" ", "");
                txt = $.trim(txt);
                var sameKeywordFlag = 0;
                $(this).parent().find("span").each(function() {
                    if ($(this).text().toLowerCase() == "#" + txt.toLowerCase()) {
                        sameKeywordFlag = 1;
                    }
                });
                if (sameKeywordFlag == 0) {
                    if (txt) $("<span/>", {
                        class: "keywords-for-tags",
                        text: '#' + txt.toLowerCase(),
                        insertBefore: this
                    });
                }
                this.value = "";
            }
            //if want event on focusout ---> $(this).focusout();
        },
        paste: function() {
            //alert("hellqo");
        }
    });
    //on paste event for tagged keywords
    $(".tag-input").bind("paste", function(e) {
        // access the clipboard using the api
        var pastedData = e.originalEvent.clipboardData.getData('text');
        e.preventDefault();
        var keywords = pastedData.split(" ");
        for (var i = 0; i < keywords.length; i++) {
            var txt = keywords[i]; // allowed all characters
            var sameKeywordFlag = 0;
            $(this).parent().find("span").each(function() {
                if ($(this).text().toLowerCase() == "#" + txt.toLowerCase()) {
                    sameKeywordFlag = 1;
                }
            });
            if (sameKeywordFlag == 0) {
                if (txt) $("<span/>", {
                    class: "tagged-keyword-span",
                    text: '#' + txt.toLowerCase(),
                    insertBefore: this
                });
            }
        }
    });
    $('.tag-text').on('click', 'span', function() {
        $(this).remove(); //    if(confirm("Remove "+ $(this).text() +"?"))
    });
    /*=====  End of script for keyword tags  ======*/
});

/*====================================
=            autocomplete            =
====================================*/
$(function() {
    var location = [
        "Alocation",
        "Blocationg",
        "Clocation"

    ];
    var interest = [
        "agriculture",
        "Advertising",
        "Architecture",
        "Arts",
        "Animals"
    ];
    //autocomplet must fire on "#add-location-input" first as it has css related to it
    $("#add-location-input").autocomplete({
        source: location
    });
    $("#add-interest-input").autocomplete({
        source: interest
    });
});

/*=====  End of autocomplete  ======*/
/*===========================================
=            adding item to list            =
===========================================*/
$("#add-interest-button").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    var userinterestInput = $("#add-interest-input").val();
    console.log(userinterestInput);
    $(".add-interest-list li:first-child").clone(true, true).prependTo('.add-interest-list').removeAttr('style')
        .find('span').text("");
    $(".add-interest-list li:first-child span").text(userinterestInput);
});
$("#add-location-button").on('click', function(event) {
    event.preventDefault();
    /* Act on the event */
    var userlocationInput = $("#add-location-input").val();
    console.log(userlocationInput);
    $(".add-location-list li:first-child").clone(true, true).prependTo('.add-location-list').removeAttr('style')
        .find('span').text("");
    $(".add-location-list li:first-child span").text(userlocationInput);
});

/*=====  End of adding item to list  ======*/

/*===============================================
=            deleting item from list            =
===============================================*/
$(function() {


    // run the currently selected effect
    function runEffect(target) {
        // get effect type from
        // var selectedEffect = $("#effectTypes").val();

        // Most effect types need no options passed by default
        var options = {};
        // some effects have required parameters
        /*if (selectedEffect === "scale") {
            options = {
                percent: 50
            };
        } else if (selectedEffect === "size") {
            options = {
                to: {
                    width: 200,
                    height: 60
                }
            };
        }*/
        // Run the effect
        $(target).hide("fade", options, 500);
    };

    // Set effect from select menu value
    $(".add-interest-list, .add-location-list").find('.fa-close').on("click", function() {
        console.log($(this));
        var target = $(this).parents("li");
        console.log(target);
        runEffect(target);
    });
});


/*=====  End of deleting item from list  ======*/
</script>
<!--====  End of include : campaign_targeting.php  ====-->
