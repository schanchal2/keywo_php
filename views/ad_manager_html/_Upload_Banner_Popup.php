<!-- popup in "Create_Edit_Campaign_Page_****.php" -->
<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<!-- 



 -->
<main class="inner-7x innerT keyword-markt-popup-common ADP9">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <!-- 




         -->
            <a class="btn btn-primary" data-toggle="modal" href='#upload_creatives'>Trigger modal</a>
            <div class="modal fade keyword-popup" id="upload_creatives" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content no-radius ">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="knowMoreLabel">Upload Image</h4>
                        </div>
                        <div class="modal-body">
                            <p class="innerLR innerT half margin-none">Select Photos from Manage Creatives</p>
                            <!-- 


                         -->
                            <div id="carousel-id" class="carousel bg-color-White  slide carousel--in-popup carousel--image-select" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php 

                                    $totalNumbersOfImages = 23;
                                    if ($totalNumbersOfImages % 9 !=0) {
                                    	$totalNumbersOfSlides= intval($totalNumbersOfImages / 9 )+1; 
                                    }
                                    else {
                                    	$totalNumbersOfSlides= intval($totalNumbersOfImages / 9 );
                                    	 }
                        
                                     for ($i=0 ,$k=$totalNumbersOfImages; $i < $totalNumbersOfSlides; $i++) { ?>
                                    <div class="item <?php if($i==0){echo 'active';} ?>">
                                        <?php for ($j=0; $j < 9 && $k>0; $j++,$k--){ ?>
                                        <div class="col-xs-4 innerAll half l-h0">
                                            <input type="checkbox" name="image_<?php echo $i.$j?>" id="image_<?php echo $i.$j?>">
                                            <label for="image_<?php echo $i.$j?>">
                                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=150&h=70">
                                            </label>
                                        </div>
                                        <?php }?>
                                    </div>
                                    <?php } ?>
                                </div>
                                <a class="left carousel-control" href="#carousel-id" data-slide="prev">
                                    <i class="fa fa-chevron-left f-sz18"></i>
                                </a>
                                <a class="right carousel-control" href="#carousel-id" data-slide="next">
                                    <i class="fa fa-chevron-right f-sz18"></i>
                                </a>
                            </div>
                            <!-- 


                             -->
                        </div>
                        <div class="modal-footer innerAll border-none padding-top-none">
                            <!-- <button type="button" class="btn bg-color-App-Primary text-color-White" ">Close</button> -->
                            <input value="Upload" type="button" class="btn-trading-wid-auto innerMTB" data-dismiss="modal">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- 


             -->
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
