<!-- popup in "Create_Edit_Campaign_Page_****.php" -->
<?php include("../layout/header.php");?>
<!--======================================
=            For apearance of            =
=======================================-->
<!-- .my-info-card -->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_earnings.css" type="text/css" />
<!--====  End of For apearance of  ====-->
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_ad_manager.css" type="text/css" />
<!-- 



 -->
<main class="inner-7x innerT keyword-markt-popup-common ADP9">
    <div class="container row-10 text-color-grayscale-4 padding-none">
        <div class="row">
            <!-- 




         -->
            <a class="btn btn-primary" data-toggle="modal" href='#upload_creatives' tabindex="1">Select Post From your Timeline</a>
            <div class="modal fade madal--1" id="upload_creatives" tabindex="-1" role="dialog" aria-labelledby="knowMoreLabel">
                <!-- keyword-popup -->
                <div class="modal-dialog innerMT inner-9x" role="document">
                    <div class="modal-content no-radius ">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id="knowMoreLabel">Select Post From your Timeline</h4>
                        </div>
                        <div class="modal-body">
                            <div class="pramote_post--modal-body">
                                <div role="tabpanel" class="innerLR innerT half margin-none">
                                    <!-- Nav tabs -->
                                    <div class="row">
                                        <form action="" class="form-inline">
                                            <div class="filter">
                                                <div class="col-xs-8">
                                                    <ul class="nav nav-pills nav-justified pramote_post--nav l-h10 in	innerTB" role="tablist">
                                                        <li role="presentation" class="active">
                                                            <a href="#all" aria-controls="all" role="tab" data-toggle="tab">ALL</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#status" aria-controls="status" role="tab" data-toggle="tab">STATUS</a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#blog" aria-controls="blog" role="tab" data-toggle="tab">BLOG   </a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#video" aria-controls="video" role="tab" data-toggle="tab">VIDEO   </a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#image" aria-controls="image" role="tab" data-toggle="tab">IMAGE </a>
                                                        </li>
                                                        <li role="presentation">
                                                            <a href="#audio" aria-controls="audio" role="tab" data-toggle="tab">AUDIO</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="pull-right margin-none ">
                                                        <div class="input-group pramote_post--modal-body__searchbox">
                                                            <input type="text" class="form-control f-sz14  text-grayscale-be" placeholder="Search">
                                                            <span class="input-group-btn ">
                                					<button class="btn btn-default" type="button ">  <i class="fa fa-search text-grayscale-6 "></i></button>
                                					</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <!-- Tab panes -->
                                    <div class="tab-content innerMT">
                                        <div role="tabpanel" class="tab-pane active" id="all"> all</div>
                                        <div role="tabpanel" class="tab-pane" id="status"> status</div>
                                        <div role="tabpanel" class="tab-pane" id="blog"> blog</div>
                                        <div role="tabpanel" class="tab-pane" id="video"> video</div>
                                        <div role="tabpanel" class="tab-pane" id="image"> image</div>
                                        <div role="tabpanel" class="tab-pane" id="audio"> audio</div>
                                    </div>
                                </div>
                            </div>
                            <!-- 


                         	-->
                            <!-- 


                             -->
                        </div>
                        <!--   <div class="modal-footer innerAll border-none padding-top-none">
                            <button type="button" class="btn bg-color-App-Primary text-color-White" ">Close</button>
                            <input value="Upload" type="button" class="btn-trading-wid-auto innerMTB" data-dismiss="modal">
                        </div> -->
                    </div>
                </div>
            </div>
        </div>
        <!-- 


             -->
    </div>
</main>
<!--=============================
=            Pop-ups            =
==============================-->
<!--====  End of Pop-ups  ====-->
<?php include("../layout/transparent_footer.php");?>
<!--=====================================
=            on-page script             =
======================================-->
<script type="text/javascript">
loadPramotPostList("#all");
$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
    e.target // newly activated tab
    e.relatedTarget // previous active tab
        // console.log("_**************" + e.target);
        // console.log("_**************" + e.relatedTarget);
        // console.log(e);
        // console.log(this.hash);
    loadPramotPostList(this.hash);
});

function loadPramotPostList(tabPaneId) {
    // body...
    $(tabPaneId).load('pramot_post_list.php', {
            postType: tabPaneId.substr(1)
        },
        function() {
            /* Stuff to do after the page is loaded */
        });
}
</script>
<!--====  End of on-page script   ====-->
</body>

</html>
