<?php
session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";
require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once ("{$docrootpath}helpers/coreFunctions.php");

$kwdDbConn = createDBConnection("dbkeywords");
$conn = $kwdDbConn["connection"];
$result=getMaintainenceMsg($conn);
$message=$result["errMsg"]["message"];
$panelPixel = '';
$message = '';
if($message!="") {
    $panelPixel = 'top:75px';
    ?>

    <div class="container-fluid bg-red text-color-White">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center has-error innerTB half"><i class="fa fa-warning"></i> <?= $message; ?></div>
            </div>
        </div>
    </div>
    <?php
}else{
    $panelPixel = 'top:44px';
}
?>