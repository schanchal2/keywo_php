 <?php

/*================================= Include helper files =========================================*/
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once "../../config/countryArray.php";
require_once "../../models/keywords/userCartModel.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/sessionHelper.php";
require_once "../../helpers/errorMap.php";

error_reporting(0);

 if(isset($_COOKIE["keywo_ver_1"]) && !empty($_COOKIE["keywo_ver_1"])){
 $kwdDbConn = createDBConnection('dbkeywords');
 if (noError($kwdDbConn)) {
     $kwdDbConn = $kwdDbConn["connection"];
 }
     checkForSession($kwdDbConn);
 }

include("../layout/header.php");

?>

<main class="bg-concrete" style="padding-top:80px;">
  <div class="prelogin-wrapper">
    <div class="container">
      <div class="keywo_tagline">
        <h1 class="text-center text-blue keywo_tagline_heading  half innerTB" id="page_title">
          NOW! TURN YOUR EVERYDAY <span class="text-black">INTERNET ACTIVITIES</span> INTO <span class="text-black">INCOME</span>
        </h1>
      </div>

      <!--============================== CONSUMING AND UPLOADING CONTENT ==============================-->
      <div class="row top-block">
        <div class="col-xs-8">
          <!--carousel-->
          <div class="row">
            <div class="col-xs-12 half innerR">
              <div id="myCarousel" class="carousel slide" style="height:423px;">
                  <!-- Carousel items -->
                  <div class="carousel-inner prelogin-carousel">
                  <!-- slider is loaded using ajax-->
                  </div><!--/carousel-inner-->
                  </div><!--/myCarousel-->
              </div>
          </div>
          <!--============================== TEXT INVEST IN KEYWORDS ==============================-->
          <div class="row">
            <div class="col-xs-12 half innerR">
              <div class="prelogin_invest_keyword">
                  <h3 id="bannerTitle" class=" text-center bg-white innerTB text-blue">INVEST IN KEYWORDS</h3>
              </div>
            </div>
          </div>
        </div>
        <!-- col-xs-8 -->

          <!--============================== Register Now Form ==============================-->
          <div class="col-xs-4">
              <div class="card innerAll register-block inner-2x innerLR">
                  <div class="registration">
                      <div class="col-xs-12 form-title">
                          <h2>REGISTER NOW</h2>
                      </div>
                      <form id="registerForm">
                          <div class="row  innerB">
                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <input type="text" name="firstName" id="firstName" class="checkName form-control register-form-input" placeholder="First Name">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                              <div class=" col-xs-6 padding-left-none">
                                  <div class="form-group">
                                      <input type="text" name="lastName" id="lastName" class="checkName form-control register-form-input" placeholder="Last Name">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                          </div>
                          <div class="row innerB">
                              <div class=" col-xs-12" data-placement="right">
                                  <div class="form-group">
                                      <input type="text" name="email" id="email" class="form-control register-form-input" placeholder="Email">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                          </div>
                          <div class="row  innerB">
                              <div class="col-xs-12">
                                  <div class="form-group">
                                      <input type="text" id="accountHandle" name="accountHandle" class="form-control register-form-input" placeholder="Account handle">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                          </div>
                          <div class="row innerB">
                              <div class="col-xs-6">
                                  <div class="form-group">
                                      <input type="password" name="password" id="password" class="passwordValidation form-control register-form-input" placeholder="Password">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                              <div class="col-xs-6 padding-left-none">
                                  <div class="form-group">
                                      <input type="password" name="confirmPassword" id="confirmPassword" class="passwordIdenticalValidation form-control register-form-input" placeholder="Confirm Password">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                          </div>
                          <div class="row  innerB">
                              <div class="col-xs-6">
                                  <select class="selectpicker select-input chosen-select-deselect" data-placeholder="Select a Country" id="country" name="country" onChange="printCities(this.value)">
                                      <option value="">Select Country</option>
                                      <?php
                                foreach ($countryCity as $key => $country)
                                {
                                echo "<option value=\"$key\">{$key}</option>";
                                }
                                ?>
                                  </select>
                              </div>
                              <div class="col-xs-6 padding-left-none">
                                  <select class="selectpicker  select-input chosen-select-deselect selCity" data-placeholder="Select city" id="city" name="city">
                                  </select>
                              </div>
                          </div>
                          <div class="row  innerB">
                              <div class="col-xs-12">
                                  <div class="">
                                      <input type="text" name="referal" id="referal" class="form-control register-form-input" placeholder="Referal code - get upto 10% discount">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                          </div>
                          <div class="row  half innerB">
                              <div class="col-xs-12">
                                  <div class="radio radio-primary radio-div">
                                      <input type="radio" name="gender" id="male" value="male" checked="true">
                                      <label for="male">
                                          Male
                                      </label>
                                  </div>
                                  <div class="radio radio-primary radio-div">
                                      <input type="radio" name="gender" id="female" value="female">
                                      <label for="female">
                                          Female
                                      </label>
                                  </div>
                                  <div class="radio radio-primary radio-div">
                                      <input type="radio" name="gender" id="other" value="other">
                                      <label for="other">
                                          Other
                                      </label>
                                  </div>
                              </div>
                          </div>
                          <div class="row  half innerB">
                              <div class="col-xs-12">
                                  <div class="captcha">
                                      <div class="recaptcha">
                                          <!--<div class="g-recaptcha" data-sitekey="6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r">
                                    <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r&amp;co=aHR0cHM6Ly9zZWFyY2h0cmFkZS5jb206NDQz&amp;hl=en&amp;v=r20161109131337&amp;size=normal&amp;cb=b5irftqmaw1g" title="recaptcha widget" width="304" height="78" role="presentation" frameborder="0" scrolling="no" name="undefined">
                                    </iframe>
                                  </div>-->
                                          <div class="g-recaptcha" data-sitekey="<?php echo $captchaPublickey; ?>"></div>
                                      </div>
                                      <!--recaptcha-->
                                  </div>
                              </div>
                          </div>
                          <div class="row  innerB">
                              <div class="col-xs-12">
                                  <input class="agreeCheckbox" name="acceptTerms" id="agree" type="checkbox">
                                  <a href="#" class="agreeText">I agree to the terms and conditions</a>
                              </div>
                          </div>
                          <div class="row  innerB form-title">
                              <button class="btn btn-signup" type="button" id="registerbutton" onclick="signupSubmit();" name="submit" value="Sign Up">Sign Up</button>
                              <span class="loading-icon"></span>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>

      <!--============================== EARNING FEATURES ==============================-->
      <div class="earning-wrapper">
        <div class="row middle-block">
          <div class="col-xs-3 padding-left-none">
            <div class="side-left">
              <div class="innerL">
                <h4 class="text-blue margin-top-none">
                  <span class="how-to-span">How to</span><br />
                  <span class="earn-feature-span">EARN?</span>
                </h4>
              </div>
              <div class="side-left-features">
                <ul class="nav nav-pills nav-stacked earn-list" id="keywo-earn">
                </ul>
              </div>
            </div>
            <!-- side-left -->
          </div>
          <!-- col-xs-3 -->

          <div class="col-xs-6">
            <div class="text-center" style="margin:0 auto;position:relative;">
            <div class="embed-responsive embed-responsive-16by9" style="border: 5px solid white;border-radius: 5px;">
              <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ELwct3HQp3E" frameborder="0" allowfullscreen></iframe>
            </div>
                  <h4 class="how-to-earn text-blue" id="short_desc"> How to Search and Earn </h4>
                  <p class="text-justify" id="long_desc">
                      Isn’t it amazing to get paid for simply searching online ? Keywo is a unique platform that pays you for searching from it’s search engine. You can simply search like you do in other search engines and get paid for every search you carry out.
                  </p>
            </div>
          </div>
          <!-- col-xs-6 -->

            <div class="col-xs-3 padding-right-none">
              <div class="side-left pull-right">
                <div class="innerR">
                  <h4 class="text-right text-blue margin-top-none">
                    <span class="how-to-span">Keywo</span><br />
                    <span class="earn-feature-span">FEATURES</span>
                  </h4>
                </div>
                <div class="side-right-features">
                  <ul class="nav nav-pills nav-stacked earn-list text-right" id="keywo-feature">
                  </ul>
                </div>
              </div>
            </div>
            <!-- col-xs-3 -->
          </div>


        </div>
      <!-- earning-wrapper -->

      <!--============================== TOTAL EARNING DISPLAYED  ==============================-->
      <div class="keywo-analytics innerTB" id="pool-stats-main-div">
          <!-- load pool stats details in this pool-stats-main-div div -->
          <?php include("poolStats.php"); ?>
      </div>
      <!-- keywo-analytics-->
    </div>
    <!-- container -->
  </div>
  <!-- prelogin-wrapper -->
</main>


<?php include("../layout/prelogin_footer.php"); ?>
<script>

      // get global root url from config.php file.
      var rootURL = '<?php echo $rootUrl; ?>';

      // get the keywo's landing page CMS slider.
      getLandingSlider(rootURL);
      $('#registerForm').bootstrapValidator({
          message: 'This value is not valid',
          container: 'tooltip',
          feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          excluded: [':disabled'],
          fields: {
            firstName:{
              validators: {
                      notEmpty: {
                          message: 'The First Name is required and can\'t be empty'
                      }
                  }
            },
            lastName:{
              validators: {
                      notEmpty: {
                          message: 'The Last Name is required and can\'t be empty'
                      }
                  }
            },
            country: {
                group: '.col-xs-6',
                  validators: {
                      notEmpty: {
                          message: 'The country is required and can\'t be empty'
                      }
                  }
              },

              city: {
                group: '.col-xs-6',
                  validators: {
                      notEmpty: {
                          message: 'The City is required and can\'t be empty'
                      }
                  }
              },
              acceptTerms:{
                group: '.col-xs-12',
                validators: {
                      notEmpty: {
                          message: 'You have to accept the terms and policies'
                      }
                  }
              }
          }
      }).on("success.form.bv",function(){
      });

      getFeatureData(rootURL);

      getEarnData(rootURL);

      getPageData(rootURL);
      $(document).ready(function(){
        $("#form_prelogin_login_username").focus();
        $('#registerbutton').on('click',function(){

          $(".registration input").each(function(){
            if($(this).val() == "" && $(this).attr("id") != "referal"){
              $(this).parent().attr("class", "has-error has-feedback chosen-search");
              $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            }
          });

        });
      });


</script>
