<?php
session_start();
  // 1. total count of registed users
  // 2. total count of keyword sold
  // 3. Get pool stats details by calling pool stats API.
  // 4. calculate total payout given retrieve from API response
  // 5. calculate total income earned

  require_once('../../models/user/authenticationModel.php');
  require_once('../../models/landing/landingFeatureModel.php');
   require_once('../../models/header/headerModel.php');
    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../helpers/deviceHelper.php";
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/coreFunctions.php";

  error_reporting(0);
  // crete database connection
  $keywordDbConn = createDBConnection("dbkeywords");
  $searchDbConn = createDBConnection("dbsearch");

    $postLikeCount=getTotalLikeCount("like");
    if(noError($postLikeCount))
    {
        $postLikeCount=$postLikeCount["errMsg"]["total_like_count"];
    }else{
        $postLikeCount=0;
    }

    $postCreateCount=getTotalLikeCount("posts");
    if(noError($postCreateCount))
    {
        $postCreateCount=$postCreateCount["errMsg"]["all"]["total_post"];
    }else{
        $postCreateCount=0;
    }


    $postShareCount=getTotalLikeCount("share");
    if(noError($postShareCount))
    {
        $postShareCount=$postShareCount["errMsg"]["total_post"];
    }else{
        $postShareCount=0;
    }

  if(noError($keywordDbConn) && noError($searchDbConn)){
    $keywordDbConn = $keywordDbConn["connection"];
    $searchDbConn  = $searchDbConn["connection"];

        // 1. total count of registed users
    $getTotalRegisteredUser = getKeywoStats($keywordDbConn);
// echo "<br><br><br>getTotalRegisteredUser";
// printArr($getTotalRegisteredUser);
// echo "getTotalRegisteredUser End<br><br>";
    if(noError($getTotalRegisteredUser)){
      $getTotalRegisteredUser = $getTotalRegisteredUser["data"][0]["registered_users"];
      if($getTotalRegisteredUser <= 0){
        $getTotalRegisteredUser = 0;
      }

              // 2. Get Payout per Like Count
      
      $userPerLike = getSettingsFromSearchAdmin($searchDbConn);
// echo "<br><br><br>userPerLike";
// printArr($userPerLike);
// echo "userPerLike End<br><br>";
      if(noError($userPerLike)){
        $userPerLike      = $userPerLike['data'];
        $userPerLikeCount = $userPerLike['current_max_payout'] / 4;
        if($userPerLikeCount <= 0){
          $userPerLikeCount = 0;
        }

                    // 3. Get pool stats details by calling pool stats API.
        $getPoolStats = getPoolStats();
                  //  printArr($getPoolStats);
// echo "<br><br><br>getPoolStats";
// printArr($getPoolStats);
// echo "getPoolStats End<br><br>";
        if(noError($getPoolStats)){
          $getPoolStats = $getPoolStats["errMsg"];
                          //printArr($getPoolStats);

                          // 4. calculate total payout given retrieve from API response
          $totalSearchPayout = array('search_earning'=> $getPoolStats["search_earning"]);
          $calculateTotalPayout = calculateTotalPayout($totalSearchPayout);
// echo "<br><br><br>calculateTotalPayout";
// printArr($calculateTotalPayout);
// echo "calculateTotalPayout End<br><br>";
          if(noError($calculateTotalPayout)){
            $calculateTotalPayout = $calculateTotalPayout["total_payout_given"];

            $totalIncome = array('puchase'=> $getPoolStats["purchases"], 'trade_fees' => $getPoolStats["trade_fees"], 'keyword_unsold_refund' => $getPoolStats["total_unsold_kwd_refund"]);
            $totalIncomeEarned = calculateTotalIncome($totalIncome);
// echo "<br><br><br>totalIncomeEarned";
// printArr($totalIncomeEarned);
// echo "totalIncomeEarned End<br><br>";
            if(noError($totalIncomeEarned)){
              $totalIncomeEarned = number_format($totalIncomeEarned["total_income_earned"], 2);
            }else{
              print("Error : Fetching total income.");
              exit;
            }
          }else{
            print("Error : Fetching total payout.");
            exit;
          }
        }else{
          print("Error : Fetching pool stats details.");
          exit;
        }
      }else{
        print("Failure : Fetching Payout Per Like details.");
        exit;
      }
    }else{
      print("Failure : Fetching total registered user.");
      exit;
    }
  }else{
    print("Failure : Unable to create database connection");
    exit;
  }

  
 ?>
<!--<div class="row  text-center">
  <div class="number-info">
    <h3 class="number-text"><?php //echo $getTotalRegisteredUser; ?></h3>
    <p>Register Users</p>
  </div>
  <div class="number-info">
    <h3 class="number-text"><?php //echo "$".formatNumberToSort($calculateTotalPayout, 2); ?></h3>
    <p>Payout Given</p>
  </div>
  <div class="number-info">
    <h3 class="number-text"><?php //echo $getKeywordSold; ?></h3>
    <p> Keyword Sold</p>
  </div>
  <div class="number-info">
    <h3 class="number-text"><?php //echo "$".formatNumberToSort($totalIncomeEarned, 2); ?></h3>
    <p> Income Earned</p>
  </div>
</div>-->


<div class="row">
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Liked</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postLikeCount,0); ?></div>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Created</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postCreateCount,0); ?></div>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Shared</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postShareCount,0); ?></div>
    </div>
  </div>
</div>
<div class="row innerT">
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Payout Earned</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($calculateTotalPayout,4)." ".ucwords($keywoDefaultCurrencyName); ?> </div>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Payout Per Like</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?php echo formatNumberToSort("{$userPerLikeCount}", 4)." ".ucwords($keywoDefaultCurrencyName); ?> </div>
    </div>
  </div>
  <div class="col-xs-4">
    <div class="row">
      <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">User Registered</div>
      <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?php echo formatNumberToSort("{$getTotalRegisteredUser}", 0); ?> </div>
    </div>
  </div>
</div>