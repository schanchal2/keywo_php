 <?php
session_start();

/*================================= Include helper files =========================================*/
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once "../../config/countryArray.php";
require_once "../../models/keywords/userCartModel.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/sessionHelper.php";
require_once "../../helpers/errorMap.php";

error_reporting(0);

 if(isset($_COOKIE["keywo_ver_1"]) && !empty($_COOKIE["keywo_ver_1"])){
     $_SESSION["email"] = $_COOKIE['keywo_ver_1'];

     $kwdDbConn = createDBConnection('dbkeywords');
     if (noError($kwdDbConn)) {
         $kwdDbConn = $kwdDbConn["connection"];
     }
     checkForSession($kwdDbConn);

     print("<script>");
     print("var t = setTimeout(\"window.location='".$rootUrl."index.php';\", 000);");
     print("</script>");

 }

 include("../layout/header.php");

?>

<main class="bg-concrete inner-8x innerT">
  <div class="prelogin-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="keywo_tagline inner-4x innerMTB">
            <h1 class="text-center text-blue keywo_tagline_heading  half innerTB" id="login_page_title">
              WORLD'S FIRST <span class="text-black">SOCIAL MEDIA PLATFORM</span> <br><span>THAT PAYS WHILE YOU</span>
            </h1>
          </div>
        </div>
      </div>

      <!--============================== CONSUMING AND UPLOADING CONTENT ==============================-->
      <div class="row top-block innerT">
        <div class="col-xs-12">
          <div id="index-Carousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators index-carousel-indicators">
            <li data-target="#index-Carousel" class="active" data-slide-to="0" >
              <div class="caro_ribbon text-center f-sz24 innerAll ">Like a Post</div>
              <div class="index-arrow-right-1"></div>
              <div class="index-arrow-right-2"></div>
            </li>
            <li data-target="#index-Carousel"  data-slide-to="1">
              <div class="caro_ribbon text-center f-sz24 innerAll ">Create a Post</div>
              <div class="index-arrow-right-1"></div>
              <div class="index-arrow-right-2"></div>
            </li>
            <li data-target="#index-Carousel"  data-slide-to="2">
              <div class="caro_ribbon text-center f-sz24 innerAll ">Share a Post</div>
            </li>
          </ol>
            <div class="carousel-inner" style="width:50%;float:left">
              <div class="item active">
                <img src="<?php echo $rootUrl; ?>images/landingPage/like.png" alt="" style="height:490px">
              </div>

              <div class="item">
                <img src="<?php echo $rootUrl; ?>images/landingPage/create.png" alt="" style="height:490px">
              </div>

              <div class="item">
                <img src="<?php echo $rootUrl; ?>images/landingPage/share.png" alt="" style="height:490px">
              </div>
            </div>
            <div class="col-xs-6 padding-right-none">
                <div class="card innerAll register-block">
                    <div class="registration">
                      <div class="row">
                        <div class="col-xs-12">
                            <div class="text-center f-sz20 text-Blue innerMT">REGISTER NOW</div>
                        </div>
                      </div>
                        <form class="innerMT" id="registerForm">
                            <div class="row  innerB">
                                  <div class="col-xs-6">
                                      <div class="form-group">
                                          <input type="text" name="firstName" id="firstName" class="checkName form-control register-form-input" placeholder="First Name">
                                          <span class="" aria-hidden="true"></span>
                                      </div>
                                  </div>
                           
                                <div class=" col-xs-6 ">
                                    <div class="form-group">
                                        <input type="text" name="lastName" id="lastName" class="checkName form-control register-form-input" placeholder="Last Name">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row innerB">
                                <div class=" col-xs-12" data-placement="right">
                                    <div class="form-group">
                                        <input type="text" name="email" id="email" class="form-control register-form-input" placeholder="Email">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row  innerB">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="text" id="accountHandle" name="accountHandle" class="form-control register-form-input" placeholder="Account Handle">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row innerB">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <input type="password" name="password" id="password" class="passwordValidation form-control register-form-input" placeholder="Password">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row innerB">
                              <div class="col-xs-12">
                                  <div class="form-group">
                                      <input type="password" name="confirmPassword" id="confirmPassword" class="passwordIdenticalValidation form-control register-form-input" placeholder="Confirm Password">
                                      <span class="" aria-hidden="true"></span>
                                  </div>
                              </div>
                            </div>
                            <div class="row  innerB">
                                <div class="col-xs-12">
                                    <div class="">
                                        <input type="text" name="referal" id="referal" class="form-control register-form-input" placeholder="Referal code - get upto 10% discount">
                                        <span class="" aria-hidden="true"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="row  half innerB">
                                <div class="col-xs-12">
                                    <div class="captcha">
                                        <div class="recaptcha">
                                            <!--<div class="g-recaptcha" data-sitekey="6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r">
                                      <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r&amp;co=aHR0cHM6Ly9zZWFyY2h0cmFkZS5jb206NDQz&amp;hl=en&amp;v=r20161109131337&amp;size=normal&amp;cb=b5irftqmaw1g" title="recaptcha widget" width="304" height="78" role="presentation" frameborder="0" scrolling="no" name="undefined">
                                      </iframe>
                                    </div>-->
                                            <div class="g-recaptcha" data-sitekey="<?php echo $captchaPublickey; ?>"></div>
                                        </div>
                                        <!--recaptcha-->
                                    </div>
                                </div>
                            </div>
                            <div class="row  innerB">
                                <div class="col-xs-12">
                                    <input class="agreeCheckbox" name="acceptTerms" id="agree" type="checkbox">
                                    <span>I confirm that I am 13 years of Age And Agree to the </span><br><a href="../about/terms_of_use.php" class="text-center"><div class="text-center">Terms And Condition.</div></a>
                                </div>
                            </div>
                            <div class="row  innerB form-title">
                                <button class="btn btn-signup" type="button" id="registerbutton" onclick="signupSubmit();" name="submit" value="Sign Up">Sign Up</button>
                                <span class="loading-icon"></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <!-- col-xs-8 -->

          <!--============================== Register Now Form ==============================-->
      </div>
      <?php include("keywo_stats.php"); ?>
    </div>
    <!-- container -->
  </div>
  <!-- prelogin-wrapper -->
</main>


<?php include("../layout/prelogin_footer.php"); ?>
<script>

      // get global root url from config.php file.
      var rootURL = '<?php echo $rootUrl; ?>';

      // get the keywo's landing page CMS slider.
      //getLandingSlider(rootURL);
      $('#registerForm').bootstrapValidator({
          message: 'This value is not valid',
          container: 'tooltip',
          feedbackIcons: {
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
          },
          excluded: [':disabled'],
          fields: {
            firstName:{
              validators: {
                      notEmpty: {
                          message: 'The First Name is required and can\'t be empty'
                      }
                  }
            },
            lastName:{
              validators: {
                      notEmpty: {
                          message: 'The Last Name is required and can\'t be empty'
                      }
                  }
            }
            // ,
            // acceptTerms:{
            //   group: '.col-xs-12',
            //   validators: {
            //         notEmpty: {
            //             message: 'You have to accept the terms and policies'
            //         }
            //     }
            // }
          }
      }).on("success.form.bv",function(){
      });

      $(document).ready(function(){
        $('#index-carousel').carousel()
        $("#form_prelogin_login_username").focus();
        $('#registerbutton').on('click',function(){

          $(".registration input").each(function(){
            if($(this).val() == "" && $(this).attr("id") != "referal"){
              $(this).parent().attr("class", "has-error has-feedback chosen-search");
              $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
            }
          });

        });
      });


</script>
