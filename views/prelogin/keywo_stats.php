<?php

require_once('../../models/user/authenticationModel.php');
require_once('../../models/landing/landingFeatureModel.php');
require_once('../../models/header/headerModel.php');
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/deviceHelper.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/cdp/cdpUtilities.php";

error_reporting(0);

// create keyword database connection
$keywordDbConn = createDBConnection("dbkeywords");
if(noError($keywordDbConn)){
    $keywordDbConn = $keywordDbConn["connection"];

    // 1. get total post like count
    $postLikeCount=getTotalLikeCount("like");
    if(noError($postLikeCount)){
        $postLikeCount = $postLikeCount["errMsg"]["total_like_count"];
        if(empty($postLikeCount)){
            $postLikeCount = 0;
        }

        // 2. get total post created count
        $postCreateCount=getTotalLikeCount("posts");
        if(noError($postCreateCount)){
            $postCreateCount=$postCreateCount["errMsg"]["all"]["total_post"];
            if(empty($postCreateCount)){
                $postCreateCount = 0;
            }

            // 3. get total post shared count
            $postShareCount=getTotalLikeCount("share");
            if(noError($postShareCount)){
                $postShareCount=$postShareCount["errMsg"]["total_post"];
                if(empty($postShareCount)){
                    $postShareCount = 0;

                }

                // 4. Get pool stats details by calling pool stats API.
                $getPoolStats = getPoolStats();
                if(noError($getPoolStats)){
                    $getPoolStats = $getPoolStats["errMsg"];

                    // 5. calculate total payout given retrieve from API response
                    $payoutGiven = array('search_earning'=> $getPoolStats["search_earning"], 'social_content_view_earnings' => $getPoolStats["social_content_view_earnings"]);
                    $calculateTotalPayout = calculateTotalPayout($payoutGiven);

                    if(noError($calculateTotalPayout)){

                        $totalPayoutGiven = $calculateTotalPayout["total_payout_given"];

                        // 6. Get current payout amount from payout.json file.
                        $currentPayout = getCurrentPayout();
                        if(noError($currentPayout)){
                            $currentPayout = $currentPayout["current_payout"];
                            $currentPayout = number_format(($currentPayout/4), 4);

                            // 7. get total registerd user
                            $getTotalRegisteredUser = getKeywoStats($keywordDbConn);
                            if(noError($getTotalRegisteredUser)){

                                $getTotalRegisteredUser = $getTotalRegisteredUser["data"][0]["active_users"];
                                if(empty($getTotalRegisteredUser)){
                                    $getTotalRegisteredUser = 0;
                                }

                            }else{
                                print('Error: Getting total registered user.');
                                exit;
                            }
                        }else{
                            print('Error: Current payout given');
                            exit;
                        }
                    }else{
                        print('Error: Calculate total payout');
                        exit;
                    }
                }else{
                    print('Error: Network stats details');
                    exit;
                }
            }else{
                print('Error: Total shared post.');
                exit;
            }
        }else{
            print('Error: Total created post.');
            exit;
        }
    }else{
        print('Error: Total liked post.');
        exit;
    }
}else{
    print('Oops! Something went wrong.');
    exit;
}

?>

<div class="row">
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Liked</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postLikeCount,0); ?></div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Created</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postCreateCount,0); ?></div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Post Shared</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($postShareCount,0); ?></div>
        </div>
    </div>
</div>
<div class="row innerT">
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Payout Earned</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?= formatNumberToSort($totalPayoutGiven,4)." ".ucwords($keywoDefaultCurrencyName); ?> </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">Payout Per Like</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?php echo formatNumberToSort("{$currentPayout}", 4)." ".ucwords($keywoDefaultCurrencyName); ?> </div>
        </div>
    </div>
    <div class="col-xs-4">
        <div class="row">
            <div class="col-xs-6 f-sz20 text-center text-white innerTB summary-box-left">User Registered</div>
            <div class="col-xs-6 f-sz20 text-center text-Blue innerTB summary-box-right "><?php echo formatNumberToSort("{$getTotalRegisteredUser}", 0); ?> </div>
        </div>
    </div>
</div>
