<?php
  // 1. total count of registed users
  // 2. total count of keyword sold
  // 3. Get pool stats details by calling pool stats API.
  // 4. calculate total payout given retrieve from API response
  // 5. calculate total income earned

  require_once('../../models/user/authenticationModel.php');
  require_once('../../models/landing/landingFeatureModel.php');

    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../helpers/deviceHelper.php";
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/coreFunctions.php";

  error_reporting(0);
  // crete database connection
  $keywordDbConn = createDBConnection("dbkeywords");

  if(noError($keywordDbConn)){
        $keywordDbConn = $keywordDbConn["connection"];

        // 1. total count of registed users
        $getTotalRegisteredUser = getKeywoStats($keywordDbConn);
// echo "<br><br><br>getTotalRegisteredUser";
// printArr($getTotalRegisteredUser);
// echo "getTotalRegisteredUser End<br><br>";
        if(noError($getTotalRegisteredUser)){
              $getTotalRegisteredUser = $getTotalRegisteredUser["data"][0]["registered_users"];
              if($getTotalRegisteredUser <= 0){
                    $getTotalRegisteredUser = 0;
              }

              // 2. total count of keyword sold
              $getKeywordSold = getFirstPurchaseKeywordSold($keywordDbConn);
// echo "<br><br><br>getKeywordSold";
// printArr($getKeywordSold);
// echo "getKeywordSold End<br><br>";
              if(noError($getKeywordSold)){
                    $getKeywordSold = $getKeywordSold["data"][0]["total_keyword_sold"];
                    if($getKeywordSold <= 0){
                          $getKeywordSold = 0;
                    }

                    // 3. Get pool stats details by calling pool stats API.
                    $getPoolStats = getPoolStats();
                  //  printArr($getPoolStats);
// echo "<br><br><br>getPoolStats";
// printArr($getPoolStats);
// echo "getPoolStats End<br><br>";
                    if(noError($getPoolStats)){
                          $getPoolStats = $getPoolStats["errMsg"];
                          //printArr($getPoolStats);

                          // 4. calculate total payout given retrieve from API response
                          $totalSearchPayout = array('search_earning'=> $getPoolStats["search_earning"]);
                          $calculateTotalPayout = calculateTotalPayout($totalSearchPayout);
// echo "<br><br><br>calculateTotalPayout";
// printArr($calculateTotalPayout);
// echo "calculateTotalPayout End<br><br>";
                          if(noError($calculateTotalPayout)){
                                $calculateTotalPayout = $calculateTotalPayout["total_payout_given"];

                                $totalIncome = array('puchase'=> $getPoolStats["purchases"], 'trade_fees' => $getPoolStats["trade_fees"], 'keyword_unsold_refund' => $getPoolStats["total_unsold_kwd_refund"]);
                                $totalIncomeEarned = calculateTotalIncome($totalIncome);
// echo "<br><br><br>totalIncomeEarned";
// printArr($totalIncomeEarned);
// echo "totalIncomeEarned End<br><br>";
                                if(noError($totalIncomeEarned)){
                                      $totalIncomeEarned = number_format($totalIncomeEarned["total_income_earned"], 2);
                                }else{
                                      print("Error : Fetching total income.");
                                      exit;
                                }
                          }else{
                                print("Error : Fetching total payout.");
                                exit;
                          }
                    }else{
                          print("Error : Fetching pool stats details.");
                          exit;
                    }
              }else{
                    print("Failure : Fetching total keyword sold details.");
                    exit;
              }
        }else{
              print("Failure : Fetching total registered user.");
              exit;
        }
  }else{
        print("Failure : Unable to create database connection");
        exit;
  }
 ?>
 <div class="row  text-center">
     <div class="number-info">
         <h3 class="number-text"><?php echo $getTotalRegisteredUser; ?></h3>
         <p>Register Users</p>
     </div>

     <div class="number-info">
         <h3 class="number-text"><?php echo "$".formatNumberToSort($calculateTotalPayout, 2); ?></h3>
         <p>Payout Given</p>
     </div>

     <div class="number-info">
       <h3 class="number-text"><?php echo $getKeywordSold; ?></h3>
       <p>	Keyword Sold</p>
     </div>

     <div class="number-info">
       <h3 class="number-text"><?php echo "$".formatNumberToSort($totalIncomeEarned, 2); ?></h3>
       <p> Income Earned</p>
     </div>
   </div>
