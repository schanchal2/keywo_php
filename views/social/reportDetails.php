<?php

session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

$acHandler     = $_POST['acHandler'];#99
$postId        = $_POST['postId'];
$created_at    = $_POST['created_at'];
$postType      = $_POST['postType'];

$flagSetForReportSet = 1;

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
    $user_id="user_id";
    $email = $_SESSION["email"];
    $getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
    if(noError($getUserInfo)){
        $getUserInfo = $getUserInfo["errMsg"];
        $posted_by = $getUserInfo["user_id"];
        $getReport = getReportInfo($posted_by,$postId,$created_at);

        $getReportList = getCategoryandLimit();
        // printArr($getReportList);
        $getReportList = $getReportList["errMsg"];

        if(noError($getReport)){
            $getCode = $getReport["errCode"];
            $getCount = $getReport["errMsg"]["report_count"];
            $flagSetForReportSet = 0;
            if($getCount==0){
              $flagSetForReportSet = 1;
            }else {
                $flagSetForReportSet = 0;
            }
        }else {
              $flagSetForReportSet = 1;
        }
  }

  $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
  $emailofSpecialUser = array();
  $user_id="user_id,account_handle";
  $getUserInfo = getUserInfo($acHandler, $walletURLIPnotification.'api/notify/v2/', $user_id);
  if(noError($getUserInfo)){
    $getUserInfo = $getUserInfo["errMsg"];
    $user_id     = $getUserInfo["user_id"];
    $accHandle   = $getUserInfo["account_handle"];
  }

    //Get Special User
//    $getSpecialUserData = getSpecialUserPost();
//    if(noError($getSpecialUserData)){
//        $getSpecialUserData = $getSpecialUserData["errMsg"];
//    }
//    foreach($getSpecialUserData as $specialUser){
//        $emailofSpecialUser[] = $specialUser['email'];
//    }

    if(!in_array($email,$emailofSpecialUser)){
        //get Folllowed User
        $getFollowed         = getFollowUser($targetDirAccountHandler);

        //check for value
        $followFlag          = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

    }else{
        $followFlag = 1;
    }

//Get Post details
  $postDetail = getPostDetail($postId, $created_at,$_SESSION['id'], $followFlag);
  if ($postDetail['errMsg'][0]['post_type'] == 'share') {
      $postCreatorId = $postDetail['errMsg'][0]["post_details"]["parent_post"]['post_id']["posted_by"];
  } else {
      $postCreatorId = $postDetail['errMsg'][0]['posted_by'];
  }
  // printArr($postCreatorId);
  //get blocked array
  $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
  $userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);
  // printArr($userIdsOfAccHandler); die;
//find in array and redirect
  if (in_array($postCreatorId,$userIdsOfAccHandler)) {
    $followFlag = '1';
    $rootForRedirection =  $rootUrl."views/social/not_found_error.php";
  	print("<script>");
  	print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
  	print("</script>");
  }else{
    $followFlag = '0';
  }

  if($flagSetForReportSet == 1){

?>

<!-- Report Modal -->
  <form id="reportForm" name="reportForm" class="" >
			<!-- Modal content-->
			<div class="report-popup">
				<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-blue">What kind of abuse you are reporting?</h4>
				</div>
				<div class="modal-body">
		<div class="radio radio-primary radio-div">
        <?php
          $count=1;
        foreach ($getReportList as $key => $value) {
          $count= $count+1; ?>
							<input class="option-select" type="radio" name="option" id="reportPostID_<?php echo $count; ?>" value="<?php echo $key ?>" onclick="ShowHideDiv()">
							<label for="reportPostID_<?php echo $count; ?>" style="display:block;">
								  <?php echo $key ?>
							</label>
          <?php } ?>

          <input class="option-select" type="radio" name="option" id="reportPostID_0" value="other" onclick="ShowHideDiv()">
          <label for="reportPostID_0" style="display:block;">
            Others(Specify any)
          </label>

          </div>
          <div id="commentText">
            <div class="">
              <input type="text" id="modalComment" autocomplete="off" required class="form-control other-input-comment innerMT half checkEmpty"/>
            </div>
          </div>

				</div>
				<div class="modal-footer border-none">
					<button type="button" class="btn btn-social" onclick = "reportUserAcc();">Report</button>
				</div>
			</div>
<form>

  <?php } else { ?>
    <!-- Modal content-->
    <div class="modal-body">This post has been already reported by you..</div>
    <div class="modal-footer">
      <input type="button" class="form-post-btn" data-dismiss = "modal" onclick="" value = "OK">
    </div>
  <?php } ?>

<?php } else {
	print("<script>");
	print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	print("</script>");
	die;
}
 ?>

<script>


$(document).ready(function() {
  var flag = '<?php echo $flagSetForReportSet ; ?>';
  if(flag != 0){
    commentText.style.display = 'none';
    $("#reportPostID_2").attr('checked', true);
  }
});

function ShowHideDiv() {
    var report_id = $(".option-select:checked").val();
    if(report_id=="other")
    {
      $("#commentText").show();
    }
  else{
      $("#commentText").hide();
  }
}

function reportUserAcc() {
    var notempty = isNotEmpty("reportForm");
    var report_text = $(".option-select:checked").val();
        console.log(report_text);
    if(report_text!="other")
    {
        console.log(report_text);
      var postId     = '<?php echo $postId; ?>';
      var created_at = '<?php echo $created_at; ?>';
      var postType   = '<?php echo $postType; ?>';
      var acHandler = '<?php echo $acHandler; ?>';

        console.log(report_text);
        $.ajax({
          url:'../../controllers/social/setReportController.php',
          type: 'POST',
          dataType:'json',
          data: ({report_id:report_id,postId:postId,report_text:report_text,created_at:created_at,postType:postType}),
          beforeSend: function() {
            $(".btn-social").prop("disabled",true);
            $(".btn-social").text("Reporting...");
          } ,
          complete: function(){
            $(".btn-social").prop("disabled",false);
            $(".btn-social").text("Report");
          },
          success: function(data){
            if (data.errCode == -1) {
                showToast("success",'You have Reported Post Successfully');
                $.ajax({
                  url:'../../controllers/social/setReportCountController.php',
                  type: 'POST',
                  dataType:'json',
                  data: ({postId:postId,created_at:created_at,postType:report_text,email:acHandler}),
                  beforeSend: function() {
                    $(".btn-social").prop("disabled",true);
                    $(".btn-social").text("Reporting...");
                  } ,
                  complete: function(){
                    $(".btn-social").prop("disabled",false);
                    $(".btn-social").text("Report");
                  },
                  success: function(data){
                  }
                });
            } else if (data.errCode == 100) {
              window.location.href = rootUrl;
            }else {
                showToast("failed",'User action Failed');
            }
            $("#showReportDetails").modal("hide");
          },
          error: function(data) {
                showToast("failed",'Error in Connection');
          }
        });
    }
    else{
    if (notempty) {
      if(isNotEmpty("reportForm") & descriptionReportBoxCheck("reportForm")){
        var report_id = $(".option-select:checked").val();
        var  report_text= $("#reportForm #modalComment").val();
        var postId = '<?php echo $postId; ?>';
        var created_at = '<?php echo $created_at; ?>';
        var postType = '<?php echo $postType; ?>';
          $.ajax({
            url:'../../controllers/social/setReportController.php',
            type: 'POST',
            dataType:'json',
            data: ({report_id:report_id,postId:postId,report_text:report_text,created_at:created_at,postType:postType}),
            beforeSend: function() {
              $(".btn-social").prop("disabled",true);
              $(".btn-social").text("Reporting...");
            } ,
            complete: function(){
              $(".btn-social").prop("disabled",false);
              $(".btn-social").text("Report");
            },
            success: function(data){
              if (data.errCode == -1) {
                  showToast("success",'You have Reported Post Successfully');
              } else {
                  showToast("failed",'User action Failed');
              }
              $("#showReportDetails").modal("hide");
            },
            error: function(data) {
                  showToast("failed",'Error in Connection');
            }
          });
      }
    }
  }
}
</script>
