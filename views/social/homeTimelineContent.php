
<?php

session_start();

/* Add DB Management Files */
require_once '../../config/config.php';
require_once '../../config/db_config.php';
require_once '../../models/social/commonFunction.php';
require_once '../../models/social/socialModel.php';
require_once('../../models/wallet/walletKYCModel.php');
require_once '../../helpers/arrayHelper.php';
require_once '../../helpers/errorMap.php';
require_once '../../helpers/coreFunctions.php';
require_once '../../helpers/imageFunctions.php';
require_once '../../helpers/stringHelper.php';
require_once '../../helpers/deviceHelper.php';
require_once '../../IPBlocker/ipblocker.php';
require_once '../../IPBlocker/showCaptcha.php';

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
  //session is not active, redirect to login page
  print("<script>");
  print("var t = setTimeout(\"window.location='" . $rootUrl . "';\", 000);");
  print("</script>");
  die;
}

$targetDirAccountHandler = "{$docRoot}json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/";

/* Search database connection */
$searchDbConn = createDBConnection('dbsearch');
if (noError($searchDbConn)) {
  $searchDbConn = $searchDbConn["connection"];
  $msg = "Success : Search database connection";
  $returnArr=array();
  $extraArgs=array();
  $emailofSpecialUser = array();
  $returnArr = setErrorStack($returnArr, -1, $msg, $extraArgs);
} else {
  $msg = 'Error : Search database connection';
  $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);
}

$type = $_GET['type']; //print_r($type); die;
$timelineData = json_decode($_SESSION["userPost"]);

$userId = $_SESSION['id'];
$data = $timelineData->$type;
$valueUI = $_GET['n'];
$checkScrollStatus = 'true';
// print_r($data);die;
//get nth value from UI
if (empty($valueUI)) {
  $i = $j = 0;
} else {
  $i = $j = $valueUI;
}

$hideAction = "hide";
$userPendingInteraction = "no_of_qualified_interactions_pending,kyc_current_level,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic";
$getSearchType = getSearchType($_SESSION['email'], $userPendingInteraction);
if (noError($getSearchType)) {
  $getSearchType = $getSearchType["data"];
  $userSearchType = $getSearchType["user_search_type"];
  $profilePic = $getSearchType["profile_pic"];
} else {
  print('Error: Fetching user interation type');
  exit;
}

$s = $i + 10;
$n = 0;
$postDisplayIdsArray = array();
$bulkData = array();
$bulkPostId = "";
$counter = 0;

for ($j; $j < count($data) && $j < $s; $j++) {
  $postIddata = array(
    'post_id' => $data[$j]->_id,
    'time' => $data[$j]->created_at,
  );

  array_push($bulkData, $postIddata);
  $bulkPostId = json_encode($bulkData);
}

// Get like post activity count from node database

$activityCount = getPostActivityCount($bulkPostId, '');


for ($i; $i < count($data) && $i < $s; $i++) {
    $post_earnings = $data[$i]->post_earnings;
    $postedBy      = $data[$i]->posted_by;
    $postId        = $data[$i]->_id ;
    $createdAt     = $data[$i]->created_at;
    $postType      = $data[$i]->post_type;
    $emailId       = $data[$i]->user_ref->email;
  //Check Like post details
  $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $data[$i]->post_type);
  //check cdp status
  $getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);
  //post keywords
  $postDisplayIds = $postId . ',';
  $keywords = $data[$i]->keywords;
  $keywords = implode(' ', $keywords);
  //post time
    $mil = $data[$i]->created_at;
    $seconds = $mil / 1000;
    $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
    $timestamp2 = strtotime($created_at);
    if (isset($data[$i]->post_collection)) {
        $postCollectionName = $data[$i]->post_collection;
    }

  //get user ip address
  $ipAddr = getClientIP();

    //check request for blocking IP address.
    $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    $extraArgs = array();
    $searchCount = '';
  // reserved for social

  $targetActivityCount = $activityCount['errMsg'][$postId];
  //printArr($targetActivityCount);
  $like_count = $targetActivityCount['like_count'];
  $commentCount = $targetActivityCount['comment_count'];
  $shareCount = $targetActivityCount['share_count'];


  if(isset($targetActivityCount['parent_like_count']))
  {
      $parentLikeCount = $targetActivityCount['parent_like_count'];
   }else
   {
     $parentLikeCount = "";
   }


  if(isset($targetActivityCount['parent_comment_count']))
  {
      $parentCommentCount = $targetActivityCount['parent_comment_count'];
   }else
   {
     $parentCommentCount = "";
   }
  if(isset($targetActivityCount['parent_share_count']))
  {
     $parentShareCount = $targetActivityCount['parent_share_count'];
   }else
   {
     $parentShareCount = "";
   }
 

  //get post short description trimmed according to "$validateCharCountOnSocial" in config file
  //get short description with mention links
  if(isset($data[$i]->post_mention)){
      $postMention = $data[$i]->post_mention;
  }else{
      $postMention = " ";
  }
  $shortDescriptionText    = getLinksOnText(rawurldecode($data[$i]->post_short_desc), $postMention, $_SESSION["account_handle"]);
  $shortDescription        = $shortDescriptionText["text"];
  $descLenWithoutStriptag  = strlen($shortDescription);
  $descLenWithStriptag     = strlen(strip_tags($shortDescription));
  $diff                    = $descLenWithoutStriptag - $descLenWithStriptag;
  $shortDescData           = ($descLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $diff) . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $shortDescription;

  $postSharedType = $data[$i]->post_type;
  //Check             Bookmark post details
  $getBookMarkPost = getBookMarkPost($_SESSION["account_handle"], $postId, $postSharedType);

  ?>
    <input type="hidden" id="post_collection<?php echo $postId; ?>" value="<?php echo $postCollectionName; ?>">
    <!--check post is shared post or not. if no shared post then-->

    <?php
    global $cdnSocialUrl;
    global $rootUrlImages;

    if(isset($data[$i]->user_ref->profile_pic) && !empty($data[$i]->user_ref->profile_pic)){
        $extensionUP  = pathinfo($data[$i]->user_ref->profile_pic, PATHINFO_EXTENSION);
        //CDN image path
        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/profile/' . $data[$i]->user_ref->account_handle . '_' . $data[$i]->user_ref->profile_pic . '.40x40.' . $extensionUP;

        //server image path
        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data[$i]->user_ref->account_handle.'/profile/'.$data[$i]->user_ref->account_handle.'_'.$data[$i]->user_ref->profile_pic;

        // check for image is available on CDN
        $file = $imageFileOfCDNUP;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $imgSrc = $imageFileOfLocalUP;
        } else {
            $imgSrc = $imageFileOfCDNUP;
        }
    }else{
        $imgSrc = $rootUrlImages."default_profile.jpg";
    }
    if ($data[$i]->post_type !== "share") {
    ?>
    <div class="social-timeline-image innerMB inner-2x">
        <div class="social-card card innerLR  innerT innerMT" id="<?php echo $data[$i]->_id; ?>">
            <div class="social-card-created-at" id="<?php echo $data[$i]->created_at; ?>">
                <div class="row">
                    <div class="col-xs-8">
                        <div class="social-timeline-profile-pic pull-left">
                            <img src="<?php echo $imgSrc; ?>" class=" img-responsive" />
                        </div>
                        <div class="social-timeline-profile-details pull-left innerL">
                            <div class="social-user-link-name">
                                <?php if ($_SESSION['id'] == $postedBy) {?>
                                <a href="<?php echo $rootUrl; ?>views/social/privateTimeline.php">
                                    <?php } else {?>
                                    <a href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($data[$i]->user_ref->email); ?>" target="_blank">
                                        <?php }?>
                                        <?php echo $data[$i]->user_ref->first_name; ?>
                                        <?php echo $data[$i]->user_ref->last_name; ?>
                                    </a>
                            </div>
                            <!---->
                            <div class="social-user-link">
                              <?php if ($_SESSION['id'] == $postedBy) {?>
                              <a href="<?php echo $rootUrl; ?>views/social/privateTimeline.php">
                                  <?php } else {?>
                                  <a href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($data[$i]->user_ref->email); ?>" target="_blank">
                                      <?php }?>
                                    <?php echo '@' . $data[$i]->user_ref->account_handle; ?>
                                </a>
                            </div>
                            <!--social-user-link-->
                        </div>
                        <!--social-timeline-profile-details-->
                    </div>
                    <!--post right corner action-->
                    <div class="col-xs-4 social-post-options">
                        <?php    include('commonDropDownOption.php');?>
                    </div>
                    <!--end of post action-->
                </div>
            </div>
            <!--social-timeline-details-->
            <?php if (isset($data[$i]->keywords[0]) && !empty($data[$i]->keywords[0])) {
      ?>
            <div class="social-timeline-keywords-details">
                <div class="row half innerT">
                    <div class="col-xs-12">
                        <?php
foreach ($data[$i]->keywords as $keyword) {
        $keywos = "<span>#" . $keyword . "</span> ";
        ?>
                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keyword); ?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword; ?>">
                                <?php echo $keywos; ?>
                            </a>
                            <?php }?>
                    </div>
                </div>
            </div>
            <?php }?>
            <!--social-timeline-keywords-details-->
            <!-- switch case-->
            <?php
$value = $data[$i]->post_type;
    switch ($value) {
    case 'video':
      ?>
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <?php $originUrl = $rootUrl . 'views/social/' . basename($_SERVER['SCRIPT_NAME']);?>
                    <!--social-timeline-video-->
                    <div class="social-timeline-content-video">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                if(isset($data[$i]->post_details->video_thumbnail) && !empty($data[$i]->post_details->video_thumbnail)){

                                    $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/post/video/' . $data[$i]->post_details->video_thumbnail;

                                    //server image path
                                    $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data[$i]->user_ref->account_handle.'/post/video/'.$data[$i]->post_details->video_thumbnail;

                                    // check for image is available on CDN
                                    $file = $imageFileOfCDNUP;
                                    $file_headers = @get_headers($file);
                                    if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                        $imgSrc = $imageFileOfLocalUP;
                                    } else {
                                        $imgSrc = $imageFileOfCDNUP;
                                    }
                                }
                                if(isset($data[$i]->post_details->video_thumbnail)){
                                ?>
                                    <div align="center" class="video-div embed-responsive embed-responsive-16by9" style="background: url('<?php echo $imgSrc;; ?>') no-repeat; background-size: cover !important;">
                                        <?php
                                        if (strpos($data[$i]->post_details->video_thumbnail, 'youtube') !== false) {
                                        ?>
                                            <img class="play-img img-responsive" src="<?php echo $rootUrlImages . 'y3.png';?>">
                                        <?php
                                          } else {
                                        ?>
                                            <img class="play-img-vimeo play-img img-responsive" src="<?php echo $rootUrlImages . 'v3.png';?>">
                                        <?php
                                          }
                                          ?>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-video-->
                <?php break;
    case 'audio':
      ?>
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <!--social-timeline-audio-->
                    <div class="social-timeline-sound">
                        <div class="row">
                            <div class="col-xs-12 modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                                <?php if (strpos($data[$i]->post_details->asset_url, 'https://soundcloud.com/') !== false) { ?>
                                    <a href="<?php echo $data[$i]->post_details->asset_url; ?>" target="_blank"><?php echo $data[$i]->post_details->asset_url; ?></a>
                                <?php } elseif (strpos($data[$i]->post_details->asset_url, 'https://w.soundcloud.com/') !== false) { ?>
                                    <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $data[$i]->post_details->asset_url; ?>">
                                    </iframe>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-audio-->
                <?php break;
    case 'blog':
      ?>
      <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">

      <!--social-timeline-content-blog-->
      <div class="row">
        <div class="col-xs-12">
          <div class="social-timeline-content-blog-nothumb half innerMT">
            <?php $extension  = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);
              //CDN image path
              $imageFileOfCDN = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/post/blog/featured/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

              //server image path
              $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data[$i]->user_ref->account_handle . '/post/blog/featured/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

              // check for image is avilable on CDN
              $file = $imageFileOfCDN;
              $file_headers = @get_headers($file);
              if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $finalImagePath = $imageFileOfLocal;
              } else {
                $finalImagePath = $imageFileOfCDN;
              }

              if (isset($data[$i]->post_details->img_file_name) && !empty($data[$i]->post_details->img_file_name)) { ?>
                <!--social-timeline-blog-image-->
                <div class="row">
                  <div class="col-xs-12">
                    <div class="blog-nothumb-content">
                      <a>
                      <span class="blog-nothumb-head">
                        <?php echo $data[$i]->post_details->blog_title; ?>
                      </span>
                      </a>
                      <div class="social-timeline-content-blog-thumb">
                        <div class="row">
                          <div class="col-xs-12 blog-display-img">
                            <img src="<?php echo $finalImagePath ?>" class="img-responsive modal--show--post" data-id="<?php echo $data[$i]->_id; ?>" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>" />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!--social-timeline-blog-image-->
              <?php } else {        ?>
                <div class="row half innerTB border-top border-bottom">
                  <div class="col-xs-12">
                    <div class="blog-nothumb-content">
                      <a>
                      <span class="blog-nothumb-head">
                        <?php echo $data[$i]->post_details->blog_title; ?>
                      </span>
                      </a>
                      <span class="blog-nothumb-body modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                      <?php //printArr($data[$i]->post_details->blog_content);
                      $blogDescriptionText = getLinksOnText(rawurldecode($data[$i]->post_details->blog_content), $postMention, $_SESSION["account_handle"]);

                      $blogDescription             = $blogDescriptionText["text"];
                      $descBlogLenWithoutStriptag  = strlen($blogDescription);
                      $descBlogLenWithStriptag     = strlen(strip_tags($blogDescription));
                      $diffBlog                    = $descBlogLenWithoutStriptag - $descBlogLenWithStriptag;
                      $blogContent                 = ($descBlogLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($blogDescription, 0, $validateCharCountOnSocial + $diffBlog) 
                      . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $blogDescription;
                      echo $blogContent;?>
                      </span>
                    </div>
                  </div>
                </div>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
      <!--social-timeline-content-blog-->
      <?php break;
        case 'image':
      ?>
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <!--social-timeline-image-->
                    <?php
                    $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);

                    //CDN image path
                    $imageFileOfCDN = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/post/images/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

                    //server image path
                    $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data[$i]->user_ref->account_handle . '/post/images/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

                    // check for image is avilable on CDN
                    $file = $imageFileOfCDN;
                    $file_headers = @get_headers($file);
                    if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $finalImagePath = $imageFileOfLocal;
                    }
                    else {
                        $finalImagePath = $imageFileOfCDN;
                    }

                    ?>
                    <div class="social-timeline-content-image">
                        <div class="row">
                            <div class="col-xs-12 post-display-img">
                                <img src="<?php echo $finalImagePath; ?>" class="img-responsive" />
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-image-->
                <?php break;
    case 'status':
      ?>
                <!--social-timeline-status-->
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <div class="social-timeline-user-status">
                        <div class="row border-top border-bottom">
                            <div class="col-xs-12">
                                <div class="user-status-content half innerTB">
                                    <span class="text-dark-gray">
                <?php echo $shortDescData; ?>
              </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-status-->
                <?php break;
    }
    ?>
                <!--end of switch case-->
                <!--post floor-->
                <div class="social-timeline-earning-comments-view innerT">
                    <div class="row">
                        
                        <div class="col-xs-5">
                            <div>
                                <label>Earning : </label>
                                <span>
                                <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                      origPrice="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </span>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            
                                <div class="">
                                    <ul class="list-inline pull-right margin-bottom-none">
                                    <li>
                                      <div>
                                      <?php 


                                      if (!empty($like_count)) { ?>
                                        <a id="like-count<?php echo $data[$i]->_id; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30' ,'<?php echo $like_count; ?>');">
                                          
                                            <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span>
                                        </a>
                                        <?php } else {
                                          ?>
                                        <a id="like-count<?php echo $data[$i]->_id; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30','<?php echo $like_count;?>');">
                                        <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                        <?php } ?> 
                                        <label class="pull-right">Likes</label>
                                        </div>
                                      </li>
                                        <li>
                                            <div>
                                                <span class="half innerR comment-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$commentCount}", 0); ?></span>
                                                <label class="pull-right">Comments</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div>
                                                <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0); ?></span>
                                                <label>Shares</label>
                                            </div>
                                        </li>
                                        <!-- <li class="padding-right-none">
                                            <div>
                                                <span class=""><?php $viewCount = $data[$i]->post_views_count;
    //echo formatNumberToSort("{$viewCount}", 0);?></span>
                                                <label>Views</label>
                                            </div>
                                        </li> -->
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-earning-comments-view-->
                    <!--social-timeline-likes-dislikes-->
                    <div class="social-timeline-likes-dislikes">
                        <div class="row">
                        <div class="col-xs-6">
                            <div class="social-likes-dislikes">
                                <ul class="list-inline margin-bottom-none">
                                    <li>
                                    <div>
                                        <a href="JavaScript:void(0);" class="like-area">
                                        <?php
                                            if($getPostCdpStatus == 1) {?>
                                              <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } elseif ($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } else {
                                            if ($userId != $postedBy) {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-thumbs-o-up half innerR" onclick="socialCDPForPost(this,'<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '<?php echo $data[$i]->post_type; ?>', '<?php echo $data[$i]->user_ref->account_handle; ?>', '<?php echo $postedBy; ?>', '', '<?php echo $searchCount; ?>', '<?php echo $ipAddr; ?>', '<?php echo $keywords; ?>','<?php echo $data[$i]->user_ref->email; ?>');"></i>
                                            <?php } else {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php }
                                          }?>
                                        </a>
                                    </div>
                                    </li>
                                </ul>
                            </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="social-comments-shares-views">
                                    <ul class="list-inline pull-right margin-bottom-none">
                                        <li>
                                            <div>
                                                <a onclick="commentDivShow('<?php echo $data[$i]->_id; ?>');">
                                                    <i class="fa fa-comments-o"></i>
                                                    <span>Comments</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="padding-right-none">
                                            <div>
                                                <a onclick="sharePostModal('<?php echo $data[$i]->_id; ?>','<?php echo $data[$i]->created_at; ?>','<?php echo $data[$i]->post_type; ?>','<?php echo $data[$i]->user_ref->account_handle; ?>', 'create','<?php echo $data[$i]->user_ref->email; ?>');">
                                                    <i class="fa fa-share-square-o"></i>
                                                    <span>Share</span>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="comment-hidden-enter-event-<?php echo $data[$i]->_id; ?>" execute-enetr-event="true" execute-enter-event-main-page="true">
                    <div id="comment-include-<?php echo $data[$i]->_id; ?>" class="comment-append-section comments-module bg-light-purple row" style="display:none;">
                        <div class="col-xs-12">
                            <?php
$commentPostId = $data[$i]->_id;
    $commentPostCreationTime = $data[$i]->created_at;
    $commentPostCreator = $data[$i]->user_ref->email;
    include 'postPageCommentWidget.php';
    ?>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--end of post floor-->
    </div>
    <!--social-card-->
    </div>
    <!--if shared post then-->
    <?php } else {?>
    <?php include "sharePostContent.php";?>
    <!--if closing-->
    <?php }
} //for closing

if ($i == count($data) - 1 || $i == count($data)) {
  $checkScrollStatus = 'false';
}
?>
    <div id="checkScrollStatus-<?php echo $valueUI; ?>" data-scroll-allow="<?php echo $checkScrollStatus; ?>"></div>

    <?php if ($checkScrollStatus == "false") {?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">Follow More User To See More Post</h4>
    </div>
    <?php }?>
    <!-- Report Modal -->

    <!--end-remove-post-->

    <script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip();

    var removePostId, removePostType, removePostCreatedAt;

    function removePost(postId, postType, postCreatedAt) {
        $('#showModalPost .close-dialog').click();
        $("#removePostModal").modal("show");
        removePostId = postId;
        removePostType = postType;
        removePostCreatedAt = postCreatedAt;
    }
    $(".yes-remove-post-btn").click(function() {
        removePersonalPost(removePostId, removePostType, removePostCreatedAt);
        $('#removePostModal').modal('hide');
    });
    $('.modal--show--post').on('click', function(event) {
      // event.preventDefault();
      /**
       * to ignore modal popup on child "a" tag of a container for which we want modal popup.
       */
      if (event.target.nodeName != "A") {

      var postid, createdat, searchcount, ipaddr, emailid, usersearchtype;
      postid         = $(this).data("postid");
      createdat      = $(this).data("createdat");
      searchcount    = $(this).data("searchcount");
      ipaddr         = $(this).data("ipaddr");
      emailid        = $(this).data("emailid");
      usersearchtype = $(this).data("usersearchtype");
      showModalPost(postid, createdat, searchcount, ipaddr, emailid, usersearchtype) ;}
    });
    </script>
    <?php
$postDisplayIdsArray = json_encode($postDisplayIdsArray); // printArr($postDisplayIdsArray);
// $postDisplayIds      = rtrim($postDisplayIds,',');
$updatePostDisplayCount = updatePostDisplayCount($postDisplayIdsArray);
// print_r($updatePostDisplayCount);
?>
        <!--====  End of include : homeTimelineContent.php  ====-->
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>