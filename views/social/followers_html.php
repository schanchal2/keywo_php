<!--========================================================================================================================================
=            "follower" page and "following" page will be same as below, only "follow" button wont be there in "following" page            =
=========================================================================================================================================-->
<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
    <div class="container">
        <div class="col-xs-3">
            <div class="social-left-panel left-panel-modules">
                <div class="card my-info-card social-card">
                    <div class="clearfix innerAll">
                        <div class="my-info-img pull-left innerMR">
                            <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG" />
                        </div>
                        <!-- my-picture  -->
                        <div class="my-info-detail pull-left">
                            <div class="my-info-name-container text-blue innerMB">
                                <span class="my-info-name"> Palak Hazare</span>
                                <span class="my-info-name-edit pull-right">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                    </span>
                            </div>
                            <!-- my-name  -->
                            <div class="my-info-status">
                                MP from Tiruvampuram. Author of 15 books. Formar minister of state governanace of irctc co in
                            </div>
                            <!-- my-status  -->
                        </div>
                    </div>
                    <!-- my-info-detail  -->
                    <div class="clearfix">
                        <div class="col-xs-12 text-center">
                            <div class="col-xs-4 padding-none pull-left">
                                <div class="text-deep-sky-blue my-info-posts">
                                    Posts
                                </div>
                                <div class="text-blue">
                                    3
                                </div>
                            </div>
                            <div class="col-xs-4 padding-none">
                                <div class="text-deep-sky-blue my-info-following">
                                    Following
                                </div>
                                <div class="text-blue">
                                    37
                                </div>
                            </div>
                            <div class="col-xs-4 padding-none pull-right">
                                <div class="text-deep-sky-blue my-info-followers">
                                    Followers
                                </div>
                                <div class="text-blue">
                                    31
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- posts-followers-following  -->
                </div>
                <!-- my-info-card  -->
                <div class="suggested-card">
                    <div class="card social-card right-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4>Suggested</h4>
                                </div>
                            </div>
                        </div>
                        <div class="suggested-module">
                            <div class="row margin-none innerTB border-bottom">
                                <div class="col-xs-3">
                                    <img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png">
                                </div>
                                <div class="padding-left-none col-xs-6">
                                    <div class="social-user-link-name">
                                        <a>shashi tharoor</a>
                                    </div>
                                    <div class="social-user-link">
                                        <a href="#">@shahitharoor</a>
                                    </div>
                                </div>
                                <div class=" col-xs-3">
                                    <a href="" class="half innerR"><i class="fa fa-check"></i></a>
                                    <a href=""><i class="fa fa-close"></i></a>
                                </div>
                            </div>
                            <div class="row margin-none innerTB border-bottom ">
                                <div class="col-xs-3">
                                    <img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png">
                                </div>
                                <div class="padding-left-none col-xs-6">
                                    <div class="social-user-link-name">
                                        <a>shashi tharoor</a>
                                    </div>
                                    <div class="social-user-link">
                                        <a href="#">@shahitharoor</a>
                                    </div>
                                </div>
                                <div class="col-xs-3">
                                    <a href="" class="half innerR"><i class="fa fa-check"></i></a>
                                    <a href=""><i class="fa fa-close"></i></a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-12">
                                    <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--suggested-module-->
                <div class="followed-keywords-module inner-2x innerMTB">
                    <div class="card social-card right-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4>Followed Keywords</h4>
                                </div>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT padding-left-none text-right">
                                <span>15000</span>
                                <a class="half innerL"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT padding-left-none text-right">
                                <span>15000</span>
                                <a class="half innerL"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-12">
                                <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                            </div>
                        </div>
                    </div>
                    <!-- card -->
                </div>
                <!--followed-keywords-module-->
            </div>
            <!-- social-left-panel  -->
        </div>
        <!-- col-xs-3 -->
        <div class="col-xs-6">
            <div class="social-center-panel half innerLR">
                <!-- =================== social-all-status-tabs ================== -->
                <div class="social-all-status-tabs">
                    <div class="row followers-unfollowers">
                        <div class="col-xs-3">
                            <a href="#" class="active">FOLLOWING</a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" class="">FOLLOWERS</a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" class="">SUGGESTED</a>
                        </div>
                        <div class="col-xs-3">
                            <a href="#" class="">BLOCKED</a>
                        </div>
                    </div>
                </div>
                <!--social-all-status-tabs-->
                <div class="row">
                    <div class="follower-container">
                        <div class="h4">A</div>
                        <ul class="list-unstyled social-card">
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social-dark pull-right" value="Unfollow" />
                                </div>
                            </li>
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social pull-right" value="Follow" />
                                </div>
                            </li>
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social-dark pull-right" value="Unfollow" />
                                </div>
                            </li>
                        </ul>
                        <div class="h4">B</div>
                        <ul class="list-unstyled social-card">
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social pull-right" value="Follow" />
                                </div>
                            </li>
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social pull-right" value="Follow" />
                                </div>
                            </li>
                            <li class="innerTB clearfix">
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <img class="img-circle" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                                    <a class="follower-name" href="#">asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <a class="follower-id" href="#">@asdasdasd456</a>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                                    <input type="button" class="btn-social pull-right" value="Follow" />
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- social-center-panel  -->
        </div>
        <!-- col-xs-6 -->
        <div class="col-xs-3">
            <div class="social-right-panel">
                <div class="row innerMB inner-2x">
                    <div class="col-xs-12">
                        <div class="pull-left half search-box-comments" style="width:100%;">
                            <div class="input-group" style="width:100%;">
                                <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="" style="width:100%;">
                                <input type="submit" class="social-search-btn-header" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card social-card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-5 padding-right-none">
                                <h4>Trending</h4>
                            </div>
                            <div class="col-xs-7 padding-left-none">
                                <div class="trending-content">
                                    <ul class="list-inline innerMT text-right">
                                        <li>
                                            <a class="active" href="#">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#">
                                                <i class="fa fa-play-circle"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#">
                                                <i class="fa fa-image"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#">
                                                <i class="fa fa-soundcloud"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Follow keyword Module-->
                    <div class="follow-keyword-module">
                        <div class="row active margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT">
                                <a class="half innerL pull-right">Follow</a>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT">
                                <a class="half innerL pull-right">Follow</a>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT">
                                <a class="half innerL pull-right">Follow</a>
                            </div>
                        </div>
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-8">
                                <div class="followed-keywords-name">
                                    <a>#hotel</a>
                                </div>
                                <div>
                                    <label>Earning : </label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                            </div>
                            <div class="col-xs-4 innerT">
                                <a class="half innerL pull-right">Follow</a>
                            </div>
                        </div>
                    </div>
                    <!--Follow keyword Module-->
                </div>
                <div class="popular-blog-module">
                    <div class="card social-card right-panel-modules inner-2x innerMTB">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-5 padding-right-none">
                                    <h4>Popular</h4>
                                </div>
                                <div class="col-xs-7 padding-left-none">
                                    <div class="trending-content">
                                        <ul class="list-inline innerMT text-right">
                                            <li>
                                                <a class="active" href="#">
                                                    <i class="fa fa-file-text"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-play-circle"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-image"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-soundcloud"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--bg-light-gray-->
                        <div class="row margin-none blog-module border-bottom">
                            <a>
                                <div class="row margin-none innerT">
                                    <div class="col-xs-4">
                                        <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png">
                                    </div>
                                    <div class="col-xs-8">
                                        <span class="ellipsis-multiline text-black">
                  DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
                </span>
                                    </div>
                                </div>
                            </a>
                            <div class="row margin-none innerT">
                                <a>
                                    <div class="col-xs-7 text-black">
                                        <label>Earning :</label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </a>
                                <div class="col-xs-5">
                                    <a>
                                    </a><a class="text-social-primary pull-right">157k Views</a>
                                </div>
                            </div>
                        </div>
                        <!--row-->
                        <div class="row margin-none blog-module">
                            <div class="row margin-none innerT">
                                <div class="col-xs-4">
                                    <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png">
                                </div>
                                <div class="col-xs-8">
                                    <span>
                  Description
                </span>
                                </div>
                            </div>
                            <div class="row margin-none innerT border-bottom">
                                <div class="col-xs-7">
                                    <label>Earning :</label>
                                    <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                </div>
                                <div class="col-xs-5">
                                    <a class="text-social-primary pull-right">157k Views</a>
                                </div>
                            </div>
                        </div>
                        <!--row-->
                        <div class="row margin-none half innerTB border-bottom">
                            <div class="col-xs-12">
                                <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--popular blog module-->
            </div>
            <!-- social-left-panel  -->
        </div>
        <!-- col-xs-3 -->
    </div>
    <!-- container -->
</main>
<?php include("../layout/transparent_footer.php");?>
