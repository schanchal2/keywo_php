<?php
	session_start();

  /*Include required files*/
	require_once('../../config/config.php');
	require_once('../../config/db_config.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/arrayHelper.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../models/social/socialModel.php');
	require_once('../../models/social/commonFunction.php');

    global $rootUrl;

    $postId                  = cleanXSS(trim(urldecode($_POST['postId'])));
    $postTime                = cleanXSS(trim(urldecode($_POST['postTime'])));
    $postMethod              = cleanXSS(trim(urldecode($_POST['postMethod'])));
    $email                   = cleanXSS(trim(urldecode($_POST['email'])));
    $emailofSpecialUser      = array();
    $keywoKeywords           = "";
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $user_id                 = "user_id,account_handle";
    $getUserInfo             = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
    if(noError($getUserInfo)){
        $getUserInfo = $getUserInfo["errMsg"];
        $user_id     = $getUserInfo["user_id"];
        $accHandle   = $getUserInfo["account_handle"];
    }

    //Get Special User
    //$getSpecialUserData = getSpecialUserPost();
    //if(noError($getSpecialUserData)){
    //    $getSpecialUserData = $getSpecialUserData["errMsg"];
    //}
    //foreach($getSpecialUserData as $specialUser){
    //    $emailofSpecialUser[] = $specialUser['email'];
    //}

    if(!in_array($email,$emailofSpecialUser)){
        //get Folllowed User
        $getFollowed = getFollowUser($targetDirAccountHandler);

        //check for value
        $followFlag  = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

    }else{
        $followFlag = 1;
    }

    if (!empty($postId)) {
        $postDetail = getPostDetail($postId, $postTime, $_SESSION['id'], $followFlag);

        if (noError($postDetail)) {
            $postDetail = $postDetail['errMsg'][0];
        } elseif ($postDetail['errCode'] == 4) { ?>
            <div class="modal-body">This Post No longer Exist</div>
            <div class="modal-footer"><input type="button" class="form-post-btn" data-dismiss = "modal" onclick="" value = "OK"></div>
            <script>
            if (socialFileUrl == "social/index") {
                $("#home-next-post-data").attr('data-count', '0');
                $('#home-next-post-data').attr('data-post-type', 'audio');
                $('#home-next-post-data').attr('data-scroll-allow', 'true');
                $('#home-next-post-data').attr('data-status-empty', '');
                loadHomePostTimeline(lastPostTime, "all", searchKeywo, searchText);
            } else if (socialFileUrl == "social/privateTimeline") {
                loadPostTimeline("all");
            }  else if ( socialFileUrl == 'social/socialPostDetails') {
                location.reload();
            }
            </script>
        <?php	die;
        } else {
            exit();
        }
    }

    $userId   = $_SESSION['id'];
    $postType = $postDetail['post_type'];

	if ($postType !== "share") {
		$postShareCreatedAt = $postDetail["created_at"];
		$postKeyword        = $postDetail["keywords"][0];
		$postKeywords       = $postDetail["keywords"];
		$mil                = $postDetail['created_at'];
		$postShortDesc      = $postDetail['post_short_desc'];
        if(isset($postDetail['post_details'])) {
            if(isset($postDetail['post_details']['asset_url'])){
                $postAssetUrl = $postDetail['post_details']["asset_url"];
            }else{
                $postAssetUrl = "";
            }

            if(isset($postDetail['post_details']['blog_content'])){
                $content = $postDetail['post_details']['blog_content'];
            }else{
                $content = "";
            }

            if(isset($postDetail['post_details']['asset_url'])){
                $postAssetUrl = $postDetail['post_details']["asset_url"];
            }else{
                $postAssetUrl = "";
            }

            if(isset($postDetail['post_details']['img_file_name'])){
                $postImgFileName = $postDetail["post_details"]["img_file_name"];
            }else{
                $postImgFileName = "";
            }

            if(isset($postDetail['post_details']['blog_title'])){
                $postParentBlogTitle = $postDetail['post_details']['blog_title'];
            }else{
                $postParentBlogTitle = "";
            }

            $postCollectionName = "";

        } else {
            $postAssetUrl        = "";
            $content             = "";
            $postAssetUrl        = "";
            $postImgFileName     = "";
            $postParentBlogTitle = "";
            $postCollectionName  = "";
        }

    $postAccHandle  = $postDetail["user_ref"]["account_handle"];
    $postParentId   = $postDetail["_id"];
    $postCreatedAt  = $postDetail["created_at"];
    $postParentType = $postType;

	} elseif ($postType == "share") {
		$postShareCreatedAt  = $postDetail["created_at"];
		$postKeyword         = $postDetail["post_details"]["parent_post"]["post_id"]["keywords"][0];
		$postKeywords        = $postDetail["post_details"]["parent_post"]["post_id"]["keywords"];
		$mil                 = $postDetail["post_details"]["parent_post"]["post_id"]['created_at'];
		$postShortDesc       = $postDetail["post_details"]["parent_post"]["post_id"]["post_short_desc"];
        if(isset($postDetail["post_details"]["parent_post"]["post_id"]['post_details']["asset_url"])) {
		    $postAssetUrl = $postDetail["post_details"]["parent_post"]["post_id"]['post_details']["asset_url"];
        }else{
            $postAssetUrl = "";
        }

        if(isset($postDetail["post_details"]["parent_post"]["post_id"]["post_details"]['blog_content'])) {
            $content = ($postDetail["post_details"]["parent_post"]["post_id"]["post_details"]['blog_content']);
        }else{
            $content = "";
        }

        if(isset($postDetail["post_details"]["parent_post"]["post_id"]['post_details']["asset_url"])) {
            $postAssetUrl = $postDetail["post_details"]["parent_post"]["post_id"]['post_details']["asset_url"];
        }else{
            $postAssetUrl = "";
        }

		
		$postAccHandle = $postDetail["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"];
		$postParentId  = $postDetail["post_details"]["parent_post"]["post_id"]["_id"];
		$postCreatedAt = $postDetail["post_details"]["parent_post"]["post_id"]["created_at"];

        if(isset($postDetail["post_details"]["parent_post"]["post_id"]["post_details"]["img_file_name"])) {
            $postImgFileName = $postDetail["post_details"]["parent_post"]["post_id"]["post_details"]["img_file_name"];
        }else{
            $postImgFileName = "";
        }

		$postParentType = $postDetail["post_details"]["parent_post"]["post_id"]["post_type"];

        if(isset($postDetail["post_details"]["parent_post"]["post_id"]["post_details"]["blog_title"])) {
            $postParentBlogTitle = ($postDetail["post_details"]["parent_post"]["post_id"]["post_details"]["blog_title"]);
        }else{
            $postParentBlogTitle = "";
        }

        if(isset($postDetail["post_details"]["parent_post"]["post_collection"])) {
            $postCollectionName = $postDetail["post_details"]["parent_post"]["post_collection"];
        }else{
            $postCollectionName = "";
        }
	}
    //printArr($postShortDesc);
	$seconds      = $mil / 1000;
    $created_at   = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
	$timestamp2   = strtotime($created_at);
    $ext          = explode('.', $postImgFileName);
    $sharingLinks = $rootUrl . "views/social/socialPostDetails.php?timestamp=" . $postTime . "&post_id=" . base64_encode($postId) . "&posttype=" . base64_encode($postType) . "&email=" . base64_encode($email);
    $sharingLinks = urlencode($sharingLinks);
    $fbLink       = $rootUrl . "views/social/socialPostDetails.php?timestamp=" . $postTime . "&post_id=" . base64_encode($postId) . "&posttype=" . base64_encode($postType) . "&email=" . base64_encode($email);
    $fbLink       = urlencode($fbLink);
    $fbCaption    = "Re-Engineering Internet Search Bisuness";
    $fbCaption    = urlencode($fbCaption);

?>
<input type="hidden" id="post_collection<?php echo $postParentId; ?>" value="<?php echo $postCollectionName; ?>">
<div class="social-timeline-image">

  <div class="social-card card half innerAll" id="social-timeline-image">
    <div class="">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="text-social-primary">Share Post</h4>
        </div>
      </div>
      <!--row-->
    </div>
    <!--social-timeline-details-->

    <div class="innerT inner-2x clearfix">
      <div class="col-xs-6 innerL half">
        <span>Share on your wall</span>
      </div>
      <div class="col-xs-6">
        <div class="pull-right">
          <div class="innerMR pull-left">Others:</div>
          <div class="pull-right margin-left-none shared-post-icons">
              <a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=145634995501895&description=I joined Keywo, they pay bitcoins for every like I make. You can earn bitcoins too. Join Today!&display=popup&caption=Re-Engineering%20Internet%20Search%20Bisuness&link=<?php echo $fbLink; ?>&name=Keywo.com&picture=<?php echo $rootUrlImages; ?>referNearn.png&redirect_uri=https://facebook.com">
                  <i class="fa fa-facebook-square"></i>
              </a>
              <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $sharingLinks; ?>&text=I joined Keywo, they pay bitcoins for every like I make. Join Today!">
                  <i class="fa fa-twitter-square text-light-blue half innerMLR"></i>
              </a>
              <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $sharingLinks; ?>&title=Sign up on Keywo.com and start earning today!&source=Keywo.com">
                  <i class="fa fa-linkedin-square"></i>
              </a>
          </div>
        </div>
      </div>
    </div>

    <div id ="share-post-desc">
      <div class="row">
        <div class="col-xs-12">
					<div>
	          <textarea placeholder="Say something about this..." rows="4" type="text" class="form-control checkEmpty can-mention-user inputor" id="modalComment"></textarea>
					</div>
				</div>
      </div>
    </div>
		<?php if(isset($postKeyword) && !empty($postKeyword)) { ?>
	    <div class=" innerTB">
	      <div class="row">
	        <div class="col-xs-12">
	          <?php
	            foreach ($postKeywords as $keyword) {
	              $keywords = "<span>#".$keyword."</span> ";
	              $keywoKeywords .= $keyword . ", ";

	          ?>
	          <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword;?>"><?php echo $keywords;?></a>
	          <?php } ?>
	        </div>
	      </div>
	    </div>
		<?php } ?>

    <div class="innerTB">
      <div class="row">
        <div class="col-xs-12 word-wrap">
          <?php echo rawurldecode($postShortDesc); ?>
        </div>
      </div>
    </div>

    <?php
    	switch (true) {
            case ($postType == 'video' || $postParentType == 'video'):
    ?>
    <div id="myModal" class="social-timeline-content-video">
      <div class="row">
        <div class="col-xs-12">
          <div align="center" class="embed-responsive embed-responsive-16by9" data-toggle="modal" data-target="#status_video_modal">
            <!-- <video loop class="embed-responsive-item">
                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
            </video> -->
        <iframe  id="cartoonVideo" width="560" height="258" src="<?php echo  $postAssetUrl; ?>" frameborder="0" allowfullscreen></iframe>
        </div>
        </div>
      </div>
    </div>
    <?php
    		break;
       		case ($postType == 'audio' || $postParentType == 'audio'):
    ?>
    <div class="social-timeline-sound">
      <div class="row">
        <div class="col-xs-12">
         <a data-id="social-timeline-sound">
             <?php if (strpos($postAssetUrl, 'https://soundcloud.com/') !== false) { ?>
                 <a href="<?php echo $postAssetUrl; ?>" target="_blank"><?php echo $postAssetUrl; ?></a>
             <?php } elseif (strpos($postAssetUrl, 'https://w.soundcloud.com/') !== false) { ?>
                 <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $postAssetUrl; ?>">
                 </iframe>
             <?php } ?>
        </a>
        </div>
      </div>
    </div>
    <?php
    		break;
       		case ($postType == 'blog' || $postParentType == 'blog'):
    ?>
    <div class="social-timeline-content-blog-thumb word-wrap">
      <div class="row">
          <div class="col-xs-12">
						<?php echo $postParentBlogTitle; ?>
					</div>
					<div class="col-xs-12">
          <?php  if(isset($postImgFileName) && !empty($postImgFileName)){ ?>
             <a data-toggle="modal" data-target="#status_blog_image_modal">
                 <img src="<?php echo $cdnSocialUrl . "users/" . $postAccHandle . "/post/blog/featured/" . $ext[1] . "/" . $postParentId . '_' . $postCreatedAt . '_' .$postImgFileName . ".725x500." . $ext[1]; ?>" class="img-responsive">
              </a>

          <?php }else{
							$blogContent = mb_strlen($content) > $validateCharCountOnSocial ? mb_substr($content, 0, $validateCharCountOnSocial) . '<a href="socialPostDetails.php?post_id='.base64_encode($postId).'&timestamp='.$postTime.'" target="_blank">....See More</a>' : $content;
              echo rawurldecode($blogContent);
          } ?>
          </div>
      </div>
    </div>
    <?php
        break;
          case ($postType == 'image' || $postParentType == 'image'):
    ?>
    <div class="social-timeline-content-image">
      <div class="row">
        <div class="col-xs-12">
        <a data-toggle="modal" href="#status_image_modal">
					<?php
                        $extension = pathinfo($postImgFileName, PATHINFO_EXTENSION);
                        //CDN image path
                        $imageFileOfCDN = $cdnSocialUrl . "users/" . $postAccHandle . "/post/images/" . $ext[1] . "/" . $postParentId . '_' . $postCreatedAt . '_' . $postImgFileName . ".725x500." . $ext[1];

                        //server image path
                        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postAccHandle . '/post/images/' . $ext[1] . '/' . $postParentId . '_' . $postCreatedAt . '_' . $postImgFileName;

                        // check for image is avilable on CDN
                        $file = $imageFileOfCDN;
                        $file_headers = @get_headers($file);
                        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                            $finalImagePath = $imageFileOfLocal;
                        } else {
                            $finalImagePath = $imageFileOfCDN;
                        }
					?>
          <img src="<?php echo $finalImagePath; ?>" class="img-responsive">
        </a>
        </div>
      </div>
    </div>
    <?php
    		break;
    	}
    ?>
    <div class=" innerTB">
      <div class="row">
        <div class="col-xs-12">

        </div>
      </div>
    </div>

    <div class="">
      <div class="row">
        <div class="col-xs-12">
					<div class="col-xs-2 offset-3 pull-right padding-right-none text-right">
						<div class="">
							<input value="Post" class="form-post-btn text-center btn-block" type="button"  id="shareUserPost" onclick = "shareUserPost('<?php echo $postId; ?>', '<?php echo $postTime; ?>', '<?php echo $postParentId; ?>', '<?php echo $postShareCreatedAt; ?>', '<?php echo $postMethod; ?>', '<?php echo $postParentType; ?>', '<?php echo $email; ?>', '<?php echo implode(',',$postKeywords);?>' );"/>
						</div>
					</div>
					<div class="pull-right padding-none form-select-dropdown">
            <select class="selectpicker" name="postscope" id="share_post_scope">
              <option value="Public">Public</option>
              <option value="Followers">Followers</option>
              <!-- <option value="Followers of Followers">Followers of Followers</option> -->
              <option value="Only Me">Only Me</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--social-card-->
</div>
<script>
/*------------mention users------------------*/
var tags           = [];
var suggestionData = [];
$(function() {
  $(".can-mention-user").on("keyup", function(e) {
      if ($(this).val().length >= 1) {
          mentionOnKeyup($(this).val());
      }
  });
});

function mentionOnKeyup(mentionInput) {
    var atkeyword = mentionInput.split(' ');
    if ((atkeyword[atkeyword.length - 1]).charAt(0) == '@') {
        var getSuggestion = (atkeyword[atkeyword.length - 1]).substring(1);
        if (getSuggestion != "") {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: { lastChar: getSuggestion },
                url: '../../controllers/social/getMentionUser.php',
                async: false,
                success: function(data) {
                    if (data.errCode == -1) {
                        tags = [];
                        for (var i = 0; i < data.errMsg.length; i++) {
                            tags.push('{"name":"' + data.errMsg[i].first_name + ' ' + data.errMsg[i].last_name + '","img":"' + data.errMsg[i].profile_pic + '","job":"' + data.errMsg[i].account_handle + '","email":"' + data.errMsg[i].email + '"}');
                        }
                        suggestionData = $.map(tags, function(values) {
                            var value = JSON.parse(values);
                            return { 'name': value.name, 'img': "../../images/rightpanel-img/" + value.img, 'job': value.job, 'email': value.email };
                        });
                    }
                },
                error: function() {
                    return false;
                }
            });
            if (getSuggestion.length == 1) {
                $('.inputor').atwho({
                    at: "@" + getSuggestion,
                    data: suggestionData,
                    limit: 10,
                    displayTpl: "<li class='sugg-wrapper'><img class='sugg-img' src='${img}' height='32' width='32'/><div class='sugg-name'> ${name}<div class='keyhandle'>${job}</div> </div><div class='hidden selectedEmail'>${email}</div></li>",
                    insertTpl: '@${job}',
                });
                $('.inputor').on("inserted.atwho", function(event, query) {
                 var htmlText   = query.html(query.html());
                 console.log(htmlText.find(".selectedEmail").html());
               });
            }
        }
    }
}
/*------------end of mention users------------------*/
</script>
