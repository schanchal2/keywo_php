<?php

// $url=parse_url("<?php echo $rootUrl?>views/social/followers.php?type=member#dgfdg");
// echo $url["fragment"]; //This variable contains the fragment
// die;
?>

<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once "../../models/social/socialModel.php";
require_once('../../models/social/commonFunction.php');
/* Add Helpers */
require_once('../../helpers/deviceHelper.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../IPBlocker/ipblocker.php');


//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

$postId        = base64_decode($_REQUEST['post_id']);
$postType      = base64_decode($_REQUEST['posttype']);
$postTime      = cleanXSS(trim(urldecode($_GET['timestamp'])));
$postDetail    = getPostDetail($postId, $postTime);
if(noError($postDetail)){
	$errorCode                                 = $postDetail["errCode"];
	 $postDetail                               = $postDetail["errMsg"][0];
	 $posted_by                                = $postDetail['posted_by'];
	 $image                                    = $postDetail['post_details']['img_file_name'];
	 $accountHandle                            = $postDetail['user_ref']['account_handle'];
	 $postType                                 = $postDetail['post_type'];
	 $ext									                     = explode(".",$image);
	 $mil                                      = $postDetail['created_at'];
	 $seconds                                  = $mil / 1000;
	 $created_at                               = date("d-m-Y H:i:s", $seconds);
	 $timestamp2                               = strtotime($created_at);
}else{
	$errorCode =  $postDetail["errCode"];
	$errorMsg  =  $postDetail["errMsg"];
}

$userId = $_SESSION['id'];

//check request for blocking IP address.
$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
$extraArgs = array();
$checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywords); //$ipAddr

if(noError($checkIPInfo)){
	// Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
	$checkIPInfo = $checkIPInfo["errMsg"];
	$searchCount = $checkIPInfo["search_count"];
	setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
}else{
	setErrorStack($returnArr, 11, $errMsg,  extraArgs);

}
$targetDirAccountHandler                    = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

//Check Like post details
$getLikePostStatus = getLikePostStatus($accountHandle, $postId, $postType);

//Get like post activity count to node database
$activityCount = getPostActivityCount($postId, $postTime);
if (noError($activityCount)) {
	$like_count = $activityCount['errMsg']['like_count'];
	$returnArr  = setErrorStack($returnArr, -1, null, $extraArgs);

} else {
	$errCode   = $activityCount['errCode'];
	$errMsg    = $activityCount['errMsg'];
	$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
}
?>


<?php
include("../layout/header.php");
?>

<main class="social-main-container inner-7x innerT" >
<div class="container">

	<div class="col-xs-3">
		<div class="social-left-panel">
		<?php include("leftInfoUser.php"); ?>
		<!-- my-info-card  -->
		<?php include("widgetsForPost.php"); ?>

	</div>
	<!-- social-left-panel  -->
	</div>

  	<!-- col-xs-3 -->
	<div class="col-xs-9" id="social-timeline-audio-preview">
		<div class="social-center-panel half innerLR">
			<?php if($errorCode != 4){?>
      <div class="social-timeline-video inner-2x innerMB">
      						<div class="social-card card half innerAll" data-toggle="modal" data-target="#status_video_modal">
      								<div class="">
      									<div class="row half innerTB">
      										<div class="col-xs-8">
      											<div class="social-timeline-profile-pic pull-left">
      												<img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $postDetail["user_ref"]["profile_pic"]; ?>" class=" img-responsive">
      											</div>
      											<div class="social-timeline-profile-details pull-left innerL">
      												<div class="social-user-link-name">
                                <a href="#">
          												<?php echo $postDetail["user_ref"]["first_name"]; ?> <?php echo $postDetail["user_ref"]["last_name"]; ?>
          											</a>
      												</div>
      												<!---->
      												<div class="social-user-link">
                                <a href="#">
          												<?php echo '@' . $postDetail["user_ref"]["account_handle"]; ?>
          											</a>
      												</div>
      												<!--social-user-link-->
      											</div>
      											<!--social-timeline-profile-details-->
      										</div>
                          <div class="col-xs-4 social-post-options">
                  					<div class="dropdown pull-right">
                  						<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  							<span class="fa fa-chevron-down"></span>
                  						</a>
                  						<?php if ($userId == $postDetail["posted_by"]) {?>
                  							<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                  							<li>
                  								<a data-toggle="modal" href="#editPostDetails" onclick = "editPersonalPost('<?php echo $postDetail["_id"]; ?>', '<?php echo $postDetail["created_at"]; ?>','<?php echo $postDetail["post_type"];?>','<?php echo $postDetail["user_ref"]["account_handle"];?>');">
                                  <i class="fa fa-pencil" ></i>
                                  Edit Post
                                 </a>
                  							</li>
                  							<li>
                  								<a href="#" onclick = "hidePost('<?php echo $postDetail["_id"]; ?>','<?php echo $postType;?>','<?php echo $mil;?>', '');">
                  									<i class="fa fa-eye-slash" ></i>
                  									Hide Post
                  								</a>
                  							</li>
                  							<li>
																	<a href="#" data-toggle="modal" data-target="#removePostModal" data-postid='<?php echo $postDetail["_id"]; ?>' data-posttype='<?php echo $postType; ?>' data-postcreatedat='<?php echo $mil; ?>'>
									                  <i class="fa fa-trash" ></i>
									                  Remove Post
									                </a>
                  							</li>
                  						</ul>
                  						<?php } else {?>
                  						<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                  							<li>
                  								<a href="#">
                  									<i class="fa fa-bookmark"></i>
                  									Save Post
                  								</a>
                  							</li>
                  							<li>
                  								<a href="#" onclick = "hidePost('<?php echo $postDetail["_id"]; ?>','<?php echo $postType;?>','<?php echo $mil;?>', '');">
                  									<i class="fa fa-eye-slash"></i>
                  									Hide Post
                  								</a>
                  							</li>
                  							<li>
                  								<a href="#">
                  									<i class="fa fa-file-text"></i>
                  									Report Post
                  								</a>
                  							</li>
																<li>
																	<?php
																	$followFlag = false;
																	$getFollowingDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($data[$i]->user_ref->account_handle[0]).".json";
																	if(file_exists($getFollowingDetails)) {
																	$jsondataFollowing = file_get_contents($getFollowingDetails);
																	$dataFollowing = json_decode($jsondataFollowing, true);
																	if (in_array($data[$i]->posted_by, $dataFollowing["followings"])) {
																	$followFlag = true;
																	}  } ?>
																		<a href="javascript:;" onclick = "followUserAcc(this,'<?php echo $data[$i]->user_ref->email; ?>');">
																		<?php    if($followFlag){  $buttoVal = 'Unfollow'; ?>
																		<i class="fa fa-user-times"></i>
																		<?php   } else {  ?>
																		<i class="fa fa-user"></i>
																		<?php $buttoVal = 'Follow';        }  ?>
																		<?php echo $buttoVal; ?> User
																		</a>
																</li>
                  							<li>
                  								<a href="#">
                  									<i class="fa fa-key"></i>
                  									Unfollow #<span>keyword</span>
                  								</a>
                  							</li>
																<li>
																<?php
																	$blockFlag = false;
																	$getBlockDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($data[$i]->user_ref->account_handle[0]).".json";
																	if(file_exists($getBlockDetails)) {
																	$jsondataBlock = file_get_contents($getBlockDetails);
																	$dataBlock = json_decode($jsondataBlock, true);
																	if (in_array($data[$i]->posted_by, $dataBlock["blocked"])) {
																	$blockFlag = true;
																	}  } ?>
																		<a href="javascript:;" onclick = "blockUserAcc(this,'<?php echo $data[$i]->user_ref->email; ?>');">
																		<?php  if($blockFlag){  $buttoVal = 'Unblock'; ?><i class="fa fa-user"></i>
																		<?php } else { $buttoVal = 'Block';?><i class="fa fa-user-times"></i><?php  } ?>
																		<?php echo $buttoVal;?> #<span>Account</span>
																		</a>
																</li>
                  						</ul>
                  						<?php }?>
                  						<!--check for whether cdp done or not-->
                  					</div>
                  					<br />
                  					<div class="pull-right innerT">
                  						  <span class="social-post-time"><span title="<?php  echo  date("h:i A", $timestamp2)." - ".date('d M Y', $timestamp2);;?>"><?php include("getDateFormate.php"); ?></span>
                  					</div>
                  				</div>
      									</div>
      									<!--row-->
      								</div>
      								<!--social-timeline-details-->
      								<div class="social-timeline-keywords-details">
      									<div class="row half innerT">
                          <div class="col-xs-12">
                          <?php
													$keywords = $postDetail['keywords'];
                          foreach ($keywords as $keywo) {
                          ?>
                          <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keywo ; ?>">#<?php echo $keywo ; ?></a>
                          <?php } ?>
                          </div>
      									</div>
      								</div>
      								<!--social-timeline-tags-details-->

      								<!--social-timeline-user-message-->
      								<div class="social-timeline-content-message">
      									<div class="row">
      										<div class="col-xs-12">
														<?php
															switch ($postType) {
																		case 'video':
														?>
      											<p class="innerMB">
      												  <?php echo $postDetail['post_short_desc'];; ?>
      											</p>
														<?php
														break;
														case 'audio':
														?>
														<p class="innerMB">
      												  <?php echo $postDetail['post_short_desc'];; ?>
      											</p>
														<?php
														break;
														case 'image':
														?>
														<p class="innerMB">
      												  <?php echo $postDetail['post_short_desc'];; ?>
      											</p>
														<img src="<?php echo $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postDetail["_id"] . '_' . $postDetail["created_at"] . '_' . $postDetail["post_details"]["img_file_name"] . ".725x500." . $ext[1]; ?>" class="img-responsive" />

														<?php
														break;
														case 'status':
														?>
														<p class="innerMB">
      												  <?php echo $postDetail['post_short_desc'];; ?>
      											</p>
														<?php } ?>
      										</div>

      									</div>
      								</div>
      								<!--social-timeline-tags-details-->

											<?php if($postType=="video" || $postType=="audio") {?>
      								<!--social-timeline-video-->
      								<div class="social-timeline-content-video">
      									<div class="row">
      										<div class="col-xs-12">
      											<div align="center" class="embed-responsive embed-responsive-16by9">
      										    <!-- <video loop class="embed-responsive-item">
      										        <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
      										    </video> -->
															<iframe width="560" height="258" src="<?php echo  $postDetail['post_details']["asset_url"]; ?>" frameborder="0" allowfullscreen></iframe>

      												</div>
      										</div>
      									</div>
      								</div>

											<?php } ?>
      								<!--social-timeline-video-->
      								<div class="social-timeline-earning-comments-view innerT">
      									<div class="row">
													<?php if($postType!="status") { ?>
      										<div class="col-xs-4">
      											<div>
      												<label>Earning : </label>
      												<span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
      											</div>
      										</div>
      										<div class="col-xs-8">	<?php } else { ?>
														<div class="col-xs-12">	<?php } ?>
      											<div class="">
      												<ul class="list-inline pull-right margin-bottom-none">
      													<li>
      														<div>
      															<span class="half innerR comment-count">40</span>
      															<label class="pull-right">Comments</label>
      														</div>
      													</li>
      													<li>
      														<div>
      															<span class="">456</span>
      															<label>Shares</label>
      														</div>
      													</li>
      													<li class="padding-right-none">
      														<div>
      															<span class="">557k</span>
      															<label>Views</label>
      														</div>
      													</li>
      												</ul>
      											</div>
      										</div>
      									</div>
      								</div>
      								<div class="social-timeline-likes-dislikes">
      									<div class="row">
      										<div class="col-xs-6">
      											<div class="social-likes-dislikes">
      												<ul class="list-inline margin-bottom-none">
      													<li>
      														<div>
      															<a>
																	<?php if($getLikePostStatus == 1){ ?>
																		<i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-up half innerR" onclick="updateLikePostCount('<?php echo $postId; ?>', '<?php echo $postTime; ?>', '<?php echo $postType; ?>', '<?php echo $accountHandle; ?>')"></i>
																	<?php } else {?>
																		<i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" onclick="updateLikePostCount('<?php echo $postId; ?>', '<?php echo $postTime; ?>', '<?php echo $postType; ?>', '<?php echo $accountHandle; ?>')"></i>
																	<?php } ?>
																	<span id="like-count<?php echo $postId; ?>"><?php echo $like_count; ?></span>

																</a>
      														</div>
      													</li>
      												</ul>
      											</div>
      										</div>
      										<div class="col-xs-6">
      											<div class="social-comments-shares-views">
      												<ul class="list-inline pull-right margin-bottom-none">
      													<li>
      														<div>
      															<a>
      																<i class="fa fa-comments-o"></i>
      																<span>Comments</span>
      															</a>
      														</div>
      													</li>
      													<li class="padding-right-none">
      														<div>
      															<a>
      																<i class="fa fa-share-square-o"></i>
      																<span>Share</span>
      															</a>
      														</div>
      													</li>
      												</ul>
      											</div>
      										</div>
      									</div>
      								</div>
                      <div class="comments-module bg-light-purple">

                      <input type = "hidden" id  = "comment-basic-details" post-id = "<?php echo $postDetail["_id"];?>" post-created-at = "<?php echo $postDetail["created_at"]; ?>" call-ajax-flag = "true" call-ajax-get-flag = "true" call-ajax-edit-flag = "true">

                        <div class="row comments-module-wrapper">
          								<div class="col-xs-1 wid-50">
          									<img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $postDetail["user_ref"]["profile_pic"]; ?>">
          								</div>
          								<div class="col-xs-11">
          									<div id = "comment-form" class="form-group">
          										<input type = "text" id = "user-comment-text" class = "form-control checkEmpty" placeholder = "Write Comment Here">
          									</div>
          								</div>
          							</div>


          							<div class="row comments-module-wrapper myComment1" style = "display:none;">
          								<div class="col-xs-1 wid-50">
          									<img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $userDetail['errMsg']['profile_pic']; ?>">
          								</div>
          								<div class="col-xs-10 padding-left-none">
          									<div class="comments-module-comment-block">
          										<a href="#" class="comments-module-username"><?php echo $userDetail['errMsg']["first_name"]; ?> <?php echo $userDetail['errMsg']["last_name"]; ?></a>
          										<span id = "user-comment-title" class="comments-module-comment "></span>
          									</div>
          									<div class="comments-module-time-block">
          										<a href="#" class="comments-module-reply">Reply</a>
          										<span id = "comment-created-time" class="comments-module-hrs"></span>
          									</div>
          								</div>
                          <div class="col-xs-1">
                            <a  class="edit-comment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                              <li class="border-bottom clearfix">
                                <a data-toggle="modal" href="#22" ><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                              </li>
                              <li>
                                <a data-toggle="modal" data-target="#deleteConfirmation" href="#22q" ><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                              </li>
                            </ul>
														<div>

														</div>
                          </div>
          							</div>
                        <!--likes & Dislikes-->
                        <!-- <div class="comment-wrap">
													<div>


					<div class="row comments-module-wrapper">
					<div class="col-xs-1 wid-50">
						<img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
					</div>
					<div class="col-xs-10 padding-left-none">
						<div class="comments-module-comment-block">
													<a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
							<span class="comments-module-comment">fsdf</span>
						</div>
						<div class="comments-module-time-block">
							<a href="#" class="comments-module-reply">Reply</a>
							<span class="comments-module-hrs">12  min </span>
						</div>
						<div class="row comments-module-wrapper">
							<div class="col-xs-1 wid-50">
								<img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
							</div>
							<div class="col-xs-11">
								<div id="comment-form" class="form-group">
									<input type="text" id="user-comment-text" class="form-control checkEmpty" placeholder="Write Comment Here">
								</div>
							</div>
						</div>

					</div>
					<div class="col-xs-1">
												<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-pencil"></i></a>
						<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
							<li>
								<a data-toggle="modal" onclick="editDeleteComment('update', '58d36d3118cae6ed2418fb40');">Edit</a>
							</li>
							<li>
								<a data-toggle="modal" onclick="deleteConfirmation('delete', '58d36d3118cae6ed2418fb40');">Delete</a>
							</li>
						</ul>
											</div>

				</div>
	<div id="get-more-comment" style="display:none;" remainingcount="0" commentloadclick="false">commentAppendData('LoadMore','1490251057894','10')</div>

</div>
                        </div> -->

												<div id="comment-append">

												</div>

                        <!--comments-module-wrapper-->
          							<!--<div class="row comments-module-wrapper">
          								<div class="col-xs-1">
          									<img src="<?php echo $rootUrlImages?>social/comment.png">
          								</div>
          								<div class="col-xs-11">
          									<div class="comments-module-comment-block">
          										<a href="#" class="comments-module-username">David Mondrus</a>
          										<span class="comments-module-comment">not "totally" new</span>
          									</div>
          									<div class="comments-module-time-block">
          										<a href="#" class="comments-module-reply">Reply</a>
          										<span class="comments-module-hrs">23 hrs</span>
          									</div>
          								</div>
          							</div>
          							<!--comments-module-wrapper -->
          							<div id = "commentLoadMoreButton" class="row comments-module-wrapper" style = "display:none;">
          								<div class="col-xs-12">
          									<a id = "loadMoreCommentPostPage" class="comments-module-all-comments" onclick = "" >View <span id = "remainingLoadCount" class="comment-count"></span> more comments </a>
          								</div>
          							</div>
          						</div>
      						</div>
                	<?php if($postType!="status") { include("widgetsForBelowPost.php"); } ?>
      					</div>


			</div>
			<!--social-timeline-blog-->
			<?php }else{
				echo "<b>".$errorMsg."</b>";

			}?>
			<?php //if($postType!="status") { include("widgetsForBelowPost.php"); } ?>
    </div>
  </div>
</div>

<!-- Blog Form starts-->
<link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
<link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico" >
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">

<!--modal to display editing data-->
<div class="modal fade" id="editPostDetails" role="dialog">
  <div class="modal-dialog">
    <div class="card social-card clearfix social-form-modal">
      <div class="clearfix innerR innerT">
        <i class="fa fa-times close-button" data-dismiss="modal"></i>
      </div>
      <div class = "modal-body">

      </div>
    </div>
  </div>
</div>
<?php } else {
	header('location:'. $rootUrl .'views/prelogin/index.php');
	} ?>
<?php include('../layout/social_footer.php'); ?>
<!--social-timeline-modal-on-remove-post-->
<div class="modal fade" id="removePostModal" role="dialog">
	<div class="modal-dialog modal-sm">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix innerR innerT">
				<i class="fa fa-times close-button" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body">
				<h4>Are you sure you want to delete?</h4>
				<button type="button" id="" class="btn btn-danger">Yes</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <!-- <div class="clearfix closebutton-absolute">
        <button type="button" class="close-button close-dialog" data-dismiss="modal"></button>
      </div> -->
			<div class="modal-header">
					<i class="fa fa-times close-button" data-dismiss="modal"></i>
				<h4 class="modal-title text-blue">Delete</h4>
			</div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>

<!-- confirmation dialog -->
<div id="deleteConfirmation" class="modal fade" role="dialog">
  <div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">

			<!-- Modal content-->
			<div class="">
				<div class="modal-header">
						<i class="fa fa-times close-button" data-dismiss="modal"></i>
					<h4 class="modal-title text-blue">Delete</h4>
				</div>
				<div class="modal-body">
					Are you sure you want to delete comment?
				</div>
				<div class="modal-footer border-none">
					<button type="button" class="btn btn-social-wid-auto" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
  </div>
</div>
<!-- Trigger the modal with a button -->
<button type="button" class="" data-toggle="modal" data-target="#myReportModal">Open Modal</button>

<!-- Report Modal -->
<div id="myReportModal" class="report-popup modal fade" role="dialog">
  <div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">

			<!-- Modal content-->
			<div class="">
				<div class="modal-header">
						<i class="fa fa-times close-button" data-dismiss="modal"></i>
					<h4 class="modal-title text-blue">What kind of abuse you are reporting?</h4>
				</div>
				<div class="modal-body">
					<div class="text-blue report-question">It should not be on keywo - </div>
					<div class="radio radio-primary radio-div">
							<input type="radio" name="gender" id="rude" value="rude" checked="true">
							<label for="rude" style="display:block;">
									It's rude, vulger or uses bad language
							</label>
					</div>
					<div class="radio radio-primary radio-div">
							<input type="radio" name="gender" id="exploit" value="exploit">
							<label for="exploit">
									It's sexually exploit
							</label>
					</div>
					<div class="radio radio-primary radio-div">
							<input type="radio" name="gender" id="hate" value="hate">
							<label for="hate">
									It's harrasement or hate speech
							</label>
					</div>
					<div class="radio radio-primary radio-div">
							<input type="radio" name="gender" id="spam" value="spam">
							<label for="spam">
									It's spamthreatening, violent or suicidal
							</label>
					</div>
					<div class="radio radio-primary radio-div">
							<input type="radio" name="gender" id="other" value="other">
							<label for="other">
									Others(Specify any)
							</label>
							<input type="text" class="other-input-comment innerMT half"/>
					</div>

				</div>
				<div class="modal-footer border-none">
					<button type="button" class="btn btn-social" data-dismiss="modal">Report</button>
				</div>
			</div>
		</div>
  </div>
</div>


<script>
$('#removePostModal').on('show.bs.modal', function (event) {
  var button        = $(event.relatedTarget); // Button that triggered the modal
  var postId        = button.data('postid'); // Extract info from data-* attributes
  var postType      = button.data('posttype');
  var postCreatedAt = button.data('postcreatedat');
  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
  var modal = $(this);
  modal.find('.btn.btn-danger').attr('id', 'post_id_' + postId);
  $('#post_id_' + postId).click(function(){
    removePersonalPost(postId, postType, postCreatedAt);
    $('#removePostModal').modal('hide');
  });
});
</script>
