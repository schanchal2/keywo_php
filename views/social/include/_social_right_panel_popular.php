<!--=========================================================
=            include _social_right_panel_popular            =
==========================================================-->
<div class="bg-light-gray right-panel-modules-head">
    <div class="row margin-none">
        <div class="col-xs-4 padding-right-none">
            <h4 class="half innerMTB">Popular</h4>
        </div>
        <div class="col-xs-8 padding-left-none">
            <div class="popular-content">
                <ul id="popular-post-click" class="list-inline half innerMTB text-right">
                    <li>
                        <a id="popular-post-type-blog" class="popular-active-indicate active" onclick="getPopularPost('blog');">
                            <i class="fa fa-file-text"></i>
                        </a>
                    </li>
                    <li>
                        <a id="popular-post-type-status" class="popular-active-indicate " onclick="getPopularPost('status');">
                            <i class="fa fa-edit"></i>
                        </a>
                    </li>
                    <li>
                        <a id="popular-post-type-video" class="popular-active-indicate " onclick="getPopularPost('video');">
                            <i class="fa fa-play-circle"></i>
                        </a>
                    </li>
                    <li>
                        <a id="popular-post-type-image" class="popular-active-indicate " onclick="getPopularPost('image');">
                            <i class="fa fa-image"></i>
                        </a>
                    </li>
                    <li>
                        <a id="popular-post-type-audio" class="popular-active-indicate " onclick="getPopularPost('audio');">
                            <i class="fa fa-soundcloud"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="popular-post-details">
    <?php include('widgetPopularPost.php'); ?>
</div>
<!--====  End of include _social_right_panel_popular  ====-->
