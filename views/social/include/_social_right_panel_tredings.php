<!--============================================================
=            include :_social_right _panel_tredings            =
=============================================================-->
<div class="bg-light-gray right-panel-modules-head">
    <div class="row margin-none">
        <div class="col-xs-4 padding-right-none">
            <h4 class="half innerMTB">Trending</h4>
        </div>
        <div class="col-xs-8 padding-left-none">
            <div class="trending-content">
                <ul class="list-inline half innerMTB text-right">
                    <li>
                        <a class="active" href="#" data-id="blog">
                            <i class="fa fa-file-text"></i>
                        </a>
                    </li>
                    <li>
                        <a class="" href="#" data-id="status">
                            <i class="fa fa-edit"></i>
                        </a>
                    </li>
                    <li>
                        <a class="" href="#" data-id="video">
                            <i class="fa fa-play-circle"></i>
                        </a>
                    </li>
                    <li>
                        <a class="" href="#" data-id="image">
                            <i class="fa fa-image"></i>
                        </a>
                    </li>
                    <li>
                        <a class="" href="#" data-id="audio">
                            <i class="fa fa-soundcloud"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="trending-keywords-data">
    <?php include('trendingKeywords.php'); ?>
</div>
<!--====  End of include :_social_right _panel_tredings  ====-->
