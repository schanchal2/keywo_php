<!--=======================================================
=            include: _social_right_panel_searchbar.php            =
========================================================-->
<div class="social-right-panel-search">
    <div class="input-group social-right-panel-search__input-group" id="social-search-text">
        <input type="text" class="checkEmpty form-control social-right-panel-search__form-control" id="social-search-text-right-side" placeholder="Search">
        <div class="input-group-addon social-right-panel-search__input-group-addon">
            <button type="submit" class="btn btn-sm btn-default social-right-panel-search__search-btn"><i class="fa fa-search"> </i> </button>
        </div>
    </div>
</div>
<!--====  End of include: _social_right_panel_searchbar.php  ====-->
