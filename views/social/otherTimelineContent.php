<!--====================================================
=            include : otherTimelineContent            =
=====================================================-->
<!-- for getting post -->
<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');
require_once('../../models/wallet/walletKYCModel.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once('../../IPBlocker/ipblocker.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

$userId             = $_SESSION["id"];
// making curl request
$type               = cleanXSS(rawurldecode($_POST['type']));
$dataCount          = cleanXSS(rawurldecode($_POST['dataCount']));
$lastDataCreateTime = cleanXSS(rawurldecode($_POST['lastDataCreateTime']));
$emailId            = base64_decode(cleanXSS(rawurldecode($_POST['otherEmail'])));

//get other user info
$postFields         = "active,short_description,followings,followers,post_count,profile_pic,first_name,last_name,creationTime,account_handle,user_id";
$otherUserDetails   = getUserInfo($emailId, $walletURLIPnotification . 'api/notify/v2/', $postFields);

$userPendingInteraction = "no_of_qualified_interactions_pending,kyc_current_level,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic";
$getSearchType = getSearchType($_SESSION['email'], $userPendingInteraction);
if (noError($getSearchType)) {
  $getSearchType = $getSearchType["data"];
  $userSearchType = $getSearchType["user_search_type"];
  $profilePic = $getSearchType["profile_pic"];
} else {
  print('Error: Fetching user interation type');
  exit;
}

if(noError($otherUserDetails)){
    $userStatus         = $otherUserDetails["errMsg"]["active"];

    if ($userStatus != 1) {
        $rootForRedirection =  $rootUrl."views/social/not_found_error.php";
        print("<script>");
        print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
        print("</script>");
        exit;
    }

    $postedBy           = $otherUserDetails["errMsg"]["user_id"];
    $otherAccHandle     = $otherUserDetails["errMsg"]["account_handle"];
    $viewType           = "other";
    $sessionUserId      = $_SESSION["id"];
    $postData           = getUserTimelinePost($postedBy, $type, $lastDataCreateTime, $viewType, $otherAccHandle, $sessionUserId);
    $dataEmptyFlag      = "false";
    $bulkData           = array();
    $blockFlag          = false;
}

$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$emailofSpecialUser = array();

if(isset($postData["errMsg"])){
    foreach ($postData["errMsg"] as $data) {
        $postIddata = array(
            'post_id' => $data['_id'],
            'time'    => $data['created_at']
        );
        array_push($bulkData, $postIddata);
    }
}


$bulkPostId = json_encode($bulkData);
//   Get like post activity count from node database
$activityCount = getPostActivityCount($bulkPostId, '');
// printArr($postData["errMsg"]);

foreach ($postData["errMsg"] as $key => $data) {
 // printArr($data); die();
// echo $data['created_at'];
if ($key < 10) {
$mil          = $data['created_at'];
$lastDataTime = $data['created_at'];
$seconds      = $mil / 1000;
$created_at                = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
$timestamp2   = strtotime($created_at);
$postId       = $data['_id'];
$postType     = $data['post_type'];
$createdAt    = $data['created_at'];
//get user ip address
$ipAddr       = getClientIP();
$post_earnings= $data['post_earnings'];

//check request for blocking IP address.
$user_agent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
$extraArgs   = array();


// $keywords="";
// $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywords); //$ipAddr

// if (noError($checkIPInfo)) {
//     // Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
//     $checkIPInfo = $checkIPInfo["errMsg"];
//     $searchCount["searchCount"] = $checkIPInfo["search_count"];
//     setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
// } else {
//     setErrorStack($returnArr, 11, $errMsg,  extraArgs);
// }
    $searchCount = "";
    if(isset($searchCount)){

    }else{
        $searchCount="";
    }
//reserved for social
    global $cdnSocialUrl;
    global $rootUrlImages;

if(isset($data["user_ref"]["profile_pic"]) && !empty($data["user_ref"]["profile_pic"])){
    $extensionUP  = pathinfo($data["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
    //CDN image path
    $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data["user_ref"]["account_handle"] . '/profile/' . $data["user_ref"]["account_handle"] . '_' . $data["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

    //server image path
    $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data["user_ref"]["account_handle"].'/profile/'.$data["user_ref"]["account_handle"].'_'.$data["user_ref"]["profile_pic"];

    // check for image is available on CDN
    $file = $imageFileOfCDNUP;
    $file_headers = @get_headers($file);
    if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $imgSrc = $imageFileOfLocalUP;
    } else {
        $imgSrc = $imageFileOfCDNUP;
    }
}else{
    $imgSrc = $rootUrlImages."default_profile.jpg";
} 

//Check Like post details
$getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);
//check cdp status
$getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);

// printArr($activityCount['errMsg'][$postId]);
$targetActivityCount = $activityCount['errMsg'][$postId];
$like_count          = $targetActivityCount['like_count'];
$commentCount        = $targetActivityCount['comment_count'];
$shareCount          = $targetActivityCount['share_count'];


  if(isset($targetActivityCount['parent_like_count']))
  {
      $parentLikeCount = $targetActivityCount['parent_like_count'];
   }else
   {
     $parentLikeCount = "";
   }


  if(isset($targetActivityCount['parent_comment_count']))
  {
      $parentCommentCount = $targetActivityCount['parent_comment_count'];
   }else
   {
     $parentCommentCount = "";
   }
  if(isset($targetActivityCount['parent_share_count']))
  {
     $parentShareCount = $targetActivityCount['parent_share_count'];
   }else
   {
     $parentShareCount = "";
   }



// Get bookmark Details
$getBookMarkPost = getBookMarkPost($_SESSION["account_handle"], $postId, $postType );

//Get Special User
//$getSpecialUserData = getSpecialUserPost();
//if(noError($getSpecialUserData)){
//    $getSpecialUserData = $getSpecialUserData["errMsg"];
//}
//
//if($getSpecialUserData!="No Result") {
//    foreach ($getSpecialUserData as $specialUser) {
//        $emailofSpecialUser[] = $specialUser['email'];
//    }
//}else{
//    $emailofSpecialUser[] = " ";
//}

    //get short description with mention links
    $shortDescriptionText = "";
    $shortDescData = "";

    if(isset($data["post_mention"])){
        $shortDescriptionText = getLinksOnText(rawurldecode($data["post_short_desc"]), $data["post_mention"], $_SESSION["account_handle"]);

        $shortDescription     = $shortDescriptionText["text"];
        $descLenWithoutStriptag  = strlen($shortDescription);
        $descLenWithStriptag     = strlen(strip_tags($shortDescription));
        $diff                    = $descLenWithoutStriptag - $descLenWithStriptag;
        $shortDescData           = ($descLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $diff) . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $shortDescription;
    }

    ?>
<!--check post is shared post or not. if no shared post then-->
<?php if ($data["post_type"] !== "share") {?>

<div class="social-timeline-image inner-2x innerMB social-card" id="post_<?php echo $data["_id"]; ?>">
    <div class=" card innerLR innerT innerMT">
            <div class="row">
                <div class="col-xs-8">
                    <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $imgSrc; ?>" class=" img-responsive"  />
                    </div>
                    <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                            <a href="#">
                                <?php echo $data["user_ref"]["first_name"]; ?> <?php echo $data["user_ref"]["last_name"]; ?>
                            </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                            <a href="#">
                                <?php echo '@' . $data["user_ref"]["account_handle"]; ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 social-post-options">
                    <?php
                    $postDetail['created_at'] =  $data['created_at'];
                    include('commonDropDownOption.php');?>
                </div>
            </div>
        <div class="social-timeline-keywords-details">
            <div class="row half innerT">
                <?php if(isset($data["keywords"][0]) && !empty($data["keywords"][0])) { ?>
                    <div class="col-xs-8">
                        <?php
                        $totalKeywords="";
                        $keywoKeywords = " ";
                        foreach ($data["keywords"] as $keyword) {
                            $totalKeywords.=" ".$keyword;
                            $keywords = "<span>#".$keyword."</span> ";
                            $keywoKeywords .= $keyword . ", ";
                            ?>
                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keyword);?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword;?>"><?php echo $keywords;?></a>
                        <?php }
                        $keywoKeywords = rtrim($keywoKeywords,', ');
                        $totalKeywords=trim($totalKeywords," ");

                        //check request for blocking IP address.
                        $user_agent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
                        // $extraArgs   = array();
                        // $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywoKeywords); //$ipAddr
                        // if (noError($checkIPInfo)) {
                        //     // Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
                        //     $checkIPInfo = $checkIPInfo["errMsg"];
                        //     $searchCount["searchCount"] = $checkIPInfo["search_count"];
                        //     setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
                        // } else {
                        //     setErrorStack($returnArr, 11, $errMsg,  extraArgs);
                        // }

                       // Reserved for soacial
                        ?>
                    </div>
                <?php } ?>
            </div>
        </div>
        <!--social-timeline-keywords-details-->

        <!-- switch case-->
        <?php
        $value = $data["post_type"];
        switch ($value) {
        case 'video':
        ?>
        <div class="modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="" data-ipaddr="" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="">
            <div class="social-timeline-content-message">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="innerMB">
                            <?php
                            // $statusData = mb_strlen($shortDescription) > $validateCharCountOnSocial + $counterText ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $counterText) . "<a href='javascript:;'><span class='text-blue'> ....See More</span></a>" : $shortDescription;
                            echo $shortDescData;
                            ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--social-timeline-video-->
            <div class="social-timeline-content-video">
                <div class="row">
                    <div class="col-xs-12">
                        <div align="center" class="embed-responsive embed-responsive-16by9">
                            <?php
                            if(isset($data["post_details"]["video_thumbnail"]) && !empty($data["post_details"]["video_thumbnail"])){
                                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data["user_ref"]["account_handle"] . '/post/video/' . $data["post_details"]["video_thumbnail"];

                                //server image path
                                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data["user_ref"]["account_handle"].'/post/video/'.$data["post_details"]["video_thumbnail"];

                                // check for image is available on CDN
                                $file = $imageFileOfCDNUP;
                                $file_headers = @get_headers($file);
                                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    $imgSrc = $imageFileOfLocalUP;
                                } else {
                                    $imgSrc = $imageFileOfCDNUP;
                                }
                            }
                            ?>
                            <div align="center" class="video-div embed-responsive embed-responsive-16by9" style="background: url('<?php echo $imgSrc; ?>'); no-repeat; background-size: cover !important;">
                                <?php
                                if (strpos($data["post_details"]["video_thumbnail"], 'youtube') !== false) {
                                    ?>
                                    <img class="play-img img-responsive" src="<?php echo $rootUrlImages . 'y3.png';?>">
                                    <?php
                                } else {
                                    ?>
                                    <img class="play-img-vimeo play-img img-responsive" src="<?php echo $rootUrlImages . 'v3.png';?>">
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!--social-timeline-video-->
            <?php	break;
            case 'audio':
                ?>
                <!--social-timeline-user-message-->
                <div class="modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="" data-ipaddr="" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="">
                    <div class="social-timeline-content-message">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php
                                    // $statusData = mb_strlen($shortDescription) > $validateCharCountOnSocial + $counterText ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $counterText) . "<a href='javascript:;'><span class='text-blue'> ....See More</span></a>" : $shortDescription;
                                    echo $shortDescData;
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-user-message-->
                <!--social-timeline-audio-->
                <div class="social-timeline-sound">
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if (strpos($data["post_details"]["asset_url"], 'https://soundcloud.com/') !== false) { ?>
                                <a href="<?php echo $data["post_details"]["asset_url"]; ?>" target="_blank"><?php echo $data["post_details"]["asset_url"]; ?></a>
                            <?php } elseif (strpos($data["post_details"]["asset_url"], 'https://w.soundcloud.com/') !== false) { ?>
                                <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $data["post_details"]["asset_url"]; ?>">
                                </iframe>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <!--social-timeline-audio-->
                <?php break;
            case 'blog':
                ?>
                <!--social-timeline-content-blog-->
      <div class="modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="blog">
        <div class="row">
          <div class="col-xs-12">
            <div class="social-timeline-content-blog-nothumb half innerMT">
            <?php if (isset($data["post_details"]["img_file_name"]) && !empty($data["post_details"]["img_file_name"])) { ?>
            <!--social-timeline-blog-image-->
            <div class="row">
              <div class="col-xs-12">
                <div class="blog-nothumb-content">
                  <a>
                  <span class="blog-nothumb-head">
                    <?php echo $data["post_details"]["blog_title"]; ?>
                  </span>
                  </a>
                  <div class="social-timeline-content-blog-thumb">
                    <div class="row">
                      <div class="col-xs-12 blog-display-img">
                        <?php $extension = pathinfo($data["post_details"]["img_file_name"], PATHINFO_EXTENSION);

                        //CDN image path
                        $imageFileOfCDN = $cdnSocialUrl . 'users/' . $data["user_ref"]["account_handle"] . '/post/blog/featured/' . $extension . '/' . $data["_id"] . '_' . $data["created_at"] . '_' . $data["post_details"]["img_file_name"] ;

                        //server image path
                        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data["user_ref"]["account_handle"] . '/post/blog/featured/' . $extension . '/' . $data["_id"] . '_' . $data["created_at"] . '_' . $data["post_details"]["img_file_name"] ;

                        // check for image is avilable on CDN
                        $file = $imageFileOfCDN;
                        $file_headers = @get_headers($file);
                        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $finalImagePath = $imageFileOfLocal;
                        }
                        else {
                        $finalImagePath = $imageFileOfCDN;
                        }

                        ?>
                        <img src="<?php echo $finalImagePath; ?>" class="img-responsive modal--show--post" data-id="<?php echo $data['_id']; ?>" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $_SESSION["email"] ?>" data-usersearchtype="blog"/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--social-timeline-blog-image-->
            <?php } else { ?>
              <div class="row half innerTB border-top border-bottom">
                <div class="col-xs-12">
                  <div class="blog-nothumb-content">
                    <a>
                    <span class="blog-nothumb-head">
                      <?php echo $data["post_details"]["blog_title"]; ?>
                    </span>
                    </a>
                    <span class="blog-nothumb-body modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $_SESSION["email"] ?>" data-usersearchtype="blog">
                    <?php //printArr($data[$i]->post_details->blog_content);
                      $blogDescriptionText = getLinksOnText(rawurldecode($data["post_details"]["blog_content"]), $data["post_mention"], $_SESSION["account_handle"]);
                      $blogDescription            = $blogDescriptionText["text"];
                      $descBlogLenWithoutStriptag = strlen($blogDescription);
                      $descBlogLenWithStriptag    = strlen(strip_tags($blogDescription));
                      $diffBlog                   = $descBlogLenWithoutStriptag - $descBlogLenWithStriptag;
                      $blogData                   = ($descBlogLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($blogDescription, 0, $validateCharCountOnSocial + $diffBlog) 
                      . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $blogDescription;
                      echo $blogData;
                    ?>
                  </span>
                </div>
              </div>
            </div>
          
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
                <?php break;
            case 'image':
                ?>
                <div class="modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="" data-ipaddr="" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php
                                    // $statusData = mb_strlen($shortDescription) > $validateCharCountOnSocial + $counterText ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $counterText) . "<a href='javascript:;'><span class='text-blue'> ....See More</span></a>" : $shortDescription;
                                    echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <!--social-timeline-image-->
                    <div class="social-timeline-content-image">
                        <div class="row">
                            <div class="col-xs-12 post-display-img">
                                <?php $extension = pathinfo($data["post_details"]["img_file_name"], PATHINFO_EXTENSION);

                                //CDN image path
                                $imageFileOfCDN = $cdnSocialUrl . 'users/' . $data["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $data["_id"] . '_' . $data["created_at"] . '_' . $data["post_details"]["img_file_name"] ;

                                //server image path
                                $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $data["_id"] . '_' . $data["created_at"] . '_' . $data["post_details"]["img_file_name"] ;

                                // check for image is avilable on CDN
                                $file = $imageFileOfCDN;
                                $file_headers = @get_headers($file);
                                if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    $finalImagePath = $imageFileOfLocal;
                                }
                                else {
                                    $finalImagePath = $imageFileOfCDN;
                                }

                                ?>

                                <img src="<?php echo $finalImagePath;  ?>" class="img-responsive" />

                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-image-->
                <?php break;
            case 'status':
                ?>
                <div class="modal--show--post" data-postid="<?php echo $data['_id']; ?>" data-createdat="<?php echo $data['created_at']; ?>" data-searchcount="" data-ipaddr="" data-emailid="<?php echo   $emailId; ?>" data-usersearchtype="">
                    <!--social-timeline-status-->
                    <div class="social-timeline-user-status half innerMT">
                        <div class="row border-top border-bottom">
                            <div class="col-xs-12">
                                <div class="user-status-content  half innerTB">
              <span class="text-dark-gray">
                <?php
                // $statusData = mb_strlen($shortDescription) > $validateCharCountOnSocial + $counterText ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $counterText) . "<a href='javascript:;'><span class='text-blue'> ....See More</span></a>" : $shortDescription;
                echo $shortDescData;
                ?>
              </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-status-->
                <?php break;
            }
            ?>
            <!--end of switch case-->

            <!--post floor-->
            <div class="social-timeline-earning-comments-view innerT">
                <div class="row">
                    
                    <div class="col-xs-5">
                        <div>
                            <label>Earning : </label>
                            <span>
                                                                <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>

                            </span>
                        </div>
                    </div>
                    <div class="col-xs-7">
                            <div class="">
                                <ul class="list-inline pull-right margin-bottom-none">
                                    <li>
                  <div>
                    <?php if (!empty($like_count)) { ?>
                      <a id="like-count<?php echo $data["_id"]; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data["_id"]; ?>', '<?php echo $data["created_at"]; ?>','0', '30' ,'<?php echo $like_count; ?>');">
                      <span class="half innerR like-count<?php echo $data["_id"]; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                    <?php } else { ?>
                      <a id="like-count<?php echo $data["_id"]; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data["_id"]; ?>', '<?php echo $data["created_at"]; ?>', '0', '30','<?php echo $like_count;?>');">
                      <span class="half innerR like-count<?php echo $data["_id"]; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                    <?php }  ?>
                    <label class="pull-right">Likes</label>
                  </div>
                </li>
                                    <li>
                                        <div>
                                            <span class="half innerR comment-count<?php echo $data['_id']; ?>"><?php echo formatNumberToSort("{$commentCount}", 0); ?></span>
                                            <label class="pull-right">Comments</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0); ?></span>
                                            <label>Shares</label>
                                        </div>
                                    </li>
                                    <!-- <li class="padding-right-none">
                                        <div>
                                            <span class=""><?php //echo formatNumberToSort("{$data['post_views_count']}", 0); ?></span>
                                            <label>Views</label>
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-earning-comments-view-->

                <!--social-timeline-likes-dislikes-->
                <div class="social-timeline-likes-dislikes">
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="social-likes-dislikes" >
                                <ul class="list-inline margin-bottom-none">
                                    <li>
                                        <div>
                                            <a href="JavaScript:void(0);" class="like-area">
                      <?php
                                            if($getPostCdpStatus == 1) { ?>
                                              <i id="thumbs-up<?php echo $data["_id"]; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } elseif ($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) { ?>
                                            <i id="thumbs-up<?php echo $data["_id"]; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } else {
                                            if ($userId != $data["posted_by"]) { ?>
                                            <i id="thumbs-up<?php echo $data["_id"]; ?>" class="likes-click fa fa-thumbs-o-up half innerR" onclick="socialCDPForPost(this,'<?php echo $data["_id"]; ?>', '<?php echo $data["created_at"]; ?>', '<?php echo $data["post_type"]; ?>', '<?php echo $data["user_ref"]["account_handle"]; ?>', '<?php echo $data["posted_by"]; ?>', '', '<?php echo $searchCount; ?>', '<?php echo $ipAddr; ?>', '<?php echo $totalKeywords; ?>','<?php echo $data["user_ref"]["email"]; ?>');"></i>
                                            <?php } else {?>
                                            <i id="thumbs-up<?php echo $data["_id"]; ?>" class="likes-click fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php }
                                          }?>
                                                </a>                                          
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="social-comments-shares-views">
                                <ul class="list-inline pull-right margin-bottom-none">
                                    <li>
                                        <div>
                                            <a onclick = "commentDivShow('<?php echo $data["_id"];?>');">
                                                <i class="fa fa-comments-o"></i>
                                                <span>Comments</span>
                                            </a>
                                        </div>
                                    </li>
                                    <li class="padding-right-none">
                                        <div>
                                            <a data-toggle="modal" onclick = "sharePostModal('<?php echo $data["_id"]; ?>','<?php echo $data["created_at"]; ?>','<?php echo $data["post_type"];?>','<?php echo $data["user_ref"]["account_handle"];?>', 'create','<?php echo $data["user_ref"]["email"];?>');">
                                                <i class="fa fa-share-square-o"></i>
                                                <span>Share</span>
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="innerLR">
                <input type="hidden" id="comment-hidden-enter-event-<?php echo $data['_id']; ?>" execute-enetr-event="true" execute-enter-event-main-page="true">
                <div id="comment-include-<?php echo $data['_id']; ?>" class="comment-append-section comments-module bg-light-purple row" style="display:none;">
                    <div class="col-xs-12">
                        <?php
                    $commentPostId = $data["_id"];
                    $commentPostCreationTime = $data["created_at"];
                    $commentPostCreator = $data['user_ref']['email'];
                    include ('postPageCommentWidget.php');
                  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end of post floor-->
  </div>
      <!--comment div-->
</div>
</div>
  <!--social-card-->
<!--social-timeline-image-->
<!--if shared post then-->
<?php } else {  ?>
  <?php include("sharePostContent.php"); ?>
<!--if closing-->
<?php } ?>
<?php
  }
    }
    if (count($postData["errMsg"]) < 11) {
        echo "<center>" . "You joined us at " . date("jS F Y", ($_SESSION["userJoinAt"] / 1000)) . "</center>";
        $dataEmptyFlag = "true";
    }
    ?>

    <div id='private-last-data-time-<?php echo $dataCount; ?>' lastDataTime = '<?php echo $lastDataTime; ?>' data-empty='<?php echo $dataEmptyFlag; ?>' style="display:none;"></div>
    <!-- foreach closing -->

</div>
<script>
    $('.modal--show--post').on('click', function(event) {
    // event.preventDefault();
    /**
     * to ignore modal popup on child "a" tag of a container for which we want modal popup.
     */
    if (event.target.nodeName != "A") {

    var postid, createdat, searchcount, ipaddr, emailid, usersearchtype;
    postid         = $(this).data("postid");
    createdat      = $(this).data("createdat");
    searchcount    = $(this).data("searchcount");
    ipaddr         = $(this).data("ipaddr");
    emailid        = $(this).data("emailid");
    usersearchtype = $(this).data("usersearchtype");
    showModalPost(postid, createdat, searchcount, ipaddr, emailid, usersearchtype) ;}
  }); 
</script>
<!--====  End of include : otherTimelineContent  ====-->

<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>