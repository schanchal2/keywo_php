<!--===============================================
=            include :leftInfoUser.php            =
================================================-->
<?php //echo "Nikhil"; die;
$socialFileUrlShare = basename($_SERVER['PHP_SELF'], ".php"); //echo $socialFileUrlShare; die;
if ($socialFileUrlShare == "otherTimeline") {
$email = $otherEmail;
} elseif($socialFileUrlShare == "otherFollowersList") {
$email = base64_decode($_GET["email"]);
}else {
$email = $_SESSION["email"];
}
//get user info
$emailofSpecialUser = array();
$postFields = "short_description,followings,followers,post_count,profile_pic,first_name,last_name,creationTime,account_handle,user_id";
$userDetail = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $postFields);
//set user joining date in session
$_SESSION["userJoinAt"] = $userDetail['errMsg']['creationTime'];
if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare == "otherFollowersList") {
$firstName = $userDetail['errMsg']["first_name"];
$lastName  = $userDetail['errMsg']["last_name"];
} else {
$firstName = $_SESSION["first_name"];
$lastName  = $_SESSION["last_name"];
}
if(isset($userDetail["errMsg"]["profile_pic"]) && !empty($userDetail["errMsg"]["profile_pic"])){
global $cdnSocialUrl;
global $rootUrlImages;
$extensionUP  = pathinfo($userDetail["errMsg"]["profile_pic"], PATHINFO_EXTENSION);
//CDN image path
$imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $userDetail["errMsg"]["account_handle"] . '/profile/' . $userDetail["errMsg"]["account_handle"] . '_' . $userDetail["errMsg"]["profile_pic"] . '.70x70.' . $extensionUP;
//server image path
$imageFileOfLocalUP = $rootUrlImages.'social/users/'.$userDetail["errMsg"]["account_handle"].'/profile/'.$userDetail["errMsg"]["account_handle"].'_'.$userDetail["errMsg"]["profile_pic"];
// check for image is available on CDN
$file = $imageFileOfCDNUP;
$file_headers = @get_headers($file);
if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
$imgSrc = $imageFileOfLocalUP;
} else {
$imgSrc = $imageFileOfCDNUP;
}
}else{
$imgSrc = $rootUrlImages."default_profile.jpg";
}
$fileName = basename($_SERVER['SCRIPT_NAME'], ".php");
if ($fileName == 'otherTimeline') {
$getSpecialUserData = getSpecialUserPost();
}
if(noError($getSpecialUserData)){
$getSpecialUserData = $getSpecialUserData["errMsg"];
}
foreach($getSpecialUserData as $specialUser){
$emailofSpecialUser[] = $specialUser['email'];
}
?>
<div class="card my-info-card social-card">
  <div class="clearfix innerAll padding-bottom-none">
    <div class="my-info-img pull-left innerMR">
      <img class="img-responsive" src="<?php echo  $imgSrc; ?>"/>
    </div>
    <!-- my-picture  -->
    <div class="my-info-detail pull-left">
      <div class="my-info-name-container text-blue innerMB">
        <!-- my-name  -->
        <span class="my-info-name ellipses-wid ellipses"><?php echo $firstName . ' ' . $lastName ?></span>
        <?php if ($socialFileUrlShare != "otherTimeline" && $socialFileUrlShare != "otherFollowersList") { ?>
        <a class="my-info-name-edit pull-right">
          <i class="fa fa-pencil" aria-hidden="true"></i>
        </a>
        <?php } ?>
      </div>
      <!-- my-status  -->
      <div class="my-info-status">
        <?php if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare == "otherFollowersList") {
        if (!empty($userDetail['errMsg']['short_description'])) {
        echo $userDetail['errMsg']['short_description'];
        } else {
        echo "<div class='editable-empty editable-pre-wrapped'>No status..</div>";
        }
        } else { ?>
        <a id="my-info-name-edit"
          data-type="textarea"
          title="Change Description"
          data-pk="1"
          data-placement="right"
          data-title="Enter username" data-container="body">
          <?php`
          echo $userDetail['errMsg']['short_description'];
          ?>
        </a>
        <?php } ?>
      </div>
    </div>
  </div>
  <!-- my-info-detail  -->
  <div class="clearfix half innerT">
    <div class="col-xs-12  innerB">
      <div class="col-xs-4 padding-none pull-left">
        <?php   if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare=="otherFollowersList") {  ?>
        <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($email); ?>">
          <?php } else { ?>
          <a href="../../views/social/privateTimeline.php">
            <?php } ?>
            <div class="text-deep-sky-blue my-info-posts">
              Posts
            </div>
            <div class="text-blue innerL my-info-posts-count">
              <?php echo $userDetail['errMsg']['post_count']; ?>
            </div>
          </a>
        </div>
        <?php  if (!in_array($email, $emailofSpecialUser)) { ?>
        <div class="col-xs-4 padding-none">
          <?php if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare=="otherFollowersList") {  ?>
          <a href="../../views/social/otherFollowersList.php?type=followings&email=<?php echo base64_encode($email); ?>">
            <?php } else { ?>
            <a href="../../views/social/followers.php?type=followings">
              <?php } ?>
              <div class="text-deep-sky-blue my-info-following">
                Following
              </div>
              <?php   if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare=="otherFollowersList") {  ?>
              <div class="text-blue my-info-following-count inner-2x innerL">
                <?php echo $userDetail['errMsg']['followings']; ?>
              </div>
              <?php } else { ?>
              <div class="text-blue my-info-following-count inner-2x innerL"  id="followingCount">
                <?php echo $userDetail['errMsg']['followings']; ?>
              </div>
              <?php } ?>
            </a>
          </div>
          <div class="col-xs-4 padding-none text-right">
            <?php   if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare=="otherFollowersList") {  ?>
            <a href="../../views/social/otherFollowersList.php?type=followers&email=<?php echo base64_encode($email); ?>">
              <?php   }else{?>
              <a href="../../views/social/followers.php?type=followers">
                <?php } ?>
                <div class="text-deep-sky-blue my-info-followers">
                  Followers
                </div>
                <div class="text-blue my-info-followers-count inner-2x innerR"  id="followerCount">
                  <?php echo $userDetail['errMsg']['followers']; ?>
                </div>
              </a>
            </div>
            <?php } ?>
          </div>
          <?php
          if ($socialFileUrlShare == "otherTimeline" || $socialFileUrlShare == "otherFollowersList") {
          $followFlag              = "false";
          $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
          $getFollowingDetails     = $targetDirAccountHandler . $_SESSION["account_handle"] . "_info_" . strtolower($userDetail['errMsg']['account_handle'][0]) . ".json";
          if (file_exists($getFollowingDetails)) {
          $jsondataFollowing = file_get_contents($getFollowingDetails);
          $dataFollowing     = json_decode($jsondataFollowing, true);
          if (in_array($userDetail['errMsg']['user_id'], $dataFollowing["followings"])) {
          $followFlag = "true";
          }
          }
          ?>
          <?php  //if (!in_array($email, $emailofSpecialUser)) { ?>
          <div class="col-xs-12 half innerB">
            <input type="button" id ='<?php echo $userDetail["errMsg"]["account_handle"]; ?>' class="btn-social<?php if($followFlag=='true'){echo "-dark";} ?> pull-right follow-unfollow-button otherFollowBtn" onclick="ajaxAppFollowEvent('<?php echo $userDetail["errMsg"]["account_handle"]; ?>','<?php echo $email; ?>');" value = '<?php if($followFlag=='true'){echo "Unfollow";} else {echo "Follow";}?>'>
          </div>
          <?php //} ?>
          <!-- class="btn-trading-wid-auto pull-right boxshadow" -->
          <?php } ?>
        </div>
        <!-- posts-followers-following  -->
      </div>
      <!--====  End of include :leftInfoUser.php  ====-->