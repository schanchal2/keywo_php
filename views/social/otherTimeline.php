<?php
	session_start();
	require_once('../../config/config.php');
	require_once('../../config/db_config.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../models/social/socialModel.php');
    require_once('../../models/social/commonFunction.php');
	require_once ('../../helpers/deviceHelper.php');
	require_once ('../../helpers/arrayHelper.php');
	require_once('../../IPBlocker/ipblocker.php');
	require_once("../../models/social/commonFunction.php");

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email             = $_SESSION["email"];
	  $otherEmail        = base64_decode($_REQUEST["email"]);
	  $otherEmailId      = $_REQUEST["email"];
		$otherType         = $_REQUEST["type"];
		$blockFlag         = false;
		$blockFlagForOther = false;


		$postFields         = "short_description,followings,followers,post_count,profile_pic,first_name,last_name,creationTime,account_handle,user_id";
	  $otherUserDetails   = getUserInfo($otherEmail, $walletURLIPnotification . 'api/notify/v2/', $postFields);
	  if(noError($otherUserDetails)){
	    $postedBy           = $otherUserDetails["errMsg"]["user_id"];
	    $otherAccHandle     = $otherUserDetails["errMsg"]["account_handle"];
	    $viewType           = "other";
	    $sessionUserId      = $_SESSION["id"];
	    $postData           = getUserTimelinePost($postedBy, $type, $lastDataCreateTime, $viewType, $otherAccHandle, $emailId, $sessionUserId);
	    $dataEmptyFlag      = "false";
	    $bulkData           = array();
	    $blockFlag          = false;
	  }else{
	    $rootForRedirection =  $rootUrl."views/social/not_found_error.php";
	  	print("<script>");
	  	print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
	  	print("</script>");
	  }

		$user_id="user_id,account_handle";
		$getUserInfo = getUserInfo($otherEmail,$walletURLIPnotification.'api/notify/v2/',$user_id);
		if(noError($getUserInfo)){
			$getUserInfo    = $getUserInfo["errMsg"];
			$otherUserId    = $getUserInfo["user_id"];
			$otherAccHandle = $getUserInfo["account_handle"];
 		}
		// Check For blocked User
		$targetDirAccountHandler = "{$docRoot}json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

        $userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);
        if (in_array($otherUserId,$userIdsOfAccHandler)) {
            $blockFlagStatus = 1;
        }else{
            $blockFlagStatus = 0;
        }

        if($blockFlagStatus == 1 ){
            echo '<script type="text/javascript">
	window.location = "not_found_error.php"
	</script>';
            exit;
        }

        ?>
        
<main class="social-main-container inner-7x innerT" >
<div class="container row-10">
<div class="row">
	<div class="col-xs-3  social-right-position">
		<div class="social-left-panel">
			<?php include("leftInfoUser.php"); ?>
			<div class="inner-2x innerMT">
				<?php include("personalDetail.php"); ?>
			</div>
			<div class="inner-2x innerMT">
				<?php include("personalWorkDetail.php"); ?>
			</div>
			<!-- my-info-card  -->
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-6">
		<div class="social-center-panel">

				<!-- =================== social-all-status-tabs ================== -->
					<div class="social-all-status-tabs">
						<div class="row">
							<div class="col-xs-12">
								<ul class="list-inline margin-bottom-none">
									<li>
										<a id="all" href="#all" class="social-status-tabs">ALL</a>
									</li>
									<li>
										<a id="status" href="#status" class="social-status-tabs">STATUS</a>
									</li>
									<li>
										<a id="blog" href="#blog" class="social-status-tabs">BLOG</a>
									</li>
									<li>
										<a id="video" href="#video" class="social-status-tabs">VIDEO</a>
									</li>
									<li>
										<a id="image" href="#image" class="social-status-tabs">IMAGE</a>
									</li>
									<li>
										<a id="audio" href="#audio" class="social-status-tabs">AUDIO</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- social-all-status-tabs -->
					<!--display content-->
					<div id="show-error-message"></div>
					<!--<div id = "otherPostType" type = '<?php //if (!empty($otherType)) { echo $otherType; } ?>'></div>-->
					<div id="private-next-post-data" style="display:none;" data-count="0" data-type="<?php if (!empty($otherType)) { echo $otherType; } else { echo 'all';} ?>" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
					<div id="private-post-data">
						<!--display loading content div before data is loaded-->
						<div id="loading-content-div"><?php include('loadingContent.php'); ?></div>
						<!--end of loading content div-->
						<!--content from ajax-->
				</div>
				<div id = "ajaxLoader1" class="text-center" style="display:none; ">
					<img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
				</div>
				<!--display content-->
		</div>
		<!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

	<div class="col-xs-3 social-right-position">
		<div class="social-right-panel">
			<div id = "suggested-user-details">
				<?php include("suggestedUser.php"); ?>
			</div>
			<div class="followed-keywords-module inner-2x innerMTB">
				<?php include("followedKeyword.php"); ?>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->
</div>
</div>
<!-- container -->
</main>


<!--share-post-modal-->
<div class="modal fade" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body-share-post">

			</div>
		</div>
	</div>
</div>
<!--end-share-post-->

<!--modal to display editing data-->
<div class="modal fade" id="editPostDetails" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body">

			</div>
		</div>
	</div>
</div>


<!--modal to display editing data-->
<div class="modal fade row-10" id="showModalPost" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body-social-user-image">

			</div>
		</div>
	</div>
</div>

<!-- Likes List Modal -->
<div id="myModal_like" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal">
            <div class="" id="likeUserList">

            </div>
        </div>
    </div>
</div>


<!--modal to report User-->
<div class="modal fade" id="showReportDetails" role="dialog">
	<div class="modal-dialog">
	    <div class="card social-card clearfix social-form-modal">
	        <div class = "modal-body-report-user">

	        </div>
	    </div>
	</div>
</div>


<!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>


<!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>


<!--modal to show Post is Deleted User-->
<div class="modal fade" id="postDeletedModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class = "modal-body-post-deleted">

			</div>
		</div>
	</div>
</div>

<!--social-timeline-modal-on-block-post-->
    <div class="modal fade" id="blockUserAcc" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <!-- Modal content-->
                <div class="">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-blue">Block</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to Block Account?
                    </div>
                    <div class="modal-footer border-none">
                        <button type="button" class="btn btn-social-wid-auto yes-remove-block-btn" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<!-- else part if session expires -->
	<?php
	} else {
			header('location:'. $rootUrl .'../../views/prelogin/index.php');
	}
	?>


<?php include('../layout/social_footer.php'); ?>
<!-- Blog Form starts-->
<link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />

<link href="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">

<script>
	var bottom         = $(document).height() - $(window).height(); //for endless scrolling
	var successflag    = 0;
	var otherUserId    = "<?php echo $otherUserId ;?>";
	var otherAccHandle = "<?php echo $otherAccHandle; ?>";
	var otherEmail     = "<?php echo $otherEmailId; ?>";

	$(document).scroll(function(){
		/*
		For endless scrolling
		*/
		var win = $(window);
		// Each time the user scrolls
		win.scroll(function() {
			// End of the document reached?
			if ($(document).height() - win.height() == win.scrollTop()) {
				// Do the stuff
				if(successflag == 0){
					var privateScrollAllow = $("#private-next-post-data").attr("data-scroll-allow");
					var privateDataEmpty = $("#private-next-post-data").attr("data-status-empty");
					var privateDataType = $("#private-next-post-data").attr("data-type");
					if (privateScrollAllow == "true" && privateDataEmpty == "false") {
						$('#ajaxLoader1').show();
						loadOtherTimeline(privateDataType, otherEmail);
					}
					successflag = 1;
				}
			}else{
				// console.log("else");
				successflag = 0;
			}
		});
	});

  //--------------------------      Confirmation to block User-----------------------------//
  function blockUserAccByConfirmation(ele,email,postId, postType, postCreatedAt) {
    $('#showModalPost .close-dialog').click();
      $(ele).css('pointer-events','none');
    $("#blockUserAcc").modal("show");
      emailId         = email;
      elem            = ele;
      blockPostId        = postId;
      blockPostType      = postType;
      blockPostCreatedAt = postCreatedAt;
      emailId            = email;
    }
    $("#blockUserAcc .yes-remove-block-btn").click(function(){
      $("this").prop("disabled", "disabled");
      removePersonalPostForBlockedUser(elem,emailId,blockPostId, blockPostType, blockPostCreatedAt);
      $('#blockUserAcc').modal('hide');
    })
</script>
