
<?php
require_once "../../models/social/commonFunction.php";
if (isset($_POST['postType'])) {
    session_start();
    /* Add Required Files */
    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/deviceHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/coreFunctions.php";
    require_once "../../helpers/imageFunctions.php";
    require_once "../../models/social/socialModel.php";
    require_once "../../models/user/authenticationModel.php";

    $popularPostType = cleanXSS(urldecode($_POST['postType']));


} else {
	$popularPostType = 'blog';
}
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	//session is not active, redirect to login page
	print("<script>");
	print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	print("</script>");
	exit();
	die();
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

global $cdnSocialUrl;
global $rootUrlImages;


$listCount = "";
//get blocked array
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);

//get popular post
$getPopularPostDetails = getPopularPostDetails($popularPostType);
$final = array();
if (noError($getPopularPostDetails)) {
	$getPopularPostDetails = $getPopularPostDetails['errMsg'];
switch ($popularPostType) {
	case 'blog':
  $listCount = 0;
  echo '<div id="popular-blog-module">';
		foreach ($getPopularPostDetails as $key => $value) {

      if(!in_array($value['post_id']['posted_by'],$userIdsOfAccHandler)){
        $listCount = $listCount + 1;
        if ($listCount > 5) {
          break;
        }
          $final       = $value['post_id']['post_details'];      		
      		if (!empty($value['post_id']["post_details"]["img_file_name"])) { 
      			$extension = pathinfo($value['post_id']['post_details']['img_file_name'], PATHINFO_EXTENSION);

		        //CDN image path
		        $imageFileOfCDN = $cdnSocialUrl . 'users/' . $value['post_id']['user_ref']['account_handle'] . '/post/blog/featured/' . $extension . '/' . $value['post_id']['_id'] . '_' . $value['post_id']['created_at'] . '_' . $value['post_id']['post_details']['img_file_name'];

		        //server image path
		        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $value['post_id']['user_ref']['account_handle'] . '/post/blog/featured/' . $extension . '/' . $value['post_id']['_id'] . '_' . $value['post_id']['created_at'] . '_' . $value['post_id']['post_details']['img_file_name'];

		        // check for image is avilable on CDN
		        $file = $imageFileOfCDN;
		        $file_headers = @get_headers($file);
		        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
		        	$finalImagePath = $imageFileOfLocal;
		        }
		        else {
		        	$finalImagePath = $imageFileOfCDN;
		        }
      		} else {
      			$finalImagePath = $rootUrlImages .'social/popular_blog.png';
      		}

?>
			
				<div class="row margin-none blog-module border-bottom" >
					<div class="row margin-none innerT" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['post_id']['user_ref']['email']);?>");'>
						<div class="col-xs-4">
							
								<img class="popular_blog_img" src="<?php echo $finalImagePath;  ?>" />
							
						</div>
						<div class="col-xs-8">
							
								<span class="ellipsis-multiline text-black l-h15">
									<?php
						                $postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_details"]["blog_title"]);
						                $postDescCount = strlen( $postDesc);
						                if ($postDescCount >= 50) {
						                  $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postDescText = $postDesc;
						                }
						                echo rawurldecode($postDescText);
						            ?>
								</span>
							
						</div>
					</div>
					<div class="row margin-none innerT">
						<div class="col-xs-8 text-black">
							<label>Earning : </label>							
							<span>
							                                 <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$value['post_id']['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_id']['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_id']['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
							</span>
						</div>
						<div class="col-xs-4" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
							<a class="text-social-primary pull-right"><?php echo $value['post_id']["post_views_count"] . ' Views';?></a>
						</div>
					</div>
				</div>
<?php }
// printArr(($key));
// $key++;

// if($key>2){
//   exit;
// }
}	?>
<?php
		break;
	case 'video':
	echo '<div class="" id="popular-video-module">';
		foreach ($getPopularPostDetails as $key => $value) {

      if(!in_array($value['post_id']['posted_by'],$userIdsOfAccHandler)){
        $listCount = $listCount + 1;
        if ($listCount > 5) {
          break;
        }

?>

		<!--=============popular-video-module===============-->
		
			<div class="row margin-none video-module border-bottom" >
				<div class="row margin-none innerT" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['post_id']['user_ref']['email']);?>");'>
					<div class="col-xs-4">
                        <?php
                        if(isset($value['post_id']["post_details"]["video_thumbnail"]) && !empty($value['post_id']["post_details"]["video_thumbnail"])){
                            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $value['post_id']["user_ref"]["account_handle"] . '/post/video/' . $value['post_id']["post_details"]["video_thumbnail"];

                            //server image path
                            $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$value['post_id']["user_ref"]["account_handle"].'/post/video/'.$value['post_id']["post_details"]["video_thumbnail"];

                            // check for image is available on CDN
                            $file = $imageFileOfCDNUP;
                            $file_headers = @get_headers($file);
                            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                $imgSrc = $imageFileOfLocalUP;
                            } else {
                                $imgSrc = $imageFileOfCDNUP;
                            }
                        }
                        ?>
							<img class="popular_blog_img" src="<?php echo $imgSrc; ?>" />
						
					</div>
					<div class="col-xs-8">
						
							<span class="ellipsis-multiline text-black l-h15">
                                <?php
                                if(isset($value['post_id']['post_details']['video_desc'])){
                                    $videoTitle = $value['post_id']['post_details']['video_desc'];
                                }

                                if (empty($value['post_id']['post_short_desc'])  && !(empty($videoTitle))) {
                                    $postDesc = $videoTitle;
                                } else if (!empty($value['post_id']['post_short_desc'])) {
                                    $postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']['post_short_desc']);
                                } else {
                                    $postDesc = 'Title Unavailable';
                                }
                                $postDescCount = strlen( $postDesc);
                                if ($postDescCount >= 50) {
                                    $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                                } else {
                                    $postDescText = $postDesc;
                                }
                                echo rawurldecode($postDescText);
                                ?>

							</span>
					
					</div>
				</div>
				<div class="row margin-none innerT">
					<div class="col-xs-7 text-black">
						<span>
							<span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value['post_id']['post_earnings']} ".$keywoDefaultCurrencyName; ?>" class="currency">
								<?= $value['post_id']['post_earnings'] >= 0?formatNumberToSort("{$value['post_id']['post_earnings']}", 2):0 ; ?> <?php echo $keywoDefaultCurrencyName; ?>
							</span>
						</span>
					</div>
					<div class="col-xs-5" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
						<a class="text-social-primary pull-right"><?php echo $value['post_id']["post_views_count"] . ' Views';?></a>
					</div>
				</div>
			</div>
		<?php } } ?>
<?php
		break;
	case 'image':
	echo '<div id="popular-image-module">';
		foreach ($getPopularPostDetails as $key => $value) {
      	if(!in_array($value['post_id']['posted_by'],$userIdsOfAccHandler)){
	        $listCount = $listCount + 1;
	        if ($listCount > 5) {
	          break;
        	}
		$extension = pathinfo($value['post_id']['post_details']['img_file_name'], PATHINFO_EXTENSION);

        //CDN image path
        $imageFileOfCDN = $cdnSocialUrl . 'users/' . $value['post_id']['user_ref']['account_handle'] . '/post/images/' . $extension . '/' . $value['post_id']['_id'] . '_' . $value['post_id']['created_at'] . '_' . $value['post_id']['post_details']['img_file_name'];

        //server image path
        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $value['post_id']['user_ref']['account_handle'] . '/post/images/' . $extension . '/' . $value['post_id']['_id'] . '_' . $value['post_id']['created_at'] . '_' . $value['post_id']['post_details']['img_file_name'];

        // check for image is avilable on CDN
        $file = $imageFileOfCDN;
        $file_headers = @get_headers($file);
        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
        	$finalImagePath = $imageFileOfLocal;
        }
        else {
        	$finalImagePath = $imageFileOfCDN;
        }

		// 	// printArr($value);

?>

		<!--============= popular-image-module ===============-->
		
			<div class="row margin-none image-module border-bottom" >
				<div class="row margin-none innerTB" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['post_id']['user_ref']['email']);?>");'>
					<div class="col-xs-4">
						
							<img class="popular_blog_img" src="<?php echo $finalImagePath; ?>" />

							<!--<img class="popular_blog_img" src="<?php echo $cdnSocialUrl . "users/" . $value['post_id']["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $value['post_id']["_id"] . '_' . $value['post_id']["created_at"] . '_' . $value['post_id']["post_details"]["img_file_name"];?>" />-->
						
					</div>
					<div class="col-xs-8">
					
							<span class="ellipsis-multiline text-black l-h15">
								<?php
								if (!empty($value['post_id']["post_short_desc"])) {
									$text = $value['post_id']["post_short_desc"];
								} else {
									$text = "See " . $value['post_id']['user_ref']['first_name'] . " " . $value['post_id']['user_ref']['last_name'] . "'s image which is trending now on Keywo";
								}
					                $postDesc = preg_replace('/<\/?a[^>]*>/','',$text);
					                $postDescCount = strlen( $postDesc);
					                if ($postDescCount >= 50) {
					                  $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
					                } else {
					                  $postDescText = $postDesc;
					                }
					                echo rawurldecode($postDescText);
					            ?>
							</span>
					
					</div>
				</div>
				<div class="row margin-none innerT">
					<div class="col-xs-7 text-black">
						<span>
							<span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value['post_id']['post_earnings']} ".$keywoDefaultCurrencyName; ?>" class="currency">
								<?= $value['post_id']['post_earnings'] >= 0?formatNumberToSort("{$value['post_id']['post_earnings']}", 2):0 ; ?> <?php echo $keywoDefaultCurrencyName; ?>
							</span>
						</span>
					</div>
					<div class="col-xs-5" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
						<a class="text-social-primary pull-right"><?php echo $value['post_id']["post_views_count"] . ' Views';?></a>
					</div>
				</div>

			</div>
		<?php } } ?>
<?php
		break;
	case 'audio':
	echo '<div id="popular-sound-module">';
	foreach ($getPopularPostDetails as $key => $value) {
    if(!in_array($value['post_id']['posted_by'],$userIdsOfAccHandler)){
      $listCount = $listCount + 1;
      if ($listCount > 5) {
        break;
      }
		// printArr($value);
        if(isset($value['post_id']['post_details']['img_file_name'])){
            $ext         = explode(".",$value['post_id']['post_details']['img_file_name']);
        }

?>

		<!--============= popular-sound-module ===============-->
		
			<div class="row margin-none sound-module border-bottom" >
				<div class="row margin-none innerT" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['post_id']['user_ref']['email']);?>");'>
					<div class="col-xs-4">
						<img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png" />
					</div>
					<div class="col-xs-8">
						
						<span class="ellipsis-multiline text-black l-h15">
							<?php
							if (!empty($value['post_id']["post_short_desc"])) {
									$text = $value['post_id']["post_short_desc"];
								} else {
									$text = "Listen to " . $value['post_id']['user_ref']['first_name'] . " " . $value['post_id']['user_ref']['last_name'] . "'s audio clip which is trending now on Keywo";
								}
				                  $postDesc = preg_replace('/<\/?a[^>]*>/','',$text);
				                  $postDescCount = strlen( $postDesc);
				                  if ($postDescCount >= 50) {
				                    $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
				                  } else {
				                    $postDescText = $postDesc;
				                  }
				                  echo rawurldecode($postDescText);
				                ?>
						</span>
						
						<span class="" style="display:block;">
							<!-- By Doors Down -->
						</span>
					</div>
					<div class="col-xs-3">
						<span class="text-gray">
							<!-- <span>3.45 mins</span> -->
						</span>
					</div>
				</div>
				<div class="row margin-none innerT">
					<div class="col-xs-7 text-black">
						<span>
							<span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value['post_id']['post_earnings']} ".$keywoDefaultCurrencyName; ?>" class="currency">
								<?= $value['post_id']['post_earnings'] >= 0?formatNumberToSort("{$value['post_id']['post_earnings']}", 2):0 ; ?> <?php echo $keywoDefaultCurrencyName; ?>
							</span>
						</span>
					</div>
					<div class="col-xs-5" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
						<a class="text-social-primary pull-right"><?php echo $value['post_id']["post_views_count"] . ' Views';?></a>
					</div>
				</div>
			</div>
		<?php } } ?>


<?php
		break;
	case 'status':
		echo '<div id="popular-blog-module">';
		foreach ($getPopularPostDetails as $key => $value) {
      if(!in_array($value['post_id']['posted_by'],$userIdsOfAccHandler)){
        $listCount = $listCount + 1;
        if ($listCount > 5) {
          break;
        }

        if(isset($value['post_id']['post_details']['img_file_name'])){
            $ext         = explode(".",$value['post_id']['post_details']['img_file_name']);
        }
			// printArr($value);

?>
			
				<div class="row margin-none blog-module border-bottom" >
					<div class="row margin-none innerT" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['post_id']['user_ref']['email']);?>");'>
					<div class="col-xs-4">
						<i class="fa  fa-edit widget-img-ico  media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>
					</div>
					<div class="col-xs-8">
						<span class="ellipsis-multiline text-black l-h15">
									<?php
						                $postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_short_desc"]);
						                $postDescCount = strlen( $postDesc);
						                if ($postDescCount >= 50) {
						                  $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postDescText = $postDesc;
						                }
						                echo rawurldecode($postDescText);
						            ?>
						</span>
					</div>
					</div>
					<div class="row margin-none innerT">
						<div class="col-xs-7 text-black">
							<span>
								<span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$value['post_id']['post_earnings']} ".$keywoDefaultCurrencyName; ?>" class="currency">
									<?= $value['post_id']['post_earnings'] >= 0?formatNumberToSort("{$value['post_id']['post_earnings']}", 2):0 ; ?> <?php echo $keywoDefaultCurrencyName; ?>
								</span>
							</span>
						</div>
						<div class="col-xs-5" onclick = 'loadPopularPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
							<a class="text-social-primary pull-right"><?php echo $value['post_id']["post_views_count"] . ' Views';?></a>
						</div>
					</div>
				</div>
<?php 	} }	?>
<?php
		break;
} // end of Switch case
?>
				<!-- <div class="row margin-none half innerTB border-bottom bg-view-all">
					<div class="col-xs-12">
						<a href="#" class="text-deep-sky-blue pull-right bg-view-all">View all</a>
					</div>
				</div> -->
		</div>

<?php
	} else if($getPopularPostDetails['errCode'] == 4) {
		echo "<div> <center>No data Found </center></div>";
	} else {
		echo "<div> <center>Unable to Load</center></div>";
	}//End of No error if else

?>
<!-- <script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script> -->
