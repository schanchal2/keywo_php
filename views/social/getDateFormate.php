<?php
if(isset($_POST['createdAt'])) {
    require_once ('../../config/config.php');
    require_once ($docRoot.'helpers/coreFunctions.php');
    require_once($docRoot.'helpers/coreFunctions.php');
    require_once($docRoot.'helpers/stringHelper.php');
    require_once($docRoot.'helpers/errorMap.php');
    require_once ('../../helpers/arrayHelper.php');
    if ($_POST['createdAt'] != '') {
        $mil = $_POST['createdAt'];
        $seconds = $mil / 1000;
        $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
        $timestamp2 = strtotime($created_at);
    }
}
$currentDate  = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s"));
$startDateFormat = new DateTime($created_at);
$EndDateFormat = new DateTime($currentDate);

$uDiff = ($startDateFormat->format('u') - $EndDateFormat->format('u')) / (1000 * 1000);
$diff = $startDateFormat->diff($EndDateFormat);
$day    = (int) $diff->format('%d') - $uDiff;
$second = (int) $diff->format('%s') - $uDiff;
$minute = (int) ($diff->format('%i'))- $uDiff; // convert minutes into seconds
$hours  = (int) ($diff->format('%h')) - $uDiff;; // convert hours into seconds
if($day <= 0){
    if($hours < 1)
    {
        if($minute==0)
        {
            if($second==0)
            {
                $timeToDisplay =  "Just now";
            }else{
                $timeToDisplay =   $second."  sec ";
            }
        }else{
            $timeToDisplay =   $minute."  min ";
        }
    }else {
        $timeToDisplay =  $hours."  hrs";
    }
}else {
    $timeToDisplay =  uDateTime("d F",$mil);
}

echo $timeToDisplay;

?>