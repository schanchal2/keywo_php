<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

$type                    = cleanXSS(trim(urldecode($_POST["type"])));
$jsonFileNo              = cleanXSS(trim(urldecode($_POST["jsonFileNo"])));
$jsonFileInnerNo         = cleanXSS(trim(urldecode($_POST["jsonFileInnerNo"])));
$divAppendId             = cleanXSS(trim(urldecode($_POST["divAppendId"])));
$jasonFileNo             = '';
$remaining               = '';
$pageScrollEnd           = 'true';
$Final                   = array();
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION['account_handle']."/";
$k                       = 0;
$fileOfFollowings = dirToArray($targetDirAccountHandler);
foreach($fileOfFollowings as $key => $fileOfFollowingsDetails)
{
    $Final = $targetDirAccountHandler.$fileOfFollowingsDetails;
    $jsonOfFollowings = file_get_contents($Final);
    $data = json_decode($jsonOfFollowings, true);
    if(isset($data[$type])){
        $a = count($data[$type]);
    }else{
        $a = 0;
    }

    if($a>0)
    {
        break;
    };
}
//$lastWordForFirst=substr($Final, -6);
//$firstWord = $lastWordForFirst[0];
$dataCount = "";
$useerIdCount = 0;
$setter = array();
$userIdsOfFollowings = array();
$endFlag = true;
$flag = true;
if (empty($jsonFileNo)) {
    $i = 0;
} else {
    $i = $jsonFileNo;
}

for ($i; $i < count($fileOfFollowings) && $endFlag; $i++ ) {
    $Final12= $targetDirAccountHandler.$fileOfFollowings[$i];
    $jsonOfFollowings12 = file_get_contents($Final12);
    $data12 = json_decode($jsonOfFollowings12, true);
    if(isset($data12[$type])){
        $dataIds = $data12[$type];
        $dataCount = count($data12[$type]);
    }else{
        $dataIds = array();
        $dataCount = 0;
    }
    $lastWord=substr($Final12, -6);
    $userIdsOfFollowings = array();

    $appended =  0;
    if (!empty($jsonFileInnerNo) && !empty($jsonFileNo) && $i == $jsonFileNo) {
        $k = $jsonFileInnerNo;
    } else {
        $k = 0;
    }
    for($k; $k < count($dataIds) && $flag; $k++) {
        if ( $useerIdCount < 15) {
            $useerIdCount = $useerIdCount+ count($dataIds[$k]);
            $userIdsOfFollowings[] = $dataIds[$k];
            $appended = $appended+1;
        } else {
            $flag = false;
            $endFlag = false;
            $lastWordJson = $fileOfFollowings[$i];
            $remaining = $appended;
            $jasonFileNo = $i;
        }
    }
    $word = $lastWord[0];
    if ($dataCount > 0){
        $geter = array( "count" => $dataCount, "user_id" => $userIdsOfFollowings);
        $setter[$word]= $geter;
    }
}
if ($i == count($fileOfFollowings) && $k == $dataCount) {
    $pageScrollEnd = 'false';
}
$jsonCOnverts              = json_encode($setter);
$user_required_info12      = 'first_name,last_name,account_handle,profile_pic,user_id,email';
$userDetailedInformation12 = getUserInfoForMultiUser($jsonCOnverts,$walletURLIPnotification.'api/notify/v2/',$user_required_info12);
$userDetailedInformation12 = $userDetailedInformation12['errMsg'];
//printArr($userDetailedInformation12);
if (empty($userDetailedInformation12) && $type == "followings") { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">Once you start following users, you can see them listed here.</h4>
    </div>
<?php } elseif (empty($userDetailedInformation12) && $type == "followers") { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">See the list of users who are following you, over here.</h4>
    </div>
<?php }
$s= 0;
foreach($userDetailedInformation12 as $key => $filesDetails) {
//    printArr($filesDetails);
    if (count($filesDetails['user_info']) > 0 ) {
        if ($s == 0) {
            if (empty($jsonFileInnerNo)) {
                ?>
                <div class="h4"> <?php if(!empty($divAppendId)){ echo strtoupper($key);} ?></div>
            <?php }
        } else {?>
            <div class="h4"> <?php echo strtoupper($key);?></div>
            <?php
        }
    }
    ?>
    <ul class="list-unstyled social-card">
        <?php
        foreach($filesDetails['user_info'] as $key1 => $fileOfFollowingsDetails) {
            if(isset($fileOfFollowingsDetails["profile_pic"]) && !empty($fileOfFollowingsDetails["profile_pic"])){
                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($fileOfFollowingsDetails["profile_pic"], PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $fileOfFollowingsDetails["account_handle"] . '/profile/' . $fileOfFollowingsDetails["account_handle"] . '_' . $fileOfFollowingsDetails["profile_pic"] . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$fileOfFollowingsDetails["account_handle"].'/profile/'.$fileOfFollowingsDetails["account_handle"].'_'.$fileOfFollowingsDetails["profile_pic"];

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                $imgSrc = $rootUrlImages."default_profile.jpg";
            }
        ?>
        <li class="innerTB clearfix">
            <div class="col-xs-5 ellipses ">
                <img class="img-circle" src="<?php echo $imgSrc; ?>">
                <a class="follower-name innerL half" <?php if($fileOfFollowingsDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($fileOfFollowingsDetails['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> title="<?php echo ucwords($fileOfFollowingsDetails['first_name']) ." ". ucwords($fileOfFollowingsDetails['last_name']);?>"><?php echo ucwords($fileOfFollowingsDetails['first_name']) ." ". ucwords($fileOfFollowingsDetails['last_name']);?> </a>
            </div>
            <div class="col-xs-3">
                <a class="follower-id" <?php if($fileOfFollowingsDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($fileOfFollowingsDetails['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> ><?php echo '@'.$fileOfFollowingsDetails['account_handle'];?></a>
            </div>
            <?php
            $followFlag = false;
            if ($type == 'followers') {
                $getFollowingDetails = $targetDirAccountHandler.$_SESSION['account_handle']."_info_" .$key.".json";
                if(file_exists($getFollowingDetails)) {
                    $jsonOfFollowings23 = file_get_contents($getFollowingDetails);
                    $data23 = json_decode($jsonOfFollowings23, true);
                    // printArr($data23);
                    if (in_array($fileOfFollowingsDetails['user_id'], $data23["followings"])) {
                        $followFlag = true;
                    }
                }
            } else {
                $followFlag = true;
            }
            ?>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">

                <input type = "button" id ='<?php echo $fileOfFollowingsDetails["account_handle"]; ?>' class="btn-social<?php if($followFlag){echo "-dark";} ?> pull-right follow-unfollow-button" onclick="ajaxAppFollowEvent('<?php echo $fileOfFollowingsDetails["account_handle"]; ?>','<?php echo $fileOfFollowingsDetails["email"]; ?>');" value = '<?php if($followFlag){echo "Unfollow";} else {echo "Follow";}?>'>

            </div>

            <?php }
            ?>
    </ul>
    <?php
    $s = $s+1;
}
?>

<div id="followingAppend<?php echo $divAppendId; ?>" jsonFileNo = "<?php echo $jasonFileNo; ?>" fileReadNo = "<?php echo $remaining; ?>" pageScrollEnd = "<?php echo $pageScrollEnd; ?>"></div>
