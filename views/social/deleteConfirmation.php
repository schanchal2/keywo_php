<?php
session_start();

	/* Add Required Files */
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/deviceHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../helpers/imageFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once "../../backend_libraries/xmlProcessor/xmlProcessor.php";

	$id = cleanXSS(urldecode($_POST['commentId'])); 
	$postId = cleanXSS(urldecode($_POST['postId'])); 
	$callType = cleanXSS(urldecode($_POST['callType'])); 
	$commentId = cleanXSS(urldecode($_POST['CommentReplyId']));
?>



<div class="modal-body">
	Are you sure you want to delete comment?
</div>

<div class="modal-footer">
	<input type="button" class="form-post-btn btn-default	" data-dismiss = "modal" value = "No">	 
	<input type="button" class="form-post-btn" onclick="callCommentDeleteEditFunction('<?php echo $id; ?>','<?php echo $postId; ?>','<?php echo $callType; ?>','<?php echo $commentId; ?>');" value = "Yes">
</div>