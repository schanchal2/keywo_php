<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../../layout/header.php");
$email      = $_SESSION["email"];

}

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_first_time.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<main class="container first_time_like_container inner-7x innerTB">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Like this post and earn 0.0025 Credit</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
			    <div class="card social-card right-panel-modules">
			        <div class="bg-light-gray right-panel-modules-head">
			            <div class="row margin-none">
			                <div class="col-xs-12">
			                    <h4 class="half innerMTB">Task List</h4>
			                </div>
			            </div>
			        </div>
			        <div class="suggested-module">
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Update Profile
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Follow 5 accounts (5)
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Like a post
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Create a post
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
						<div class="row innerTB half border-bottom">
						    <div class="col-xs-10">
						        <div class="">
						            Share a post
						        </div>
						    </div>
						    <div class=" col-xs-2">
						        <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
						    </div>
						</div>
			        </div>
			    </div>
			</div>
		</div>
		<div class="col-xs-6">
			<form action="">
				<div class="progress">
				  <div class="progress-bar" role="progressbar" style="width: 60%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<div class="social-card card">
					<div class="social-timeline-image innerMB">
					    <div class="social-card card half innerAll" id="social-timeline-image">
					        <div class="">
					            <div class="row half innerTB">
					                <div class="col-xs-8">
					                    <div class="social-timeline-profile-pic pull-left">
					                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
					                    </div>
					                    <div class="social-timeline-profile-details pull-left innerL">
					                        <div class="social-user-link-name">
					                            <a href="#" class="ellipses ellipses-general">
					                                    tom hanks
					                                </a>
					                        </div>
					                        <!---->
					                        <div class="social-user-link">
					                            <a href="#">
					                                    @tomhankennew
					                                </a>
					                        </div>
					                        <!--social-user-link-->
					                    </div>
					                    <!--social-timeline-profile-details-->
					                </div>
					                <div class="col-xs-4 social-post-options">
					                    <div class="dropdown pull-right">
					                        <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star-o"></i></span>
					                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					                            <span class="fa fa-chevron-down"></span>
					                        </a>
					                    </div>
					                    <br>
					                    <div class="pull-right social-post-time-container">
					                        <span class="social-post-time text-light-grey"><span>3</span> hrs</span>
					                    </div>
					                </div>
					            </div>
					            <!--row-->
					        </div>
					        <!--social-timeline-details-->
					        <div class="social-timeline-keywords-details">
					            <div class="row">
					                <div class="col-xs-12">
					                    <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="dynamic keyword name">
					                            #bitcoin</a>
					                    <a href="#" class="social-keywords-tags half innerL">#India</a>
					                </div>
					            </div>
					        </div>
					        <!--social-timeline-keywords-details-->
					        <!--social-timeline-user-message-->
					        <div class="social-timeline-content-message">
					            <div class="row">
					                <div class="col-xs-12">
					                    <p class="innerMB">
					                        The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
					                    </p>
					                </div>
					            </div>
					        </div>
					        <!--social-timeline-tags-details-->
					        <!--  content-image-->
					        <div class="social-timeline-content-image">
					            <div class="row">
					                <div class="col-xs-12 post-display-img">
					                    <a data-toggle="modal" href="#status_image_modal">
					                        <img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive" />
					                    </a>
					                </div>
					            </div>
					        </div>
					        <!--social-timeline-content-image-->
					        <div class="social-timeline-earning-comments-view innerT">
					            <div class="row">
					                <div class="col-xs-4">
					                    <div>
					                        <label>Earning : </label>
					                        <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
					                    </div>
					                </div>
					                <div class="col-xs-8">
					                    <div class="">
					                        <ul class="list-inline pull-right margin-bottom-none">
					                           	<li class="padding-right-none">
					                           	    <div>
					                           	        <span class="">770k</span>
					                           	        <label>Likes</label>
					                           	    </div>
					                           	</li>
					                            <li>
					                                <div>
					                                    <span class="half innerR">40</span>
					                                    <label class="pull-right">Comments</label>
					                                </div>
					                            </li>
					                            <li>
					                                <div>
					                                    <span class="">456</span>
					                                    <label>Shares</label>
					                                </div>
					                            </li>
					                        </ul>
					                    </div>
					                </div>
					            </div>
					        </div>
					        <!--social-timeline-earning-comments-view-->
					        <!--social-timeline-likes-dislikes-->
					        <div class="social-timeline-likes-dislikes">
					            <div class="row">
					                <div class="col-xs-6">
					                    <div class="social-likes-dislikes">
					                        <ul class="list-inline margin-bottom-none">
					                            <li>
					                                <div>
					                                    <a class="like-area">
					                                        <i class="likes-click fa fa-thumbs-o-up half innerR"></i>
					                                    </a>
					                                </div>
					                            </li>
					                        </ul>
					                    </div>
					                </div>
					                <div class="col-xs-6">
					                    <div class="social-comments-shares-views">
					                        <ul class="list-inline pull-right margin-bottom-none">
					                            <li>
					                                <div>
					                                    <a class="comment-click">
					                                        <i class="fa fa-comments-o"></i>
					                                        <span>Comments</span>
					                                    </a>
					                                </div>
					                            </li>
					                            <li class="padding-right-none">
					                                <div>
					                                    <a data-toggle="modal" data-target="#share-modal">
					                                        <i class="fa fa-share-square-o"></i>
					                                        <span>Share</span>
					                                    </a>
					                                </div>
					                            </li>
					                        </ul>
					                    </div>
					                </div>
					            </div>
					            <div class="row comments-section comments-module" style="display:none;">
					                <div class="clearfix comments-module-wrapper">
					                    <div class="col-xs-1 wid-50">
					                        <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
					                    </div>
					                    <div class="col-xs-10 padding-left-none">
					                        <div class="comments-module-comment-block">
					                            <a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
					                            <span class="comments-module-comment">hdf</span>
					                        </div>
					                        <div class="comments-module-time-block">
					                            <a href="javascript:;" class="comments-module-reply">Reply</a>
					                            <span class="comments-module-hrs">23  hrs</span>
					                        </div>
					                        <div class="row reply-box innerMT comments-module-wrapper" style="display:none;">
					                            <div class="col-xs-1 padding-none innerMR">
					                                <img class="reply-img" src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
					                            </div>
					                            <div class="col-xs-10 padding-none">
					                                <div id="comment-form" class="form-group">
					                                    <input type="text" id="user-comment-text" class="form-control checkEmpty reply-input-box" placeholder="Write Comment Here">
					                                </div>
					                            </div>
					                        </div>
					                    </div>
					                    <div class="position-relative pull-right innerMR comment-main-dropdown">
					                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
					                        <ul class="dropdown-menu comment-main-dropdown pull-right" aria-labelledby="dLabel">
					                            <li>
					                                <a data-toggle="modal" onclick="editDeleteComment('update', '58d3628a87feca59069b5505');"><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
					                            </li>
					                            <li>
					                                <a data-toggle="modal" onclick="deleteConfirmation('delete', '58d3628a87feca59069b5505');"><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
					                            </li>
					                        </ul>
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					    <!--social-card-->
					</div>
					
				</div>
				<div class="row innerMT inner-2x">
					<div class="col-xs-12">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Next">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Skip">
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<?php include('../../layout/social_footer.php'); ?>

