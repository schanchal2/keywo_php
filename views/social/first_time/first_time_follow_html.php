<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../../layout/header.php");
$email      = $_SESSION["email"];
}
?>
<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_first_time.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<main class="container first_time_follow_container inner-7x innerTB">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Follow any 5 accounts</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
				<div class="card social-card right-panel-modules inner-2x innerMT">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
								<h4 class="half innerMTB">Task List</h4>
							</div>
						</div>
					</div>
					<div class="suggested-module">
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Update Profile
								</div>
							</div>
							<div class=" col-xs-2">
								<a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Follow 5 accounts (5)
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Like a post
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Create a post
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Share a post
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<form action="">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: 40%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<ul class="list-unstyled activity-log__list">
					<li class="card innerAll border-bottom ">
						<div class="media">
							<div class="media-left">
								<div class="first_time_follow-pic"></div>
							</div>
							<div class="media-body f-sz15">
								<div class="row">
									<div class="col-xs-9">
										<div class="row">
											<div class="col-xs-6">
												<div class="ellipses margin-none f-wt7 f-sz16">Salena Gomez</div>
											</div>
											<div class="col-xs-6">
												<div class="ellipses margin-none text-light-grey">@salenagomez</div>
											</div>
											<div class="col-xs-12">
												<div class=" ellipses margin-none text-light-grey l-h14">I made $700 With my first post and Lorem ipsum dolor sit amet.</div>
											</div>
										</div>
									</div>
									<div class="col-xs-3">
										<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Follow">
									</div>
								</div>
								<!--
								-->
							</div>
						</div>
					</li>
					<li class="card innerAll border-bottom ">
						<div class="media">
							<div class="media-left">
								<div class="first_time_follow-pic"></div>
							</div>
							<div class="media-body f-sz15">
								<div class="row">
									<div class="col-xs-9">
										<div class="row">
											<div class="col-xs-6">
												<div class="ellipses margin-none f-wt7 f-sz16">Salena Gomez</div>
											</div>
											<div class="col-xs-6">
												<div class="ellipses margin-none text-light-grey">@salenagomez</div>
											</div>
											<div class="col-xs-12">
												<div class=" ellipses margin-none text-light-grey l-h14">I made $700 With my first post and Lorem ipsum dolor sit amet.</div>
											</div>
										</div>
									</div>
									<div class="col-xs-3">
										<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Follow">
									</div>
								</div>
								<!--
								-->
							</div>
						</div>
					</li>
					<li class="card innerAll border-bottom ">
						<div class="media">
							<div class="media-left">
								<div class="first_time_follow-pic"></div>
							</div>
							<div class="media-body f-sz15">
								<div class="row">
									<div class="col-xs-9">
										<div class="row">
											<div class="col-xs-6">
												<div class="ellipses margin-none f-wt7 f-sz16">Salena Gomez</div>
											</div>
											<div class="col-xs-6">
												<div class="ellipses margin-none text-light-grey">@salenagomez</div>
											</div>
											<div class="col-xs-12">
												<div class=" ellipses margin-none text-light-grey l-h14">I made $700 With my first post and Lorem ipsum dolor sit amet.</div>
											</div>
										</div>
									</div>
									<div class="col-xs-3">
										<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Follow">
									</div>
								</div>
								<!--
								-->
							</div>
						</div>
					</li>
				</ul>
				<div class="row innerMT inner-2x">
					<div class="col-xs-12">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Next">
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</main>
<?php include('../../layout/social_footer.php'); ?>