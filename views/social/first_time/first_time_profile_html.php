<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../../layout/header.php");
$email      = $_SESSION["email"];

}

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_first_time.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<main class="container first_time_container inner-7x innerT">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Welcome to Keywo</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
			    <div class="card social-card right-panel-modules inner-2x innerMT">
			        <div class="bg-light-gray right-panel-modules-head">
			            <div class="row margin-none">
			                <div class="col-xs-12">
			                    <h4 class="half innerMTB">Task List</h4>
			                </div>
			            </div>
			        </div>
			        <div class="suggested-module">
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Update Profile
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Follow 5 accounts (5)
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Like a post
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
			            <div class="row innerTB half border-bottom">
			                <div class="col-xs-10">
			                    <div class="">
			                        Create a post
			                    </div>
			                </div>
			                <div class=" col-xs-2">
			                    <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
			                </div>
			            </div>
						<div class="row innerTB half border-bottom">
						    <div class="col-xs-10">
						        <div class="">
						            Share a post
						        </div>
						    </div>
						    <div class=" col-xs-2">
						        <!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
						    </div>
						</div>
			        </div>
			    </div>
			</div>
		</div>
		<div class="col-xs-6">
			<form action="">
				<div class="progress">
				  <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<div class="social-card card innerB inner-2x">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="text-dark-blue text-center f-wt7">Update Your Profile</h4>
						</div>
					</div>
					<div class="row innerMT inner-4x">
						<div class="col-xs-12">
							<div class="first_time_profile-pic"></div>
							<div class="something_aboutyou innerMT inner-3x">
								<input type="text" class="form-control" placeholder="Say something about yourself">
								<div class="innerAll">
									<i class="fa fa-upload text-dark-blue f-sz16" aria-hidden="true"></i>
									<input type="file" class="hidden pic-upload">
									<span class="upload-pic">Upload Pic</span>
									<a class="innerML inner-3x" href=""><i class="fa fa-trash text-dark-blue clearPic"></i><span>Clear Pic</span></a>
								</div>
							</div>
						</div>
					</div>
					<div class="row innerMT inner-4x">
						<div class="col-xs-12">
							<!-- <div class="form-group">
								<input type="text" class="form-control first_time_input innerMB inner-2x" placeholder="First Name">
							</div>
							<div class="form-group">
								<input type="text" class="form-control first_time_input innerMB inner-2x" placeholder="Last Name">
							</div> -->
              <div class="form-group">
								<input type="text" class="form-control first_time_input innerMB inner-2x" placeholder="Handle">
							</div>
							<div class="form-group relative">
								<input type="text" class="form-control first_time_input innerMB inner-2x" id="datepicker" placeholder="Date of Birth">
								<i class="fa fa-calendar text-dark-blue calendar" aria-hidden="true"></i>
							</div>
							<div class="form-group">
								<input type="text" class="form-control first_time_input innerMB inner-2x" placeholder="City">
							</div>
              <div class="form-group">
								<input type="text" class="form-control first_time_input innerMB inner-2x" placeholder="Interest">
							</div>
							<div class="form-group">
							    <div class="radio radio-primary radio-div">
							        <input type="radio" name="gender" id="male" value="male" checked="true">
							        <label for="male">
							            Male
							        </label>
							    </div>
							    <div class="radio radio-primary radio-div">
							        <input type="radio" name="gender" id="female" value="female">
							        <label for="female">
							            Female
							        </label>
							    </div>
							    <div class="radio radio-primary radio-div">
							        <input type="radio" name="gender" id="other" value="other">
							        <label for="other">
							            Other
							        </label>
							    </div>
							</div>
						</div>
					</div>
				</div>
				<div class="row innerMT inner-2x">
					<div class="col-xs-12">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Next">
					</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script>
	$(function() {
		$('#datepicker').datepicker({
		    format: 'dd/mm/yyyy',
		    autoclose: true,
		    keyboardNavigation : true ,
		    daysOfWeekDisabled : [0],
		    changeMonth:true,
		    changeYear:true
		});
	});
	$(".upload-pic").click(function(){
		$(".pic-upload").trigger('click');
	})
	$(".clearPic").click(function(){
		$(".first_time_profile-pic").css('background-image','url("../images/rightpanel-img/Default_profile_image10.PNG")')
	})
</script>
<?php include('../../layout/social_footer.php'); ?>
