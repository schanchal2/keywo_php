<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../../layout/header.php");
$email      = $_SESSION["email"];
}
?>
<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_first_time.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<main class="container first_time_like_container inner-7x innerTB">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Create a post and earn 0.0025 Credit</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
				<div class="card social-card right-panel-modules">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
								<h4 class="half innerMTB">Task List</h4>
							</div>
						</div>
					</div>
					<div class="suggested-module">
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Update Profile
								</div>
							</div>
							<div class=" col-xs-2">
								<a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Follow 5 accounts (5)
								</div>
							</div>
							<div class=" col-xs-2">
								<a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Like a post
								</div>
							</div>
							<div class=" col-xs-2">
								<a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a>
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Create a post
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
						<div class="row innerTB half border-bottom">
							<div class="col-xs-10">
								<div class="">
									Share a post
								</div>
							</div>
							<div class=" col-xs-2">
								<!-- <a href="" class="half text-right text-App-Success innerR"><i class="fa fa-check"></i></a> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-6">
			<form action="">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<div class="social-card card">
					<div class="social-search-box social-card innerMB">
						<div class="card innerAll" id="social-micro-blog-box">
							<form name="status-post" id="status-post" class="margin-none">
								<div class="row">
									<div class="col-xs-12">
										<div class="form-group margin-none">
											<input type="hidden" name="post-type" value="status">
											<div class="">
												<textarea rows="4" class="share-post-box compressed-div form-control checkEmpty" name="modalComment" placeholder="Share Something..."></textarea>
											</div>
										</div>
									</div>
								</div>
								<div class="keywords-for-tags row innerAll clearfix">
									<div id="tags" class="tag-text" data-id="image-selected-keywords">
										<input type="" class="tag-input checkEmpty preview-tag-change" maxlength="50" style="outline: none;" placeholder="Add #Keywords(min 1-max 3)" value="" />
										<input type="hidden" name="modalKeywords" />
									</div>
								</div>
								<div class="row post-bottom-controls innerMT">
									<div class="share-textbox-quick-links">
										<div class="col-xs-4">
											<a class="innerR" data-toggle="modal" href="#blogModal">
												<i class="fa fa-file-text"></i>
											</a>
											<a class="innerR" data-toggle="modal" href="#videoModal">
												<i class="fa fa-play-circle"></i>
											</a>
											<a data-toggle="modal" href="#imageModal" class="innerR">
												<i class="fa fa-picture-o"></i>
											</a>
											<a data-toggle="modal" href="#audioModal" class="innerR">
												<i class="fa fa-soundcloud"></i>
											</a>
										</div>
										<div class="col-xs-6">
											<select class="selectpicker" name="postscope">
												<option value="Only Me">Only Me</option>
												<option value="Public">Public</option>
												<option value="Followers">Followers</option>
												<option value="Followers of Followers">Followers of Followers</option>
											</select>
										</div>
										<div class="col-xs-2">
											<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Post">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="row innerMT inner-2x">
					<div class="col-xs-12">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Next">
						<input type="button" class="btn-social-wid-auto btn-xs pull-right" value="Skip">
					</div>
				</div>
			</form>
		</div>
	</div>
	</main>
	<?php include('../../layout/social_footer.php'); ?>