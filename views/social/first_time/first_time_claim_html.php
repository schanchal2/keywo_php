<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../../layout/header.php");
$email      = $_SESSION["email"];

}

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_first_time.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<main class="container first_time_container inner-7x innerT">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Welcome to Keywo</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-6 col-md-offset-3">
			<form action="">
				<div class="progress">
				  <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
				<div class="social-card card innerB inner-1x innerAll">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="text-dark-blue text-center f-wt7">Claim Your Keywords </h4>
						</div>
					</div>

					<div class="row">
						<div class="col-xs-12 col-xs-12 innerB inner-1x">
							<span>As a Privileged customer of Search Trade family, Keywo allows you to claim your existing Search Trade Keywords as a welcome gift <br/>Please select keywords to claim</span>
						</div>
					</div>
          <div class="row">
            <div class="col-xs-12 col-xs-12 innerB inner-1x">
              <label for="Boom"> Base </label><br>
              <label for="Moon"> Moon</label><br>
            </div>
          </div>
          <div class="row">
  					<div class="col-xs-11 innerB">
  						<input type="button" class="btn-social-dark btn-xs pull-left inner-2x innerMR" value="Claim">
              <input type="button" class="btn-social-dark btn-xs pull-left" value="Dont Claim">
  					</div>
  				</div>
				</div>
			</form>
		</div>
	</div>
</main>
<script>
	$(function() {
		$('#datepicker').datepicker({
		    format: 'dd/mm/yyyy',
		    autoclose: true,
		    keyboardNavigation : true ,
		    daysOfWeekDisabled : [0]
		});
	});
	$(".upload-pic").click(function(){
		$(".pic-upload").trigger('click');
	})
</script>
<?php include('../../layout/social_footer.php'); ?>
