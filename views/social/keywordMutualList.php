<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once "{$docrootpath}models/user/authenticationModel.php";
require_once "{$docrootpath}models/analytics/userRegistration_analytics.php";
require_once "{$docrootpath}helpers/deviceHelper.php";
require_once "{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php";
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";
require_once "{$docrootpath}models/social/socialModel.php";
require_once "{$docrootpath}models/social/commonFunction.php";
require_once("{$docrootpath}models/keywords/keywordCdpModel.php");


$conn = createDBConnection('dbkeywords');

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//$email = base64_decode($_GET["email"]);
$email = base64_decode($_GET["email"]);
$type = $_GET["type"];
$sessionEmail = $_SESSION["email"]; 
//$type = $_GET["type"];

$user_id="user_id,account_handle"; 
$getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$user_id); 
if(noError($getUserInfo)){
    $getUserInfo             = $getUserInfo["errMsg"];
    $otherAccountHandlerId   = $getUserInfo["user_id"];
    $accountHandle           = $getUserInfo["account_handle"];
}else{
    echo '<script type="text/javascript">
            window.location = "not_found_error.php"
            </script>';
}

$type                    = cleanXSS(trim(urldecode($_POST["type"]))); 
$jsonFileNo              = cleanXSS(trim(urldecode($_POST["jsonFileNo"]))); 
$jsonFileInnerNo         = cleanXSS(trim(urldecode($_POST["jsonFileInnerNo"]))); 
$divAppendId             = cleanXSS(trim(urldecode($_POST["divAppendId"]))); 
$jasonFileNo             = '';
$remaining               = '';
$pageScrollEnd           = 'true';
$targetDirOtherAccountHandler = "../../json_directory/social/followerwonk/".$accountHandle."/"; 
$targetDirAccountHandler      = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
// Get Blocked User
$blockFlag = false;
$getBlockDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($accountHandle[0]).".json"; 
if(file_exists($getBlockDetails)) {
    $jsondataBlock = file_get_contents($getBlockDetails);
    $dataBlock = json_decode($jsondataBlock, true);
    if (in_array($otherAccountHandlerId, $dataBlock["blocked"])) {
        $blockFlag = true;
    }  }

// Get Blocked User
$blockFlagOfOther = false;
$targetDirAccountViewerOtherHandler = "../../json_directory/social/followerwonk/".$accountHandle."/";


$getBlockDetails = $targetDirAccountViewerOtherHandler.$accountHandle."_info_" .strtolower($_SESSION["account_handle"][0]).".json";
if(file_exists($getBlockDetails)) {
    $jsondataBlockOfOther = file_get_contents($getBlockDetails);
    $dataBlockOfOther = json_decode($jsondataBlockOfOther, true);
    if (in_array($_SESSION["id"], $dataBlockOfOther["blocked"])) {
        $blockFlagOfOther = true;
    }  }

if($blockFlag == true){
    echo '<script type="text/javascript">
                window.location = "not_found_error.php"
                </script>';
    exit;
}elseif($blockFlagOfOther == true) {
    echo '<script type="text/javascript">
                window.location = "not_found_error.php"
                </script>';
    exit;
}

//Find files of logged in Other handler
$targetDirOtherAccountHandler = "../../json_directory/social/followerwonk/".$accountHandle."/"; 
$filesOfAccHandle = dirToArray($targetDirOtherAccountHandler);
foreach ($filesOfAccHandle as $key => $fileInfoInitial){
    $filesOfAccHandle[$key]=str_replace($accountHandle."_info_","",$fileInfoInitial);
}

//Find files of logged in account handler
$targetDirAccountViewerHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$filesOFOthers = dirToArray($targetDirAccountViewerHandler);

//Replace file of other account handler
foreach ($filesOFOthers as $key => $fileInfoInitial){
    $filesOFOthers[$key]=str_replace($_SESSION["account_handle"]."_info_","",$fileInfoInitial);
}

//Find out Common files from each other
$mutualElementOfFile1 = array_intersect($filesOfAccHandle, $filesOFOthers);

//again append name of account handler to final file
$valueOfMut = array();
foreach ($mutualElementOfFile1 as $value){
    $valueOfMut[] = $_SESSION["account_handle"]."_info_".$value;
}

// Set particuler variable for pagination
$dataCount = "";
$userIdsOfMutual = array();
$useerIdCount = 0;
$setter = array();
$endFlag = true;
$flag = true;
if (empty($jsonFileNo)) {
    $i = 0;
} else {
    $i = $jsonFileNo;
}

if(empty($valueOfMut))
{

?>
<div class="modal-header">
       <h4 class="modal-title text-blue">No mutual keywords.</h4>
   </div>

<?php }
//find following data of common file of account handle
for ($i; $i < count($valueOfMut) && $endFlag; $i++ ) {
    $lastWordForFirst = substr($valueOfMut[$i], -6);
    $Final12 = $targetDirAccountViewerHandler . $valueOfMut[$i];
    $jsonOfMutual = file_get_contents($Final12);
    $dataOfMutual = json_decode($jsonOfMutual, true);
    if (isset($dataOfMutual["followedkeyword"])) {
    $dataCount = count($dataOfMutual["followedkeyword"]);
    $dataIds = $dataOfMutual["followedkeyword"];

    $finalFileOfMutual = "../../json_directory/social/followerwonk/" . $accountHandle . "/" . $accountHandle . "_info_" . $lastWordForFirst;

    $finalSessionFileOfMutual = "../../json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/" . $_SESSION["account_handle"] . "_info_" . $lastWordForFirst;

    $jsonOfMutual3 = file_get_contents($finalFileOfMutual);
    $dataOfMutual = json_decode($jsonOfMutual3, true);

        $dataCountOfMutual = $dataOfMutual["followedkeyword"];
        $lastWord = substr($valueOfMut[$i], -6);
        $lastWordForFirst = substr($valueOfMut[$i], -6);
        $firstWord = $lastWordForFirst[0];


        $jsonOfMutual4 = file_get_contents($finalSessionFileOfMutual);
        $dataOfMutual1 = json_decode($jsonOfMutual4, true);
        $dataCountOfMutual4 = $dataOfMutual1["followedkeyword"];
        $lastWord = substr($valueOfMut[$i], -6);
        $lastWordForFirst = substr($valueOfMut[$i], -6);
        $firstWord = $lastWordForFirst[0];


        $appended = 0;
        if (!empty($jsonFileInnerNo) && !empty($jsonFileNo) && $i == $jsonFileNo) {
            $k = $jsonFileInnerNo;
        } else {
            $k = 0;
        }

        for ($k; $k < count($dataIds) && $flag; $k++) {
            if ($useerIdCount < 15) {
                $useerIdCount = $useerIdCount + count($dataIds[$k]);
                $userIdsOfMutual[] = $dataIds[$k];

                $appended = $appended + 1;
            } else {
                $flag = false;
                $endFlag = false;
                $lastWordJson = $valueOfMut[$i];
                $remaining = $appended;
                $jasonFileNo = $i;
            }
        }
        $word = $lastWord[0];

        //Find mutual element from file
        $mutualElementOfFile = array_intersect($dataCountOfMutual, $dataCountOfMutual4);
        $arrayFile = array_values(array_filter($mutualElementOfFile));
        $mutualarrayCount = count($mutualElementOfFile);
        if ($mutualarrayCount > 0) {
            $getter = array("count" => $mutualarrayCount, "user_id" => $arrayFile);
            $setter[$word] = $getter;
        }

        //indexing array after intersect


        $jsonCOnverts = json_encode($setter);
        $s = 0;

        foreach ($mutualElementOfFile as $key => $filesDetails) {
            $MutualKeyword = $filesDetails["keyword"];

            $words = explode(" ", $MutualKeyword);
            $acronym = "";

            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            if (count($filesDetails) > 0) {
                if ($s == 0) {
                    if (empty($jsonFileInnerNo)) { ?>
                        <div class="h4">  <?php if (!empty($divAppendId)) {
                                echo strtoupper($acronym);
                            } ?></div>
                    <?php }
                } else {
                    ?>
                    <div class="h4"><?php echo strtoupper($key); ?></div>
                    <?php
                }
            }
            ?>
            <ul class="card social-card followed-list all-box-shadow padding-none clearfix" style="list-style:none;">
                <li class="clearfix border-bottom">
                    <div class="col-xs-12 innerAll">
                        <div class="col-xs-1 padding-left-none half innerT">
                            <img src="<?php echo $rootUrlImages ?>keyword_search_icon.png" class="img-responsive"/>
                        </div>
                        <div class="col-xs-5 text-blue innerMT padding-right-none">
                            <div class="followed-keyword-name"><?php echo "#{$MutualKeyword}" ?>
                            </div>
                        </div>

                        <div class="col-xs-6" id="followunfollow">
                            <?php

                            $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $MutualKeyword);  // Used for getting details from revenue table.
                            if (noError($kwdRevenueDetails)) {
                                $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
                                $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                                $CountFollowedKeyword = $kwdRevenueDetails["follow_unfollow"];

                                $keywordFollowerCount = json_decode($CountFollowedKeyword, true);
                                $keywordFollowerCounts = $keywordFollowerCount["email"];
                                $keywordFollowerCount = count($keywordFollowerCounts);

                                if (in_array($email, $keywordFollowerCounts)) { ?>
                                    <input value="Unfollow" type="button"
                                           id="km_followButton_<?php echo cleanXSS($MutualKeyword); ?>"
                                           class="btn-social-wid-auto-dark pull-right innerMR"
                                           onclick="followUnfollow('<?php echo cleanXSS($MutualKeyword); ?>', 'km')"/>
                                <?php } else { ?>
                                    <input value="Follow" type="button"
                                           id="km_followButton_<?php echo cleanXSS($MutualKeyword); ?>"
                                           class="btn-social-wid-auto-dark pull-right innerMR"
                                           onclick="followUnfollow('<?php echo cleanXSS($MutualKeyword); ?>', 'km')"/>
                                <?php } ?>
                                <span>
                </span>
                            <?php } ?>
                        </div>
                    </div>
                </li>
            </ul>
            <?php
            $s = $s + 1;
        }
    }
}
 if(!empty($MutualKeyword)) { }
    else { ?>

<div class="modal-header">
       <h4 class="modal-title text-blue">No mutual keywords.</h4>
   </div>
<?php } ?>
<div id="followingAppend<?php echo $divAppendId; ?>" jsonFileNo = "<?php echo $jasonFileNo; ?>" fileReadNo = "<?php echo $remaining; ?>" pageScrollEnd = "<?php echo $pageScrollEnd; ?>"></div>
