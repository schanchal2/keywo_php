<?php 
  include("../layout/header.php");
  // $previous = "javascript:history.go(-2)";
  if(isset($_SERVER['HTTP_REFERER'])) {
    //$previous = $_SERVER['HTTP_REFERER'];
    $previous = "javascript:history.go(-2)";
  }
  if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
?>

<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app.css" type="text/css"/>
<main class="inner-7x innerT">
  <div class="container">
    <div class="row card not-found-container">
      <div class="col-xs-12">
        <h1 class="text-blue">Error</h1>
      </div>
      <div class="col-xs-12 text-blue">
        <h4 class="margin-top-none">The page, post or link you are looking for may be</h4>
        <h4>broken, removed or does not exist</h4>
      </div>
      <div class="col-xs-12 innerMTB">
        <img src="<?php echo $rootUrlImages?>error_icon.png" alt="">
      </div>
      <div class="col-xs-12 innerB inner-2x">
        <div>Go back to <a href="<?= $previous ?>">Previous page</a> or <a href="<?php echo $rootUrl;?>">Home page</a></div>
      </div>
    </div>
  </div>
</main>

<?php 
  } else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
  }


  include('../layout/social_footer.php'); 


?>
