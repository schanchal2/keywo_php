<!--===============================================
=            include : followedKeyword            =
================================================-->
<?php
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');
$extraArg = array();

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
    $docrootpath = __DIR__;
    $docrootpath = explode('/views', $docrootpath);
    $docrootpath = $docrootpath[0] . "/";

    /* Add DB Management Files */
    require_once "{$docrootpath}config/config.php";
    require_once "{$docrootpath}config/db_config.php";
    /* Add Model */
    require_once("{$docrootpath}helpers/deviceHelper.php");
    require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");
    require_once "{$docrootpath}helpers/arrayHelper.php";
    require_once "{$docrootpath}helpers/stringHelper.php";
    require_once "{$docrootpath}helpers/errorMap.php";
    require_once "{$docrootpath}helpers/coreFunctions.php";
    require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
    require_once("../../models/social/commonFunction.php");

    // Database connection.
    $conn = createDBConnection('dbkeywords');

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$accountHandle  = $_SESSION["account_handle"];
$followedKeyword = array();

$email = $_SESSION["email"];
//echo $accountHandleType                         = cleanXSS(trim(urldecode($_POST["accountHandleType"])));

//  if(is_array($_POST) && sizeof($_POST) > 0)
//  {

    $targetDirAccountHandler = $docRoot."json_directory/social/followerwonk/".$accountHandle."/";
    $fileOfFollowings        = dirToArray($targetDirAccountHandler);

    foreach($fileOfFollowings as $key => $fileOfFollowingsDetails)
    {
        $Final= $targetDirAccountHandler.$fileOfFollowingsDetails;
        $jsonOfFollowings = file_get_contents($Final);

        $data = json_decode($jsonOfFollowings, true);

        $getkeyword = $data["followedkeyword"];

        array_multisort($time, SORT_ASC, $points);

        foreach($getkeyword as $key => $keyword){

//                $keyword     = $key["keyword"]; echo "<br>keyword ".$keyword;
//                $createdTime = $key["created_at"]; echo "<br>CreatedAt ".$createdTime;

            if(!empty($keyword["keyword"])){
                $followedKeyword[] = $keyword["keyword"];
            }
        }
    }

$getkeyword = getFollowFollowerData($accountHandle,"followedkeyword","technique");

?>
<div id="followedKeyword">
    <div class="card social-card right-panel-modules inner-2x innerMT">
        <div class="bg-light-gray right-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4 class="half innerMTB">Followed Keywords</h4>
                </div>
            </div>
        </div>
        <?php //
            $pageUrl = "views/social/viewKeywoTimeline.php";
            if(count($followedKeyword) == 0)
            { ?>
        <div>
            <center>No Keyword Followed</center>
        </div>
        <?php //
            }else {
            $i  = 0;
            //krsort($getkeyword);
            foreach ($getkeyword as $keyword) {
                $followedkeyword = $keyword["keyword"];
                $i++;
                if ($i < 6) {

                    $kwdRevenueDetails = getRevenueDetailsByKeyword($conn,$followedkeyword);
                    if(noError($kwdRevenueDetails)) {
                        $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                        $interactionCount = $kwdRevenueDetails["app_kwd_search_count"];
                        $userKwdSearchCount = $kwdRevenueDetails["user_kwd_ownership_earnings"];

                    ?>
        <div class="row margin-none half innerTB border-bottom">
            <div class="col-xs-9">
                <div class="followed-keywords-name">
                    <a href="<?php echo $rootUrl.$pageUrl; ?>?keyword=<?php echo urlencode($followedkeyword); ?>">#<?php echo $followedkeyword; ?></a>
                </div>
                <div>
                    <label>Earning : </label>
                    <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                    origPrice="<?php echo number_format("{$userKwdSearchCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$userKwdSearchCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$userKwdSearchCount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>

                </div>
            </div>
        </div>
        <?php } } } } ?>
        <?php if (isset($followedKeyword) && !empty($followedKeyword))
        {
        $count = count($followedKeyword);
        if($count <= 5)
        { } else { ?>
        <div class="row margin-none half innerTB border-bottom bg-view-all">
            <div class="col-xs-12">
                <a href="viewallFollowedKeyword.php" target="_blank" class="text-deep-sky-blue pull-right">View all</a>
            </div>
        </div>
        <?php }} ?>
    </div>
    <!-- card -->
</div>
<?php }
else{
    $returnArr["errCode"] = 53;
    $errMsg = "Please Login To System.";
    $returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);
}?>
<!--====  End of include : followedKeyword  ====-->
