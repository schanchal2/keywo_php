<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add Global variables */

/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
// require_once "../../models/social/socialModel.php";
$extraArg = array();


  if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
  {
    $email = $_SESSION["email"];
    $followingArray = array('followings','followers','suggested','blocked');
    $followingType = urldecode($_GET['type']);
    $followerFlag = false;

    // To check URL Type is not other than following , follower, suggested, blocked.
    $n = 0;
    foreach ($followingArray as $key => $value) {
      if ($value == strtolower($followingType)) {
        break;
      }
     $n = $n + 1;
    }
    if (count($followingArray) == $n) {
      $followerFlag = true;
    }

    // Redirecting to index page
    if ($followerFlag){
      header('location:'. $rootUrl);
      die;
    }
    if (empty($followingType)) {
      $followingType = "followings";
    }
// die();
?>
    <!--========================================================================================================================================
=            "follower" page and "following" page will be same as below, only "follow" button wont be there in "following" page            =
=========================================================================================================================================-->
    <?php
      include("../layout/header.php");
      ?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container row-10">
            <div class="row">
                <div class="col-xs-3 social-right-position">
                    <div class="social-left-panel">
                        <?php include("leftInfoUser.php"); ?>
                    </div>
                    <div class="followed-keywords-module inner-2x innerMTB">
                        <?php //include("followedKeyword.php"); ?>
                        <!-- card -->
                    </div>
                    <div id="suggested-user-details">
                        <?php include("suggestedUser.php"); ?>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="social-center-panel innerLR">
                        <!-- =================== social-all-status-tabs ================== -->
                        <div class="social-all-status-tabs">
                            <div class="row followers-unfollowers">
                                <div class="col-xs-3">
                                    <a id="followings" href="#followings" class="active">FOLLOWING</a>
                                </div>
                                <div class="col-xs-3">
                                    <a id="suggestedUserList" href="#suggested" class="">SUGGESTED</a>
                                </div>
                                <div class="col-xs-3">
                                    <a id="followers" href="#followers" class="">FOLLOWERS</a>
                                </div>
                                <div class="col-xs-3">
                                    <a id="block" href="#block" class="">BLOCKED</a>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id="followersType" type="<?php echo $followingType; ?>"></div>
                            <div id="followingAjaxCall" ajxCallFlag="true"></div>
                            <!-- Followings Data Containt -->
                            <div id="followerDataAppend" followingDataAppendCount="" followingDataLoopCount="" pageScrollEndFlag="" style="display:none;"></div>
                            <div id="followerdivAppendId" style="display:none;">1</div>
                            <div id="follow-data">
                                <!--content from ajax-->
                            </div>
                            <!-- Followings Data Containt End -->
                            <!-- Follower Data Containt -->
                            <div id="followingDataAppend" followingDataAppendCount="" followingDataLoopCount="" pageScrollEndFlag="" style="display:none;"></div>
                            <div id="followingdivAppendId" style="display:none;">1</div>
                            <div id="followings-data" class="follower-container">
                                <!--content from ajax-->
                            </div>
                            <!-- Follower Data Containt End -->
                            <!-- Followings Data Containt -->
                            <div id="blockDataAppend" followingDataAppendCount="" followingDataLoopCount="" pageScrollEndFlag="" style="display:none;"></div>
                            <div id="blockdivAppendId" style="display:none;">1</div>
                            <div id="follow-data">
                                <!--content from ajax-->
                            </div>
                            <!-- Followings Data Containt End -->
                            <!-- Ajax Loader Image -->
                            <div id="ajaxLoader" class="text-center" style="display:none; ">
                                <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style="width: 60px;">
                            </div>
                            <!-- Ajax Loder Image End -->
                        </div>
                    </div>
                    <!-- social-center-panel  -->
                </div>
                <div class="col-xs-3 social-right-position">
                    <div class="social-right-panel">
                        <div class="row  inner-2x">
                            <div class="col-xs-12">
                                <?php //include 'include/_social_right_panel_searchbar.php'; ?>
                            </div>
                        </div>
                        <div class="card social-card right-panel-modules">
                            <?php include 'include/_social_right_panel_tredings.php'; ?>
                        </div>
                        <div class="popular-modules">
                            <div class="card social-card right-panel-modules inner-2x innerMTB">
                                <?php include 'include/_social_right_panel_popular.php'; ?>
                            </div>
                        </div>
                        <!--End of Popular Post Widget-->
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <?php include("../layout/social_footer.php");

}
  else{
    $returnArr["errCode"] = 53;
    $errMsg = "Please Login To System.";
     $returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);
     header('location:'. $rootUrl .'views/prelogin/index.php');
  }

?>
    <script>
    $(document).ready(function() {
        var followdocLoadType = $('#followersType').attr('type');
        if (followdocLoadType == 'followings') {
            $('#followers').removeClass('active');
            $('#followings').addClass('active');
            $('#suggestedUserList').removeClass('active');
            loadFollowings(followdocLoadType);
        } else if (followdocLoadType == 'followers') {
            $('#followers').addClass('active');
            $('#followings').removeClass('active');
            $('#suggestedUserList').removeClass('active');
            loadFollowers(followdocLoadType);
        } else if (followdocLoadType == 'suggested') {
            $('#suggestedUserList').addClass('active');
            $('#followings').removeClass('active');
            $('#followers').removeClass('active');
            loadSuggestedList(followdocLoadType);
        }

        /*
 For endless scrolling
 */
        var bottom = $(document).height() - $(window).height();
        var successflag = 0;
        $(document).scroll(function() {
            var win = $(window);
            // Each time the user scrolls
            win.scroll(function() {
                // End of the document reached?
                if ($(document).height() - win.height() == win.scrollTop()) {
                    // Do the stuff
                    if (successflag == 0) {
                        //do stuff here
                        successflag = 1;
                        var followloadType = $('#followersType').attr('type');
                        console.log(followloadType);
                        if (followloadType == 'followings') {
                            loadFollowings(followloadType);
                        } else if (followloadType == 'followers') {
                            loadFollowers(followloadType);
                        } else if (followloadType == 'blocked') {
                            loadBlocked(followloadType);
                        }
                    }
                } else {
                    successflag = 0;
                }
            });
        });

    });
    </script>
