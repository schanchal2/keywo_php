<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<main class="social-main-container inner-7x innerT" >
<div class="container notification-page-container">
	<div class="col-xs-3">
		<div class="social-left-panel inner-8x innerMT">
      <div class="card left-panel-modules half innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Notification</h4>
							</div>
						</div>
				</div>
				<div class="notification-page-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>All</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>General</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Search</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Social</a>
						</div>
            <div class="social-user-setting-name border-bottom">
							<a>Keyword</a>
						</div>
            <div class="social-user-setting-name border-bottom">
							<a>Wallet</a>
						</div>
            <div class="social-user-setting-name border-bottom">
							<a>Referral</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-9">
		<div class="col-xs-12 innerMB padding-none">
			<div class="pull-left">
				<h4 class="text-blue half innerMT">Notification </h4>
			</div>
		  <div class="pull-right">
				<form class="clearfix margin-none">
					<div class="pull-left half innerMT search-box-comments">
							<div class="input-group">
									<input type="text" id="search_box" name="q" value="<?php echo $keywords; ?>"
												 class="form-control" placeholder="Search" required>
									<input type="submit" class="social-search-btn-header" value=""/>
							</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-xs-12 followed-people-container padding-none half innerMT">
			<div class="innerMB notification-day innerML">
				Today
			</div>
			<div class="">
        <ul class="card social-card notification-list all-box-shadow padding-none clearfix" style="list-style:none;">
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-light-blue"></i>
											<i class="fa fa-thumbs-o-up text-white"></i>
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-light-blue"></i>
											<i class="fa fa-comments-o text-white"></i>
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-lighter-orange"></i>
											<img src="<?php echo $rootUrlImages?>wallet.png">
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-light-blue"></i>
											<i class="fa fa-thumbs-o-down text-white"></i>
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
        </ul>
			</div>
      <div class="innerMB notification-day innerML">
        Yesterday
      </div>
      <div class="">
        <ul class="card social-card followed-list all-box-shadow padding-none clearfix" style="list-style:none;">
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-light-orange"></i>
											<i class="fa fa-group text-white"></i>
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-dark-green"></i>
											<i class="fa fa-search text-white"></i>
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-dark-blue"></i>
											<img src="<?php echo $rootUrlImages?>keywo_single_logo.png">
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-6 padding-none">
                <div class="padding-left-none">
									<div class="innerMR pull-left">
										<div class="image-circle-container">
											<i class="fa fa-circle text-light-grey"></i>
											<img src="<?php echo $rootUrlImages?>Keyword_Search.png">
										</div>
									</div>
                </div>
                <div class="innerMT padding-none notification-text">
									<span class="text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
              </div>
              <div class="col-xs-6 innerMT padding-right-none">
                <div class="col-xs-10 padding-left-none notification-text">
                  I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
                </div>
								<div class="col-xs-2 padding-right-none">
                  <span class="time-text">2:15 pm</span>
								</div>
							</div>
            </div>
          </li>
        </ul>
      </div>
    </div>
    <!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

</div>
<!-- container -->
</main>


<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
