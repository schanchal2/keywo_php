<!--===============================================
=            include :personalWorkDetail.php            =
================================================-->
<?php
  $socialFileUrlShare = basename($_SERVER['PHP_SELF'], ".php");

  if ($socialFileUrlShare == "otherTimeline") {
    $email = $otherEmail;
  } else {
    $email = $_SESSION["email"];
  }
  //get user info
  $postFields = "user_id";
  $userDetail = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $postFields);

  if ($userDetail["errCode"] == -1) {
    $userWorkDetail = getUserWorkInfo($userData["user_id"], $walletURLIPnotification . 'api/notify/v2/');
    if ($userWorkDetail["errCode"] == -1) {
      //printArr($userWorkDetail);
      $companyName = !empty($userWorkDetail["errMsg"]["company_info"][0][0]["company_name"]) ? $userWorkDetail["errMsg"]["company_info"][0][0]["company_name"] : '-';
      $designation = !empty($userWorkDetail["errMsg"]["company_info"][0][0]["designation"]) ? $userWorkDetail["errMsg"]["company_info"][0][0]["designation"] : '-';
      $skills      = !empty($userWorkDetail["errMsg"]["skills"]) ? implode(', ', $userWorkDetail["errMsg"]["skills"]) : '-';
    } else {
      $companyName = '-';
      $designation = '-';
      $skills      = '-';
    }

?>
<ul class="list-group text-justify card border-All">
   <li class=" list-group-item list-group border-none"><p class="text-blue fsz18"><i class="fa fa-building-o" aria-hidden="true"></i> Work Detail</p>
     <span>Company Name</span>
     <span class="pull-right text-blue ellipses ellipses-general text-right"><?php echo $companyName; ?></span><hr>
     </li>
   <li class="list-group-item list-group border-none">
         <span>Designation</span>
         <span class="pull-right text-blue ellipses ellipses-general text-right"><?php echo $designation; ?></span><hr>
   </li>
   <li class="list-group-item list-group border-none">
         <span>Professional Skill</span>
         <span class="pull-right text-blue ellipses ellipses-general text-right"><?php echo trim($skills, ","); ?></span>
   </li>
   <!-- <li class="list-group-item list-group border-none text-justify">
         <span>Mobile No</span>
         <span class="pull-right text-blue">9002123654</span><hr>
   </li> -->
   <!-- <li class="list-group-item list-group border-none text-justify">
         <span>Location</span>
         <span class="pull-right text-blue ellipses">Thane,India</span><hr>
   </li>
   <li class="list-group-item list-group border-none text-justify">
         <span>Contact No.</span>
         <span class="pull-right text-blue ellipses">022-123-456</span>
   </li> -->
</ul>
<?php } else { ?>
  <ul class="list-group text-justify card border-All ">
    <li class="list-group-item list-group border-none">
          <span>Something went wrong..</span>
    </li>
  </ul>
<?php } ?>
<!--====  End of include :personalWorkDetail.php  ====-->
