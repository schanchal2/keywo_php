<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once "../../helpers/imageFunctions.php";




if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//Get following array
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
//Get suggested user
$email = "acer@grr.la";
$user_id="user_id,account_handle,first_name,last_name,email";
$getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$user_id);
if(noError($getUserInfo)) {
    $getUserInfo          = $getUserInfo["errMsg"];
    $accountHandlerUserId = $getUserInfo["user_id"];
    $accountHandler       = $getUserInfo["account_handle"];

}
$userDetails = getAllUserforFollowingList();
$userDetails = $userDetails['errMsg'];
$jsonWriteSpecailUser       = followUserAutomatically($accountHandler,$accountHandlerUserId,$userDetails);


?>