<?php
	session_start();

	require_once('../../config/config.php');
	require_once('../../config/db_config.php');
	require_once('../../helpers/coreFunctions.php');
	require_once('../../helpers/arrayHelper.php');
	require_once('../../helpers/stringHelper.php');
	require_once('../../helpers/errorMap.php');
	require_once('../../models/social/socialModel.php');
	require_once('../../models/social/commonFunction.php');

//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}
//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}



	$postId    = cleanXSS(trim(urldecode($_POST['postId'])));
	$postType  = cleanXSS(trim(urldecode($_POST['postType'])));
	$acHandler = cleanXSS(trim(urldecode($_POST['acHandler'])));
	$postTime  = cleanXSS(trim(urldecode($_POST['postTime'])));

	if(isset($_POST['email'])){
        $email     = cleanXSS(trim(urldecode($_POST['email'])));
    }else{
        $email = "";
    }

    $emailofSpecialUser = array();

	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

	$user_id="user_id,account_handle";
	$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
	if(noError($getUserInfo)){
		$getUserInfo = $getUserInfo["errMsg"];
		$user_id     = $getUserInfo["user_id"];
		$accHandle   = $getUserInfo["account_handle"];
	}
    //Get Special User
//    $getSpecialUserData = getSpecialUserPost();
//    if(noError($getSpecialUserData)){
//        $getSpecialUserData = $getSpecialUserData["errMsg"];
//    }
//    if($getSpecialUserData!="No Result") {
//        foreach ($getSpecialUserData as $specialUser) {
//            $emailofSpecialUser[] = $specialUser['email'];
//        }
//    }

    if(!in_array($email,$emailofSpecialUser)){
        //get Folllowed User
        $getFollowed         = getFollowUser($targetDirAccountHandler);

        //check for value
        $followFlag          = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

    }else{
        $followFlag = 1;
    }
//	 printArr($followFlag); die;
    $type = "";
	if (!empty($postId)) {
		$postDetail = getPostDetail($postId, $postTime,$_SESSION['id'],$followFlag);
	} else {
        $type = "create";
		$postDetail = array('errCode' => -1, 'errMsg' => '');
	}
	if (noError($postDetail)) {
        if($type=="create"){
            $postDetail = $postDetail['errMsg'];
        }else{
            $postDetail = $postDetail['errMsg'][0];
        }


		if(isset($postDetail['post_details']['img_file_name'])){
            $filename = $postDetail['post_details']['img_file_name'];
        }else{
            $filename = "";
        }

		$ext      = explode('.', $filename);
		//printArr($postDetail); die;
		//variables for shared post data
        if(isset($postDetail["post_details"]["parent_post"]["post_id"])){
            $postSharedParent      = $postDetail["post_details"]["parent_post"]["post_id"];
        }else{
            $postSharedParent      = "";
        }


        if(isset($postDetail["post_details"]["parent_post"]["post_id"])){
            $postSharedParentId    = $postSharedParent["_id"];
        }else{
            $postSharedParentId      = "";
        }

        if(isset($postSharedParent["user_ref"]["first_name"])){
            $postSharedParentFname = $postSharedParent["user_ref"]["first_name"];
        }else{
            $postSharedParentFname      = "";
        }

        if(isset($postSharedParent["user_ref"]["last_name"])){
            $postSharedParentLname = $postSharedParent["user_ref"]["last_name"];
        }else{
            $postSharedParentLname      = "";
        }

        if(isset($postSharedParent["keywords"][0])){
            $postSharedKeyword     = $postSharedParent["keywords"][0];
        }else{
            $postSharedKeyword      = "";
        }

        if(isset($postSharedParent["post_short_desc"])){
            $postSharedKeywords    = $postSharedParent["keywords"];
        }else{
            $postSharedKeywords      = "";
        }


        if(isset($postSharedParent["post_short_desc"])){
            $postSharedShortDesc   = $postSharedParent["post_short_desc"];
        }else{
            $postSharedShortDesc      = "";
        }

        if(isset($postSharedParent["post_type"])){
            $postSharedType        = $postSharedParent["post_type"];
        }else{
            $postSharedType      = "";
        }





	  if(isset($postSharedParent["post_details"]["asset_url"])){
          $postSharedAssetUrl    = $postSharedParent["post_details"]["asset_url"];
      }else{
          $postSharedAssetUrl = "";
      }


        if(isset($postSharedParent["post_details"]["blog_title"])){
            $postSharedBlogTitle   = $postSharedParent["post_details"]["blog_title"];
        }else{
            $postSharedBlogTitle = "";
        }


        if(isset($postSharedParent["post_details"]["img_file_name"])){
            $postSharedImgUrl      = $postSharedParent["post_details"]["img_file_name"];
        }else{
            $postSharedImgUrl = "";
        }


        if(isset($postSharedParent["post_details"]["blog_content"])){
            $postSharedBlogContent = $postSharedParent["post_details"]["blog_content"];
        }else{
            $postSharedBlogContent = "";
        }




    if(isset($_SESSION["profile_pic"]) && !empty($_SESSION["profile_pic"])){
        global $cdnSocialUrl;
        global $rootUrlImages;

        $extensionUP  = pathinfo($_SESSION["profile_pic"], PATHINFO_EXTENSION);
        //CDN image path
        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $_SESSION["account_handle"] . '/profile/' . $_SESSION["account_handle"] . '_' . $_SESSION["profile_pic"] . '.40x40.' . $extensionUP;

        //server image path
        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$_SESSION["account_handle"].'/profile/'.$_SESSION["account_handle"].'_'.$_SESSION["profile_pic"];

        // check for image is available on CDN
        $file = $imageFileOfCDNUP;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $sessionImgSrc = $imageFileOfLocalUP;
        } else {
            $sessionImgSrc = $imageFileOfCDNUP;
        }
    }else{
        $sessionImgSrc = $rootUrlImages."default_profile.jpg";
    }
	?>

	<?php
		switch ($postType) {
	        case 'video':
	?>
				<form id="video-post" name="video-post" class="innerAll padding-bottom-none padding-top-none" >
					<div class="video-form-modal">
						<div class="video-form">
							<div class="innerMAll video-add-link">
								<input type="hidden" name="post-type" value="video">
								<input type="hidden" name="post-id" value="<?php
                                if(isset($postDetail['_id'])){
                                    echo $postDetail['_id'];
                                }
                                ?>">
								<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
								<input type="hidden" name="post-create-time" value="<?php
                    if(isset($postDetail['_id'])) {
                        echo $postDetail['created_at'];
                    }
                    ?>">
								<div>
									<input type="text" id="video_link" class="form-control video-card checkEmpty checkVideoLink preview" data-id="video-input" data-api="videoAPI" placeholder="Add Link" name="asset_url" value = "<?php
                                    if(isset($postDetail['_id'])){
                                        echo $postDetail['post_details']['asset_url'];
                                    }
                                    ?>"/>
								</div>
								<a data-toggle="tooltip" title="Vimeo" class="vimeo-icon">
									<img src="<?php echo $rootUrlImages; ?>vimeo_form_icon.png" alt="">
								</a>
								<a data-toggle="tooltip" title="Youtube" class="youtube-icon">
									<img src="<?php echo $rootUrlImages; ?>youtube_form_icon.png" alt="">
								</a>
							</div>
							<?php if (!empty($postDetail['_id'])) {?>
							<iframe width="420" height="250" src="<?php echo $postDetail["post_details"]["asset_url"]; ?>" frameborder="0" allowfullscreen></iframe>
							<?php }?>
							<div class="innerMAll">
								<div>
									<textarea type="text"  id = "modalComment"  class="form-control preview can-mention-user inputor" name = "modalComment"  data-id="video-preview-description" data-limit="" placeholder="Add Description"><?php
                                        if(isset($postDetail['_id'])) {
                                            echo preg_replace('/<\/?a[^>]*>/', '', rawurldecode($postDetail["post_short_desc"]));
                                        }
                                        ?></textarea>
								</div>
							</div>
							<div class="innerMAll clearfix">
								<div id="tags" class="tag-text"  data-id="video-selected-keywords">
								<?php
									$keywordinputValue = "";
                                $keywords = "";
                                if(isset($postDetail['_id'])){
                                    foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                                        $keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
                                        $keywordinputValue .= "#".$keywordValue."  ";
                                    }
                                    $keywords = rtrim($keywords,', ');
                                }

								?>
								<?php
									echo $keywords;
									if (!empty($postDetail['_id'])) {
								?>
									<input type = "text" id="videoKeywords" value = "<?php echo $keywordinputValue; ?>" readonly disabled>
								<?php
									}
								?>
									<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
									<input type="hidden" name = "modalKeywords" />
								</div>
							</div>
							<div class="innerMAll pull-left form-select-dropdown">
								<select class="selectpicker" name="postscope">
									<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
									<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
									<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
								</select>
							</div>
							<div class="clearfix">
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Preview" id="videoPreview" class="form-preview-btn"/>
								</div>
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Post" class="form-post-btn" id="videoPost" onclick = "createNewPost('video', 'video-post' ,'<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>')"/>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="video-preview-form clearfix innerAll">
					<div class="col-xs-12">
						<div class="social-timeline-profile-pic pull-left">
							<img src="<?php echo $sessionImgSrc; ?>" class=" img-responsive"  />
						</div>
						<div class="social-timeline-profile-details pull-left innerL">
							<div class="social-user-link-name">
								<a href="#">
							<?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"];?>
								</a>
							</div>
							<!---->
							<div class="social-user-link">
								<a href="#">
									<?php echo $_SESSION["account_handle"]; ?>
								</a>
							</div>
							<!--social-user-link-->
						</div>
					</div>
					<div class="col-xs-12 innerMT">
						<div class="social-keywords-tags social-timeline-keywords-details half innerB" id="video-selected-keywords">

						</div>
						<div class="innnerB" id="video-preview-description" style="color: #535353;">

						</div>
						<div class="innerB innerT" >
							<!-- video Preview -->

							<div id="video-input">

							</div>
						</div>
						<div class="hide-preview clearfix pull-right">
							<!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
							<span><i class='compress-icon fa fa-angle-up'></i></span>
						</div>
					</div>
				</div>

	<?php
			break;
	   		case 'audio':
	?>
				<form id="audio-post" class="innerAll padding-bottom-none padding-top-none margin-bottom-none" name="audio-post">
					<div class="audio-form-modal">
						<div class="audio-form">
							<div class="innerMAll audio-add-link">
								<input type="hidden" name="post-type" value="audio">
								<input type="hidden" name="post-id" value="<?php
                                if(isset($postDetail['_id'])){
                                    echo $postDetail['_id'];
                                }
                               ?>">
								<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
								<input type="hidden" name="post-create-time" value="<?php
                                if(isset($postDetail['_id'])) {
                                    echo $postDetail['created_at'];
                                }
                                ?>">
								<div>
									<input type="text" data-id="audio-input" id="audio-input" class="form-control audio-card checkEmpty checkSoundCloudLink" name="asset_url" placeholder="Sound Cloud Link/IFrame" value = "<?php
                                    if(isset($postDetail['_id'])) {
                                        echo $postDetail['post_details']['asset_url'];
                                    }
                                    ?>"/>
								</div>
								<a data-toggle="tooltip" title="Sound cloud" class="sound-cloud-icon">
									<img src="<?php echo $rootUrlImages; ?>sound_cloud_form_icon.png" alt=""/>
								</a>
							</div>
							<?php if (!empty($postDetail['_id'])) {?>
							<div class="social-timeline-sound">
								<div class="row">
									<div class="col-xs-12">
										<iframe width="100%" height="150" scrolling="no" frameborder="no"
										src="<?php echo $postDetail['post_details']['asset_url']; ?>">
										</iframe>
									</div>
								</div>
							</div>
							<?php }?>
							<div class="innerMAll">
								<div>
									<textarea type="text" id = "modalComment" class="form-control preview can-mention-user inputor" name = "modalComment" data-id="audio-preview-description" data-limit="" placeholder="Add Description"><?php
                                        if(isset($postDetail['_id'])) {
                                            echo preg_replace('/<\/?a[^>]*>/', '', rawurldecode($postDetail['post_short_desc']));
                                        }

                                        ?></textarea>
								</div>
							</div>
							<div class="innerMAll clearfix">
								<div id="tags" class="tag-text"  data-id="image-selected-keywords">
								<?php
									$keywordinputValue = "";
                                     $keywords ="";
									if(isset($postDetail['_id'])){
                                        foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                                            $keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
                                            $keywordinputValue .= "#".$keywordValue."  ";
                                        }
                                        $keywords = rtrim($keywords,', ');
                                    }

								?>
								<?php
									echo $keywords;
									if (!empty($postDetail['_id'])) {
								?>
									<input type = "text" id="audioKeywords"  value = "<?php echo $keywordinputValue; ?>" readonly disabled>
								<?php
									}
								?>
									<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
									<input type="hidden" name = "modalKeywords" />
								</div>
							</div>
							<div class="innerMAll pull-left form-select-dropdown">
								<select class="selectpicker" name="postscope">
									<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
									<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
									<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
								</select>
							</div>
							<div class="clearfix">
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Preview" id="audioPreview" class="form-preview-btn"/>
								</div>
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Post" class="form-post-btn" id="audioPost" onclick = "createNewPost('audio', 'audio-post' ,'<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>')"/>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="audio-preview-form clearfix innerAll">
					<div class="col-xs-12">
						<div class="social-timeline-profile-pic pull-left">
							<img src="<?php echo $sessionImgSrc; ?>" class=" img-responsive"  />
						</div>
						<div class="social-timeline-profile-details pull-left innerL">
							<div class="social-user-link-name">
								<a href="#">
									<?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"];?>
								</a>
							</div>
							<!---->
							<div class="social-user-link">
								<a href="#">
										<?php echo $_SESSION["account_handle"]; ?>
								</a>
							</div>
							<!--social-user-link-->
						</div>
					</div>
					<div class="col-xs-12 innerMT">
						<div class="social-keywords-tags social-timeline-keywords-details half innerB" id="image-selected-keywords">

						</div>
						<div class="innnerB" id="audio-preview-description" style="color: #535353;">

						</div>
						<div class="innerB innerT iframe-box">
							<div class="loader-audio-form">
								<i class='fa fa-spinner fa-pulse fa-fw'></i>
							</div>
                            <div id="audio-preview-wrapper"> </div>
						</div>
						<div class="hide-preview clearfix pull-right">
							<!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
							<span><i class='compress-icon fa fa-angle-up'></i></span>
						</div>
					</div>
				</div>

	<?php
			break;
	   		case 'blog':
	?>
	<!-- Blog Form starts-->
				<form id="blog-post" class="innerAll padding-bottom-none padding-top-none" name="blog-post" enctype = "multipart/form-data" >
					<div class="blog-form-modal">
						<div class="blog-form clearfix">
							<div class="innerMAll">
								<input type="hidden" name="post-type" value="blog">
								<input type="hidden" name="post-id" value="<?php echo $postDetail['_id']; ?>">
								<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
								<input type="hidden" name="post-create-time" value="<?php echo $postDetail['created_at']; ?>">
								<div>
									<input type="text" id="blog-input" data-id="blog-title" class="form-control blog-card preview checkEmpty" placeholder="Blog Title" name="modalTitle" value = "<?php
                                    if(isset($postDetail['_id'])) {
                                        echo $postDetail['post_details']['blog_title'];
                                    }
                                    ?>"/>
								</div>
							</div>
							 <div class="hero-unit innerT innerB innerML innerMB innerMR clearfix checkEmpty">
								<div id="alerts"></div>
								<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
									<div class="btn-group  margin-bottom-none">
										<a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="fa fa-font"></i><b class="caret"></b></a>
										<ul class="dropdown-menu">
										</ul>
									</div>
									<div class="btn-group  margin-bottom-none">
										<a class="dropdown-toggle social-form-btn" style="padding: 0px 6px;" data-toggle="dropdown" title="Font Size"><i class="fa fa-text-height" style="padding: 6px 0px;"></i>&nbsp;<b class="caret"></b></a>
										<ul class="dropdown-menu">
											<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
											<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
											<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										</ul>
									</div>
									<div class="btn-group">
										<a class="social-blog-form-btn" data-edit="bold" data-toggle="tooltip" title="Bold (Ctrl/Cmd+B)"><i class="fa fa-bold"></i></a>
										<a class="social-blog-form-btn" data-edit="italic" data-toggle="tooltip" title="Italic (Ctrl/Cmd+I)"><i class="fa fa-italic"></i></a>
										<a class="social-blog-form-btn" data-edit="strikethrough" data-toggle="tooltip" title="Strikethrough"><i class="fa fa-strikethrough"></i></a>
										<a class="social-blog-form-btn" data-edit="underline" data-toggle="tooltip" title="Underline (Ctrl/Cmd+U)"><i class="fa fa-underline"></i></a>
									</div>
									<div class="btn-group">
										<a class="social-blog-form-btn" data-edit="insertunorderedlist" data-toggle="tooltip" title="Bullet list"><i class="fa fa-list-ul"></i></a>
										<a class="social-blog-form-btn" data-edit="insertorderedlist" data-toggle="tooltip" title="Number list"><i class="fa fa-list-ol"></i></a>
										<a class="social-blog-form-btn" data-edit="outdent" data-toggle="tooltip" title="Reduce indent (Shift+Tab)"><i class="fa fa-outdent"></i></a>
										<a class="social-blog-form-btn" data-edit="indent" data-toggle="tooltip" title="Indent (Tab)"><i class="fa fa-indent"></i></a>
									</div>
									<div class="btn-group">
										<a class="social-blog-form-btn" data-edit="justifyleft" data-toggle="tooltip" title="Align Left (Ctrl/Cmd+L)"><i class="fa fa-align-left"></i></a>
										<a class="social-blog-form-btn" data-edit="justifycenter" data-toggle="tooltip" title="Center (Ctrl/Cmd+E)"><i class="fa fa-align-center"></i></a>
										<a class="social-blog-form-btn" data-edit="justifyright" data-toggle="tooltip" title="Align Right (Ctrl/Cmd+R)"><i class="fa fa-align-right"></i></a>
										<a class="social-blog-form-btn" data-edit="justifyfull" data-toggle="tooltip" title="Justify (Ctrl/Cmd+J)"><i class="fa fa-align-justify"></i></a>
									</div>
									<div class="btn-group">
										<a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" data-toggle="tooltip" title="Hyperlink"><i class="fa fa-link"></i></a>
										<div class="dropdown-menu input-append">
											<input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
											<button class="btn" type="button">Add</button>
										</div>
										<a class="social-blog-form-btn" data-edit="unlink" data-toggle="tooltip" title="Remove Hyperlink"><i class="fa fa-cut"></i></a>

									</div>

									<div class="btn-group fileUpload">
										<a class="social-blog-form-btn" data-toggle="tooltip" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="fa fa-picture-o"></i></a>
										<input type="file" class="upload" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
									</div>
									<div class="btn-group">
										<a class="social-blog-form-btn" data-edit="undo" data-toggle="tooltip" title="Undo (Ctrl/Cmd+Z)"><i class="fa fa-undo"></i></a>
										<a class="social-blog-form-btn" data-edit="redo" data-toggle="tooltip" title="Redo (Ctrl/Cmd+Y)"><i class="fa fa-repeat"></i></a>
									</div>
									<input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
								</div>

								<div id="editor" palceholder="Blog content" class="blog-preview" data-id="blog-preview-description" contenteditable="true">
									<?php
                                    if(isset($postDetail['_id'])) {
                                        echo preg_replace('/<\/?a[^>]*>/', '', rawurldecode($postDetail['post_details']['blog_content']));
                                    }
                                    ?>
								</div>

							</div>
							<!-- hero-unit -->
							<div class="innerMAll clearfix">
								<div id="tags" class="tag-text"  data-id="blog-selected-keywords">
								<?php
									$keywordinputValue = "";
                                $keywords= "";
                                    if(isset($postDetail['_id'])) {

                                        foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                                            $keywords .= "<span style = 'display:none;'>#" . $keywordValue . "</span> ";
                                            $keywordinputValue .= "#" . $keywordValue . "  ";
                                        }
                                        $keywords = rtrim($keywords, ', ');
                                    }
								?>
								<?php
									echo $keywords;
									if (!empty($postDetail['_id'])) {
								?>
									<input type = "text" id="blogKeywords"  value = "<?php echo $keywordinputValue; ?>" readonly disabled>
								<?php
									}
								?>
									<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
									<input type="hidden" name = "modalKeywords" />
								</div>
							</div>
							<div class="innerMAll blog-add-link">
								<div>
								<input type="text" id="blog-img-path" class="form-control imageCheck blog-card" placeholder="Add Images : " value  = "<?php
                                    if(isset($postDetail["_id"])) {
                                        echo $postDetail['post_details']['img_file_name'];
                                    }
                                    ?>"readonly disabled/>
								</div>
								<div class="fileUpload blog-form-icon">
									<span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
									<span class="browse-span">Browse</span>
									<input type="file" class="upload" id="blog-image-upload" name="fileToUpload" onchange = "imagePostPreview('#blog-image-upload','#image-feature-preview',null,'');"/>
								</div>
								<?php
									if (!empty($postDetail["post_details"]["img_file_name"])) {
										$imageMiddleUrl = $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/blog/featured/" . $ext[1] . "/" . $postDetail["_id"] . '_' . $postDetail["created_at"] . '_' . $postDetail["post_details"]["img_file_name"] . ".725x500." . $ext[1];
									} else {
										$imageMiddleUrl = "";
									}

								?>
								<div class = "img-responsive" style = " margin-top:20px;">
									<img id = "image-feature-preview" class = "img-responsive" src = "<?php if (!empty($postDetail['_id'])) {echo $imageMiddleUrl; }?>">
								</div>

							</div>
							<input type="hidden" name="imageUrl" value="<?php echo $postDetail['post_details']['img_file_name']; ?>">
							<div class="innerMAll pull-left form-select-dropdown">
								<select class="selectpicker" name="postscope">
									<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
									<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
									<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
								</select>
							</div>
							<div class="clearfix">
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Preview" id="blogPreview" class="form-preview-btn"/>
								</div>
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Post" class="form-post-btn" id="blogPost" onclick = "createNewPost('blog', 'blog-post' ,'<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>')"/>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="blog-preview-form clearfix innerAll">
					<div class="col-xs-12">
						<div class="social-timeline-profile-pic pull-left">
							<img src="<?php echo $sessionImgSrc; ?>" class=" img-responsive"  />
						</div>
						<div class="social-timeline-profile-details pull-left innerL">
							<div class="social-user-link-name">
								<a href="#">
									<?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"];?>
								</a>
							</div>
							<!---->
							<div class="social-user-link">
								<a href="#">
										<?php echo $_SESSION["account_handle"]; ?>
								</a>
							</div>
							<!--social-user-link-->
						</div>
					</div>
					<div class="col-xs-12 innerMT">
						<div class="social-keywords-tags social-timeline-keywords-details half innerB" id="blog-selected-keywords">

						</div>
						<div class="innerB" id="blog-title">

						</div>
						<div class="innnerB" id="blog-preview-description" style="color: #535353;">

						</div>
						<div class="innerB innerT post-display-img">
							<img class="img-preview img-responsive" src=""  style="display:none;"/>
							<!-- image Preview -->
						</div>
						<div class="hide-preview clearfix pull-right">
							<!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
							<span><i class='compress-icon fa fa-angle-up'></i></span>
						</div>
					</div>
				</div>


	<?php
			break;
	   		case 'image':
	?>
				<form id="image-post" name="image-post" class="innerAll padding-bottom-none padding-top-none" enctype = "multipart/form-data">
					<div class="image-form-modal">
						<div class="image-form">
							<div class="innerMAll image-form-add-image">
								<div>
									<input type="text" id="image-input" class="form-control image-card imageCheck checkEmpty" placeholder="Image URL" value = "<?php
                                    if(isset($postDetail['_id'])) {
                                        echo $postDetail['post_details']['img_file_name'];
                                    }
                                    ?>"readonly disabled/>
								</div>
								<div class="fileUpload image-form-icon">
									<span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
									<span class="browse-span">Browse</span>
									<input type="file" id="image-form-upload" class="upload" name="fileToUpload" onchange = "imagePostPreview('#image-form-upload','#image-feature-preview',null,'');"/>
								</div>
								<?php
                                      if(isset($postDetail['_id'])) {
                                          if (!isset($postDetail['updated_at'])) {
                                              $imagePostMiddleUrl = $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postDetail["_id"] . '_' . $postDetail["created_at"] . '_' . $postDetail["post_details"]["img_file_name"] . ".725x500." . $ext[1];
                                          } else {
                                              $imagePostMiddleUrl = $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postDetail["_id"] . '_' . $postDetail["created_at"] . '_' . $postDetail["post_details"]["img_file_name"] . ".725x500." . $ext[1];
                                          }
                                      }
								?>
								<div class = "img-responsive" style = " <?php //if (empty($postDetail['_id'])) {echo 'display:none'; }?>; margin-top:20px;">
									<img id = "image-feature-preview" class = "img-responsive" src = "<?php if (!empty($postDetail['_id'])) {echo $imagePostMiddleUrl;} ?>">
								</div>
							</div>
							<div class="innerMAll">
								<input type="hidden" class="form-control" name="post-type" value="image">
								<input type="hidden" name="post-id" value="<?php
                                if(isset($postDetail['_id'])){
                                    echo $postDetail['_id'];
                                }
                                ?>">
								<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
								<input type="hidden" name="imageUrl" value="<?php
                                if(isset($postDetail['_id'])) {
                                    echo $postDetail['post_details']['img_file_name'];
                                }
                                ?>">
								<input type="hidden" name="post-create-time" value="<?php
                                 if(isset($postDetail['_id'])) {
                                     echo $postDetail['created_at'];
                                 }
                                ?>">
								<div>
									<textarea type="text" id = "modalComment" class="form-control preview can-mention-user inputor" name = "modalComment" data-id="image-preview-description" data-limit=""  placeholder="Add Description"><?php
                                        if(isset($postDetail['_id'])) {
                                            echo preg_replace('/<\/?a[^>]*>/', '', rawurldecode($postDetail["post_short_desc"]));
                                        }

                                        ?></textarea>
								</div>
							</div>
							<div class="innerMAll clearfix">
								<div id="tags" class="tag-text"  data-id="image-selected-keywords">
								<?php
                                $keywords ="";
                                if(isset($postDetail['_id'])) {
                                    $keywordinputValue = "";
                                    foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                                        $keywords .= "<span style = 'display:none;'>#" . $keywordValue . "</span> ";
                                        $keywordinputValue .= "#" . $keywordValue . "  ";
                                    }
                                    $keywords = rtrim($keywords, ', ');
                                }
								?>
								<?php
									echo $keywords;
									if (!empty($postDetail['_id'])) {
								?>
									<input type = "text" id="imageKeywords"  value = "<?php echo $keywordinputValue; ?>" readonly disabled>
								<?php
									}
								?>

									<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
									<input type="hidden" name = "modalKeywords" />
								</div>
							</div>
							<div class="innerMAll pull-left form-select-dropdown">
								<select class="selectpicker" name="postscope">
									<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
									<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
									<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
								</select>
							</div>
							<div class="clearfix">
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Preview" id="imagePreview" class="form-preview-btn"/>
								</div>
								<div class="innerAll clearfix pull-right">
									<input type="button" value="Post" class="form-post-btn" id="imagePost" onclick = "createNewPost('image', 'image-post' ,'<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>')"/>
								</div>
							</div>
						</div>
					</div>
				</form>
				<div class="image-preview-form clearfix innerAll">
					<div class="col-xs-12">
						<div class="social-timeline-profile-pic pull-left">
							<img src="<?php echo $sessionImgSrc; ?>" class=" img-responsive"  />
						</div>
						<div class="social-timeline-profile-details pull-left innerL">
							<div class="social-user-link-name">
								<a href="#">
									<?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"];?>
								</a>
							</div>
							<!---->
							<div class="social-user-link">
								<a href="#">
										<?php echo $_SESSION["account_handle"]; ?>
								</a>
							</div>
							<!--social-user-link-->
						</div>
					</div>
					<div class="col-xs-12 innerMT">
						<div class="social-keywords-tags social-timeline-keywords-details half innerB" id="image-selected-keywords">

						</div>
						<div class="innnerB" id="image-preview-description" style="color: #535353;">

						</div>
						<div class="innerB innerT  post-display-img">
							<img class="img-preview img-responsive" src=""  style="display:none;"/>
							<!-- image Preview -->
						</div>
						<div class="hide-preview clearfix pull-right">
							<!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
							<span><i class='compress-icon fa fa-angle-up'></i></span>
						</div>
					</div>
				</div>

	<?php
			break;
	   		case 'status':
	?>

				<form name="status-post-modal" id="status-post-modal" class="margin-none">
					<div class="innerMAll">
						<input type="hidden" class="form-control" name="post-type" value="status">
						<input type="hidden" name="post-id" value="<?php echo $postDetail['_id']; ?>">
						<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
						<input type="hidden" name="post-create-time" value="<?php echo $postDetail['created_at']; ?>">
						<div>
							<textarea type="text"  id = "modalComment"  class="form-control checkEmpty preview can-mention-user inputor" rows = "4" name = "modalComment"  data-limit=""  placeholder="Share Something..."><?php echo preg_replace('/<\/?a[^>]*>/','',rawurldecode($postDetail["post_short_desc"]));?></textarea>
						</div>
					</div>
					<div class="innerMAll clearfix">
						<div id="tags" class="tag-text"  data-id="image-selected-keywords">
						<?php
						$keywords="";
							$keywordinputValue = "";
							foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
								$keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
								$keywordinputValue .= "#".$keywordValue."  ";
							}
							$keywords = rtrim($keywords,', ');
						?>
						<?php
							echo $keywords;
							if (!empty($postDetail['_id'])) {
						?>
							<input type = "text" id="imageKeywords"  value = "<?php echo $keywordinputValue; ?>" readonly disabled>
						<?php
							}
						?>

							<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
							<input type="hidden" name = "modalKeywords" />
						</div>
					</div>
					<div class="innerMAll pull-left form-select-dropdown">
						<select class="selectpicker" name="postscope">
							<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
							<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
							<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
						</select>
					</div>
					<div class="innerAll clearfix pull-right">
						<input type="button" value="Post" class="form-post-btn" id="statusPost" onclick = "createNewPost('status-modal', 'status-post-modal' ,'<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>')"/>
					</div>

				</form>

	<?php
			break;
			case 'share':
	?>

	<form name="share-post-modal" id="share-post-modal" class="margin-none">
		<div class="innerMAll">
			<input type="hidden" class="form-control" name="post-type" value="status">
			<input type="hidden" name="post-id" value="<?php echo $postDetail['_id']; ?>">
			<input type="hidden" name="post-method" value="<?php if (empty($postDetail['_id'])) {echo 'create'; } else {echo 'update';}?>">
			<input type="hidden" name="post-create-time" value="<?php echo $postDetail['created_at']; ?>">
			<div id ="share-post-desc">
				<textarea type="text"  id="modalComment"  class="form-control checkEmpty preview can-mention-user inputor" rows = "4" name = "modalComment"  data-limit=""  placeholder="Share Something..."><?php echo preg_replace('/<\/?a[^>]*>/','',rawurldecode($postDetail["post_short_desc"]));?></textarea>
			</div>
		</div>
		<div class="innerMAll clearfix">
			<div id="tags" class="tag-text"  data-id="image-selected-keywords">
			<?php
				$keywordinputValue = "";
                $keywords = "";
				foreach ($postDetail['post_details']['parent_post']['post_id']['keywords'] as $keywordKey => $keywordValue) {
					$keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
					$keywordinputValue .= "#".$keywordValue."  ";
				}
				$keywords = rtrim($keywords,', ');
			?>
			<?php
				echo $keywords;
				if (!empty($postDetail['_id'])) {
			?>
				<input type = "text" id="imageKeywords"  value = "<?php echo $keywordinputValue; ?>" readonly disabled>
			<?php
				}
			?>

				<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" placeholder="Add #Keywords(min 1-max 3)" value = "" />
				<input type="hidden" name = "modalKeywords" />
			</div>
		</div>
		<?php include('shareParentPostContent.php'); ?>
		<div class="innerMAll pull-left form-select-dropdown">
			<select class="selectpicker" name="postscope" id="share_post_scope">
				<option value="Public" <?php if ($postDetail['post_scope'] == 'Public'){echo "selected";}?>>Public</option>
				<option value="Followers" <?php if ($postDetail['post_scope'] == 'Followers'){echo "selected";}?>>Followers</option>
				<option value="Only Me" <?php if ($postDetail['post_scope'] == 'Only Me'){echo "selected";}?>>Only Me</option>
			</select>
		</div>
		<div class="innerAll clearfix pull-right">
			<input type="button" value="Post" class="form-post-btn" id="sharePost" onclick = "shareUserPost('<?php echo $postDetail['_id']; ?>','<?php echo $postDetail['created_at']; ?>','<?php echo $postDetail['post_details']['parent_post']['post_id']['_id'];?>','<?php echo $postDetail['post_details']['parent_post']['post_id']['created_at'];?>','<?php if (empty($postDetail["_id"])) {echo "create"; } else {echo "update";}?>', '<?php echo $postDetail['post_details']['parent_post']['post_id']['post_type']; ?>','<?php echo $postDetail['post_details']['parent_post']['post_id']['user_ref']['email']; ?>', '<?php echo implode(',', $postDetail['post_details']['parent_post']['post_id']['keywords'])?>')"/>
		</div>

	</form>

	<?php
				break;
		}
	}
?>

<script>
/*------------mention users------------------*/
var tags           = [];
var suggestionData = [];
$(function() {
  $(".can-mention-user").on("keyup", function(e) {
      if ($(this).val().length >= 1) {
          mentionOnKeyup($(this).val());
      }
  });
});

function mentionOnKeyup(mentionInput) {
    var atkeyword = mentionInput.split(' ');
    if ((atkeyword[atkeyword.length - 1]).charAt(0) == '@') {
        var getSuggestion = (atkeyword[atkeyword.length - 1]).substring(1);
        if (getSuggestion != "") {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: { lastChar: getSuggestion },
                url: '../../controllers/social/getMentionUser.php',
                async: false,
                success: function(data) {
                    if (data.errCode == -1) {
                        tags = [];
                        for (var i = 0; i < data.errMsg.length; i++) {
                            tags.push('{"name":"' + data.errMsg[i].first_name + ' ' + data.errMsg[i].last_name + '","img":"' + data.errMsg[i].profile_pic + '","job":"' + data.errMsg[i].account_handle + '","email":"' + data.errMsg[i].email + '"}');
                        }
                        suggestionData = $.map(tags, function(values) {
                            var value = JSON.parse(values);
                            return { 'name': value.name, 'img': "../../images/rightpanel-img/" + value.img, 'job': value.job, 'email': value.email };
                        });
                    }
                },
                error: function() {
                    return false;
                }
            });
            if (getSuggestion.length == 1) {
                $('.inputor').atwho({
                    at: "@" + getSuggestion,
                    data: suggestionData,
                    limit: 10,
                    displayTpl: "<li class='sugg-wrapper'><img class='sugg-img' src='${img}' height='32' width='32'/><div class='sugg-name'> ${name}<div class='keyhandle'>${job}</div> </div><div class='hidden selectedEmail'>${email}</div></li>",
                    insertTpl: '@${job}',
                });
                $('.inputor').on("inserted.atwho", function(event, query) {
                 var htmlText   = query.html(query.html());
                 console.log(htmlText.find(".selectedEmail").html());
               });
            }
        }
    }
}
/*------------end of mention users------------------*/


	$('#editor').wysiwyg();
  $('[data-toggle="tooltip"]').tooltip();
	// for upload image in blog form
	$('#blog-image-upload').change(function() {
			var filename = $('#blog-image-upload').val().replace(/C:\\fakepath\\/i, '');
	    // var filename = $('#blog-image-upload').val();
	    $('#blog-img-path').val(filename);
			$(".img-preview").css("display","inline-block");
			if(imagePreviewExtensionCheck("blog-post")){
	      $(".img-preview").css("display","inline-block");
	    }else{
	      $(".img-preview").css("display","none");
	    }
});
	//Blog preview button click
  $("#blogPreview").click(function(){
    $(".blog-preview-form").slideDown(600);
  });
	$(".hide-preview").click(function(){
    $(".audio-preview-form").slideUp(600);
    $(".image-preview-form").slideUp(600);
    $(".video-preview-form").slideUp(600);
    $(".blog-preview-form").slideUp(600);
    $(".loader-audio-form").show();
    $("#audio-iframe").hide();
  });
	$("#editor").blur(function(){
		$("#editor").find("img").addClass("img-responsive");
		displayPreview($(this));
	});
	$('div#editor').keyup(function(e){
   $("#editor").find("img").addClass("img-responsive");
	 displayPreview($(this));
});

</script>


<?php //include('../layout/social_footer.php'); ?>
