<?php
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}
//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


      $email = base64_decode($_GET["email"]);
      $type = $_GET["type"];

      $user_id="user_id,account_handle";
      $getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$user_id);
      if(noError($getUserInfo)){
          $getUserInfo             = $getUserInfo["errMsg"];
          $otherAccountHandlerId   = $getUserInfo["user_id"];
          $accountHandle           = $getUserInfo["account_handle"];
        }else{
          echo '<script type="text/javascript">
          window.location = "not_found_error.php"
          </script>';
        }
          $type                    = cleanXSS(trim(urldecode($_POST["type"])));
          $jsonFileNo              = cleanXSS(trim(urldecode($_POST["jsonFileNo"])));
          $jsonFileInnerNo         = cleanXSS(trim(urldecode($_POST["jsonFileInnerNo"])));
          $divAppendId             = cleanXSS(trim(urldecode($_POST["divAppendId"])));
          $jasonFileNo             = '';
          $remaining               = '';
          $pageScrollEnd           = 'true';
          $blockFlag               = false;
          $targetDirOtherAccountHandler = "../../json_directory/social/followerwonk/".$accountHandle."/";
          $targetDirAccountHandler      = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
          $final = array();

          $fileOfFollowings = dirToArray($targetDirOtherAccountHandler);
          foreach($fileOfFollowings as $key => $fileOfFollowingsDetails)
          {
            $Final = $targetDirOtherAccountHandler.$fileOfFollowingsDetails;
            $jsonOfFollowings = file_get_contents($Final);
            $data = json_decode($jsonOfFollowings, true);
            $a = count($data[$type]);
            if($a>0)
            {
              break;
            };
          }

          $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
          $userIdsOfAccHandler = getFollowUser($targetDirAccountHandler);

        // Get Blocked User
        $getBlockedArray   = getBlockedUser($targetDirAccountHandler);
        if (in_array($otherAccountHandlerId, $getBlockedArray)) {
         $blockFlag = true;
        }

          //Check For Block User
          if($blockFlag == true){
            echo '<script type="text/javascript">
            window.location = "not_found_error.php"
            </script>';
            exit;
          }

//          $lastWordForFirst=substr($Final, -6);
//          $firstWord = $lastWordForFirst[0];
          $dataCount = "";
          $userIdsOfType = array();
          $useerIdCount = 0;
          $setter = array();
          $endFlag = true;
          $flag = true;


          if (empty($jsonFileNo)) {
            $i = 0;
          } else {
            $i = $jsonFileNo;
          }

    for ($i; $i < count($fileOfFollowings) && $endFlag; $i++ ) {
      $filesOfType= $targetDirOtherAccountHandler.$fileOfFollowings[$i];
      $jsonOfFollowings12 = file_get_contents($filesOfType);
      $dataOfTypes = json_decode($jsonOfFollowings12, true);
      $dataCount = count($dataOfTypes[$type]);
      $lastWord  = substr($filesOfType, -6);
      $userIdsOfType = array();
      $dataIds = $dataOfTypes[$type];

    $dataCountOfBlockedUser = array();
    $finalFileOfBlockedUser = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/".$_SESSION["account_handle"]."_info_".$lastWord;
    if(file_exists($finalFileOfBlockedUser)){
        $jsonOfBlockedUser = file_get_contents($finalFileOfBlockedUser);
        $dataOfBlockedUser = json_decode($jsonOfBlockedUser, true);
        $dataCountOfBlockedUser =$dataOfBlockedUser["blocked"];
        if(empty($dataCountOfBlockedUser)){
          $dataCountOfBlockedUser = $dataCountOfBlockedUser;
        }
    }


      $appended =  0;
      if (!empty($jsonFileInnerNo) && !empty($jsonFileNo) && $i == $jsonFileNo) {
        $k = $jsonFileInnerNo;
      } else {
        $k = 0;
      }

      for($k; $k < count($dataIds) && $flag; $k++) {
        if ( $useerIdCount < 15) {
          $useerIdCount = $useerIdCount+ count($dataIds[$k]);
          $userIdsOfType[] = $dataIds[$k];
           $appended = $appended+1;
        } else {
          $flag = false;
          $endFlag = false;
          $lastWordJson = $fileOfFollowings[$i];
          $remaining = $appended;
          $jasonFileNo = $i;
        }
      }

      $word = $lastWord[0];
      $mutualElementOfFile = array_diff($userIdsOfType, $dataCountOfBlockedUser);
      // printArr($mutualElementOfFile);

      //indexing array after intersect
      $array = array_values(array_filter($mutualElementOfFile));
      if ($dataCount > 0){
        $geter = array( "count" => $dataCount, "user_id" => $array);
        $setter[$word]= $geter;
      }
    }
    $k =  "";
    if ($i == count($fileOfFollowings) && $k == $dataCount) {
      $pageScrollEnd = 'false';
    }
    $jsonCOnverts = json_encode($setter);
    $user_required_info = 'first_name,last_name,account_handle,profile_pic,user_id,email';
    $userDetailedInformation = getUserInfoForMultiUser($jsonCOnverts,$walletURLIPnotification.'api/notify/v2/',$user_required_info);
    $userDetailedInformation = $userDetailedInformation['errMsg'];
    if (empty($userDetailedInformation) && $type == "followings") { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue"> User is not following anyone.</h4>
    </div>
<?php } elseif (empty($userDetailedInformation) && $type == "followers") { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">User has no followers.</h4>
    </div>
<?php }
    $s= 0;
    foreach($userDetailedInformation as $key => $filesDetails) {
      if(!empty($filesDetails['user_id'])){
        if (count($filesDetails) > 0 ) {
            if ($s == 0) {
              if (empty($jsonFileInnerNo)) { ?>
              <div class="h4">  <?php if(!empty($divAppendId)){ echo strtoupper($key);} ?></div>
              <?php }
            } else { ?>
              <div class="h4"><?php echo strtoupper($key);?></div>
            <?php
            }
          }
      }
      ?>
        <ul class="list-unstyled social-card">
      <?php
      foreach($filesDetails['user_info'] as $key1 => $fileOfFollowingsDetails) {
        if(isset($fileOfFollowingsDetails["profile_pic"]) && !empty($fileOfFollowingsDetails["profile_pic"])){
            global $cdnSocialUrl;
            global $rootUrlImages;

            $extensionUP  = pathinfo($fileOfFollowingsDetails["profile_pic"], PATHINFO_EXTENSION);
            //CDN image path
            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $fileOfFollowingsDetails["account_handle"] . '/profile/' . $fileOfFollowingsDetails["account_handle"] . '_' . $fileOfFollowingsDetails["profile_pic"] . '.40x40.' . $extensionUP;

            //server image path
            $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$fileOfFollowingsDetails["account_handle"].'/profile/'.$fileOfFollowingsDetails["account_handle"].'_'.$fileOfFollowingsDetails["profile_pic"];

            // check for image is available on CDN
            $file = $imageFileOfCDNUP;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $imgSrc = $imageFileOfLocalUP;
            } else {
                $imgSrc = $imageFileOfCDNUP;
            }
        }else{
            $imgSrc = $rootUrlImages."default_profile.jpg";
        }
      ?>
          <li class="innerTB clearfix">
            <div class="col-xs-5 ellipses ">
              <img class="img-circle" src="<?php echo $imgSrc; ?>">
              <a class="follower-name innerL half" <?php if($fileOfFollowingsDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($fileOfFollowingsDetails['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?>   title="<?php echo ucwords($fileOfFollowingsDetails['first_name']) ." ". ucwords($fileOfFollowingsDetails['last_name']);?>"><?php echo ucwords($fileOfFollowingsDetails['first_name']) ." ". ucwords($fileOfFollowingsDetails['last_name']);?> </a>
            </div>
            <div class="col-xs-3">
              <a class="follower-id" <?php if($fileOfFollowingsDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($fileOfFollowingsDetails['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> ><?php echo '@'.$fileOfFollowingsDetails['account_handle'];?></a>
            </div>
            <?php
          $followFlag = false;
          $setButtonFlag = false;
          if ($type == 'followings') {
          $getFollowingDetails = $targetDirOtherAccountHandler.$accountHandle."_info_" .$key.".json";
            if(file_exists($getFollowingDetails)) {
              $jsonOfType = file_get_contents($getFollowingDetails);
              $dataOfType = json_decode($jsonOfType, true);
              if (in_array($fileOfFollowingsDetails['user_id'],$userIdsOfAccHandler)) {
                $followFlag = true;
              }
            }
          } elseif ($type == 'followers') {
             $getFollowingDetails = $targetDirOtherAccountHandler.$accountHandle."_info_" .$key.".json";
              if(file_exists($getFollowingDetails)) {
                $jsonOfType = file_get_contents($getFollowingDetails);
                $dataOfType = json_decode($jsonOfType, true);
                if (in_array($fileOfFollowingsDetails['user_id'],$userIdsOfAccHandler)) {
                  $followFlag = true;
                }
              }
            }   else{
              $followFlag = true;
          }

          if($_SESSION['id']==$fileOfFollowingsDetails['user_id']){
            $setButtonFlag = true;
          }else{
            $setButtonFlag = false;
          }
        ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <?php if($setButtonFlag == true){

          }else{ ?>
          <input type = "button" id ='<?php echo $fileOfFollowingsDetails["account_handle"]; ?>' class="btn-social<?php if($followFlag){ echo "-dark";} ?> pull-right follow-unfollow-button" onclick="ajaxAppFollowEvent('<?php echo $fileOfFollowingsDetails["account_handle"]; ?>','<?php echo $fileOfFollowingsDetails["email"]; ?>');" value = '<?php if($followFlag){echo "Unfollow";} else {echo "Follow";}?>'>
          <?php } ?>
        </div>

  <?php }
  ?>
        </ul>
  <?php
      $s = $s+1;
    }
  ?>

  <div id="followingAppend<?php echo $divAppendId; ?>" jsonFileNo = "<?php echo $jasonFileNo; ?>" fileReadNo = "<?php echo $remaining; ?>" pageScrollEnd = "<?php echo $pageScrollEnd; ?>"></div>
