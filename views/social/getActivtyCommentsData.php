<?php


	header("Access-Control-Allow-Origin: *");
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');
	session_start();
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once('../../helpers/deviceHelper.php');
	require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";

	if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	   print("<script>");
	   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	   print("</script>");
	   die;
	}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


	$commentPostTime   = cleanXSS(rawurldecode($_POST['lastCommTime']));
	$commentEndingDate = cleanXSS(rawurldecode($_POST['commLastDate']));
	$userId = $_SESSION['id'];
	$CommentAppendLimit  = 11;	
	$k = 0;
	$commentLastTime = '';

	if (empty($commentPostTime)) {
        $date  = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s"));
		$LastCommentTime = strtotime($date) * 1000 ;
	} else {
		$LastCommentTime = $commentPostTime;
	}
	
	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
	$ActivityCommentData     = getActivityCommentHiddenPostData($userId, $LastCommentTime,'comment', json_encode($blockUserArray));
	// printArr($ActivityCommentData);
	if (noError($ActivityCommentData)) {
		$ActivityCommentData = $ActivityCommentData['errMsg'];
?>
		<div class="row">
            <div class="col-lg-12">
		<?php
			if (count($ActivityCommentData) <= 0) {
				echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>"There is nothing to show here right now."</p><p>"You can see all your comments posted here."</p></b></center></li></ul>';
				die;
			}
			foreach ($ActivityCommentData as $date => $commentData) {
                $date          = uDateTime("m/d/y",$date);;
				if ($k < $CommentAppendLimit-1) {
				    $todayDate  = uDateTime("m/d/y",date("m/d/y"));
				if ($todayDate == $date) {
					$displayDate = 'Today';
				} else {
                    $yesterdayDate  = uDateTime("m/d/y",date("m/d/Y", strtotime("yesterday")));
					if ($yesterdayDate == $date) {
						$displayDate = 'Yesterday';
					} else {
                        $displayDate =  uDateTime("jS F Y",$date);					}
				}
				if($commentEndingDate != $date) {
		?>
					<div class="h4  innerL ">
						<?php echo $displayDate; ?>
					</div>
		<?php
				}
		?>
					<ul class="list-unstyled activity-log__list margin-none">
						<?php
							foreach ($commentData as $key => $value) {	
								$commentLastTime = $value['created_at'];
								if ($k < $CommentAppendLimit-1) {
												
								// if ($value['post_id']['post_type'] == 'share') {
								// printArr($value);
									$k = $k + 1;
									$commentLastTime = $value['created_at'];
									$commentRawtext = preg_replace('/<\/?a[^>]*>/','',$value['comment']);
					                $CommentDescCount = strlen($commentRawtext);
					                if ($CommentDescCount >= 100) {
					                  $commentText = substr($commentRawtext,0, 100).'...';
					                } else {
					                  $commentText = $commentRawtext;
					                }

					                $PostId = $value['post_id']['_id'];
					                // $commentId = $value['_id'];
					                if (!empty($value['comment_id'])) {
					                	$commentRplyId = $value['comment_id'];
					                	$commentType = 'reply';
					                } else {
					                	$commentRplyId = '';
					                	$commentType = 'comment';
					                }
								switch ($value['post_id']['post_type']) {
									case 'blog':
										//if (empty($value['post_id']['post_details']['img_file_name'])) {
											$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
										//} else {
											//$extBlog = explode(".",$value['post_id']["post_details"]["img_file_name"]);
											//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_id"]["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value['post_id']["_id"] . '_' . $value['post_id']["created_at"] . '_' . $value['post_id']["post_details"]["img_file_name"] .'" alt="...">';
										//}
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_details"]["blog_title"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
						                // $commentText = $value['comment'];
										break;
									case 'video':
										// $YoutubeVideoId = getYoutubeIdFromUrl($value['post_id']['post_details']['asset_url']);
										$imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16 "></i>';
                                        if(isset($value['post_id']['post_details']['video_desc'])){
                                            $videoTitle = $value['post_id']['post_details']['video_desc'];
                                        }else{
                                            $videoTitle = "";
                                        }


                                        if(isset($value['post_id']['post_short_desc'])){
                                            $videoDescription = $value['post_id']['post_short_desc'];
                                        }else{
                                            $videoDescription = "";
                                        }


                                        if (empty($videoDescription) && !(empty($videoTitle)) ) {;
                                            $postDesc = $videoTitle;
                                        } else if (!empty($videoDescription)) {
                                            $postDesc = preg_replace('/<\/?a[^>]*>/','',$videoDescription);
                                        } else {
                                            $postDesc = 'Title Unavailable';
                                        }
                                        $postDescCount = strlen( $postDesc);
                                        if ($postDescCount >= 50) {
                                            $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
                                        } else {
                                            $postContent = $postDesc;
                                        }

										break;
									case 'audio':
										$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
										if (!empty($value['post_id']["post_short_desc"])) {
											$postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_short_desc"]);
							                $postDescCount = strlen($postDesc);
							                if ($postDescCount >= 50) {
							                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							                } else {
							                  $postContent = $postDesc;
							                }
						            	} else {
						            		$postContent = 'Title Unavailable';
						            	}						      
										break;
									case 'image':
										//if (empty($value['post_id']['post_details']['img_file_name'])) {
                                            $imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center innerT f-sz16"></i>';
                                            //$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
										//} else {
											//$extImg = explode(".",$value['post_id']["post_details"]["img_file_name"]);
											//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value['post_id']["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value['post_id']["_id"] . '_' . $value['post_id']["created_at"] . '_' . $value['post_id']["post_details"]["img_file_name"] .'" alt="...">';
										//}
										if (!empty($value['post_id']["post_short_desc"])) {
											$postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_short_desc"]);
							                $postDescCount = strlen($postDesc);
							                if ($postDescCount >= 50) {
							                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							                } else {
							                  $postContent = $postDesc;
							                }
						            	} else {
						            		$postContent = 'Title Unavailable';
						            	}
										break;
									case 'status':
										$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
						                
										break;
									case 'share':

										$imagePath = '<i class="fa fa-share-square-o media-object--activity-log media-object--activity-log--icon text-white text-center f-sz16 " style = "background-color: #5cb85c"></i>';
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value['post_id']["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
										// $sharePostType = $value['post_details']['parent_post']['post_id']['post_type'];

										// switch ($sharePostType) {
										// 	case 'blog':
										// 		$shareBlogImgPath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
										// 		if (empty($shareBlogImgPath)) {
										// 			$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
										// 		} else {
										// 			$extBlog = explode(".",$shareBlogImgPath);
										// 			$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareBlogImgPath .'" alt="...">';
										// 		}
										// 		$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
								  //               $postDescCount = strlen($postDesc);
								  //               if ($postDescCount >= 50) {
								  //                 $postContent = substr($postDesc,0, 50).'...';
								  //               } else {
								  //                 $postContent = $postDesc;
								  //               }

										// 		break;
										// 	case 'video':
										// 		$shareVideoPath = $value['post_details']['parent_post']['post_id']['post_details']['asset_url'];
										// 		$YoutubeVideoId = getYoutubeIdFromUrl($shareVideoPath);
										// 		$imagePath = '<img class="media-object media-object--activity-log " src="'. $YoutubeVideoId .'" alt="...">';
										// 		$videoTitle = getYoutubeTitleFromUrl($shareVideoPath);
										// 		if ($videoTitle != '') {
										// 			$postDesc = $videoTitle;
										// 		} else if (!empty($value['post_short_desc'])) {
										// 			$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
										// 		} else {
										// 			$postDesc = 'Title Unavailable';
										// 		}
										// 		$postDescCount = strlen( $postDesc);
										// 		if ($postDescCount >= 50) {
										// 			$postContent = substr($postDesc,0, 50).'...';
										// 		} else {
										// 			$postContent = $postDesc;
										// 		}
										// 		break;
										// 	case 'audio':
										// 		$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
										// 		if (!empty($value["post_short_desc"])) {
										// 			$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
									 //                $postDescCount = strlen($postDesc);
									 //                if ($postDescCount >= 50) {
									 //                  $postContent = substr($postDesc,0, 50).'...';
									 //                } else {
									 //                  $postContent = $postDesc;
									 //                }
								  //           	} else {
								  //           		$postContent = 'Title Unavailable';
								  //           	}
										// 		break;
										// 	case 'image':
										// 		$shareImagePath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
										// 		if (empty($shareImagePath)) {
										// 			$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
										// 		} else {
										// 			$extImg = explode(".",$shareImagePath);
										// 			$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareImagePath .'" alt="...">';
										// 		}
										// 		if (!empty($value["post_short_desc"])) {
										// 			$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
									 //                $postDescCount = strlen($postDesc);
									 //                if ($postDescCount >= 50) {
									 //                  $postContent = substr($postDesc,0, 50).'...';
									 //                } else {
									 //                  $postContent = $postDesc;
									 //                }
								  //           	} else {
								  //           		$postContent = 'Title Unavailable';
								  //           	}
										// 		break;
										// 	case 'status':
										// 		$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										// 		$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
								  //               $postDescCount = strlen($postDesc);
								  //               if ($postDescCount >= 50) {
								  //                 $postContent = substr($postDesc,0, 50).'...';
								  //               } else {
								  //                 $postContent = $postDesc;
								  //               }
										// 		break;								
										// 	default:
										// 		$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										// 		$postContent = 'Title Unavailable';

										// 		break;
										// } // End of Share post Switch case
										$bookPostId = $value['_id'];
						                $bookPostType = 'share';
						                $bookPostcreatedAt = $value['created_at'];
						                if(isset($value['user_ref']['email'])){
                                            $bookPostEmail = $value['user_ref']['email'];
                                        }else{
                                            $bookPostEmail = "";
                                        }

										break;							
									default:
										$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										$postContent = 'Title Unavailable';
										break;
								}// End Of switch Case

								// $('#comment-basic-details'+postId).attr('post-created-at');
								// $('#comment-basic-details'+postId).attr('post-created-at');

								// $('#comment-basic-details'+postId).attr('post-created-at');
								// $('#comment-basic-details'+postId).attr('post-created-at');
						?>
								<li id = "comment-data-delete-<?php echo $PostId; ?>" class="card innerAll border-bottom commented-list-<?php echo $value['_id']; ?>" comment-user-email = "<?php echo $_SESSION['email'];?>">
								<div id = "comment-basic-details<?php echo $value['post_id']['_id'];?>" post-created-at = "<?php echo $value['post_id']['created_at']; ?>"></div>
									<div class="media">
										<div class="media-left">
											<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']);?>" target="_blank">
												<?php echo $imagePath; ?>
											</a>
										</div>
										<div class="media-body f-sz15">
											<div class="row">
												<div class="col-xs-11">
													<div class="row">
														<div class="col-xs-6">
															<p class="ellipses innerTB half margin-none">
																<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']);?>" target="_blank">
																	<?php echo rawurldecode($postContent); ?>	
																</a>
															</p>
														</div>
														<div class="col-xs-6">
															<p class="ellipses innerTB half margin-none">
																<span class="text-color-Text-Primary-Blue ">
																	<a href= "<?php echo $rootUrl; ?>" target = "_blank">
																		<?php echo $_SESSION['first_name'].' '.$_SESSION['last_name'];?>
																	</a>
																</span> 
																commented on 
																<span class="text-color-Text-Primary-Blue ">
																	<a <?php if($value['post_id']['user_ref']['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($value['post_id']['user_ref']['email']); ?>&type=<?php echo 'all'; ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> target = "_blank">
																		<?php echo $value['post_id']['user_ref']['first_name'] . ' ' . $value['post_id']['user_ref']['last_name']; ?>
																	</a>
																</span> 
																post
															</p>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12">
															<div class="media margin-none">
																<div class="media-left">
																	<i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
																</blockquote>
															</div>
															<div class="media-body ">
																<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['post_id']['_id']) . '&timestamp=' . $value['post_id']['created_at'] . '&posttype=' . base64_encode($value['post_id']['post_type']);?>" target="_blank">
																	<blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> 
																		<?php echo $commentText; ?>
																	</blockquote>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-xs-1">
												<a class="btn btn-link" onclick = "deleteConfirmation('<?php echo $PostId; ?>', '<?php echo $value['_id']; ?>','<?php echo $commentType; ?>','<?php echo $commentRplyId; ?>');" role="button"> 
													<i class="fa fa-trash-o text-color-Text-Primary-Blue  f-sz18 pull-right"></i>
												</a>
											</div>
										</div>
									</div>
								</div>
								</li>
						<?php
								// } //end of post type if condition
							} //End of inner if loop to check no of printed data
							} //End of inner foreach
						?>													
					</ul>
				
		<?php
				} // End of outer if loop to check no of printed data
			} // End of Outer Foreach
		?>
		</div>
<?php
	} else {
		echo "<div><center>Please Try After Some Time</center></div>";
	} // End of no error if


?>

<div id = "comment-frontendArray-<?php echo $commentPostTime; ?>" comment-start = "<?php echo $commentLastTime;?>" commentDataEnd = "<?php if ($k+1 < $CommentAppendLimit) {echo 'true'; }?>" comment-prev-date = '<?php echo $date; ?>' style = "display:none;"></div>