<?php
session_start();

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

    $email      = $_SESSION["email"];

    ?>
    <main class="social-main-container inner-7x innerT" >
        <div class="container">
            <div class="col-xs-3">
                <div class="social-left-panel left-panel-modules ">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll padding-bottom-none">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG"/>
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container text-blue innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name"><?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"]?></span>
                                    <a class="my-info-name-edit pull-right">
                                        <i class="fa fa-pencil" aria-hidden="true"></i>
                                    </a>
                                </div>



						<!-- my-status  -->
				  <div class="my-info-status">
				      <a id="my-info-name-edit"
							data-type="textarea"
							title="Change Description"
							data-pk="1"
							data-placement="right"
							data-title="Enter username">
								<?php
								 	$userDetail = getUserInfo($_SESSION["email"], $walletURLIPnotification . 'api/notify/v2/', 'short_description');
								 	echo $userDetail['errMsg']['short_description'];
								?>
				      </a>
			      </div>
			    </div>
				</div>
				<!-- my-info-detail  -->
				<div class="clearfix half innerT">
					<div class="col-xs-12  innerB">
						<div class="col-xs-4 padding-none pull-left">
							<div class="text-deep-sky-blue my-info-posts">
								Posts
							</div>
							<div class="text-blue innerL my-info-posts-count">
								3
							</div>
						</div>
						<div class="col-xs-4 padding-none">
							<div class="text-deep-sky-blue my-info-following">
								Following
							</div>
							<div class="text-blue my-info-following-count inner-2x innerL">
								37
							</div>
						</div>
						<div class="col-xs-4 padding-none text-right">
							<div class="text-deep-sky-blue my-info-followers">
								Followers
							</div>
							<div class="text-blue my-info-followers-count inner-2x innerR">
								31
							</div>
						</div>
					</div>
				</div>
				<!-- posts-followers-following  -->
			</div>
			<!-- my-info-card  -->

			<div class="suggested-card">
				<div class="card social-card right-panel-modules inner-2x innerMT">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
									<h4>Suggested</h4>
								</div>
							</div>
					</div>
						<div class="suggested-module">
							<div class="row margin-none innerTB border-bottom">
								<div class="col-xs-3">
									<img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png">
								</div>
								<div class="padding-left-none col-xs-6">
									<div class="social-user-link-name">
										<a>shashi tharoor</a>
									</div>
									<div class="social-user-link">
                		<a href="#">@shahitharoor</a>
              		</div>
								</div>
								<div class=" col-xs-3">
									<a href="" class="half innerR"><i class="fa fa-check"></i></a>
									<a href=""><i class="fa fa-close"></i></a>
								</div>
							</div>

                                <div class="row margin-none innerTB border-bottom ">
                                    <div class="col-xs-3">
                                        <img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png">
                                    </div>
                                    <div class="padding-left-none col-xs-6">
                                        <div class="social-user-link-name">
                                            <a>shashi tharoor</a>
                                        </div>
                                        <div class="social-user-link">
                                            <a href="#">@shahitharoor</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="" class="half innerR"><i class="fa fa-check"></i></a>
                                        <a href=""><i class="fa fa-close"></i></a>
                                    </div>
                                </div>

                                <div class="row margin-none half innerTB border-bottom">
                                    <div class="col-xs-12">
                                        <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->

	<div class="col-xs-6">
    <div class="card social-card inner-2x innerMB">
      <div class="card social-card innerAll innerMB">
        <h5 class="text-blue margin-none">FOLLOWED KEYWORDS</h5>
      </div>
    </div>
    <div class="card social-card inner-2x innerMB">
      <div class="keyword-detail-card" id="k1">
        <!-- <div class="keyword-data innerAll clearfix">
          <div class="col-xs-8 padding-none">
            <div class="col-xs-12 half innerMB padding-none">
              <span class="text-blue">
                #Keyword
              </span>
                                </div>
                                <div class="col-xs-12 padding-none">
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Interaction :&nbsp</label>
                                            <span>23</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Earning :&nbsp</label>
                                            <span>2342K</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 innerMT">
                                <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Unfollow"/>
                                <span>
              <i class='compress-expand-icon fa fa-angle-down'></i>
            </span>
          </div>
        </div>
 -->

  <div class="keyword-data innerAll clearfix">
          <div class="col-xs-12 padding-none innerMB">
            <div class="col-xs-5 half innerL">
            <div class="row">
              <div class="col-xs-12 half innerMB padding-none">
              <span class="text-blue">
                #Keyword
              </span>
            </div>
            <div class="col-xs-12 padding-none">
                <div class="comn-lft-rght-cont">
                  <label>Interaction :&nbsp</label>
                  <span>23</span>
                </div>            
            </div>
            </div>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="row">
                <div class="col-xs-12 padding-none">
              <div class="comn-lft-rght-cont">
             &nbsp;
              </div>
            </div>
            <div class="col-xs-12 padding-none">
              <div class="comn-lft-rght-cont">
                  <label>Earning :&nbsp</label>
                  <span>2342K</span>
                </div>
            </div>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="row">
                <div class="col-xs-12">
              &nbsp;
            </div>
              <div class="col-xs-12">
              <input type="button" class="btn-social-wid-auto-dark pull-right half innerMR" value="Unfollow"/>
            <span>
              <i class='compress-expand-icon fa fa-angle-down'></i>
            </span>
            </div>
              </div>
            </div>
          </div>          
        </div>

        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
          <div class="col-xs-12 padding-none innerMB">
            <div class="col-xs-5 half innerL">
            <div class="row">
              <div class="col-xs-12 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Followed by :&nbsp</label>
                <span>500 Users</span>
              </div>
            </div>
            <div class="col-xs-12 padding-none innerMT">
              <a class="view-analytics">View Analytics</a>
            </div>
            </div>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="row">
                <div class="col-xs-12 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Current Bid :&nbsp</label>
                <span>23 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
            <div class="col-xs-12 padding-none innerMT">
              <div class="comn-lft-rght-cont">
                <label>Asking Price :&nbsp</label>
                <span>2342 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
              </div>
            </div>
            <div class="col-xs-3">
              <div class="row">
                <div class="col-xs-12">
                <input type="button" class="btn-social-wid-auto-dark pull-right half innerMR" value="Buy Now"/>
            </div>
              <div class="col-xs-12 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right half innerMR" value="Buy Now"/>
            </div>
              </div>
            </div>
          </div>          
        </div>
       <!--  <div class="keyword-detail-data innerAll clearfix" style="display:none;">
          <div class="col-xs-12 padding-none innerMB">
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Followed by :&nbsp</label>
                <span>500 Users</span>
              </div>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Current Bid :&nbsp</label>
                <span>23 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
            <div class="col-xs-4">
                <input type="button" class="btn-social-wid-auto pull-right innerMR" value="Place Bid"/>
            </div>
          </div>
          <div class="col-xs-12 padding-none">
            <div class="col-xs-4 padding-none">
              <a class="view-analytics">View Analytics</a>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Asking Price :&nbsp</label>
                <span>2342 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
            <div class="col-xs-4">
                <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Buy Now"/>
            </div>
          </div>
        </div> -->
      </div>
    </div>
    <div class="card social-card inner-2x innerMB">
      <div class="keyword-detail-card" id="k2">
        <div class="keyword-data innerAll clearfix">
          <div class="col-xs-8 padding-none">
            <div class="col-xs-12 half innerMB padding-none">
              <span class="text-blue">
                #Keyword
              </span>
                                </div>
                                <div class="col-xs-12 padding-none">
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Interaction :&nbsp</label>
                                            <span>23</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Earning :&nbsp</label>
                                            <span>2342K</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 innerMT">
                                <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Unfollow"/>
                                <span>
              <i class='compress-expand-icon fa fa-angle-down'></i>
            </span>
                            </div>
                        </div>
                        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
                            <div class="col-xs-12 padding-none innerMB">
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Followed by :&nbsp</label>
                                        <span>500 Users</span>
                                    </div>
                                </div>
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Current Bid :&nbsp</label>
                                        <span>23 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <input type="button" class="btn-social-wid-auto pull-right innerMR" value="Place Bid"/>
                                </div>
                            </div>
                            <div class="col-xs-12 padding-none">
                                <div class="col-xs-4 padding-none">
                                    <a class="view-analytics">View Analytics</a>
                                </div>
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Asking Price :&nbsp</label>
                                        <span>2342 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Buy Now"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card social-card inner-2x innerMB">
                    <div class="keyword-detail-card" id="k3">
                        <div class="keyword-data innerAll clearfix">
                            <div class="col-xs-8 padding-none">
                                <div class="col-xs-12 half innerMB padding-none">
              <span class="text-blue">
                #Keyword
              </span>
            </div>
            <div class="col-xs-12 padding-none">
              <div class="col-xs-6 padding-none">
                <div class="comn-lft-rght-cont">
                  <label>Interaction :&nbsp</label>
                  <span>23</span>
                </div>
              </div>
              <div class="col-xs-6 padding-none">
                <div class="comn-lft-rght-cont">
                  <label>Earning :&nbsp</label>
                  <span>2342K</span>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-4 innerMT">
            <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Unfollow"/>
            <span>
              <i class='compress-expand-icon fa fa-angle-down'></i>
            </span>
          </div>
        </div>
        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
          <div class="col-xs-12 padding-none innerMB">
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Followed by :&nbsp</label>
                <span>500 Users</span>
              </div>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Current Bid :&nbsp</label>
                <span>23 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
            <div class="col-xs-4">
                <input type="button" class="btn-social-wid-auto pull-right innerMR" value="Place Bid"/>
            </div>
          </div>
          <div class="col-xs-12 padding-none">
            <div class="col-xs-4 padding-none">
              <a class="view-analytics">View Analytics</a>
            </div>
            <div class="col-xs-4 padding-none">
              <div class="comn-lft-rght-cont">
                <label>Asking Price :&nbsp</label>
                <span>2342 <?php echo $keywoDefaultCurrencyName; ?></span>
              </div>
            </div>
            <div class="col-xs-4">
                <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Buy Now"/>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card social-card inner-2x innerMB">
      <div class="keyword-detail-card" id="k4">
        <div class="keyword-data innerAll clearfix">
          <div class="col-xs-8 padding-none">
            <div class="col-xs-12 half innerMB padding-none">
              <span class="text-blue">
                #Keyword
              </span>
                                </div>
                                <div class="col-xs-12 padding-none">
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Interaction :&nbsp</label>
                                            <span>23</span>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Earning :&nbsp</label>
                                            <span>2342K</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4 innerMT">
                                <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Unfollow"/>
                                <span>
              <i class='compress-expand-icon fa fa-angle-down'></i>
            </span>
                            </div>
                        </div>
                        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
                            <div class="col-xs-12 padding-none innerMB">
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Followed by :&nbsp</label>
                                        <span>500 Users</span>
                                    </div>
                                </div>
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Current Bid :&nbsp</label>
                                        <span>23 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <input type="button" class="btn-social-wid-auto pull-right innerMR" value="Place Bid"/>
                                </div>
                            </div>
                            <div class="col-xs-12 padding-none">
                                <div class="col-xs-4 padding-none">
                                    <a class="view-analytics">View Analytics</a>
                                </div>
                                <div class="col-xs-4 padding-none">
                                    <div class="comn-lft-rght-cont">
                                        <label>Asking Price :&nbsp</label>
                                        <span>2342 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <input type="button" class="btn-social-wid-auto-dark pull-right innerMR" value="Buy Now"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- social-center-panel  -->
            </div>
            <!-- col-xs-6 -->

            <div class="col-xs-3">
                <div class="social-right-panel">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-left half search-box-comments">
                                <div class="input-group">
                                    <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="" style="height: 35px;">
                                    <input type="submit" class="social-search-btn-header" value="" style="background-position: 5px 5px;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card social-card right-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-5 padding-right-none">
                                    <h4>Trending</h4>
                                </div>
                                <div class="col-xs-7 padding-left-none">
                                    <div class="trending-content">
                                        <ul class="list-inline innerMT text-right">
                                            <li>
                                                <a class="active" href="#">
                                                    <i class="fa fa-file-text"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-play-circle"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-image"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#">
                                                    <i class="fa fa-soundcloud"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--Follow keyword Module-->
                        <div class="follow-keyword-module">
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--Follow keyword Module-->
                    </div>

                    <div class="popular-blog-module">
                        <div class="card social-card right-panel-modules inner-2x innerMTB">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-5 padding-right-none">
                                        <h4>Popular</h4>
                                    </div>
                                    <div class="col-xs-7 padding-left-none">
                                        <div class="trending-content">
                                            <ul class="list-inline innerMT text-right">
                                                <li>
                                                    <a class="active" href="#">
                                                        <i class="fa fa-file-text"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#">
                                                        <i class="fa fa-play-circle"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#">
                                                        <i class="fa fa-image"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#">
                                                        <i class="fa fa-soundcloud"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--bg-light-gray-->
                            <div class="row margin-none blog-module border-bottom">
                                <a>
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png">
                                        </div>
                                        <div class="col-xs-8">
									<span class="ellipsis-multiline text-black">
										DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
									</span>
                                        </div>
                                    </div>
                                </a><div class="row margin-none innerT"><a>
                                        <div class="col-xs-7 text-black">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                    </a><div class="col-xs-5"><a>
                                        </a><a class="text-social-primary pull-right">157k Views</a>
                                    </div>
                                </div>
                            </div>
                            <!--row-->
                            <div class="row margin-none blog-module">
                                <div class="row margin-none innerT">
                                    <div class="col-xs-4">
                                        <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png">
                                    </div>
                                    <div class="col-xs-8">
									<span>
										Description
									</span>
                                    </div>
                                </div>
                                <div class="row margin-none innerT border-bottom">
                                    <div class="col-xs-7">
                                        <label>Earning :</label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                    <div class="col-xs-5">
                                        <a class="text-social-primary pull-right">157k Views</a>
                                    </div>
                                </div>
                            </div>
                            <!--row-->
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-12">
                                    <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--popular blog module-->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>


    <!-- else part if session expires -->
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
<script>
    $(".compress-expand-icon").click(function(){
        console.log($(this).parent().parent().parent().parent().attr("id"));
        if($(this).hasClass("fa-angle-down")){
            $("#"+$(this).parent().parent().parent().parent().attr("id")+" .keyword-detail-data").slideDown(600);
            $(this).attr("class","compress-expand-icon fa fa-angle-up");
        }else{
            $("#"+$(this).parent().parent().parent().parent().attr("id")+" .keyword-detail-data").slideUp(600);
            $(this).attr("class","compress-expand-icon fa fa-angle-down");
        }

    });
</script>