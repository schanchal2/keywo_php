<!--====================================================
=            include : commonDropDownOption            =
=====================================================-->
<?php
if(isset($postDetail["user_ref"]["account_handle"])){
    $accHandleInitial = $postDetail["user_ref"]["account_handle"];
}else{
    $accHandleInitial = " ";
}

  $pageName         = basename($_SERVER['SCRIPT_NAME'], ".php");
  $hideAction       = "hide";
  if ($pageName     == "otherTimelineContent") {
    $accHandleInitial = $postData["errMsg"][0]["user_ref"]["account_handle"];
} ?>
    <div class="dropdown pull-right">
        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <span class="fa fa-chevron-down"></span>
        </a>
        <?php   if (in_array($emailId, $emailofSpecialUser)) { ?>
        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
            <li>
                <a onclick="hidePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $createdAt; ?>', '<?php echo $postSharedType;?>','<?php echo  $emailId; ?>','<?php echo  $hideAction; ?>');">
                    <i class="fa fa-eye-slash"></i> Hide Post
                </a>
            </li>
            <li>
                <a onclick="setReportAccountOfUser('<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>','<?php echo  $emailId; ?>');">
                    <i class="fa fa-file-text"></i> Report Post
                </a>
            </li>
        </ul>
        <?php } else { ?>
        <?php if ($userId == $postedBy) {?>
        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
            <li>
                <!-- <a data-toggle="modal" href="#editPostDetails" onclick="editPersonalPost('<?php echo $postId; ?>','<?php echo $createdAt; ?>','<?php echo $postType; ?>','<?php echo $accHandleInitial;?>','<?php echo $emailId; ?>');"> -->
                <a onclick="editPersonalPost('<?php echo $postId; ?>','<?php echo $createdAt; ?>','<?php echo $postType; ?>','<?php echo $accHandleInitial;?>','<?php echo $emailId; ?>','update');">
                    <i class="fa fa-pencil"></i> Edit Post
                </a>
            </li>
            <li>
                <a onclick="hidePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $createdAt; ?>', '<?php echo $postSharedType;?>','<?php echo  $emailId; ?>','<?php echo  $hideAction; ?>');">
                    <i class="fa fa-eye-slash"></i> Hide Post
                </a>
            </li>
            <li>
                <a onclick="removePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $createdAt;?>');">
                    <i class="fa fa-trash"></i> Remove Post
                </a>
            </li>
        </ul>
        <?php } else {?>
        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
            <li>
                <a href="#" onclick="hidePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $createdAt; ?>', '<?php echo $postSharedType;?>','<?php echo  $emailId; ?>','<?php echo  $hideAction; ?>');">
                    <i class="fa fa-eye-slash"></i> Hide Post
                </a>
            </li>
            <li>
                <a onclick="setReportAccountOfUser('<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>','<?php echo  $emailId; ?>');">
                    <i class="fa fa-file-text"></i> Report Post
                </a>
            </li>
            <li>
                <?php
                    $followFlag = false;
                   $getFollowingArray = getFollowUser($targetDirAccountHandler);
                    if (in_array($postedBy, $getFollowingArray)) {
                    $followFlag = true;
                }  ?>
                    <a href="javascript:;" class="otherFollowDropDown" onclick="followAccountOfUser(this,'<?php echo $emailId; ?>','<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>');">
                        <?php    if($followFlag){  $buttoVal = 'Unfollow'; ?>
                        <i class="fa fa-user-times"></i>
                        <?php   } else {  ?>
                        <i class="fa fa-user"></i>
                        <?php $buttoVal = 'Follow'; } ?>
                        <?php echo $buttoVal; ?> User
                    </a>
            </li>
            <li>
                <?php
                $blockFlag = false;
                $getBlockedArray   = getBlockedUser($targetDirAccountHandler);
                if (in_array($postedBy, $getBlockedArray)) {
                    $blockFlag = true;
                }
                ?>
                    <a href="javascript:;" onclick="blockUserAccByConfirmation(this,'<?php echo $emailId; ?>','<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>');">
                        <?php  if($blockFlag){  $buttoVal = 'Unblock'; ?><i class="fa fa-user"></i>
                        <?php } else { $buttoVal = 'Block';?><i class="fa fa-user-times"></i>
                        <?php  } ?>
                        <?php echo $buttoVal;?> #<span>Account</span>
                    </a>
            </li>
        </ul>
        <?php } }?>

        <?php if($getBookMarkPost==1) { ?>
            <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star text-yellow bookmarkpop<?php echo $postId; ?>"  onclick = "setBookmarkAccountOfUser('<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>','<?php echo $emailId; ?>');"></i></span>
        <?php } else{ ?>
            <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star-o bookmarkpop<?php echo $postId; ?>"  id="" onclick = "setBookmarkAccountOfUser('<?php echo  $postId; ?>','<?php echo $postType; ?>','<?php echo $createdAt; ?>','<?php echo $emailId; ?>');"></i></span>
        <?php } ?>
        
    </div>

<?php

//$ipAddr = getClientIP();
////
////$ipAddr = '95.211.211.186';//getClientIP();
////printArr($ipAddr);
//$ipInfo = file_get_contents('http://ip-api.com/json/' . $ipAddr);
//$ipInfo = json_decode($ipInfo);
//$timezone = $ipInfo->timezone;
//date_default_timezone_set($timezone);
//$timeZoneValue =  date_default_timezone_get();
////printArr($timeZoneValue);
//$defalutTimeZone =  $timeZoneValue;
//$timeZone = date('Y-m-d H:i:s', $timestamp2);
//$createdTimeValue = new DateTime($timeZone, new DateTimeZone($timeZoneValue));
//$createdTimeValue->setTimezone(new DateTimeZone($defalutTimeZone));
//$createdTime = $createdTimeValue->format('Y-m-d H:i:s');
//$timestamp2 = strtotime($createdTime);
?>
    <br />
    <div class="pull-right social-post-time-container">
        <span class="social-post-time"><span title="<?php  echo  uDateTime("h:i A",$postDetail['created_at'])." - ".uDateTime("d M Y",$postDetail['created_at']);?>"><?php include("getDateFormate.php"); ?></span>
    </div>
    <!--====  End of include : commonDropDownOption  ====-->
