<?php
/**
 *      Social Post Details
 */
/* Add Session Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
/* Add DB Management Files */
require_once "../../config/config.php";
require_once "../../config/db_config.php";
/* Add Model */
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once "../../models/social/socialModel.php";
require_once('../../models/social/commonFunction.php');
/* Add Helpers */
require_once('../../helpers/deviceHelper.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../helpers/imageFunctions.php";

require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once('../../IPBlocker/ipblocker.php');

//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	//session is not active, redirect to login page
	print("<script>");
	print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	print("</script>");
	die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

	$bulkData   = array();
    $returnArr  = array();
	$postId     = base64_decode($_REQUEST['post_id']);
	$postType   = base64_decode($_REQUEST['posttype']);
	$postTime   = cleanXSS(trim(urldecode($_GET['timestamp'])));
	$email      = base64_decode($_REQUEST['email']);

    $emailofSpecialUser      = array();
	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

	$user_id     = "user_id,account_handle";
	$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);

	if(noError($getUserInfo)){
		$getUserInfo = $getUserInfo["errMsg"];
		$user_id     = $getUserInfo["user_id"];
		$accHandle   = $getUserInfo["account_handle"];
	}

    if(!in_array($email,$emailofSpecialUser)){
        //get Folllowed User
        $getFollowed = getFollowUser($targetDirAccountHandler);
        //check for value
        $followFlag  = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

    }else{
        $followFlag = 1;
    }

    //Get Post details
    $postDetail = getPostDetail($postId, $postTime,$_SESSION['id'], $followFlag);

    if (noError($postDetail)) {
//        printArr($postDetail);
    	$errorCode          = $postDetail["errCode"];
    	$postDetail         = $postDetail["errMsg"][0];
    	$postedBy           = $postDetail['posted_by'];
    	$image              = $postDetail['post_details']['img_file_name'];
    	$accountHandle      = $postDetail['user_ref']['account_handle'];
    	$createdAt          = $postDetail['created_at'];
     	$emailId            = $postDetail["user_ref"]["email"];
    	$postType           = $postDetail['post_type'];
        $post_earnings      = $postDetail['post_earnings'];
    	$postCollectionName = $postDetail['post_collection'][0];
    	$ext                = explode(".",$image);
    	$mil                = $postDetail['created_at'];
    	$seconds            = $mil / 1000;
        $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));


        $timestamp2         = strtotime($created_at);
    	$postViewCount      = $postDetail['post_views_count'];

	    //shared post details
		$postSharedParent          = $postDetail["post_details"]["parent_post"]["post_id"];
		$postSharedParentId        = $postSharedParent["_id"];
		$postSharedParentUserId    = $postSharedParent["user_ref"]["user_id"];
		$postSharedParentFname     = $postSharedParent["user_ref"]["first_name"];
		$postSharedParentLname     = $postSharedParent["user_ref"]["last_name"];
		$postSharedParentAccHandle = $postSharedParent["user_ref"]["account_handle"];
		$postSharedParentPostedBy  = $postSharedParent["posted_by"];
		$postSharedCreatedAt       = $postSharedParent["created_at"];
		$postSharedKeyword         = $postSharedParent["keywords"][0];
		$postSharedKeywords        = $postSharedParent["keywords"];
		$postSharedShortDesc       = $postSharedParent["post_short_desc"];
		$postSharedType            = $postSharedParent["post_type"];
		$postSharedAssetUrl        = $postSharedParent["post_details"]["asset_url"];
		$postSharedBlogTitle       = $postSharedParent["post_details"]["blog_title"];
		$postSharedImgUrl          = $postSharedParent["post_details"]["img_file_name"];
		$postSharedBlogContent     = $postSharedParent["post_details"]["blog_content"];        
        $post_share_earnings_parent = $postSharedParent["post_earnings"];

        if(isset($postDetail["user_ref"]["profile_pic"]) && !empty($postDetail["user_ref"]["profile_pic"])){
            $extensionUP  = pathinfo($postDetail["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
            //CDN image path
            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $postDetail["user_ref"]["account_handle"] . '/profile/' . $postDetail["user_ref"]["account_handle"] . '_' . $postDetail["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

            //server image path
            $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$postDetail["user_ref"]["account_handle"].'/profile/'.$postDetail["user_ref"]["account_handle"].'_'.$postDetail["user_ref"]["profile_pic"];

            // check for image is available on CDN
            $file = $imageFileOfCDNUP;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $userImgSrc = $imageFileOfLocalUP;
            } else {
                $userImgSrc = $imageFileOfCDNUP;
            }
        }else{
            $userImgSrc = $rootUrlImages."default_profile.jpg";
        }

    } else {
    	$errorCode = $postDetail["errCode"];
    	$errorMsg  = $postDetail["errMsg"];
    	$rootForRedirection =  $rootUrl."views/social/not_found_error.php";
    	print("<script>");
    	print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
    	print("</script>");
    }

    $userId = $_SESSION['id'];

    //get list of keywords
    if($postType == 'share'){
    	$keywords = $postSharedParent['keywords'];
    }else{
    	$keywords = $postDetail["keywords"];
    }
    //post view count on display popup
    $setPostViewCount = postViewCount($postId, $postTime);
    $keywordsList     = implode(' ', $keywords);

    //get user ip address
    $ipAddr = getClientIP();

    //check request for blocking IP address.
    $user_agent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    $extraArgs   = array();
//    $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywords); //$ipAddr
//
//    if (noError($checkIPInfo)) {
//    	// Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
//    	$checkIPInfo = $checkIPInfo["errMsg"];
//    	$searchCount = $checkIPInfo["search_count"];
//    	setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
//    } else {
        $searchCount = '';
//    	setErrorStack($returnArr, 11, $errMsg,  extraArgs);
//    }
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

    //Check Like post details
    $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);
    //check cdp status
    $getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);

    //Check Bookmark post details
    $getBookMarkPost  = getBookMarkPost($_SESSION["account_handle"],$postId, $postType );


    $postIddata = array(
    	'post_id' =>  $postId,
    	'time'    =>  $postTime
    );

    array_push($bulkData, $postIddata);
    $bulkPostId = json_encode($bulkData);

    // Get like post activity count from node database
    $activityCount = getPostActivityCount($bulkPostId, '');
    if(noError($activityCount)){
    	$targetActivityCount = $activityCount['errMsg'][$postId];
    	$like_count          = $targetActivityCount['like_count'];
    	$commentCount        = $targetActivityCount['comment_count'];
    	$shareCount          = $targetActivityCount['share_count'];
    	$parentLikeCount     = $targetActivityCount['parent_like_count'];
    	$parentCommentCount  = $targetActivityCount['parent_comment_count'];
    	$parentShareCount    = $targetActivityCount['parent_share_count'];
    } else {
    	$errCode   = $activityCount['errCode'];
    	$errMsg    = $activityCount['errMsg'];
    	$returnArr = setErrorStack($returnArr, $errCode, $errMsg, $extraArgs);
    }
    // Get Special User
//    $getSpecialUserData = getSpecialUserPost();
//    if(noError($getSpecialUserData)){
//    	$getSpecialUserData = $getSpecialUserData["errMsg"];
//
//    }
//    foreach($getSpecialUserData as $specialUser){
//    	$emailofSpecialUser[] = $specialUser['email'];
//    }


}
// Get Blocked User
$postCreatorId           = $postSharedParent["posted_by"];
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$userIdsOfAccHandler     = getBlockedUser($targetDirAccountHandler);
if (in_array($postedBy,$userIdsOfAccHandler)) {
    $blockFlagStatus = '1';
}else{
    $blockFlagStatus = '0';
}

if($blockFlagStatus == 1 ){
	echo '<script type="text/javascript">
	window.location = "not_found_error.php"
	</script>';
	exit;
}

?>
<input type="hidden" id="post_collection<?php echo $postId; ?>" value="<?php echo $postCollectionName; ?>">
<main class="social-main-container inner-7x innerT" >
<div class="container row-10">
<div class="row">
	<div class="col-xs-3 social-right-position">
        <div class="social-left-panel">
            <?php include("leftInfoUser.php"); ?>
            <!-- my-info-card  -->
            <div id = "popularaPostWidget">
                <?php include("widgetsOtherPost.php"); ?>
            </div>
        </div>
    <!-- social-left-panel  -->
	</div>
  	<!-- col-xs-3 -->
	<div class="col-xs-9" id="social-timeline-audio-preview">
		<div class="social-center-panel">
			<?php if($errorCode == 2){?>
				Please Enter valid Url
				<?php die; } ?>
			<?php if($errorCode != 4){?>
            <div class="social-timeline-video inner-2x innerMB">
            <div class="social-card card innerT innerLR" data-toggle="modal" data-target="#status_video_modal">
                <div class="">
                    <div class="row half innerTB">
                        <div class="col-xs-8">
                            <div class="social-timeline-profile-pic pull-left">
                                <img src="<?php echo $userImgSrc; ?>" class=" img-responsive">
                            </div>
                    <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                            <?php if ($_SESSION['id'] == $postedBy) { ?>
                                <a href="../../views/social/privateTimeline.php">
                            <?php } else { ?>
                                <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($emailId); ?>" target="_blank">
                            <?php } ?>
                                <?php echo $postDetail["user_ref"]["first_name"]; ?> <?php echo $postDetail["user_ref"]["last_name"]; ?>
                            </a>
                        </div>

                        <div class="social-user-link">
                        <?php if ($_SESSION['id'] == $postedBy) { ?>
                                    <a href="../../views/social/privateTimeline.php">
                        <?php } else { ?>
                            <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($emailId); ?>" target="_blank">
                            <?php } ?>
                            <?php echo '@' . $postDetail["user_ref"]["account_handle"]; ?>
                        </div>
                        <!--social-user-link-->
                    </div>
                    <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4 social-post-options">
                      <?php    include('commonDropDownOption.php');?>
                    </div>
                  </div>
                <!--row-->
                </div>
                <!--social-timeline-details-->
                <div class="social-timeline-keywords-details">
                    <div class="row half innerT">
                      <div class="col-xs-12">
                      <?php

                      foreach ($keywords as $keywo) {
                      ?>
                      <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keywo);?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keywo ; ?>">#<?php echo $keywo ; ?></a>
                      <?php } ?>
                      </div>
                    </div>
                </div>
                <!--social-timeline-tags-details-->

                <!--social-timeline-user-message-->
                <div class="social-timeline-content-message">
                    <div class="row">
                        <div class="col-xs-12">
                        <?php
                        //get short description with mention links
                        $shortDescriptionText = getLinksOnText(rawurldecode($postDetail["post_short_desc"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
                        $shortDescription     = $shortDescriptionText["text"];
                        $counterText          = $shortDescriptionText["counter"];

                        switch ($postType) {
                            case 'video':
                        ?>
                        <p class="innerMB">
                              <?php echo $shortDescription; ?>
                        </p>
                                <?php
                                break;
                                case 'audio':
                                ?>
                                <p class="innerMB">
                              <?php echo $shortDescription; ?>
                        </p>
                                <?php
                                break;
                                case 'image':
                                ?>
                                <p class="innerMB">
                              <?php echo $shortDescription; ?>
                        </p>

                        <?php
                            $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);

                            //CDN image path
                            $imageFileOfCDN =  $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postDetail["_id"] . '_' . $postDetail["created_at"] . '_' . $postDetail["post_details"]["img_file_name"] ; ;

                            //server image path
                            $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postDetail["user_ref"]["account_handle"] . '/post/images/' . $ext[1] . '/' . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"];

                            // check for image is avilable on CDN
                            $file = $imageFileOfCDN;
                            $file_headers = @get_headers($file);

                            if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                            $finalImagePath = $imageFileOfLocal;
                            }
                            else {
                            $finalImagePath = $imageFileOfCDN;
                            }
                        ?>
                        <img src="<?php echo $finalImagePath  ?>" class="img-responsive" />

                        <?php
                        break;
                        case 'status':
                        ?>
                        <p class="innerMB">
                          <?php echo $shortDescription; ?>
                        </p>
                        <?php
                        break;
                        case 'blog':
                            $blogDescriptionText = getLinksOnText(rawurldecode($postDetail["post_details"]["blog_content"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
                            $blogDescription     = $blogDescriptionText["text"];
                            $counterText         = $blogDescriptionText["counter"];
                        ?>
                        <div class="social-timeline-content-blog-thumb">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="text-social-primary"><?php echo $postDetail["post_details"]["blog_title"]; ?></h4>
                                </div>

                                <?php
                                if(isset($postDetail["post_details"]["img_file_name"]) && !empty($postDetail["post_details"]["img_file_name"])) {
                                    $extBlog = explode(".",$postDetail["post_details"]["img_file_name"]);
                                ?>

                                <?php
                                //CDN image path
                                $imageFileOfCDN =  $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/blog/featured/" . $ext[1] . "/" . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"];

                                //server image path
                                $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postDetail["user_ref"]["account_handle"] . '/post/blog/featured/' . $ext[1] . '/' . $postId . '_' . $createdAt  . '_' . $postDetail["post_details"]["img_file_name"];

                                // check for image is avilable on CDN
                                $file = $imageFileOfCDN;
                                $file_headers = @get_headers($file);
                                if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                $finalImagePath = $imageFileOfLocal;
                                }
                                else {
                                $finalImagePath = $imageFileOfCDN;
                                }

                                ?>
                                    <div class="col-xs-12">
                                        <img src="<?php echo $finalImagePath ?>" class="img-responsive" />
                                    </div>
                                <?php } ?>
                                <div class="col-xs-12">
                                    <p class="innerT">
                                        <?php echo $blogDescription; ?>
                                    </p>
                                </div>
                            </div>
                            </div>
                            <?php
                            break;
                            case 'share':
                            ?>
                            <p class="innerMB">
                            <?php
                            echo $shortDescription; ?>
														<div class="row border-top border-bottom">
                            <?php include('shareParentPostContent.php');?>
														</div>
                            </p>
                            <?php } ?>
                        </div>

                    </div>
                </div>
                <!--social-timeline-tags-details-->

                <?php if($postType=="video") { ?>
                <!--social-timeline-video-->
                <div class="social-timeline-content-video">
                    <div class="row">
                        <div class="col-xs-12">
                            <div align="center" class="embed-responsive embed-responsive-16by9">
                            <!-- <video loop class="embed-responsive-item">
                                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
                            </video> -->
                            <iframe width="560" height="258" src="<?php echo  $postDetail['post_details']["asset_url"]; ?>" frameborder="0" allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>
                <?php if($postType=="audio") { ?>
                    <!--social-timeline-video-->
                    <div class="social-timeline-content-video">
                        <div class="row">
                            <div class="col-xs-12">
                                <div>
                                    <!-- <video loop class="embed-responsive-item">
                                        <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
                                    </video> -->
                                    <?php if (strpos($postDetail["post_details"]["asset_url"], 'https://soundcloud.com/') !== false) { ?>
                                        <a href="<?php echo $postDetail["post_details"]["asset_url"]; ?>" target="_blank"><?php echo $postDetail["post_details"]["asset_url"]; ?></a>
                                    <?php } elseif (strpos($postDetail["post_details"]["asset_url"], 'https://w.soundcloud.com/') !== false) { ?>
                                        <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $postDetail["post_details"]["asset_url"]; ?>">
                                        </iframe>
                                    <?php } else { ?>
                                        <iframe width="560" height="258" src="<?php echo  $postDetail['post_details']["asset_url"]; ?>" frameborder="0" allowfullscreen></iframe>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php } ?>
                <!--social-timeline-video-->
                <div class="social-timeline-earning-comments-view innerT">
                    <div class="row">
                                
                        <div class="col-xs-5">
                            <div>
                                <label>Earning : </label>
                                <span>
                                  <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                        origPrice="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="">
                                <ul class="list-inline pull-right margin-bottom-none">
                                    <li>
                  <div>
                   <?php if(!empty($like_count)){ ?>
                                        <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postTime; ?>','0', '30','<?php echo $like_count;?>');"><span class="half innerR like-count<?php echo $data["_id"]; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                    <?php }  else { ?>
                                        <a id="like-count<?php echo $postId; ?>"  data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postTime; ?>','0', '30','<?php echo $like_count;?>');"><span class="half innerR like-count<?php echo $data["_id"]; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                    <?php } ?>
                    <label class="pull-right">Likes</label>
                  </div>
                </li>
                                    <li>
                                        <div>
                                            <span class="half innerR comment-count<?php echo $postDetail["_id"];?>"><?php echo formatNumberToSort("{$commentCount}", 0);?></span>
                                            <label class="pull-right">Comments</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div>
                                            <span class=""><?php echo formatNumberToSort("{$shareCount}", 0);?></span>
                                            <label>Shares</label>
                                        </div>
                                    </li>
                                    <!-- <li class="padding-right-none">
                                        <div>
                                            <span class=""><?php echo formatNumberToSort("{$postViewCount}", 0);?></span>
                                            <label>Views</label>
                                        </div>
                                    </li> -->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="social-timeline-likes-dislikes">
                <div class="row">
                <div class="col-xs-6">
                    <div class="social-likes-dislikes">
                        <ul class="list-inline margin-bottom-none">
                            <li>
                                <div>

                                <a href="JavaScript:void(0);" class="like-area">
                                    <?php
                                        if($postType == "share"){
                                            $CDPSharedBy = $postedBy;
                                            $CDPPostedBy = $postSharedParentPostedBy;
                                        } else {
                                            $CDPSharedBy = '';
                                            $CDPPostedBy = $postedBy;
                                        }
                                        if($getPostCdpStatus == 1) { ?>
                                            <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                        <?php } elseif ($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) {?>
                                            <i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                        <?php } else {
                                            if ($userId != $postedBy) { ?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-thumbs-o-up half innerR" onclick="socialCDPForPost(this,'<?php echo $postId; ?>', '<?php echo $postTime; ?>', '<?php echo $postType; ?>', '<?php echo $accountHandle; ?>', '<?php echo $CDPPostedBy; ?>', '<?php echo $CDPSharedBy; ?>', '<?php echo $searchCount; ?>', '<?php echo $ipAddr; ?>', '<?php echo $keywordsList; ?>', '<?php echo $emailId; ?>');"></i>
                                            <?php } else { ?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php }
                                          }?>
                                </a>   
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="social-comments-shares-views">
                        <ul class="list-inline pull-right margin-bottom-none">
                            <li>
                                <div>
                                    <a onclick = "commentAppendData('function','','3', '', '<?php echo $postDetail["_id"];?>');">
                                        <i class="fa fa-comments-o"></i>
                                        <span>Comments</span>
                                    </a>
                                </div>
                            </li>
                            <li class="padding-right-none">
                                <div>
                                    <a data-toggle="modal" onclick = "sharePostModal('<?php echo $postDetail['_id']; ?>','<?php echo $postDetail['created_at']; ?>','<?php echo $postDetail['post_type'];?>','<?php echo $postDetail['user_ref']['account_handle'];?>', 'create', '<?php echo $postDetail['user_ref']['email']; ?>');">
                                        <i class="fa fa-share-square-o"></i>
                                        <span>Share</span>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
          </div>
          <input type = "hidden" id = "comment-hidden-enter-event-<?php echo $postDetail["_id"]; ?>" execute-enetr-event = "true" execute-enter-event-main-page = "true">
          <div class="comments-module bg-light-purple row">
              <div class="col-xs-12">
                <?php
                  $commentPostId = $postDetail["_id"];
                  $commentPostCreationTime = $postDetail["created_at"];
                  $commentPostCreator = $postDetail['user_ref']['email'];
                  include ('postPageCommentWidget.php');
                ?>
            </div>
        </div>
        </div>
        <?php
            $relatedpostType = $postType;
            include("widgetsRelatedPost.php");
        ?>
        </div>
      </div>
        <!--social-timeline-blog-->
        <?php }else{
                $rootForRedirection =  $rootUrl."views/social/not_found_error.php";
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
                print("</script>");
         die;
        }?>
        <?php //if($postType!="status") { include("widgetsForBelowPost.php"); } ?>
    </div>
  </div>
</div>

</div>
</main>
<!-- Blog Form starts-->
<link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
<link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico" >
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
<?php include('../layout/social_footer.php'); ?>



    <!--modal to display editing data-->
    <div class="modal fade" id="editPostDetails" role="dialog">
      <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal">
          <div class="clearfix innerR innerT">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class = "modal-body">

          </div>
        </div>
      </div>
    </div>

    <!--modal to show delete confirmation of comments-->
    <div class="modal fade" id="deleteCommentModal" role="dialog">
      <div class="modal-dialog" style="margin-top: 150px;">
        <div class="card social-card clearfix social-form-modal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-blue">Delete</h4>
          </div>
          <div class = "modal-body-social-user-image">

          </div>
        </div>
      </div>
    </div>

    <!--modal to report User-->
    <div class="modal fade" id="showReportDetails" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <div class = "modal-body-report-user">

                </div>
            </div>
        </div>
    </div>

    <!--- Common modal for All ---------->

    <!-- Likes List Modal -->
    <div id="myModal_like" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal">
          <!-- Modal content-->
          <div class="" id="likeUserList">

          </div>
        </div>
      </div>
    </div>

    <!--social-timeline-modal-on-remove-post-->
    <div class="modal fade" id="removePostModal" role="dialog">
        <div class="modal-dialog">
          <div class="card social-card clearfix social-form-modal">
            <!-- Modal content-->
            <div class="">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-blue">Delete</h4>
              </div>
              <div class="modal-body">
                Are you sure you want to delete post?
              </div>
              <div class="modal-footer border-none">
								<button type="button" class="btn btn-social-wid-auto yes-remove-post-btn" data-dismiss="modal">Yes</button>
								<button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>
    </div>

    <!--social-timeline-modal-on-block-post-->
    <div class="modal fade" id="blockUserAcc" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <!-- Modal content-->
                <div class="">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-blue">Block</h4>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to Block Account?
                    </div>
                    <div class="modal-footer border-none">
                        <button type="button" class="btn btn-social-wid-auto yes-remove-block-btn" data-dismiss="modal">Yes</button>
                        <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--modal to display editing data-->
    <div class="modal fade" id="showModalPost" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <div class="clearfix closebutton-absolute">
                    <i class="fa fa-times close-button" data-dismiss="modal"></i>
                </div>
                <div class = "modal-body-social-user-image">

                </div>
            </div>
        </div>
    </div>

    <script>
    var removePostId, removePostType, removePostCreatedAt;
    function removePost(postId, postType, postCreatedAt) {
      $("#removePostModal").modal("show");
        removePostId        = postId;
        removePostType      = postType;
        removePostCreatedAt = postCreatedAt;
      }
      $(".yes-remove-post-btn").click(function(){
        removePersonalPost(removePostId, removePostType, removePostCreatedAt);
        $('#removePostModal').modal('hide');
      });

    //--------------------------      Confirmation to block User-----------------------------//
    function blockUserAccByConfirmation(ele,email,postId, postType, postCreatedAt) {
        $(ele).css('pointer-events','none');
      $('#showModalPost .close-dialog').click();
      $("#blockUserAcc").modal("show");
        emailId            = email;
        elem               = ele;
        blockPostId        = postId;
        blockPostType      = postType;
        blockPostCreatedAt = postCreatedAt;
        emailId            = email;
      }
      $("#blockUserAcc .yes-remove-block-btn").click(function(){
        $("this").prop("disabled", "disabled");
        removePersonalPostForBlockedUser(elem,emailId,blockPostId, blockPostType, blockPostCreatedAt);
        $('#blockUserAcc').modal('hide');
      });
    </script>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>