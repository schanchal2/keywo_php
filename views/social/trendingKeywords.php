<?php
  session_start();


if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
  require_once "../../config/config.php";
  require_once "../../config/db_config.php";
  require_once "../../helpers/deviceHelper.php";
  require_once "../../helpers/stringHelper.php";
  require_once "../../models/social/socialModel.php";
  require_once "../../models/social/commonFunction.php";
  require_once "../../helpers/stringHelper.php";
require_once "../../helpers/arrayHelper.php";
  require_once "../../helpers/errorMap.php";
  require_once "../../helpers/coreFunctions.php";
  require_once "../../models/keywords/keywordCdpModel.php";
  require_once "../../helpers/coreFunctions.php";
  // Database connection.
  $conn  = createDBConnection('dbkeywords');
  $email = $_SESSION["email"];

  if(noError($conn)){
      $conn = $conn["connection"];
  }else{
      print_r("Database Error");
  }


    //check session & redirect to FTU page
    $redirectUrl = $rootUrl . "/views/user/ft_like.php";
    if($_SESSION["userFteStatusFlag"]==0){
        //session is not active, redirect to login page
        print("<script>");
        print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
        print("</script>");
        die;
    }


    //get post type on click
  if (empty($_POST['postType']) || !isset($_POST['postType'])) {
    $postType = 'blog';
  } else {
    $postType = cleanXSS(urldecode($_POST['postType']));
  }
  $currentTime      = round(microtime(true) * 1000);
  $trendingKeywords = getTrendingKeywords($currentTime, $postType); //print_r($trendingKeywords); //die;
  if ($trendingKeywords["errCode"] == 5) {
    echo "<div><center>No data Found</center></div>";
  } elseif ($trendingKeywords["errCode"] == -1) {
    $trendingKeywos = $trendingKeywords["errMsg"];
    foreach ($trendingKeywos as $keywos) {
      //print_r($keywos["keyword"]);die;
      $followedKeywordStatus = getFollowKeyword($_SESSION["account_handle"], $keywos["keyword"]);
      $keywordPageUrl = "views/social/viewKeywoTimeline.php";

?>
<!--=============trending-keywords-blog===============-->
<div class="follow-keyword-module" id="" style="display:block;">
  <div class="row margin-none half innerTB border-bottom">

    <div class="col-xs-9">
      <div class="followed-keywords-name">
        <a href = "<?php echo $rootUrl.$keywordPageUrl; ?>?keyword=<?php echo urlencode($keywos['keyword']); ?>"class="tooltip-class" data-toggle="tooltip" data-placement="top" title="<?php echo $keywos["keyword"]; ?>" data-original-title="<?php echo $keywos["keyword"]; ?>">#<?php echo $keywos["keyword"]; ?></a>
      </div>
      <?php


        $keyword = $keywos["keyword"];

        $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $keyword);

        if (noError($kwdRevenueDetails)) {
          $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
          if (isset($kwdRevenueDetails["data"][0]) || !empty($kwdRevenueDetails["data"][0])) {
              $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
          } else {
              $kwdRevenueDetails = array();
          }
//          $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
            if(isset($kwdRevenueDetails["app_kwd_search_count"])){
                $interactionCount = $kwdRevenueDetails["app_kwd_search_count"];
            }else{
                $interactionCount = 0;
            }


            if(isset($kwdRevenueDetails["keyword"])){
                $keywords = $kwdRevenueDetails["keyword"];
            }else{
                $keywords = 0;
            }

            if(isset($kwdRevenueDetails["app_kwd_ownership_earnings"])){
                $userKwdSearchCount = $kwdRevenueDetails["app_kwd_ownership_earnings"];

            }else{
                $userKwdSearchCount = 0;
            }

            if(isset($kwdRevenueDetails["follow_unfollow"])){
                $CountFollowedKeyword = $kwdRevenueDetails["follow_unfollow"];
            }else{
                $CountFollowedKeyword = 0;
            }


            $keywordFollowerCounts  = 0;
            $keywordFollowerCount = json_decode($CountFollowedKeyword, true);
            $keywordFollowerCountsArray = $keywordFollowerCount["email"];
            if(isset($keywordFollowerCountsArray)){
              $keywordFollowerCount = count($keywordFollowerCounts);
            }else{
              $keywordFollowerCount = 0;
              $keywordFollowerCountsArray = array();
            }

            if (in_array($email, $keywordFollowerCountsArray)) {
                $buttoVal = 'Unfollow';
            } else {
                $buttoVal = 'Follow';
            }



//          printArr($keywordFollowerCount);
          $interactionCount = json_decode($interactionCount, true); //printArr($interactionCount);

            if(isset($interactionCount["total"])){
                $keywordInteractioncount = $interactionCount["total"];
            }else{
                $keywordInteractioncount = 0;
            }


//          if ($keywordInteractioncount == '') {
//            $keywordInteractioncount = 0;
//          }

          if ($userKwdSearchCount == '') {
            $userKwdSearchCount = 0;
          }
          $total = 0;
          $userKwdSearchCount = json_decode($userKwdSearchCount, true);

//          printArr($userKwdSearchCount);
          if($userKwdSearchCount>0){
              foreach ($userKwdSearchCount as $key => $totalEarning) {
                  $totalEarning = (double) $totalEarning;
                  $total = $total + $totalEarning;
              }
          }
        }
      ?>
      <div>
                  <label>Earning : </label>
        <span>
                       <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                   origPrice="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$total}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>

          </span>
        </span>
      </div>
    </div>
    <div class="col-xs-3 innerT">
        <input value="<?php echo $buttoVal; ?>" type="button" id="<?php echo 'fu_'.cleanXSS($keywos["keyword"]); ?>" class="social-btn-link pull-right" onclick="followUnfollow('<?php echo cleanXSS($keywos["keyword"]); ?>', '<?php echo $email; ?>')" data-trending-keyword="<?php echo cleanXSS($keywos["keyword"]);?>"/>
    </div>
  </div>
</div>
<?php } } else {
  echo "<div><center>Unable to Load</center></div>";
} ?>

<?php }
else{
    $returnArr["errCode"] = 53;
    $errMsg = "Please Login To System.";
    $returnArr = setErrorStack($returnArr, 53, $errMsg, $extraArg);
}?>
<!-- tooltip jquery code starts here -->
<script>
$(document).ready(function(){
$('[data-toggle="tooltip"]').tooltip();
});
</script>
<!-- tooltip jquery code ends here -->
