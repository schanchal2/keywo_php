<!--=====================================================
=            include : searchTimelineContent            =
======================================================-->
<?php
  session_start();
  
  ini_set('default_charset','utf-8');
  header('Content-type: text/html; charset=utf-8');

  /* Add DB Management Files */
  require_once ('../../config/config.php');
  require_once ('../../config/db_config.php');
  /*Add helpers */
  require_once ('../../helpers/arrayHelper.php');
  require_once ('../../helpers/errorMap.php');
  require_once ('../../helpers/coreFunctions.php');
  require_once ('../../helpers/imageFunctions.php');

  require_once ('../../helpers/deviceHelper.php');
  require_once ('../../helpers/stringHelper.php');

  /* Add Model */
  require_once ('../../models/social/commonFunction.php');
  require_once('../../models/wallet/walletKYCModel.php');
  require_once ('../../models/social/socialModel.php');
  require_once ('../../IPBlocker/ipblocker.php');
  require_once ('../../IPBlocker/showCaptcha.php');

  if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
     //session is not active, redirect to login page
     print("<script>");
     print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
     print("</script>");
     die;
  }

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


  //Get blocked array
  $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
  $files2                  = dirToArray($targetDirAccountHandler);
  $blockedData             = "";
  $userIdsOfAccHandler     = array();
  $returnArr               = array();
  $extraArgs               = array();
$emailofSpecialUser = array();
  $hideAction              = "hide";
  $userIdsOfAccHandler     = getBlockedUser($targetDirAccountHandler);

  //print_r($userIdsOfAccHandler); die;
  /* Search database connection */
  $searchDbConn = createDBConnection('dbsearch');
  if(noError($searchDbConn)){
    $searchDbConn = $searchDbConn["connection"];
    $msg          = "Success : Search database connection";
    $returnArr    = setErrorStack($returnArr, -1, $msg, $extraArgs);
    $getPostIntervalDetails = getSettingsFromSearchAdmin($searchDbConn);
    if(noError($getPostIntervalDetails)){
      $msg       = 'Success : Getting post interval from search admin';
      $Interval  = '';//$getPostIntervalDetails['data']['post_view_interval'];
      $returnArr = setErrorStack($returnArr, -1, $msg, $Interval);
    }else{
      $msg       =  'Error : Getting post interval from search admin';
      $returnArr = setErrorStack($returnArr, 65, $msg, null);
    }
  }else{
    $msg       =  'Error : Search database connection';
    $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);
  }

  $type              = $_POST['type']; //print_r($type); die;
  $timelineData      = json_decode($_POST['postdata']['errMsg']);
  $userId            = $_SESSION['id'];
  $data              = $timelineData->$type; //print_r($timelineData);die;
  $valueUI           = $_POST['n'];
  $checkScrollStatus = 'true';
  //print_r($data);die;
  //get nth value from UI
  if (empty($valueUI)) {
    $i = $j = 0;
  } else {
    $i =$j= $valueUI;
  }

  $userPendingInteraction = "no_of_qualified_interactions_pending,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic";
  $getSearchType          = getSearchType($_SESSION['email'], $userPendingInteraction);
  if (noError($getSearchType)) {
    $getSearchType  = $getSearchType["data"];
    $userSearchType = $getSearchType["user_search_type"];
  } else {
    print('Error: Fetching user interation type');
    exit;
  }

  $s                   = $i+10;
  $n                   = 0;
  $postDisplayIdsArray = array();
  $bulkData            = array();
  $counter             = 0;
  $bulkPostId          = array();

for ($j; $j < count($data) && $j < $s; $j++) {

    if(isset($data[$j]->_id)){
        $postIddata = array(
            'post_id' => $data[$j]->_id,
            'time'    => $data[$j]->created_at
        );
    }else{
        $postIddata = array(
            'post_id' => "",
            'time'    => ""
        );
    }
    array_push($bulkData, $postIddata);
    $bulkPostId = json_encode($bulkData);
}

// Get like post activity count from node database
    $activityCount = getPostActivityCount($bulkPostId, '');

for ($i; ($i < count($data) && $i < $s); $i++) {

    // printArr($data[$i]);

    if (!in_array($data[$i]->posted_by, $userIdsOfAccHandler)) {
    // echo "i : " .$i . "<br>";
    //echo "id : " . $data[$i]->_id . "<br>";
    //Check Like post details
    $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $data[$i]->_id, $data[$i]->post_type);
    //check cdp status
    $getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $data[$i]->_id);
    //post keywords

//    $postDisplayIds .= $data->_id.',';

    $postedBy      = $data[$i]->posted_by;
    $postId        = $data[$i]->_id ;
    $createdAt     = $data[$i]->created_at;
    $postType      = $data[$i]->post_type;
    $emailId       = $data[$i]->user_ref->email;

    $keywords        = $data[$i]->keywords;
    $post_earnings   = $data[$i]->post_earnings;
    $keywords        = implode(' ', $keywords);
    //post time
    $mil                = $data[$i]->created_at;
    $seconds            = $mil / 1000;
    $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
    $timestamp2         = strtotime($created_at);
    $postCollectionName = $data[$i]->post_collection;

    //get user ip address
    $ipAddr = getClientIP();

    //check request for blocking IP address.
    $user_agent  = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
    $extraArgs   = array();

    $targetActivityCount = $activityCount['errMsg'][$data[$i]->_id]; //printArr($targetActivityCount);
    $like_count          = $targetActivityCount['like_count'];
    $commentCount        = $targetActivityCount['comment_count'];
    $shareCount          = $targetActivityCount['share_count'];

    $postId              = $data[$i]->_id;
    $postSharedType      = $data[$i]->post_type;

 if(isset($targetActivityCount['parent_like_count']))
  {
      $parentLikeCount = $targetActivityCount['parent_like_count'];
   }else
   {
     $parentLikeCount = "";
   }


  if(isset($targetActivityCount['parent_comment_count']))
  {
      $parentCommentCount = $targetActivityCount['parent_comment_count'];
   }else
   {
     $parentCommentCount = "";
   }
  if(isset($targetActivityCount['parent_share_count']))
  {
     $parentShareCount = $targetActivityCount['parent_share_count'];
   }else
   {
     $parentShareCount = "";
   }




    //Check Bookmark post details
    $getBookMarkPost     = getBookMarkPost($_SESSION["account_handle"],$postId, $postSharedType );

    //get post short description trimmed according to "$validateCharCountOnSocial" in config file
    //get short description with mention links
        if(isset($data[$i]->post_mention)){
            $post_mention = $data[$i]->post_mention;
        }else{
            $post_mention = "";
        }
    $shortDescriptionText = getLinksOnText(rawurldecode($data[$i]->post_short_desc), $post_mention, $_SESSION["account_handle"]);
    $shortDescription     = $shortDescriptionText["text"];
  $descLenWithoutStriptag  = strlen($shortDescription);
  $descLenWithStriptag     = strlen(strip_tags($shortDescription));
  $diff                    = $descLenWithoutStriptag - $descLenWithStriptag;
  $shortDescData           = ($descLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $diff) . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $shortDescription;

?>
    <input type="hidden" id="post_collection<?php echo $data[$i]->_id; ?>" value="<?php echo $postCollectionName; ?>">
    <!--check post is shared post or not. if no shared post then-->
    <?php
        global $cdnSocialUrl;
        global $rootUrlImages;
      if(isset($data[$i]->user_ref->profile_pic) && !empty($data[$i]->user_ref->profile_pic)){
          $extensionUP  = pathinfo($data[$i]->user_ref->profile_pic, PATHINFO_EXTENSION);
          //CDN image path
          $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/profile/' . $data[$i]->user_ref->account_handle . '_' . $data[$i]->user_ref->profile_pic . '.40x40.' . $extensionUP;

          //server image path
          $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data[$i]->user_ref->account_handle.'/profile/'.$data[$i]->user_ref->account_handle.'_'.$data[$i]->user_ref->profile_pic;

          // check for image is available on CDN
          $file = $imageFileOfCDNUP;
          $file_headers = @get_headers($file);
          if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
              $imgSrc = $imageFileOfLocalUP;
          } else {
              $imgSrc = $imageFileOfCDNUP;
          }
      }else{
          $imgSrc = $rootUrlImages."default_profile.jpg";
      }  
      if ($data[$i]->post_type !== "share") {
    ?>
    <div class="social-timeline-image social-card inner-2x innerMB">
        <div class="card innerT innerLR innerMT" id="<?php echo $data[$i]->_id; ?>">
            <div class="social-card-created-at" id="<?php echo $data[$i]->created_at; ?>">
                <div class="row innerB half">
                    <div class="col-xs-8">
                        <div class="social-timeline-profile-pic pull-left">
                            <img src="<?php echo $imgSrc; ?>" class=" img-responsive" />
                        </div>
                        <div class="social-timeline-profile-details pull-left innerL">
                            <div class="social-user-link-name">
                                <?php if ($_SESSION['id'] == $data[$i]->posted_by) { ?>
                                <a href="../../views/social/privateTimeline.php">
                                    <?php } else { ?>
                                    <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($data[$i]->user_ref->email); ?>" target="_blank">
                                        <?php } ?>
                                        <?php echo $data[$i]->user_ref->first_name; ?>
                                        <?php echo $data[$i]->user_ref->last_name; ?>
                                    </a>
                            </div>
                            <!---->
                            <div class="social-user-link">
                              <?php if ($_SESSION['id'] == $data[$i]->posted_by) { ?>
                              <a href="../../views/social/privateTimeline.php">
                                  <?php } else { ?>
                                  <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($data[$i]->user_ref->email); ?>" target="_blank">
                                      <?php } ?>
                                    <?php echo '@' . $data[$i]->user_ref->account_handle; ?>
                                </a>
                            </div>
                            <!--social-user-link-->
                        </div>
                        <!--social-timeline-profile-details-->
                    </div>
                    <!--post right corner action-->
                    <div class="col-xs-4 social-post-options">
                        <?php    include('commonDropDownOption.php');?>
                    </div>
                    <!--end of post action-->
                </div>
                <!--row-->
            </div>
            <!--social-timeline-details-->
            <?php if(isset($data[$i]->keywords[0]) && !empty($data[$i]->keywords[0])) { ?>
            <div class="social-timeline-keywords-details">
                <div class="row half innerTB">
                    <div class="col-xs-12 l-h10">
                        <?php
              foreach ($data[$i]->keywords as $keyword) {
                $keywos = "<span>#".$keyword."</span> ";
                // $newKeyword = $keyword;
                // print_r($keywos); die;
                //$keywos = rtrim($keywos,', ');
            ?>
                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keyword);?>" class="social-keywords-tags half innerL" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword;?>">
                                <?php echo $keywos;?>
                            </a>
                            <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!--social-timeline-keywords-details-->
            <!-- switch case-->
            <?php
      $value = $data[$i]->post_type;
      switch ($value) {
          case 'video':
      ?>
                <!-- <div onclick="showModalPost('<?php //echo $data[$i]->_id; ?>', '<?php //echo $data[$i]->created_at; ?>', '<?php //echo $searchCount; ?>', '<?php //echo $ipAddr; ?>' , '<?php //echo  $data[$i]->user_ref->email; ?>', '<?php //echo $userSearchType; ?>');"> -->
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo  $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
             
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row half innerTB">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <?php $originUrl = $rootUrl.'views/social/'. basename($_SERVER['SCRIPT_NAME']);?>
                    <!--social-timeline-video-->
                    <div class="social-timeline-content-video">
                        <div class="row">
                            <div class="col-xs-12">
                                <?php
                                                                if(isset($data[$i]->post_details->video_thumbnail) && !empty($data[$i]->post_details->video_thumbnail)){

                                        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/post/video/' . $data[$i]->post_details->video_thumbnail;

                                        //server image path
                                        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data[$i]->user_ref->account_handle.'/post/video/'.$data[$i]->post_details->video_thumbnail;

                                        // check for image is available on CDN
                                        $file = $imageFileOfCDNUP;
                                        $file_headers = @get_headers($file);
                                        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                            $imgSrc = $imageFileOfLocalUP;
                                        } else {
                                            $imgSrc = $imageFileOfCDNUP;
                                        }
                                    }

          ?>
                                    <div align="center" class="video-div embed-responsive embed-responsive-16by9" style="background: url('<?php echo $imgSrc; ?>');">
                                        <?php
                                        if (strpos($data[$i]->post_details->video_thumbnail, 'youtube') !== false) {
                                            ?>
                                            <img class="play-img img-responsive" src="<?php echo $rootUrlImages . 'y3.png';?>">
                                            <?php
                                        } else{
                                            ?>
                                            <img class="play-img-vimeo play-img img-responsive" src="<?php echo $rootUrlImages . 'v3.png';?>">
                                            <?php
                                        }
                                        ?>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-video-->
                <?php break;
          case 'audio':






      ?>
                <!-- <div onclick="showModalPost('<?php //echo $data[$i]->_id; ?>', '<?php //echo $data[$i]->created_at; ?>', '<?php //echo $searchCount; ?>', '<?php //echo $ipAddr; ?>','<?php //echo  $data[$i]->user_ref->email; ?>', '<?php //echo $userSearchType; ?>');"> -->
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo  $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row half innerTB">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <!--social-timeline-audio-->
                    <div class="social-timeline-sound">
                        <div class="row">
                            <!-- <div class="col-xs-12" onclick="showModalPost('<?php //echo $data[$i]->_id; ?>', '<?php //echo $data[$i]->created_at; ?>', '<?php //echo $searchCount; ?>', '<?php //echo $ipAddr; ?>''<?php //echo  $data[$i]->user_ref->email; ?>', '<?php //echo $userSearchType; ?>');"> -->
                            <div class="col-xs-12" >
                                <?php if (strpos($data[$i]->post_details->asset_url, 'https://soundcloud.com/') !== false) { ?>
                                    <a href="<?php echo $data[$i]->post_details->asset_url; ?>" target="_blank"><?php echo $data[$i]->post_details->asset_url; ?></a>
                                <?php } elseif (strpos($data[$i]->post_details->asset_url, 'https://w.soundcloud.com/') !== false) { ?>
                                    <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $data[$i]->post_details->asset_url; ?>">
                                    </iframe>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-audio-->
                <?php break;
                  case 'blog':
                ?>
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo  $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                  <!--social-timeline-content-blog-->
                  <div class="social-timeline-content-blog-nothumb half innerMT">
                  <?php
                  $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);
                  //CDN image path
                  $imageFileOfCDN =  $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle .  '/post/blog/featured/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name ;

                  //server image path
                  $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data[$i]->user_ref->account_handle . '/post/blog/featured/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

                  // check for image is avilable on CDN
                  $file = $imageFileOfCDN;
                  $file_headers = @get_headers($file);
                  if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $finalImagePath = $imageFileOfLocal;
                  } else {
                    $finalImagePath = $imageFileOfCDN;
                  }
                  if (isset($data[$i]->post_details->img_file_name) && !empty($data[$i]->post_details->img_file_name)) {?>
                  <!--social-timeline-blog-image-->
                  <div class="row half innerTB">
                    <div class="col-xs-12">
                      <div class="blog-nothumb-content">
                          <a>
                            <span class="blog-nothumb-head">
                              <?php echo $data[$i]->post_details->blog_title; ?>
                            </span>
                          </a>
                          <div class="social-timeline-content-blog-thumb">
                            <div class="row">
                              <div class="col-xs-12 blog-display-img">
                              <!-- <img src="<?php // echo $finalImagePath  ?>" class="img-responsive" data-id="<?php // echo $data[$i]->_id; ?>" onclick="showModalPost('<?php // echo $data[$i]->_id; ?>', '<?php //echo $data[$i]->created_at; ?>', '<?php // echo $searchCount; ?>', '<?php // echo $ipAddr; ?>','<?php // echo  $data[$i]->user_ref->email; ?>', '<?php //echo $userSearchType; ?>');" /> -->
                              <img src="<?php echo $finalImagePath  ?>" class="img-responsive" data-id="<?php echo $data[$i]->_id; ?>" />
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--social-timeline-blog-image-->
                    <?php } else { ?>
                      <div class="row half innerTB border-top border-bottom">
                        <div class="col-xs-12">
                          <div class="blog-nothumb-content">
                            <a>
                              <span class="blog-nothumb-head">
                                <?php echo $data[$i]->post_details->blog_title; ?>
                              </span>
                            </a>
                            <span class="blog-nothumb-body">
                            <?php
                            $blogDescriptionText = getLinksOnText(rawurldecode($data[$i]->post_details->blog_content), $post_mention, $_SESSION["account_handle"]);

                          $blogDescription             = $blogDescriptionText["text"];
                          $descBlogLenWithoutStriptag  = strlen($blogDescription);
                          $descBlogLenWithStriptag     = strlen(strip_tags($blogDescription));
                          $diffBlog                    = $descBlogLenWithoutStriptag - $descBlogLenWithStriptag;
                          $blogContent                 = ($descBlogLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($blogDescription, 0, $validateCharCountOnSocial + $diffBlog) 
                          . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $blogDescription;
                          echo $blogContent; ?>
                            </span>
                          </div>
                        </div>
                      </div>
                    
                    <?php } ?>
                  </div>
                </div>
                <!--social-timeline-content-blog-->
                <?php break;
          case 'image':
      ?>
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo  $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <!--social-timeline-user-message-->
                    <div class="social-timeline-content-message">
                        <div class="row half innerTB">
                            <div class="col-xs-12">
                                <p class="innerMB">
                                    <?php echo $shortDescData; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--social-timeline-user-message-->
                    <!--social-timeline-image-->
                    <?php
        $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);

        //CDN image path
        $imageFileOfCDN = $cdnSocialUrl . 'users/' . $data[$i]->user_ref->account_handle . '/post/images/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

        //server image path
        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $data[$i]->user_ref->account_handle . '/post/images/' . $extension . '/' . $data[$i]->_id . '_' . $data[$i]->created_at . '_' . $data[$i]->post_details->img_file_name;

        // check for image is avilable on CDN
        $file = $imageFileOfCDN;
        $file_headers = @get_headers($file);
        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $finalImagePath = $imageFileOfLocal;
        }
        else {
        $finalImagePath = $imageFileOfCDN;
        }

      ?>
                        <div class="social-timeline-content-image">
                            <div class="row">
                                <div class="col-xs-12 post-display-img">
                                    <?php $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION); ?>
                                    <img src="<?php echo $finalImagePath; ?>" class="img-responsive" />
                                </div>
                            </div>
                        </div>
                </div>
                <!--social-timeline-image-->
                <?php break;
          case 'status':








      ?>
                <!--social-timeline-status-->
                <!-- <div onclick="showModalPost('<?php //echo $data[$i]->_id; ?>', '<?php //echo $data[$i]->created_at; ?>', '<?php //echo $searchCount; ?>', '<?php //echo $ipAddr; ?>','<?php //echo  $data[$i]->user_ref->email; ?>', '<?php //echo $userSearchType; ?>');"> -->
                <div class="modal--show--post" data-postid="<?php echo $data[$i]->_id; ?>" data-createdat="<?php echo $data[$i]->created_at; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo  $data[$i]->user_ref->email; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                    <div class="social-timeline-user-status half innerMT word-wrap">
                        <div class="row half innerTB border-top border-bottom">
                            <div class="col-xs-12">
                                <div class="user-status-content">
                                    <!--  -->
                                    <span class="text-dark-gray"><?php echo $shortDescData; ?></span>
                                    <!--  -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--social-timeline-status-->
                <?php break;
      }
      ?>
      <!--end of switch case-->
      <!--post floor-->
      <div class="social-timeline-earning-comments-view innerT">
        <div class="row">
          
          <div class="col-xs-5">
            <div>
              <label>Earning : </label>
              <span>
                <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                      origPrice="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
              </span>
            </div>
          </div>
          <div class="col-xs-7">
         
            <div class="">
              <ul class="list-inline pull-right margin-bottom-none">
                <li>
                  <div>
                  <?php if (!empty($like_count)) { ?>
                    <a id="like-count<?php echo $data[$i]->_id; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30' ,'<?php echo $like_count; ?>');">
                        <?php $likeCount = $like_count; ?>
                        <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$likeCount}", 0); ?></span>
                    </a>
                    <?php } else {?>
                    <a id="like-count<?php echo $data[$i]->_id; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30','<?php echo $like_count;?>');">
                    <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                    <?php } ?> 
                    <label class="pull-right">Likes</label>
                  </div>
                </li>
                <li>
                  <div>
                    <span class="half innerR comment-count<?php echo $data[$i]->_id;?>"><?php echo formatNumberToSort("{$commentCount}", 0);?></span>
                    <label class="pull-right">Comments</label>
                  </div>
                </li>
                <li>
                  <div>
                    <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0);?></span>
                    <label>Shares</label>
                  </div>
                </li>
                <!-- <li class="padding-right-none">
                  <div>
                    <span class=""><?php echo formatNumberToSort("{$data[$i]->post_views_count}", 0); ?></span>
                    <label>Views</label>
                  </div>
                </li> -->
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!--social-timeline-earning-comments-view-->

      <!--social-timeline-likes-dislikes-->
      <div class="social-timeline-likes-dislikes">
        <div class="row">
          <div class="col-xs-6">
            <div class="social-likes-dislikes">
              <ul class="list-inline margin-bottom-none">
                <li>
                  <div>
                  <a href="JavaScript:void(0);" class="like-area">
                      <?php
                                            if($getPostCdpStatus == 1) {?>
                                              <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } elseif ($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } else {
                                            if ($userId != $data[$i]->posted_by) {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-thumbs-o-up half innerR" onclick="socialCDPForPost(this,'<?php echo $data[$i]->_id; ?>', '<?php echo $data[$i]->created_at; ?>', '<?php echo $data[$i]->post_type; ?>', '<?php echo $data[$i]->user_ref->account_handle; ?>', '<?php echo $data[$i]->posted_by; ?>', '', '<?php echo $searchCount; ?>', '<?php echo $ipAddr; ?>', '<?php echo $keywords; ?>','<?php echo $data[$i]->user_ref->email; ?>');"></i>
                                            <?php } else {?>
                                            <i id="thumbs-up<?php echo $data[$i]->_id; ?>" class="likes-click fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php }
                                          }?>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="social-comments-shares-views">
                                    <ul class="list-inline pull-right margin-bottom-none">
                                        <li>
                                            <div>
                                                <a onclick="commentDivShow('<?php echo $data[$i]->_id;?>');">
                                                    <i class="fa fa-comments-o"></i>
                                                    <span>Comments</span>
                                                </a>
                                            </div>
                                        </li>
                                        <li class="padding-right-none">
                                            <div>
                                                <a onclick="sharePostModal('<?php echo $data[$i]->_id; ?>','<?php echo $data[$i]->created_at; ?>','<?php echo $data[$i]->post_type;?>','<?php echo $data[$i]->user_ref->account_handle;?>', 'create','<?php echo $data[$i]->user_ref->email; ?>');">
                                                    <i class="fa fa-share-square-o"></i>
                                                    <span>Share</span>
                                                </a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end of post floor-->
                  <div class="innerLR">
                    <input type="hidden" id="comment-hidden-enter-event-<?php echo $data[$i]->_id; ?>" execute-enetr-event="true" execute-enter-event-main-page="true">
                    <div id="comment-include-<?php echo $data[$i]->_id; ?>" class="comment-append-section comments-module bg-light-purple row" style="display:none;">
                      <div class="col-xs-12">
                        <?php
                        $commentPostId = $data[$i]->_id;
                        $commentPostCreationTime = $data[$i]->created_at;
                        $commentPostCreator = $data[$i]->user_ref->email;
                        include ('postPageCommentWidget.php');
                        ?>
                      </div>
                    </div>
                  </div>
                </div>
                <!--social-card-->
             
        </div>
        <!--if shared post then-->
        <?php } else { ?>
        <?php include("sharePostContent.php"); ?>
        <!--if closing-->
        <?php }
 } }//for closing

if ($i == count($data) - 1 || $i == count($data)) {
  $checkScrollStatus = 'false';
}
?>
        <div id="checkScrollStatus-<?php echo $valueUI; ?>" data-scroll-allow="<?php echo $checkScrollStatus; ?>"></div>

        <?php if($checkScrollStatus=="false"){ ?>
        <div class="modal-header">
            <h4 class="modal-title text-blue">End of Search Result/No Search Found</h4>
        </div>
        <?php } ?>
        <!-- Report Modal -->
        <div id="myReportModal" class="report-popup modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-blue">What kind of abuse you are reporting?</h4>
                        </div>
                        <div class="modal-body">
                            <div class="text-blue report-question">It should not be on keywo - </div>
                            <div class="radio radio-primary radio-div">
                                <input type="radio" name="gender" id="rude" value="rude" checked="true">
                                <label for="rude" style="display:block;">
                                    It's rude, vulger or uses bad language
                                </label>
                            </div>
                            <div class="radio radio-primary radio-div">
                                <input type="radio" name="gender" id="exploit" value="exploit">
                                <label for="exploit">
                                    It's sexually exploit
                                </label>
                            </div>
                            <div class="radio radio-primary radio-div">
                                <input type="radio" name="gender" id="hate" value="hate">
                                <label for="hate">
                                    It's harrasement or hate speech
                                </label>
                            </div>
                            <div class="radio radio-primary radio-div">
                                <input type="radio" name="gender" id="spam" value="spam">
                                <label for="spam">
                                    It's spamthreatening, violent or suicidal
                                </label>
                            </div>
                            <div class="radio radio-primary radio-div">
                                <input type="radio" name="gender" id="other" value="other">
                                <label for="other">
                                    Others(Specify any)
                                </label>
                                <input type="text" class="other-input-comment innerMT half" />
                            </div>
                        </div>
                        <div class="modal-footer border-none">
                            <button type="button" class="btn btn-social" data-dismiss="modal">Report</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end-remove-post-->
        <script>
        //tooltip
        $('[data-toggle="tooltip"]').tooltip();

        var removePostId, removePostType, removePostCreatedAt;

        function removePost(postId, postType, postCreatedAt) {
            $('#showModalPost .close-dialog').click();
            $("#removePostModal").modal("show");
            removePostId = postId;
            removePostType = postType;
            removePostCreatedAt = postCreatedAt;
        }
        $(".yes-remove-post-btn").click(function() {
            removePersonalPost(removePostId, removePostType, removePostCreatedAt);
            $('#removePostModal').modal('hide');
        });
             $('.modal--show--post').on('click', function(event) {
            // event.preventDefault();
            /**
             * to ignore modal popup on child "a" tag of a container for which we want modal popup.
             */
            if (event.target.nodeName != "A") {

            var postid, createdat, searchcount, ipaddr, emailid, usersearchtype;
            postid         = $(this).data("postid");
            createdat      = $(this).data("createdat");
            searchcount    = $(this).data("searchcount");
            ipaddr         = $(this).data("ipaddr");
            emailid        = $(this).data("emailid");
            usersearchtype = $(this).data("usersearchtype");
            showModalPost(postid, createdat, searchcount, ipaddr, emailid, usersearchtype) ;}
          });  
        </script>
        <?php
$postDisplayIdsArray    = json_encode($postDisplayIdsArray);
// $postDisplayIds      = rtrim($postDisplayIds,',');
$updatePostDisplayCount = updatePostDisplayCount($postDisplayIdsArray);
// print_r($updatePostDisplayCount);
?>

<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>