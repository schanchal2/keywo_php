<!DOCTYPE html>
<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();

if(isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
    $docrootpath = __DIR__;
    $docrootpath = explode('/views', $docrootpath);
    $docrootpath = $docrootpath[0] . "/";
    include("../layout/header.php");
    require_once "{$docrootpath}config/config.php";
    require_once "{$docrootpath}config/db_config.php";
    /* Add Model */
    require_once("{$docrootpath}helpers/deviceHelper.php");
    require_once("{$docrootpath}backend_libraries/xmlProcessor/xmlProcessor.php");
    require_once "{$docrootpath}helpers/arrayHelper.php";
    require_once "{$docrootpath}helpers/stringHelper.php";
    require_once "{$docrootpath}helpers/errorMap.php";
    require_once "{$docrootpath}helpers/coreFunctions.php";
    require_once "{$docrootpath}models/social/socialModel.php";
    require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
    require_once("{$docrootpath}models/social/commonFunction.php");
    require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php");

//check for session

    $email           = $_SESSION["email"];
    $clientSessionId = session_id();
    $conn            = createDBConnection('dbkeywords');

    if(noError($conn)){
        $conn = $conn["connection"];
    }else{
        print_r("Database Error");
    }

    $accountHandle  = $_SESSION["account_handle"];
    $followedKeyword = array();


    $getkeyword = getFollowFollowerData($accountHandle,"followedkeyword","technique");

    $userCartDetails = getUserCartDetails($email, $conn);

    if(noError($userCartDetails)){
        $userCartDetails = $userCartDetails["errMsg"]["user_cart"];
        $userCartDetails = json_decode($userCartDetails, TRUE);
    }else{
        print('Error: Fetching cart details');
        exit;
    }

    ?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
          type="text/css"/>

    <main class="social-main-container inner-7x innerT">
        <div class="container row-10">
            <div class="row">
                <!-- col-xs-3 -->
                <div class="col-xs-3">
                    <div class="social-left-panel">
                        <?php include("leftInfoUser.php"); ?>
                        <div class="followed-keywords-module inner-2x innerMTB">
<!--                            --><?php //include("followedKeyword.php"); ?>
                        </div>
                        <div id="suggested-user-details">
                            <?php include("suggestedUser.php"); ?>
                        </div>
                    </div>
                </div>
            <!-- col-xs-3 -->

            <div class="col-xs-6">
                <div class="card social-card inner-2x innerMB">
                    <div class="card social-card innerAll innerMB">
                        <h5 class="text-blue margin-none">FOLLOWED KEYWORDS</h5>
                    </div>
                </div>
                <?php

                $i= 0;
                $pageUrl = "views/keywords/analytics/keyword_analytics.php";
                foreach ($getkeyword as $keyword) {
                $keyword = $keyword["keyword"];

                $kwdRevenueDetails = getRevenueDetailsByKeyword($conn,$keyword); // Used for getting details from revenue table.
                if(noError($kwdRevenueDetails)) {
                    $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
                    $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                    $interactionCount = $kwdRevenueDetails["app_kwd_search_count"];
                    $userKwdSearchCount = $kwdRevenueDetails["app_kwd_ownership_earnings"];
                    $CountFollowedKeyword = $kwdRevenueDetails["follow_unfollow"];

                    $keywordFollowerCount = json_decode($CountFollowedKeyword, true);
                    $keywordFollowerCounts = $keywordFollowerCount["email"];
                    $keywordFollowerCount = count($keywordFollowerCounts);

                    // $interactionCount = json_decode($interactionCount, true);
                    // $keywordInteractioncount = $interactionCount["total"];

                    // if ($keywordInteractioncount == '') {
                    //     $keywordInteractioncount = 0;
                    // }

                    // if ($userKwdSearchCount == '') {
                    //     $userKwdSearchCount = 0;
                    // }
                    // $total = 0;
                    // $userKwdSearchCount = json_decode($userKwdSearchCount, true);
                    // foreach ($userKwdSearchCount as $key => $totalEarning) {
                    //     $total = $total + $totalEarning;
                    // }

                ?>
                <div class="card social-card inner-2x innerMB">
                    <div class="keyword-detail-card" id="<?php echo 'k'.$i; ?>">
                        <div class="keyword-data innerAll clearfix">
                            <div class="col-xs-4 half innerL">
                                <div class="row">
                                    <div class="col-xs-12 half innerMB padding-none">
                                          <span class="text-blue">
                                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keyword); ?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword; ?>">
                            <?php echo "#{$keyword}"; ?></a>
                                          </span>
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Interaction :&nbsp</label>
                                            <span class=""  title="<?php echo $keywordInteractioncount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $keywordInteractioncount; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 padding-none">
                                <div class="row">
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Earning :&nbsp</label>
                                            <span class="" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                  origPrice="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$total}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="col-xs-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-xs-12" id="followunfollow">

                                        <?php
                                        if (in_array($email, $keywordFollowerCounts)) {

                                            $buttoVal = 'Unfollow';
                                        } else {
                                            $buttoVal = 'Follow';
                                        }
                                        ?>
                                        <input value="<?php echo $buttoVal; ?>" type="button" id="<?php echo 'fu_'.$keyword; ?>" class="btn-social-wid-auto-dark pull-right innerMR" onclick="followUnfollow('<?php echo cleanXSS($keyword); ?>', '<?php echo $email; ?>')" />
                                        <span> <i class='compress-expand-icon fa fa-angle-down'></i> </span>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
                            <?php

                            $checkForKeywordAvailability = checkForKeywordAvailability($keyword,$conn);  // Used for getting detail from ownership table.

                            if(noError($checkForKeywordAvailability)){

                            $availabilityFlag = $checkForKeywordAvailability['errMsg']; //echo $availabilityFlag;
                            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                            $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
                            $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
                            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                            $CartStatus = $checkForKeywordAvailability['status'];
                            $activeBids = $checkForKeywordAvailability["active_bids"];

                            if($availabilityFlag == 'keyword_available')
                            {
                            ?>
                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Current Bid :&nbsp
                                                        <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                        <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <?php if(in_array($keyword, $userCartDetails)){ ?>
                                                    <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" class="btn-social-wid-auto-dark pull-right half innerMR" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                <?php }else{ ?>
                                                    <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($keyword); ?>" type="button" class="btn-social-wid-auto-dark pull-right half innerMR" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($keyword); ?>','<?php echo cleanXSS($keyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <?php } elseif($availabilityFlag == 'keyword_not_available'){
                                $activeBids = json_decode($activeBids, true);
                                foreach($activeBids as $key => $bidValue){
                                    $bidValue = explode('~~', $bidValue);
                                    $bidderEmail[] = $bidValue[1];

                                }
                                if(in_array($email, $bidderEmail)){
                                    $bidStatus = true;
                                }else{
                                    $bidStatus = false;
                                }
                                ?>

                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Current Bid :&nbsp
                                                        <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                                            <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                  origPrice="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                                        <?php } else{ ?>
                                                            <span class="currency">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                            origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                        <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                                            <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                  origPrice="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                                        <?php } else{ ?>
                                                            <span class="currency">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                            origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="pull-left col-xs-6 col-md-12 padding-right-none">
                                                <?php

                                                if($email == $kwdOwnerId){
                                                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto pull-right" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                                                        </div>
                                                        <?php
                                                    }else{
                                                        // set ask
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto pull-right" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                                        </div>
                                                        <?php
                                                    }

                                                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto-dark pull-right" id="acceptKeyword" value="<?php echo $keyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                                                        </div>
                                                        <?php
                                                    }
                                                }else{

                                                    if($CartStatus == "sold"){

                                                        if(empty($activeBids)){
                                                            ?>
                                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                <button class="btn-social-wid-auto pull-right" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                                                            </div>
                                                            <?php
                                                        }else{
                                                            if($bidStatus){
                                                                ?>
                                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                    <button class="btn-social-wid-auto pull-right" value="<?php echo $keyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                                                                </div>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                    <button class="btn-social-wid-auto pull-right" value="<?php echo $keyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                                                                </div>
                                                                <?php
                                                            }
                                                        }

                                                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                            ?>
                                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                <button class="btn-social-wid-auto-dark pull-right"  value="<?php echo $keyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $keyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                                                            </div>
                                                            <?php
                                                        }

                                                    }
                                                }

                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php  }}elseif($availabilityFlag == "keyword_blocked" || $checkForKeywordAvailability["errCode"] == 4){  ?>

                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $keyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="">Current Bid :&nbsp </label>
                                                    <span class="currency ellipses">&nbsp;<a href="#" class="ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                             origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                        <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                Not Available
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php  } ?>


                        </div>
                    </div>
                </div>
                <?php $i++; }} ?>




                <!-- social-center-panel  -->
            </div>
            <!-- col-xs-6 -->

            <div class="col-xs-3">
                <div class="social-right-panel">
                    <!--search box-->
                    <div class="row innerMB inner-2x">
                        <div class="col-xs-12">
                            <?php include 'include/_social_right_panel_searchbar.php'; ?>
                        </div>
                    </div>
                    <!--trending keywords-->
                    <div class="card social-card right-panel-modules innerMT inner-4x">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-4 padding-right-none">
                                    <h4 class="half innerMTB">Trending</h4>
                                </div>
                                <div class="col-xs-8 padding-left-none">
                                    <div class="trending-content">
                                        <ul class="list-inline half innerMTB text-right">
                                            <li>
                                                <a class="active" href="#" data-id="blog">
                                                    <i class="fa fa-file-text"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="status">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="video">
                                                    <i class="fa fa-play-circle"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="image">
                                                    <i class="fa fa-image"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="audio">
                                                    <i class="fa fa-soundcloud"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="trending-keywords-data">
                            <?php include('trendingKeywords.php'); ?>
                        </div>
                    </div>

                    <div class="popular-modules">
                        <div class="card social-card right-panel-modules inner-2x innerMTB">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-4 padding-right-none">
                                        <h4 class="half innerMTB">Popular</h4>
                                    </div>
                                    <div class="col-xs-8 padding-left-none">
                                        <div class="popular-content">
                                            <ul id = "popular-post-click" class="list-inline half innerMTB text-right">
                                                <li>
                                                    <a id = "popular-post-type-blog" class="popular-active-indicate active" onclick = "getPopularPost('blog');">
                                                        <i class="fa fa-file-text"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id = "popular-post-type-status" class="popular-active-indicate " onclick = "getPopularPost('status');">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id = "popular-post-type-video" class="popular-active-indicate " onclick = "getPopularPost('video');" >
                                                        <i class="fa fa-play-circle"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id = "popular-post-type-image" class="popular-active-indicate " onclick = "getPopularPost('image');">
                                                        <i class="fa fa-image"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a id = "popular-post-type-audio" class="popular-active-indicate " onclick = "getPopularPost('audio');">
                                                        <i class="fa fa-soundcloud"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id = "popular-post-details">
                                <?php include('widgetPopularPost.php'); ?>
                            </div>
                        </div>
                    </div>
                    <!--popular blog module-->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>


    <!-- else part if session expires -->
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
<script src="../../js/marketplace.js"></script>
<script>
    $(".compress-expand-icon").click(function(){
        console.log($(this).parents(".keyword-detail-card").attr("id"));
        if($(this).hasClass("fa-angle-down")){
            $("#"+$(this).parents(".keyword-detail-card").attr("id")+" .keyword-detail-data").stop().slideDown(600);
            $(this).attr("class","compress-expand-icon fa fa-angle-up");
        }else{
            $("#"+$(this).parents(".keyword-detail-card").attr("id")+" .keyword-detail-data").stop().slideUp(600);
            $(this).attr("class","compress-expand-icon fa fa-angle-down");
        }

    });

    var rooturl = '<?php echo $rootUrl; ?>'; //alert(rooturl);


</script>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>