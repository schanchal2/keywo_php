<?php


header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// error_reporting(-1);
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/imageFunctions.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}
//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}



$getBookmarkJsonArray = base64_decode(cleanXSS($_POST['bookmarkArray']));
$bookmarkStart        = cleanXSS(rawurldecode($_POST['bookmarkStart']));
$bookmarkEndingDate   = cleanXSS(rawurldecode($_POST['bookLastDate']));
$sendBookmarkArray    = array();
$bookMarkAppendLimit  = 11;	
$k                    = 0;

if (empty($getBookmarkJsonArray)) {
	$getBookmarkArray = getBookmarkPostIds($_SESSION['account_handle'], 'sort');
} else {
	$getBookmarkArrayResult = (array) json_decode($getBookmarkJsonArray);
	$getBookmarkArray['errCode'] = -1;
	$getBookmarkArray['errMsg'] = $getBookmarkArrayResult;

}

if (!isset($bookmarkStart) || empty($bookmarkStart)) {
	$bookmarkStart = 0;
}

if (noError($getBookmarkArray)) {
	$getBookmarkArray = $getBookmarkArray['errMsg'];	
	for ($i = $bookmarkStart; $i < count($getBookmarkArray) && $i < ($bookmarkStart + $bookMarkAppendLimit); $i++) {
		if (count($getBookmarkArray[$i]) > 0) {
			// unset($getBookmarkArray[$i]['bookmark_at']);
			$sendBookmarkArray[] = $getBookmarkArray[$i];
		}
	}
	$k = count($sendBookmarkArray);
	$sendBookmarkArray = json_encode($sendBookmarkArray);
	// printArr($sendBookmarkArray);
	// printArr($k);
	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
	$bookmarkPostAllData     = getActivityBookMarkLikePosts($sendBookmarkArray, json_encode($blockUserArray));
}

if($bookmarkPostAllData['errMsg'] == array()) {
    $dataOfBookmark = true;
}
if (noError($bookmarkPostAllData)) {
	$bookmarkPostAllData = $bookmarkPostAllData['errMsg'];
?>


<div class="row">
	<div class="col-lg-12">
	<?php
		if (isset($dataOfBookmark)) {
			echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>"See all your starred posts in one place."</p></b></center></li></ul>';
			?>
            <div id="bookmark-post-data-append" pageScrollEndFlag="false" style="display:none;"></div
            <?php

		}
		foreach ($bookmarkPostAllData as $date => $bookmarkData) {
            $dateToDisply  = uDateTime("d-m-Y H:i:s",date("m/d/y"),$date);
            $todayDate      = uDateTime("m/d/y",date("m/d/y"));
			// printArr($bookmarkData);
			// echo "<br>"	.$date;
			// echo "<br>".date("m/d/Y", strtotime("yesterday"));
			if ($todayDate == $date) {
				$displayDate = 'Today';
			} else {
                $yesterdayDate  = uDateTime("m/d/y",date("m/d/Y", strtotime("yesterday")));
				if ($yesterdayDate == $date) {
					$displayDate = 'Yesterday';
				} else {
                    $displayDate =  uDateTime("jS F Y",date("m/d/y"),$dateToDisply);
                }
			}
			if($bookmarkEndingDate != $date) {
	?>
			<div class="h4  innerL ">
				<?php echo $displayDate; ?>
			</div>		
			<?php } ?>	
			<ul class="list-unstyled activity-log__list margin-none">
			<?php 
			
				foreach ($bookmarkData as $key1 => $value) {	
					
					// if ($value['post_type'] == 'share') {
						// printArr($value);	
					switch ($value['post_type']) {
						case 'blog':
							//if (empty($value['post_details']['img_file_name'])) {
								$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
							//} else {
								//$extBlog = explode(".",$value["post_details"]["img_file_name"]);
								//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value["_id"] . '_' . $value["created_at"] . '_' . $value["post_details"]["img_file_name"] .'" alt="...">';
							//}
							$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_details"]["blog_title"]);
			                $postDescCount = strlen($postDesc);
			                if ($postDescCount >= 50) {
			                  $postContent = mb_substr($postDesc,0,50, "utf-8").'...';
			                } else {
			                  $postContent = $postDesc;
			                }
			                $bookPostId = $value['_id'];
			                $bookPostType = 'blog';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;
						case 'video':
							$YoutubeVideoId = getYoutubeIdFromUrl($value['post_details']['asset_url']);
							//$imagePath = '<img class="media-object media-object--activity-log " src="'. $YoutubeVideoId .'" alt="...">';
                            $imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16"></i>';
                            $videoTitle = getYoutubeTitleFromUrl($value['post_details']['asset_url']);
							if ($videoTitle != '') {
								$postDesc = $videoTitle;
							} else if (!empty($value['post_short_desc'])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
							} else {
								$postDesc = 'Title Unavailable';
							}
							$postDescCount = strlen( $postDesc);
							if ($postDescCount >= 50) {
								$postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							} else {
								$postContent = $postDesc;
							}
							$bookPostId = $value['_id'];
			                $bookPostType = 'video';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;
						case 'audio':
							$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
							if (!empty($value["post_short_desc"])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
			            	} else {
			            		$postContent = 'Title Unavailable';
			            	}
			            	$bookPostId = $value['_id'];
			                $bookPostType = 'audio';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;
						case 'image':
							//if (empty($value['post_details']['img_file_name'])) {
                            $imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center innerT f-sz16"></i>';
                            //$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
							//} else {
								//$extImg = explode(".",$value["post_details"]["img_file_name"]);
								//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value["_id"] . '_' . $value["created_at"] . '_' . $value["post_details"]["img_file_name"] .'" alt="...">';
							//}
							if (!empty($value["post_short_desc"])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
			            	} else {
			            		$postContent = 'Title Unavailable';
			            	}
			            	$bookPostId = $value['_id'];
			                $bookPostType = 'image';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;
						case 'status':
							$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
							$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
			                $postDescCount = strlen($postDesc);
			                if ($postDescCount >= 50) {
			                  $postContent = mb_substr($postDesc,0,50, "utf-8").'...';
			                } else {
			                  $postContent = $postDesc;
			                }
			                $bookPostId = $value['_id'];
			                $bookPostType = 'status';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;
						case 'share':
							// $imagePath = '<img class="media-object media-object--activity-log " src="https://placeholdit.imgix.net/~text?w=100&h=50" alt="...">';
							$sharePostType = $value['post_details']['parent_post']['post_id']['post_type'];
							switch ($sharePostType) {
								case 'blog':
									$shareBlogImgPath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
									//if (empty($shareBlogImgPath)) {
										$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
									//} else {
										//$extBlog = explode(".",$shareBlogImgPath);
										//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareBlogImgPath .'" alt="...">';
									//}
									$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
					                $postDescCount = strlen($postDesc);
					                if ($postDescCount >= 50) {
					                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
					                } else {
					                  $postContent = $postDesc;
					                }

									break;
								case 'video':
									$shareVideoPath = $value['post_details']['parent_post']['post_id']['post_details']['asset_url'];
									$YoutubeVideoId = getYoutubeIdFromUrl($shareVideoPath);
									//$imagePath = '<img class="media-object media-object--activity-log " src="'. $YoutubeVideoId .'" alt="...">';
                                    $imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16"></i>';
                                    $videoTitle = getYoutubeTitleFromUrl($shareVideoPath);
									if ($videoTitle != '') {
										$postDesc = $videoTitle;
									} else if (!empty($value['post_short_desc'])) {
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
									} else {
										$postDesc = 'Title Unavailable';
									}
									$postDescCount = strlen( $postDesc);
									if ($postDescCount >= 50) {
										$postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
									} else {
										$postContent = $postDesc;
									}
									break;
								case 'audio':
									$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
									if (!empty($value["post_short_desc"])) {
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
					            	} else {
					            		$postContent = 'Title Unavailable';
					            	}
									break;
								case 'image':
									$shareImagePath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
									//if (empty($shareImagePath)) {
                                    $imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center innerT f-sz16 "></i>';
                                    //$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
									//} else {
										//$extImg = explode(".",$shareImagePath);
										//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareImagePath .'" alt="...">';
									//}
									if (!empty($value["post_short_desc"])) {
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
					            	} else {
					            		$postContent = 'Title Unavailable';
					            	}
									break;
								case 'status':
									$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
									$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
					                $postDescCount = strlen($postDesc);
					                if ($postDescCount >= 50) {
					                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
					                } else {
					                  $postContent = $postDesc;
					                }
									break;								
								default:
									$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
									$postContent = 'Title Unavailable';

									break;
							} // End of Share post Switch case
							$bookPostId = $value['_id'];
			                $bookPostType = 'share';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail = $value['user_ref']['email'];
							break;							
						default:
							$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
							$postContent = 'Title Unavailable';
							break;
					}// End Of switch Case
			?>
					<li class="card innerAll border-bottom ">
						<div class="media">
							<div class="media-left">
								<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']);?>" target="_blank">
									<?php echo $imagePath; ?>
								</a>
							</div>
							<div class="media-body f-sz15">
								<div class="row">
									<div class="col-xs-11">
										<div class="row">
											<div class="col-xs-6">
												<p class="ellipses innerTB half margin-none">
													<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']);?>" target="_blank">
														<?php echo rawurldecode($postContent); ?>	
													</a>
												</p>
											</div>
											<div class="col-xs-6">
												<p class="ellipses innerTB half margin-none">You have Bookmarked 
													<a <?php if($value['posted_by'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($value['user_ref']['email']); ?>&type=<?php echo 'all'; ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> target = "_blank">
														<span class="text-color-Text-Primary-Blue ">
															<?php echo $value['user_ref']['first_name'] . ' ' . $value['user_ref']['last_name']; ?>
														</span> 
													</a>
													post
												</p>
											</div>
										</div>
									</div>
									<div class="col-xs-1">
										<a class="btn btn-link" onclick = "setBookmarkAccountOfUser('<?php echo $bookPostId ; ?>','<?php echo $bookPostType;?>','<?php echo $bookPostcreatedAt; ?>','<?php echo $bookPostEmail; ?>');" role="button"> <i id = "bookmark<?php echo $bookPostId; ?>" class="fa fa-star text-yellow  f-sz18 pull-right" ></i></a>
									</div>
								</div>
							</div>
						</div>
					</li>
			<?php 
				// } //end of post type if condition
				} //End of Inner For Loop
			// if($bookmarkEndingDate != $date) {
			?>
			</ul>

	<?php
			// }
		} //End of outer for loop
	?>
		
	</div>
</div>

<?php
	} else {
		echo "<div><center>Please Try After Some Time</center></div>";
}// End of no error of curl result
	// base64_encode(data)
?>

<div id = 'bookmark-frontendArray-<?php echo $bookmarkStart; ?>' bookmark-started-array = '<?php echo base64_encode(json_encode($getBookmarkArray));?>'  book-start = '<?php echo $i;?>' bookDataEnd = '<?php if ($k < $bookMarkAppendLimit) {echo 'true'; }?>' bookmark-prev-date = '<?php echo $date; ?>' style = 'display:none;'></div>