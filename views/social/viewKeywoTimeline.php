<?php
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
// echo "Testing"; die;
session_start();

header('Content-Type: text/html; charset=utf-8');
require_once '../../config/config.php';
if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $rootUrl . "';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

	$docrootpath = __DIR__;
	$docrootpath = explode('/views', $docrootpath);
	$docrootpath = $docrootpath[0] . "/";

	include("../layout/header.php");
	require_once "{$docrootpath}helpers/stringHelper.php";
	require_once "{$docrootpath}helpers/errorMap.php";
	require_once "{$docrootpath}helpers/coreFunctions.php";
	require_once "{$docrootpath}models/social/socialModel.php";
	require_once("{$docrootpath}models/keywords/keywordCdpModel.php");
	require_once("{$docrootpath}/views/keywords/marketplace/tradePopupDialogBox.php");

	$email           = $_SESSION["email"];
	$clientSessionId = session_id();
	$conn            = createDBConnection('dbkeywords');

	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}

    $searchKeyword = cleanXSS(trim(urldecode($_GET["keyword"])));
    $searchText    = cleanXSS(trim(urldecode($_GET["searchText"])));

    if (empty($searchKeyword) && empty($searchText)) {
        $rootForRedirection =  $rootUrl."";           //views/social/not_found_error.php
        print("<script>");
        print("var t = setTimeout(\"window.location='".$rootForRedirection."';\", 000);");
        print("</script>");
        exit;
    }

	?>
	<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
		  type="text/css"/>
    <main class="social-main-container inner-7x innerT" >
	<div class="container row-10">
		<div class="row">
			<!-- col-xs-3 -->
			<div class="col-xs-3 social-right-position">
				<div class="social-left-panel">
						<?php include("leftInfoUser.php"); ?>
					<div class="followed-keywords-module inner-2x innerMTB">
						<?php include("followedKeyword.php"); ?>
					</div>
					<div id="suggested-user-details">
						<?php include("suggestedUser.php"); ?>
					</div>
				</div>
			</div>
			<!-- END col-xs-3 -->
			<div class="col-xs-6">
                <div class="social-center-panel">
                        <?php
                        if (!empty($searchKeyword)) {
                        $pageUrl = "views/keywords/analytics/keyword_analytics.php";
                        $kwdRevenueDetails = getRevenueDetailsByKeyword($conn, $searchKeyword);    // Used for getting details from revenue table.
                        if (noError($kwdRevenueDetails)) {
                        $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                        $interactionCount = $kwdRevenueDetails["app_kwd_search_count"];
                        $userKwdSearchCount = $kwdRevenueDetails["app_kwd_ownership_earnings"];
                        $CountFollowedKeyword = $kwdRevenueDetails["follow_unfollow"];
                        //$searchKeyword         = $kwdRevenueDetails["keyword"];
                        $keywordFollowerCount = json_decode($CountFollowedKeyword, true);
                        $keywordFollowerCounts = $keywordFollowerCount["email"];
                        $keywordFollowerCount = count($keywordFollowerCounts);
                        // $interactionCount = json_decode($interactionCount, true);
                        // $keywordInteractioncount = $interactionCount["total"];
                        // if ($keywordInteractioncount == '') {
                        //     $keywordInteractioncount = 0;
                        // }
                        // if ($userKwdSearchCount == '') {
                        //     $userKwdSearchCount = 0;
                        // }
                        // $total = 0;
                        // $userKwdSearchCount = json_decode($userKwdSearchCount, true);
                        // foreach ($userKwdSearchCount as $key => $totalEarning) {
                        //     $total = $total + $totalEarning;
                        // }

                        ?>
                <div class="card social-card inner-2x innerMB">
                    <div class="keyword-detail-card" id="k1">

                        <div class="keyword-data innerAll clearfix">
                            <div class="col-xs-4 half innerL">
                                <div class="row">
                                    <div class="col-xs-12 half innerMB padding-none">
                                          <span class="text-blue">
                                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($searchKeyword); ?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $searchKeyword; ?>">
                            <?php echo "#{$searchKeyword}"; ?></a>
                                          </span>
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Interaction :&nbsp</label>
                                            <span class=""  title="<?php echo $keywordInteractioncount; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo $keywordInteractioncount; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-5 padding-none">
                                <div class="row">
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <div class="comn-lft-rght-cont">
                                            <label>Earning :&nbsp</label>
                                            <span class="" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                  origPrice="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$total}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$total}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="row">
                                    <div class="col-xs-12">
                                        &nbsp;
                                    </div>
                                    <div class="col-xs-12" id="followunfollow">

                                        <?php
                                        if (in_array($email, $keywordFollowerCounts)) {

                                            $buttoVal = 'Unfollow';
                                        } else {
                                            $buttoVal = 'Follow';
                                        }
                                        ?>
                                        <input value="<?php echo $buttoVal; ?>" type="button" id="<?php echo 'fu_'.cleanXSS($searchKeyword); ?>" class="btn-social-wid-auto-dark pull-right innerMR" onclick="followUnfollow('<?php echo cleanXSS($searchKeyword); ?>', '<?php echo $email; ?>')" />
                                        <span> <i class='compress-expand-icon fa fa-angle-down'></i> </span>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="keyword-detail-data innerAll clearfix" style="display:none;">
                            <?php
                            $checkForKeywordAvailability = checkForKeywordAvailability($searchKeyword,$conn);  // Used for getting detail from ownership table.
                            if(noError($checkForKeywordAvailability)){
                            $availabilityFlag = $checkForKeywordAvailability['errMsg']; //echo $availabilityFlag;
                            $checkForKeywordAvailability = $checkForKeywordAvailability["keyword_details"];
                            $highestBidAmtForKwd = (float)$checkForKeywordAvailability["highest_bid_amount"];
                            $kwdAskPrice = (float)$checkForKeywordAvailability['ask_price'];
                            $kwdOwnerId = $checkForKeywordAvailability['buyer_id'];
                            $CartStatus = $checkForKeywordAvailability['status'];
                            $activeBids = $checkForKeywordAvailability["active_bids"];

                            if($availabilityFlag == 'keyword_available')
                            { ?>
                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $searchKeyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Current Bid :&nbsp
                                                            <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                            origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                            <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                            origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                <?php if(in_array($searchKeyword, $userCartDetails)){ ?>
                                                    <input value="Remove" type="button" id="km_cartButton_<?php echo cleanXSS($searchKeyword); ?>" class="btn-social-wid-auto-dark pull-right half innerMR" onclick="return addRemoveFromCart('<?php echo $email;  ?>' ,'<?php echo cleanXSS($searchKeyword); ?>','<?php echo cleanXSS($searchKeyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                <?php }else{ ?>
                                                    <input value="Add To Cart" id="km_cartButton_<?php echo cleanXSS($searchKeyword); ?>" type="button" class="btn-social-wid-auto-dark pull-right half innerMR" onclick="return addRemoveFromCart('<?php echo $email;  ?>', '<?php echo cleanXSS($searchKeyword); ?>','<?php echo cleanXSS($searchKeyword); ?>', '<?php echo $clientSessionId; ?>', '<?php echo $rootUrl; ?>', 'km')" />
                                                <?php } ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php } elseif($availabilityFlag == 'keyword_not_available'){
                                $activeBids = json_decode($activeBids, true);
                                foreach($activeBids as $key => $bidValue)
                                {
                                    $bidValue = explode('~~', $bidValue);
                                    $bidderEmail[] = $bidValue[1];
                                }
                                if(in_array($email, $bidderEmail))
                                {
                                    $bidStatus = true;
                                }
                                else
                                {
                                    $bidStatus = false;
                                }
                                ?>

                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $searchKeyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Current Bid :&nbsp
                                                    <?php if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){ ?>
                                                        <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                              origPrice="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$highestBidAmtForKwd}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$highestBidAmtForKwd}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                                    <?php } else{ ?>
                                                        <span class="currency">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                    origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    <?php } ?>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                    <?php if(isset($kwdAskPrice) && !empty($kwdAskPrice)){ ?>
                                                        <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                              origPrice="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdAskPrice}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdAskPrice}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                                    <?php } else{ ?>
                                                        <span class="currency">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                    origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    <?php } ?>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="pull-left col-xs-6 col-md-12 padding-right-none">
                                                <?php

                                                if($email == $kwdOwnerId){
                                                    if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto pull-right" value="<?php echo $searchKeyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Ask</button>
                                                        </div>
                                                        <?php
                                                    }else{
                                                        // set ask
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto pull-right" value="<?php echo $searchKeyword; ?>" type="button" onclick="openTradeDialog(this);">Set Ask</button>
                                                        </div>
                                                        <?php
                                                    }

                                                    if(isset($highestBidAmtForKwd) && !empty($highestBidAmtForKwd)){
                                                        ?>
                                                        <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                            <button class="btn-social-wid-auto-dark pull-right" id="acceptKeyword" value="<?php echo $searchKeyword; ?>" type="button" data-toggle="modal" data-target="#keyword-popup-confirm-accept">Accept Bid</button>
                                                        </div>
                                                        <?php
                                                    }
                                                }else{

                                                    if($CartStatus == "sold"){

                                                        if(empty($activeBids)){
                                                            ?>
                                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                <button class="btn-social-wid-auto pull-right" value="<?php echo $searchKeyword; ?>" type="button" onclick="openTradeDialog(this);">Place Bid</button>
                                                            </div>
                                                            <?php
                                                        }else{
                                                            if($bidStatus){
                                                                ?>
                                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                    <button class="btn-social-wid-auto pull-right" value="<?php echo $searchKeyword; ?>" type="button" onclick="openTradeDialog(this);">Edit Bid</button>
                                                                </div>
                                                                <?php
                                                            }else{
                                                                ?>
                                                                <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                    <button class="btn-social-wid-auto pull-right" value="<?php echo $searchKeyword; ?>" type="button " onclick="openTradeDialog(this); " >Place Bid</button>
                                                                </div>
                                                                <?php
                                                            }
                                                        }

                                                        if(isset($kwdAskPrice) && !empty($kwdAskPrice)){
                                                            ?>
                                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                <button class="btn-social-wid-auto-dark pull-right"  value="<?php echo $searchKeyword; ?>" id="buyNowKeyword" type="button" data-toggle="modal" onclick="buyNowClick('<?php echo $searchKeyword;  ?>','<?php  echo $kwdAskPrice; ?>');" >Buy Now</button>
                                                            </div>
                                                            <?php
                                                        }

                                                    }
                                                }

                                                ?>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php }}elseif($availabilityFlag == "keyword_blocked" || $checkForKeywordAvailability["errCode"] == 4){  ?>

                                <div class="col-xs-12 padding-none innerMB">
                                    <div class="col-xs-4 half innerL">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label>Followed by :&nbsp</label>
                                                    <span><?php echo "{$keywordFollowerCount} User"?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <a class="view-analytics" href="<?php echo $rootUrl.$pageUrl; ?>?q=<?php echo $searchKeyword; ?>" target="_blank">View Analytics</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-5 padding-none">
                                        <div class="row">
                                            <div class="col-xs-12 padding-none">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="">Current Bid :&nbsp </label>
                                                        <span class="currency ellipses">&nbsp;<a href="#" class="ellipses" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                                                </div>
                                            </div>
                                            <div class="col-xs-12 padding-none innerMT">
                                                <div class="comn-lft-rght-cont">
                                                    <label class="ellipses">Asking Price :&nbsp
                                                        <span class="currency ellipses">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <div class="row">

                                            <div class="col-xs-12">
                                                Not Available
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
                        <?php }} ?>

                <div class="social-all-status-tabs">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="list-inline pull-left margin-none">
                                <li>
                                    <a id="all" href="#all" class="social-status-tabs active">ALL</a>
                                </li>
                                <li>
                                    <a id="people" href="#people" class="social-status-tabs people-search-result">PEOPLE</a>
                                </li>
                                <li>
                                    <a id="status" href="#status" class="social-status-tabs">STATUS</a>
                                </li>
                                <li>
                                    <a id="blog" href="#blog" class="social-status-tabs">BLOG</a>
                                </li>
                                <li>
                                    <a id="video" href="#video" class="social-status-tabs">VIDEO</a>
                                </li>
                                <li>
                                    <a id="image" href="#image" class="social-status-tabs">IMAGE</a>
                                </li>
                                <li>
                                    <a id="audio" href="#audio" class="social-status-tabs">AUDIO</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                
				<div id="post-count" class="hide innerTB bg-white" data-post-count="" data-post-count-msg="" style="text-align:center;cursor:pointer">
					<a></a>
				</div>
				<div id = "ajaxLoader1" class="text-center" style="display:none; ">
					<img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
				</div>
				<div id="show-error-message"></div>
				<div id="private-next-post-data" style="display:none;" data-count="0" data-type="all" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
				<div id="home-next-post-data" style="display:none;" data-count="0" data-post-type="all" data-scroll-allow="true" data-status-empty=""></div>
				<div id="home-post-data" class="">
					<!--display loading content div before data is loaded-->
					<div id="loading-content-div"><?php include('loadingContent.php'); ?></div>
					<div id = "ajaxLoader" class="text-center" style="display:none; ">
						<img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
					</div>
				</div>
			</div>
            </div>
            
			<!-- END col-xs-6 -->

			<div class="col-xs-3 social-right-position">
				<div class="social-right-panel">
					<!--search box-->
				  <div class="row innerMB inner-2x">
            <div class="col-xs-12">
              <?php include 'include/_social_right_panel_searchbar.php'; ?>
            </div>
          </div>
					<!--trending keywords-->
					<div class="card social-card right-panel-modules innerMT inner-2x">
						<div class="bg-light-gray right-panel-modules-head">
							<div class="row margin-none">
								<div class="col-xs-4 padding-right-none">
									<h4 class="half innerMTB">Trending</h4>
								</div>
								<div class="col-xs-8 padding-left-none">
									<div class="trending-content">
										<ul class="list-inline half innerMTB text-right">
											<li>
												<a class="active" href="#" data-id="blog">
													<i class="fa fa-file-text"></i>
												</a>
											</li>
											<li>
												<a class="" href="#" data-id="status">
													<i class="fa fa-edit"></i>
												</a>
											</li>
											<li>
												<a class="" href="#" data-id="video">
													<i class="fa fa-play-circle"></i>
												</a>
											</li>
											<li>
												<a class="" href="#" data-id="image">
													<i class="fa fa-image"></i>
												</a>
											</li>
											<li>
												<a class="" href="#" data-id="audio">
													<i class="fa fa-soundcloud"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div id="trending-keywords-data">
							<?php include('trendingKeywords.php'); ?>
						</div>
					</div>
					<!--popular Posts-->
					<div class="popular-modules">
						<div class="card social-card right-panel-modules inner-2x innerMTB">
							<div class="bg-light-gray right-panel-modules-head">
								<div class="row margin-none">
									<div class="col-xs-4 padding-right-none">
										<h4 class="half innerMTB">Popular</h4>
									</div>
									<div class="col-xs-8 padding-left-none">
										<div class="popular-content">
											<ul id = "popular-post-click" class="list-inline half innerMTB text-right">
												<li>
													<a id = "popular-post-type-blog" class="popular-active-indicate active" onclick = "getPopularPost('blog');">
														<i class="fa fa-file-text"></i>
													</a>
												</li>
												<li>
													<a id = "popular-post-type-status" class="popular-active-indicate " onclick = "getPopularPost('status');">
														<i class="fa fa-edit"></i>
													</a>
												</li>
												<li>
													<a id = "popular-post-type-video" class="popular-active-indicate " onclick = "getPopularPost('video');" >
														<i class="fa fa-play-circle"></i>
													</a>
												</li>
												<li>
													<a id = "popular-post-type-image" class="popular-active-indicate " onclick = "getPopularPost('image');">
														<i class="fa fa-image"></i>
													</a>
												</li>
												<li>
													<a id = "popular-post-type-audio" class="popular-active-indicate " onclick = "getPopularPost('audio');">
														<i class="fa fa-soundcloud"></i>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id = "popular-post-details">
								<?php include('widgetPopularPost.php'); ?>
							</div>
						</div>
					</div>
					<!--END popular Posts-->
				</div>
			</div>
			<!--  -->
			</div>
			<!-- END row -->
		</div>
		<!-- END container -->
	</main>
	<!-- else part if session expires -->
	<?php
} else {
	header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<!--modal to display editing data-->
<div class="modal fade row-10" id="showModalPost" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body-social-user-image">
			</div>
		</div>
	</div>
</div>
<!--modal to report User-->
<div class="modal fade row-10" id="showReportDetails" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class = "modal-body-report-user">

			</div>
		</div>
	</div>
</div>
<!--modal to show Post is Deleted User-->
<div class="modal fade row-10" id="postDeletedModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class = "modal-body-post-deleted">

			</div>
		</div>
	</div>
</div>
<!--modal to display editing data-->
<div class="modal fade row-10" id="editPostDetails" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body">

			</div>
		</div>
	</div>
</div>
<!-- Likes List Modal -->
<div id="myModal_like" class="modal fade row-10" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<!-- Modal content-->
			<div class="" id="likeUserList">

			</div>
		</div>
	</div>
</div>
<!--modal to show delete confirmation of comments-->
<div class="modal fade row-10" id="deleteCommentModal" role="dialog">
	<div class="modal-dialog" style="margin-top: 150px;">
		<div class="card social-card clearfix social-form-modal">
			<div class="modal-header">
				<button type="button" class=" close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title text-blue">Delete</h4>
			</div>
			<div class = "modal-body-social-user-image">

			</div>
		</div>
	</div>
</div>
<!--social-timeline-modal-on-block-post-->
<div class="modal fade row-10" id="blockUserAcc" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<!-- Modal content-->
			<div class="">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-blue">Block</h4>
				</div>
				<div class="modal-body">
					Are you sure you want to Block Account?
				</div>
				<div class="modal-footer border-none">
					<button type="button" class="btn btn-social-wid-auto yes-remove-block-btn" data-dismiss="modal">Yes</button>
					<button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>
</div>
<!--social-timeline-modal-on-remove-post-->
<div class="modal fade" id="removePostModal" role="dialog">
    <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal">
            <!-- Modal content-->
            <div class="">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title text-blue">Delete</h4>
                </div>
                <div class="modal-body">
                    Are you sure you want to delete post?
                </div>
                <div class="modal-footer border-none">
                    <button type="button" class="btn btn-social-wid-auto yes-remove-post-btn" data-dismiss="modal">Yes</button>
                    <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('../layout/social_footer.php'); ?>
<script src="../../js/marketplace.js"></script>
<!--script for getting 10-10 post on every scroll down-->
<script>
	var lastPostTime = 0;
	$(document).ready(function(){

		/* Assign empty url value to the iframe src attribute when
		 modal hide, which stop the video playing */
		$("#showModalPost").on('hide.bs.modal', function(){
			$("#cartoonVideo").attr('src', '');
		});


		var bottom = $(document).height() - $(window).height(); //for endless scrolling
		var successflag = 0;
		$(document).scroll(function(){
			/*
			 For endless scrolling
			 */
			var win = $(window);
			// Each time the user scrolls
			win.scroll(function() {
				// End of the document reached?
				if ($(document).height() - win.height() == win.scrollTop()) {
					// Do the stuff
					if(successflag == 0){
						var lastPostTime = $("#home-post-data .social-timeline-image .social-card:last-child .social-card-created-at").attr("id");
						var postType     = $(".social-all-status-tabs .active").attr("id");
						//function to call get post from session
						loadNextPost(lastPostTime, postType);
						successflag = 1;
					}
				}else{
					// console.log("else");
					successflag = 0;
				}
			});
		});
	});

	//function to get next data & append
	function loadNextPost(lastPostTime, type) {
		var n 							= $("#home-next-post-data").attr("data-count");
		var viewPostType    = $('#home-next-post-data').attr('data-post-type');
		var checkScrollDown = $('#home-next-post-data').attr('data-scroll-allow');
		var scrollAllowed   = $('#home-next-post-data').attr('data-status-empty');
		if (checkScrollDown == 'true' & scrollAllowed == 'true') {

			$.ajax({
				type: 'GET',
				dataType: 'html',
				data: {type:type, lastPostTime:lastPostTime, n:n},
				url: '../../views/social/homeTimelineContent.php',
				success: function(posthomedata) {
					$('#home-post-data').append(posthomedata);
					$("#home-next-post-data").attr("data-count",parseInt(n)+10);
					var checkScrollStatus = $('#checkScrollStatus-' + n).attr('data-scroll-allow');
					$('#home-next-post-data').attr('data-status-empty', checkScrollStatus);
				},
				beforeSend: function(posthomedata) {
					$('#home-next-post-data').attr('data-scroll-allow', 'false');
					$('#ajaxLoader').show();
				},
				complete: function(posthomedata) {
					$('#home-next-post-data').attr('data-scroll-allow', 'true');
					$('#ajaxLoader').hide();
				},
				error: function(posthomedata) {
					$('#loading-content-div').hide();
					// $('#show-error-message').text('No Post Yet!!');
				}
			});
		}
	}

	//--------------------------      Confirmation to block User-----------------------------//
	function blockUserAccByConfirmation(ele,email,postId, postType, postCreatedAt) {
        $(ele).css('pointer-events','none');
		$('#showModalPost .close-dialog').click();
		$("#blockUserAcc").modal("show");
		emailId         = email;
		elem            = ele;
		blockPostId        = postId;
		blockPostType      = postType;
		blockPostCreatedAt = postCreatedAt;
		emailId            = email;
	}
	$("#blockUserAcc .yes-remove-block-btn").click(function(){
		$("this").prop("disabled", "disabled");
		removePersonalPostForBlockedUser(elem,emailId,blockPostId, blockPostType, blockPostCreatedAt);
		$('#blockUserAcc').modal('hide');
	});

	$(".compress-expand-icon").click(function(){
		 console.log($(this).parents(".keyword-detail-card").attr("id"));
		if($(this).hasClass("fa-angle-down")){
			$("#"+$(this).parents(".keyword-detail-card").attr("id")+" .keyword-detail-data").stop().slideDown(600);
			$(this).attr("class","compress-expand-icon fa fa-angle-up");
		}else{
			$("#"+$(this).parents(".keyword-detail-card").attr("id")+" .keyword-detail-data").stop().slideUp(600);
			$(this).attr("class","compress-expand-icon fa fa-angle-down");
		}

	});

</script>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>