<?php

// error_reporting(1);
// error_reporting(E_ERROR | E_PARSE);
// ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);


ini_set('default_charset','utf-8');
// header('Content-type: text/html; charset=utf-8');

session_start();

/* Add Global variables */
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once "{$docrootpath}config/config.php";
require_once "{$docrootpath}config/db_config.php";
require_once("{$docrootpath}helpers/deviceHelper.php");
require_once "{$docrootpath}helpers/arrayHelper.php";
require_once "{$docrootpath}helpers/stringHelper.php";
require_once "{$docrootpath}helpers/errorMap.php";
require_once "{$docrootpath}helpers/coreFunctions.php";


//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../../layout/header.php");
    $email      = $_SESSION["email"];

    $user_id    = $_SESSION['id'];
    $accountHandle = $_SESSION["account_handle"];

    $require             = $userRequiredFields . ",user_id,gender,country,city,mobile_number,address1,address2,profile_pic,hobbies,state,date_of_birth,zip,countryCode";

   $requestUrl          = $NotificationURL.'v2/';
    $getUserInfo = getUserInfo($email, $requestUrl, $require);

    if(noError($getUserInfo)){
        $getUserInfo    =   $getUserInfo["errMsg"];
        $firstName      =   $getUserInfo["first_name"];
        $lastName       =   $getUserInfo["last_name"];
        $country        =   $getUserInfo["country"];
        $state          =   $getUserInfo["state"];
        $city           =   $getUserInfo["city"];
        $zip            =   $getUserInfo["zip"];
        $gender         =   $getUserInfo["gender"];
        $profilePic     =   $getUserInfo["profile_pic"];
        $address1       =   $getUserInfo["address1"];
        $address2       =   $getUserInfo["address2"];
        $mobile_number  =   $getUserInfo["mobile_number"];
        $dateOfBirth    =   $getUserInfo["date_of_birth"];
        $countryCode    =   $getUserInfo["countryCode"];
        $hobbies        =   $getUserInfo["hobbies"];

        $hobbies = array_values(array_filter($hobbies));

        // get education work info
        $getEduDetails = getEducationWorkInfo($user_id);

        if(noError($getEduDetails)){
            $getEduDetails = $getEduDetails["errMsg"];

            $getSchoolInfo = $getEduDetails["school_info"];
            $getUniversityInfo = $getEduDetails["university_details"];
            $universityRadioParam = $getEduDetails["university"];
            $curricularActivities = $getEduDetails["activity"];
            $companyInfo = $getEduDetails["company_info"];
            $workSkills = $getEduDetails["skills"];

            $curricularActivities = array_values(array_filter($curricularActivities));
            $workSkills = array_values(array_filter($workSkills));

            $totalSchoolInfoCount = count($getSchoolInfo);
            $totalUniversityInfoCount = count($getUniversityInfo);
            $totalcompanyInfoCount = count($companyInfo);

        }else if($getEduDetails["errCode"] != 12){
            print('Error : Fetching user education detaisl');
            // exit;
        }
    }else{
        print('Error: Fetching get user info');
        //exit;
    }


    ?>
    <main class="social-main-container inner-7x innerT" id="mainprofile">
        <div class="container" >
            <div class="row">
                <div class="col-xs-3">
                    <!-- social left panel -->
                    <?php include 'myProfile_left_panel.php'; ?>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <div class="col-xs-9 social-center-panel">
                    <div class="profile-details-div">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-heading">
                                    <h4 class="text-blue margin-none">Settings and Preferences</h4> :
                                    <h5 class=" margin-none">Profile</h5>
                                </div>
                                <!-- profile heading -->
                            </div>
                        </div>
                        <!-- heading row -->
                        <?php include 'myProfile_PersonalInformation.php'; ?>
                        <!-- Personal detail row -->
                        <?php include 'myProfile_EducationalInformation.php'; ?>
                        <!-- Education detail -->
                        <?php include 'myProfile_WorkInformation.php'; ?>
                        <!-- Work information -->
                    </div>
                    <!--activity log-->
                    <div id = "activity-title-div" class="col-xs-12 innerMB padding-none activity-log-details-div" style="display:none;">
                        <div class="pull-left">
                            <h4 class="text-blue half innerMT">Activity Log :</h4>
                        </div>
                        <div class="pull-left half innerMT innerML">
                            <div id="activityType" class="heading">Followed People</div>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                    <div class=" followed-people-container padding-none">
                        <div id="followingDataAppend" followingDataAppendCount="" followingDataLoopCount="" pageScrollEndFlag="" ajxCallFlag="true" followingDataExistCount="0" style="display:none;">1</div>
                        <div id="bookmark-post-data-append" bookmark-array="" bookmark-start="" ajaxCallFlag="true" pageScrollEndFlag="" bookmark-last-date="" style="display:none;"></div>
                        <div id="like-post-data-append" like-array="" like-start="" ajaxCallFlag="true" pageScrollEndFlag="" like-last-date="" style="display:none;"></div>
                        <div id="comment-post-data-append" comment-start="" ajaxCallFlag="true" pageScrollEndFlag="" comment-last-date="" style="display:none;"></div>
                        <div id="hidden-post-data-append" l-value="0" hidden-start="" ajaxCallFlag="true" pageScrollEndFlag="" hidden-last-date="" style="display:none;"></div>
                        <div id="keyword-follow-user-data-append" keyword-follow-json="" keyword-follow-start="" ajaxCallFlag="true" pageScrollEndFlag="" keyword-follow-last-keyword="" followingDataExistCount="0" style="display:none;"></div>
                        <div id="followings-data" class="follower-container">
                            <!--content from ajax for Followings-->
                        </div>
                        <div id="activity-log-details" class="follower-container" activity-type-to-load="">
                            <!--data will Append Here-->
                        </div>

                        <div id="change-password-details" class="password-container" activity-type-to-load="">
                            <!--data will Append Here-->
                        </div>

                        <div id="ajaxLoader" class="text-center" style="display:none; ">
                            <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style="width: 60px;">
                        </div>
                    </div>
                    <!--end activity log-->
                    <!-- social-center-panel  -->
                </div>
                <!-- col-xs-9 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <div id="image-upload-confirmation-dialog" class="modal fade in keyword-popup keyword-markt-popup-common" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content row">
                <div class="modal-header custom-modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Warning</h4>
                </div>
                <div class="modal-body">
                    <div>
                        <div class="">
                            <div class="row">
                                <div class="col-md-12 innerMB text-center">
                                    <span class="text-black" id="invalidImageResponse">Please upload valid image</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 innerMB">
                                    <div class="text-left">
                                        <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                    </div>
                                </div>
                                <div class="col-md-6 innerMB">
                                    <div class="text-left pull-right">
                                        <!-- <input value="Yes" type="button" id="buy-yes-btn" class="btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;-->
                                        <input value="OK" type="button" id="buy-no-btn" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- else part if session expires -->
    <?php
} else {
    header('location:'. $rootUrl .'views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../../layout/social_footer.php'); ?>

        <!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <button type="button" class=" close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>

<!--modal to show Post is Deleted User-->
<div class="modal fade" id="postDeletedModal" role="dialog">
    <div class="modal-dialog">
        <div class="card social-card clearfix social-form-modal">
            <div class = "modal-body-post-deleted">

            </div>
        </div>
    </div>
</div>

        <script>
        var rootUrl = '<?php echo $rootUrl; ?>';

        var arr = [];
        var cocurricularAct = [];
        var skills = [];

        var counter = '<?php echo $totalSchoolInfoCount; ?>';
        if (counter == '') {
            counter = 0;
        }
        // var universityCounter = 0;

        var universityCounter = '<?php echo $totalUniversityInfoCount; ?>';

        if (universityCounter == '') {
            universityCounter = 0;
        }
        // var companyCounter = 0;

        var companyCounter = '<?php echo $totalcompanyInfoCount; ?>';
        if (companyCounter == '') {
            companyCounter = 0;
        }

        $(".chosen-select-deselect").chosen({
            enable_split_word_search: false,
            width: "100%"
        });
        $(".chosen-search").keyup(function() {
            if ($(".chosen-results").children().hasClass("no-results")) {
                $(".no-results").text("No results match");
            }
        });

        $(document).ready(function() {
            var x = 0,
                y = 0,
                z = 0; //initlal text box count

            counter = parseInt(counter);
            if (counter == 0) {
                x = 0;
            } else {
                x = counter;
            }

            universityCounter = parseInt(universityCounter);
            if (universityCounter == 0) {
                y = 0;
            } else {
                y = universityCounter;
            }

            companyCounter = parseInt(companyCounter);
            if (companyCounter == 0) {
                z = 0;
            } else {
                z = companyCounter;
            }
            getUserActivityCount();
            $('#userProfileSettingDiv').on('click', function() {
                // $(window).scrollTop(0);
                // $('html,body').animate({scrollTop: $(window).offset().top},'slow');
                $("html, body").animate({
                    scrollTop: 0
                }, 300);
                followPeopleInitialize();
                bookmarkPostInitialize();
                commentInitialize();
                hiddenPostInitialize();
                followKeywordInitialize();
                likeInitialize();
                $(".profile-details-div").show();
                $(".activity-log-details-div").hide();
                $('#activity-log-details').hide();
                $('#activity-log-details').attr('activity-type-to-load', '');
            });

            /*
            For endless scrolling
            */
            var bottom = $(document).height() - $(window).height();
            var successflag = 0;
            $(document).scroll(function() {
                var win = $(window);
                // Each time the user scrolls
                win.scroll(function() {
                    // End of the document reached?
                    if ($(document).height() - win.height() == win.scrollTop()) {
                        // Do the stuff
                        if (successflag == 0) {
                            //do stuff here
                            successflag = 1;
                            var ActivityLoadType = $('#activity-log-details').attr('activity-type-to-load');
                            if (ActivityLoadType == 'postLike') {
                                getActivityLike();
                            } else if (ActivityLoadType == 'postComment') {
                                getActivityComment();
                            } else if (ActivityLoadType == 'postBookmark') {
                                getActivityBookmarkPost();
                            } else if (ActivityLoadType == 'followPeople') {
                                loadFollowedPeople();
                            } else if (ActivityLoadType == 'followKeyword') {
                                getActivityfollowKeywordPost();
                            } else if (ActivityLoadType == 'hiddenPost') {
                                getActivityHiddenPost();
                            }
                            // if (followloadType == 'followings') {
                            //  loadFollowings(followloadType);
                            // } else if (followloadType == 'followers') {
                            //  loadFollowers(followloadType);
                            // }
                        }
                    } else {
                        successflag = 0;
                    }
                });
            });

            $(".add-to-btn").click(function(e) { //on add input button click
                var schoolVal = $("#highSchool_0").find(".school-input").val();
                var startYearVal = $("#highSchool_0").find(".year-start").val();
                var endYearVal = $("#highSchool_0").find(".year-end").val();
                if ($("#highSchool_0").find(".school-input").val() != "") {
                    selecteddata = '';
                    $("#highSchool_0").find("input").val("");
                    if (x < 10) {
                        x++;
                        selecteddata += '<div id="highSchool_' + x + '" class="col-xs-12 innerB innerMT">';
                        selecteddata += '<input type="text" id="schoolName_' + x + '" class="school-input form-control" placeholder="Which school did you attend?" value="' + schoolVal + '"/>';
                        selecteddata += '<input type="text" min="0" maxlength="4" id="from_year_' + x + '" class="years year-start form-control number-only" value="' + startYearVal + '" />';
                        selecteddata += '<div class="years-to">To</div>';
                        selecteddata += '<input type="text" min="0" maxlength="4" id="to_year_' + x + '" class="years year-end  form-control number-only" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( Time period in years )</span>';
                        selecteddata += '<span data-id="highSchool_' + x + '"class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>';
                        selecteddata += '</div>';

                        $(".high-school-module").append(selecteddata);

                        var fromYr = $('#from_year_'+x).val();
                        var toYr = $('#to_year_'+x).val();

                        fromYr = parseInt(fromYr);
                        toYr = parseInt(toYr);

                        if(toYr > fromYr){
                            $("#highSchool_0").find(".school-input").focus();
                        }else{
                            showToast("failed", 'To Year should be greater then From Year');
                            $('#highSchool_'+x).remove();
                            x = x - 1;
                        }
                    }

                    counter = x;
                }
            });

            $(".university-add-to-btn").click(function(e) { //on add input button click
                var universityVal = $("#university_0").find(".university-input-name ").val();
                var universitySpecVal = $("#university_0").find(".university-input-specification ").val();
                var startYearVal = $("#university_0").find(".year-start").val();
                var endYearVal = $("#university_0").find(".year-end").val();
                if ($("#university_" + y).find("input").val() != "") {
                    selecteddata = '';
                    $("#university_0").find("input").val("");
                    if (y < 10) {
                        y++;
                        selecteddata += '<div id="university_'+ y + '" class="padding-left-none innerT" style="position:relative;">';
                        selecteddata += '<input type="text" id="university_name_' + y + '" class="university-input-name form-control" placeholder="Which university did you attend?" value="' + universityVal + '" />';
                        selecteddata += '<input type="text" id="university_specification_' + y + '" class="university-input-specification form-control" placeholder = "Specification" value="' + universitySpecVal + '"/>';
                        selecteddata += '<input type="text" min="0" id="university_from_date_' + y + '" class="years year-start form-control number-only" value="' + startYearVal + '" />';
                        selecteddata += '<div class="years-to"> To </div>';
                        selecteddata += '<input type="text" min="0" maxlength="4" id="university_to_date_' + y + '" class="years year-end form-control number-only" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( Time period in years )</span>';
                        selecteddata += '<span data-id="university_' + y + '" class="text-color-Gray remove-input pull-right innerMR remove_field"><i class="fa fa-trash"></i></span>'
                        selecteddata += '</div>';

                        $(".university-module").append(selecteddata);

                        var fromYr = $('#university_from_date_'+y).val();
                        var toYr = $('#university_to_date_'+y).val();

                        fromYr = parseInt(fromYr);
                        toYr = parseInt(toYr);

                        if(toYr > fromYr){
                            $("#university_0").find(".university-input-name").focus();
                        }else{
                            showToast("failed", 'To Year should be greater then From Year');
                            $('#university_'+y).remove();
                            y = y - 1;
                        }
                    }

                    universityCounter = y;
                }
            });

            $(".company-add-to-btn").click(function(e) { //on add input button click
                var workVal = $("#workDetail_0").find(".company-input-name ").val();
                var designationVal = $("#workDetail_0").find(".company-input-designation ").val();
                var startYearVal = $("#workDetail_0").find(".year-start").val();
                var endYearVal = $("#workDetail_0").find(".year-end").val();
                if ($("#workDetail_" + z).find("input").val() != "") {
                    selecteddata = '';
                    $("#workDetail_0").find("input").val("");
                    if (z < 10) {
                        z++;
                        selecteddata += '<div id="workDetail_' + z + '" class="col-xs-12">';
                        selecteddata += '<div class="padding-left-none  innerT">';
                        selecteddata += '<input type="text" id="company_name_' + z + '"  class="company-input-name form-control" placeholder="Company name" value="' + workVal + '"/>';
                        selecteddata += '<input type="text" id="designation_' + z + '"  class="company-input-designation form-control" placeholder="Add designation" value="' + designationVal + '"/>';
                        selecteddata += '<input type="text" min="0" maxlength="4" id="company_from_date_' + z + '" class="years year-start form-control number-only" value="' + startYearVal + '"/>';
                        selecteddata += '<div class="years-to"> To </div>';
                        selecteddata += '<input type="text" min="0" maxlength="4" id="company_to_date_' + z + '"  class="years year-end form-control number-only" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( In Years )</span>';
                        selecteddata += '<span data-id="workDetail_' + z + '" class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>';
                        selecteddata += '</div>';
                        selecteddata += '</div>';

                        $(".work-section").append(selecteddata);

                        var fromYr = $('#company_from_date_'+z).val();
                        var toYr = $('#company_to_date_'+z).val();

                        fromYr = parseInt(fromYr);
                        toYr = parseInt(toYr);

                        if(toYr > fromYr){
                            $("#workDetail_0").find(".company-input-name").focus();
                        }else {
                            showToast("failed", 'To Year should be greater then From Year');
                            $('#workDetail_' + z).remove();
                            z = z - 1;
                        }
                    }

                    companyCounter = z;
                }
            });
            $(".high-school-module").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                x--;

                counter = x + 1;

            });
            $(".university-module").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                console.log($("#" + $(this).attr("data-id")));
                y--;

                universityCounter = y + 1;
            });
            $(".work-section").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                z--;

                companyCounter = z + 1;
            });

        });
        $(function() { // DOM ready

            // ::: TAGS BOX

            $("#tags input").on({

                keyup: function(ev) {

                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        arr.push(txt);
                        $('#hobbiesMain').val(arr);
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });

            $('#tags').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });

            $("#tagsCurricular input").on({
                keyup: function(ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        cocurricularAct.push(txt);
                        $('#curricularActivities').val(cocurricularAct);
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });
            $('#tagsCurricular').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });
            $("#tagsProfessional input").on({
                keyup: function(ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        skills.push(txt);
                        $('#professionalSkills').val(skills);
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });
            $('#tagsProfessional').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });
        });


        function getState() {
            var val = document.getElementById('country').value;
            val = val.split('~~');

            var cid = val[0]; // retrieve country id
            var countryCode = val[1]; // retrieve country code number

            $('#countryCodeNumber').text('+'+countryCode);

            $.ajax({
                type: "POST",
                dataType: "html",
                url: "../../../controllers/widget/getStates.php",
                data: 'countryID=' + cid,
                success: function(data) {
                    $("#state").html(data);
                }
            });
        }

        function getCity() {
            var val = document.getElementById('state').value;
            $.ajax({
                type: "POST",
                url: "../../../controllers/widget/getCities.php",
                data: 'stateID=' + val,
                success: function(data) {
                    $("#city").html(data);
                }
            });
        }

        function uploadProfile() {

            var first_name =  $('#first_name').val();
            first_name =  first_name.trim();
            var last_name =  $('#last_name').val();
            last_name = last_name.trim();
            var address1 = $('#address1').val();
            address1 = address1.trim();
            var address2 = $('#address2').val();
            address2 = address2.trim();

            var country = $('#country').find(":selected").text();
            country = country.trim();

            var state = $('#state').find(":selected").text();
            state = state.trim();

            if(state == 'Select State'){
                state = '';
            }

            var city = $('#city').find(":selected").text();
            city = city.trim();


            if(city == 'Select City'){
                city = '';
            }

            var mobile_number = $('#mobile_number').val();

            var zip = $('#zip').val();

            var birthday = $('#dob').val();
            var calcAge = birthday;
            var dobYear = '';

            if(first_name != ''){
                if(last_name != ''){
                    //if(mobile_number != ''){
                        //if(mobile_number.length == 10){
                           // if(zip != ''){
                               // if(zip.length == 6){
                                    if(country != ''){
                                        if(state != ''){

                                            if(calcAge != ''){
                                                calcAge = calcAge.split('/');
                                            }

                                            // get date of birth year
                                            dobYear = parseInt(calcAge[2]);

                                            // date of birth calculation
                                            var now = new Date();
                                            var nowYear = now.getFullYear();
                                            var age = nowYear - dobYear;

                                            if(age > 13){

                                                var myHobbies = $('#hobbiesMain').val();
                                                var existHobbies = $('#hobbiesAlreadyExist').val();

                                                if (myHobbies != '') {
                                                    myHobbies = existHobbies + "," + myHobbies;
                                                } else {
                                                    myHobbies = existHobbies;
                                                }

                                                var file_data = $("#fileToUpload").prop("files")[0];
                                                //console.log(file_data); return;
                                                if (typeof(file_data) != "undefined") {
                                                    var img_name = file_data["name"];
                                                    var img_type = file_data["type"];
                                                    var img_size = file_data["size"];
                                                }

                                                var profileInfo = {
                                                    'first_name': first_name,
                                                    'last_name': last_name,
                                                    'mobile_number': mobile_number,
                                                    'gender': $('input[name=gender]:checked', '#profile-gender').val(),
                                                    'address1': address1,
                                                    'address2': address2,
                                                    'zip': zip,
                                                    'city': city,
                                                    'country': country,
                                                    'state': state,
                                                    'date_of_birth': birthday,
                                                    'country_code': $('#countryCodeNumber').text(),
                                                    'my_hobbies': myHobbies,
                                                    'profile_pic': $('#get-profile-pic-name').val(),
                                                    'img_name': img_name,
                                                    'img_type': img_type,
                                                    'img_size': img_size,
                                                    'profile_info': 'profile'
                                                };

                                                // var profileInfo = JSON.stringify(profileInfo);
                                                profileInfo = JSON.stringify(profileInfo);

                                                var form_data = new FormData(); // Creating object of FormData class
                                                form_data.append("fileToUpload", file_data);
                                                form_data.append('data', profileInfo);

                                                //console.log(file_data); return;

                                                $('#save-profile-btn').prop('disabled', true);

                                                $.ajax({
                                                    url: rootUrl + "controllers/user/updateProfileController.php",
                                                    dataType: "json",
                                                    type: "POST",
                                                    cache: false,
                                                    contentType: false,
                                                    processData: false,
                                                    data: form_data,

                                                    success: function(data) {
                                                        console.log(data);
                                                        if (data.errCode == -1) {
                                                            showToast("success", data.errMsg);
                                                            window.setTimeout(function() {
                                                                location.reload()
                                                               // loadPersonalInfo()
                                                            }, 200)
                                                        } else {
                                                            showToast("failed", data.errMsg);
                                                        }
                                                    },
                                                    error: function(xhr, status, error) {
                                                        alert(error);
                                                    }
                                                });

                                            }else{
                                                $('#dobEmpty').html('Age should be above 13 years.');
                                                $('#dobEmpty').delay(10000).fadeOut();
                                            }

                                        }else{
                                            $('#stateEmpty').html('Please select state');
                                            $('#stateEmpty').delay(10000).fadeOut();
                                        }
                                    }else{
                                        $('#countryEmpty').html('Please select country');
                                        $('#countryEmpty').delay(10000).fadeOut();
                                    }
                            /*    }else{
                                    $('#zipCodeEmpty').text('Zip code must be 6 digits.');
                                    $('#zipCodeEmpty').delay(10000).fadeOut();
                                }
                            }else{
                                $('#zipCodeEmpty').text('Zip code is required.');
                                $('#zipCodeEmpty').delay(10000).fadeOut();
                            }*/
                        /*}else{
                            $('#mobileNumberEmpty').text('Mobile number must be 10 digits.');
                            $('#mobileNumberEmpty').delay(10000).fadeOut();
                        }*/
                    /*}else {
                        $('#mobileNumberEmpty').html('Mobile number is required');
                        $('#mobileNumberEmpty').delay(10000).fadeOut();
                    }*/
                }else{
                    $('#lastNameEmpty').html('Last name is required');
                    $('#lastNameEmpty').delay(10000).fadeOut();
                }
            }else{
                $('#firstNameEmpty').html('First name is required');
                $('#firstNameEmpty').delay(10000).fadeOut();
            }
        }

        function loadPersonalInfo(){
           // alert('i m in');
            //$('#profileLeftPanel').load(rootUrl+'views/social/user_social/myProfile_left_panel.php');
            //$('#personalProfile').load(rootUrl+'views/social/user_social/myProfile_PersonalInformation.php');
           // $('#mainprofile').html('');
           // $('#mainprofile').removeClass('inner-7x');
           // $('.mainprofile').load(rootUrl+'views/social/user_social/myprofile.php');
        }

        $(function() {
            $('#datepicker_dob').datepicker({
                maxDate: 0,
                dateFormat: 'dd/mm/yy',
                autoclose: true,
                changeYear: true,
                changeMonth: true,
                keyboardNavigation : false ,
                daysOfWeekDisabled : [0],
                yearRange: "-70:+0",

                onSelect: function (date) {
                    var dob = new Date(date);
                    var today = new Date();
                    if (dob.getFullYear() + 13 > today.getFullYear())
                    {
                        /* Not eligible for update profile */
                        $("#dob").addClass("text-dark-red");
                        $("#dob").val($(this).val());

                    }else{
                        $("#dob").val($(this).val());
                    }
                }
            });
        });

        function isNumberKey(evt)
        {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        function filePreview(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    // $('#uploadImg + img').remove();
                    $('#myImage ').remove();
                    $('#uploadImg').after('<img id="myImage" class="img-responsive profile-detail-img profile-img-size" src="' + e.target.result + '" width="233" height="200"/>');
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#fileToUpload").change(function() {

            /* var det = this.files;
             console.log(det);*/
            var file = this.files[0];
            var fileType = file["type"];
            var imageName = file["name"];

            var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];

            // alert('file '+file + '   filetype '+fileType+'     format '+validImageTypes);

            if ($.inArray(fileType, validImageTypes) < 0) {
                $('#invalidImageResponse').html('Please upload valid image of jpg, jpeg and png format.');
                $('#image-upload-confirmation-dialog').modal('show');
            } else {
                $('#get-profile-pic-name').val(imageName);
                filePreview(this);
            }
        });

        $(".allownumericwithoutdecimal").on("keypress keyup blur paste", function(event) {
            var that = this;
            if (event.type === "paste") {
                setTimeout(function() {
                    $(that).val($(that).val().replace(/[^\d].+/, ""));
                }, 100);
            } else {
                if (event.which < 48 || event.which > 57) {
                    event.preventDefault();
                } else {
                    $(this).val($(this).val().replace(/[^\d].+/, ""));
                }
            }
        });

        function updateEducation() {

            var universityId = $('input[name=universityRadio]:checked', '#universityId').val();
            var co_curricular_act = $('#curricularActivities').val();
            var existCuricularActivities = $('#existCuricularActivities').val();

            var schoolArr = [];
            var universityArr = [];

            if (co_curricular_act != '') {
                co_curricular_act = existCuricularActivities + "," + co_curricular_act;
            } else {
                co_curricular_act = existCuricularActivities;
            }

            if(counter > 0){
                for (i = 1; i <= counter; i++) {
                    var school_name = $('#schoolName_' + i).val();
                    var from_date = $('#from_year_' + i).val();
                    var to_date = $('#to_year_' + i).val();

                    if (school_name != '' && !isNaN(from_date) && !isNaN(to_date)) {
                        var data = {
                            "school_name": school_name,
                            "from_date": from_date,
                            "to_date": to_date
                        };

                        schoolArr.push(data);
                    }
                }
            }

            if(universityCounter > 0){
                console.log(universityCounter);
                for (j = 1; j <= universityCounter; j++) {
                    var university_name = $('#university_name_' + j).val();
                    var university_specification = $('#university_specification_' + j).val();
                    var university_from_date = $('#university_from_date_' + j).val();
                    var university_to_date = $('#university_to_date_' + j).val();

                    if (university_name != '' && university_specification != '' && !isNaN(university_from_date) && !isNaN(university_to_date)) {
                        var data1 = {
                            "university_name": university_name,
                            "university_specification": university_specification,
                            "university_from_date": university_from_date,
                            "university_to_date": university_to_date
                        };

                        universityArr.push(data1);
                    }


                }
            }


            if ( schoolArr.length > 0 || universityArr.length > 0) {

                $('#save-education-btn').prop('disabled', true);

                $.ajax({
                    url: rootUrl + "controllers/user/updateProfileController.php",
                    dataType: "json",
                    type: "POST",
                    data: {
                        school_info: schoolArr,
                        university_info: universityArr,
                        profile_info: encodeURIComponent('education'),
                        universityId: universityId,
                        curricular_acvities: co_curricular_act,
                        flag_status: 999
                    },

                    success: function(data) {
                        console.log(data);
                        if (data.errCode == -1) {
                            showToast("success", data.errMsg);
                            window.setTimeout(function() {
                                location.reload()
                            }, 2000)
                        } else {
                            showToast("failed", data.errMsg);
                        }
                    },
                    error: function(xhr, status, error) {
                        alert(error);

                    }
                });
            } else {
                showToast("failed", 'Please enter education details');
            }
        }

        function updateWorkInfo() {

            var companyArr = [];
            var professionalSkills = $('#professionalSkills').val();
            var existingSkills = $('#existingProfessionalSkills').val();

            if (professionalSkills != '') {
                professionalSkills = existingSkills + "," + professionalSkills;
            } else {
                professionalSkills = existingSkills;
            }

            professionalSkills = professionalSkills.replace(/ /g, '');

            if(companyCounter > 0){
                for (i = 0; i <= companyCounter; i++) {
                    var company_name = $('#company_name_' + i).val();
                    var designation = $('#designation_' + i).val();
                    var company_from_date = $('#company_from_date_' + i).val();
                    var company_to_date = $('#company_to_date_' + i).val();

                    if (company_name != '' && designation != '' && !isNaN(company_from_date) && !isNaN(company_to_date)) {
                        var comData = {
                            "company_name": company_name,
                            "designation": designation,
                            "company_from_date": company_from_date,
                            "company_to_date": company_to_date
                        };

                        companyArr.push(comData);
                    }
                }
            }

            if(companyArr.length > 0){

                $('#save-workinfo-btn').prop('disabled', true);

                $.ajax({
                    url: rootUrl + "controllers/user/updateProfileController.php",
                    dataType: "json",
                    type: "POST",
                    data: {
                        company_info: companyArr,
                        skills: professionalSkills,
                        profile_info: encodeURIComponent('work'),
                        flag_status: 999
                    },

                    success: function(data) {
                        console.log(data);
                        if (data.errCode == -1) {
                            showToast("success", data.errMsg);
                            window.setTimeout(function() {
                                location.reload()
                            }, 2000)
                        } else {
                            showToast("failed", data.errMsg);
                        }
                    },
                    error: function(xhr, status, error) {
                        alert(error);
                        showToast('failed', error);
                    }
                });
            }else{
                showToast("failed", 'Please enter company details.');
            }

        }

        $(document).ready(function() {

            //attach keypress to input
            $('.number-only').keydown(function(event) {
                // Allow special chars + arrows
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9
                    || event.keyCode == 27 || event.keyCode == 13
                    || (event.keyCode == 65 && event.ctrlKey === true)
                    || (event.keyCode >= 35 && event.keyCode <= 39)){


                    return;
                }else {
                    // If it's not a number stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                        event.preventDefault();
                    }
                }
            });

        });
        </script>
