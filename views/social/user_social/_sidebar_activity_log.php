<!--==============================
=            include "_sidebar_activity_log.php"            =
===============================-->
<div class="card left-panel-modules inner-2x innerMT">
    <div class="bg-light-gray left-panel-modules-head">
        <div class="row margin-none">
            <div class="col-xs-12">
                <h4>Activity Log</h4>
            </div>
        </div>
    </div>
    <div class="settings-preference-module">
        <div class="margin-none">
            <div class="social-user-setting-name border-bottom">
                <a>Likes<span>(2423)</span> </a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Comments <span>(2423)</span></a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Hidden Comments <span>(2423)</span></a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Hidden Post <span>(2423)</span></a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Post Bookmarks <span>(2443)</span></a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Followed People <span>(2723)</span></a>
            </div>
            <div class="social-user-setting-name border-bottom">
                <a>Followed Keywords <span>(24)</span></a>
            </div>
        </div>
    </div>
</div>
<!--====  End of include "_sidebar_activity_log.php"  ====-->
