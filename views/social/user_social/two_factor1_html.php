<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container row-10 padding-none">
            <div class="row">
                <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                        <div class="like-unlike-user-profile-">
                            <img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png" />
                            <div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
                                <div class="col-xs-6 padding-none">
                                    <div class="col-xs-12 padding-none">
                                        Lorem
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <span class="account-handle-name">@loremi</span>
                                    </div>
                                </div>
                                <div class="col-xs-6 padding-none innerMT">
                                    <input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile" />
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Settings and Preferences</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Profile</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Change Password</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Notification Preferences</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom active">
                                        <a>Two Factor (2FA)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Activity Log</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Likes / Dislikes</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Comments <span>(2423)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Post Bookmarks <span>(2443)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed People <span>(2723)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed Keywords <span>(24)</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
                <div class="col-xs-9">
                    <div class="social-center-panel">
                        <div class="row">
                            <div class="col-xs-12 innerB">
                                <div class="profile-heading ">
                                    <h4 class="text-color-Text-Primary-Blue margin-none f-sz20  ">Settings and Preferences</h4> :
                                    <h5 class=" margin-none f-sz15">Two Factor (2FA)</h5>
                                </div>
                                <!-- profile heading -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="bg-white border-all clearfix innerAll inner-2x Authentication text-color-Light-Black">
                                <p class="f-sz15 l-h16">Two-factor Authentication adds an extra layer of security to your account. Once enabled, Sign-In will require you to enter aunique verification code generated by an app on your mobile device or sent via SMS text message, In addition to your username and password.</p>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="text-color-Text-Primary-Blue margin-top-none">Choose Your Security Method</h4>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="bg-color-grayscale-fb border-all innerAll text-color-grayscale-4">
                                            <h4 class="text-center f-sz16">Transaction pin</h4>
                                            <div class="btn-block text-center innerAll">
                                                <i class="fa fa-check-square fa-2x text-color-grayscale-80 f-sz20"></i>

                                            </div>
                                            <p class="text-center f-sz13">Transaction pin lets you authenticate your identity while making transations.</p>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <div class="bg-color-grayscale-fb border-all innerAll text-color-grayscale-4">
                                            <h4 class="text-center f-sz16">Google Authenticator</h4>
                                            <div class="btn-block text-center innerAll">
                                                <i class="fa fa-check-square fa-2x text-color-App-Success f-sz20"></i>
                                            </div>
                                            <p class="text-center  f-sz13">An authenticator lets you generate codes without needing to receive text messages / voice calls during sign-in</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="innerT inner-2x">
                                    <button type="button" class="btn  pull-right  btn-trading-wid-auto ">Next</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-center-panel  -->
                </div>
                <!-- col-xs-6 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../layout/social_footer.php'); ?>
