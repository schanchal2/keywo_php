<main class="social-main-container innerT UA6">
<div class="container padding-none row-10">
    <div class="row">
        <div class="col-xs-9">
            <div class="social-center-panel">
                <div class="row">
                    <div class="col-xs-12 innerB">
                        <div class="profile-heading">
                            <h4 class="text-color-Text-Primary-Blue margin-none f-sz20">Settings and Preferences</h4> :
                            <h5 class=" margin-none  f-sz15">Change Password</h5>
                        </div>
                        <!-- profile heading -->
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <!-- <div class="card bg-white border-all innerAll half text-color-Light-Black"> -->
                    <div class="bg-white border-all clearfix innerAll  text-color-Light-Black">
                        <!-- <div class="card social-card all-box-shadow inner-2x innerMT clearfix"> -->
                        <div class="row">
                            <div class="col-xs-12">
                                <form class="form-horizontal" id="input-wth-bg">
                                    <div class="form-group">
                                        <div class=" half innerMLR">
                                            <label for="inputPassword3"
                                                   class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span
                                                        class="pull-left">Old Password</span></label>
                                            <div class="col userInput">
                                                <div class="input-group text-color-Gray materialize bdrB">
                                                    <input placeholder="" type="Password" name="currentPassword"
                                                           id="currentPassword"
                                                           class="form-control text-color-Text-Primary-Blue padding-left-none">
                                                    <span class="input-group-addon bg-white text-color-grayscale-be"> <i
                                                                class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                </div>
                                            </div>
                                            <div id="errorPassword" class="text-red"> <?php echo $errMessage; ?></div>


                                            <label for="inputPassword3"
                                                   class="col control-label f-sz12 help-block fw-100">
                                                <div class="pull-left text-color-Red"></div>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class=" half innerMLR">
                                            <label for="inputPassword3"
                                                   class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span
                                                        class="pull-left">New Password</span></label>
                                            <div class="col userInput">
                                                <div class="input-group text-color-Gray materialize bdrB">
                                                    <input placeholder="" type="Password" name="newPassword"
                                                           id="newPassword"
                                                           class="form-control text-color-Text-Primary-Blue padding-left-none">
                                                    <span class="input-group-addon bg-white text-color-grayscale-be"> <i
                                                                class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                </div>
                                            </div>
                                            <div id="erroremailshow" class="text-red"> <?php echo $errMessage; ?></div>


                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class=" half innerMLR">
                                            <label for="inputPassword3"
                                                   class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span
                                                        class="pull-left">Confirm Password </span></label>
                                            <div class="col userInput">
                                                <div class="input-group text-color-Gray materialize bdrB">
                                                    <input placeholder="" type="Password" name="confirmPassword"
                                                           id="confirmPassword"
                                                           class="form-control text-color-Text-Primary-Blue padding-left-none">
                                                    <span class="input-group-addon bg-white text-color-grayscale-be"> <i
                                                                class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                </div>
                                            </div>
                                            <label for="inputPassword3"
                                                   class="col control-label f-sz12 help-block fw-100">
                                                <div class="pull-left text-color-Red"></div>
                                            </label>
                                        </div>
                                    </div>
                                </form>
                                <div class="clearfix ">
                                    <button type="button"    onclick="resetForm(this);" class="btn pull-right  btn-trading-dark"> &nbsp; Cancel
                                        &nbsp; </button>
                                    <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto "
                                            onclick="passwordSubmit(this);"> &nbsp; Save Changes &nbsp; </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- social-center-panel  -->
        </div>
        <!-- col-xs-6 -->
    </div>
</div>
</main>
<script>

    $("#currentPassword").keyup(function(e){
        if (e.which != 32) {
            currentPassword = $("#currentPassword").val();
            if(currentPassword.length > 5){
                $.ajax({
                    url: '../../../controllers/user/checkPasswordController.php',
                    type: 'POST',
                    dataType: 'json',
                    data: ({password: currentPassword}),
                    success: function (data) {
                        console.log(data);
                        if (data.errCode == -1) {
                            $('#errorPassword').html("");
                            $("#newPassword").prop("disabled", false);
                            $("#confirmPassword").prop("disabled", false);
                        } else if (data.errCode == 3) {
                            $('#errorPassword').html('Your Entered Wrong Password!!!!');
                            $("#newPassword").prop("disabled", true);
                            $("#confirmPassword").prop("disabled", true);
                        } else {
                            showToast("failed", 'User action Failed');
                        }
                    },
                    error: function (data) {
                        showToast("failed", 'Error in Connection');
                    }
                });
            }
        } else {
            $(this).val($(this).val().replace(/\s+/g, ''));
        }
    });

    $(document).ready(function () {
        $("#newPassword").prop("disabled", true);
        $("#confirmPassword").prop("disabled", true);
    });


    $("#newPassword").keyup(function(e){
        if (e.which != 32) {

        } else {
            $(this).val($(this).val().replace(/\s+/g, ''));
        }
    });

    $("#confirmPassword").keyup(function(e){
        if (e.which != 32) {

        } else {
            $(this).val($(this).val().replace(/\s+/g, ''));
        }
    });
    

</script>