<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");
        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container">
            <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
            <div class="col-xs-3">
                <div class="social-left-panel left-panel-modules">
                    <div class="like-unlike-user-profile-">
                        <img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png" />
                        <div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
                            <div class="col-xs-6 padding-none">
                                <div class="col-xs-12 padding-none">
                                    Lorem
                                </div>
                                <div class="col-xs-12 padding-none">
                                    <span class="account-handle-name">@loremi</span>
                                </div>
                            </div>
                            <div class="col-xs-6 padding-none innerMT">
                                <input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile" />
                            </div>
                        </div>
                    </div>
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4>Settings and Preferences</h4>
                                </div>
                            </div>
                        </div>
                        <div class="settings-preference-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Profile</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Change Password</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Notification Preferences</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Two Factor (2FA)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4>Activity Log</h4>
                                </div>
                            </div>
                        </div>
                        <div class="settings-preference-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Likes / Dislikes</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Comments <span>(2423)</span></a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Bookmarks <span>(2443)</span></a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Followed People <span>(2723)</span></a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Followed Keywords <span>(24)</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
            <div class="col-xs-9">
                 <div class="social-center-panel">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="profile-heading">
                                <h4 class="text-blue margin-none">Settings and Preferences</h4> :
                                <h5 class=" margin-none">Notification Preferences</h5>
                            </div>
                            <!-- profile heading -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="bg-white border-all clearfix inner-2x innerMT  Authentication text-Light-Black">
                            <h4 class="text-blue margin-none innerT" > WALLET </h4>

                            <p>
                                Select your preferred mode to receive notifications about deposites and withdrawal from the available options like SMS, Email, Push or all of the available options.


                            </p>
                            <h4 class="text-blue margin-none innerT" > KEYWORD </h4>
                        </div>
                    </div>
                </div>
                <!-- social-center-panel  -->
            </div>
            <!-- col-xs-6 -->
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../layout/social_footer.php'); ?>
