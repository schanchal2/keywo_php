<div class="card social-card all-box-shadow inner-2x innerMT innerMB clearfix">
    <div class="">
        <!-- education detail section -->
        <div class="">
            <div class="col-xs-12 border-bottom">
                <div class="education-information-heading innerTB">
                    <h4 class="text-blue margin-bottom-none">
                                                Educational Information
                                            </h4>
                </div>
                <!-- Title of education card -->
            </div>

            <div class="innerMT">
                <div class="educational-information-form position-relative">
                    <form class="clearfix" class="educational-information">
                        <div class="col-xs-12 innerMT high-school-wrapper-div">
                            <label>
                                High School
                            </label>
                            <div class="add-to-btn">
                                <span><i class="fa fa-plus text-blue"></i></span>
                                <span class="add-btn">Add</span>
                            </div>
                        </div>
                        <div class="high-school-module" id="work-info">
                            <div id="highSchool_0" class="col-xs-12 inner-2x innerB innerMT">
                                <div class="form-group">
                                    <input type="text" id="schoolName_0" name="schoolName_0" class="school-input form-control" placeholder="Which school did you attend?" />
                                    <input type="text" min="0" id="from_year_0" min="0" maxlength="4" class="years year-start form-control number-only"  />
                                    <div class="years-to">
                                        To
                                    </div>
                                    <input type="text" min="0" id="to_year_0" min="0" maxlength="4" class="years year-end form-control number-only" />
                                    <span class="time-period">( Time period in years )</span>
                                </div>
                            </div>
                            <?php  if($totalSchoolInfoCount > 0){ foreach($getSchoolInfo as $key => $schDetails){  ?>
                            <div id="highSchool_<?php echo $key + 1; ?>" class="col-xs-12 inner-2x innerB innerMT">
                                <div class="form-group">
                                    <input type="text" id="schoolName_<?php echo $key + 1; ?>" class="school-input form-control" placeholder="Which school did you attend?" value="<?php echo $schDetails["school_name"]; ?>" />
                                    <input type="text" min="0" maxlength="4" id="from_year_<?php echo $key + 1; ?>" class="years year-start form-control number-only" value="<?php echo $schDetails["from_date"]; ?>" />
                                    <div class="years-to">
                                        To
                                    </div>
                                    <input type="text" min="0" maxlength="4" id="to_year_<?php  echo $key + 1; ?>" class="years year-end form-control number-only" value="<?php echo $schDetails["to_date"]; ?>" />
                                    <span class="time-period">( Time period in years )</span>
                                    <span data-id="highSchool_<?php echo $key + 1; ?>" class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>
                                </div>
                            </div>
                            <?php  } } ?>
                        </div>
                        <!-- High school section  -->
                        <div class="col-xs-12 university-section">
                            <label>
                                University
                            </label>
                            <div class="university-add-to-btn">
                                <span><i class="fa fa-plus text-blue"></i></span>
                                <span class="add-btn">Add</span>
                            </div>
                            <div class="university-selections">
                                <div class="innerMT" id="universityId">
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="universityRadio" id="university" value="university" <?php if($universityRadioParam=='university' ){ echo "checked=true"; }else if(empty($universityRadioParam)){ echo "checked=true"; } ?> >
                                        <label for="university">
                                            University
                                        </label>
                                    </div>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="universityRadio" id="universityGrad" value="universityGrad" <?php if($universityRadioParam=='universityGrad' ){ echo "checked=true"; } ?> >
                                        <label for="universityGrad">
                                            University (postgraduate)
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="university-module">
                                <div id="university_0" class="padding-left-none inner-2x innerTB" style="position:relative;">
                                    <div class="form-group">
                                        <input type="text" id="university_name_0"  name="university_name_0" class="university-input-name form-control" placeholder="Which university did you attend?" />
                                        <input type="text" id="university_specification_0" class="university-input-specification form-control" placeholder="Specification" />
                                        <input type="text" min="0" id="university_from_date_0" maxlength="4" class="years year-start form-control number-only" />
                                        <div class="years-to">
                                            To
                                        </div>
                                        <input type="text" min="0" id="university_to_date_0" maxlength="4" class="years year-end form-control number-only" />
                                        <span class="time-period">( Time period in years )</span></div>
                                </div>
                                <?php if($totalUniversityInfoCount > 0){ foreach($getUniversityInfo as $key => $uniDetails){ ?>
                                <div id="university_<?php echo $key + 1; ?>" class="padding-left-none innerT" style="position:relative;">
                                    <input type="text" id="university_name_<?php echo $key + 1; ?>" class="university-input-name form-control" placeholder="Which university did you attend?" value="<?php echo $uniDetails["university_name"] ?>" />
                                    <input type="text" id="university_specification_<?php echo $key + 1; ?>" class="university-input-specification form-control" placeholder="Specification" value="<?php echo $uniDetails["university_specification"] ?>" />
                                    <input type="text" maxlength="4" min="0"  id="university_from_date_<?php echo $key + 1; ?>" class="years year-start form-control number-only" value="<?php echo $uniDetails["university_from_date"] ?>" />
                                    <div class="years-to">
                                        To
                                    </div>
                                    <input type="text" min="0" maxlength="4" id="university_to_date_<?php echo $key + 1; ?>" class="years year-end form-control number-only" value="<?php echo $uniDetails["university_to_date"] ?>" />
                                    <span class="time-period">( Time period in years )</span>
                                    <span data-id="university_<?php echo $key + 1; ?>" class="text-color-Gray remove-input pull-right remove_field"><i class="fa fa-trash"></i></span>
                                </div>
                                <?php } } ?>
                            </div>
                        </div>
                        <!-- university section -->
                        <div class="col-xs-8 innerMB">
                            <label>
                                Co-curricular Activities
                            </label>
                            <div id="tagsCurricular" class="border-bottom" style="word-break: break-word;">
                                <?php

                                                        $existActivities =  implode(",", $curricularActivities);
                                                        if(count($curricularActivities) > 0){

                                                            foreach($curricularActivities as $key => $myActivities){ ?>
                                    <span> <?php echo $myActivities; ?> </span>
                                    <?php   }
                                                        }
                                                        ?>
                                    <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" />
                            </div>
                            <input type="hidden" id="curricularActivities" value="" />
                            <input type="hidden" id="existCuricularActivities" value="<?php if(isset($existActivities) && !empty($existActivities)){ echo $existActivities; } ?>" />
                        </div>
                        <div class="col-xs-4">
                            <label for=""></label>
                            <br />
                            <div class="row">
                                <div class="col-xs-6 text-center padding-none">
                                    <input type="button" id="save-education-btn" class="btn-social-wid-auto btn-xs" value="Save Changes" onclick="updateEducation();" />
                                </div>
                                <!-- <div class="col-xs-6 text-center padding-none">
                                                            <input type="button" class="btn-social-wid-auto-dark btn-xs" value="Reset" />
                                                        </div>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
