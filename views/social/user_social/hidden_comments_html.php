<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container row-10 padding-none">
            <div class="row">
                <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                        <div class="like-unlike-user-profile-">
                            <img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png" />
                            <div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
                                <div class="col-xs-6 padding-none">
                                    <div class="col-xs-12 padding-none">
                                        Lorem
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <span class="account-handle-name">@loremi</span>
                                    </div>
                                </div>
                                <div class="col-xs-6 padding-none innerMT">
                                    <input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile" />
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Settings and Preferences</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Profile</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Change Password</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Notification Preferences</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Two Factor (2FA)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include '_sidebar_activity_log.php'; ?>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
                <div id="activity__log">
                    <div class="col-xs-9">
                        <div class="social-center-panel">
                            <div class="row">
                                <div class="col-xs-12 innerB">
                                    <div class="profile-heading ">
                                        <h4 class="text-color-Text-Primary-Blue margin-none f-sz20">Activity Log</h4> :
                                        <h5 class=" margin-none f-sz15">Hidden Comment</h5>
                                        <div class="pull-right">
                                            <form class="clearfix margin-none">
                                                <div class="pull-left search-box-comments">
                                                    <div class="input-group">
                                                        <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="">
                                                        <input type="submit" class="search-btn-header" value="">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- profile heading -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="h4  innerL ">Today</div>
                                <ul class="list-unstyled innerMT activity-log__list">
                                    <li class="card innerAll border-bottom ">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object media-object--activity-log " src="https://placeholdit.imgix.net/~text?w=100&h=50" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                                <!-- 
                                             -->
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16 "></i>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center f-sz16 "></i>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="h4  innerT innerL ">Yesterday</div>
                                <ul class="list-unstyled innerMT activity-log__list">
                                    <?php for ($i=0; $i <5 ; $i++) { ?>
                                    <li class="card innerAll border-bottom">
                                        <div class="media">
                                            <div class="media-left">
                                                <a href="#">
                                                    <img class="media-object media-object--activity-log " src="https://placeholdit.imgix.net/~text?w=100&h=50" alt="...">
                                                </a>
                                            </div>
                                            <div class="media-body f-sz15">
                                                <div class="row">
                                                    <div class="col-xs-11">
                                                        <div class="row">
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">I made $700 With my first post and Lorem ipsum dolor sit amet.</p>
                                                            </div>
                                                            <div class="col-xs-6">
                                                                <p class="ellipses innerTB half margin-none">You have Hidden <span class="text-color-Text-Primary-Blue ">Lorem Ipsum </span> Comment.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xs-12">
                                                                <div class="media margin-none">
                                                                    <div class="media-left">
                                                                        <i class="fa fa-comments-o text-color-Text-Primary-Blue "> </i>
                                                                        </blockquote>
                                                                    </div>
                                                                    <div class="media-body ">
                                                                        <blockquote class="activity-log__comment border-none f-sz14 padding-none margin-none text-color-grayscale-5 "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur cupiditate aliquid tenetur recusandae ab aut incidunt optio, ut error dolor sit ex nulla modi laboriosam qui dolore, aperiam quia molestiae!
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-1">
                                                        <a class="btn btn-link" href="#" role="button"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
                                                    </div>
                                                </div>
                                                <!-- 
                                             -->
                                            </div>
                                        </div>
                                    </li>
                                    <?php  } ?>
                                </ul>
                            </div>
                        </div>
                        <!-- social-center-panel  -->
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../layout/social_footer.php'); ?>
