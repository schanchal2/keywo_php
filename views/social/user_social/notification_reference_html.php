<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT">
        <div class="container padding-none row-10">
            <div class="row">
                <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                        <div class="like-unlike-user-profile-">
                            <img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png" />
                            <div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
                                <div class="col-xs-6 padding-none">
                                    <div class="col-xs-12 padding-none">
                                        Lorem
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <span class="account-handle-name">@loremi</span>
                                    </div>
                                </div>
                                <div class="col-xs-6 padding-none innerMT">
                                    <input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile" />
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Settings and Preferences</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Profile</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Change Password</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Notification Preferences</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Two Factor (2FA)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Activity Log</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Likes / Dislikes</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Comments <span>(2423)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Post Bookmarks <span>(2443)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed People <span>(2723)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed Keywords <span>(24)</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
                <div class="col-xs-9">
                    <div class="">
                        <div class="social-center-panel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="profile-heading">
                                        <h4 class="text-blue margin-none f-sz20">Settings and Preferences</h4> :
                                        <h5 class=" margin-none f-sz15"> Notification Preferences</h5>
                                    </div>
                                    <!-- profile heading -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="bg-white border-all clearfix innerMT innerAll Authentication text-color-Light-Black">
                                    <div class="">
                                        <div class="col-xs-6">
                                            <h4 class="text-blue margin-none innerT"> WALLET </h4>
                                            <p>
                                                Select your preferred mode to receive notifications about deposites and withdrawal from the available options like SMS, Email, Push or all of the available options.
                                            </p>
                                        </div>
                                        <div class="col-sm-4 col-sm-offset-2">
                                            <div class="checkbox row text-color-orange">
                                                <label class="col-xs-4">
                                                    EMAIL
                                                </label>
                                                <label class="col-xs-4">
                                                    SMS
                                                </label>
                                                <label class="col-xs-4">
                                                    PUSH
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="col-xs-12">
                                            <form action="" method="POST" class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Deposit :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a10" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a10" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a11" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a11" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a12" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a12" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Withdrawal :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a13" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a13" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a14" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a14" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a15" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a15" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="col-xs-6">
                                            <h4 class="text-blue margin-none innerT"> KEYWORD </h4>
                                            <p>
                                                Select your preferred mode to receive notifications about Bids, Asking Price, Keywords broughts, Keywords Sold from the available options like SMS, Email, Push or all off the available options.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="col-xs-12">
                                            <form action="" method="POST" class="form-horizontal" role="form">
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Buy :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a16" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a16" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a17" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a17" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a18" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a18" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Bid :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a9" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a9" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a8" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a8" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a7" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a7" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Ask :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a6" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a6" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a5" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a5" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a4" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a4" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class=" half innerMLR">
                                                        <label for="inputPassword3" class="col-sm-8 control-label text-right fw-100 "><span class="text-blue">Keyword License :</span></label>
                                                        <div class="col-sm-4">
                                                            <div class="row">
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a3" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a3" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a2" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a2" class="fa"></label>
                                                                </div>
                                                                <div class="col-xs-3 col-xs-offset-1">
                                                                    <input class=" user_checkbox" id="a1" type="checkbox" name="user_checkboxIP[]" value="31">
                                                                    <label for="a1" class="fa"></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="innerB clearfix ">
                                                    <button type="button" class="btn pull-right  btn-trading-dark"> &nbsp; Cancel &nbsp; </button>
                                                    <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto "> &nbsp; Save Changes &nbsp; </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- social-center-panel  -->
                    </div>
                </div>
            </div>
            <!-- col-xs-6 -->
        </div>
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../layout/social_footer.php'); ?>
