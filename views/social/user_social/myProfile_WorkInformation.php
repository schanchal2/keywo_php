<div class="card social-card all-box-shadow inner-2x innerMB clearfix">
    <div class="">
        <!-- work information section -->
        <div class="">
            <div class="col-xs-12 border-bottom">
                <div class="work-information-heading innerTB">
                    <h4 class="text-blue margin-bottom-none">
                                                Work Information
                                            </h4>
                </div>
                <!-- Title of work card -->
            </div>
            <div class="innerMT">
                <div class="work-information-form">
                    <form class="clearfix" class="work-information">
                        <div class="work-detail">
                            <div class="col-xs-12 innerMT">
                                <label>
                                    Company Name & Designation
                                </label>
                                <div class="company-add-to-btn">
                                    <span><i class="fa fa-plus text-blue"></i></span>
                                    <span class="add-btn">Add Company</span>
                                </div>
                            </div>
                            <!-- Company name -->
                            <div class="work-section">
                                <div id="workDetail_0" class="col-xs-12">
                                    <div class="padding-left-none inner-2x innerT innerB">
                                        <input type="text" id="company_name_0" class="company-input-name form-control" placeholder="Company name" />
                                        <input type="text" id="designation_0" class="company-input-designation form-control" placeholder="Add designation" />
                                        <input type="text" min="0" maxlength="4" id="company_from_date_0" class="years year-start form-control number-only" />
                                        <div class="years-to">
                                            To
                                        </div>
                                        <input type="text" min="0" maxlength="4" id="company_to_date_0" class="years year-end form-control number-only" />
                                        <span class="time-period">( In Years )</span>
                                    </div>
                                </div>
                                <?php

                                                        if($totalcompanyInfoCount > 0){ foreach($companyInfo as $key => $comDetails){ ?>
                                    <div id="workDetail_<?php echo $key + 1; ?>" class="col-xs-12">
                                        <div class="padding-left-none inner-2x innerT innerB">
                                            <input type="text" id="company_name_<?php echo $key + 1; ?>" class="company-input-name form-control" placeholder="Company name" value="<?php echo $comDetails["company_name"]; ?>" />
                                            <input type="text" id="designation_<?php echo $key + 1; ?>" class="company-input-designation form-control" placeholder="Add designation" value="<?php echo $comDetails["designation"]; ?>" />
                                            <input type="text" min="0" maxlength="4" id="company_from_date_<?php echo $key + 1; ?>" class="years year-start form-control number-only" value="<?php echo $comDetails["company_from_date"]; ?>" />
                                            <div class="years-to">
                                                To
                                            </div>
                                            <input type="text" min="0" maxlength="4" id="company_to_date_<?php echo $key + 1; ?>" class="years year-end form-control number-only" value="<?php echo $comDetails["company_to_date"]; ?>" />
                                            <span class="time-period">( In Years )</span>
                                            <span data-id="workDetail_<?php echo $key + 1; ?>" class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>
                                        </div>
                                    </div>
                                    <?php
                                                            }
                                                        }
                                                        ?>
                            </div>
                        </div>
                        <!-- university section -->
                        <div class="inner-2x innerMT">
                            <div class="col-xs-8 innerMB">
                                <label>
                                    Professional Skills
                                </label>
                                <div id="tagsProfessional" class="border-bottom" style="word-break: break-word;">
                                    <?php

                                                            $existingWorkSkills = implode(",", $workSkills);
                                                            if(count($workSkills) > 0){
                                                                foreach($workSkills as $key => $mySkills){
                                                                    ?>
                                        <span> <?php echo $mySkills; ?></span>
                                        <?php
                                                                }
                                                            }

                                                            ?>
                                            <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" />
                                </div>
                                <input type="hidden" id="professionalSkills" value="" />
                                <input type="hidden" id="existingProfessionalSkills" value=" <?php if(isset($existingWorkSkills) && !empty($existingWorkSkills)){ echo $existingWorkSkills; } ?>" />
                            </div>
                            <div class="col-xs-4 submit-buttons innerMB">
                                <div class="row">
                                    <div class="col-xs-6 text-center padding-none">
                                        <input type="button" id="save-workinfo-btn" class="btn-social-wid-auto" value="Save Changes" onclick="updateWorkInfo();" />
                                    </div>
                                    <!--<div class="col-xs-6 text-center padding-none">
                                                                <input type="button" class="btn-social-wid-auto-dark" value="Reset" />
                                                            </div>-->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
