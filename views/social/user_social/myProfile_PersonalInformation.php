<div class="card social-card all-box-shadow  innerMT clearfix" id="personalProfile">
    <div class="">
        <div class="">
            <div class="col-xs-12 border-bottom">
                <div class="personal-information innerTB">
                    <h4 class="text-blue margin-bottom-none">Personal Information</h4>
                </div>
            </div>
            <div class="col-xs-12 innerMT">
                <div class=" row personal-detail-form">
                    <form class="clearfix" enctype="multipart/form-data" id="personalInformation">
                        <div class="col-xs-8  padding-none">
                            <div class="row">
                                <!-- first name and last name -->
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            First Name <span class="mandatory">*</span>
                                        </label>
                                        <input name="firstname" type="text" class="form-control" id="first_name" value="<?php if(isset($firstName) && !empty($firstName)){ echo ucfirst($firstName); } ?>" />
                                        <span class="mendatoryFailureMsg" id="firstNameEmpty"> </span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            Last Name <span class="mandatory">*</span>
                                        </label>
                                        <input name="lastname" type="text" class="form-control" id="last_name" value="<?php if(isset($lastName) && !empty($lastName)){ echo ucfirst($lastName); } ?>" />
                                        <span class="mendatoryFailureMsg" id="lastNameEmpty"> </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            Mobile Number
                                        </label>
                                        <div class="input-group">
                                            <span class="input-group-addon bg-white border-none padding-none" id="countryCodeNumber"><?php if(!empty($countryCode)){ echo "+".$countryCode; } ?></span>
                                        <input type="text" class="form-control" name="mobileNumber" onkeypress="return isNumberKey(event)" id="mobile_number" value="<?php if(!empty($mobile_number)){ echo $mobile_number; } ?>" maxlength="10">
                                        <span class="mendatoryFailureMsg" id="mobileNumberEmpty"> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 innerMTB" id="profile-gender">
                                    <label></label>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="male" value="male" <?php if($gender=="male" ){ echo "checked=true";} ?> >
                                        <label for="male">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="female" value="female" <?php if($gender=="female" ){ echo "checked=true";} ?> >
                                        <label for="female">
                                            Female
                                        </label>
                                    </div>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="other" value="other" <?php if($gender=="other" ){ echo "checked=true";} ?> >
                                        <label for="other">
                                            Other
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Address 1</label>
                                        <textarea placeholder="Address 1" id="address1" name="address1" class="form-control innerMT" cols=40 rows=6>
                                            <?php if(!empty($address1)){ echo ltrim($address1); } ?>
                                        </textarea>
                                    </div>
                                </div>
                                <div class="col-xs-6 innerMT">
                                    <div class="form-group">
                                        <label>Address 2</label>
                                        <textarea placeholder="Address 2" id="address2" name="address2" class="form-control" cols=40 rows=6>
                                            <?php if(!empty($address2)){ echo trim($address2); } ?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>
                                            Zip Code
                                        </label>
                                        <input type="text" name="zipcode" id="zip" onkeypress="return isNumberKey(event)" maxlength="6" class="form-control" value="<?php if(!empty($zip)){ echo $zip; } ?>" /> <span id="zipFaiureMsg"> </span>
                                        <span class="mendatoryFailureMsg" id="zipCodeEmpty"> </span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label> Select Country <span class="mandatory">*</span> </label>
                                        <select class="select-input chosen-select-deselect" data-placeholder="Select Country" id="country" onChange="getState()">
                                            <?php
                                                $getCountry = file_get_contents($rootUrl."controllers/widget/getCountries.php");
                                                $countryJson = json_decode($getCountry, TRUE);
                                                $count = count($countryJson['country']);
                                            ?>
                                                <option value="<?php echo $country;?>" disabled selected='selected'>
                                                    <?php echo $country;?>
                                                </option>
                                                <?php 
                                                for($i=0; $i<$count; $i++){
                                                    $cid = $countryJson['country'][$i]['country_id'];
                                                    $countryCodeNumber = $countryJson['country'][$i]['country_code_number'];
                                                    $sendIdAndCode = $cid."~~".$countryCodeNumber ;
                                                ?>
                                                <option value="<?php echo $sendIdAndCode; ?>" <?php if($countryJson[ 'country'][$i][ 'country_id']==$country){ ?> selected="selected"
                                                    <?php }?> >
                                                    <?php echo $countryJson['country'][$i]['name']; ?>
                                                </option>
                                                <?php } ?>
                                        </select>
                                        <span class="mendatoryFailureMsg" id="countryEmpty"> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Select State <span class="mandatory">*</span></label>
                                        <select name="state" id="state" class="countrySelect form-control" data-placeholder="Select State" onChange="getCity()">
                                            <option disabled selected='selected' value="<?php if(!empty($state)) { echo $state; }else{ echo "Select State"; }?>">
                                                <?php if(!empty($state)) { echo $state; }else{ echo "Select State"; }?>
                                            </option>
                                        </select>
                                        <span class="MendatoryfailureMsg" id="stateEmpty"> </span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label>Select City</label>
                                        <select name="city" id="city" class="countrySelect form-control">
                                            <option disabled selected='selected' value="<?php if(!empty($city)) { echo $city; }else{ echo "Select City"; }?>">
                                                <?php if(!empty($city)) { echo $city; }else{ echo "Select City"; }?>
                                            </option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-6 ">
                                    <div class="form-group">
                                        <label>Email Id</label>
                                        <input type="email" class="form-control" value="<?php echo $email; ?>" readonly disabled/>
                                    </div>
                                </div>
                                <div class="col-xs-6 calendar-section">
                                    <label>Date Of Birth <span class="mandatory">*</span> </label>
                                    <input readonly type="text" class="form-control date-input" id="dob" placeholder="DD/MM/YYYY" value="<?php if(!empty($dateOfBirth)){ echo $dateOfBirth; } ?>" disabled />
                                    <div class="form-group">
                                        <div class="">
                                            <input type="text" class="form-control date-of-birth" value="DD/MM/YYYY" placeholder="" id="datepicker_dob" >
                                            <span class="text-Blue calendar"> <i class="fa fa-calendar" id=""></i> </span>
                                            <span class="mendatoryFailureMsg" id="dobEmpty"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 innerMTB">
                                    <label>Hobbies</label>
                                    <div id="tags" class="border-bottom" style="word-break: break-word;">
                                        <?php
                                                $existHobbies = implode(",", $hobbies);
                                               // echo "exist ". $existHobbies;
                                                foreach($hobbies as $key => $value){ ?>
                                            <span> <?php echo $value; ?></span>
                                            <?php } ?>
                                            <?php //$myHobbies = $hobbies; $hobbies = implode(",", $hobbies) ?>
                                            <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" id="hobbies1" value="" />
                                    </div>
                                    <!-- <input type="text" class="form-control"/> -->
                                    <!-- <span><i class="fa fa-plus text-blue add-hobbies"></i></span> -->
                                </div>
                                <input type="hidden" id="hobbiesMain" value="" name="myHobbies" />
                                <input type="hidden" id="hobbiesAlreadyExist" value="<?php if(isset($existHobbies) && !empty($existHobbies)){ echo $existHobbies; } ?>" name="hobbiesAlreadyExist" />
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="row">
                                <label>
                                    Change Profile Pic
                                </label>
                                <div class="innerMT profile-img-right-panel" id="profilePicImg">
                                    <div id="uploadImg">
                                        <img id="myImage" class="img-responsive profile-detail-img profile-img-size" src="<?php echo $imgSrc; ?>" />
                                    </div>

                                    <input type="hidden" id="get-profile-pic-name" class="picImg" value="<?php echo $profilePic;  ?>" />
                                </div>
                                <!-- Change pic div -->
                                <div class="col-xs-12 padding-none inner-3x innerMT">
                                    <div class="">
                                        <div class="fileUpload image-form-icon">
                                            <span><i class="fa fa-folder text-blue pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
                                            <span class="browse-span">Browse</span>
                                            <input type="file" class="upload picImg" name="fileToUpload" id="fileToUpload"  value="<?php  echo $profilePic; ?>"  />
                                        </div>
                                        <!-- upload pic from file -->
                                    </div>
                                </div>
                                <div class="col-xs-12 padding-none innerMT inner-8x">
                                    <div class="row  innerMT inner-8x">
                                        <div class="col-xs-6 text-center innerMT">
                                            <input type="button" id="save-profile-btn" class="btn-social-wid-auto btn-xs" value="Save Changes" onclick="uploadProfile();" />
                                        </div>
                                        <!-- <div class="col-xs-6 text-center">
                                                                    <input type="button" class="btn-social-wid-auto-dark btn-xs" value="Reset" />
                                                                </div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<style>
    .mandatory{
        color:red;
    }

    .mendatoryFailureMsg{
        color: red;
        font-size:12px;
    }

</style>
