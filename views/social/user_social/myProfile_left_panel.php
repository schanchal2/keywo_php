
<div class="social-left-panel" id="profileLeftPanel">
    <div class="">
        <?php


    /*    if(isset($profilePic) && !empty($profilePic)){



            global $cdnSocialUrl;
            global $rootUrlImages;

            $extensionUP  = pathinfo($profilePic, PATHINFO_EXTENSION);
            //CDN image path
            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $profilePic . '.232x232.' . $extensionUP;

            //server image path
            $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$profilePic;

            // check for image is available on CDN
            $file = $imageFileOfCDNUP;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $imgSrc = $imageFileOfLocalUP;
            } else {
                $imgSrc = $imageFileOfCDNUP;
            }

            echo "profile ".$imgSrc; die;
        }else{
            $imgSrc = $rootUrlImages."default_profile.jpg";
        }*/

        if(isset($profilePic) && !empty($profilePic)){
            if($profilePic == $rootUrlImages.'default_profile.jpg'){

                $imgSrc = $rootUrlImages."default_profile.jpg";

            }else{

                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($profilePic, PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $profilePic . '.232x232.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$profilePic;

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }

            }
        }


        ?>
        <img class="img-responsive" src="<?php echo $imgSrc; ?>" />
        <div class="card social-card clearfix innerTB all-box-shadow name-accounthandle-card">
            <div class="col-xs-12">
                <div class="ellipses">
                    <?php  echo $_SESSION["first_name"]." ".$_SESSION["last_name"]; ?>
                </div>
                <div class="">
                    <div class="account-handle-name">
                        <?php echo "@".$accountHandle;  ?>
                    </div>
                </div>
            </div>
            <!-- <div class="col-xs-6">
            <input type="button" class="btn-social-wid-auto pull-right btn-xs" value="Edit Profile" />
        </div>-->
        </div>
    </div>
    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4>Settings and Preferences</h4>
                </div>
            </div>
        </div>
        <div class="settings-preference-module">
            <div class="margin-none">
                <div id="userProfileSettingDiv" class="social-user-setting-name border-bottom acivity-tab-display active">
                    <a>Profile</a>
                </div>
                <div id="changePassword" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a>Change Password</a>
                </div>
                <!--<div class="social-user-setting-name border-bottom">
                    <a>Notification Preferences</a>
                </div>-->
            </div>
        </div>
    </div>
    <div class="card left-panel-modules inner-2x innerMT">
        <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
                <div class="col-xs-12">
                    <h4>Activity Log</h4>
                </div>
            </div>
        </div>
        <div class="settings-preference-module">
            <div class="margin-none">
                <div id="likeDislikeActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="likeDislikeActivity">Likes <span id = "likeDislikeActivityCount">(0)</span>
                        <span id = "likeDislikeActivityDivLoader" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
                <div id="commentActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="commentActivity">Comments <span id = "commentActivityCount">(0)</span>
                        <span id = "commentActivityDivLoader" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
                <div id="hiddenPostActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="hiddenPostActivity">Hidden Post <span id = "hiddenPostActivityCount">(0)</span>
                        <span id = "hiddenPostActivityDivLoader" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
                <div id="bookmarkPostActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="bookmarkPostActivity">Post Bookmarks <span id = "bookmarkPostActivityCount">(0)</span>
                        <span id = "bookmarkPostActivityDivLoder" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
                <div id="followedPeopleActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="followedPeopleActivity">Followed People <span id = "followedPeopleActivityCount">(0)</span>
                        <span id = "followedPeopleActivityDivLoder" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
                <div id="followedKeywordActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
                    <a class="clearfix" id="followedKeywordActivity">Followed Keywords <span id = "followedKeywordActivityCount">(0)</span>
                        <span id = "followedKeywordActivityDivLoder" class = "pull-right" style = "display:none;"><img height="20" src = "<?php echo $rootUrl;?>images/login_loader.gif"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>