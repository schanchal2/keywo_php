<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-7x innerT UA6">
        <div class="container padding-none row-10">
            <div class="row">
                <!--==================================
            =            copied from [feature/sn_integration]"views/social/likes_dislikes_html"            =
            ===================================-->
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                        <div class="like-unlike-user-profile-">
                            <img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png" />
                            <div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
                                <div class="col-xs-6 padding-none">
                                    <div class="col-xs-12 padding-none">
                                        Lorem
                                    </div>
                                    <div class="col-xs-12 padding-none">
                                        <span class="account-handle-name">@loremi</span>
                                    </div>
                                </div>
                                <div class="col-xs-6 padding-none innerMT">
                                    <input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile" />
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Settings and Preferences</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Profile</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom active">
                                        <a>Change Password</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Notification Preferences</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Two Factor (2FA)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Activity Log</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Likes / Dislikes</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Comments <span>(2423)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Post Bookmarks <span>(2443)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed People <span>(2723)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed Keywords <span>(24)</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <!--====  End of copied from [feature/sn_integration]"likes_dislikes_html"  ====-->
                <div class="col-xs-9">
                    <div class="social-center-panel">
                        <div class="row">
                            <div class="col-xs-12 innerB">
                                <div class="profile-heading">
                                    <h4 class="text-color-Text-Primary-Blue margin-none f-sz20">Settings and Preferences</h4> :
                                    <h5 class=" margin-none  f-sz15">Change Password</h5>
                                </div>
                                <!-- profile heading -->
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <div class="card bg-white border-all innerAll half text-color-Light-Black"> -->
                            <div class="bg-white border-all clearfix innerAll  text-color-Light-Black">
                                <!-- <div class="card social-card all-box-shadow inner-2x innerMT clearfix"> -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <form class="form-horizontal">
                                            <div class="form-group">
                                                <div class=" half innerMLR">
                                                    <label for="inputPassword3" class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span class="pull-left">Old Password</span></label>
                                                    <div class="col userInput">
                                                        <div class="input-group text-color-Gray materialize bdrB">
                                                            <input placeholder="" type="Password" class="form-control text-color-Text-Primary-Blue padding-left-none" value="******">
                                                            <span class="input-group-addon bg-white text-color-grayscale-be"> <i class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                        </div>
                                                    </div>
                                                    <!-- <span class="help-block">Password can not be less than 8 characters</span> -->
                                                    <label for="inputPassword3" class="col control-label f-sz12 help-block fw-100"><div class="pull-left text-color-Red"> </div></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class=" half innerMLR">
                                                    <label for="inputPassword3" class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span class="pull-left">New Password</span></label>
                                                    <div class="col userInput">
                                                        <div class="input-group text-color-Gray materialize bdrB">
                                                            <input placeholder="" type="Password" class="form-control text-color-Text-Primary-Blue padding-left-none" value="******">
                                                            <span class="input-group-addon bg-white text-color-grayscale-be"> <i class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                        </div>
                                                    </div>
                                                    <label for="inputPassword3" class="col control-label f-sz12 help-block fw-100"><div class="pull-left text-color-Red"> Password can not be less than 8 characters</div></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class=" half innerMLR">
                                                    <label for="inputPassword3" class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span class="pull-left">Confirm Password </span></label>
                                                    <div class="col userInput">
                                                        <div class="input-group text-color-Gray materialize bdrB">
                                                            <input placeholder="" type="Password" class="form-control text-color-Text-Primary-Blue padding-left-none" value="******">
                                                            <span class="input-group-addon bg-white text-color-grayscale-be"> <i class="fa  fa-lock  text-color-Darkest-Gray"></i> </span>
                                                        </div>
                                                    </div>
                                                    <label for="inputPassword3" class="col control-label f-sz12 help-block fw-100"><div class="pull-left text-color-Red"></div></label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class=" half innerMLR">
                                                    <label for="inputPassword3" class="col control-label text-left fw-100 text-color-grayscale-6 f-sz15"><span class="pull-left">Enter Authentication Code (2FA)</span></label>
                                                    <div class="col userInput">
                                                        <div class="text-color-Gray materialize bdrB">
                                                            <input placeholder="" type="text" class="form-control text-color-Text-Primary-Blue padding-left-none" value="">
                                                        </div>
                                                    </div>
                                                    <label for="inputPassword3" class="col control-label f-sz12 help-block fw-100"><div class="pull-left text-color-Red"></div></label>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="clearfix ">
                                            <button type="button" class="btn pull-right  btn-trading-dark"> &nbsp; Cancel &nbsp; </button>
                                            <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto "> &nbsp; Save Changes &nbsp; </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-center-panel  -->
                </div>
                <!-- col-xs-6 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../../layout/social_footer.php'); ?>
