<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <main class="social-main-container inner-7x innerT">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <div class="social-left-panel">
                        <div class="">
                            <img class="img-responsive" src="<?php echo $rootUrlImages?>a_girl.jpg" />
                            <div class="card social-card clearfix innerTB all-box-shadow name-accounthandle-card">
                                <div class="col-xs-6">
                                    <div class="">
                                        Lorem
                                    </div>
                                    <div class="">
                                        <div class="account-handle-name">@loremi</div>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <input type="button" class="btn-social-wid-auto pull-right btn-xs" value="Edit Profile" />
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Settings and Preferences</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Profile</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Change Password</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Notification Preferences</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Two Factor (2FA)</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card left-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray left-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4>Activity Log</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="settings-preference-module">
                                <div class="margin-none">
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Likes / Dislikes</a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Comments <span>(2423)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Post Bookmarks <span>(2443)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed People <span>(2723)</span></a>
                                    </div>
                                    <div class="social-user-setting-name border-bottom">
                                        <a>Followed Keywords <span>(24)</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <div class="col-xs-9 social-center-panel">
                    <div class="">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="profile-heading">
                                    <h4 class="text-blue margin-none">Settings and Preferences</h4> :
                                    <h5 class=" margin-none">Profile</h5>
                                </div>
                                <!-- profile heading -->
                            </div>
                        </div>
                        <!-- heading row -->
                        <div class="card social-card all-box-shadow  innerMT clearfix">
                            <div class="">
                                <!-- Profile section -->
                                <div class="">
                                    <div class="col-xs-12 border-bottom">
                                        <div class="personal-information innerTB">
                                            <h4 class="text-blue margin-bottom-none">
                      Personal Information
                    </h4>
                                        </div>
                                        <!-- Title of personal card -->
                                    </div>
                                    <div class="col-xs-12 innerMT">
                                        <div class=" row personal-detail-form">
                                            <form class="clearfix">
                                                <div class="col-xs-8 form-group padding-none">
                                                    <div class="row">
                                                        <!-- first name and last name -->
                                                        <div class="col-xs-6">
                                                            <label>
                                                                First Name
                                                            </label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <label>
                                                                Last Name
                                                            </label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <!-- first name and last name -->
                                                        <div class="col-xs-6 innerMT">
                                                            <label class="innerB">
                                                                Mobile Number
                                                            </label>
                                                            <!-- <input type="number" class="form-control country-code" value="+91" placeholder="+91"/> -->
                                                            <label class="sr-only" for="inlineFormInputGroup">Username</label>
                                                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                                                <div class="input-group-addon country-code-box">
                                                                    <select>
                                                                        <option value="">+1</option>
                                                                        <option value="">+4</option>
                                                                        <option value="">+323</option>
                                                                        <option value="">+2</option>
                                                                    </select>
                                                                </div>
                                                                <input type="number" class="form-control" id="" placeholder="Contact No.">
                                                            </div>
                                                            <!-- <input type="number" class="form-control contact-number"/> -->
                                                        </div>
                                                        <div class="col-xs-6 innerMTB">
                                                            <label></label>
                                                            <div class="radio radio-primary radio-div">
                                                                <input type="radio" name="gender" id="male" value="male" checked="true">
                                                                <label for="male">
                                                                    Male
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-primary radio-div">
                                                                <input type="radio" name="gender" id="female" value="female">
                                                                <label for="female">
                                                                    Female
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-primary radio-div">
                                                                <input type="radio" name="gender" id="other" value="other">
                                                                <label for="other">
                                                                    Other
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <!-- gender selection -->
                                                        <div class="col-xs-6 innerMT">
                                                            <textarea placeholder="Address 1" class="form-control"></textarea>
                                                        </div>
                                                        <div class="col-xs-6 innerMT">
                                                            <textarea placeholder="Address 2" class="form-control"></textarea>
                                                        </div>
                                                        <!-- Textarea for addresses -->
                                                        <div class="col-xs-6 innerMT">
                                                            <label>
                                                                Zip Code
                                                            </label>
                                                            <input type="text" class="form-control" />
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <label for=""></label>
                                                            <br />
                                                            <select class="select-input chosen-select-deselect" data-placeholder="Select City" name="city">
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">45</option>
                                                                <option value="">6</option>
                                                            </select>
                                                        </div>
                                                        <!-- Zip code and city -->
                                                        <div class="col-xs-6 clearfix">
                                                            <label for=""></label>
                                                            <br />
                                                            <select class="select-input chosen-select-deselect" data-placeholder="Select State" name="state">
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">45</option>
                                                                <option value="">6</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-6 clearfix">
                                                            <label for=""></label>
                                                            <br />
                                                            <select class="select-input chosen-select-deselect" data-placeholder="India" name="country">
                                                                <option value="">1</option>
                                                                <option value="">2</option>
                                                                <option value="">3</option>
                                                                <option value="">4</option>
                                                                <option value="">45</option>
                                                                <option value="">6</option>
                                                            </select>
                                                        </div>
                                                        <!-- State and Country -->
                                                        <div class="col-xs-6 innerMT">
                                                            <label>
                                                                Email Id
                                                            </label>
                                                            <input type="email" class="form-control" />
                                                        </div>
                                                        <div class="col-xs-6 innerMT">
                                                            <label>
                                                                Alternate Email Id
                                                            </label>
                                                            <input type="email" class="form-control" />
                                                        </div>
                                                        <!-- for email addresses -->
                                                        <div class="col-xs-12 innerMT">
                                                            <label>
                                                                Hobbies
                                                            </label>
                                                            <div id="tags" class="border-bottom" style="word-break: break-word;">
                                                                <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" />
                                                            </div>
                                                            <!-- <input type="text" class="form-control"/> -->
                                                            <!-- <span><i class="fa fa-plus text-blue add-hobbies"></i></span> -->
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4">
                                                    <div class="row">
                                                        <label>
                                                            Change Profile Pic
                                                        </label>
                                                        <div class="innerMT profile-img-right-panel">
                                                            <img class="img-responsive profile-detail-img" src="<?php echo $rootUrlImages?>a_girl.jpg" />
                                                        </div>
                                                        <!-- Change pic div -->
                                                        <div class="col-xs-12 padding-none inner-3x innerMT">
                                                            <div class="">
                                                             <!--    <div class="fileUpload image-form-icon">
                                                                    <span><i class="fa fa-folder text-blue pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
                                                                    <span class="browse-span">Browse</span>
                                                                    <input onchange="readURL(event)" class="uploadImageCrop upload" type="file" class="upload" name="fileToUpload" />
                                                                </div> -->
                                                                <!-- upload pic from file -->
                                                            </div>
                                                            <!-- <div class="col-xs-6 padding-none text-right">
                                    <div class="fileUpload image-form-icon">
                                                    <span><i class="fa fa-camera text-blue margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
                                                    <span class="browse-span">Take Photo</span>
                                                    <input type="file" class="upload" name="fileToUpload" />
                                                </div>
                                    //upload pic from camera
                                  </div> -->
                                                        </div>
                                                        <div class="col-xs-12 padding-none inner-3x innerMT calendar-section">
                                                            <label>Date Of Birth</label>
                                                            <input type="text" class="form-control date-input" placeholder="DD/MM/YYYY" />
                                                            <div class="form-group">
                                                                <div class="">
                                                                    <input type="text" class="form-control date-of-birth" value="DD/MM/YYYY" placeholder="" id="datepicker">
                                                                    <span class="text-Blue calendar"> <i class="fa fa-calendar" id=""></i> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 padding-none innerMT innerMB">
                                                            <div class="row innerMT">
                                                                <div class="col-xs-6 text-center">
                                                                    <input type="button" class="btn-social-wid-auto btn-xs" value="Save Changes" />
                                                                </div>
                                                                <div class="col-xs-6 text-center">
                                                                    <input type="button" class="btn-social-wid-auto-dark btn-xs" value="Reset" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- personal info form -->
                        </div>
                        <!-- Personal detail row -->
                        <div class="card social-card all-box-shadow inner-2x innerMT innerMB clearfix">
                            <div class="">
                                <!-- education detail section -->
                                <div class="">
                                    <div class="col-xs-12 border-bottom">
                                        <div class="education-information-heading innerTB">
                                            <h4 class="text-blue margin-bottom-none">
                                Educational Information
                              </h4>
                                        </div>
                                        <!-- Title of education card -->
                                    </div>
                                    <div class="innerMT">
                                        <div class="educational-information-form position-relative">
                                            <form class="clearfix" class="educational-information">
                                                <div class="col-xs-12 innerMT high-school-wrapper-div">
                                                    <label>
                                                        High School
                                                    </label>
                                                    <div class="add-to-btn">
                                                        <span><i class="fa fa-plus text-blue"></i></span>
                                                        <span class="add-btn">Add</span>
                                                    </div>
                                                </div>
                                                <div class="high-school-module">
                                                    <div id="highSchool_0" class="col-xs-12 inner-2x innerB innerMT">
                                                        <input type="text" class="school-input form-control" placeholder="Which school did you attend?" />
                                                        <input type="number" class="years year-start form-control" />
                                                        <div class="years-to">
                                                            To
                                                        </div>
                                                        <input type="number" class="years year-end form-control" />
                                                        <span class="time-period">( Time period in years )</span>
                                                    </div>
                                                </div>
                                                <!-- High school section  -->
                                                <div class="col-xs-12 university-section">
                                                    <label>
                                                        University
                                                    </label>
                                                    <div class="university-add-to-btn">
                                                        <span><i class="fa fa-plus text-blue"></i></span>
                                                        <span class="add-btn">Add</span>
                                                    </div>
                                                    <div class="university-selections">
                                                        <div class="innerMT">
                                                            <div class="radio radio-primary radio-div">
                                                                <input type="radio" name="universityRadio" id="university" value="university">
                                                                <label for="university">
                                                                    University
                                                                </label>
                                                            </div>
                                                            <div class="radio radio-primary radio-div">
                                                                <input type="radio" name="universityRadio" id="universityGrad" value="universityGrad">
                                                                <label for="universityGrad">
                                                                    University (postgraduate)
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="university-module">
                                                        <div id="university_0" class="padding-left-none inner-2x innerTB" style="position:relative;">
                                                            <input type="text" class="university-input-name form-control" placeholder="Which university did you attend?" />
                                                            <input type="text" class="university-input-specification form-control" placeholder="Specification" />
                                                            <input type="number" class="years year-start form-control" />
                                                            <div class="years-to">
                                                                To
                                                            </div>
                                                            <input type="number" class="years year-end form-control" />
                                                            <span class="time-period">( Time period in years )</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- university section -->
                                                <div class="col-xs-8 innerMB">
                                                    <label>
                                                        Co-curricular Activities
                                                    </label>
                                                    <div id="tagsCurricular" class="border-bottom" style="word-break: break-word;">
                                                        <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" />
                                                    </div>
                                                    <!-- <input type="text" class="form-control"/> -->
                                                </div>
                                                <div class="col-xs-4">
                                                    <label for=""></label>
                                                    <br />
                                                    <div class="row">
                                                        <div class="col-xs-6 text-center padding-none">
                                                            <input type="button" class="btn-social-wid-auto btn-xs" value="Save Changes" />
                                                        </div>
                                                        <div class="col-xs-6 text-center padding-none">
                                                            <input type="button" class="btn-social-wid-auto-dark btn-xs" value="Reset" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Education detail -->
                        <div class="card social-card all-box-shadow inner-2x innerMB clearfix">
                            <div class="">
                                <!-- work information section -->
                                <div class="">
                                    <div class="col-xs-12 border-bottom">
                                        <div class="work-information-heading innerTB">
                                            <h4 class="text-blue margin-bottom-none">
                                Work Information
                              </h4>
                                        </div>
                                        <!-- Title of work card -->
                                    </div>
                                    <div class="innerMT">
                                        <div class="work-information-form">
                                            <form class="clearfix" class="work-information" onSubmit="return false;">
                                                <div class="work-detail">
                                                    <div class="col-xs-12 innerMT">
                                                        <label>
                                                            Company Name & Designation
                                                        </label>
                                                        <div class="company-add-to-btn">
                                                            <span><i class="fa fa-plus text-blue"></i></span>
                                                            <span class="add-btn">Add Company</span>
                                                        </div>
                                                    </div>
                                                    <!-- Company name -->
                                                    <div class="work-section">
                                                        <div id="workDetail_0" class="col-xs-12">
                                                            <div class="padding-left-none inner-2x innerT innerB">
                                                                <input type="text" class="company-input-name form-control" placeholder="Company name" />
                                                                <input type="text" class="company-input-designation form-control" placeholder="Add designation" />
                                                                <input type="number" class="years year-start form-control" />
                                                                <div class="years-to">
                                                                    To
                                                                </div>
                                                                <input type="number" class="years year-end form-control" />
                                                                <span class="time-period">( In Years )</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- university section -->
                                                <div class="inner-2x innerMT">
                                                    <div class="col-xs-8 innerMB">
                                                        <label>
                                                            Professional Skills
                                                        </label>
                                                        <div id="tagsProfessional" class="border-bottom" style="word-break: break-word;">
                                                            <input type="text" class="form-control tag-input" style="width: 100px;border: none;display: inline-block;" />
                                                        </div>
                                                        <!-- <input type="text" class="form-control"/> -->
                                                    </div>
                                                    <div class="col-xs-4 submit-buttons innerMB">
                                                        <div class="row">
                                                            <div class="col-xs-6 text-center padding-none">
                                                                <input type="button" class="btn-social-wid-auto" value="Save Changes" />
                                                            </div>
                                                            <div class="col-xs-6 text-center padding-none">
                                                                <input type="button" class="btn-social-wid-auto-dark" value="Reset" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Work information -->
                    </div>
                    <!-- social-center-panel  -->
                </div>
                <!-- col-xs-9 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <div class="modal fade" id="cropModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Profile Pic</h5>
                    <!--       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
                </div>
                <div class="modal-body">
                    <style type="text/css">
                    .cropit-preview {
                        width: 200px;
                        height: 200px;
                        margin-left: calc(50% - 100px);
                    }
                    .cropit-image-zoom-input{
                        margin: 10px 0;
                    }
                    </style>
                    <!--  <div class="image-croper">          </div> -->
                    <div id="image-cropper">
                        <!-- This is where the preview image is displayed -->
                        <div class="cropit-preview"     ></div>
                        <!-- This range input controls zoom -->
                        <!-- You can add additional elements here, e.g. the image icons -->
                        <input type="range" class="cropit-image-zoom-input" />
                        <!-- This is where user selects new image -->
                        <input type="file" class="cropit-image-input" />
                        <!-- The cropit- classes above are needed
               so cropit can identify these elements -->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-saveCropedImage">Save changes</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <!-- Blog Form ends-->
        <?php include('../../layout/social_footer.php'); ?>
        <!-- <script src="<?php //echo $rootUrl; ?>frontend_libraries/jquery.cropbox.js"></script> -->
        <script src="<?php echo $rootUrl; ?>frontend_libraries/jquery.cropit/jquery.cropit.js"></script>
        <script type="text/javascript">
        $('.profile-detail-img').on('click', function(event) {
            event.preventDefault();
            $("#cropModal").modal({
                backdrop: 'static',
                // keyboard: false,
                show: true
            }).on('shown.bs.modal', function(e) {
                console.log($(this)); 
                $('#image-cropper').cropit({
                    // imageBackground: true,
                    // imageBackgroundBorderWidth: 15
                    imageState: {
                        // src: 'http://lorempixel.com/500/400/'
                        src: $(".profile-detail-img").attr('src')
                    }
                });
                $(this).find('.modal-footer').on('click', '.btn-saveCropedImage', function(event) {
                    event.preventDefault();
                    /* Act on the event */
                    var imageData = $('#image-cropper').cropit('export');
                     $(".profile-detail-img").attr('src', imageData);
                     $("#cropModal").modal('hide');
                });
                // $('string/element/array/function/jQuery object/string, context')
            });
        });
        // $('#image-cropper').cropit();

        // .find('img').attr('src',  $(".profile-detail-img").attr('src'));
        </script>
        <script>
        $(function() {
            $('#datepicker').datepicker({
                format: 'dd/mm/yyyy',
                autoclose: true,
                keyboardNavigation: true,
                daysOfWeekDisabled: [0]
            });
        });

        function readURL(event) {
            $("#cropModal").modal({
                backdrop: 'static',
                // keyboard: false,
                show: true
            }).on('shown.bs.modal', function(e) {
                var getImagePath = URL.createObjectURL(event.target.files[0]);
                console.log(getImagePath);
                $(".image-croper").append('<img src="' + getImagePath + '">');
                // var options =
                //     {
                //         thumbBox: '.cropFrame',
                //         imgSrc: getImagePath
                //     }
                //     $(".image-croper").cropbox({
                //         thumbBox: '.cropFrame',
                //         imgSrc: getImagePath,
                //         width: 200,
                // height: 200
                //     })
            });
        }
        // $(".uploadImageCrop").change(function(){
        //     var uploadedImg = $(this).val();
        //     $("#cropModal").modal({
        //         backdrop: 'static',
        //         keyboard: false,
        //         show:true
        //     }).on('shown.bs.modal', function (e){
        //         $(".image-croper").append('<img src="'+uploadedImg+'">')
        //     });
        // })
        $("#datepicker").change(function() {
            $(".date-input").val($(this).val());
        });


        $(".chosen-select-deselect").chosen({
            enable_split_word_search: false,
            width: "100%"
        });
        $(".chosen-search").keyup(function() {
            if ($(".chosen-results").children().hasClass("no-results")) {
                $(".no-results").text("No results match");
            }
        });

        $(document).ready(function() {
            var x = 0,
                y = 0,
                z = 0; //initlal text box count
            $(".add-to-btn").click(function(e) { //on add input button click
                var schoolVal = $("#highSchool_0").find(".school-input").val();
                var startYearVal = $("#highSchool_0").find(".year-start").val();
                var endYearVal = $("#highSchool_0").find(".year-end").val();
                if ($("#highSchool_0").find(".school-input").val() != "") {
                    selecteddata = '';
                    $("#highSchool_0").find("input").val("");
                    if (x < 10) {
                        x++;
                        selecteddata += '<div id="highSchool_' + x + '" class="col-xs-12 innerB innerMT">';
                        selecteddata += '<input type="text" class="school-input form-control" placeholder="Which school did you attend?" value="' + schoolVal + '"/>';
                        selecteddata += '<input type="number" class="years year-start form-control" value="' + startYearVal + '" />';
                        selecteddata += '<div class="years-to">To</div>';
                        selecteddata += '<input type="number" class="years year-end  form-control" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( Time period in years )</span>';
                        selecteddata += '<span data-id="highSchool_' + x + '"class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>';
                        selecteddata += '</div>';
                        $(".high-school-module").append(selecteddata)
                        $("#highSchool_0").find(".school-input").focus();
                    }

                }
            });
            $(".university-add-to-btn").click(function(e) { //on add input button click
                var universityVal = $("#university_0").find(".university-input-name ").val();
                var universitySpecVal = $("#university_0").find(".university-input-specification ").val();
                var startYearVal = $("#university_0").find(".year-start").val();
                var endYearVal = $("#university_0").find(".year-end").val();
                if ($("#university_" + y).find("input").val() != "") {
                    selecteddata = '';
                    $("#university_0").find("input").val("");
                    if (y < 10) {
                        y++;
                        selecteddata += '<div id="university_' + y + '" class="padding-left-none innerT" style="position:relative;">';
                        selecteddata += '<input type="text" class="university-input-name form-control" placeholder="Which university did you attend?" value="' + universityVal + '" />';
                        selecteddata += '<input type="text" class="university-input-specification form-control" placeholder = "Specification" value="' + universitySpecVal + '"/>';
                        selecteddata += '<input type="number" class="years year-start form-control" value"' + startYearVal + '"/>';
                        selecteddata += '<div class="years-to"> To </div>';
                        selecteddata += '<input type="number" class="years year-end form-control" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( Time period in years )</span>';
                        selecteddata += '<span data-id="university_' + y + '" class="text-color-Gray remove-input pull-right innerMR remove_field"><i class="fa fa-trash"></i></span>'
                        selecteddata += '</div>';
                        $(".university-module").append(selecteddata)
                        $("#university_0").find(".university-input-name").focus();
                    }
                }

            });
            $(".company-add-to-btn").click(function(e) { //on add input button click
                var workVal = $("#workDetail_0").find(".company-input-name ").val();
                var designationVal = $("#workDetail_0").find(".company-input-designation ").val();
                var startYearVal = $("#workDetail_0").find(".year-start").val();
                var endYearVal = $("#workDetail_0").find(".year-end").val();
                if ($("#workDetail_" + z).find("input").val() != "") {
                    selecteddata = '';
                    $("#workDetail_0").find("input").val("");
                    if (z < 10) {
                        z++;
                        selecteddata += '<div id="workDetail_' + z + '" class="col-xs-12">';
                        selecteddata += '<div class="padding-left-none  innerT">';
                        selecteddata += '<input type="text" class="company-input-name form-control" placeholder="Company name" value="' + workVal + '"/>';
                        selecteddata += '<input type="text" class="company-input-designation form-control" placeholder="Add designation" value="' + designationVal + '"/>';
                        selecteddata += '<input type="number" class="years year-start form-control" value="' + startYearVal + '"/>';
                        selecteddata += '<div class="years-to"> To </div>';
                        selecteddata += '<input type="number" class="years year-end form-control" value="' + endYearVal + '"/>';
                        selecteddata += '<span class="time-period">( In Years )</span>';
                        selecteddata += '<span data-id="workDetail_' + z + '" class="text-color-Gray remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-trash"></i></span>';
                        selecteddata += '</div>';
                        selecteddata += '</div>';
                        $(".work-section").append(selecteddata)
                        $("#workDetail_0").find(".company-input-name").focus();
                        // $("#workDetail_0").clone().appendTo(".work-section").attr("id","workDetail_"+z);
                        // $("#workDetail_"+z).append('<span data-id="workDetail_'+z+'" class="text-color-Gray remove-input pull-right innerMT innerMR remove_field">'+
                        // '<i class="fa fa-trash"></i></span></div>');
                        // $("#workDetail_"+z).find("input").val("");
                    }
                }
            });
            $(".high-school-module").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                x--;
            });
            $(".university-module").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                y--;
            });
            $(".work-section").on("click", ".remove_field", function(e) {
                e.preventDefault();
                $("#" + $(this).attr("data-id")).remove();
                z--;
            });

        });
        $(function() { // DOM ready

            // ::: TAGS BOX

            $("#tags input").on({
                keyup: function(ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });
            $('#tags').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });

            $("#tagsCurricular input").on({
                keyup: function(ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });
            $('#tagsCurricular').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });
            $("#tagsProfessional input").on({
                keyup: function(ev) {
                    // if: comma|enter (delimit more keyCodes with | pipe)
                    if (/(188|13|32)/.test(ev.which)) {
                        var txt = this.value.replace(/[^a-z0-9\+\-\.\#]/ig, ''); // allowed characters
                        if (txt) $("<span/>", {
                            text: txt.toLowerCase(),
                            insertBefore: this
                        });
                        this.value = "";
                    }
                }
            });
            $('#tagsProfessional').on('click', 'span', function() {
                //if(confirm("Remove "+ $(this).text() +"?"))
                $(this).remove();
            });
        });
        </script>
