<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../../layout/header.php");

		$email      = $_SESSION["email"];

?>
<main class="social-main-container inner-7x innerT" >
<div class="container">
	<div class="col-xs-3">
		<div class="social-left-panel">
      <div class="like-unlike-user-profile-">
				<img class="img-responsive" src="<?php echo $rootUrlImages?>a_girl.jpg"/>
				<div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
					<div class="col-xs-6 padding-none">
						<div class="col-xs-12 padding-none">
							Lorem
						</div>
						<div class="col-xs-12 padding-none">
							<div class="account-handle-name">@loremi</div>
						</div>
					</div>
					<div class="col-xs-6 padding-none innerMT">
						<input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile"/>
					</div>
				</div>
      </div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Settings and Preferences</h4>
						</div>
					</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Profile</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Change Password</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Notification Preferences</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Two Factor (2FA)</a>
						</div>
					</div>
				</div>
			</div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Activity Log</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Likes / Dislikes</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Comments <span>(2423)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Post Bookmarks <span>(2443)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed People <span>(2723)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed Keywords <span>(24)</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-6">
    <div class="social-center-panel">
      <div class="row">
        <div class="col-xs-12">
          <div class="profile-heading">
              <h4 class="text-blue margin-none">Activity Log</h4> : <h5 class="margin-none">Post Bookmark</h5>
          </div>
          <!-- profile heading -->
        </div>
      </div>
      <!-- heading row -->

			<div class="day-activity">
					<div class="row">
						<div class="col-xs-12">
							<div class="profile-heading">
									<h5 class=" inner-2x innerT margin-bottom-none">Today</h5>
							</div>
						</div>
					</div>
			</div>
			<!--============= Social Timeline Image ======================-->
			<div class="social-timeline-image innerMTB" >
				<div class="social-card card half innerAll" id="social-timeline-image">
						<div class="">
							<div class="row half innerTB">
								<div class="col-xs-8">
									<div class="social-timeline-profile-pic pull-left">
										<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
									</div>
									<div class="social-timeline-profile-details pull-left innerL">
										<div class="social-user-link-name">
											<a href="#">
												tom hanks
											</a>
										</div>
										<!---->
										<div class="social-user-link">
											<a href="#">
												@tomhankennew
											</a>
										</div>
										<!--social-user-link-->
									</div>
									<!--social-timeline-profile-details-->
								</div>
								<div class="col-xs-4 social-post-options">
									<div class="dropdown pull-right">
										<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="fa fa-chevron-down"></span>
										</a>
										<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
										<li>
											<a href="#">
												<i class="fa fa-bookmark"></i>
												Save Post
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-eye-slash"></i>
												Hide Post
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-file-text"></i>
												Report Post
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-user-times"></i>
												Unfollow User
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-key"></i>
												Unfollow #<span>keyword</span>
											</a>
										</li>
										<li>
											<a href="#">
												<i class="fa fa-user-times"></i>
												Unfollow #<span>Account</span>
											</a>
										</li>
									</ul>
										<span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
									</div>
									<div class="pull-right social-post-time-container">
										<span class="social-post-time"><span>3</span> hrs</span>
									</div>
								</div>
							</div>
							<!--row-->
						</div>
						<!--social-timeline-details-->

						<div class="social-timeline-keywords-details">
							<div class="row">
								<div class="col-xs-12">
									<a href="#" class="social-keywords-tags">#bitcoin</a>
									<a href="#" class="social-keywords-tags half innerL">#India</a>
								</div>
							</div>
						</div>
						<!--social-timeline-keywords-details-->

						<!--social-timeline-user-message-->
						<div class="social-timeline-content-message">
							<div class="row">
								<div class="col-xs-12">
									<p class="innerMB">
										The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
										Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
									</p>
								</div>
							</div>
						</div>
						<!--social-timeline-tags-details-->

						<!--  content-image-->
						<div class="social-timeline-content-image">
							<div class="row">
								<div class="col-xs-12">
								<a  data-toggle="modal" href="#status_image_modal">
									<img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive" />
								</a>
								</div>
							</div>
						</div>
						<!--social-timeline-content-image-->

						<div class="social-timeline-earning-comments-view innerT">
							<div class="row">
								<div class="col-xs-4">
									<div>
										<label>Earning : </label>
										<span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
									</div>
								</div>
								<div class="col-xs-8">
									<div class="">
										<ul class="list-inline pull-right margin-bottom-none">
											<li>
												<div>
													<span class="half innerR">40</span>
													<label class="pull-right">Comments</label>
												</div>
											</li>
											<li>
												<div>
													<span class="">456</span>
													<label>Shares</label>
												</div>
											</li>
											<li class="padding-right-none">
												<div>
													<span class="">557k</span>
													<label>Views</label>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<!--social-timeline-earning-comments-view-->

						<!--social-timeline-likes-dislikes-->
						<div class="social-timeline-likes-dislikes">
							<div class="row">
								<div class="col-xs-6">
									<div class="social-likes-dislikes">
										<ul class="list-inline margin-bottom-none">
											<li>
												<div>
													<a>
														<i class="fa fa-thumbs-o-up half innerR"></i>
														<span>770</span>
													</a>
												</div>
											</li>
											<li>
												<div>
													<a>
														<i class="fa fa-thumbs-o-down half innerR"></i>
														<span>770</span>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="social-comments-shares-views">
										<ul class="list-inline pull-right margin-bottom-none">
											<li>
												<div>
													<a>
														<i class="fa fa-comments-o"></i>
														<span>Comments</span>
													</a>
												</div>
											</li>
											<li class="padding-right-none">
												<div>
													<a>
														<i class="fa fa-share-square-o"></i>
														<span>Share</span>
													</a>
												</div>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>

				</div>
				<!--social-card-->
			</div>
			<!--social-timeline-image-->

    </div>
	  <!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

	<div class="col-xs-3">
		<div class="search_bar_post_bookmark">
			<div class="row">
				<div class="col-xs-12">
					<input type="text" class="form-control" placeholder="Search" />
				</div>
			</div>
		</div>
		<div class="card left-panel-modules inner-5x innerMT">
			<div class="bg-light-gray left-panel-modules-head">
				<div class="row margin-none">
					<div class="col-xs-12">
							<h4>Type</h4>
					</div>
				</div>
			</div>
			<div class="settings-preference-module">
				<div class="margin-none">
					<div class="social-user-setting-name border-bottom">
						<a>All</a>
					</div>
					<div class="social-user-setting-name border-bottom">
						<a>Audio</a>
					</div>
					<div class="social-user-setting-name border-bottom">
						<a>Video</a>
					</div>
					<div class="social-user-setting-name border-bottom">
						<a>Blog</a>
					</div>
				</div>
			</div>
		</div>

	</div>
	<!--col-xs-3-->

</div>
<!-- container -->
</main>


<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../../layout/social_footer.php'); ?>
<script>
	$(".chosen-select-deselect").chosen({
	    enable_split_word_search: false,
	    width: "100%"
	});
	$(".chosen-search").keyup(function() {
	    if ($(".chosen-results").children().hasClass("no-results")) {
	        $(".no-results").text("No results match");
	    }
	});
	$(function() {
		$('#datepicker').datepicker({
		    format: 'dd/mm/yyyy',
		    autoclose: true,
		    keyboardNavigation : true ,
		    daysOfWeekDisabled : [0]
		});
	});
	$("#datepicker").change(function(){
		$(".date-input").val($(this).val());
	});




	$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".wrapper-div");//$(".input_fields_wrap"); //Fields wrapper
    //var add_button      = $(".add-to-btn");//$(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(".add-to-btn").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $('.high-school-wrapper-div').append('<div class="col-xs-10 padding-left-none inner-2x innerB">'+
						'<input type="text" class="school-input" placeholder="which school did you attend?"/>'+
						'&nbsp<input type="number" class="years"/><div class="years-to">&nbspTo&nbsp</div>'+
						'<input type="number" class="years"/><span class="time-period">( Time period in year )</span>'+
						'<span class="remove-input pull-right innerMT innerMR remove_field"><i class="fa fa-remove"></i></span></div>'); //add input box
        }
    });
		$(".university-add-to-btn").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $('.university-section').append('<div class="padding-left-none inner-2x innerB" style="position:relative;">'+
						'<input type="text" class="university-input-name" placeholder="which university did you attend?"/>'+
						'&nbsp<input type="text" class="university-input-specification" placeholder="specification"/>'+
						'&nbsp<input type="number" class="years"/><div class="years-to">&nbspTo&nbsp</div><input type="number" class="years"/>'+
						'<span class="time-period">( Time period in year )</span><span class="remove-input pull-right innerMT innerMR remove_field">'+
						'<i class="fa fa-remove"></i></span></div>'); //add input box
        }
    });
		$(".company-add-to-btn").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $('.work-detail').append('<div class="col-xs-12 work-section"><div class="padding-left-none inner-2x innerT innerB">'+
						'<input type="text" class="company-input-name" placeholder="company name"/>'+
						'&nbsp<input type="text" class="company-input-designation" placeholder="add designation"/>'+
						'&nbsp<input type="number" class="years"/><div class="years-to">&nbspTo&nbsp</div><input type="number" class="years"/>'+
						'<span class="time-period">( In Years )</span></div><span class="remove-input pull-right innerMT innerMR remove_field">'+
						'<i class="fa fa-remove"></i></span></div>'); //add input box
        }
    });

    $(".university-section").on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    $(".high-school-wrapper-div").on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
    $(".work-detail").on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
$(function(){ // DOM ready

  // ::: TAGS BOX

  $("#tags input").on({
    keyup : function(ev) {
      // if: comma|enter (delimit more keyCodes with | pipe)
      if(/(188|13|32)/.test(ev.which)) {
				var txt= this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
	      if(txt) $("<span/>",{text:txt.toLowerCase(), insertBefore:this});
	      this.value="";
			}
    }
  });
  $('#tags').on('click', 'span', function() {
    //if(confirm("Remove "+ $(this).text() +"?"))
		$(this).remove();
  });

	$("#tagsCurricular input").on({
		keyup : function(ev) {
			// if: comma|enter (delimit more keyCodes with | pipe)
			if(/(188|13|32)/.test(ev.which)) {
				var txt= this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
				if(txt) $("<span/>",{text:txt.toLowerCase(), insertBefore:this});
				this.value="";
			}
		}
	});
	$('#tagsCurricular').on('click', 'span', function() {
    //if(confirm("Remove "+ $(this).text() +"?"))
		$(this).remove();
  });
	$("#tagsProfessional input").on({
		keyup : function(ev) {
			// if: comma|enter (delimit more keyCodes with | pipe)
			if(/(188|13|32)/.test(ev.which)) {
				var txt= this.value.replace(/[^a-z0-9\+\-\.\#]/ig,''); // allowed characters
				if(txt) $("<span/>",{text:txt.toLowerCase(), insertBefore:this});
				this.value="";
			}
		}
	});
	$('#tagsProfessional').on('click', 'span', function() {
    //if(confirm("Remove "+ $(this).text() +"?"))
		$(this).remove();
  });
});
</script>
