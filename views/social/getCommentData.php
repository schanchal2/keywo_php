<?php

ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
	session_start();
	/* Add Required Files */
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/deviceHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../helpers/imageFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once "../../backend_libraries/xmlProcessor/xmlProcessor.php";



	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		// printArr($_SESSION);
		$userId           = $_SESSION['id'];
		$lastCommentTime  = cleanXSS(urldecode($_POST['lastCommentTime']));
		$postId           = cleanXSS(urldecode($_POST['postId']));
		$postCreationTime = cleanXSS(urldecode($_POST['postCreatedAt']));
		$limit            = cleanXSS(urldecode($_POST['limit']));
		$commentId        = cleanXSS(urldecode($_POST['commentId']));
		$returnArray      = array();
        $appendLoadCOmmentCount = 0;
        $nextPostCount = 0;
		$k = 0;
		$viewMoreFlag = 0;
		// echo "Comment : ".urlencode($commentText);

		$getCommentCall = getCommentData($userId, $postId, $postCreationTime, $lastCommentTime, $limit , $commentId);
		// printArr($getCommentCall);
		if (noError($getCommentCall)) {
			$getCommentCall = $getCommentCall['errMsg'];
			// printArr($getCommentCall);

			//Get blocked array
			$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
			$getBlockedArray = getBlockedUser($targetDirAccountHandler);

			if ($getCommentCall != 'No Result') {
				for ($i = 0; $i < count($getCommentCall); $i++) {
					$mil = $getCommentCall[$i]['editTime'];
					$seconds = $mil / 1000;
                    $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
					$timestamp2 = strtotime($created_at);


	if(isset($getCommentCall[$i]["_id"]) && !empty($getCommentCall[$i]["_id"])) {
			$commentUserId = $getCommentCall[$i]["comment_by"];
			if(isset($getCommentCall[$i]["user_ref"]["profile_pic"]) && !empty($getCommentCall[$i]["user_ref"]["profile_pic"])){
                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($getCommentCall[$i]["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $getCommentCall[$i]["user_ref"]["account_handle"] . '/profile/' . $getCommentCall[$i]["user_ref"]["account_handle"] . '_' . $getCommentCall[$i]["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$getCommentCall[$i]["user_ref"]["account_handle"].'/profile/'.$getCommentCall[$i]["user_ref"]["account_handle"].'_'.$getCommentCall[$i]["user_ref"]["profile_pic"];

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                $imgSrc = $rootUrlImages."default_profile.jpg";
            }
			if(!in_array($commentUserId,$getBlockedArray)){
				$k = $k+1;
				$viewMoreFlag = $viewMoreFlag+1;
				if($k>11){
						break;
				}
				//

				if($limit==30){
					if($k==4){
							break;
					}
				}

	?>
		<input type = "hidden" id = "comment-indivisual-div-<?php echo $getCommentCall[$i]['_id']; ?>"  call-ajax-reply-get-flag = "true">
				<div id = "commented-list-<?php echo $getCommentCall[$i]['_id']; ?>" class="row comments-module-wrapper UI--comments-module-wrapper" comment-user-email = "<?php echo $getCommentCall[$i]['user_ref']['email']; ?>">
					<div class="col col-50">
						<img src="<?php echo $imgSrc; ?>">
					</div>
					<div id = "comment-edit-display-<?php echo $getCommentCall[$i]["_id"]; ?>" class="col-xs-10  padding-none comment-editing-post-<?php echo $postId; ?>">
						<div class="comments-module-comment-block">
						<?php
							if ($_SESSION['id'] == $getCommentCall[$i]['comment_by']) {
				                $commentHref = $rootUrl."views/social/privateTimeline.php";
				            } else {
				               	$commentHref = $rootUrl."views/social/otherTimeline.php?email=".base64_encode($getCommentCall[$i]["user_ref"]["email"]);
				            }
				        ?>
							<a href="<?php echo $commentHref; ?>" class="comments-module-username"><?php echo $getCommentCall[$i]["user_ref"]["first_name"]; ?> <?php echo $getCommentCall[$i]["user_ref"]["last_name"]; ?></a>
							<span id = "Comment-og-text<?php echo $getCommentCall[$i]['_id']; ?>" style = "display:none;"><?php echo $getCommentCall[$i]['comment']; ?></span>

							<?php

                            if(isset( $getCommentCall[$i]['comment_mention'])){
                                $mention                = $getCommentCall[$i]['comment_mention'];
                            }else{
                                $mention                = "";
                            }
								$commnetTextCount       = '';
								$commentText1           = '';
								$commentText2           = '';

								// printArr($mention);
								$getCommentText         = getLinksOnText($getCommentCall[$i]['comment'], $mention, $_SESSION['account_handle']);
								// printArr($getCommentText);
								$commentExtraCount      = $getCommentText['counter'];
								$commentTextWithMention = $getCommentText['text'];
								$commnetTextCount       = strlen( $commentTextWithMention);
								if ($commnetTextCount  >= (600+$commentExtraCount)) {
									$commentText1 = substr($commentTextWithMention,0, (300+$commentExtraCount)).'<a id = "seeMore-load'.$getCommentCall[$i]['_id'].'" class="text-blue" onclick = seeMoreComment("'.$getCommentCall[$i]["_id"].'")> 	..see more</a>';
									$commentText2 = substr($commentTextWithMention,(301+$commentExtraCount)).'<br>';
								} else {
									$commentText1 = $commentTextWithMention;
								}
							?>
							<span id = "user-comment-text-display-<?php echo $getCommentCall[$i]['_id']; ?>" class="comments-module-comment"><?php echo $commentText1;
									if ($commentText2 != ''){
								?>
								<span id = "comment-text-2<?php echo $getCommentCall[$i]['_id']; ?>" style = "display:none;"><?php  echo $commentText2;  ?>
								</span>
								<?php } ?></span>
						</div>
						<div class="comments-module-time-block">

							<a id = "comment-reply-click-<?php echo $getCommentCall[$i]["_id"]; ?>" class="comments-module-reply" onclick = "getCommentReplies('function', '', '30', '<?php echo $getCommentCall[$i]["_id"]; ?>','1','<?php echo $postId; ?>');">Reply <span id = "reply-count-<?php echo $getCommentCall[$i]["_id"]; ?>"><?php if ($getCommentCall[$i]['replyCount'] > 0){ echo ' ('.$getCommentCall[$i]["replyCount"].') '; } //printArr($getCommentCall[$i]);?></span>
							</a>
							<span class="comments-module-hrs">
                                    <?php //$created_at = $getCommentCall[$i]['editTime'];
                                        $mil = $getCommentCall[$i]['editTime'];
                                        $seconds = $mil / 1000;
                                        $created_at = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s",$seconds));
                                        include("getDateFormate.php");

                                    ?></span>
						</div>
						<div id = "comment-reply-append-<?php echo $getCommentCall[$i]["_id"]; ?>" class = "comment-append-reply UI--comment-append-reply " style="display:none;">
							<!--all reply will append through Ajax-->
						</div>
						<?php //if ($getCommentCall[$i]['replyCount'] > 0){?>
						<!--<a id = "show-reply-comment-<?php echo $getCommentCall[$i]["_id"]; ?>" class="comments-module-reply" onclick = "getCommentReplies('function', '', '3', '<?php echo $getCommentCall[$i]["_id"]; ?>','1');">View <?php echo $getCommentCall[$i]["replyCount"]; ?> Replies</a>-->
						<?php 
							if(isset($_SESSION["profile_pic"]) && !empty($_SESSION["profile_pic"])){
                                global $cdnSocialUrl;
                                global $rootUrlImages;

                                $extensionUP  = pathinfo($_SESSION["profile_pic"], PATHINFO_EXTENSION);
                                //CDN image path
                                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $_SESSION["account_handle"] . '/profile/' . $_SESSION["account_handle"] . '_' . $_SESSION["profile_pic"] . '.40x40.' . $extensionUP;

                                //server image path
                                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$_SESSION["account_handle"].'/profile/'.$_SESSION["account_handle"].'_'.$_SESSION["profile_pic"];

                                // check for image is available on CDN
                                $file = $imageFileOfCDNUP;
                                $file_headers = @get_headers($file);
                                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    $sessionImgSrc = $imageFileOfLocalUP;
                                } else {
                                    $sessionImgSrc = $imageFileOfCDNUP;
                                }
				            }else{
				                $sessionImgSrc = $rootUrlImages."default_profile.jpg";
				            }
						?>
						<div id = "comment-reply-div-<?php echo $getCommentCall[$i]["_id"]; ?>" class="row comments-module-wrapper  comment-reply-create UI--comment-reply-create" style = "display : none;">
							<div class="col-xs-1 wid-50">
								<img class = "reply-img" src="<?php echo $sessionImgSrc; ?>">
							</div>
							<!--<div class="col-xs-11">
								<div id="comment-form" class="form-group">
									<input type="text" id="user-comment-reply-text" class="form-control checkEmpty" placeholder="Write Comment Here">
								</div>
							</div>-->
						</div>
						<div id = "commentReplyLoadMoreButton<?php echo $getCommentCall[$i]['_id']; ?>" class="row comments-module-wrapper UI--comments-module-wrapper" style = "display:none;">
							<div class="col-xs-12">
								<a id = "loadMoreCommentReplyPostPage<?php echo $getCommentCall[$i]['_id']; ?>" class="comments-module-all-comments" onclick = "" >View <span id = "remainingReplyLoadCount<?php echo $getCommentCall[$i]['_id']; ?>" class="comment-count"></span> more Replies
								</a>
							</div>
						</div>
					</div>
					<div id = "comment-edit-<?php echo $getCommentCall[$i]['_id']; ?>" class="col-xs-11 comment-editing comment-editing-each-post-<?php echo $postId; ?>" style = "display :none;">
					</div>
					<div id = "comment-edit-display-action-<?php echo $getCommentCall[$i]["_id"]; ?>" class="col-xs-1 pull-right UI--comment-editing-action-post comment-editing-action-post-<?php echo $postId; ?>">
						<?php if ( $getCommentCall[$i]['comment_by'] == $_SESSION['id'] ) {?>
							<a  class="edit-comment UI--edit-comment pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                              <li class="border-bottom clearfix">
                                <a data-toggle="modal" onclick = "editDeleteComment('update', '<?php echo $getCommentCall[$i]["_id"]; ?>', '<?php echo $postId; ?>');" ><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                              </li>
                              <li>
                                <a data-toggle="modal" data-target="#deleteConfirmation"  onclick = "deleteConfirmation('<?php echo $postId; ?>', '<?php echo $getCommentCall[$i]["_id"]; ?>','comment','');" ><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                              </li>
                            </ul>

						<?php } else if ($getCommentCall[$i]['post_id']['posted_by'] == $_SESSION['id']) {?>
						<a onclick = "deleteConfirmation('<?php echo $postId; ?>', '<?php echo $getCommentCall[$i]["_id"]; ?>','comment','');"  href="#11" ><i class="fa fa-close"></i></a>
						<?php }?>
					</div>
				</div>
	<?php
		}
	}
}


				// Get comment last value after break.
				$lastComment = $i-1;
				$postIds     = array(array(
				'post_id' => (int)$postId,
				'time'    => (int)$postCreationTime));
				$postIds 	 = json_encode($postIds);

				// printArr($postIds);
				$nextPostCount = getPostActivityCount($postIds, $getCommentCall[$lastComment]['created_at']);

				// printArr($nextPostCount);

				if ($nextPostCount['errMsg']['remainingComments'] > 0 ) {
					$LoadMoreFlg = 'true';
				} else {
					$LoadMoreFlg = 'false';
				}

				if ($nextPostCount['errMsg']['remainingComments'] >= 10 ) {
					$appendLoadCOmmentCount = $nextPostCount['errMsg']['remainingComments'];//"10+";
				} else {
					$appendLoadCOmmentCount = $nextPostCount['errMsg']['remainingComments'];
				}
				$remainingActualCount = $appendLoadCOmmentCount + (count($getCommentCall) - $viewMoreFlag);
			} else {
				$LoadMoreFlg = 'false';
			}
			if ($LoadMoreFlg == 'true') {
				$loadMoreFunctn = "commentAppendData('LoadMore','".$getCommentCall[$lastComment]['created_at']."','50','','".$postId."')";
			} else {
				$loadMoreFunctn = '';
			}


			echo "<div id = 'get-more-comment" . $lastCommentTime . $postId . "' style = 'display:none;' remainingCount = '".$appendLoadCOmmentCount."' commentLoadClick = '".$LoadMoreFlg."' commentTotalCount = '".$nextPostCount['errMsg'][$postId]['comment_count']."'>".$loadMoreFunctn."</div>";
		} else if ($getCommentCall['errCode'] == 4){
			echo "<center>Sorry Please try After Some Time</center>";
			print("<script>");
		    print("var t = setTimeout(\"postDeletedModal('This Post No longer Exist'); postLoadAsPerUrl(); \", 000);");
		    print("</script>");
		} else {
			echo "<center>Unable to Load Comments</center>";
		}
	} else {
		//session is not active, redirect to login page
	    print("<script>");
	    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	    print("</script>");
	    die;
	}

?>
