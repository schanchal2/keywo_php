<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");
		require_once ("../../models/social/socialModel.php");
		require_once("../../models/social/commonFunction.php");


		$email = $_SESSION["email"];
?>

<main class="social-main-container inner-7x innerT" >
<div class="container row-10">
<div class="row">
	<div class="col-xs-3 social-right-position">
		<div class="social-left-panel">
			<?php include("leftInfoUser.php"); ?>
			<div class="inner-2x innerMT">
				<?php include("personalDetail.php"); ?>
			</div>
			<div class="inner-2x innerMT">
				<?php include("personalWorkDetail.php"); ?>
			</div>
			<!-- my-info-card  -->
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-6">
		<div class="social-center-panel">
			<!-- =================== social-search-box ================== -->
			<div class="social-search-box social-card innerMB">
				<span class="pull-right post-edit-icon" style="position: absolute;right: 20px;top: 5px;">
						<i class="fa fa-edit pull-right half innerMT fa-lg text-primary-blue"></i>
				</span>
				<div class="card share-something-textarea" id="social-micro-blog-box">
				<form onSubmit="return false;" name="status-post" id="status-post" class="margin-none">
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group margin-none">
								<input type="hidden" name="post-type" value="status">
								<div class="share-something-compressed share-small">
									<textarea  rows="4" class="share-post-box compressed-div form-control checkEmpty" name="modalComment" id="modalComment" placeholder="Share Something..."></textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="keywords-for-tags row innerAll clearfix" style="display:none;">
						<div id="tags" class="tag-text">
						<?php
							$keywordinputValue = "";
							foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
								$keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
								$keywordinputValue .= "#".$keywordValue."  ";
							}
							$keywords = rtrim($keywords,', ');
						?>
						<?php
							echo $keywords;
							if (!empty($postDetail['_id'])) {
						?>
							<input type = "text" value = "<?php echo $keywordinputValue; ?>" readonly>
						<?php
							}
						?>
							<input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" style="outline: none;" placeholder="Add #Keywords(min 1-max 3)" value = "" />
							<input type="hidden" name = "modalKeywords" />
						</div>
					</div>
					<div class="row post-bottom-controls innerMT" style="display:none;">
						<div class="share-textbox-quick-links">
							<div class="col-xs-4">
								<span class="innerR" data-toggle="modal" href="#editPostDetails" onclick = "editPersonalPost('', '', 'blog','','')">
									<a data-toggle="tooltip" data-placement="top" title="Blog">
										<i class="fa fa-file-text"></i>
									</a>
								</span>

								<span class="innerR" data-toggle="modal" href="#editPostDetails" onclick = "editPersonalPost('', '', 'video','','')">
									<a data-toggle="tooltip" data-placement="top" title="Video">
										<i class="fa fa-play-circle"></i>
									</a>
								</span>

								<span data-toggle="modal" href="#editPostDetails" class="innerR"  onclick = "editPersonalPost('', '', 'image','','')">
									<a data-toggle="tooltip" data-placement="top" title="Image">
										<i class="fa fa-picture-o"></i>
									</a>
								</span>

								<span data-toggle="modal" href="#editPostDetails" class="innerR"  onclick = "editPersonalPost('', '', 'audio','','')">
									<a data-toggle="tooltip" data-placement="top" title="Audio">
										<i class="fa fa-soundcloud"></i>
									</a>
								</span>
							</div>
							<div class="col-xs-8">
								<div class="pull-left innerML inner-3x padding-none">
									<select class="selectpicker" name="postscope">
										<option value="Public">Public</option>
										<option value="Followers">Followers</option>
										<option value="Only Me">Only Me</option>
									</select>
								</div>
								<div class="pull-right">
									<div class="">
										<input value="Post" class="form-post-btn text-center" type="button"  id="statusPost" onclick = "createNewPost('status', 'status-post', 'create');"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
				</div>

			</div>
			<!-- social-search-box -->

				<!-- =================== social-all-status-tabs ================== -->
					<div class="social-all-status-tabs">
						<div class="row">
							<div class="col-xs-12">
								<ul class="list-inline margin-bottom-none">
									<li>
										<a id="all" href="#all" class="social-status-tabs active">ALL</a>
									</li>
									<li>
										<a id="status" href="#status" class="social-status-tabs">STATUS</a>
									</li>
									<li>
										<a id="blog" href="#blog" class="social-status-tabs">BLOG</a>
									</li>
									<li>
										<a id="video" href="#video" class="social-status-tabs">VIDEO</a>
									</li>
									<li>
										<a id="image" href="#image" class="social-status-tabs">IMAGE</a>
									</li>
									<li>
										<a id="audio" href="#audio" class="social-status-tabs">AUDIO</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- social-all-status-tabs -->
					<!--display content-->
					<div id="show-error-message"></div>
					<div id="private-next-post-data" style="display:none;" data-count="0" data-type="all" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
					<div id="private-post-data">
						<!--display loading content div before data is loaded-->
						<div id="loading-content-div"><?php include('loadingContent.php'); ?></div>
						<!--end of loading content div-->
						<!--content from ajax-->
				</div>
				<div id = "ajaxLoader1" class="text-center" style="display:none; ">
					<img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
				</div>
				<!--display content-->
		</div>
		<!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

	<div class="col-xs-3 social-right-position">
		<div class="social-right-panel">
			<div id="suggested-user-details">
					<?php include("suggestedUser.php"); ?>
			</div>
		</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->
	</div>
</div>
<!-- container -->
</main>


<!--share-post-modal-->
<div class="modal fade" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog" role="document">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body-share-post">

			</div>
		</div>
	</div>
</div>
<!--end-share-post-->

<!--modal to display editing data-->
<div class="modal fade" id="editPostDetails" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body">

			</div>
		</div>
	</div>
</div>


<!--modal to display editing data-->
<div class="modal fade row-10" id="showModalPost" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix closebutton-absolute">
				<i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
			</div>
			<div class = "modal-body-social-user-image">

			</div>
		</div>
	</div>
</div>


<!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>


<!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>


<!--modal to show Post is Deleted User-->
<div class="modal fade" id="postDeletedModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class = "modal-body-post-deleted">

			</div>
		</div>
	</div>
</div>
<!-- Likes List Modal -->
<div id="myModal_like" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<!-- Modal content-->
			<div class="" id="likeUserList">

			</div>
		</div>
	</div>
</div>

<!--social-timeline-modal-on-remove-post-->
<div class="modal fade" id="removePostModal" role="dialog">
  <div class="modal-dialog">
    <div class="card social-card clearfix social-form-modal">
      <!-- Modal content-->
      <div class="">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-blue">Delete</h4>
        </div>
        <div class="modal-body">
          Are you sure you want to delete post?
        </div>
        <div class="modal-footer border-none">
          <button type="button" class="btn btn-social-wid-auto yes-remove-post-btn" data-dismiss="modal">Yes</button>
          <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!--end-remove-post-->
	<!-- else part if session expires -->
	<?php
	} else {
			header('location:'. $rootUrl .'../../views/prelogin/index.php');
	}
	?>


<?php include('../layout/social_footer.php'); ?>
<!-- Blog Form starts-->
<link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
<link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico" >
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">


<script>
	var bottom = $(document).height() - $(window).height(); //for endless scrolling
	var successflag = 0;
	$(document).scroll(function(){
		/*
		For endless scrolling
		*/
		var win = $(window);
		// Each time the user scrolls
		win.scroll(function() {
			// End of the document reached?
			if ($(document).height() - win.height() == win.scrollTop()) {
				// Do the stuff
				if(successflag == 0){
					var privateScrollAllow = $("#private-next-post-data").attr("data-scroll-allow");
					var privateDataEmpty = $("#private-next-post-data").attr("data-status-empty");
					var privateDataType = $("#private-next-post-data").attr("data-type");
					if (privateScrollAllow == "true" && privateDataEmpty == "false") {
						$('#ajaxLoader1').show();
						loadPostTimeline(privateDataType);
					}
					successflag = 1;
				}
			}else{
				// console.log("else");
				successflag = 0;
			}
		});
	});
</script>
