<!--=============================================
=            include : suggestedUser            =
==============================================-->
<?php
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


    //Get following array
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $getFollowingArray = getFollowUser($targetDirAccountHandler);
    //Get suggested user

    //Get Blocked Array
    $getBlockedArray   = getBlockedUser($targetDirAccountHandler);

    if(count($getBlockedArray)==0 ){
      $getFinalArray = $getFollowingArray;
    }elseif(count($getFollowingArray)==0){
        $getFinalArray = $getBlockedArray;
    }else{
      //merge two arrayHelper
      $getFinalArray = array_merge($getFollowingArray, $getBlockedArray);
    }

    $getSuggested = getSuggestedUser($_SESSION['id']);
    if(noError($getSuggested)){
        $getSuggestedList = $getSuggested["errMsg"];
    }
?>

    <div class="suggested-card">
        <div class="card social-card left-panel-modules innerMB inner-2x">
            <div class="bg-light-gray left-panel-modules-head">
                <div class="row margin-none">
                    <div class="col-xs-12">
                        <h4 class="half innerMTB">Suggested</h4>
                    </div>
                </div>
            </div>
            <div class="suggested-module">
                <?php

                foreach($getFinalArray as $followingArray){
                  foreach($getSuggestedList  as $key => $getSuggestedDetails) {

                    $suggestedId = $getSuggestedDetails['user_id'];
                    // if(isset($getSuggestedDetails["profile_pic"]) && !empty($getSuggestedDetails["profile_pic"])){
                    //     $imgSrc = $rootUrlImages.'social/users/'.$getSuggestedDetails["account_handle"].'/profile/'.$getSuggestedDetails["account_handle"].'_'.$getSuggestedDetails["profile_pic"];
                    // }else{
                    //     $imgSrc = $rootUrlImages."default_profile.jpg";
                    // }
                    if($suggestedId==$followingArray){
                      unset($getSuggestedList[$key]);
                    }
                  }
                }
                $getSuggestedList = array_values($getSuggestedList);
                    if($getSuggested['errCode']==4){ 
                ?>
                        <div>
                             <center>No Data Found</center>
                        </div>
                <?php
                    }elseif(count($getSuggestedList)==0) { 
                ?>
                        <div>
                            <center>No Data Found</center>
                        </div>
                <?php 

                    }else{  
                        $getSuggestedDetails  = $getSuggestedList;   
                        $initialValue = rand(0,(count($getSuggestedDetails) - 3));
                        if ($initialValue < 0) {
                            $initialValue = 0;
                        }
                        for($i = $initialValue ; $i < count($getSuggestedDetails) && $i <= ($initialValue + 2) ; $i++) {
                            if(isset($getSuggestedDetails[$i]["profile_pic"]) && !empty($getSuggestedDetails[$i]["profile_pic"])){
                                global $cdnSocialUrl;
                                global $rootUrlImages;

                                $extensionUP  = pathinfo($getSuggestedDetails[$i]["profile_pic"], PATHINFO_EXTENSION);
                                //CDN image path
                                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $getSuggestedDetails[$i]["account_handle"] . '/profile/' . $getSuggestedDetails[$i]["account_handle"] . '_' . $getSuggestedDetails[$i]["profile_pic"] . '.40x40.' . $extensionUP;

                                //server image path
                                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$getSuggestedDetails[$i]["account_handle"].'/profile/'.$getSuggestedDetails[$i]["account_handle"].'_'.$getSuggestedDetails[$i]["profile_pic"];

                                // check for image is available on CDN
                                $file = $imageFileOfCDNUP;
                                $file_headers = @get_headers($file);
                                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    $imgSrc = $imageFileOfLocalUP;
                                } else {
                                    $imgSrc = $imageFileOfCDNUP;
                                }
                            }else{
                                $imgSrc = $rootUrlImages."Suggested_profile.png";
                            }
                ?>
                            <div id = "suggested<?php echo $getSuggestedDetails[$i]['_id']; ?>" class="row margin-none innerTB border-bottom">
                                <div class="col-xs-3">
                                    <img class="suggested-user-image" src="<?php echo $imgSrc; ?>" />
                                </div>
                                <div class="padding-left-none col-xs-6">
                                    <?php if($getSuggestedDetails[$i]['user_id'] != $_SESSION['id']) { ?>
                                    <div class="social-user-link-name">
                                        <a href="otherTimeline.php?email=<?php echo base64_encode($getSuggestedDetails[$i]['email']); ?>">
                                            <?php  echo $getSuggestedDetails[$i]['first_name']; echo " ". $getSuggestedDetails[$i]['last_name'];?>
                                        </a>
                                        <?php } else { ?>
                                        <a href="<?php echo $rootUrl; ?>">
                                            <?php echo $getSuggestedDetails[$i]['last_name'];?>
                                        </a>
                                        <?php } ?>
                                    </div>
                                    <?php if($getSuggestedDetails[$i]['user_id'] != $_SESSION['id']) { ?>
                                    <div class="social-user-link">
                                        <a href="otherTimeline.php?email=<?php echo base64_encode($getSuggestedDetails[$i]['email']); ?>">@<?php echo $getSuggestedDetails[$i]['account_handle'];?></a>
                                        <?php } else { ?>
                                        <a href="<?php echo $rootUrl; ?>">@<?php echo $getSuggestedDetails[$i]['account_handle'];?></a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class=" col-xs-3 text-right">
                                    <a id="suggested<?php echo $getSuggestedDetails[$i]["account_handle"];?>" onclick="ajaxSuggestedFollow(this,'<?php echo $getSuggestedDetails[$i]["account_handle"]; ?>','<?php echo $getSuggestedDetails[$i]["email"]; ?>');" class="half innerR"><i class="fa fa-check"></i></a>
                                    <a onclick = "getSuggestedUserData();"><i class="fa fa-close"></i></a>
                                </div>
                            </div>
                <?php 
                        } // End Of Foreach
                    } // End of else
                if(count($getSuggestedDetails)>3 && count($getSuggestedList) != 0 ){
                ?>
                <div class="row margin-none half innerTB border-bottom bg-view-all">
                    <div class="col-xs-12">
                        <a href="<?php echo $rootUrl;?>views/social/followers.php?type=suggested" class="text-deep-sky-blue pull-right">View all</a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <!--====  End of include : suggestedUser  ====-->
