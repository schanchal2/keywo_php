<!--================================================
=            include : sharePostContent            =
=================================================-->
<!--========= shared social-timeline-image ===========-->
<?php
$socialFileUrlShare = basename($_SERVER['PHP_SELF'], ".php");
$searchCount = '';
$hideAction = "hide";
if ($socialFileUrlShare == "homeTimelineContent" || $socialFileUrlShare == "searchTimelineContent") {
  // printArr($data[$i]); //die;
  $postId                     = $data[$i]->_id;
  $userPic                    = $data[$i]->user_ref->profile_pic;
  $userFname                  = $data[$i]->user_ref->first_name;
  $userLname                  = $data[$i]->user_ref->last_name;
  $userAccHandle              = $data[$i]->user_ref->account_handle;
  $emailId                    = $data[$i]->user_ref->email;
  $userPostedBy               = $data[$i]->posted_by;
  $postCreatedAt              = $data[$i]->created_at;
  $postType                   = $data[$i]->post_type;
  $postShortDesc              = $data[$i]->post_short_desc;
  $postCollectionName         = $data[$i]->post_collection;
  $postSharedParent           = $data[$i]->post_details->parent_post->post_id;
  $postSharedParentId         = $postSharedParent->_id;
  $postSharedParentPostedBy   = $postSharedParent->posted_by;
  $postSharedParentUserId     = $postSharedParent->posted_by;
  $postSharedParentEmail      = $postSharedParent->user_ref->email;
  $postSharedParentFname      = $postSharedParent->user_ref->first_name;
  $postSharedParentLname      = $postSharedParent->user_ref->last_name;
  $postSharedParentAccHandle  = $postSharedParent->user_ref->account_handle;
  $postSharedKeyword          = $postSharedParent->keywords[0];
  $postSharedKeywords         = $postSharedParent->keywords;
  $postSharedShortDesc        = $postSharedParent->post_short_desc;
  $postSharedType             = $postSharedParent->post_type;
  $postSharedCreatedAt        = $postSharedParent->created_at;

    if(isset($postSharedParent->post_details->asset_url)){
         $postSharedAssetUrl     = $postSharedParent->post_details->asset_url;
    }else{
        $postSharedAssetUrl = " ";
    }

    if(isset($postSharedParent->post_details->blog_title )){
        $postSharedBlogTitle        = $postSharedParent->post_details->blog_title;

    }else{
         $postSharedBlogTitle = " ";
    }


    if(isset($postSharedParent->post_details->img_file_name)){
        $postSharedImgUrl           = $postSharedParent->post_details->img_file_name;
    }else{
        $postSharedImgUrl = " ";
    }


    if(isset($postSharedParent->post_details->blog_content )){
        $postSharedBlogContent      = $postSharedParent->post_details->blog_content;

    }else{
        $postSharedBlogContent = " ";
    }


  $postSharedParentType       = $data[$i]->post_details->parent_post->post_id->post_type;
  $postViewCount              = $data[$i]->post_views_count;
  $post_share_earnings        = $data[$i]->post_earnings;
  $post_share_earnings_parent = $data[$i]->post_details->parent_post->post_id->post_earnings;
} elseif ($socialFileUrlShare == "privateTimelineContent" || $socialFileUrlShare == "otherTimelineContent") {
  //printArr($data);die;
  $postId                    = $data["_id"];
  $userPic                   = $data["user_ref"]["profile_pic"];
  $userFname                 = $data["user_ref"]["first_name"];
  $userLname                 = $data["user_ref"]["last_name"];
  $userAccHandle             = $data["user_ref"]["account_handle"];
  $emailId                   = $data["user_ref"]["email"];
  $userPostedBy              = $data["posted_by"];
  $postCreatedAt             = $data["created_at"];
  $postType                  = $data["post_type"];
  $postShortDesc             = $data["post_short_desc"];
  $postSharedParent          = $data["post_details"]["parent_post"]["post_id"];
  $postSharedParentId        = $postSharedParent["_id"];
  $postSharedParentPostedBy  = $postSharedParent["posted_by"];
  $postSharedParentUserId    = $postSharedParent["posted_by"];
  $postSharedParentEmail     = $postSharedParent["user_ref"]["email"];
  $postSharedParentFname     = $postSharedParent["user_ref"]["first_name"];
  $postSharedParentLname     = $postSharedParent["user_ref"]["last_name"];
  $postSharedParentAccHandle = $postSharedParent["user_ref"]["account_handle"];
  $postSharedKeyword         = $postSharedParent["keywords"][0];
  $postSharedKeywords        = $postSharedParent["keywords"];
  $postSharedShortDesc       = $postSharedParent["post_short_desc"];
  $postSharedType            = $postSharedParent["post_type"];
  $postSharedCreatedAt       = $postSharedParent["created_at"];

  if(isset($postSharedParent["post_details"]["asset_url"])){
      $postSharedAssetUrl    = $postSharedParent["post_details"]["asset_url"];
  }else{
      $postSharedAssetUrl    = " ";
  }

    if(isset($postSharedParent["post_details"]["blog_title"])){
        $postSharedBlogTitle       = $postSharedParent["post_details"]["blog_title"];
    }else{
        $postSharedBlogTitle    = " ";
    }

    if(isset($postSharedParent["post_details"]["blog_content"])){
        $postSharedBlogContent     = $postSharedParent["post_details"]["blog_content"];
    }else{
        $postSharedBlogContent    = " ";
    }

    if(isset($postSharedParent["post_details"]["img_file_name"])){
        $postSharedImgUrl          = $postSharedParent["post_details"]["img_file_name"];
    }else{
        $postSharedImgUrl    = " ";
    }


  $postSharedParentType      = $data["post_details"]["parent_post"]["post_id"]["post_type"];
  $post_share_earnings             = $data['post_earnings'];
  $post_share_earnings_parent      = $data['post_details']["parent_post"]["post_id"]['post_earnings'];
}

$postTime         = $postCreatedAt;
$keywords         = implode(' ', $postSharedKeywords);

//Check Like post details
$getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);

//check cdp status
$getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);

//Get Special User
//$getSpecialUserData =
// ();
//if(noError($getSpecialUserData)){
//    $getSpecialUserData = $getSpecialUserData["errMsg"];
//}
//$emailofSpecialUser     = array();
//
//if($getSpecialUserData!="No Result") {
//    foreach($getSpecialUserData as $specialUser){
//        $emailofSpecialUser[] = $specialUser['email'];
//    }
//}
global $cdnSocialUrl;
global $rootUrlImages;

if(isset($userPic) && !empty($userPic)){
    $extensionUP  = pathinfo($userPic, PATHINFO_EXTENSION);
    //CDN image path
    $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $userAccHandle . '/profile/' . $userAccHandle . '_' . $userPic . '.40x40.' . $extensionUP;

    //server image path
    $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$userAccHandle.'/profile/'.$userAccHandle.'_'.$userPic;

    // check for image is available on CDN
    $file = $imageFileOfCDNUP;
    $file_headers = @get_headers($file);
    if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
        $imgSrc = $imageFileOfLocalUP;
    } else {
        $imgSrc = $imageFileOfCDNUP;
    }
} else {
    $imgSrc = $rootUrlImages."default_profile.jpg";
}

?>
    <input type="hidden" id="post_collection<?php echo $postId; ?>" value="<?php echo $postCollectionName; ?>">
    <div class="social-timeline-image inner-2x innerMB" id="post_<?php echo $postId; ?>">
        <div class="social-card card innerLR innerT innerMT" id="social-timeline-image">
            <!-- <div class="half innerAll"> -->
            <div class="row">
                <div class="col-xs-6">
                    <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $imgSrc; ?>" class=" img-responsive" />
                    </div>
                    <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                            <a href="#">
                                <?php if ($_SESSION['id'] == $userPostedBy) { ?>
                                <a href="../../views/social/privateTimeline.php">
                                    <?php } else { ?>
                                    <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($emailId); ?>" target="_blank">
                                        <?php } ?>
                                        <?php echo $userFname; ?>
                                        <?php echo $userLname; ?>
                                    </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                            <a href="#">
                                <?php echo '@' . $userAccHandle; ?>
                            </a>
                        </div>
                        <!--social-user-link-->
                    </div>
                    <!--social-timeline-profile-details-->
                </div>
                <div class="col-xs-6 social-post-options">
                    <div class="dropdown pull-right">
                        <span class="inner-2x innerR text-social-gray">Shared post</span>
                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="fa fa-chevron-down"></span>
                        </a>
                        <?php
              if ($userId == $userPostedBy) { ?>
                <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">

                 <li>
                   <a data-toggle="modal" href="#editPostDetails" onclick ="editPersonalPost('<?php echo $postId; ?>','<?php echo $postCreatedAt; ?>','<?php echo $postType;?>','<?php echo $userAccHandle;?>','<?php echo $emailId; ?>','update');">
                     <i class="fa fa-pencil" ></i>
                     Edit Post
                   </a>
                 </li>
                 <li>
                  <a onclick ="hidePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $postCreatedAt; ?>','<?php echo $postSharedType; ?>','<?php echo $emailId; ?>','<?php echo  $hideAction; ?>');">
                    <i class="fa fa-eye-slash" ></i>
                    Hide Post
                  </a>
                </li>

                <li>
                  <a onclick="removePost('<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $postCreatedAt; ?>');">
                    <i class="fa fa-trash" ></i>
                    Remove Post
                  </a>
                </li>
              </ul>
              <?php } else { ?>
              <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
              <li>
                <a onclick ="hidePost('<?php echo $postId; ?>','<?php echo $postType;?>','<?php echo $postCreatedAt; ?>','<?php echo $postSharedType; ?>','<?php echo $emailId; ?>','<?php echo  $hideAction; ?>');">
                  <i class="fa fa-eye-slash"></i>
                  Hide Post
                </a>
              </li>
              <li>
                  <a onclick = "setReportAccountOfUser('<?php echo $postId;?>','<?php echo $postSharedParentType;?>','<?php echo $postCreatedAt; ?>','<?php echo $emailId; ?>');">
                  <i class="fa fa-file-text"></i>
                  Report Post
                  </a>
              </li>
              <li>
                <?php
                $followFlag = false;
                $getFollowingDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($userAccHandle[0]).".json";
                if(file_exists($getFollowingDetails)) {
                $jsondataFollowing = file_get_contents($getFollowingDetails);
                $dataFollowing = json_decode($jsondataFollowing, true);
                if (in_array($userPostedBy, $dataFollowing["followings"])) {
                $followFlag = true;
                }  } ?>
                                            <a href="javascript:;" onclick="followAccountOfUser(this,'<?php echo $emailId; ?>','<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $postCreatedAt; ?>');">
                                                <?php    if($followFlag){  $buttoVal = 'Unfollow'; ?>
                                                <i class="fa fa-user-times" id="<?php echo $postId; ?>"></i>
                                                <?php   } else {  ?>
                                                <i class="fa fa-user" id="<?php echo $postId; ?>"></i>
                                                <?php $buttoVal = 'Follow';        }  ?>
                                                <?php echo $buttoVal; ?> User
                                            </a>
                                    </li>
                                    <li>
                                        <?php
                $blockFlag = false;
                $getBlockDetails = $targetDirAccountHandler.$_SESSION["account_handle"]."_info_" .strtolower($userAccHandle[0]).".json";
                if(file_exists($getBlockDetails)) {
                $jsondataBlock = file_get_contents($getBlockDetails);
                $dataBlock = json_decode($jsondataBlock, true);
                if (in_array($userPostedBy, $dataBlock["blocked"])) {
                $blockFlag = true;
                }  } ?>
                                            <a href="javascript:;" onclick="blockUserAccByConfirmation(this,'<?php echo $emailId; ?>','<?php echo $postId; ?>','<?php echo $postType; ?>','<?php echo $postCreatedAt; ?>');">
                                                <?php  if($blockFlag){  $buttoVal = 'Unblock'; ?><i class="fa fa-user"></i>
                                                <?php } else { $buttoVal = 'Block';?><i class="fa fa-user-times"></i>
                                                <?php  } ?>
                                                <?php echo $buttoVal;?> #<span>Account</span>
                                            </a>
                                    </li>
                                </ul>
                                <?php }  ?>
                                <?php if ($getBookMarkPost == 1) { ?>
                                <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star text-yellow" id="bookmark<?php echo $postId; ?>" onclick = "setBookmarkAccountOfUser('<?php echo $postId ; ?>','<?php echo $postType;?>','<?php echo $postCreatedAt; ?>','<?php echo $postSharedParentEmail; ?>');"></i></span>
                                <?php } else { ?>
                                <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star-o"  id="bookmark<?php echo $postId; ?>"  onclick = "setBookmarkAccountOfUser('<?php echo $postId ; ?>','<?php echo $postType;?>','<?php echo $postCreatedAt; ?>','<?php echo $postSharedParentEmail; ?>');"></i></span>
                                <?php } ?>
                    </div>
                    <br/>
                    <div class="pull-right social-post-time-container">
                        <span class="social-post-time"> <span class="social-post-time"><span title="<?php  echo  uDateTime("h:i A",$postCreatedAt)." - ".uDateTime("d M Y",$postCreatedAt);?>"><?php include("getDateFormate.php"); ?></span>
                    </div>
                    <br>
                </div>
            </div>
            <!--row-->
            <!-- </div> -->
            <!--social-timeline-details-->
            <?php if(isset($postSharedKeyword) && !empty($postSharedKeyword)) { ?>
            <div class="social-timeline-keywords-details">
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        $keywoKeywords = " ";
                foreach ($postSharedKeywords as $keyword) {
                  $keywords = "<span>#".$keyword."</span> ";
                  $keywoKeywords .= $keyword . ", ";

              ?>
                            <a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keyword);?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keyword;?>">
                                <?php echo $keywords;?>
                            </a>
                            <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <!--popup-->
            <div onclick="showModalPost('<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>', '', '<?php echo $ipAddr; ?>', '<?php echo  $emailId; ?>', '<?php echo $userSearchType; ?>');">
                <!--social-timeline-user-message-->
                <div class="social-timeline-content-message">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="innerMB">
                                <?php
                  $statusData = strlen($postShortDesc) > $validateCharCountOnSocial ? substr($postShortDesc, 0, $validateCharCountOnSocial) . "<a href='#'><span class='text-blue'>....See More</span></a>" : $postShortDesc;
                  echo rawurldecode($statusData);
                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--popup closed-->
            <div class="row border-top border-bottom">
            <?php include("shareParentPostContent.php"); ?>
            </div>
            <!--social-timeline-other-user-content-message-->
            <div class="social-timeline-earning-comments-view innerT">
                <div class="row">
                    <div class="col-xs-5">
                        <div>
                          <label>Earning : </label>
                          <span>
                                                        <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                     origPrice="<?php echo number_format("{$post_share_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_share_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_share_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>

                          </span>
                        </div>
                    </div>
                    <div class="col-xs-7">
                        <div class="">
                            <ul class="list-inline pull-right margin-bottom-none">
                              <li>
                                <div>
                                <?php if (!empty($like_count)) { ?>
                                  <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>', '0', '30' ,'<?php echo $like_count; ?>');">
                                      <?php $likeCount = $like_count; ?>
                                      <span class="half innerR like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$likeCount}", 0); ?></span>
                                  </a>
                                  <?php } else {?>
                                  <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>', '0', '30','<?php echo $like_count;?>');">
                                  <span class="half innerR like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                  <?php } ?> 
                                  <label class="pull-right">Likes</label>
                                  </div>
                                </li>
                                <li>
                                    <div>
                                        <span class="half innerR comment-count<?php echo $postId;?>"><?php echo formatNumberToSort("{$commentCount}", 0);?></span>
                                        <label class="pull-right">Comments</label>
                                    </div>
                                </li>
                                <li>
                                    <div>
                                        <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0);?></span>
                                        <label>Shares</label>
                                    </div>
                                </li>
                                <!-- <li class="padding-right-none">
                                    <div>
                                        <span class=""><?php //echo formatNumberToSort("{$postViewCount}", 0);?></span>
                                        <label>Views</label>
                                    </div>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--social-timeline-earning-comments-view-->
            <!--social-timeline-likes-dislikes-->
            <div class="social-timeline-likes-dislikes">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="social-likes-dislikes">
                            <ul class="list-inline margin-bottom-none">
                                <li>
                                    <div>
                                        <a href="JavaScript:void(0);" class="like-area">
                                            <?php
                                            $cdpKeywords         = implode(' ', $postSharedKeywords);;
                                            if($postType == "share"){
                                              $CDPSharedBy = $userPostedBy;
                                              $CDPPostedBy = $postSharedParentPostedBy;
                                            } else {
                                              $CDPSharedBy = '';
                                              $CDPPostedBy = $userPostedBy;
                                            }
                                            if($getPostCdpStatus == 1) {?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                            <?php } elseif ($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) {?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                                              <?php } else {
                                                //if ($userId != $userPostedBy &&  ($userSearchType == "Qualified")) {
                                                if ($userId != $userPostedBy) { ?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-thumbs-o-up half innerR"
                                                   onclick="socialCDPForPost(this,'<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>', '<?php echo $postType; ?>', '<?php echo $userAccHandle; ?>', '<?php echo $CDPPostedBy; ?>', '<?php echo $CDPSharedBy; ?>', '', '<?php echo $ipAddr; ?>', '<?php echo $cdpKeywords; ?>','<?php echo $emailId; ?>');"></i>
                                              <?php } else { ?>
                                                <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>

                                              <?php } }?>
                                        </a>
                                        <!-- <?php if(!empty($like_count)){
                    ?>
                                        <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>','0', '30','<?php echo $like_count;?>');">
                                            <?php echo $like_count; ?>
                                        </a>
                                        <?php }  else { ?>
                                        <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postCreatedAt; ?>','0', '30','<?php echo $like_count;?>');"></a>
                                        <?php } ?> -->
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="social-comments-shares-views">
                            <ul class="list-inline pull-right margin-bottom-none">
                                <li>
                                    <div>
                                        <a onclick="commentDivShow('<?php echo $postId;?>');">
                                            <i class="fa fa-comments-o"></i>
                                            <span>Comments</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="padding-right-none">
                                    <div>
                                        <a onclick="sharePostModal('<?php echo $postId; ?>','<?php echo $postCreatedAt; ?>','<?php echo $postType;?>','<?php echo $userAccHandle;?>', 'create','<?php echo $emailId;?>');">
                                            <i class="fa fa-share-square-o"></i>
                                            <span>Share</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" id="comment-hidden-enter-event-<?php echo $postId; ?>" execute-enetr-event="true" execute-enter-event-main-page="true">
        <div id="comment-include-<?php echo $postId; ?>" class="comments-module bg-light-purple row" style="display:none;">
            <div class="col-xs-12">
                <?php

    $commentPostId           = $postId;
    $commentPostCreationTime = $postCreatedAt;
    $commentPostCreator      = $postSharedParentEmail;
    include ('postPageCommentWidget.php');
  ?>
        </div>

            </div>
        </div>
        <!--social-card-->
    </div>
    </div>
    <!--====  End of include : sharePostContent  ====-->
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>