<?php
  include("../layout/header.php");
?>

<main class="social-main-container inner-7x innerT" >
<div class="container">
  <div class="col-xs-3">
  		<div class="social-left-panel">
  		  <div class="card my-info-card social-card">
  			  <div class="clearfix innerAll padding-bottom-none">
  					<div class="my-info-img pull-left innerMR">
  			      <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG"/>
  			    </div>
  					<!-- my-picture  -->
  			    <div class="my-info-detail pull-left">
  			      <div class="my-info-name-container text-blue innerMB">
  			        <span class="my-info-name"> Palak Hazare</span>
  			        <a class="my-info-name-edit pull-right" >
  			          <i class="fa fa-pencil" aria-hidden="true"></i>
  			        </a>
  			      </div>
  						<!-- my-name  -->
  			      <div class="my-info-status">
  							<a id="my-info-name-edit"
  									data-type="textarea"
  									title="Change Description"
  									data-pk="1"
  									data-placement="right"
  									data-title="Enter username">
  			        MP from Tiruvampuram. Author of 15 books. Formar minister of state governanace of irctc co in
  							</a>
  			      </div>
  						<!-- my-status  -->
  			    </div>
  				</div>
  				<!-- my-info-detail  -->
  				<div class="clearfix half innerT">
  					<div class="col-xs-12  innerB">
  						<div class="col-xs-4 padding-none pull-left">
  							<div class="text-deep-sky-blue my-info-posts">
  								Posts
  							</div>
  							<div class="text-blue innerL my-info-posts-count">
  								3
  							</div>
  						</div>
  						<div class="col-xs-4 padding-none">
  							<div class="text-deep-sky-blue my-info-following">
  								Following
  							</div>
  							<div class="text-blue my-info-following-counts inner-2x innerL">
  								37
  							</div>
  						</div>
  						<div class="col-xs-4 padding-none text-right">
  							<div class="text-deep-sky-blue my-info-followers">
  								Followers
  							</div>
  							<div class="text-blue my-info-followers-count inner-2x innerR">
  								31
  							</div>
  						</div>
  					</div>
  				</div>
  				<!-- posts-followers-following  -->
  			</div>
  			<!-- my-info-card  -->


        <div class="popular-image-module">
          <div class="card social-card right-panel-modules inner-2x innerMTB">
            <div class="bg-light-gray right-panel-modules-head">
              <div class="row margin-none">
              <div class="col-xs-12 padding-right-none">
                <h4 class="half innerMTB">Other Audios</h4>
                </div>
              </div>
            </div>
            <!--bg-light-gray-->
            <div class="row margin-none blog-module border-bottom">
              <div class="row margin-none sound-module">
								<div class="row margin-none innerT">
									<div class="col-xs-4">
										<img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png">
									</div>
									<div class="col-xs-5">
										<a class="text-ellipsis">Here without you</a>
										<span class="" style="display:block;">
											By Doors Down
										</span>
									</div>
									<div class="col-xs-3">
										<span class="text-gray">
											<span>3.45 mins</span>
										</span>
									</div>
								</div>
								<div class="row margin-none innerT">
									<div class="col-xs-7 text-black">
										<label>Earning :</label>
										<span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
									</div>
									<div class="col-xs-5">
										<a class="text-social-primary pull-right">157k Views</a>
									</div>
								</div>
							</div>
            </div>
            <!--row-->
            <div class="row margin-none blog-module border-bottom">
              <div class="row margin-none sound-module">
								<div class="row margin-none innerT">
									<div class="col-xs-4">
										<img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png">
									</div>
									<div class="col-xs-5">
										<a class="text-ellipsis">Here without you</a>
										<span class="" style="display:block;">
											By Doors Down
										</span>
									</div>
									<div class="col-xs-3">
										<span class="text-gray">
											<span>3.45 mins</span>
										</span>
									</div>
								</div>
								<div class="row margin-none innerT">
									<div class="col-xs-7 text-black">
										<label>Earning :</label>
										<span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
									</div>
									<div class="col-xs-5">
										<a class="text-social-primary pull-right">157k Views</a>
									</div>
								</div>
							</div>
            </div>
            <!--row-->
            <div class="row margin-none half innerTB">
              <div class="col-xs-12">
                <a href="#" class="text-deep-sky-blue pull-right">View all <i class="innerL fa fa-refresh"></i></a>
              </div>
            </div>
          </div>
        </div>
        <!--popular blog module-->
  		</div>
  		<!-- social-left-panel  -->

  	</div>
  	<!-- col-xs-3 -->

	<div class="col-xs-9" id="social-timeline-audio-preview">
		<div class="social-center-panel half innerLR">
      <div class="social-timeline-sound inner-2x innerMB">
							<div class="social-card card half innerAll" data-toggle="modal" data-target="#status_sound_modal" id="social-timeline-sound">
									<div class="">
										<div class="row half innerTB">
											<div class="col-xs-8">
												<div class="social-timeline-profile-pic pull-left">
													<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
												</div>
												<div class="social-timeline-profile-details pull-left innerL">
													<div class="social-user-link-name">
														<a href="#">
															tom hanks
														</a>
													</div>
													<!---->
													<div class="social-user-link">
														<a href="#">
															@tomhankennew
														</a>
													</div>
													<!--social-user-link-->
												</div>
												<!--social-timeline-profile-details-->
											</div>
											<div class="col-xs-4 social-post-options">
											<div class="dropdown pull-right">
												<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="fa fa-chevron-down"></span>
												</a>
												<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
												<li>
													<a href="#">
														<i class="fa fa-trash"></i>
														Remove Post
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-bookmark"></i>
														Hide Post
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-pencil"></i>
														Edit Post
													</a>
												</li>
											</ul>
												<span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
											</div>
											<br>
											<div class="pull-right innerT">
												<span class="social-post-time"><span>3</span> hrs</span>
											</div>
										</div>
										</div>
										<!--row-->
									</div>
									<!--social-timeline-details-->

									<div class="social-timeline-keywords-details">
										<div class="row half innerT">
											<div class="col-xs-12">
												<a href="#" class="social-keywords-tags">#bitcoin</a>
												<a href="#" class="social-keywords-tags half innerL">#India</a>
											</div>
										</div>
									</div>
									<!--social-timeline-tags-details-->

									<!--====social-timeline-user-message====-->
									<div class="social-timeline-content-message">
										<div class="row">
											<div class="col-xs-12">
												<p class="innerMB">
													The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
													Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
												</p>
											</div>
										</div>
									</div>
									<!--social-timeline-content-details-->

									<!--social-timeline-details-->
									<div class="social-timeline-sound">
										<div class="row">
											<div class="col-xs-12">
											 <a data-id="social-timeline-sound">
												<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/298167372&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">
											</iframe>
											</a>
											</div>
										</div>
									</div>
									<!--social-timeline-content-blog-image-->
									<div class="social-timeline-earning-comments-view innerT">
									<div class="row">
										<div class="col-xs-4">
											<div>
												<label>Earning : </label>
												<span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
											</div>
										</div>
										<div class="col-xs-8">
											<div class="">
												<ul class="list-inline pull-right margin-bottom-none">
													<li>
														<div>
															<span class="half innerR">40</span>
															<label class="pull-right">Comments</label>
														</div>
													</li>
													<li>
														<div>
															<span class="">456</span>
															<label>Shares</label>
														</div>
													</li>
													<li class="padding-right-none">
														<div>
															<span class="">557k</span>
															<label>Views</label>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- earnings -->
								<div class="social-timeline-likes-dislikes">
									<div class="row">
										<div class="col-xs-6">
											<div class="social-likes-dislikes">
												<ul class="list-inline margin-bottom-none">
													<li>
														<div>
															<a>
																<i class="fa fa-thumbs-o-up half innerR"></i>
																<span>770</span>
															</a>
														</div>
													</li>
													<li>
														<div>
															<a>
																<i class="fa fa-thumbs-o-down half innerR"></i>
																<span>770</span>
															</a>
														</div>
													</li>
												</ul>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="social-comments-shares-views">
												<ul class="list-inline pull-right margin-bottom-none">
													<li>
														<div>
															<a>
																<i class="fa fa-comments-o"></i>
																<span>Comments</span>
															</a>
														</div>
													</li>
													<li class="padding-right-none">
														<div>
															<a>
																<i class="fa fa-share-square-o"></i>
																<span>Share</span>
															</a>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!-- likes & Dislikes -->

                <div class="comments-module bg-light-purple">
              <div class="row comments-module-wrapper">
                <div class="col-xs-1">
                  <img src="<?php echo $rootUrlImages?>social/comment.png">
                </div>
                <div class="col-xs-11">
                  <div class="form-group">
                    <input type="text" class="form-control">
                  </div>
                </div>
              </div>

              <div class="row comments-module-wrapper">
                <div class="col-xs-1">
                  <img src="<?php echo $rootUrlImages?>social/comment.png">
                </div>
                <div class="col-xs-11">
                  <div class="comments-module-comment-block">
                    <a href="#" class="comments-module-username">David Mondrus</a>
                    <span class="comments-module-comment">not "totally" new</span>
                  </div>
                  <div class="comments-module-time-block">
                    <a href="#" class="comments-module-reply">Reply</a>
                    <span class="comments-module-hrs">23 hrs</span>
                  </div>
                </div>
              </div>
              <!--row -->
              <div class="row comments-module-wrapper">
                <div class="col-xs-1">
                  <img src="<?php echo $rootUrlImages?>social/comment.png">
                </div>
                <div class="col-xs-11">
                  <div class="comments-module-comment-block">
                    <a href="#" class="comments-module-username">David Mondrus</a>
                    <span class="comments-module-comment">not "totally" new</span>
                  </div>
                  <div class="comments-module-time-block">
                    <a href="#" class="comments-module-reply">Reply</a>
                    <span class="comments-module-hrs">23 hrs</span>
                  </div>
                </div>
              </div>
              <!--comments-module-wrapper -->
              <div class="row comments-module-wrapper">
                <div class="col-xs-12">
                  <a href="#" class="comments-module-all-comments">View <span class="comment-count">21</span> more comments </a>
                </div>
              </div>

            </div>
            <!--comments module-->
							</div>
							<!--social-card-->
						</div>

        <div class="row">
          <div class="col-xs-12 innerT padding-left-none">
            <h5 class="text-social-primary"><strong>Related Audios</strong></h5>
          </div>
          <div class="col-xs-4 padding-left-none">
            <div id="popular-sound-module" style="">
              <div class="card social-card">
                <div class="row margin-none sound-module border-bottom">
  								<div class="row margin-none innerT">
  									<div class="col-xs-4">
  										<img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png">
  									</div>
  									<div class="col-xs-5">
  										<a class="text-ellipsis">Here without you</a>
  										<span class="" style="display:block;">
  											By Doors Down
  										</span>
  									</div>
  									<div class="col-xs-3">
  										<span class="text-gray">
  											<span>3.45 mins</span>
  										</span>
  									</div>
  								</div>
  								<div class="row margin-none innerT">
  									<div class="col-xs-7 text-black">
  										<label>Earning :</label>
  										<span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
  									</div>
  									<div class="col-xs-5">
  										<a class="text-social-primary pull-right">157k Views</a>
  									</div>
  								</div>
  								<div class="row margin-none half innerTB border-bottom">
  									<div class="col-xs-12">
  										<a href="#" class="text-deep-sky-blue pull-right">View all</a>
  									</div>
  								</div>
  							</div>
              </div>

							</div>
            <!--popular-sound-module-->
          </div>
          <!--col-xs-4-->
          <div class="col-xs-4 padding-left-none">
            <div id="popular-sound-module" style="">
              <div class="card social-card">
                <div class="row margin-none sound-module border-bottom">
  								<div class="row margin-none innerT">
  									<div class="col-xs-4">
  										<img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png">
  									</div>
  									<div class="col-xs-5">
  										<a class="text-ellipsis">Here without you</a>
  										<span class="" style="display:block;">
  											By Doors Down
  										</span>
  									</div>
  									<div class="col-xs-3">
  										<span class="text-gray">
  											<span>3.45 mins</span>
  										</span>
  									</div>
  								</div>
  								<div class="row margin-none innerT">
  									<div class="col-xs-7 text-black">
  										<label>Earning :</label>
  										<span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
  									</div>
  									<div class="col-xs-5">
  										<a class="text-social-primary pull-right">157k Views</a>
  									</div>
  								</div>
  								<div class="row margin-none half innerTB border-bottom">
  									<div class="col-xs-12">
  										<a href="#" class="text-deep-sky-blue pull-right">View all</a>
  									</div>
  								</div>
  							</div>
              </div>

							</div>
            <!--popular-sound-module-->
          </div>
          <!--col-xs-4-->
        </div>
			</div>
			<!--social-timeline-blog-->
    </div>
  </div>
</div>
</main>

<?php include('../layout/social_footer.php'); ?>
