<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
			type="text/css"/>
<main class="social-main-container inner-7x innerT" >
<div class="container">
	<div class="col-xs-3">
		<div class="social-left-panel left-panel-modules">
      <div class="like-unlike-user-profile-">
				<img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png"/>
				<div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
					<div class="col-xs-6 padding-none">
						<div class="col-xs-12 padding-none">
							Lorem
						</div>
						<div class="col-xs-12 padding-none">
							<span class="account-handle-name">@loremi</span>
						</div>
					</div>
					<div class="col-xs-6 padding-none innerMT">
						<input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile"/>
					</div>
				</div>
      </div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Settings and Preferences</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Profile</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Change Password</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Notification Preferences</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Two Factor (2FA)</a>
						</div>
					</div>
				</div>
			</div>
      <!--Settings and Preferences-->

			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Activity Log</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Likes / Dislikes</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Comments <span>(2423)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Post Bookmarks <span>(2443)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed People <span>(2723)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed Keywords <span>(24)</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

  <div class="col-xs-6">
    <div class="social-center-panel half innerLR">


      <!--Activity log -->
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left">
            <h4 class="text-blue half innerMT">Activity Log :</h4>
          </div>
          <div class="pull-left half innerMT innerML">
            <div class="heading">Post Bookmarks</div>
          </div>
        </div>
      </div>
      <!--activity log-->
          <!--display content-->
          <div id="private-post-data">
            <!--content from ajax-->
        </div>
        <!--display content-->

          <!--============= Social Timeline Image ======================-->
          <div class="social-timeline-image half innerMTB" >
            <div class="innerMB like-dislike-time innerML">
      				Today
      			</div>
            <div class="social-card card half innerAll" id="social-timeline-image">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-8">
                      <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                      </div>
                      <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                          <a href="#">
                            tom hanks
                          </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                          <a href="#">
                            @tomhankennew
                          </a>
                        </div>
                        <!--social-user-link-->
                      </div>
                      <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4 social-post-options">
                      <div class="dropdown pull-right">
                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="fa fa-chevron-down"></span>
                        </a>
                        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                        <li>
                          <a href="#">
                            <i class="fa fa-bookmark"></i>
                            Save Post
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-eye-slash"></i>
                            Hide Post
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-file-text"></i>
                            Report Post
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-user-times"></i>
                            Unfollow User
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-key"></i>
                            Unfollow #<span>keyword</span>
                          </a>
                        </li>
                        <li>
                          <a href="#">
                            <i class="fa fa-user-times"></i>
                            Unfollow #<span>Account</span>
                          </a>
                        </li>
                      </ul>
                        <span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
                      </div>
                      <br />
                      <div class="pull-right innerT">
                        <span class="social-post-time"><span>3</span> hrs</span>
                      </div>
                    </div>
                  </div>
                  <!--row-->
                </div>
                <!--social-timeline-details-->

                <div class="social-timeline-keywords-details">
                  <div class="row">
                    <div class="col-xs-12">
                      <a href="#" class="social-keywords-tags">#bitcoin</a>
                      <a href="#" class="social-keywords-tags half innerL">#India</a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-keywords-details-->

                <!--social-timeline-user-message-->
                <div class="social-timeline-content-message">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="innerMB">
                        The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
                        Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                      </p>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->

                <!--  content-image-->
                <div class="social-timeline-content-image">
                  <div class="row">
                    <div class="col-xs-12">
                    <a  onclick="test(this)" data-id="social-timeline-image">
                      <img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive" />
                    </a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-content-image-->

                <div class="social-timeline-earning-comments-view innerT">
                  <div class="row">
                    <div class="col-xs-4">
                      <div>
                        <label>Earning : </label>
                        <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                      </div>
                    </div>
                    <div class="col-xs-8">
                      <div class="">
                        <ul class="list-inline pull-right margin-bottom-none">
                          <li>
                            <div>
                              <span class="half innerR">40</span>
                              <label class="pull-right">Comments</label>
                            </div>
                          </li>
                          <li>
                            <div>
                              <span class="">456</span>
                              <label>Shares</label>
                            </div>
                          </li>
                          <li class="padding-right-none">
                            <div>
                              <span class="">557k</span>
                              <label>Views</label>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!--social-timeline-earning-comments-view-->

                <!--social-timeline-likes-dislikes-->
                <div class="social-timeline-likes-dislikes">
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="social-likes-dislikes">
                        <ul class="list-inline margin-bottom-none">
                          <li>
                            <div>
                              <a>
                                <i class="fa fa-thumbs-o-up half innerR"></i>
                                <span>770</span>
                              </a>
                            </div>
                          </li>
                          <li>
                            <div>
                              <a>
                                <i class="fa fa-thumbs-o-down half innerR"></i>
                                <span>770</span>
                              </a>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-xs-6">
                      <div class="social-comments-shares-views">
                        <ul class="list-inline pull-right margin-bottom-none">
                          <li>
                            <div>
                              <a>
                                <i class="fa fa-comments-o"></i>
                                <span>Comments</span>
                              </a>
                            </div>
                          </li>
                          <li class="padding-right-none">
                            <div>
                              <a>
                                <i class="fa fa-share-square-o"></i>
                                <span>Share</span>
                              </a>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
            <!--social-card-->
          </div>
          <!--social-timeline-image-->

          <!--============= Social Timeline Video ======================-->
          <div class="social-timeline-video inner-2x innerMTB">
            <div class="social-card card half innerAll">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-8">
                      <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                      </div>
                      <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                          <a href="#">
                            tom hanks
                          </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                          <a href="#">
                            @tomhankennew
                          </a>
                        </div>
                        <!--social-user-link-->
                      </div>
                      <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4">
                    </div>
                  </div>
                  <!--row-->
                </div>
                <!--social-timeline-details-->
                <div class="social-timeline-keywords-details">
                  <div class="row half innerT">
                    <div class="col-xs-12">
                      <a href="#" class="social-keywords-tags">#bitcoin</a>
                      <a href="#" class="social-keywords-tags half innerL">#India</a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->

                <!--social-timeline-user-message-->
                <div class="social-timeline-content-message">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="innerMB">
                        The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
                        Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                      </p>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->

                <!--social-timeline-details-->
                <div class="social-timeline-content-video">
                  <div class="row">
                    <div class="col-xs-12">
                      <div align="center" class="embed-responsive embed-responsive-16by9">
                        <!-- <video loop class="embed-responsive-item">
                            <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
                        </video> -->
                        <iframe width="560" height="258" src="https://www.youtube.com/embed/CMdHDHEuOUE" frameborder="0" allowfullscreen></iframe>
                    </div>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->
            </div>
            <!--social-card-->
          </div>
          <!--social-timeline-video-->

          <!--============= Social Timeline Blog ======================-->
          <div class="social-timeline-video inner-2x innerMTB">
            <div class="social-card card half innerAll" id="social-timeline-blog">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-8">
                      <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                      </div>
                      <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                          <a href="#">
                            tom hanks
                          </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                          <a href="#">
                            @tomhankennew
                          </a>
                        </div>
                        <!--social-user-link-->
                      </div>
                      <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4">
                    </div>
                  </div>
                  <!--row-->
                </div>
                <!--social-timeline-details-->

                <div class="social-timeline-keywords-details">
                  <div class="row half innerT">
                    <div class="col-xs-12">
                      <a href="#" class="social-keywords-tags">#bitcoin</a>
                      <a href="#" class="social-keywords-tags half innerL">#India</a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->

                <!--====social-timeline-user-message====-->
                <div class="social-timeline-content-message">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="innerMB">
                        The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
                        Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                      </p>
                    </div>
                  </div>
                </div>
                <!--social-timeline-content-details-->

                <!--social-timeline-details-->
                <div class="social-timeline-content-blog-thumb">
                  <div class="row">
                    <a data-id="social-timeline-blog" onclick="test(this)">
                    <div class="col-xs-12">
                      <img src="<?php echo $rootUrlImages?>social/blog-image.png" class="img-responsive" />
                    </div>
                    </a>
                  </div>
                </div>
                <!--social-timeline-content-blog-image-->
            </div>
            <!--social-card-->
          </div>
          <!--social-timeline-blog-->

          <!--============= Social Timeline Blog without image ======================-->
          <div class="social-timeline-video inner-2x innerMTB">
            <div class="social-card card half innerAll" id="social-timeline-blog-noimage">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-8">
                      <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                      </div>
                      <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                          <a href="#">
                            tom hanks
                          </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                          <a href="#">
                            @tomhankennew
                          </a>
                        </div>
                        <!--social-user-link-->
                      </div>
                      <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4">
                    </div>
                  </div>
                  <!--row-->
                </div>
                <!--social-timeline-details-->

                <div class="social-timeline-keywords-details">
                  <div class="row half innerT">
                    <div class="col-xs-12">
                      <a href="#" class="social-keywords-tags">#bitcoin</a>
                      <a href="#" class="social-keywords-tags half innerL">#India</a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->


                <!--social-timeline-content-blog-nothumb-->
                <div class="social-timeline-content-blog-nothumb half innerMT">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="blog-nothumb-content">
                        <a data-id="social-timeline-blog-noimage " onclick="test(this)">
                          <span class="blog-nothumb-head">
                            I Made $700 With My First Post & $0.06 With My Third - Steemit: A Lesson in
                            Free Market Economics.
                          </span>
                          <span class="blog-nothumb-body">
                            When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                            I was equal parts excited & skeptical about the whole thing. Then I made my first
                            post and after 24 hours I was a believer.
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--social-timeline-content-blog-nothumb-->
            </div>
            <!--social-card-->
          </div>
          <!--social-timeline-blog-->

          <!--============= Social Timeline Blog ======================-->
          <div class="social-timeline-video inner-2x innerMTB">
            <div class="social-card card half innerAll">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-8">
                      <div class="social-timeline-profile-pic pull-left">
                        <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                      </div>
                      <div class="social-timeline-profile-details pull-left innerL">
                        <div class="social-user-link-name">
                          <a href="#">
                            tom hanks
                          </a>
                        </div>
                        <!---->
                        <div class="social-user-link">
                          <a href="#">
                            @tomhankennew
                          </a>
                        </div>
                        <!--social-user-link-->
                      </div>
                      <!--social-timeline-profile-details-->
                    </div>
                    <div class="col-xs-4">
                    </div>
                  </div>
                  <!--row-->
                </div>
                <!--social-timeline-details-->

                <div class="social-timeline-keywords-details">
                  <div class="row half innerT">
                    <div class="col-xs-12">
                      <a href="#" class="social-keywords-tags">#bitcoin</a>
                      <a href="#" class="social-keywords-tags half innerL">#India</a>
                    </div>
                  </div>
                </div>
                <!--social-timeline-tags-details-->


                <!--social-timeline-content-blog-nothumb-->
                <div class="social-timeline-content-blog-nothumb half innerMT">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="blog-nothumb-content">
                        <a>
                          <span class="blog-nothumb-head">
                            I Made $700 With My First Post & $0.06 With My Third - Steemit: A Lesson in
                            Free Market Economics.
                          </span>
                          <span class="blog-nothumb-body">
                            When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                            I was equal parts excited & skeptical about the whole thing. Then I made my first
                            post and after 24 hours I was a believer.
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!--social-timeline-content-blog-nothumb-->
            </div>
            <!--social-card-->
          </div>
          <!--social-timeline-blog-->

          <!--Social ADDS-->
          <div class="social-timeline-video inner-2x innerMTB">
            <div class="social-card card half innerAll">
                <div class="">
                  <div class="row half innerTB">
                    <div class="col-xs-12">
                      <img src="<?php echo $rootUrlImages?>social/adds.png" class=" img-responsive"  />
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <!--============= Social Timeline Status ======================-->
            <div class="social-timeline-status">
              <div class=" half innerAll card" id="social-timeline-status">
                  <div class="">
                    <div class="row half innerTB">
                      <div class="col-xs-8">
                        <div class="social-timeline-profile-pic pull-left">
                          <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                        </div>
                        <div class="social-timeline-profile-details pull-left innerL">
                          <div class="social-user-link-name">
                            <a href="#">
                              tom hanks
                            </a>
                          </div>
                          <!---->
                          <div class="social-user-link">
                            <a href="#">
                              @tomhankennew
                            </a>
                          </div>
                          <!--social-user-link-->
                        </div>
                        <!--social-timeline-profile-details-->
                      </div>
                      <div class="col-xs-4">
                      </div>
                    </div>
                    <!--row-->
                  </div>
                  <!--social-timeline-details-->

                  <div class="social-timeline-keywords-details">
                    <div class="row half innerT">
                      <div class="col-xs-12">
                        <a href="#" class="social-keywords-tags half innerR">#bitcoin</a>
                        <a href="#" class="social-keywords-tags half innerR">#India</a>
                      </div>
                    </div>
                  </div>
                  <!--social-timeline-tags-details-->


                  <!--social-timeline-content-blog-nothumb-->
                  <div class="social-timeline-user-status half innerMT">
                    <div class="row">
                      <div class="col-xs-12">
                        <div class="user-status-content">
                          <a data-id="social-timeline-status" onclick="test(this)">
                            <span class="text-dark-gray">
                              When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                                I was equal parts excited & skeptical about the whole thing. Then I made my first
                                post and after 24 hours I was a believer.
                            </span>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!--social-timeline-content-blog-nothumb-->
              </div>
              <!--social-card-->
            </div>
            <!--social-timeline-blog-->

            <!--============= Social Timeline soundcloud ======================-->
            <div class="social-timeline-sound inner-2x innerMTB">
              <div class="social-card card half innerAll" id="social-timeline-sound">
                  <div class="">
                    <div class="row half innerTB">
                      <div class="col-xs-8">
                        <div class="social-timeline-profile-pic pull-left">
                          <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
                        </div>
                        <div class="social-timeline-profile-details pull-left innerL">
                          <div class="social-user-link-name">
                            <a href="#">
                              tom hanks
                            </a>
                          </div>
                          <!---->
                          <div class="social-user-link">
                            <a href="#">
                              @tomhankennew
                            </a>
                          </div>
                          <!--social-user-link-->
                        </div>
                        <!--social-timeline-profile-details-->
                      </div>
                      <div class="col-xs-4">
                      </div>
                    </div>
                    <!--row-->
                  </div>
                  <!--social-timeline-details-->

                  <div class="social-timeline-keywords-details">
                    <div class="row half innerT">
                      <div class="col-xs-12">
                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                      </div>
                    </div>
                  </div>
                  <!--social-timeline-tags-details-->

                  <!--====social-timeline-user-message====-->
                  <div class="social-timeline-content-message">
                    <div class="row">
                      <div class="col-xs-12">
                        <p class="innerMB">
                          The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
                          Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                        </p>
                      </div>
                    </div>
                  </div>
                  <!--social-timeline-content-details-->

                  <!--social-timeline-details-->
                  <div class="social-timeline-sound">
                    <div class="row">
                      <div class="col-xs-12">
                       <a onclick="test(this)" data-id="social-timeline-sound">
                        <iframe width="100%" height="150" scrolling="no" frameborder="no"
                        src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/298167372&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">
                      </iframe>
                      </a>
                      </div>
                    </div>
                  </div>
                  <!--social-timeline-content-blog-image-->
              </div>
              <!--social-card-->
            </div>
            <!--social-timeline-soundcloud-->


    </div>
    <!-- social-center-panel  -->
  </div>
    <!-- social-center-panel  -->
	<!-- col-xs-6 -->
  <div class="col-xs-3">
    <div class="social-right-panel">
      <div class="row">
				<div class="col-xs-12">
					<div class="pull-left half search-box-comments" style="width:100%;">
							<div class="input-group" style="width:100%;">
									<input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="" style="width:100%;">
									<input type="submit" class="social-search-btn-header" value="">
							</div>
					</div>
				</div>
			</div>
      <div class="card left-panel-modules inner-4x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Type</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>All</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Audio</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Video</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Blog</a>
						</div>
					</div>
				</div>
			</div>
      <!--Settings and Preferences-->
    </div>
  </div>


</div>
<!-- container -->
</main>


<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
