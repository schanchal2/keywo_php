<?php
session_start();

/* Add DB Management Files */
require_once('../../config/config.php');
require_once('../../config/db_config.php');

/*Add helpers */
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once ('../../helpers/deviceHelper.php');

/* Add Model */
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        $bulkData                = array();
		$userId                  = $_SESSION['id'];
		$postId                  = cleanXSS(trim(urldecode($_POST['postId'])));
		$postTime                = cleanXSS(trim(urldecode($_POST['postTime'])));
		$searchCount             = cleanXSS(trim(urldecode($_POST['searchCount'])));
		$ipAdd                   = cleanXSS(trim(urldecode($_POST['ipAddress'])));
		$email                   = cleanXSS(trim(urldecode($_POST['email'])));
		$userSearchType          = cleanXSS(trim(urldecode($_POST['userSearchType'])));
        $emailofSpecialUser      = array();
		$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

        $user_id     ="user_id,account_handle";
        $getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
        if(noError($getUserInfo)){
            $getUserInfo = $getUserInfo["errMsg"];
            $user_id     = $getUserInfo["user_id"];
            $accHandle   = $getUserInfo["account_handle"];
        }
        //Get Special User
//        $getSpecialUserData = getSpecialUserPost();
//        if(noError($getSpecialUserData)){
//            $getSpecialUserData = $getSpecialUserData["errMsg"];
//        }
//        if($getSpecialUserData!="No Result") {
//            foreach ($getSpecialUserData as $specialUser) {
//                $emailofSpecialUser[] = $specialUser['email'];
//            }
//        }

        if(!in_array($email,$emailofSpecialUser)){
            //get Folllowed User
            $getFollowed         = getFollowUser($targetDirAccountHandler);
            //check for value
            $followFlag          = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

        }else{
            $followFlag = 1;
        }

        $postDetail = getPostDetail($postId, $postTime,$_SESSION['id'],$followFlag);

        if (noError($postDetail)) {
            $postDetail                = $postDetail['errMsg'][0];
            // printArr($postDetail);
            $post_earnings             = $postDetail['post_earnings'];
            $accountHandle             = $postDetail['user_ref']['account_handle'];
            $emailId                   = $postDetail['user_ref']['email'];
            $postType                  = $postDetail['post_type'];
            $postedBy                  = $postDetail['posted_by'];
            $postCollectionName        = $postDetail['post_collection'][0];

            if(isset($postDetail['post_details']['img_file_name'])){
                $filename                  = $postDetail['post_details']['img_file_name'];
            }else{
                $filename                  = " ";
            }

            $createdAt                 = $postDetail['created_at'];
            $mil                       = $postDetail['created_at'];
            $seconds                   = $mil / 1000;
            $created_at                = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
            $timestamp2                = strtotime($created_at);
            $ext                       = explode('.', $filename);
            $postViewCountDisplay      = $postDetail['post_views_count'];

            // varibales of shared post content
            if(isset($postDetail["post_details"]["parent_post"]["post_id"])){
                $postSharedParent                  = $postDetail["post_details"]["parent_post"]["post_id"];
            }else{
                $postSharedParent                  = " ";
            }

            //printArr($postSharedParent);

            if(isset($postSharedParent["_id"])){
                $postSharedParentId        = $postSharedParent["_id"];
            }else{
                $postSharedParent          = " ";
            }

            if(isset($postSharedParent["_id"])){
                $postSharedParentId        = $postSharedParent["_id"];
            }else{
                $postSharedParentId          = " ";
            }

            if(isset($postSharedParent["user_ref"]["first_name"])){
                $postSharedParentFname     = $postSharedParent["user_ref"]["first_name"];
            }else{
                $postSharedParentFname          = " ";
            }

            if(isset($postSharedParent["user_ref"]["last_name"])){
                $postSharedParentLname     = $postSharedParent["user_ref"]["last_name"];
            }else{
                $postSharedParentLname          = " ";
            }

            if(isset($postSharedParent["user_ref"]["user_id"])){
                $postSharedParentUserId    = $postSharedParent["user_ref"]["user_id"];
            }else{
                $postSharedParentUserId          = " ";
            }

            if(isset($postSharedParent["user_ref"]["account_handle"])){
                $postSharedParentAccHandle = $postSharedParent["user_ref"]["account_handle"];
            }else{
                $postSharedParentAccHandle          = " ";
            }

            if(isset($postSharedParent["user_ref"]["email"])){
                $postSharedParentEmail      = $postSharedParent["user_ref"]["email"];
            }else{
                $postSharedParentEmail          = " ";
            }

            if(isset($postSharedParent["posted_by"])){
                $postSharedParentPostedBy  = $postSharedParent["posted_by"];
            }else{
                $postSharedParentPostedBy          = " ";
            }

            if(isset($postSharedParent["created_at"])){
                $postSharedCreatedAt       = $postSharedParent["created_at"];
            }else{
                $postSharedCreatedAt          = " ";
            }

            if(isset($postSharedParent["keywords"][0])){
                $postSharedKeyword         = $postSharedParent["keywords"][0];
            }else{
                $postSharedKeyword          = " ";
            }

            if(isset($postSharedParent["keywords"])){
                $postSharedKeywords        = $postSharedParent["keywords"];
            }else{
                $postSharedKeywords          = " ";
            }

            if(isset($postSharedParent["post_short_desc"])){
                $postSharedShortDesc       = $postSharedParent["post_short_desc"];
            }else{
                $postSharedShortDesc          = " ";
            }

            if(isset($postSharedParent["post_type"])){
                $postSharedType            = $postSharedParent["post_type"];
            }else{
                $postSharedType          = " ";
            }

            if(isset($postSharedParent["post_details"]["asset_url"])){
                $postSharedAssetUrl        = $postSharedParent["post_details"]["asset_url"];
            }else{
                $postSharedAssetUrl          = " ";
            }


            if(isset($postSharedParent["post_details"]["blog_title"])){
                $postSharedBlogTitle       = $postSharedParent["post_details"]["blog_title"];
            }else{
                $postSharedBlogTitle          = " ";
            }

            if(isset($postSharedParent["post_details"]["img_file_name"])){
                $postSharedImgUrl          = $postSharedParent["post_details"]["img_file_name"];
            }else{
                $postSharedImgUrl          = " ";
            }

            if(isset($postSharedParent["post_details"]["blog_content"])){
                $postSharedBlogContent     = $postSharedParent["post_details"]["blog_content"];
            }else{
                $postSharedBlogContent          = " ";
            }

            if(isset($postSharedParent["post_earnings"])){
                $post_share_earnings_parent = $postSharedParent["post_earnings"];
            }else{
                $post_share_earnings_parent          = " ";
            }

            if(isset($postDetail["user_ref"]["profile_pic"]) && !empty($postDetail["user_ref"]["profile_pic"])){
                $extensionUP  = pathinfo($postDetail["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $postDetail["user_ref"]["account_handle"] . '/profile/' . $postDetail["user_ref"]["account_handle"] . '_' . $postDetail["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$postDetail["user_ref"]["account_handle"].'/profile/'.$postDetail["user_ref"]["account_handle"].'_'.$postDetail["user_ref"]["profile_pic"];

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                  $imgSrc = $rootUrlImages."default_profile.jpg";
            } 

            //get list of keywords
            if($postType == 'share'){
                $keywords = $postSharedParent['keywords'];
            }else{
                $keywords = $postDetail["keywords"];
            }
            $keywordsList = implode(' ', $keywords);

            //get user ip address
            $ipAddr = getClientIP();

            //post view count on display popup
            $postViewCount    = postViewCount($postId, $postTime);
            //printArr($postViewCount);

            //Check Like post details
            $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);
            //check cdp status
            $getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);

            $postIddata = array(array(
            'post_id' => $postId,
            'time'    => $postTime
            ));

            $bulkPostId = json_encode($postIddata);
            //Get like post activity count from node database
            $activityCount = getPostActivityCount($bulkPostId, '');

            $targetActivityCount = $activityCount['errMsg'][$postId];
            $like_count          = $targetActivityCount['like_count'];
            $commentCount        = $targetActivityCount['comment_count'];
            $shareCount          = $targetActivityCount['share_count'];

           // $parentViewCount     = $targetActivityCount['parent_view_count'];


            if(isset($targetActivityCount['parent_like_count']))
            {
                $parentLikeCount = $targetActivityCount['parent_like_count'];
            }else
            {
                $parentLikeCount = "";
            }

            if(isset($targetActivityCount['parent_view_count']))
            {
                $parentViewCount = $targetActivityCount['parent_view_count'];
            }else
            {
                $parentViewCount = "";
            }


            if(isset($targetActivityCount['parent_comment_count']))
            {
                $parentCommentCount = $targetActivityCount['parent_comment_count'];
            }else
            {
                $parentCommentCount = "";
            }
            if(isset($targetActivityCount['parent_share_count']))
            {
                $parentShareCount = $targetActivityCount['parent_share_count'];
            }else
            {
                $parentShareCount = "";
            }

            //path for json directory
            $targetDirAccountHandler  = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";


            // $postSharedParenEmail
            if(isset($postSharedParent["posted_by"])){
                $postCreatorId = $postSharedParent["posted_by"];
            }else{
                $postCreatorId = " ";
            }

             $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
             $userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);
             if (in_array($postCreatorId,$userIdsOfAccHandler)) {
                 $blockFlagStatus = '1';
            }else{
                 $blockFlagStatus = '0';
            }
            //Check account is blocked is not
            if($blockFlagStatus=='1' ) { ?>
            <script>$('.close-button').hide();</script>
            <?php
            print("<script>");
            print("var t = setTimeout(\"postDeletedModal('This accout is  blocked!!!!'); postLoadAsPerUrl(); \", 000);");
            print("</script>");
            exit;	}else{ ?>
            <script>$('.close-button').show();</script>
            <?php }

			//Get bookmark Details
			$getBookMarkPost = getBookMarkPost($_SESSION["account_handle"],$postId, $postType );
		?>
		<script>$('#postDeletedModal').modal('hide');</script>

<input type="hidden" id="post_collection<?php echo $postId; ?>" value="<?php echo $postCollectionName; ?>">
<div class="social-card card innerT innerLR">
    <div class="social-card-created-at">
      <div class="row">
        <div class="col-xs-8">
          <div class="social-timeline-profile-pic pull-left">
            <img src="<?php echo $imgSrc; ?>" class=" img-responsive">
          </div>
          <div class="social-timeline-profile-details pull-left innerL">
						<div class="social-user-link-name">
							<?php if ($_SESSION['id'] == $postedBy) { ?>
								<a href="../../views/social/privateTimeline.php">
							<?php } else { ?>
								<a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($emailId); ?>" target="_blank">
							<?php } ?>
								<?php echo $postDetail["user_ref"]["first_name"]; ?> <?php echo $postDetail["user_ref"]["last_name"]; ?>
							</a>
						</div>
						<!---->
						<div class="social-user-link">
							<?php if ($_SESSION['id'] == $postedBy) { ?>
										<a href="../../views/social/privateTimeline.php">
							<?php } else { ?>
								<a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($emailId); ?>" target="_blank">
								<?php } ?>
								<?php echo '@' . $postDetail["user_ref"]["account_handle"]; ?>
						</div>
            <!--social-user-link-->
          </div>
          <!--social-timeline-profile-details-->
        </div>
				<div class="col-xs-4 social-post-options">
						<?php
							if($postType == 'share'){
								$emailId = $postSharedParentEmail;
							}
						 include('commonDropDownOption.php');?>
				</div>
      </div>
      <!--row-->
    </div>
    <!--social-timeline-details-->

    <div class="social-timeline-keywords-details">
      <div class="row half innerT">
				  <?php
						if(isset($postDetail["keywords"]) && !empty($postDetail["keywords"])) {
							$keywords = $postDetail["keywords"];
						} elseif (isset($postSharedKeyword) && !empty($postSharedKeyword)) {
							$keywords = $postSharedKeywords;
						}
					?>
					<div class="col-xs-8">
						<?php
							foreach ($keywords as $keywo) {
						?>
						<a href="viewKeywoTimeline.php?keyword=<?php echo urlencode($keywo);?>" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="<?php echo $keywo ; ?>">#<?php echo $keywo ; ?></a>
						<?php } ?>
					</div>
					 <div class="col-xs-4">
						<a href="socialPostDetails.php?post_id=<?php echo base64_encode($postId); ?>&timestamp=<?php echo $postTime; ?>&posttype=<?php echo base64_encode($postType); ?>&email=<?php echo base64_encode($email); ?>" class="text-deep-sky-blue pull-right" target="_blank">Go to post page</a>
					</div>
			</div>
    </div>

    <!--social-timeline-tags-details-->

    <!--social-timeline-user-message-->
    <div class="social-timeline-content-message">
      <?php if ($postType == "status") { ?>
      <div class="row border-top border-bottom">
        <?php } else { ?>
          <div class="row">
      <?php } ?>
        <div class="col-xs-12">
          <div class="innerTB half">
          <p class="text-dark-gray">
            <?php
            //get short description with mention links

            $shortDescription = "";
            if(isset($postDetail["post_mention"])){
                $shortDescriptionText = getLinksOnText(rawurldecode($postDetail["post_short_desc"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
                $shortDescription     = $shortDescriptionText["text"];
                $counterText          = $shortDescriptionText["counter"];
            }else{
                $shortDescriptionText = " ";
            }


            //printArr($postDetail);die;
            if($postType != "blog") {
            //     echo $postDetail['post_details']['blog_title'];
            // } else {
                echo $shortDescription;
            }
            ?>
          </p>
          </div>
        </div>
      </div>
    </div>


    <!--social-timeline-tags-details-->

    <!--social-timeline-video-->
    <?php
    	switch ($postType) {
            case 'video':
    ?>
    <div id="myModal" class="social-timeline-content-video">
      <div class="row">
        <div class="col-xs-12">
          <div align="center" class="embed-responsive embed-responsive-16by9" data-toggle="modal" data-target="#status_video_modal">
            <!-- <video loop class="embed-responsive-item">
                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
            </video> -->
        <iframe  id="cartoonVideo" width="560" height="258" src="<?php echo  $postDetail['post_details']["asset_url"] . '?autoplay=1'; ?>" frameborder="0" allowfullscreen></iframe>
        </div>
        </div>
      </div>
    </div>
    <?php
    		break;
       		case 'audio':
    ?>
    <div class="social-timeline-sound">
      <div class="row">
        <div class="col-xs-12">
         <a data-id="social-timeline-sound">
             <?php if (strpos($postDetail["post_details"]["asset_url"], 'https://soundcloud.com/') !== false) { ?>
                 <a href="<?php echo $postDetail["post_details"]["asset_url"]; ?>" target="_blank"><?php echo $postDetail["post_details"]["asset_url"]; ?></a>
             <?php } elseif (strpos($postDetail["post_details"]["asset_url"], 'https://w.soundcloud.com/') !== false) { ?>
                 <iframe width="100%" height="150" scrolling="no" frameborder="no" src="<?php echo $postDetail["post_details"]["asset_url"]; ?>">
                 </iframe>
             <?php } ?>
        </a>
        </div>
      </div>
    </div>
    <?php
    	break;
      case 'blog':
    ?>
    <div class="social-timeline-content-blog-thumb word-wrap">
      <?php  if(isset($postDetail["post_details"]["img_file_name"]) && !empty($postDetail["post_details"]["img_file_name"])) { ?>
        <span class="blog-nothumb-head">
        <?php echo $postDetail['post_details']['blog_title']; ?>
        </span>
        <div class="row">
          <div class="col-xs-12 post-display-img">
  					<a data-toggle="modal" data-target="#status_blog_image_modal">
            <?php
            //CDN image path
            $imageFileOfCDN =  $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/blog/featured/" . $ext[1] . "/" . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"];

            //server image path
            $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postDetail["user_ref"]["account_handle"] . '/post/blog/featured/' . $ext[1] . '/' . $postId . '_' . $createdAt  . '_' . $postDetail["post_details"]["img_file_name"];

            // check for image is avilable on CDN
            $file = $imageFileOfCDN;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
              $finalImagePath = $imageFileOfLocal;
            } else {
              $finalImagePath = $imageFileOfCDN;
            } ?>
            <img src="<?php echo $finalImagePath ; ?>" class="img-responsive main-img">
            </a>
  				</div>
        </div>
      <?php } else {
        $blogDescriptionText = getLinksOnText(rawurldecode($postDetail["post_details"]["blog_content"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
        $blogDescription            = $blogDescriptionText["text"];
        $descBlogLenWithoutStriptag = strlen($blogDescription);
        $descBlogLenWithStriptag    = strlen(strip_tags($blogDescription));
        $diffBlog                   = $descBlogLenWithoutStriptag - $descBlogLenWithStriptag;
        $blogData                   = ($descBlogLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($blogDescription, 0, $validateCharCountOnSocial + $diffBlog) 
        . '<a href="socialPostDetails.php?post_id='.base64_encode($postId).'&timestamp='.$postTime.'" target="_blank"><span class="text-blue">....See More</span></a>' : $blogDescription;
			?>
        <div class="row half innerTB border-top border-bottom">
					<div class="col-xs-12">
            <span class="blog-nothumb-head">
              <?php echo $postDetail['post_details']['blog_title']; ?>
            </span>
					<?php echo $blogData; ?>
				  </div>
        </div>
      <?php } ?>
    </div>

    <?php
      break;
      case 'image':
    ?>

    <div class="social-timeline-content-image">
      <div class="row">
        <div class="col-xs-12 post-display-img">
        <a data-toggle="modal" href="#status_image_modal">
            <?php

            $extension = pathinfo($postDetail["post_details"]["img_file_name"], PATHINFO_EXTENSION);

            //CDN image path
            $imageFileOfCDN =  $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"] ;

            //server image path
             $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postDetail["user_ref"]["account_handle"] . '/post/images/' . $ext[1] . '/' . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"];

            // check for image is avilable on CDN
            $file = $imageFileOfCDN;
            $file_headers = @get_headers($file);
            if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $finalImagePath = $imageFileOfLocal;
            }
            else {
                $finalImagePath = $imageFileOfCDN;
            }

            ?>
          <img src="<?php echo $finalImagePath ?>" class="img-responsive main-img">
        </a>
        </div>
      </div>
    </div>


    <?php
    		break;
       		case 'share':
					//printArr($postDetail); die;
    ?>
   <div class="social-timeline-user-status">
     <div class="row">
       <div class="col-xs-12">
         <div class="user-status-content  half innerTB">
           <span class="text-dark-gray">
           <div class="row border-top border-bottom">
						 	<?php include("shareParentPostContent.php"); ?>
              </div>
           </span>
         </div>
       </div>
     </div>
	 </div>


    <?php
    		break;
    	}
    ?>
    <!--social-timeline-video-->

		<?php //if($postType!="status") {?>
    <div class="social-timeline-earning-comments-view innerT">
      <div class="row">
        <div class="col-xs-5">
          <div>
            <label>Earning : </label>
              <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                    origPrice="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                        </div>
        </div>
        <div class="col-xs-7">
          <div class="">
            <ul class="list-inline pull-right margin-bottom-none">
              <li>
                  <div>
                    <?php if (!empty($like_count)) { ?>
                      <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postTime; ?>','0', '30' ,'<?php echo $like_count; ?>');">
                      <span class="half innerR like-count<?php echo $postId ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                    <?php } else { ?>
                      <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $postTime; ?>', '0', '30','<?php echo $like_count;?>');">
                      <span class="half innerR like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                    <?php }  ?>
                    <label class="pull-right">Likes</label>
                  </div>
                </li>
              <li>
                <div>
                  <span class="half innerR comment-count<?php echo $postDetail['_id']; ?>"><?php echo formatNumberToSort("{$commentCount}", 0);?></span>
                  <label class="pull-right">Comments</label>
                </div>
              </li>
              <li>
                <div>
                  <span class=""><?php echo formatNumberToSort("{$shareCount}", 0);?></span>
                  <label>Shares</label>
                </div>
              </li>
             <!--  <li class="padding-right-none">
                <div>
                  <span class=""><?php echo formatNumberToSort("{$postViewCountDisplay}", 0);?></span>
                  <label>Views</label>
                </div>
              </li> -->
            </ul>
          </div>
        </div>
      </div>
    </div>
		<?php //} ?>
    <div class="social-timeline-likes-dislikes">
      <div class="row">
        <div class="col-xs-6">
          <div class="social-likes-dislikes">
            <ul class="list-inline margin-bottom-none">
              <li>
                <div>
                    <a href="JavaScript:void(0);" class="like-area">
                          <?php
                          if($postType == "share"){
                              $CDPSharedBy = $postedBy;
                              $CDPPostedBy = $postSharedParentPostedBy;
                          } else {
                              $CDPSharedBy = '';
                              $CDPPostedBy = $postedBy;
                          }
                          if($getPostCdpStatus == 1) { ?>
                            <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-coin-hand half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                          <?php } elseif($getLikePostStatus == 1 || $_SESSION["likeIconDisableStatus"] == 1) { ?>
                              <i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>
                          <?php } else {
                               if ($userId != $postedBy) { ?> <!--  &&  ($userSearchType == "Qualified")) { ?> -->
                                    <i id="thumbs-up<?php echo $postId; ?>" class="likes-click fa fa-thumbs-o-up half innerR" onclick="socialCDPForPost(this,'<?php echo $postId; ?>', '<?php echo $postTime; ?>', '<?php echo $postType; ?>', '<?php echo $accountHandle; ?>', '<?php echo $CDPPostedBy; ?>', '<?php echo $CDPSharedBy; ?>', '<?php echo $searchCount; ?>', '<?php echo $ipAddr; ?>', '<?php echo $keywordsList; ?>', '<?php echo $email; ?>');"></i>
                               <?php } else { ?>
                                   <i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>

                               <?php }
                          } ?>
                        </a>
                </div>
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="social-comments-shares-views">
            <ul class="list-inline pull-right margin-bottom-none">
              <li>
                <div>
                  <a onclick = "commentAppendData('function','','3', '', '<?php echo $postId;?>');">
                    <i class="fa fa-comments-o"></i>
                    <span>Comments</span>
                  </a>
                </div>
              </li>
              <li class="padding-right-none">
                <div>
									<a data-toggle="modal" onclick = "sharePostModal('<?php echo $postId; ?>','<?php echo $createdAt; ?>','<?php echo $postType;?>','<?php echo $postDetail["user_ref"]["account_handle"];?>', 'create', '<?php echo $postDetail["user_ref"]["email"];?>');">

                    <i class="fa fa-share-square-o"></i>
                    <span>Share</span>
                  </a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
</div>
<div class="innerLR">
<div id = "comment-include-<?php echo $postDetail['_id']; ?>" class="comments-module bg-light-purple row" style="//display:none;" >

                                        <div class="col-xs-12">
  <?php
  $commentPostId = $postDetail['_id'];
  $commentPostCreationTime = $createdAt;
		if($postType == 'share'){
			$commentPostCreator  = $postSharedParentEmail;
		}else{
			  $commentPostCreator = $postDetail['user_ref']['email'];
		}
    include ('postPageCommentWidget.php');
    // echo "<script>commentDivShow("+$commentPostId+");</script>"
  ?></div>

</div></div>

<?php if($postType=="image" || $postType=="status"){
 ?>
 <!--social-card-->

	<?php } ?>
<?php } else {
	?>
		<div id = "social-post-delete-div<?php  echo $postId; ?>" class="social-timeline-content-message" modal-show = 'none' style="display:none;"></div>
	<?php
			}
		} else {
			print("<script>");
			print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
			print("</script>");
			die;
		} ?>
<script>
/*------------mention users------------------*/
var tags           = [];
var suggestionData = [];
$(function() {
  $(".can-mention-user").on("keyup", function(e) {
      if ($(this).val().length >= 1) {
          mentionOnKeyup($(this).val());
      }
  });
});

function mentionOnKeyup(mentionInput) {
    var atkeyword = mentionInput.split(' ');
    if ((atkeyword[atkeyword.length - 1]).charAt(0) == '@') {
        var getSuggestion = (atkeyword[atkeyword.length - 1]).substring(1);
        if (getSuggestion != "") {
            $.ajax({
                type: 'POST',
                dataType: 'JSON',
                data: { lastChar: getSuggestion },
                url: '../../controllers/social/getMentionUser.php',
                async: false,
                success: function(data) {
                    if (data.errCode == -1) {
                        tags = [];
                        for (var i = 0; i < data.errMsg.length; i++) {
                            tags.push('{"name":"' + data.errMsg[i].first_name + ' ' + data.errMsg[i].last_name + '","img":"' + data.errMsg[i].profile_pic + '","job":"' + data.errMsg[i].account_handle + '","email":"' + data.errMsg[i].email + '"}');
                        }
                        suggestionData = $.map(tags, function(values) {
                            var value = JSON.parse(values);
                            return { 'name': value.name, 'img': "../../images/rightpanel-img/" + value.img, 'job': value.job, 'email': value.email };
                        });
                    }
                },
                error: function() {
                    return false;
                }
            });
            if (getSuggestion.length == 1) {
                $('.inputor').atwho({
                    at: "@" + getSuggestion,
                    data: suggestionData,
                    limit: 10,
                    displayTpl: "<li class='sugg-wrapper'><img class='sugg-img' src='${img}' height='32' width='32'/><div class='sugg-name'> ${name}<div class='keyhandle'>${job}</div> </div><div class='hidden selectedEmail'>${email}</div></li>",
                    insertTpl: '@${job}',
                });
                $('.inputor').on("inserted.atwho", function(event, query) {
                 var htmlText   = query.html(query.html());
                 console.log(htmlText.find(".selectedEmail").html());
               });
            }
        }
    }
}
/*------------end of mention users------------------*/
</script>
    <script>
        //tooltip
        $('[data-toggle="tooltip"]').tooltip({
            trigger:'hover'
        });
    </script>