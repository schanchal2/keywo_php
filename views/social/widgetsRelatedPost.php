<?php
// printArr($_SESSION);
$popularHandlerName      = $_SESSION['account_handle'];
$popularType             = $postDetail['post_type'];
$popularPostId           = $postId;
$popularPostCreationTime = $postTime;
$popularKeywords         = $postDetail['keywords'];

global $cdnSocialUrl;
global $rootUrlImages;

if ($popularType == 'image') {
  $popularLimit = '9';
} else {
  $popularLimit = '3';
}
// printArr($popularKeywords);

if ($popularType == 'share') {
  $popularType = $postSharedParent["post_type"];
  $popularKeywords = $postSharedParent["keywords"];
}
$popularKeywords = json_encode($popularKeywords);
$getRelatedPostDetails = getRelatedPostDetails($popularHandlerName, $popularType, $popularPostId, $popularPostCreationTime, $popularKeywords, 50);
// printArr($getRelatedPostDetails);
// printArr($popularType);

//get blocked array
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);

  if (noError($getRelatedPostDetails)) {
    $getRelatedPostDetails = $getRelatedPostDetails['errMsg'];
    if (count($getRelatedPostDetails) > 0) {

?>
      <div class="row bottom-related-post">
        <div class="col-xs-12 innerT">
          <h5 class="text-social-primary"><strong>Related <?php echo ucwords($popularType); ?></strong></h5>
        </div>

<?php
      $postDesc = '';
      $postDescCount = '';
      $postDescText = '';
      switch ($popularType) {
        case 'video':
          foreach ($getRelatedPostDetails as $key => $value) {
            if(!in_array($value['posted_by'],$userIdsOfAccHandler)){
              $listCount = $listCount + 1;
              if ($listCount > 3) {
                break;
              }
          // to get Thumbnail image of video url.
?>
  <!--bg-light-gray-->

          <div class="col-xs-4">
            <div class="card social-card" >
              <div class="row margin-none video-module border-bottom">
                <div class="row margin-none innerT" onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                  <div class="col-xs-4">
                    <a>
                        <?php
                        if(isset($value["post_details"]["video_thumbnail"]) && !empty($value["post_details"]["video_thumbnail"])){
                            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $value["user_ref"]["account_handle"] . '/post/video/' . $value["post_details"]["video_thumbnail"];

                            //server image path
                            $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$value["user_ref"]["account_handle"].'/post/video/'.$value["post_details"]["video_thumbnail"];

                            // check for image is available on CDN
                            $file = $imageFileOfCDNUP;
                            $file_headers = @get_headers($file);
                            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                $imgSrc = $imageFileOfLocalUP;
                            } else {
                                $imgSrc = $imageFileOfCDNUP;
                            }
                        }
                        ?>

                      <img class="popular_blog_img" src="<?php echo $imgSrc; ?>">
                    </a>
                  </div>
                  <div class="col-xs-8">
                  <a>
                      <span class="ellipsis-multiline text-black">
                        <?php
                        $videoTitle = $value['post_details']['video_desc'];
                        if (empty($value['post_short_desc']) && !(empty($videoTitle)) ) {;
                            $postDesc = $videoTitle;
                        } else if (!empty($value['post_short_desc'])) {
                            $postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
                        } else {
                            $postDesc = 'Title Unavailable';
                        }
                        $postDescCount = strlen( $postDesc);
                        if ($postDescCount >= 50) {
                            $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                        } else {
                            $postDescText = $postDesc;
                        }
//                        printArr($postDescText);
                        echo rawurldecode($postDescText);
                        ?>
                      </span>
                    </a>
                  </div>
                </div>
                <div class="row margin-none half innerB">
                  <div class="col-xs-8 text-black">
                    <label>Earning : </label>
                    <span>
                                   <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                         origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                    </span>
                  </div>
                  <div class="col-xs-4" onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>);?>");'>
                    <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } } ?>
<?php
        break;
        case 'audio':
          foreach ($getRelatedPostDetails as $key => $value) {
            if(!in_array($value['posted_by'],$userIdsOfAccHandler)){
              $listCount = $listCount + 1;
              if ($listCount > 3) {
                break;
              }
            $ext         = explode(".",$value['post_details']['img_file_name']);
?>

          <div class="col-xs-4">
            <div id="popular-sound-module" style="" >
              <div class="card social-card">
                <div class="row margin-none sound-module border-bottom">
                  <div class="row margin-none innerT" onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                    <div class="col-xs-4">
                      <img class="popular_blog_img" src="<?php echo $rootUrl; ?>/images/social/sound-trending.png">
                    </div>
                    <div class="col-xs-8">
                      <a class="ellipsis-multiline text-black l-h15">
                        <?php
                          $postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
                          $postDescCount = strlen( $postDesc);
                          if ($postDescCount >= 50) {
                            $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                          } else {
                            $postDescText = $postDesc;
                          }
                          echo rawurldecode($postDescText);
                        ?>
                      </a>
                      <span class="" style="display:block;">
                        <!-- By Doors Down -->
                      </span>
                    </div>
                  <!--   <div class="col-xs-3">
                      <span class="text-gray">
                         <span>3.45 mins</span>
                      </span>
                    </div>  -->
                  </div>
                  <div class="row margin-none innerT">
                    <div class="col-xs-8 text-black">
                      <label>Earning : </label>
                      <span>
                                      <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                            origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                      </span>
                    </div>
                    <div class="col-xs-4" onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                      <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
                    </div>
                  </div>

                </div>
              </div>

              </div>
            <!--popular-sound-module-->
          </div>
          <?php } } ?>
<?php
        break;
        case 'image':
        // printArr($getRelatedPostDetails);
          for ($i = 0; $i < count($getRelatedPostDetails); $i++) {
            if(!in_array($value['posted_by'],$userIdsOfAccHandler)){
              $listCount = $listCount + 1;
              if ($listCount > 3) {
                break;
              }
?>
          <div class="col-xs-4">
            <div class="popular-image-module" >
              <div class="card social-card right-panel-modules">
                <div class="row margin-none blog-module border-bottom">
                  <div class="row margin-none innerTB">
                  <?php for ($j = $i; $j <= ($i+2) && $j < count($getRelatedPostDetails) ; $j++) { ?>
                    <div class="col-xs-4" onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($getRelatedPostDetails[$j]['_id']) . '&timestamp=' . $getRelatedPostDetails[$j]['created_at'] . '&posttype=' . base64_encode($getRelatedPostDetails[$j]['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                    <?php
                      $ext         = explode(".",$getRelatedPostDetails[$j]['post_details']['img_file_name']);

                      // $ext         = explode(".",$getPopularPostWidget[$j]['post_details']['img_file_name']);
                      $extension = pathinfo($getRelatedPostDetails[$j]['post_details']['img_file_name'], PATHINFO_EXTENSION);

                      //CDN image path
                      $imageFileOfCDN = $cdnSocialUrl . 'users/' . $getRelatedPostDetails[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getRelatedPostDetails[$j]["_id"] . '_' . $getRelatedPostDetails[$j]["created_at"] . '_' . $getRelatedPostDetails[$j]["post_details"]["img_file_name"];

                      //server image path
                      $imageFileOfLocal = $rootUrl . 'images/social/users/' . $getRelatedPostDetails[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getRelatedPostDetails[$j]["_id"] . '_' . $getRelatedPostDetails[$j]["created_at"] . '_' . $getRelatedPostDetails[$j]["post_details"]["img_file_name"];

                      // check for image is avilable on CDN
                      $file = $imageFileOfCDN;
                      $file_headers = @get_headers($file);
                      if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                        $finalImagePath = $imageFileOfLocal;
                      }
                      else {
                        $finalImagePath = $imageFileOfCDN;
                      }
                    ?>
                      <a>
                      <img class="popular_blog_img img-responsive" src="<?php echo $finalImagePath;//$cdnSocialUrl . "users/" . $getRelatedPostDetails[$j]["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $getRelatedPostDetails[$j]["_id"] . '_' . $getRelatedPostDetails[$j]["created_at"] . '_' . $getRelatedPostDetails[$j]["post_details"]["img_file_name"]; ?>" />
                      <div class="half innerT">
                        <span>
                          <?php echo "<span class='text-social-primary'>" . $getRelatedPostDetails[$j]["post_views_count"] . "</span><span class='text-social-primary'> Views</span>";?>
                        </span>
                      </div>
                      </a>
                    </div>
                    <?php } $i = $j-1;?>
                  </div>
                </div>
                <!--row-->
              </div>
            </div>
            <!--popular blog module-->
          </div>
<?php
          } }
        break;
        case 'blog':
          foreach ($getRelatedPostDetails as $key => $value) {
            if(!in_array($value['posted_by'],$userIdsOfAccHandler)){
              $listCount = $listCount + 1;
              if ($listCount > 3) {
                break;
              }
            $ext         = explode(".",$value['post_details']['img_file_name']);
            if (!empty($value["post_details"]["img_file_name"])) {
              $extension = pathinfo($getRelatedPostDetails[$j]['post_details']['img_file_name'], PATHINFO_EXTENSION);

              //CDN image path
              $imageFileOfCDN = $cdnSocialUrl . 'users/' . $getRelatedPostDetails[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getRelatedPostDetails[$j]["_id"] . '_' . $getRelatedPostDetails[$j]["created_at"] . '_' . $getRelatedPostDetails[$j]["post_details"]["img_file_name"];

              //server image path
              $imageFileOfLocal = $rootUrl . 'images/social/users/' . $getRelatedPostDetails[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getRelatedPostDetails[$j]["_id"] . '_' . $getRelatedPostDetails[$j]["created_at"] . '_' . $getRelatedPostDetails[$j]["post_details"]["img_file_name"];

              // check for image is avilable on CDN
              $file = $imageFileOfCDN;
              $file_headers = @get_headers($file);
              if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $finalImagePath = $imageFileOfLocal;
              }
              else {
                $finalImagePath = $imageFileOfCDN;
              }
            } else {
              $finalImagePath = $rootUrlImages .'social/popular_blog.png';
            }
?>
            <div class="col-xs-4">
              <div class="popular-blog-module">
                <div class="card social-card right-panel-modules">
                  <div class="row margin-none blog-module border-bottom">
                    <div class="row margin-none innerT"  onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                        <div class="col-xs-4">
                          <a>
                            <img class="popular_blog_img" src="<?php echo $finalImagePath;  ?>"  />
                          </a>
                        </div>
                      <div class="col-xs-8">
                        <a>
                          <span class="ellipsis-multiline">
                           <?php
                            $postDesc = $value["post_details"]["blog_title"];
                            $postDescCount = strlen( $postDesc);
                            if ($postDescCount >= 50) {
                              $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                            } else {
                              $postDescText = $postDesc;
                            }
                            echo rawurldecode($postDescText);
                          ?>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="row margin-none innerT">
                      <div class="col-xs-8">
                        <label>Earning : </label>
                        <span>
                                       <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                             origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                        </span>
                      </div>
                      <div class="col-xs-4">
                        <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
                      </div>
                    </div>
                  </div>
                  <!--row-->
                </div>
              </div>
              <!--popular blog module-->
            </div>
          <?php } } ?>

<?php
        break;
        case 'status':
          // printArr($getRelatedPostDetails);
          foreach ($getRelatedPostDetails as $key => $value) {
            // printArr($userIdsOfAccHandler);
            // printArr($value['posted_by']);
            if(!in_array($value['posted_by'],$userIdsOfAccHandler)){
            $listCount = $listCount + 1;
              if ($listCount > 3) {
                break;
              }
?>
            <div class="col-xs-4">
              <div class="popular-blog-module">
                <div class="card social-card right-panel-modules">
                  <div class="row margin-none blog-module border-bottom">
                    <div class="row margin-none innerT"  onclick = 'loadRelatedPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                      <div class="col-xs-4">
                          <i class="fa  fa-edit widget-img-ico  media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>
                      </div>
                      <div class="col-xs-8">
                        <a>
                          <span class="ellipsis-multiline">
                            <?php
                              $postDesc = preg_replace('/<\/?a[^>]*>/','',rawurldecode($value["post_short_desc"]));
                              $postDescCount = strlen( $postDesc);
                              if ($postDescCount >= 50) {
                                $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                              } else {
                                $postDescText = $postDesc;
                              }
                              echo ($postDescText);
                            ?>
                          </span>
                        </a>
                      </div>
                    </div>
                    <div class="row margin-none innerT">
                      <div class="col-xs-8">
                        <label>Earning : </label>
                        <span>
                                          <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>

                        </span>
                      </div>
                      <div class="col-xs-4">
                        <a class="text-social-primary pull-right">
                          <?php echo $value['post_views_count'].' Views'; ?>
                        </a>
                      </div>
                    </div>
                  </div>
                  <!--row-->
                </div>
              </div>
              <!--popular blog module-->
            </div>
<?php
          } }
          break;
?>
<?php
      } //End of Switch case
?>
      <!--col-xs-4-->
        </div>
<?php
    } // End of count If condition
  } //End of NoError if condition
?>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>