<?php


	header("Access-Control-Allow-Origin: *");
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');
	session_start();
	require_once "../../config/config.php"; 
	require_once "../../config/db_config.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once('../../helpers/deviceHelper.php');
	require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";

	if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	   print("<script>");
	   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	   print("</script>");
	   die;
	}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


	$getLikeJsonArray = base64_decode(cleanXSS($_POST['likeArray']));
	$likeStart        = cleanXSS(rawurldecode($_POST['likeStart']));
	$likekEndingDate  = cleanXSS(rawurldecode($_POST['likLastDate']));
	$sendLikeArray    = array();	
	$likeAppendLimit  = 11;	
	$k                = 0;
// printArr($getLikeJsonArray);

	if (empty($getLikeJsonArray)) {
		$getLikeArray = getLikePostIds($_SESSION['account_handle'], 'sort');
	} else {
		$getLikeArray = (array) json_decode($getLikeJsonArray);
		// printArr($getLikeArray);		
	}

	if (!isset($likeStart) || empty($likeStart)) {
		$likeStart = 0;
	}

	
	// printArr($getLikeArray);
	for ($i = $likeStart; $i < count($getLikeArray) && $i < ($likeStart + $likeAppendLimit); $i++) { 
		if (count($getLikeArray[$i]) > 0) {
			$sendLikeArray[] = $getLikeArray[$i];
		}
	}
	$k = count($sendLikeArray);
	// printArr($sendLikeArray);
	$sendLikeArray = json_encode($sendLikeArray);
	// printArr($sendLikeArray);

	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
	$likePostAllData         = getActivityBookMarkLikePosts($sendLikeArray, json_encode($blockUserArray));
//	 printArr($likePostAllData);
	if (noError($likePostAllData)) {
		$likePostAllData = $likePostAllData['errMsg'];
	// printArr($likePostAllData)
        //

?>

		<div>
		<div class="col-lg-12 col-xs-12 like-dislike-container padding-none">
		<?php 
		if (count($likePostAllData) <= 0) {
			echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>"There is nothing to show here right now."</p><p>"Come back later once you have liked any post."</p></b></center></li></ul>';
			die;
		}
//        $mil = $getCommentCall[$i]['editTime'];
//        $seconds = $mil / 1000;
//        $created_at         = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s", $seconds));
//        $timestamp2 = strtotime($created_at);

        foreach ($likePostAllData as $date => $likeData) {
//		    printArr("1".$date);
            $dateToDisply  = uDateTime("d-m-Y H:i:s",date("m/d/y"),$date);
                $todayDate  = uDateTime("m/d/y",date("m/d/y"));
				// echo "<br>lopping : "	.$date;
				// echo "<br>".date("m/d/Y", strtotime("yesterday"));
				if ($todayDate == $date) {
					$displayDate = 'Today';
				} else {
                    $yesterdayDate  = uDateTime("m/d/y",date("m/d/Y", strtotime("yesterday")));
					if ($yesterdayDate == $date) {
						$displayDate = 'Yesterday';
					} else {
                        $displayDate =  uDateTime("jS F Y",date("m/d/y"),$dateToDisply);
					}
				}
				if($likekEndingDate != $date) {
		?>
				<div class="h4  innerL ">
					<?php echo $displayDate; ?>
				</div>
		<?php
				}
		?>
			
				<ul class="list-unstyled activity-log__list margin-none">
				<?php
					foreach ($likeData as $key => $value) {				
						// if ($value['post_type'] == 'share') {
						// printArr($value);	
						switch ($value['post_type']) {
							case 'blog':
								//if (empty($value['post_details']['img_file_name'])) {
									$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
								//} else {
									//$extBlog = explode(".",$value["post_details"]["img_file_name"]);
									//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value["_id"] . '_' . $value["created_at"] . '_' . $value["post_details"]["img_file_name"] .'" alt="...">';
								//}
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_details"]["blog_title"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
				                $bookPostId = $value['_id'];
				                $bookPostType = 'blog';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;
							case 'video':
								$YoutubeVideoId = getYoutubeIdFromUrl($value['post_details']['asset_url']);
								//$imagePath = '<img class="media-object media-object--activity-log " src="'. $YoutubeVideoId .'" alt="...">';
                                $imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16"></i>';
                                $videoTitle = getYoutubeTitleFromUrl($value['post_details']['asset_url']);
								if ($videoTitle != '') {
									$postDesc = $videoTitle;
								} else if (!empty($value['post_short_desc'])) {
									$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
								} else {
									$postDesc = 'Title Unavailable';
								}
								$postDescCount = strlen( $postDesc);
								if ($postDescCount >= 50) {
									$postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
								} else {
									$postContent = $postDesc;
								}
								$bookPostId = $value['_id'];
				                $bookPostType = 'video';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;
							case 'audio':
								$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
								if (!empty($value["post_short_desc"])) {
									$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
					                $postDescCount = strlen($postDesc);
					                if ($postDescCount >= 50) {
					                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
					                } else {
					                  $postContent = $postDesc;
					                }
				            	} else {
				            		$postContent = 'Title Unavailable';
				            	}
				            	$bookPostId = $value['_id'];
				                $bookPostType = 'audio';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;
							case 'image':
                                $extImg = explode(".",$value["post_details"]["img_file_name"]);
                                $imageFileOfCDN = $cdnSocialUrl . "users/" . $value["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value["_id"] . '_' . $value["created_at"] . '_' . $value["post_details"]["img_file_name"];

                                // check for image is avilable on CDN
                                // $file = $imageFileOfCDN;
                                // $file_headers = @get_headers($file);
                                // if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                     $imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center f-sz16"></i>';
                                // } else {
                                // }
                                if (!empty($value["post_short_desc"])) {
									$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
					                $postDescCount = strlen($postDesc);
					                if ($postDescCount >= 50) {
					                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
					                } else {
					                  $postContent = $postDesc;
					                }
				            	} else {
				            		$postContent = 'Title Unavailable';
				            	}
				            	$bookPostId = $value['_id'];
				                $bookPostType = 'image';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;
							case 'status':
								$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
				                $bookPostId = $value['_id'];
				                $bookPostType = 'status';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;
							case 'share':
								// $imagePath = '<img class="media-object media-object--activity-log " src="https://placeholdit.imgix.net/~text?w=100&h=50" alt="...">';
								$sharePostType = $value['post_details']['parent_post']['post_id']['post_type'];
								switch ($sharePostType) {
									case 'blog':
										$shareBlogImgPath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
										//if (empty($shareBlogImgPath)) {
											$imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
										//} else {
											//$extBlog = explode(".",$shareBlogImgPath);
											//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/blog/featured/" . $extBlog[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareBlogImgPath .'" alt="...">';
										//}
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }

										break;
									case 'video':
										$shareVideoPath = $value['post_details']['parent_post']['post_id']['post_details']['asset_url'];
										$YoutubeVideoId = getYoutubeIdFromUrl($shareVideoPath);
										//$imagePath = '<img class="media-object media-object--activity-log " src="'. $YoutubeVideoId .'" alt="...">';
                                        $imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16"></i>';
                                        $videoTitle = getYoutubeTitleFromUrl($shareVideoPath);
										if ($videoTitle != '') {
											$postDesc = $videoTitle;
										} else if (!empty($value['post_short_desc'])) {
											$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
										} else {
											$postDesc = 'Title Unavailable';
										}
										$postDescCount = strlen( $postDesc);
										if ($postDescCount >= 50) {
											$postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
										} else {
											$postContent = $postDesc;
										}
										break;
									case 'audio':
										$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
										if (!empty($value["post_short_desc"])) {
											$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
							                $postDescCount = strlen($postDesc);
							                if ($postDescCount >= 50) {
							                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							                } else {
							                  $postContent = $postDesc;
							                }
						            	} else {
						            		$postContent = 'Title Unavailable';
						            	}
										break;
									case 'image':
										$shareImagePath = $value['post_details']['parent_post']['post_id']['post_details']['img_file_name'];
										//if (empty($shareImagePath)) {
											$imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center innerT f-sz16 "></i>';
										//} else {
											//$extImg = explode(".",$shareImagePath);
											//$imagePath = '<img class="media-object media-object--activity-log " src=" '. $cdnSocialUrl . "users/" . $value["post_details"]["parent_post"]["post_id"]["user_ref"]["account_handle"] . "/post/images/" . $extImg[1] . "/" . $value["post_details"]["parent_post"]["post_id"]["_id"] . '_' . $value["post_details"]["parent_post"]["post_id"]["created_at"] . '_' . $shareImagePath .'" alt="...">';
										//}
										if (!empty($value["post_short_desc"])) {
											$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
							                $postDescCount = strlen($postDesc);
							                if ($postDescCount >= 50) {
							                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							                } else {
							                  $postContent = $postDesc;
							                }
						            	} else {
						            		$postContent = 'Title Unavailable';
						            	}
										break;
									case 'status':
										$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
						                $postDescCount = strlen($postDesc);
						                if ($postDescCount >= 50) {
						                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
						                } else {
						                  $postContent = $postDesc;
						                }
										break;								
									default:
										$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
										$postContent = 'Title Unavailable';

										break;
								} // End of Share post Switch case
								$bookPostId = $value['_id'];
				                $bookPostType = 'share';
				                $bookPostcreatedAt = $value['created_at'];
				                $bookPostEmail = $value['user_ref']['email'];
								break;							
							default:
								$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
								$postContent = 'Title Unavailable';
								break;
						}// End Of switch Case

				?>
						<li class="card innerAll border-bottom">
							<div class="media">
								<div class="media-left">
									<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']);?>" target="_blank">
									<?php echo $imagePath; ?>
									</a>
								</div>
								<div class="media-body f-sz15">
									<div class="row">
										<div class="col-xs-11">
											<div class="row">
												<div class="col-xs-6">
													<p class="ellipses innerTB half margin-none">
														<a href="<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']);?>" target="_blank">
															<?php echo rawurldecode($postContent); ?>	
														</a>
													</p>
												</div>
												<div class="col-xs-6">
													<p class="ellipses innerTB half margin-none">
														<a href= "<?php echo $rootUrl; ?>" target = "_blank">
														<span class="text-color-Text-Primary-Blue ">
															<?php echo $_SESSION['first_name'].' '.$_SESSION['last_name'];?>
														</span>
														</a>
														likes 
														<a <?php if($value['posted_by'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($value['user_ref']['email']); ?>&type=<?php echo 'all'; ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> target = "_blank">
															<span class="text-color-Text-Primary-Blue ">
																<?php echo $value['user_ref']['first_name'] . ' ' . $value['user_ref']['last_name']; ?>
															</span> 
														</a>
														post.
													</p>
												</div>
											</div>
										</div>
										<div class="col-xs-1">
											<a class="btn btn-link" role="button"> <i class="fa fa-thumbs-o-up text-color-Text-Primary-Blue f-sz18 pull-right" style="cursor: not-allowed;"></i></a>
										</div>
									</div>
									<!--
									-->
								</div>
							</div>
						</li>
				<?php
						// } //end of post type if condition
					} //End of Inner For Loop
				?>
				</ul>
		<?php
				
			} //End of outer For Loop
		?>
		</div>
		</div>

<?php 
	} else {
		echo "<div><center>Please Try After Some Time</center></div>";
	}
?>

<div id = "like-frontendArray-<?php echo $likeStart; ?>" like-started-array = "<?php echo base64_encode(json_encode($getLikeArray));?>"  like-start = "<?php echo $i;?>" likeDataEnd = "<?php if ($k < $likeAppendLimit) {echo 'true'; }?>" like-prev-date = '<?php echo $date; ?>' style = "display:none;"></div>