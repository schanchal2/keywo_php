<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once "../../models/social/socialModel.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/commonFunction.php";

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

$emailId        = $_GET["email"];
$type           = $_GET["type"];
$followingArray = array('followings','followers','suggested','blocked');
$followingType  = urldecode($_GET['type']);
$followerFlag   = false;

$user_id="user_id,account_handle";
$getUserInfo = getUserInfo($emailId,$walletURLIPnotification.'api/notify/v2/',$user_id);
if(noError($getUserInfo)) {
    $getUserInfo = $getUserInfo["errMsg"];
    $otherAccountHandlerId = $getUserInfo["user_id"];
    $accountHandle = $getUserInfo["account_handle"];


// Get Blocked User
    $blockFlag = false;
//Get Blocked Array
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/";
    $getBlockedArray = getBlockedUser($targetDirAccountHandler);
    if (in_array($otherAccountHandlerId, $getBlockedArray)) {
        $blockFlag = true;
    }


// To check URL Type is not other than following , follower, suggested, blocked.
    $n = 0;
    foreach ($followingArray as $key => $value) {
        if ($value == strtolower($followingType)) {
            break;
        }
        $n = $n + 1;
    }
    if (count($followingArray) == $n) {
        $followerFlag = true;
    }

// Redirecting to index page
    if ($followerFlag) {
        header('location:' . $rootUrl);
        die;
    }
    if (empty($followingType)) {
        $followingType = "followings";
    }
}
?>

<!--========================================================================================================================================
=            "follower" page and "following" page will be same as below, only "follow" button wont be there in "following" page            =
=========================================================================================================================================-->
<?php include("../layout/header.php");
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
      type="text/css"/>
<main class="social-main-container inner-7x innerT" >
    <?php
    if($blockFlag == true){
        include('not_found_error.php');
        exit;
    }
    ?>
    <div class="container">
        <div class="col-xs-3">
            <div class="social-left-panel">
                <?php include("leftInfoUser.php"); ?>
                <!-- my-info-card  -->
            </div>
            <!-- social-left-panel  -->
        </div>
        <!-- col-xs-3 -->
        <div class="col-xs-6">
            <div class="social-center-panel half innerLR">
                <!-- =================== social-all-status-tabs ================== -->
                <div class="social-all-status-tabs">
                    <div class="row followers-unfollowers">
                        <div class="col-xs-4">
                            <a id="OtherFollowings" href="#followings" class="active">FOLLOWING</a>
                        </div>
                        <div class="col-xs-4">
                            <a id="OtheFollower" href="#follower" class="">FOLLOWERS</a>
                        </div>
                        <div class="col-xs-4">
                            <a id="mutualList" href="#mutualList" class="">MUTUAL</a>
                        </div>
                    </div>

                    <script>
                        var url = window.location.hash;
                        console.log(url);
                    </script>

                    <div class="row innerT" id="mutualButton">
                        <div class="col-xs-6 padding-left-none">
                            <a id="mutual" href="#mutual" class="btn btn-block bg-white social-card no-radius">Friends</a>
                        </div>
                        <div class="col-xs-6 padding-right-none">
                            <a id="mutualkeyword" href="#mutualkeyword" class="btn btn-block bg-white social-card no-radius">Keyword   </a></div>
                    </div>

                </div>
                <div class="row">
                    <div id="followersType" type = "<?php echo $followingType; ?>"></div>
                    <div id = "followingAjaxCall" ajxCallFlag = "true"></div>

                    <!-- Followings Data Containt -->
                    <div id = "followerDataAppend" followingDataAppendCount = "" followingDataLoopCount = "" pageScrollEndFlag = "" style="display:none;"></div>
                    <div id = "followerdivAppendId" style="display:none;">1</div>
                    <div id="follow-data">
                        <!--content from ajax-->
                    </div>
                    <!-- Followings Data Containt End -->

                    <!-- Follower Data Containt -->
                    <div id = "followingDataAppend" followingDataAppendCount = "" followingDataLoopCount = "" pageScrollEndFlag = ""   style="display:none;"></div>
                    <div id = "followingdivAppendId" style="display:none;">1</div>
                    <div  id="followings-data" class="follower-container">
                        <!--content from ajax-->
                    </div>
                    <!-- Follower Data Containt End -->

                    <!-- Ajax Loader Image -->
                    <div id = "ajaxLoader" class="text-center" style="display:none; ">
                        <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
                    </div>
                    <!-- Ajax Loder Image End -->

                </div>
            </div>
            <!-- social-center-panel  -->
        </div>
        <!-- col-xs-6 -->
        <div class="col-xs-3 social-right-position">
            <div class="social-right-panel">
                <!--search box-->
<!--                <div class="row innerMB inner-2x">-->
<!--                    <div class="col-xs-12">-->
<!--                        <div class="pull-left half search-box-comments" style="width:100%;">-->
<!--                            <div class="input-group" style="width:100%;">-->
<!--                                <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="" style="width:100%;">-->
<!--                                <input type="submit" class="search-btn-header" value="">-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
                <!--trending keywords-->
                <div class="card social-card right-panel-modules row inner-2x">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-4 padding-right-none">
                                <h4 class="half innerMTB">Trending</h4>
                            </div>
                            <div class="col-xs-8 padding-left-none">
                                <div class="trending-content">
                                    <ul class="list-inline half innerMTB text-right">
                                        <li>
                                            <a class="active" href="#" data-id="blog">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#" data-id="video">
                                                <i class="fa fa-play-circle"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#" data-id="image">
                                                <i class="fa fa-image"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#" data-id="audio">
                                                <i class="fa fa-soundcloud"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="" href="#" data-id="status">
                                                <i class="fa fa-list-alt"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="trending-keywords-data">
                        <?php include('trendingKeywords.php'); ?>
                    </div>
                </div>

                <!--popular Posts-->
                <div class="popular-modules">
                    <div class="card social-card right-panel-modules inner-2x innerMTB">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-4 padding-right-none">
                                    <h4 class="half innerMTB">Popular</h4>
                                </div>
                                <div class="col-xs-8 padding-left-none">
                                    <div class="popular-content">
                                        <ul id = "popular-post-click" class="list-inline half innerMTB text-right">
                                            <li>
                                                <a id = "popular-post-type-blog" class="popular-active-indicate active" onclick = "getPopularPost('blog');">
                                                    <i class="fa fa-file-text"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id = "popular-post-type-video" class="popular-active-indicate " onclick = "getPopularPost('video');" >
                                                    <i class="fa fa-play-circle"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id = "popular-post-type-image" class="popular-active-indicate " onclick = "getPopularPost('image');">
                                                    <i class="fa fa-image"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id = "popular-post-type-audio" class="popular-active-indicate " onclick = "getPopularPost('audio');">
                                                    <i class="fa fa-soundcloud"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a id = "popular-post-type-status" class="popular-active-indicate " onclick = "getPopularPost('status');">
                                                    <i class="fa fa-list-alt"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id = "popular-post-details">
                            <?php include('widgetPopularPost.php'); ?>
                        </div>
                    </div>
                </div>
                <!--End of Popular Post Widget-->
            </div>
            <!-- social-left-panel  -->
        </div>
        <!-- col-xs-3 -->
    </div>
    <!-- <div onclick="loadMore('following');">load More</div> -->
    <!-- container -->
</main>

<?php include("../layout/social_footer.php"); ?>

<script>

    $(document).ready(function(){

        if(url=="#mutualList" || url=="#mutualkeyword" || url=="#mutual"){
            $("#mutualButton").show();
        }else{
            $("#mutualButton").hide();
        }

        var email = '<?php echo $emailId;?>';
        var type = '<?php echo $type;?>';
        var followdocLoadType = $('#followersType').attr('type');
        if (type == 'followings') {
            $('#OtheFollower').removeClass('active');
            $('#OtherFollowings').addClass('active');
            loadFollowingsOfOther(type,email);
        } else if (type == 'followers') {
            $('#OtheFollower').addClass('active');
            $('#OtherFollowings').removeClass('active');
            loadFollowingsOfOther(type,email);
        }

        /*
         For endless scrolling
         */


        var bottom = $(document).height() - $(window).height();
        var successflag = 0;
        $(document).scroll(function(){
            var win = $(window);
            // Each time the user scrolls
            win.scroll(function() {
                // End of the document reached?
                if ($(document).height() - win.height() == win.scrollTop()) {
                    // Do the stuff
                    if(successflag == 0){
                        //do stuff here
                        successflag = 1;
                        var followloadType = $('#followersType').attr('type');
                        if (followloadType == 'followings') {
                            loadFollowingsOfOther(followloadType,email);
                        } else if (followloadType == 'followers') {
                            loadFollowingsOfOther(followloadType,email);
                        }
                    }
                }else{
                    successflag = 0;
                }
            });
        });


    });

    // To load OtherFollowings List
    $('#OtherFollowings').on('click', function () {
        var email = '<?php echo $emailId;?>';
        var type = 'followings';
        $('#followersType').attr('type','following');
        $('#followerDataAppend').attr('pageScrollEndFlag','true');
        $('#followerDataAppend').attr('followingDataAppendCount','');
        $('#followerDataAppend').attr('followingDataLoopCount','');
        $('#followingDataAppend').attr('pageScrollEndFlag','true');
        $('#followingDataAppend').attr('followingDataAppendCount','');
        $('#followingDataAppend').attr('followingDataLoopCount','');
        $('#followingdivAppendId').text('1');
        $('#followerdivAppendId').text('1');
        $('#OtherFollowings').addClass('active');
        $('#OtheFollower').removeClass('active');
        $('#mutual').removeClass('active');
        loadFollowingsOfOther(type,email);

//  $("#mutualButton").show();
        $("#mutualButton").hide();
    });

    // To load OtheFollower List
    $('#OtheFollower').on('click', function () {
        var email = '<?php echo $emailId;?>';
        var type = 'followers';
        $('#followersType').attr('type','followers');
        $('#followerDataAppend').attr('pageScrollEndFlag','true');
        $('#followerDataAppend').attr('followingDataAppendCount','');
        $('#followerDataAppend').attr('followingDataLoopCount','');
        $('#followingDataAppend').attr('pageScrollEndFlag','true');
        $('#followingDataAppend').attr('followingDataAppendCount','');
        $('#followingDataAppend').attr('followingDataLoopCount','');
        $('#followingdivAppendId').text('1');
        $('#followerdivAppendId').text('1');
        $('#OtheFollower').addClass('active');
        $('#OtherFollowings').removeClass('active');
        $('#mutual').removeClass('active');
        loadFollowingsOfOther(type,email);
//  $("#mutualButton").show();
        $("#mutualButton").hide();
    });


    // To load mutual List
    $('#mutual').on('click', function () {
        var email = '<?php echo $emailId;?>';
        var type = 'followings';
        $('#followersType').attr('type','mutual');
        $('#followerDataAppend').attr('pageScrollEndFlag','true');
        $('#followerDataAppend').attr('followingDataAppendCount','');
        $('#followerDataAppend').attr('followingDataLoopCount','');
        $('#followingDataAppend').attr('pageScrollEndFlag','true');
        $('#followingDataAppend').attr('followingDataAppendCount','');
        $('#followingDataAppend').attr('followingDataLoopCount','');
        $('#followingdivAppendId').text('1');
        $('#followerdivAppendId').text('1');
        $('#mutual').addClass('active');
        $('#OtherFollowings').removeClass('active');
        $('#OtheFollower').removeClass('active');
        loadMutual(type,email);
        var url = window.location.hash;
        console.log(url);
        $("#mutualButton").show();
//    $("#mutualButton").hide();
    });

    // To load mutual List
    $('#mutualList').on('click', function () {
        var email = '<?php echo $emailId;?>';
        var type = 'followings';
        $('#followersType').attr('type','mutual');
        $('#followerDataAppend').attr('pageScrollEndFlag','true');
        $('#followerDataAppend').attr('followingDataAppendCount','');
        $('#followerDataAppend').attr('followingDataLoopCount','');
        $('#followingDataAppend').attr('pageScrollEndFlag','true');
        $('#followingDataAppend').attr('followingDataAppendCount','');
        $('#followingDataAppend').attr('followingDataLoopCount','');
        $('#followingdivAppendId').text('1');
        $('#followerdivAppendId').text('1');
        $('#mutual').addClass('active');
        $('#OtherFollowings').removeClass('active');
        $('#OtheFollower').removeClass('active');
        loadMutual(type,email);
        var url = window.location.hash;
        console.log(url);
        $("#mutualButton").show();
//    $("#mutualButton").hide();
    });


    // To load mutualKeyword List

    $('#mutualkeyword').on('click', function () {
        var email = '<?php echo $emailId;?>';
        var type = 'followedkeyword';
        $('#followersType').attr('type','mutualkeyword');
        $('#followerDataAppend').attr('pageScrollEndFlag','true');
        $('#followerDataAppend').attr('followingDataAppendCount','');
        $('#followerDataAppend').attr('followingDataLoopCount','');
        $('#followingDataAppend').attr('pageScrollEndFlag','true');
        $('#followingDataAppend').attr('followingDataAppendCount','');
        $('#followingDataAppend').attr('followingDataLoopCount','');
        $('#followingdivAppendId').text('1');
        $('#followerdivAppendId').text('1');
        $('#mutualkeyword').addClass('active');
        $('#OtherFollowings').removeClass('active');
        $('#OtheFollower').removeClass('active');
        loadMutualKeyword(type,email);
    });

</script>
