<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/arrayHelper.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
  //session is not active, redirect to login page
  print("<script>");
  print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
  print("</script>");
  die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

$email      = $_SESSION["email"];
$category   = explode('-', $_GET["category"]);
$offsetFrom = 0;
if (isset($_SESSION["nextOffsetFrom"]) && $_SESSION["nextOffsetFrom"] == -1) {
    $_SESSION["nextOffsetFrom"] = 0;
}
if (isset($_SESSION["nextOffsetFrom"])) {
    $offsetFrom = $_SESSION["nextOffsetFrom"];
}
$getData = getNotificationData($_SESSION["id"], $category[1], $offsetFrom);
//printArr($getData);
if ($getData["errCode"] == -1 && !empty($getData["errMsg"]["batched_container"])) {
  $notificationData = $getData["errMsg"]["batched_container"];
  $nextOffsetFrom   = $getData["errMsg"]["bkwd_cursor_offset_from"];

  $scrollAllow                = "true";
  $_SESSION["nextOffsetFrom"] = $nextOffsetFrom;
  if ($_SESSION["nextOffsetFrom"] == -1) {
      $scrollAllow = "false";
  }


} else { $notificationData = array(); ?>

  <div class="modal-header">
      <h4 class="modal-title text-blue">No Notification</h4>
  </div>
<?php } ?>

  <div class="col-lg-12">
      <ul class="list-unstyled innerMT notification__list">
    <?php
    for ($data = 0; $data < count($notificationData); $data++) {
            $currentDateTime       = $notificationData[$data]["created_at"];
            $previousDate          = $notificationData[$data - 1]["created_at"];
            $seconds               = $currentDateTime / 1000;
            $previousSeconds       = $previousDate / 1000;
            $createdAt             = uDateTime("d-m-Y h:i a",date("d-m-Y H:i:s", $seconds));
            $previousCreatedAt     = uDateTime("d-m-Y h:i a",date("d-m-Y H:i:s", $previousSeconds));
            $timestamp             = explode(" ", $createdAt);
            $dateForMonthConv1     = uDateTime("d-m-Y",date("d-m-Y H:i:s", $seconds));
            $dateForYesterday      = uDateTime("d-m-Y",date("d-m-Y H:i:s", $previousSeconds));
            $dateForMonthConv2     = uDateTime("d-m-Y",date("d-m-Y H:i:s"));
//            printArr($dateForMonthConv2);
      $j                     = 1;
      if ($dateForMonthConv1 == $dateForMonthConv2) {
        $displayDate = "Today";
        $j + 1;
      } elseif ($dateForMonthConv1 == $dateForYesterday) {
        $displayDate = "Yesterday";
      } else {
          $displayDate     = uDateTime("jS M Y",date("d-m-Y H:i:s", $seconds));
      }

      $dateDis  = $dateForMonthConv1;
      $prevDate     = uDateTime("d-m-Y",date("d-m-Y H:i:s", $previousSeconds));
    ?>
    <?php if ($prevDate != $dateForMonthConv1) { ?>
      <div class="h4 innerL"><?php echo $displayDate; ?></div>
    <?php } ?>

        <li class="card innerAll border-bottom ">
          <div class="media">
            <div class="media-left">
              <a class="f-sz13">
                <?php
                  $value = $notificationData[$data]["category"];
                  switch ($value) {
                    case 'like':
                ?>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-thumbs-up fa-stack-1x text-white"></i>
                </span>
                <?php break;
                  case 'following':
                ?>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa fa-group fa-stack-1x text-white"></i>
                </span>
                <?php break;
                  case 'reply':
                ?>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa  fa-comments-o fa-stack-1x text-white"></i>
                </span>
                <?php break;
                  case 'comment':
                ?>
                <span class="fa-stack fa-lg">
                  <i class="fa fa-circle fa-stack-2x"></i>
                  <i class="fa  fa-comments-o fa-stack-1x text-white"></i>
                </span>
                <?php break;
                  case 'wallet':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/notification_wallet.png">
                <?php break;
                  case 'search':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/keywordsearch.png">
                <?php break;
                  case 'keyword':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/notification_keywo.png">
                <?php break;
                  case 'purchase':
                ?>
               <?php break;
                  case 'buy':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/keywordsearch.png">


<?php break;
                  case 'bid':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/keywordsearch.png">
                  <?php break;
                  case 'ask':
                ?>
                  <img class="media-object--notification" src="<?=$rootUrlImages ?>icons/keywordsearch.png">




                <?php break; } ?>
              </a>
            </div>
            <div class="media-body f-sz15">
              <div class="row">
                <div class="col-xs-10">
                  <div class="row">
                    <div class="col-xs-12">
                      <p class="ellipses innerTB half margin-none"><?php echo $notificationData[$data]["notification_body"]; ?></p>
                    </div>
                    <!-- <div class="col-xs-5">
                      <p class="ellipses innerTB half margin-none">I Made $700 With My First Post and $0.06 </p>
                    </div> -->
                  </div>
                </div>
                <div class="col-xs-2">
                  <p class="half innerTB margin-none pull-right text-color-grayscale-be">
                  <?php
                    echo $timestamp[1] . $timestamp[2];
                  ?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </li>

    <?php } //closing for "for loop" ?>
      </ul>
  </div>
<?php
} else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<script>
    $(document).ready(function() {
        var successflag = 0;
        $(document).scroll(function() {
            /*
             For endless scrolling
             */
            var win = $(window);
            // Each time the user scrolls
            win.scroll(function() {
                // End of the document reached?
                if ($(document).height() - win.height() == win.scrollTop()) {
                    var statusScroll = "<?php echo $_SESSION["nextOffsetFrom"]; ?>";
                    if (successflag == 0 && statusScroll != -1) {
                        var category = $(".active").attr('id');
                        nextNotificationData(category);
                        successflag = 1;
                    }
                } else {
                    successflag = 0;
                }
            });
        });
    });


    function nextNotificationData(category) {
        var scrollAllow  = "<?php echo $scrollAllow; ?>";
        if (scrollAllow == "true") {
            $.ajax({
                type: 'GET',
                dataType: 'html',
                data: {category: category},
                url: 'notificationData.php',
                async: false,
                success: function (data) {
                    $("#append-data-notify").append(data);
                },
                error: function () {
                    return false;
                }
            });
        }
    }
</script>