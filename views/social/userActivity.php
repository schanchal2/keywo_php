<?php

    session_start();

	//check for session

    //check session & redirect to FTU page
    $redirectUrl = $rootUrl . "/views/user/ft_like.php";
    if($_SESSION["userFteStatusFlag"]==0){
        //session is not active, redirect to login page
        print("<script>");
        print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
        print("</script>");
        die;
    }

	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];
        $accountHandle = $_SESSION["account_handle"];

        $require = $userRequiredFields . ",user_id,profile_pic";
        $requestUrl          = $NotificationURL.'v2/';
        $getUserInfo = getUserInfo($email, $requestUrl, $require);
        if(noError($getUserInfo)){
            $getUserInfo = $getUserInfo["errMsg"];
            $profilePic = $getUserInfo["profile_pic"];

            if(isset($profilePic) && !empty($profilePic)){
                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($profilePic, PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $profilePic . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$profilePic;

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                $imgSrc = $rootUrlImages."default_profile.jpg";
            }
        }else{
            print('Error: Fetching user info');
            exit;
        }


        ?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
			type="text/css"/>
<main class="social-main-container inner-7x innerT" >
<div class="container">
	<div class="col-xs-3">
		<div class="social-left-panel">
      <div class="like-unlike-user-profile-">
				<img class="img-responsive" src="<?php echo $imgSrc; ?>" />
				<div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
					<div class="col-xs-6 padding-none">
						<div class="col-xs-12 padding-none">
							<?php echo $_SESSION["first_name"]." ".$_SESSION["last_name"]; ?>
						</div>
						<div class="col-xs-12 padding-none">
							<div class="account-handle-name"><?php echo "@".$accountHandle; ?></div>
						</div>
					</div>
					<div class="col-xs-6 padding-none innerMT">
						<!--<input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile"/>-->

                        <a  href="<?php echo $rootUrl; ?>views/social/user_social/myprofile.php" class="btn-social-wid-auto pull-right">Edit Profile</a>
					</div>
				</div>
      </div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Settings and Preferences</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a href="<?php echo $rootUrl; ?>views/social/user_social/myprofile.php">Profile</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Change Password</a>
						</div>
						<!--<div class="social-user-setting-name border-bottom">
							<a>Notification Preferences</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Two Factor (2FA)</a>
						</div>-->
					</div>
				</div>
			</div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
							<h4>Activity Log</h4>
						</div>
					</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div id = "likeDislikeActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
							<a id = "likeDislikeActivity">Likes <span id = "likeDislikeActivityCount">(0)</span></a>
						</div>
						<div id = "commentActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
							<a id = "commentActivity">Comments <span id = "commentActivityCount">(0)</span></a>
						</div>
						<div id = "hiddenPostActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
							<a id = "hiddenPostActivity">Hidden Post <span id = "hiddenPostActivityCount">(0)</span></a>
						</div>
						<div id = "bookmarkPostActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
							<a id = "bookmarkPostActivity">Post Bookmarks <span id = "bookmarkPostActivityCount">(0)</span></a>
						</div>
						<div id = "followedPeopleActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display active">
							<a id = "followedPeopleActivity">Followed People <span id = "followedPeopleActivityCount">(0)</span></a>
						</div>
						<div id = "followedKeywordActivityDiv" class="social-user-setting-name border-bottom acivity-tab-display">
							<a id = "followedKeywordActivity">Followed Keywords <span id = "followedKeywordActivityCount">(0)</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-9">
		<div class="col-xs-12 innerMB padding-none">
			<div class="pull-left">
				<h4 class="text-blue half innerMT">Activity Log :</h4>
			</div>
			<div class="pull-left half innerMT innerML">
				<div id = "activityType" class="heading">Followed People</div>
			</div>
			<div class="pull-right">
				<div id = "keywordSortingType" class="pull-left innerMR" style = "display: none;">
					<div class="sort-by">
						<label>Sort By : </label>
						<select>
							<option>Latest</option>
							<option>Oldest</option>
						</select>
					</div>
				</div>
				<div class="pull-right">
					<form class="clearfix margin-none">
						<div class="pull-left half innerMT search-box-comments">
								<div class="input-group">
										<input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="">
										<input type="submit" class="social-search-btn-header" value="">
								</div>
						</div>
					</form>
				</div>
			</div>
			<!--<div class="pull-right">

				<form class="clearfix margin-none">
					<div class="pull-left half innerMT search-box-comments">
							<div class="input-group">
									<input type="text" id="search_box" name="q" value="<?php echo $keywords; ?>"
												 class="form-control" placeholder="Search" required>
									<input type="submit" class="search-btn-header" value=""/>
							</div>
					</div>
				</form>
			</div>-->
		</div>
		<div class="col-xs-12 followed-people-container padding-none">
			<div id = "followingDataAppend" followingDataAppendCount = "" followingDataLoopCount = "" pageScrollEndFlag = ""  ajxCallFlag = "true" style="display:none;">1</div>


			<div id = "bookmark-post-data-append" bookmark-array = "" bookmark-start = "" ajaxCallFlag = "true" pageScrollEndFlag = "" bookmark-last-date = "" style="display:none;"></div>

			<div id = "like-post-data-append" like-array = "" like-start = "" ajaxCallFlag = "true" pageScrollEndFlag = "" like-last-date = "" style="display:none;"></div>

			<div id = "comment-post-data-append" comment-start = "" ajaxCallFlag = "true" pageScrollEndFlag = "" comment-last-date = "" style="display:none;"></div>

			<div id = "hidden-post-data-append" hidden-start = "" ajaxCallFlag = "true" pageScrollEndFlag = "" hidden-last-date = "" style="display:none;"></div>

			<div id = "keyword-follow-user-data-append" keyword-follow-json = "" keyword-follow-start = "" ajaxCallFlag = "true" pageScrollEndFlag = "" keyword-follow-last-keyword = "" style="display:none;"></div>
			
			<div  id="followings-data" class="follower-container">
              <!--content from ajax for Followings-->
            </div>
			<div id = "activity-log-details" class="follower-container" activity-type-to-load = "followPeople">
				<!--data will Append Here-->
			</div>
			<div id = "ajaxLoader" class="text-center" style="display:none; ">
              <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style = "width: 60px;">
            </div>















			<!--<div class="innerMB followed-time innerML">
				A
			</div>
			<div class="">
				<ul class="card social-card followed-list all-box-shadow padding-none clearfix" style="list-style:none;">
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-1 padding-left-none">
                <img src="<?php echo $rootUrlImages?>uploadingcontent.png" class="img-responsive followed-user-img"/>
              </div>
              <div class="col-xs-3 text-blue innerMT padding-right-none">
                <div class="followed-user-name">Ban Fernandis</div>
              </div>
              <div class="col-xs-3 innerMT">
                <div class="followed-user-account-handle">@babbuFer</div>
              </div>
              <div class="col-xs-5 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow"/>
              </div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-1 padding-left-none">
                <img src="<?php echo $rootUrlImages?>uploadingcontent.png" class="img-responsive followed-user-img"/>
              </div>
              <div class="col-xs-3 text-blue innerMT padding-right-none">
                <div class="followed-user-name">Ban Fernandis</div>
              </div>
              <div class="col-xs-3 innerMT">
                <div class="followed-user-account-handle">@babbuFer</div>
              </div>
              <div class="col-xs-5 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow"/>
              </div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-1 padding-left-none">
                <img src="<?php echo $rootUrlImages?>uploadingcontent.png" class="img-responsive followed-user-img"/>
              </div>
              <div class="col-xs-3 text-blue innerMT padding-right-none">
                <div class="followed-user-name">Ban Fernandis</div>
              </div>
              <div class="col-xs-3 innerMT">
                <div class="followed-user-account-handle">@babbuFer</div>
              </div>
              <div class="col-xs-5 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow"/>
              </div>
            </div>
          </li>
				</ul>
			</div>
      <div class="innerMB followed-time innerML">
        B
      </div>
      <div class="">
        <ul class="card social-card followed-list all-box-shadow padding-none clearfix" style="list-style:none;">
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-1 padding-left-none">
                <img src="<?php echo $rootUrlImages?>uploadingcontent.png" class="img-responsive followed-user-img"/>
              </div>
              <div class="col-xs-3 text-blue innerMT padding-right-none">
                <div class="followed-user-name">Ban Fernandis</div>
              </div>
              <div class="col-xs-3 innerMT">
                <div class="followed-user-account-handle">@babbuFer</div>
              </div>
              <div class="col-xs-5 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow"/>
              </div>
            </div>
          </li>
          <li class="clearfix border-bottom">
            <div class="col-xs-12 innerAll">
              <div class="col-xs-1 padding-left-none">
                <img src="<?php echo $rootUrlImages?>uploadingcontent.png" class="img-responsive followed-user-img"/>
              </div>
              <div class="col-xs-3 text-blue innerMT padding-right-none">
                <div class="followed-user-name">Ban Fernandis</div>
              </div>
              <div class="col-xs-3 innerMT">
                <div class="followed-user-account-handle">@babbuFer</div>
              </div>
              <div class="col-xs-5 innerMT">
                <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow"/>
              </div>
            </div>
          </li>
        </ul>
      </div>-->
    </div>
    <!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

</div>
<!-- container -->
</main>

<!--modal to show delete confirmation of comments-->
<div class="modal fade" id="deleteCommentModal" role="dialog">
  <div class="modal-dialog" style="margin-top: 150px;">
    <div class="card social-card clearfix social-form-modal">
      <div class="modal-header">
        <button type="button" class=" close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title text-blue">Delete</h4>
      </div>
      <div class = "modal-body-social-user-image">

      </div>
    </div>
  </div>
</div>

<!--modal to show Post is Deleted User-->
<div class="modal fade" id="postDeletedModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class = "modal-body-post-deleted">

			</div>
		</div>
	</div>
</div>

<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>


<script type="text/javascript">
	$(document).ready(function(){
		

		/*
		For endless scrolling
		*/
		var bottom = $(document).height() - $(window).height();
		var successflag = 0;
		$(document).scroll(function(){
			var win = $(window);
			// Each time the user scrolls
			win.scroll(function() {
				// End of the document reached?
				if ($(document).height() - win.height() == win.scrollTop()) {
				 // Do the stuff
					if(successflag == 0){
						//do stuff here
						successflag = 1;
						var ActivityLoadType = $('#activity-log-details').attr('activity-type-to-load');
						if (ActivityLoadType == 'postLike') {
							getActivityLike();
						} else if (ActivityLoadType == 'postComment') {
							getActivityComment();
						} else if (ActivityLoadType == 'postBookmark') {
							getActivityBookmarkPost();
						} else if (ActivityLoadType == 'followPeople') {
							loadFollowedPeople();
						} else if (ActivityLoadType == 'followKeyword') {
							getActivityfollowKeywordPost();
						} else if (ActivityLoadType == 'hiddenPost') {
							getActivityHiddenPost();
						}
						// if (followloadType == 'followings') {
						// 	loadFollowings(followloadType);
						// } else if (followloadType == 'followers') {
						// 	loadFollowers(followloadType);
						// }
					}
				}else{
				  successflag = 0;
				}
			});
		});

	});





</script>