<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../layout/header.php");

		$email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container inner-8x innerT">
        <div class="container">
            <div class="col-xs-3">
                <div class="social-left-panel left-panel-modules">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll padding-bottom-none">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG" />
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container text-blue innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name"><?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"]?></span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-info-status">
                                    <a id="my-info-name-edit" data-type="textarea" title="Change Description" data-pk="1" data-placement="right" data-title="Enter username">
                                        <?php
                                    $userDetail = getUserInfo($_SESSION["email"], $walletURLIPnotification . 'api/notify/v2/', 'short_description');
                                    echo $userDetail['errMsg']['short_description'];
                                ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                        <div class="clearfix half innerT">
                            <div class="col-xs-12  innerB">
                                <div class="col-xs-4 padding-none pull-left">
                                    <div class="text-deep-sky-blue my-info-posts">
                                        Posts
                                    </div>
                                    <div class="text-blue innerL my-info-posts-count">
                                        3
                                    </div>
                                </div>
                                <div class="col-xs-4 padding-none">
                                    <div class="text-deep-sky-blue my-info-following">
                                        Following
                                    </div>
                                    <div class="text-blue my-info-following-count inner-2x innerL">
                                        37
                                    </div>
                                </div>
                                <div class="col-xs-4 padding-none text-right">
                                    <div class="text-deep-sky-blue my-info-followers">
                                        Followers
                                    </div>
                                    <div class="text-blue my-info-followers-count inner-2x innerR">
                                        31
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- posts-followers-following  -->
                    </div>



                    <div class="card social-card right-panel-modules inner-2x innerMTB">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-4 padding-right-none">
                                    <h4 class="half innerMTB">Trending</h4>
                                </div>
                                <div class="col-xs-8 padding-left-none">
                                    <div class="trending-content">
                                        <ul class="list-inline half innerMTB text-right">
                                            <li>
                                                <a class="active" href="#" data-id="#trending-keywords">
                                                    <i class="fa fa-file-text"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="#trending-post">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="#trending-videos">
                                                    <i class="fa fa-play-circle"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="#trending-images">
                                                    <i class="fa fa-image"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="#" data-id="#trending-sound">
                                                    <i class="fa fa-soundcloud"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Follow keyword Module-->
                        <div class="follow-keyword-module" id="trending-keywords" style="display:block;">
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--Follow keyword Module-->
                        <!--=============trending-post===============-->
                        <div class="" id="trending-post" style="display:none">
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--=============trending-videos===============-->
                        <div class="" id="trending-videos" style="display:none">
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--============= trending-images ===============-->
                        <div id="trending-images" style="display:none">
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--============= trending-sound ===============-->
                        <div id="trending-sound" style="display:none">
                            <div class="row active margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT">
                                    <a class="half innerL pull-right">Follow</a>
                                </div>
                            </div>
                        </div>
                        <!--trending sound-->
                    </div>
                    <div class="popular-modules">
                        <div class="card social-card right-panel-modules inner-2x innerMTB">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-4 padding-right-none">
                                        <h4 class="half innerMTB">Popular</h4>
                                    </div>
                                    <div class="col-xs-8 padding-left-none">
                                        <div class="popular-content">
                                            <ul class="list-inline half innerMTB text-right">
                                                <li>
                                                    <a class="active" href="#" data-id="#popular-blog-module">
                                                        <i class="fa fa-file-text"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#" data-id="#popular-post-module">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#" data-id="#popular-video-module">
                                                        <i class="fa fa-play-circle"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#" data-id="#popular-image-module">
                                                        <i class="fa fa-image"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="" href="#" data-id="#popular-sound-module">
                                                        <i class="fa fa-soundcloud"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--bg-light-gray-->
                            <div id="popular-blog-module">
                                <div class="row margin-none blog-module border-bottom">
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png" />
                                        </div>
                                        <div class="col-xs-8">
                                            <span class="ellipsis-multiline text-black">
                                            DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                                <div class="row margin-none blog-module">
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/popular_blog.png" />
                                        </div>
                                        <div class="col-xs-8">
                                            <span>
                                            Description
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerT border-bottom">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                    <div class="row margin-none half innerTB border-bottom bg-view-all">
                                        <div class="col-xs-12">
                                            <a href="#" class="text-deep-sky-blue pull-right bg-view-all">View all</a>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--=============popular-post-module===============-->
                            <div class="" id="popular-post-module" style="display:none">
                                <div class="row margin-none video-module border-bottom">
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/video-trending.png" />
                                        </div>
                                        <div class="col-xs-8">
                                            <span class="ellipsis-multiline text-black">
                                            DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                    <div class="row margin-none half innerTB border-bottom">
                                        <div class="col-xs-12">
                                            <a href="#" class="text-deep-sky-blue pull-right bg-view-all">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div> <!--=============popular-video-module===============-->
                            <div class="" id="popular-video-module" style="display:none">
                                <div class="row margin-none video-module border-bottom">
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/video-trending.png" />
                                        </div>
                                        <div class="col-xs-8">
                                            <span class="ellipsis-multiline text-black">
                                            DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                    <div class="row margin-none half innerTB border-bottom">
                                        <div class="col-xs-12">
                                            <a href="#" class="text-deep-sky-blue pull-right bg-view-all">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--============= popular-image-module ===============-->
                            <div id="popular-image-module" style="display:none">
                                <div class="row margin-none image-module border-bottom">
                                    <div class="row margin-none innerT border-bottom">
                                        <div class="col-xs-4">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/image-trending.png" />
                                        </div>
                                        <div class="col-xs-8">
                                            <span class="ellipsis-multiline text-black">
                                            DescriptionD escriptionDescr iptionDescriptionDe scriptionscri ptionDescriptionDescr iption DescriptionDescription
                                        </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                    <div class="row margin-none half innerTB  bg-view-all ">
                                        <div class="col-xs-12">
                                            <a href="#" class="text-deep-sky-blue pull-right bg-view-all">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--============= popular-sound-module ===============-->
                            <div id="popular-sound-module" style="display:none">
                                <div class="row margin-none sound-module">
                                    <div class="row margin-none innerT">
                                        <div class="col-xs-3">
                                            <img class="popular_blog_img" src="<?php echo $rootUrlImages?>social/sound-trending.png" />
                                        </div>
                                        <div class="col-xs-6 padding-none">
                                            <a class="text-ellipsis">Here without you</a>
                                            <span class="" style="display:block;">
                                            By Doors Down
                                        </span>
                                        </div>
                                        <div class="col-xs-3 text-right">
                                            <span class="text-gray">
                                            <span>3.45 mins</span>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="row margin-none innerTB border-bottom">
                                        <div class="col-xs-7">
                                            <label>Earning :</label>
                                            <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                        </div>
                                        <div class="col-xs-5">
                                            <a class="text-social-primary pull-right">157k Views</a>
                                        </div>
                                    </div>
                                    <div class="row  bg-view-all margin-none half innerTB">
                                        <div class="col-xs-12">
                                            <a href="#" class="text-deep-sky-blue pull-right ">View all</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--trending sound-->
                        </div>
                    </div>
                    <!--popular blog module-->

                    <!--suggested-module-->
                    <div class="followed-keywords-module inner-2x innerMTB">
                        <div class="card social-card right-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4 class="half innerMTB">Followed Keywords</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT padding-left-none text-right">
                                    <span>15000</span>
                                    <a class="half innerL"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom">
                                <div class="col-xs-8">
                                    <div class="followed-keywords-name">
                                        <a>#hotel</a>
                                    </div>
                                    <div>
                                        <label>Earning : </label>
                                        <span>24.25 <?php echo $keywoDefaultCurrencyName; ?></span>
                                    </div>
                                </div>
                                <div class="col-xs-4 innerT padding-left-none text-right">
                                    <span>15000</span>
                                    <a class="half innerL"><i class="fa fa-eye"></i></a>
                                </div>
                            </div>
                            <div class="row margin-none half innerTB border-bottom bg-view-all">
                                <div class="col-xs-12">
                                    <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                                </div>
                            </div>
                        </div>
                        <!-- card -->
                    </div>
                    <!--followed-keywords-module-->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-6">
                <div class="social-center-panel half innerLR">

                <div class="panel panel-primary publicwall-panel radius-none">
                      <div class="panel-heading text-center fsz14">In Publishing and  graphic design
                          <button type="button" class="close closeBtn text-white" id="close">&times;</button>
                      </div>
                       
                      <div class="panel-body innerAll half">
                      In Publishing and graphic design, lorem ipsum is filler text commonly used to demonstrate the graphic elements of a document or a visual presentation. Replacing meaningful content with placholder text allows designers to
                      </div>
                </div>
                    <!-- =================== social-search-box ================== -->
                    <div class="social-search-box social-card innerMB">
                        <div class="card share-something-textarea" id="social-micro-blog-box">
                            <form name="status-post" id="status-post" class="margin-none">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group margin-none">
                                            <input type="hidden" name="post-type" value="status">
                                            <div class="share-something-compressed share-small">
                                                <textarea rows="4" class="share-post-box compressed-div form-control checkEmpty" name="modalComment" placeholder="Share Something..."></textarea>
                                            </div>
                                            <span class="pull-right post-edit-icon" style="position: absolute;right: 10px;top: 5px;">
                                                <i class="fa fa-edit pull-right half innerMT fa-lg text-primary-blue"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="keywords-for-tags row innerAll clearfix" style="display:none;">
                                    <div id="tags" class="tag-text" data-id="image-selected-keywords">
                                        <?php
                $keywordinputValue = "";
                foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                  $keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
                  $keywordinputValue .= "#".$keywordValue."  ";
                }
                $keywords = rtrim($keywords,', ');
              ?>
                                            <?php
                echo $keywords;
                if (!empty($postDetail['_id'])) {
              ?>
                                                <input type="text" value="<?php echo $keywordinputValue; ?>" readonly>
                                                <?php
                }
              ?>
                                                    <input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" style="outline: none;" placeholder="Add #Keywords(min 1-max 3)" value="" />
                                                    <input type="hidden" name="modalKeywords" />
                                    </div>
                                </div>
                                <div class="row post-bottom-controls innerMT" style="display:none;">
                                    <div class="share-textbox-quick-links">
                                        <div class="col-xs-4">
                                            <a class="innerR" data-toggle="modal" href="#blogModal">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                            <a class="innerR" data-toggle="modal" href="#videoModal">
                                                <i class="fa fa-play-circle"></i>
                                            </a>
                                            <a data-toggle="modal" href="#imageModal" class="innerR">
                                                <i class="fa fa-picture-o"></i>
                                            </a>
                                            <a data-toggle="modal" href="#audioModal" class="innerR">
                                                <i class="fa fa-soundcloud"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="col-xs-9 padding-none">
                                                <select class="selectpicker" name="postscope">
                                                    <option value="Only Me">Only Me</option>
                                                    <option value="Public">Public</option>
                                                    <option value="Followers">Followers</option>
                                                    <option value="Followers of Followers">Followers of Followers</option>
                                                </select>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="pull-right ">
                                                    <a class="social-form-buttons" href="#" id="statusPost">
                                            Post
                                             </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- social-search-box -->
                    <!-- =================== social-all-status-tabs ================== -->
                    <div class="social-all-status-tabs">
                        <div class="row">
                            <div class="col-xs-12">
                                <ul class="list-inline margin-bottom-none">
                                    <li>
                                        <a href="#" class="active">ALL</a>
                                    </li>
                                    <li>
                                        <a href="#">STATUS</a>
                                    </li>
                                    <li>
                                        <a>BLOG</a>
                                    </li>
                                    <li>
                                        <a>VIDEO</a>
                                    </li>
                                    <li>
                                        <a>IMAGE</a>
                                    </li>
                                    <li>
                                        <a>AUDIO</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- social-all-status-tabs -->
                    <!--display content-->
                    <div id="private-post-data">
                        <!--content from ajax-->
                    </div>
                    <!--display content-->
                    <!--============= Social Timeline Image ======================-->
                    <div class="social-timeline-image innerMTB">
                        <div class="social-card card half innerAll" id="social-timeline-image">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#" class="ellipses ellipses-general">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star-o"></i></span>
                                            <span><i class="fa fa-dot-circle-o cdp-green"></i></span>
                                        </div>
                                        <div class="pull-right social-post-time-container">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="dynamic keyword name">
                                                #bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-keywords-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--  content-image-->
                            <div class="social-timeline-content-image">
                                <div class="row">
                                    <div class="col-xs-12 post-display-img">
                                        <a data-toggle="modal" href="#status_image_modal">
                                            <img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive" />
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-earning-comments-view-->
                            <!--social-timeline-likes-dislikes-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes" id="abc">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a href="JavaScript:void(0);" class="like-area a">
                                                            <i class="likes-click fa fa-thumbs-o-up half innerR"></i>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a class="comment-click">
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a data-toggle="modal" data-target="#share-modal">
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row comments-section comments-module" style="display:none;">
                                    <div class="clearfix comments-module-wrapper">
                                        <div class="col-xs-1 wid-50">
                                            <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                                        </div>
                                        <div class="col-xs-10 padding-left-none">
                                            <div class="comments-module-comment-block">
                                                <a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
                                                <span class="comments-module-comment">hdf</span>
                                            </div>
                                            <div class="comments-module-time-block">
                                                <a href="javascript:;" class="comments-module-reply">Reply</a>
                                                <span class="comments-module-hrs">23  hrs</span>
                                            </div>
                                            <div class="row reply-box innerMT comments-module-wrapper" style="display:none;">
                                                <div class="col-xs-1 padding-none innerMR">
                                                    <img class="reply-img" src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                                                </div>
                                                <div class="col-xs-10 padding-none">
                                                    <div id="comment-form" class="form-group">
                                                        <input type="text" id="user-comment-text" class="form-control checkEmpty reply-input-box" placeholder="Write Comment Here">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="position-relative pull-right innerMR comment-main-dropdown">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu comment-main-dropdown pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a data-toggle="modal" onclick="editDeleteComment('update', '58d3628a87feca59069b5505');"><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                                                </li>
                                                <li>
                                                    <a data-toggle="modal" onclick="deleteConfirmation('delete', '58d3628a87feca59069b5505');"><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-image-->
                    <div class="social-timeline-image innerMTB">
                        <div class="social-card card half innerAll" id="social-timeline-image">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                            tom hanks
                          </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                            @tomhankennew
                          </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span class="innerLR half text-color-Gray bookmark-click"><i class="fa fa-star-o"></i></span>
                                            <span><i class="fa fa-dot-circle-o cdp-green"></i></span>
                                        </div>
                                        <div class="pull-right social-post-time-container">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="dynamic keyword name">
                        #bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-keywords-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-user-status user-status-content">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMTB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-earning-comments-view-->
                            <!--social-timeline-likes-dislikes-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a class="like-area">
                                                            <i class="likes-click fa fa-thumbs-o-up half innerR"></i>
                                                            <span data-toggle="modal" data-target="#likeModal">770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a class="comment-click">
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a data-toggle="modal" data-target="#share-modal">
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-status-->
                    <!--========= shared social-timeline-image ===========-->
                    <div class="social-timeline-image  inner-2x innerMTB">
                        <div class="social-card card half" id="social-timeline-image">
                            <div class="half innerAll">
                                <div class="row half innerTB">
                                    <div class="col-xs-6">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-6 social-post-options">
                                        <div class="dropdown pull-right">
                                            <span class="inner-2x innerR text-social-gray">Shared post</span>
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o text-color-Gray cdp-green"></i></span>
                                        </div>
                                        <br>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-keywords-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB text-black">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <div class="social-timeline-other-user-content bg-light-grey half innerTB">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="">
                                            <span class="inner-2x innerR text-social-gray text-small"><strong>Posted By :</strong>
                                                    <a href="#" class="text-social-primary">Vishal Gupta</a>
                                                </span>
                                            <span class="social-post-time"><span>3</span> hrs ago</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                                <!--  content-image-->
                                <div class="social-timeline-content-image">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <a data-toggle="modal" href="#status_image_modal">
                                                <img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!--social-timeline-content-image-->
                                <div class="social-timeline-earning-comments-view innerT">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <div>
                                                <label>Earning : </label>
                                                <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                            </div>
                                        </div>
                                        <div class="col-xs-8">
                                            <div class="">
                                                <ul class="list-inline pull-right margin-bottom-none">
                                                    <li>
                                                        <div>
                                                            <span class="">40</span>
                                                            <label class="pull-right">Comments</label>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <span class="">456</span>
                                                            <label>Shares</label>
                                                        </div>
                                                    </li>
                                                    <li class="padding-right-none">
                                                        <div>
                                                            <span class="">557k</span>
                                                            <label>Views</label>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--social-timeline-earning-comments-view-->
                                <!--social-timeline-likes-dislikes-->
                                <div class="social-timeline-likes-dislikes">
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="social-likes-dislikes">
                                                <ul class="list-inline margin-bottom-none">
                                                    <li>
                                                        <div>
                                                            <a>
                                                                <i class="fa fa-thumbs-o-up half innerR"></i>
                                                                <span>770</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div>
                                                            <a>
                                                                <i class="fa fa-thumbs-o-down half innerR"></i>
                                                                <span>770</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xs-6">
                                            <div class="social-comments-shares-views">
                                                <ul class="list-inline pull-right margin-bottom-none">
                                                    <li>
                                                        <div>
                                                            <a>
                                                                <i class="fa fa-comments-o"></i>
                                                                <span>Comments</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                    <li class="padding-right-none">
                                                        <div>
                                                            <a>
                                                                <i class="fa fa-share-square-o"></i>
                                                                <span>Share</span>
                                                            </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-other-user-content-message-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-earning-comments-view-->
                            <!--social-timeline-likes-dislikes-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                    <!--============= Social Timeline Video ======================-->
                    <div class="social-timeline-video inner-2x innerMTB">
                        <div class="social-card card half innerAll" data-toggle="modal" data-target="#status_video_modal">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
                                        </div>
                                        <div class="pull-right social-post-time-container">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-video-->
                            <div class="social-timeline-content-video">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div align="center" class="embed-responsive embed-responsive-16by9">
                                            <!-- <video loop class="embed-responsive-item">
                                                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
                                            </video> -->
                                            <iframe width="560" height="258" src="https://www.youtube.com/embed/CMdHDHEuOUE" frameborder="0" allowfullscreen></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-video-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-video-->
                    <!--============= Social Timeline Blog ======================-->
                    <div class="social-timeline-video inner-2x innerMTB">
                        <div class="social-card card half innerAll" id="social-timeline-blog">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--====social-timeline-user-message====-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-details-->
                            <!--social-timeline-details-->
                            <div class="social-timeline-content-blog-thumb">
                                <div class="row">
                                    <a data-id="social-timeline-blog" onclick="postOpenModel(this)">
                                        <div class="col-xs-12">
                                            <a data-toggle="modal" data-target="#status_blog_image_modal">
                                                <img src="<?php echo $rootUrlImages?>social/blog-image.png" class="img-responsive" />
                                            </a>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Earning-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-blog-->
                    <!--============= Social Timeline Blog without image ======================-->
                    <div class="social-timeline-video inner-2x innerMTB">
                        <div class="social-card card half innerAll" id="social-timeline-blog-noimage">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-content-blog-nothumb half innerMT">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="blog-nothumb-content">
                                            <a data-id="social-timeline-blog-noimage " data-toggle="modal" data-target="#status_blog_noimage_modal">
                                                <span class="blog-nothumb-head">
                                                        I Made $700 With My First Post & $0.06 With My Third - Steemit: A Lesson in
                                                        Free Market Economics.
                                                    </span>
                                                <span class="blog-nothumb-body">
                                                        When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                                                        I was equal parts excited & skeptical about the whole thing. Then I made my first
                                                        post and after 24 hours I was a believer.
                                                    </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Earning-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--likes & Dislikes-->
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-blog-->
                    <!--Social ADDS-->
                    <div class="social-timeline-video inner-2x innerMTB">
                        <div class="social-card card half innerAll">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-12">
                                        <img src="<?php echo $rootUrlImages?>social/adds.png" class=" img-responsive" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--============= Social Timeline Status ======================-->
                    <div class="social-timeline-status">
                        <div class=" half innerAll card" id="social-timeline-status">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                            tom hanks
                                                        </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                            @tomhankennew
                                                        </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-trash"></i> Remove Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-pencil"></i> Edit Post
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags half innerR">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerR">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-user-status half innerMT">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="user-status-content">
                                            <a data-id="social-timeline-status" data-toggle="modal" href="#status_user_modal">
                                                <span class="text-dark-gray">
                                                            When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                                                                I was equal parts excited & skeptical about the whole thing. Then I made my first
                                                                post and after 24 hours I was a believer.
                                                        </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings -->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-blog-->
                    <!--============= Social Timeline soundcloud ======================-->
                    <div class="social-timeline-sound inner-2x innerMTB">
                        <div class="social-card card half innerAll" data-toggle="modal" data-target="#status_sound_modal" id="social-timeline-sound">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive" />
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                            tom hanks
                                                        </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                            @tomhankennew
                                                        </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-trash"></i> Remove Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-pencil"></i> Edit Post
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--====social-timeline-user-message====-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-details-->
                            <!--social-timeline-details-->
                            <div class="social-timeline-sound">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a data-id="social-timeline-sound">
                                            <iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/298167372&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">
                                            </iframe>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- earnings -->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & Dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                    <!--social-timeline-soundcloud-->
                </div>
                <!-- social-center-panel  -->
            </div>
            <!-- col-xs-6 -->
            <div class="col-xs-3">
                <div class="social-right-panel">
                    <div class="row innerMB inner-2x">
                        <div class="col-xs-12">
                            <div class="pull-left half search-box-comments" style="width:100%;">
                                <div class="input-group" style="width:100%;">
                                    <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required="" style="width:100%;">
                                    <input type="submit" class="search-btn-header" value="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- my-info-card  -->
                    <div class="suggested-card">
                        <div class="card social-card right-panel-modules inner-2x innerMT">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4 class="half innerMTB">Suggested</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="suggested-module">
                                <div class="row margin-none innerTB border-bottom">
                                    <div class="col-xs-3">
                                        <img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png" />
                                    </div>
                                    <div class="padding-left-none col-xs-6">
                                        <div class="social-user-link-name">
                                            <a>shashi tharoor</a>
                                        </div>
                                        <div class="social-user-link">
                                            <a href="#">@shahitharoor</a>
                                        </div>
                                    </div>
                                    <div class=" col-xs-3">
                                        <a href="" class="half innerR"><i class="fa fa-check"></i></a>
                                        <a href=""><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                                <div class="row margin-none innerTB border-bottom ">
                                    <div class="col-xs-3">
                                        <img class="suggested-user-image" src="<?php echo $rootUrlImages?>social/suggested.png" />
                                    </div>
                                    <div class="padding-left-none col-xs-6">
                                        <div class="social-user-link-name">
                                            <a>shashi tharoor</a>
                                        </div>
                                        <div class="social-user-link">
                                            <a href="#">@shahitharoor</a>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <a href="" class="half innerR"><i class="fa fa-check"></i></a>
                                        <a href=""><i class="fa fa-close"></i></a>
                                    </div>
                                </div>
                                <div class="row margin-none half innerTB border-bottom bg-view-all">
                                    <div class="col-xs-12">
                                        <a href="#" class="text-deep-sky-blue pull-right">View all</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    
                </div>
                <!-- social-right-panel  -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>
    <div class="modal fade" id="audioModal" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card clearfix social-form-modal">
                <div class="clearfix innerR innerT">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="audio-post" class="innerAll padding-bottom-none padding-top-none margin-bottom-none" name="audio-post">
                    <div class="audio-form-modal">
                        <div class="audio-form">
                            <div class="innerMAll audio-add-link">
                                <input type="hidden" name="post-type" value="audio">
                                <!-- <input type="hidden" name="posted_by" value="102"> -->
                                <!-- has to change -->
                                <div>
                                    <input type="text" id="audio-input" class="form-control audio-card checkEmpty checkSoundCloudLink" name="asset_url" placeholder="Sound Cloud Title" />
                                </div>
                                <a data-toggle="tooltip" title="Sound cloud" class="sound-cloud-icon">
                                <img src="<?php echo $rootUrlImages; ?>sound_cloud_form_icon.png" alt=""/>
                            </a>
                            </div>
                            <div class="innerMAll">
                                <div>
                                    <textarea type="text" class="form-control preview" name="modalComment" data-id="audio-preview-description" data-API="soundcloud" data-limit="" placeholder="Add Description"></textarea>
                                </div>
                            </div>
                            <div class="innerMAll clearfix">
                                <div id="tags" class="tag-text" data-id="selected-keywords">
                                    <input type="text" class="tag-input checkEmpty preview-tag-change" placeholder="Add #Keywords(min 1-max 3)" />
                                    <input type="hidden" name="modalKeywords" />
                                </div>
                            </div>
                            <div class="innerMAll  pull-left form-select-dropdown">
                                <select class="selectpicker" name="postscope">
                                    <option value="Only Me">Only Me</option>
                                    <option value="Public">Public</option>
                                    <option value="Followers">Followers</option>
                                    <option value="Followers of Followers">Followers of Followers</option>
                                </select>
                            </div>
                            <div class="clearfix">
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Preview" id="audioPreview" class="form-preview-btn" />
                                </div>
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Post" class="form-post-btn" id="audioPost" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="audio-preview-form clearfix innerAll">
                    <div class="col-xs-12">
                        <div class="social-timeline-profile-pic pull-left">
                            <img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $data[" user_ref "]["profile_pic "]; ?>" class=" img-responsive" />
                        </div>
                        <div class="social-timeline-profile-details pull-left innerL">
                            <div class="social-user-link-name">
                                <a href="#">
                                Tom Hank    <?php// echo $data["user_ref"]["first_name"]; ?> <?php// echo $data["user_ref"]["last_name"]; ?>
                                </a>
                            </div>
                            <!---->
                            <div class="social-user-link">
                                <a href="#">
                                    @tomhanknew<?php //echo $data["user_ref"]["account_handle"]; ?>
                                </a>
                            </div>
                            <!--social-user-link-->
                        </div>
                    </div>
                    <div class="col-xs-12 innerMT">
                        <div class="social-keywords-tags half innerB" id="selected-keywords">
                        </div>
                        <div class="innnerB" id="audio-preview-description" style="color: #535353;">
                        </div>
                        <div class="innerB innerT iframe-box">
                            <div class="loader-audio-form">
                                <i class='fa fa-spinner fa-pulse fa-fw'></i>
                            </div>
                            <iframe id="audio-iframe" style="display:none;" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/304747566&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
                        </div>
                        <div class="hide-preview clearfix pull-right">
                            <!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
                            <span><i class='compress-icon fa fa-angle-up'></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Audio Form -->
    <div class="modal fade" id="imageModal" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card social-form-modal">
                <div class="clearfix innerR innerT">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="image-post" name="image-post" class="innerAll padding-bottom-none padding-top-none" enctype="multipart/form-data">
                    <div class="image-form-modal">
                        <div class="image-form">
                            <div class="innerMAll image-form-add-image">
                                <div>
                                    <input type="text" id="image-input" class="form-control image-card checkEmpty" placeholder="Image URL" readonly/>
                                </div>
                                <div class="fileUpload image-form-icon">
                                    <span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
                                    <span class="browse-span">Browse</span>
                                    <input type="file" id="image-form-upload" class="upload" name="fileToUpload" />
                                </div>
                            </div>
                            <div class="innerMAll">
                                <input type="hidden" class="form-control" name="post-type" value="image">
                                <div>
                                    <textarea type="text" class="form-control" name="modalComment" placeholder="Add Description"></textarea>
                                </div>
                            </div>
                            <div class="innerMAll clearfix">
                                <div id="tags" class="tag-text">
                                    <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)" />
                                    <input type="hidden" name="modalKeywords" />
                                </div>
                            </div>
                            <div class="innerMAll pull-left form-select-dropdown">
                                <select class="selectpicker" name="postscope">
                                    <option value="Only Me">Only Me</option>
                                    <option value="Public">Public</option>
                                    <option value="Followers">Followers</option>
                                    <option value="Followers of Followers">Followers of Followers</option>
                                </select>
                            </div>
                            <div class="clearfix">
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Preview" class="form-preview-btn" />
                                </div>
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Post" class="form-post-btn" id="imagePost" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Image Form -->
    <div class="modal fade" id="videoModal" role="dialog">
        <div class="modal-dialog">
            <div class="card social-card social-form-modal">
                <div class="clearfix innerR innerT">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form id="video-post" name="video-post" class="innerAll padding-bottom-none padding-top-none">
                    <div class="video-form-modal">
                        <div class="video-form">
                            <div class="innerMAll video-add-link">
                                <input type="hidden" name="post-type" value="video">
                                <div>
                                    <input type="text" class="form-control video-card checkEmpty checkVideoLink" placeholder="Add Link" name="asset_url" />
                                </div>
                                <a data-toggle="tooltip" title="Vimeo" class="vimeo-icon">
                                <img src="<?php echo $rootUrlImages; ?>vimeo_form_icon.png" alt="">
                            </a>
                                <a data-toggle="tooltip" title="Youtube" class="youtube-icon">
                                <img src="<?php echo $rootUrlImages; ?>youtube_form_icon.png" alt="">
                            </a>
                            </div>
                            <div class="innerMAll">
                                <div>
                                    <textarea type="text" class="form-control" name="modalComment" placeholder="Add Description"></textarea>
                                </div>
                            </div>
                            <div class="innerMAll clearfix">
                                <div id="tags" class="tag-text">
                                    <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)" />
                                    <input type="hidden" name="modalKeywords" />
                                </div>
                            </div>
                            <div class="innerMAll pull-left form-select-dropdown">
                                <select class="selectpicker" name="postscope">
                                    <option value="Only Me">Only Me</option>
                                    <option value="Public">Public</option>
                                    <option value="Followers">Followers</option>
                                    <option value="Followers of Followers">Followers of Followers</option>
                                </select>
                            </div>
                            <div class="clearfix">
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Preview" class="form-preview-btn" />
                                </div>
                                <div class="innerAll clearfix pull-right">
                                    <input type="button" value="Post" class="form-post-btn" id="videoPost" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Video Form -->
    <!-- -->
    <!-- ============= Blog Form starts has to be changed ================================-->
    <link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
    <link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico">
    <link href="../../frontend_li$login_statusbraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
    <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
    <link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
    <!-- ============= Blog Form starts Blog Form starts has to be changed ================================-->
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <div class="modal fade" id="blogModal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix innerR innerT">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form id="blog-post" class="innerAll padding-bottom-none padding-top-none" name="blog-post" enctype="multipart/form-data">
                        <div class="blog-form-modal">
                            <div class="blog-form clearfix">
                                <div class="innerMAll">
                                    <input type="hidden" name="post-type" value="blog">
                                    <div>
                                        <input type="text" id="blog-input" class="form-control blog-card checkEmpty" placeholder="Blog Title" name="modalTitle" />
                                    </div>
                                </div>
                                <div class="hero-unit innerT innerB innerML innerMB innerMR clearfix">
                                    <div id="alerts"></div>
                                    <div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                            </ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn dropdown-toggle social-form-btn" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a data-edit="fontSize 5">
                                                        <font size="5">Huge</font>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-edit="fontSize 3">
                                                        <font size="3">Normal</font>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a data-edit="fontSize 1">
                                                        <font size="1">Small</font>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
                                            <a class="social-blog-form-btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
                                            <a class="social-blog-form-btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
                                            <a class="social-blog-form-btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
                                            <a class="social-blog-form-btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
                                            <a class="social-blog-form-btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
                                            <a class="social-blog-form-btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
                                            <a class="social-blog-form-btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
                                            <a class="social-blog-form-btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
                                            <a class="social-blog-form-btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>
                                            <div class="dropdown-menu input-append">
                                                <input class="span2" placeholder="URL" type="text" data-edit="createLink" />
                                                <button class="btn" type="button">Add</button>
                                            </div>
                                            <a class="social-blog-form-btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
                                            <input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
                                        </div>
                                        <div class="btn-group">
                                            <a class="social-blog-form-btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
                                            <a class="social-blog-form-btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
                                        </div>
                                        <input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
                                    </div>
                                    <div id="editor" palceholder="Blog content" contenteditable="true">
                                    </div>
                                </div>
                                <!-- hero-unit -->
                                <div class="innerMAll clearfix">
                                    <div id="tags" class="tag-text">
                                        <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)" />
                                        <input type="hidden" name="modalKeywords" />
                                    </div>
                                </div>
                                <div class="innerMAll blog-add-link">
                                    <div>
                                        <input type="text" id="blog-img-path" class="form-control blog-card checkEmpty" placeholder="Add Images : " readonly/>
                                    </div>
                                    <div class="fileUpload blog-form-icon">
                                        <span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
                                        <span class="browse-span">Browse</span>
                                        <input type="file" class="upload" id="blog-image-upload" name="img_file_name" />
                                    </div>
                                </div>
                                <div class="innerMAll pull-left form-select-dropdown">
                                    <select class="selectpicker" name="postscope">
                                        <option value="Only Me">Only Me</option>
                                        <option value="Public">Public</option>
                                        <option value="Followers">Followers</option>
                                        <option value="Followers of Followers">Followers of Followers</option>
                                    </select>
                                </div>
                                <div class="clearfix">
                                    <div class="innerAll clearfix pull-right">
                                        <input type="button" value="Preview" class="form-preview-btn" />
                                    </div>
                                    <div class="innerAll clearfix pull-right">
                                        <input type="button" value="Post" class="form-post-btn" id="blogPost" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Blog Form ends-->
        <!-- ================= status Image Modal =================== -->
        <div class="modal fade" id="status_image_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-image">
                        <div class="social-card card half innerAll" id="social-timeline-image">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#" class="ellipses ellipses-general">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-keywords-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--  content-image-->
                            <div class="social-timeline-content-image">
                                <div class="row">
                                    <div class="col-xs-12 post-display-img">
                                        <a data-toggle="modal" href="#status_image_modal">
                                            <img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive modal-img-size">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-earning-comments-view-->
                            <!--social-timeline-likes-dislikes-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                        <div class="comments-module bg-light-purple">
                            <div class="row comments-module-wrapper">
                                <div class="col-xs-1">
                                    <img src="<?php echo $rootUrlImages?>social/comment.png">
                                </div>
                                <div class="col-xs-10">
                                    <div class="form-group">
                                        <input type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row comments-module-wrapper">
                                <div class="col-xs-1">
                                    <img src="<?php echo $rootUrlImages?>social/comment.png">
                                </div>
                                <div class="col-xs-10">
                                    <div class="comments-module-comment-block">
                                        <a href="#" class="comments-module-username">David Mondrus</a>
                                        <span class="comments-module-comment">not "totally" new</span>
                                    </div>
                                    <div class="comments-module-time-block">
                                        <a href="#" class="comments-module-reply">Reply</a>
                                        <span class="comments-module-hrs">23 hrs</span>
                                    </div>
                                </div>
                            </div>
                            <!--row -->
                            <div class="row comments-module-wrapper">
                                <div class="col-xs-1">
                                    <img src="<?php echo $rootUrlImages?>social/comment.png">
                                </div>
                                <div class="col-xs-10">
                                    <div class="comments-module-comment-block">
                                        <a href="#" class="comments-module-username">David Mondrus</a>
                                        <span class="comments-module-comment">not "totally" new</span>
                                    </div>
                                    <div class="comments-module-time-block">
                                        <a href="#" class="comments-module-reply">Reply</a>
                                        <span class="comments-module-hrs">23 hrs</span>
                                    </div>
                                </div>
                            </div>
                            <!--comments-module-wrapper -->
                            <div class="row comments-module-wrapper">
                                <div class="col-xs-12">
                                    <a href="#" class="comments-module-all-comments">View <span class="comment-count">21</span> more comments </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ================= status video Modal =================== -->
        <div class="modal fade" id="status_video_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-video">
                        <div class="social-card card half innerAll">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-user-message-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-video-->
                            <div class="social-timeline-content-video">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div align="center" class="embed-responsive embed-responsive-16by9" data-toggle="modal" data-target="#status_video_modal">
                                            <!-- <video loop class="embed-responsive-item">
                                                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
                                            </video> -->
                                            <iframe width="560" height="258" src="https://www.youtube.com/embed/CMdHDHEuOUE" frameborder="0" allowfullscreen=""></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-video-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- ================= status blog Modal with image =================== -->
        <div class="modal fade" id="status_blog_image_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-video">
                        <div class="social-card card half innerAll" id="social-timeline-blog">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                            tom hanks
                                                        </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                            @tomhankennew
                                                        </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-8">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="#" class="text-deep-sky-blue pull-right" target="_blank">Go to post page</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--====social-timeline-user-message====-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-details-->
                            <!--social-timeline-details-->
                            <div class="social-timeline-content-blog-thumb">
                                <div class="row">
                                    <a data-id="social-timeline-blog" onclick="postOpenModel(this)">
                                    </a>
                                    <div class="col-xs-12">
                                        <a data-id="social-timeline-blog" onclick="postOpenModel(this)">
                                        </a>
                                        <a data-toggle="modal" data-target="#status_blog_image_modal">
                                            <img src="<?php echo $rootUrlImages?>social/blog-image.png" class="img-responsive">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Earning-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- ================= Image blog  Modal with no-image=================== -->
        <div class="modal fade" id="status_blog_noimage_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-video">
                        <div class="social-card card half innerAll" id="social-timeline-blog-noimage">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                        tom hanks
                                                    </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                        @tomhankennew
                                                    </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Save Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-eye-slash"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-file-text"></i> Report Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow User
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-key"></i> Unfollow #<span>keyword</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-user-times"></i> Unfollow #<span>Account</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-8">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                    <div class="col-xs-4">
                                        <a href="#" class="text-deep-sky-blue pull-right" target="_blank">Go to post page</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-content-blog-nothumb half innerMT">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="blog-nothumb-content">
                                            <a data-id="social-timeline-blog-noimage " data-toggle="modal" data-target="#status_blog_noimage_modal">
                                                <span class="blog-nothumb-head">
                                                        I Made $700 With My First Post &amp; $0.06 With My Third - Steemit: A Lesson in
                                                        Free Market Economics.
                                                    </span>
                                                <span class="blog-nothumb-body">
                                                        When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                                                        I was equal parts excited &amp; skeptical about the whole thing. Then I made my first
                                                        post and after 24 hours I was a believer.
                                                    </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Earning-->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--likes & Dislikes-->
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- ================= status user Modal =================== -->
        <div class="modal fade" id="status_user_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-status">
                        <div class=" half innerAll card" id="social-timeline-status">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                            tom hanks
                                                        </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                            @tomhankennew
                                                        </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-trash"></i> Remove Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-pencil"></i> Edit Post
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags half innerR">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerR">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-user-status half innerMT">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="user-status-content">
                                            <a data-id="social-timeline-status" data-toggle="modal" href="#status_user_modal">
                                                <span class="text-dark-gray">
                                                            When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
                                                                I was equal parts excited &amp; skeptical about the whole thing. Then I made my first
                                                                post and after 24 hours I was a believer.
                                                        </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-nothumb-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Earnings -->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- ================= status soundcloud Modal =================== -->
        <div class="modal fade" id="status_sound_modal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-sound">
                        <div class="social-card card half innerAll" data-toggle="modal" data-target="#status_sound_modal" id="social-timeline-sound">
                            <div class="">
                                <div class="row half innerTB">
                                    <div class="col-xs-8">
                                        <div class="social-timeline-profile-pic pull-left">
                                            <img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive">
                                        </div>
                                        <div class="social-timeline-profile-details pull-left innerL">
                                            <div class="social-user-link-name">
                                                <a href="#">
                                                            tom hanks
                                                        </a>
                                            </div>
                                            <!---->
                                            <div class="social-user-link">
                                                <a href="#">
                                                            @tomhankennew
                                                        </a>
                                            </div>
                                            <!--social-user-link-->
                                        </div>
                                        <!--social-timeline-profile-details-->
                                    </div>
                                    <div class="col-xs-4 social-post-options">
                                        <div class="dropdown pull-right">
                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="fa fa-chevron-down"></span>
                                            </a>
                                            <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-trash"></i> Remove Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-bookmark"></i> Hide Post
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <i class="fa fa-pencil"></i> Edit Post
                                                    </a>
                                                </li>
                                            </ul>
                                            <span><i class="fa  fa-dot-circle-o cdp-red"></i></span>
                                        </div>
                                        <br>
                                        <div class="pull-right innerT">
                                            <span class="social-post-time"><span>3</span> hrs</span>
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="social-timeline-keywords-details">
                                <div class="row half innerT">
                                    <div class="col-xs-12">
                                        <a href="#" class="social-keywords-tags">#bitcoin</a>
                                        <a href="#" class="social-keywords-tags half innerL">#India</a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-tags-details-->
                            <!--====social-timeline-user-message====-->
                            <div class="social-timeline-content-message">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="innerMB">
                                            The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost. Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-details-->
                            <!--social-timeline-details-->
                            <div class="social-timeline-sound">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <a data-id="social-timeline-sound">
                                            <iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/298167372&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">
                                            </iframe>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--social-timeline-content-blog-image-->
                            <div class="social-timeline-earning-comments-view innerT">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div>
                                            <label>Earning : </label>
                                            <span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
                                        </div>
                                    </div>
                                    <div class="col-xs-8">
                                        <div class="">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <span class="half innerR">40</span>
                                                        <label class="pull-right">Comments</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <span class="half innerR">456</span>
                                                        <label>Shares</label>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <span class="half innerR">557k</span>
                                                        <label>Views</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- earnings -->
                            <div class="social-timeline-likes-dislikes">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="social-likes-dislikes">
                                            <ul class="list-inline margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-up half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-thumbs-o-down half innerR"></i>
                                                            <span>770</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="social-comments-shares-views">
                                            <ul class="list-inline pull-right margin-bottom-none">
                                                <li>
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-comments-o"></i>
                                                            <span>Comments</span>
                                                        </a>
                                                    </div>
                                                </li>
                                                <li class="padding-right-none">
                                                    <div>
                                                        <a>
                                                            <i class="fa fa-share-square-o"></i>
                                                            <span>Share</span>
                                                        </a>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- likes & Dislikes -->
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="share-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="card social-card social-form-modal">
                    <div class="clearfix closebutton-absolute">
                       <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class="social-timeline-image">
                        <div class="social-card card half innerAll" id="social-timeline-image">
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="text-social-primary border-bottom">Share Post</h4>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!--social-timeline-details-->
                            <div class="innerTB">
                                <div class="col-xs-6">
                                    <label>Share on your wall</label>
                                </div>
                                <div class="col-xs-6">
                                    <span class="pull-right">
                                                <label>Others:</label>
                                            <a><i class="fa fa-facebook-square"></i></a>
                                            <a><i class="fa fa-twitter-square"></i></a>
                                            <a><i class="fa fa-linkedin-square"></i></a>
                                        </span>
                                </div>
                            </div>
                            <div class=" innerTB">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class=" innerTB">
                                <div class="row">
                                    <div class="col-xs-12">
                                        Image will come here
                                    </div>
                                </div>
                            </div>
                            <div class="">
                                <div class="row">
                                    <div class="col-xs-8">
                                        <div class="col-xs-9 padding-none">
                                            <select class="selectpicker" name="postscope">
                                                <option value="Only Me">Only Me</option>
                                                <option value="Public">Public</option>
                                                <option value="Followers">Followers</option>
                                                <option value="Followers of Followers">Followers of Followers</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-3">
                                            <div class="pull-right ">
                                                <a class="social-form-buttons" href="#" id="statusPost">
                                                    Post
                                                     </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-card-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Likes List Modal -->
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="">
                        <div class="modal-header half innerB">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-blue"><i class="fa fa-thumbs-up innerMR"></i>93</h4>
                        </div>
                        <div class="modal-body like-list-body padding-none">
                            <?php for($i = 0 ; $i < 10; $i++){?>
                            <div class="innerAll border-bottom clearfix">
                                <div class="innerML half pull-left">
                                    <img src="<?php echo $rootUrl?>/images/uploadingcontent.png" class="liked-user-img img-responsive img-circle">
                                </div>
                                <div class=" pull-left text-blue innerMT innerLR">
                                    <div class="liked-user-name">Ban Fernandis</div>
                                </div>
                                <div class="pull-left innerMT innerLR">
                                    <div class="liked-user-account-handle text-color-Gray">@babbuFer</div>
                                </div>
                                <div class="innerAll half pull-right">
                                    <i class="text-blue half innerT fa fa-ellipsis-v"></i>
                                </div>
                                <div class="innerMT innerMR half pull-right">
                                    <input type="button" class="btn-social-wid-auto-dark pull-right" value="Unfollow">
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        
        <script>
        $('.trending-content li a').on('click', function() {
            var j = $('.trending-content li a');
            $('.trending-content li').each(function() {
                var k = $(this).find('a').data('id');
                $(k).hide();
                $(j).removeClass('active');
            });
            var i = $(this).data('id');
            $(i).show();
            $(this).addClass('active');
        });



        $('.popular-modules li a').on('click', function() {
            var j = $('.popular-modules li a');
            $('.popular-modules li').each(function() {
                var k = $(this).find('a').data('id');
                $(k).hide();
                $(j).removeClass('active');
            });
            var i = $(this).data('id');
            $(i).show();
            $(this).addClass('active');
        });

        //Bookmark click
        $(".bookmark-click").click(function() {
            console.log($(this).find("i"));
            if ($(this).find("i").hasClass("fa-star-o")) {
                $(this).find("i").attr("class", "fa fa-star text-yellow");
            } else {
                $(this).find("i").attr("class", "fa fa-star-o");
            }
        });

        //Like click
        // $(".likes-click").click(function() {
        //     if ($(this).hasClass("fa-thumbs-o-up")) {
        //         $(this).removeClass("fa-thumbs-o-up");
        //         $(this).addClass("fa-thumbs-up");
        //     }
        // });

        //like hover
        $(".like-area").hover(function() {
            if ($(this).find("i").hasClass("fa-thumbs-up")) {
                $(".likes-click").css("cursor", "not-allowed");
                $(".likes-click").css("opacity", "0.6");
            } else {
                $(".likes-click").css("cursor", "pointer");
                $(".likes-click").css("opacity", "1");
            }
        });

        //comment click
        $(".comment-click").click(function() {
            //alert("here");
            $(".comments-section").slideDown();
        });

        //reply click
        $(".comments-module-reply").click(function() {
            $(".reply-box").slideDown();
        });

        $("#close").click(function(){
            $(".panel").hide();
        })

        $(".like-area").click(function(){
             $(this).find("i").addClass("fa-coin-hand").removeClass("fa-thumbs-o-up");
          
          
        });

        var anchors = document.querySelectorAll('a.like-area')

        Array.prototype.forEach.call(anchors, function(anchor) {
            anchor.addEventListener('click', explode)
        })

        function explode(e) {
            [].map.call(anchors, function(el) {
                   el.classList.toggle('active');
             });
            var x = e.clientX
            var y = e.clientY
            var c = document.createElement('canvas')
            var ctx = c.getContext('2d')
            var ratio = window.devicePixelRatio
            var particles = []
            var cardDormer = document.getElementById("abc");
            cardDormer.appendChild(c)
            
            c.style.position = 'absolute'
            c.style.left = (-25) + 'px'
            c.style.top = (-25) + 'px'
            c.style.pointerEvents = 'none'
            c.style.width = 50 + 'px'
            c.style.height = 50 + 'px'
            c.width = 100 * ratio
            c.height = 100 * ratio
            
            function Particle() {
                return {
                    x: c.width / 2,
                    y: c.height / 2,
                    radius: 10,
                    //color: 'rgb(' + [r(100,255), r(100,255), r(100,255)].join(',') + ')',
                    color:'rgb(0,0,0)',
                    rotation: r(0,360, true),
                    speed:4,
                    friction: 0.9,
                    opacity: r(0,0.5, true),
                    yVel: 0,
                    gravity: 0
                }
            }
            
            for(var i=0; ++i<5;) {
                particles.push(Particle())
            }
            
            
            
            function render() {
                ctx.clearRect(0, 0, c.width, c.height)
                
                particles.forEach(function(p, i) {
                    
                    angleTools.moveOnAngle(p, p.speed)
                    
                    p.opacity -= 0.01
                    p.speed *= p.friction
                    p.radius *= p.friction
                    
                    p.yVel += p.gravity
                    p.y += p.yVel
                    
                    if(p.opacity < 0) return
                    if(p.radius < 0) return
                    
                    ctx.beginPath()
                    ctx.globalAlpha = p.opacity
                    ctx.fillStyle = p.color
                    ctx.arc(p.x, p.y, p.radius, 0, 2 * Math.PI, false)
                    ctx.fill()
                })
            }
            
            ;(function renderLoop(){
                requestAnimationFrame(renderLoop)
                render()
            })()
            
            setTimeout(function() {
                cardDormer.removeChild(c)
            }, 3000)
        }

        var angleTools={getAngle:function(t,n){var a=n.x-t.x,e=n.y-t.y;return Math.atan2(e,a)/Math.PI*180},getDistance:function(t,n){var a=t.x-n.x,e=t.y-n.y;return Math.sqrt(a*a+e*e)},moveOnAngle:function(t,n){var a=this.getOneFrameDistance(t,n);t.x+=a.x,t.y+=a.y},getOneFrameDistance:function(t,n){return{x:n*Math.cos(t.rotation*Math.PI/180),y:n*Math.sin(t.rotation*Math.PI/180)}}};
        function r(a,b,c){ return parseFloat((Math.random()*((a?a:1)-(b?b:0))+(b?b:0)).toFixed(c?c:0)); }
        </script>
