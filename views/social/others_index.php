<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");
		$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css?<?php echo date('l jS \of F Y h:i:s A'); ?>"
			type="text/css"/>
<main class="social-main-container inner-7x innerT" >
<div class="container">
	<div class="col-xs-3">
		<div class="social-left-panel left-panel-modules">
		  <div class="card my-info-card social-card">
			  <div class="clearfix innerAll padding-bottom-none">
					<div class="my-info-img pull-left innerMR">
			      <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG"/>
			    </div>
					<!-- my-picture  -->
			    <div class="my-info-detail pull-left">
			      <div class="my-info-name-container text-blue innerMB">
							<!-- my-name  -->
			        <span class="my-info-name"><?php echo $_SESSION["first_name"] . ' ' . $_SESSION["last_name"]?></span>
			        <!-- <a class="my-info-name-edit pull-right">
			          <i class="fa fa-pencil" aria-hidden="true"></i>
			        </a> -->
			      </div>



						<!-- my-status  -->
				  <div class="my-info-status">
				      <a id="my-info-name-edit"
							data-type="textarea"
							title="Change Description"
							data-pk="1"
							data-placement="right"
							data-title="Enter username">
								<?php
								 	$userDetail = getUserInfo($_SESSION["email"], $walletURLIPnotification . 'api/notify/v2/', 'short_description');
								 	echo $userDetail['errMsg']['short_description'];
								?>
				      </a>
			      </div>
			    </div>
				</div>
				<!-- my-info-detail  -->
				<div class="clearfix half innerT">
					<div class="col-xs-12  innerB">
						<div class="col-xs-4 padding-none pull-left">
							<div class="text-deep-sky-blue my-info-posts">
								Posts
							</div>
							<div class="text-blue innerL my-info-posts-count">
								3
							</div>
						</div>
						<div class="col-xs-4 padding-none">
							<div class="text-deep-sky-blue my-info-following">
								Following
							</div>
							<div class="text-blue my-info-following-count inner-2x innerL">
								37
							</div>
						</div>
						<div class="col-xs-4 padding-none text-right">
							<div class="text-deep-sky-blue my-info-followers">
								Followers
							</div>
							<div class="text-blue my-info-followers-count inner-2x innerR">
								31
							</div>
						</div>
					</div>
          <div class="col-xs-12 half innerB">
            <button class="btn-trading-wid-auto pull-right boxshadow">Follow</button>
          </div>
				</div>
				<!-- posts-followers-following  -->
			</div>
			<!-- my-info-card  -->
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-6">
		<div class="social-center-panel half innerLR">


				<!-- =================== social-all-status-tabs ================== -->
					<div class="social-all-status-tabs">
						<div class="row">
							<div class="col-xs-12">
								<ul class="list-inline">
									<li>
										<a href="#" class="active">ALL</a>
									</li>
									<li>
										<a href="#">STATUS</a>
									</li>
									<li>
										<a>BLOG</a>
									</li>
									<li>
										<a >VIDEO</a>
									</li>
									<li>
										<a>IMAGE</a>
									</li>
									<li>
										<a>AUDIO</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<!-- social-all-status-tabs -->
					<!--display content-->
					<div id="private-post-data">
						<!--content from ajax-->
				</div>
				<!--display content-->

					<!--============= Social Timeline Image ======================-->
					<div class="social-timeline-image innerMTB" >
						<div class="social-card card half innerAll" id="social-timeline-image">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-8">
											<div class="social-timeline-profile-pic pull-left">
												<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
											</div>
											<div class="social-timeline-profile-details pull-left innerL">
												<div class="social-user-link-name">
													<a href="#">
														tom hanks
													</a>
												</div>
												<!---->
												<div class="social-user-link">
													<a href="#">
														@tomhankennew
													</a>
												</div>
												<!--social-user-link-->
											</div>
											<!--social-timeline-profile-details-->
										</div>
										<div class="col-xs-4 social-post-options">
											<div class="dropdown pull-right">
												<a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="fa fa-chevron-down"></span>
												</a>
												<ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
												<li>
													<a href="#">
														<i class="fa fa-bookmark"></i>
														Save Post
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-eye-slash"></i>
														Hide Post
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-file-text"></i>
														Report Post
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-user-times"></i>
														Unfollow User
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-key"></i>
														Unfollow #<span>keyword</span>
													</a>
												</li>
												<li>
													<a href="#">
														<i class="fa fa-user-times"></i>
														Unfollow #<span>Account</span>
													</a>
												</li>
											</ul>
												<span><i class="fa  fa-dot-circle-o cdp-green"></i></span>
											</div>
											<br />
											<div class="pull-right innerT">
												<span class="social-post-time"><span>3</span> hrs</span>
											</div>
										</div>
									</div>
									<!--row-->
								</div>
								<!--social-timeline-details-->

								<div class="social-timeline-keywords-details">
									<div class="row">
										<div class="col-xs-12">
											<a href="#" class="social-keywords-tags">#bitcoin</a>
											<a href="#" class="social-keywords-tags half innerL">#India</a>
										</div>
									</div>
								</div>
								<!--social-timeline-keywords-details-->

								<!--social-timeline-user-message-->
								<div class="social-timeline-content-message">
									<div class="row">
										<div class="col-xs-12">
											<p class="innerMB">
												The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
												Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
											</p>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->

								<!--  content-image-->
								<div class="social-timeline-content-image">
									<div class="row">
										<div class="col-xs-12">
										<a  onclick="postOpenModel(this)" data-id="social-timeline-image">
											<img src="<?php echo $rootUrlImages?>social/timeline-image.png" class="img-responsive" />
										</a>
										</div>
									</div>
								</div>
								<!--social-timeline-content-image-->

								<div class="social-timeline-earning-comments-view innerT">
									<div class="row">
										<div class="col-xs-4">
											<div>
												<label>Earning : </label>
												<span>24.25 <span class="currency"><?php echo $keywoDefaultCurrencyName; ?></span></span>
											</div>
										</div>
										<div class="col-xs-8">
											<div class="">
												<ul class="list-inline pull-right margin-bottom-none">
													<li>
														<div>
															<span class="half innerR">40</span>
															<label class="pull-right">Comments</label>
														</div>
													</li>
													<li>
														<div>
															<span class="">456</span>
															<label>Shares</label>
														</div>
													</li>
													<li class="padding-right-none">
														<div>
															<span class="">557k</span>
															<label>Views</label>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
								<!--social-timeline-earning-comments-view-->

								<!--social-timeline-likes-dislikes-->
								<div class="social-timeline-likes-dislikes">
									<div class="row">
										<div class="col-xs-6">
											<div class="social-likes-dislikes">
												<ul class="list-inline margin-bottom-none">
													<li>
														<div>
															<a>
																<i class="fa fa-thumbs-o-up half innerR"></i>
																<span>770</span>
															</a>
														</div>
													</li>
													<li>
														<div>
															<a>
																<i class="fa fa-thumbs-o-down half innerR"></i>
																<span>770</span>
															</a>
														</div>
													</li>
												</ul>
											</div>
										</div>
										<div class="col-xs-6">
											<div class="social-comments-shares-views">
												<ul class="list-inline pull-right margin-bottom-none">
													<li>
														<div>
															<a>
																<i class="fa fa-comments-o"></i>
																<span>Comments</span>
															</a>
														</div>
													</li>
													<li class="padding-right-none">
														<div>
															<a>
																<i class="fa fa-share-square-o"></i>
																<span>Share</span>
															</a>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>

						</div>
						<!--social-card-->
					</div>
					<!--social-timeline-image-->

					<!--============= Social Timeline Video ======================-->
					<div class="social-timeline-video inner-2x innerMTB">
						<div class="social-card card half innerAll">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-8">
											<div class="social-timeline-profile-pic pull-left">
												<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
											</div>
											<div class="social-timeline-profile-details pull-left innerL">
												<div class="social-user-link-name">
													<a href="#">
														tom hanks
													</a>
												</div>
												<!---->
												<div class="social-user-link">
													<a href="#">
														@tomhankennew
													</a>
												</div>
												<!--social-user-link-->
											</div>
											<!--social-timeline-profile-details-->
										</div>
										<div class="col-xs-4">
										</div>
									</div>
									<!--row-->
								</div>
								<!--social-timeline-details-->
								<div class="social-timeline-keywords-details">
									<div class="row half innerT">
										<div class="col-xs-12">
											<a href="#" class="social-keywords-tags">#bitcoin</a>
											<a href="#" class="social-keywords-tags half innerL">#India</a>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->

								<!--social-timeline-user-message-->
								<div class="social-timeline-content-message">
									<div class="row">
										<div class="col-xs-12">
											<p class="innerMB">
												The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
												Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
											</p>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->

								<!--social-timeline-details-->
								<div class="social-timeline-content-video">
									<div class="row">
										<div class="col-xs-12">
											<div align="center" class="embed-responsive embed-responsive-16by9">
										    <!-- <video loop class="embed-responsive-item">
										        <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
										    </video> -->
												<iframe width="560" height="258" src="https://www.youtube.com/embed/CMdHDHEuOUE" frameborder="0" allowfullscreen></iframe>
										</div>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->
						</div>
						<!--social-card-->
					</div>
					<!--social-timeline-video-->

					<!--============= Social Timeline Blog ======================-->
					<div class="social-timeline-video inner-2x innerMTB">
						<div class="social-card card half innerAll" id="social-timeline-blog">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-8">
											<div class="social-timeline-profile-pic pull-left">
												<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
											</div>
											<div class="social-timeline-profile-details pull-left innerL">
												<div class="social-user-link-name">
													<a href="#">
														tom hanks
													</a>
												</div>
												<!---->
												<div class="social-user-link">
													<a href="#">
														@tomhankennew
													</a>
												</div>
												<!--social-user-link-->
											</div>
											<!--social-timeline-profile-details-->
										</div>
										<div class="col-xs-4">
										</div>
									</div>
									<!--row-->
								</div>
								<!--social-timeline-details-->

								<div class="social-timeline-keywords-details">
									<div class="row half innerT">
										<div class="col-xs-12">
											<a href="#" class="social-keywords-tags">#bitcoin</a>
											<a href="#" class="social-keywords-tags half innerL">#India</a>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->

								<!--====social-timeline-user-message====-->
								<div class="social-timeline-content-message">
									<div class="row">
										<div class="col-xs-12">
											<p class="innerMB">
												The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
												Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
											</p>
										</div>
									</div>
								</div>
								<!--social-timeline-content-details-->

								<!--social-timeline-details-->
								<div class="social-timeline-content-blog-thumb">
									<div class="row">
										<a data-id="social-timeline-blog" onclick="test(this)">
										<div class="col-xs-12">
											<img src="<?php echo $rootUrlImages?>social/blog-image.png" class="img-responsive" />
										</div>
										</a>
									</div>
								</div>
								<!--social-timeline-content-blog-image-->
						</div>
						<!--social-card-->
					</div>
					<!--social-timeline-blog-->

					<!--============= Social Timeline Blog without image ======================-->
					<div class="social-timeline-video inner-2x innerMTB">
						<div class="social-card card half innerAll" id="social-timeline-blog-noimage">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-8">
											<div class="social-timeline-profile-pic pull-left">
												<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
											</div>
											<div class="social-timeline-profile-details pull-left innerL">
												<div class="social-user-link-name">
													<a href="#">
														tom hanks
													</a>
												</div>
												<!---->
												<div class="social-user-link">
													<a href="#">
														@tomhankennew
													</a>
												</div>
												<!--social-user-link-->
											</div>
											<!--social-timeline-profile-details-->
										</div>
										<div class="col-xs-4">
										</div>
									</div>
									<!--row-->
								</div>
								<!--social-timeline-details-->

								<div class="social-timeline-keywords-details">
									<div class="row half innerT">
										<div class="col-xs-12">
											<a href="#" class="social-keywords-tags">#bitcoin</a>
											<a href="#" class="social-keywords-tags half innerL">#India</a>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->


								<!--social-timeline-content-blog-nothumb-->
								<div class="social-timeline-content-blog-nothumb half innerMT">
									<div class="row">
										<div class="col-xs-12">
											<div class="blog-nothumb-content">
												<a data-id="social-timeline-blog-noimage " onclick="test(this)">
													<span class="blog-nothumb-head">
														I Made $700 With My First Post & $0.06 With My Third - Steemit: A Lesson in
														Free Market Economics.
													</span>
													<span class="blog-nothumb-body">
														When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
														I was equal parts excited & skeptical about the whole thing. Then I made my first
														post and after 24 hours I was a believer.
													</span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!--social-timeline-content-blog-nothumb-->
						</div>
						<!--social-card-->
					</div>
					<!--social-timeline-blog-->

					<!--============= Social Timeline Blog ======================-->
					<div class="social-timeline-video inner-2x innerMTB">
						<div class="social-card card half innerAll">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-8">
											<div class="social-timeline-profile-pic pull-left">
												<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
											</div>
											<div class="social-timeline-profile-details pull-left innerL">
												<div class="social-user-link-name">
													<a href="#">
														tom hanks
													</a>
												</div>
												<!---->
												<div class="social-user-link">
													<a href="#">
														@tomhankennew
													</a>
												</div>
												<!--social-user-link-->
											</div>
											<!--social-timeline-profile-details-->
										</div>
										<div class="col-xs-4">
										</div>
									</div>
									<!--row-->
								</div>
								<!--social-timeline-details-->

								<div class="social-timeline-keywords-details">
									<div class="row half innerT">
										<div class="col-xs-12">
											<a href="#" class="social-keywords-tags">#bitcoin</a>
											<a href="#" class="social-keywords-tags half innerL">#India</a>
										</div>
									</div>
								</div>
								<!--social-timeline-tags-details-->


								<!--social-timeline-content-blog-nothumb-->
								<div class="social-timeline-content-blog-nothumb half innerMT">
									<div class="row">
										<div class="col-xs-12">
											<div class="blog-nothumb-content">
												<a>
													<span class="blog-nothumb-head">
														I Made $700 With My First Post & $0.06 With My Third - Steemit: A Lesson in
														Free Market Economics.
													</span>
													<span class="blog-nothumb-body">
														When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
														I was equal parts excited & skeptical about the whole thing. Then I made my first
														post and after 24 hours I was a believer.
													</span>
												</a>
											</div>
										</div>
									</div>
								</div>
								<!--social-timeline-content-blog-nothumb-->
						</div>
						<!--social-card-->
					</div>
					<!--social-timeline-blog-->

					<!--Social ADDS-->
					<div class="social-timeline-video inner-2x innerMTB">
						<div class="social-card card half innerAll">
								<div class="">
									<div class="row half innerTB">
										<div class="col-xs-12">
											<img src="<?php echo $rootUrlImages?>social/adds.png" class=" img-responsive"  />
										</div>
									</div>
								</div>
							</div>
						</div>

						<!--============= Social Timeline Status ======================-->
						<div class="social-timeline-status">
							<div class=" half innerAll card" id="social-timeline-status">
									<div class="">
										<div class="row half innerTB">
											<div class="col-xs-8">
												<div class="social-timeline-profile-pic pull-left">
													<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
												</div>
												<div class="social-timeline-profile-details pull-left innerL">
													<div class="social-user-link-name">
														<a href="#">
															tom hanks
														</a>
													</div>
													<!---->
													<div class="social-user-link">
														<a href="#">
															@tomhankennew
														</a>
													</div>
													<!--social-user-link-->
												</div>
												<!--social-timeline-profile-details-->
											</div>
											<div class="col-xs-4">
											</div>
										</div>
										<!--row-->
									</div>
									<!--social-timeline-details-->

									<div class="social-timeline-keywords-details">
										<div class="row half innerT">
											<div class="col-xs-12">
												<a href="#" class="social-keywords-tags half innerR">#bitcoin</a>
												<a href="#" class="social-keywords-tags half innerR">#India</a>
											</div>
										</div>
									</div>
									<!--social-timeline-tags-details-->


									<!--social-timeline-content-blog-nothumb-->
									<div class="social-timeline-user-status half innerMT">
										<div class="row">
											<div class="col-xs-12">
												<div class="user-status-content">
													<a data-id="social-timeline-status" onclick="test(this)">
														<span class="text-dark-gray">
															When I first found  about Steemit through Amanda Rachwitz (check out her blog!)
																I was equal parts excited & skeptical about the whole thing. Then I made my first
																post and after 24 hours I was a believer.
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
									<!--social-timeline-content-blog-nothumb-->
							</div>
							<!--social-card-->
						</div>
						<!--social-timeline-blog-->

						<!--============= Social Timeline soundcloud ======================-->
						<div class="social-timeline-sound inner-2x innerMTB">
							<div class="social-card card half innerAll" id="social-timeline-sound">
									<div class="">
										<div class="row half innerTB">
											<div class="col-xs-8">
												<div class="social-timeline-profile-pic pull-left">
													<img src="<?php echo $rootUrlImages?>social/user-other-pic.png" class=" img-responsive"  />
												</div>
												<div class="social-timeline-profile-details pull-left innerL">
													<div class="social-user-link-name">
														<a href="#">
															tom hanks
														</a>
													</div>
													<!---->
													<div class="social-user-link">
														<a href="#">
															@tomhankennew
														</a>
													</div>
													<!--social-user-link-->
												</div>
												<!--social-timeline-profile-details-->
											</div>
											<div class="col-xs-4">
											</div>
										</div>
										<!--row-->
									</div>
									<!--social-timeline-details-->

									<div class="social-timeline-keywords-details">
										<div class="row half innerT">
											<div class="col-xs-12">
												<a href="#" class="social-keywords-tags">#bitcoin</a>
												<a href="#" class="social-keywords-tags half innerL">#India</a>
											</div>
										</div>
									</div>
									<!--social-timeline-tags-details-->

									<!--====social-timeline-user-message====-->
									<div class="social-timeline-content-message">
										<div class="row">
											<div class="col-xs-12">
												<p class="innerMB">
													The brilliant/Bad Ass Lena Headey and another Brit who's often mistaken as my twin, the genius Nick Frost.
													Excited about these roles because Lena and Nick are playing Julia and Ricky - Paige's mom and dad.
												</p>
											</div>
										</div>
									</div>
									<!--social-timeline-content-details-->

									<!--social-timeline-details-->
									<div class="social-timeline-sound">
										<div class="row">
											<div class="col-xs-12">
											 <a onclick="test(this)" data-id="social-timeline-sound">
												<iframe width="100%" height="150" scrolling="no" frameborder="no"
												src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/298167372&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false">
											</iframe>
											</a>
											</div>
										</div>
									</div>
									<!--social-timeline-content-blog-image-->
							</div>
							<!--social-card-->
						</div>
						<!--social-timeline-soundcloud-->


		</div>
		<!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

	<div class="col-xs-3">
		<div class="social-right-panel">
			<div class="card social-card right-panel-modules">
				<div class="bg-light-gray right-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4><i class="fa fa-user"></i> Personal Details</h4>
							</div>
						</div>
				</div>
						<div class="row margin-none">
							<div class="col-xs-12">
								<div>
									<ul class="personal-details-content">
										<li>
											<label>Name :</label>
											<a>Lorem Ipsum</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>

					<div class="card social-card right-panel-modules inner-2x innerMT">
						<div class="bg-light-gray right-panel-modules-head">
							<div class="row margin-none">
								<div class="col-xs-12">
										<h4><i class="fa fa-briefcase"></i> Work Details</h4>
									</div>
								</div>
						</div>
								<div class="row margin-none">
									<div class="col-xs-12">
										<div>
											<ul class="personal-details-content">
												<li>
													<label>Name :</label>
													<a>Lorem Ipsum</a>
												</li>
												<li>
													<label>Name :</label>
													<a>Lorem Ipsum</a>
												</li>
												<li>
													<label>Name :</label>
													<a>Lorem Ipsum</a>
												</li>
												<li>
													<label>Name :</label>
													<a>Lorem Ipsum</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>

		</div>
		<!-- social-right-panel  -->
	</div>
	<!-- col-xs-3 -->
</div>
<!-- container -->
</main>

<div class="modal fade" id="audioModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card clearfix social-form-modal">
			<div class="clearfix innerR innerT">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form id="audio-post" class="innerAll padding-bottom-none padding-top-none margin-bottom-none" name="audio-post">
				<div class="audio-form-modal">
					<div class="audio-form">
						<div class="innerMAll audio-add-link">
							<input type="hidden" name="post-type" value="audio">
							<!-- <input type="hidden" name="posted_by" value="102"> --><!-- has to change -->
							<div>
								<input type="text" id="audio-input" class="form-control audio-card checkEmpty checkSoundCloudLink" name="asset_url" placeholder="Sound Cloud Title"/>
							</div>
							<a data-toggle="tooltip" title="Sound cloud" class="sound-cloud-icon">
								<img src="<?php echo $rootUrlImages; ?>sound_cloud_form_icon.png" alt=""/>
							</a>
						</div>
						<div class="innerMAll">
							<div>
								<textarea type="text" class="form-control preview" name = "modalComment" data-id="audio-preview-description" data-API="soundcloud" data-limit="" placeholder="Add Description"></textarea>
							</div>
						</div>
						<div class="innerMAll clearfix">
							<div id="tags" class="tag-text" data-id="selected-keywords">
						    <input type="text" class="tag-input checkEmpty preview-tag-change" placeholder="Add #Keywords(min 1-max 3)"/>
								<input type="hidden" name = "modalKeywords"/>
						  </div>
						</div>
						<div class="innerMAll  pull-left form-select-dropdown">
							<select class="selectpicker" name="postscope">
								<option value="Only Me">Only Me</option>
								<option value="Public">Public</option>
								<option value="Followers">Followers</option>
								<option value="Followers of Followers">Followers of Followers</option>
							</select>
						</div>
						<div class="clearfix">
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Preview" id="audioPreview" class="form-preview-btn"/>
							</div>
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Post" class="form-post-btn" id="audioPost" />
							</div>
						</div>
					</div>
				</div>
			</form>
			<div class="audio-preview-form clearfix innerAll">
				<div class="col-xs-12">
					<div class="social-timeline-profile-pic pull-left">
						<img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $data["user_ref"]["profile_pic"]; ?>" class=" img-responsive"  />
					</div>
					<div class="social-timeline-profile-details pull-left innerL">
							<div class="social-user-link-name">
								<a href="#">
								Tom Hank	<?php// echo $data["user_ref"]["first_name"]; ?> <?php// echo $data["user_ref"]["last_name"]; ?>
								</a>
							</div>
							<!---->
							<div class="social-user-link">
								<a href="#">
									@tomhanknew<?php //echo $data["user_ref"]["account_handle"]; ?>
								</a>
							</div>
							<!--social-user-link-->
					</div>
				</div>
				<div class="col-xs-12 innerMT">
					<div class="social-keywords-tags half innerB" id="selected-keywords">

					</div>
					<div class="innnerB" id="audio-preview-description" style="color: #535353;">

					</div>
					<div class="innerB innerT iframe-box">
						<div class="loader-audio-form">
							<i class='fa fa-spinner fa-pulse fa-fw'></i>
						</div>
						<iframe id="audio-iframe" style="display:none;" width="100%" height="100" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/304747566&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
					</div>
					<div class="hide-preview clearfix pull-right">
						<!-- <input type="button" value="Hide Preview" class="form-preview-btn hide-preview"/> -->
						<span><i class='compress-icon fa fa-angle-up'></i></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Audio Form -->

<div class="modal fade" id="imageModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card social-form-modal">
			<div class="clearfix innerR innerT">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form id="image-post" name="image-post" class="innerAll padding-bottom-none padding-top-none" enctype = "multipart/form-data">
				<div class="image-form-modal">
					<div class="image-form">
						<div class="innerMAll image-form-add-image">
							<div>
								<input type="text" id="image-input" class="form-control image-card checkEmpty" placeholder="Image URL" readonly/>
							</div>
							<div class="fileUpload image-form-icon">
								<span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
								<span class="browse-span">Browse</span>
								<input type="file" id="image-form-upload" class="upload" name="fileToUpload" />
							</div>
						</div>
						<div class="innerMAll">
							<input type="hidden" class="form-control" name="post-type" value="image">
							<div>
								<textarea type="text" class="form-control" name = "modalComment" placeholder="Add Description"></textarea>
							</div>
						</div>
						<div class="innerMAll clearfix">
							<div id="tags" class="tag-text">
						    <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)"/>
								<input type="hidden" name = "modalKeywords" />
						  </div>
						</div>
						<div class="innerMAll pull-left form-select-dropdown">
							<select class="selectpicker" name="postscope">
								<option value="Only Me">Only Me</option>
								<option value="Public">Public</option>
								<option value="Followers">Followers</option>
								<option value="Followers of Followers">Followers of Followers</option>
							</select>
						</div>
						<div class="clearfix">
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Preview" class="form-preview-btn"/>
							</div>
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Post" class="form-post-btn" id="imagePost" />
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Image Form -->

<div class="modal fade" id="videoModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card social-form-modal">
			<div class="clearfix innerR innerT">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form id="video-post" name="video-post" class="innerAll padding-bottom-none padding-top-none" >
				<div class="video-form-modal">
					<div class="video-form">
						<div class="innerMAll video-add-link">
							<input type="hidden" name="post-type" value="video">
							<div>
								<input type="text" class="form-control video-card checkEmpty checkVideoLink" placeholder="Add Link" name="asset_url"/>
							</div>
							<a data-toggle="tooltip" title="Vimeo" class="vimeo-icon">
								<img src="<?php echo $rootUrlImages; ?>vimeo_form_icon.png" alt="">
							</a>
							<a data-toggle="tooltip" title="Youtube" class="youtube-icon">
								<img src="<?php echo $rootUrlImages; ?>youtube_form_icon.png" alt="">
							</a>
						</div>
						<div class="innerMAll">
							<div>
								<textarea type="text" class="form-control" name = "modalComment" placeholder="Add Description"></textarea>
							</div>
						</div>
						<div class="innerMAll clearfix">
							<div id="tags" class="tag-text">
						    <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)"/>
								<input type="hidden" name = "modalKeywords" />
						  </div>
						</div>
						<div class="innerMAll pull-left form-select-dropdown">
							<select class="selectpicker" name="postscope">
								<option value="Only Me">Only Me</option>
								<option value="Public">Public</option>
								<option value="Followers">Followers</option>
								<option value="Followers of Followers">Followers of Followers</option>
							</select>
						</div>
						<div class="clearfix">
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Preview" class="form-preview-btn"/>
							</div>
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Post" class="form-post-btn" id="videoPost"/>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Video Form -->


<!-- Blog Form starts-->
<link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
<link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico" >
<link href="../../frontend_li$login_statusbraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
<!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
<script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
<link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">


<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<div class="modal fade" id="blogModal" role="dialog">
	<div class="modal-dialog">
		<div class="card social-card social-form-modal">
			<div class="clearfix innerR innerT">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<form id="blog-post" class="innerAll padding-bottom-none padding-top-none" name="blog-post" enctype = "multipart/form-data" >
				<div class="blog-form-modal">
					<div class="blog-form clearfix">
						<div class="innerMAll">
							<input type="hidden" name="post-type" value="blog">
							<div>
								<input type="text" id="blog-input" class="form-control blog-card checkEmpty" placeholder="Blog Title" name="modalTitle"/>
							</div>
						</div>
						<div class="hero-unit innerT innerB innerML innerMB innerMR clearfix">
							<div id="alerts"></div>
							<div class="btn-toolbar" data-role="editor-toolbar" data-target="#editor">
								<div class="btn-group">
									<a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" title="Font"><i class="icon-font"></i><b class="caret"></b></a>
										<ul class="dropdown-menu">
										</ul>
									</div>
								<div class="btn-group">
									<a class="social-blog-form-btn dropdown-toggle social-form-btn" data-toggle="dropdown" title="Font Size"><i class="icon-text-height"></i>&nbsp;<b class="caret"></b></a>
										<ul class="dropdown-menu">
										<li><a data-edit="fontSize 5"><font size="5">Huge</font></a></li>
										<li><a data-edit="fontSize 3"><font size="3">Normal</font></a></li>
										<li><a data-edit="fontSize 1"><font size="1">Small</font></a></li>
										</ul>
								</div>
								<div class="btn-group">
									<a class="social-blog-form-btn" data-edit="bold" title="Bold (Ctrl/Cmd+B)"><i class="icon-bold"></i></a>
									<a class="social-blog-form-btn" data-edit="italic" title="Italic (Ctrl/Cmd+I)"><i class="icon-italic"></i></a>
									<a class="social-blog-form-btn" data-edit="strikethrough" title="Strikethrough"><i class="icon-strikethrough"></i></a>
									<a class="social-blog-form-btn" data-edit="underline" title="Underline (Ctrl/Cmd+U)"><i class="icon-underline"></i></a>
								</div>
								<div class="btn-group">
									<a class="social-blog-form-btn" data-edit="insertunorderedlist" title="Bullet list"><i class="icon-list-ul"></i></a>
									<a class="social-blog-form-btn" data-edit="insertorderedlist" title="Number list"><i class="icon-list-ol"></i></a>
									<a class="social-blog-form-btn" data-edit="outdent" title="Reduce indent (Shift+Tab)"><i class="icon-indent-left"></i></a>
									<a class="social-blog-form-btn" data-edit="indent" title="Indent (Tab)"><i class="icon-indent-right"></i></a>
								</div>
								<div class="btn-group">
									<a class="social-blog-form-btn" data-edit="justifyleft" title="Align Left (Ctrl/Cmd+L)"><i class="icon-align-left"></i></a>
									<a class="social-blog-form-btn" data-edit="justifycenter" title="Center (Ctrl/Cmd+E)"><i class="icon-align-center"></i></a>
									<a class="social-blog-form-btn" data-edit="justifyright" title="Align Right (Ctrl/Cmd+R)"><i class="icon-align-right"></i></a>
									<a class="social-blog-form-btn" data-edit="justifyfull" title="Justify (Ctrl/Cmd+J)"><i class="icon-align-justify"></i></a>
								</div>
								<div class="btn-group">
								<a class="social-blog-form-btn dropdown-toggle" data-toggle="dropdown" title="Hyperlink"><i class="icon-link"></i></a>
									<div class="dropdown-menu input-append">
										<input class="span2" placeholder="URL" type="text" data-edit="createLink"/>
										<button class="btn" type="button">Add</button>
									</div>
									<a class="social-blog-form-btn" data-edit="unlink" title="Remove Hyperlink"><i class="icon-cut"></i></a>

								</div>

								<div class="btn-group">
									<a class="social-blog-form-btn" title="Insert picture (or just drag & drop)" id="pictureBtn"><i class="icon-picture"></i></a>
									<input type="file" data-role="magic-overlay" data-target="#pictureBtn" data-edit="insertImage" />
								</div>
								<div class="btn-group">
									<a class="social-blog-form-btn" data-edit="undo" title="Undo (Ctrl/Cmd+Z)"><i class="icon-undo"></i></a>
									<a class="social-blog-form-btn" data-edit="redo" title="Redo (Ctrl/Cmd+Y)"><i class="icon-repeat"></i></a>
								</div>
								<input type="text" data-edit="inserttext" id="voiceBtn" x-webkit-speech="">
							</div>

							<div id="editor" palceholder="Blog content" contenteditable="true">
							</div>

						</div>
						<!-- hero-unit -->
						<div class="innerMAll clearfix">
							<div id="tags" class="tag-text">
						    <input type="text" class="tag-input checkEmpty" placeholder="Add #Keywords(min 1-max 3)"/>
								<input type="hidden" name = "modalKeywords" />
						  </div>
						</div>
						<div class="innerMAll blog-add-link">
							<div>
									<input type="text" id="blog-img-path" class="form-control blog-card checkEmpty" placeholder="Add Images : " readonly/>
							</div>
							<div class="fileUpload blog-form-icon">
								<span><i class="fa fa-folder-open pull-left margin-right-none half innerMT innerMR" aria-hidden="true"></i></span>
								<span class="browse-span">Browse</span>
								<input type="file" class="upload" id="blog-image-upload" name="img_file_name" />
							</div>
						</div>
						<div class="innerMAll pull-left form-select-dropdown">
							<select class="selectpicker" name="postscope">
								<option value="Only Me">Only Me</option>
								<option value="Public">Public</option>
								<option value="Followers">Followers</option>
								<option value="Followers of Followers">Followers of Followers</option>
							</select>
						</div>
						<div class="clearfix">
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Preview" class="form-preview-btn"/>
							</div>
							<div class="innerAll clearfix pull-right">
								<input type="button" value="Post" class="form-post-btn" id="blogPost" />
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
