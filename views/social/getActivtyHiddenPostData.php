<?php


	header("Access-Control-Allow-Origin: *");
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');
	session_start();
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once('../../helpers/deviceHelper.php');
	require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";

	if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	   print("<script>");
	   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	   print("</script>");
	   die;
	}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


	//printArr($_POST);

	$hiddenPostTime    = cleanXSS(rawurldecode($_POST['lastHiddTime']));
	$hiddenEndingDate  = cleanXSS(rawurldecode($_POST['hiddLastDate']));
    $l                 = cleanXSS(rawurldecode($_POST['lValue']));
	$userId            = $_SESSION['id'];
	$hiddenAppendLimit = 11;
	$k                 = 0;
    $hiddenLastTime    = '';

	$hideAction        = "unhide";
    $hiddenPrevDate    = $hiddenEndingDate;

	if (empty($hiddenPostTime)) {
        $date           = uDateTime("d-m-Y H:i:s",date("d-m-Y H:i:s"));
		$LastHiddenTime = strtotime($date) * 1000 ;
	} else {
		$LastHiddenTime = $hiddenPostTime;
	}
	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $blockUserArray          = getBlockedUser($targetDirAccountHandler);
	$activityHiddenData      = getActivityCommentHiddenPostData($userId, $LastHiddenTime,'hidden_post', json_encode($blockUserArray));
	//printArr($activityHiddenData['errMsg']);

	if (noError($activityHiddenData)) {
		$activityHiddenData  = $activityHiddenData['errMsg'];
?>

		<div class="row">
			<div class="col-lg-12">
            <?php
            if(isset($activityHiddenData['errCode'])) {
                if ($activityHiddenData['errCode'] == 4) {
                    echo '<ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>"Posts hidden by you will be shown here."</p></b></center></li></ul>';

                }
            }
			foreach ($activityHiddenData as $date => $hiddenData) {
				if ($k < $hiddenAppendLimit-1) {
                    $todayDate  = uDateTime("m/d/y",date("m/d/y"));
				if ($todayDate == $date) {
					$displayDate = 'Today';
				} else {
                    $yesterdayDate  = uDateTime("m/d/y",date("m/d/Y", strtotime("yesterday")));
					if ($yesterdayDate == $date) {
						$displayDate = 'Yesterday';
					} else {
                        $displayDate = uDateTime("jS F Y",strtotime($date));
					}
				}			
				if($hiddenEndingDate != $date) {
		    ?>
            <div class="h4  innerL ">
                <?php echo $displayDate; ?>
            </div>
            <?php
                }
            ?>
                    <ul class="list-unstyled activity-log__list margin-none">
		    <?php
            //printArr($hiddenData);
				foreach ($hiddenData as $key => $value) {

					if ($k < $hiddenAppendLimit-1) {
                        $k              = $k + 1;
                        $hiddenLastTime = $value['hide_at'];

					switch ($value['post_type']) {
						case 'blog':
                            $imagePath = '<i class="fa fa-file-text media-object--activity-log media-object--activity-log--icon bg-grayscale-80 text-white text-center innerT f-sz16 "></i>';
							$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_details"]["blog_title"]);
			                $postDescCount = strlen($postDesc);
			                if ($postDescCount >= 50) {
			                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
			                } else {
			                  $postContent = $postDesc;
			                }
							break;
						case 'video':
							$imagePath = '<i class="fa fa-play-circle media-object--activity-log media-object--activity-log--icon bg-youtube text-white text-center f-sz16 "></i>';
							$videoTitle = getYoutubeTitleFromUrl($value['post_details']['asset_url']);
							if ($videoTitle != '') {
								$postDesc = $videoTitle;
							} else if (!empty($value['post_short_desc'])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
							} else {
								$postDesc = 'Title Unavailable';
							}
							$postDescCount = strlen($postDesc);
							if ($postDescCount >= 50) {
								$postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
							} else {
								$postContent = $postDesc;
							}
							break;
						case 'audio':
							$imagePath = '<i class="fa fa-soundcloud media-object--activity-log media-object--activity-log--icon bg-sound-cloud text-white text-center f-sz16 "></i>';
							if (!empty($value["post_short_desc"])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
			            	} else {
			            		$postContent = 'Title Unavailable';
			            	}						      
							break;
						case 'image':
                                $imagePath = '<i class="fa fa-photo media-object--activity-log media-object--activity-log--icon bg-Light-Blue text-white text-center innerT f-sz16"></i>';
							if (!empty($value["post_short_desc"])) {
								$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
				                $postDescCount = strlen($postDesc);
				                if ($postDescCount >= 50) {
				                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
				                } else {
				                  $postContent = $postDesc;
				                }
			            	} else {
			            		$postContent = 'Title Unavailable';
			            	}
							break;
						case 'status':
							$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
							$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
			                $postDescCount = strlen($postDesc);
			                if ($postDescCount >= 50) {
			                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
			                } else {
			                  $postContent = $postDesc;
			                }
			                
							break;
						case 'share':

							$imagePath = '<i class="fa fa-share-square-o media-object--activity-log media-object--activity-log--icon text-white text-center f-sz16 " style = "background-color: #5cb85c"></i>';
							$postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
			                $postDescCount = strlen($postDesc);
			                if ($postDescCount >= 50) {
			                  $postContent = mb_substr($postDesc,0, 50,"utf-8").'...';
			                } else {
			                  $postContent = $postDesc;
			                }

							$bookPostId        = $value['_id'];
			                $bookPostType      = 'share';
			                $bookPostcreatedAt = $value['created_at'];
			                $bookPostEmail     = $value['user_ref']['email'];
							break;							
						default:
							$imagePath = '<i class="fa  fa-edit media-object--activity-log media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>';
							$postContent = 'Title Unavailable';
							break;
					}// End Of switch Case
		        ?>
					<li id="post_<?php echo $value['_id'] ?>" class="card innerAll border-bottom ">
						<div class="media">
							<div class="media-left">
                                <?php echo $imagePath; ?>
							</div>
							<div class="media-body f-sz15">
								<div class="row">
									<div class="col-xs-11">
										<div class="row">
											<div class="col-xs-6">
												<p class="ellipses innerTB half margin-none width-250">
                                                    <?php echo $postContent; ?>
												</p>
											</div>
											<div class="col-xs-6">
													<p class="ellipses innerTB half margin-none">
													You have Hidden 
													<span class="text-color-Text-Primary-Blue ">
														<a <?php if($value['user_ref']['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($value['user_ref']['email']); ?>&type=<?php echo 'all'; ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> target = "_blank">
															<?php echo $value['user_ref']['first_name'] . ' ' . $value['user_ref']['last_name']; ?>
														</a>
													</span> 
													post
												</p>
											</div>
										</div>
										<div class="row">
										</div>
									</div>
									<div class="col-xs-1">
                                        <?php
                                            $postType = $value["post_type"];
                                            if ($value['post_type'] == "share") {
                                                $postType = $value["post_details"]["parent_post"]["post_id"]["post_type"];
                                            }
                                        ?>
										<a class="btn btn-link" href="#" role="button" onclick="hidePost('<?php echo $value['_id']; ?>','<?php echo $postType; ?>','<?php echo $value["created_at"]; ?>', '','<?php echo  $value["user_ref"]["email"]; ?>','<?php echo  $hideAction; ?>');"> <i class="fa fa-eye text-color-Text-Primary-Blue  f-sz18 pull-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</li>
		    <?php
				} //End of inner if loop to check no of printed data
				} //End of inner foreach
		    ?>
				    </ul>
        <?php
			} // End of outer if loop to check no of printed data
                $hiddenPrevDate = $date;
		} // End of Outer Foreach
		?>
			</div>
		</div>

<?php
	} else {
		echo "<div><center>Please Try After Some Time</center></div>";
	} // End of no error if

$hiddenDataEnd = '';
if ($hiddenLastTime == '') {
    $hiddenLastTime = $hiddenPostTime;
}
if ($l + $k >= $hiddenAppendLimit) {
	    $hiddenDataEnd = 'true';
}
?>

<div id = "hidden-frontendArray-<?php echo $hiddenPostTime; ?>" hidden-start = "<?php echo $hiddenLastTime;?>" hiddenDataEnd = "<?php echo $hiddenDataEnd; ?>" hidden-prev-date = '<?php echo $hiddenPrevDate; ?>' l-value = '<?php echo $l + $k; ?>' style = "display:none;"></div>