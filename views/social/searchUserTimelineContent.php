<!-- for getting post -->
<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once('../../IPBlocker/ipblocker.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


$userId             = $_SESSION["id"];
// making curl request
$searchText         = cleanXSS(rawurldecode($_POST['searchUserName']));
$dataCount          = cleanXSS(rawurldecode($_POST['dataCount']));
$lastDataCreateTime = cleanXSS(rawurldecode($_POST['lastDataCreateTime']));
$searchUserData     = getSearchUserResult($searchText, $userId, $lastDataCreateTime);
// printArr($searchUserData);die;
if ($searchUserData["errCode"] == -1) {
    $searchUserData = $searchUserData["errMsg"];
} else { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">No Search Found</h4>
    </div>
<?php exit; }
$dataEmptyFlag      = "false";
$bulkData           = array();

//Get following array
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$getFollowingArray = getFollowUser($targetDirAccountHandler);

//for not allowing blocked user in UI
$getBlockedArray   = getBlockedUser($targetDirAccountHandler);

foreach ($getBlockedArray as $blockedUser) {
  foreach ($searchUserData  as $key => $getSearchDetails) {
      $searchedId = $getSearchDetails['user_id'];
      if ($searchedId == $blockedUser) {
        unset($searchUserData[$key]);
      }
  }
}
$searchUserData = array_values($searchUserData);

//for printing search user data
foreach ($searchUserData as $key => $data) {
//printArr($data);
    if(isset($data["profile_pic"]) && !empty($data["profile_pic"])){
        global $cdnSocialUrl;
        global $rootUrlImages;

        $extensionUP  = pathinfo($data["profile_pic"], PATHINFO_EXTENSION);
        //CDN image path
        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $data["account_handle"] . '/profile/' . $data["account_handle"] . '_' . $data["profile_pic"] . '.40x40.' . $extensionUP;

        //server image path
        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$data["account_handle"].'/profile/'.$data["account_handle"].'_'.$data["profile_pic"];

        // check for image is available on CDN
        $file = $imageFileOfCDNUP;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $imgSrc = $imageFileOfLocalUP;
        } else {
            $imgSrc = $imageFileOfCDNUP;
        }
    }else{
          $imgSrc = $rootUrlImages."default_profile.jpg";
    } 

    //printArr($data);
    //echo $data['created_at'];
    if ($key < 10) {
        $lastDataTime = $data['creationTime'];

        ?>
        <div class="follower-container innerT">
            <ul class="list-unstyled social-card">
                <li class="innerTB clearfix">
                    <div class="col-xs-5 ellipses ">
                        <img class="img-circle" src="<?php echo  $imgSrc; ?>">
                        <a class="follower-name innerL half" <?php if($data['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($data['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?>   title="<?php echo ucwords($data['first_name']) ." ". ucwords($data['last_name']);?>"><?php echo ucwords($data['first_name']) ." ". ucwords($data['last_name']);?> </a>
                    </div>
                    <div class="col-xs-3">
                        <a class="follower-id" <?php if($data['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($data['email']); ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> ><?php echo '@'.$data['account_handle'];?></a>
                    </div>
                    <?php
                    $followFlag = false;
                    $setButtonFlag = false;
                    if (in_array($data['user_id'],$getFollowingArray)) {
                        $followFlag = true;
                    }
                    ?>
                    <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <?php if($_SESSION['id']==$data['user_id']){

                        }else{ ?>
                            <input type = "button" id ='<?php echo $data["account_handle"]; ?>' class="btn-social<?php if($followFlag){ echo "-dark";} ?> pull-right follow-unfollow-button" onclick="ajaxAppFollowEvent('<?php echo $data["account_handle"]; ?>','<?php echo $data["email"]; ?>');" value = '<?php if($followFlag){echo "Unfollow";} else {echo "Follow";}?>'>
                        <?php } ?>
                    </div>

                    <?php }
                    ?>
            </ul>
        </div>
        <?php
} ?>
<div class="modal-header">
 <h4 id="search-error-msg" class="modal-title text-blue">
<?php if (count($searchUserData) < 11) {
    $dataEmptyFlag = "true"; ?>
   End of Search Result
<?php } ?>
</h4>
</div>

<div id='private-last-data-time-<?php echo $dataCount; ?>' lastDataTime = '<?php echo $lastDataCreateTime; ?>' data-empty='<?php echo $dataEmptyFlag; ?>' style="display:none;"></div>
<!-- foreach closing -->

<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip();
</script>
