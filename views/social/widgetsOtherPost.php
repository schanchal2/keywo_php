<!--====================================================
=            include : widgetsOtherPost.php            =
=====================================================-->
<?php 
  if (isset($_POST['userId']) && isset($_POST['postId']) && isset($_POST['postCreationTime']) && isset($_POST['postType']) && isset($_POST['keywords'])) {
    session_start();
    /* Add Required Files */
    require_once "../../config/config.php";
    require_once "../../config/db_config.php";
    require_once "../../helpers/errorMap.php";
    require_once "../../helpers/arrayHelper.php";
    require_once "../../helpers/deviceHelper.php";
    require_once "../../helpers/stringHelper.php";
    require_once "../../helpers/coreFunctions.php";
    require_once "../../helpers/imageFunctions.php";
    require_once "../../models/social/socialModel.php";
    require_once "../../models/social/commonFunction.php";
    require_once "../../models/user/authenticationModel.php";
    // printArr($_POST); die();

    // $popularLimit         = cleanXSS(urldecode($_POST['limit']));
    $popularUserId           = cleanXSS(urldecode($_POST['userId']));
    $popularPostId           = cleanXSS(urldecode($_POST['postId']));
    $popularPostType         = cleanXSS(urldecode($_POST['postType']));    
    $popularKeywords         = cleanXSS(urldecode($_POST['keywords']));    
    $popularPostSharedType   = cleanXSS(urldecode($_POST['postSharedType']));
    $popularPostCreationTime = cleanXSS(urldecode($_POST['postCreationTime']));
    $postAccHandler          = cleanXSS(urldecode($_POST['accountHandler']));    
    $popularKeywordsArray    = explode(" ",$popularKeywords);

  } else if (isset($postDetail['posted_by']) && isset($postDetail['post_type']) && isset($postDetail['created_at'])) {    
    $popularUserId           = $postDetail['posted_by'];    
    $popularPostId           = $postId;
    $popularPostType         = $postDetail['post_type'];
    $popularPostSharedType   = $postSharedParent["post_type"];
    $popularPostCreationTime = $postDetail["created_at"]; 
    $postAccHandler          = $postDetail['user_ref']['account_handle'];
    if($popularPostType      == 'share'){
      $popularKeywords       = $postSharedParent['keywords'];
    }else{
      $popularKeywords       = $postDetail["keywords"];
    } 
    $popularKeywordsArray    = $popularKeywords;
  }

global $cdnSocialUrl;
global $rootUrlImages;


if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   //session is not active, redirect to login page
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   exit();
   die();
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

  if ($popularPostType == 'image') {
    $popularLimit            = '9';
  } else {
    $popularLimit            = '3';
  }
 
  if ($popularPostType == 'share' && ($popularPostSharedType == 'video' || $popularPostSharedType == 'image' || $popularPostSharedType == 'blog' || $popularPostSharedType == 'audio' || $popularPostSharedType == 'status')) {
    $popularPostTypeValue = $popularPostSharedType;
  } else {
    $popularPostTypeValue = $popularPostType;
  }
 
  // to read Json File.
  if(isset($popularPostType) && !empty($popularPostType)){ 
    $targetDirOtherPost = "../../json_directory/social/followerwonk/". $postAccHandler ."/";
    $files2 = dirToArray($targetDirOtherPost);
    // printArr($files2); 
    foreach($files2 as $key => $files2Details)
    {
      $Final= $targetDirAccountHandler.$files2Details;
      $lastWordForFirst=substr($Final, -6);
      $firstWord = $lastWordForFirst[0];

      $sessionUserName = $_SESSION['account_handle'];
      $sessionUserName = $sessionUserName[0];
      if ($firstWord == $sessionUserName) {
        $userJsonFile = $files2Details ; 
        break;
      }
    }
    $popularKeywordsJson = json_encode($popularKeywordsArray);
    $getPopularPostWidget = getOtherPostWidget($popularUserId, $popularPostId, $popularPostCreationTime, $popularPostTypeValue, $popularKeywordsJson, $popularLimit, $_SESSION['id'], $userJsonFile);
    if (noError($getPopularPostWidget)) {
      $getPopularPostWidget = $getPopularPostWidget['errMsg'];
      // printArr($getPopularPostWidget);
      if (count($getPopularPostWidget) > 0 ) {
      
?>
<div class="popular-image-module">
  <div class="card social-card right-panel-modules inner-2x innerMTB">
    <div class="bg-light-gray right-panel-modules-head">
      <div class="row margin-none">
      <div class="col-xs-12 padding-right-none">
          <h4>Other <?php echo ucwords($popularPostTypeValue); if ($popularPostTypeValue != 'status') {echo 's';}?></h4>
        </div>
      </div>
    </div>
<?php
  //check for parent share post type if post is SAHRED  
  $postDesc = '';
  $postDescCount = '';
  $postDescText = '';
  switch ($popularPostTypeValue) {
    case 'video':
    // printArr($getPopularPostWidget);
    foreach ($getPopularPostWidget as $key => $value) {
?>
      <!--bg-light-gray-->
      <div class="row margin-none video-module border-bottom" >
        <div class="row margin-none innerT" onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
          <div class="col-xs-4">
          <a >

              <?php
              if(isset($value["post_details"]["video_thumbnail"]) && !empty($value["post_details"]["video_thumbnail"])){
                  $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $value["user_ref"]["account_handle"] . '/post/video/' . $value["post_details"]["video_thumbnail"];

                  //server image path
                  $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$value["user_ref"]["account_handle"].'/post/video/'.$value["post_details"]["video_thumbnail"];

                  // check for image is available on CDN
                  $file = $imageFileOfCDNUP;
                  $file_headers = @get_headers($file);
                  if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                      $imgSrc = $imageFileOfLocalUP;
                  } else {
                      $imgSrc = $imageFileOfCDNUP;
                  }
              }
              ?>
            <img class="popular_blog_img" src="<?php echo $imgSrc; ?>">
            </a>
          </div>
          <div class="col-xs-8">
            <a >
              <span class="ellipsis-multiline text-black l-h15">
                <?php
                  $videoTitle = $value['post_details']['video_desc'];
                  if (empty($value['post_short_desc'])  && !(empty($videoTitle))) {
                    $postDesc = $videoTitle;
                  } else if (!empty($value['post_short_desc'])) {
                    $postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
                  } else {                    
                    $postDesc = 'Title Unavailable';
                  }
                  $postDescCount = strlen( $postDesc);
                  if ($postDescCount >= 50) {
                    $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                  } else {
                    $postDescText = $postDesc;
                  }
                  echo rawurldecode($postDescText);
                ?>
              </span>
            </a>
          </div>
        </div>
        <div class="row margin-none half innerB">
          <div class="col-xs-8 text-black">
            <label>Earning : </label>
            <span>
                 <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                       origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
            </span>
          </div>
          <div class="col-xs-4" onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
            <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
          </div>
        </div>
      </div>
      <?php }?>
      <!--row-->
      <div class="row margin-none half innerTB">        
<?php
      break;
    case 'audio':
    // printArr($getPopularPostWidget);
    foreach ($getPopularPostWidget as $key => $value) {
      $ext         = explode(".",$value['post_details']['img_file_name']);
?>
      <!--bg-light-gray-->
      <div class="row margin-none blog-module border-bottom">
        <div class="row margin-none sound-module"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
          <div class="row margin-none innerT">
            <div class="col-xs-4">
              <img class="popular_blog_img" src="<?php echo $rootUrl; ?>/images/social/sound-trending.png">
            </div>
            <div class="col-xs-8">
              <a class="ellipsis-multiline  word-wrap  l-h15 text-black">
                <?php 
                  $postDesc = preg_replace('/<\/?a[^>]*>/','',$value["post_short_desc"]);
                  $postDescCount = strlen( $postDesc);
                  if ($postDescCount >= 50) {
                    $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                  } else {
                    $postDescText = $postDesc;
                  }
                  echo rawurldecode($postDescText);
                ?>
              </a>
              <span class="" style="display:block;">
                <!-- By Doors Down -->
              </span>
            </div>
            <div class="col-xs-3">
              <span class="text-gray">
                <!-- <span>3.45 mins</span> -->
              </span>
            </div>
          </div>
          <div class="row margin-none innerT">
            <div class="col-xs-8 text-black">
              <label>Earning : </label>
              <span>
                   <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                         origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
              </span>
            </div>
            <div class="col-xs-4"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
              <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
            </div>
          </div>
          
        </div>
      </div>
      <?php } ?>
      
      <div class="row margin-none half innerTB">        
<?php
      break;
    case 'blog':
    foreach ($getPopularPostWidget as $key => $value) {
      $ext         = explode(".",$value['post_details']['img_file_name']);
      if (!empty($value["post_details"]["img_file_name"])) {
        // $ext         = explode(".",$getPopularPostWidget[$j]['post_details']['img_file_name']);
        $extension = pathinfo($getPopularPostWidget["post_details"]["img_file_name"], PATHINFO_EXTENSION);

        //CDN image path
        $imageFileOfCDN = $cdnSocialUrl . 'users/' . $getPopularPostWidget["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getPopularPostWidget["_id"] . '_' . $getPopularPostWidget["created_at"] . '_' . $getPopularPostWidget["post_details"]["img_file_name"];

        //server image path
        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $getPopularPostWidget["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getPopularPostWidget["_id"] . '_' . $getPopularPostWidget["created_at"] . '_' . $getPopularPostWidget["post_details"]["img_file_name"];

        // check for image is avilable on CDN
        $file = $imageFileOfCDN;
        $file_headers = @get_headers($file);
        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
          $finalImagePath = $imageFileOfLocal;
        }
        else {
          $finalImagePath = $imageFileOfCDN;
        }
      } else {
        $finalImagePath = $rootUrlImages .'social/popular_blog.png';
      }
?>
      <div class="row margin-none blog-module border-bottom">
        <div class="row margin-none innerT"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
          <div class="col-xs-4">
          <a>
            <img class="popular_blog_img" src="<?php echo $finalImagePath;  ?>" />
            </a>
          </div>
          <div class="col-xs-8">
          <a>
            <span class="ellipsis-multiline">
              <?php 
                $postDesc = $value["post_details"]["blog_title"];
                $postDescCount = strlen( $postDesc);
                if ($postDescCount >= 50) {
                  $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                } else {
                  $postDescText = $postDesc;
                }
                echo rawurldecode($postDescText);
              ?>
            </span>
          </a>
          </div>
        </div>
        <div class="row margin-none innerT">
          <div class="col-xs-8">
            <label>Earning : </label>
            <span>
                <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                      origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
            </span>
          </div>
          <div class="col-xs-4"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
            <a class="text-social-primary pull-right"><?php echo $value["post_views_count"] . ' Views';?></a>
          </div>
        </div>
      </div>
      <?php } ?>
      <div class="row margin-none half innerTB">
<?php
      break;
    case 'image':
    for ($i = 0; $i < count($getPopularPostWidget); $i++) {
?>
      <!--bg-light-gray-->
      <div class="row margin-none blog-module border-bottom" >
        <div class="row margin-none innerT">
        <?php for ($j = $i; $j <= ($i+2) && $j < count($getPopularPostWidget) ; $j++) { //printArr($getPopularPostWidget[$j]);?>

          <div class="col-xs-4" onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($getPopularPostWidget[$j]['_id']) . '&timestamp=' . $getPopularPostWidget[$j]['created_at'] . '&posttype=' . base64_encode($getPopularPostWidget[$j]['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
            <a>
            <?php 
              // $ext         = explode(".",$getPopularPostWidget[$j]['post_details']['img_file_name']);
              $extension = pathinfo($getPopularPostWidget[$j]["post_details"]["img_file_name"], PATHINFO_EXTENSION);

              //CDN image path
              $imageFileOfCDN = $cdnSocialUrl . 'users/' . $getPopularPostWidget[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getPopularPostWidget[$j]["_id"] . '_' . $getPopularPostWidget[$j]["created_at"] . '_' . $getPopularPostWidget[$j]["post_details"]["img_file_name"];

              //server image path
              $imageFileOfLocal = $rootUrl . 'images/social/users/' . $getPopularPostWidget[$j]["user_ref"]["account_handle"] . '/post/images/' . $extension . '/' . $getPopularPostWidget[$j]["_id"] . '_' . $getPopularPostWidget[$j]["created_at"] . '_' . $getPopularPostWidget[$j]["post_details"]["img_file_name"];

              // check for image is avilable on CDN
              $file = $imageFileOfCDN;
              $file_headers = @get_headers($file);
              if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $finalImagePath = $imageFileOfLocal;
              }
              else {
                $finalImagePath = $imageFileOfCDN;
              }

            ?>
            <img class="popular_blog_img img-responsive" src="<?php echo $finalImagePath; //echo $cdnSocialUrl . "users/" . $getPopularPostWidget[$j]["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $getPopularPostWidget[$j]["_id"] . '_' . $getPopularPostWidget[$j]["created_at"] . '_' . $getPopularPostWidget[$j]["post_details"]["img_file_name"] ; ?>">
            <div class="half innerT">

            
              <span>
              <?php echo "<span class='text-social-primary'>" . $getPopularPostWidget[$j]["post_views_count"] . "</span><span class='text-social-primary'> Views</span>";?>
              </span>
            </div>
            </a>
          </div>
        <?php 
              }$i = $j-1;
        ?>
        </div>
      </div>
    <?php } ?>      
      <!--row-->
      <div class="row margin-none half innerTB border-bottom">
        
<?php
        break;
      case 'status':
        foreach ($getPopularPostWidget as $key => $value) {
          //printArr($value);
?>
      
        <div class="row margin-none blog-module border-bottom">
          <div class="row margin-none innerT"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
            <div class="col-xs-4">
                <i class="fa  fa-edit widget-img-ico  media-object--activity-log--icon bg-Text-Primary-Blue text-white text-center f-sz16 "></i>
            </div>
            <div class="col-xs-8">
            <a href = "<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']);?>" target="_blank">
              <span class="display-in-block ellipses width-100-percent">
                <?php 
                  $postDesc = preg_replace('/<\/?a[^>]*>/','',rawurldecode($value["post_short_desc"]));
                  $postDescCount = strlen( $postDesc);
                  if ($postDescCount >= 50) {
                    $postDescText = mb_substr($postDesc,0, 50,"utf-8").'...';
                  } else {
                    $postDescText = $postDesc;
                  }
                  echo $postDescText;
                ?>
              </span>
            </a>
            </div>
          </div>
          <div class="row margin-none">
            <div class="row margin-none innerT border-bottom">
              <div class="col-xs-8">
                <label>Earning : </label>
                <span>
                     <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                           origPrice="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$value['post_earnings']}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$value['post_earnings']}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                </span>
              </div>
              <div class="col-xs-4"  onclick = 'loadOtherPostDetailPage("<?php echo $rootUrl . 'views/social/socialPostDetails.php?post_id=' . base64_encode($value['_id']) . '&timestamp=' . $value['created_at'] . '&posttype=' . base64_encode($value['post_type']) . '&email=' . base64_encode($value['user_ref']['email']);?>");'>
                <a class="text-social-primary pull-right"><?php echo $value['post_views_count'] . ' Views'; ?></a>
              </div>
            </div>
          </div>
        </div>
<?php 
        }
?>
        <div class="row margin-none half innerTB">          
<?php
    }//End Of Switch case    
    $popularkeyw = implode(" ",$popularKeywordsArray);  
?>
          <div class="col-xs-12">            
            <a id = "other-post-refresh-btn" class="text-deep-sky-blue pull-right" onclick = "reloadOtherPosts('<?php echo $popularUserId; ?>', '<?php echo $popularPostId; ?>', '<?php echo $popularPostCreationTime; ?>', '<?php echo $popularPostType; ?>', '<?php echo $popularPostSharedType; ?>', '<?php echo $popularkeyw; ?>', '<?php echo $postAccHandler; ?>');"><i class="innerL fa fa-refresh"></i></a>


          <?php if (count($getPopularPostWidget) >= 3) { ?>
            <a <?php if($getPopularPostWidget[0]['posted_by'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($getPopularPostWidget[0]['user_ref']['email']); ?>&type=<?php echo $getPopularPostWidget[0]['post_type']; ?>" <?php } else { ?> href= "<?php echo $rootUrl; ?>" <?php } ?> class="text-deep-sky-blue pull-right">View all</a>
          <?php } ?>
          </div>
        </div>
      </div>
    </div>
<?php
    }// End Of no error
  }//End Of post Type if condition  
} // End of count check
 ?>
<!--====  End of include : widgetsOtherPost.php  ==== -->
          <script>
              //tooltip
              $('[data-toggle="tooltip"]').tooltip({
                  trigger:'hover'
              });
          </script>