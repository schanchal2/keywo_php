<?php
/**
 **********************************************************************************
 *                  setPostController.php
 * ********************************************************************************
 *      This controller is used for Setting Post Details to social
 */

/* Add Seesion Management Files */
header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();

require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";

//session is not active, redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

          $type                    = cleanXSS(trim(urldecode($_POST["type"])));
          $jsonFileNo              = cleanXSS(trim(urldecode($_POST["jsonFileNo"])));
          $jsonFileInnerNo         = cleanXSS(trim(urldecode($_POST["jsonFileInnerNo"])));
          $divAppendId             = cleanXSS(trim(urldecode($_POST["divAppendId"])));
          $remaining               = '';
          $pageScrollEnd           = 'true';
          $Final                   = array();
          $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION['account_handle']."/";
          $targetDirAccountHandlerOfBlockList = $_SESSION['account_handle'].'_info_blockList.json';

          $filesOfBlocked = dirToArray($targetDirAccountHandler);
          foreach($filesOfBlocked as $key=>$files){
            if ($files == $targetDirAccountHandlerOfBlockList)
            {
                $set = array_splice($filesOfBlocked, $key, 1);
                $flag = 0;
            }
          }
          //print_r($filesOfBlocked);
          foreach($filesOfBlocked as $key => $filesOfBlockedDetails)
          {
            $Final= $targetDirAccountHandler.$filesOfBlockedDetails;
            $jsondata = file_get_contents($Final);
            $data = json_decode($jsondata, true);
            if(isset($data[$type])){
                $a = count($data[$type]);
            }else{
                $a = 0;
            }

            if($a>0)
            {
              break;
            };
          }
          $lastWordForFirst=substr($Final, -6);
          $firstWord = $lastWordForFirst[0];

          $dataCount = "";
          $userIdsOfBlocked = array();
          $useerIdCount = 0;
          $setter = array();
          $endFlag = true;
          $flag = true;
          if (empty($jsonFileNo)) {
            $i = 0;
          } else {
            $i = $jsonFileNo;
          }

    for ($i; $i < count($filesOfBlocked) && $endFlag; $i++ ) {
      $Final12= $targetDirAccountHandler.$filesOfBlocked[$i];
      $jsondata12 = file_get_contents($Final12);
      $dataOfBlocked = json_decode($jsondata12, true);
        if(isset($dataOfBlocked[$type])){
            $dataCount = count($dataOfBlocked[$type]);
        }else{
            $dataCount = 0;
        }
      $lastWord=substr($Final12, -6);
      $userIdsOfBlocked = array();
      if(isset($dataOfBlocked[$type])){
          $dataIds = $dataOfBlocked[$type];
      }else{
          $dataIds = array();
      }

      $appended =  0;
      if (!empty($jsonFileInnerNo) && !empty($jsonFileNo) && $i == $jsonFileNo) {
        $k = $jsonFileInnerNo;
      } else {
        $k = 0;
      }
      for($k; $k < count($dataIds) && $flag; $k++) {
        if ( $useerIdCount < 10) {
          $useerIdCount = $useerIdCount+ count($dataIds[$k]);
          $userIdsOfBlocked[] = $dataIds[$k];
          $appended = $appended+1;
        } else {
          $flag = false;
          $endFlag = false;
          $lastWordJson = $filesOfBlocked[$i];
          $remaining = $appended;
          $jsonFileNo = $i;
        }
      }
      $word = $lastWord[0];
      if ($dataCount > 0){
        $geter = array( "count" => $dataCount, "user_id" => $userIdsOfBlocked);
        $setter[$word]= $geter;
      }
    }
    if ($i == count($filesOfBlocked) && $k == $dataCount) {
      $pageScrollEnd = 'false';
    }
    $jsonCOnverts = json_encode($setter);
    $user_required_info12 = 'first_name,last_name,account_handle,profile_pic,user_id,email';
    $userDetailedInformation12 = getUserInfoForMultiUser($jsonCOnverts,$walletURLIPnotification.'api/notify/v2/',$user_required_info12);
    $userDetailedInformation12 = $userDetailedInformation12['errMsg'];

    if (empty($userDetailedInformation12) && $type == "blocked") { ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">You have not blocked any users.</h4>
    </div>
<?php }
  $s= 0;
    foreach($userDetailedInformation12 as $key => $filesDetails) {
      if (count($filesDetails) > 0 ) {
        if ($s == 0) {
          if (empty($jsonFileInnerNo)) {

  ?>
     <div class="h4">  <?php if(!empty($divAppendId)){ echo strtoupper($firstWord);} ?></div>
     <?php }  } else { ?>
      <div class="h4"><?php echo strtoupper($key);?></div>
      <?php      }  }  ?>
        <ul class="list-unstyled social-card margin-none">
      <?php  
        foreach($filesDetails['user_info'] as $key1 => $filesOfBlockedDetails) {  
          if(isset($filesOfBlockedDetails["profile_pic"]) && !empty($filesOfBlockedDetails["profile_pic"])){
              global $cdnSocialUrl;
              global $rootUrlImages;

              $extensionUP  = pathinfo($filesOfBlockedDetails["profile_pic"], PATHINFO_EXTENSION);
              //CDN image path
              $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $filesOfBlockedDetails["account_handle"] . '/profile/' . $filesOfBlockedDetails["account_handle"] . '_' . $filesOfBlockedDetails["profile_pic"] . '.40x40.' . $extensionUP;

              //server image path
              $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$filesOfBlockedDetails["account_handle"].'/profile/'.$filesOfBlockedDetails["account_handle"].'_'.$filesOfBlockedDetails["profile_pic"];

              // check for image is available on CDN
              $file = $imageFileOfCDNUP;
              $file_headers = @get_headers($file);
              if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                  $imgSrc = $imageFileOfLocalUP;
              } else {
                  $imgSrc = $imageFileOfCDNUP;
              }
            }else{
                $imgSrc = $rootUrlImages."default_profile.jpg";
            }
      ?>

          <li class="innerTB clearfix">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <img class="img-circle" src="<?php echo $imgSrc; ?>">
              <a class="follower-name" href="#"><?php echo ucwords($filesOfBlockedDetails['first_name']) ." ". ucwords($filesOfBlockedDetails['last_name']);?> </a>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
              <a class="follower-id" href="#"><?php echo '@'.$filesOfBlockedDetails['account_handle'];?></a>
            </div>
            <?php
          $followFlag = false;
          if ($type == 'blocked') {
            $getFollowingDetails = $targetDirAccountHandler.$_SESSION['account_handle']."_info_" .$key.".json";
            if(file_exists($getFollowingDetails)) {
              $jsonDataOFBlocked = file_get_contents($getFollowingDetails);
              $dataBlocked = json_decode($jsonDataOFBlocked, true);
              if (in_array($filesOfBlockedDetails['user_id'], $dataBlocked["blocked"])) {
                $followFlag = true;
              }
            }
          } else {
             $followFlag = true;
          }
        ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
          <input type = "button" id ='<?php echo $filesOfBlockedDetails["account_handle"]; ?>' class="btn-social<?php if($followFlag){echo "-dark";} ?> pull-right follow-unfollow-button" onclick="ajaxAppBlockedEvent('<?php echo $filesOfBlockedDetails["account_handle"]; ?>','<?php echo $filesOfBlockedDetails["email"];?>');" value = '<?php if($followFlag){echo "Unblock";} else {echo "Block";}?>'>
        </div>
        <?php }  ?>
    </ul>
  <?php      $s = $s+1;    }  ?>
  <div id="followingAppend<?php echo $divAppendId; ?>" jsonFileNo = "<?php echo $jsonFileNo; ?>" fileReadNo = "<?php echo $remaining; ?>" pageScrollEnd = "<?php echo $pageScrollEnd; ?>"></div>