<?php


	header("Access-Control-Allow-Origin: *");
	ini_set('default_charset','utf-8');
	header('Content-type: text/html; charset=utf-8');
	session_start();
	require_once "../../config/config.php";
	require_once "../../config/db_config.php";
	require_once "../../models/user/authenticationModel.php";
	require_once "../../models/analytics/userRegistration_analytics.php";
	require_once '../../helpers/deviceHelper.php';
	require_once '../../backend_libraries/xmlProcessor/xmlProcessor.php';
	require_once "../../helpers/arrayHelper.php";
	require_once "../../helpers/stringHelper.php";
	require_once "../../helpers/errorMap.php";
	require_once "../../helpers/coreFunctions.php";
	require_once "../../models/social/socialModel.php";
	require_once "../../models/social/commonFunction.php";
	require_once "../../models/keywords/keywordCdpModel.php";

	if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	   print("<script>");
	   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
	   print("</script>");
	   die;
	}
//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}



	$jsonFileNo        = cleanXSS(rawurldecode($_POST['jsonFileNo']));
	$jsonFileInnerNo   = cleanXSS(rawurldecode($_POST['jsonFileInnerNo']));
	$lastFollowKeyword = cleanXSS(rawurldecode($_POST['lastKeyword']));
	$type              = 'followedkeyword';
	$accountHandle     = $_SESSION['account_handle'];
	$breakData         = 0;
	$dataPrintingLimit = 8;
	$dataCount         = 0;
	$userIds12         = "";
	$useerIdCount      = 0;
	$setter            = array();
	$endFlag           = true;
	$returnJsonFileNo = '';
	$newBreakingData = 0;
	$newDataExistCount = cleanXSS(rawurldecode($_POST['DataExistLoopCount']));;

	$conn              = createDBConnection('dbkeywords');
	if(noError($conn)){
	    $conn = $conn["connection"];
	}else{
	    print_r("Database Error");
	}
	



	$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$accountHandle."/";

      $fileOfFollowings = dirToArray($targetDirAccountHandler);
      
      // $flag = true;
      if (empty($jsonFileNo)) {
        $i = 0;
      } else {
        $i = $jsonFileNo;
      }
      $returnJsonFileNo = $i;
      // printArr($fileOfFollowings);
?>
	<div class="col-xs-12 followed-keywords-container padding-none">
<?php
    for ($i; $i < count($fileOfFollowings) && $endFlag; $i++ ) {
		$Final12= $targetDirAccountHandler.$fileOfFollowings[$i];
		$jsonOfFollowings12 = file_get_contents($Final12);
		$data12 = json_decode($jsonOfFollowings12, true);
		if (isset($data12[$type])) {
            $dataCount = count($data12[$type]);
            $lastWord=substr($Final12, -6);
            $userIds12 = "";
            $dataIds = $data12[$type];
            // printArr($dataIds);
            $appended =  0;
        }


		if ($dataCount > 0 && $lastWord[0] != '_') {
			if ($lastWord[0] != $lastFollowKeyword){
?>
		<div class="innerMB followed-time innerML">
			<?php echo ucwords($lastWord[0]);?>
		</div>
	<?php 
			}
	?>
		<div class="">
		
<?php
			if (!empty($jsonFileInnerNo) && !empty($jsonFileNo) && $i == $jsonFileNo) {
				$k = $jsonFileInnerNo;
			} else {
				$k = 0;
			}

	      	for($k; $k < count($dataIds) && $newBreakingData < 10 ; $k++) {	
	      		$newBreakingData = $newBreakingData + 1;
	      		$breakData = $breakData + 1;
	      		if ($breakData >= $dataPrintingLimit) {
	      			$endFlag = false;
	      		}
	      		if (!empty($dataIds[$k]['keyword'])) { 
	      			$kwdRevenueDetails = getRevenueDetailsByKeyword($conn,$dataIds[$k]['keyword']);
                    if(noError($kwdRevenueDetails)) {
                        $kwdRevenueTableName = $kwdRevenueDetails["table_name"];
                        $kwdRevenueDetails = $kwdRevenueDetails["data"][0];
                        $interactionCount = $kwdRevenueDetails["app_kwd_search_count"];
                        $userKwdSearchCount = $kwdRevenueDetails["app_kwd_ownership_earnings"];

                        $interactionCount = json_decode($interactionCount, true); //printArr($interactionCount);
                        $keywordInteractioncount = $interactionCount["total"];

                        if($keywordInteractioncount == '')
                        {
                            $keywordInteractioncount = 0;
                        }

                        if($userKwdSearchCount == '')
                        {
                            $userKwdSearchCount = 0;
                        }
				?>		<ul class="card social-card followed-list all-box-shadow padding-none clearfix" style="list-style:none;">
							<li class="clearfix border-bottom">
								<div class="col-xs-12 innerAll">
									<div class="col-xs-1 padding-left-none half innerT">
										<img src="<?php echo $rootUrlImages?>keyword_search_icon.png" class="img-responsive"/>
									</div>
									<div class="col-xs-3 text-blue innerMT padding-right-none">
										<div class="followed-keyword-name"><?php echo '#'.ucwords($dataIds[$k]['keyword']); ?></div>
									</div>
									<div class="col-xs-3 innerMT">
										<div class="comn-lft-rght-cont">
											<label>Earning :&nbsp</label>
				                                            <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                  origPrice="<?php echo number_format("{$userKwdSearchCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$userKwdSearchCount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$userKwdSearchCount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                                            
                                        </div>
									</div>
									<div class="col-xs-3 innerMT">
										<div class="comn-lft-rght-cont">
											<label>Interaction :&nbsp</label>
											<span class="text-black"><?php echo $keywordInteractioncount; ?></span>
										</div>
									</div>
									<div class="col-xs-2 innerMT">
										<input type="button" id="km_followButton_<?php echo cleanXSS($dataIds[$k]['keyword']); ?>" class="btn-social-wid-auto-dark pull-right activity-follow-button-js-<?php echo cleanXSS($dataIds[$k]['keyword']); ?>" value="Unfollow" onclick = "followUnfollow('<?php echo cleanXSS($dataIds[$k]['keyword']); ?>', 'km')"/>
									</div>
								</div>
							</li>
						</ul>
	<?php
						$newDataExistCount = $newDataExistCount + 1;
					} // end of noError of $kwdRevenueDetails 
				} // end of dataIds if condition
			} // end of inner for loop



			$word = $lastWord[0];

		}// end of dataCount if condition
		if ($endFlag) {
    		$returnJsonFileNo = $returnJsonFileNo + 1;
    	}

	} // end of outer for loop

	$pageScrollEnd = '';
    $k = '';
	if ($i == count($fileOfFollowings) && $k == $dataCount) {
	$pageScrollEnd = 'true';
	}
?>
		
	</div>
</div>

<div id = 'keyword-follow-frontendArray-<?php echo $lastFollowKeyword; ?>' keyword-follow-started-json = '<?php echo $returnJsonFileNo; ?>'  keyfollow-start = '<?php echo $k;?>' keywoFollowDataEnd = '<?php echo $pageScrollEnd; ?>' keyword-follow-prev-keyword = '<?php echo $word; ?>' DataExistCount = "<?php echo $newDataExistCount; ?>" style = 'display:none;'></div>


<?php

if ($newDataExistCount <= 0) {
	echo '<div class="col-xs-12 followed-keywords-container padding-none"><div class=""><ul class="list-unstyled activity-log__list margin-none"><li class="card innerAll border-bottom "><center><b><p>"See the list of keywords you follow over here."</p></b></center></li></ul></div></div>';
	die;
}


?>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>
