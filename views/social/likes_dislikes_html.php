<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>"
			type="text/css"/>
<main class="social-main-container inner-7x innerT" >
<div class="container">
	<div class="col-xs-3">
		<div class="social-left-panel left-panel-modules">
      <div class="like-unlike-user-profile-">
				<img class="img-responsive" src="<?php echo $rootUrlImages?>uploadingcontent.png"/>
				<div class="card social-card innerAll clearfix all-box-shadow name-accounthandle-card">
					<div class="col-xs-6 padding-none">
						<div class="col-xs-12 padding-none">
							Lorem
						</div>
						<div class="col-xs-12 padding-none">
							<span class="account-handle-name">@loremi</span>
						</div>
					</div>
					<div class="col-xs-6 padding-none innerMT">
						<input type="button" class="btn-social-wid-auto pull-right" value="Edit Profile"/>
					</div>
				</div>
      </div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Settings and Preferences</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Profile</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Change Password</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Notification Preferences</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Two Factor (2FA)</a>
						</div>
					</div>
				</div>
			</div>
			<div class="card left-panel-modules inner-2x innerMT">
				<div class="bg-light-gray left-panel-modules-head">
					<div class="row margin-none">
						<div class="col-xs-12">
								<h4>Activity Log</h4>
							</div>
						</div>
				</div>
				<div class="settings-preference-module">
					<div class="margin-none">
						<div class="social-user-setting-name border-bottom">
							<a>Likes / Dislikes</a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Comments <span>(2423)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Post Bookmarks <span>(2443)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed People <span>(2723)</span></a>
						</div>
						<div class="social-user-setting-name border-bottom">
							<a>Followed Keywords <span>(24)</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- social-left-panel  -->
	</div>
	<!-- col-xs-3 -->

	<div class="col-xs-9">
		<div class="col-xs-12 innerMB padding-none">
			<div class="pull-left">
				<h4 class="text-blue half innerMT">Activity Log :</h4>
			</div>
			<div class="pull-left half innerMT innerML">
				<div class="heading">Likes & Dislikes</div>
			</div>
			<div class="pull-right">
				<form class="clearfix margin-none">
					<div class="pull-left half innerMT search-box-like-dislike">
							<div class="input-group">
									<input type="text" id="search_box" name="q" value="<?php echo $keywords; ?>"
												 class="form-control" placeholder="Search" required>
									<input type="submit" class="social-search-btn-header" value=""/>
							</div>
					</div>
				</form>
			</div>
		</div>
		<div class="col-xs-12 like-dislike-container padding-none">
			<div class="innerMB like-dislike-time innerML">
				Today
			</div>
			<div class="">
				<ul class="card social-card like-dislike-list all-box-shadow padding-none clearfix" style="list-style:none;">
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<div class="innerMB like-dislike-time innerML">
				Yesterday
			</div>
			<div class="">
				<ul class="card social-card like-dislike-list all-box-shadow padding-none clearfix" style="list-style:none;">
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
					<li class="clearfix border-bottom">
						<div class="col-xs-12 innerAll">
							<div class="col-xs-6 padding-none">
								<div class="col-xs-2 padding-none">
									<img class="like-dislike-img img-responsive" src="<?php echo $rootUrlImages ?>uploadingcontent.png"/>
								</div>
								<div class="col-xs-10 innerMT post-earning-text">
									I made 700$ with my frst postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
							</div>
							<div class="col-xs-6 innerMT padding-right-none">
								<div class="col-xs-10 padding-none user-like-post">
									<span class="user-liked-post text-blue">Vishal gupta</span> likes <span class="user-posted-post text-blue">lorem ipsum's</span> postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's postVishal gupta likes lorem ipsum's post
								</div>
								<div class="thumbs-icons col-xs-2 padding-right-none">
									<span class="pull-left"><i class="fa fa-thumbs-o-up text-blue" aria-hidden="true"></i></span>
									<span class="pull-right"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
    <!-- social-center-panel  -->
	</div>
	<!-- col-xs-6 -->

</div>
<!-- container -->
</main>


<!-- else part if session expires -->
<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>

<!-- Blog Form ends-->
<?php include('../layout/social_footer.php'); ?>
