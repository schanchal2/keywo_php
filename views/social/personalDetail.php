<!--===============================================
=            include :personalDetail.php            =
================================================-->
<?php
  $socialFileUrlShare = basename($_SERVER['PHP_SELF'], ".php");

  if ($socialFileUrlShare == "otherTimeline") {
    $email = $otherEmail;
  } else {
    $email = $_SESSION["email"];
  }
  //get user info
  $postFields     = "city,country,date_of_birth,mobile_number,hobbies,first_name,last_name,creationTime,account_handle,user_id";
  $userDetail     = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $postFields);

  if ($userDetail["errCode"] == -1) {
    $userData       = $userDetail["errMsg"];
    $userWorkDetail = getUserWorkInfo($userData["user_id"], $walletURLIPnotification . 'api/notify/v2/');
    // printArr($userData);
    // printArr($userWorkDetail);
    if ($userWorkDetail["errCode"] == -1) {
      $education = '-'; //$userWorkDetail["errMsg"]["education"];
    } else {
      $education = '-';
    }
    if ($userDetail["errCode"] == -1) {
        $dob     = !empty($userData["date_of_birth"]) ? $userData["date_of_birth"] : '-';
        $mobile  = !empty($userData["mobile_number"]) ? $userData["mobile_number"] : '-';
        $hobbies = !empty($userData["hobbies"]) ? implode(', ', $userData["hobbies"]) : '-';
    } else {
        $dob     = '-';
        $mobile  = '-';
        $hobbies = '-';
    }
    if ($socialFileUrlShare == "otherTimeline") {
      $firstName     = $userData["first_name"];
      $lastName      = $userData["last_name"];
      $accountHandle = $userData["account_handle"];
    } else {
      $firstName     = $_SESSION["first_name"];
      $lastName      = $_SESSION["last_name"];
      $accountHandle = $_SESSION["account_handle"];
    }
?>
<ul class="list-group text-justify card border-All ">
     <li class="list-group-item list-group border-none">
        <div class="text-blue ellipses fsz18"><?php echo $firstName . ' ' . $lastName; ?></div >
        <!-- <i class="fa fa-ellipsis-v fa_custom pull-right"></i> -->
        <h5 class="line-height text-muted MT-2"><?php echo $accountHandle; ?></h5>
    </li>
    <?php if ($socialFileUrlShare == "privateTimeline") { ?>
    <li class=" list-group-item list-group border-none">
         <span>Email id</span>
         <span class="pull-right text-blue ellipses ellipses-wid text-right"><?php echo $email; ?></span><hr>
    </li>
    <?php } ?>
    <li class="list-group-item list-group border-none">
          <span>Hometown</span>
          <span class="pull-right text-blue ellipses ellipses-wid text-right">
            <?php 
              if (!empty($userData["city"]) && !empty($userData["country"])) {
                echo $userData["city"] . ', ' . $userData["country"];
              } elseif (!empty($userData["city"]) && empty($userData["country"])) {
                echo $userData["city"];
              } elseif (empty($userData["city"]) && !empty($userData["country"])) {
                echo $userData["country"];
              } else {
                echo '-';
              }
            ?>
          </span><hr>
    </li>
    <?php if ($socialFileUrlShare == "privateTimeline") { ?>
    <li class="list-group-item list-group border-none">
          <span>Date of Birth</span>
          <span class="pull-right text-blue"><?php echo $dob; ?></span><hr>
    </li>
    <li class="list-group-item list-group border-none">
          <span>Mobile No </span>
          <span class="pull-right text-blue"><?php echo $mobile; ?></span><hr>
    </li>
    <?php } ?>
    <li class="list-group-item list-group border-none">
          <span>Education</span>
          <span class="pull-right text-blue ellipses ellipses-wid text-right"><?php echo $education; ?></span><hr>
    </li>
    <li class="list-group-item list-group border-none">
          <span>Hobbies</span>
          <span class="pull-right text-blue ellipses"><?php echo $hobbies; ?></span>
    </li>
</ul>
<?php } else { ?>
  <ul class="list-group text-justify card border-All ">
    <li class="list-group-item list-group border-none">
          <span>Something went wrong..</span>
    </li>
  </ul>
<?php } ?>
<!--====  End of include :personalDetail.php  ====-->
