<?php
    session_start();
    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");
    require_once ("../../models/social/socialModel.php");
    require_once("../../models/social/commonFunction.php");

    $email                   = $_SESSION["email"];
    $_SESSION["profile_pic"] = $getUserInteractionDetails['errMsg']['profile_pic'];

    $announcementFlag = 0;
    //get anoncement
  $getAnnouncement = getAnnouncement();
  if(noError($getAnnouncement)){
      $getAnnouncement = $getAnnouncement["errMsg"];
            $announcementFlag = 1;
  }
?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <style type="text/css">
    /**
     *
     * to work stacked modal on index.php
     *
     */
    /*    .modal-backdrop:nth-of-type(odd) {
        z-index: 1051 !important;
    }

    .modal-backdrop:nth-of-type(even) {
        z-index: 1040 !important;
    }*/
    </style>
    <main class="social-main-container inner-7x innerT">
        <div class="container row-10">
            <div class="row">
                <div class="col-xs-3 social-right-position">
                    <div class="social-left-panel">
                        <?php include("leftInfoUser.php"); ?>
                        <div class="followed-keywords-module inner-2x innerMTB">
                            <?php include("followedKeyword.php"); ?>
                            <!-- card -->
                        </div>
                        <div id="suggested-user-details">
                            <?php include("suggestedUser.php"); ?>
                        </div>
                        <!-- my-info-card  -->
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <div class="col-xs-6">
<!--                    --><?php //if(count($getAnnouncement)>0 && ($announcementFlag==1)){
//                foreach($getAnnouncement as $announcements){ ?>
<!--                    <div class="alert alert-warning alert-dismissible fade in" role="alert">-->
<!--                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">-->
<!--                            <span aria-hidden="true">×</span>-->
<!--                        </button> <strong>--><?php //echo $announcements['announcement_text']; ?><!--</strong>-->
<!--                    </div>-->
<!--                    --><?php // } } ?>

                    <?php if(count($getAnnouncement)>0 && ($announcementFlag==1)){
                        foreach($getAnnouncement as $announcements){ ?>
                            <div class="panel panel-primary publicwall-panel radius-none" id="annoncement<?php echo $announcements['_id']; ?>">
                                <div class="panel-heading text-center fsz14"><?php echo $announcements['title']; ?>
                                    <button type="button" class="close closeBtn text-white" id="close<?php echo $announcements['_id']; ?>" onclick="closeAnnoncement('<?php echo $announcements['_id']; ?>');">&times;</button>
                                </div>

                                <div class="panel-body innerAll half">
                                    <?php echo $announcements['announcement_text']; ?>
                                </div>
                            </div>
                        <?php  } } ?>


                    <div class="social-center-panel" style="position:relative;">
                        <!-- =================== social-search-box ================== -->
                        <div class="social-search-box social-card innerMB">
                            <span class="pull-right post-edit-icon" style="position: absolute;right: 20px;top: 5px;">
                        <i class="fa fa-edit pull-right half innerMT fa-lg text-primary-blue"></i>
                </span>
                            <div class="card share-something-textarea" id="social-micro-blog-box">
                                <form onSubmit="return false;" name="status-post" id="status-post" class="margin-none">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group margin-none">
                                                <input type="hidden" name="post-type" value="status">
                                                <div class="share-something-compressed share-small">
                                                    <div>
                                                        <textarea rows="4" class="share-post-box compressed-div form-control checkEmpty can-mention-user inputor" name="modalComment" id="modalComment" placeholder="Share Something..."></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="keywords-for-tags row innerAll clearfix" style="display:none;">
                                        <div id="tags" class="tag-text">
                                            <?php
                            $keywordinputValue = "";
                            foreach ($postDetail['keywords'] as $keywordKey => $keywordValue) {
                                $keywords .= "<span style = 'display:none;'>#".$keywordValue."</span> ";
                                $keywordinputValue .= "#".$keywordValue."  ";
                            }
                            $keywords = rtrim($keywords,', ');
                        ?>
                                                <?php
                            echo $keywords;
                            if (!empty($postDetail['_id'])) {
                        ?>
                                                    <input type="text" value="<?php echo $keywordinputValue; ?>" readonly>
                                                    <?php
                            }
                        ?>
                                                        <input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" style="outline: none;" placeholder="Add #Keywords(min 1-max 3)" value="" />
                                                        <input type="hidden" name="modalKeywords" />
                                        </div>
                                    </div>
                                    <div class="row post-bottom-controls innerMT" style="display:none;">
                                        <div class="share-textbox-quick-links">
                                            <div class="col-xs-4">
                                                <span class="innerR" data-toggle="modal" href="#editPostDetails" onclick="editPersonalPost('','', 'blog','','')">
                                    <a data-toggle="tooltip" data-placement="top" title="Blog">
                                        <i class="fa fa-file-text"></i>
                                    </a>
                                </span>
                                                <span class="innerR" data-toggle="modal" href="#editPostDetails" onclick="editPersonalPost('', '', 'video','','')">
                                    <a data-toggle="tooltip" data-placement="top" title="Video">
                                        <i class="fa fa-play-circle"></i>
                                    </a>
                                </span>
                                                <span data-toggle="modal" href="#editPostDetails" class="innerR" onclick="editPersonalPost('', '', 'image','','')">
                                    <a data-toggle="tooltip" data-placement="top" title="Image">
                                        <i class="fa fa-picture-o"></i>
                                    </a>
                                </span>
                                                <span data-toggle="modal" href="#editPostDetails" class="innerR" onclick="editPersonalPost('', '', 'audio','','')">
                                    <a data-toggle="tooltip" data-placement="top" title="Audio">
                                        <i class="fa fa-soundcloud"></i>
                                    </a>
                                </span>
                                            </div>
                                            <div class="col-xs-8">
                                                <div class="pull-left innerML inner-3x padding-none">
                                                    <select class="selectpicker" name="postscope">
                                                        <option value="Public">Public</option>
                                                        <option value="Followers">Followers</option>
                                                        <option value="Only Me">Only Me</option>
                                                    </select>
                                                </div>
                                                <div class="pull-right">
                                                    <div class="">
                                                        <input value="Post" class="form-post-btn text-center" type="button" id="statusPost" onclick="createNewPost('status', 'status-post', 'create');" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- social-search-box -->
                        <!-- =================== social-all-status-tabs ================== -->
                        <div class="social-all-status-tabs">
                            <div class="row">
                                <div class="col-xs-12">
                                    <ul class="list-inline margin-bottom-none">
                                        <li>
                                            <a id="all" href="#all" class="social-status-tabs active">ALL</a>
                                        </li>
                                        <li>
                                            <a id="status" href="#status" class="social-status-tabs">STATUS</a>
                                        </li>
                                        <li>
                                            <a id="blog" href="#blog" class="social-status-tabs">BLOG</a>
                                        </li>
                                        <li>
                                            <a id="video" href="#video" class="social-status-tabs">VIDEO</a>
                                        </li>
                                        <li>
                                            <a id="image" href="#image" class="social-status-tabs">IMAGE</a>
                                        </li>
                                        <li>
                                            <a id="audio" href="#audio" class="social-status-tabs">AUDIO</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- social-all-status-tabs -->
                        <!--display content-->
                        <div id="post-count" class="hide innerTB bg-white" data-post-count="" data-post-count-msg="" style="text-align:center;cursor:pointer">
                            <a> </a>
                        </div>
                        <div id="ajaxLoader1" class="text-center" style="display:none; ">
                            <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style="width: 60px;">
                        </div>
                        <div id="show-error-message"></div>
                        <div id="home-next-post-data" style="display:none;" data-count="0" data-post-type="all" data-scroll-allow="true" data-status-empty=""></div>
                        <!-- <div id="home-post-data"> -->
                        <div id="home-post-data">
                            <!--display loading content div before data is loaded-->
                            <div id="loading-content-div">
                                <?php include('loadingContent.php'); ?>
                            </div>
                            <!--end of loading content div-->
                            <!--content from ajax-->
                        </div>
                        <!--display content-->
                        <div id="ajaxLoader" class="text-center" style="display:none; ">
                            <img class="" src="<?php echo $rootUrlImages;?>ajaxloader.GIF" style="width: 60px;">
                        </div>
                    </div>
                    <!-- social-center-panel  -->
                </div>
                <!-- col-xs-6 -->
                <div class="col-xs-3 social-right-position">
                    <div class="social-right-panel">
                        <!--search box-->
                        <div class="row innerMB inner-2x">
                            <div class="col-xs-12">
                                <?php include 'include/_social_right_panel_searchbar.php'; ?>
                            </div>
                        </div>
                        <!--trending keywords-->
                        <div class="card social-card right-panel-modules innerMT inner-2x">
                            <?php include 'include/_social_right_panel_tredings.php'; ?>
                        </div>
                        <!--popular Posts-->
                        <div class="popular-modules">
                            <div class="card social-card right-panel-modules inner-2x innerMTB">
                            <?php include 'include/_social_right_panel_popular.php'; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->
        </div>
        </div>
        <!-- container -->
        <div class="modal fade" id="showModalPost" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <div class="clearfix closebutton-absolute">
                        <i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body-social-user-image row-10">
                    </div>
                </div>
            </div>
        </div>
        <!--modal to report User-->
        <div class="modal fade" id="showReportDetails" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <div class="modal-body-report-user">
                    </div>
                </div>
            </div>
        </div>
        <!--modal to show Post is Deleted User-->
        <div class="modal fade" id="postDeletedModal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <div class="modal-body-post-deleted">
                    </div>
                </div>
            </div>
        </div>
        <!--modal to display editing data-->
        <div class="modal fade" id="editPostDetails" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <div class="clearfix closebutton-absolute">
                        <i class="fa fa-times close-button close-dialog" data-dismiss="modal"></i>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>
            </div>
        </div>
        <!-- Likes List Modal -->
        <div id="myModal_like" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="" id="likeUserList">
                    </div>
                </div>
            </div>
        </div>
        <!--modal to show delete confirmation of comments-->
        <div class="modal fade" id="deleteCommentModal" role="dialog">
            <div class="modal-dialog" style="margin-top: 150px;">
                <div class="card social-card clearfix social-form-modal">
                    <div class="modal-header">
                        <button type="button" class=" close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-blue">Delete</h4>
                    </div>
                    <div class="modal-body-social-user-image">
                    </div>
                </div>
            </div>
        </div>
        <!--social-timeline-modal-on-block-post-->
        <div class="modal fade" id="blockUserAcc" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-blue">Block</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to Block Account?
                        </div>
                        <div class="modal-footer border-none">
                            <button type="button" class="btn btn-social-wid-auto yes-remove-block-btn" data-dismiss="modal">Yes</button>
                            <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <!--social-timeline-modal-on-remove-post-->
        <div class="modal fade" id="removePostModal" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title text-blue">Delete</h4>
                        </div>
                        <div class="modal-body">
                            Are you sure you want to delete post?
                        </div>
                        <div class="modal-footer border-none">
                            <button type="button" class="btn btn-social-wid-auto yes-remove-post-btn" data-dismiss="modal">Yes</button>
                            <button type="button" class="btn btn-social-wid-auto-dark" data-dismiss="modal">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Blog Form starts-->
    <!--=====================================================
    =            check why it is in between page            =
    ======================================================-->
    <link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
    <link rel="shortcut icon" href="https://mindmup.s3.amazonaws.com/lib/img/favicon.ico">
    <link href="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
    <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
    <link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
    <!--====  End of check why it is in between page  ====-->
    <!-- else part if session expires -->
    <?php
    } else {
            header('location:'. $rootUrl .'../../views/prelogin/index.php');
    }
    ?>
        <!--modal to display editing data-->
        <?php include('../layout/social_footer.php'); ?>
        <!--script for getting 10-10 post on every scroll down-->
        <script>
        var lastPostTime = 0;
        $(document).ready(function() {
             
            //call api for social feature, using function logoutActivity
            <?php resetLiveCount($_SESSION['id']); ?>

            /* Assign empty url value to the iframe src attribute when
             modal hide, which stop the video playing */
            $("#showModalPost").on('hide.bs.modal', function() {
                $("#cartoonVideo").attr('src', '');
            });


            var bottom = $(document).height() - $(window).height(); //for endless scrolling
            var successflag = 0;
            $(document).scroll(function() {
                /*
                For endless scrolling
                */
                var win = $(window);
                // Each time the user scrolls
                win.scroll(function() {
                    // End of the document reached?
                    if ($(document).height() - win.height() == win.scrollTop()) {
                        // Do the stuff
                        if (successflag == 0) {
                            var lastPostTime = $("#home-post-data .social-timeline-image .social-card:last-child .social-card-created-at").attr("id");
                            var postType = $(".social-all-status-tabs .active").attr("id");
                            //function to call get post from session
                            loadNextPost(lastPostTime, postType);
                            successflag = 1;
                        }
                    } else {
                        // console.log("else");
                        successflag = 0;
                    }
                });
            });
        });

        //function to get next data & append
        function loadNextPost(lastPostTime, type) {
            var n = $("#home-next-post-data").attr("data-count");
            var viewPostType = $('#home-next-post-data').attr('data-post-type');
            var checkScrollDown = $('#home-next-post-data').attr('data-scroll-allow');
            var scrollAllowed = $('#home-next-post-data').attr('data-status-empty');
            if (checkScrollDown == 'true' & scrollAllowed == 'true') {

                $.ajax({
                    type: 'GET',
                    dataType: 'html',
                    data: {
                        type: type,
                        lastPostTime: lastPostTime,
                        n: n
                    },
                    url: '../../views/social/homeTimelineContent.php',
                    success: function(posthomedata) {
                        $('#home-post-data').append(posthomedata);
                        $("#home-next-post-data").attr("data-count", parseInt(n) + 10);
                        var checkScrollStatus = $('#checkScrollStatus-' + n).attr('data-scroll-allow');
                        $('#home-next-post-data').attr('data-status-empty', checkScrollStatus);
                    },
                    beforeSend: function() {
                        $('#home-next-post-data').attr('data-scroll-allow', 'false');
                        $('#ajaxLoader').show();
                    },
                    complete: function() {
                        $('#home-next-post-data').attr('data-scroll-allow', 'true');
                        $('#ajaxLoader').hide();
                    },
                    error: function() {
                        $('#loading-content-div').hide();
                        // $('#show-error-message').text('No Post Yet!!');
                    }
                });
            }
        }

        //--------------------------      Confirmation to block User-----------------------------//
        function blockUserAccByConfirmation(ele, email, postId, postType, postCreatedAt) {
            $('#showModalPost .close-dialog').click();
            $(ele).css('pointer-events','none');
            $("#blockUserAcc").modal("show");
            emailId = email;
            elem = ele;
            blockPostId = postId;
            blockPostType = postType;
            blockPostCreatedAt = postCreatedAt;
            emailId = email;
        }
        $("#blockUserAcc .yes-remove-block-btn").click(function() {
            $("this").prop("disabled", "disabled");
            removePersonalPostForBlockedUser(elem, emailId, blockPostId, blockPostType, blockPostCreatedAt);
            $('#blockUserAcc').modal('hide');
        });
        //--------------------------      removePost-----------------------------//
        // var removePostId, removePostType, removePostCreatedAt;

        // function removePost(postId, postType, postCreatedAt) {
        //     $('#showModalPost .close-dialog').click();
        //     $("#removePostModal").modal("show");
        //     removePostId = postId;
        //     removePostType = postType;
        //     removePostCreatedAt = postCreatedAt;
        // }
        // $(".yes-remove-post-btn").click(function() {
        //     removePersonalPost(removePostId, removePostType, removePostCreatedAt);
        //     $('#removePostModal').modal('hide');
        // });




       function closeAnnoncement(id){
               $("#annoncement"+id).hide();
       }
        </script>
