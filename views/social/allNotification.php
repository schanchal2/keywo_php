<?php
session_start();

include("../layout/header.php");
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/arrayHelper.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
  //session is not active, redirect to login page
  print("<script>");
  print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
  print("</script>");
  die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
  <div class="container row-10 padding-none">
    <div class="row">
      <div class="col-xs-3">
        <div class="social-left-panel left-panel-modules">
          <div class="card left-panel-modules inner-8x innerMT">
            <div class="bg-light-gray left-panel-modules-head">
              <div class="row margin-none">
                <div class="col-xs-12">
                  <h4>Notification </h4>
                </div>
              </div>
            </div>
            <div class="settings-preference-module">
              <div class="margin-none">
                <div class="social-user-setting-name border-bottom">
                  <a id="notify-all" class="active notify-view-data"><b>All</b></a>
                </div>
                <div class="social-user-setting-name border-bottom">
                  <a class="accordion-toggle side-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnetwo"><b>Social</b></a>
                  <div id="collapseOnetwo" class="panel-collapse collapse">
                    <a id="notify-comment" class="border-bottom notify-view-data">Comment</a>
                    <a id="notify-following" class="border-bottom notify-view-data">Following</a>
                    <a id="notify-like" class="border-bottom notify-view-data">Like</a>
                    <a id="notify-mention" class="border-bottom notify-view-data">Mention</a>
                    <a id="notify-reply" class="notify-view-data">Reply</a>
                  </div>
                </div>
                <div class="social-user-setting-name border-bottom">
                  <a class="accordion-toggle side-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOnethree"><b>Keyword</b></a>
                  <div id="collapseOnethree" class="panel-collapse collapse">
                    <a id="notify-ask" class="border-bottom notify-view-data">Set ask</a>
                    <a id="notify-bid" class="border-bottom notify-view-data">Place bid</a>
                    <a id="notify-buy" class="border-bottom notify-view-data">Buy keyword</a>
<!--                     <a id="notify-deposit" class="border-bottom notify-view-data">Deposit</a>
                    <a id="notify-withdrawal" class="notify-view-data">Withdrawal</a>
 -->                  </div>
                </div>
                <div class="social-user-setting-name border-bottom">
                  <a id="notify-wallet" class="notify-view-data"><b>Wallet</b></a>
                </div>
              </div>
            </div>
          </div>
          <?php include '_sidebar_activity_log.php'; ?>
        </div>
      </div>

      <div id="activity__log">
        <div class="col-xs-9">
          <div class="social-center-panel">
            <div class="row">
              <div class="col-xs-12 innerB">
                <div class="profile-heading ">
                  <h4 class="text-color-Text-Primary-Blue margin-none f-sz20">Notification</h4>
                  <h5 class=" margin-none f-sz15"></h5>
                  <div class="pull-right">
                    <form class="clearfix margin-none">
                      <div class="pull-left search-box-comments">
                        <div class="input-group">
                          <!-- <input type="text" id="search_box" name="q" value="" class="form-control" placeholder="Search" required=""> -->
                          <!-- <input type="submit" class="search-btn-header" value=""> -->
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="append-data-notify">
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
<!-- else part if session expires -->
<?php
} else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
}

include('../layout/social_footer.php');
?>
