<?php

header("Access-Control-Allow-Origin: *");
ini_set('default_charset','utf-8');
header('Content-type: text/html; charset=utf-8');
session_start();
require_once "../../config/config.php";
require_once "../../config/db_config.php";
require_once "../../models/user/authenticationModel.php";
require_once "../../models/analytics/userRegistration_analytics.php";
require_once('../../helpers/deviceHelper.php');
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";




if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
   print("<script>");
   print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
   print("</script>");
   die;
}

//check session & redirect to FTU page
$redirectUrl = $rootUrl . "/views/user/ft_like.php";
if($_SESSION["userFteStatusFlag"]==0){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$redirectUrl."';\", 000);");
    print("</script>");
    die;
}


        //Get following array
        $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
        $getFollowingArray = getFollowUser($targetDirAccountHandler);

        //Get Blocked Array
        $getBlockedArray   = getBlockedUser($targetDirAccountHandler);

          if(count($getBlockedArray)==0 ){
            $getFinalArray = $getFollowingArray;
          }elseif(count($getFollowingArray)==0){
              $getFinalArray = $getBlockedArray;
          }else{
            //merge two arrayHelper
            $getFinalArray = array_merge($getFollowingArray, $getBlockedArray);
          }



        //Get suggested user
        $getSuggestedList = getSuggestedUser($_SESSION['id']);
//        printArr($getSuggestedList);
          if(noError($getSuggestedList)){
              $getSuggested             = $getSuggestedList["errMsg"];
          }
          foreach($getFinalArray as $followingArray){
            foreach($getSuggested  as $key => $getSuggestedDetails) {
                $suggestedId = $getSuggestedDetails['user_id'];
                if($suggestedId==$followingArray){
                  unset($getSuggested[$key]);
                }
            }
          }
          $getSuggested = array_values($getSuggested);



if($getSuggestedList['errCode']==4){
    ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">No users in suggested list: We will show a list of users you might be interested in following based on the ones you are following.</h4>
    </div>
    <?php
}elseif(count($getSuggested)==0) {
    ?>
    <div class="modal-header">
        <h4 class="modal-title text-blue">No users in suggested list: We will show a list of users you might be interested in following based on the ones you are following.</h4>
    </div>
    <?php
}else { ?>
<ul class="list-unstyled social-card innerMT inner-2x">
    <?php
    foreach ($getSuggested as $key => $getSuggestedDetails) {
        if (isset($getSuggestedDetails["profile_pic"]) && !empty($getSuggestedDetails["profile_pic"])) {
            global $cdnSocialUrl;
            global $rootUrlImages;

            $extensionUP  = pathinfo($getSuggestedDetails["profile_pic"], PATHINFO_EXTENSION);
            //CDN image path
            $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $getSuggestedDetails["account_handle"] . '/profile/' . $getSuggestedDetails["account_handle"] . '_' . $getSuggestedDetails["profile_pic"] . '.40x40.' . $extensionUP;

            //server image path
            $imageFileOfLocalUP = $rootUrlImages . 'social/users/' . $getSuggestedDetails["account_handle"] . '/profile/' . $getSuggestedDetails["account_handle"] . '_' . $getSuggestedDetails["profile_pic"];

            // check for image is available on CDN
            $file = $imageFileOfCDNUP;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                $imgSrc = $imageFileOfLocalUP;
            } else {
                $imgSrc = $imageFileOfCDNUP;
            }
        } else {
            $imgSrc = $rootUrlImages . "default_profile.jpg";
        }
        ?>

        <li class="innerTB clearfix">
            <div class="col-xs-5 ellipses ">
                <img class="img-circle" src="<?php echo $imgSrc; ?>">
                <a class="follower-name innerL half" <?php if ($getSuggestedDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($getSuggestedDetails['email']); ?>" <?php } else { ?> href="<?php echo $rootUrl; ?>" <?php } ?>
                   title="<?php echo ucwords($getSuggestedDetails['first_name']) . " " . ucwords($getSuggestedDetails['last_name']); ?>"><?php echo ucwords($getSuggestedDetails['first_name']) . " " . ucwords($getSuggestedDetails['last_name']); ?> </a>
            </div>
            <div class="col-xs-3">
                <a class="follower-id" <?php if ($getSuggestedDetails['user_id'] != $_SESSION['id']) { ?> href="<?php echo $rootUrl; ?>views/social/otherTimeline.php?email=<?php echo base64_encode($getSuggestedDetails['email']); ?>" <?php } else { ?> href="<?php echo $rootUrl; ?>" <?php } ?> ><?php echo '@' . $getSuggestedDetails['account_handle']; ?></a>
            </div>
            <?php
            $followFlag = false;
            ?>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <input type="button" id='<?php echo $getSuggestedDetails["account_handle"]; ?>'
                       class="btn-social<?php if ($followFlag) {
                           echo "-dark";
                       } ?> pull-right follow-unfollow-button"
                       onclick="ajaxAppFollowEvent('<?php echo $getSuggestedDetails["account_handle"]; ?>','<?php echo $getSuggestedDetails["email"]; ?>');"
                       value='<?php if ($followFlag) {
                           echo "Unfollow";
                       } else {
                           echo "Follow";
                       } ?>'>
            </div>
        </li>
    <?php }
    }
?>
</ul>
