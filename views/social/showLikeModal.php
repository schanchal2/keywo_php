<?php


session_start();

/* Add DB Management Files */
require_once ('../../config/config.php');

/* Add Helpers */
require_once ('../../helpers/coreFunctions.php');
require_once ('../../helpers/errorMap.php');
require_once ('../../helpers/arrayHelper.php');
require_once ('../../helpers/stringHelper.php');
require_once "../../models/social/commonFunction.php";

/* Add Model */
require_once ('../../models/social/socialModel.php');

error_reporting(0);
$extraArg = array();
// printArr($_SESSION); die();
if(isset($_SESSION) && !empty($_SESSION)){
    $returnArr = array();

    $postId                  = cleanXSS(trim(urldecode($_POST['post_id'])));
    $postTime                = cleanXSS(trim(urldecode($_POST['created_at'])));
    $lowerLimit              = cleanXSS(trim(urldecode($_POST['likelowerLimit'])));
    $upperLimit              = cleanXSS(trim(urldecode($_POST['likeupperLimit'])));;

    $getUserDetails          = getUserDetailsOnActivity($postId, $postTime, 'like', $lowerLimit, $upperLimit);
    // printArr($getUserDetails);
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION['account_handle']."/";
    $bulkData            = array();

    $postIddata = array(
        'post_id' => $postId,
        'time'    => $postTime
    );

    array_push($bulkData, $postIddata);
    $bulkPostId = json_encode($bulkData);


    // Get like post activity count from node database
    $activityCount = getPostActivityCount($bulkPostId, '');
    $targetActivityCount = $activityCount['errMsg'][$postId]; //printArr($targetActivityCount);
    $like_count          = $targetActivityCount['like_count'];

    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $files2 = dirToArray($targetDirAccountHandler);
    $blockedData = "";
    for ($i= 0; $i < count($files2); $i++ ) {
      $Final12= $targetDirAccountHandler.$files2[$i];
      $jsondata12 = file_get_contents($Final12);
      $data12 = json_decode($jsondata12, true);
      $userIds12 = "";
      $userIds12 = $data12["blocked"];
        for($k=0; $k < count($userIds12); $k++) {
          $userIdsOfAccHandler[] = $userIds12[$k];
        }
    }

    //get blocked array
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
    $userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);


    if(noError($getUserDetails)){
        $errMsg         = "Success : User details found";
        $getUserDetails = $getUserDetails['errMsg'][0]['liked_by'];
        $returnArr      = setErrorStack($returnArr, -1, $errMsg, $extraArg);
        if ($lowerLimit == 0) {
          foreach($getUserDetails as $newgetUserDetails){
            $newArr[] = $newgetUserDetails['user_id'];
          }

          // printArr($userIdsOfAccHandler);
          // printArr($newArr);

          //Find out Common files from each other
          $mutualElementOfFile = array_diff($newArr,$userIdsOfAccHandler);
          // printArr($mutualElementOfFile);

?>

        <div class="modal-header half innerB">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-blue"><i class="fa fa-thumbs-up innerMR"></i><?php
            echo $like_count;
             ?></h4>
        </div>
        <?php }?>
        <div class="modal-body like-list-body padding-none">
            <?php

            /* To Create Array of all Following User Ids. */
            $files2 = dirToArray($targetDirAccountHandler);
            $followedUser = array();
            foreach($files2 as $key => $files2Details)
            {
                $Final= $targetDirAccountHandler.$files2Details;
                $jsondata = file_get_contents($Final);
                $data = json_decode($jsondata, true);
                foreach ($data['followings'] as $key => $value) {
                   $followedUser[] = $value;
                }
            }

            /* Creating Dynamic HTML */
            foreach($getUserDetails as $key => $value) {
            // printArr($value);
            if(isset($value["profile_pic"]) && !empty($value["profile_pic"])){
                global $cdnSocialUrl;
                global $rootUrlImages;

                $extensionUP  = pathinfo($value["profile_pic"], PATHINFO_EXTENSION);
                //CDN image path
                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $value["account_handle"] . '/profile/' . $value["account_handle"] . '_' . $value["profile_pic"] . '.40x40.' . $extensionUP;

                //server image path
                $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$value["account_handle"].'/profile/'.$value["account_handle"].'_'.$value["profile_pic"];

                // check for image is available on CDN
                $file = $imageFileOfCDNUP;
                $file_headers = @get_headers($file);
                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                    $imgSrc = $imageFileOfLocalUP;
                } else {
                    $imgSrc = $imageFileOfCDNUP;
                }
            }else{
                  $imgSrc = $rootUrlImages."default_profile.jpg";
            }
            if(!in_array($value['user_id'],$userIdsOfAccHandler)){

              if ($key < $upperLimit) {
                $nextLikeCount = $nextLikeCount + 1;
            ?>

                <div class="innerAll border-bottom clearfix">
                    <div class="innerML half pull-left">
                        <img src="<?php echo $imgSrc; ?>" class="liked-user-img img-responsive img-circle">
                    </div>
                    <div class=" pull-left text-blue innerMT innerLR">
                        <div class="liked-user-name"><?php echo $value['first_name'] . ' ' . $value['last_name']; ?></div>
                    </div>
                    <div class="pull-left innerMT innerLR">
                        <div class="liked-user-account-handle text-color-Gray"><?php echo '@'.$value['account_handle']?></div>
                    </div>

                    <?php if ($_SESSION['account_handle'] != $value['account_handle']) { ?>
                        <div class="innerAll half pull-right">
                            <!-- <i class="text-blue half innerT fa fa-ellipsis-v"></i> -->
                        </div>
                        <div class="innerMT innerMR half pull-right">
                        <?php
                        /* Searching following array and deside button text */
                        $followFlag = in_array($value['user_id'], $followedUser);
                        if ($followFlag) {
                            $followBtnLink = 'Unfollow';
                        } else {
                            $followBtnLink = 'Follow';
                        }
                        ?>
                            <input type="button" id  = "<?php echo $value['account_handle'];?>" class="btn-social-wid-auto<?php if ($followFlag) { echo '-dark'; }?> pull-right" value="<?php echo $followBtnLink; ?>" onclick="<?php if ($_SESSION['account_handle'] != $value['account_handle']) { ?>ajaxAppFollowEvent('<?php echo $value["account_handle"]; ?>','<?php echo $value["email"]; ?>');<?php } ?>" style= "<?php if ($_SESSION['account_handle'] == $value['account_handle']) { echo 'cursor: not-allowed;';}?>">
                        </div>
                    <?php } ?>
                </div>
                <?php
              }
            }
          }
            // if(count($getUserDetails)==)
            // printArr(count($getUserDetails));
            // printArr($upperLimit);
            if(count($getUserDetails)==$upperLimit){
            if (count($getUserDetails) > ($lowerLimit - 1)) {
              ?>
              <div id = "likeLoadMore-<?php echo $lowerLimit; ?>"><a onclick = 'LikeCountModal("<?php echo $postId; ?>", "<?php echo $postTime; ?>" , "<?php echo ($lowerLimit + ($upperLimit)); ?>", "<?php echo $upperLimit; ?>");'>Load More</a></div>
              <?php
            }
            }?>
        </div>
        <?php
    }else{
        $errMsg         = "Error : User details found";
        $returnArr      = setErrorStack($returnArr, 3, $errMsg, $extraArg);
    }

} else {
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

?>
