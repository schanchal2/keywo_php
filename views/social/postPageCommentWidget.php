<!--====================================================
=            include: postPageCommentWidget            =
=====================================================-->
<?php 
  if(isset($_SESSION["profile_pic"]) && !empty($_SESSION["profile_pic"])){
      global $cdnSocialUrl;
      global $rootUrlImages;

      $extensionUP  = pathinfo($_SESSION["profile_pic"], PATHINFO_EXTENSION);
      //CDN image path
      $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $_SESSION["account_handle"] . '/profile/' . $_SESSION["account_handle"] . '_' . $_SESSION["profile_pic"] . '.40x40.' . $extensionUP;

      //server image path
      $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$_SESSION["account_handle"].'/profile/'.$_SESSION["account_handle"].'_'.$_SESSION["profile_pic"];

      // check for image is available on CDN
      $file = $imageFileOfCDNUP;
      $file_headers = @get_headers($file);
      if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
          $sessionImgSrc = $imageFileOfLocalUP;
      } else {
          $sessionImgSrc = $imageFileOfCDNUP;
      }
  }else{
      $sessionImgSrc = $rootUrlImages."default_profile.jpg";
  }
?>
<input type="hidden" id="comment-basic-details<?php echo $commentPostId; ?>" class="comment-special-class" post-id="<?php echo  $commentPostId;?>" post-creater-email="<?php echo  $commentPostCreator;?>" post-created-at="<?php echo $commentPostCreationTime; ?>" call-ajax-flag="true" call-ajax-get-flag="true" call-ajax-edit-flag="true" call-reply-ajax-flag="true">
<div id="comment-text-add<?php echo  $commentPostId;?>" class="comments-module-wrapper row">
    <!-- <div class="col col-50"> -->
    <div class="col-xs-1 col-50">
        <img width="30" height="30" src="<?php echo $sessionImgSrc; ?>">
    </div>
    <!-- <div class="col col-440 padding-left-none comment-text-empty"> -->
    <div class="col-xs-11 padding-none comment-text-empty">
        <div id="comment-form<?php echo $commentPostId; ?>" class="form-group">
            <input type="text" id="user-comment-text<?php echo $commentPostId; ?>" class="form-control user-comment-input checkEmpty can-mention-user inputor" placeholder="Write Comment Here" maxlength="600">
        </div>
    </div>
</div>
<div class="comment-inner-section" id="comment-append<?php echo $commentPostId; ?>">
    <!--all comment will append in this div by JQuery-->
</div>
<!--<div class="row comments-module-wrapper myComment1" style = "display:none;">
  <div class="col-xs-1">
    <img src="<?php echo $rootUrlImages; ?>rightpanel-img/<?php echo $_SESSION['profile_pic']; ?>">
  </div>
  <div class="col-xs-10">
    <div class="comments-module-comment-block">
      <a href="#" class="comments-module-username"><?php echo $_SESSION["first_name"]; ?> <?php echo $_SESSION["last_name"]; ?></a>
      <span id = "user-comment-title<?php echo $commentPostId; ?>" class="comments-module-comment "></span>
    </div>
    <div class="comments-module-time-block">
      <a href="#" class="comments-module-reply">Reply</a>
      <span id = "comment-created-time<?php echo $commentPostId; ?>" class="comments-module-hrs"></span>
    </div>
  </div>
  <div class="col-xs-1">
    <a  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-pencil"></i></a>
    <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
      <li>
        <a data-toggle="modal" href="#22" >Edit</a>
      </li>
      <li>
        <a data-toggle="modal" href="#22q" >Delete</a>
      </li>
    </ul>
  </div>
</div>-->
<!--likes & Dislikes-->
<!--comments-module-wrapper-->
<!--<div class="row comments-module-wrapper">
    <div class="col-xs-1">
        <img src="<?php echo $rootUrlImages?>social/comment.png">
    </div>
    <div class="col-xs-11">
        <div class="comments-module-comment-block">
            <a href="#" class="comments-module-username">David Mondrus</a>
            <span class="comments-module-comment">not "totally" new</span>
        </div>
        <div class="comments-module-time-block">
            <a href="#" class="comments-module-reply">Reply</a>
            <span class="comments-module-hrs">23 hrs</span>
        </div>
    </div>
</div>
<!--comments-module-wrapper -->
<div id="commentLoadMoreButton<?php echo $commentPostId; ?>" class="row comments-module-wrapper" style="display:none;">
    <div class="col-xs-12">
        <a id="loadMoreCommentPostPage<?php echo $commentPostId; ?>" class="comments-module-all-comments" onclick="">View <span id = "remainingLoadCount<?php echo $commentPostId; ?>" class="comment-count"></span> more comments </a>
    </div>
</div>
<!--====  End of include: postPageCommentWidget  ====-->
<script>
    $(function () {
    $('input').blur();
});
</script>