<?php
    session_start();

    //check for session
    if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

        include("../layout/header.php");

        $email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_social.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="social-main-container">
        <div class="container row-10">
            <div class="row">
                <div class="col-xs-3">
                    <div class="social-left-panel left-panel-modules">
                    </div>
                    <!-- social-left-panel  -->
                </div>
                <!-- col-xs-3 -->
                <div class="col-xs-6">
                </div>
                <!-- col-xs-6 -->
                <div class="col-xs-3">
                    <div class="social-right-panel">
                    <ul class="list-group text-justify card border-All ">
                         <li class=" list-group-item border-none">
                            <span class="text-blue fsz18">Lorem ipsum</span >
                            <i class="fa fa-ellipsis-v fa_custom pull-right"></i>
                            <h5 class="line-height text-muted MT-2">@loremipsum45</h5>
                        </li>
                        <li class=" list-group-item list-group border-none">
                             <span>Email id</span>
                             <span class="pull-right text-blue ellipses">lorem56@yahho.com</span><hr>
                        </li>
                       
                        <li class="list-group-item list-group border-none">
                              <span>Hometown</span>
                              <span class="pull-right text-blue ellipses">Mumbai, India</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none">
                              <span>Date of Birth</span>
                              <span class="pull-right text-blue">11th May,1981</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none">
                              <span>Mobile No</span>
                              <span class="pull-right text-blue">9002123654</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none">
                              <span>Education</span>
                              <span class="pull-right text-blue ellipses">BCA</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none">
                              <span>Hobbies</span>
                              <span class="pull-right text-blue ellipses">Swimming, Reading</span>
                        </li>
                    </ul>


                     <ul class="list-group text-justify card border-All">
                        <li class=" list-group-item list-group border-none"><p class="text-blue fsz18"><i class="fa fa-building-o" aria-hidden="true"></i> Work Detail</p>
                          <span>Company Name</span>
                          <span class="pull-right text-blue ellipses">xyztechnosoft</span><hr>
                          </li>
                        <li class="list-group-item list-group border-none">
                              <span>Designation</span>
                              <span class="pull-right text-blue ellipses">Web Designer</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none text-justify">
                              <span>Professional Skill</span>
                              <span class="pull-right text-blue">Photoshop</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none text-justify">
                              <span>Mobile No</span>
                              <span class="pull-right text-blue">9002123654</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none text-justify">
                              <span>Location</span>
                              <span class="pull-right text-blue ellipses">Thane,India</span><hr>
                        </li>
                        <li class="list-group-item list-group border-none text-justify">
                              <span>Contact No.</span>
                              <span class="pull-right text-blue ellipses">022-123-456</span>
                        </li>
                    </ul>


                      




                        <?php 

                    /**
                    * For right panel of "http://pasteboard.co/Pfbjyw2Ug.jpg"
                    * origanal page referenced is social/index-html.php
                    * list could hav been used
                    * 
                    */
                      ?>
                        <!-- <div class="inner-2x innerMB">
                            <div class="card right-panel-modules personal_details">
                                <div class="bg-light-gray right-panel-modules-head">
                                    <div class="row margin-none">
                                        <div class="col-xs-12 innerB half">
                                            <h4 class="half innerMTB f-sz22"> Lorem ipsum</h4>
                                            <span class="personal_details__account_handel_name text-color-White opacity-9">@dfgdghgd</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-none  border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Email id </div>
                                        <div class="personal_details__value">loremipsum@dolor.sit</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Hometown </div>
                                        <div class="personal_details__value"> Mumbai, India</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Date of Birth </div>
                                        <div class="personal_details__value">11th May, 1981</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Mobile No. </div>
                                        <div class="personal_details__value">+91 8888888888</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Education </div>
                                        <div class="personal_details__value">BCA</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Hobbies </div>
                                        <div class="personal_details__value">Swimming, Readding</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="personal_details__key text-color-Text-Primary-Blue"> Languages Known </div>
                                        <div class="personal_details__value">English, Hindi</div>
                                    </div>
                                </div>
                            </div>
                            <!-- card -->
                      <!--   </div>
                        <div class="inner-2x innerMB">
                            <div class="card right-panel-modules work_details">
                                <div class="bg-light-gray right-panel-modules-head">
                                    <div class="row margin-none">
                                        <div class="col-xs-12">
                                            <h4 class="half innerMTB">Work Deatils</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="work_details__key text-color-Text-Primary-Blue"> Company Name </div>
                                        <div class="work_details__value">abcxyzabcxyzabcxyz Company</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="work_details__key text-color-Text-Primary-Blue"> Designation </div>
                                        <div class="work_details__value"> Web Designer</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="work_details__key text-color-Text-Primary-Blue">Professional skills </div>
                                        <div class="work_details__value">Photoshop</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="work_details__key text-color-Text-Primary-Blue"> Location </div>
                                        <div class="work_details__value">Thane, India</div>
                                    </div>
                                </div>
                                <div class="row margin-none border-bottom">
                                    <div class="col-xs-12">
                                        <div class="work_details__key text-color-Text-Primary-Blue"> Contact No. </div>
                                        <div class="work_details__value">022-123-123</div>
                                    </div>
                                </div>
                            </div>
                            <!-- card --> 
                        <!-- </div> --> 
                        <!-- 

                    -->
                    </div>
                    <!-- social-right-panel  -->
                </div>
                <!-- col-xs-3 -->
            </div>
        </div>
        <!-- container -->
    </main>
    <!-- -->
    <!-- ============= Blog Form starts has to be changed ================================-->
    <link rel="apple-touch-icon" href="//mindmup.s3.amazonaws.com/lib/img/apple-touch-icon.png" />
    <link rel="shortcut icon" href="http://mindmup.s3.amazonaws.com/lib/img/favicon.ico">
    <link href="../../frontend_li$login_statusbraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.css" rel="stylesheet">
    <!--<link href="http://netdna.bootstrapcdn.com/font-awesome/3.0.2/css/font-awesome.css" rel="stylesheet">-->
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/jquery.hotkeys.js"></script>
    <script src="../../frontend_libraries/bootstrap-wysiwyg-master/external/google-code-prettify/prettify.js"></script>
    <link href="../../frontend_libraries/bootstrap-wysiwyg-master/index.css" rel="stylesheet">
    <script>
        jQuery("ul li").append("<hr/>");
        jQuery("ul li").css({
            "padding-top":"0"
            "padding-bottom":"0"
        })
    </script>
    <!-- ============= Blog Form starts Blog Form starts has to be changed ================================-->
    <!-- else part if session expires -->
    <?php
} else {
        header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
        <?php include('../layout/social_footer.php'); ?>
