<!--social-timeline-tags-details-->
<?php //printArr($postDetail); die;
  $file      = basename($_SERVER['SCRIPT_NAME'], ".php");
  $extension = pathinfo($postSharedImgUrl, PATHINFO_EXTENSION);
  if(!isset($postSharedParentUserId)){
      $postSharedParentUserId = "";
  }

  if(!isset($parentCommentCount)){
      $parentCommentCount= "";
  }

  if(!isset($post_share_earnings_parent)){
      $post_share_earnings_parent = "";
  }

  if(!isset($parentShareCount)){
      $parentShareCount = "";
  }
?>

  <div class="col-xs-12">
    <div class="social-timeline-other-user-content half innerTB">
    <div class="row">
      <div class="col-xs-12">
        <div class="">
          <span class="pull-right text-color-Slate-Grey text-small">Posted by:
            <?php if ($_SESSION['id'] == $postSharedParentUserId) { ?>
              <a href="../../views/social/privateTimeline.php" class="text-social-primary" target="_blank">
            <?php } else { ?>
              <a href="../../views/social/otherTimeline.php?email=<?php echo base64_encode($postSharedParentEmail); ?>" class="text-social-primary" target="_blank">
            <?php } ?>
              <?php echo $postSharedParentFname; ?> <?php echo $postSharedParentLname; ?>
            </a>
          </span>
          <!-- <span class="social-post-time"><span title="<?php  //echo  date("h:i A", $timestamp2)." - ".date('d M Y', $timestamp2);;?>"><?php //include("getDateFormate.php"); ?></span> -->
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
        <p class="innerMB text-color-Username-Follow">
          <?php
            if ($file == 'socialPostDetails') {
              echo rawurldecode($postSharedShortDesc);
            } else {
              $parentPostData = strlen($postSharedShortDesc) > $validateCharCountOnSocial ? substr($postSharedShortDesc, 0, $validateCharCountOnSocial) . '<a href="socialPostDetails.php?post_id='.base64_encode($postSharedParentId).'&timestamp='.$postSharedCreatedAt.'&posttype='.base64_encode($postSharedType).'" target="_blank">....See More</a>' : $postSharedShortDesc;
              echo rawurldecode($parentPostData);
            }
          ?>
        </p>
      </div>
    </div>
    <!-- switch case-->
    <?php
    $parentPostValue = $postSharedType;
    switch ($parentPostValue) {
        case 'video':
    ?>
    <div class="social-timeline-content-video">
      <div class="row">
        <div class="col-xs-12 modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
        <?php
          $YoutubeVideoId = getYoutubeIdFromUrl($postSharedAssetUrl);
          $pageName = basename($_SERVER['SCRIPT_NAME'], ".php");
        ?>
          <div align="center" class="embed-responsive embed-responsive-16by9" <?php if ($pageName != 'showPostModal') {?>style="background: url('<?php echo $YoutubeVideoId; ?>'); background-position: center; background-size: 100%;" <?php } ?>>
            <!-- <video loop class="embed-responsive-item">
                <source src=http://techslides.com/demos/sample-videos/small.mp4 type=video/mp4>
            </video> -->

            <?php if ($pageName == 'showPostModal' || $pageName == 'socialPostDetails') { ?>
            <iframe width="560" height="258" src="<?php echo $postSharedAssetUrl; ?>?autoplay=1" frameborder="0" allowfullscreen></iframe>
            <?php } else {  ?>
              <img class="img-responsive" src="<?php echo $rootUrlImages . 'y3.png';?>" style = "max-width: 70px; margin-top: 20%;">
            <?php } ?>
        </div>
        </div>
      </div>
    </div>
    <?php	break;
        case 'audio':
    ?>
    <div class="social-timeline-sound">
      <div class="row">
        <div class="col-xs-12">
          <iframe width="100%" height="150" scrolling="no" frameborder="no"
          src="<?php echo $postSharedAssetUrl; ?>">
          </iframe>
        </div>
      </div>
    </div>
    <?php break;
        case 'blog':
        if ($file == 'socialPostDetails') {
    ?>
    <div class="social-timeline-content-blog-nothumb border-none">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="text-social-primary"><?php echo $postSharedBlogTitle; ?></h4>
        </div>

        <?php if(isset($postSharedImgUrl) && !empty($postSharedImgUrl)) { ?>
          <div class="col-xs-12 modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
            <img src="<?php echo $cdnSocialUrl . "users/" . $postSharedParentAccHandle . "/post/blog/featured/" . $extension . "/" . $postSharedParentId . '_' . $postSharedCreatedAt . '_' . $postSharedImgUrl . ".725x500." . $extension; ?>" class="img-responsive" />
          </div>
        <?php } ?>

        <div class="col-xs-12">
          <p class="innerT">
            <?php echo rawurldecode($postSharedBlogContent); ?>
          </p>
        </div>
      </div>
    </div>
    <?php } else { ?>

    <div class="social-timeline-content-blog-nothumb border-none">
      <div class="row">
        <div class="col-xs-12">
          <div class="blog-nothumb-content">
              <span class="blog-nothumb-head text-color-Gray">
                <strong>
                  <?php echo $postSharedBlogTitle; ?>
                </strong>
              </span>
              <?php if (isset($postSharedImgUrl) && !empty($postSharedImgUrl)) { ?>
                <!--social-timeline-blog-image-->
                <div class="social-timeline-content-blog-thumb">
                  <div class="row">
                    <div class="col-xs-12 blog-display-img modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                      <img src="<?php echo $cdnSocialUrl . 'users/' . $postSharedParentAccHandle .  '/post/blog/featured/' . $extension . '/' . $postSharedParentId . '_' . $postSharedCreatedAt . '_' . $postSharedImgUrl; ?>" class="img-responsive"/>
                    </div>
                  </div>
                </div>
                <!--social-timeline-blog-image-->
              <?php } else { ?>
                <span class="blog-nothumb-body btn-block text-color-Gray modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
                  <?php
                    $blogText    = strip_tags($postSharedBlogContent);
                    $blogContent = strlen($blogText) > $validateCharCountOnSocial ? substr($blogText, 0, $validateCharCountOnSocial) . '<a href="socialPostDetails.php?post_id='.base64_encode($postSharedParentId).'&timestamp='.$postSharedCreatedAt.'&posttype='.base64_encode($postSharedType).'" target="_blank">....See More</a>' : $blogText;
                    echo rawurldecode($blogContent);
                  ?>
                </span>
              <?php } ?>
          </div>
        </div>
      </div>
    </div>
    <?php }
      break;
        case 'image':
    ?>
    <!--  content-image-->
    <div class="social-timeline-content-image">
      <div class="row">
        <div class="col-xs-12 modal--show--post" data-postid="<?php echo $postId; ?>" data-createdat="<?php echo $postCreatedAt; ?>" data-searchcount="<?php echo $searchCount; ?>" data-ipaddr="<?php echo $ipAddr; ?>" data-emailid="<?php echo $emailId; ?>" data-usersearchtype="<?php echo $userSearchType; ?>">
          <?php
            $extension = pathinfo($postSharedImgUrl, PATHINFO_EXTENSION);

            //CDN image path
            $imageFileOfCDN = $cdnSocialUrl . 'users/' . $postSharedParentAccHandle . '/post/images/' . $extension . '/' . $postSharedParentId . '_' . $postSharedCreatedAt . '_' . $postSharedImgUrl;

            //server image path
            $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postSharedParentAccHandle . '/post/images/' . $extension . '/' . $postSharedParentId . '_' . $postSharedCreatedAt . '_' . $postSharedImgUrl;

            // check for image is avilable on CDN
            $file = $imageFileOfCDN;
            $file_headers = @get_headers($file);
            if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $finalImagePath = $imageFileOfLocal;
            } else {
            $finalImagePath = $imageFileOfCDN;
            }

        ?>

          <img src="<?php echo $finalImagePath; ?>" class="img-responsive" />

        </div>
      </div>
    </div>
    <!--social-timeline-content-image-->
    <!-- <?php // break;
        // case 'status':
    ?>
    <div class="social-timeline-user-status half innerMT">
      <div class="row">
        <div class="col-xs-12">
          <div class="user-status-content">
            <span class="text-dark-gray">
              <?php
                // $statusData = strlen($postSharedShortDesc) > $validateCharCountOnSocial ? substr($postSharedShortDesc, 0, $validateCharCountOnSocial) . "<a href='#'><span class='text-blue'>....See More</span></a>" : $postSharedShortDesc;
                // echo $statusData;
              ?>
            </span>
          </div>
        </div>
      </div>
    </div> -->
    <?php break;
    }
    ?>
    <!--end of switch case-->
    <div class="social-timeline-earning-comments-view innerT">
  <div class="row">
    <div class="col-xs-5">
      <div>
        <label class="text-color-Gray">Earning : </label>
        <span>
                       <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                   origPrice="<?php echo number_format("{$post_share_earnings_parent}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_share_earnings_parent}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_share_earnings_parent}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
        </span>
      </div>
    </div>
    <div class="col-xs-7">
      <div class="">
        <ul class="list-inline pull-right margin-bottom-none">
          <li>
            <div>
            <?php
            $likeCount = "";
            if (!empty($parentLikeCount)) { ?>
            <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30' ,'<?php echo $parentLikeCount; ?>');">
                <?php $likeCount = $parentLikeCount; ?>
                <span class="text-color-Gray half innerR parent-like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$likeCount}", 0); ?></span>
            </a>
            <?php } else {?>
            <a id="like-count<?php echo $postId; ?>" data-toggle="modal" onclick="LikeCountModal('<?php echo $postId; ?>', '<?php echo $data[$i]->created_at; ?>', '0', '30','<?php echo $likeCount;?>');">
            <span class="text-color-Gray half innerR parent-like-count<?php echo $postId; ?>"><?php echo formatNumberToSort("{$likeCount}", 0); ?></span></a>
            <?php } ?> 
            <label class="pull-right text-color-Gray">Likes</label>
            </div>
          </li>
          <li>
            <div>
              <span class="text-color-Gray half innerR parent-comment-count<?php echo $postId;?>"><?php echo formatNumberToSort("{$parentCommentCount}", 0);?></span>
              <label class="pull-right text-color-Gray">Comments</label>
            </div>
          </li>
          <li>
            <div>
              <span class="text-color-Gray"><?php echo formatNumberToSort("{$parentShareCount}", 0);?></span>
              <label class="text-color-Gray">Shares</label>
            </div>
          </li>
          <!-- <li class="padding-right-none">
            <div>
              <span class="text-color-Gray"><?php //echo formatNumberToSort("{$parentViewCount}", 0);?></span>
              <label class="text-color-Gray">Views</label>
            </div>
          </li> -->
        </ul>
      </div>
    </div>
  </div>
  </div>
</div>

</div>
<!--social-timeline-earning-comments-view-->
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>