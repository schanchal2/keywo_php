<?php
header("Access-Control-Allow-Origin: *");
header('Content-Type: text/html; charset=utf-8');

//check for session
session_start();

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    require_once("../layout/header.php");
    require_once $docRoot."/models/social/socialModel.php";
    require_once "../../models/social/commonFunction.php";
}else{
    header("Location:../../index.php");
    exit();
}



//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}



$email      = $_SESSION["email"];
$id      = $_SESSION["id"];
$userType = '';

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}

$postFields = "short_description,followings,followers,post_count,profile_pic,first_name,last_name,creationTime,account_handle,user_id";
$userDetail = getUserInfo($_SESSION["email"], $walletURLIPnotification . 'api/notify/v2/', $postFields);
if(noError($userDetail)) {
    $userDetail = $userDetail["errMsg"];

    $userCreationTime = $userDetail['creationTime'];

    $userType = getFirstTimeUserType($userCreationTime);

    $getFtUserDetail = getFirstTimeUserStatus($kwdDbConn, $email, $id);

    if (noError($getFtUserDetail)) {
        $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
        $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);
        if ($recCount > 0) {
            if($row['user_follow'] != 0) {
                if($userType == 'ftUser_new'){
                    print("<script>");
                    print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_like.php';\", 000);");
                    print("</script>");
                    exit();
                }else if($userType == 'ftUser_old'){
                    print("<script>");
                    print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_claim.php';\", 000);");
                    print("</script>");
                    exit();
                }
            }

        } else {

            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_profile.php';\", 000);");
            print("</script>");
            exit();
        }
    } else {
        $retArray['errCode'] = 23;
        $retArray['errMsg'] = "Error getting user status";
    }
}else{
    $retArray['errCode'] = 24;
    $retArray['errMsg'] = "Error getting user info";
}

//Get following array
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$getFollowingArray = getFollowUser($targetDirAccountHandler);

//Get suggested user
$getFollowed = array();
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";
$getFollowed         = getFollowUser($targetDirAccountHandler);
//printArr($getFollowed);

$getSuggested = getSuggestedUser($_SESSION['id']);
if(noError($getSuggested)){
    $getSuggested             = $getSuggested["errMsg"];
}
?>



<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_first_time.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<main class="container first_time_follow_container inner-7x innerTB">
	<div class="row">
		<div class="col-xs-12">
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Follow any 5 accounts</div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
				<div class="card social-card right-panel-modules">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
								<h4 class="half innerMTB">Task List</h4>
							</div>
						</div>
					</div>
                    <!-- Ftue task list suggested module -->
                    <?php require_once "ftue_task_list.php"; ?>
                    <!-- Ftue task list suggested module ends-->
				</div>
			</div>
		</div>

        <div id="followingCount" class="hidden"><?php echo $userDetail['followings']; ?></div>

		<div class="col-xs-6">
            <?php         if($getSuggested['errCode'] != 4) { ?>
			<form action="">
				<div class="progress">
					<div class="progress-bar" role="progressbar" style="width: 40%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
				</div>
                <input type="hidden" id="page" current="follow" next="<?php echo (($userType == 'ftUser_new')? 'like' : 'claim'); ?>">
				<ul class="list-unstyled activity-log__list">

                    <?php

                        foreach ($getSuggested as $key => $getSuggestedDetails) {
                            if ($key > 9) {
                                break;
                            }
                            if(isset($getSuggestedDetails["profile_pic"]) && !empty($getSuggestedDetails["profile_pic"])) {
                                global $cdnSocialUrl;
                                global $rootUrlImages;

                                $extensionUP = pathinfo($getSuggestedDetails["profile_pic"], PATHINFO_EXTENSION);
                                //CDN image path
                                $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $getSuggestedDetails["account_handle"] . '/profile/' . $getSuggestedDetails["account_handle"] . '_' . $getSuggestedDetails["profile_pic"] . '.40x40.' . $extensionUP;

                                //server image path
                                $imageFileOfLocalUP = $rootUrlImages . 'social/users/' . $getSuggestedDetails["account_handle"] . '/profile/' . $getSuggestedDetails["account_handle"] . '_' . $getSuggestedDetails["profile_pic"];

                                // check for image is available on CDN
                                $file = $imageFileOfCDNUP;
                                $file_headers = @get_headers($file);
                                if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                    $imgSrc = $imageFileOfLocalUP;
                                } else {
                                    $imgSrc = $imageFileOfCDNUP;
                                }
                            }
                            ?>
                            <li class="card innerAll border-bottom ">
                                <div class="media">
                                    <div class="media-left">
                                        <div class="first_time_follow-pic">
                                            <img src="<?php echo $imgSrc; ?>" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="media-body f-sz15">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="ellipses margin-none f-wt7 f-sz16"><?php echo "{$getSuggestedDetails['first_name']} {$getSuggestedDetails['last_name']}"; ?></div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div class="ellipses margin-none text-light-grey"><?php echo "@{$getSuggestedDetails['account_handle']}"; ?></div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php

                                            if (in_array($getSuggestedDetails['user_id'], $getFollowed)) {
                                                $followFlag = true;
                                            } else {
                                                $followFlag = false;
                                            }
                                            ?>
                                            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                                                <input type="button"
                                                       id='<?php echo $getSuggestedDetails["account_handle"]; ?>'
                                                       class="btn-social<?php if ($followFlag) {
                                                           echo "-dark";
                                                       } ?> pull-right follow-unfollow-button"
                                                       onclick="ajaxAppFollowEventForFTU('<?php echo $getSuggestedDetails["account_handle"]; ?>','<?php echo $getSuggestedDetails["email"]; ?>');"
                                                       value='<?php if ($followFlag) {
                                                           echo "Unfollow";
                                                       } else {
                                                           echo "Follow";
                                                       } ?>'>
                                            </div>

                                        </div>
                                        <!--
                                        -->
                                    </div>
                                </div>
                            </li>

                        <?php }
                    ?>
				</ul>

				<div class="row innerMT inner-2x">
					<div class="col-xs-12">
                        <button type="submit" class="btn-social-wid-auto btn-xs pull-right innerLR" id="ft_next" value="Next">Next</button>
					</div>
				</div>
			</div>
		</form>
        <?php }else{ ?>
            <div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">No Suggested User Found.... Please Try after some time</div>
      <?php   } ?>
	</div>
</div>
</main>
<?php include('../layout/social_footer.php');

echo "<script> var email = '".$email."'; var handle = '".$handle."'; var session_id = '".session_id()."'; </script>";
?>
<script src=<?php echo "{$rootUrlJs}ft-user.js"; ?> >

</script>
