<?php

session_start();

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');


//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    require_once("../layout/header.php");
}else{
    header("Location:../../index.php");
    exit();
}

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)) {
    $kwdDbConn = $kwdDbConn["connection"];
}

$email   = $_SESSION["email"];
$id      = $_SESSION["id"];

$currDate = date("Y-m-d");
$startDate = date('Y', strtotime("$currDate -13 year"));
$endDate = date('Y', strtotime("$currDate -70 year"));

$startYear = date('Y/m/d', strtotime("$currDate -13 year"));
$endYear = date('Y/m/d', strtotime("$currDate -70 year"));

$userPendingInteraction = "creationTime,no_of_qualified_interactions_pending,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic,trend_preference";
$getUserInteractionDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userPendingInteraction);

$userCreationTime = $getUserInteractionDetails['errMsg']['creationTime'];

$userType = getFirstTimeUserType($userCreationTime);

$getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$email,$id);

if(noError($getFtUserDetail))
{ ?>
    <link rel="stylesheet" href="
    <?php echo $rootUrlCss; ?>app_first_time.css
    <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<?php
    $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
    $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);
    if($recCount > 0){
        if($userType == 'ftUser_new'){
            if($row['user_follow'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_follow.php';\", 000);");
                print("</script>");
                exit();
            }else if($row['like_status'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_like.php';\", 000);");
                print("</script>");
                exit();
            }else if($row['post_status'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_create.php';\", 000);");
                print("</script>");
                exit();
            }else if($row['share_post'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_share.php';\", 000);");
                print("</script>");
                exit();
            }else{
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/dashboard/index.php';\", 000);");
                print("</script>");
                exit();
            }
        }else{
            if($row['account_handle'] == NULL || $row['account_handle'] == ""){
                /*print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_profile.php';\", 000);");
                print("</script>");
                exit();*/
            }else if($row['user_follow'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_follow.php';\", 000);");
                print("</script>");
                exit();
            }else if($row['kwd_claim'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/user/ft_claim.php';\", 000);");
                print("</script>");
                exit();
            }else{
                print("<script>");
                print("var t = setTimeout(\"window.location='".$rootUrl."views/dashboard/index.php';\", 000);");
                print("</script>");
                exit();
            }
        }

    }
}else{
    $retArray['errCode'] =  22;
    $retArray['errMsg'] = "Error getting user status";
}

?>
<main class="container first_time_container inner-7x innerT">
        <div class="row">
            <div class="col-xs-12">
                <div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Welcome to Keywo</div>
            </div>
        </div>
        <div class="innerT inner-2x"></div>
        <div class="row">
            <div class="col-xs-3">
                <div class="suggested-card">
                    <div class="card social-card right-panel-modules">
                        <div class="bg-light-gray right-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="half innerMTB">Task List</h4>
                                </div>
                            </div>
                        </div>
                        <!-- Ftue task list suggested module -->
                        <?php require_once "ftue_task_list.php"; ?>
                        <!-- Ftue task list suggested module ends-->
                    </div>
                </div>
            </div>
            <div class="col-xs-6" id="ft_userForm">
                <form id="ft_userUpdateForm" enctype="multipart/form-data" class="innerB">
                    <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                    <input type="hidden" id="page" current="profile" next="follow" >
                    <div class="social-card card innerB inner-2x">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="text-dark-blue text-center f-wt7">Update Your Profile</h4>
                            </div>
                        </div>
                        <div class="row innerMT inner-4x">
                            <div class="col-xs-12">
                                <div class="first_time_profile-pic"></div>
                                <div class="something_aboutyou innerMT inner-3x form-group" id="hideglyp_onempty">
                                    <input type="text" class="form-control first_time_input" id = "ft_shortDesc" name = "ft_shortDesc" placeholder="Say something about yourself">
                                    <div class="innerAll">
                                        <i class="fa fa-upload text-dark-blue f-sz16" aria-hidden="true"></i>
                                        <input type="file" class="hidden pic-upload" name="fileUpload" id="fileUpload">
                                        <span class="upload-pic">Upload Pic</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row innerMT inner-4x" id="ft_profile_tooltip">
                            <div class="col-xs-12">
                                <?php if ($userType == 'ftUser_old'){ ?>
                                <div class="form-group">
                                    <input type="text" class="form-control first_time_input innerMB inner-2x" name="accountHandle" id="accountHandle" placeholder="Handle">
                                </div>
                                <?php } ?>
                                <div class="form-group relative" id="dobth">
                                    <input type="text" class="form-control first_time_input innerMB inner-2x" readonly='true' id="datepicker_dob" name="datepicker_dob" placeholder="Date of Birth">
                                    <i class="fa fa-calendar text-dark-blue calendar" aria-hidden="true"></i>
                                </div>
                                <div class="form-group">
                                <!-- <input type="text" class="form-control first_time_input innerMB inner-2x" name="ft_city" id="ft_city" placeholder="City"> -->
                                <!--=============================================================================
                                =            code copied from views/social/user_social/myprofile.php            =
                                ==============================================================================-->
                                <?php
                                $getCountry = file_get_contents($rootUrl."controllers/widget/getCountries.php");
                                $countryJson = json_decode($getCountry, TRUE);
                                $count = count($countryJson['country']);
                                ?>
                                <select class="form-control first_time_input innerMB inner-2x padding-top-none padding-bottom-none chosen-select-deselect padng-l-7" data-placeholder="Select Country" id="ft_country" name="ft_country" onChange = "populateState()">
                                    <option value="" disabled selected = 'selected'>Select Country</option>
                                        <?php for($i=0; $i<$count; $i++){
                                        $cid = $countryJson['country'][$i]['country_id'];
                                        $countryCodeNumber = $countryJson['country'][$i]['country_code_number'];
                                        /*$sendIdAndCode = $cid."~~".$countryCodeNumber ;*/
                                        ?>
                                        <option value="<?php echo $cid; ?>"><?php echo $countryJson['country'][$i]['name']; ?></option><?php } ?>
                                    </select>
                                <!--====  End of code copied from views/social/user_social/myprofile.php  ====-->
                                </div>
                                <div class="form-group hidden">
                                    <select name = "ft_state" id = "ft_state" name="ft_state" class="countrySelect form-control first_time_input innerMB inner-2x padding-top-none padding-bottom-none  select-input chosen-select-deselect padng-l-7" data-placeholder="Select State" onChange = "populateCity()" >
                                        <option disabled selected = 'selected' value=""></option>
                                    </select>
                                </div>
                                <div class="form-group hide">
                                    <select name = "ft_city" id = "ft_city" name="ft_city" class="countrySelect form-control first_time_input innerMB inner-2x padding-top-none padding-bottom-none  select-input chosen-select-deselect padng-l-7">
                                        <option disabled selected = 'selected' value = ""></option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control first_time_input innerMB inner-2x" name="ft_interest" id="ft_interest" placeholder="Interest">
                                </div>
                                <div class="form-group">
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="male" value="male" checked>
                                        <label for="male">
                                            Male
                                        </label>
                                    </div>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="female" value="female">
                                        <label for="female">
                                            Female
                                        </label>
                                    </div>
                                    <div class="radio radio-primary radio-div">
                                        <input type="radio" name="gender" id="other" value="other">
                                        <label for="other">
                                            Other
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row innerMT inner-2x">
                        <div class="col-xs-12">
                            <button type="submit" class="btn-social-wid-auto btn-xs pull-right innerLR" id="ft_next" value="Next" >Next</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </main>
<div id="image-upload-confirmation-dialog" class="modal fade in keyword-popup keyword-markt-popup-common" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content row">
            <div class="modal-header custom-modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div class="">
                        <div class="row">
                            <div class="col-md-12 innerMB text-center">
                                <span class="text-black" id="invalidImageResponse">Please upload valid image</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 innerMB">
                                <div class="text-left">
                                    <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                </div>
                            </div>
                            <div class="col-md-6 innerMB">
                                <div class="text-left pull-right">
                                    <!-- <input value="Yes" type="button" id="buy-yes-btn" class="btn-trading-wid-auto-dark innerMTB" >&nbsp;&nbsp;-->
                                    <input value="OK" type="button" id="buy-no-btn" class="btn-trading-wid-auto innerMTB" onclick="cancel();">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?/* } */?>
<?php include('../layout/social_footer.php');

echo "<script> var startDate = '".$startDate."'; var endDate = '".$endDate."'; var session_id = '".session_id()."'; var startYear = '".$startYear."'; var endYear = '".$endYear."'; </script>";

?>
<script src=<?php echo "{$rootUrlJs}ft-user.js"; ?> ></script>
<script>

    var userType = '<?php echo $userType; ?>';

    $(document).ready(function(){
        $("#form_prelogin_login_username").focus();
      /*  $('#ft_next').on('click',function(){

            $(".ft_userForm input").each(function(){
                if($(this).val() == ""){
                    $(this).parent().attr("class", "has-error has-feedback chosen-search");
                    $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                }
            });

        });*/
/*$("#ft_next").on('click', function () {

    if (ft_shortDesc.text === '' || ft_shortDesc.text == null ) {
                // Hide the success icon
              
                $("#hideglyp_onempty [class*=glyphicon ]").hide();
                // $('hideglyp_onempty.glyphicon').hide();
            }
   });*/
});

    $('#ft_userUpdateForm').bootstrapValidator({
        message: 'This value is not valid',
        container: 'tooltip',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        // excluded: [':disabled'],
        fields: {
             ft_shortDesc: {
                validators: {
                    stringLength: {
                        max: 60,
                        message: 'The full name must be less than 60 characters'
                    }
                }
            },
//            accountHandle:{
//                validators: {
//                    notEmpty: {
//                        message: 'The account handle is required and can\'t be empty'
//                    }
//                }
//            },
            ft_country: {
                validators: {
                    notEmpty: {
                        message: 'The country is required and can\'t be empty'
                    }
                }
            },
            datepicker_dob: {
                 validators: {

                     notEmpty: {
                         message: 'The date of birth can\'t be empty'
                     }
                 }
            },
        }
    }).on("success.form.bv",function(){

    });


	$(function() {
		$('#datepicker_dob').datepicker({
            // maxDate: 0,
		    dateFormat: 'dd/mm/yy',
             //startDate: startYear,
             //endDate: endYear,
           // yearRange: '1945:2004',
            yearRange: "-70:+0",
		    autoclose: true,
            changeYear: true,
            changeMonth: true,
		    keyboardNavigation : false ,
		    daysOfWeekDisabled : [0],

            onSelect: function(date) {
                var date = date.split('/');
                date = new Date(date[1]+'/'+date[0]+'/'+date[2]);
                var today = new Date();
                var birthDate = new Date(date);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();

                if (age >= 13) {
                 // if (m < 0 || (m === 0 && today.getDate() >= birthDate.getDate())) {
                   /* if ($("#datepicker_dob").hasClass('text-dark-red')) {
                          $("#datepicker_dob").removeClass('text-dark-red').addClass("text-dark-gray");
                          $("#dobth [class*=bv-no-label ]").addClass('glyphicon-ok');
                          $("#dobth [class*=bv-no-label ]").removeClass('glyphicon-remove');
                          $("#dobth").removeClass('has-error');
                          $("#dobth").addClass('has-success');
                    } else {
                          $("#datepicker_dob").addClass("text-dark-gray");
                          $("#dobth [class*=bv-no-label ]").addClass('glyphicon-ok');
                          $("#dobth [class*=bv-no-label ]").removeClass('glyphicon-remove');
                          $("#dobth").removeClass('has-error');
                          $("#dobth").addClass('has-success');
                    }*/

                    $("#datepicker_dob").addClass("text-dark-gray");
                    $("#dobth [class*=bv-no-label]").addClass('glyphicon-ok');
                    $("#dobth [class*=bv-no-label]").removeClass('glyphicon-remove');
                    $("#dobth").removeClass('has-error');
                    $("#dobth").addClass('has-success');
                 /* } else {

                    $("#datepicker_dob").addClass("text-dark-red");

                  }*/
                } else {
//                  $("#datepicker_dob").addClass("text-dark-red");
                  $("#dobth [class*=bv-no-label ]").removeClass('glyphicon-ok');
                  $("#dobth [class*=bv-no-label ]").addClass('glyphicon-remove');
                  $("#dobth").addClass('has-error');
                  $("#dobth").removeClass('has-success');

                }

                // if (f  13.00789) {
                //     /* Not eligible for update profile */
                //     if ($("#datepicker_dob").hasClass('text-dark-red')) {
                //         $("#datepicker_dob").removeClass('text-dark-red').addClass("text-dark-gray");

                //     } else {
                //         $("#datepicker_dob").addClass("text-dark-gray");
                //     }

                // } else {

                //     $("#datepicker_dob").addClass("text-dark-red");

                // }
            }
		});
	});


	$(".upload-pic").click(function(){
		$(".pic-upload").trigger('click');
	})

    /*======================================================================================
    =            to add selected profile picture preview before sending to server            =
    ======================================================================================*/

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                // $('#uploadImg + img').remove();
                /*$('#myImage ').remove();
                $('#uploadImg').after('<img id="myImage" class="img-responsive profile-detail-img" src="' + e.target.result + '" width="233" height="200"/>');*/
                $('.first_time_profile-pic').css({"background-image": "url('"+e.target.result+"')", "background-size": "100% 100%"});
            }
            reader.readAsDataURL(input.files[0]);
        }
    }


    $("#fileUpload").change(function () {

        var file = this.files[0];
        var fileType = file["type"];
        //var imageName = file["name"];

        var validImageTypes = ["image/jpg", "image/jpeg", "image/png"];

        if ($.inArray(fileType, validImageTypes) < 0) {
            $('#invalidImageResponse').html('Please upload valid image of jpg, jpeg and png format.');
            $('#image-upload-confirmation-dialog').modal('show');
        } else {
            /*$('#get-profile-pic-name').val(imageName);*/
            filePreview(this);
        }
    });

    /*=====  End of to add seleted profile picture previw before sending to server  ======*/




    /*=====  End of to add seleted profile picture previw before sending to server  ======*/

</script>
