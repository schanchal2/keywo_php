<?php
		include("../layout/header.php");
		error_reporting(0);

		if(isset($_POST) && !empty($_POST)){
				if($_POST["errCode"] == 4 || $_POST["errCode"] == 11){
						$errMessage = "Link is already used / modified.";
				}
				if($_POST["errCode"] == 10){
						$errMessage = "Oops, that link has expired.";
				}
				if($_POST["errCode"] == 198){
						$errMessage = $_POST["errMsg"];
				}
				if($_POST["errCode"] == 197){
						$errMessage = $_POST["errMsg"];
				}
				if($_POST["errCode"] == 196){
						$errMessage = $_POST["errMsg"];
				}
				if($_POST["errCode"] == 195){
						$errMessage = $_POST["errMsg"];
				}
				if($_POST["errCode"] == 52){
						$errMessage = "Server Error, Please contact to keywo.";
				}if($_POST["errCode"] == 3){
						$errMessage = "Authentication denied due to reuse/modified resent verification link.";
				}
		}
?>
	<main>
		<!-- Main div start -->
		<div class="main-div">
			<!-- Forgot container -->
			<div class="container forgot-container">
				<div class="row">
					<div class="col-xs-12">
						<div class="col-xs-3">
						</div>
						<!-- Main forgot password start-->
						<div class="col-xs-7 main-forget-pass padding-none">
								<div class="main-head innerAll">
									<!-- Forgot password Label -->
									<strong><span class="activation-text-header">Resend Activation Link</span></strong>
									<!-- Forgot password Label End -->
								</div>
								<hr class="header-margin">

								<!-- main description -->
								<div class="main-desc">
									Please enter the email id you provided during registration process. In order to receive your access code by email.
								</div>
								<!-- main description -->
								<form>
									<div class="row flex-box">
										<div class="col-xs-4 text-right padding-none">
											<label class="email-label innerR" for="exampleInputEmail2">Enter Your Email-Id:    <span class="text-red">*</span>
											</label>
										</div>
										<div class="col-xs-5">
											<div class="forgot-email-icons">
	                  						<input type="email" id="activateEmail" class=" form-control forgot-email-input">
                  							<i class="fa fa-envelope" aria-hidden="true"></i>
											</div>
										</div>
										<div class="col-xs-3 padding-none">
											<button type="submit" id="resendActivationEmail" class="btn btn-xs btn_Act_Link text-center" value="">Submit</button>
										</div>
									</div>
									<div id="erroremailshow" class="text-red col-xs-offset-3 innerL inner-2x innerMTB"><?php echo $errMessage; ?></div>
									</div>
								</form>

								
						</div>
						<!-- Main forgot password End here-->
						<div class="col-xs-2">
						</div>
					</div>
				</div>
			</div>
			<!-- Forgot container end -->
		</div>
		<!-- Main div End here -->
	</main>
<?php include('../layout/transparent_footer.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
	  $("#activateEmail").focus();
    });
</script>