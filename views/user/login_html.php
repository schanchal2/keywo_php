<?php include("../layout/header.php"); ?>
<main class="login-page">
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-offset-2">
                <!-- 

              -->
                <div class="panel no-radius border-color-Darkest-Gray margin-none f-sz15">
                    <div class="panel-heading no-radius text-color-Username-Link f-sz18 border-bottom l-h10  innerAll">
                        <p class="leadCorrect-b innerT"> Create Campaigns </p>
                    </div>
                    <form class="form-horizontal">
                        <!--  -->
                        <div class="panel-body innerAll inner-2x">
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3">
                                    <div class="form-group innerMB">
                                        <label for="inputEmail3" class="col-sm-4 control-label">Login ID</label>
                                        <div class="col-sm-8">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group margin-bottom-none">
                                        <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
                                        <div class="col-sm-8">
                                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                        <div class="panel-footer no-radius bg-White clearfix innerAll padding-top-none l-h10 border-none">
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3">
                                    <div class="form-group innerMB">
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" class="btn btn-default">Log in</button> or
                                            <button type="submit" class="btn btn-default">Sign up</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-xs-offset-3">
                                    <div class="form-group margin-bottom-none">
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" class="btn btn-link">Resend Activation Link</button> |
                                            <button type="submit" class="btn btn-link">Forget Password ?</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </form>
                </div>
            </div>
            <!--  -->
        </div>
        <!-- 

               -->
    </div>
</main>
<?php include('../layout/transparent_footer.php'); ?>
<script type="text/javascript">
$(document).ready(function() {
    $("#form_login_username").focus();
});
</script>
