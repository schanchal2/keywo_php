<?php
// Include model file
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../models/user/authenticationModel.php');
require_once('../../helpers/coreFunctions.php');
include("../layout/header.php");
// getting url values using $_GET variable.
if(isset($_GET) && !empty($_GET)){
  $auth   = $_GET["auth"];
  $email  = urldecode($_GET["email"]);
  $flag   = urldecode($_GET["flag"]);
  $timestamp = $_GET["time"];
}
$errMessage = '';
$tokenArray = array("email" => $email, "auth" => $auth, "timestamp" => $timestamp);
// call function to verify the authentication link whcih is definded in models/user/authenticationModel.php file.
$verifyResetLink = verifyResetPswdAuthLink($tokenArray);
if(!noError($verifyResetLink)){
    if(isset($verifyResetLink["errMsg"]) && !empty($verifyResetLink["errMsg"])){
          $errMessage = $verifyResetLink["errMsg"];
    }
}
?>
<main class="bg-concrete inner-9x innerT forgot-password-page">
<div class="container inner-9x innerT">
  <div class="row">
    <div class="col-xs-8 padding-none col-xs-offset-2 card">
      <div class="row margin-none forgot-password-text ">
        <h4 class="pull-left text-primary-blue innerMTB innerL">
        Reset Your Account Password
        </h4>
      </div>
      <form class=" innerAll">
        <div class="row">
          <div class="col-xs-6 col-xs-offset-3">
            <div class="form-group password-field forgot-email-icons">
              <input type="password" id="newPassword" class="passwordValidation password-field form-control" placeholder=" "/>
              <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
          </div>
        </div>
        <div class="row ">
          <div class="col-xs-6 col-xs-offset-3 ">
            <div class="form-group password-field forgot-email-icons">
              <input type="password" id="newConfirmPassword" class="passwordIdenticalValidation password-field form-control"  placeholder=" "/>
              <i class="fa fa-lock" aria-hidden="true"></i>
            </div>
          </div>
          <input type="hidden" id="forgetEmail" name="email" value="<?php echo $email; ?>" />
          <input type="hidden" id="forgetAuth" name="auth" value="<?php echo $auth; ?>" />
          <input type="hidden" id="currentTime" name="time" value="<?php echo $timestamp; ?>" />
        </div>
        <div class="row">
          <div class="col-xs-4 col-xs-offset-4">
            <input type="button" id="resetPassword" class="btn proper-reset-btn login-signbutton reset-login " value="Reset Password"/>
            <input type="button" class="login-signbutton btn proper-cancel-btn innerTB innerR" onclick="clearPassword();" value="Clear"/>
          </div>
          <div id="erroremailshow" class="text-red"> <?php echo $errMessage; ?></div>
        </div>
      </form>
    </div>
  </div>
</div>
</main>
<?php include('../layout/transparent_footer.php'); ?>
<script type="text/javascript">
$(document).ready(function(){
  $("#newPassword").focus();
});
</script>