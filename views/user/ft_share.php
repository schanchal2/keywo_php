<?php
session_start();
//check for session

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    require_once("../layout/header.php");
    require_once "../../models/social/socialModel.php";
    require_once('../../models/social/commonFunction.php');
}else{
    header("Location:../../index.php");
    exit();
}


//check session & redirect to login page
/*if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}*/

//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}


$email      = $_SESSION["email"];
$id      = $_SESSION["id"];

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}

$getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$email,$id);

if(noError($getFtUserDetail)) {
    $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
    $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);
    if ($recCount > 0) {
        if ($row['share_post'] != 0) {
            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/dashboard/index.php';\", 000);");
            print("</script>");
            exit();
        }else if($row['user_follow'] == 0){
            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_follow.php';\", 000);");
            print("</script>");
            exit();
        }
    } else {
        print("<script>");
        print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_profile.php';\", 000);");
        print("</script>");
        exit();
    }
}else{
    $retArray['errCode'] = 23;
    $retArray['errMsg'] = "Error getting user status";
}


$bulkData                = array();
$userId                  = $_SESSION['id'];

$postId                  = 2111;
$postTime                = 1496844569954;
$email                   = "001@grr.la";
$userSearchType          = "image";

//$postId                  = cleanXSS(trim(urldecode($_POST['postId'])));
//$postTime                = cleanXSS(trim(urldecode($_POST['postTime'])));
$searchCount             = cleanXSS(trim(urldecode($_POST['searchCount'])));
$ipAdd                   = cleanXSS(trim(urldecode($_POST['ipAddress'])));
//$email                   = cleanXSS(trim(urldecode($_POST['email'])));
//$userSearchType          = cleanXSS(trim(urldecode($_POST['userSearchType'])));
$emailofSpecialUser      = array();
$targetDirAccountHandler = "../../json_directory/social/followerwonk/".$_SESSION["account_handle"]."/";

$user_id     ="user_id,account_handle";
$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $user_id);
if(noError($getUserInfo)){
    $getUserInfo = $getUserInfo["errMsg"];
    $user_id     = $getUserInfo["user_id"];
    $accHandle   = $getUserInfo["account_handle"];
}
//Get Special User
//$getSpecialUserData = getSpecialUserPost();
//if(noError($getSpecialUserData)){
//    $getSpecialUserData = $getSpecialUserData["errMsg"];
//}
//foreach($getSpecialUserData as $specialUser){
//    $emailofSpecialUser[] = $specialUser['email'];
//}

if(!in_array($email,$emailofSpecialUser)){
    //get Folllowed User
    $getFollowed         = getFollowUser($targetDirAccountHandler);

    //check for value
    $followFlag          = getFollowUserValue($getFollowed,$user_id,$_SESSION['id']);

}else{
    $followFlag = 1;
}

$postDetail          = getPostDetail($postId, $postTime,$_SESSION['id'],$followFlag);
// printArr($postDetail);

//Check account is blocked is not
if($postDetail['errCode'] == 4) { ?>
    <script>$('.close-button').hide();</script>
    <?php
    print("<script>");
    print("var t = setTimeout(\"postDeletedModal('This Post No longer Exist'); postLoadAsPerUrl(); \", 000);");
    print("</script>");
    exit;	}else{ ?>
    <script>$('.close-button').show();</script>
<?php }

if (noError($postDetail)) {
    $postDetail = $postDetail['errMsg'][0];
    $accountHandle = $postDetail['user_ref']['account_handle'];
    $emailId = $postDetail['user_ref']['email'];
    $postType = $postDetail['post_type'];
    $postedBy = $postDetail['posted_by'];
    $postCollectionName = $postDetail['post_collection'][0];
    $filename = $postDetail['post_details']['img_file_name'];
    $createdAt = $postDetail['created_at'];
    $emailId = $postDetail["user_ref"]["email"];
    $mil = $postDetail['created_at'];
    $seconds = $mil / 1000;
    $created_at = date("d-m-Y H:i:s", $seconds);
    $timestamp2 = strtotime($created_at);
    $ext = explode('.', $filename);
    $postViewCountDisplay = $postDetail['post_views_count'];
    $post_earnings= $postDetail['post_earnings'];

// varibales of shared post content
    $postSharedParent = $postDetail["post_details"]["parent_post"]["post_id"];
// printArr($postSharedParent);
    $postSharedParentId = $postSharedParent["_id"];
    $postSharedParentFname = $postSharedParent["user_ref"]["first_name"];
    $postSharedParentLname = $postSharedParent["user_ref"]["last_name"];
    $postSharedParentUserId = $postSharedParent["user_ref"]["user_id"];
    $postSharedParentAccHandle = $postSharedParent["user_ref"]["account_handle"];
    $postSharedParentEmail = $postSharedParent["user_ref"]["email"];
    $postSharedParentPostedBy = $postSharedParent["posted_by"];
    $postSharedCreatedAt = $postSharedParent["created_at"];
    $postSharedKeyword = $postSharedParent["keywords"][0];
    $postSharedKeywords = $postSharedParent["keywords"];
    $postSharedShortDesc = $postSharedParent["post_short_desc"];
    $postSharedType = $postSharedParent["post_type"];
    $postSharedAssetUrl = $postSharedParent["post_details"]["asset_url"];
    $postSharedBlogTitle = $postSharedParent["post_details"]["blog_title"];
    $postSharedImgUrl = $postSharedParent["post_details"]["img_file_name"];
    $postSharedBlogContent = $postSharedParent["post_details"]["blog_content"];

    if(isset($postDetail["user_ref"]["profile_pic"]) && !empty($postDetail["user_ref"]["profile_pic"])){
        global $cdnSocialUrl;
        global $rootUrlImages;

        $extensionUP  = pathinfo($postDetail["user_ref"]["profile_pic"], PATHINFO_EXTENSION);
        //CDN image path
        $imageFileOfCDNUP = $cdnSocialUrl . 'users/' . $accountHandle . '/profile/' . $accountHandle . '_' . $postDetail["user_ref"]["profile_pic"] . '.40x40.' . $extensionUP;

        //server image path
        $imageFileOfLocalUP = $rootUrlImages.'social/users/'.$accountHandle.'/profile/'.$accountHandle.'_'.$postDetail["user_ref"]["profile_pic"];

        // check for image is available on CDN
        $file = $imageFileOfCDNUP;
        $file_headers = @get_headers($file);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            $imgSrc = $imageFileOfLocalUP;
        } else {
            $imgSrc = $imageFileOfCDNUP;
        }
    }else{
        $imgSrc = $rootUrlImages."default_profile.jpg";
    }

//get list of keywords
    if ($postType == 'share') {
        $keywords = $postSharedParent['keywords'];
    } else {
        $keywords = $postDetail["keywords"];
    }
    $keywordsList = implode(' ', $keywords);

//get user ip address
    $ipAddr = getClientIP();

//post view count on display popup
    $postViewCount = postViewCount($postId, $postTime);
//printArr($postViewCount);

//check cdp status
    //$getPostCdpStatus = getPostCdpStatus($_SESSION['account_handle'], $postId);
//Check Like post details
    $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $postType);

    $postIddata = array(array(
        'post_id' => $postId,
        'time' => $postTime
    ));

    $bulkPostId = json_encode($postIddata);
//Get like post activity count from node database
    $activityCount = getPostActivityCount($bulkPostId, '');

    $targetActivityCount = $activityCount['errMsg'][$postId];
    $like_count = $targetActivityCount['like_count'];
    $commentCount = $targetActivityCount['comment_count'];
    $shareCount = $targetActivityCount['share_count'];
    $parentLikeCount = $targetActivityCount['parent_like_count'];
    $parentCommentCount = $targetActivityCount['parent_comment_count'];
    $parentShareCount = $targetActivityCount['parent_share_count'];
    $parentViewCount = $targetActivityCount['parent_view_count'];


//path for json directory
    $targetDirAccountHandler = "../../json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/";

// Get Blocked User
    $blockFlag = false;
    $getBlockDetails = $targetDirAccountHandler . $_SESSION["account_handle"] . "_info_" . strtolower($accountHandle[0]) . ".json";
    if (file_exists($getBlockDetails)) {
        $jsondataBlock = file_get_contents($getBlockDetails);
        $dataBlock = json_decode($jsondataBlock, true);
        if (in_array($postedBy, $dataBlock["blocked"])) {
            $blockFlag = true;
        }
    }

// $postSharedParenEmail
    $postCreatorId = $postSharedParent["posted_by"];
//  printArr($postCreatorId);

    $targetDirAccountHandler = "../../json_directory/social/followerwonk/" . $_SESSION["account_handle"] . "/";
    $userIdsOfAccHandler = getBlockedUser($targetDirAccountHandler);
    if (in_array($postCreatorId, $userIdsOfAccHandler)) {
        $blockFlagForShare = '1';
    } else {
        $blockFlagForShare = '0';
    }
//Check account is blocked is not
    if ($blockFlag == 1 || $blockFlagForShare == '1') { ?>
        <script>$('.close-button').hide();</script>
        <?php
        print("<script>");
        print("var t = setTimeout(\"postDeletedModal('This accout is  blocked!!!!'); postLoadAsPerUrl(); \", 000);");
        print("</script>");
        exit;
    } else { ?>
        <script>$('.close-button').show();</script>
    <?php }

//path for json directory
    $targetDirAccountHandlerOFOther = "../../json_directory/social/followerwonk/" . $accountHandle . "/";
// Get Blocked User
    $blockFlag = false;
    $getBlockDetailsOfOther = $targetDirAccountHandlerOFOther . $accountHandle . "_info_" . strtolower($_SESSION["account_handle"][0]) . ".json";
    if (file_exists($getBlockDetailsOfOther)) {
        $jsondataBlockDetailsOfOther = file_get_contents($getBlockDetailsOfOther);
        $dataBlockOfOther = json_decode($jsondataBlockDetailsOfOther, true);
        if (in_array($_SESSION["id"], $dataBlockOfOther["blocked"])) {
            $blockFlagOfOther = true;
        }
    }

//Check account is blocked is not Of Other
    if ($blockFlagOfOther == 1) {
        echo '<script type="text/javascript">
				window.location = "not_found_error.php"
				</script>';
        exit;
    } else { ?>
        <script>$('.close-button').show();</script>
    <?php }
//Get bookmark Details
    $getBookMarkPost = getBookMarkPost($_SESSION["account_handle"], $postId, $postType);
}

?>
<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_first_time.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<main class="container first_time_like_container inner-7x innerTB">
	<div class="row">
		<div class="col-xs-12">
            <?php

                $getKwdAdminSetting = getAdminSettingsFromKeywordAdmin($kwdDbConn);
                if(noError($getKwdAdminSetting)){
                    $getKwdAdminSetting = $getKwdAdminSetting["data"];
                    $postSharerPercent =  $getKwdAdminSetting["post_share_percent"];
                }else{
                    $postSharerPercent = 0;
                }
            ?>
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7" id='notify'><?php echo "Share this post and earn {$postSharerPercent}% of post earning when anyone like this post"; ?></div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
				<div class="card social-card right-panel-modules">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
								<h4 class="half innerMTB">Task List</h4>
							</div>
						</div>
					</div>
                    <!-- Ftue task list suggested module -->
                    <?php require_once "ftue_task_list.php"; ?>
                    <!-- Ftue task list suggested module ends-->
				</div>
			</div>
		</div>
		<div class="col-xs-6">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <input type="hidden" id="page" current="share" next="finish">
            <div id="pageContent" class="row-10">
                <div class="social-card card innerLR  innerT">
                <div class="social-timeline-image innerMB">
                    <div class="half innerAll" id="social-timeline-image">
                        <div class="">
                            <div class="row half innerTB">
                                <div class="col-xs-8">
                                    <div class="social-timeline-profile-pic pull-left">
                                        <img src="<?php echo $imgSrc; ?>" class=" img-responsive" />
                                    </div>
                                    <div class="social-timeline-profile-details pull-left innerL">
                                        <div class="social-user-link-name">
                                            <a href="#" class="ellipses ellipses-general">
                                                <?php echo $postDetail["user_ref"]["first_name"]; ?> <?php echo $postDetail["user_ref"]["last_name"]; ?>
                                            </a>
                                        </div>
                                        <!---->
                                        <div class="social-user-link">
                                            <a href="#">
                                                <?php echo '@' . $postDetail["user_ref"]["account_handle"]; ?>
                                            </a>
                                        </div>
                                        <!--social-user-link-->
                                    </div>
                                    <!--social-timeline-profile-details-->
                                </div>
                                <div class="col-xs-4 social-post-options">
                                    <div class="pull-right">
                                        <span class="innerLR half text-color-Gray"><i class="fa fa-star-o"></i></span>
                                        <a>
                                            <span class="fa fa-chevron-down"></span>
                                        </a>
                                    </div>
                                    <br>
                                    <div class="pull-right social-post-time-container">
                                        <span class="social-post-time text-light-grey"><span title="<?php  echo  uDateTime("h:i A",$postDetail['created_at'])." - ".uDateTime("d M Y",$postDetail['created_at']);?>"><?php include("../social/getDateFormate.php"); ?></span></span>
                                    </div>
                                </div>
                                </div>
                            </div>
                            <!--row-->
                        </div>

                        <?php
                        if(isset($postDetail["keywords"]) && !empty($postDetail["keywords"])) {
                            $keywords = $postDetail["keywords"];
                        } elseif (isset($postSharedKeyword) && !empty($postSharedKeyword)) {
                            $keywords = $postSharedKeywords;
                        }
                        ?>

                        <!--social-timeline-details-->
                        <div class="social-timeline-keywords-details">
                            <div class="row">
                                <div class="col-xs-12">
                                    <?php
                                    foreach ($keywords as $keywo) {
                                        ?>
                                        <a href="#" class="social-keywords-tags" data-toggle="tooltip" data-placement="top" title="dynamic keyword name">
                                            #<?php echo $keywo ; ?></a>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        <!--social-timeline-keywords-details-->
                        <!--social-timeline-user-message-->
                        <div class="social-timeline-content-message">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p class="innerMB">
                                        <?php
                                        //get short description with mention links
                                        $shortDescriptionText = getLinksOnText(rawurldecode($postDetail["post_short_desc"]), $postDetail["post_mention"], $_SESSION["account_handle"]);
                                        $shortDescription     = $shortDescriptionText["text"];
                                        $counterText          = $shortDescriptionText["counter"];
                                        //printArr($postDetail);die;
                                        if($postType != "blog") {
                                        //     echo $postDetail['post_details']['blog_title'];
                                        // } else {
                                        echo $shortDescription;
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!--social-timeline-tags-details-->
                        <!--  content-image-->
                        <div class="social-timeline-content-image">
                            <div class="row">
                                <div class="col-xs-12 post-display-img">
                                    <a data-toggle="modal" href="#status_image_modal">
                                        <?php

                                        $extension = pathinfo($data[$i]->post_details->img_file_name, PATHINFO_EXTENSION);

                                        //CDN image path
                                        $imageFileOfCDN =  $cdnSocialUrl . "users/" . $postDetail["user_ref"]["account_handle"] . "/post/images/" . $ext[1] . "/" . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"] ;

                                        //server image path
                                        $imageFileOfLocal = $rootUrl . 'images/social/users/' . $postDetail["user_ref"]["account_handle"] . '/post/images/' . $ext[1] . '/' . $postId . '_' . $createdAt . '_' . $postDetail["post_details"]["img_file_name"];

                                        // check for image is avilable on CDN
                                        $file = $imageFileOfCDN;
                                        $file_headers = @get_headers($file);
                                        if($file_headers[0] == 'HTTP/1.1 404 Not Found') {
                                            $finalImagePath = $imageFileOfLocal;
                                        }
                                        else {
                                            $finalImagePath = $imageFileOfCDN;
                                        }

                                        ?>
                                        <img src="<?php echo $finalImagePath ?>" class="img-responsive main-img">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <!--social-timeline-content-image-->
                        <div class="social-timeline-earning-comments-view innerT">
                            <div class="row">
                                <div class="col-xs-4">
                                    <div>
                                        <label>Earning : </label>
                                        <span>
                             <span class="currency" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$post_earnings}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$post_earnings}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></span>
                            </span>
                                    </div>
                                </div>
                                <div class="col-xs-8">
                                    <div class="">
                                        <ul class="list-inline pull-right margin-bottom-none">
                                            <li class="padding-right-none">
                                                <div>
                                                    <?php
                                                  if (!empty($like_count)) { ?>
                                                    <a id="like-count<?php echo $data[$i]->_id; ?>">

                                                        <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span>
                                                    </a>
                                                    <?php } else {
                                                      ?>
                                                    <a id="like-count<?php echo $data[$i]->_id; ?>">
                                                    <span class="half innerR like-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$like_count}", 0); ?></span></a>
                                                    <?php } ?>
                                                    <label>Likes</label>
                                                </div>
                                            </li>
                                            <li>
                                                <div>
                                                    <span class="half innerR comment-count<?php echo $data[$i]->_id; ?>"><?php echo formatNumberToSort("{$commentCount}", 0); ?></span>
                                                    <label class="pull-right">Comments</label>
                                                </div>
                                            </li>
                                            <li class="padding-right-none">
                                                <div>
                                                    <span class="my-share-post-count"><?php echo formatNumberToSort("{$shareCount}", 0); ?></span>
                                                    <label>Shares</label>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--social-timeline-earning-comments-view-->
                        <!--social-timeline-likes-dislikes-->
                        <div class="social-timeline-likes-dislikes">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="social-likes-dislikes">
                                        <ul class="list-inline margin-bottom-none">
                                            <li>
                                                <div>
                                                    <a class="like-area">

                                                    <i id="thumbs-up<?php echo $postId; ?>" class="fa fa-thumbs-o-up half innerR" style="cursor: not-allowed; opacity: 0.6;"></i>


                                                    </a>

                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="social-comments-shares-views">
                                        <ul class="list-inline pull-right margin-bottom-none">
                                            <li>
                                                <div>
                                                    <a class="comment-click">
                                                        <i class="fa fa-comments-o"></i>
                                                        <span>Comments</span>
                                                    </a>
                                                </div>
                                            </li>
                                            <li class="padding-right-none">
                                                <div>
                                                    <a data-toggle="modal" onclick = "sharePostModal('<?php echo $postId; ?>','<?php echo $createdAt; ?>','<?php echo $postType;?>','<?php echo $postDetail["user_ref"]["account_handle"];?>', 'create','<?php echo $postDetail['user_ref']['email']; ?>');">
                                                        <i class="fa fa-share-square-o"></i>
                                                        <span>Share</span>
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--social-card-->
                </div>
                <div class="row innerMT inner-2x">
                <div class="col-xs-12">
                    <button type="submit" class="btn-social-wid-auto btn-xs pull-right innerLR" id="ft_navigator" value="Skip">Skip</button>
                </div>
            </div>
            </div>
		</div>
	</div>
<div id="myModal_like" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <!-- Modal content-->
                    <div class="" id="likeUserList">
                    </div>
                </div>
            </div>
        </div>

        <!--modal to display editing data-->
        <div class="modal fade" id="showModalPost" role="dialog">
            <div class="modal-dialog">
                <div class="card social-card clearfix social-form-modal">
                    <div class="clearfix closebutton-absolute">
                        <i class="fa fa-times close-button" data-dismiss="modal"></i>
                    </div>
                    <div class = "modal-body-social-user-image">

                    </div>
                </div>
            </div>
        </div>

	</main>
	<?php include('../layout/social_footer.php');

    echo "<script> var email = '".$email."'; var handle = '".$handle."'; var session_id = '".session_id()."'; </script>";

	?>
    <script src=<?php echo "{$rootUrlJs}ft-user.js"; ?> ></script>
<script>
    //tooltip
    $('[data-toggle="tooltip"]').tooltip({
        trigger:'hover'
    });
</script>