<?php

    session_start();

    require_once('../../config/config.php');
    require_once('../../config/db_config.php');
    require_once('../../helpers/errorMap.php');
    require_once('../../helpers/coreFunctions.php');
    require_once('../../helpers/sessionHelper.php');

    $val = setcookie( "keywo_ver_1", $_SESSION["email_handle"], time()-3600, "/" ) ;

    $kwdDbConn = createDBConnection('dbkeywords');
    if (noError($kwdDbConn)) {
        $kwdDbConn = $kwdDbConn["connection"];

    }else{
        $returnArr = array();
        $msg = "Failure : Unable to create search database connection";
        $returnArr = setErrorStack($returnArr, 1, $msg, $extraArgs);
    }

    $val = deleteSession($_SESSION["email"], $kwdDbConn);

    //destroy session
    session_destroy();
    $redirectURL = $rootUrl."views/prelogin/index.php";

    print("<script>");
    print("window.location='".$redirectURL."'");
    print("</script>");
    exit;

 ?>
