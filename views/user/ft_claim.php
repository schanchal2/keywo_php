<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 1/6/17
 * Time: 6:06 PM
 */

session_start();
//check for session
$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');


require_once("../../models/keywords/userDashboardModel.php");

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    require_once("../layout/header.php");
}else{
    header("Location:../../index.php");
    exit();
}


//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}


$email      = $_SESSION["email"];
$id      = $_SESSION["id"];

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}

$userPendingInteraction = "creationTime,no_of_qualified_interactions_pending,system_mode,unread_notification_count,currencyPreference,no_of_searches_in_last_hour,last_hour_search_time,profile_pic,trend_preference";
$getUserInteractionDetails = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', $userPendingInteraction);

$userCreationTime = $getUserInteractionDetails['errMsg']['creationTime'];

$userType = getFirstTimeUserType($userCreationTime);

if($userType == 'ftUser_old'){

    $getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$email,$id);

    if(noError($getFtUserDetail)) {
        $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
        $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);
        if ($recCount > 0) {
            if ($row['kwd_claim'] != 0) {
                print("<script>");
                print("var t = setTimeout(\"window.location='" . $rootUrl . "views/dashboard/index.php';\", 000);");
                print("</script>");
                exit();
            }else if($row['user_follow'] == 0){
                print("<script>");
                print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_follow.php';\", 000);");
                print("</script>");
                exit();
            }
        } else {
            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_profile.php';\", 000);");
            print("</script>");
            exit();
        }
    }else{
        $retArray['errCode'] = 23;
        $retArray['errMsg'] = "Error getting user status";
    }

}else if($userType == 'ftUser_new'){
    print("<script>");
    print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_profile.php';\", 000);");
    print("</script>");
    exit();
}

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_first_time.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<?php

$getMyKeywordDetails = getMyKeywordDetails($kwdDbConn,$email);
if(noError($getMyKeywordDetails)) {

    $getMyKeywordDetails = $getMyKeywordDetails["errMsg"];
    $myKeywords = $getMyKeywordDetails["transaction_details"];
    $myKeywords = json_decode($myKeywords, true);
    $myKeywords = array_reverse($myKeywords);
    $kwdCount = count($myKeywords);

    if($kwdCount > 0) {

        ?>
        <main class="container first_time_container inner-7x innerT">
            <div class="row">
                <div class="col-xs-12">
                    <div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7">Welcome to Keywo</div>
                </div>
            </div>
            <div class="innerT inner-2x"></div>
            <div class="row">
                <div class="col-xs-3">
                    <div class="suggested-card">
                        <div class="card social-card right-panel-modules">
                            <div class="bg-light-gray right-panel-modules-head">
                                <div class="row margin-none">
                                    <div class="col-xs-12">
                                        <h4 class="half innerMTB">Task List</h4>
                                    </div>
                                </div>
                            </div>
                            <!-- Ftue task list suggested module -->
                            <?php require_once "ftue_task_list.php"; ?>
                            <!-- Ftue task list suggested module ends-->
                        </div>
                    </div>
                </div>
                <div class="col-xs-6">
                    <form action="">
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" style="width: 100%" aria-valuenow="25"
                                 aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                        <input type="hidden" id="page" current="claim" next="finish">
                        <div class="social-card card innerB inner-1x innerAll">
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="text-dark-blue text-center f-wt7">Claim Your Keywords </h4>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-12 col-xs-12 innerB inner-1x">
                                    <span>As a early supporter of Searchtrade.com, we are offering you free claim to your existing Searchtrade Keywords on Keywo as a token of appreciation <br/>List of keywords you own at Searchtrade. </span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-11 innerB">
                                    <button type="submit" class="btn-social-dark btn-xs pull-left inner-2x innerMR"
                                            id="claim" value="Claim">Claim</button>
                                    <button type="submit" class="btn-social-dark btn-xs pull-left innerLR" id="unclaim"
                                            value="Dont Claim">Dont Claim</button>
                                </div>
                            </div>
                            <div class="row claim-key-max-height">
                                <?php
                                foreach ($myKeywords as $key => $kwdArr) { ?>

                                    <div class="col-xs-4 col-xs-4 innerB inner-1x">
                                        <label for="<?php echo $kwdArr['keyword']; ?>"> <?php echo $kwdArr['keyword']; ?> </label><br>
                                        <!--<label for="Moon"> Moon</label><br>-->
                                    </div>
                                <?php } ?>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </main>

        <?php
    }else{

        $status = updateFirstTimeUserStatus($kwdDbConn, $email, $id, 'claim');
        if(noError($status)){
            $errCode = -1;
            $errMsg = "Success: update first time user success";

            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/dashboard/index.php';\", 000);");
            print("</script>");
            exit();
        }else{
            print("<script>");
            print("alert('Error: update first time user process.')");
            print("</script>");
            exit();
        }

    }
}

?>

<?php include('../layout/social_footer.php');

echo "<script> var email = '".$email."'; var handle = '".$handle."'; var session_id = '".session_id()."'; </script>";

?>
<script>
    /*$(function() {
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            keyboardNavigation : true ,
            daysOfWeekDisabled : [0]
        });
    });
    $(".upload-pic").click(function(){
        $(".pic-upload").trigger('click');
    })*/
</script>

<script src=<?php echo "{$rootUrlJs}ft-user.js"; ?> >

</script>

