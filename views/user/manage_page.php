<?php
require_once ('../../config/config.php');

include("../layout/header.php");

if(isset($_POST) && !empty($_POST)){
    $errCode = $_POST["errCode"];
}

if($errCode == 500){
    $errMsg = $_POST["errMsg"];
}
?>

<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app.css" type="text/css"/>
<main class="inner-7x innerT" style="padding-top:150px;">
    <div class="container">
        <div class="row card not-found-container">
            <div class="col-xs-12">
                <h1 class="text-blue">Warning</h1>
            </div>
            <div class="col-xs-12 text-blue">
                <h4 class="margin-top-none"> <?php echo $errMsg; ?></h4>
            </div>
            <div class="col-xs-12 innerMTB">
                <img src="<?php echo $rootUrlImages?>error_icon.png" alt="">
            </div>
            <div class="col-xs-12 innerB inner-2x">
                <div> <a href="<?php echo $rootUrl ?>views/user/logout.php">Logout Here</a> or <a href="<?php echo $rootUrl ?>views/prelogin/index.php"> Home Page </a></a></div>
            </div>
        </div>
    </div>
</main>
