<?php
/*$docrootpath=explode("views/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];*/

session_start();

require_once ('../../config/config.php');
require_once ('../../config/db_config.php');

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
    require_once("../layout/header.php");
    require_once("../../models/cdp/cdpUtilities.php");
}else{
    header("Location:../../index.php");
    exit();
}


//check session & redirect to login page
if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}

$email      = $_SESSION["email"];
$id      = $_SESSION["id"];
$handle      = $_SESSION['account_handle'];

$kwdDbConn = createDBConnection("dbkeywords");
if(noError($kwdDbConn)){
    $kwdDbConn = $kwdDbConn["connection"];
}

$getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$email,$id);

if(noError($getFtUserDetail)) {
    $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
    $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);
    if ($recCount > 0) {
        if ($row['post_status'] != 0) {
            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_share.php';\", 000);");
            print("</script>");
            exit();
        }else if($row['user_follow'] == 0){
            print("<script>");
            print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_follow.php';\", 000);");
            print("</script>");
            exit();
        }
    } else {
        print("<script>");
        print("var t = setTimeout(\"window.location='" . $rootUrl . "views/user/ft_profile.php';\", 000);");
        print("</script>");
        exit();
    }
}else{
    $retArray['errCode'] = 23;
    $retArray['errMsg'] = "Error getting user status";
}


?>
<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_first_time.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<main class="container first_time_like_container inner-7x innerTB processNext">
	<div class="row">
		<div class="col-xs-12">
            <?php
            // 1. Get current payout amount from payout.json file.
            $currentPayout = getCurrentPayout();
            if(noError($currentPayout)) {
                $currentPayout = $currentPayout["current_payout"];
                $currentPayout = number_format(($currentPayout / 4), 8);
            }else{
                $currentPayout = 0.0000;
            }
            ?>
			<div class="text-center bg-white innerTB half f-sz16 text-dark-blue f-wt7" id="notify">Create a post and earn <?php echo formatNumberToSort("{$currentPayout}", 4)." $keywoDefaultCurrencyName"; ?></div>
		</div>
	</div>
	<div class="innerT inner-2x"></div>
	<div class="row">
		<div class="col-xs-3">
			<div class="suggested-card">
				<div class="card social-card right-panel-modules">
					<div class="bg-light-gray right-panel-modules-head">
						<div class="row margin-none">
							<div class="col-xs-12">
								<h4 class="half innerMTB">Task List</h4>
							</div>
						</div>
					</div>
                    <!-- Ftue task list suggested module -->
                    <?php require_once "ftue_task_list.php"; ?>
                    <!-- Ftue task list suggested module ends-->
				</div>
			</div>
		</div>
		<div class="col-xs-6">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: 80%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <input type="hidden" id="page" current="create" next="share">
            <div id="pageContent" class="row-10">
                <div class="social-card card">
                    <div class="social-search-box social-card innerMB">
                        <div class="card innerAll" id="social-micro-blog-box">
                            <form onSubmit="return false;" name="status-post" id="status-post" class="margin-none">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="form-group margin-none">
                                            <input type="hidden" name="post-type" value="status">
                                            <div class="share-something-compressed share-small">
                                                <div>
                                                    <textarea rows="4" class="ft_create_textarea share-post-box compressed-div form-control checkEmpty can-mention-user inputor" name="modalComment" id="modalComment" placeholder="Share Something..."></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row innerAll clearfix">
                                    <div id="tags" class="tag-text" data-id="image-selected-keywords">

                                        <input type="<?php if (empty($postDetail['_id'])) {echo 'text'; } else {echo 'hidden';}?>" class="tag-input checkEmpty preview-tag-change" maxlength="50" style="outline: none;" placeholder="Add #Keywords(min 1-max 3)" value="" />

                                        <input type="hidden" name="modalKeywords" />
                                    </div>
                                </div>
                                <div class="row innerMT">
                                    <div class="share-textbox-quick-links">
                                        <div class="col-xs-4">
                                            <a class="innerR btn-is-disabled" data-toggle="modal" href="#blogModal">
                                                <i class="fa fa-file-text"></i>
                                            </a>
                                            <a class="innerR btn-is-disabled" data-toggle="modal" href="#videoModal">
                                                <i class="fa fa-play-circle"></i>
                                            </a>
                                            <a data-toggle="modal" href="#imageModal" class="innerR btn-is-disabled">
                                                <i class="fa fa-picture-o"></i>
                                            </a>
                                            <a data-toggle="modal" href="#audioModal" class="innerR btn-is-disabled">
                                                <i class="fa fa-soundcloud"></i>
                                            </a>
                                        </div>
                                        <div class="col-xs-6">
                                            <select class="selectpicker" name="postscope">
                                                <option value="Public">Public</option>
                                                <option value="Followers">Followers</option>
                                                <option value="Only Me">Only Me</option>
                                            </select>
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="button" id="statusPost" class="btn-social-wid-auto btn-xs pull-right" value="Post" onclick="ftueCreateNewPost('status', 'status-post', 'create');" >
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    </div>
                    <div class="row innerMT inner-2x">
                        <div class="col-xs-12">
                            <button type="submit" class="btn-social-wid-auto btn-xs pull-right innerLR" id="ft_navigator" value="Skip">Skip</button>
                    </div>
                </div>
            </div>
		</div>
	</div>
	</main>
    <?php include('../layout/social_footer.php');

    echo "<script> var email = '".$email."'; var handle = '".$handle."'; var session_id = '".session_id()."'; </script>";
    ?>
    <script src=<?php echo "{$rootUrlJs}ft-user.js"; ?> ></script>
