<?php
/* --------------------------------------------------------------------------------------------
*    Verify Activation Link
*---------------------------------------------------------------------------------------------
*   This verify is used to verify user when user clicked on verifaction activation link.
*/

session_start();

ini_set('default_charset', 'utf-8');
header('Content-type: text/html; charset=utf-8');

require_once "../../config/config.php";
require_once('../../config/db_config.php');
require_once("../../models/user/authenticationModel.php");
require_once "../../models/analytics/userRegistration_analytics.php";
require_once "../../models/social/socialModel.php";
require_once "../../models/social/commonFunction.php";
require_once('../../backend_libraries/xmlProcessor/xmlProcessor.php');
require_once "../../helpers/imageFunctions.php";
require_once "../../helpers/deviceHelper.php";
require_once "../../helpers/arrayHelper.php";
require_once "../../helpers/stringHelper.php";
require_once "../../helpers/errorMap.php";
require_once "../../helpers/coreFunctions.php";
require_once "../../models/analytics/domain_analytics.php";

error_reporting(0);

$returnArr = array();
$extraArgs = array();

$urlAuth        =   $_GET["auth"];  // get auth from url
$email          =   $_GET["email"]; // get email id from  url
$dechexTime     =   $_GET["sn_t"];  // get verification sent time which in the form of string i.e decimal to hexadecimal
$flag           =   $_GET["flag"];  // get flag value from url

//for xml writing essential
$xmlProcessor = new xmlProcessor();

$xmlfilename = "user_account_verification.xml";

$xmlArray = initializeXMLLog($email);

$xml_data['request']["data"] = '';
$xml_data['request']["attribute"] = $xmlArray["request"];

$xml_data['step']["data"] = 'User Resend Verification';
$logStorePathUserMgmt = $logPath["userManagement"];


if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
    if($_SESSION["email"] == $email){
        $verifyUser = userVerification($urlAuth, $email, $dechexTime, $flag, $xml_data);
        if($verifyUser["errCode"] == 199){
            $errCode = $verifyUser["errCode"];
            $errMsg = $verifyUser["errMsg"];
            $xml_data = $verifyUser["xml_data"];
            $redirectUrl = $verifyUser["redirectUrl"];
        }else{
            $errCode = $verifyUser["errCode"];
            $errMsg = $verifyUser["errMsg"];
            $xml_data = $verifyUser["xml_data"];
            $redirectUrl = $verifyUser["redirectUrl"];
        }
    }else{
        $errCode = 500;
        $errMsg = "A user is already logged in, Please log out from the current session and then verify.";
        $msg = "Failed : Session already exist";
        $xml_data["step1"]["data"] = '1. ' . $msg;

        $responseArr["errCode"]=$errCode;
        $responseArr["errMsg"]=$msg;
        $xml_data['response']["data"] = "";
        $xml_data['response']["attributes"] = $responseArr;
        $redirectUrl = '../user/manage_page.php';

    }
}else{
    $verifyUser = userVerification($urlAuth, $email, $dechexTime, $flag, $xml_data);
    if($verifyUser["errCode"] == 199){
        $errCode = $verifyUser["errCode"];
        $errMsg = $verifyUser["errMsg"];
        $xml_data = $verifyUser["xml_data"];
        $redirectUrl = $verifyUser["redirectUrl"];
    }else{
        $errCode = $verifyUser["errCode"];
        $errMsg = $verifyUser["errMsg"];
        $xml_data = $verifyUser["xml_data"];
        $redirectUrl = $verifyUser["redirectUrl"];
    }
}




$xmlProcessor->writeXML($xmlfilename, $logStorePathUserMgmt, $xml_data, $xmlArray["activity"]);


function userVerification($urlAuth, $email, $dechexTime, $flag, $xml_data){

        global $walletURLIP, $userRequiredFields;

        $returnArr = array();

        if(isset($urlAuth) && !empty($urlAuth) && isset($email) && !empty($email) && isset($dechexTime) && !empty($dechexTime) && isset($flag) && !empty($flag)){


            // get user_id, password and salt value of user from @wallet server.
            $userRequiredData = $userRequiredFields.",user_id,password,salt,active";
            $requestURL = $walletURLIP . 'api/v3/';

            $getUserDetails = getUserInfo($email, $requestURL, $userRequiredData);
            if(noError($getUserDetails)){

                $msg = "Success : Get user details.";
                $xml_data["step1"]["data"] = '1. ' . $msg;

                $getUserDetails     =   $getUserDetails["errMsg"];
                $accountId          =   $getUserDetails["_id"];
                $password           =   $getUserDetails["password"];
                $salt               =   $getUserDetails["salt"];
                $isAccountActive    =   $getUserDetails["active"];

                if($isAccountActive == 0){
                   // get current time
                    $currentTime = time();

                    // convert time (i.e. converted time from decimal to hexadecimal) to  hexadecimal to decimal;
                    $hexdec = hexdec($dechexTime);

                    // generate auth token
                    $genAuth = sha1($accountId.$hexdec.$password.$salt).$dechexTime.dechex($accountId);

                    // token validation
                    if($urlAuth == $genAuth){

                        $msg = "Success : Token is valid.";
                        $xml_data["step2"]["data"] = '2. ' . $msg;

                        // validate link expiry
                        $timeDiff = $currentTime - $hexdec;

                        if($timeDiff < 3600){

                            $msg = "Success : Verification link is valid.";
                            $xml_data["step3"]["data"] = '3. ' . $msg;

                            $connKeyword = createDBConnection('dbkeywords');

                            if(noError($connKeyword)){
                                $connKeyword = $connKeyword["connection"];

                                $msg = "Success : Database connection.";
                                $xml_data["step4"]["data"] = '4. ' . $msg;

                                $verifyActivationLink = verifyUser($email);

                                if(noError($verifyActivationLink)){

                                    //specail user getter & write in json_directory/socail/followerwonk
                                    $specialUser = getSpecialUser();
                                    $xml_data["step11"]["data"] = "11. {$specialUser}";
                                    $jsonWriteSpecailUser       = createJsonFileSpecial(strtolower($verifyActivationLink["errMsg"]["account_handle"]), $specialUser);
                                    $xml_data["step11"]["data"] = "11. {$jsonWriteSpecailUser}";

                                    $verifyActivationLink = $verifyActivationLink["errMsg"];

                                    $verifyAnalytics = "active_users";
                                    $addResendActivationAnalytic = insert_user_statistics($verifyAnalytics, $connKeyword);

                                    if (noError($addResendActivationAnalytic)) {

                                        // close keyword database connection
                                        mysqli_close($connKeyword);

                                        // create  search database connnection
                                        $searchDbConn = createDBConnection('dbsearch');
                                        if(noError($searchDbConn)){

                                            $searchDbConn = $searchDbConn["connection"];

                                            $getSystemMode = getSettingsFromSearchAdmin($searchDbConn);
                                            if(noError($getSystemMode)){
                                                $getSystemMode = $getSystemMode["data"]["default_system_mode"];


                                                $_SESSION["email"]          = $email;
                                                $_SESSION["account_handle"] = $verifyActivationLink["account_handle"];
                                                $_SESSION["id"]             = $verifyActivationLink["user_id"];
                                                $_SESSION["first_name"]     = $verifyActivationLink["first_name"];
                                                $_SESSION["last_name"]      = $verifyActivationLink["last_name"];
                                                $_SESSION["system_mode"]    = $getSystemMode;
                                                $_SESSION["gender"]         = $verifyActivationLink["gender"];
                                                $_SESSION["pending_qualified_search"] = $verifyActivationLink["no_of_qualified_interactions_pending"];

                                                $msg = "Success : update daily analytics for " . $verifyAnalytics;
                                                $xml_data["step5"]["data"] = '5. ' . $msg;

                                                $redirectUrl = $rootUrl."../dashboard/index.php";
                                                $errCode = 199;
                                                $errMsg = "success";

                                                $responseArr["errCode"]="-1";
                                                $responseArr["errMsg"]= "Successfully verified";
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;

                                                /**************insert domain analytics *******************/
                                                $domain =strtolower(trim(end(explode("@",$email))));
                                                insert_domain_activity("total_verified_user",$domain, $searchDbConn);
                                                deduct_field_count_domain("total_unverified_user",$domain, $searchDbConn);
                                                /**************insert domain analytics *******************/

                                                $returnArr["errCode"] = $errCode;
                                                $returnArr["errMsg"] = $msg;
                                                $returnArr["xml_data"] = $xml_data;
                                                $returnArr["redirectUrl"] = $redirectUrl;

                                            }else{
                                                $msg = "Failed : Database connection error";
                                                $resendVerifMsg = "Failed : Database connection error ";

                                                $xml_data["step7"]["data"] = '7. ' . $msg;

                                                $responseArr["errCode"]=$getSystemMode["errCode"];
                                                $responseArr["errMsg"]=$getSystemMode["errMsg"];
                                                $xml_data['response']["data"] = "";
                                                $xml_data['response']["attributes"] = $responseArr;

                                                $redirectUrl = "../user/resendActivationLink.php";
                                                $errCode = 201;
                                                $errMsg = $resendVerifMsg;

                                                $returnArr["errCode"] = $errCode;
                                                $returnArr["errMsg"] = $msg;
                                                $returnArr["xml_data"] = $xml_data;
                                                $returnArr["redirectUrl"] = $redirectUrl;
                                            }

                                        }else{
                                            $msg = "Failed : Database connection error";
                                            $resendVerifMsg = "Failed : Database connection error ";

                                            $xml_data["step6"]["data"] = '6. ' . $msg;

                                            $errCode = 201;
                                            $errMsg = $resendVerifMsg;

                                            $responseArr["errCode"]=$errCode;
                                            $responseArr["errMsg"]=$errMsg;
                                            $xml_data['response']["data"] = "";
                                            $xml_data['response']["attributes"] = $responseArr;

                                            $redirectUrl = "../user/resendActivationLink.php";

                                            $returnArr["errCode"] = $errCode;
                                            $returnArr["errMsg"] = $msg;
                                            $returnArr["xml_data"] = $xml_data;
                                            $returnArr["redirectUrl"] = $redirectUrl;

                                        }
                                    } else {
                                        $msg = "Failure : update daily analytics for " . $verifyAnalytics;
                                        $resendVerifMsg = "Failure : Update analytics ";

                                        $xml_data["step5"]["data"] = '5. ' . $msg;

                                        $errCode = 200;
                                        $errMsg = $resendVerifMsg;

                                        $responseArr["errCode"]=$errCode;
                                        $responseArr["errMsg"]=$errMsg;
                                        $xml_data['response']["data"] = "";
                                        $xml_data['response']["attributes"] = $responseArr;

                                        $redirectUrl = "../user/resendActivationLink.php";

                                        $returnArr["errCode"] = $errCode;
                                        $returnArr["errMsg"] = $msg;
                                        $returnArr["xml_data"] = $xml_data;
                                        $returnArr["redirectUrl"] = $redirectUrl;
                                    }
                                } else if ($verifyActivationLink["errCode"] == 37) {
                                    $msg = $verifyActivationLink["errMsg"];
                                    $verifyErrCode = $verifyActivationLink["errCode"];

                                    $xml_data["step5"]["data"] = '5. ' . $msg;

                                    $errCode = $verifyErrCode;
                                    $errMsg = "Account is already activated";

                                    $responseArr["errCode"]=$errCode;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;

                                    $redirectUrl = "../prelogin/index.php";

                                    $returnArr["errCode"] = $errCode;
                                    $returnArr["errMsg"] = $msg;
                                    $returnArr["xml_data"] = $xml_data;
                                    $returnArr["redirectUrl"] = $redirectUrl;


                                } else {
                                    $msg = $verifyActivationLink["errMsg"];
                                    $failureErrCode = $verifyActivationLink["errCode"];
                                    $xml_data["step6"]["data"] = '6. ' . $msg;

                                    $errCode = $failureErrCode;
                                    $errMsg = $msg;

                                    $responseArr["errCode"]=$errCode;
                                    $responseArr["errMsg"]=$errMsg;
                                    $xml_data['response']["data"] = "";
                                    $xml_data['response']["attributes"] = $responseArr;

                                    $redirectUrl = "../user/resendActivationLink.php";

                                    $returnArr["errCode"] = $errCode;
                                    $returnArr["errMsg"] = $msg;
                                    $returnArr["xml_data"] = $xml_data;
                                    $returnArr["redirectUrl"] = $redirectUrl;

                                }
                            }else{
                                 $msg = "Request cannot be process, Please try after sometime.";
                                $xml_data["step4"]["data"] = '4. ' . $msg;

                                $errCode = 199;
                                $errMsg = $msg;

                                $responseArr["errCode"]=$errCode;
                                $responseArr["errMsg"]=$errMsg;
                                $xml_data['response']["data"] = "";
                                $xml_data['response']["attributes"] = $responseArr;

                                $redirectUrl = "../user/resendActivationLink.php";

                                $returnArr["errCode"] = $errCode;
                                $returnArr["errMsg"] = $msg;
                                $returnArr["xml_data"] = $xml_data;
                                $returnArr["redirectUrl"] = $redirectUrl;

                            }
                        }else{
                            $msg = "The link is no longer valid. Please update your email id again to receive a new link.";
                            $xml_data["step3"]["data"] = '3. ' . $msg;

                            $errCode = 198;
                            $errMsg = $msg;

                            $responseArr["errCode"]=$errCode;
                            $responseArr["errMsg"]=$errMsg;
                            $xml_data['response']["data"] = "";
                            $xml_data['response']["attributes"] = $responseArr;

                            $redirectUrl = "../user/resendActivationLink.php";

                            $returnArr["errCode"] = $errCode;
                            $returnArr["errMsg"] = $msg;
                            $returnArr["xml_data"] = $xml_data;
                            $returnArr["redirectUrl"] = $redirectUrl;

                        }
                    }else{
                        $msg = "There seems to be a problem with this link.";
                        $xml_data["step2"]["data"] = '2. ' . $msg;

                        $errCode = 197;
                        $errMsg = $msg;

                        $responseArr["errCode"]=$errCode;
                        $responseArr["errMsg"]=$errMsg;
                        $xml_data['response']["data"] = "";
                        $xml_data['response']["attributes"] = $responseArr;

                        $redirectUrl = "../user/resendActivationLink.php";

                        $returnArr["errCode"] = $errCode;
                        $returnArr["errMsg"] = $msg;
                        $returnArr["xml_data"] = $xml_data;
                        $returnArr["redirectUrl"] = $redirectUrl;
                    }
                }else{

                    $msg = "Account is already activated";
                    $xml_data["step1"]["data"] = '2. ' . $msg;

                    $errCode = 999;
                    $errMsg = $msg;

                    $responseArr["errCode"] = $errCode;
                    $responseArr["errMsg"] = $errMsg;
                    $xml_data['response']["data"] = "";
                    $xml_data['response']["attributes"] = $responseArr;

                    $redirectUrl = "../prelogin/index.php";

                    $returnArr["errCode"] = $errCode;
                    $returnArr["errMsg"] = $msg;
                    $returnArr["xml_data"] = $xml_data;
                    $returnArr["redirectUrl"] = $redirectUrl;

                }
            }else{
                $msg = "Failed : Request cannot be process, please contact to keywo or try after sometime.";
                $xml_data["step1"]["data"] = '1. ' . $msg;

                $errCode = 196;
                $errMsg = $msg;

                $responseArr["errCode"]=$errCode;
                $responseArr["errMsg"]=$errMsg;
                $xml_data['response']["data"] = "";
                $xml_data['response']["attributes"] = $responseArr;

                $redirectUrl = "../user/resendActivationLink.php";

                $returnArr["errCode"] = $errCode;
                $returnArr["errMsg"] = $msg;
                $returnArr["xml_data"] = $xml_data;
                $returnArr["redirectUrl"] = $redirectUrl;
            }

        }else{
            $msg = "Failed: Madatory filed are missing.";
            $xml_data["step1"]["data"] = '1. ' . $msg;

            $errCode = 195;
            $errMsg = $msg;

            $responseArr["errCode"]=$errCode;
            $responseArr["errMsg"]=$errMsg;
            $xml_data['response']["data"] = "";
            $xml_data['response']["attributes"] = $responseArr;

            $redirectUrl = "../user/resendActivationLink.php";

            $returnArr["errCode"] = $errCode;
            $returnArr["errMsg"] = $msg;
            $returnArr["xml_data"] = $xml_data;
            $returnArr["redirectUrl"] = $redirectUrl;

        }

        return $returnArr;
}



// redirect to corresponding page on error or success
echo "<script src=" . $rootUrl . "frontend_libraries/jquery/jquery-1.12.4.min.js></script>";
echo "<script src=" . $rootUrl . "frontend_libraries/tzDetector/jstz.js></script>";
include('../layout/transparent_footer.php');

print('<html>');
print('<body>');
print('<form action="' . $redirectUrl . '" method= "post" id="verify_submit" >');
print('<input type="hidden" name="errCode" value="' . $errCode . '" />');
print('<input type="hidden" name="errMsg" value="' . $errMsg . '" />');
print('</form>');
print('<script>');
print('$(document).ready(function(){ $("#verify_submit").submit(); })');
print('</script>');
print('</body>');
print('<html>');


?>
