<?php include("../layout/header.php"); ?>
  <main class="login-page">
    <div class="container">
      <div class="row">
      <div class="col-xs-2"></div>
        <div class="col-xs-8 loginpage">
        	 <div class="row">
        	 	<div class="border_title_bottom">
            		<h4 class="logintitle half innerTB">Login to keywo</h4>
          		</div>
        	 </div>
        	 <form class="form-horizontal" id="formID"  class="formular" method="post">
        	  <div class="row">
        	 	<div class="col-xs-12">

        	 			 <div class="form-group">
        	 			 <div class="row">
						    <label for="inputEmail3" class="col-xs-offset-2 col-xs-2 control-label padding-right-none">Login Id</label>
						    <div class="col-xs-4">
						      <input type="text" class="form-control" id="form_login_username" name="username" value="" data-toggle="popover" data-content="Please Enter the Valid login ID" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
						    </div>
						  </div>
        	 		</div>

        	 			<div class="form-group">
        	 			<div class="row">
						   <label for="inputEmail3" class="col-xs-offset-2 col-xs-2 control-label padding-right-none">Password</label>
						   <div class="col-xs-4">
						   <div>
						    <input type="password" class="form-control" id="form_login_password" class="" name="password" value="" data-toggle="popover"  data-content="Please Enter the valid Password">
						</div>
						</div>
					</div>
        	 		</div>
        	 		<div class="col-xs-12 margin-bottom ">
                <button type="submit" id="internalLoginButton" class="login-signbutton" >Login</button>
                <span class="input_label text-Arapawa"> &nbsp; or &nbsp;</span>
                <a href="../user/signup.php" class="login-signbutton">Sign Up</a>
            </div>
            <div class="active_link margin-bottom">
               <p>
               <a href="../user/forgotPassword.php">Forgot Password?</a>
               <a id="resend-link" style="visibility:hidden;" href="../user/resendActivationLink.php">Resend Activation Link</a>

               </p>
            </div>
        	 	</div>
        	 	<!-- col-xs-12 -->
        	 </div>
        	 <!-- row -->
        	 </form>
        	<div class="row">
        		<strong><div class="innerAll text-center text-red" id="loginErrMsg" ></div></strong>
        	</div>

        </div>
        <!-- col-xs-8 -->
          <div class="col-xs-2"></div>
    </div>
  </div>
  </main>
  <?php include('../layout/transparent_footer.php'); ?>
  <script type="text/javascript">
$(document).ready(function(){
	  $("#form_login_username").focus();
    });
</script>
