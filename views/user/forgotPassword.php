<?php
		include("../layout/header.php");
?>
	<main>
		<!-- Main div start -->
		<div class="main-div">
			<!-- Forgot container -->
			<div class="container forgot-container">
				<div class="row">
					<div class="col-xs-12">
						<!-- Main forgot password start-->
						<div class="main-forget-pass">
								<div class="main-head innerAll">
									<!-- Forgot password Label -->
									<strong><span class="activation-text-header">Forgot Your Password ?</span></strong>
									<!-- Forgot password Label End -->
								</div>
								<hr class="margin-up">

								<!-- main description -->
								<div class="main-desc innerAll">
									Please enter the email id you provided during registration process. In order to receive your access code by email.
								</div>
								<!-- main description -->
								<form>
									<div class="row flex-box">
										<div class="col-xs-4 text-right padding-none">
											<label class="" for="exampleInputEmail2">Enter Your Email-Id: 
												<span class="text-red">*</span>
											</label>
										</div>
										<div class="col-xs-5">
											<div class="forgot-email-icons">
	                  							<input type="email" id="forget_password_email" class=" form-control forgot-email-input">
	                  							<i class="fa fa-envelope" aria-hidden="true"></i>
											</div>
										</div>
										<div class="col-xs-3 padding-none">
											<button type="submit" id="forgetPassword" class="btn btn-xs btn_Act_Link text-center half " value="">Submit</button>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-5 col-xs-offset-4">
											<div id="erroremailshow" class="text-red text-center innerMTB"></div>
										</div>
									</div>
								</form>
						</div>
						<!-- Main forgot password End here-->
					</div>
				</div>
			</div>
			<!-- Forgot container end -->
		</div>
		<!-- Main div End here -->
	</main>
<?php
		include("../layout/transparent_footer.php");
?>

<script type="text/javascript">
$(document).ready(function(){
	  $("#forget_password_email").focus();
    });
</script>