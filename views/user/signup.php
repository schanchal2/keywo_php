<?php
  require_once('../../config/config.php');
  require_once "../../config/countryArray.php";

  include("../layout/header.php");
?>
    <style type="text/css">
    .has-success .chosen-container-single .chosen-single div b,
    .has-error .chosen-container-single .chosen-single div b {
        display: none;
    }
    
    .has-feedback .cityselector label~.form-control-feedback,
    .has-feedback .cityselector label~.form-control-feedback {
        top: 10px;
    }
    </style>
    <main class="prelogin_signup">
        <div class="container inner-6x innerMTB" style="position: relative;">
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <div class="signup-page bg-white">
                    <div class="row margin-none">
                        <div class="col-xs-12 padding-none">
                            <div style="position: relative">
                                <h4 class="signup-title innerTB">Sign Up</h4>
                            </div>
                        </div>
                    </div>
                    <!--  row -->
                    <form id="registerForm" class="user-register-form inner-2x innerT innerLR">
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>First Name <span class="text-red">*</span></label>
                                        <div class="">
                                            <input class="checkName form-control" name="firstName" id="firstName" type="text">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Last Name <span class="text-red">*</span></label>
                                        <div class="">
                                            <input class="checkName form-control" name="lastName" id="lastName" type="text">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Account Handle <span class="text-red">*</span></label>
                                        <div class="">
                                            <input class="form-control" name="accountHandle" id="accountHandle" type="text">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- row -->
                        <div class="row inner-3x innerT">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Email <span class="text-red">*</span></label>
                                        <div class="">
                                            <input class="form-control" name="email" id="email" type="text">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <label>Referral Code</label>
                                        <div class="">
                                            <input class="form-control" name="referal" id="referal" type="text">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="">
                                        <div class="gendergroup">
                                            <div class="radio radio-primary radio-div signupRadio">
                                                <input type="radio" name="gender" id="male" value="male" checked="true">
                                                <label for="male" class="radioLabel">
                                                    Male
                                                </label>
                                            </div>
                                            <div class="radio radio-primary radio-div signupRadio">
                                                <input type="radio" name="gender" id="female" value="female">
                                                <label for="female" class="radioLabel">
                                                    Female
                                                </label>
                                            </div>
                                            <div class="radio radio-primary radio-div signupRadio">
                                                <input type="radio" name="gender" id="other" value="other">
                                                <label for="other" class="radioLabel">
                                                    Other
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- row -->
                        <div class="row inner-3x innerT">
                            <div class="col-xs-6">
                                <div class="cityselector">
                                    <label>Country
                                        <span class="text-red">*</span>
                                    </label>
                                    <select class="dropdown_citycountry chosen-select-deselect" id="country" name="country" onChange="printCities(this.value)">
                                        <option value="">Select Country</option>
                                        <?php foreach ($countryCity as $key => $country) {
                                        echo "<option value=\"$key\">{$key}</option>";
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class=" cityselector">
                                    <label>City
                                        <span class="text-red">*</span>
                                    </label>
                                    <select class="form-control dropdown_citycountry chosen-select-deselect selSignupCity" id="city" data-placeholder="Select City" name="city">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row inner-3x innerT">
                            <div class="col-xs-6 ">
                                <div class="form-group">
                                    <div class="">
                                        <label>Password <span class="text-red">*</span></label>
                                        <div>
                                            <input class="passwordValidation form-control" name="password" id="password" type="password">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="">
                                        <label>Confirm Password <span class="text-red">*</span></label>
                                        <div>
                                            <input class="passwordIdenticalValidation form-control" name=confirmPassword id="confirmPassword" type="password">
                                            <span class="" aria-hidden="true"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 recapchatext" style="text-align:center;">
                                <input value="1" name="flag" type="hidden">
                                <div class="recaptcha" style="width: 304px;margin: 0 auto;">
                                    <!--<div class="g-recaptcha" data-sitekey="6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r">
                                            <iframe src="https://www.google.com/recaptcha/api2/anchor?k=6LfgLwkTAAAAAKRgHG-XuVwvgAs8RWJWJ_SbdI6r&amp;co=aHR0cHM6Ly9zZWFyY2h0cmFkZS5jb206NDQz&amp;hl=en&amp;v=r20161109131337&amp;size=normal&amp;cb=b5irftqmaw1g" title="recaptcha widget" width="304" height="78" role="presentation" frameborder="0" scrolling="no" name="undefined">
                                            </iframe>
                                    </div>-->
                                    <div class="g-recaptcha" data-sitekey="<?php echo $captchaPublickey; ?>"></div>
                                </div>
                                <!--recaptcha-->
                            </div>
                            <!--./col-md-12-->
                        </div>
                        <!--./row-->
                        <div class="row">
                            <div class="col-xs-12 " style="margin-bottom: 20px; text-align:center;">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="acceptTerms" id="agree">
                                        <a href="" class="text-blue">I agree to the Terms and Condition of Use</a>
                                    </label>
                                </div>
                                <!-- <a href="#" class="text-summer-sky">By clicking the Sign Up button I agree to the Terms of Use</a> -->
                            </div>
                        </div>
                        <!--./row-->
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="login-signup-div">
                                    <button class="btn btn-signup" id="signup" onclick="signupSubmit();">Sign Up
                                    </button>
                                    <span class="loading-icon"></span>
                                    <span class="innerL text-dark-gray"> Already Have An Account? <a href="../user/login.php" class="text-light-blue">Login</a></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- signup-page -->
        </div>
        <!-- colo-xs-8 -->
        <div class="col-xs-2"></div>
        <!--./container-->
    </main>
    <?php include("../layout/transparent_footer.php"); ?>
    <script>
    $(document).ready(function() {
        $('#firstName').focus();
        $('#registerForm').bootstrapValidator({
            message: 'This value is not valid',
            container: 'tooltip',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            excluded: [':disabled'],
            fields: {
                country: {
                    group: '.col-xs-6',
                    validators: {
                        notEmpty: {
                            message: 'The country is required and can\'t be empty'
                        }
                    }
                },
                city: {
                    group: '.col-xs-6',
                    validators: {
                        notEmpty: {
                            message: 'The City is required and can\'t be empty'
                        }
                    }
                },
                acceptTerms: {
                    group: '.col-xs-12',
                    validators: {
                        notEmpty: {
                            message: 'You have to accept the terms and policies'
                        }
                    }
                }
            }
        });

        $('.chosen-select-deselect').chosen();
        $('#signup').on('click', function() {
            $(".signup-page input").each(function() {
                if ($(this).val() == "" && ($(this).attr("type") != "radio" || $(this).attr("type") != "checkbox")) {
                    if ($(this).attr("id") != "referal") {
                        $(this).parent().attr("class", "has-error has-feedback chosen-search");
                        $(this).siblings().attr("class", "glyphicon glyphicon-remove form-control-feedback glyphicon-error-color error-cross");
                    }
                }
            });
        });
    });
    </script>
