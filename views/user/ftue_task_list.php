<?php
/**
 * Created by PhpStorm.
 * User: trilok
 * Date: 21/6/17
 * Time: 11:47 AM
 */

header("Access-Control-Allow-Origin: *");
header('Content-Type: text/html; charset=utf-8');
session_start();

$docrootpath=explode("views/",$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__))[0];

require_once ("{$docrootpath}config/config.php");
require_once ("{$docrootpath}config/db_config.php");
require_once ("{$docrootpath}helpers/errorMap.php");
require_once ("{$docrootpath}helpers/arrayHelper.php");
require_once ("{$docrootpath}helpers/coreFunctions.php");
require_once ("{$docrootpath}helpers/stringHelper.php");

$retArray = array();

if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    $user_email = $_SESSION["email"];
    $user_id = $_SESSION["id"];

    $kwdDbConn = createDBConnection("dbkeywords");

    if(noError($kwdDbConn)){
        $kwdDbConn = $kwdDbConn["connection"];

        $getFtUserDetail = getFirstTimeUserStatus($kwdDbConn,$user_email,$user_id);

        if(noError($getFtUserDetail)) {
            $row = mysqli_fetch_assoc($getFtUserDetail['errMsg']);
            $recCount = mysqli_num_rows($getFtUserDetail['errMsg']);

            if($recCount > 0){ ?>
                <div class="suggested-module">
                    <div class="row innerTB half border-bottom">
                        <div class="col-xs-10">
                            <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_profile.php"; ?>">
                                <div class="">
                                    Update Profile
                                </div>
                            </a>
                        </div>
                        <div class=" col-xs-2">
                            <a class="half text-right text-App-Success innerR"><?= ($recCount != 0) ? "<i class=\"fa fa-check\"></i>" : "<i id='profile'></i>"; ?></a>
                        </div>
                    </div>
                    <div class="row innerTB half border-bottom">
                        <div class="col-xs-10">
                            <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_follow.php"; ?>">
                                <div class="">
                                    Follow 5 accounts
                                </div>
                            </a>
                        </div>
                        <div class=" col-xs-2">
                            <a class="half text-right text-App-Success innerR"><?= ($row['user_follow'] != 0) ? "<i class=\"fa fa-check\"></i>" : "<i id='follow'></i>"; ?></a>
                        </div>
                    </div>

                    <?php
                    if($userType == 'ftUser_old'){ ?>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_claim.php"; ?>">
                                    <div class="">
                                        Claim / Unclaim keywords
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><i id='claim'></i></a>
                            </div>
                        </div>
                    <?php } else{ ?>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_like.php"; ?>">
                                    <div class="">
                                        Like a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><?= ($row['like_status'] != 0) ? "<i class=\"fa fa-check\"></i>" : "<i id='like'></i>"; ?></a>
                            </div>
                        </div>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_create.php"; ?>">
                                    <div class="">
                                        Create a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><?= ($row['post_status'] != 0) ? "<i class=\"fa fa-check\"></i>" : "<i id='create'></i>"; ?></a>
                            </div>
                        </div>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_share.php"; ?>">
                                    <div class="">
                                        Share a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><?= ($row['share_post']) != 0 ? "<i class=\"fa fa-check\"></i>" : "<i id='share'></i>"; ?></a>
                            </div>
                        </div>
                    <?php } ?>

                </div>

            <?php }else{ ?>

                <div class="suggested-module">
                    <div class="row innerTB half border-bottom">
                        <div class="col-xs-10">
                            <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_profile.php"; ?>">
                                <div class="">
                                    Update Profile
                                </div>
                            </a>
                        </div>
                        <div class=" col-xs-2">
                            <a href="" class="half text-right text-App-Success innerR"><i id='profile'></i></a>
                        </div>
                    </div>
                    <div class="row innerTB half border-bottom">
                        <div class="col-xs-10">
                            <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_follow.php"; ?>">
                                <div class="">
                                    Follow 5 accounts
                                </div>
                            </a>
                        </div>
                        <div class=" col-xs-2">
                            <a href="" class="half text-right text-App-Success innerR"><i id='follow'></i></a>
                        </div>
                    </div>
                    <?php
                    if($userType == 'ftUser_old'){ ?>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_claim.php"; ?>">
                                    <div class="">
                                        Claim / Unclaim keywords
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR" href=><i id='claim'></i></a>
                            </div>
                        </div>
                    <?php } else{ ?>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_like.php"; ?>">
                                    <div class="">
                                        Like a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><i id='like'></i></a>
                            </div>
                        </div>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_create.php"; ?>">
                                    <div class="">
                                        Create a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><i id='create'></i></a>
                            </div>
                        </div>
                        <div class="row innerTB half border-bottom">
                            <div class="col-xs-10">
                                <a class="text-color-grayscale-3" href="<?php echo "{$rootUrl}views/user/ft_share.php"; ?>">
                                    <div class="">
                                        Share a post
                                    </div>
                                </a>
                            </div>
                            <div class=" col-xs-2">
                                <a class="half text-right text-App-Success innerR"><i id='share'></i></a>
                            </div>
                        </div>
                    <?php } ?>
                </div>

            <?php }
        }else{
            $retArray['errCode'] = 3;
            $retArray['errMsg'] = 'Error : Fetching details';
        }

    }else{
        $retArray['errCode'] = 2;
        $retArray['errMsg'] = 'Error : Connection failure';
    }

}else{
    $retArray['errCode'] = 1;
    $retArray['errMsg'] = 'Error : Unauthorized access';
}

?>