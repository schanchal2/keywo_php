<?php

// include required files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../models/app/appModel.php');

error_reporting(0);
//connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
  $connSearch = $connSearch["connection"];
}else{
  print("Search Database Error");
  exit();
}

$searchkey = $keywords;

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
  $email = $_SESSION["email"];
  $login_status = 1;
}else{
  $login_status = 0;
}

$requiredFields="favourite_app_ids";
$getUserInfo = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $requiredFields);
if(noError($getUserInfo)){
  $getUserInfo = $getUserInfo["errMsg"];
  $favSearchApp = $getUserInfo["favourite_app_ids"];
}else{
  print('Error: fetching User Info');
  exit;
}

//code for the get self url
$self_link = $_SERVER['PHP_SELF'];
$self_link_array = explode('/', $self_link);
$self_page = end($self_link_array);

// getting search categories details from search_categories table (db).
if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
  $searchDetails = getFavSearchCategories($favSearchApp, $connSearch);
  if(noError($searchDetails)){
    $searchDetails = $searchDetails["data"];
  }else{
    print("Error: Fetching search categories details");
    exit;
  }
}
else{
  $defaultSearchApps = getFavLoggedOutApps($connSearch);
  if(noError($defaultSearchApps)){
    $defaultSearchApps = $defaultSearchApps["errMsg"];
  }else{
    print("Error: Fetching favorite app details on logged out mode");
    exit;
  }
}

include("../layout/header.php");
$currentPageUrl = substr(strrchr($currentResultsPageUrl, "/"), 1);
?>
    <div class="col-xs-12">
        <div class="search-categories inner-9x innerML">
            <ul class="nav nav-pills">
                <li role="presentation">
                    <a href="javascript:;" onclick='loadCategoryResults("<?php echo $currentPageUrl; ?>", "<?php echo $searchkey;  ?>", this)' id="allResult" class="active">
                        <?php echo cleanDisplayParameter($connSearch, $appName); ?>
                    </a>
                </li>
                <?php


        if($login_status != 1){
          foreach($defaultSearchApps as $key => $appSearch){
              $defaultAppSearch=  $appSearch["currentResultPageURL"];
              if($currentResultsPageUrl != $defaultAppSearch) {
                echo "<li><a id='loading' href='javascript:;' onclick='loadCategoryResults(\"../" . $appSearch["currentResultPageURL"] . "\", \"" . $searchkey . "\", this)'>" . cleanDisplayParameter($connSearch, $appSearch["app_name"]) . "</a></li>";
              }
          }

        }else{
          if(!empty($searchDetails)){
            foreach($searchDetails as $key => $category_title){
                $categoryTitle = $category_title["currentResultPageURL"];
                if($currentResultsPageUrl != $categoryTitle) {
                  echo "<li><a id='loading' href='javascript:;' onclick='loadCategoryResults(\"../" . $category_title["currentResultPageURL"] . "\", \"" . $searchkey . "\", this)'>" . cleanDisplayParameter($connSearch, $category_title["app_name"]) . "</a></li>";
                }
           }
          }else{
            echo '<li><a href="../../apps/apps.php"><img class="innerR" src="'.$rootUrlImages.'mangFav.png"><strong>Manage App Shortcut</strong></a></li>';
          }
        }
        ?>
            </ul>
        </div>
    </div>
    <!-- code for google search result -->
    <style>
    #___gcse_0 {
        display: none;
    }
    
    .gsc-result {
        margin-bottom: 10px !important;
    }
    
    .maindiv {
        margin-top: 10px !important;
    }
    
    .robotoMedium {
        font-family: robotoMedium;
    }
    
    .robotoRegular {
        font-family: robotoRegular;
    }
    
    .selectedPageNo {
        background-color: #428BCA;
        color: white;
    }
    
    .bottomBorder {
        border-bottom: 3px solid white;
        color: white;
    }
    
    #___gcse_1 {
        width: 100%;
        float: left;
        margin-top: -5px;
        display: none;
    }
    
    .gcsc-branding {
        display: none;
    }
    
    .gsc-cursor-box {
        text-align: center;
    }
    
    .gsc-adBlock {
        height: auto !important;
    }
    
    .gsc-search-box {
        visibility: hidden;
    }
    
    .gsc-table-result,
    .gsc-thumbnail-inside,
    .gsc-url-top {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }
    
    .gsc-wrapper {
        position: relative;
        display: block;
        width: 600px;
    }
    
    .gsc-result-info-container {
        text-align: left;
        background-color: #F3F3F3;
        padding: 0px;
        border-top: medium none;
    }
    
    .gsc-control-cse {
        font-family: Arial, sans-serif;
        background-color: #F3F3F3;
        border: 0px;
    }
    
    .gsc-above-wrapper-area {
        border-bottom: 0px solid #F3F3F3;
        padding: 5px 0 5px 0;
    }
    
    .cse .gsc-resultsHeader,
    .gsc-resultsHeader {
        border: block;
        display: none;
    }
    
    #adBlock {
        background: #FFFFFF none repeat scroll 0 0;
        margin: 0;
        padding: 0;
        background-color: #fdf6e5;
        border-left: 1px solid #fdf6e5;
        border-right: 1px solid #fdf6e5;
        border-bottom: 1px solid #E6E6E6;
    }
    
    .gsc-loading-fade .gsc-result-info,
    .gsc-loading-refinementsArea,
    .gsc-loading-resultsRoot {
        opacity: 1 !important;
    }
    
    .gsc-table-cell-snippet-close,
    .gs-promotion-text-cell {
        border-top: none;
    }
    
    .gsc-table-cell-thumbnail,
    .gs-promotion-image-cell {
        border-top: none;
    }
    
    #web {
        margin-top: 10px !important;
    }
    
    .create-user:hover {
        text-decoration: underline !important;
        color: rgb(37, 124, 199);
    }
    
    h4,
    .h4 {
        font-size: 18px !important;
        margin-top: 10px !important;
        margin-bottom: 10px !important;
    }
    
    .whiteBg {
        padding: 8px;
    }
    
    .active {
        font-weight: bold;
    }
    
    .gsc-table-cell-snippet-close
    /*, .gs-promotion-text-cell*/
    
    {
        padding-left: 0 !important;
    }
    
    .gs-web-image-box {
        padding-top: 4px;
    }
    
    .gsc-thumbnail {
        padding-right: 8px;
    }
    .gsc-result-info{
      padding-left: 0px;
    }
    </style>
    <?php
  include("../layout/prelogin_footer.php");
?>
        <script>
        (function() {
            var cx = 'partner-pub-6535511040694921:3732337095';
            var gcse = document.createElement('script');
            gcse.type = 'text/javascript';
            gcse.async = false;
            gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(gcse, s);
        })();

        function loadCategoryResults(pageUrl, q, element) {
            var self_page = '<?php echo $self_page; ?>';

            $.ajax({
                type: 'GET',
                url: pageUrl,
                dataType: 'html',
                data: {
                    q: q,
                    pageUrl: pageUrl
                },
                success: function(data) {
                    if (pageUrl == '../google/google_result.php' || pageUrl == 'google_result.php') {
                        $("#searchContainer").hide();
                        $("#categoriesResultContainer").html('');
                        $("#categoriesResultContainer").hide();
                        $('a.active').removeClass('active');
                        $(element).addClass('active');

                        $('#gsc-i-id1').val($('#search_box').val());
                        $('.gsc-search-button').click();
                        $("#___gcse_1").show(); //on success

                        $("html, body").animate({
                            scrollTop: 0
                        }, "slow");
                    } else if (pageUrl == '../dailymotion/daily-motion_result.php' || pageUrl == 'daily-motion_result.php' || pageUrl == '../youtube/youtube_result.php' || pageUrl == 'youtube_result.php') {
                        $("#___gcse_1").hide(); //on success

                        $("#searchContainer").css('display', 'none');
                        $("#categoriesResultContainer").html('');
                        $("#categoriesResultContainer").show();
                        $("#categoriesResultContainer").addClass("col-xs-12 inner-2x innerMT search_result_dailymotion");

                        $("#categoriesResultContainer").html(data);
                        $('a.active').removeClass('active');
                        $(element).addClass('active');
                        $(window).scrollTop(0);
                    } else {
                        $("#___gcse_1").hide(); //on success
                        $('a.active').removeClass('active');
                        $(element).addClass('active');
                        $("#searchContainer").css('display', 'none');
                        $("#categoriesResultContainer").html('');
                        $("#categoriesResultContainer").show();

                        $("#categoriesResultContainer").html(data);
                        $('#loading a.active').removeClass('active');
                        $(element).addClass('active');
                        $(window).scrollTop(0);
                    }
                }
            });
            return false;
        }
        </script>
