<?php

// include dependent files
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../models/search/widgetSearchModel.php');

error_reporting(0);
// connecting to search Database
$conn = createDbConnection('dbsearch');

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print("Search Database Error");
    exit();
}

//get query string from url
$sq = stripcslashes(rawurldecode($_GET["q"])); //echo $sq;
$sq = strtolower($sq);
if(strpos($sq, '&#039;')!== false){
    $sq = str_replace("&#039;", "'", $sq);
}
if(strpos($sq, '&quot;')!== false){
    $sq = str_replace("&quot;", '"', $sq);
}
if(strpos($sq,'/')!== false){
    $sq = str_replace("/", "", $sq);
}
if(strpos($sq,"\'")!== false){
    $sq = str_replace("\'", "", $sq);
}
if(strpos($sq,'\\')!== false){
    $sq = str_replace("\\", "", $sq);
}

/****For Weather Widget****/
if(strpos($sq,'singapore')!== false){
    $sq = str_replace("singapore","Singapore",$sq);
}
if(strpos($sq,'translator:')!== false){
    $sq1 = str_replace("translator:","",$sq);
    if(empty($sq1)){
        $sq = "translator:";
    }
}
/****For Calculator Widget****/
$trig = array("sqrt","sin","cos","tan");
foreach($trig as $value)
{
    if(strpos($sq,$value)!== false){
        if(strpos($sq,'calculator:')!== false){
            $sq = str_replace("calculator:", "", $sq);
        }
        if(strpos($sq,'Calculator:')!== false){
            $sq = str_replace("Calculator:", "", $sq);
        }
        $sq = str_replace($value, "", $sq);
        $sq = str_replace(array( '(', ')' ), '', $sq);
        if($value === "sqrt"){
            $sq = sqrt($sq);
        }elseif($value === "sin"){
            $sq = sin($sq);
        }elseif($value === "cos"){
            $sq = cos($sq);
        }elseif($value === "tan"){
            $sq = tan($sq);
        }
    }
}
$mathString = trim($sq);
$number =  preg_replace("/[^a-z0-9]/i", '', $mathString);

//Function to Query Search
$detail = query_analyzer($sq, $conn); //echo "<pre>"; print_r($detail); echo "</pre>";
if(noError($detail)){
    $detail = $detail["errMsg"];
}else{
    printArr("Error analyzing Query");
}
$unittype= array();
$unittype[0] = "Area";
$unittype[1] = "Bandwidth";
$unittype[2] = "Storage";
$unittype[3] = "Length";
$unittype[4] = "Energy";
$unittype[5] = "Frequency";
$unittype[6] = "Mileage";
$unittype[7] = "Mass";
$unittype[8] = "Angle";
$unittype[9] = "Pressure";
$unittype[10] = "Speed";
$unittype[11] = "Volume";
$unittype[12] = "Temperature";
$unittype[13] = "Timeunit";
$unitcomm = array();
foreach($unittype as $unittypee){
    array_push($unitcomm, strtolower($unittypee.':'));
}

//printArr($unitcomm);
$keys = array_keys($detail["types"]); //print_r($keys);
$widgetType = $keys[0];
if($widgetType == ""){
    $widgetType = $keys[1];
} //echo $widgetType;

$status = "N";
if (array_key_exists('btc', $detail["words"]) || array_key_exists('pak', $detail["words"])) {
    $status = "Y";
}else{
    $status = "N";
}
if($widgetType === "currency" && ($detail["types"]["currency"]>1) || ( ($detail["types"]["currency"]>=1 && $status == "Y") ) || strtolower($sq) === "currency:"){
?>
    <div  class="dialog_apps">
        <?php include ("../../controllers/search/widgetCurrencyController.php"); ?>
    </div>

<?php }
      else if($widgetType === "timezone" && $detail["types"]["timezone"]>1 || strtolower($sq) === "timezone:") {
    ?>
    <div class="dialog_apps">
        <?php include ("../../controllers/search/widgetTimeZoneController.php"); ?>
    </div>
<?php }else if($widgetType === "calculator" && $detail["types"]["calculator"]>0 || (is_numeric($number)))
{  ?>
    <div class="dialog_apps">
        <?php

        include ("../../controllers/search/calciResultController.php"); ?>
    </div>
<?php }else if($widgetType === "weather" && $detail["types"]["weather"]>1 || strtolower($sq) === "weather:")
{  ?><div class="dialog_apps">
    <?php if($_GET["status"] == "a"){?>
    <div id = "widgetHtml" style=" float: left;color: #676767; width: 600px; border: 1px solid white; padding: 8px; box-shadow:0 2px 2px 0 rgba(0, 0, 0,.05), 0 1px 4px 0 rgba(0,0,0, .08), 0 3px 1px -2px rgba(0,0,0, .02); display: inline; background: white; display:none;"><h4>Unable to fetch current location for weather</h4></div>
<?php }
    include ("../../controllers/search/weatherResultController.php"); ?>
    </div>
<?php }else if($widgetType === "translator" && $detail["types"]["translator"]>1 || strtolower($sq) === "translator:")
{?>
    <div class="dialog_apps">
        <?php include("../widgets/language_translator/language_translator.php"); ?>
    </div>
<?php }else if($widgetType === "coins" && ($detail["types"]["coins"]>1) || strtolower($sq) === "coins:"){?>
    <div class="dialog_apps">
        <?php  include("../../controllers/search/cryptoResultController.php"); ?>
    </div>
<?php } else if(in_array($widgetType, $unittype, true) || $detail["types"][$widgetType]>1 || in_array(strtolower($sq), $unitcomm, true )) {	?>
    <div class="dialog_apps">
        <?php include ("../../controllers/search/unitResultController.php"); ?>
    </div>
<?php }
?>
