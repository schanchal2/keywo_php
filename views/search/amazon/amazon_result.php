<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once("amazonHelper.php");

error_reporting(E_All);
// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
  $connSearch = $connSearch["connection"];
}else{
  print("Search Database Error");
  exit();
}

// getting current time from server where files are stored.
$time = time();
$count = 0;
$cntArrayVal = array();

// getting search query from url.
$keywords = rawurldecode($_REQUEST['q']);

//convert search query into array of keywords.
$keywordsArr = explode(" ",$keywords);
$searchkey='';

//looping through keywordsArr to remove extra spaces.
foreach($keywordsArr as $key => $keyword){
  if($keyword!=""){
    $searchkey.=$keyword." ";
  }
}

// sanitizing search query from tags, extra spaces and special characters.
$searchkey = strip_tags(trim($searchkey));
$searchkey = preg_replace("/\s\s([\s]+)?/", " ",$searchkey);

// creating directory to store amazon result in json format.
$thisdir = $docRoot."json_directory/AmazonJsonFiles";
if(!is_dir($thisdir)){
  mkdir($thisdir, 0777);
}

$keywd = explode(" ",$searchkey);
$i = 0;
$key= '';

foreach($keywd as $p){
  $p = strtolower($p);
  if($i > 0) $key.= "_";
  $key .= $p;
  $i++;
}

// creating json file with search keywords.
$jsonFile = $key.".json";

/*
Purpose: 	1) check whether file exist or not
            2) if exist check file creation time
            3) creation time greater than 24 hours (i.e 84600 seconds) then
               file get deleted and create a new file with current time and create new
               file with search keywords.
*/
if(file_exists($thisdir."/".$jsonFile)){
  if(($time - filemtime($thisdir."/".$jsonFile)) >= 86400)  // 24*60*60
  {
    unlink($thisdir."/".$jsonFile);
  }
}
/*
Purpose:	1) check whether file exist or not
            2) If file exist then get file from server using file_get_contents() function without calling amazon api.
*/
if(file_exists($thisdir."/".$jsonFile)){
  $getJsonResult = file_get_contents($thisdir."/".$jsonFile);
  $getJsonResult = unserialize($getJsonResult);
  displayPaginateResult($getJsonResult, $jsonFile, $connSearch);
}else{
  $result = displayApiResult($searchkey, $connSearch);
  if(noError($result)){
    $result = $result["errMsg"];
    displayPaginateResult($result, $jsonFile, $connSearch);
  }else{
    print($result["errMsg"]);
  }
}
?>

<!-- Pagination through results from amazon -->
<script>
  function loadPageAmazon(pageNo, searchQuery){
    var secId = $('#paginate').parent().attr('id');
      url = "../amazon/amazon_result.php";
    $.ajax({
      type: 'GET',
      url: url,
      dataType: 'html',
      data: {pageno:pageNo, q:searchQuery},
      success: function (data) {
        //if(appStatus == "TRUE"){
          $("#searchContainer").html("");
          $("#searchContainer").html(data);
//        }else{
//          $("#"+secId).html('');
//          $("#"+secId).html(data);
//        }
      }
    });
    return false;
  }
  // scroll to top in pagination on search result page.
  $(document).ready(function(){
    $(this).scrollTop(0);
  });
</script>