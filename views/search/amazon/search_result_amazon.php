<?php
die;
session_start();

// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../IPBlocker/ipblocker.php');

require_once("../../../backend_libraries/xmlProcessor/xmlProcessor.php");
require_once("../../../helpers/deviceHelper.php");

//get query string from url
$keywords =  cleanXSS(rawurldecode($_REQUEST['q']));
if(isset($keywords) && empty($keywords)){
    header("location:".$rootUrl);
}

// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];

    // clean search query
    $keywords =  searchKeySanitizer($keywords);



    //get user ip address
    $ipAddr = getClientIP();
    //$ipAddr = '198.268.2.73';
    $appId = 8; // amazon app id

    $appDetails = getAppDetails($connSearch, $appId);

    if(noError($appDetails)){
        $appDetails =   $appDetails['errMsg'][0];
        $appName    =   $appDetails['app_name'];
        $currentResultsPageUrl =  $appDetails['currentResultPageURL']; //used for dynamic search categories
        $logFileName           =  $appDetails['logFileName'];    // amazon log file name

        // ip search count
        //  $ipSearchCount = 10;
    }else{
        print("Error: Fetching app details");
        exit;
    }
}else{
    print("Search Database Error");
    exit();
}

//check rquest for blocking IP address.
$user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
$extraArgs = array();
$checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywords); //$ipAddr

if(noError($checkIPInfo)){
    // Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
    $checkIPInfo = $checkIPInfo["errMsg"];
    $searchCount = $checkIPInfo["search_count"];
    setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
}else{
    setErrorStack($returnArr, 11, $errMsg,  extraArgs);

}
?>

<!-- Header include -->
<?php include("../../layout/header.php"); ?>
<main class="clearfix">
    <div id="main-area">
        <!-- Widget + Search result Container -->
        <div>
            <div id="searchCategory" >
                <?php $appName = 'Amazon';
                include ("../search_categories.php"); ?>
            </div>
            <!-- getting widget calculation -->
            <div id="widget" class="inner-6x innerML" style="display:none;"></div>
            <!-- Search Result Container -->
            <section id="searchContainer" class="col-xs-12"></section>
            <section id="categoriesResultContainer" style="display:none;"></section>
            <gcse:searchbox></gcse:searchbox>
            <gcse:searchresults></gcse:searchresults>
        </div>
        <!-- Ads container -->
        <div id="sidebarFlip" style="display:none;" class="col-xs-6" >
            <h1>Adds will come here</h1>
        </div>
        <!-- for scrolling search result page Up -->
        <a href="#0" class="cd-top" style="left:75%;"></a>
</main>
<!-- end -->
<input type="hidden" id="login_status" value="<?php echo $login_status; ?>" />

<!-- Include footer-- >
<?php include('../../layout/transparent_footer.php');?>

<!-- ScrollTop -->
<script src='../../../js/search.js'> </script>
<script>
    var appId = '<?php echo $appId; ?>';
    var appName = '<?php echo $appName; ?>';
    var searchQuery = '<?php echo $keywords; ?>';
    var originIp = '<?php echo $ipAddr; ?>';
    var rootUrl  = '<?php echo $rootUrl; ?>';
    var logFileName = '<?php echo $logFileName; ?>';
    var ipSearchCount = '<?php echo $searchCount; ?>';
    var rootUrlSearch = '<?php echo $rootUrlSearch; ?>';
    var currentCurrPref = '<?php echo $userCurrencyPreference; ?>';

    //Get flipKart results from ajax
    $.ajax({
        type:'GET',
        url: rootUrlSearch+'amazon/amazon_result.php',
        data: {q:searchQuery},
        dataType: 'html',
        async:true,
        success: function(data){
// This will replace all content in the body tag with what has been retrieved
            $("#searchContainer").html();
            $("#searchContainer").html(data).fadeIn(1000);
        }
    });

    //Get Coin distribution Process result using ajax
    var coinDist = coinDistributionProcess(appId, appName, searchQuery, originIp, rootUrl, logFileName, ipSearchCount);
    console.log(coinDist);

    //Get widget result using ajax
    loadWidgets(searchQuery, currentCurrPref, rootUrlSearch);
</script>
<!-- End -->
