<?php
die;
session_start();

// include required files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../models/search/searchLandingModel.php');

$appId          = $_REQUEST["appId"];
$appLogo        = urldecode($_REQUEST["appLogo"]);
$appURL         = urldecode($_REQUEST["appURL"]);
$appName        = urldecode($_REQUEST["appName"]);
$rootUrl        = urldecode($_REQUEST["rootUrl"]);

$newServerRootUrl = $docRoot.'json_directory/';
$dir = $newServerRootUrl."AmazonJsonFiles/";

// connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
$connSearch = $connSearch["connection"];
 ?>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2 padding-none">
                <div class="inner-2x innerR">
                    <img class="search-landing-img img-responsive half innerT " src="<?php echo cleanDisplayParameter($connSearch, $rootUrl.$appLogo); ?>" />
                </div>
            </div>
            <div class="col-xs-10 padding-none innerMT inner-2x">
                <form class="amazon-search-landing-form clearfix" id="my_form" style="width:100%;" action="<?php echo cleanDisplayParameter($connSearch, $appURL); ?>" enctype="application/x-www-form-urlencoded" role="search" onsubmit=" return checkEmptyField();">
                    <div class="main-search-box pull-left">
                        <!--===============Search Box====================-->
                        <input type="text" class="landing-search-box form-control autoSuggest xssValidation" name="q" placeholder="Enter your search term and start earning" />
                    </div>
                    <div class="main-search-button pull-left">
                        <!--===============Search Button====================-->
                        <input type="submit" value="<?php echo cleanDisplayParameter($connSearch, ucwords($appName)); ?> Search" class="bg-skyblue text-white landing-search-btn" />
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Popular Search query items -->
        <div class="col-xs-6">

            <h3 class="text-summer-sky">
                Popular
            </h3>
            <?php

            $getKeyword = getPopularKeyword($newServerRootUrl, $appId, $appName);
            if(noError($getKeyword)){
                $getKeyword = $getKeyword["keywords"];
                foreach($getKeyword as $prdKey => $prd){
                    $prd = $prd.".json";
                    $getPopularResult = file_get_contents($dir.$prd);
                    $getPopularResult = unserialize($getPopularResult);
                    if(!empty($getPopularResult)){
                        foreach($getPopularResult[2] as $key => $jsonVal){
                            if($key < 5){?>
                <div class="card clearfix innerMB">

                    <div class="embed-responsive-item  img-responsive pull-left half innerMR product-img-box">
                        <?php
                                        $popularImage = $jsonVal["imageURL"];
                                        if($popularImage == ''){
                                            print('<img src="'.$rootUrl.'images/noImage.png" class="itemImgCss" alt=""/>');
                                        }else{
                                            print('<img src="'.$popularImage.'" class="product-img" alt=""/>');
                                        }
                                        ?>
                    </div>

                    <div class="pull-left innerAll half product-detail">
                        <div class="product-title innerMB">
                            <?php
                                            print('<a href="'.$jsonVal["DetailPageURL"].'" target="_blank">');
                                            if(strlen($jsonVal["title"]) < 60){
                                                print( substr($jsonVal["title"], 0, 60));
                                            }else{
                                                print( substr($jsonVal["title"], 0, 60).'...');
                                            }
                                            print('</a>');
                                            ?>
                        </div>
                        <div class="product-by">
                            <?php
                                            $brand = $jsonVal["brand"];
                                            if(empty($brand)){

                                            }else{
                                                if(strlen($brand) < 15){
                                                    print(substr('by '.$brand, 0, 15));
                                                }else{
                                                    print(substr('by '.$brand, 0, 15).'...');
                                                }
                                            }
                                            ?>
                        </div>
                        <div class="product-price">
                            Price: <span>
                        <?php
                        $price = $jsonVal["price"];
                        print($price);
                        ?>
                        </span>
                        </div>
                    </div>
                </div>
                <?php
                            }
                        }
                        break;
                    }
                }
            }else{
                print("Error: ".$getKeyword["errMsg"]);
                exit;
            }
            ?>
        </div>
        <!-- /Popular Search query items -->
        <!-- /Popular search query-->
        <!-- Recent search query-->
        <div class="col-xs-6">
            <!-- Recent search Header -->
            <h3 class="text-summer-sky">
                Recent
            </h3>
            <!-- /Recent search Header -->
            <!-- Recent Search query items -->
            <?php

                // getting recent serach query from user history table.
                $getRecentKeyword = getRecentSearchQuery($appId, $connSearch);

                if(noError($getRecentKeyword)){
                    $getRecentKeyword = $getRecentKeyword["errMsg"]["keyword"];
                    $getRecentKeyword = trim($getRecentKeyword);
                    $getRecentKeyword = explode(" ", $getRecentKeyword);
                    $getRecentKeyword = implode("_", $getRecentKeyword);
                    $getRecentKeyword = $getRecentKeyword.".json";

                    if(file_exists($dir.$getRecentKeyword)){
                        echo "file exist";
                        //if file exist in directory then fetch result from json file.
                        $getResult = file_get_contents($dir.$getRecentKeyword);
                        $getResult = unserialize($getResult);
                    }else{

                        // if file not exist then get recently created file from AmazonJsonFIles directory to show results.
                        $getResult = getLatestJson($dir);
                        if(noError($getResult)){
                            $getResult = $getResult["errMsg"];
                            $getResult = file_get_contents($dir.$getResult);
                            $getResult = unserialize($getResult);
                        }else{
                            print("Error: Getting recent json file from AmazonJsonFIles directory");
                            exit;
                        }
                    }
                    // Show result from json files.
                    foreach($getResult[0] as $key => $jsonVal){
                        if($key < 5){?>
                <div class="card clearfix innerMB">
                    <div class="img-responsive pull-left half innerMR product-img-box">
                        <?php
                                    $recentImage = $jsonVal["imageURL"];
                                    if($recentImage == ''){
                                        print('<img src="'.$rootUrl.'images/noImage.png" class="itemImgCss" alt=""/>');
                                    }else{
                                        print('<img src="'.$recentImage.'" class="product-img" alt=""/>');
                                    }
                                    ?>
                    </div>
                    <div class="pull-left innerAll half product-detail">
                        <?php
                                    print('<a href="'.$jsonVal["DetailPageURL"].'" target="_blank">');
                                    print('<div class="product-title innerMB">');
                                    if(strlen($jsonVal["title"]) < 60){
                                        print( substr($jsonVal["title"], 0, 60));
                                    }else{
                                        print( substr($jsonVal["title"], 0, 60).'...');
                                    }
                                    print('</div>');
                                    print('</a>');
                                    ?>
                            <div class="product-by">
                                <?php
                                        $brand = $jsonVal["brand"];
                                        if(empty($brand)){

                                        }else{
                                            if(strlen($brand) < 15){
                                                print(substr('by '.$brand, 0, 15));
                                            }else{
                                                print(substr('by '.$brand, 0, 15).'...');
                                            }
                                        }
                                        ?>
                            </div>
                            <div class="product-price">
                                <?php
                                        $price = $jsonVal["price"];
                                        if(empty($price)){
                                            print("Price: <span style='color:red;'>  N/A </span>");
                                        }else{
                                            print("Price: ".$price);
                                        }
                                        ?>
                            </div>
                    </div>
                </div>
                <?php  }
                    }
                }else{
                    print("Error: Fetching Recently Search Query");
                    exit;
                }

            }else{
                print("Error: Search Database connection");
                exit;
            }

            ?>
        </div>
    </div>
    <script>
    // Autosuggest on Apps and searchresult pages.
    $(function() {
        $(".autoSuggest").autocomplete({
            source: "../../../controllers/search/autoSuggestController.php?appJsonId=<?php echo $appId; ?>",
            minLength: 1
        });
    });

    $(function(){
        $('.xssValidation').on("keydown", function(e){
            if (e.shiftKey && (e.which == 188 || e.which == 190)) {
                e.preventDefault();
            }
        });
    });
    </script>
