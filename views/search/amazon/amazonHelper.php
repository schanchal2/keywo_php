<?php
die;
function call_amazon_api($q, $start){
    //adding Global variables ;
    global $fetch_url;
    global $fetch_xml;

    // Your AWS Access Key ID, as taken from the AWS Your Account page
    $aws_access_key_id = "AKIAJ6RHCSJF26SW2NPA";

    // Your AWS Secret Key corresponding to the above ID, as taken from the AWS Your Account page
    $aws_secret_key = "5GdSqPBg4LKqqeD03zCbGD4YzAdA4P5zsTdrINop";

    // The region you are interested in
    $endpoint = "webservices.amazon.com";
    $uri = "/onca/xml";
    $params = array( "Service" => "AWSECommerceService", "Operation" => "ItemSearch", "Condition" => "All", "AWSAccessKeyId" => "AKIAJ6RHCSJF26SW2NPA", "AssociateTag" => "searchtrade-21", "SearchIndex" => "All", "Keywords" => $q, "ItemPage" => $start,  "ResponseGroup" => "Images,ItemAttributes,Offers" );

    // Set current timestamp if not set
    if (!isset($params["Timestamp"])) {
        $params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');
    }

    // Sort the parameters by key
    ksort($params);

    $pairs = array();

    foreach ($params as $key => $value) {
        array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
    }

    // Generate the canonical query
    $canonical_query_string = join("&", $pairs);

    // Generate the string to be signed
    $string_to_sign = "GET\n".$endpoint."\n".$uri."\n".$canonical_query_string;

    // Generate the signature required by the Product Advertising API
    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $aws_secret_key, true));

    // Generate the signed URL
    $request_url = 'http://'.$endpoint.$uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);
    //echo "Signed URL: \"".$request_url."\"";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$request_url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    $xml_response = curl_exec($ch);
    if ($xml_response === False)
    {
        return False;
    }
    else{
        $parsed_xml = simplexml_load_string($xml_response);
        return ($parsed_xml === False) ? False : $parsed_xml; //xml_response
    }
    curl_close($ch);

    // assigning values to global variables;
    $fetch_url=$request_url;
    $fetch_xml=$parsed_xml;
}

function xml2array ( $xmlObject, $out = array () ){
    foreach ( (array) $xmlObject as $index => $node )
        $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
    return $out;
}

function displayApiResult($searchkey, $connSearch){

    global $docRoot;

    $prdArray 			= array();
    $arr 				= array();
    $mainArr 			= array();
    $strItemDetails 	= array();
    $xml 				= array();
    $returnArr			= array();
    $recoveredArray		= array();

    for($i=1;$i<=6;$i++){
        if($i == 1){
            array_push($arr, $i);
            $details = call_amazon_api($searchkey, $i);
            $xmlArray = xml2array($details);
            $xmlArray = json_encode($xmlArray);
            $mainArr[] = $xmlArray;
        }else{
            array_push($arr, $i-1);
            $details = call_amazon_api($searchkey, $i-1);
            $xmlArray = xml2array($details);
            $xmlArray = json_encode($xmlArray);
            $mainArr[] = $xmlArray;
        }
    }

    foreach($mainArr as $key => $arrVal){
        $arrVal = json_decode($arrVal, true);
        foreach($arrVal["Items"]["Item"] as $num => $prodInfo){
            $returnArr[$num]["DetailPageURL"]	= 	$prodInfo["DetailPageURL"];
            $returnArr[$num]["imageURL"] 		=	$prodInfo["MediumImage"]["URL"];
            $returnArr[$num]["title"]			=	$prodInfo["ItemAttributes"]["Title"];
            $returnArr[$num]["brand"]			=	$prodInfo["ItemAttributes"]["Brand"];
            $returnArr[$num]["price"]			=	$prodInfo["ItemAttributes"]["ListPrice"]["FormattedPrice"];
        }
        $strItemDetails[] = $returnArr;
    }

    foreach($strItemDetails as $key => $val){
        foreach($val as $key => $prd){
            $cntArrayVal[] = $prd;
        }
    }

    if(count($cntArrayVal) == 60){
             $thisdir = $docRoot."json_directory/AmazonJsonFiles";
        if(!is_dir($thisdir)){
            mkdir($thisdir, 0777);
        }
        $keywds = explode(" ",$searchkey);
        $i = 0;
        $key= '';

        foreach($keywds as $p){
            $p = strtolower($p);
            if($i > 0) $key.= "_";
            $key .= $p;
            $i++;
        }

        $jsonFile = $key.".json";
        $serializeData = serialize($strItemDetails);
        file_put_contents($thisdir."/".$jsonFile, $serializeData);
        $recoveredData = file_get_contents($thisdir."/".$jsonFile);
        $recoveredArray["errCode"] = -1;
        $recoveredArray["errMsg"] = unserialize($recoveredData);
    }else{
        $recoveredArray["errCode"] = 5;
        $recoveredArray["errMsg"] = "<div  class='pull-left inner-5x innerML'><h1> No matching results found !!! </h1><h4>Please check the spelling or try different keywords </h4></div>";
    }

    return $recoveredArray;
}

function displayJSONResult($JsonResult, $connSearch){
    ?>
        <div class="col-xs-6 inner-2x">
            <div class="col-xs-12 inner-2x innerMB">
                <div class="padding-none amazon-search-results">

                    <?php

                    foreach($JsonResult as $x => $data){
                    $data = json_decode($data , true);

                    foreach($data["Items"]["Item"] as $y => $jsonData){
                    ?>
                    <div class="col-xs-5 inner-2x innerMB">
                        <div class="card amazon-card">
                            <div>
                                <?php
                                print("<a  href='".cleanDisplayParameter($connSearch,$jsonData["DetailPageURL"])."' target='_blank'>");
                                ?>
                                <?php
                                print("<p class='innerT'><img class='product-img img-responsive' src='".cleanDisplayParameter($connSearch,$jsonData["MediumImage"]["URL"])."' /></p>");
                                ?>

                            </div>
                                <div class="product-detail-box innerAll">
                                    <div class="product-box-title innerMB">
                                        <?php print("<p class='product_title'>".cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Title"])."</p>");?>
                                    </div>
                                    <div class="product-box-by inner-2x innerMB text-black">
                                        <?php  print("<p class='product_brand' > by ".cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Brand"])."</p>"); ?>
                                    </div>
                                    <div class="product-box-price inner-2x innerMB">
                                        <?php
                                            $price = $jsonData["ItemAttributes"]["ListPrice"]["FormattedPrice"];
                                            if($price == ""){
                                                print ("<p class='product_price'>Price: N/A </p>");
                                            }
                                            else {
                                                print("<p class='product_price'>Price: " . cleanDisplayParameter($connSearch, $price) . "</p>");
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="innerB amazon-buy-button">
                                    <div class="product-buy-now text-center text-white">
                                        BUY NOW
                                    </div>
                                </div>
                            <?php print("</a>");?>
                        </div>
                    </div>
                        <?php } }?>
                </div>
            </div>
        </div>
        <?php }

function dataArray($arr, $connSearch)
{
    $newArr = array();
    foreach ($arr as $key => $val) {
        $val = json_decode($val, true);
        foreach ($val["Items"]["Item"] as $a => $j) {
            $newArr[] = $j;
        }
    }

    $tot = count($newArr);
    $tmp = '';

    for ($i = 0; $i < $tot - 1; $i++) {
        for ($j = $i + 1; $j < $tot; $j++) {
            $iVal = $newArr[$i]["ItemAttributes"]["ListPrice"]["FormattedPrice"];
            $jVal = $newArr[$j]["ItemAttributes"]["ListPrice"]["FormattedPrice"];
            $iVal = preg_replace('/[^A-Za-z0-9\. -]/', '', $iVal);
            $jVal = preg_replace('/[^A-Za-z0-9\. -]/', '', $jVal);

            if ($iVal < $jVal) {
                $tmp = $newArr[$i];
                $newArr[$i] = $newArr[$j];
                $newArr[$j] = $tmp;
            }
        }
    }
?>
<div class="col-xs-6 inner-2x">
    <div class="col-xs-12 inner-2x innerMB">
        <div class="padding-none amazon-search-results">

        <?php
    foreach ($newArr as $y => $jsonData) {
        ?>
            <div class="col-xs-5 inner-2x innerMB">
                <div class="card amazon-card">
        <div>
            <?php
            print("<a  href='" . cleanDisplayParameter($connSearch, $jsonData["DetailPageURL"]) . "' target='_blank'>");
            ?>
            <?php
            print("<p class='innerT'><img class='product-img img-responsive' src='" . cleanDisplayParameter($connSearch, $jsonData["SmallImage"]["URL"]) . "' /></p>");
            ?>

        </div>

        <div class="product-detail-box innerAll">
            <div class="product-box-title innerMB">
                <?php print("<p class='product_title'>" . cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Title"]) . "</p>"); ?>
            </div>
            <div class="product-box-by inner-2x innerMB text-black">
                <?php print("<p class='product_brand' > by " . cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Brand"]) . "</p>"); ?>
            </div>
            <div class="product-box-price inner-2x innerMB">
                <?php
                $price = $jsonData["ItemAttributes"]["ListPrice"]["FormattedPrice"];
                if ($price == "") {
                    print ("<p class='product_price'>Price: N/A </p>");
                } else {
                    print("<p class='product_price'>Price: " . cleanDisplayParameter($connSearch, $price) . "</p>");
                }
                ?>
            </div>
        </div>
        <div class="innerB amazon-buy-button">
            <div class="product-buy-now text-center text-white">
                BUY NOW
            </div>
        </div>
        <?php print("</a>"); ?>
                    </div>
                    </div>
        <?php
    }
    ?> </div>
    </div>
    </div>

            <?php
}

function displayPaginateResult($my_array, $jFile, $connSearch){

    $jFile = preg_replace("/[_]+/i ", " ", $jFile);
    $jFile = preg_replace('/\\.[^.\\s]{3,4}$/', '', $jFile);
    $array = explode(" ", $jFile);
    $array2 = array();
    foreach($array as $array1){
        $string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
        $test .='%'.$string.'';
        array_push($array2, $string);
    }

    $jFile = implode(" ", $array2);
    $jFile = cleanDisplayParameter($connSearch, $jFile);

    $arr = $my_array;
    $rows_per_page = 1;
    $numrows = count($arr);
    // Calculate number of $lastpage
    $lastpage = ceil($numrows/$rows_per_page);
    $lastpage = $lastpage - 1;
    // condition inputs/set default
    if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
    } else {
        $pageno = 1;
    }

    // validate/limit requested $pageno
    $pageno = (int)$pageno;
    if ($pageno > $lastpage) {
        $pageno = $lastpage;
    }
    if ($pageno < 1) {
        $pageno = 1;
    }
    // Find start and end array index that corresponds to the requested pageno
    $start = ($pageno - 1) * $rows_per_page;
    $end = $start + $rows_per_page - 1;
    // limit $end to highest array index
    if($end > $numrows - 1){
        $end = $numrows - 1;
    }

    ?>

<div class="col-xs-6 inner-2x">
    <div class="col-xs-12 inner-2x innerMB">
        <div class="padding-none amazon-search-results">
    <?php
    // display array from $start to $end
    for($i = $start;$i <= $end; $i++){
        foreach($arr[$i] as $key => $value) {
            ?>
            <div class="col-xs-5 inner-2x innerMB">
                <div class="card amazon-card">
            <div>
                <?php
                print("<a  href='" . cleanDisplayParameter($connSearch, $value["DetailPageURL"]) . "' target='_blank'>");
                ?>
                <?php
                print("<p class='innerT'><img class='product-img img-responsive' src='" . cleanDisplayParameter($connSearch, $value["imageURL"]) . "' /></p>");
                ?>

            </div>

            <div class="product-detail-box innerAll">
                <div class="product-box-title innerMB">
                    <?php print("<p class='product_title'>" . cleanDisplayParameter($connSearch, $value["title"]) . "</p>"); ?>
                </div>
                <div class="product-box-by inner-2x innerMB text-black">
                    <?php print("<p class='product_brand' > by " . cleanDisplayParameter($connSearch, $value["brand"]) . "</p>"); ?>
                </div>
                <div class="product-box-price inner-2x innerMB">
                    <?php
                    $price = $value["price"];
                    if ($price == "") {
                        print ("<p class='product_price'>Price: N/A </p>");
                    } else {
                        print("<p class='product_price'>Price: " . cleanDisplayParameter($connSearch, $price) . "</p>");
                    }
                    ?>
                </div>
            </div>
            <div class="innerB amazon-buy-button">
                <div class="product-buy-now text-center text-white">
                    BUY NOW
                </div>
            </div>
            <?php print("</a>"); ?>
            </div>
            </div>
            <?php
        }

    }
    ?>
        </div>
    </div>

            <?php

    echo "<br><br>";
    echo "<div id='paginate' class='pagebtn-group text-center'>";

    // first/prev pagination hyperlinks
    if ($pageno == 1) {
        //echo " FIRST PREV ";
    } else {
        echo " <button type='button' class='pagebtn' onclick='loadPageAmazon(1, \"$jFile\")'>FIRST</button> ";
        $prevpage = $pageno-1;
        echo " <button type='button' class='pagebtn' onclick='loadPageAmazon($prevpage, \"$jFile\")'>PREV</button> ";
    }

    $paginationStart = (($pageno-5)<=0)?1:($pageno-5);
    $paginationEnd = (($pageno+5)<=$lastpage)?($pageno+5):$lastpage;
    for($paginationIterator=$paginationStart; $paginationIterator<=$paginationEnd; $paginationIterator++){
        $class = "pagebtn-warning";
        if($paginationIterator==$pageno)
            $class = "pagebtn-success";
        echo " <button type='button' class='pagebtn  $class' onclick='loadPageAmazon($paginationIterator, \"$jFile\")' > $paginationIterator </button>";
    }
    // next/last pagination hyperlinks
    if ($pageno == $lastpage) {
        //echo " NEXT LAST ";
    } else {
        $nextpage = $pageno+1;
        echo " <button onclick='loadPageAmazon($nextpage, \"$jFile\")' type='button' class='pagebtn'>NEXT</button> ";
        echo " <button onclick='loadPageAmazon($lastpage, \"$jFile\")' type='button' class='pagebtn'>LAST</button> ";
    }
    echo "</div>";
    echo "<br>";
}?>
</div>
    <?php


function displayApiResultAdd($searchkey){

    global $docRoot;
    $arr = array();
    $mainArr = array();
    $data = array();
    $xml = array();

    for($i=1;$i<=6;$i++){
        if($i == 1){
            array_push($arr, $i);
            $details = call_amazon_api($searchkey,$i);
            $xmlArray = xml2array($details);
            $xmlArray = json_encode($xmlArray);
            $mainArr[] = $xmlArray;
            //array_pop($arr);
        }else{
            array_push($arr, $i-1);
            $details = call_amazon_api($searchkey,$i-1);
            $xmlArray = xml2array($details);
            $xmlArray = json_encode($xmlArray);
            $mainArr[] = $xmlArray;
            array_pop($arr);
        }
    }


    $thisdir = $docRoot."json_directory/AmazonJsonFiles";

    if(!is_dir($thisdir)){

        mkdir($thisdir, 0777);

    }

    $keyword = explode(" ", $searchkey);
    $i = 0;
    $key= '';

    foreach($keyword as $p){
        $p = strtolower($p);
        if($i > 0) $key.= "_";
        $key .= $p;
        $i++;
    }

    $jFile = $key.".json";

    $serializeData = serialize($mainArr);

    file_put_contents($thisdir."/".$jFile,$serializeData );

    $recoveredData = file_get_contents($thisdir."/".$jFile);

    $recoveredArray = unserialize($recoveredData);

    return $recoveredArray;

}


function dataArray1($arr, $connSearch){
    $newArr = array();
    foreach($arr as $key => $val){
        $val = json_decode($val, true);
        foreach($val["Items"]["Item"] as $a => $j){
            $newArr[] = $j;
        }
    }
    $tot = count($newArr);
    $tmp = '';

    for($i=0; $i<$tot-1; $i++){
        for($j=$i+1; $j<$tot; $j++){
            $iVal = $newArr[$i]["ItemAttributes"]["ListPrice"]["FormattedPrice"];
            $jVal = $newArr[$j]["ItemAttributes"]["ListPrice"]["FormattedPrice"];
            $iVal = preg_replace('/[^A-Za-z0-9\. -]/', '', $iVal);
            $jVal = preg_replace('/[^A-Za-z0-9\. -]/', '', $jVal);
            if($iVal < $jVal){
                $tmp = $newArr[$i];
                $newArr[$i] = $newArr[$j];
                $newArr[$j] = $tmp;
            }
        }
    }
    //displayJSONResult($newArr);
    $count = count($newArr);
    ?>

<div class="col-xs-6 inner-2x">
    <div class="col-xs-12 inner-2x innerMB">
        <div class="padding-none amazon-search-results">
            <?php
    foreach($newArr as $y => $jsonData){
        if($y <= 5){ ?>
            <div class="col-xs-5 inner-2x innerMB">
                <div class="card amazon-card">
            <div  class="rhsvw">
                <div>
                    <?php
                    print("<a  href='" . cleanDisplayParameter($connSearch, $jsonData["DetailPageURL"]) . "' target='_blank'>");
                    ?>
                    <?php
                    print("<p class='innerT'><img class='product-img img-responsive' src='" . cleanDisplayParameter($connSearch, $jsonData["SmallImage"]["URL"]) . "' /></p>");
                    ?>

                </div>

                <div class="product-detail-box innerAll">
                    <div class="product-box-title innerMB">
                        <?php print("<p class='product_title'>" . cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Title"]) . "</p>"); ?>
                    </div>
                    <div class="product-box-by inner-2x innerMB text-black">
                        <?php print("<p class='product_brand' > by " . cleanDisplayParameter($connSearch, $jsonData["ItemAttributes"]["Brand"]) . "</p>"); ?>
                    </div>
                    <div class="product-box-price inner-2x innerMB">
                        <?php
                        $price = $jsonData["ItemAttributes"]["ListPrice"]["FormattedPrice"];
                        if ($price == "") {
                            print ("<p class='product_price'>Price: N/A </p>");
                        } else {
                            print("<p class='product_price'>Price: " . cleanDisplayParameter($connSearch, $price) . "</p>");
                        }
                        ?>
                    </div>
                </div>
                <div class="innerB amazon-buy-button">
                    <div class="product-buy-now text-center text-white">
                        BUY NOW
                    </div>
                </div>
                <?php print("</a>"); ?>
                ?>
            </div>
            </div>
            </div>
            <?php
        }
    }?>
            </div>
            </div>
            </div>
            <?php
}

?>
