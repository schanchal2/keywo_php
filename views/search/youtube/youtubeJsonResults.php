<?php
die;

?>

<div id="related" style="display:none;background:white">
	<div id="relatedDetail" style="max-height: 370px;float: left;width: 40%;overflow-y: auto;">
		<?php
		// include required files
		require_once('../../../config/config.php');
		require_once('../../../config/db_config.php');
		require_once('../../../models/search/searchResultModel.php');
		require_once('../../../helpers/errorMap.php');
		require_once('../../../helpers/sessionHelper.php');
		require_once('../../../helpers/arrayHelper.php');
		require_once('../../../helpers/coreFunctions.php');
		require_once('../../../helpers/stringHelper.php');
		error_reporting(E_All);
		// connecting to search Database
		$connSearch = createDbConnection('dbsearch');
		if(noError($connSearch)){
			$connSearch = $connSearch["connection"];
		}else{
			print("Search Database Error");
			exit();
		}
		foreach ($relatedVideo['items'] as $value){?>
		<div class="clearfix inner-2x innerMB" style='background: white;padding:9.1px;height: 82px;'>
			<?php $relatedid = $value['id']['videoId']; ?>
			<a href="javascript:;" onclick="videoDisplay('<?php echo $relatedid; ?>');">
				<div style='float:left;width:39%'><img src='<?php echo $value['snippet']['thumbnails']['medium']['url']; ?>' style='width: 100%;height:auto;'></div>
			</a>
			<div style='float:left;width:60%;height:auto;overflow:hidden;font-size: 11px;padding: 0% 1.5%;font-weight: bold;'>
				<a href="javascript:void(0);" onclick="videoDisplay('<?php echo $relatedid; ?>');"><?php echo substr(strip_tags(cleanDisplayParameter($connSearch, $value['snippet']['title'])), 0, 80).'...' ; ?></a><br>
				<span style='font-weight:100;font-size:11px;'>by: <?php echo substr(strip_tags(cleanDisplayParameter($connSearch, $value['snippet']['channelTitle'])), 0, 50).'...' ; ?></span>
			</div>
		</div>
		<?php }
		?>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12">
			<div class="row innerMT" id="searchResults">
				<?php
					if(count($youTubeResult['items']) > 0) {
						foreach ($youTubeResult['items'] as $key => $searchResult) {
				$id = $searchResult['id']['videoId']; ?>
				<div class="col-xs-12">
					<div class="innerML">
						<div class="card innerAll innerMB clearfix ">
							<div class="card-img pull-left hover_effect" videoId="<?php echo $searchResult['id']['videoId']; ?>"  onclick="videoDisplay('<?php echo $id; ?>');" >
								<img src="<?php echo $searchResult['snippet']['thumbnails']['default']['url']; ?>">
								<div class="info">
									<img src="../../../images/play_overlay_icon.png">
								</div>
							</div>
							<div class="pull-left card-info innerLR">
								<?php $id = $searchResult['id']['videoId'];?>
								<span class="card-title" onclick="videoDisplay('<?php echo $id; ?>');" > <a href="#" title="<?php echo strip_tags(cleanDisplayParameter($connSearch, $searchResult['snippet']['title'])); ?>"> <?php echo substr(strip_tags(cleanDisplayParameter($connSearch, $searchResult['snippet']['title'])), 0, 80).'...' ; ?> </a></span>
								<?php if($searchResult['snippet']['channelTitle'] != "") { ?>
								<p class="card-by">
									by
									<span
									id="channelName_'+i+'"> <?php echo substr(strip_tags(cleanDisplayParameter($connSearch, ucwords($searchResult['snippet']['channelTitle']))), 0, 50) . '...'; ?> </span>
								</p>
								<p class="card-time">
									<?php
									$datetime1 = new DateTime($searchResult['snippet']['publishedAt']);
									$datetime2 = new DateTime(date("Y/m/d"));
									$interval = $datetime1->diff($datetime2);
									$array = (array)$interval;
									if ($array['y'] > 0) {
										echo $array['y'] . " Year(s) ago";
									} else if ($array['m'] > 0) {
										echo $array['m'] . " Month(s) ago";
									} else if ($array['d'] > 0) {
										echo $array['d'] . " Day(s) ago";
									} ?>
								</p>
								<p class="card-text" id="description_'+i+'"> <?php echo substr($searchResult['snippet']['description'], 0, 80) . '...'; ?>
								</p>
							</div>
						</div>
					</div>
				</div>
				<?php
				}
				}
				}else{
				echo "<div class='row'>
						<div class='noresult inner-6x innerML' style='min-width:700px;'>
								<span style='color:gray;font-size: 12px;'></span><span><h1>No matching results found!!!</h1>
						<h4>Please check the spelling or try different keywords.</h4></span></div></div>";
						}
					?>
			</div>

		</div>
	</div>
</div>
