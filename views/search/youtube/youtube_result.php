<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');

error_reporting(E_All);
// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
}else{
    print("Search Database Error");
    exit();
}


//get query string from url
$keywords = urldecode($_REQUEST['q']);
$appStatus = $_REQUEST['app_status'];
$searchkey = "";

//convert search query into array of keywords.
$keywordsArr = explode(" ", $keywords);

//looping through keywordsArr to remove extra spaces.
foreach ($keywordsArr as $key => $keyword) {
    if ($keyword != "") {
        $searchkey .= $keyword . " ";
    }
}

// sanitizing search query from tags, extra spaces and special characters.
$searchkey = strip_tags(trim($searchkey));
$youTubeResult = array();
$dir = $docRoot . "json_directory/youtubeJsonFiles";
if (!is_dir($dir)) {
    mkdir($dir, 0777);
}

$keyword = explode(" ", $searchkey);
$i = 0;
$key = '';
$key = implode('_',$keyword);
$jsonFile = $key . '.json';
if (file_exists($dir . "/" . $jsonFile)) {
    if (time() - filemtime($dir . "/" . $jsonFile) >= 86400){  // 86400 is the time of one day calculate in seconds
        unlink($dir . "/" . $jsonFile);
    }
}

// Check cache file exist and not request to next page
if (file_exists($dir . "/" . $jsonFile)) {
    $_SESSION["reqType"] = 'json';
    $youTubeResult = file_get_contents($dir . "/" . $jsonFile); // Get stored file from server
    $youTubeResult = json_decode($youTubeResult, true); // Convert files content array
    $relatedVideo = $youTubeResult['relatedVideo'];
    include("youtubeJsonResults.php");
    echo "</div></div>";
} else {
    // Call when request search is new
    $_SESSION["reqType"] = 'api';

    include("youtubeApiResults.php");

    echo "</div></div>";
}

echo "<div class='ytDialog' style='display:none; background: rgb(178, 178, 178) ; overflow:hidden' >";
echo "</div>";

?>
<script>
    $(document).ready(function () {
        var appStatus = '<?php echo $appStatus; ?>';
        if(appStatus != "TRUE"){
            $(".hover_effect").hover(function (e) {
                    $(this).find(".info").show();
                }, function (e) {
                    $(".hover_effect").show();
                    $(this).find(".info").hide();
                }
            );
            $(".ytDialog").dialog({
                autoOpen: false,
                resizable: false,
                modal: true,
                maxheight: 380,
                width: "70%",
                top: "33%",
                left: "17%",
                open: function () {
                    jQuery('.ui-widget-overlay').bind('click', function () {
                        $(".ytDialog").html("");
                        $(".ytDialog").dialog('close');
                    })
                    $(".ui-dialog .ui-dialog-titlebar").hide();
                    $(".ui-dialog").css("padding",0);
                    $(".ui-dialog").css("position",'fixed');
                    var dialogId = $(".ytDialog").attr("id");
                    $("#"+dialogId).css("height","");
                }
            });
        }
    });

    function videoDisplay(videoId) {
        var appended = $("#relatedDetail").html();
        $(".ytDialog").html("");
        $(".ytDialog").html("<div style='width:60%;float:left;'><iframe frameborder='0' width='100%' height='370' src='https://www.youtube.com/embed/" + videoId + "?autoplay=1' allowfullscreen></iframe></div>" +$("#related").html());
        if($.trim(appended) == ''){
            $(".ytDialog").html("<div style='width:60%;float:left;margin-left:20%;background:transparent!important;'><iframe frameborder='0' width='100%' height='370' src='https://www.youtube.com/embed/" + videoId + "?autoplay=1' allowfullscreen></iframe></div>" +$("#related").html());

            $(".ytDialog").css({"background":"transparent"});
        }
        $(".ytDialog").dialog("open");
    }
</script>
<style>
    .hover_effect:hover{
        text-decoration: underline;
        color:rgb(37, 124, 199);
    }
    .ytDialog ui-dialog-content ui-widget-content{
        background: transparent !important;
    }
    iframe{
        height: 370px !important;
    }
    .ui-dialog .ui-dialog-content {
        padding:0px !important;
        background:transparent !important;
    }
    .ui-widget-overlay{
        opacity: 0.7;
    }
</style>
