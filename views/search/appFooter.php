<?php

// connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
$connSearch = $connSearch["connection"];
//Getting app footer details
$appFooter = getAppFooter($connSearch);
?>


<div class="col-xs-12">
<div class='text-center innerAll search-landing-thumbnails'>
    <?php

        if(noError($appFooter)){
            $appFooter = $appFooter['errMsg'];
            foreach($appFooter as $key=> $value){
                $applinkurl=$value["app_url"]."?short=set";
                // <!-- ====================== Search Engines Thumnails =====================================-->
                    echo "<a href=../".$applinkurl.">
                                 <img data-placement=\"top\" data-toggle=\"tooltip\" data-original-title=\"".$value["app_name"]."\" src=\"../../../".$value["app_thumbnail"]."\"
                                 onmouseover=\"this.src='../../../".$value["app_thumbnail_blue"]."'\" onmouseout=\"this.src='../../../".$value["app_thumbnail"]."'\">
                        </a>";
            }
        }else{
            print("Error: Fetching app footer details");
        }
    }else{
        print("Error: Search Database connection");
        exit;
    }
?>
    </div>
</div>

<div class="col-xs-12">
    <!-- ====================== Widgets Thumnails =====================================-->
    <div class="text-center inner-3x innerAll padding-top-none search-landing-thumbnails">

        <a href="" data-toggle="modal" title="" data-target="#myLargeModalLabel" onclick="openWidget('calculator:', 'Calculator');">
          <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Calculator"  src="<?php echo $rootUrlImages; ?>widgetThumbnail/Calculator.svg"
               onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Calculator.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Calculator.svg'">
        </a>
        <a data-toggle="modal" data-target="#myLargeModalLabel" onclick="openWidget('currency:+btc+to+usd', 'Currency Converter');">
        <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Currency converter" src="<?php echo $rootUrlImages; ?>widgetThumbnail/Currency_Converter.svg"
             onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Currency_Converter.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Currency_Converter.svg'">
        </a>
        <a data-toggle="modal" data-target="#myLargeModalLabel" onclick="openWidget('coins:', 'Digital Currency');">
        <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Digital Currency" src="<?php echo $rootUrlImages; ?>widgetThumbnail/Digital_Currency.svg"
             onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Digital_Currency.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Digital_Currency.svg'">
        </a>
        <a data-toggle="modal" data-target="#myLargeModalLabel" onclick="openWidget('12+gmt+to+ist', 'Timezone Converter');">
        <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Timezone converter" src="<?php echo $rootUrlImages; ?>widgetThumbnail/Timezone_Converter.svg"
             onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Timezone_Converter.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Timezone_Converter.svg'">
        </a>
        <a data-toggle="modal" data-target="#myLargeModalLabel"  onclick="openWidget('length:', 'Unit Converter');">
        <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Unit Converter" src="<?php echo $rootUrlImages; ?>widgetThumbnail/Unit_Converter.svg"
             onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Unit_Converter.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Unit_Converter.svg'">
        </a>
        <a data-toggle="modal" data-target="#myLargeModalLabel" onclick="openWidget('weather:', 'Weather Forecast');">
        <img class="widget-landing-img" data-placement="top" data-toggle="tooltip" data-original-title="Weather Forecast" src="<?php echo $rootUrlImages; ?>widgetThumbnail/Weather_Forcast.svg"
             onmouseover="this.src='<?php echo $rootUrlImages; ?>widgetThumbnailBlue/Weather_Forcast.svg'" onmouseout="this.src='<?php echo $rootUrlImages; ?>widgetThumbnail/Weather_Forcast.svg'">

        <a>
    </div>
</div>

<!-- Widget modal -->
<div class="modal fade bs-example-modal-lg"  id="myLargeModalLabel" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-vertical-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div id="widgetModal"></div>
      </div>
    </div>
  </div>
</div>

<!-- appFooter End -->


<script>

    function openWidget(query, type) {
        var base_url = '<?= $rootUrlSearch; ?>';
        $("#widgetTitle").html(type+" Widget");
        $("#widgetModal").html(" ");
        $.ajax({
            type: 'GET',
            dataType:'html',
            url:base_url+'widget_page.php?q='+query,
            success: function(data){
                $("#widgetModal").html(" ");
                $("#widgetModal").html(data);
                $(".widgets-center").css("margin","0 auto");
            }
        });
    }

</script>
