<?php
session_start();
// Including requires files

require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/deviceHelper.php');
require_once('../../helpers/arrayHelper.php');
require_once('../../models/utilities/utilities.php');

//$_SESSION["email"] = "amitmishra@bitstreet.in";

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
      $email = $_SESSION["email"];


      include("../layout/header.php");

      $searchDbConn = createDBConnection("dbsearch");

      if(noError($searchDbConn)){
            $searchDbConn = $searchDbConn["connection"];

            $getExchangeRate = getCurrencyExchangeRate($exchageCurrencyCode, $searchDbConn);
            if(noError($getExchangeRate)){
                  $getExchangeRate = $getExchangeRate["exchange_rate"];

                  $getExchangeRate = json_encode($getExchangeRate);

            }else{
                print('Error: Fetching exchange rate');
                exit;
            }
      }else{
            print('Error: unable to connect search database');
            exit;
      }


      // access $userRequiredFields field from config.php file
      $userRequiredFields .= ",no_of_qualified_searches_pending,total_no_of_searches_in_last_hour,last_hour_search_time";

      // 1. get current user search type i.e. qualified or unqualified.
      $searchType = getSearchType($email, $userRequiredFields);
      if(noError($searchType)){
          $searchType = $searchType["user_search_type"];

          $appId = 1;  // google app id
          $searchQuery = "coin distribution process"; // search query
          $appName = "Google";

          // sanitize the search query
          $searchQuery = searchKeySanitizer($searchQuery);

          // pass parameters to appSearchCountUpdate() function in array format.
          $appUpdateParam = array("app_id" => $appId, "app_name" => $appName, "search_query" => $searchQuery, "search_type" => $searchType);

          if($searchType == "Qualified"){
              /*
                2. Check total search count searched on particular IP.
                  - Get max ip limit set by admin from admin_settings table.
                3. Get current payout amount from payout.json file (IT$ amount).
                4. Check if logged in user has referral user or not (in cdp cron structure).
                5. Credit user interaction earning using API request.
                6. Insert content_consumer interaction earning transaction by executing API insertUserTransaction90
                   with type = "interaction_earning"
                7. Insert transaction into week wise user_search_history table into MYSQL search database.
                8. Deduct qualified search of logged in user by executing getUserInfo() function.
                9. Insert/update qualified search count + 1 into keywo_stats table of keyword MYSQL database.
                10. write CDP process into xml logs.
              */

              // 3. Get current payout amount from payout.json file.
              $currentPayout = getCurrentPayout();
              if(noError($currentPayout)){
                  $currentPayout = $currentPayout["current_payout"];
                  //
                  $currentPayout = number_format(($currentPayout/4), 2);

              }else{
                  print($currentPayout["errMsg"]);
                  exit;
              }
          }else{


              // unqualified process
              /*
                1. Update totalUnqualifiedSearches field by 1 into sc_app_details table of corresponding appId.
                2. Create / update app json file where unique keyword with count = 1 or increase count of existing keyword.
                3. Update totalKeywordSearched field with total number of keyword count number into sc_app_details table.
                4. Update week wise user search transaction history table with type = "Unqualified" into mysql search database
                5. Increasing logged in user's unqualified search count into "userdetails" using API (@notification server).
                6. Update unqualified_searches count by 1 into keywo_stats table of corresponding appId (instead of updating
                   pool unqualified searches in pool user).
              */


              //   1. Update totalUnqualifiedSearches field by 1 into sc_app_details table of corresponding appId.

              $updateKeywordSerches = appSearchCountUpdate($appUpdateParam, $searchDbConn);

          }
      }else{
          print('Error: Fetching user info');
          exit;
      }

}


 ?>




<html>

  <body>
      <form action="#" method="post">
        <br />  Search Query : <br />
          <input type="text" name="q" /><br />
          <button type="submit"> Submit </button>

      </form>
  </body>

</html>
