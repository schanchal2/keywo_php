<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');

error_reporting(E_All);
// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
}else{
    print("Search Database Error");
    exit();
}

// getting search query from url.
$keywords 	= rawurldecode($_REQUEST["q"]);

$array 		= explode(" ", $keywords);
$test		= '"';
$test1 		= "'";

$array2 = array();
foreach($array as $array1){
    $string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    $test1 .='%'.$string.'';
    array_push($array2, $string);
}
$test1 .= '%\'';

$SearchString = implode(" ",$array2);
$test .= '%'.$keywords.'%"';
$query1 .='i.description LIKE '.$test1.' OR  i.title LIKE '.$test.'';
$searchData = $query1;
$pageno1=1;
if(isset($_REQUEST["pageno"]) && !in_array($_REQUEST["pageno"], $blanks)){
    $pageno1=preg_replace('#[^0-9]#i','',$_REQUEST["pageno"]);
}
$resultsPerPage=10;
$start = ($pageno1-1)*$resultsPerPage;
$pageno1=$_REQUEST["pageno"];
$row_count = getBitcoinNews('', $searchData, "bitcoin", '', $connSearch);
$resultsPerPage =10;
$row_count = $row_count["errMsg"];
$row_count = COUNT($row_count);
$lastpage1 = $row_count/$resultsPerPage;
$lastpage1 = ceil($lastpage1);

if($pageno1<=1){
    $pageno1=1;
}else if($pageno1>$lastpage1){
    $pageno1=$lastpage1;
}
if(!isset($pageno1) || ($pageno1 == 1)){
    $start = '0';
}else{
    $start = $start;
}
    ?>

            <div style="color:gray; margin-left: 40px;" ><?php if($row_count > 0){echo $row_count." results found.";}?></div>
            <?php if($row_count == 0){?>
                <div class="inner-6x innerML" style="width: 100%; float: left;">
                    <h1>No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>
                </div>
                <?php
            }else{
                $getNews = getBitcoinNews($start, $searchData, "bitcoin", '10', $connSearch);
                if(noError($getNews)){
                    $getNews = $getNews["errMsg"];
                    foreach($getNews as $key => $result){
                        ?>

                        <div class="col-xs-12">
                            <div class="innerML inner-3x">

                                <div class="card innerAll innerMB clearfix ">
                                   <?php  echo '<h4 class="text-light-blue margin-top-none"><a href="'.$result['link'].'" target="_blank" >'.stripslashes($result['title']).'</a></h4>' ?>
                                    <div class="">
                                        <!-- <div class="card-img pull-left"><img src="../../../images/search/mqdefault.jpg">
                                        </div> -->
                                        <?php
                                        if(!null == $result['url']){
                                            echo '<img src="'.$result['url'].'" width="120" height="78" align="left" style="margin-right:15px;" border="0"/>';
                                        }
                                        ?>
                                        <div class="pull-left card-info">
                                            <p class="card-text margin-bottom-none">
                                                <?php
                                                    if(strlen($result["description"]) < 250){
                                                    echo strip_tags($result["description"]).' <br>';
                                                    }else{
                                                    echo substr(strip_tags($result["description"]), 0, 250).'... <br>';
                                                    }
                                                ?>
                                            </p>
                                            <span class="card-by"><?php echo $result['source']; ?></span>
                                           <?php
                                           $time = $result['publish_date'];
                                            // $totalTime = convertDate("F,d Y",$time);
                                            $totalTime = date("F,d Y",strtotime($time));
                                            ?>
                                            <span class="card-time"><?php echo $totalTime;?></span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <?php
                    }
                }else{
                    print("Error: Fetching News Result");
                    exit;
                }
            }
            ?>

        <?php
        if($row_count != 0){
        ?>
        <div id="paginate" class="text-center" style="margin-top: 10px;">
            <?php
            if ($pageno1 == 1) {
            } else {
                echo " <button type='button' class='pagebtn' onclick='loadBitcoinNews(1, \"$SearchString\")'>FIRST</button> ";
                $prevpage = $pageno1-1;
                echo " <button type='button' class='pagebtn' onclick='loadBitcoinNews($prevpage, \"$SearchString\")'>PREV</button> ";
            }
            $paginationStart = (($pageno1-3)<=0)?1:($pageno1-3);
            $paginationEnd = (($pageno1+3)<=$lastpage1)?($pageno1+3):$lastpage1;
            for($paginationIterator=$paginationStart; $paginationIterator<=$paginationEnd; $paginationIterator++){
                $class = "pagebtn-warning";
                if($paginationIterator==$pageno1)
                    $class = "pagebtn-success";
                echo " <button type='button' class='pagebtn  $class' onclick='loadBitcoinNews($paginationIterator, \"$SearchString\")' > $paginationIterator </button>";
            }

            // next/last pagination hyperlinks
            if ($pageno1 == $lastpage1) {

            } else {
                $nextpage = $pageno1+1;
                echo " <button onclick='loadBitcoinNews($nextpage, \"$SearchString\")' type='button' class='pagebtn'>NEXT</button> ";
                echo " <button onclick='loadBitcoinNews($lastpage1, \"$SearchString\")' type='button' class='pagebtn'>LAST</button> ";
            }
            echo "</div>";
            }
            echo "<br>";
            ?>
        </div>

<style>
    .newsDataDiv {
        border-bottom: 10px solid #f3f3f3;
        margin-left: 15px;
    }
</style>
<script>
    function loadBitcoinNews(pageNo, searchQuery){
        url = "../bitcoinnews/bitcoin_result.php";
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            data: {pageno:pageNo,q:searchQuery},
            success: function (data) {
                $("#searchContainer").html('');
                $("#searchContainer").html(data);
            }
        });
        return false;
    }
    $(document).ready(function(){
        $(this).scrollTop(0);
    });
</script>
