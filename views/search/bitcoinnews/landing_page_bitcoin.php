<?php
die;
session_start();

// include required files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../models/search/searchLandingModel.php');

$appId        = $_REQUEST["appId"];
$appLogo      = urldecode($_REQUEST["appLogo"]);
$appURL       = urldecode($_REQUEST["appURL"]);
$appName      = urldecode($_REQUEST["appName"]);
$rootUrl      = urldecode($_REQUEST["rootUrl"]);
$login_status = $_REQUEST["loginStatus"];

$newServerRootUrl = $docRoot.'json_directory/';

$appNameOnButton = $appName;
$appName         = strtolower($appName);
$appName         = explode(' ', $appName);
$appName         = implode("_", $appName);

$dir = $newServerRootUrl."SearchKeywordsJsonFiles/";
if(!is_dir($dir)){
    mkdir($dir, 0777);
}

$jsonAppName = $dir.$appId."_".$appName."_KeywordSearched.json";
if(!file_exists($jsonAppName)){
    $fp = fopen($jsonAppName, "w");
}

//connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
    $getLatestNews = getBitcoinLatestNews($connSearch);
    if(noError($getLatestNews)){
        // getting latest bitcoin news.
        $getLatestNews = $getLatestNews["errMsg"];

        // getting popular keyword from 13_bitcoin_keywordsearched.json
        $getKeyword = getPopularKeyword($newServerRootUrl, $appId, $appName);
        if(noError($getKeyword)){
            $getKeyword = $getKeyword["keywords"];
            if(empty($getKeyword)){
                $getKeyword = array('bitcoin');
            }
            $getPopularNews = getBitcoinPopularNews($getKeyword, 5, $connSearch);
            if(noError($getPopularNews)){
                $getPopularNews = $getPopularNews["data"];
            }else{
                print("Error: ".$getPopularNews["errMsg"]);
                exit;
            }
        }else{
            print("Error: ".$getKeyword["errMsg"]);
            exit;
        }
    }else{
        print("Error: Fetching Bitcoin Latest News");
        exit;
    }
}else{
    print("Error: Database connection");
    exit;
}
?>
    <div class="row">
        <div class="col-xs-12">
            <div class="col-xs-2 padding-none">
                <div class="inner-2x innerR">
                    <img class="search-landing-img img-responsive half innerT" src="<?php echo cleanDisplayParameter($connSearch, $rootUrl.$appLogo); ?>">
                </div>
            </div>
            <div class="col-xs-10 padding-none">
                <form class="bitcoin-search-landing-form clearfix" style="width:100%;" id="my_form" action="<?php echo cleanDisplayParameter($connSearch, $appURL); ?>" enctype="application/x-www-form-urlencoded" role="search" onsubmit=" return checkEmptyField();">
                    <div class="col-xs-9 padding-right-none">
                        <!--===============Search Box====================-->
                        <input type="text" class="landing-search-box form-control autoSuggest ui-autocomplete-input xssValidation" name="q" placeholder="Enter your search term and start earning" autocomplete="off">
                    </div>
                    <div class="col-xs-3 padding-none">
                        <!--===============Search Button====================-->
                        <input type="submit" value="<?php echo cleanDisplayParameter($connSearch, ucwords($appNameOnButton)); ?> Search" class="bg-skyblue text-white landing-search-btn">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Latest Search query items -->
        <div class="col-xs-6">
            <h3 class="text-summer-sky">
            Latest News
        </h3>
            <?php
        foreach($getLatestNews as $key => $news){
        print('<div class="card clearfix innerAll innerMB">
            <div class="pull-left innerAll half news-detail">');
            print('<div class="newsTitle text-blue"> <a style="0"  href='.$news["link"].' target="_blank">');
                    if(strlen($news["title"]) < 100){
                    print($news["title"]);
                    }else{
                    print(substr($news["title"], 0, 100).'...');
                    }
                    print('</a></div>');
            print('<div class="newsDescription  innerMB">');
                if(strlen($news["description"]) < 200){
                print(strip_tags($news["description"]));
                }else{
                print(substr(strip_tags($news["description"]), 0, 200).'...');
                }
                print('</div>');
            if($news['source']!=null){
            print( '<div class="news-by"><span >By '.$news['source'].'</span> <span >'.date("F,d Y",strtotime($news['publish_date'])).'</span></div>');
            }else{
            print('<span >'.date("F,d Y",strtotime($news['publish_date'])).'</span>');
            }
            print('</div></div>');
        }
        ?>
                <div class="card clearfix innerAll innerMB">
                    <div class="pull-left innerAll half news-detail">
                        <div class="news-title text-blue">
                            <?php
                        print('<a href='.$news["link"].' target="_blank">');
                        if(strlen($news["title"]) < 100){
                            print($news["title"]);
                        }else{
                            print(substr($news["title"], 0, 100).'...');
                        }
                        print('</a>');
                    ?>
                        </div>
                        <div class="news-description innerMB">
                            <?php
                       if(strlen($news["description"]) < 200){
                           print(strip_tags($news["description"]));
                       }else{
                           print(substr(strip_tags($news["description"]), 0, 200).'...');
                       }
                   ?>
                        </div>
                        <div class="news-by">
                            <?php
                            if($news['source']!=null){
                            print( '<div><span >By '.$news['source'].'</span> <span>'.date("F,d Y",strtotime($news['publish_date'])).'</span></div>');
                            }else{
                            print('<span>'.date("F,d Y",strtotime($news['publish_date'])).'</span>');
                            }
                    ?>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /Latest Search query items -->
        <!-- Popular Search query items -->
        <div class="col-xs-6">
            <h3 class="text-summer-sky">
            Popular News
        </h3>
            <div class="card clearfix innerAll innerMB">
                <div class="pull-left innerAll half news-detail">
                    <div class="news-title text-blue">
                        <?php
                            print('<a href='.$news["link"].' target="_blank">');
                            if(strlen($news["title"]) < 100){
                                print($news["title"]);
                            }else{
                                print(substr($news["title"], 0, 100).'...');
                            }
                            print('</a></div>');
                    ?>
                            <div class="news-description innerMB">
                                <?php
                          if(strlen($news["description"]) < 200){
                              print(strip_tags($news["description"]));
                          }else{
                              print(substr(strip_tags($news["description"]), 0, 200).'...');
                          }
                  ?>
                            </div>
                            <div class="news-by">
                                <?php
                            if($news['source']!=null){
                            print( '<span>By '.$news['source'].'</span> <span>'.date("F,d Y",strtotime($news['publish_date'])).'</span>');
                            }else{
                            print('<span>'.date("F,d Y",strtotime($news['publish_date'])).'</span>');
                            }
                    ?>
                            </div>
                    </div>
                </div>
            </div>
            <!-- /Popular Search query items -->
        </div>
        <script>
        // Autosuggest on Apps and searchresult pages.
        $(function() {
            $(".autoSuggest").autocomplete({
                source: "../../../controllers/search/autoSuggestController.php?appJsonId=<?php echo $appId; ?>",
                minLength: 1
            });
        });

        $(function(){
            $('.xssValidation').on("keydown", function(e){
                if (e.shiftKey && (e.which == 188 || e.which == 190)) {
                    e.preventDefault();
                }
            });
        });
        </script>
