<?php
die;
session_start();
$json = file_get_contents('https://blockexplorer.com/api/addr/'.$searchtBlock);
$dataUser = json_decode($json,true);
//total value count
$totalValue = $dataUser['totalReceived'] + $dataUser['totalSent'];
//this is if the result found
$blockTransUserStatus = empty($dataUser['addrStr']) ? 'Y' : 'N';
if($blockTransUserStatus == "N"){ ?>
    <div class="row blockstran2">
            <!--user Address details-->
            <div class="col-xs-12">
                <div class="">
                    <label>Address :</label>
                    <p><?php echo ($dataUser['addrStr']); ?></p>

                </div>
            </div>

            <!--user Summary details-->
            <div class="col-xs-12">
                <div class="">
                    <h4>Summary</h4>
                </div>

                <div class="row margin-none">
                    <div class="col-xs-6 leftr">
                        <div class="row margin-none">
                            <label class="col-xs-3">Total Transactions</label>
                            <div class="8">#<?php echo ($dataUser['txApperances']); ?></div>
                        </div>

                        <div class="row margin-none">
                            <label class="col-xs-3">Total Value</label>
                            <div class="8">
                                <?php	if ($totalValue > $maxPriceDispCompare){	?>

                                    <a style="color:#005580;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($totalValue, 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($totalValue, 8) . " IT$"; 	?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                <?php	}else{	?>

                                    <a style="color:#005580;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($totalValue, 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($totalValue, 8)). " IT$";?>">
                                        <?php echo (number_format($totalValue, 8)). " IT$";	?> </a>

                                <?php	}	?>
                            </div>
                        </div>
                    </div>


                    <div class="col-xs-6 right">
                        <div class="row margin-none">
                            <label class="col-xs-3">Total Received</label>
                            <div class="8">
                                <?php	if ($dataUser['totalReceived'] > $maxPriceDispCompare){		?>

                                    <a style="color:green;" href="javascript:;" class="text-success Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['totalReceived'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($dataUser['totalReceived'], 8) . " IT$"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                <?php	}else{	?>

                                    <a style="color:green;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($dataUser['totalReceived'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($dataUser['totalReceived'], 8)). " IT$";?>"><?php echo (number_format($dataUser['totalReceived'], 8)). " IT$";	?> </a>

                                <?php	}	?>
                            </div>
                        </div>

                        <div class="row margin-none">
                            <label class="col-xs-3">Total Sent</label>
                            <div class="8">
                                <?php	if ($dataUser['totalSent'] > $maxPriceDispCompare){	?>

                                    <a style="color:#CE5050;" href="javascript:;" class="text-red Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['totalSent'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($dataUser['totalSent'], 8) . " IT$"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                <?php	}else{	?>

                                    <a style="color:#CE5050;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($dataUser['totalSent'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($dataUser['totalSent'], 8)). " IT$";?>"><?php echo (number_format($dataUser['totalSent'], 8)). " IT$";?> </a>

                                <?php }	?>
                            </div>
                        </div>

                        <div class="row margin-none">
                            <label class="col-xs-3">Final Balance</label>
                            <div class="8">
                                <?php	if ($dataUser['balance'] > $maxPriceDispCompare){	?>

                                    <a style="color:#4ac3e7;" href="javascript:;" class="text-primary Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['balance'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($dataUser['balance'], 8) . " IT$"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                <?php	}else{	?>

                                    <a style="color:#4ac3e7;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($dataUser['balance'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($dataUser['balance'], 8)). " IT$";?>"><?php echo (number_format($dataUser['balance'], 8)). " IT$";	?> </a>

                                <?php	} ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        <!--col-xs-12-->
        <div class="col-xs-12">
            <div class="">
                <label>Transactions</label>
            <table  cellspacing="0" class="users" id="blockUserTransPagination">
                <tbody>
                <!-- first hash loop tab --->
                <?php
                $json = file_get_contents('https://blockexplorer.com/api/txs?address='.$searchtBlock.'&pageNum=0');
                $dataUserTrans = json_decode($json,true);
                $j=0;
                foreach($dataUserTrans['txs'] as $userTransData)
                {
                    $btime = $userTransData['time'];
                    $htime = gmdate('d-m-Y | H:i:s', $btime);
                    $j++;
                    ?>
                    <tr>
                        <td>
                            <div class="td1">
                                <div class="tdleft"><a href="javascript:;" onClick="ajaxBlockSingleTrans('<?php echo $userTransData['txid']; ?>');"><?php echo $userTransData['txid']; ?></a></div>
                                <div class="tdright"><?php if(empty ($htime)) { echo 'No Time Generated'; }else{ echo $htime; } ?> </div>
                            </div>
                            <!-----title-->
                            <div class="td2">
                                <div class="tdleft1">
                                    <?php
                                    $i = 0; $dis ='';$tdleft12=''; $vinStatus = "N";
                                    foreach($userTransData['vin'] as $inputData)
                                    {
                                        $i++;
                                        if($i > 5){
                                            $vinStatus = "Y";
                                            $dis = "style = 'display:none;'";
                                            $tdleft12 = "transdrag$j";
                                        }
                                        if(!empty($inputData['addr'])){
                                            $transUser_id = "'".$inputData['addr']."'";
                                            ?>
                                            <div id= "vin<?php echo $j; ?>" class="tdleft12 <?=$tdleft12;?>" <?php echo $dis; ?>><?php if($inputData['addr'] == $dataUser['addrStr'] ) { echo $inputData['addr']; }else{ echo '<a  href="javascript:;" onClick="ajaxBlockUserTrans('.$transUser_id.');">'.($inputData['addr']).'</a>'; } ?> </div>
                                        <?php }else { ?>
                                            <div class="tdleft12">No Inputs (Newly Generated Coins)</div>
                                        <?php }
                                    }
                                    if($userTransData['valueOut'] < 0){
                                        echo "<div class='tdright2' ><img src='images/images2.0/right_red.png' style='height: 23px;'></div>";
                                    }else{
                                        echo "<div class='tdright2' ><img src='images/images2.0/right_green.png' style='height: 23px;'></div>";
                                    }	?>
                                </div>
                                <!-- End --->
                                <!-- Output hash loop tab --->
                                <div class="tdright1">
                                    <?php
                                    $i = 0; $dis ='';$tdleft12=''; $voutStatus = "N";
                                    foreach($userTransData['vout'] as $outputData)
                                    {
                                        $i++;
                                        if($i > 5){
                                            $voutStatus = "Y";
                                            $dis = "style = 'display:none;'";
                                            $tdleft12 = "transdrag$j";
                                        }
                                        if(!empty($outputData['scriptPubKey']['addresses'][0])){
                                            $transUserOut_id = "'".$outputData['scriptPubKey']['addresses'][0]."'";	?>

                                            <div id= "vin<?php echo $j; ?>" class="tdleft2 <?=$tdleft12;?>" <?php echo $dis; ?>><?php if($outputData['scriptPubKey']['addresses'][0] == $dataUser['addrStr'] ) { echo $outputData['scriptPubKey']['addresses'][0]; }else{ echo '<a href="javascript:;" onClick="ajaxBlockUserTrans('.$transUserOut_id.');">'.($outputData['scriptPubKey']['addresses'][0]).'</a>'; } ?> </div>

                                        <?php }else { ?>

                                            <div class="tdleft12">No Output (Newly Generated Coins)</div>

                                        <?php } ?>

                                        <div id="vin<?php echo $j; ?>" class="tdright2 <?=$tdleft12;?>" <?php echo $dis; ?>>

                                            <?php	if ($outputData['value'] > $maxPriceDispCompare){?>

                                                <a style="font-weight: bold;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['valueOut'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($outputData['value'], 8) . " IT$";?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                            <?php	}else{ ?>

                                                <a style="font-weight: bold;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($outputData['value'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($outputData['value'], 8)). " IT$";?>"><?php echo (number_format($outputData['value'], 8)). " IT$";?> </a>

                                            <?php 	}	?>
                                        </div>
                                    <?php	}	?>
                                </div>
                                <!-- End --->
                                <!-- Total output transaction amount --->
                                <div class="con" >
								<span class="btnt">
									<?php	if ($userTransData['valueOut'] > $maxPriceDispCompare){		?>

                                        <a  href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($userTransData['valueOut'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($userTransData['valueOut'], 8) . " IT$"; ?>">						<?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                                    <?php 	}else{	?>

                                        <a  href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($userTransData['valueOut'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($userTransData['valueOut'], 8)). " IT$";?>"><?php echo (number_format($userTransData['valueOut'], 8)). " IT$";	?> </a>

                                    <?php	} ?>
									</span><br><br>
                                    <!-- Total Confirmations --->
                                    <?php if(!empty($userTransData['confirmations'])){?>

                                        <span class="btnt1"><?php echo $userTransData['confirmations']; ?> Confirmations</span>

                                    <?php }else { ?>

                                        <span class="btnt2"> Unconfirmed Transaction!</span>

                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                            if( ($vinStatus == "Y") || ($voutStatus == "Y") ) {
                                echo "<span class='showme' id='show_more".$j."'>Show More...</span>";
                            }
                            ?>
                        </td>
                    </tr>
                <?php }	?>
                <!-- End main loop--->
                </tbody>
            </table>
            <?php
            $transactions = $dataUser['txApperances'];
            $rawTransaction = $transactions/10;
            $blockUsertransactionN0 = ceil($rawTransaction);
            $blockUserTransfileName = basename($_SERVER["SCRIPT_FILENAME"], '.php');
            ?>
            <div id="blockUserLoadMore"><center><input type = "button" value="Load More Data" class="btn btn-sm btn-lg btn-block" style="background-color:#2fa4d7; color:white;"></center></div>
        </div>
            </div>
        <!--user transaction table -->
    </div>
<?php }else{
    echo "<div style='margin-top:100px;'><div style='width: 80%; margin-top: 52px;margin-left: 159px;margin:auto;margin-top: 96px;'><h1> No matching results found !!! </h1></div><div style=' width: 80%; margin-top: 52px;margin-left: 159px;margin:auto; '> <h4>Please check the spelling or try different keywords </h4> </div></div>";
} ?>
<script src="../../../js/search.js"></script>
<script>
    var pageNo = 0;
    var userAdd ='<?php echo $searchtBlock; ?>';
    var blockUserTransfileName ='<?php echo $blockUserTransfileName; ?>';
    var blockUsertransactionN0 ='<?php echo $blockUsertransactionN0; ?>';
    var blockUserTransAjaxNo = 0;
    var transactions='<?php echo $transactions; ?>';

    //ajax calls to block trans
    ajaxBlockSingleTrans(transd_id);
    ajaxBlockUserTrans(transUser_id);
    ajaxBlockUserTransOuT(transUserOut_id);

    //show hide js
    $(".showme").on('click', function() {
        var click_id = ($(this).attr('id'));
        var id_no = click_id.slice(9);
        var without_id_no = click_id.slice(0, 9);
        $(".transdrag"+id_no).toggle(50);
        if(without_id_no == 'show_more'){
            $("#show_more"+id_no).attr('id', 'show_less'+id_no);
            $("#show_less"+id_no).html('Show Less...');
        }else{
            $("#show_less"+id_no).attr('id', 'show_more'+id_no);
            $("#show_more"+id_no).html('Show More...');
        }
    });
</script>