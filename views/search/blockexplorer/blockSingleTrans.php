<?php
die;
session_start();
$json = file_get_contents('https://blockexplorer.com/api/tx/'.$searchtBlock);
$dataUser = json_decode($json,true);

//this is if the result found
$blockSingleTransStatus = empty($dataUser['txid']) ? 'Y' : 'N';
if($blockSingleTransStatus == "N") {
    $valueIn = empty($dataUser['valueIn']) ? 0 : $dataUser['valueIn'];
    $valueOut = empty($dataUser['valueOut']) ? 0 : $dataUser['valueOut'];
    $valueFee = empty($dataUser['fees']) ? 0 : $dataUser['fees'];
    $totalValue = $valueIn + $valueOut;
    $finalValue = $valueIn - $valueOut;

//time set
    $btime = $dataUser['time'];
    $htime = gmdate('d-m-Y | H:i:s', $btime);
    $finalValue_css = '';
    if($finalValue < 0){
        $finalValue_css ='red';
    }else{
        $finalValue_css ='#4ac3e7';
    }?>
    <div class="row blockstran2">

        <!--=============== TRANSACTIONS HASH ====================-->
        <div class="row">
            <label class="col-xs-4">
                Transaction Hash :
            </label>
            <div class="col-xs-8">
                <p>
                    <?php echo ($dataUser['txid']); ?>
                </p>
            </div>
        </div>
        <!--row-->

        <!--=============== SUMMARY ====================-->

        <div class="row">
            <div class="col-xs-12">
                <h5 class="">Summary</h5>
            </div>

            <div class="col-xs-6 left">
                <div class="row">
                    <label class="col-xs-4">
                        Block Time
                    </label>
                    <div class="col-xs-8">
                        <p class="text-primary">
                            <?php echo $htime ; ?>
                        </p>
                    </div>
                </div>
                <!--block time-->

                <div class="row">
                    <label class="col-xs-4">
                        Size
                    </label>
                    <div class="col-xs-8">
                        <p class="text-primary">
                            <?php echo ($dataUser['size']/1000)." KB"; ?>
                        </p>
                    </div>
                </div>
                <!--Size-->

                <div class="row">
                    <label class="col-xs-4">
                        Total Value
                    </label>
                    <div class="col-xs-8">
                        <?php	if ($totalValue > $maxPriceDispCompare){ ?>
                            <a style="color:#005580;" href="javascript:;" class="text-primary Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($totalValue, 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($totalValue, 8) . " IT$"; ?>">	<?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                        <?php }else{ ?>

                            <a style="color:#005580;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($totalValue, 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($totalValue, 8)). " IT$";?>">
                                <?php echo (number_format($totalValue, 8)). " IT$";	?> </a>
                        <?php } ?>
                    </div>
                </div>
                <!--Total Value-->
            </div>
            <!--col-xs-6-->

            <div class="col-xs-6 right">
                <div class="row">
                    <label class="col-xs-4">
                        Total Received
                    </label>
                    <div class="col-xs-8">
                        <?php	if ($valueIn > $maxPriceDispCompare){ ?>
                            <a style="color:green;" href="javascript:;" class="text-success Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($valueIn, 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($valueIn, 8) . " IT$";
                            ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                        <?php }else{ ?>

                            <a style="color:green;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($valueIn, 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo (number_format($valueIn, 8)). " IT$";?>"><?php echo (number_format($valueIn, 8)). " IT$";?> </a>

                        <?php } ?>
                    </div>
                </div>
                <!--Total Received-->

                <div class="row">
                    <label class="col-xs-4">
                        Total Sent
                    </label>
                    <div class="col-xs-8">
                        <?php	if ($dataUser['valueOut'] > $maxPriceDispCompare){	?>
                            <a style="color:#CE5050;" href="javascript:;" class="text-danger Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['valueOut'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($dataUser['valueOut'], 8) . " IT$"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                        <?php }else{ ?>

                            <a style="color:#CE5050;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($dataUser['valueOut'], 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($dataUser['valueOut'], 8)). " IT$";?>"><?php echo (number_format($dataUser['valueOut'], 8)). " IT$";?> </a>
                        <?php }?>
                    </div>
                </div>
                <!--Total Sent-->

                <div class="row">
                    <label class="col-xs-4">
                        Final Balance
                    </label>
                    <div class="col-xs-8">
                        <?php	if ($dataUser['valueOut'] > $maxPriceDispCompare){	?>
                            <a style="color:<?php echo $finalValue_css; ?>"   href="javascript:;" class="text-red Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ IT$';?>" data-original-title="<?php echo (number_format($dataUser['valueOut'], 8)).' IT$';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($finalValue, 8) . " IT$"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ IT$"; ?> </a>

                        <?php }else{ ?>

                            <a style="color:<?php echo $finalValue_css; ?>"  href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($finalValue, 8)).' IT$';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($finalValue, 8)). " IT$";?>"><?php echo (number_format($finalValue, 8)). " IT$";?> </a>
                        <?php } ?>
                    </div>
                </div>
                <!--block time-->
            </div>
            <!--col-xs-6-->
        </div>
        <!--row-->
    </div>
<?php }else{
    echo "<div style='margin-top:100px;'><div style='width: 80%; margin-top: 52px;margin-left: 159px;margin:auto;margin-top: 96px;'><h1> No matching results found !!! </h1></div><div style=' width: 80%; margin-top: 52px;margin-left: 159px;margin:auto; '> <h4>Please check the spelling or try different keywords </h4> </div></div>";
} ?>

<script src="../../../js/search.js"></script>
<script type="text/javascript">
    //ajax calls user trans input and output
    ajaxBlockSingleTrans(transUser_id);
    ajaxBlockUserTransOuT(transUserOut_id);
</script>