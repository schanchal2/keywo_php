<?php
die;
//block number or hash id
error_reporting(0);
session_start();

$hashCode = "";
if($dataStatus == 'B'){
    $blockJson = file_get_contents('https://blockexplorer.com/api/block-index/'.$searchtBlock);
    $dataBlockHight = json_decode($blockJson, true);
    $hashCode = $dataBlockHight['blockHash'];
}else{
    $hashCode = $searchtBlock;
}
$hashJson = file_get_contents('https://blockexplorer.com/api/block/'.$hashCode);
$data = json_decode($hashJson,true);
$rewardValue = empty($data['reward']) ? 0 : $data['reward'];
//this is if the result found
$blockTransStatus = empty($data['hash']) ? 'Y' : 'N';
if($blockTransStatus == "N") {
//time set

    $btime = $data['time'];
    $htime = gmdate('d-m-Y | H:i:s', $btime);
//all transations openssl_pkey_get_details(key)
    $transactionJson = file_get_contents('https://blockexplorer.com/api/txs/?block='.$hashCode.'&pageNum=0');
    $dataTrans = json_decode($transactionJson, true);
    $countDataIn = 0;
    $countDataOut = 0;
    foreach($dataTrans['txs'] as $userTransData)
    {
        $inValue = empty($userTransData['vin']['n']) ? 0 : $userTransData['valueIn']['n'];
        $outValue = empty($userTransData['valueOut']) ? 0 : $userTransData['valueOut'];
        $countDataIn = $countDataIn + $inValue;
        $countDataOut = $countDataOut + $outValue;
    }
    //total value count
    $totalcount = $countDataIn +$countDataOut ;
?>
    <br>
    <main>
        <div class="container-fluid blockstran2">
            <div class="row">
                <div class="col-xs-7 search_result_blockexplorer_level_2" id="search_result_blockexplorer_level_2">
                    <div class="innerML inner-3x">
                        <!--
                      -->
                        <div class="card innerAll innerMB clearfix leftheadd">
                            <span class="block_num">Block #447617Block #<?php echo ($data['height']); ?></span>
                            <span class="hashmain"> (<?php echo ($data['hash']); ?>)</span>
                        </div>
                        <!-- 
                         -->
                        <div class="card innerTB innerMB clearfix  Summery">
                            <span class="card-title innerLR">  Summary </span>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td> Transactions </td>
                                        <td> #<?php echo count($data['tx']); ?></td>
                                    </tr>
                                    <tr>
                                        <td> Block Time </td>
                                        <td> <?php echo $htime; ?></td>
                                    </tr>
                                    <tr>
                                        <td> Size </td>
                                        <td> <?php echo ($data['size']/1000)." KB"; ?></td>
                                    </tr>
                                    <tr>
                                        <td> Block Reward </td>
                                        <td> 
                                             <?php 

                                             if ($rewardValue > $maxPriceDispCompare){    ?>
                                                <a href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($rewardValue, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($rewardValue, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                                            <?php }else { ?>

                                                <a href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($rewardValue, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php  echo (number_format($rewardValue, 8)). " BTC";?>"><?php echo (number_format($rewardValue, 8)). " BTC";?> </a>
                                            <?php } ?>
                                         </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td> Total Received </td>
                                        <td> <?php  
                                        if ($countDataIn > $maxPriceDispCompare){   ?>
                                                    <a href="javascript:;" class="text-green" data-toggle="tooltip" title=""
                                                     data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($countDataIn, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($countDataIn, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                                                <?php }else { ?>

                                                    <a style="color:green;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($countDataIn, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php     echo (number_format($countDataIn, 8)). " BTC";?>">
                                                        <?php echo (number_format($countDataIn, 8)). " BTC";?> </a>

                            <?php } ?>
                            </td>
                                    </tr>
                                    <tr>
                                        <td> Total Sent </td>
                                        <td>
                                                    <?php   if ($countDataOut > $maxPriceDispCompare){  ?>
                                            <a style="color:#CE5050;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($countDataOut, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($countDataOut, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                                        <?php }else { ?>

                                            <a style="color:#CE5050;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($countDataOut, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php  echo (number_format($countDataOut, 8)). " BTC";?>">
                                                <?php echo (number_format($countDataOut, 8)). " BTC";?> </a>

                                        <?php } ?>
                            </td>
                                    </tr>
                                    <tr>
                                        <td> Total Value </td>
                                        <td> <?php if ($totalcount > $maxPriceDispCompare){ ?>
                                <a style="color:#005580;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($totalcount, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($totalcount, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                            <?php }else { ?>

                                <a style="color:#005580;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($totalcount, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php    echo (number_format($totalcount, 8)). " BTC";?>">
                                    <?php echo (number_format($totalcount, 8)). " BTC";?> </a>

                            <?php }?></td>
                                    </tr>
                                    <tr>
                                        <td> Version </td>
                                        <td><?php echo $data['version']; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="card innerTB innerMB clearfix Transactions ">
                            <span class="card-title innerLR innerMB">  Transactions </span>
                            <ul class="list-unstyled   ">
                                <!-- first hash loop tab  -->
                                <?php
                                $j = 0;
                                foreach($dataTrans['txs'] as $userTransData)
                                {
                                    $inValue = empty($userTransData['valueIn']) ? 0 : $userTransData['valueIn'];
                                    $outValue = empty($userTransData['valueOut']) ? 0 : $userTransData['valueOut'];
                                    $j++;
                                    //time set
                                    $utime = $userTransData['time'];
                                    $uhtime = gmdate('d-m-Y | H:i:s', $utime);
                                    ?>
                                    <li class="innerAll clearfix Transaction innerMB">
                                        <p class="Transaction-id innerTB"><a href="javascript:; onClick="ajaxBlockSingleTrans('<?php echo $userTransData['txid']; ?>');"> <?php echo $userTransData['txid']; ?> </a><span class="pull-right text-dark-gray"> <?php echo ($uhtime); ?></span></p>
                                            <div class="row margin-none">
                                                <div class="col-xs-4">
                                                    <div class="Transaction-From" style="width:initial">
                                                        <!-- input hash loop tab --->
                                                        <ul class="list-unstyled">
                                                            <?php
                                                            $i = 0; $dis ='';$tdleft12=''; $vinStatus = "N";
                                                            foreach($userTransData['vin'] as $inputData)
                                                            {
                                                                $i++;
                                                                if($i > 5){
                                                                    $vinStatus = "Y";
                                                                    $dis = "style = 'display:none;'";
                                                                    $tdleft12 = "transdrag$j";
                                                                }
                                                                if(!empty($inputData['addr'])){?>
                                                                    <li id= "vin<?php echo $j; ?>" <?=$tdleft12;?>" <?php echo $dis; ?>><a   href="javascript:;" onClick="ajaxBlockUserTrans('<?php echo ($inputData['addr']); ?>');"><?php echo ($inputData['addr']); ?></a></li>
                                                                <?php }else { ?>
                                                                    <li class="text-dark-gray">No Inputs (Newly Generated Coins)</li>
                                                                <?php }
                                                            }
                                                            //                                            if($outValue < 0){
                                                            //                                                echo "<div class='tdright2' ><img src='images/images2.0/right_red.png' style='height: 23px;'></div>";
                                                            //                                            }else{
                                                            //                                                echo "<div class='tdright2' ><img src='images/images2.0/right_green.png' style='height: 23px;'></div>";
                                                            //                                            }?>
                                                        </ul>
                                                        <span class="glyphicon glyphicon-menu-right"></span>
                                                    </div>
                                                </div>
                                                <!--col-xs-6-->
                                                <div class="col-xs-8">
                                                    <div class="Transaction-To" style="width: initial;">
                                                        <?php
                                                        $i = 0; $dis ='';$tdleft12=''; $voutStatus = "N";
                                                        foreach($userTransData['vout'] as $outputData)
                                                        {
                                                            $i++;
                                                            if($i > 5){
                                                                $voutStatus = "Y";
                                                                $dis = "style = 'display:none;'";
                                                                $tdleft12 = "transdrag$j";
                                                            }
                                                            if(!empty($outputData['scriptPubKey']['addresses'][0])){
                                                                $transUserOut_id = "'".$outputData['scriptPubKey']['addresses'][0]."'";
                                                                ?>
                                                                <div class="row margin-none">
                                                                <div class="col-xs-8">
                                                                <div id= "vin<?php echo $j; ?>" class="tdleft2 <?=$tdleft12;?>"
                                                                    <?php echo $dis; ?>>
                                                                    <a class=""  href="javascript:;" onClick="ajaxBlockUserTrans(<?php echo $transUserOut_id ;?>);">
                                                                        <?php echo ($outputData['scriptPubKey']['addresses'][0]);?>
                                                                    </a>
                                                                </div>
                                                            <?php }else { ?>
                                                                <p class="text-dark-gray">No Output (Newly Generated Coins)</p>
                                                            <?php } ?>
                                                            </div>
                                                            <div class="col-xs-4 padding-right-none">
                                                                <div class="pull-right" id="vin<?php echo $j; ?>" class="tdright2 <?=$tdleft12;?>" <?php echo $dis; ?>>
                                                                    <?php	if ($outputData['value'] > $maxPriceDispCompare){	?>
                                                                        <a class="pull-right" href="javascript:;" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($outputData['value'], 8) . " BTC";
                                                                        ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>
                                                                    <?php }else { ?>

                                                                        <a style="font-weight: bold;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($outputData['value'], 8)). " BTC";?>"><?php echo (number_format($outputData['value'], 8)). " BTC";?> </a>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>

                                                        <?php	} ?>
                                                        <!-- Total output transaction amount -->

                                                        <span class="totalBTC pull-right innerAll innerMTB" id="vin<?php echo $j; ?>" class="tdright2 <?=$tdleft12;?>" <?php echo $dis; ?>>
                                                <?php	if ($outputData['value'] > $maxPriceDispCompare){	?>
                                                    <a href="javascript:;" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($outputData['value'], 8) . " BTC";
                                                    ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>
                                                <?php }else { ?>

                                                    <a style="font-weight: bold;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($outputData['value'], 8)). " BTC";?>"><?php echo (number_format($outputData['value'], 8)). " BTC";?> </a>
                                                <?php } ?>
                                            </span>
                                                        <div class="clearfix"></div>
                                                        <!-- Total Confirmations --->
                                                        <?php if(!empty($userTransData['confirmations'])){?>

                                                            <span class="Confirmations pull-right innerAll"><?php echo $userTransData['confirmations']; ?> Confirmations</span>

                                                        <?php }else { ?>
                                                            <span class="Confirmations pull-right innerAll"> Unconfirmed Transaction!</span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <!--col-xs-6-->
                                            </div>
                                        <!--row-->


                                        <!-- Output hash loop tab --->

                                        <!-- End --->
                                    </li>

                                <?php } ?>


                            </ul>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </main>
<script src="../../../js/search.js"></script>
<script type="text/javascript">
    var transactions='<?php echo $transactions; ?>';
    var pageNum = 0;
    var transactionNo='<?php echo $transactionN0; ?>';
    var blockTransfileName='<?php echo $blockTransfileName; ?>';
    var blockTransAjaxNo = 0;
    var hashCode='<?php echo $hashCode; ?>';
    var blockTransError = "blockTransError";
    var data1=null;

    ajaxBlockSingleTrans(transd_id);
    ajaxBlockUserTrans(transUser_id);
    ajaxBlockUserTransOuT(transUserOut_id);

    //show hide js
    $(".showme").on('click', function() {
        var click_id = ($(this).attr('id'));
        var id_no = click_id.slice(9);
        var without_id_no = click_id.slice(0, 9);
        $(".transdrag"+id_no).toggle(50);
        if(without_id_no == 'show_more'){
            $("#show_more"+id_no).attr('id', 'show_less'+id_no);
            $("#show_less"+id_no).html('Show Less...');
        }else{
            $("#show_less"+id_no).attr('id', 'show_more'+id_no);
            $("#show_more"+id_no).html('Show More...');
        }
    });
</script>
