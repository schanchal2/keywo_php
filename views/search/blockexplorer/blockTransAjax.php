<?php

die;
//block number or hash id
session_start();
$hashCode = $_POST['blockHash'];
$hashJson = file_get_contents('https://blockexplorer.com/api/block/'.$hashCode);
$data = json_decode($hashJson,true);
$rewardValue = empty($data['reward']) ? 0 : $data['reward'];
$blockTransStatus = empty($data['hash']) ? 'Y' : 'N';
if($blockTransStatus == "N") {
//time set
    $btime = $data['time'];
    $htime = gmdate('d-m-Y | H:i:s', $btime);
//all transations details
    $transactionJson = file_get_contents('https://blockexplorer.com/api/txs/?block='.$hashCode.'&pageNum=0');
    $dataTrans = json_decode($transactionJson,true);
    $countDataIn = 0;
    $countDataOut = 0;
    foreach($dataTrans['txs'] as $userTransData)
    {
        $inValue = empty($userTransData['valueIn']) ? 0 : $userTransData['valueIn'];
        $outValue = empty($userTransData['valueOut']) ? 0 : $userTransData['valueOut'];
        $countDataIn = $countDataIn + $inValue;
        $countDataOut = $countDataOut + $outValue;
    }
    //total value count
    $totalcount = $countDataIn +$countDataOut ;
    $maxPriceDisplay = 1000;
    $maxPriceDispCompare = 999.99999999;
    ?>
    <div class="blockstran2">
        <!--Transactions table detail dalog -->
        <div class="leftdett">
            <div class="leftheadd">
                <span class="block_num">Block #<?php echo ($data['height']); ?></span>
                <span class="hashmain"> (<?php echo ($data['hash']); ?>)</span>
            </div>
            <div class="row transd">
                <div class="summaray">Summary</div>
                <div class="col-sm-6 leftd">
                    <div class="col-sm-6 hashd1">
                        <p>Transactions</p>
                        <p>Block Time</p>
                        <p>Size</p>
                        <p>Block Reward</p>
                    </div>
                    <div class="col-sm-6 hashd2" >
                        <p>#<?php echo count($data['tx']); ?></p>
                        <p><?php echo $htime; ?></p>
                        <p><?php echo ($data['size']/1000)." KB"; ?></p>
                        <p>
                            <?php	if ($rewardValue > $maxPriceDispCompare){	?>
                                <a href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($rewardValue, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($rewardValue, 8) . " BTC";?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                            <?php	}else{	?>

                                <a href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($rewardValue, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($rewardValue, 8)). " BTC";?>">
                                    <?php echo (number_format($rewardValue, 8)). " BTC";?> </a>

                            <?php	}	?>
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="col-sm-6 hashd1" >
                        <p>Total Received </p>
                        <p>Total Sent </p>
                        <p>Total Value</p>
                        <p>Version </p>
                    </div>
                    <div class="col-sm-6 hashd2 " >
                        <p>
                            <?php	if ($countDataIn > $maxPriceDispCompare){	?>

                                <a style="color:green;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($countDataIn, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($countDataIn, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                            <?php	}else{	?>

                                <a style="color:green;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($countDataIn, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($countDataIn, 8)). " BTC";?>">
                                    <?php echo (number_format($countDataIn, 8)). " BTC";?> </a>

                            <?php } ?>

                        </p>
                        <p>
                            <?php	if ($countDataOut > $maxPriceDispCompare){	?>

                                <a style="color:#CE5050;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($countDataOut, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($countDataOut, 8) . " BTC"; ?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                            <?php }else{ ?>

                                <a style="color:#CE5050;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($countDataOut, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($countDataOut, 8)). " BTC";?>">
                                    <?php echo (number_format($countDataOut, 8)). " BTC";?> </a>

                            <?php	}	?>
                        </p>
                        <p>
                            <?php	if ($totalcount > $maxPriceDispCompare){	?>

                                <a style="color:#005580;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($totalcount, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($totalcount, 8) . " BTC"; 	?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                            <?php	}else{	?>

                                <a style="color:#005580;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($totalcount, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($totalcount, 8)). " BTC";?>">
                                    <?php echo (number_format($totalcount, 8)). " BTC";	?> </a>

                            <?php	}	?>

                        </p>
                        <p><?php echo ($data['version'])?></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="rightdet1">
            <div class="trans">Transactions</div>
            <table  cellspacing="0" class="users" id="blockTransAjaxPagination">
                <tbody>
                <!-- first hash loop tab --->
                <?php
                $j = 0;
                foreach($dataTrans['txs'] as $userTransData)
                {
                    $inValue = empty($userTransData['valueIn']) ? 0 : $userTransData['valueIn'];
                    $outValue = empty($userTransData['valueOut']) ? 0 : $userTransData['valueOut'];
                    $j++;
                    //time set
                    $utime = $userTransData['time'];
                    $uhtime = gmdate('d-m-Y | H:i:s', $utime);
                    ?>
                    <tr>
                        <td>
                            <div class="td1">
                                <div class="tdleft"><a href="javascript:;" onClick="ajaxBlockSingleTrans('<?php echo $userTransData['txid']; ?>');"><?php echo $userTransData['txid']; ?></a></div>
                                <div class="tdright"><?php echo ($uhtime); ?></div>
                            </div>
                            <div class="td2">
                                <!-- input hash loop tab --->
                                <div class="tdleft1">
                                    <?php
                                    $i = 0; $dis ='';$tdleft12=''; $vinStatus = "N";
                                    foreach($userTransData['vin'] as $inputData)
                                    {
                                        $i++;
                                        if($i > 5){
                                            $vinStatus = "Y";
                                            $dis = "style = 'display:none;'";
                                            $tdleft12 = "transdrag$j";
                                        }
                                        if(!empty($inputData['addr'])){?>

                                            <div id= "vin<?php echo $j; ?>" class="tdleft12 <?=$tdleft12;?>" <?php echo $dis; ?>><a  href="javascript:;" onClick="ajaxBlockUserTrans('<?php echo ($inputData['addr']); ?>');"><?php echo ($inputData['addr']); ?></a></div>
                                        <?php }else { ?>

                                            <div class="tdleft12">No Inputs (Newly Generated Coins)</div>

                                        <?php }
                                    }
                                    if($outValue < 0){
                                        echo "<div class='tdright2' ><img src='images/images2.0/right_red.png' style='height: 23px;'></div>";
                                    }else{
                                        echo "<div class='tdright2' ><img src='images/images2.0/right_green.png' style='height: 23px;'></div>";
                                    }?>
                                </div>
                                <!-- End --->
                                <!-- Output hash loop tab --->
                                <div class="tdright1">
                                    <?php
                                    $i = 0; $dis ='';$tdleft12=''; $voutStatus = "N";
                                    foreach($userTransData['vout'] as $outputData)
                                    {
                                        $i++;
                                        if($i > 5){
                                            $voutStatus = "Y";
                                            $dis = "style = 'display:none;'";
                                            $tdleft12 = "transdrag$j";
                                        }
                                        if(!empty($outputData['scriptPubKey']['addresses'][0])){
                                            $transUserOut_id = "'".$outputData['scriptPubKey']['addresses'][0]."'";
                                            ?>
                                            <div id= "vin<?php echo $j; ?>" class="tdleft2 <?=$tdleft12;?>" <?php echo $dis; ?>><a  href="javascript:;" onClick="ajaxBlockUserTrans(<?php echo $transUserOut_id ;?>);"><?php echo ($outputData['scriptPubKey']['addresses'][0]);?></a> </div>

                                        <?php }else { ?>

                                            <div class="tdleft12">No Output (Newly Generated Coins)</div>

                                        <?php } ?>

                                        <div id="vin<?php echo $j; ?>" class="tdright2 <?=$tdleft12;?>" <?php echo $dis; ?>>

                                            <?php if ($outputData['value'] > $maxPriceDispCompare){	?>

                                                <a style="font-weight: bold;" href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($outputData['value'], 8) . " BTC";?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                                            <?php	}else{	?>

                                                <a style="font-weight: bold;" href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($outputData['value'], 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($outputData['value'], 8)). " BTC";?>"><?php echo (number_format($outputData['value'], 8)). " BTC";
                                                    ?> </a>

                                            <?php	}	?>

                                        </div>

                                    <?php	} ?>
                                </div>
                                <!-- End --->
                                <!-- Total output transaction amount --->
                                <div class="con" >
									<span class="btnt">
									<?php 	if ($outValue > $maxPriceDispCompare){	?>

                                        <a  href="javascript:;" class="Hover_color" data-toggle="tooltip" title="" data-placement="bottom" data-old="<?php echo (number_format($maxPriceDisplay, 2)).'+ BTC';?>" data-original-title="<?php echo (number_format($outValue, 8)).' BTC';?>" style="border:none;" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php echo number_format($outValue, 8) . " BTC"; 	?>"><?php echo (number_format($maxPriceDisplay, 2)). "+ BTC"; ?> </a>

                                    <?php	}else{	?>

                                        <a  href="javascript:;" class="" style="border:none;" data-old="<?php echo (number_format($outValue, 8)).' BTC';?>" onclick="convertPrice('<?php echo $_SESSION['curr_pref']; ?>');" origPrice="<?php	 echo (number_format($outValue, 8)). " BTC";?>">
										<?php echo (number_format($outValue, 8)). " BTC";?> </a>

                                    <?php	}	?>

									</span><br><br>
                                    <!-- Total Confirmations --->
                                    <?php if(!empty($userTransData['confirmations'])){?>

                                        <span class="btnt1"><?php echo $userTransData['confirmations']; ?> Confirmations</span>

                                    <?php }else { ?>

                                        <span class="btnt2"> Unconfirmed Transaction!</span>

                                    <?php } ?>
                                </div>
                            </div>
                            <?php
                            if( ($vinStatus == "Y") || ($voutStatus == "Y") ) {
                                echo "<span class='showme' id='show_more".$j."'>Show More...</span>";
                            }
                            ?>
                        </td>
                    </tr>
                <?php	}?>
                <!-- End main loop--->
                </tbody>
            </table>
            <?php
            $transactions = count($data['tx']);
            $rawTransaction = $transactions/10;
            $transactionN0 = ceil($rawTransaction);
            $blockTransfileName = basename($_SERVER["SCRIPT_FILENAME"], '.php');
            ?>
            <div id="blockAjaxLoadMore"><input type = "button" value="Load More Data" class="btn btn-sm btn-lg btn-block" style="background-color:#2fa4d7; color:white;"></div>
            <div id='loader-icon'><img src='images/images2.0/LoaderIcon.gif' /></div>
        </div>
    </div>
<?php }else{
    echo "<div style='margin-top:100px;'><div style='width: 80%; margin-top: 52px;margin-left: 159px;margin:auto;margin-top: 96px;'><h1> No matching results found !!! </h1></div><div style=' width: 80%; margin-top: 52px;margin-left: 159px;margin:auto; '> <h4>Please check the spelling or try different keywords </h4> </div></div>";
} ?>
<script src="../../../js/search.js"></script>
<script type="text/javascript">
    var transactions='<?php echo $transactions; ?>';
    var pageNum = 0;
    var transactionNo='<?php echo $transactionN0; ?>';
    var blockTransfileName='<?php echo $blockTransfileName; ?>';
    var blockTransAjaxNo = 0;
    var hashCode='<?php echo $hashCode; ?>';

    //ajax calls to trans
    ajaxBlockSingleTrans(transd_id);
    ajaxBlockUserTrans(transUser_id);
    ajaxBlockUserTransOuT(transUserOut_id);

    //show hide js
    $(".showme").on('click', function() {
        var click_id = ($(this).attr('id'));
        var id_no = click_id.slice(9);
        var without_id_no = click_id.slice(0, 9);
        $(".transdrag"+id_no).toggle(50);
        if(without_id_no == 'show_more'){
            $("#show_more"+id_no).attr('id', 'show_less'+id_no);
            $("#show_less"+id_no).html('Show Less...');
        }else{
            $("#show_less"+id_no).attr('id', 'show_more'+id_no);
            $("#show_more"+id_no).html('Show More...');
        }
    });
</script>