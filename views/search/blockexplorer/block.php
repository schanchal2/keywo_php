<?php
die;
?>

<div class="col-xs-6 search_result_blockexplorer">
    <div class="innerML inner-3x">
        <p class="text-dark-gray"> Latest Blocks</p>
        <table class="table display text-center">
            <thead>
            <tr>
                <th class="rby1">Blocks Id</th>
                <th class="rby">Block Created</th>
                <th class="rby">Transactions</th>
                <th class="rby">Total Size</th>
                <th class="rby">Relayed By</th>
            </tr>
            </thead>


            <tbody>
            <?php
            $json = file_get_contents('https://blockexplorer.com/api/blocks');
            $data = json_decode($json,true);

            $i=0;
            foreach($data['blocks'] as $LatestBlock)
            {
                $i++;

                //time set
                $btime = $LatestBlock['time'];
                $htime = gmdate('d-m-Y | H:i:s', $btime);

                ?>

                <tr>
                    <td class="rbyt text-left">
                        <span class="bitd">
                           <a  href="javascript:;" onClick="ajaxBlockTrans('<?php echo $LatestBlock['hash'];?>')"><?php echo ($LatestBlock['height']); ?></a>
                        </span>
                    </td>
                    <td class="rby">
                        <span class="date_t"><?php echo $htime; ?></span>
                    </td>
                    <td class="rby">
                        <span class="transt" ><?php echo ($LatestBlock['txlength']); ?></span>
                    </td>
                    <td class="rby">
					<span class="transt"><?php echo ($LatestBlock['size']/1000)." KB"; ?></span>
                    </td>
                    <?php
                        if(!empty($LatestBlock['poolInfo']['url'])){?>
                            <td  class="tdrby">
                                <span>
                                    <a target="_blank" href="<?php echo ($LatestBlock['poolInfo']['url']); ?>"><?php echo ($LatestBlock['poolInfo']['poolName']); ?></a>
                                </span>
                            </td>
                        <?php }else { ?>
                            <td  class="tdrby">
                                <span >- Empty -</span>
                            </td>
                        <?php } ?>
                </tr>
                <?php
                if($i == 10){
                    break;
                }
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<script type="text/javascript">

    //ajax call to trans
    function ajaxBlockTrans(trans_id) {
        $.ajax({
            url: 'blockTransAjax.php',
            type: 'POST',
            data: ({ blockHash:trans_id }),
            success: function(data){
                $(".replaceDiv").empty();
                $(".replaceDiv").html(data);
                $("html, body").animate({ scrollTop: 0 }, 400);

            }
        });
    }
</script>