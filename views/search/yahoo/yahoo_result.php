<!-- API Calling Code here -->
<?php
die;
require_once ('../../../helpers/stringHelper.php');
error_reporting(0);

//get query string from url
$query = cleanXSS(urldecode($_REQUEST['q']));

// condition inputs/set default
if (isset($_REQUEST['pageNo'])) {
    $pageNo = $_REQUEST['pageNo'];
} else {
    $pageNo = 11;
}?>
<div>
    <?php
    if(isset($query) && $query != ""){
        $totalRows = 101;
        $rowsPerPage = 10;
        $numRows = $totalRows;
        $lastPage = 91;

        // validate/limit requested $pageno
        $pageNo = (int)$pageNo;
        if ($pageNo > $lastPage) {
            $pageNo = $lastPage;
        }
        if ($pageNo < 1) {
            $pageNo = 1;
        }
        //Find start and end array index that corresponds to the requested pageNo
        $start = ($pageNo - 1) * $rowsPerPage;
        $end = $start + $rowsPerPage - 1;

        //limit $end to highest array index
        if($end > $numRows - 1){
            $end = $numRows - 1;
        }
        $pageLimit=10;
        $url="https://search.yahoo.com/search?p=".rawurlencode($query)."&b=".$pageNo."&pz=".$pageLimit."&xargs=0";
        $result=file_get_contents($url);
        $doc = new DOMDocument();
        @$doc->loadHTML($result);

        //get image tags from HTML page
        $links = $doc->getElementById("results")->textContent;
        $end = '{display:none!important}';
        $res=substr($links, -24);
        if($res === $end) { ?>
            <div style="width: 100%; float: left;">
                <h1>No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>
            </div>
            <?php
        } else{
            //get updated links and image tags
            $content=$doc->saveHTML();
            print_r($content);
            if(!strpos($content, 'We did not find results') !== false){
                echo "<div id='paginate' style='text-align:center;' class='pagebtn-group'>";
                //first/prev pagination hyperlinks
                if ($pageNo == 11) {
                    //echo " FIRST PREV ";
                } else {
                        $firstPgBtn = "First";
                        $prevPgBtn = "Previous";
                    echo " <button type='button' class='pagebtn' onclick='loadPageYahoo(11, \"$query\")'>$firstPgBtn</i></button> ";
                    $prevPage = $pageNo-10;
                    echo " <button type='button' class='pagebtn' onclick='loadPageYahoo($prevPage, \"$query\")'>$prevPgBtn</i></button> ";
                }
                $paginationStart = (($pageNo-100) <= 0)?11:($pageNo - 100);
                $paginationEnd = (($pageNo+100) <= $lastPage)?($pageNo + 100):$lastPage;
                for($paginationIterator = $paginationStart; $paginationIterator <= $paginationEnd; $paginationIterator += 10){
                    $class = "pagebtn-warning";
                    if($paginationIterator == $pageNo)
                        $class = "pagebtn-success";
                    if($paginationIterator != '1'){
                        //get the page numbers
                        $page= substr($paginationIterator, 0, 1);
                    }
                    echo " <button type='button' class='pagebtn  $class' onclick='loadPageYahoo($paginationIterator, \"$query\")' >$page</button>";
                }

                //next/last pagination hyperlinks
                if ($pageNo == $lastPage) {
                    //echo " NEXT LAST ";
                } else {
                    $nextPage = $pageNo + 10;
                        $nextPgBtn = "Next";
                        $lastPgBtn = "Last";
                    echo "&nbsp;<button onclick='loadPageYahoo($nextPage, \"$query\")' type='button' class='pagebtn'>$nextPgBtn</button> ";
                    echo "<button onclick='loadPageYahoo($lastPage, \"$query\")' type='button' class='pagebtn'>$lastPgBtn</i></button> ";
                }
                echo "</div>";
                echo "<br>";
            }

        }
    }
    ?>
</div>

<script>
    $(document).ready(function(){
        $('#b_results .b_algo a').add('#b_results .b_ans a').add('#b_results .b_ad a').click(function(){
            window.open(this.href);
            return false;
        });
        $(this).scrollTop(0);
    });
    function loadPageYahoo(pageNo, searchQuery){
            url = 'yahoo_result.php';

        $.ajax({
            type:'GET',
            url:url,
            data: {pageNo:pageNo, q:searchQuery},
            dataType:'html',
            async:true,
            success: function(data){
                $("#searchContainer").css('display','none');
                $("#categoriesResultContainer").css('display','block');
                $("#categoriesResultContainer").html('');
                $("#categoriesResultContainer").html(data);
            },
        });
    }
</script>