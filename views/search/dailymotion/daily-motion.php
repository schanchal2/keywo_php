<?php
die;
session_start();

// include required files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../models/search/searchLandingModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');

error_reporting(0);
// connecting to keywords Database
$kwdConn = createDbConnection('dbkeywords');

if(noError($kwdConn)){
  $kwdConn = $kwdConn["connection"];
}else{
  print("Keyword Database Error");
  exit();
}


//session existence
checkForSession($kwdConn);

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
  $email = $_SESSION["email"];
  $login_status = 1;
}else{
  $login_status = 0;
}

$appId = 9;

// connecting to keywords Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
  $connSearch = $connSearch["connection"];
  // fetching app detail from search database in sc_app_details.
  $appDetails = getAppDetails($connSearch, $appId);

  if(noError($appDetails)){
    $appDetails = $appDetails["errMsg"][0];
    $appLogo = $appDetails["searchengine_images"];
    $appName = $appDetails["app_name"];
    $appURL  = $appDetails["searchresult_url"];
    $landingPage = $appDetails["landingPage_url"];
  }else{
    print("Error: Fetching App Details");
  }
}else{
  print("Error: Database connection");
  exit;
}

//include header file
include("../../layout/header.php");
?>
<main class="innerT inner-7x">
  <div class="container">
  <!-- Call Landing page here-->
  <div class="loadLandingPage"></div>
  <div class="row">
    <?php include('../appFooter.php');?>
  </div>
  <div class="row">
    <div class="landing-page-info-boxes">
      <div class="col-xs-4">
        <div class="card">
          <div class="innerAll text-center text-deep-sky-blue innerT margin-bottom-none">
            <strong>REFER AND EARN</strong>
          </div>
          <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray info-text">
            Refer your friend to searchtrade and earn income when they search or buy keywords
          </p>
          <div class="row innerB">
            <div class="col-xs-6">
              <div class="text-red text-center">
                <strong class="search-landing-buttons">REFER NOW</strong>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="text-center">
                <strong class="search-landing-buttons"><a>LEARN MORE</a></strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-4">
        <div class="card">
          <div class="innerAll text-center text-deep-sky-blue margin-bottom-none">
            <strong>INVEST IN KEYWORDS</strong>
          </div>
          <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray info-text">
            Buy search keywords and earn residual income everytime word you own is searched
          </p>
          <div class="row innerB">
            <div class="col-xs-6">
              <div class="text-red text-center">
                <strong class="search-landing-buttons">BUY NOW</strong>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="text-center">
                <strong class="search-landing-buttons"><a>LEARN MORE</a></strong>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-xs-4">
        <div class="card">
          <div class="innerAll text-center text-deep-sky-blue margin-bottom-none">
            <strong>ANDROID APP</strong>
          </div>
          <p class="innerAll text-center padding-top-none margin-bottom-none text-dark-gray info-text">
            Download Android app today and experiance searchtrade on your mobile
          </p>
          <div class="row innerB">
            <div class="col-xs-12">
              <div class="text-center">
                <strong class="search-landing-buttons"><a>DOWNLOAD NOW</a></strong>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</main>
<?php include '../../layout/after_login_footer.php' ?>
<!-- Script for JavaScript -->
<script type="text/javascript">
  $(document).ready(function() {
    var rootUrl       = '<?php echo $rootUrl; ?>';
    var loginStatus   = '<?php echo $login_status; ?>';
    var appId         = '<?php echo $appId; ?>';
    var searchRootUrl = '<?php echo $searchRootUrl; ?>';
    var landing       = '<?php echo $landingPage; ?>';
    var appLogo       = '<?php echo $appLogo;  ?>';
    var appName       = '<?php echo $appName; ?>';
    var appURL        = '<?php echo $appURL; ?>';
    searchLandingAjax(loginStatus, searchRootUrl, landing, appId,  rootUrl, appLogo, appName, appURL);
  });
</script>
<script src="../../../js/search.js"></script>
