<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
error_reporting(E_All);
// connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
$connSearch = $connSearch["connection"];
}else{
print("Search Database Error");
exit();
}
global $docRoot;
$page = array();
$kwdArr = array();
//get query string from ur
$searchkey = urldecode($_REQUEST['q']);
$kwdArr = explode(" ",  $searchkey);
$key = implode("_",$kwdArr);
$filename = $key . '.json'; // Create search keyword filename
// Initialize cache file directory
$dir = "{$docRoot}json_directory/dailymotionJsonFiles";
$array2 = array();
foreach($kwdArr as $array1){
$string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
$string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
$test .='%'.$string.'';
array_push($array2, $string);
}
$searchkey = implode(" ", $array2);
$searchkey = cleanDisplayParameter($connSearch, $searchkey);
// Create dirtectory if not exist
if (!is_dir($dir)) {
mkdir($dir, 0777, true); // Create directory if not exixt
}
// Display results according to request
if (isset($_REQUEST['page']) && $_REQUEST['page'] > 5) {
// Hit dailymotion api and store results in array
for ($i = 1; $i <= 5; $i++) {
$chk[] = file_get_contents("https://api.dailymotion.com/videos?search=" . rawurlencode($_REQUEST['q']) . "&page=" . $i . "&limit=20&fields=id,title,description,channel,thumbnail_url,owner.screenname");
$chk[$i - 1] = json_decode($chk[$i - 1], true);
}
$results = json_encode($chk, JSON_UNESCAPED_UNICODE);
file_put_contents($dir . '/' . $filename, $results); // Store Api results as .json file
$results = file_get_contents($dir . '/' . $filename); // Get content from stored file
$results = json_decode($results, true);
;
createImageUrlJson($results, $filename, dirname($dir), 9);
} elseif (file_exists($dir . '/' . $filename)) {
if (time() - filemtime($dir . '/' . $filename) >= 86400) { //Check cache file mor than 1 day
// Delete cache file from server
unlink($dir . '/' . $filename);
for ($i = 1; $i <= 5; $i++) {
$chk[] =  strip_tags(file_get_contents("https://api.dailymotion.com/videos?search=" . rawurlencode($_REQUEST['q']) . "&page=" . $i . "&limit=20&fields=id,title,description,channel,thumbnail_url,owner.screenname"));
$chk[$i - 1] = json_decode($chk[$i - 1], true);
}
$videoId = $chk[0]['list'][0]['id'];
// Get related videos result from api
$relatedVideo = file_get_contents("https://api.dailymotion.com/video/".$videoId."/related?fields=id,title,description,channel,thumbnail_url,owner.screenname&limit=5");
$relatedVideo = json_decode($relatedVideo,true);
$chk['related'] = $relatedVideo;
$results = json_encode($chk, JSON_UNESCAPED_UNICODE);
// Store Api results as .json file
file_put_contents($dir . '/' . $filename, $results);
$results = file_get_contents($dir . '/' . $filename);
$results = json_decode($results, true);
//Create a json file for image URLs for image caching
createImageUrlJson($results,$filename,dirname($dir),9);
} else {
// Get content of stored cache file
$results = file_get_contents($dir . '/' . $filename);
$results = json_decode($results, true);
}
} else {
for ($i = 1; $i <= 5; $i++) {
$chk[] = strip_tags(file_get_contents("https://api.dailymotion.com/videos?search=" . rawurlencode($_REQUEST['q']) . "&page=" . $i . "&limit=20&fields=id,title,description,channel,thumbnail_url,owner.screenname"));
$chk[$i - 1] = json_decode($chk[$i - 1], true);
}
$videoId = $chk[0]['list'][0]['id'];
$relatedVideo = file_get_contents("https://api.dailymotion.com/video/$videoId/related?fields=id,title,description,channel,thumbnail_url,owner.screenname&limit=5");
$relatedVideo = json_decode($relatedVideo,true);
$chk['related'] = $relatedVideo;
$results = json_encode($chk, JSON_UNESCAPED_UNICODE);
file_put_contents($dir . '/' . $filename, $results); // Store Api results as .json file
$results = json_decode($results, true);
//Create a json file for image URLs for image caching
createImageUrlJson($results, $filename, dirname($dir),9);
}
$pageno = $_REQUEST['page'];
$lastpage = 5;
if($pageno == "")
{
$pageno = 1;
}
if ($pageno <= 1) {
$pageno = 1;
} else if ($pageno > $lastpage) {
$pageno = $lastpage;
}
/* Related video results */
$relVideoResults = $results['related'];
if ($pageno > 0 && $pageno <= 5) {
$page = $results[$pageno - 1];
}
$thumbURL = " ";
// Display related video results
echo '<div id="related" style="display:none;background: white">';
    echo '<div style="max-height: 370px;float: left;width: 40%;overflow-y: scroll;">';
        foreach ($relVideoResults['list'] as $value){
        ?>
        <div style='background: white;padding:9.1px;height: 82px;'>
            <?php $relatedId = $value['id'] ?>
            <a href="javascript:void(0);"  onclick="videoDisplay('<?php echo $relatedId; ?>');">
                <div style='float:left;width:39%'><img src='<?php echo cleanDisplayParameter($connSearch, $value['thumbnail_url']); ?>' style='width: 100%;height:70Px;'></div>
            </a>
            <div style='float:left;width:60%;font-size: 11px;padding: 0% 1.5%;font-weight: bold;'>
                <a  href="javascript:void(0);" onclick="videoDisplay('<?php echo cleanDisplayParameter($connSearch, $relatedId); ?>');"><?php echo  substr(strip_tags(cleanDisplayParameter($connSearch, $value['title'])), 0, 80).'...' ; ?></a><br/>
                <span style='font-weight:100;font-size:11px;'>by: <?php echo substr(strip_tags(cleanDisplayParameter($connSearch, $value['channel'])), 0, 50).'...' ; ?></span>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<div class="">
    <div class="row">
        <div class="col-xs-12">
            <div class="row" id="search_result_dailymotion">
                <?php if(count($page['list']) > 0) {
                // Display searched query video results
                foreach ($page['list'] as $value) {
                $thumbURL = basename($value['thumbnail_url']);
                if(!empty($thumbURL)) {
                if(file_exists("{$_SERVER['DOCUMENT_ROOT']}json_directory/dailymotionImages/{$thumbURL}")) {
                $thumbURL = "/dailymotionImages/{$thumbURL}";
                } else{
                $thumbURL = $value['thumbnail_url'];
                }
                }else{
                $thumbURL = "/images/DM_Default.png";
                }
                ?>
                <div class="col-xs-12">
                    <div class="innerML inner-3x">
                        <div class="card innerAll innerMB clearfix ">
                            <div class="card-img pull-left hover_effect" videoId="<?php cleanDisplayParameter($connSearch, $value['id']) ?>"  onclick="videoDisplay('<?php echo $value['id']; ?>');">
                                <img src="<?php echo cleanDisplayParameter($connSearch, $value['thumbnail_url']); ?>">
                                <div class="info">
                                    <img src="../../../images/play_overlay_icon.png">
                                </div>
                            </div>
                            <div class="pull-left card-info innerLR">
                                <span class="card-title"  onclick="videoDisplay('<?php echo $value['id']; ?>');"> <a href="#" title="<?php echo strip_tags(cleanDisplayParameter($connSearch,$value['title']));?>">
                                <?php echo substr(strip_tags(cleanDisplayParameter($connSearch,$value['title'])), 0, 80);?> </a></span>
                                <p class="card-by half innerMB">
                                    <?php
                                    if ($value['owner.screenname'] != '') {
                                    echo "by " . cleanDisplayParameter($connSearch, $value['owner.screenname']);
                                    }?>
                                </p>
                                <p class="card-text channel half innerMB"> Channel:- <?php echo  substr(strip_tags(cleanDisplayParameter($connSearch, $value['channel'])), 0, 60); ?></p>
                                <p class="card-text desc half innerMB">
                                    <?php
                                    if (strlen($value['description']) > 90) {
                                    echo cleanDisplayParameter($connSearch, substr($value['description'], 0, 90) . '...... ');
                                    } else {
                                    echo cleanDisplayParameter($connSearch, $value['description']);
                                    }
                                    ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                }
                echo "<div class='dmDialog' style='display:none; background: rgb(178, 178, 178) !important; overflow: hidden;' ></div>";
            echo "</div>";
            echo "<div id='paginate' class='pagebtn-group text-center'>";
                if ($pageno > 1) {
                echo "<button value='1' class='pagebtn' onclick='loadPagination(this,\"$searchkey\")'>First</i></button>";
                }
                if ($pageno > 1) {
                echo "<button value='".($pageno - 1)."' class='pagebtn' onclick='loadPagination(this,\"$searchkey\")'>Previous</i></button>";
                }
                if ($pageno == 1) {
                $startLoop = 1;
                $endLoop = ($lastpage < 5) ? $lastpage : 5;
                } else if ($pageno == $lastpage) {
                $startLoop = (($lastpage - 5) < 1) ? 1 : ($lastpage - 5);
                $endLoop = $lastpage;
                } else {
                $startLoop = (($pageno - 3) < 1) ? 1 : ($pageno - 3);
                $endLoop = (($pageno + 3) > $lastpage) ? $lastpage : ($pageno + 3);
                }
                for ($i = $startLoop; $i <= $endLoop; $i++) {
                $class = "pagebtn-warning";
                if ($i == $pageno) {
                $class = "pagebtn-success";
                echo "<button  class='pagebtn  $class' ><mark style='background-color: #26A3DD;border: 0;color: white;'>" . $pageno . "</mark></button>";
                } else {
                echo "<button value='".$i."' type='button' onclick='loadPagination(this,\"$searchkey\")' class='pagebtn  $class'>" . $i . "</button>";
                }
                }
                if ($pageno < $lastpage) {
                echo "<button value='".($pageno + 1)."' onclick='loadPagination(this,\"$searchkey\")' class='pagebtn' >Next</button>";
                }
                if ($pageno != $lastpage) {
                echo "<button value='".$lastpage."' type='button' onclick='loadPagination(this,\"$searchkey\")' class='pagebtn'>Last</button>";
                }
            echo "</div>";
            }else{
            echo '<div class="noresult inner-6x innerML" style=" min-width:700px;">';
                echo '<span style="color:gray;font-size: 12px;"></span>';
                echo '<span>';
                    echo '<h1>No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>';
                echo '</span>';
            echo '</div>';
            echo '</div>';
            }
            ?>
            </div>

        </div>
    </div>
</div>
<script>
function loadPagination(page, keyword){
var page = $(page).val();
var q = keyword;
var url = "../dailymotion/daily-motion_result.php";
$.ajax({
type: 'GET',
url: url,
dataType: 'html',
data: {page:page, q:q},
success: function (data) {
$("#searchContainer").css('display', 'none');
$("#categoriesResultContainer").css('display', 'block');
$("#categoriesResultContainer").html('');
$("#categoriesResultContainer").html(data);
}
});
$(window).scrollTop(0);
}
</script>
<script>
$(document).ready(function(){
var appStatus = '<?php echo $appStatus; ?>';
if(appStatus != "TRUE"){
$( ".hover_effect" ).hover(function(e) {
$(this).find(".info").show();
}, function(e) {
$(".hover_effect").show();
$(this).find(".info").hide();
}
);
$( ".dmDialog" ).dialog({
autoOpen: false,
resizable: false,
modal: true,
height:380,
width: "70%",
top:"33%",
left:"17%",
open: function() {
jQuery('.ui-widget-overlay').bind('click', function() {
$(".dmDialog").html("");
$(".dmDialog").dialog('close');
});
$(".ui-dialog .ui-dialog-titlebar").hide();
$(".ui-dialog").css("position",'fixed');
$(".ui-dialog").css("padding",'0px');
var dialogId = $(".dmDialog").attr("id");
$("#"+dialogId).css("height","");
}
});
}
});
</script>
<script>
function videoDisplay(videoId){
var appended=$("#related").html();
$( ".dmDialog" ).html("");
$( ".dmDialog" ).html("<div style='width:60%;float:left;'><iframe frameborder='0' width='100%' height='370' src='https://www.dailymotion.com/embed/video/"+videoId+"?autoplay=1' allowfullscreen></iframe></div>"+appended); //+appended
$( ".dmDialog" ).dialog( "open" );
$(".dmDialog").css({"background":"transparent"});
}
</script>
<style>
.ui-widget-overlay{
opacity: 0.7;
}
</style>
