<?php
die;
class Flipkart {

    //Affiliate ID and token are entered through the constructor
    private $affiliateId;
    private $token;
    private $response_type;
    private $api_base = 'https://affiliate-api.flipkart.net/affiliate/api/';
    private $verify_ssl = false;

    /**
     * Obtains the values for required variables during initialization
     * @param string $affiliateId Your affiliate id.
     * @param string $token Access token for the API.
     * @param string $response_type Can be json/xml.
     * @return void
     **/
    function __construct($affiliateId, $token, $response_type="json"){
        $this->affiliateId = $affiliateId;
        $this->token = $token;
        $this->response_type = $response_type;

        //Add the affiliateId and response_type to the base URL to complete it.
        $this->api_base.= $this->affiliateId.'.'.$this->response_type;
    }

    /**
     * Calls the API directory page and returns the response.
     *
     * @return string Response from the API
     **/
    public function api_home(){
        //echo $this->api_base;
        return $this->sendRequest($this->api_base);
    }

    /**
     * Used to call URLs that are taken from the API directory.
     * Any change in the URL makes it invalid and the API refuses to respond.
     * The URLs have a timeout of ~4 hours, after which a new URL is to be
     * taken from the API homepage.
     *
     * @return string Response from the API
     **/
    public function call_url($url){
        return $this->sendRequest($url);
    }

    /**
     * Sends the HTTP request using cURL.
     *
     * @param string $url The URL for the API
     * @param int $timeout Timeout before the request is cancelled.
     * @return string Response from the API
     **/
    /* private function sendRequest($url, $timeout=30){ */
    private function sendRequest($url, $timeout=30){
        //Make sure cURL is available
        if (function_exists('curl_init') && function_exists('curl_setopt')){
            //The headers are required for authentication
            $headers = array(
                'Cache-Control: no-cache',
                'Fk-Affiliate-Id: '.$this->affiliateId,
                'Fk-Affiliate-Token: '.$this->token
            );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Freebucks/0.1');
            curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $this->verify_ssl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            $result = curl_exec($ch);

            curl_close($ch);

            return $result ? $result : false;
        } else {
            //Cannot work without cURL
            return false;
        }
    }
}

function get_flipkart_category($conn){
    global $flipKartAffiliateID;
    global $flipKartAccessToken;

    $flipkart_cat = new Flipkart($flipKartAffiliateID, $flipKartAccessToken, "json");
    $cat_url = "https://affiliate-api.flipkart.net/affiliate/api/".$flipKartAffiliateID.".json";

    //echo $cat_url;
    $cat_detail = $flipkart_cat -> call_url($cat_url );
    if(!$cat_detail){
        echo "Error: Could not retrive category list";
        exit();
    }

    $cat_detail = json_decode($cat_detail,true);
    //printArr($cat_detail);
    return $cat_detail;
}

/*
	function: 	insert_rec(),
	Purpose: 	To insert record in table,
	Arguments:	(string)$table as table name,
				(string)$field as table rows,
				(string)$value as table records,
				(string)$conn as database connection
*/
function insert_rec($table, $field, $value, $conn){

    $returnArr = array();
    $query = "insert into $table ($field) values ($value)";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = "Flipkart Product Insert Successfuly";
    }else{
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

/*
	function: 	select(),
	Purpose: 	To select record from table,
	Arguments:	(string)$table as table name,
				(string)$field as table rows,
				(string)$conn as database connection
*/
function select($field,$table, $conn){
    $returnArr = array();
    $query = "SELECT $field FROM $table";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"][-1]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"][5]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}


function selectPagination($field,$table,$condition, $conn){
    $returnArr = array();
    $query = "SELECT $field as count FROM $table where $condition";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res = $row;
        $returnArr["errCode"][-1]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"][5]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}

/*
	function: 	selectCat(),
	Purpose: 	To select record from table,
	Arguments:	(string)$table as table name,
				(string)$field as table rows,
				(string)$conn as database connection
*/
function selectCat($field, $table, $conn){

    $returnArr = array();
    $query = "select $field from $table";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"][-1]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"][5]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}


function selectCondition($field,$table,$condition, $conn){
    $query = "SELECT $field FROM $table where $condition";
    $res = mysqli_query($conn, $query);
    if(!$res)
    {
        echo "Error: Query execution <br />";
    } else	{
        $row = mysqli_num_rows($res);
        if($row == 0)
        {
            return 0;
        } else {
            while($rec = mysqli_fetch_array($res))
            {
                $data[] = $rec;
            }
            return $data;
        }
    }
}

/*
	function: 	delete_rec(),
	Purpose: 	To delete record from table,
	Arguments:	(string)$table as table name,
				(string)$conn as database connection
*/
function delete_rec($table, $conn){
    $returnArr = array();
    $query = "delete from $table";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = "Table Truncate Successfully";
    }else{
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

function get_flipkart_category_details($url){

    global $flipKartAffiliateID;
    global $flipKartAccessToken;

    $flipkart_fetch_cat= new Flipkart($flipKartAffiliateID, $flipKartAccessToken, "json");
    $details = $flipkart_fetch_cat -> call_url($url);

    if(!$details){
        echo 'Error: Could not retrieve products list.';
        exit();
    }

    $details = json_decode($details,true);

    return $details;
}

/*
	function: 	truncateTable(),
	Purpose: 	To truncate the table,
	Arguments:	(string)$table as table name,
				(string)$conn as database connection
*/
function truncateTable($table, $conn){

    $returnArr = array();
    $query = "truncate table $table";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = "Table Truncate Successfully";
    }else{
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;

}

/*
	function: 	update(),
	Purpose: 	To update the records in table,
	Arguments:	(string)$table as table name,
				(string)$field as table field values,
				(string)$conn as database connection
*/
function update($table, $field, $conn){

    $returnArr = array();
    $query = "update $table set $field";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $returnArr["errCode"][-1] = -1;
        $returnArr["errMsg"] = "Table update Successfully";
    }else{
        $returnArr["errCode"][5] = 5;
        $returnArr["errMsg"] = $result["errMsg"];
    }

    return $returnArr;
}

/*function updateCondition($table, $field, $condition){

    $sql = "update $table set $field where $condition";
    $res = mysqli_query($conn, $sql);

    return $res;
}*/

function flipkartPagination($offset, $srch, $cid, $catName, $no_of_rows, $conn){
    $returnArr = array();
    global $blanks;

    /* $keyword = explode(" ",$srch);
    //echo $keyword;
    //print_r($keyword);
    $i = 0;
    $q= '';

     foreach($keyword as $p){
        $p = strtolower($p);
        if($i > 0) $q.= " OR ";
       // $q.= "LOWER(name) like '%$p%'";
       $q .= "MATCH(title,category_name) AGAINST ('$p' IN BOOLEAN MODE)";
        $i++;
    } */

//$query1="SELECT *, MATCH(title,category_name) AGAINST ( '".$srch."' IN BOOLEAN MODE) AS Score  FROM flipkart_product where ".$q;
//echo $query1;

    if((!in_array($offset, $blanks)) && (!in_array($no_of_rows, $blanks))){
        if($cid == ''){
            $query = "SELECT itemId,title,price,prod_img_url,prod_url,prod_brand,category_name,category_id,prod_currency, sum(MATCH(title,category_name,description) AGAINST ( '".$srch."' IN BOOLEAN MODE)) AS Score  FROM flipkart_product  WHERE  MATCH(title,category_name,description) AGAINST ('".$srch."' IN BOOLEAN MODE) group by title,category_name,description order by Score DESC  LIMIT ".$offset.",".$no_of_rows.";";

        }else{
            $query = "SELECT itemId,title,price,prod_img_url,prod_url,prod_brand,category_name,category_id,prod_currency, sum(MATCH(title,category_name,description) AGAINST ( '".$catName."' IN BOOLEAN MODE)) AS Score  FROM flipkart_product  WHERE category_id=$cid  group by title,category_name,description order by Score DESC  LIMIT ".$offset.",".$no_of_rows.";";

        }
        //echo $query;
    }else{
        if($cid == ''){
            $query = "SELECT itemId,title,price,prod_img_url,prod_url,prod_brand,category_name,category_id,prod_currency, sum(MATCH(title,category_name,description) AGAINST ( '".$srch."' IN BOOLEAN MODE)) AS Score FROM  flipkart_product  WHERE MATCH(title,category_name,description) AGAINST ('".$srch."' IN BOOLEAN MODE) group by title,category_name,description order by Score DESC";

        } else{
            $query = "SELECT itemId,title,price,prod_img_url,prod_url,prod_brand,category_name,category_id,prod_currency, sum(MATCH(title,category_name,description) AGAINST ( '".$catName."' IN BOOLEAN MODE)) AS Score FROM  flipkart_product  WHERE category_id=$cid  group by title,category_name,description order by Score DESC";

        }
        //echo $query;
    }
    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"][-1]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"][5]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }
    return $returnArr;
}

function multiRequest($data, $options = array()) {
    // array of curl handles
    $curly = array();
    // data to be returned
    $result = array();
    // printArr($data);
    // die();
    // multi handle
    $mh = curl_multi_init();

    // loop through $data and create curl handles
    // then add them to the multi-handle
    foreach ($data as $id => $d) {
        // echo $d["category_id"]."<br />";
        // echo $curly[$id]."<br />";
        $curly[$id] = curl_init();
        //$url = $d["feed_url"];
        $url = (is_array($d) && !empty($d['feed_url'])) ? $d['feed_url'] : $d;
        echo $url."<br />";
        curl_setopt($curly[$id], CURLOPT_URL, $url);
        curl_setopt($curly[$id], CURLOPT_HEADER,         false);
        curl_setopt($curly[$id], CURLOPT_RETURNTRANSFER, true);
        //curl_setopt($curly[$id], CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curly[$id], CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($curly[$id],  CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curly[$id], CURLOPT_SSL_VERIFYHOST, 2);
        // post?
        /*   if (is_array($d)) {
             if (!empty($d['post'])) {
               curl_setopt($curly[$id], CURLOPT_POST,       1);
               curl_setopt($curly[$id], CURLOPT_POSTFIELDS, $d['post']);
             }

           }  */
        // extra options?

        if (!empty($options)) {
            curl_setopt_array($curly[$id], $options);
        }
        curl_multi_add_handle($mh, $curly[$id]);
    }
    // execute the handles
    $running = null;
    do {
        curl_multi_exec($mh, $running);
    } while($running > 0);
    // get content and remove handles

    foreach($curly as $id => $c) {
        $result[$id] = curl_multi_getcontent($c);
        curl_multi_remove_handle($mh, $c);
    }
    // all done

    curl_multi_close($mh);
    //printArr($result);

    return $result;

}

function flipkartCategories($conn){
    $returnArr = array();
    $query = "SELECT category_id, category_name,feed_url from flipkart_category";

    $result = runQuery($query, $conn);
    if(noError($result)){
        $res = array();
        while ($row = mysqli_fetch_assoc($result["dbResource"]))
            $res[] = $row;
        $returnArr["errCode"][-1]=-1;
        $returnArr["errMsg"]=$res;
    } else {
        $returnArr["errCode"][5]=5;
        $returnArr["errMsg"]=$result["errMsg"];
    }

    return $returnArr;
}


?>
