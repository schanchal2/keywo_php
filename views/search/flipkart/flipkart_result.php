<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');
require_once("flipkartHelper.php");

error_reporting(E_All);
// connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
  $connSearch = $connSearch["connection"];
}else{
  print("Search Database Error");
  exit();
}

//Require API app ID and Token
$flipKartAffiliateID = "searchtra";
$flipKartAccessToken = "6feefe6ccbcb443cb44786b651122561";

// getting search query from url.
$keywords = $_REQUEST['q'];
$array = explode(" ", $keywords);
$pageno1 = $_REQUEST['pageno1'];
$catId = $_GET['category_id'];
$catName = $_GET['category_name'];

$array2 = array();
foreach($array as $array1){
  $string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
  $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
  $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
  $test .='%'.$string.'';
  array_push($array2, $string);
}
$srch = implode(" ", $array2);
$srch = cleanDisplayParameter($connSearch, $srch);
?>

<?php
$pageno1=1;
if(isset($_REQUEST["pageno1"]) && !in_array($_REQUEST["pageno1"], $blanks)){
  $pageno1=preg_replace('#[^0-9]#i','',$_REQUEST["pageno1"]);
}
$resultsPerPage=12;
$start = ($pageno1-1)*$resultsPerPage;
$pageno1=$_REQUEST["pageno1"];
$row_count = flipkartPagination('', $srch, $catId, $catName, '', $connSearch);
$resultsPerPage =12;
$row_count = $row_count["errMsg"];
$row_count = COUNT($row_count);


if($row_count == 0) {
  echo '<div class="greyBody"><br>
				<div class="mainnews">
						<div class="pull-left inner-6x innerML">
							<h1 class="margin-top-none">No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>
						</div>
			   </div>';
}

$lastpage1 = $row_count/$resultsPerPage;
$lastpage1 = ceil($lastpage1);
if($pageno1<=1){
  $pageno1=1;
}else if($pageno1>$lastpage1){
  $pageno1=$lastpage1;
}
if(!isset($pageno1) || ($pageno1 == 1)){
  $start = '0';
}else{
  $start = $start;
}

  if(isset($srch) && !empty($srch)){
    $fetchproduct=flipkartPagination($start, $srch, $catId, $catName, '12', $connSearch);

    if(!$fetchproduct){
      echo "Query execution error while fetching products. ".mysqli_error();
    }else{
      $fetchproduct = $fetchproduct["errMsg"];
?>
      <div class="col-xs-6 inner-2x">
            <div class="col-xs-12 inner-2x innerMB">
                <div class="padding-none flipkart-search-results">
                  <?php
                  foreach($fetchproduct as $x => $prd){?>
                    <div class="col-xs-5 inner-2x innerMB">
                        <div class="card flipkart-card">
                                <div>
                                  <?php
                                  print("<a  href='".cleanDisplayParameter($connSearch, $prd["prod_url"])."' target='_blank'>");
                                  ?>
                                   <?php
                                           print("<p class='innerT'><img class='product-img img-responsive' src='".cleanDisplayParameter($connSearch, $prd["prod_img_url"])."' /></p>");
                                    ?>

                                </div>
                                <div class="product-detail-box innerAll">
                                    <div class="product-box-title innerMB">
                                        <?php  print("<p  class='product_title'>".cleanDisplayParameter($connSearch, $prd["title"])."</p>");?>
                                    </div>
                                    <div class="product-box-price innerMB">
                                        <?php
                                              $currency = cleanDisplayParameter($connSearch, $prd["prod_currency"]);
                                              if($currency == "INR"){
                                              $currency = str_replace("INR","Rs. ","$currency");
                                              }
                                              $price = cleanDisplayParameter($connSearch, $prd["price"]);
                                              if($price == 0){
                                              print ("<p class='product_price'>Price: N/A </p>");
                                              }
                                              else{
                                              print( "<p class='product_price'>".$currency." ".$price."</p>");
                                              }
                                         ?>
                                    </div>
                                    <div class="product-box-main-title innerMB">
                                           <?php print("<p class='product_title'>".cleanDisplayParameter($connSearch, $prd["prod_brand"]));?>
                                    </div>
                                </div>
                                <div class="innerB">
                                    <div class="product-buy-now text-center text-white">
                                        BUY NOW
                                    </div>
                                </div>
                          <?php print("</a>");?>
                        </div>
                    </div>
                  <?php } ?>
                </div>
            </div>
            <?php
    }
  }
  ?>
    <?php
    if($row_count != 0){
    ?>

    <div id="paginate" class="col-xs-12 text-center inner-2x innerMB">
      <?php
      if ($pageno1 == 1) {
      } else {
        echo " <button type='button' class='pagebtn' onclick='loadPageFlipkart(1, \"$srch\")'>FIRST</button> ";
        $prevpage = $pageno1-1;
        echo " <button type='button' class='pagebtn' onclick='loadPageFlipkart($prevpage, \"$srch\")'>PREV</button> ";
      }
      $paginationStart = (($pageno1-0)<=0)?1:($pageno1-0);
      $paginationEnd = (($pageno1+5)<=$lastpage1)?($pageno1+5):$lastpage1;
      for($paginationIterator=$paginationStart; $paginationIterator<=$paginationEnd; $paginationIterator++){
        $class = "pagebtn-warning";
        if($paginationIterator==$pageno1)
          $class = "pagebtn-success";
        echo " <button type='button' class='pagebtn  $class' onclick='loadPageFlipkart($paginationIterator, \"$srch\")' > $paginationIterator </button>";

      }
      // next/last pagination hyperlinks
      if ($pageno1 == $lastpage1) {
      } else {
        $nextpage = $pageno1+1;
        echo " <button onclick='loadPageFlipkart($nextpage, \"$srch\")' type='button' class='pagebtn'>NEXT</button>";
        echo " <button onclick='loadPageFlipkart($lastpage1, \"$srch\")' type='button' class='pagebtn'>LAST</button>";
      }
      echo "</div>";
      }
      echo "<br>";
      ?>
    </div>

<script>
  var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
      body = document.body;
  var val = 1;

  $(document).ready(function(){
    $(this).scrollTop(0);

    $('#cat').click(function() {
      classie.toggle( this, 'active' );
      classie.toggle( menuLeft, 'cbp-spmenu-open' );
    });


    $("#cat").hover(function(){
      $(this).stop().animate({marginLeft: "-50px"}, 1000);
    }, function(){
      $(this).stop().animate({marginLeft: "-225px"}, 1000);
    });
    $("#comPrice").hover(function(){
      $(this).stop().animate({marginLeft: "-50px"}, 1000);
    }, function(){
      $(this).stop().animate({marginLeft: "-225px"}, 1000);
    });
  });

  //left menu of category hide on body click
  $('#main-area').click(function(){
    if(val != 1)
    {
      val = 1;
      var catHide = $( "#cbp-spmenu-s1" ).hasClass('cbp-spmenu-open');
      if (catHide == true){
        $( "#cbp-spmenu-s1" ).removeClass('cbp-spmenu-open');
      }

    }else{
      val = 0;
    }
  });
</script>
<script>
  function loadPageFlipkart(pageNo, srch){
    var url = '<?php echo $rootUrlSearch; ?>';
      url = "../flipkart/flipkart_result.php";
    $.ajax({
      type: 'POST',
      url: url,
      dataType: 'html',
      data: {pageno1:pageNo,q:srch},
      success: function (data) {
        $("#searchContainer").html('');
        $("#searchContainer").html(data);
      }
    });
    return false;
  }

</script>
