<?php
die;
session_start();


// include required files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');

$appId 			= $_REQUEST["appId"];
$appLogo 		= urldecode($_REQUEST["appLogo"]);
$appURL 		= urldecode($_REQUEST["appURL"]);
$appName 		= urldecode($_REQUEST["appName"]);
$rootUrl 	    = urldecode($_REQUEST["rootUrl"]);

//connecting to search Database
$connSearch = createDbConnection('dbsearch');
if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
?>
<div class="row">
    <div class="col-xs-12">
        <div class="text-center innerMB">
            <img class="search-landing-img innerMAll" src="<?php echo cleanDisplayParameter($connSearch, $rootUrl.$appLogo); ?>" />
        </div>
    </div>
</div>
<div class="row">
    <form id="my_form" class="clearfix" action="<?php echo cleanDisplayParameter($connSearch, $appURL); ?>" enctype="application/x-www-form-urlencoded"  role="search" onsubmit=" return checkEmptyField();">
        <div class="main-search-box pull-left">
            <!--===============Search Box====================-->
            <input type="text" class="landing-search-box form-control autoSuggest xssValidation" name="q"  placeholder="Enter your search term and start earning"/>
        </div>
        <div class="main-search-button pull-left">
            <!--===============Search Button====================-->
            <input type="submit" value="<?php echo cleanDisplayParameter($connSearch, ucwords($appName)); ?> Search" class="bg-skyblue text-white landing-search-btn"/>
        </div>
    </form>
</div>
<?php
}else{
    print("Error: Search Database connection");
    exit;
}
?>
<script>
    // Autosuggest on Apps and searchresult pages.
    $(function() {
        $(".autoSuggest").autocomplete({
            source: "../../../controllers/search/autoSuggestController.php?appJsonId=<?php echo $appId; ?>",
            minLength: 1
        });
    });

    $(function(){
        $('.xssValidation').on("keydown", function(e){
            if (e.shiftKey && (e.which == 188 || e.which == 190)) {
                e.preventDefault();
            }
        });
    });

</script>
