<?php
die;
// include dependent files
require_once('../../../config/config.php');
require_once('../../../config/db_config.php');
require_once('../../../models/search/searchResultModel.php');
require_once('../../../helpers/errorMap.php');
require_once('../../../helpers/sessionHelper.php');
require_once('../../../helpers/arrayHelper.php');
require_once('../../../helpers/coreFunctions.php');
require_once('../../../helpers/stringHelper.php');

error_reporting(E_All);
//connecting to search Database
$connSearch = createDbConnection('dbsearch');

if(noError($connSearch)){
    $connSearch = $connSearch["connection"];
}else{
    print("Search Database Error");
    exit();
}

// getting search query from url.
$keywords = $_REQUEST['q'];
$array = explode(" ",$keywords);
$test ='\'';
$array2 = array();
foreach($array as $array1){
    $string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    $test .='%'.$string.'';
    array_push($array2,$string);
}
$SearchString = implode(" ",$array2);
$SearchString = cleanDisplayParameter($connSearch, $SearchString);
$test .='%\'';
$query1 .='(i.description != null OR i.description LIKE '.$test.') OR  (i.description != null AND i.title LIKE '.$test.')';
if(count($array2) > 1){
    $test1 ='\'';
    krsort($array2);
    foreach($array2 as $array3){
        $test1 .='%'.$array3.'';
    }
    $test1 .='%\'';
    $query1 .='(i.description != null OR i.description LIKE '.$test1.') OR  (i.description != null AND i.title LIKE '.$test1.')';
}
$searchData = $query1;

displayPaginateResult($searchData, $keywords, $array2, $SearchString, $connSearch);

//Display news result with pagination.
function displayPaginateResult($searchData, $keywords, $array2, $SearchString, $connSearch){

    $query='SELECT i.item_id FROM items i LEFT JOIN images e ON i.item_id = e.item_id WHERE  '.$searchData.' order by i.publish_date desc';
    $resultQuery = runQuery($query, $connSearch);
    $total_rows = mysqli_num_rows($resultQuery["dbResource"]);

    //If results of query are less than 10 then search for related results.
    if($total_rows < 5 && count($array2) > 1){
        for($i=0;$i<count($array2);$i++){
            if(end($array2) !== $array2[$i]){
                $fkey .= ' OR ';
            }
            $fkey .= 'i.title LIKE \'%'.$array2[$i].'%\'';
        }
        $searchData = $fkey;
        $query='SELECT i.item_id FROM items i LEFT JOIN images e ON i.item_id = e.item_id WHERE '.$searchData.' order by i.publish_date desc';
        $resultQuery = runQuery($query, $connSearch);
        $total_rows = mysqli_num_rows($resultQuery["dbResource"]);
    }
    $rows_per_page = 15;
    $numrows = $total_rows;
    // Calculate number of $lastpage
    $lastpage = ceil($numrows/$rows_per_page);
    // condition inputs/set default
    if (isset($_GET['pageno'])) {
        $pageno = $_GET['pageno'];
    } else {
        $pageno = 1;
    }
    // validate/limit requested $pageno
    $pageno = (int)$pageno;
    if ($pageno > $lastpage) {
        $pageno = $lastpage;
    }
    if ($pageno < 1) {
        $pageno = 1;
    }
    // Find start and end array index that corresponds to the requested pageno
    $start = ($pageno - 1) * $rows_per_page;
    $end = $start + $rows_per_page -1;

    // limit $end to highest array index
    if($end > $numrows - 1){
        $end = $numrows - 1;
    }
    $query='SELECT i.item_id ,i.title,i.description,i.link,i.publish_date, i.source,e.url FROM items i LEFT JOIN images e ON i.item_id = e.item_id WHERE '.$searchData.' order by i.publish_date desc LIMIT '.$rows_per_page.' OFFSET '.$start;
    $resultQuery = runQuery($query, $connSearch);
    if($_REQUEST['app_status'] != "TRUE"){?>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
                    <div class="row search_result_stnews" id="search_result_stnews">
                        <div class="result-found"><?php if($total_rows > 0){echo $total_rows." results found.";}?></div>
                        <?php if($total_rows == 0){?>
                            <div class = "errmsg inner-5x innerML" style="width: 100%; float: left;">
                                <h1>No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>
                            </div>
                            <?php
                        }
                        while($newsresult = mysqli_fetch_assoc($resultQuery["dbResource"])){ ?>
                            <div class="col-xs-12">
                                <div class="innerML inner-3x">
                                    <div class="card innerAll innerMB clearfix ">
                                        <?php echo '<h4 class="text-light-blue margin-top-none news-title"><a href="'.cleanDisplayParameter($connSearch, $newsresult['link']).'" target="_blank">'.cleanDisplayParameter($connSearch, stripslashes($newsresult['title'])).'</a></h4>'; ?>
                                        <div class="innerML">
                                            <div class="card-img pull-left">
                                                <?php
                                                if(!null == $newsresult['url']){
                                                    echo '<img src="'.cleanDisplayParameter($connSearch, $newsresult['url']).'" />';
                                                }else{
                                                    echo '<img src="../../../images/No-Image-Available.png" />';
                                                }?>
                                            </div>
                                            <div class="pull-left card-info innerLR">
                                                <p class="card-text margin-bottom-none">
                                                    <?php
                                                    $time = $newsresult['publish_date'];
                                                    $totalTime = date("F,d Y", strtotime($time));
                                                    if(strlen($newsresult["description"]) < 250){
                                                        echo  strip_tags($newsresult["description"]).' <br>';
                                                    }else{
                                                        echo  substr(strip_tags($newsresult["description"]), 0, 250).'... <br>';
                                                    }
                                                    ?>
                                                </p>
                                                <?php echo '<span class="card-by">By '.cleanDisplayParameter($connSearch, $newsresult['source']).'</span>';
                                                '<span class="card-by">'.cleanDisplayParameter($connSearch, $totalTime).'</span>'; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <?php   } ?>
                    </div>

                </div>
            </div>
        </div>
        <?php
        if($total_rows > 0){
            echo "<br><br>";
            echo "<div id='paginate' style='text-align:center;  width: 780px!important;' class='pagebtn-group'>";
            // first/prev pagination hyperlinks
            if ($pageno == 1) {
                //echo " FIRST PREV ";
            } else {
                echo " <button type='button' class='pagebtn' onclick='loadPageNews(1, \"$SearchString\")'>First</i></button> ";
                $prevpage = $pageno-1;
                echo " <button type='button' class='pagebtn' onclick='loadPageNews($prevpage, \"$SearchString\")'>Previous</i></button> ";
            }
            $paginationStart = (($pageno-5)<=0)?1:($pageno-5);
            $paginationEnd = (($pageno+5)<=$lastpage)?($pageno+5):$lastpage;
            for($paginationIterator=$paginationStart; $paginationIterator<=$paginationEnd; $paginationIterator++){
                $class = "pagebtn-warning";
                if($paginationIterator==$pageno)
                    $class = "pagebtn-success";
                echo " <button type='button' class='pagebtn  $class' onclick='loadPageNews($paginationIterator, \"$SearchString\")' > $paginationIterator </button>";
            }
            // next/last pagination hyperlinks
            if ($pageno == $lastpage) {
                //echo " NEXT LAST ";
            } else {
                $nextpage = $pageno+1;
                echo " <button onclick='loadPageNews($nextpage, \"$SearchString\")' type='button' class='pagebtn'>Next</button> ";
                echo " <button onclick='loadPageNews($lastpage, \"$SearchString\")' type='button' class='pagebtn'>Last</i></button> ";
            }
            echo "</div>";
            echo "<br>";
        }
    }else{ ?>
        <div class="container-fluid">

            <div class="row search_result_stnews" id="search_result_stnews">
                <div class="result-found" ><?php if($total_rows > 0){echo $total_rows." results found.";}?></div>
                <?php if($total_rows == 0){ ?>
                    <div style="width: 90%; margin-left: 5%;">
						<span class="inner-5x innerML">
							<h1>No matching results found!!!</h1><h4>Please check the spelling or try different keywords.</h4>
						</span>
                    </div>
                    <?php
                }
                while($newsresult = mysqli_fetch_assoc($resultQuery["dbResource"])){	?>

                    <div class="col-xs-12">
                        <div class="innerML inner-3x">
                            <div class="card innerAll innerMB clearfix ">
                                <?php echo '<h4 class="text-light-blue margin-top-none news-title"><a href="'.cleanDisplayParameter($connSearch, $newsresult['link']).'" target="_blank">'.cleanDisplayParameter($connSearch, stripslashes($newsresult['title'])).'</a></h4>'; ?>
                                <div class="innerML">
                                    <div class="card-img pull-left">
                                        <?php
                                        if(!null == $newsresult['url']){
                                            echo '<img src="'.cleanDisplayParameter($connSearch, $newsresult['url']).'" />';
                                        }else{
                                            echo '<img src="../../../images/No-Image-Available.png" />';
                                        }?>
                                    </div>
                                    <div class="pull-left card-info innerLR">
                                        <p class="card-text margin-bottom-none">
                                            <?php
                                            $time = $newsresult['publish_date'];
                                            $totalTime = date("F,d Y", strtotime($time));
                                            if(strlen($newsresult["description"]) < 250){
                                                echo  strip_tags($newsresult["description"]).' <br>';
                                            }else{
                                                echo  substr(strip_tags($newsresult["description"]), 0, 250).'... <br>';
                                            }
                                            ?>
                                        </p>
                                        <?php echo '<span class="card-by">By '.cleanDisplayParameter($connSearch, $newsresult['source']).'</span>';
                                        '<span class="card-by">'.cleanDisplayParameter($connSearch, $totalTime).'</span>'; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        if($total_rows > 0){
            echo "<br><br>";
            echo "<div id='paginate' style='text-align:center;' class='pagebtn-group'>";

            // first/prev pagination hyperlinks
            if ($pageno == 1) {
                //echo " FIRST PREV ";
            } else {
                echo " <button type='button' class='pagebtn' onclick='loadPageNews(1, \"$SearchString\")'>|<</i></button> ";
                $prevpage = $pageno-1;
                echo " <button type='button' class='pagebtn' onclick='loadPageNews($prevpage, \"$SearchString\")'><<</i></button> ";
            }
            $paginationStart = (($pageno-2)<=0)?1:($pageno-2);
            $paginationEnd = (($pageno+2)<=$lastpage)?($pageno+2):$lastpage;
            for($paginationIterator=$paginationStart; $paginationIterator<=$paginationEnd; $paginationIterator++){
                $class = "pagebtn-warning";
                if($paginationIterator==$pageno)
                    $class = "pagebtn-success";
                echo " <button type='button' class='pagebtn  $class' onclick='loadPageNews($paginationIterator, \"$SearchString\")' > $paginationIterator </button>";
            }
            // next/last pagination hyperlinks
            if ($pageno == $lastpage) {
                //echo " NEXT LAST ";
            } else {
                $nextpage = $pageno+1;
                echo " <button onclick='loadPageNews($nextpage, \"$SearchString\")' type='button' class='pagebtn'>>></i></button> ";
                echo " <button onclick='loadPageNews($lastpage, \"$SearchString\")' type='button' class='pagebtn'>>|</i></button> ";
            }
            echo "</div>";
            echo "<br>";
        }
    }
}
?>
<!-- Pagination through results from amazon -->
<script>
    function loadPageNews(pageNo, searchQuery){
        var appStatus = '<?php echo $_REQUEST['app_status']; ?>';
        if(appStatus != "TRUE"){
            url = "../stnews/stnews_result.php";
        }else{
            url = "stnews/stnews_result.php";
        }
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'html',
            data: {pageno:pageNo, q:searchQuery, app_status:appStatus},
            success: function (data) {
                $("#searchContainer").css('display', 'none');
                $("#categoriesResultContainer").css('display', 'block');
                $("#categoriesResultContainer").html('');
                $("#categoriesResultContainer").html(data);
            }
        });
        return false;
    }
    $(document).ready(function(){
        $(this).scrollTop(0);
    });
</script>
