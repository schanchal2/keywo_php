<!--
   tooltip in marketplace section is on right side
   cant have it on bottom,
   because marketplace section is having scroll on content extend (overflow-y auto and overflow hidden property in css) -->
<?php
   include("../../layout/header.php");

   ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <!-- container starts here -->
    <main class="main-keyword">
        <div class="keyword-marketplace keyword-markt-popup-common">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="heading-key-markt margin-top-none">Marketplace - Buy and Trade Keywords</h3>
                    </div>
                </div>
                <br>
                <!-- <h3>Marketplace - Buy and Trade Keywords</h3> -->
                <div class="row position-relative">
                    <p class="errMsg">Error Message</p>
                    <div class="col-md-8 padding-left-none padding-right-none">
                        <!-- search bar starts here -->
                        <div class="row">
                            <div class="col-md-12 padding-left-none">
                                <div id="custom-search-input">
                                    <div class="input-group col-md-12 padding-left-none padding-right-none">
                                        <form>
                                            <input type="text" class="search-query form-control ellipses inner-3x innerR" placeholder="check keywords availability" />
                                            <span class="input-group-btn">
                              <button class="btn btn-danger" type="button">
                              <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                            </span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- search bar ends here -->
                        <!-- right-side-scroll starts here -->
                        <div class="row">
                            <div class="col-md-12 padding-left-none">
                                <div class="right-side-scroll">
                                    <!-- ul li starts here -->
                                    <ul class="search-box" id="keyword-ellipses">
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#party" data-toggle="tooltip" data-placement="bottom">#party</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="254" data-toggle="tooltip" data-placement="bottom">254</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;   <a href="#" title="-" data-toggle="tooltip" data-placement="bottom">-</a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12 innerMB">
                                                                <input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-place-bid" />
                                                            </div>
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Accept Bid" type="button" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="254" data-toggle="tooltip" data-placement="bottom">254</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;   <a href="#" title="-" data-toggle="tooltip" data-placement="bottom">-</a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Add to Cart" type="button" data-toggle="modal" data-target="#keyword-popup-ask-bid" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="254" data-toggle="tooltip" data-placement="bottom">254</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;   <a href="#" title="-" data-toggle="tooltip" data-placement="bottom">-</a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Set Ask" type="button" data-toggle="modal" data-target="#keyword-popup-set-ask" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="216321651651651616514614614655" data-toggle="tooltip" data-placement="bottom">215</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp; <a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-edit-ask" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="216321651651651616514614614655" data-toggle="tooltip" data-placement="bottom">215</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp; <a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-edit-ask" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="216321651651651616514614614655" data-toggle="tooltip" data-placement="bottom">215</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp; <a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-edit-ask" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                        <li>
                                            <!-- row starts here -->
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="text-left">
                                                        <h5 class="text-blue keyword-name ellipses"><a href="#" class="display-in-block-txt-blk" title="#partypartypartypartypartypartypartypartypartypartypartypartypartyparty" data-toggle="tooltip" data-placement="bottom">#partypartypartypartypartypartypartypartypartypartypartypartypartyparty</a></h5>
                                                        <label class="keyword-grey pull-left">Followed by : </label>
                                                        <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="5000 users" data-toggle="tooltip" data-placement="bottom">5000 users</a></span>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Interaction : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="216321651651651616514614614655" data-toggle="tooltip" data-placement="bottom">215</a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Earning : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1.25K <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1.25K <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="row">
                                                        <div class="text-left">
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Current Bid : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp;<a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                            </div>
                                                            <div class="pull-left col-xs-12">
                                                                <label class="keyword-grey pull-left">Asking Price : </label>
                                                                <span class="keyword-grey-span pull-r-marketplace">&nbsp; <a href="#" title="1254 <?php echo $keywoDefaultCurrencyName; ?>" data-toggle="tooltip" data-placement="bottom">1254 <?php echo $keywoDefaultCurrencyName; ?></a> </span>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 pull-right">
                                                    <div class="row">
                                                        <div class="text-right">
                                                            <div class="pull-left col-xs-6 col-md-12">
                                                                <input value="Edit Ask" type="button" data-toggle="modal" data-target="#keyword-popup-edit-ask" class="btn-trading-dark" />
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- row ends here -->
                                        </li>
                                    </ul>
                                    <!-- ul li ends here -->
                                </div>
                                <div class="footer-right-side-scroll padding-none">
                                    <div class="row">
                                        <div class="col-md-6 padding-right-none">
                                            <div class="keyword-radio-btn">
                                                <div class="row">
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="radio radio-primary radio-div">
                                                            <input type="radio" name="gender" id="male" value="male" checked="true">
                                                            <label for="male">
                                                                All
                                                            </label>
                                                        </div>
                                                        <div class="radio radio-primary radio-div">
                                                            <input type="radio" name="gender" id="female" value="female">
                                                            <label for="female">
                                                                Unowned
                                                            </label>
                                                        </div>
                                                        <div class="radio radio-primary radio-div">
                                                            <input type="radio" name="gender" id="other" value="other">
                                                            <label for="other">
                                                                Owned
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="footerul-r-side-scrl innerAll">
                                                <ol class="breadcrumb pull-right">
                                                    <li><a href="#">Agreement</a></li>
                                                    <li><a href="#">FAQ's</a></li>
                                                    <li><a href="#">How it Works</a>&nbsp;&nbsp;<a href="#"><i class="fa fa-refresh" aria-hidden="true"></i></a></li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- right-side-scroll ends here -->
                    </div>
                    <div class="col-md-4 padding-right-none">
                        <!-- row keyword-info-blocks div starts here -->
                        <div class="row keyword-info-blocks">
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status">current price</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">1200 - 1800</div>
                                    <div class="keyword-num-status light-blue-col"><a href="#" type="button" class="btn-trading-wthout-bg" data-toggle="modal" data-target="#keyword-popup-markt-slab">current slab <i class="fa fa-link" aria-hidden="true"></i></a></div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">12,545</div>
                                    <div class="keyword-num-status light-blue-col">keyword Sold</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">25,525</div>
                                    <div class="keyword-num-status">Keywords owners</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status">Keywords traded</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">2,546 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status light-blue-col">profit from trading</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status light-blue-col">lifetime interactions</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,54,625</div>
                                    <div class="keyword-num-status">yesterday's interactions</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status">lifetime payout</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                            <!-- col-xs-6 starts here -->
                            <div class="col-xs-6 inner-2x innerMB">
                                <div class="keyword-info">
                                    <div class="keyword-info-num bg-blue-key-dark">2,546 <?php echo $keywoDefaultCurrencyName; ?></div>
                                    <div class="keyword-num-status light-blue-col">yesterday's payout</div>
                                </div>
                            </div>
                            <!-- col-xs-6 ends here -->
                        </div>
                        <!-- row keyword-info-blocks div starts here -->
                    </div>
                </div>
                <div class="horizontal-line"></div>
                <div class="row">
                    <div class="keyword-market-botm" id="keyword-ellipses">
                        <div class="col-md-6 padding-left-none">
                            <!-- trade analytics tabs starts here -->
                            <div class="trade-analytics-tabs">
                                <h6 class="head-small-key-markt">trade analytics</h6>
                                <!-- tabs starts here -->
                                <!-- container starts here -->
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 padding-none" id="tooltip-arrw--left">
                                            <div class="tab_container tabs tab-links">
                                                <input id="tab1" type="radio" name="tabs" checked>
                                                <label class="tabs-head" for="tab1"><span>latest trade</span></label>
                                                <input id="tab2" type="radio" name="tabs">
                                                <label class="tabs-head" for="tab2"><span>ask</span></label>
                                                <input id="tab3" type="radio" name="tabs">
                                                <label class="tabs-head" for="tab3"><span>bid</span></label>
                                                <input id="tab4" type="radio" name="tabs">
                                                <label class="tabs-head" for="tab4"><span>Newly Owned</span></label>
                                                <section id="content1" class="tab-content">
                                                    <table class="table">
                                                        <tbody>
                                                            <ul class="border-all">
                                                                <!-- li starts here -->
                                                                <!-- <li>
                                                <div class="comn-lft-rght-cont">
                                                   <label class="ellipses-market-trade-label"><a href="#">#party</a></label>
                                                   <span class="pull-right ellipses-market-trade-span">23,525 <?php echo $keywoDefaultCurrencyName; ?></span>
                                                   <div class="clearfix"></div>
                                                </div>
                                                </li> -->
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="common-on-hover">
                                                                        <span>
                                                   <a href="#">
                                                   <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
                                                   </a>
                                                   </span>
                                                                        <span class="pull-right"><a href="#">View More</a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <!-- li ends here -->
                                                            </ul>
                                                        </tbody>
                                                    </table>
                                                </section>
                                                <section id="content2" class="tab-content">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>bbbb</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>bbbb</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                            <tr>
                                                                <td>bbbb</td>
                                                                <td>@mdo</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                                <section id="content3" class="tab-content">
                                                    <table class="table">
                                                        <tbody>
                                                            <ul class="border-all">
                                                                <!-- li starts here -->
                                                                <!-- <li>
                                                <div class="comn-lft-rght-cont">
                                                   <label class="ellipses-market-trade-label"><a href="#">#party</a></label>
                                                   <span class="pull-right ellipses-market-trade-span">23,525 <?php echo $keywoDefaultCurrencyName; ?></span>
                                                   <div class="clearfix"></div>
                                                </div>
                                                </li> -->
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="comn-lft-rght-cont">
                                                                        <label class="ellipses-market-trade-label"><a href="#" title="#ellipses-market-trade-spanellipses-market-trasdasdadadadsadaddadade-spanellipses-market-trade-span" data-toggle="tooltip" data-placement="bottom">#ellipses-market-trade-spanellipses-market-trade-spanellipses-market-trade-span</a></label>
                                                                        <span class="pull-right ellipses-market-trade-span pull-right"><a href="#" title="522423453245354325432542" data-toggle="tooltip" data-placement="bottom">23,5852252542542525 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div class="common-on-hover">
                                                                        <span>
                                                   <a href="#">
                                                   <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
                                                   </a>
                                                   </span>
                                                                        <span class="pull-right"><a href="#">View More</a></span>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </li>
                                                                <!-- li ends here -->
                                                            </ul>
                                                        </tbody>
                                                    </table>
                                                </section>
                                                <section id="content4" class="tab-content">
                                                    <table class="table">
                                                        <tbody>
                                                            <tr>
                                                                <td>ddddddd</td>
                                                                <td>@ddddddd</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Otto</td>
                                                                <td>@ddddddd</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Otto</td>
                                                                <td>@ddddddd</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </section>
                                            </div>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                </div>
                                <!-- container ends here -->
                                <!-- tab ends here -->
                            </div>
                            <!-- trade analytics tabs ends here -->
                        </div>
                        <div class="col-md-3">
                            <h6 class="head-small-key-markt">interaction analytics</h6>
                            <!-- ul starts here -->
                            <ul class="border-all">
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#s" data-toggle="tooltip" data-placement="bottom">#p</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23," data-toggle="tooltip" data-placement="bottom">23,</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li footer starts here -->
                                <li>
                                    <div class="common-on-hover">
                                        <span>
                           <a href="#">
                           <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
                           </span>
                                        <span class="pull-right"><a href="#">View More</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li footer ends here -->
                            </ul>
                            <!-- ul ends here -->
                        </div>
                        <div class="col-md-3">
                            <h6 class="head-small-key-markt">earning analytics</h6>
                            <!-- ul starts here -->
                            <ul class="border-all">
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li starts here -->
                                <li>
                                    <div class="comn-lft-rght-cont">
                                        <label class="ellipses-market-trade-label"><a href="#" title="#partyinthesky" data-toggle="tooltip" data-placement="bottom">#party#party#party#party</a></label>
                                        <span class="pull-right ellipses-market-trade-span text-right"><a href="#" title="23,525245244343" data-toggle="tooltip" data-placement="bottom">23,525245244343</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li ends here -->
                                <!-- li footer starts here -->
                                <li>
                                    <div class="common-on-hover">
                                        <span>
                           <a href="#">
                           <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
                           </span>
                                        <span class="pull-right"><a href="#">View More</a></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- li footer ends here -->
                            </ul>
                            <!-- ul ends here -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- container ends here -->
            <!-- edit bid popup starts here -->
            <div id="keyword-popup-ask-bid" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Edit Bid</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB">
                                            <span class="keyword-grey-span pull-r-marketplace">#keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left">Asking Price : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">INR : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market span-input-keyword" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB  span-blue-keyword-market">
                                            <div class="text-left">
                                                <input value="Delete" type="button" class="btn-trading-wid-auto">
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                                <input value="Place Bid" type="button" class="btn-trading-wid-auto innerMTB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- edit bid popup ends here -->
            <!-- market slab popup starts here -->
            <div id="keyword-popup-markt-slab" class="modal fade in keyword-popup current-slab-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Keyword Market Slab</h4>
                        </div>
                        <div class="modal-body padding-none">
                            <div>
                                <div class="keyword-market-botm">
                                    <!-- table starts here -->
                                    <div class="pagination-content text-center">
                                        <div class="pagination-head half innerAll bg-grey">
                                            <div class="row">
                                                <div class="col-md-3 text-white">Start Limit</div>
                                                <div class="col-md-3 text-white">End Limit</div>
                                                <div class="col-md-3 text-white">USD Price</div>
                                                <div class="col-md-3 text-white padding-left-none padding-right-none">Current BTC Price</div>
                                            </div>
                                        </div>
                                        <ul class="border-all keyword-analytics-li-pad  keyword-slab-ul">
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li>
                                                <!-- row starts here   -->
                                                <div class="row">
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">1</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">600</label>
                                                    </div>
                                                    <div class="col-md-3 text-black margin-bottom-none">
                                                        <label class="text-black margin-bottom-none">262.34</label>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="row">
                                                            <label class="text-black margin-bottom-none">0.10523000</label>
                                                        </div>
                                                    </div>
                                                    <!-- row ends here -->
                                                </div>
                                            </li>
                                            <!-- li ends here -->
                                            <!-- li starts here -->
                                            <li style="display:none;"></li>
                                            <!-- li ends here -->
                                        </ul>
                                    </div>
                                    <!-- table ends here -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- market slab popup ends here -->
            <!-- place bid popup starts here -->
            <div id="keyword-popup-place-bid" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Place Bid</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB">
                                            <span class="keyword-grey-span pull-r-marketplace">#keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left">Asking Price : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">INR : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left">
                                                <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                                <input value="Place Bid" type="button" class="btn-trading-wid-auto innerMTB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- place bid popup ends here -->
            <!-- set ask popup starts here -->
            <div id="keyword-popup-set-ask" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Set Ask</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB">
                                            <span class="keyword-grey-span pull-r-marketplace">#keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left">Your Ask : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;Not set</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">INR : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left">
                                                <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                                <input value="Set Ask" type="button" class="btn-trading-wid-auto innerMTB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- set ask popup ends here -->
            <!-- edit ask popup starts here -->
            <div id="keyword-popup-edit-ask" class="modal fade in keyword-popup" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content row">
                        <div class="modal-header custom-modal-header">
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <h4 class="modal-title">Edit Ask</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-12 innerMB">
                                            <span class="keyword-grey-span pull-r-marketplace">#keyword</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left">Your Ask : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24,588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">Highest Bid : </span>
                                                <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;94,588 <?php echo $keywoDefaultCurrencyName; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="text-left">
                                                <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-left pull-right">
                                                <span class="span-keyword-market pull-left">INR : &nbsp;</span>
                                                <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 innerMB  span-blue-keyword-market">
                                            <div class="text-left">
                                                <input value="Delete" type="button" class="btn-trading-wid-auto">
                                            </div>
                                        </div>
                                        <div class="col-md-6 innerMB">
                                            <div class="text-left pull-right">
                                                <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
                                                <input value="Set Ask" type="button" class="btn-trading-wid-auto innerMTB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- edit ask popup ends here -->
            <br/>
            <br/>
        </div>
    </main>
    <?php
   include("../../layout/transparent_footer.php");

   ?>
