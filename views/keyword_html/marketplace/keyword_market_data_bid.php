			<?php
include("../../layout/header.php");

?>
<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_keyword.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>

<main class="main-keyword">
  <!-- keyword-marketplace starts here -->
  <div class="keyword-marketplace keyword-marketplace-data keyword-markt-popup-common">

    <!-- container starts here -->
    <div class="container">

      <!-- row starts here -->
      <div class="row">
<div class="col-xs-3">
<!-- keymarketplace-data-left starts here -->
  <div class="keymarketplace-data key-mar-data-floatingmenu">
<div class="row">
<div class="col-xs-12"><div class="back-to-keymar">
<a href="index.php"><i class="fa fa-angle-left" aria-hidden="true"></i> &nbsp;&nbsp;Back to Keyword Market</a>
</div></div>
</div>

<!-- row starts here -->
<div class="row">
  <!-- trade analytics col starts here -->
  <div class="col-md-12 inner-3x innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Trade Analytics
          </a>
          <a href="keyword_market_data_latest_trade.php" class="list-group-item text-primary">Latest Trade</a>
          <a href="keyword_market_data_ask.php" class="list-group-item">Ask</a>
          <a href="keyword_market_data_bid.php" class="list-group-item active">Bid</a>
          <a href="keyword_market_data_newly_owned.php" class="list-group-item">Newly Owned</a>
      </div>
  </div>
  <!-- trade analytics col ends here -->


  <!-- interaction analytics col starts here -->
  <div class="col-md-12 half innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Interaction Analytics
          </a>
          <a href="#" class="list-group-item text-primary">All</a>
          <a href="#" class="list-group-item">Blog</a>
          <a href="#" class="list-group-item">Video</a>
          <a href="#" class="list-group-item">Image</a>
          <a href="#" class="list-group-item">Audio</a>
          <a href="#" class="list-group-item">Search</a>

      </div>
  </div>
  <!-- interaction analytics col ends here -->

  <!-- earning analytics col starts here -->
  <div class="col-md-12 half innerMT">
      <div class="list-group list-cust border-all">
          <a href="#" class="list-group-item">
              Earning Analytics
          </a>
          <a href="#" class="list-group-item text-primary">All</a>
          <a href="#" class="list-group-item">Blog</a>
          <a href="#" class="list-group-item">Video</a>
          <a href="#" class="list-group-item">Image</a>
          <a href="#" class="list-group-item">Audio</a>
          <a href="#" class="list-group-item">Search</a>

      </div>
  </div>
  <!-- earning analytics col ends here -->

  <!-- ask col starts here -->
  <!-- <div class="col-md-12">
    <div class="pull-left col-xs-12 keyword-markt-ask half">
                                     <label class="pull-left">Bid : </label>
                                     <span class="pull-right">7,436</span>
                                  </div>
  </div>
  <div class="col-md-12">
  &nbsp;
  </div> -->
  <!-- ask col ends here -->





  </div>
<!-- row ends here -->
  </div>
<!-- keymarketplace-data-left ends here -->
</div>

<div class="col-xs-9">
<!-- keymarketplace-data-right starts here -->
<div class="keymarketplace-data key-mar-data-floatingmenu">
<h3 class="heading-key-markt margin-top-none inner-3x innerMB marg-botm">Keyword Market Data</h3>
<!-- pagination starts here -->
<div class="pagination-content text-center">
<!-- pagination head starts here -->
  <div class="pagination-head half innerAll padding-key-market-data">
<div class="row">
<div class="col-xs-4 text-white">keywords</div>
<div class="col-xs-3 text-white">Bid Price</div>
<div class="col-xs-5 text-white padding-left-none padding-right-none">
<!-- keyword-radio-btn starts here -->
  <div class="keyword-radio-btn pull-right">
                                   <div class="row">
                                      <div class="col-xs-12 padding-right-none">
                                         <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                            <input type="radio" name="gender" id="male" value="male" checked="true">
                                            <label for="male">
                                            Latest
                                            </label>
                                         </div>
                                         <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                            <input type="radio" name="gender" id="female" value="female">
                                            <label for="female">
                                            Highest
                                            </label>
                                         </div>
                                         <div class="radio radio-primary radio-div margin-top-none margin-bottom-none">
                                            <input type="radio" name="gender" id="other" value="other">
                                            <label for="other">
                                            Lowest
                                            </label>
                                         </div>
                                      </div>
                                   </div>
                                </div>
<!-- keyword-radio-btn ends here     -->
</div>
</div>
  </div>
<!-- pagination head ends here -->

<!-- pagination body starts here -->
<ul class="border-all">




<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->


<!-- li starts here -->
<li>
<!-- row starts here   -->
	<div class="row">
		<div class="col-md-4">
			<span class="txt-blue ellipses"><a href="#" class="display-in-block" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="#e">#g</a></span>
		</div>
		<div class="col-md-3 text-black">
			<label class="text-black ellipses margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="2,540 <?php echo $keywoDefaultCurrencyName; ?>">2,540 <?php echo $keywoDefaultCurrencyName; ?></a></label>
		</div>
		<div class="col-md-5 pull-right text-right">
			<div class="row pull-right">
				<div class="col-xs-6 text-right padding-right-none">
					<input value="Place Bid" type="button" class="btn-trading" data-toggle="modal" data-target="#keyword-popup-set-ask">
					</div>
					<div class="col-xs-6 text-right">
						<input value="Buy Now" type="button" class="btn-trading-dark" data-toggle="modal" data-target="#enquirypopup">
						</div>
					</div>
				</div>
			</div>
			<!-- row ends here -->
		</li>
<!-- li ends here -->

                                       </ul>
<!-- pagination body ends here -->

<!-- pagination number starts here -->
<div class="col-md-12 inner-2x innerMT padding-left-none padding-right-none">
    <div class="pull-left col-xs-12 half">

<div class="row">
<div class="col-md-5 padding-left-none padding-right-none"><div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div></div>
<div class="col-md-7 padding-left-none padding-right-none">
	<div class="pagination-cont pull-right">

																 <span class="span-keyword-market pull-left">Go to : &nbsp;</span>
																 <input type="text" value="" placeholder="" class="span-blue-keyword-market border-all">

																 <ul class="pagination">
														 		              <li class="disabled"><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
														 		              <li class="active"><a href="#">1 <span class="sr-only">(current)</span></a></li>
														 		              <li><a href="#">2</a></li>
														 		              <li><a href="#">3</a></li>
														 		              <li><a href="#">4</a></li>
														 		              <li><a href="#">5</a></li>
														 		              <li><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
														 		            </ul>

	</div>
</div>
</div>




                                  </div>
  </div>
<!-- pagination number starts here -->

</div>
<!-- pagination ends here -->
</div>
<!-- keymarketplace-data-right ends here -->
</div>

      </div>
      <!-- row ends here -->

    </div>
    <!-- container ends here -->
		<!-- place bid popup starts here -->
		<div id="keyword-popup-place-bid" class="modal fade in keyword-popup" role="dialog">
			 <div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content row">
						 <div class="modal-header custom-modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title">Place Bid</h4>
						 </div>
						 <div class="modal-body">
								<div>
									 <div class="">
											<div class="row">
												 <div class="col-md-12 innerMB">
														<span class="keyword-grey-span pull-r-marketplace">#keyword</span>
												 </div>
											</div>
											<div class="row">
												 <div class="col-md-6">
														<div class="text-left">
															 <span class="span-keyword-market pull-left">Asking Price : </span>
															 <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
														</div>
												 </div>
												 <div class="col-md-6">
														<div class="text-left pull-right">
															 <span class="span-keyword-market pull-left">Highest Bid : </span>
															 <span class="span-blue-keyword-market pull-r-marketplace">&nbsp;24, 588 <?php echo $keywoDefaultCurrencyName; ?></span>
														</div>
												 </div>
											</div>
											<div class="row">
												 <div class="col-md-6">
														<div class="text-left">
															 <span class="span-keyword-market pull-left"><?php echo $keywoDefaultCurrencyName; ?> : &nbsp;</span>
															 <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
														</div>
												 </div>
												 <div class="col-md-6">
														<div class="text-left pull-right">
															 <span class="span-keyword-market pull-left">INR : &nbsp;</span>
															 <input type="text" value="" placeholder="Enter amount" class="span-blue-keyword-market  span-input-keyword" />
														</div>
												 </div>
											</div>
											<div class="row">
												 <div class="col-md-6 innerMB">
														<div class="text-left">
															 <!-- <input value="Delete" type="button" class="btn-trading-wid-auto"> -->
														</div>
												 </div>
												 <div class="col-md-6 innerMB">
														<div class="text-left pull-right">
															 <input value="Cancel" type="button" class="btn-trading-wid-auto-dark innerMTB">&nbsp;&nbsp;
															 <input value="Place Bid" type="button" class="btn-trading-wid-auto innerMTB">
														</div>
												 </div>
											</div>
									 </div>
								</div>
						 </div>
					</div>
			 </div>
		</div>
		<!-- place bid popup ends here -->
  </div>
  <!-- keyword-marketplace ends here -->

</main>
<?php
   include("../../layout/transparent_footer.php");

   ?>
