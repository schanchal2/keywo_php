<?php
   include("../../layout/header.php");

   ?>
   <link rel="stylesheet" href="
      <?php echo $rootUrlCss; ?>app_keyword.css
      <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main>
  <div class="container">
<input type="button" name="" value="Click here for Bitgo Payment" data-toggle="modal" data-target="#procceedToPayment">
  </div>
</main>
<!-- BitGio payment modal -->

<div id="procceedToPayment" class="keyword-markt-popup-common modal fade" role="dialog">

  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header innerAll">
        <h4 class="margin-none text-blue">Keyword Purchase</h4>
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-minus-circle text-color-Gray"></i></button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-xs-12 keywo-banner half innerMT">
            <img src="<?php echo $rootUrlImages?>/keywo_banner.png" class="img-responsive" alt="Keywo">
          </div>
        </div>
        <div class="row innerMT inner-2x">
          <div class="col-xs-12">
            <div class="purchase-keywords-list">
              Purchase of keyword <a class="text-blue half innerMLR" data-toggle="tooltip" title="#palak">#palak</a><a class="text-blue half innerMLR" data-toggle="tooltip" title="#palak">#work</a><a class="text-blue half innerMLR" data-toggle="tooltip" title="#palak">#home</a><a class="text-blue half innerMLR" data-toggle="tooltip" title="#palak">#from</a>
            </div>
          </div>
        </div>
        <div class="row innerMTB">
          <div class="col-xs-3">
            Payable amount
          </div>
          <div class="col-xs-1">
            :
          </div>
          <div class="col-xs-8">
            200 <?php echo $keywoDefaultCurrencyName; ?>
          </div>
          <div class="col-xs-3">
            Wallet blanace
          </div>
          <div class="col-xs-1">
            :
          </div>
          <div class="col-xs-8">
            200 <?php echo $keywoDefaultCurrencyName; ?>
          </div>
          <div class="col-xs-3">
            Payment Due
          </div>
          <div class="col-xs-1">
            :
          </div>
          <div class="col-xs-8">
            200 <?php echo $keywoDefaultCurrencyName; ?> (0.07982 BTC)
          </div>
        </div>
        <div class="row">
          <div class="col-xs-3 padding-none">
            <a><img class="img-responsive" src='https://chart.googleapis.com/chart?cht=qr&chl=hello&chs=180x180&choe=UTF-8&chld=L|2' alt='QR Code'/></a>
          </div>
          <div class="col-xs-9 padding-left-none innerMT">
            <div class="">
              Send excatly <span class="text-blue">3.49382749 BTC</span> to this address
            </div>
            <div class="innerMTB form-group">
              <input type="text" name="" class="bg-light-purple form-control" value="1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX">
            </div>
            <div class="row">
              <div class="col-xs-8 half innerL">
                <div class="btn btn-social-wid-auto pull-left">
                  Copy Address
                </div>
              </div>
              <div class="col-xs-4 text-right">
                <span>13:27</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-12">
            <div class="inner-3x innerMT text-center">
              Waiting for payment...
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

   <?php
      include("../../layout/social_footer.php");

      ?>
