<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<br>
<br>
<br>
<br>
<br>
<br>

<ul>
    <li class="text-White"> White</li>
    <li class="text-Blue"> Blue</li>
    <li class="text-Black"> Black</li>
    <li class="text-Dark-Gray"> Dark-Gray</li>
    <li class="text-Darkest-Gray"> Darkest-Gray</li>
    <li class="text-Light-Blue"> Light-Blue</li>
    <li class="text-Primary-blue"> Primary-blue</li>
    <li class="text-Text-Primary-Blue"> Text-Primary-Blue</li>
    <li class="text-Dark-Blue"> Dark-Blue</li>
    <li class="text-Lightest-grey"> Lightest-grey</li>
    <li class="text-Lightest-Blue"> Lightest-Blue</li>
    <li class="text-Dark-Blue-Hvr"> Dark-Blue-Hvr</li>
    <li class="text-Light-Blue-Hvr"> Light-Blue-Hvr</li>
    <li class="text-Bg-Concrete"> Bg-Concrete</li>
    <li class="text-Light-Black"> Light-Black</li>
    <li class="text-Red"> Red</li>
    <li class="text-Deep-Sky-Blue"> Deep-Sky-Blue</li>
    <li class="text-Summer-Sky"> Summer-Sky</li>
    <li class="text-Slate-Grey"> Slate-Grey</li>
    <li class="text-Arapawa"> Arapawa</li>
    <li class="text-Light-Grey"> Light-Grey</li>
    <li class="text-Light-Gray"> Light-Gray</li>
    <li class="text-Gray"> Gray</li>
    <li class="text-Light-Gray-Border"> Light-Gray-Border</li>
    <li class="text-Placeholder"> Placeholder</li>
    <li class="text-Padding"> Padding</li>
    <li class="text-Badge-Danger"> Badge-Danger</li>
    <li class="text-Badge-Blue"> Badge-Blue</li>
    <li class="text-Results-Num-Txt-Col"> Results-Num-Txt-Col</li>
    <li class="text-Results-Title-Col"> Results-Title-Col</li>
    <li class="text-Results-Desc-Col"> Results-Desc-Col</li>
    <li class="text-App-Primary"> App-Primary</li>
    <li class="text-App-Success"> App-Success</li>
    <li class="text-Username-Link"> Username-Link</li>
    <li class="text-Username-Follow"> Username-Follow</li>
</ul>
<ul>
    <li class="bg-White"> White</li>
    <li class="bg-Blue"> Blue</li>
    <li class="bg-Black"> Black</li>
    <li class="bg-Dark-Gray"> Dark-Gray</li>
    <li class="bg-Darkest-Gray"> Darkest-Gray</li>
    <li class="bg-Light-Blue"> Light-Blue</li>
    <li class="bg-Primary-blue"> Primary-blue</li>
    <li class="bg-Text-Primary-Blue"> Text-Primary-Blue</li>
    <li class="bg-Dark-Blue"> Dark-Blue</li>
    <li class="bg-Lightest-grey"> Lightest-grey</li>
    <li class="bg-Lightest-Blue"> Lightest-Blue</li>
    <li class="bg-Dark-Blue-Hvr"> Dark-Blue-Hvr</li>
    <li class="bg-Light-Blue-Hvr"> Light-Blue-Hvr</li>
    <li class="bg-Bg-Concrete"> Bg-Concrete</li>
    <li class="bg-Light-Black"> Light-Black</li>
    <li class="bg-Red"> Red</li>
    <li class="bg-Deep-Sky-Blue"> Deep-Sky-Blue</li>
    <li class="bg-Summer-Sky"> Summer-Sky</li>
    <li class="bg-Slate-Grey"> Slate-Grey</li>
    <li class="bg-Arapawa"> Arapawa</li>
    <li class="bg-Light-Grey"> Light-Grey</li>
    <li class="bg-Light-Gray"> Light-Gray</li>
    <li class="bg-Gray"> Gray</li>
    <li class="bg-Light-Gray-Border"> Light-Gray-Border</li>
    <li class="bg-Placeholder"> Placeholder</li>
    <li class="bg-Padding"> Padding</li>
    <li class="bg-Badge-Danger"> Badge-Danger</li>
    <li class="bg-Badge-Blue"> Badge-Blue</li>
    <li class="bg-Results-Num-Txt-Col"> Results-Num-Txt-Col</li>
    <li class="bg-Results-Title-Col"> Results-Title-Col</li>
    <li class="bg-Results-Desc-Col"> Results-Desc-Col</li>
    <li class="bg-App-Primary"> App-Primary</li>
    <li class="bg-App-Success"> App-Success</li>
    <li class="bg-Username-Link"> Username-Link</li>
    <li class="bg-Username-Follow"> Username-Follow</li>
</ul>

