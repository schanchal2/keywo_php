<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerTB WT5">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            wallwt sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <div class="row">
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item active"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of wallwt sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row ">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Cashout </h4>
                        <div class="card bg-White border-all  padding-bottom-none innerMB f-sz15">
                            <div class="row">
                                <div class="col-xs-12 innerMTB inner-2x">
                                    <form class="form-horizontal">
                                        <!-- 
                                     -->
                                        <div class="form-group  margin-bottom-none filter  clearfix">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right  text-grayscale-6">Enter Amount : </label>
                                            <div class="col-sm-4">
                                                <div class="input-group text-grayscale-4">
                                                    <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="" placeholder="400">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"><?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="400" type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6">USD</span>
                                                </div>
                                            </div>
                                            <!-- <button type="submit" class="bg-Light-Blue btn-xs btn  col-sm-2 text-White pull-right wallet-Request">Proceed</button> -->
                                            <div class="col-md-2 text-center keyword-checkout">
                                                <input value="Proceed" type="button" class="btn-pay-now f-sz18 l-h12">
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="current-wallet-balance innerMT innerAll inner-2x">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="current-w1">
                                        <h5 class="f-sz16 margin-top-none">Current Wallet Balance is : 300 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your cashout amount : 400 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h5 class="f-sz16 text-center margin-top-none">Cashout <?php echo $keywoDefaultCurrencyName; ?> with <i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                    <div class="row leadCorrect-t ">
                                        <div class="col method">Method</div>
                                        <div class="col text-center fees ">Fees</div>
                                        <div class="col text-center netAmmount">Net Ammount</div>
                                        <div class="col currency"></div>
                                    </div>
                                    <hr class="margin-top-none">
                                    <div class="row leadCorrect-t">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16 ">
                                                <input type="radio" name="method" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label class="" for="Bitcoin">
                                                    Bitcoin
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees ">0.00083</div>
                                        <div class="col text-center netAmmount">0.08</div>
                                        <div class="col currency text-right">BTC</div>
                                    </div>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16 ">
                                                <input type="radio" name="method" id="PayPal" value="Bitcoin" checked="">
                                                <label class="" for="PayPal">
                                                    PayPal
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-center fees ">1.00</div>
                                        <div class="col text-center netAmmount">99.00</div>
                                        <div class="col currency text-right">USD</div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center innerMB keyword-checkout">
                                    <input value="Proceed to Cashout" type="button" class="btn-pay-now f-sz16 l-h12 innerMT inner-2x" data-toggle="modal" data-target="#proceed-to-cashout">
                                </div>
                            </div>
                        </div>
                        <div>
                            <!-- 

                         -->
                            <div class="row  inner-2x innerT">
                                <div class="col-xs-6 ">
                                    <div class="bg-White border-all clearfix cashout-card">
                                        <div class="modal-header padding-none">
                                            <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> <p class="leadCorrect-b">Current Bitcoin Cashout Address </p></h5> </div>
                                        <div class="modal-body padding-none">
                                            <p class="innerAll text-grayscale-5 f-sz16 margin-bottom-none l-h10 inner-2x"><span class="underline-address">Bitcoin Wallet</span> Address</p>
                                            <form class="form-horizontal innerLR inner-2x">
                                                <div class="form-group margin-bottom-none">
                                                    <div class="col-sm-12">
                                                        <div class="half innerMLR">
                                                            <input disabled="" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb change-input" value="asd3 as5w df6f asd5" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="innerAll inner-2x clearfix">
                                            <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto f-sz14 change-input">Change</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 ">
                                    <div class="bg-White border-all clearfix cashout-card">
                                        <div class="modal-header padding-none">
                                            <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> <p class="leadCorrect-b">Current Paypal Cashout Address</p> </h5> </div>
                                        <div class="modal-body padding-none">
                                            <p class="innerAll text-grayscale-5 f-sz16 margin-bottom-none l-h10 inner-2x"><span class="underline-address">PayPal Email</span> Address</p>
                                            <form class="form-horizontal innerLR inner-2x">
                                                <div class="form-group margin-bottom-none">
                                                    <div class="col-sm-12">
                                                        <div class="half innerMLR">
                                                            <input type="email" disabled="" class="form-control b-shadow-none inputAddress bg-grayscale-fb change-input" value="asd@asd.com">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="innerAll clearfix inner-2x">
                                            <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto f-sz14 change-input">Change</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 

                             -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive wallet__trasactionHistory text-Light-Black innerT inner-2x">
                            <table class="table table-hover border-all text-center ">
                                <thead class="text-White ">
                                    <tr>
                                        <th> Quantity </th>
                                        <th>Discription </th>
                                        <th>
                                            <label class="dropdownOptions ">
                                                <select class="selectpicker">select class="selectpicker">
                                                    <option title="Status">Completed</option>
                                                    <option title="Status">In Progress</option>
                                                    <option title="Status">etc.</option>
                                                </select>
                                            </label>
                                        </th>
                                        <th>Date </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue "> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue "> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue"> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue"> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue"> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-Dark-Blue text-left"> 100 <?php echo $keywoDefaultCurrencyName; ?> </td>
                                        <td class="text-grayscale-4"> You cashout </td>
                                        <td class="text-blue"> In Progress </td>
                                        <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                        <td><a href="# " data-toggle="modal" data-target="#Detail"> Detail </a></td>
                                        <td>
                                            <div class="btn-group action-btn " role="group ">
                                                <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest"> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row keyword-marketplace-data innerT inner-2x">
                    <div class="col-md-5">
                        <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
                    </div>
                    <div class="col-md-7">
                        <div class="pagination-cont pull-right">
                            <ul class="pagination">
                                <li class="disabled"><a  class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                                <li class="disabled"><a  class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
    <!-- 


     -->
    <div class="modal fade madal--1" id="proceed-to-cashout" tabindex="-1" role="dialog" aria-labelledby="">
        <div class="modal-dialog innerMT inner-9x" role="document">
            <div class="modal-content no-radius ">
                <div class="modal-header border-none">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b" id=""> </h4>
                </div>
                <form class="form-horizontal">
                    <div class="modal-body padding-bottom-none ">
                        <div class="panel-body innerMLR inner-4x padding-none">
                            <p class="text-center f-sz18 text-color-Blue margin-none"> Your Bitcoin address is not set. Please set your Bitcoin Wallet address.</p>
                        </div>
                    </div>
                    <div class="modal-footer innerAll border-none padding-top-none">
                        <input value="Cancle" type="button" class="btn-trading-wid-auto innerMTB innerMR" data-dismiss="modal">
                        <input value="Go to cashout setting" type="button" class="btn-trading-dark btn-trading-wid-auto innerMTB" data-dismiss="modal">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });



    jQuery(document).ready(function($) {
        $("button.change-input").on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $(this).text("Verify Now").attr('type', 'submit');
            $(this).parents(".cashout-card").find('input.change-input').removeAttr('disabled')
        });
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php");?>
<?php 

/*=============================================
(22-2-17)
    -placehoder color is kept same as it of in "/views/prafull_wallet/CashoutSetting.php" and of  "CashoutSetting UI Design"

(27-2-17)
    BUG:EG-291
        - changes made to siblings copied to this page. 
        -------------------------------------------------- 
        -1)Enter amount text and amount font color is different as per design sheet of cash out page.                               @done
        -2)Bad spacing  at start of Label in proceed and proceed to purchase boxes.                                                 @done                                       
        -3)Change payment text with cashout from proceed to purchase boxes.                                                         @done
        -4)Large text font size of proceed to purchase boxes as compare to design sheet.                                            @photoshop unavailable  
        -5)Current Bitcoin Cashout Address and Current PayPal Cashout Address text font are different as compare to design sheet.   @photoshop unavailable
        --------------------------------------------------
        
        
            TODO:
            - 1.input type can b changed to text from number as it is done in page "views/prafull_wallet/purchase.php"              @done
            - 2.vertical spacing can be incresed in "purchase boxes" to match layout with PSD 
            - 3.compare this page with "views/prafull_wallet/purchase.php".
    
=============================================*/
?>
