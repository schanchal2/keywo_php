<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block text-left"> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <div class="row">
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item active"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Account Level</h4>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll">
                                        <span class="level-verified-tick half innerT">
                            <i class="fa fa-check-circle text-App-Success pull-right"></i>
                        </span>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 1</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15">  Email Verified</span>
                                        <span class="text-Blue btn-block margin-none text-center">abc@asd.com</span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">100 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Request<span class="pull-right f-sz15">100 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Purchase<span class="pull-right f-sz15">100 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">100 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15">40 per Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15">1%</span></li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-App-Success innerMT text-White">Completed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll ">
                                        <span class="level-verified-tick half innerT"><i class="fa fa-check-circle text-App-Success pull-right"></i></span>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 2</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15">   Mobile Verified</span>
                                        <span class="text-Blue btn-block margin-none text-center"> +91 8989898989 </span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">200 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Request<span class="pull-right f-sz15">200 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Purchase<span class="pull-right f-sz15">200 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">200 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15">60 per Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15">0.7%</span></li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" class="btn bg-App-Success innerMT text-White">Completed</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="bg-White border-all clearfix innerB account_level-card">
                                    <div class="modal-header half innerAll ">
                                        <span class="level-verified-tick half innerT"></span>
                                        <h4 class="modal-title innerT account_level-title text-Text-Primary-Blue text-center">Level 3</h4> </div>
                                    <div class="modal-body innerAll">
                                        <span class="btn-block  text-Dark-Gray text-center f-sz15"> Verify photo ID and Address Proof</span>
                                        <span class="text-Blue btn-block margin-none text-center">  &nbsp;</span>
                                        <ul class="list-unstyled text-Dark-Gray">
                                            <li class="innerT f-sz16">Send<span class="pull-right f-sz15">300 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Request<span class="pull-right f-sz15">300 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Purchase<span class="pull-right f-sz15">300 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Cashout<span class="pull-right f-sz15">300 <?php echo $keywoDefaultCurrencyName; ?></span></li>
                                            <li class="innerT f-sz16">Interactions<span class="pull-right f-sz15">80 per Day</span></li>
                                            <li class="innerT f-sz16">Transaction fees<span class="pull-right f-sz15">0.5%</span></li>
                                        </ul>
                                        <div class="text-center">
                                            <button type="button" data-target="#OTPVerification" data-toggle="modal" class="btn bg-Blue innerMT text-White">Apply Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->

    <!-- Modal -->
    <div id="OTPVerification" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close text-color-Gray" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-blue">Mobile Verification</h4>
          </div>
          <div class="modal-body text-center">
            <div class="">
              <h5 class="text-dark-green">Please enter OTP that you received</h5>
            </div>
            <div class="">
              <input type="number" name="" value="">
            </div>
            <div class="innerMTB inner-2x">
              <input type="button" class="btn-social-wid-auto btn-xs innerMR" value="Resend OTP">
              <input type="button" class="btn-social-dark btn-xs" value="Continue">
            </div>
          </div>
        </div>

      </div>
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================
              21-2-17



              (24-2-17)
              BUG:  Eg-289
                1)removed
                2)Transaction fees text spelling is wrong from Level 2 box.                                             @done   corrected
                3)Need to make small size of account level boxes.(See the design sheet).                                @done   modified model header section
                4)Bad spacing in between Level text and Email Verified (All three Level boxes.)                         @done   added ".margin-none"
                5)Place mobile number instead of Email address (level2 Box.)                                            @done   replaced




=========================================================================================================================================-->
