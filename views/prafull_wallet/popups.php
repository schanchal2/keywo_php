<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container">
        <div class="col-xs-3">
            <div class="social-left-panel">
                <!-- my-info-card  -->
                <div class="my-info-card bg-White border-all">
                    <div class="clearfix innerAll ">
                        <div class="my-info-img pull-left innerMR">
                            <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                        </div>
                        <!-- my-picture  -->
                        <div class="my-info-detail pull-left">
                            <div class="my-info-name-container innerMB">
                                <!-- my-name  -->
                                <span class="my-info-name">Vishal Gupta</span>
                            </div>
                            <!-- my-status  -->
                            <div class="my-balance-status">
                                Balance
                                <span class="my-balance-count text-Blue btn-block text-left"> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                            </div>
                        </div>
                    </div>
                    <!-- my-info-detail  -->
                </div>
            </div>
            <br>
            <!-- social-left-panel  -->
            <!--====================================
            =            wallwt sidebar            =
            =====================================-->
            <div class="keymarketplace-data key-mar-data-floatingmenu">
                <!-- row starts here -->
                <div class="row">
                    <!-- trade analytics col starts here -->
                    <div class="col-md-12 inner-2x">
                        <div class="list-group list-cust border-all">
                            <a href="" class="list-group-item"> Wallet</a>
                            <a href="" class="list-group-item"> Transaction History</a>
                            <a href="" class="list-group-item"> Send</a>
                            <a href="" class="list-group-item active"> Request</a>
                            <a href="" class="list-group-item"> Purchase</a>
                            <a href="" class="list-group-item"> Cashout</a>
                            <a href="" class="list-group-item"> Currency Peferences</a>
                            <a href="" class="list-group-item"> Cashout setting</a>
                            <a href="" class="list-group-item"> Two Factor (2FA)</a>
                            <a href="" class="list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                        </div>
                    </div>
                    <!-- trade analytics col ends here -->
                    <div class="col-md-12">
                        &nbsp;
                    </div>
                    <!-- ask col ends here -->
                </div>
                <!-- row ends here -->
            </div>
            <!--====  End of wallwt sidebar  ====-->
        </div>
        <!-- col-xs-3 -->
        <div class="col-xs-9">
            <div class="row">
                <div class="col-lg-12">
                <h4 class="text-Blue margin-top-none"> Request</h4>
                    <div class="card bg-White border-all innerAll inner-2x"></div>
                </div>
                <div class="col-xs-12 inner-5x innerMTB">
                  <input type="button" name="" value="googleAuthenticatorVerification" data-target="#googleAuthenticator" data-toggle="modal">
                  <input type="button" name="" value="backupCodeVerification" data-target="#backupCodeVerification" data-toggle="modal">
                </div>
                <div class="col-xs-12 inner-5x innerMTB">
                  <input type="button" name="" value="cannotAccess" data-target="#cantAccessPhone" data-toggle="modal">
                  <input type="button" name="" value="signInVerification" data-target="#signInVerification" data-toggle="modal">
                </div>
            </div>


        </div>
        <!-- col-xs-9 -->
    </div>
    <!-- container -->



  <!-- googleAuthenticator Modal -->
  <div id="googleAuthenticator" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Google Authenticator Verification</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="authenticator-text innerMB">
              Enter your OTP (one time password) sent on your authenticator app<i class="fa fa-asterisk error-color"></i>
            </div>
            <div class="form-group">
              <input type="text" name="" class="form-control bg-light-purple" value="">
            </div>
            <div class="">
              <div class="">
                <a href="#" class="text-sky-blue">Cant't access any of your phone?</a>
              </div>
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll pull-left">
                  Cancel
                </div>
                <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right">
                  Submit
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- cantAccessPhone Modal -->
  <div id="cantAccessPhone" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="form-group access-options">
              <div class="radio radio-primary">
                  <input type="radio"  id="accessBtn1" name="accessBtn" value="">
                  <label for="accessBtn1" class="radioLabel">
                      I already have the one timebackup verification code
                  </label>
              </div>
              <div class="radio radio-primary">
                  <input type="radio" id="accessBtn2" name="accessBtn" value="">
                  <label for="accessBtn2" class="radioLabel">
                      I don't have phone or backup verification code
                  </label>
              </div>
            </div>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll pull-left">
                  Proceed
                </div>
                <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right">
                  Cancel
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- backupCodeVerification Modal -->
  <div id="backupCodeVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="">
            <div class="text-center innerMB">
              <div class="backup-text border-bottom half innerB innerMB">
                Backup Code Verification
              </div>
            </div>
            <div class="">
              <p class="margin-top-none">The backup code is used to access your account in case of emergencies like misplaced or lost phone or if you have problems receiving code.</p>
            </div>
            <div class="">
              <div class="backup-verification-text innerMB half">
                Enter Backup Verification code<i class="fa fa-asterisk error-color"></i>
              </div>
              <div class="form-group">
                <input type="text" name="" class="form-control bg-light-purple" value="">
              </div>
            </div>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll pull-left">
                  Verify code
                </div>
                <div class="btn btn-social-wid-auto-dark btn-xs innerMAll pull-right">
                  Cancel
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- signInVerification Modal -->
  <div id="signInVerification" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header innerLR innerT padding-bottom-none">
          <h4 class="text-blue">Can't access your phone?</h4>
          <span class="close text-color-Gray innerR" data-dismiss="modal"><i class="fa fa-close"></i></span>
        </div>
        <div class="modal-body">
          <div class="text-center">
            <div class="innerMB">
              <div class="signin-text border-bottom half innerB">
                Sign-in Verification Code
              </div>
            </div>
            <div class="">
              <p class="margin-top-none">If you can't access your phone in addition to the backup verification code<br>
                Contact to <a href="#">contact@searchtrade.com</a>
              </p>
            </div>
            <div class="">
              <div class="clearfix btn-bottom">
                <div class="btn btn-social-wid-auto btn-xs innerMAll">
                  Back to Home
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



    <script type="text/javascript">
    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================
            ( 16-02-17)
            table base created

            /**

                TODO:
                    ( 17-02-17)
                - appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
                - color name comflict in '.bg-blue-key-dark, .bg-blue-key'
                - looks better if .my-info-card bottom padding is removed
                    ( 18-02-17)
                - input highlighting can be improoved.
             */








=========================================================================================================================================-->
