<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerTB WT1  ">
<div class="container padding-none row-10">
    <div class="row">
        <div class="col-xs-3">
            <div class="social-left-panel">
                <!-- my-info-card  -->
                <div class="my-info-card bg-White border-all text-Light-Black">
                    <div class="clearfix innerAll ">
                        <div class="my-info-img pull-left innerMR">
                            <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                        </div>
                        <!-- my-picture  -->
                        <div class="my-info-detail pull-left">
                            <div class="my-info-name-container innerMB">
                                <!-- my-name  -->
                                <span class="my-info-name">Vishal Gupta</span>
                            </div>
                            <!-- my-status  -->
                            <div class="my-balance-status">
                                Balance
                                <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                            </div>
                        </div>
                    </div>
                    <!-- my-info-detail  -->
                </div>
            </div>
            <br>
            <!-- social-left-panel  -->
            <!--====================================
            =            wallet sidebar            =
            =====================================-->
            <div class="keymarketplace-data key-mar-data-floatingmenu">
                <div class="row">
                    <div class="col-md-12 inner-2x">
                        <div class="list-group list-cust border-all">
                            <a href="" class="f-sz16 list-group-item"> Wallet</a>
                            <a href="" class="f-sz16 list-group-item active"> Transaction History</a>
                            <a href="" class="f-sz16 list-group-item"> Send</a>
                            <a href="" class="f-sz16 list-group-item"> Request</a>
                            <a href="" class="f-sz16 list-group-item"> Purchase</a>
                            <a href="" class="f-sz16 list-group-item"> Cashout</a>
                            <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                            <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                            <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                            <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                        </div>
                    </div>
                </div>
                <!-- row ends here -->
            </div>
            <!--====  End of wallet sidebar  ====-->
        </div>
        <!-- col-xs-3 -->
        <div class="col-xs-9">
            <div class="row">
                <div class="col-xs-3 innerMB">
                    <div class="keyword-info">
                        <div class="keyword-info-num bg-blue-key">2,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                        <div class="keyword-num-status">Content Consumption</div>
                    </div>
                </div>
                <div class="col-xs-3 innerMB">
                    <div class="keyword-info">
                        <div class="keyword-info-num bg-blue-key-dark">12,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                        <div class="keyword-num-status light-blue-col">Content Upload</div>
                    </div>
                </div>
                <div class="col-xs-3 innerMB">
                    <div class="keyword-info">
                        <div class="keyword-info-num bg-blue-key">12,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                        <div class="keyword-num-status ">Referral</div>
                    </div>
                </div>
                <div class="col-xs-3 innerMB">
                    <div class="keyword-info">
                        <div class="keyword-info-num bg-blue-key-dark">12,545 <?php echo $keywoDefaultCurrencyName; ?></div>
                        <div class="keyword-num-status light-blue-col">Keyword</div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 filter">
                    <form class="form-inline innerMT">
                        <div class="form-group innerR">
                            <label class="text-Gray f-sz15">Sort By : </label>
                            <div class="input-group">
                                <div class="Sort-by">
                                    <label class="dropdownOptions pull-right ">
                                        <select class="selectpicker">
                                            <option value="">All</option>
                                            <option value="sent">Sent</option>
                                            <option value="received">Received</option>
                                            <option value="paymentrequest">Request</option>
                                            <option value="cashout">Cashout</option>
                                            <option value="purchase">Purchase</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="text-Gray f-sz15">Date : </label>
                            <div class="input-group text-Blue dateRange">
                                <input type="text" class="form-control text-Blue" value="10/9/2017-12/9/2017" placeholder="" id="datepicker" name="daterange">
                                <span class="input-group-addon  bg-white text-Blue"> <i class="fa fa-calendar" id=""></i> </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group ">
                                <button type="button" class="btn bg-Lightest-Blue text-White innerLR" data-toggle="modal" data-target="#DownloadTransactionHistory"> <i class="fa fa-download"></i> </button>
                            </div>
                        </div>
                        <div class="form-group pull-right">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button">  <i class="fa fa-search text-grayscale-6"></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive wallet__trasactionHistory  text-Light-Black">
                        <table class="table table-hover border-all text-center">
                            <thead class="text-White">
                                <tr>
                                    <th class="text-left">Type </th>
                                    <th> Quantity </th>
                                    <th>Discription </th>
                                    <th>
                                        <label class="dropdownOptions ">
                                            <select class="selectpicker">
                                                <option title="Status">Completed</option>
                                                <option title="Status">In Progress</option>
                                                <option title="Status">etc.</option>
                                            </select>
                                        </label>
                                    </th>
                                    <th>Date </th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left"> Sent </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">to xyz               </span> </td>
                                    <td> Transaction Confirmed <a href="#" class="text-Light-Blue btn-block Transaction-Confirmed">736 confirmation</a> </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#"> Detail </a></td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Recived </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td> You Recived <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">from xyz </span> </td>
                                    <td class="text-Light-Blue"> In Progress </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#"> Detail       </a></td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Purchase </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">@4.22USD</span> </td>
                                    <td> You Purchased <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">via xyz </span> </td>
                                    <td class="text-App-Success"> Completed </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#">   Detail     </a></td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Cashout </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">to xyz </span> </td>
                                    <td class="text-App-Success"> Completed </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#">   Detail     </a></td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Request Sent </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">to xyz </span> </td>
                                    <td class="text-"> Accepted </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#"> Detail       </a></td>
                                </tr>
                                <tr>
                                    <td class="text-left"> Request Recived </td>
                                    <td class="text-Dark-Blue"> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block">to xyz </span> </td>
                                    <td class="text-Red"> Rejected <i class="fa fa-question-circle text-Gray"></i> </td>
                                    <td> 10th Jan, 2015 </td>
                                    <td><a href="#">   Detail     </a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row keyword-marketplace-data innerT inner-2x">
                <div class="col-md-5">
                    <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
                </div>
                <div class="col-md-7">
                    <div class="pagination-cont pull-right">
                        <ul class="pagination">
                            <li ><a  class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li><a class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- col-xs-9 -->
    </div>
</div>
<!-- container -->
</main>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript">
$('.filter input').on('focus', function(event) {
event.preventDefault();
$(this).parent().addClass('input-is-focused');
});
$('.filter input').on('focusout', function(event) {
event.preventDefault();
$(this).parent().removeClass('input-is-focused');
});
</script>
<!--=======================================
=            date range picker            =
========================================-->
<!-- Enable either of the plugin below and in "layout/transparent_footer.php"-->
<!--  PLUGIN 2:  bootstrap.daterangepicker -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script type="text/javascript">
$('input[name="daterange"]').daterangepicker();
</script>
<!--  END PLUGIN 2:  bootstrap.daterangepicker -->
<!-- <script type="text/javascript">
$('#datepicker').dateRangePicker({
autoClose: true,
separator: '-',
format: 'DD/MM/YYYY'
}).bind('datepicker-first-date-selected', function(event, obj) {}).bind('datepicker-change', function(event, obj) {
var newdate = (obj.value);
})
</script> -->
<!--=====  End of date range select  ======-->
<!--
-->
<!--========================================================================================================================================
( 16-02-17)
table base created
/**
TODO:
( 17-02-17)
- appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
- color name comflict in '.bg-blue-key-dark, .bg-blue-key'
- looks better if .my-info-card bottom padding is removed
( 18-02-17)
- input highlighting can be improoved.
(21-2-17)
- date picker can be improoved
(22-2-17)
- added popup and removed
- daterange select input box width should be increased (solved)
- two plugins added for date range select
1. used in admin section
2. suggested by Vijay.
*/
=========================================================================================================================================-->
<!--==========================
(24-2-17)
BUG:EG-298
- colorchange is not visible to me as it is in attachment given to me
- changes made to siblings copied to this page.
(24-2-17)
BUG:EG-293
- th font-weght set to normal
===========================-->