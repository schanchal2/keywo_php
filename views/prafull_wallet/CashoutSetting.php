<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            wallwt sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <!-- row starts here -->
                    <div class="row">
                        <!-- trade analytics col starts here -->
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item active"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                        <!-- trade analytics col ends here -->
                        <!-- ask col ends here -->
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of wallwt sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20">Cashout setting</h4>
                        <div>
                            <div class="inner-2x  text-Gray">
                                <div class="row">
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10    text-Text-Primary-Blue f-sz18">Please set your Bitcoin Cashout Address</h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4 ">
                                                <p class="innerMB inner-2x">Enter <b>Bitcoin Wallet</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="asdf sdff fghh jhk2" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <button type="button" class="btn btn-primary pull-right btn-trading">Verify Now</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10  text-Text-Primary-Blue f-sz18">Please set your Paypal Cashout Address</h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Paypal Email</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="xyz@abc.com" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <button type="button" class="btn btn-primary pull-right btn-trading">Verify Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- 

                                 -->
                                <p class="innerTB inner-2x  margin-none text-Red f-sz15">Below cards are for process refference only</p>
                                <div class="row">
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18">Awaiting Verification</h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Bitcoin Wallet</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="asdf sdff fghh jhk2" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <p class="margin-bottom-none innerT"><small class="text-Red f-sz14">A verification link has been send to your registerd email</small></p>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <button type="button" class="btn btn-primary pull-right btn-trading-dark ">Cancel</button>
                                                <button type="button" class="btn btn-primary pull-right btn-trading innerMR">Resend</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18">Awaiting Verification</h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Paypal Email</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="xyz@abc.com" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <p class="margin-bottom-none innerT"><small class="text-Red f-sz14">A verification link has been send to your registerd email</small></p>
                                            </div>
                                            <div class=" innerAll clearfix inner-2x ">
                                                <button type="button" class="btn btn-primary pull-right btn-trading-dark ">Cancel</button>
                                                <button type="button" class="btn btn-primary pull-right btn-trading innerMR">Resend</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- 

                                 -->
                                <div class="row  inner-2x innerT">
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix innerB inner-2x">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> Current Bitcoin Cashout Address </h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Bitcoin Wallet</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="asdf sdff fghh jhk2" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <p class="margin-bottom-none innerT"><small class="text-App-Success f-sz14 ">Succesfully verified</small></p>
                                            </div>
                                            <div class="clearfix">
                                                <button type="button" class="btn btn-primary pull-right btn-trading inner-2x innerMR">Change</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-6 ">
                                        <div class="bg-White border-all clearfix innerB inner-2x">
                                            <div class="modal-header padding-none">
                                                <h5 class="modal-title innerAll innerMT l-h10 text-Text-Primary-Blue f-sz18"> Current Paypal Cashout Address </h5> </div>
                                            <div class="modal-body padding-bottom-none inner-2x innerAll text-grayscale-4">
                                                <p class="innerMB inner-2x">Enter <b>Paypal Email</b> Address</p>
                                                <form class="form-horizontal  half innerLR">
                                                    <div class="form-group margin-bottom-none">
                                                        <div class="col-sm-12">
                                                            <div class="">
                                                                <input placeholder="xyz@abc.com" type="email" class="form-control b-shadow-none inputAddress bg-grayscale-fb text-Light-Blue" value="" placeholder="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <p class="margin-bottom-none innerT"><small class="text-App-Success f-sz14 ">Succesfully verified</small></p>
                                            </div>
                                            <div class="clearfix">
                                                <button type="button" class="btn btn-primary pull-right btn-trading inner-2x innerMR">Change</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="innerTB inner-2x  margin-none text-Red f-sz15">Verified message will be displayed only once</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
    <script type="text/javascript">
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================  

(28-2-17)
    BUG:EG-294
        1)Cash out setting box title Text color are different as compare to design sheet(Make little bit dark)              @done
        2)Enter Bitcoin Wallet Address and Enter Paypal Email Address text font are different as compare to design sheet.   @spok to bilal, it will be bold instedn of underline
        3)Bad spacing between Cash out Setting text and Boxes.                                                              @done
           
=========================================================================================================================================-->
