<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>
    <!-- <link rel="stylesheet" href="<?php //echo $rootUrl; ?>../../frontend_libraries/jquery/jquery-ui.min.css" type="text/css" /> -->
    <!-- Bootstrap -->
    <link rel="stylesheet" href="<?php echo $rootUrl; ?>../../css/app_wallet.css" type="text/css" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <h1>Hello, world!</h1>
    <input id="IconDemo" class='Default' type="text" />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!-- <script src="<?php echo $rootUrl; ?>../../frontend_libraries/bootstrap/js/bootstrap.min.js"></script> -->
    <script src="<?php echo $rootUrl; ?>../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.Default').MonthPicker();
    });
    
    </script>
</body>

</html>
