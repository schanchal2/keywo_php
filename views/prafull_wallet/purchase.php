<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT WT8">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            wallwt sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <div class="row">
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item active"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of wallwt sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20"> Purchase</h4>
                        <div class="card bg-White border-all   padding-bottom-none innerMB f-sz15">
                            <div class="row">
                                <div class="col-xs-12 innerMT inner-2x">
                                    <form class="form-horizontal">
                                        <!-- 
                                     -->
                                        <div class="form-group  margin-bottom-none filter ">
                                            <label for="inputPassword3" class="col-sm-2 control-label text-right  text-grayscale-6">Enter Amount : </label>
                                            <div class="col-sm-4">
                                                <div class="input-group text-grayscale-4">
                                                    <input type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="" placeholder="400">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="400" type="text" class="form-control f-sz15 text-grayscale-4 placeholder-grayscale-4" value="">
                                                    <span class="input-group-addon  bg-white f-sz15 text-grayscale-6">USD</span>
                                                </div>
                                            </div>
                                            <!-- <button type="submit" class="bg-Light-Blue btn-xs btn  col-sm-2 text-White pull-right wallet-Request">Proceed</button> -->
                                            <div class="col-md-2 text-center keyword-checkout">
                                                <input value="Proceed" type="button" class="btn-pay-now f-sz18 l-h12">
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                        <!-- 


                        -->
                        <div class="current-wallet-balance innerMT innerAll inner-2x">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="current-w1">
                                        <h5 class="f-sz16 margin-top-none">Current Wallet Balance is : 300 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Your payment amount : 400 <?php echo $keywoDefaultCurrencyName; ?></h5>
                                        <h5 class="f-sz16">Please Purchase 100 <?php echo $keywoDefaultCurrencyName; ?> to settle payment now</h5>
                                    </div>
                                </div>
                                <div class="col-md-4 padding-none">
                                    <h5 class="f-sz16 text-center margin-top-none">Purchase <?php echo $keywoDefaultCurrencyName; ?> with<i class="fa fa-question-circle pull-right" aria-hidden="true"></i></h5>
                                    <div class="row leadCorrect-t">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16 ">
                                                <input type="radio" name="method" id="Bitcoin" value="Bitcoin" checked="true">
                                                <label class="" for="Bitcoin">
                                                    Bitcoin
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-right fees ">0.00083</div>
                                        <div class="col  text-right pull-right">BTC</div>
                                    </div>
                                    <div class="row">
                                        <div class="col method">
                                            <div class="radio radio-div margin-top-none padding-left-none f-sz16 ">
                                                <input type="radio" name="method" id="PayPal" value="Bitcoin" checked="">
                                                <label class="" for="PayPal">
                                                    PayPal
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col text-right fees ">1.00</div>
                                        <div class="col  text-right pull-right">USD</div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-center keyword-checkout">
                                    <input value="Proceed to Purchase" type="button" class="btn-pay-now f-sz16 l-h12 innerMT " data-toggle="modal" data-target="#proceed-to-cashout">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
    <script type="text/javascript">
    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================  
(24-2-17)
    BUG:EG-305
        - input text field changed typ from number to text.
    BUG:EG-306
        - input text field changed typ from number to text.
(27-2-17)
    




=========================================================================================================================================-->
