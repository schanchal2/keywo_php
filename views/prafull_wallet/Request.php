<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<main class="social-main-container inner-7x innerT WT3">
<div class="container padding-none row-10">
    <div class="row">
        <div class="col-xs-3">
            <div class="social-left-panel">
                <!-- my-info-card  -->
                <div class="my-info-card bg-White border-all">
                    <div class="clearfix innerAll ">
                        <div class="my-info-img pull-left innerMR">
                            <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                        </div>
                        <!-- my-picture  -->
                        <div class="my-info-detail pull-left">
                            <div class="my-info-name-container innerMB">
                                <!-- my-name  -->
                                <span class="my-info-name">Vishal Gupta</span>
                            </div>
                            <!-- my-status  -->
                            <div class="my-balance-status">
                                Balance
                                <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                            </div>
                        </div>
                    </div>
                    <!-- my-info-detail  -->
                </div>
            </div>
            <br>
            <!--====================================
            =            wallwt sidebar            =
            =====================================-->
            <div class="keymarketplace-data key-mar-data-floatingmenu">
                <!-- row starts here -->
                <div class="row">
                    <!-- trade analytics col starts here -->
                    <div class="col-md-12 inner-2x">
                        <div class="list-group list-cust border-all">
                            <a href="" class="f-sz16 list-group-item"> Wallet</a>
                            <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                            <a href="" class="f-sz16 list-group-item"> Send</a>
                            <a href="" class="f-sz16 list-group-item active"> Request</a>
                            <a href="" class="f-sz16 list-group-item"> Purchase</a>
                            <a href="" class="f-sz16 list-group-item"> Cashout</a>
                            <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                            <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                            <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                            <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                        </div>
                    </div>
                    <!-- trade analytics col ends here -->
                    <div class="col-md-12">
                        &nbsp;
                    </div>
                    <!-- ask col ends here -->
                </div>
                <!-- row ends here -->
            </div>
            <!--====  End of wallwt sidebar  ====-->
        </div>
        <!-- col-xs-3 -->
        <div class="col-xs-9">
            <div class="row">
                <div class="col-lg-12">
                    <h4 class="text-Blue margin-top-none"> Request</h4>
                    <div class="card bg-White border-all innerAll inner-2x">
                        <div class="row">
                            <div class="col-xs-9 col-xs-offset-1">
                                <form class="form-horizontal ">
                                    <!-- <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div> -->
                                    <div class="form-group filter">
                                        <label for="inputPassword3" class="col-sm-2 control-label  text-right padding-right-none text-grayscale-5 f-sz16">Form :</label>
                                        <div class="col-sm-10">
                                            <div class="input-group text-Gray">
                                                <input placeholder="Enter email or handle" type="text" class="form-control placeholder-grayscale-be text-grayscale-be f-sz14" value="" placeholder="">
                                                <span class="input-group-addon  bg-white text-grayscale-be "> <i class="fa fa-group" ></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    -->
                                    <div class="form-group filter">
                                        <label for="inputPassword3" class="col-sm-2 control-label text-right padding-right-none text-grayscale-5 f-sz16">Amount :</label>
                                        <div class="col-sm-5">
                                            <div class="input-group placeholder-grayscale-4 text-grayscale-4">
                                                <input placeholder="5228.65" type="text" class="form-control placeholder-grayscale-4 text-grayscale-4" value="" placeholder="">
                                                <span class="input-group-addon  bg-white f-sz15"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <div class="input-group placeholder-grayscale-4 text-grayscale-4">
                                                <input placeholder="5228.65" type="text" class="form-control placeholder-grayscale-4 text-grayscale-4 f-sz15" value="" placeholder="">
                                                <span class="input-group-addon  bg-white f-sz15">USD</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--
                                    -->
                                    <div class="form-group filter">
                                        <label for="inputPassword3" class="col-sm-2 control-label text-right padding-right-none text-grayscale-5 f-sz16">Note :</label>
                                        <div class="col-sm-10">
                                            <div class="input-group text-Blue btn-block">
                                                <textarea placeholder="Write an optional message" name="" id="input" class="form-control  placeholder-grayscale-be text-grayscale-be f-sz14" rows="3" required="required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn-trading-wid-auto btn-xs btn text-White pull-right wallet-Request">Request</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 filter">
                    <form class="form-inline innerMT inner-2x">
                        <!--
                        -->
                        <div class="form-group innerR ">
                            <label class="f-sz15 text-grayscale-6 ">Sort By : </label>
                            <div class="input-group ">
                                <div class="Sort-by ">
                                    <label class="dropdownOptions pull-right ">
                                        <select class="selectpicker ">
                                            <option>All</option>
                                            <option>Sent</option>
                                            <option>Sent</option>
                                            <option>Sent</option>
                                            <option>Sent</option>
                                        </select>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!--
                        -->
                        <div class="form-group ">
                            <label class="text-Gray f-sz15">Date : </label>
                            <div class="input-group text-Blue dateRange">
                                <input type="text" class="form-control text-Blue" value="10/9/2017-12/9/2017" placeholder="" id="datepicker" name="daterange">
                                <span class="input-group-addon  bg-white text-Blue"> <i class="fa fa-calendar" id=""></i> </span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="input-group ">
                                <button type="button" class="btn bg-Lightest-Blue text-White innerLR "> <i class="fa fa-download "></i> </button>
                            </div>
                        </div>
                        <div class="form-group pull-right ">
                            <div class="input-group ">
                                <input type="text" class="form-control f-sz14 placeholder-grayscale-be text-grayscale-be" placeholder="Search">
                                <span class="input-group-btn ">
                                    <button class="btn btn-default" type="button ">  <i class="fa fa-search text-grayscale-6 "></i></button>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row ">
                <div class="col-xs-12 ">
                    <div class="table-responsive wallet__trasactionHistory text-Light-Black ">
                        <table class="table table-hover border-all text-center ">
                            <thead class="text-White ">
                                <tr>
                                    <th class="text-left ">Request Type </th>
                                    <th> Quantity </th>
                                    <th>Discription </th>
                                    <th>
                                        <label class="dropdownOptions ">
                                            <select class="selectpicker">select class="selectpicker">
                                                <option title="Status">Completed</option>
                                                <option title="Status">In Progress</option>
                                                <option title="Status">etc.</option>
                                            </select>
                                        </label>
                                    </th>
                                    <th>Date </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Sent </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td class="text-grayscale-4"> You sent <?php echo $keywoDefaultCurrencyName; ?> to xyz </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail        </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Recived </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td class="text-grayscale-4"> You Recived <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">from xyz </span> </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail       </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Purchase </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">@4.22USD</span> </td>
                                    <td class="text-grayscale-4"> You Purchased <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">via xyz </span> </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail        </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Cashout </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td class="text-grayscale-4"> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">to xyz </span> </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail        </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Request Sent </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td class="text-grayscale-4"> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">to xyz </span> </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail        </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-left text-grayscale-4"> Request Recived </td>
                                    <td class="text-Dark-Blue "> 3,255.223<?php echo $keywoDefaultCurrencyName; ?> </td>
                                    <td class="text-grayscale-4"> You sent <?php echo $keywoDefaultCurrencyName; ?> <span class="btn-block ">to xyz </span> </td>
                                    <td class="text-orange "> Pending </td>
                                    <td class="text-grayscale-4"> 10th Jan, 2015 </td>
                                    <td><a href="#" data-toggle="modal" data-target="#Detail "> Detail        </a></td>
                                    <td>
                                        <div class="btn-group action-btn" role="group ">
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#AcceptRequest "> <i class="fa fa-check text-grayscale-6"> </i> </button>
                                            <button type="button" class="btn text-Light-Black innerLR padding-right-none" data-toggle="modal" data-target="#RejectRequest "> <i class="fa fa-close text-grayscale-6"> </i> </button>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!--===============================================
                        =            avoid this in INTEGRATION            =
                        ================================================-->
                        <!--====  End of avoid this in INTEGRATION  ====-->
                    </div>
                </div>
            </div>
            <div class="row keyword-marketplace-data innerT inner-2x">
                <div class="col-md-5">
                    <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
                </div>
                <div class="col-md-7">
                    <div class="pagination-cont pull-right">
                        <ul class="pagination">
                            <li ><a onclick="showNewerRequest('<?php echo $cashoutRequests['forword_cursor']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                            <li><a onclick="showOlderRequest('<?php echo $cashoutRequests['backward_cursor']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- col-xs-9 -->
    </div>
</div>
<!-- container -->
<!--===========================================
=            Accept request pop-up            =
============================================-->
<div class="modal madal--1 fade" id="AcceptRequest" tabindex="-1" role="dialog ">
    <div class="modal-dialog" role="document ">
        <div class="bg-White border-all clearfix ">
            <div class="modal-header ">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close "><span aria-hidden="true ">&times;</span></button>
            <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b">Accept Request</h4> </div>
            <div class="modal-body innerAll ">
                <p class="margin-none text-center">Are you sure you want to send <span class="text-Blue ">100 <?php echo $keywoDefaultCurrencyName; ?>  ( 100 USD ) </span>to Lorem ipsum?</p>
            </div>
            <div class="innerAll clearfix innerMB">
                <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto-dark innerMR ">No</button>
                <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto innerMR ">Yes</button>
            </div>
        </div>
    </div>
</div>
<!--====  End of Accept request pop-up  ====-->
<!--===================================
=            RejectRequest            =
====================================-->
<div class="modal  madal--1  fade" id="RejectRequest" tabindex="-1" role="dialog ">
    <div class="modal-dialog" role="document ">
        <div class="bg-White border-all clearfix ">
            <form action="" method="POST" class="" role="form ">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close "><span aria-hidden="true ">&times;</span></button>
                <h4 class="modal-title innerT text-color-Username-Link l-h10 leadCorrect-b">Reject Request</h4> </div>
                <div class="modal-body innerAll clearfix">
                    <div class="form-group margin-bottom-none ">
                        <label for="textarea" class="col-sm-3 padding-none f-wt1 f-sz15"><small>Reason for Rejection :</small></label>
                        <div class="col-sm-9 padding-none ">
                            <textarea name="" id="textarea" class="form-control" rows="3" required="required "></textarea>
                        </div>
                    </div>
                </div>
                <div class="innerAll innerMB clearfix">
                    <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto-dark innerMR ">OK</button>
                    <button type="button" class="btn btn-primary pull-right btn-trading-wid-auto innerMR ">Cancel</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--====  End of RejectRequest  ====-->
<!--============================
=            detail            =
=============================-->
<div class="modal fade" id="Detail" tabindex="-1" role="dialog ">
    <div class="modal-dialog" role="document ">
        <div class="bg-White border-all clearfix ">
            <form action="" method="POST" class="form-horizontal" role="form ">
                <div class="modal-header ">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close "><span aria-hidden="true ">&times;</span></button>
                <h4 class="modal-title text-Text-Primary-Blue ">Request</h4> </div>
                <div class="modal-body ">
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Quantity :</div>
                        <div class="col-sm-9 text-Blue margin-none h4 ">1234.25IT$</div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">From :</div>
                        <div class="col-sm-9 text-Gray ">asd@ad.com</div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Country :</div>
                        <div class="col-sm-9 text-Gray ">India</div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Description :</div>
                        <div class="col-sm-9 text-Gray ">Lorem has requested you for 1000 <?php echo $keywoDefaultCurrencyName; ?></div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Status :</div>
                        <div class="col-sm-9 text-orange ">Pendding <i class="fa fa-circle "></i> </div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Date :</div>
                        <div class="col-sm-9 text-Gray ">10th Jan, 2017 10:12 AM</div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">IP :</div>
                        <div class="col-sm-9 text-Gray ">123.123.12.12</div>
                    </div>
                    <div class="row innerMTB ">
                        <div class="col-sm-3 text-Blue ">Note :</div>
                        <div class="col-sm-9 text-Gray "> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis facere iure eaque, rerum ipsum, corporis quibusdam voluptates dicta natus ratione a? Odit! </div>
                    </div>
                </div>
                <div class=" innerAll clearfix ">
                    <button type="button" class="btn btn-primary pull-right btn-trading-dark innerMR ">Reject</button>
                    <button type="button" class="btn btn-primary pull-right btn-trading innerMR ">Accept</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--====  End of detail  ====-->
</main>
<?php include("../layout/transparent_footer.php");?>
<script type="text/javascript ">
$('.selectpicker').selectpicker({
// style: 'btn-info',
// size: 4
});
$('.filter input').on('focus', function(event) {
event.preventDefault();
$(this).parent().addClass('input-is-focused');
});
$('.filter input').on('focusout', function(event) {
event.preventDefault();
$(this).parent().removeClass('input-is-focused');
});
</script>
<!--=======================================
=            date range picker            =
========================================-->
<!-- Enable either of the plugin below and in "layout/transparent_footer.php"-->
<!--  PLUGIN 2:  bootstrap.daterangepicker -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script type="text/javascript">
$('input[name="daterange"]').daterangepicker();
</script>
<!--  END PLUGIN 2:  bootstrap.daterangepicker -->
<!-- <script type="text/javascript">
$('#datepicker').dateRangePicker({
autoClose: true,
separator: '-',
format: 'DD/MM/YYYY'
}).bind('datepicker-first-date-selected', function(event, obj) {}).bind('datepicker-change', function(event, obj) {
var newdate = (obj.value);
})
</script> -->
<!--=====  End of date range select  ======-->
<!--========================================================================================================================================
( 16-02-17)
table base created
/**
TODO:
( 17-02-17)
- appearance might change after removel of any fallback css like "app.css" , apearence needs to check again after removel of extra stylsheets(s).
- color name comflict in '.bg-blue-key-dark, .bg-blue-key'
- looks better if .my-info-card bottom padding is removed
( 18-02-17)
- input highlighting can be improoved.
*/
(28-2-17)
BUG:EG-298
Issue List:
1)Bad spacing in-between text label and text box.                                                       @done
2)Date picker missing from Request page.                                                                @done
3)Status drop down list is missing.                                                                     @done(17-03-03 16:14)
4)Request received transaction page Text font color are different as compare to design sheet.
5)Request Text are missing before Type text from request transaction list box.                          @done
=========================================================================================================================================-->