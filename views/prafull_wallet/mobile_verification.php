<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
  <div class="container">
    <div class="mobile-container card">
      <div class="text-blue border-bottom">
        <h4 class="inner-2x innerMT innerL">Mobile verification</h4>
      </div>
      <div class="mobile-detail text-center">
        <form>
          <div class="innerMT inner-2x">
            Enter your 10 digit mobile no.
          </div>
          <div class="innerMTB">
            <input type="number" class="country-code"/>
            <input type="number" value="">
          </div>
          <div class="">
            <input type="button" class="btn-social-wid-auto btn-xs" value="Submit">
          </div>
        </form>
      </div>
    </div>
  </div>
</main>
<?php include("../layout/transparent_footer.php");?>
