<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<main class="social-main-container inner-7x innerT">
  <div class="container">
    <div class="row card">
      <div class="col-xs-12 border-bottom">
        <h4 class="text-blue">Document Verification</h4>
      </div>
      <div class="col-xs-12">
        <div class="innerMT">
          User have to select the type of document and feed the document number, name as in document and attached the scanned copy of that particular document.
        </div>
        <div>
          <h6><span class="note-text">Note:-&nbsp</span><span>The attached scanned copy of document should be attested</span></h6>
        </div>
        <div>
          <div class="text-blue">
            Document required for KYC updation
          </div>
          <div class="">
            - Two photo id proofs <br>
            - Two address id proofs
          </div>
        </div>
        <div class="">
          <div class="text-blue innerMTB">
            <strong>Update your Id proof KYC information</strong>
          </div>
          <div class="id-proof-table">
            <table class="table table-bordered">
              <thead class="bg-grey text-white">
                <th>
                  Documents
                </th>
                <th>
                  Select Document Type
                </th>
                <th>
                  Upload Scanned Document
                </th>
                <th>
                  &nbsp;
                </th>
                <th>
                  Status
                </th>
              </thead>
              <tbody>
                <tr>
                  <td>
                    Document 1
                  </td>
                  <td>
                    <select class="" name="">
                      <option value="Select type">Select type</option>
                      <option value="2">2</option>
                    </select>
                  </td>
                  <td>
                    <div class="choose-file-box">
                      <input type="file" class="choose-id-proof" data-id="idProof1"/>
                      <div class="choose-file-text text-white pull-left">
                        Choose file
                      </div>
                      <div class="ellipses ellipses-wid file-name-text" id="idProof1">
                        No file choosen
                      </div>
                    </div>

                  </td>
                  <td>
                    <input type="button" class="btn-social-dark btn-xs" data-target="#documentConfirmation" data-toggle="modal" value="Submit"/>
                  </td>
                  <td>
                    <div class="text-dark-green">
                      <strong>Approved</strong>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>
                    Document 2
                  </td>
                  <td>
                    <select class="" name="">
                      <option value="Select type">Select type</option>
                      <option value="2">2</option>
                    </select>
                  </td>
                  <td>
                    <div class="choose-file-box">
                      <input type="file" class="choose-id-proof" data-id="idProof2"/>
                      <div class="choose-file-text text-white pull-left">
                        Choose file
                      </div>
                      <div class="ellipses ellipses-wid file-name-text" id="idProof2">
                        No file choosen
                      </div>
                    </div>

                  </td>
                  <td>
                    <input type="button" class="btn-social-dark btn-xs" value="Submit"/>
                  </td>
                  <td>
                    <div class="error-color">
                      <strong>Rejected</strong>
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div class="">
          <div class="text-blue innerMTB">
            <strong>Update your Address proof KYC information</strong>
          </div>
          <div class="address-proof-table">
              <table class="table table-bordered">
                <thead class="bg-grey text-white">
                  <th>
                    Documents
                  </th>
                  <th>
                    Select Document Type
                  </th>
                  <th>
                    Upload Scanned Document
                  </th>
                  <th>
                    &nbsp;
                  </th>
                  <th>
                    Status
                  </th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      Document 1
                    </td>
                    <td>
                      <select class="" name="">
                        <option value="Select type">Select type</option>
                        <option value="2">2</option>
                      </select>
                    </td>
                    <td>
                      <div class="choose-file-box">
                        <input type="file" class="choose-id-proof" data-id="idProof3"/>
                        <div class="choose-file-text text-white pull-left">
                          Choose file
                        </div>
                        <div class="ellipses ellipses-wid file-name-text" id="idProof3">
                          No file choosen
                        </div>
                      </div>

                    </td>
                    <td>
                      <input type="button" class="btn-social-dark btn-xs" value="Submit"/>
                    </td>
                    <td>
                      <div class="text-yellow">
                        <strong>In progress</strong>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      Document 2
                    </td>
                    <td>
                      <select class="" name="">
                        <option value="Select type">Select type</option>
                        <option value="2">2</option>
                      </select>
                    </td>
                    <td>
                      <div class="choose-file-box">
                        <input type="file" class="choose-id-proof" data-id="idProof4"/>
                        <div class="choose-file-text text-white pull-left">
                          Choose file
                        </div>
                        <div class="ellipses ellipses-wid file-name-text" id="idProof4">
                          No file choosen
                        </div>
                      </div>

                    </td>
                    <td>
                      <input type="button" class="btn-social-dark btn-xs" value="Submit"/>
                    </td>
                    <td>
                      <div class="text-light-blue">
                        <strong>Pending</strong>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Modal -->
  <div id="documentConfirmation" class="modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close text-color-Gray" data-dismiss="modal">&times;</button>
          <h4 class="modal-title text-blue">Document Confirmation</h4>
        </div>
        <div class="modal-body text-center">
          <div class="">
            <h4 class="text-dark-green">We have received your document and they are now being  processed</h4>
          </div>
          <div class="">
            You will receive email once your verification is completed.
          </div>
          <div class="innerMTB inner-2x">
            <input type="button" class="btn-social-wid-auto btn-xs" value="Continue">
          </div>
        </div>
      </div>

    </div>
  </div>
</main>


<?php include("../layout/transparent_footer.php");?>
<script>
  $(".choose-id-proof").change(function(){
    var id = "#"+$(this).data("id");
    console.log(id);
    $(id).html($(this).val().replace(/C:\\fakepath\\/i, ''));
  });
</script>
