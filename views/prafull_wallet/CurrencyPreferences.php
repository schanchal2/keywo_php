<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<main class="social-main-container inner-7x innerT">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            wallwt sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <!-- row starts here -->
                    <div class="row">
                        <!-- trade analytics col starts here -->
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item active"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                        <!-- trade analytics col ends here -->
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of wallwt sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none"> Currency Preferences </h4>
                        <div class="card bg-White border-all innerTB inner-2x">
                            <div class="row innerLR">
                                <div class="col-xs-12">
                                    <p class="text-grayscale-4"> Please select your display Currency (for easy reference only) </p>
                                    <form class="form-horizontal  half innerLR">
                                        <div class="form-group margin-bottom-none">
                                            <div class="col-sm-12">
                                                <div class="">
                                                    <label class="text-grayscale-3 f-sz15 f-wt4" for="exampleInputEmail1">Current Display Currency : <span class="text-Blue f-sz16">USD</span></label>
                                                    <div class="input-group">
                                                        <div class="Sort-by materialize">
                                                            <label class="dropdownOptions pull-right">
                                                                <select class="selectpicker ">
                                                                    <option disabled="" selected="">USD</option>
                                                                    <option><?php echo $keywoDefaultCurrencyName; ?></option>
                                                                </select>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="clearfix ">
                                        <button type="button" class="btn  pull-right  innerMR btn-trading-wid-auto "> &nbsp; Save Changes &nbsp; </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
</main>
<?php include("../layout/transparent_footer.php");?>
<!--========================================================================================================================================        
 BUG:EG-296
    Issue List:
    1)Need to removed cancel button from Currency Preferences box.                  @done(17-03-02 19:00) 
    2)Text font color are different as compare to design sheet.                     @done(17-03-02 19:00) 
    3)By default USD  Currency should be display inside Currency dropdown text box. @done(17-02-28 16:12) 

=========================================================================================================================================-->
