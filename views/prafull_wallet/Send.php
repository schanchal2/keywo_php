<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_wallet.css<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
<br>
<!-- Latest compiled and minified CSS -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css"> -->
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<main class="social-main-container inner-7x innerT">
    <div class="container padding-none row-10">
        <div class="row">
            <div class="col-xs-3">
                <div class="social-left-panel">
                    <!-- my-info-card  -->
                    <div class="my-info-card bg-White border-all text-Light-Black">
                        <div class="clearfix innerAll ">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=350&h=150">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="my-info-name-container innerMB">
                                    <!-- my-name  -->
                                    <span class="my-info-name">Vishal Gupta</span>
                                </div>
                                <!-- my-status  -->
                                <div class="my-balance-status">
                                    Balance
                                    <span class="my-balance-count text-Blue btn-block "> 12345.45 <?php echo $keywoDefaultCurrencyName; ?> </span>
                                </div>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                </div>
                <br>
                <!-- social-left-panel  -->
                <!--====================================
            =            wallet sidebar            =
            =====================================-->
                <div class="keymarketplace-data key-mar-data-floatingmenu">
                    <div class="row">
                        <div class="col-md-12 inner-2x">
                            <div class="list-group list-cust border-all">
                                <a href="" class="f-sz16 list-group-item"> Wallet</a>
                                <a href="" class="f-sz16 list-group-item"> Transaction History</a>
                                <a href="" class="f-sz16 list-group-item active"> Send</a>
                                <a href="" class="f-sz16 list-group-item"> Request</a>
                                <a href="" class="f-sz16 list-group-item"> Purchase</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout</a>
                                <a href="" class="f-sz16 list-group-item"> Currency Peferences</a>
                                <a href="" class="f-sz16 list-group-item"> Cashout setting</a>
                                <a href="" class="f-sz16 list-group-item"> Security Settings</a>
                                <a href="" class="f-sz16 list-group-item"> Account level   <span class="pull-right"><i class="fa fa-circle text-Blue"></i> <i class="fa fa-circle-thin text-Dark-Gray "></i> <i class="fa fa-circle-thin text-Dark-Gray"></i> </span>   </a>
                            </div>
                        </div>
                    </div>
                    <!-- row ends here -->
                </div>
                <!--====  End of wallet sidebar  ====-->
            </div>
            <!-- col-xs-3 -->
            <div class="col-xs-9">
                <div class="row">
                    <div class="col-lg-12">
                        <h4 class="text-Blue margin-top-none f-sz20">Send</h4>
                        <div class="card bg-White border-all innerAll inner-2x text-Light-Black">
                            <div class="row">
                                <div class="col-xs-9 col-xs-offset-1">
                                    <form class="form-horizontal ">
                                        <div class="form-group filter">
                                            <label for="" class="col-sm-2 control-label text-right f-sz16 padding-right-none" ">Recipient :</label>
                                        <div class="col-sm-10 ">
                                            <div class="input-group text-Gray ">
                                                <input placeholder="Enter email or handle " type="text " class="form-control text-Gray " value=" " placeholder=" ">
                                                <span class="input-group-addon bg-white text-Gray "> <i class="fa fa-group " ></i> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- 
                                     -->
                                    <div class="form-group filter ">
                                        <label for=" " class="col-sm-2 control-label text-right f-sz16 padding-right-none "">Amount :</label>
                                            <div class="col-sm-5">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="0000.00" type="text" class="form-control text-Gray" value="" placeholder="">
                                                    <span class="input-group-addon  bg-white text-Gray"> <?php echo $keywoDefaultCurrencyName; ?> </span>
                                                </div>
                                            </div>
                                            <div class="col-sm-5">
                                                <div class="input-group text-Gray">
                                                    <input placeholder="0000.00" type="text" class="form-control text-Gray" value="" placeholder="">
                                                    <span class="input-group-addon  bg-white text-Gray">USD</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- 
                                     -->
                                        <div class="form-group filter">
                                            <label for="" class="col-sm-2 control-label text-right f-sz16 padding-right-none" ">Note :</label>
                                        <div class="col-sm-10 ">
                                            <div class="input-group text-Blue btn-block ">
                                                <textarea placeholder="Write an optional message " name=" " id="input " class="form-control " rows="3 " required="required "></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-sm-offset-2 col-sm-10 ">
                                            <button type="submit " class=" btn pull-right btn-trading-wid-auto f-sz14 ">Send</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- col-xs-9 -->
        </div>
    </div>
    <!-- container -->
    <script type="text/javascript ">
    $('.selectpicker').selectpicker({
        // style: 'btn-info',
        // size: 4
    });
    $('.filter input').on('focus', function(event) {
        event.preventDefault();
        $(this).parent().addClass('input-is-focused');
    });
    $('.filter input').on('focusout', function(event) {
        event.preventDefault();
        $(this).parent().removeClass('input-is-focused');
    });
    </script>
</main>
<?php include("../layout/transparent_footer.php ");?>
<!--========================================================================================================================================        

    BUG:EG-304
        -wallet: Send: Form(Label) should replace Recipient on send form   @done Replaced (24-2-17)
    BUG:EG-303
        -drop down button is not needful on amount . @done(17-02-28 15:32) 

    made additional changes i.e made in AccountLevel.php page
    reduced space btwn lable and input as it is reduced in "views/prafull_wallet/Request.php " as bug.
            

=========================================================================================================================================-->
