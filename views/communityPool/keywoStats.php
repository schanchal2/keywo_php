<?php

session_start();

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

//include dependent files

require_once("{$docrootpath}config/config.php");
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");
require_once("{$docrootpath}models/keywords/userCartModel.php");
require_once("{$docrootpath}helpers/sessionHelper.php");
require_once("{$docrootpath}models/cdp/cdpUtilities.php");
require_once("{$docrootpath}models/wallet/walletKYCModel.php");
require_once("{$docrootpath}models/landing/landingFeatureModel.php");


error_reporting(0);

$email = $_SESSION["email"];

$conn = createDBConnection('dbkeywords');

if(noError($conn)){
    $conn = $conn["connection"];
}else{
    print_r("Database Error");
}

$dbsearch = createDBConnection('dbsearch');

if(noError($dbsearch)){
    $dbsearch = $dbsearch["connection"];
}else{
    print_r("Database Error");
}

checkForSession($conn);

//Validating User LoggedIn and LoggedOUt status

if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {



    include('../layout/header.php');

    $user_id         = $_SESSION['id'];
    $userEmail       = $_SESSION["email"];
    $requiredBalance = $userRequiredFields . ",deposit,sales,cashback,affiliate_earning,total_kwd_income,search_earning,social_content_view_earnings,search_affiliate_earnings,total_app_income,blocked_for_pending_withdrawals,blocked_for_bids,approved_withdrawals,trade_fees,purchases,renewal_fees,social_content_sharer_earnings,social_content_creator_earnings,currencyPreference,purchase_itd,kyc_current_level,mobile_number,security_preference,social_affiliate_earnings";
    $requestUrl      = $walletURL;

    $getUserDetails = getUserInfo($communityPoolUser, $requestUrl, $requiredBalance);

    if (noError($getUserDetails)) {

        $getUserDetail  = $getUserDetails['errMsg'];
        $renewalFee      = $getUserDetail['renewal_fees'];
        $renewalFees     = $renewalFee;
        $tradeFee        = $getUserDetail['trade_fees'];
        $tradeFees       = $tradeFee;
        $totalKwdIncome  = $getUserDetail['total_kwd_income'];
        $kwdSale         = $getUserDetail['purchases'];

    }
    else {
        $getUserDetails = "Error";
    }


    $getkeywoUserDetails = getUserInfo($keywoUser, $requestUrl, $requiredBalance);

    if (noError($getkeywoUserDetails)) {

        $getkeywoUserDetails = $getkeywoUserDetails['errMsg'];
        $keywo               = $getkeywoUserDetails['social_content_creator_earnings'];
        $viewerEarning       = $getkeywoUserDetails['social_content_view_earnings'];
        $sharerEarning       = $getkeywoUserDetails['social_content_sharer_earnings'];
        $contentConsumer     = $viewerEarning+$sharerEarning;

        $getkeywoUserDetails = $getkeywoUserDetails['errMsg'];
        $contentUploader     = $keywo;
    }

    $getTotalLikeCount = getTotalLikeCount("like");

    if(noError($getTotalLikeCount))
    {
        $getTotalLikeCount = $getTotalLikeCount["errMsg"];
        $totalLikeCount    = $getTotalLikeCount["total_like_count"];
    }

    $totalCollection  = $totalKwdIncome + $renewalFee + $tradeFee;

    $sumOfPayouts     = $contentConsumer+$contentUploader+$keywo+$getKeywoOwnerDetail;

    $avgPayout        = $sumOfPayouts/$totalLikeCount;

    $maintainace = "0.05";

    $maintenanceAmount    = $totalCollection*$maintainace;
    $balanceamount        = $totalCollection - ($maintenanceAmount + $sumOfPayouts);

    $getKeywordSale = getKeywordSale($conn);

        if(noError($getKeywordSale))
        {
            $getKeywordSale   = $getKeywordSale["errMsg"][0];
            $keywordSalePrice = $getKeywordSale["keyword_sale"];
        }

        $getKeywoStats = getKeywoStats($conn);

        if(noError($getKeywoStats))
        {
            $getData = $getKeywoStats["data"][0];
            $getRegisteredUser = $getData["registered_users"];
        }

        $getKycLevels = getKYCSlabByLevel($dbsearch);

        if(noError($getKycLevels))
        {
            $getKycErrMsg = $getKycLevels["errMsg"];

            $KycLevelsParam  = array();

            for($i = 0; $i < count($getKycErrMsg)-1; $i++)
            {

                $KycLevelsParam[$i]['kyc_level'] = $getKycErrMsg[$i]['kyc_level_name'];
                $KycLevelsParam[$i]['interaction'] = $getKycErrMsg[$i]['intraction'];

            }

        $sumInteraction = array();

        foreach($KycLevelsParam as $interactionValue => $subArray)
        {
            foreach ($subArray as $id=>$value)
            {
                $sumInteraction[$id]+=$value;
                $interaction = $sumInteraction["interaction"];
            }
        }

        }

        $currentPayouts  = $balanceamount/90/$getRegisteredUser/$interaction;
        $currentPayout   = number_format($currentPayouts,4);

        $getKeywordOwner = getKeywordOwner($conn);
        if(noError($getKeywordOwner))
        {
            $getkeywordOwner     = $getKeywordOwner["errMsg"][0];
            $getKeywoOwnerDetail = $getkeywordOwner["count"];
        }

    $avgLikes      = $totalLikeCount/$getRegisteredUser;
    $totalAvgCount = number_format($avgLikes,4);

    ?>
    <link rel="stylesheet" href="
    <?php echo $rootUrlCss; ?>app_statistics.css
    <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />

<main class="social-main-container inner-7x innerT">
    <div class=" container">
        <div class="text-heading text-blue text-center">Keywo Stats</div>
    </div>
    <div class="innerB inner-2x"></div>
    <div class="container row-10">
        <ul class="KS1__lists clearfix">
            <li class="KS1__list KS1__list--earnings">
                <div class="row">
                    <div class="col-xs-12 innerB">
                        <div class="KS1__list__title text-center innerAll half bg-color-Blue ">EARNINGS</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="innerLR">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Ad Revenue</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Keywords Sale</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                   origPrice="<?php echo number_format("{$kwdSale}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$kwdSale}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$kwdSale}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Commissions</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$tradeFee}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$tradeFee}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$tradeFee}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Renewal Fees</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$renewalFees}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$renewalFees}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$renewalFees}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                        </div>
                    </div>
                </div>
            </li>
            <li class="KS1__list KS1__list--pool">
                <div class="row">
                    <div class="col-xs-12 innerB">
                        <div class="KS1__list__title text-center innerAll half bg-color-Light-Blue-Hvr ">POOL</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="innerLR">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">Total Collection</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center"
                                onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                origPrice="<?php echo number_format("{$totalCollection}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$totalCollection}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$totalCollection}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                            <span class="KS1__list__ar_op text-color-Light-Blue-Hvr">Included Unclaimed Keywords Income</span>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">( - ) Server Fees</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">5%</div>

                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">( - ) Total Payouts</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$sumOfPayouts}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$sumOfPayouts}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$sumOfPayouts}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                           </div>

                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">( = ) Balance</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$balanceamount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$balanceamount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$balanceamount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                            <span class="KS1__list__ar_op text-color-Light-Blue-Hvr"></span>
                        </div>
                    </div>
                </div>
            </li>
            <li class="KS1__list KS1__list--interaction">
                <div class="row">
                    <div class="col-xs-12 innerB">
                        <div class="KS1__list__title text-center innerAll half bg-color-Blue ">INTERACTION</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="innerLR">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Total Like</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center" title="<?= isset($totalLikeCount)?($totalLikeCount):0 ?>" data-toggle="tooltip" data-placement="bottom">
                                <?= isset($totalLikeCount)?$totalLikeCount:0; ?></div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Avg Likes</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center" title="<?= isset($totalAvgCount)?($totalAvgCount):0 ?>" data-toggle="tooltip" data-placement="bottom">
                                <?= isset($totalAvgCount)?($totalAvgCount):0 ?></div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">AVG Payout</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$avgPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$avgPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$avgPayout}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Current Payout</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center"
                                onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                origPrice="<?php echo number_format("{$currentPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$currentPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$currentPayout}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="KS1__list KS1__list--payouts">
                <div class="row">
                    <div class="col-xs-12 innerB">
                        <div class="KS1__list__title text-center innerAll half bg-color-Light-Blue-Hvr ">PAYOUTS</div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="innerLR ">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Content Consumer</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$contentConsumer}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$contentConsumer}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$contentConsumer}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Content Uploader</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$contentUploader}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$contentUploader}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$contentUploader}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Keyword Owners</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$getKeywoOwnerDetail}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$getKeywoOwnerDetail}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$getKeywoOwnerDetail}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Keywo</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$keywo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$keywo}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$keywo}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                        </div>
                    </div></div>
            </li>
        </ul>
        <div class=" innerTB"></div>
        <div class="row">
            <div class="col-xs-12">
                <ul class="padding-none keywo_detail_stats">
                    <li>
                        <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                            <div class="panel-body panel-body-height ellipses"
                                onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                origPrice="<?php echo number_format("{$balanceamount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$balanceamount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$balanceamount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                            <div class="panel-footer panel-footer-height">Balance</div>
                        </div>
                    </li>
                    <li class="text-link">
                        ÷
                    </li>
                    <li>
                        <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                            <div class="panel-body panel-body-height" title="90" data-toggle="tooltip" data-placement="bottom">90</div>
                            <div class="panel-footer panel-footer-height">Days</div>
                        </div>
                    </li>
                    <li class="text-link">
                        ÷
                    </li>
                    <li>
                        <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                            <div class="panel-body panel-body-height" title="<?= isset($getRegisteredUser)?($getRegisteredUser):0 ?>" data-toggle="tooltip" data-placement="bottom">
                                <?= isset($getRegisteredUser)?($getRegisteredUser):0 ?></div>
                            <div class="panel-footer panel-footer-height">No of Users</div>
                        </div>
                    </li>
                    <li class="text-link">
                        ÷
                    </li>
                    <li>
                        <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                            <div class="panel-body panel-body-height" title="<?= isset($interaction)?($interaction):0 ?>" data-toggle="tooltip" data-placement="bottom">
                                <?= isset($interaction)?($interaction):0 ?></div>
                            <div class="panel-footer panel-footer-height">Interaction</div>
                        </div>
                    </li>
                    <li class="text-blue">
                        =
                    </li>
                    <li>
                        <div class="panel-box-dark-blue text-center bg-color-Blue">
                            <div class="panel-body panel-body-height ellipses"
                                 onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                 origPrice="<?php echo number_format("{$currentPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$currentPayout}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$currentPayout}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?>
                            </div>
                            <div class="panel-footer panel-footer-height">Current Payout</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>
<?php
}else{
    header("Location: $rootUrl./views/prelogin/index.php");
} ?>
<?php include('../layout/transparent_footer.php'); ?>
