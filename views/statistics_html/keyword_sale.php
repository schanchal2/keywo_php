<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
	<?php echo $rootUrlCss; ?>app_statistics.css
	<?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
	<?php } ?>
	<main class="social-main-container inner-6x innerTB">
		<div class=" container">
			<div class="row">
				<div class="col-xs-3">
					<h5 class="text-link f-sz15"> < Back to Keywo stats page</h5>
				</div>
				<div class="col-xs-9">
					<div class="text-center text-blue text-heading">Keywo Statistics Data</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-3">
					<div class="keymarketplace-data key-mar-data-floatingmenu inner-2x">
						<div class="row">
							<div class="col-md-12">
								<div class="list-group list-cust border-all">
									<a href="" class="f-sz16 list-group-item">Earnings</a>
									<a href="" class="f-sz16 list-group-item ">Ad Revenue</a>
									<a href="" class="f-sz16 list-group-item active">Keywords Sale</a>
									<a href="" class="f-sz16 list-group-item">Commissions</a>
									<a href="" class="f-sz16 list-group-item">Renewal Fees</a>
								</div>
							</div>
						</div>
					</div>
					<div class="keymarketplace-data key-mar-data-floatingmenu inner-2x">
						<div class="row">
							<div class="col-md-12">
								<div class="list-group list-cust border-all">
									<a href="" class="f-sz16 list-group-item">Payouts</a>
									<a href="" class="f-sz16 list-group-item ">Content Consumer</a>
									<a href="" class="f-sz16 list-group-item">Content Uploaders</a>
									<a href="" class="f-sz16 list-group-item">Keywords Owners</a>
									<a href="" class="f-sz16 list-group-item">Keywo</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-9">
					<div class="row">
						
						<div class="col-xs-11 padding-none">
							<div class="row">
								<div class="col-xs-6 ">
									<div class="tbl-top text-center">
										<div class="">Number of Advertisers:15286</div>
									</div>
								</div>
								<div class="col-xs-6 text-center">
									<div class="tbl-top">
										<span>Total Revenue:0.1341 <?php echo $keywoDefaultCurrencyName; ?></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-1 text-center">
							<div class="tbl-top">
								<i class="fa fa-calendar fa-size" aria-hidden="true"></i>
							</div>
						</div>
					</div>

					<table class="table table-striped table-responsive text-center innerMT light-sky-blue inner-2x ">
						<thead >
							<tr class="active">
								<th class="text-center">Date</th>
								<th class="text-center">Time</th>
								<th class="text-center">Transaction ID</th>
								<th class="text-center">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
							<tr>
								<td class="text-blue">8-1-2017</td>
								<td>12:30 AM</td>
								<td>ABC123</td>
								<td>0.1324<?php echo $keywoDefaultCurrencyName; ?></td>
							</tr>
						</tbody>
					</table>
					
					<div class="row">
			    		<div class="col-xs-12 ">
			    			<div class="row">
								<div class="col-xs-5 padding-left-none padding-right-none">
			    	                Note: Data will be updated in every 24 hours.
			    		        </div>
			    	            <div class="col-xs-7 padding-left-none padding-right-none">
					    			<div class="pagination-cont pull-right">
					    				<span class="statistic-paging text-right">Go to :</span>
											<input type="number" value="" id="gotovalue" placeholder="" class="statistic-paging allownumericwithoutdecimal">
									    <ul class="pagination pagination-cont" id="pagination">
									        <li class="disabled"><a href="#"> < </a></li>
									        <li class="active"><a href="#">1</a></li>
									        <li><a href="#">2</a></li>
									        <li><a href="#">3</a></li>
									        <li><a href="#">4</a></li>
									        <li><a href="#">5</a></li>
									        <li><a href="#"> > </a></li>
									    </ul>
									</div>
			    	            </div>
			               </div>
			            </div>
				    </div>
				</div>
			</div>
		</div>
   </main>