<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
    <link rel="stylesheet" href="
    <?php echo $rootUrlCss; ?>app_statistics.css
    <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <?php } ?>
    <main class="social-main-container inner-6x innerT">
        <div class=" container">
            <div class="text-heading text-blue text-center">Keywo Stats</div>
        </div>
        <div class="innerB inner-2x"></div>
        <div class="container row-10">
            <ul class="KS1__lists clearfix">
                <li class="KS1__list KS1__list--earnings">
                    <div class="row">
                        <div class="col-xs-12 innerB">
                            <div class="KS1__list__title text-center innerAll half bg-color-Blue ">EARNINGS</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="innerLR">
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Ad Revenue</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Keywords Sale</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Commissions</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Blue text-color-White">Renewal Fees</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="KS1__list KS1__list--pool">
                    <div class="row">
                        <div class="col-xs-12 innerB">
                            <div class="KS1__list__title text-center innerAll half bg-color-Light-Blue-Hvr ">POOL</div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="innerLR">
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">Total Collection</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                                <span class="KS1__list__ar_op text-color-Light-Blue-Hvr">-(minus)</span>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">Server Fees</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center"> 5% </div>
                                <span class="KS1__list__ar_op text-color-Light-Blue-Hvr">-(minus)</span>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">Total Payouts</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                                <span class="KS1__list__ar_op text-color-Light-Blue-Hvr">=(equels)</span>
                            </div>
                            <div class="KS1__list__record innerTB clearfix">
                                <div class="KS1__list__key boxshadow text-center innerTB half bg-color-Light-Blue-Hvr text-color-White">Balance</div>
                                <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                                <span class="KS1__list__ar_op text-color-Light-Blue-Hvr"></span>
                            </div>
                        </div>
                    </div>
                </li>
                <li class="KS1__list KS1__list--interaction">
                    <div class="row">
                        <div class="col-xs-12 innerB">
                            <div class="KS1__list__title text-center innerAll half bg-color-Blue ">INTERACTION</div>
                        </div>
                        <div class="clearfix"></div>
                    <div class="innerLR">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Total Like</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">40</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Total Search</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">85</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">AVG Payout</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Blue text-color-White">Current Payout</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                    </div>
                        </div>
                    </div>
                </li>
                <li class="KS1__list KS1__list--payouts">
                    <div class="row">
                        <div class="col-xs-12 innerB">
                            <div class="KS1__list__title text-center innerAll half bg-color-Light-Blue-Hvr ">PAYOUTS</div>
                        </div>
                        <div class="clearfix"></div>
                    <div class="innerLR ">
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Content Consumer</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Content Uploader</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Keyword Owners</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                        </div>
                        <div class="KS1__list__record innerTB clearfix">
                            <div class="KS1__list__key boxshadow innerTB half text-center bg-color-Light-Blue-Hvr text-color-White">Keywo</div>
                            <div class="KS1__list__value boxshadow innerTB half bg-color-White text-center">0.00 ITD</div>
                        </div>
                    </div></div>
                </li>
            </ul>
            <div class=" innerTB"></div>
            <div class="row">
                <div class="col-xs-12">
                    <ul class="padding-none keywo_detail_stats">
                        <li>
                            <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                                <div class="panel-body panel-body-height">48,522 ITD</div>
                                <div class="panel-footer panel-footer-height">Balance</div>
                            </div>
                        </li>
                        <li class="text-link">
                            ÷
                        </li>
                        <li>
                            <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                                <div class="panel-body panel-body-height">90</div>
                                <div class="panel-footer panel-footer-height">Days</div>
                            </div>
                        </li>
                        <li class="text-link">
                            ÷
                        </li>
                        <li>
                            <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                                <div class="panel-body panel-body-height">50</div>
                                <div class="panel-footer panel-footer-height">No of Users</div>
                            </div>
                        </li>
                        <li class="text-link">
                            ÷
                        </li>
                        <li>
                            <div class="panel-box text-center bg-color-Light-Blue-Hvr">
                                <div class="panel-body panel-body-height">40</div>
                                <div class="panel-footer panel-footer-height">Interaction</div>
                            </div>
                        </li>
                        <li class="text-blue">
                            =
                        </li>
                        <li>
                            <div class="panel-box-dark-blue text-center bg-color-Blue">
                                <div class="panel-body panel-body-height">48,522 ITD</div>
                                <div class="panel-footer panel-footer-height">Current Payout</div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </main>

