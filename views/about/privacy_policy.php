<div class="col-xs-9 card privacy-policy-container inner-2x innerB innerMB">
    <h3>
        Keywo - Privacy Policy
    </h3>
    <div class="list-of-terms innerMT">
        <ul class="list-group">
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    1. What Kind of information do we collect?
                </b></div>
                <p>
                    Depending on which Services you use, we collect different kinds of information from or about you.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    2. Things you do and information you provide.
                </b></div>
                <p>
                    We collect the content and other information you provide when you use our Services, including when you sign up for an account, search, send receive bitcoins, create or share, and message or communicate with others. This can include information in or about the content you provide, such as the location of a photo or the date a file was created. We also collect information about how you use our Services, such as the types of content you view or engage with or the frequency and duration of your activities.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    3. Things others do and information they provide.
                </b></div>
                <p>
                    We also collect content and information that other people provide when they use our Services, including information about you, such as when they send a payment to you or share a photo of you, send a message to you, or upload, sync or import your contact information.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    4. Your networks and connections.
                </b></div>
                <p>
                    We collect information about the people and groups you are connected to and how you interact with them, such as the people you communicate with the most or the groups you like to share with. We also collect contact information you provide if you upload, sync or import this information (such as an address book) from a device.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    5. Information about payments.
                </b></div>
                <p>
                    If you use our Services for purchases or financial transactions (like when you buy or sell keywords on Keywo or send and receive payments on Keywo wallet), we collect information about the purchase or transaction. This includes your payment information, such as your credit or debit card number and other card information, and other account and authentication information, as well as billing, shipping and contact details.

                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    6. Device information.
                </b></div>
                <p>
                    We collect information from or about the computers, phones, or other devices where you install or access our Services, depending on the permissions you&rsquo;ve granted. We may associate the information we collect from your different devices, which helps us provide consistent Services across your devices. Here are some examples of the device information we collect:&nbsp;

                    <ul class = "padding-left-none">
                        <li>Attributes such as the operating system, hardware version, device settings, file and software names and types, battery and signal strength, and device identifiers.</li>
                        <li>Device locations, including specific geographic locations, such as through GPS, Bluetooth, or WiFi signals.</li>
                        <li>Connection information such as the name of your mobile operator or ISP, browser type, language and time zone, mobile phone number and IP address.</li>
                    </ul>
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    7. Information from websites and apps that use our Services.
                </b></div>
                <p>
                    We collect information when you visit or use third-party websites and apps that use our. This includes information about the websites and apps you visit, your use of our Services on those websites and apps, as well as information the developer or publisher of the app or website provides to you or us.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    8. Information from third-party partners.
                </b></div>
                <p>
                    We receive information about you and your activities on and off Keywo from third-party partners, such as information from a partner when we jointly offer services or from an advertiser about your experiences or interactions with them.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    9. How do we use this information?
                </b></div>
                <p>
                    We are passionate about creating engaging and customized experiences for people. We use all of the information we have to help us provide and support our Services. Here&rsquo;s how:
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    10. Provide, improve and develop Services.
                </b></div>
                <p>
                    We are able to deliver our Services, personalize content, and make suggestions for you by using this information to understand how you use and interact with our Services and the people or things you&rsquo;re connected to and interested in on and off our Services.&nbsp;<br />
                    <br />
                    We also use information we have to provide shortcuts and suggestions to you. For example, we are able to suggest that your friend tag you in a picture by comparing your friend&#39;s pictures to information we&#39;ve put together from your profile pictures and the other photos in which you&#39;ve been tagged. If this feature is enabled for you, you can control whether we suggest that another user tag you in a photo using the &ldquo;Timeline and Tagging&rdquo; settings.&nbsp;<br />
                    <br />
                    When we have location information, we use it to tailor our Services for you and others, like helping you to check-in and find local events or offers in your area or tell your friends that you are nearby.&nbsp;<br />
                    <br />
                    We conduct surveys and&nbsp;research, test features in development, and analyze the information we have to evaluate and improve products and services, develop new products or features, and conduct audits and troubleshooting activities.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    11. Communicate with you.
                </b></div>
                <p>
                    We use your information to send you marketing communications, to communicate with you about our Services and let you know about our policies and terms. We also use your information to respond to you when you contact us.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    12. Show and measure ads and services.
                </b></div>
                <p>
                    We use the&nbsp;information we have&nbsp;to improve our advertising and measurement systems so we can show you relevant ads on and off our Services and measure the effectiveness and reach of ads and services.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    13. Acceptance of our terms
                </b></div>
                <p>
                    We use the information we have to help verify accounts and activity, and to promote safety and security on and off of our Services, such as by investigating suspicious activity or violations of our terms or policies. We work hard to protect your account using teams of engineers, automated systems, and advanced technology such as encryption and machine learning. We also offer easy-to-use security tools that add an extra layer of security to your account.
                </p>

                <p>
                    We use cookies and similar technologies to provide and support our Services and each of the uses outlined and described in this section of our policy.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14. How is this information shared?
                </b></div>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14 A. Sharing On Our Services
                </b></div>
                <p>
                    People use our Services to connect and share with others. We make this possible by sharing your information in the following ways:
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14 B. People you share and communicate with.
                </b></div>
                <p>
                    When you share and communicate using our Services, you choose the audience who can see what you share. For example, when you post on Keywo, you select the audience for the post, such as a customized group of individuals, all of your Friends, or members of a Group. Likewise, when you use Messenger, you also choose the people you send photos to or message.&nbsp;
                    <br />
                    <br />
                    Public information&nbsp;is any information you share with a public audience, as well as information in your&nbsp;Public Profile, or content you share on a Keywo Page or another public forum. Public information is available to anyone on or off our Services and can be seen or accessed through online search engines, APIs, and offline media, such as on TV.&nbsp;
                    <br />
                    <br />
                    <strong>
                        Apps, websites and third-party integrations on or using our Services
                    </strong>.
                </p>

                <p>
                    When you use third-party apps, websites or other services that use, or are integrated with, our Services, they may receive information about what you post or share. For example, when you make a search or share content with your Keywo friends or use the Keywo Comment or Share button on a website, the service developer or website may get information about your activities on &nbsp;the platform or receive a comment or link that you share from their website on Keywo. In addition, when you download or use such third-party services, they can access your&nbsp;Public Profile, which includes your&nbsp;username or user ID, your age range and country/language, your list of friends/followers, as well as any information that you share with them. Information collected by these apps, websites or integrated services is subject to their own terms and policies.&nbsp;
                    <br />
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14 C. Sharing within Keywo companies.
                </b></div>
                <p>
                    We share information we have about you within the family of companies that are part of Keywo
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14 D. New owner.
                </b></div>
                <p>
                    If the ownership or control of all or part of our Services or their assets changes, we may transfer your information to the new owner.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    14 E. Sharing With Third-Party Partners and Customers.
                </b></div>
                <p>
                    We work with third party companies who help us provide and improve our Services or who use advertising or related products, which makes it possible to operate our companies and provide free services to people around the world.&nbsp;
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    15. Here are the types of third parties we can share information with about you:
                </b></div>
                <p>
                    Advertising, Measurement and Analytics Services (Non-Personally Identifiable Information Only).
                </p>

                <p>
                    We want our advertising to be as relevant and interesting as the other information you find on our Services. With this in mind, we use all of the information we have about you to show you relevant ads. We do not share information that personally identifies you (personally identifiable information is information like name or email address that can by itself be used to contact you or identifies who you are) with advertising, measurement or analytics partners unless you give us permission. We may provide these partners with information about the reach and effectiveness of their advertising without providing information that personally identifies you, or if we have aggregated the information so that it does not personally identify you. For example, we may tell an advertiser how its ads performed, or how many people viewed their ads or installed an app after seeing an ad, or provide non-personally identifying demographic information (such as 25 year old female, in Madrid, who likes software engineering) to these partners to help them understand their audience or customers, but only after the advertiser has agreed to abide by our&nbsp;advertiser guidelines.
                    <br />
                    <br />
                    <strong>Vendors, service providers and other partners.</strong>
                </p>

                <p>
                    We transfer information to vendors, service providers, and other partners who globally support our business, such as providing technical infrastructure services, analyzing how our Services are used, measuring the effectiveness of ads and services, providing customer service, facilitating payments, or conducting academic research and surveys. These partners must adhere to strict confidentiality obligations in a way that is consistent with this Data Policy and the agreements we enter into with them.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    16. How can I manage or delete information about me?
                </b></div>
                <p>
                    We store data for as long as it is necessary to provide products and services to you and others, including those described above. Information associated with your account will be kept until your account is deleted, unless we no longer need the data to provide products and services.&nbsp;
                    <br />
                    <br />
                    You can delete your account any time. When you delete your account, we delete things you have posted, such as your photos and status updates. If you do not want to delete your account, but want to temporarily stop using Keywo, you may deactivate your account instead.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    17. How do we respond to legal requests or prevent harm?
                </b></div>
                <p>
                    We may access, preserve and share your information in response to a legal request (like a search warrant, court order or subpoena) if we have a good faith belief that the law requires us to do so. This may include responding to legal requests from jurisdictions outside of the Singapore where we have a good faith belief that the response is required by law in that jurisdiction, affects users in that jurisdiction, and is consistent with internationally recognized standards. We may also access, preserve and share information when we have a good faith belief it is necessary to: detect, prevent and address fraud and other illegal activity; to protect ourselves, you and others, including as part of investigations; or to prevent death or imminent bodily harm. For example, we may provide information to third-party partners about the reliability of your account to prevent fraud and abuse on and off of our Services. Information we receive about you, including financial transaction data related to purchases made with Keywo, may be accessed, processed and retained for an extended period of time when it is the subject of a legal request or obligation, governmental investigation, or investigations concerning possible violations of our terms or policies, or otherwise to prevent harm. We also may retain information from accounts disabled for violations of our terms for at least a year to prevent repeat abuse or other violations of our terms.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x"><b>
                    18. How will we notify you of changes to this policy?
                </b></div>
                <p>
                    We&rsquo;ll notify you before we make changes to this policy and give you the opportunity to review and comment on the revised policy before continuing to use our Services.
                </p>

                <h3>How to contact Keywo with questions</h3>

                <p>
                    To learn more about how privacy works on Keywo, please check out Privacy Policy. If you have questions about this policy please mail us at contact@scoinz.com
                </p>
            </li>
        </ul>
    </div>
</div>