<div class="col-xs-9 card card-social contact-container">
    <div class="row innerTB">
        <h4 class="text-center"><strong>Nice to meet you.</strong></h4>
    </div>
    <div class="row">
        <div class="col-xs-12 inner-2x innerMB">
            <div id="map">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-5">
            <div class="row">
                <div class="col-xs-12 innerMB">
                    <span class="text-blue contact-info">Contact Info</span>
                </div>
                <div class="col-xs-3 innerMB">
                    <span><strong>Address</strong></span>
                </div>
                <div class="col-xs-9 innerMB">
                    <div>
                        13-00 Far East Finance Building
                    </div>
                    <div>
                        Robinson Road, Singapore 048545
                        <br>
                        <!-- contact@searchtrade.com -->
                    </div>
                </div>
                <div class="col-xs-3 innerMB">
                    <span><strong>Email</strong></span>
                </div>
                <div class="col-xs-9 innerMB">
                    <div class="text-blue">support@keywo.com<!-- xyz@abc.in --></div>
                </div>
                <div class="col-xs-3 innerMB">
                    <div class="innerMT half">
                        <strong>Social</strong>
                    </div>
                </div>
                <div class="col-xs-9 innerMB">
                    <div class="contact-icons">
                        <i class="fa fa-facebook-square text-blue"></i>
                        <i class="fa fa-google-plus-square text-red"></i>
                        <i class="fa fa-twitter-square text-light-blue"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-7">
            <div class="innerMB">
                <span class="text-blue contact-info">Get in touch with us</span>
            </div>
            <div class="innerMB">
                <span>For any inquiries, questions or recommendation-</span><br>
                <span>Please fill out the following form</span>
            </div>
            <form id="newContactMailSend" class="clearfix contact-info-form">
                <input type="text" class="form-control" id="name" placeholder="Your name"/>
                <input type="email" class="form-control" id="email" placeholder="Your email" />
                <br>
                <textarea placeholder="Message" class="form-control innerMT" id="message" rows="6"></textarea>
                <br>
                <input type="button" class="btn-social btn-xs pull-right innerMB" id="submitFormButton" onclick="submitForm()" value="SEND EMAIL"/>
            </form>
        </div>
    </div>
</div>

<script>

    $(function() {

        var myLatLng = {lat: 1.282000, lng: 103.850791};

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatLng
        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Hello World!'
        });
        $('#datepicker').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true,
            keyboardNavigation : true ,
            daysOfWeekDisabled : [0]
        });
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    };

    function submitForm(ticketID)
    {
        var name=$("#name").val();
        var email=$("#email").val();
        var message=$("#message").val();
        
        if (name != '' && email != '' && message != '') {
            var emailValid = isValidEmailAddress(email);
            if (emailValid) {
                $.ajax({
                    type: "POST",
                    dataType: "JSON",
                    url: "../../controllers/customer/contactFormSubmit.php",
                    data: {
                        name: name,
                        email: email,
                        message:message
                    },                
                    success: function (data) {
                        console.log(data.errCode);
                        if (data.errCode == -1) {
                            $('#newContactMailSend')[0].reset();
                            showToast("success", 'We will Contact You Soon');
                        } else {
                            showToast("success", 'Please Try After Some Time');
                        }                    
                    },
                    beforeSend: function (data) {
                        $("#submitFormButton").css('pointer-events','none');
                    },
                    complete: function(data) {
                        $("#submitFormButton").css('pointer-events','auto');
                    },
                    error: function (data) {
                        showToast("failed", 'Sorry for Inconvenience, Please Try Later');
                    }
                });
            } else {
                $('#email').addClass('has-error');
                showToast("failed", 'Wrong Email Id..!');
            }
        } else {
             showToast("failed", 'All Fields Are Mandetory');
        }

    }
</script>