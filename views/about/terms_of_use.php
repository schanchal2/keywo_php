<div class="col-xs-9 card terms-of-use-container inner-2x innerB innerMB">
    <h3>
        Keywo - Terms of Use
    </h3>
    <div class="list-of-terms innerMT">
        <ul class="list-group">
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        1. Welcome to Keywo!
                    </b>
                </div>
                <p>
                    Thanks for using our products and services ("Services"). The Services are provided by Keywo.com Pte Ltd. ("Keywo"), <br /> located at 1 Scotts Road, #24-10 Shaw Centre Singapore 228208.
                </p>
                <p>
                    By using our Services, you are agreeing to these terms. Please read them carefully.
                </p>
                <p>
                    Our Services are very diverse, so sometimes additional terms or product requirements (including age requirements) may apply. Additional terms will be
                    available with the relevant Services, and those additional terms become part of your agreement with us if you use those Services.
                </p>
                <p>
                    By Clicking on "I Agree" at the end of the contract the Licensee acknowledges to be bound by all the terms as mentioned in this Agreement. This act of
                    acceptance will be construed to be a legal equivalent to your signature on a written or digital/e-contract. Only by clicking "I Agree" will you be able to
                    pay the fees to be granted a license to exploit the economic benefits derived on this platform from the specific Key Word (as described below) chosen by
                    You.
                    <br/>
                    <br/>
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        2. Using our Services
                    </b>
                </div>
                <p>
                    You must follow any policies made available to you within the Services.
                </p>
                <p>
                    Don’t misuse our Services. For example, don’t interfere with our Services or try to access them using a method other than the interface and the
                    instructions that we provide. You may use our Services only as permitted by law, including applicable export and re-export control laws and regulations. We
                    may suspend or stop providing our Services to you if you do not comply with our terms or policies or if we are investigating suspected misconduct.
                </p>
                <p>
                    Using our Services does not give you ownership of any intellectual property rights in our Services or the content you access. You may not use content from
                    our Services unless you obtain permission from its owner or are otherwise permitted by law. These terms do not grant you the right to use any branding or
                    logos used in our Services. Don’t remove, obscure, or alter any legal notices displayed in or along with our Services.
                </p>
                <p>
                    Our Services display some content that is not Keywo’s. This content is the sole responsibility of the entity that makes it available. We may review
                    content to determine whether it is illegal or violates our policies, and we may remove or refuse to display content that we reasonably believe violates our
                    policies or the law. But that does not necessarily mean that we review content, so please don’t assume that we do.
                </p>
                <p>
                    In connection with your use of the Services, we may send you service announcements, administrative messages, and other information. You may opt out of some
                    of those communications.
                </p>
                <p>
                    Some of our Services are available on mobile devices. Do not use such Services in a way that distracts you and prevents you from obeying traffic or safety
                    laws.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        3. Your Keywo Account
                    </b>
                </div>
                <p>
                    You may need a Keywo Account in order to use some of our Services. You may create your own Keywo Account, or your Keywo Account may be
                    assigned to you by an administrator.
                </p>
                <p>
                    To protect your Keywo Account, keep your password confidential. You are responsible for the activity that happens on or through your Keywo
                    Account. Try not to reuse your Keywo Account password on third-party applications. If you learn of any unauthorized use of your password or
                    Keywo Account.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        4. Privacy and Copyright Protection
                    </b>
                </div>
                <p>
                    Keywo’s privacy policies explain how we treat your personal data and protect your privacy when you use our Services. By using our Services, you agree
                    that Keywo can use such data in accordance with our privacy policies.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        5. Your Content in our Services
                    </b>
                </div>
                <p>
                    Some of our Services allow you to upload, submit, store, send or receive content. You retain ownership of any intellectual property rights that you hold in
                    that content. In short, what belongs to you stays yours.
                </p>
                <p>
                    When you upload, submit, store, send or receive content to or through our Services, you give Keywo (and those we work with) a worldwide license to
                    use, host, store, reproduce, modify, create derivative works (such as those resulting from translations, adaptations or other changes we make so that your
                    content works better with our Services), communicate, publish, publicly perform, publicly display and distribute such content. The rights you grant in this
                    license are for the limited purpose of operating, promoting, and improving our Services, and to develop new ones. This license continues even if you stop
                    using our Services. Some Services may offer you ways to access and remove content that has been provided to that Service. Also, in some of our Services,
                    there are terms or settings that narrow the scope of our use of the content submitted in those Services. Make sure you have the necessary rights to grant
                    us this license for any content that you submit to our Services.
                </p>
                <p>
                    Our automated systems analyze your content to provide you personally relevant product features, such as customized search results, tailored advertising,
                    and spam and malware detection. This analysis occurs as the content is sent, received, and when it is stored.
                </p>
                <p>
                    If you have a Keywo Account, we may display your Profile name, Profile photo, and actions you take on Keywo or on third-party applications
                    connected to your Keywo Account (such as likes, reviews you write and comments you post) in our Services, including displaying in ads and other
                    commercial contexts. We will respect the choices you make to limit sharing or visibility settings in your Keywo Account. For example, you can choose
                    your settings so your name and photo do not appear in an ad.
                </p>
                <p>
                    You can find more information about how Keywo uses and stores content in the privacy policy or additional terms for particular Services. If you
                    submit feedback or suggestions about our Services, we may use your feedback or suggestions without obligation to you.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        6. About Software in our Services
                    </b>
                </div>
                <p>
                    When a Service requires or includes downloadable software, this software may update automatically on your device once a new version or feature is
                    available. Some Services may let you adjust your automatic update settings.
                </p>
                <p>
                    Keywo gives you a personal, worldwide, royalty-free, non-assignable and non-exclusive license to use the software provided to you by Keywo as
                    part of the Services. This license is for the sole purpose of enabling you to use and enjoy the benefit of the Services as provided by Keywo, in the
                    manner permitted by these terms. You may not copy, modify, distribute, sell, or lease any part of our Services or included software, nor may you reverse
                    engineer or attempt to extract the source code of that software, unless laws prohibit those restrictions or you have our written permission.
                </p>
                <p>
                    Open source software is important to us. Some software used in our Services may be offered under an open source license that we will make available to you.
                    There may be provisions in the open source license that expressly override some of these terms.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        7. Modifying and Terminating our Services
                    </b>
                </div>
                <p>
                    We are constantly changing and improving our Services. We may add or remove functionalities or features, and we may suspend or stop a Service altogether.
                </p>
                <p>
                    You can stop using our Services at any time, although we’ll be sorry to see you go. Keywo may also stop providing Services to you, or add or create
                    new limits to our Services at any time.
                </p>
                <p>
                    We believe that you own your data and preserving your access to such data is important. If we discontinue a Service, where reasonably possible, we will
                    give you reasonable advance notice and a chance to get information out of that Service.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        8. Our Warranties and Disclaimers
                    </b>
                </div>
                <p>
                    We provide our Services using a commercially reasonable level of skill and care and we hope that you will enjoy using them. But there are certain things
                    that we don’t promise about our Services.
                </p>
                <p>
                    Other than as expressly set out in these terms or additional terms, neither Keywo nor its suppliers or distributors make any specific promises about
                    the Services. For example, we don’t make any commitments about the content within the Services, the specific functions of the Services, or their
                    reliability, availability, or ability to meet your needs. We provide the Services “as is”.
                </p>
                <p>
                    Some jurisdictions provide for certain warranties, like the implied warranty of merchantability, fitness for a particular purpose and non-infringement. To
                    the extent permitted by law, we exclude all warranties.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        9. Liability for our Services
                    </b>
                </div>
                <p>
                    When permitted by law, Keywo, and Keywo’s suppliers and distributors, will not be responsible for lost profits, revenues, or data, financial
                    losses or indirect, special, consequential, exemplary, or punitive damages.
                </p>
                <p>
                    To the extent permitted by law, the total liability of Keywo, and its suppliers and distributors, for any claims under these terms, including for any
                    implied warranties, is limited to the amount you paid us to use the Services (or, if we choose, to supplying you the Services again).
                </p>
                <p>
                    In all cases, Keywo, and its suppliers and distributors, will not be liable for any loss or damage that is not reasonably foreseeable.
                </p>
                <p>
                    We recognize that in some countries, you might have legal rights as a consumer. If you are using the Services for a personal purpose, then nothing in these
                    terms or any additional terms limits any consumer legal rights which may not be waived by contract.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        10. Business uses of our Services
                    </b>
                </div>
                <p>
                    If you are using our Services on behalf of a business, that business accepts these terms. It will hold harmless and indemnify Keywo and its
                    affiliates, officers, agents, and employees from any claim, suit or action arising from or related to the use of the Services or violation of these terms,
                    including any liability or expense arising from claims, losses, damages, suits, judgments, litigation costs and attorneys’ fees.
                </p>
                <p>
                    User Conduct
                </p>
                <p>
                    You agree that you will not engage in any activity that interferes with or disrupts the Keywo Sites or the Services (or the servers and networks
                    which are connected to the Services) or use any service to manipulate your CPU to gain distinct advantage on any of our programs. Unless you have been
                    specifically permitted to do so in a separate agreement with us, you agree that you will not reproduce, duplicate, copy, sell, trade or resell the Services
                    for any purpose. Please note that at any time, we may, in our sole discretion, terminate our legal agreement with you and deny you use of our Services if:
                </p>
                <p>
                    (A) You have breached any provision of these terms (or have acted in manner which clearly shows that you do not intend to, or are unable to comply with the
                    provisions of these terms); or
                </p>
                <p>
                    (B) We are required to do so by law (for example, where the provision of our services to you is, or becomes, unlawful); or
                </p>
                <p>
                    (C) The partner with whom we offered the Services to you has terminated its relationship with us or ceased to offer their services to you; or
                </p>
                <p>
                    (D) We are transitioning to no longer providing the Services to users in the country in which you are resident or from which you use the Services; or
                </p>
                <p>
                    (E) The provision of the Services to you is, in our opinion, no longer commercially viable.
                </p>
                <p>
                    You further agree that your use of the Services shall not be fraudulent (determined in our reasonable discretion) and must be lawful at all times. You
                    shall also comply with all usage rules found throughout the Keywo Sites and/or the Services, including, without limitation, the Do's and Don'ts
                    guidelines posted on the footer of this page and hyperlinked at: https://www.scoinz.com/doesanddont.php. In furtherance of the foregoing, and as an
                    example and not as a limitation, you agree not to use the Service in order to:
                </p>
                <ul type="disc" class = "padding-left-none">
                    <li>
                        create and/or use multiple Accounts
                    </li>
                    <li>
                        post, upload, transmit or otherwise disseminate information that is obscene, indecent, vulgar, pornographic, sexual, hateful or otherwise objectionable
                    </li>
                    <li>
                        post spam links, and/or personal referral links in an aggressive, wanton, or otherwise inappropriate fashion both on Keywo or on any other public
                        site on the web
                    </li>
                    <li>
                        defame, libel, ridicule, mock, stalk, threaten, harass, intimidate or abuse anyone, hatefully, racially, ethnically or, in a reasonable person's view,
                        otherwise offensive or objectionable
                    </li>
                    <li>
                        upload or transmit (or attempt to upload or transmit) files that contain viruses, Trojan horses, worms, time bombs, cancelbots, corrupted files or
                        data, or any other similar software or programs that may damage the operation of the Service, other users' computers, or the access to or functionality
                        of the Keywo Sites
                    </li>
                    <li>
                        violate the contractual, personal, intellectual property or other rights of any party including using, uploading, transmitting, distributing, or
                        otherwise making available any information made available through the Service in any manner that infringes any copyright, trademark, patent, trade
                        secret, or other right of any party (including rights of privacy or publicity)
                    </li>
                    <li>
                        attempt to obtain passwords or other private information from other members
                    </li>
                    <li>
                        improperly use support channels or complaint buttons to make false reports to the Company
                    </li>
                    <li>
                        develop, distribute, or publicly inform other members of "auto" software programs, "macro" software programs or other "cheat utility" software program
                        or applications in violation of the applicable License Agreements
                    </li>
                    <li>
                        exploit, distribute or publicly inform other members of any error, miscue or bug that gives an unintended advantage violate any applicable laws or
                        regulations, or promote or encourage any illegal activity including, but not limited to, hacking, cracking or distribution of counterfeit software, or
                        cheats or hacks for the Services.
                    </li>
                </ul>
                <p>
                    Accounts that have not been logged into for six months or more are deemed inactive and bitcoin earned in these accounts maybe null and void. Keywo
                    accounts are not transferable upon death or as part of a domestic relations matter or otherwise by operation of law.
                </p>
                <p>
                    If you violate the Terms, the Company reserves the right in its sole discretion to issue you a warning regarding the violation or immediately terminate or
                    suspend any or all accounts you have created using the Services. You agree that the Company need not provide you notice before terminating or suspending
                    your account(s), but it may do so at any time. Any account or accounts that are terminated shall be deemed null and void and any and all information
                    relating to such account(s) shall revert to or become the sole property of the Company, including but not limited to Keywo Rewards like Bitcoin
                    regardless of monetary value, except to the extent prohibited by applicable law. The Company reserves the right to refuse access to the Services without
                    notice for any reason, including, but not limited to, a violation of the Terms. You agree that Keywo may discontinue the Service or change the
                    content of the Service at any time, for any reason, with or without notice to you, without liability.
                    <br/>
                    <br/>
                </p>                
                <p>
                    <strong>Search Rewards Terms</strong>
                </p>
                
                <p>
                    1. To get paid user must create an account
                </p>
                <p>
                    2. Bitcoin payout per day is limited to 40 searches only
                </p>
                <p>
                    3. Payout amount will alter based on pool reserves and algorithm
                </p>
                <p>
                    4. Company can change number of searches paid for per day at will
                </p>
                <p>
                    5. No payouts will be made if pool is empty
                </p>
                <p>
                    6. Payout algorithm can be altered at anytime
                </p>
                <p>
                    7. 25% revenue share from pool can be altered at any time with prior notice
                </p>
                <p>
                    8. Company is not responsible if payout fails upon search because of technological or computer generated errors are beyond human control
                </p>
                <p>
                    9. Company can block payouts or accounts if it suspects foul play on part of users (Like auto bot detection or excessive call from particular device or IP
                    Address)
                </p>
                <p>
                    10. All earnings will be deposited in Keywo Wallet
                </p>
                <p>
                    11. Search Earning history data access is a privilege
                </p>
                <h4>
                </h4>
                <h4>
                    English Language
                </h4>
                <p>
                    The parties hereto confirm that it is their wish that these Terms and Conditions, as well as all other documents relating hereto have been and shall be
                    drawn up in the English language only.
                </p>
            </li>
            <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    <b>
                        11. About these Terms
                    </b>
                </div>
                <p>
                    We may modify these terms or any additional terms that apply to a Service to, for example, reflect changes to the law or changes to our Services. You
                    should look at the terms regularly. We’ll post notice of modifications to these terms on this page. We’ll post notice of modified additional terms in the
                    applicable Service. Changes will not apply retroactively and will become effective no sooner than seven days after they are posted. However, changes
                    addressing new functions for a Service or changes made for legal reasons will be effective immediately. If you do not agree to the modified terms for a
                    Service, you should discontinue your use of that Service.
                </p>
                <p>
                    If there is a conflict between these terms and the additional terms, the additional terms will control for that conflict.
                </p>
                <p>
                    These terms control the relationship between Keywo and you. They do not create any third party beneficiary rights.
                </p>
                <p>
                    If you do not comply with these terms, and we don’t take action right away, this doesn’t mean that we are giving up any rights that we may have (such as
                    taking action in the future).
                </p>
                <p>
                    If it turns out that a particular term is not enforceable, this will not affect any other terms.
                </p>
                <p>
                    The courts in some countries will not apply Singapore law to some types of disputes. If you reside in one of those countries, then where Singapore law is
                    excluded from applying, your country’s laws will apply to such disputes related to these terms. Otherwise, you agree that the laws Singapore, excluding
                    Singapore choice of law rules, will apply to any disputes arising out of or relating to these terms or the Services. Similarly, if the courts in your
                    country will not permit you to consent to the jurisdiction and venue of the courts in Singapore, then your local jurisdiction and venue will apply to such
                    disputes related to these terms. Otherwise, all claims arising out of or relating to these terms or the services will be litigated exclusively in the
                    Singapore, and you and Keywo consent to personal jurisdiction in those courts.
                </p>                
            </li>            
        </ul>
    </div>
</div>