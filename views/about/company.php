<?php
require_once "../../config/config.php";

?>

<div>
    <center>
        <h3>
            <div>
                "We are updating our site. This page should be live soon."
            </div>
            <div>               
                "Click <a href="<?php echo $rootUrl; ?>">here</a> to go back to your social page"
            </div>
        </h3>
    </center>
</div>


<!--<div class="col-xs-9 card inner-2x innerB">
    <h3 class="text-blue text-center">Our Story</h3>
    <div class="about-us-image">
        <div class="objective">
            <p>
                " An idea in a moment has sparked the global movement. And we are just getting started. "
            </p>
            <div class="company-info text-right">
                <strong>Lorem ipsum</strong>, co-founder, CEO
            </div>
        </div>
    </div>
    <div class="innerMTB inner-2x">
        <h4 class="text-center text-blue">
            Everybody has something to say !!!
        </h4>
        <p class="text-center">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu congue dui, vitae mattis erat.
            Praesent ut porttitor nulla, ac condimentum risus. Sed ultricies venenatis nunc sed dapibus.
            Nulla consectetur nit amet, consectetur adipiscing elit. Mauris eu congue dui, vitae mattis erat.
            Praesent ut porttitor nulla, ac c
        </p>
    </div>
    <div class="col-xs-12 padding-none text-center">
        <div class="col-xs-4">
            <div class="text-heading sub-heading">
                - Our Mission -
            </div>
            <div class="innerMT">
                " Lorem ipsum dolor sit amet consectetur."
            </div>
        </div>
        <div class="col-xs-4">
            <div class="text-heading sub-heading">
                - Our Vision -
            </div>
            <div class="innerMT">
                " Mauris eu congue dui, vitae mattis erat.
                Praesent ut porttitor"
            </div>
        </div>
        <div class="col-xs-4">
            <div class="text-heading sub-heading">
                - Live Values -
            </div>
            <div class="innerMT">
                " Mauris eu congue dui, vitae mattis erat. "
            </div>
        </div>
    </div>
</div>