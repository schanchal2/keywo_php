<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
    <div class="row contact-container">
      <div class="col-xs-3">
    		<div class="about-us-left-panel">
    		  <div class="card left-panel-modules">
    				<div class="bg-light-gray left-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB">About us</h4>
  							</div>
  						</div>
    				</div>
    				<div class="about-us-left-panel">
    					<div class="margin-none">
    						<div class="about-us-menu border-bottom about_us_menu" id="company_window">
    							<a>Company</a>
    						</div>
    						<div class="about-us-menu border-bottom about_us_menu" id="annaouncement_window">
    							<a>Announcements</a>
    						</div>
    						<div class="about-us-menu border-bottom about_us_menu" id="blog_window">
    							<a>Blog</a>
    						</div>
    						<div class="about-us-menu border-bottom about_us_menu" id="policy_window">
    							<a>Privacy Policy</a>
    						</div>
                <div class="about-us-menu border-bottom about_us_menu" id="terms_of_use_window">
    							<a>Terms of Use</a>
    						</div>
                <div class="about-us-menu border-bottom about_us_menu" id="disclaimer_window">
    							<a>Disclaimer</a>
    						</div>
                <div class="about-us-menu border-bottom about_us_menu " id="press_window">
    							<a>Press</a>
    						</div>
                <div class="about-us-menu border-bottom about_us_menu active" id="contact_window">
    							<a>contact</a>
    						</div>
                        </div>
    				</div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
    	</div>
    	<!-- col-xs-3 -->

  <div id="page_to_load">

  </div>
    </div>
      <!-- col-xs-9 -->


    </div>
  </div>
<!-- container -->
</main>

<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php

include('../layout/social_footer.php');


?>

<script>
    $( document ).ready(function() {
        $("#page_to_load").load("contact.php");
    });

    $("#contact_window").click(function(){
        $("#page_to_load").load("contact.php");
        $('.about_us_menu').removeClass('active');
        $('#contact_window').addClass('active');
    });

    $("#press_window").click(function(){
        $("#page_to_load").load("press.php");

        $('.about_us_menu').removeClass('active');
        $('#press_window').addClass('active');
    });

    $("#disclaimer_window").click(function(){
        $("#page_to_load").load("desclaimer.php");
        $('.about_us_menu').removeClass('active');
        $('#disclaimer_window').addClass('active');
    });

    $("#terms_of_use_window").click(function(){
        $("#page_to_load").load("terms_of_use.php");
        $('.about_us_menu').removeClass('active');
        $('#terms_of_use_window').addClass('active');
    });

    $("#policy_window").click(function(){
        $("#page_to_load").load("privacy_policy.php");
        $('.about_us_menu').removeClass('active');
        $('#policy_window').addClass('active');
    });

    $("#blog_window").click(function(){
        $("#page_to_load").load("blog.php");
        $('.about_us_menu').removeClass('active');
        $('#blog_window').addClass('active');
    });

    $("#annaouncement_window").click(function(){
        $("#page_to_load").load("announcement.php");
        $('.about_us_menu').removeClass('active');
        $('#annaouncement_window').addClass('active');
    });

    $("#company_window").click(function(){
        $("#page_to_load").load("company.php");
        $('.about_us_menu').removeClass('active');
        $('#company_window').addClass('active');
    });
</script>
