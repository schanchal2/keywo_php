<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_Referral_Zone.css" type="text/css" />
<main class="inner-7x innerT RZ3">
    <div class="container  row-10 referral text-color-grayscale-6 padding-none">
        <div class="row innerMT f-sz15 ">
            <div class="col Email_panel innerT">
                <div class="panel panel-default border-none ">
                    <div class="panel-heading f-sz18 text-color-Text-Primary-Blue bg-color-White innerAll padding-bottom-none"><div class="innerT">Email Template</div></div>
                    <div id="copyTarget" class="panel-body ">
                        <p class="margin-none">Hey, I just signed up with SearchTrade, it is a new search engine platform that gives back advertising income to its users.
                            <!--  -->
                            SearchTrade is pre-selling kewords which will entitle owners to earn income every time the keyword they ow will be used as
                            <!--  -->
                            part of search query on the SearchTrade platform.</p><p> Investing in keyword is an experience similar to buying a domain name.
                            <!--  -->
                            Diference being, unlike domain, keywords generate recurring income for its owner. Click the link below to learn and keyword investment today.</p>
                        <a href="#" class="text-color-Deep-Sky-Blue ">https://www.searchtrade.com </a>
                        <p class="text-color-Text-Primary-Blue f-sz16 margin-none innerT">REGISTERED OFFICE</p>
                        <p>1 Scotts Road, #24-10
                            <br> Shaw Centre, Singapore 228208
                            <br> contact@searchtrade.com
                        </p>
                    </div>
                    <div class="pull-right innerB">
                        <button type="button" id="copyButton" class="btn text-white bg-color-Lightest-Blue innerMR">Copy content</button>
                        <button type="button" class="btn text-white bg-color-Dark-Blue innerMR" onclick="history.back();">Cancel</button>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
<script>

    document.getElementById("copyButton").addEventListener("click", function() {
        copyToClipboardMsg(document.getElementById("copyTarget"), "msg");
    });

    function copyToClipboardMsg(elem, msgElem) {
        var succeed = copyReferralCode(elem);
        var msg;
        if (!succeed) {
            msg = "Copy not supported or blocked. Press ctrl+c to copy."
        } else {
            msg = "Text copied to the clipboard."
        }
        if (typeof msgElem === "string") {
            msgElem = document.getElementById(msgElem);
        }
        msgElem.innerHTML = showToast("success", msg);
        setTimeout(function() {
            msgElem.innerHTML = "";
        }, 2000);
    }

    function copyReferralCode(elem) {
        // create hidden text element, if it doesn't already exist
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            // can just use the original source element for the selection and copy
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            // must use a temporary form element for the selection and copy
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        // select the content
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch(e) {
            succeed = false;
        }
        // restore original focus
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }

        if (isInput) {
            // restore prior selection
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            // clear temporary content
            target.textContent = "";
        }
        return succeed;
    }

</script>
