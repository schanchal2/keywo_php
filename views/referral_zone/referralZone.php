
<?php

session_start();

header('Content-Type: text/html; charset=utf-8');

$docrootpath = __DIR__;
$docrootpath = explode('/views', $docrootpath);
$docrootpath = $docrootpath[0] . "/";

require_once("{$docrootpath}config/config.php"); //echo "Testing";
require_once("{$docrootpath}config/db_config.php");
require_once("{$docrootpath}/helpers/errorMap.php");
require_once("{$docrootpath}helpers/coreFunctions.php");
require_once("{$docrootpath}helpers/stringHelper.php");
require_once("{$docrootpath}helpers/arrayHelper.php");
require_once("{$docrootpath}helpers/sessionHelper.php");
require_once("{$docrootpath}models/keywords/keywordSearchModel.php");

error_reporting(0);

$email = $_SESSION["email"];

$conn = createDBConnection('dbkeywords');
if(noError($conn)) {
    $conn = $conn['errMsg'];
} else {
    printArr("Database Error");
}

if (isset($_SESSION["email"]) && !empty($_SESSION["email"]))
{
    include("../layout/header.php");
    $user_id        = $_SESSION['id'];
    $require        = $userRequiredFields . ",user_id,country,my_referral_id,ref_email,affiliate_earning,renewal_fees";
    $requestUrl     = $walletURL;
    $getUserDetails = getUserInfo($email, $requestUrl, $require);

    if(noError($getUserDetails))
    {
        $getUserDetails      = $getUserDetails["errMsg"];
        $referralCode        = $getUserDetails["my_referral_id"];
        $refEmail            = $getUserDetails["ref_email"];
        $myAffiliateEarning  = $getUserDetails["affiliate_earning"];
    }

$sharingLinks = $rootUrl."index.php?ref=".$referralCode."#signUpDialog";
$sharingLinks = urlencode($sharingLinks);
$fbLink = $rootUrl."index.php?ref=".$referralCode."#signUpDialog";
$fbLink = urlencode($fbLink);
$fbCaption = "Re-Engineering Internet Search Bisuness";
$fbCaption = urlencode($fbCaption);

$id              = $_SESSION["id"];
$type            = 'affiliate_earnings';
$currenttime     = date("d-m-Y H:i:s");
$keywoLaunchDate = '1495705269000';
$date_jumpers_lt = strtotime($currenttime) * 1000;
$date_jumpers_gt = $keywoLaunchDate ;

$getCountReferredUser  = getCountReferredUser($referralCode);
if(noError($getCountReferredUser))
{
    $getCountReferredUser = $getCountReferredUser["errMsg"];
    $getRefferedCount     = $getCountReferredUser["count"];
}
$getTransactionDetail  = getTransactionInfoByEmail($date_jumpers_lt,$date_jumpers_gt,$type,$id);

if(noError($getTransactionDetail))
{
    $getTransactionDetail = $getTransactionDetail["errMsg"]["batched_container"];

    foreach($getTransactionDetail as $getTransactionDetailType)
    {
        if(in_array("social_referral_earnings",$getTransactionDetailType))
        {
            $getReferredUserData[] = $getTransactionDetailType;
            foreach ($getReferredUserData as $transaction) {
                $metaDetails = $transaction["meta_details"]; //printArr($transaction);
                $amount = $transaction["amount"];
                $amount1[] = $amount;
                $transactionAmount = array_sum($amount1);
                //printArr($transactionAmount);
            }
        }

        if(empty($transactionAmount))
        {
            $transactionAmount = 0;
        }

        $getTransactionDetailType = $getTransactionDetailType["type"];
    }

}

?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_Referral_Zone.css" type="text/css" />
<!--

 -->
<main class="inner-7x innerT RZ1">
    <!-- Modal start -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog inner-9x innerMT" role="document">
            <div class="">
                <div class="row">
                    <div class="col banner_panel ">
                        <div class="panel panel-default border-none clearfix">
                            <div class="panel-heading f-sz18 text-color-Text-Primary-Blue bg-color-White innerAll padding-bottom-none">
                                <div class="innerT">BANNER ADS</div>
                            </div>
                            <div class="panel-body innerAll row-10">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=728&h=90">
                                            </div>
                                        </div>
                                        <div class="row innerT">
                                            <div class="col-xs-6"> <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=390&h=261"> </div>
                                            <div class="col-xs-6"> <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=390&h=261"> </div>
                                        </div>
                                        <div class="row innerT">
                                            <div class="col-xs-6"> <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=373&h=369"> </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3"> <img class="img-responsive" src="https://placeholdit.imgix.net/~text?w=160&h=600"></div>
                                </div>
                            </div>
                            <span class="pull-right innerB">
                        <button type="button" class="btn bg-color-App-Primary text-color-White innerMR" data-dismiss="modal">Close</button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--modal end-->
    <div class="container  row-10 referral text-color-grayscale-6 padding-none">
        <div class="row">
            <div class="col-xs-12">
                <h6 class="referral-heading text-color-Text-Primary-Blue f-sz20  text-center">Keywo Referral Program</h6>
            </div>
        </div>
        <div class="row">
            <div class="referral-heading_info col">
                <p class="text-center f-sz16"> Share this referral code or URL with your friends and earn 7.5% commission on every keyword they purchase , Earn 5% share of their search payout and share your friends posts and earn 50% payout on every post view. </p>
            </div>
        </div>
        <div class="row innerLR">
            <div class="col-xs-6 referral-url">
                <div class="row">
                    <form class="form-horizontal innerT clearfix">
                        <div class="margin-bottom-none filter ">
                            <label for="inputPassword3" class="col pull-left control-label  text-grayscale-6 f-sz15 f-wt1"> <span class="text-left">My referral url : </span></label>
                            <div class="col-sm-9">
                                <div class="input-group referral-url_userInput text-Gray ">
                                     <input placeholder="" id="copyTarget1" type="text" class="form-control text-Gray text-grayscale-4 placeholder-grayscale-4  bg-color-grayscale-fb f-sz15" value="<?php echo $rootUrl."index.php?ref=".$referralCode."#signUpDialog"; ?>" readonly/>
                                    <span class="input-group-addon bg-color-grayscale-fb  bg-white text-grayscale-6 f-sz15" id="copyButton1"><a href="javascript:;" title="copy Code" ><i class="fa fa-copy"></i></a></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="form-horizontal innerT inner-2x">
                        <div class="col">
                            <label class="control-label f-wt1"> Refer via social :</label>
                        </div>
                        <div class="col padding-left-none">
                            <a target="_blank" href="https://www.facebook.com/dialog/feed?app_id=145634995501895&description=I joined Keywo, they pay bitcoins for every search I make. You can earn bitcoins too. Join Today!&display=popup&caption=Re-Engineering%20Internet%20Search%20Bisuness&link=<?php echo $fbLink; ?>&name=Keywo.com&picture=<?php echo $rootUrlImages; ?>referNearn.png&redirect_uri=https://facebook.com">
                                <div class="input-group referral-url_facebook">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-facebook "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div> </a>
                        </div>
                        <div class="col padding-left-none">
                            <a target="_blank" href="https://twitter.com/intent/tweet?url=<?php echo $sharingLinks; ?>&text=I Joined Keywo, they pay bitcoins for every search I make. Join Today!">
                            <div class="input-group referral-url_twitter">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-twitter" ></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div></a>
                        </div>
                        <div class="col padding-left-none">
                            <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php echo $sharingLinks; ?>&title=Sign up on Keywo.com and start earning today!&source=Keywo.com" >
                            <div class="input-group referral-url_linkedin">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-linkedin "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 referral-code">
                <div class="row">
                    <form class="form-horizontal innerT pull-right clearfix">
                        <div class="margin-bottom-none filter ">
                            <label for="inputPassword3" class="col pull-left control-label  text-grayscale-6 f-sz15 f-wt1"> <span class="text-left">My referral code : </span></label>
                            <div class="col">
                                <div class="input-group referral-code_userInput text-Gray">
                                    <input placeholder="" id="copyTarget2" type="text" class="form-control bg-color-grayscale-fb  text-Gray text-grayscale-4 placeholder-grayscale-4 f-sz15" value="<?php echo $referralCode; ?>" readonly/>
                                    <span class="input-group-addon bg-color-grayscale-fb bg-white text-grayscale-6 f-sz15" id="copyButton2"> <a href="javascript:;" title="copy Code" ><i class="fa fa-copy"></i></a></span>
                                </div>

                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="form-horizontal innerT inner-2x pull-right">
                        <div class="col">
                            <label class="control-label f-wt1"> Refer via mail :</label>
                        </div>
                        <div class="col padding-left-none">
                            <a class="g-plus" data-action="share" data-annotation="none"  href="<?php echo $rootUrl; ?>views/referral_zone/referralEmail.php">
                            <div class="input-group referral-url_google">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-google-plus "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div></a>
                        </div>
                        <div class="col padding-left-none">
                            <a data-action="share" data-annotation="none"  href="<?php echo $rootUrl; ?>views/referral_zone/referralEmail.php">
                            <div class="input-group referral-url_envelope">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-envelope" ></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div></a>
                        </div>
                        <div class="col padding-left-none">
                            <a data-action="share" data-annotation="none"  href="<?php echo $rootUrl; ?>views/referral_zone/referralEmail.php">
                            <div class="input-group referral-url_yahoo">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-yahoo"></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="">
        <div class="row innerLR">
            <div class="col">

                <div class="f-sz15">Referred users : <span class="text-color-Text-Primary-Blue"><?= isset($getRefferedCount)?$getRefferedCount:0; ?></span> | Referral Earnings :
                    <span class="text-color-Text-Primary-Blue">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                origPrice="<?php echo number_format("{$transactionAmount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$transactionAmount}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$transactionAmount}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                </div>

            <div class="col pull-right">
                <button type="button" class="btn bg-color-Lightest-Blue text-color-White f-sz15 innerMR inner-2x">Video Ads</button>
                <button type="button" class="btn bg-color-Lightest-Blue text-color-White f-sz15" data-toggle="modal" data-target="#myModal">Banner Ads</button>
            </div>
        </div>

        <div class="row innerMT inner-2x f-sz15">

            <div class="col-xs-6 keyword_section">
                <ul class="list-unstyled innerLR">
                    <li>Keyword sold :<span class="text-color-Text-Primary-Blue pull-right"><?= isset($keywordCount)?$keywordCount:0; ?></span></li>

                    <li>Commission :
                        <span class="text-color-Text-Primary-Blue pull-right">&nbsp;<a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                            origPrice="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$commission}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </span>

                    </li>

                </ul>
                <div class="table-responsive border-all inner-2x">
                    <table class="table bg-color-White margin-bottom-none">
                        <thead>
                        <tr class="bg-color-header_1 text-color-White">
                            <th colspan="3" class="text-center f-sz18 f-wt1 l-h13"> Keyword</th>
                        </tr>
                        <tr class="bg-color-header_2">
                            <th class="f-wt1"> Keyword </th>
                            <th class="text-center f-wt1"> Sale Price</th>
                            <th class="text-right  f-wt1"> Commission </th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        if($getTransactionDetailType != $type)
                        {
                            ?>
                            <td colspan="3" class="text-center f-sz18 f-wt1 l-h13">No Transaction</td>
                            <?php
                        }
                            $i  = 0;

                            foreach($getTransactionDetail as $getTransactionDetailType)
                            {
//                                printArr($getTransactionDetailType);
                                if(in_array("affiliate_earnings",$getTransactionDetailType))
                                {
                                    $getReferredUserData[] = $getTransactionDetailType;
                                    foreach ($getReferredUserData as $transaction) {
                                        $metaDetails = $transaction["meta_details"];
                                        $keywords    = $metaDetails["keyword"];
                                        $kwdprice    = $metaDetails["gross_kwd_price"];
                                        $commission  = $metaDetails["commision"];

                                $i++;
                        if ($i < 6) {
                            ?>
                            <tr>
                                <td><?php echo "#{$keywords}"; ?></td>
                                <td class="text-center"><?php echo number_format($kwdprice,2); ?></td>
                                <td class="text-right">&nbsp;
                                    <?php if(isset($commission) && !empty($commission)){ ?>
                                    <a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                       origPrice="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("{$commission}", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("{$commission}", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a> </td>
                            <?php } else { ?>
                                <td class="text-right"><a href="#" onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');"
                                                                                                                         origPrice="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" title="<?php echo number_format("0", 4,'.',''); echo " {$keywoDefaultCurrencyName}"; ?>" data-toggle="tooltip" data-placement="bottom"><?php echo formatNumberToSort("0", 4); ?> <?php echo $keywoDefaultCurrencyName; ?></a></td>
                                <?php } ?>
                                </tr>
                        <?php }}}} ?>
                        </tbody>
                    </table>
                    <footer class="bg-color-White innerAll clearfix ">
                        <?php if(isset($keywords) && !empty($keywords))
                        {
                            $count = count($keywords);
                            if($count <= 5)
                            { } else {
                            ?>
                            <a href="#" class="pull-right innerMB btn-block text-right l-h7 text-color-Deep-Sky-Blue" > View more</a>
                        <?php }}?>
                    </footer>
                </div>
            </div>
              <div class="col-xs-6 post_section">
                <ul class="list-unstyled innerLR">
                    <li>Post Referred :<span class="text-color-Text-Primary-Blue pull-right">0</span></li>
                    <li>Post Earning : <span class="text-color-Text-Primary-Blue pull-right">0</span></li>
                </ul>
                <div class="table-responsive border-all inner-2x">
                    <table class="table bg-color-White margin-bottom-none">
                        <thead>
                            <tr class="bg-color-header_1 text-color-White">
                                                    <th colspan="3" class="text-center f-sz18 f-wt1 l-h13"> Post</th>
                                                </tr>
                            <tr class="bg-color-header_2">
                                                    <th class="f-wt1"> Date</th>
                                                    <th class="text-center  f-wt1"> View Count</th>
                                                    <th class="text-right  f-wt1"> Earnings </th>
                                                </tr>
                        </thead>
                     <!--    <tbody>
                            <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                            <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                        </tbody> -->
                    </table>
                  <!--   <footer class="bg-color-White innerAll clearfix ">
                        <a href="#" class="pull-right innerMB btn-block text-right l-h7 text-color-Deep-Sky-Blue" > View more</a>
                    </footer> -->
                </div>
            </div>

        </div>
    </div>
    </div>
</main>
<?php }else{
    header("Location: $rootUrl./views/prelogin/index.php");
} ?>
<?php include("../layout/transparent_footer.php");?>

<script>

document.getElementById("copyButton1").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget1"), "msg1");
});

document.getElementById("copyButton2").addEventListener("click", function() {
    copyToClipboardMsg(document.getElementById("copyTarget2"), "msg");
});

function copyToClipboardMsg(elem, msgElem) {
    var succeed = copyReferralCode(elem);
    var msg;
    if (!succeed) {
        msg = "Copy not supported or blocked.  Press Ctrl+c to copy."
    } else {
        msg = "Text copied to the clipboard."
    }
    if (typeof msgElem === "string") {
        msgElem = document.getElementById(msgElem);
    }
    msgElem.innerHTML = alert(msg);
    setTimeout(function() {
        msgElem.innerHTML = "";
    }, 2000);
}

function copyReferralCode(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch(e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

</script>

