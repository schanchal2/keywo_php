

<!--=========================================================
=            include: privateTimelineContent.php            =
==========================================================-->
<!-- for getting post -->
<?php
session_start();

require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../helpers/stringHelper.php');
require_once('../../helpers/errorMap.php');
require_once('../../models/social/socialModel.php');
require_once('../../models/social/commonFunction.php');
require_once ('../../helpers/deviceHelper.php');
require_once ('../../helpers/arrayHelper.php');
require_once('../../IPBlocker/ipblocker.php');

if (!isset($_SESSION["email"]) || empty($_SESSION["email"])) {
    //session is not active, redirect to login page
    print("<script>");
    print("var t = setTimeout(\"window.location='".$rootUrl."';\", 000);");
    print("</script>");
    die;
}


$userId             = $_SESSION["id"];
// making curl request
$type               = cleanXSS(rawurldecode($_POST['type']));
$dataCount          = cleanXSS(rawurldecode($_POST['dataCount']));
$lastDataCreateTime = cleanXSS(rawurldecode($_POST['lastDataCreateTime']));
$viewType           = "owner";
$otherAccHandle     = "";
$otherEmail         = "";
$sessionUserId      = "";
$postData           = getUserTimelinePost($userId, $type, $lastDataCreateTime, $viewType, $otherAccHandle, $sessionUserId);
// printArr($postData);
$dataEmptyFlag      = "false";
$bulkData           = array();
$hideAction         = "hide";
foreach ($postData["errMsg"] as $data) {

    $postIddata = array(
        'post_id' => $data['_id'],
        'time'    => $data['created_at']
    );

    array_push($bulkData, $postIddata);

}
$bulkPostId = json_encode($bulkData);
//   Get like post activity count from node database
$activityCount = getPostActivityCount($bulkPostId, '');
// printArr($postData["errMsg"]);

foreach ($postData["errMsg"] as $key => $data) {
// printArr($data);
// echo $data['created_at'];
    if ($key < 10) {
        $mil = $data['created_at'];
        $lastDataTime = $data['created_at'];
        $seconds = $mil / 1000;
        $created_at = date("d-m-Y H:i:s", $seconds);
        $timestamp2 = strtotime($created_at);
        $postId = $data['_id'];
        $post_type = $data['post_type'];
        $postTime = $data['created_at'];
        $post_earnings = $data['post_earnings'];
//get user ip address
        $ipAddr = getClientIP();

//check request for blocking IP address.
        $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
        $extraArgs = array();
// $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $keywords); //$ipAddr

// if (noError($checkIPInfo)) {
//   // Retrieving unique IP search count from checkClientIpInfo() function using IPBlocker.
//   $checkIPInfo = $checkIPInfo["errMsg"];
//   $searchCount = $checkIPInfo["search_count"];
//   setErrorStack($returnArr, -1, $checkIPInfo, $searchCount);
// } else {
//   setErrorStack($returnArr, 11, $errMsg,  extraArgs);
// }

//Check Like post details
        $getLikePostStatus = getLikePostStatus($_SESSION['account_handle'], $postId, $type);


// printArr($activityCount['errMsg'][$postId]);
        $targetActivityCount = $activityCount['errMsg'][$postId];
        $like_count = $targetActivityCount['like_count'];
        $commentCount = $targetActivityCount['comment_count'];
        $shareCount = $targetActivityCount['share_count'];

        if (isset($targetActivityCount['parent_like_count'])) {
            $parentLikeCount = $targetActivityCount['parent_like_count'];
        } else {
            $parentLikeCount = "";
        }


        if (isset($targetActivityCount['parent_comment_count'])) {
            $parentCommentCount = $targetActivityCount['parent_comment_count'];
        } else {
            $parentCommentCount = "";
        }
        if (isset($targetActivityCount['parent_share_count'])) {
            $parentShareCount = $targetActivityCount['parent_share_count'];
        } else {
            $parentShareCount = "";
        }


// Get bookmark Details
        $getBookMarkPost = getBookMarkPost($_SESSION["account_handle"], $postId, $post_type);


//get short description with mention links

        if (isset($data["post_mention"])) {
            $postMention = $data["post_mention"];
        } else {
            $postMention = "";
        }

        $shortDescriptionText = getLinksOnText(rawurldecode($data["post_short_desc"]), $postMention, $_SESSION["account_handle"]);
        $shortDescription = $shortDescriptionText["text"];
        $descLenWithoutStriptag = strlen($shortDescription);
        $descLenWithStriptag = strlen(strip_tags($shortDescription));
        $diff = $descLenWithoutStriptag - $descLenWithStriptag;
        $shortDescData = ($descLenWithStriptag > $validateCharCountOnSocial) ? mb_substr($shortDescription, 0, $validateCharCountOnSocial + $diff) . "<a href='javascript:;'><span class='text-blue'>....See More</span></a>" : $shortDescription;

//        printArr($data);
// print_r($shortDescription);
    }


?>



<!--<div class="tab-pane" id="lowest">-->
<!--    <!-- content starts here -->
<!--    <div class="col-xs-12 earnings-container padding-none">-->
<!--        <div class="">-->
<!--            <ul class="earnings-list padding-none clearfix" style="list-style:none;">-->
<!--                <li class="clearfix border-bottom padding-none innerMB border-all">-->
<!--                    <div class="card col-xs-12 innerAll padding-bottom-none padding-top-none">-->
<!--                        <div class="col-xs-9 padding-none">-->
<!--                            <div class="col-xs-12 padding-none half innerMT">-->
<!--                                <div class="padding-left-none">-->
<!--                                    <div class="innerMR pull-left">-->
<!--                                        <div class="image-circle-container">-->
<!--                                            <i class="fa fa-circle text-light-grey"></i>-->
<!--                                            <i class="fa fa-file-text text-white"></i>-->
<!--                                        </div>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                                <div class="innerMT padding-none earnings-text">-->
<!--                                    <span>How to dance on stage how to dance on stage</span>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 half innerMT padding-none clearfix">-->
<!--                                <div class="pull-left">-->
<!--                                    <span><i class="fa fa-thumbs-o-up text-blue"></i></span>-->
<!--                                    <span class="half innerMLR">300</span>-->
<!--                                    <!-- Like -->
<!--                                    <span><i class="fa fa-thumbs-o-down text-blue"></i></span>-->
<!--                                    <span class="half innerMLR">23</span>-->
<!--                                    <!-- dislike -->
<!--                                    <span><i class="fa fa-comments-o text-blue"></i></span>-->
<!--                                    <span class="half innerMLR">50</span>-->
<!--                                    <!-- comments -->
<!--                                    <span><i class="fa fa-eye text-blue"></i></span>-->
<!--                                    <span class="half innerMLR">33</span>-->
<!--                                    <!-- view -->
<!--                                </div>-->
<!--                                <div class="pull-left inner-2x innerML">-->
<!--                                    <span class="text-blue">Earnings : </span>-->
<!--                                    <div class="pull-left innerMT-3px"></div>-->
<!--                                    <label class="text-black ellipses pull-right text-left half innerMT max-width-80">-->
<!--                                        <a href="#" class="display-in-block-txt-blk pull-right" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="300.41203211313 --><?php //echo $keywoDefaultCurrencyName; ?><!--">&nbsp;300.41203211313 --><?php //echo $keywoDefaultCurrencyName; ?><!--</a></label>-->
<!--                                    <!-- earnings -->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-xs-3 innerMT pull-right text-right padding-right-none">-->
<!--                            <input type="button" class="btn-trading-wid-auto innerMB text-center" value="View Post">-->
<!--                            <input type="button" class="btn-trading-wid-auto-dark text-center" value=" Analytics ">-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </div>-->
<!--    </div>-->
<!--    <!-- content ends here -->
<!--</div>-->


<div class="tab-pane fade active in" id="highest">
    <!-- content starts here -->
    <div class="col-xs-12 earnings-container padding-none">
        <div class="">
            <ul class="earnings-list padding-none clearfix" style="list-style:none;">
                <li class="clearfix border-bottom padding-none innerMB border-all">
                    <div class="card col-xs-12 innerAll padding-bottom-none padding-top-none">
                        <div class="col-xs-9 padding-none">



                            <div class="col-xs-12 padding-none half innerMT">
                                <div class="padding-left-none">
                                    <div class="innerMR pull-left">


                                            <div class="image-circle-container">
                                                <?php
                                                    $value = $data["post_type"];
                                                    switch ($value) {
                                                    case 'video':
                                                ?>
                                                        <i class="fa fa-circle text-red"></i>
                                                        <i class="fa fa-play-circle text-white"></i>
                                                <?php	break;
                                                case 'audio':
                                                ?>
                                                    <i class="fa fa-circle text-orange"></i>
                                                    <i class="fa fa-soundcloud text-white"></i>
                                                    <?php break;
                                                case 'image':
                                                    ?>
                                                    <i class="fa fa-circle text-sky-blue"></i>
                                                    <i class="fa fa-image text-white"></i>
                                                    <?php break;
                                                case 'status':
                                                    ?>
                                                     <i class="fa fa-circle text-light-grey"></i>
                                                    <i class="fa fa-file-text text-white"></i>
                                                <?php } ?>


                                            </div>


                                    </div>
                                </div>
                                <div class="innerMT padding-none earnings-text">
                                    <span><?php echo $shortDescData; ?></span>
                                </div>
                            </div>



                            <div class="col-xs-12 half innerMT padding-none clearfix">
                                <div class="pull-left">
                                    <span><i class="fa fa-thumbs-o-up text-blue"></i></span>
                                    <span class="half innerMLR"><?php echo formatNumberToSort("{$like_count}", 0); ?></span>
                                    <!-- Like -->

                                    <!-- dislike -->
                                    <span><i class="fa fa-comments-o text-blue"></i></span>
                                    <span class="half innerMLR"><?php echo  formatNumberToSort("{$commentCount}", 0);?></span>
                                    <!-- comments -->
                                    <span><i class="fa fa-eye text-blue"></i></span>
                                    <span class="half innerMLR">33</span>
                                    <!-- view -->
                                </div>
                                <div class="pull-left inner-2x innerML">
                                    <span class="text-blue">Earnings : </span>
                                    <span>
                <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo "{$post_earnings} ".$keywoDefaultCurrencyName; ?>" class="currency">
                  <?= $post_earnings >= 0?formatNumberToSort("{$post_earnings}", 2):0 ; ?> <?php echo $keywoDefaultCurrencyName; ?>
                </span>
              </span>


                                    </label>
                                    <!-- earnings -->
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-3 innerMT pull-right text-right padding-right-none">
                            <input type="button" class="btn-trading-wid-auto innerMB text-center" value="View Post">
                            <input type="button" class="btn-trading-wid-auto-dark text-center" value=" Analytics ">
                        </div>
                    </div>
                </li>
                <!--                             -->
            </ul>
        </div>
    </div>
    <!-- content ends here -->
</div>
<?php } ?>