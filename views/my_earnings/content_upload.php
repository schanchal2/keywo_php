<?php
session_start();

//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

    include("../layout/header.php");

    $email      = $_SESSION["email"];

    ?>
    <link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_earnings.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css" />
    <main class="myearnings-main-container inner-8x innerT" id="my-earning">
        <div class="container">
            <div class="col-xs-3">
                <div class="myearnings-left-panel">
                    <div class="card my-info-card social-card">
                        <div class="clearfix innerAll border-all">
                            <div class="my-info-img pull-left innerMR">
                                <img class="img-responsive" src="<?php echo $rootUrlImages; ?>rightpanel-img/Default_profile_image.PNG">
                            </div>
                            <!-- my-picture  -->
                            <div class="my-info-detail pull-left">
                                <div class="user-name">
                                    Vishal Gupta
                                </div>
                                <div class="user-earnings-text">
                                    Earning
                                </div>
                                <h4 class="text-blue margin-none max-width-80">
                                    <span class="txt-blue ellipses"><a href="#" class="display-in-block-txt-blk" title="">243.43 <?php echo $keywoDefaultCurrencyName; ?></a></span>
                                </h4>
                            </div>
                        </div>
                        <!-- my-info-detail  -->
                    </div>
                    <!-- my-info-card  -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content consumption</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Serach</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content consumption -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Content Upload</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Blog</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Video</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Image</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Audio</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>App</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content upload -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Keyword</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Owned</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Active Trade</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Trading history</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Keyword -->
                    <div class="card left-panel-modules inner-2x innerMT">
                        <div class="bg-light-gray left-panel-modules-head">
                            <div class="row margin-none">
                                <div class="col-xs-12">
                                    <h4 class="margin-none half innerTB">Referral</h4>
                                </div>
                            </div>
                        </div>
                        <div class="earnings-left-module">
                            <div class="margin-none">
                                <div class="social-user-setting-name border-bottom">
                                    <a>Interaction</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Commission</a>
                                </div>
                                <div class="social-user-setting-name border-bottom">
                                    <a>Post Share</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Referral -->
                </div>
                <!-- social-left-panel  -->
            </div>
            <!-- col-xs-3 -->

            <div id="load-analytic-next-post-data" style="display:none;" data-count="0" data-type="all" data-scroll-allow="true" data-create-time = "" data-status-empty="false"></div>
            <!--            load Content hear-->
            <div id="load-analytic-post-data">

            </div>
            <!-- col-xs-6 -->
            <div class="col-xs-3 keyword-marketplace-data keymarketplace-data">

                <!-- Dropdown -->
                <div class="card right-panel-modules">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Blog</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix  padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Blog -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Video</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Video -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Image</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Image -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Audio</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none border-all">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Audio -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none border-all">
                            <div class="col-xs-12">
                                <h4 class="margin-none half innerTB">Search</h4>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div class="margin-none">
                            <div class="social-user-setting-name half innerTB clearfix padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Post</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Interaction</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00</a></label>
                                </div>
                            </div>
                            <div class="social-user-setting-name half innerTB clearfix border-top-none padding-bottom-none">
                                <div class="col-xs-6 text-left">
                                    <span>Earnings</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <label class="text-black ellipses pull-right margin-bottom-none margin-top-none"><a href="#" class="display-in-block-txt-blk" title="" data-toggle="tooltip" data-placement="bottom" data-original-title="00">00 <?php echo $keywoDefaultCurrencyName; ?></a></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Search -->
                <div class="card right-panel-modules inner-2x innerMT">
                    <div class="bg-light-gray right-panel-modules-head">
                        <div class="row margin-none">
                            <div class="col-xs-12 padding-none">
                                <select name="" class="earnings-select border-all half innerAll">
                                    <option>Content Consumption</option>
                                    <option>Content Upload</option>
                                    <option>Keyword</option>
                                    <option>Referral</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="earnings-right-module">
                        <div id='calendar'></div>
                        <div class="text-blue innerTB text-center download-btn">
                            <span class="download-icon">
              <i class="fa fa-download text-white"></i>
            </span>
                            <input class="btn-social-wid-auto text-center download-calendar-button" type="button" value="Download Report" />
                        </div>
                    </div>
                </div>
                <!-- Calendar -->
            </div>
            <!-- col-xs-3 -->
        </div>
        <!-- container -->
    </main>
    <?php
} else {
    header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>
<?php include('../layout/social_footer.php'); ?>
<script src="../../frontend_libraries/jquery-ui-month-picker-master/MonthPicker.js"></script>
<script type="text/javascript">
    $("#calendar").MonthPicker({
        SelectedMonth: '04/' + new Date().getFullYear(),
        OnAfterChooseMonth: function(selectedDate) {
            // Do something with selected JavaScript date.
            // console.log(selectedDate);
        },
        i18n: {
            months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        }
    });
</script>






<script>
    var bottom = $(document).height() - $(window).height(); //for endless scrolling
    var successflag = 0;
    $(document).scroll(function(){
        /*
         For endless scrolling
         */
        var win = $(window);
        // Each time the user scrolls
        win.scroll(function() {
            // End of the document reached?
            if ($(document).height() - win.height() == win.scrollTop()) {
                // Do the stuff
                if(successflag == 0){
                    var privateScrollAllow = $("#load-analytic-next-post-data").attr("data-scroll-allow");
                    var privateDataEmpty = $("#load-analytic-next-post-data").attr("data-status-empty");
                    var privateDataType = $("#load-analytic-next-post-data").attr("data-type");
                    if (privateScrollAllow == "true" && privateDataEmpty == "false") {
                        $('#ajaxLoader1').show();
                        loadAnanyticPage(privateDataType);
                    }
                    successflag = 1;
                }
            }else{
                // console.log("else");
                successflag = 0;
            }
        });
    });
</script>
