<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_Referral_Zone.css" type="text/css" />
<!-- 

 -->
<main class="inner-7x innerT RZ3">
    <div class="container  row-10 referral text-color-grayscale-6 padding-none">
        <div class="row innerMT inner-9x f-sz15 ">
            <div class="col Email_panel innerT inner-9x ">
                <div class="panel panel-default border-none">
                    <div class="panel-heading f-sz18 text-color-Text-Primary-Blue bg-color-White innerAll padding-bottom-none"><div class="innerT">Email Template</div></div>
                    <div class="panel-body innerAll">
                        <p class="margin-none">Hey, I just signed up with SearchTrade, it is a new search engine platform that gives back advertising income to its users.
                            <!--  -->
                            SearchTrade is pre-selling kewords which will entitle owners to earn income every time the keyword they ow will be used as
                            <!--  -->
                            part of search query on the SearchTrade platform.</p><p> Investing in keyword is an experience similar to buying a domain name.
                            <!--  -->
                            Diference being, unlike domain, keywords generate recurring income for its owner. Click the link below to learn and keyword investment today.</p>
                        <a href="#" class="text-color-Deep-Sky-Blue ">https://www.searchtrade.com </a>
                        <p class="text-color-Text-Primary-Blue f-sz16 margin-none innerT">REGISTERED OFFICE</p>
                        <p>1 Scotts Road, #24-10
                            <br> Shaw Centre, Singapore 228208
                            <br> contact@searchtrade.com
                        </p>
                        <span class="pull-right innerB">
                            <button type="button" class="btn text-white bg-color-Lightest-Blue innerMR">Copy content</button>
                            <button type="button" class="btn text-white bg-color-Dark-Blue innerMR">Cancel</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
