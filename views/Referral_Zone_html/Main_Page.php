<?php include("../layout/header.php");?>
<link rel="stylesheet" href="<?php echo $rootUrlCss; ?>app_Referral_Zone.css" type="text/css" />
<!-- 

 -->
<main class="inner-7x innerT RZ1">
    <div class="container  row-10 referral text-color-grayscale-6 padding-none">
        <div class="row">
            <div class="col-xs-12">
                <h6 class="referral-heading text-color-Text-Primary-Blue f-sz20  text-center">Keywo Referral Program</h6>
            </div>
        </div>
        <div class="row">
            <div class="referral-heading_info col">
                <p class="text-center f-sz16"> Share this referral code or URL with your friends and earn 7.5% commission on every keyword they purchase , Earn 5% share of their search payout and share your friends posts and earn 50% payout on every post view. </p>
            </div>
        </div>
        <div class="row innerLR">
            <div class="col-xs-6 referral-url">
                <div class="row">
                    <form class="form-horizontal innerT clearfix">
                        <div class="margin-bottom-none filter ">
                            <label for="inputPassword3" class="col pull-left control-label  text-grayscale-6 f-sz15 f-wt1"> <span class="text-left">My referral url : </span></label>
                            <div class="col-sm-9">
                                <div class="input-group referral-url_userInput text-Gray ">
                                    <input placeholder="" type="text" class="form-control text-Gray text-grayscale-4 placeholder-grayscale-4  bg-color-grayscale-fb f-sz15" value="">
                                    <span class="input-group-addon bg-color-grayscale-fb  bg-white text-grayscale-6 f-sz15">  <i class="fa fa-copy">  </i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="form-horizontal innerT inner-2x">
                        <div class="col">
                            <label class="control-label f-wt1"> Refer via social :</label>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_facebook">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-facebook "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_twitter">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-twitter" ></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_linkedin">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-linkedin "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-6 referral-code">
                <div class="row">
                    <form class="form-horizontal innerT pull-right clearfix">
                        <div class="margin-bottom-none filter ">
                            <label for="inputPassword3" class="col pull-left control-label  text-grayscale-6 f-sz15 f-wt1"> <span class="text-left">My referral code : </span></label>
                            <div class="col">
                                <div class="input-group referral-code_userInput text-Gray">
                                    <input placeholder="" type="text" class="form-control bg-color-grayscale-fb  text-Gray text-grayscale-4 placeholder-grayscale-4 f-sz15" value="">
                                    <span class="input-group-addon bg-color-grayscale-fb   bg-white text-grayscale-6 f-sz15">  <i class="fa fa-copy">  </i></span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <div class="form-horizontal innerT inner-2x pull-right">
                        <div class="col">
                            <label class="control-label f-wt1"> Refer via mail :</label>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_google">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-google-plus "></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_envelope">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-envelope" ></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                        <div class="col padding-left-none">
                            <div class="input-group referral-url_yahoo">
                                <span class="input-group-addon f-sz14">  <i class="fa fa-fw fa-yahoo"></i> </span>
                                <button type="button" class="btn btn-danger f-sz15"> Share </button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
        </div>
        <hr class="">
        <div class="row innerLR">
            <div class="col">
                <div class="f-sz15">Referred users : <span class="text-color-Text-Primary-Blue">10 </span> | Referral Earnings : <span class="text-color-Text-Primary-Blue">10.0001 IT$</span> </div>
            </div>
            <div class="col pull-right">
                <button type="button" class="btn bg-color-Lightest-Blue text-color-White f-sz15 innerMR inner-2x">Video Ads</button>
                <button type="button" class="btn bg-color-Lightest-Blue text-color-White f-sz15">Banner Ads</button>
            </div>
        </div>
        <!-- 







             -->
        <div class="row innerMT inner-2x f-sz15 ">
            <div class="col-xs-4 search_section">
                <ul class="list-unstyled innerLR">
                    <li>Searches Referred :<span class="text-color-Text-Primary-Blue pull-right">0102</span></li>
                    <li>Search referral Earning : <span class="text-color-Text-Primary-Blue pull-right">12.1212 IT$</span></li>
                </ul>
                <div class="table-responsive border-all inner-2x">
                    <table class="table bg-color-White margin-bottom-none">
                        <thead>
                            <tr class="bg-color-header_1 text-color-White">
                                                        <th colspan="3" class="text-center f-sz18  f-wt1 l-h13"> Search</th>
                                                    </tr>
                            <tr class="bg-color-header_2">
                                                        <th class="f-wt1"> Date</th>
                                                        <th class=" f-wt1 text-center"> Search Count</th>
                                                        <th class=" f-wt1 text-right"> Payout </th>
                                                    </tr>
                        </thead>
                        <tbody>
                            <tr>
                                                        <td>10 jan, 2015</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                            <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                        </tbody>
                    </table>
                      <footer class="bg-color-White innerAll clearfix ">
                        <a href="#" class="pull-right innerMB btn-block text-right l-h7 text-color-Deep-Sky-Blue" > View more</a>
                    </footer>
                </div>
            </div>
            <div class="col-xs-4 keyword_section">
                <ul class="list-unstyled innerLR">
                    <li>Keyword sold :<span class="text-color-Text-Primary-Blue pull-right">0102</span></li>
                    <li>Commission : <span class="text-color-Text-Primary-Blue pull-right">12.1212 IT$</span></li>
                </ul>
                <div class="table-responsive border-all inner-2x">
                    <table class="table bg-color-White margin-bottom-none">
                        <thead>
                            <tr class="bg-color-header_1 text-color-White">
                                                        <th colspan="3" class="text-center f-sz18 f-wt1 l-h13"> Keyword</th>
                                                    </tr>
                            <tr class="bg-color-header_2">
                                                        <th class="f-wt1"> Keyword </th>
                                                        <th class="text-center f-wt1"> Sale Price</th>
                                                        <th class="text-right  f-wt1"> Commission </th>
                                                    </tr>
                        </thead>
                        <tbody>
                            <tr>
                                                        <td>#key </td>
                                                        <td class="text-center">12.2020 IT$</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>#key </td>
                                                        <td class="text-center">12.2020 IT$</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                            <tr>
                                                        <td>#key </td>
                                                        <td class="text-center">12.2020 IT$</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                            <tr>
                                                        <td>#key </td>
                                                        <td class="text-center">12.2020 IT$</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>#key </td>
                                                        <td class="text-center">12.2020 IT$</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                        </tbody>
                    </table>
                      <footer class="bg-color-White innerAll clearfix ">
                        <a href="#" class="pull-right innerMB btn-block text-right l-h7 text-color-Deep-Sky-Blue" > View more</a>
                    </footer>
                </div>
            </div>
            <div class="col-xs-4 post_section">
                <ul class="list-unstyled innerLR">
                    <li>Post Referred :<span class="text-color-Text-Primary-Blue pull-right">0102</span></li>
                    <li>Post Earning : <span class="text-color-Text-Primary-Blue pull-right">12.1212 IT$</span></li>
                </ul>
                <div class="table-responsive border-all inner-2x">
                    <table class="table bg-color-White margin-bottom-none">
                        <thead>
                            <tr class="bg-color-header_1 text-color-White">
                                                    <th colspan="3" class="text-center f-sz18 f-wt1 l-h13"> Post</th>
                                                </tr>
                            <tr class="bg-color-header_2">
                                                    <th class="f-wt1"> Date</th>
                                                    <th class="text-center  f-wt1"> View Count</th>
                                                    <th class="text-right  f-wt1"> Earnings </th>
                                                </tr>
                        </thead>
                        <tbody>
                            <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                            <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr> <tr>
                                                        <td>10 jan, 2017</td>
                                                        <td class="text-center">20</td>
                                                        <td class="text-right">15.2020 IT$</td>
                                                    </tr>
                        </tbody>
                    </table>
                    <footer class="bg-color-White innerAll clearfix ">
                        <a href="#" class="pull-right innerMB btn-block text-right l-h7 text-color-Deep-Sky-Blue" > View more</a>
                    </footer>
                </div>
            </div>
        </div>
    </div>
    </div>
</main>
<?php include("../layout/transparent_footer.php");?>
