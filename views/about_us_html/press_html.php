<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
    <div class="row press-container">
      <div class="col-xs-3">
    		<div class="about-us-left-panel">
    		  <div class="card left-panel-modules">
    				<div class="bg-light-gray left-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB">About us</h4>
  							</div>
  						</div>
    				</div>
    				<div class="about-us-left-panel">
    					<div class="margin-none">
    						<div class="about-us-menu border-bottom">
    							<a>Company</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Announcements</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Blog</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Privacy Policy</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Terms of Use</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Disclaimer</a>
    						</div>
                <div class="about-us-menu border-bottom active">
    							<a>Press</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>contact</a>
    						</div>
              </div>
    				</div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
    	</div>
    	<!-- col-xs-3 -->

      <div class="col-xs-6 inner-2x innerB">
        <div class="row">
            <h3 class="text-black margin-top-none title">Keywo - Press</h3>
						<div class="pull-right innerMR innerMT">
								<input type="text" class="form-control date-of-birth" value="DD/MM/YYYY" placeholder="" id="datepicker">
								<span class="text-Blue calendar"> <i class="fa fa-calendar" id=""></i> </span>
						</div>
        </div>
        <div class="row press-list innerMT">
          <div class="col-xs-12 card social-card inner-2x innerMB">
            <div class="innerTB clearfix">
              <div class="pull-left">
                  <img class="press-img" src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
              </div>
              <div class="press-detail pull-left innerML">
                <div class="press-title text-blue">
                  <h4 class="margin-top-none half innerMB">Media Coverage and press release</h4>
                </div>
                <div class="press-information">
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                </div>
              </div>
              <div class="pull-left innerMT">
                <span> Publication: </span>
                <a href="" class="publisher-name">CryptoCoinNews</a>
                <span class="publish-date text-orange">08-Sept-2017</span>
              </div>
              <div class="pull-right innerMT">
                <a href="" class="read-more">Read more</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 card social-card inner-2x innerMB">
            <div class="innerTB clearfix">
              <div class="pull-left">
                  <img class="press-img" src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
              </div>
              <div class="press-detail pull-left innerML">
                <div class="press-title text-blue">
                  <h4 class="margin-top-none half innerMB">Media Coverage and press release</h4>
                </div>
                <div class="press-information">
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                </div>
              </div>
              <div class="pull-left innerMT">
                <span> Publication: </span>
                <a href="" class="publisher-name">CryptoCoinNews</a>
                <span class="publish-date text-orange">08-Sept-2017</span>
              </div>
              <div class="pull-right innerMT">
                <a href="" class="read-more">Read more</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 card social-card inner-2x innerMB">
            <div class="innerTB clearfix">
              <div class="pull-left">
                  <img class="press-img" src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
              </div>
              <div class="press-detail pull-left innerML">
                <div class="press-title text-blue">
                  <h4 class="margin-top-none half innerMB">Media Coverage and press release</h4>
                </div>
                <div class="press-information">
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                </div>
              </div>
              <div class="pull-left innerMT">
                <span> Publication: </span>
                <a href="" class="publisher-name">CryptoCoinNews</a>
                <span class="publish-date text-orange">08-Sept-2017</span>
              </div>
              <div class="pull-right innerMT">
                <a href="" class="read-more">Read more</a>
              </div>
            </div>
          </div>
          <div class="col-xs-12 card social-card inner-2x innerMB">
            <div class="innerTB clearfix">
              <div class="pull-left">
                  <img class="press-img" src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
              </div>
              <div class="press-detail pull-left innerML">
                <div class="press-title text-blue">
                  <h4 class="margin-top-none half innerMB">Media Coverage and press release</h4>
                </div>
                <div class="press-information">
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                  Use your press page to sahre links to your social media channels connect with customers.
                </div>
              </div>
              <div class="pull-left innerMT">
                <span> Publication: </span>
                <a href="" class="publisher-name">CryptoCoinNews</a>
                <span class="publish-date text-orange">08-Sept-2017</span>
              </div>
              <div class="pull-right innerMT">
                <a href="" class="read-more">Read more</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col-xs-6 -->

      <div class="col-xs-3">
        <form class="clearfix margin-none">
          <div class="pull-left search-box-group">
              <input type="text" id="search_box" name="q" value="<?php echo $keywords; ?>"
                     class="form-control" placeholder="Search" required>
              <input type="submit" class="search-icon" value=""/>
          </div>
        </form>
        <div class="card social-card innerMT innerAll right-banner">
          <div class="innerT text-center panel--right">
						<ul class="padding-none margin-none">
							<li>
								<span>For press inqueries</span>
							</li>
							<li>
								<i>abcd@searchtrade.com</i>
							</li>
							<li>
								<span>Downloads</span>
							</li>
						</ul>
						<ul class="padding-none margin-none">
							<li>
								<div class="keywo-main-img" >
									<img src="<?php echo $rootUrlImages?>keywo_main_logo.png"/>
								</div>
							</li>
							<li>
								<div class="keywo-main-img" >
									<img src="<?php echo $rootUrlImages?>keywo-img-box.png"/>
								</div>
							</li>
							<li>
								<div class="keywo-main-img" >
									<img src="<?php echo $rootUrlImages?>keywo-text-img.png"/>
								</div>
							</li>
						</ul>
						<ul class="padding-left-none margin-none innerT">
							<li>
								<span>VIDEO LINKS</span>
							</li>
							<li>
								<span>Introduction(Long)</span>
							</li>
							<li>
								<span>Introduction(Short)</span>
							</li>
							<li>
								<span>Search Miner</span>
							</li>
							<li>
								<span>Keyword Miner</span>
							</li>
							<li>
								<span>App Miner</span>
							</li>
						</ul>


					  <!-- <span>For press inqueries</span><br>
            <i>abcd@searchtrade.com</i><br>
            <span>Downloads</span><br> -->
            <!-- <img class="keywo-main-img" src="<?php echo $rootUrlImages?>keywo_main_logo.png"/>
            <img class="keywo-main-img" src="<?php echo $rootUrlImages?>"/><br>
            <img class="keywo-main-img" src="<?php echo $rootUrlImages?>keywo-text-img.png"/><br> -->
            <!-- <br><br>
            <br><br>
            <br><br>
            <br><br>
            <br><br>
            <br> -->
          </div>
        </div>
      </div>
      <!-- col-xs-3 -->
    </div>


  </div>
<!-- container -->
</main>

<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php include('../layout/social_footer.php'); ?>
<script>
$(function() {
	$('#datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			keyboardNavigation : true ,
			daysOfWeekDisabled : [0]
	});
});
</script>
