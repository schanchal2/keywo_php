<?php
session_start();
//check for session
if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
include("../layout/header.php");
$email      = $_SESSION["email"];

?>
<link rel="stylesheet" href="
  <?php echo $rootUrlCss; ?>app_about_us.css
  <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
  <main class="inner-6x innerT">
  <div class="container">
    <div class="col-xs-3">
      <div class="about-us-left-panel">
        <div class="card left-panel-modules">
          <div class="bg-light-gray left-panel-modules-head">
            <div class="row margin-none">
              <div class="col-xs-12">
                <h4 class="margin-none half innerTB">About us</h4>
              </div>
            </div>
          </div>
          <div class="about-us-left-panel">
            <div class="about-us-menu border-bottom active">
              <a>Company</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Announcements</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Blog</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Privacy Policy</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Terms of Use</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Disclaimer</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>Press</a>
            </div>
            <div class="about-us-menu border-bottom">
              <a>contact</a>
            </div>
          </div>
          <!-- content consumption -->
        </div>
        <!-- social-left-panel  -->
      </div>
    </div>
    <!-- col-xs-3 -->
    <div class="col-xs-9 card inner-2x innerB">
      <h3 class="text-blue text-center">Our Story</h3>
      <div class="about-us-image">
        <div class="objective">
          <p>
            " An idea in a moment has sparked the global movement. And we are just getting started. "
          </p>
          <div class="company-info text-right">
            <strong>Lorem ipsum</strong>, co-founder, CEO
          </div>
        </div>
      </div>
      <div class="innerMTB inner-2x">
        <h4 class="text-center text-blue">
        Everybody has something to say !!!
        </h4>
        <p class="text-center">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris eu congue dui, vitae mattis erat.
          Praesent ut porttitor nulla, ac condimentum risus. Sed ultricies venenatis nunc sed dapibus.
          Nulla consectetur nit amet, consectetur adipiscing elit. Mauris eu congue dui, vitae mattis erat.
          Praesent ut porttitor nulla, ac c
        </p>
      </div>
      <div class="col-xs-12 padding-none text-center">
        <div class="col-xs-4">
          <div class="text-heading sub-heading">
            - Our Mission -
          </div>
          <div class="innerMT">
            " Lorem ipsum dolor sit amet consectetur."
          </div>
        </div>
        <div class="col-xs-4">
          <div class="text-heading sub-heading">
            - Our Vision -
          </div>
          <div class="innerMT">
            " Mauris eu congue dui, vitae mattis erat.
            Praesent ut porttitor"
          </div>
        </div>
        <div class="col-xs-4">
          <div class="text-heading sub-heading">
            - Live Values -
          </div>
          <div class="innerMT">
            " Mauris eu congue dui, vitae mattis erat. "
          </div>
        </div>
      </div>
    </div>
    <!-- col-xs-9 -->
  </div>
  <!-- container -->
  </main>
  <?php
  } else {
  header('location:'. $rootUrl .'../../views/prelogin/index.php');
  }
  ?>
  <?php include('../layout/social_footer.php'); ?>
