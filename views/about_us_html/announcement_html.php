<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];
    }
?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
    <div class="row announcement-container">
      <div class="col-xs-3">
    		<div class="about-us-left-panel">
    		  <div class="card left-panel-modules">
    				<div class="bg-light-gray left-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB">About us</h4>
  							</div>
  						</div>
    				</div>
    				<div class="about-us-left-panel">
    					<div class="margin-none">
    						<div class="about-us-menu border-bottom">
    							<a>Company</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Announcements</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Blog</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Privacy Policy</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Terms of Use</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Disclaimer</a>
    						</div>
                <div class="about-us-menu border-bottom active">
    							<a>Press</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>contact</a>
    						</div>
              </div>
    				</div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
    	</div>
    	<!-- col-xs-3 -->

      <div class="col-xs-6 card card-social">
        <div class="row">
          <div class="col-xs-12 innerMT">
            <h4><strong>Announcement Title</strong></h4>
          </div>
          <div class="col-xs-12 innerMB inner-2x">
            <i class="fa fa-calendar text-orange innerMR half"></i>
            <span>15th March, 2015</span>
          </div>
          <div class="col-xs-12">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor pellentesque justo ac molestie. Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin. Ut dui ipsum, volutpat nec condimentum in, aliquet eget urna. Pellentesque vulputate mauris ac nisl sodales, at rutrum dui facilisis. Aenean a mi quis ipsum facilisis bibendum. Cras tristique ante ac orci vestibulum, id viverra urna rhoncus. Duis dignissim fermentum aliquet. Donec non risus non tellus scelerisque finibus eu eget tortor.
            </p>
            <p>
              Sed quis lacinia velit. Morbi a purus sed nisl porttitor congue. Aenean suscipit faucibus euismod. Aliquam erat volutpat. In lobortis ipsum ex, in fermentum nulla egestas vitae. Sed tincidunt molestie tellus. Integer fermentum turpis ex, quis dapibus dolor tincidunt sed. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed eu mollis nunc. Maecenas id nisi non augue malesuada interdum. Suspendisse ac ultricies justo. Fusce suscipit aliquet est quis ullamcorper. Pellentesque tortor sem, malesuada in orci at, molestie semper tellus. Praesent enim turpis, varius quis arcu et, sollicitudin cursus lectus. Vestibulum aliquam lectus ut magna rhoncus, ac scelerisque neque cursus. Cras ac ligula dolor.
            </p>

            <p>
              Praesent porta convallis congue. Fusce convallis suscipit tempor. Donec luctus libero quis ante congue convallis. Nullam convallis ullamcorper bibendum. Vivamus tortor arcu, pellentesque eu blandit quis, egestas eu augue. In risus ante, viverra eu consequat a, placerat nec magna. Aenean molestie ligula a turpis commodo, sed maximus nulla luctus.
            </p>
          </div>
        </div>
      </div>
      <!-- col-xs-6 -->

      <div class="col-xs-3">
        <div class="about-us-right-panel">
    		  <div class="card right-panel-modules">
    				<div class="bg-light-gray right-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB pull-left">Recent Announcement</h4>
                  <i class="pull-right fa fa-calendar half innerMT text-white"></i>
  							</div>
  						</div>
    				</div>
    				<div class="announcement-list">
              <div class="clearfix border-bottom">
                <div class="innerAll padding-bottom-none text-blue announcement-information">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor pellentesque justo acmolestie. Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin.
                </div>
                <div class="innerT half pull-right innerMR innerMB">
                  <span class="text-grayscale-80">21st Jan,2015</span>
                </div>
              </div>
              <div class="clearfix border-bottom">
                <div class="innerAll padding-bottom-none text-blue announcement-information">
                  Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin. acmolestie.
                </div>
                <div class="innerT half pull-right innerMR innerMB">
                  <span class="text-grayscale-80">21st Jan,2015</span>
                </div>
              </div>
              <div class="clearfix border-bottom">
                <div class="innerAll padding-bottom-none text-blue announcement-information">
                  Sed quis lacinia velit. Morbi a purus sed nisl porttitor congue. Aenean suscipit faucibus euismod. Aliquam erat volutpat. In lobortis ipsum ex, in fermentum nulla egestas vitae. Sed tincidunt molestie tellus
                </div>
                <div class="innerT half pull-right innerMR innerMB">
                  <span class="text-grayscale-80">21st Jan,2015</span>
                </div>
              </div>

            </div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
      </div>
      <!-- col-xs-3 -->


    </div>
  </div>
<!-- container -->
</main>

<?php

?>
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php include('../layout/social_footer.php'); ?>
