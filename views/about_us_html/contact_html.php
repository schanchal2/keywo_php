<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
    <div class="row contact-container">
      <div class="col-xs-3">
    		<div class="about-us-left-panel">
    		  <div class="card left-panel-modules">
    				<div class="bg-light-gray left-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB">About us</h4>
  							</div>
  						</div>
    				</div>
    				<div class="about-us-left-panel">
    					<div class="margin-none">
    						<div class="about-us-menu border-bottom">
    							<a>Company</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Announcements</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Blog</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Privacy Policy</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Terms of Use</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Disclaimer</a>
    						</div>
                <div class="about-us-menu border-bottom active">
    							<a>Press</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>contact</a>
    						</div>
              </div>
    				</div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
    	</div>
    	<!-- col-xs-3 -->

      <div class="col-xs-9 card card-social contact-container">
        <div class="row innerTB">
          <h4 class="text-center"><strong>Nice to meet you.</strong></h4>
        </div>
        <div class="row">
          <div class="col-xs-12 inner-2x innerMB">
            <div id="map">
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-5">
            <div class="row">
              <div class="col-xs-12 innerMB">
                <span class="text-blue contact-info">Contact Info</span>
              </div>
              <div class="col-xs-3 innerMB">
                <span><strong>Address</strong></span>
              </div>
              <div class="col-xs-9 innerMB">
                <div>123 Your street, your avenue</div>
                <div>Your country, bhs8us</div>
              </div>
              <div class="col-xs-3 innerMB">
                <span><strong>Email</strong></span>
              </div>
              <div class="col-xs-9 innerMB">
                <div class="text-blue">xyz@abc.in</div>
              </div>
              <div class="col-xs-3 innerMB">
                <div class="innerMT half">
                  <strong>Social</strong>
                </div>
              </div>
              <div class="col-xs-9 innerMB">
                <div class="contact-icons">
                  <i class="fa fa-facebook-square text-blue"></i>
                  <i class="fa fa-google-plus-square text-red"></i>
                  <i class="fa fa-twitter-square text-light-blue"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-7">
            <div class="innerMB">
              <span class="text-blue contact-info">Get in touch with us</span>
            </div>
            <div class="innerMB">
              <span>For any inquiries, questions or commendation-</span><br>
              <span>Please fill out the following form</span>
            </div>
            <form class="clearfix contact-info-form">
              <input type="text" class="form-control" placeholder="Your name"/>
              <input type="email" class="form-control" placeholder="Your email" />
              <br>
              <textarea placeholder="Message" class="form-control innerMT" rows="6"></textarea>
              <br>
              <input type="button" class="btn-social btn-xs pull-right innerMB" value="SEND EMAIL"/>
            </form>
          </div>
        </div>
        </div>
      </div>
      <!-- col-xs-9 -->


    </div>
  </div>
<!-- container -->
</main>

<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php include('../layout/social_footer.php'); ?>

<script>

$(function() {

        var myLatLng = {lat: 19.0724800, lng: 72.8653710};

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: myLatLng
        });

        var marker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          title: 'Hello World!'
        });
	$('#datepicker').datepicker({
			format: 'dd/mm/yyyy',
			autoclose: true,
			keyboardNavigation : true ,
			daysOfWeekDisabled : [0]
	});
});
</script>
