<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
  	<div class="col-xs-3">
  		<div class="about-us-left-panel">
  		  <div class="card left-panel-modules">
  				<div class="bg-light-gray left-panel-modules-head">
  					<div class="row margin-none">
  						<div class="col-xs-12">
  							<h4 class="margin-none half innerTB">About us</h4>
							</div>
						</div>
  				</div>
  				<div class="about-us-left-panel">
  					<div class="margin-none">
  						<div class="about-us-menu border-bottom">
  							<a>Company</a>
  						</div>
  						<div class="about-us-menu border-bottom">
  							<a>Announcements</a>
  						</div>
  						<div class="about-us-menu border-bottom">
  							<a>Blog</a>
  						</div>
  						<div class="about-us-menu border-bottom">
  							<a>Privacy Policy</a>
  						</div>
              <div class="about-us-menu border-bottom">
  							<a>Terms of Use</a>
  						</div>
              <div class="about-us-menu border-bottom active">
  							<a>Disclaimer</a>
  						</div>
              <div class="about-us-menu border-bottom">
  							<a>Press</a>
  						</div>
              <div class="about-us-menu border-bottom">
  							<a>contact</a>
  						</div>
            </div>
  				</div>
  			</div>
        <!-- content consumption -->
      </div>
  		<!-- social-left-panel  -->
  	</div>
  	<!-- col-xs-3 -->

    <div class="col-xs-9 card disclaimer-container inner-2x innerB innerMB">
      <h3>
        Keywo - Disclaimer
      </h3>
      <div class="list-of-terms innerMT">
        <ul class="list-group">
          <li class="list-group-item border-none padding-none">
            <div class="innerMTB inner-2x">
                1. Acceptance of our terms
            </div>
             <p>A Privacy Policy is a legal statement that specifies what the
                 business owner does with the personal data collected from users,
                  along with how the data is processed and why.
                  In 1968, Council of Europe did studies on the threat of the Internet
                 expansion as they were concerned with the effects of technology on human
                 rights. This lead to the development of policies that were to be developed
                 to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
            </p>
           </li>
           <li class="list-group-item border-none padding-none">
             <div class="innerMTB inner-2x">
                 2. Acceptance of our terms
             </div>
              <p>A Privacy Policy is a legal statement that specifies what the
                  business owner does with the personal data collected from users,
                   along with how the data is processed and why.
                   In 1968, Council of Europe did studies on the threat of the Internet
                  expansion as they were concerned with the effects of technology on human
                  rights. This lead to the development of policies that were to be developed
                  to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
             </p>
            </li>
            <li class="list-group-item border-none padding-none">
              <div class="innerMTB inner-2x">
                  3. Acceptance of our terms
              </div>
               <p>A Privacy Policy is a legal statement that specifies what the
                   business owner does with the personal data collected from users,
                    along with how the data is processed and why.
                    In 1968, Council of Europe did studies on the threat of the Internet
                   expansion as they were concerned with the effects of technology on human
                   rights. This lead to the development of policies that were to be developed
                   to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
              </p>
             </li>
             <li class="list-group-item border-none padding-none">
               <div class="innerMTB inner-2x">
                   4. Acceptance of our terms
               </div>
                <p>A Privacy Policy is a legal statement that specifies what the
                    business owner does with the personal data collected from users,
                     along with how the data is processed and why.
                     In 1968, Council of Europe did studies on the threat of the Internet
                    expansion as they were concerned with the effects of technology on human
                    rights. This lead to the development of policies that were to be developed
                    to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
               </p>
              </li>
              <li class="list-group-item border-none padding-none">
                <div class="innerMTB inner-2x">
                    5. Acceptance of our terms
                </div>
                 <p>A Privacy Policy is a legal statement that specifies what the
                     business owner does with the personal data collected from users,
                      along with how the data is processed and why.
                      In 1968, Council of Europe did studies on the threat of the Internet
                     expansion as they were concerned with the effects of technology on human
                     rights. This lead to the development of policies that were to be developed
                     to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
                </p>
               </li>
               <li class="list-group-item border-none padding-none">
                 <div class="innerMTB inner-2x">
                     6. Acceptance of our terms
                 </div>
                  <p>A Privacy Policy is a legal statement that specifies what the
                      business owner does with the personal data collected from users,
                       along with how the data is processed and why.
                       In 1968, Council of Europe did studies on the threat of the Internet
                      expansion as they were concerned with the effects of technology on human
                      rights. This lead to the development of policies that were to be developed
                      to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
                 </p>
                </li>
                <li class="list-group-item border-none padding-none">
                  <div class="innerMTB inner-2x">
                      7. Acceptance of our terms
                  </div>
                   <p>A Privacy Policy is a legal statement that specifies what the
                       business owner does with the personal data collected from users,
                        along with how the data is processed and why.
                        In 1968, Council of Europe did studies on the threat of the Internet
                       expansion as they were concerned with the effects of technology on human
                       rights. This lead to the development of policies that were to be developed
                       to protect personal data.This marks the start of what we know now as a “Privacy Policy”. While the
                  </p>
                 </li>
        </ul>
      </div>
    </div>
    <!-- col-xs-9 -->

  </div>
<!-- container -->
</main>

<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php include('../layout/social_footer.php'); ?>
