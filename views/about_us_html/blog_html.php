<?php
	session_start();

	//check for session
	if (isset($_SESSION["email"]) && !empty($_SESSION["email"])) {

		include("../layout/header.php");

		$email      = $_SESSION["email"];

?>

<link rel="stylesheet" href="
   <?php echo $rootUrlCss; ?>app_about_us.css
   <?php /*echo date('l jS \of F Y h:i:s A'); */ ?>" type="text/css"/>
<main class="inner-6x innerT">
  <div class="container">
    <div class="row about-us-blog-container">
      <div class="col-xs-3">
    		<div class="about-us-left-panel">
    		  <div class="card left-panel-modules">
    				<div class="bg-light-gray left-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12">
    							<h4 class="margin-none half innerTB">About us</h4>
  							</div>
  						</div>
    				</div>
    				<div class="about-us-left-panel">
    					<div class="margin-none">
    						<div class="about-us-menu border-bottom">
    							<a>Company</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Announcements</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Blog</a>
    						</div>
    						<div class="about-us-menu border-bottom">
    							<a>Privacy Policy</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Terms of Use</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>Disclaimer</a>
    						</div>
                <div class="about-us-menu border-bottom active">
    							<a>Press</a>
    						</div>
                <div class="about-us-menu border-bottom">
    							<a>contact</a>
    						</div>
              </div>
    				</div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
    	</div>
    	<!-- col-xs-3 -->

      <div class="col-xs-6 card card-social innerMB inner-2x">
        <div class="row">
          <div class="col-xs-12 innerMT">
            <h4><strong>10 types rules for an excellent user experience</strong></h4>
          </div>
          <div class="col-xs-12 innerMB inner-2x">
            <i class="fa fa-user text-orange innerMR half"></i>
            <span class="innerMR">Remus Richard</span>
            <i class="fa fa-calendar text-orange innerMR half"></i>
            <span>15th March, 2015</span>
          </div>
          <div class="col-xs-12 innerMB inner-2x">
            <img class="img-responsive" src="<?php echo $rootUrlImages?>about_us_meeting.jpg"/>
          </div>
          <div class="col-xs-12">
            <p>
              Sed quis lacinia velit. Morbi a purus sed nisl porttitor congue. Aenean suscipit faucibus euismod. Aliquam erat volutpat. In lobortis ipsum ex, in fermentum nulla egestas vitae. Sed tincidunt molestie tellus.
            </p>
            <p>
              Integer fermentum turpis ex, quis dapibus dolor tincidunt sed. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed eu mollis nunc. Maecenas id nisi non augue malesuada interdum.
            </p>
            <p>
              Suspendisse ac ultricies justo. Fusce suscipit aliquet est quis ullamcorper. Pellentesque tortor sem, malesuada in orci at, molestie semper tellus. Praesent enim turpis, varius quis arcu et, sollicitudin cursus lectus. Vestibulum aliquam lectus ut magna rhoncus, ac scelerisque neque cursus. Cras ac ligula dolor.
            </p>
          </div>
          <div class="col-xs-12">
            <div id="comment-include-" class="comment-append-section comments-module bg-light-purple">
               <input type="hidden" id="comment-basic-details194" class="comment-special-class" post-id="194" post-created-at="1491298335864" call-ajax-flag="true" call-ajax-get-flag="true" call-ajax-edit-flag="true" call-reply-ajax-flag="true">
               <div id="comment-text-add194" class="row comments-module-wrapper">
                  <div class="col-xs-1 wid-50">
                     <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                  </div>
                  <div class="col-xs-11 padding-left-none comment-text-empty">
                     <div id="comment-form194" class="">
                        <input type="text" id="user-comment-text194" class="form-control checkEmpty" placeholder="Write Comment Here" maxlength="600">
                     </div>
                  </div>
               </div>
               <div class="comment-inner-section" id="comment-append194">
                  <input type="hidden" id="comment-indivisual-div-58ee076b06ea2a38473bc752" call-ajax-reply-get-flag="true">
                  <div id="commented-list-58ee076b06ea2a38473bc752" class="row comments-module-wrapper">
                     <div class="col-xs-1 wid-50">
                        <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                     </div>
                     <div id="comment-edit-display-58ee076b06ea2a38473bc752" class="col-xs-10  padding-left-none comment-editing-post-194">
                        <div class="comments-module-comment-block">
                           <a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
                           <span id="Comment-og-text58ee076b06ea2a38473bc752" style="display:none;">erf</span>
                           <span id="user-comment-text-display-58ee076b06ea2a38473bc752" class="comments-module-comment">erf</span>
                        </div>
                        <div class="comments-module-time-block">
                           <a id="comment-reply-click-58ee076b06ea2a38473bc752" class="comments-module-reply" onclick="getCommentReplies('function', '', '3', '58ee076b06ea2a38473bc752','1','194');" style="pointer-events: none;">Reply <span id="reply-count-58ee076b06ea2a38473bc752"> (1) </span>
                           </a>
                           <span class="comments-module-hrs">1  sec </span>
                        </div>
                        <div id="comment-reply-append-58ee076b06ea2a38473bc752" class="comment-append-reply" style="">
                           <div id="commented-list-58ee076f06ea2a38473bc753" class="row comments-module-wrapper">
                              <div class="col-xs-1 wid-50">
                                 <img class="reply-img" src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                              </div>
                              <div id="comment-edit-display-58ee076f06ea2a38473bc753" class="col-xs-10  padding-left-none comment-data-each58ee076b06ea2a38473bc752">
                                 <div class="comments-module-comment-block">
                                    <a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
                                    <span id="Comment-reply-og-text58ee076f06ea2a38473bc753" style="display:none;">hu</span>
                                    <span id="user-comment-text-display-58ee076f06ea2a38473bc753" class="comments-module-comment">hu							</span>
                                 </div>
                                 <div class="comments-module-time-block">
                                    <a class="comments-module-reply" onclick="createReplyOnReply('58ee076b06ea2a38473bc752');">Reply</a>
                                    <span class="comments-module-hrs">1  sec </span>
                                 </div>
                                 <div id="comment-reply-div-58ee076f06ea2a38473bc753" class="row comments-module-wrapper comment-reply-create" style="display : none;">
                                    <div class="col-xs-1 wid-50">
                                       <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                                    </div>
                                 </div>
                              </div>
                              <div id="comment-edit-58ee076f06ea2a38473bc753" class="col-xs-11 comment-editing comment-editing-each58ee076b06ea2a38473bc752" style="display :none;">
                              </div>
                              <div id="comment-edit-display-action-58ee076f06ea2a38473bc753" class="col-xs-1 comment-action-each58ee076b06ea2a38473bc752">
                                 <a class="edit-comment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                                    <li class="border-bottom clearfix">
                                       <a data-toggle="modal" onclick="editDeleteReply('update', '58ee076f06ea2a38473bc753', '58ee076b06ea2a38473bc752','194');"><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                                    </li>
                                    <li>
                                       <a data-toggle="modal" data-target="#deleteConfirmation" onclick="deleteConfirmation('194', '58ee076f06ea2a38473bc753','reply','58ee076b06ea2a38473bc752');"><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                           <div id="get-more-comment-reply-58ee076b06ea2a38473bc752" style="display:none;" remainingcount="0" commentloadclick="false"></div>
                        </div>
                        <!--<a id = "show-reply-comment-58ee076b06ea2a38473bc752" class="comments-module-reply" onclick = "getCommentReplies('function', '', '3', '58ee076b06ea2a38473bc752','1');">View 0 Replies</a>-->
                        <div id="comment-reply-div-58ee076b06ea2a38473bc752" class="row comments-module-wrapper comment-reply-create">
                           <div class="col-xs-1 wid-50">
                              <img class="reply-img" src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                           </div>
                           <div class="col-xs-11">
                              <div id="comment-form58ee076b06ea2a38473bc752" class=""><input type="text" id="user-comment-reply-text-58ee076b06ea2a38473bc752" class="form-control checkEmpty reply-input-box" placeholder="Write Reply Here"></div>
                           </div>
                        </div>
                        <div id="commentReplyLoadMoreButton58ee076b06ea2a38473bc752" class="row comments-module-wrapper" style="display:none;">
                           <div class="col-xs-12">
                              <a id="loadMoreCommentReplyPostPage58ee076b06ea2a38473bc752" class="comments-module-all-comments" onclick="">View <span id="remainingReplyLoadCount58ee076b06ea2a38473bc752" class="comment-count">0</span> more Replies
                              </a>
                           </div>
                        </div>
                     </div>
                     <div id="comment-edit-58ee076b06ea2a38473bc752" class="col-xs-11 comment-editing comment-editing-each-post-194" style="display :none;">
                     </div>
                     <div id="comment-edit-display-action-58ee076b06ea2a38473bc752" class="col-xs-1 comment-editing-action-post-194">
                        <a class="edit-comment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                           <li class="border-bottom clearfix">
                              <a data-toggle="modal" onclick="editDeleteComment('update', '58ee076b06ea2a38473bc752', '194');"><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                           </li>
                           <li>
                              <a data-toggle="modal" data-target="#deleteConfirmation" onclick="deleteConfirmation('194', '58ee076b06ea2a38473bc752','comment','');"><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <input type="hidden" id="comment-indivisual-div-58ee076706ea2a38473bc751" call-ajax-reply-get-flag="true">
                  <div id="commented-list-58ee076706ea2a38473bc751" class="row comments-module-wrapper">
                     <div class="col-xs-1 wid-50">
                        <img src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                     </div>
                     <div id="comment-edit-display-58ee076706ea2a38473bc751" class="col-xs-10  padding-left-none comment-editing-post-194">
                        <div class="comments-module-comment-block">
                           <a href="<?php echo $rootUrl?>views/social/others_index.php" class="comments-module-username">Unique Unique</a>
                           <span id="Comment-og-text58ee076706ea2a38473bc751" style="display:none;">hi</span>
                           <span id="user-comment-text-display-58ee076706ea2a38473bc751" class="comments-module-comment">hi</span>
                        </div>
                        <div class="comments-module-time-block">
                           <a id="comment-reply-click-58ee076706ea2a38473bc751" class="comments-module-reply" onclick="getCommentReplies('function', '', '3', '58ee076706ea2a38473bc751','1','194');">Reply <span id="reply-count-58ee076706ea2a38473bc751"></span>
                           </a>
                           <span class="comments-module-hrs">5  sec </span>
                        </div>
                        <div id="comment-reply-append-58ee076706ea2a38473bc751" class="comment-append-reply" style="display:none;">
                           <!--all reply will append through Ajax-->
                        </div>
                        <!--<a id = "show-reply-comment-58ee076706ea2a38473bc751" class="comments-module-reply" onclick = "getCommentReplies('function', '', '3', '58ee076706ea2a38473bc751','1');">View 0 Replies</a>-->
                        <div id="comment-reply-div-58ee076706ea2a38473bc751" class="row comments-module-wrapper comment-reply-create" style="display : none;">
                           <div class="col-xs-1 wid-50">
                              <img class="reply-img" src="<?php echo $rootUrlImages?>rightpanel-img/Default_profile_image.PNG">
                           </div>
                        </div>
                        <div id="commentReplyLoadMoreButton58ee076706ea2a38473bc751" class="row comments-module-wrapper" style="display:none;">
                           <div class="col-xs-12">
                              <a id="loadMoreCommentReplyPostPage58ee076706ea2a38473bc751" class="comments-module-all-comments" onclick="">View <span id="remainingReplyLoadCount58ee076706ea2a38473bc751" class="comment-count"></span> more Replies
                              </a>
                           </div>
                        </div>
                     </div>
                     <div id="comment-edit-58ee076706ea2a38473bc751" class="col-xs-11 comment-editing comment-editing-each-post-194" style="display :none;">
                     </div>
                     <div id="comment-edit-display-action-58ee076706ea2a38473bc751" class="col-xs-1 comment-editing-action-post-194">
                        <a class="edit-comment" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-angle-down"></i></a>
                        <ul class="dropdown-menu pull-right" aria-labelledby="dLabel">
                           <li class="border-bottom clearfix">
                              <a data-toggle="modal" onclick="editDeleteComment('update', '58ee076706ea2a38473bc751', '194');"><i class="fa fa-pencil text-blue innerMR"></i>Edit</a>
                           </li>
                           <li>
                              <a data-toggle="modal" data-target="#deleteConfirmation" onclick="deleteConfirmation('194', '58ee076706ea2a38473bc751','comment','');"><i class="fa fa-trash text-blue innerMR"></i>Delete</a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div id="get-more-comment194" style="display:none;" remainingcount="0" commentloadclick="false" commenttotalcount="2"></div>
               </div>
                  <!--comments-module-wrapper -->
               <div id="commentLoadMoreButton194" class="row comments-module-wrapper" style="display:none;">
                  <div class="col-xs-12">
                     <a id="loadMoreCommentPostPage194" class="comments-module-all-comments" onclick="">View <span id="remainingLoadCount194" class="comment-count">0</span> more comments </a>
                  </div>
               </div>
            </div>
          </div>
        </div>
      </div>
      <!-- col-xs-6 -->

      <div class="col-xs-3">
        <div class="about-us-right-panel">
    		  <div class="card right-panel-modules">
    				<div class="bg-light-gray right-panel-modules-head">
    					<div class="row margin-none">
    						<div class="col-xs-12 recent-blog-heading">
    							<h4 class="margin-none half innerTB pull-left">Recent Blogs / Vlogs</h4>
                  <i class="pull-right fa fa-refresh half innerMT text-white"></i>
                  <i class="pull-right fa fa-calendar half innerR innerMT text-white"></i>
  							</div>
  						</div>
    				</div>
    				<div class="announcement-list">
              <div class="clearfix innerAll border-bottom">
                <div class="recent-blog pull-left">
                  <img src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
                </div>
                <div class="pull-left bolg-info">
                  <div class="padding-bottom-none text-blue blog-information">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor pellentesque justo acmolestie. Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin.
                  </div>
                  <div class="innerT half pull-right">
                    <span class="text-grayscale-80">21st Jan,2015</span>
                  </div>
                </div>
              </div>
              <div class="clearfix innerAll border-bottom">
                <div class="recent-blog pull-left">
                  <img src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
                </div>
                <div class="pull-left bolg-info">
                  <div class="padding-bottom-none text-blue blog-information">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor pellentesque justo acmolestie. Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin.
                  </div>
                  <div class="innerT half pull-right">
                    <span class="text-grayscale-80">21st Jan,2015</span>
                  </div>
                </div>
              </div>
              <div class="clearfix innerAll border-bottom">
                <div class="recent-blog pull-left">
                  <img src="<?php echo $rootUrlImages?>about-us-press-img.png"/>
                </div>
                <div class="pull-left bolg-info">
                  <div class="padding-bottom-none text-blue blog-information">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus auctor pellentesque justo acmolestie. Praesent dignissim orci a augue iaculis, nec scelerisque tellus sollicitudin.
                  </div>
                  <div class="innerT half pull-right">
                    <span class="text-grayscale-80">21st Jan,2015</span>
                  </div>
                </div>
              </div>
            </div>
    			</div>
          <!-- content consumption -->
        </div>
    		<!-- social-left-panel  -->
      </div>
      <!-- col-xs-3 -->


    </div>
  </div>
<!-- container -->
</main>

<?php
} else {
		header('location:'. $rootUrl .'../../views/prelogin/index.php');
}
?>


<?php include('../layout/social_footer.php'); ?>
