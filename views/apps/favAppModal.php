<?php
  require_once('../../config/config.php');
  require_once('../../config/db_config.php');
  require_once('../../helpers/errorMap.php');
  require_once('../../helpers/coreFunctions.php');
  require_once('../../models/app/appModel.php');

  error_reporting(0);

  $changeAppId = $_GET['changeAppId'];
  $action      = $_GET['action'];
  $favAppIds   = $_GET['favAppId'];
  $btnStatus   = $_GET['btnStatus'];

?>
    <!-- Modal -->
    <div class="modal-header border-none">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center text-deep-sky-blue inner-3x innerT" id="myModalLabel"> Add to favorite list</h4>
        <div class="apperror text-center"> &nbsp;
        </div>
    </div>
    <div class="modal-body innerAll">
        <div class="row">
            <div class="input-group innerLR">
                <input
                type="text"
                class="form-control landing-search-box app-search autoSuggest ui-autocomplete-input userFavAppIdInput"
                data-favappid="<?php echo $favAppIds; ?>" data-btnchange="<?php echo $btnStatus; ?>"
                data-action="<?php echo $action; ?>"
                data-changeappid="<?php echo $changeAppId; ?>" placeholder="Search App to Add in Favorite List"
                onkeypress="suggestList()"
                autocomplete="on"
                id="userFavAppId"
                name="userFavAppIdInput">
                <span class="input-group-btn">
                  <input type="button" id="favAppSubmitBtn" value="Add to Fav" class="bg-skyblue text-white landing-search-btn addFavApp innerLR  inner-3x" onclick="getAppIdByAppName('<?php echo $changeAppId; ?>', '<?php echo $action; ?>', '<?php echo $favAppIds; ?>', '<?php echo $btnStatus; ?>');">
                </span>
            </div>
            <div class="col-xs-12 innerT inner-2x">
                <h4 class="padding-none sugg text-social-primary f-sz16">Suggested App</h4></div>
            <?php
        $page         = "apps.php";
        $searchDbConn = createDBConnection("dbsearch");
        if (noError($searchDbConn)) {
          $searchDbConn = $searchDbConn["connection"];
        }
        $getSuggestedApps = getSuggestedApps($page, $favAppIds, $searchDbConn);

        if (noError($getSuggestedApps)) {
            $getSuggestedApps = $getSuggestedApps["errMsg"];
            $i = 0;
            foreach ($getSuggestedApps as $getSuggestedApp) {
        ?>
                <div class="col-xs-4 ">
                    <div class="product-template margin-none innerB">
                        <figure>
                            <img class="imagSize" src="<?php echo $rootUrl . $getSuggestedApp['app_logo'];?>">
                            <a href="#"><figcaption style="cursor: pointer;" id='addButton' onclick="favBtnAction('<?php echo $getSuggestedApp['app_id']; ?>', '<?php echo $action?>', '<?php echo $favAppIds; ?>', 'false', '<?php echo $changeAppId; ?>');"></figcaption></a>
                        </figure>
                    </div>
                </div>
                <?php
          if($i == 2) {
            break;
          }
          $i++;
        }
        } else {
          print('Error: fetching Suggested App info');
          exit;
        }
        ?>
        </div>
    </div>
    </div>
