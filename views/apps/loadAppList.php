<?php

		session_start();
		require_once('../../config/config.php');
		require_once('../../config/db_config.php');
		require_once('../../helpers/errorMap.php');
		require_once('../../helpers/coreFunctions.php');
		require_once('../../models/app/appModel.php');

		global $walletURLIP;

		error_reporting(0);

		$reloadId         = $_POST['reloadId'];
		$searchDbConn 		= createDBConnection("dbsearch");
		$login_status 		= '';
				if(noError($searchDbConn)){
					$searchDbConn = $searchDbConn["connection"];
					if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
								$email = $_SESSION["email"];
								$login_status=1;
								$required ="favourite_app_ids,default_search_appId";
								$getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$required);
								if(noError($getUserInfo)){
										$getUserInfo = $getUserInfo["errMsg"];
										$favouriteAppId = $getUserInfo["favourite_app_ids"];
										$default_app_id = $getUserInfo["default_search_appId"];
										$userFavAppId = explode(',', $favouriteAppId);
								}else{
									print('Error: fetching User Info');
									exit;
								}
								//Retriving All Apps Inforamtion
								$getAllApp = getAppInfo($searchDbConn);
								if(noError($getAllApp)){
								}
								else {
									print('Error: fetching App Info');
									exit;
								}
					}else{
						print("Error: Email ID NOT FOUND");
					}
				}else{
					print('Error: Unable to create search database connection');
					exit;
				}
		 ?>
    <?php
if($login_status == 1){
	if($reloadId == "appdefclass"){
?>
        <div class="" id="upadteApp">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="headh4 inner-2x innerMB"> Select App and Search </h4>
                </div>
            </div>
            <div class="row">
                <?php
        $getAllApp=$getAllApp['errMsg'];
          if (empty($getAllApp)) {
              echo "<tr><td colspan = '4' style = 'text-align:center;'>No Data Found</td></tr>";
          }
          foreach($getAllApp as $key => $getAllAppDetails) {
        ?>
                    <div class="col-xs-4">
                        <div class="product-template">
                            <figure>
                                <img class="imagSize" src="<?php echo $rootUrl; ?><?php echo $getAllAppDetails['app_logo']; ?>">
                                <figcaption>
                                    <div class="links">
                                        <div class="dailogM">
                                            <?php if($getAllAppDetails['app_id'] == $default_app_id){ ?>
                                            <a><button type="button"  class="defaultAppBtn" onclick="defaultBtnAction('<?php echo $getAllAppDetails['app_id']; ?>');"id="defaultAppBtn-<?php echo $default_app_id; ?>">Default</button></a>
                                            <div id="default-div" style="display:none">
                                                <?php echo $getAllAppDetails['app_id']; ?>
                                            </div>
                                            <?php }else{ ?>
                                            <button type="button" class="defaultBtn" onclick="defaultBtnAction('<?php echo $getAllAppDetails['app_id']; ?>');" id="defaultBtn-<?php echo $getAllAppDetails['app_id']; ?>">Set Default</button>
                                            <div id="set-default-div-<?php echo $getAllAppDetails['app_id']; ?>" style="display:none"></div>
                                            <?php 	} ?>
                                        </div>
                                        <div class="dailogM">
                                            <a onclick="ajaxAppInfo('<?php echo $getAllAppDetails['app_name']; ?>');" id="appRe">
                                                <button type="button" class="defaultBtn">App Info</button>
                                            </a>
                                        </div>
                                        <div class="dailogM">
                                            <a href="<?php echo $rootUrl."views/search/".$getAllAppDetails['app_url'];?>">
                                                <button type="button" class="defaultBtn">Go</button>
                                            </a>
                                        </div>
                                        <div class="dailogM" style="display:none">
                                            <form id="favAppForm_1" action="" method="post">
                                                <a class="favStarAnchor" onclick="addFavAppFunc(1)">
                                                    <button type="button" id="removeBtn1" class="defaultBtn">Remove</button>
                                                </a>
                                            </form>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <?php
                if (in_array($getAllAppDetails['app_id'], $userFavAppId))
                {	?>
                                <!-- <div class="appName text-center">
              <img src="<?php //echo $rootUrlImages?>apps/goldStar.png" class="goldStarClass" id="fav1">
              <?php //echo $getAllAppDetails['app_name'];?>
            </div> -->
                                <?php } else {
              ?>
                                <!-- <div class="appName text-center">
                <?php //echo $getAllAppDetails['app_name'];?>
              </div> -->
                                <?php
            }?>
                        </div>
                    </div>
                    <?php } ?>
            </div>
        </div>
        <?php } else if($reloadId == "appfavclass") { ?>
        <div class="" id="updateFavApp">
            <div class="row">
                <div class="col-xs-12">
                    <h4 class="headh4 inner-2x innerMB"> Select App Shortcut
					<div class="numberCount">
						[<span style="color:#000" class="half innerL">6 </span>
						<strong>/</strong>
						<?php
						if($favouriteAppId != ""){
							if(strpos($favouriteAppId, ',')){
								$favouriteAppId = explode(',', $favouriteAppId);
							}else{
								$favouriteAppId=[$favouriteAppId];
							}
						?>
						<span class="favCount half innerR"><?php echo count($favouriteAppId);?></span>
						]
					</div>
				</h4>
                </div>
            </div>

            <div class="row">

            <?php
				foreach($favouriteAppId as $favAppId) {
					$getFavAppDetails = getAppInfoById($favAppId, $searchDbConn);
					$getFavAppDetails = $getFavAppDetails['errMsg'][0];
		?>

                    <div class="col-xs-4 favApp">
                        <div class="product-template">
													<!-- <div><?php //echo $getFavAppDetails['app_logo']; ?></div> -->
                            <figure>
                                <img class="imagSize" src="<?php echo $rootUrl . $getFavAppDetails['app_logo'];?>">
                                <figcaption>
                                    <div class="links">
                                        <div class="dailogM">
                                            <a data-toggle="modal"><button type="button" class="defaultBtn" onclick="favActionModal('<?php echo $favAppId; ?>', 'change', '<?php echo $getUserInfo["favourite_app_ids"]; ?>', 'false');">Change</button></a>
                                        </div>
                                        <div class="dailogM">
                                            <a><button type="button" class="defaultBtn" onclick="favBtnAction('<?php echo $favAppId; ?>', 'remove', '<?php echo $getUserInfo["favourite_app_ids"]; ?>', 'false', '<?php echo $changeAppId = ''; ?>');">Remove</button></a>
                                        </div>
                                        <div class="dailogM">
                                            <a href="<?php echo $rootUrl."views/search/".$getFavAppDetails['app_url'];?>">
                                                <button id="btnGo" type="button" class="defaultBtn">Go</button>
                                            </a>
                                        </div>
                                    </div>
                                </figcaption>
                            </figure>
                            <!--<div class="appName text-center" style="font-weight: 400;"><?php //echo $getFavAppDetails['app_name']; ?>
			</div>-->
                        </div>
                    </div>
                    <?php } ?>
                    <div class="col-xs-4" id="hideFav">
                        <div class="product-template2 bg-white" itemscope="">
                            <figure style="box-shadow:none;">
                                <a data-toggle="modal" onclick="favActionModal('<?php echo $changeAppId = ''; ?>', 'add', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'false');">
						<!--<img style="" src="<?php echo $rootUrlImages?>apps/add.gif">-->
						<i class="fa fa-plus"></i>
					</a>
                            </figure>
                            <div class="appName text-center" style="font-weight: 400;">Add To Favourite</div>
                        </div>
                    </div>
                </div>
                <?php } else { ?>
                <span class="favCount half innerR"><?php echo 0;?></span>]</div>
        </div>

        <div class="row">
            <div class="col-xs-4 " id="hideFav" style="margin-top:20px">
                <div class="product-template2 bg-white" itemscope="">
                    <figure style="box-shadow:none;">
                        <a data-toggle="modal" onclick="favActionModal('<?php echo $changeAppId = ''; ?>', 'add', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'false');">
							<!--<img style="" src="<?php //echo $rootUrlImages?>apps/add.gif">-->
							<i class="fa fa-plus"></i>
						</a>
                    </figure>
                    <div class="appName text-center" style="font-weight: 400;">Add To Favourite</div>
                </div>
            </div>
        </div>
        <?php } ?>
        </div>
        <!-- Modal -->
        <div class="modal fade bs-example-modal-lg row-10" id="favAppsModal" tabindex="-1" role="dialog" aria-labelledby="favAppsModal">
            <div class="modal-dialog" role="document" style="width:490px">
                <div class="modal-content">
                </div>
            </div>
        </div>
        <?php }} else {
		header('location:'.$rootUrl.'views/prelogin/index.php');
} ?>
<script>
//js to load all app on fav app modal
$('.modal-content').on('keypress', '#userFavAppId', function(e) {
    $('#userFavAppId').autocomplete({
        source: '../../controllers/app/searchController.php'
    });
    if (e.which == 13) {
        var changeAppID = $(this).data("changeappid");
        changeAppID = changeAppID.toString();
        getAppIdByAppName(changeAppID, $(this).data('action'), $(this).data("favappid"), $(this).data("btnchange"));
    }
});
changeColorplus();
</script>
