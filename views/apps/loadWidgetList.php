

  <div class="row">
      <div class="col-xs-12">
        <h4 class="headh4 text-social-primary inner-2x innerMB"> Select Widget and Search</h4>
      </div>
    </div>
          <div class="row">
            <div class="col-xs-4">
              <div class="product-template3">
                <figure>
                  <img class="imagSize2 img-responsive" src="../../images/Currency_Convereter_tp.png" />
                  <figcaption>
                    <div class="links">
                      <div class="dailogM">
                        <form action="../widgets/currency_converter/search_result_currency.php" role="search" >
                          <input type="hidden" name="q" value="currency:" />
                          <a class="">
                            <button  type="submit" class="st-wid-but" id="widgets_currency_converter">
                              GO
                            </button>
                          </a>
                        </form>
                      </div>
                    </div>
                  </figcaption>
                </figure>
                <!-- <div class="appName text-center">Currency Converter</div> -->
              </div>
            </div>

            <div class="col-xs-4">
            <div class="product-template3"  >
              <figure>
                <img class="imagSize2 img-responsive" src="../../images/TimeZone_Converter_tp.png" />
                </form>
                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/timezone_converter/search_result_timezone.php" role="search" >
                        <input type="hidden" name="q" value="12 gmt to ist" />
                        <a>
                          <button  type="submit" class="st-wid-but" id="widgets_timezone_converter">
                            GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">TimeZone Converter</div> -->
            </div>
          </div>

          <div class="col-xs-4">
            <div class="product-template3">
              <figure>
                <img class="imagSize2 img-responsive" src="../../images/Unit_Converter-tp.png" />
                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/unit_converter/search_result_unit.php" role="search" >
                        <input type="hidden" name="q" value="length:" />
                        <a>
                          <button  type="submit" class="st-wid-but" id="widgets_unit_converter">
                            GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">Unit Converter</div> -->
            </div>
          </div>
          <div class="col-xs-4">
            <div class="product-template3"  >
              <figure>
                <img class="imagSize2 img-responsive" src="../../images/Calculator.png" />
                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/calculator/search_result_calci.php" role="search" >
                        <input type="hidden" name="q" value="calculator:" />
                        <a>
                          <button  type="submit" class="st-wid-but" id="widgets_calculator">
                              GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">Calculator</div> -->
            </div>
          </div>


          <div class="col-xs-4">
            <div class="product-template3"  >
              <figure>
                <img class="imagSize2 img-responsive"  src="../../images/Waether_Forcast_tp.png" />
                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/weather_forecast/search_result_weather.php" role="search" >
                        <input type="hidden" name="q" value="weather:" />
                        <a>
                          <button  type="submit" class="st-wid-but" id="widgets_weather_forecast">
                            GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">Weather Forecast</div> -->
            </div>
          </div>


          <div class="col-xs-4">
            <div class="product-template3"  >
              <figure>
                <img class="imagSize2 img-responsive" src="../../images/Language _Translator_tp.png" />
                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/language_translator/search_result_translator.php" role="search" >
                        <input type="hidden" name="q" value="translator:" />
                        <a>
                          <button  type="submit" class="st-wid-but" id="widgets_language_translator">
                            GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">Language Translator</div> -->
            </div>
          </div>


          <div class="col-xs-4">
            <div class="product-template3"  >
              <figure>
                <img class="imagSize2 img-responsive" src="../../images/Currency.png" />

                <figcaption>
                  <div class="links">
                    <div class="dailogM">
                      <form action="../widgets/crypto_currency/search_result_crypto.php" role="search" >
                        <input type="hidden" name="q" value="coins:" />
                        <a>
                          <button type="submit" class="st-wid-but" id="widgets_crypto_currency">
                            GO
                          </button>
                        </a>
                      </form>
                    </div>
                  </div>
                </figcaption>
              </figure>
              <!-- <div class="appName text-center">Crypto Currency</div> -->
            </div>
          </div>
          </div>
          <!--row-->

<!-- =========  widgets ========= -->
