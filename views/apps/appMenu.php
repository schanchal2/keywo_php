<?php
	// Include require files
  require_once('../../config/config.php');
  require_once('../../config/db_config.php');
  require_once('../../helpers/coreFunctions.php');
  require_once('../../helpers/errorMap.php');
  require_once('../../helpers/arrayHelper.php');
  require_once('../../models/search/searchResultModel.php');
  require_once('../../helpers/stringHelper.php');

  error_reporting(0);

	$email 						 = cleanXSS($_GET['email']);
  $user_id           = "user_id";
  $getUserFavAppIds  = getUserInfo($email, $walletURLIPnotification . 'api/notify/v2/', 'favourite_app_ids');

/*set database connection*/
  $searchDbConn = createDBConnection('dbsearch');
    if (noError($searchDbConn)) {
    	$searchDbConn = $searchDbConn["connection"];
    	$msg          = "Success : Database connection.";
    	$getFavApps   = getAllFavApps($getUserFavAppIds['errMsg']['favourite_app_ids'], $searchDbConn);
			$countFavApps = count($getFavApps['data']);
			if (noError($getFavApps)) {
				$getFavApps = $getFavApps["data"];
			} else {
				$msg = "Error: Select your favorite apps";
			}
    } else {
    	$msg = "Failure : Database connection.";
    }
?>

<!--get app menu response in html-->

    <ul class="nav navbar-nav" id="app-marketplace-appmenu">
      <?php
  			if ($countFavApps != 0) {
  				foreach ($getFavApps as $key => $value) {
  		?>
  		<li class="text-capitalize">
  			<a href="<?php echo $rootUrl."views/search/".$value['app_url']; ?>">
  				<img class="static-menu-icons" src="<?php echo $rootUrl.$value['app_logo'];?>">
  				<span><?php echo $value["app_name"]; ?></span>
  			</a>
  		</li>
  		<?php
  				}
  			} else {
  		?>
      <li class="active default-blue manageAppsLi">
        <img class="manageApps" src="<?php echo $rootUrlImages."mangFav.png"?>" class="pull-left innerR">
        <div class="manageAppsText text-capitalize">Manage Favourites</div>
      </li>
      <?php }?>
      <li class="text-capitalize">
        <a href="<?php echo $rootUrl; ?>views/apps/apps.php" class="">
          <img class="static-menu-icons" src="<?php echo $rootUrlImages?>arrowRight.png" />
          <span>Go to App Market</span>
        </a>
      </li>
    </ul>
