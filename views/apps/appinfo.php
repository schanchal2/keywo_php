<?php
		session_start();
		require_once('../../config/config.php');
		require_once('../../config/db_config.php');
		require_once('../../helpers/errorMap.php');
		require_once('../../helpers/coreFunctions.php');
		require_once ("../../models/app/appModel.php");
		error_reporting(0);
		global $walletURLIP;
    if(isset($_POST['appInfoAjaxPageid']))
    {
      $app_name = $_POST['appInfoAjaxPageid'];
    }
    $rootUrlImages1 = "{$rootUrl}/";
		$searchDbConn = createDBConnection("dbsearch");
		if(noError($searchDbConn)){
			$searchDbConn = $searchDbConn["connection"];
			if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
					//from app name find id of that app
					if(isset($app_name) && (!empty($app_name)))
					{
						$getAppData = getAppIDbyName($app_name,$searchDbConn);
						$appIdAjax = $getAppData["errMsg"][0]["app_id"];
						//id will get details database
						if(isset($appIdAjax))
						{
						$appdata=getAppInfoById($appIdAjax,$searchDbConn);
						$appdata=$appdata["errMsg"][0];
						}
					}
					else{
					print("Error: Fetching Selected App Information");
					exit;
					}

				//Find statics of App Details
				$page = "apps.php";
				$all_app_details = getAllApps( $page,$searchDbConn);
				if(noError($all_app_details)){
					$all_app_details = $all_app_details["errMsg"];
					$totalNetworkSearches = 0;
					$totalNetworkKeywords = 0;
					$totalNetworkEarnings = 0;
					foreach($all_app_details as $key => $value ){
					$totalNetworkEarnings += $value['totalAppEarning'];
					$totalNetworkSearches += $value['totalAnonymousSearch'] + $value['totalQualifiedSearches'] + $value['totalUnqualifiedSearches'] ;
					$totalNetworkKeywords += $value["totalKeywordSearched"];
					}
				}else{
				print("Error: Fetching App Statistics Inforamtion");
				exit;
				}

			//Find Favourate App
			$email = $_SESSION["email"];
			$login_status=1;
			$a="favourite_app_ids,default_search_appId";
			$getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$a);
			if(noError($getUserInfo)){
				$getUserInfo = $getUserInfo["errMsg"];
				 $favouriteAppId = $getUserInfo["favourite_app_ids"];
				$default_app_id = $getUserInfo["default_search_appId"];
				$favouriteAppIds = explode(',', $favouriteAppId);
				if(empty($favouriteAppId)){
					$finalVariableForFavApps = $appIdAjax;
				}
				else {
				$finalVariableForFavApps= $favouriteAppId.",".$appIdAjax;
				}
				if(in_array($appdata['app_id'], $favouriteAppIds, true))
				{
				$buttonStatus = 'Remove';
				}else{
				$buttonStatus = 'Add To Fav';
				}
			}
			else {
			print('Error: fetching App Information');
			exit;
			}

			$page="apps.php";
			$getSuggestedApp = getSuggestedApps($page,$finalVariableForFavApps,$searchDbConn);
			if(noError($getSuggestedApp)){
			$getSuggestedApp = $getSuggestedApp["errMsg"];
			}
			else {
			print('Error: fetching Suggested App info');
			exit;		}
			}	else{
				print("Error: Email ID NOT FOUND");
			}
		}else{
			print('Error: Unable to create search database connection');
			exit;
		}
    ?>
    <?php
		if($login_status == 1){
		?>
        <div class="">
            <div class="row" style="margin-top: 12px;">

                <div class="col-xs-9">
                    <div class="apperror"></div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="bg-white innerLR innerB innerMB">
                                <div class="row">
                                    <h4 class="text-social-primary margin-none innerT col"><?php echo $appdata['app_name'];?></h4>
                                </div>
                                <div class="">
                                    <div class="product-template innerT">
                                        <div class="app-info-l-image">
                                            <img class="img-responsive" src="<?php echo $rootUrlImages1; ?><?php echo $appdata['app_info_image']?>" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row ">
                                    <div class="col-xs-9">
                                        <p>
                                            <?php echo $appdata['app_description']?>
                                        </p>
                                    </div>
                                    <div class="col-xs-3 app-info text-right">
                                        <?php if($appdata['app_id'] == $default_app_id){ ?>
                                        <button type="button" class="btn-trading innerMB setButton bg-color-Red">Default</button>
                                        <div id="default-div" style="display:none">
                                            <?php echo $appdata['app_id']; ?>
                                        </div>
                                        <?php } else {?>
                                        <button type="button" class="btn-trading innerMB setButton" style="min-width:100px;" onclick="defaultBtnAction('<?php echo $appdata['app_id']; ?>');" id="defaultBtn-<?php echo $appdata['app_id']; ?>">Set Default</button>
                                        <div id="set-default-div-<?php echo $appdata['app_id']; ?>" style="display:none"></div>
                                        <?php  } ?>
                                        <form id="favAppForm_<?php echo $appdata['app_id']; ?>" action="" method="post">
                                            <a class="favStarAnchor" href="#">
                                                <?php
                                                if($buttonStatus == "Remove") { ?>
                                                    <button type="button" id="removeBtn<?php echo $appdata['app_id']; ?>" onclick="favBtnAction('<?php echo $appdata['app_id']; ?>', 'remove', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'true');" class="btn-trading-wid-auto   innerMB changeText" style="min-width:100px">
                                                        <?php echo $buttonStatus; ?>
                                                    </button>
                                                    <?php }else{ ?>
                                                    <button type="button" id="removeBtn<?php echo $appdata['app_id']; ?>" onclick="favBtnAction('<?php echo $appdata['app_id']; ?>', 'add', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'true');" class="btn-trading-wid-auto   innerMB changeText" style="min-width:100px">
                                                        <?php echo $buttonStatus; ?>
                                                    </button>
                                                    <?php } ?>
                                            </a>
                                        </form>
                                        <a href="<?php echo $rootUrl." views/search/ ".$appdata['app_url'];?>" class="btn-app btn-trading-wid-auto-dark   innerMB" style="min-width:100px;">Go </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row  innerTB text-center keyword-information-search">
                                <div class="col-xs-4">
                                    <div class="keyword-info">
                                        <div class="keyword-info-num bg-blue-key-dark padng-analytic-blocks">
                                            <?php echo $appdata['totalKeywordSearched'];?>
                                        </div>
                                        <div class="keyword-num-status light-blue-col">
                                            <h3 class="margin-none">Keyword Searched</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <?php 
			                            if ($totalNetworkKeywords != 0) {
											$keywordsPerc = number_format(($appdata["totalKeywordSearched"]/$totalNetworkKeywords)*100, 2);
										} else{
											$keywordsPerc=0;
										} 
									?>
                                    <div class="keyword-info">
                                        <div class="keyword-info-num bg-blue-key">
                                            <?php echo $keywordsPerc; ?>%
                                        </div>
                                        <div class="keyword-num-status">
                                            <h3 class="margin-none">Network Keyword</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <div class="keyword-info">
                                        <div class="keyword-info-num bg-blue-key-dark padng-analytic-blocks">
                                            <?php echo $appdata['totalAnonymousSearch'] ?>
                                        </div>
                                        <div class="keyword-num-status">
                                            <h3 class="margin-none">Searches Performed</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row innerTB text-center keyword-information-search">
                                <div class="col-xs-offset-2 col-xs-4">
                                    <?php
	                                    if ($totalNetworkSearches != 0) {
											$networkSearchesPerc = number_format((($appdata['totalAnonymousSearch'] 
												+ $appdata['totalQualifiedSearches'] 
												+ $appdata['totalUnqualifiedSearches'])/$totalNetworkSearches)*100, 2);
										} else {
											$networkSearchesPerc=0;
										}
									?>
                                        <div class="keyword-info">
                                            <div class="keyword-info-num bg-blue-key-dark padng-analytic-blocks">
                                                <?php echo $networkSearchesPerc; ?>%
                                            </div>
                                            <div class="keyword-num-status">
                                                <h3 class="margin-none">Network searches</h3>
                                            </div>
                                        </div>
                                </div>
                                <div class="col-xs-4">
                                    <?php 
                                    	if ($totalNetworkEarnings != 0) {
											$totalAppEarningsPerc = number_format(($appdata["totalAppEarning"]/$totalNetworkEarnings)*100, 2);
										} else {
											$totalAppEarningsPerc = 0;
										}
									?>
                                    <div class="keyword-info">
                                        <div class="keyword-info-num bg-blue-key">
                                            <?php echo $totalAppEarningsPerc; ?>%
                                        </div>
                                        <div class="keyword-num-status">
                                            <h3 class="margin-none">Total App Earnings</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- col-xs-8 -->
                <div class="col-xs-3" id="menu-apps">
                    <h4 class="text-center text-social-primary margin-top-none innerB">Suggested App</h4>
                    <div class="row product-template">
                        <div class="col-xs-12">
                            <?php foreach($getSuggestedApp as $key => $getSuggestedAppDetails)
		{  	$i++;?>
                            <div class="inner-2x innerMB">
                                <figure>
                                    <img class="imagSize" src="<?php echo $rootUrlImages1?><?php echo $getSuggestedAppDetails['app_logo']?>">
                                    <figcaption>
                                        <div class="links" style="margin:0px;">
                                            <div class="dailogM">
                                                <?php if($getSuggestedAppDetails['app_id'] == $default_app_id){ ?>
                                                <a href="javascript:;"><button type="button"  class="defaultAppBtn" onclick="defaultBtnAction('<?php echo $getSuggestedAppDetails['app_id']; ?>');"id="defaultAppBtn-<?php echo $default_app_id; ?>">Default</button></a>
                                                <div id="default-div" style="display:none">
                                                    <?php echo $getSuggestedAppDetails['app_id']; ?>
                                                </div>
                                                <?php }else{ ?>
                                                <button type="button" class="defaultBtn" onclick="defaultBtnAction('<?php echo $getSuggestedAppDetails['app_id']; ?>');" id="defaultBtn-<?php echo $getSuggestedAppDetails['app_id']; ?>">Set Default</button>
                                                <div id="set-default-div-<?php echo $getSuggestedAppDetails['app_id']; ?>" style="display:none"></div>
                                                <?php 	} ?>
                                            </div>
                                            <div class="dailogM">
                                                <a href="javascript:;" onclick="ajaxAppInfo('<?php echo $getSuggestedAppDetails['app_name']; ?>');" id="appRe">
                                                    <button type="button" class="defaultBtn">App Info</button>
                                                </a>
                                            </div>
                                            <div class="dailogM">
                                                <a href="<?php echo $rootUrl." views/search/ ".$getSuggestedAppDetails['app_url'];?>">
                                                    <button type="button" class="defaultBtn">Go</button>
                                                </a>
                                            </div>
                                            <div class="dailogM" style="display:none">
                                                <form id="favAppForm_1" action="" method="post">
                                                    <a class="favStarAnchor" href="#" onclick="addFavAppFunc(1)">
                                                        <button type="button" id="removeBtn1" class="defaultBtn">Remove</button>
                                                    </a>
                                                </form>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                                <!-- <div class="appName text-center">
												<?php //echo $getSuggestedAppDetails['app_name']; ?>
											</div> -->
                            </div>
                            <?php
											if($i == 4){
												break;
											}
									} ?>
                        </div>
                        <!--col-xs-12-->
                    </div>
                </div>
                <!-- col-xs-4 -->
            </div>
            <!-- row -->
        </div>
        <!-- container -->
        <?php
}else{
		header('location:'.$rootUrl.'views/prelogin/index.php');
}

?>
