<?php
session_start();
require_once('../../config/config.php');
require_once('../../config/db_config.php');
require_once('../../helpers/errorMap.php');
require_once('../../helpers/coreFunctions.php');
require_once('../../models/app/appModel.php');
global $walletURLIP;
error_reporting(0);
        $searchDbConn = createDBConnection("dbsearch");
        $login_status = '';
        if(noError($searchDbConn)){
            $searchDbConn = $searchDbConn["connection"];
            if(isset($_SESSION["email"]) && !empty($_SESSION["email"])){
                        $email = $_SESSION["email"];
                        $login_status=1;
                        $required ="favourite_app_ids,default_search_appId";
                        $getUserInfo = getUserInfo($email,$walletURLIPnotification.'api/notify/v2/',$required); //print_r($getUserInfo); die;
                        if(noError($getUserInfo)){
                                $getUserInfo = $getUserInfo["errMsg"];
                                $favouriteAppId = $getUserInfo["favourite_app_ids"];
                                $default_app_id = $getUserInfo["default_search_appId"];
                                $userFavAppId = explode(',', $favouriteAppId);
                        }else{
                            print('Error: fetching User Info');
                            exit;
                        }
                        //Retriving All Apps Inforamtion
                        $getAllApp = getAppInfo($searchDbConn);
                        if(noError($getAllApp)){
                        }
                        else {
                            print('Error: fetching App Info');
                            exit;
                        }
            }else{
                print("Error: Email ID NOT FOUND");
            }
        }else{
            print('Error: Unable to create search database connection');
            exit;
        }
 ?>
    <?php
if($login_status == 1){
    include '../layout/header.php';
?>
        <main style="padding-top:60px;">
            <div class="container row-10" id="menu-apps">
                <div class="row">
                    <div class="col-xs-3">
                        <div id="menu-apps-nav">
                            <div class="navbar-default sidebar search-side-bar" role="navigation">
                                <div class="sidebar-nav navbar-collapse padding-none">
                                    <div id="side-menu" class="">
                                        <div class="sidebar-search ">
                                            <p id="errorSearch" class="text-danger" style="display: none">Please check the spelling or try different keywords </p>
                                            <p id="errorBlank" class="text-danger" style="display: none"> Please fill out this field </p>
                                            <form id="appSearchForm">
                                                <div class="input-group">
                                                    <input id="appSuggest" name="appSuggest" class="app-search" value="" placeholder="Search" style="outline:none">
                                                    <span class="input-group-btn">
                                                        <button  id="submitApp" class="btn btn-default innerR text-social-primary search-side-bar_button-submit" type="submit" ><i class="fa fa-search"></i></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card socia-card innerMT">
                                        <ul class="nav in">
                                            <li style="display:none" id="replaceAppInfo">
                                                <a data-toggle="tab" href="#appInfo"> appInfo</a>
                                            </li>
                                            <li id="appdefclass" class="border-bottom active" class="border-bottom">
                                                <a class="head-icon" data-toggle="tab" href="#app">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="25px" height="25px" version="1.0" viewBox="0 0 581610 581610" style="clip-rule:evenodd;fill-rule:evenodd;image-rendering:optimizeQuality;shape-rendering:geometricPrecision;text-rendering:geometricPrecision">
                                                        <g id="Layer_x0020_1">
                                                            <metadata id="CorelCorpID_0Corel-Layer"></metadata>
                                                            <g id="_1809516960352">
                                                                <g id="apps">
                                                                    <path class="fil0" d="M0 145394l145394 0 0-145394-145394 0 0 145394zm218108 436216l145394 0 0-145394-145394 0 0 145394zm-218108 0l145394 0 0-145394-145394 0 0 145394zm0-218108l145394 0 0-145394-145394 0 0 145394zm218108 0l145394 0 0-145394-145394 0 0 145394zm218108-363502l0 145394 145394 0 0-145394-145394 0zm-218108 145394l145394 0 0-145394-145394 0 0 145394zm218108 218108l145394 0 0-145394-145394 0 0 145394zm0 218108l145394 0 0-145394-145394 0 0 145394z"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    Apps
                                                </a>
                                            </li>
                                            <li id="appclass" class="border-bottom">
                                                <a class="head-icon" data-toggle="tab" href="#widget">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xml:space="preserve" width="25px" height="25px" version="1.0" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 109761 121672" xmlns:xlink="http://www.w3.org/1999/xlink">
                                                        <g id="Layer_x0020_1">
                                                            <metadata id="CorelCorpID_0Corel-Layer" />
                                                            <g id="_1963891350400">
                                                                <path class="fil0" d="M17028 0l75709 0c9363,0 17024,7662 17024,17025l0 54717c0,9363 -7661,17025 -17024,17025l-75709 0c-9363,0 -17028,-7662 -17028,-17025l0 -54717c0,-9363 7665,-17025 17028,-17025zm75709 14283l-75709 0c-739,0 -1423,314 -1925,817 -502,505 -817,1186 -817,1925l0 54717c0,739 315,1419 817,1925 502,502 1186,817 1925,817l75709 0c739,0 1422,-315 1925,-817 502,-506 817,-1186 817,-1925l0 -54717c0,-739 -315,-1420 -817,-1925 -503,-503 -1186,-817 -1925,-817z" />
                                                                <path class="fil0" d="M54882 121672l9039 -10984 9039 -10986 -18078 0 -18078 0 9039 10986 9039 10984z" />
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    Widgets
                                                </a>
                                            </li>
                                            <li id="appfavclass" class="border-bottom">
                                                <a class="head-icon" data-toggle="tab" href="#favorite">
                                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" width="25px" height="25px" version="1.0" viewBox="0 0 141759 131847" style="clip-rule:evenodd;fill-rule:evenodd;image-rendering:optimizeQuality;shape-rendering:geometricPrecision;text-rendering:geometricPrecision">
                                                        <g id="Layer_x0020_1">
                                                            <metadata id="CorelCorpID_0Corel-Layer"></metadata>
                                                            <g id="_1963893275184">
                                                                <g>
                                                                    <path class="fil0" d="M31325 83771l-6554 40463c-1005 6207 2863 9225 8639 6739l35427-15240 35461 15168c5780 2474 9430-648 8149-6805l-1092-5379-2236 0-4101 0 0-3821 0-10720-10896 0-4411 0 0-4278 0-17328 0-4416 4411 0 10896 0 0-10581 0-3964 4101 0 17028 0 8501-7831c4641-4240 3386-8623-2796-9744l-38722-7045-18969-34845c-3005-5521-7873-5521-10866 8l-18902 34871-38705 7103c-6186 1134-7534 5596-3009 9966l28646 27679z"></path>
                                                                    <path class="fil0" d="M126448 67435l-4784 0-12821 0 0 11598 0 3713-3595 0-10946 0 0 16839 13080 0 1461 0 0 8664 0 6642 1662 0 15943 0 0-15306 15311 0 0-16839-15311 0 0-15311z"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                    Favorites
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- menu -->
                    </div>
                    <!--col-xs-3-->
                    <div class="col-xs-9">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade active in" id="app">
                                <div class="" id="upadteApp">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="headh4 text-social-primary inner-2x innerMB"> Select App an Search</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        $getAllApp=$getAllApp['errMsg'];
                                            if (empty($getAllApp)) {
                                                    echo "<tr><td colspan = '4' style = 'text-align:center;'>No Data Found</td></tr>";
                                            }
                                            foreach($getAllApp as $key => $getAllAppDetails) {
                                        ?>
                                            <div class="col-xs-4">
                                                <div class="product-template">
                                                    <figure>
                                                        <img class="imagSize" src="<?php echo $rootUrl; ?><?php echo $getAllAppDetails['app_logo']; ?>">
                                                        <figcaption>
                                                            <div class="links">
                                                                <div class="dailogM">
                                                                    <?php if($getAllAppDetails['app_id'] == $default_app_id){ ?>
                                                                    <a href=""><button type="button"  class="defaultAppBtn" onclick="defaultBtnAction('<?php echo $getAllAppDetails['app_id']; ?>');"id="defaultAppBtn-<?php echo $default_app_id; ?>">Default</button></a>
                                                                    <div id="default-div" style="display:none">
                                                                        <?php echo $getAllAppDetails['app_id']; ?>
                                                                    </div>
                                                                    <?php }else{ ?>
                                                                    <button type="button" class="defaultBtn" onclick="defaultBtnAction('<?php echo $getAllAppDetails['app_id']; ?>');" id="defaultBtn-<?php echo $getAllAppDetails['app_id']; ?>">Set Default</button>
                                                                    <div id="set-default-div-<?php echo $getAllAppDetails['app_id']; ?>" style="display:none"></div>
                                                                    <?php   } ?>
                                                                </div>
                                                                <div class="dailogM">
                                                                    <a href="" onclick="ajaxAppInfo('<?php echo $getAllAppDetails['app_name']; ?>');" id="appRe">
                                                                        <button type="button" class="defaultBtn">App Info</button>
                                                                    </a>
                                                                </div>
                                                                <div class="dailogM">
                                                                    <a href="<?php echo $rootUrl.'views/search/'.$getAllAppDetails['app_url'];?>">
                                                                        <button type="button" class="defaultBtn">Go</button>
                                                                    </a>
                                                                </div>
                                                                <div class="dailogM" style="display:none">
                                                                    <form id="favAppForm_1" action="" method="post">
                                                                        <a class="favStarAnchor" href="#" onclick="addFavAppFunc(1)">
                                                                            <button type="button" id="removeBtn1" class="defaultBtn">Remove</button>
                                                                        </a>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                    <?php
                                                        if (in_array($getAllAppDetails['app_id'], $userFavAppId))
                                                        {   ?>
                                                        <!--<div class="appName text-center">
                                                    <img src="<?php //echo $rootUrlImages?>apps/goldStar.png" class="goldStarClass" id="fav1">
                                                    <?php //echo $getAllAppDetails['app_name'];?>
                                                </div>-->
                                                        <?php } else {
                                                    ?>
                                                        <!--<div class="appName text-center">
                                                        <?php //echo $getAllAppDetails['app_name'];?>
                                                    </div>-->
                                                        <?php
                                                }?>
                                                </div>
                                            </div>
                                            <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <!-- =========  app ========= -->
                            <!-- =========  Widgets ========= -->
                            <div role="tabpanel" class="tab-pane fade" id="widget">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <h4 class="headh4 text-social-primary inner-2x innerMB"> Select Widget and Search</h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Currency_Convereter_tp.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/currency_converter/search_result_currency.php" role="search">
                                                                <input type="hidden" name="q" value="currency:" />
                                                                <a class="">
                                                                    <button type="submit" class="st-wid-but" id="widgets_currency_converter">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Currency Converter</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/TimeZone_Converter_tp.png" />
                                                </form>
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/timezone_converter/search_result_timezone.php" role="search">
                                                                <input type="hidden" name="q" value="12 gmt to ist" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_timezone_converter">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">TimeZone Converter</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Unit_Converter-tp.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/unit_converter/search_result_unit.php" role="search">
                                                                <input type="hidden" name="q" value="length:" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_unit_converter">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Unit Converter</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Calculator.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/calculator/search_result_calci.php" role="search">
                                                                <input type="hidden" name="q" value="calculator:" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_calculator">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Calculator</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Waether_Forcast_tp.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/weather_forecast/search_result_weather.php" role="search">
                                                                <input type="hidden" name="q" value="weather:" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_weather_forecast">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Weather Forecast</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Language _Translator_tp.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/language_translator/search_result_translator.php" role="search">
                                                                <input type="hidden" name="q" value="translator:" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_language_translator">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Language Translator</div> -->
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="product-template3">
                                            <figure>
                                                <img class="imagSize2 img-responsive" src="../../images/Currency.png" />
                                                <figcaption>
                                                    <div class="links">
                                                        <div class="dailogM">
                                                            <form action="../widgets/crypto_currency/search_result_crypto.php" role="search">
                                                                <input type="hidden" name="q" value="coins:" />
                                                                <a>
                                                                    <button type="submit" class="st-wid-but" id="widgets_crypto_currency">
                                                                        GO
                                                                    </button>
                                                                </a>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </figcaption>
                                            </figure>
                                            <!-- <div class="appName text-center">Crypto Currency</div> -->
                                        </div>
                                    </div>
                                </div>
                                <!--row-->
                            </div>
                            <!-- =========  widgets ========= -->
                            <!-- =========  Favourites ========= -->
                            <div role="tabpanel" class="tab-pane fade" id="favorite">
                                <div class="" id="updateFavApp">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <h4 class="headh4"> Select App Shortcut
                                        <div class="numberCount">
                                            [ <span style="color:#000">6 </span>
                                            <strong>/</strong>
                                            <?php
                                            if($favouriteAppId != ""){
                                                if(strpos($favouriteAppId, ',')){
                                                    $favouriteAppId = explode(',', $favouriteAppId);
                                                }else{
                                                    $favouriteAppId=[$favouriteAppId];
                                                }
                                            ?>
                                            <span class="favCount half innerR"><?php echo count($favouriteAppId);?></span> ]</div>
                                        </h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php
                                        foreach($favouriteAppId as $favAppId) {
                                            $getFavAppDetails = getAppInfoById($favAppId, $searchDbConn);
                                            $getFavAppDetails = $getFavAppDetails['errMsg'][0];
                                ?>
                                            <div class="col-xs-3 favApp">
                                                <div class="product-template">
                                                    <figure>
                                                        <img class="imagSize" src="<?php echo $rootUrl . $getFavAppDetails['app_logo'];?>">
                                                        <figcaption>
                                                            <div class="links">
                                                                <div class="dailogM">
                                                                    <a href="" data-toggle="modal"><button type="button" class="defaultBtn" onclick="favActionModal('<?php echo $favAppId; ?>', 'change', '<?php echo $getUserInfo["favourite_app_ids"]; ?>', 'false');">Change</button></a>
                                                                </div>
                                                                <div class="dailogM">
                                                                    <a href=""><button type="button" class="defaultBtn" onclick="favBtnAction('<?php echo $favAppId; ?>', 'remove', '<?php echo $getUserInfo["favourite_app_ids"]; ?>', 'false', '<?php echo $changeAppId = ''; ?>');">Remove</button></a>
                                                                </div>
                                                                <div class="dailogM">
                                                                    <a href="<?php echo $rootUrl.'views/search/'.$getFavAppDetails['app_url'];?>">
                                                                        <button id="btnGo" type="button" class="defaultBtn">Go dfgfgd</button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </figcaption>
                                                    </figure>
                                                    <!--
                                        <div class="appName text-center" style="font-weight: 400;">
                                        <?php //echo $getFavAppDetails['app_name']; ?></div>-->
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <div class="col-xs-3" id="hideFav">
                                                <div class="product-template2" itemscope="">
                                                    <figure style="box-shadow:none;">
                                                        <a data-toggle="modal" onclick="favActionModal('<?php echo $changeAppId = ''; ?>', 'add', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'false');">
                                                <!--<img style="" src="<?php// echo $rootUrlImages?>apps/add.gif">-->
                                                <i class="fa fa-plus"></i>
                                            </a>
                                                    </figure>
                                                    <div class="appName text-center" style="font-weight: 400;">Add To Favourite</div>
                                                </div>
                                            </div>
                                            <?php } else { ?>
                                            <span class="favCount half innerR"><?php echo 0;?></span>]</div>
                                    <div class="row">
                                        <div class="col-xs-3" id="hideFav">
                                            <div class="product-template2" itemscope="">
                                                <figure style="box-shadow:none;">
                                                    <a data-toggle="modal" onclick="favActionModal('<?php echo $changeAppId = ''; ?>', 'add', '<?php echo $getUserInfo['favourite_app_ids']; ?>', 'false');">
                                                    <img style="" src="<?php echo $rootUrlImages?>apps/add.gif">
                                                </a>
                                                </figure>
                                                <div class="appName text-center" style="font-weight: 400;">Add To Favourite</div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <!--tab-content-->
                    </div>
                    <!--col-xs-9-->
                </div>
                <!-- row -->
                <!-- Modal -->
                <div class="modal fade bs-example-modal-lg row-10" id="favAppsModal" tabindex="-1" role="dialog" aria-labelledby="favAppsModal">
                    <div class="modal-dialog" role="document" style="width: 490px">
                        <div class="modal-content">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- container-fluid -->
        </main>
        <?php include '../layout/transparent_footer.php' ?>
        <script>
        $("#appSearchForm").submit(function(e) {
                e.preventDefault();
                ajaxAppSuggest();
            })
            //Written For Validation oF Search box
        function ajaxAppSuggest() {
            var searchData = ($("#appSuggest").val());
            $(".errorSearch").hide();
            if ($('#appSuggest').val() == '') {
                $("#errorBlank").fadeIn("slow");
                $("#errorBlank").show().delay(3000).fadeOut();

            } else {
                var searchData = ($("#appSuggest").val());
                var searchData = searchData.toLowerCase();
                var namelist = [];
                <?php foreach($getAllApp as $value ){
        $app_name   = $value['app_name']; ?>
                var searchData2 = '<?php echo $app_name; ?>';
                var searchData2 = searchData2.toLowerCase();
                namelist.push(searchData2);
                <?php } ?>
                var found = $.inArray(searchData, namelist) > -1;
                if (found == true) {
                    ajaxAppInfoForLoad(searchData);
                } else {
                    $("#errorSearch").show().delay(3000).fadeOut();
                    $(".ui-autocomplete").hide();
                }
            }
        }

        //apps searches Form every tab
        function ajaxAppInfoForLoad(page) {
            var searchData = page.toLowerCase();
            $.ajax({
                url: 'appinfo.php',
                type: 'POST',
                dataType: 'html',
                data: ({
                    appInfoAjaxPageid: searchData
                }),
                success: function(data) {
                    $("#upadteApp").html(data);
                    $("#widget").html(data);
                    $("#updateFavApp").html(data);
                    $("#errorSearch").hide();
                    $("html, body").animate({
                        scrollTop: 0
                    }, 400);
                    window.location.hash = "#app";
                }
            });
        }
        </script>
        <?php
}else{
        header('location:'.$rootUrl.'views/prelogin/index.php');
}

?>
