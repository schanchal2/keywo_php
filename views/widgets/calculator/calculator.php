<?php

error_reporting(0);?>

<?php

//get query string from url
$sq = rawurldecode($_GET["q"]);
if(strpos($sq, '&#039;')!== false){
    $sq = str_replace("&#039;", "'", $sq);
}
if(strpos($sq, '&quot;')!== false){
    $sq = str_replace("&quot;", '"', $sq);
}

if(strpos($sq, '%')!== false){
    $sq = str_replace("%","/100",$sq);
}
$result = 0;

//Function For Query Search
$detail = query_analyzer($sq,$conn); //echo "<pre>"; print_r($detail); echo "</pre>"; die;
if(noError($detail)){
    $detail = $detail["errMsg"];
}else{
    printArr("Error analyzing Query");
}
$keys = array_keys($detail["types"]);
//echo "<pre>"; print_r($keys); echo "</pre>"; die;
//echo $keys; die;
$widgetType = $keys[0];
if($widgetType === ""){
    $widgetType = $keys[1];
}

if(strpos($sq, 'calculator:')!== false){
    $sq = str_replace("calculator:","",$sq);
}
if(strpos($sq,'Calculator:')!== false){
    $sq = str_replace("Calculator:", "", $sq);
}
if(strpos($sq,'CALCULATOR:')!== false){
    $sq = str_replace("CALCULATOR:", "", $sq);
}
if(strpos($sq,'√')!== false){
    $sq = str_replace("√","sqrt",$sq);
}
if(strpos($sq,'π')!== false){
    $sq = str_replace("π","PI",$sq);
}
if(strpos($sq,'Π')!== false){
    $sq = str_replace("Π","PI",$sq);
}
$trig = array("sqrt", "sin", "cos", "tan"); //echo "<pre>"; print_r($keys); echo "</pre>"; die;
foreach($trig as $value)
{
    if(strpos($sq,$value)!== false){
        $sq = str_replace($value, "", $sq);
        $sq = str_replace(array( '(', ')' ), '', $sq);
        if($value === "sqrt"){
            $sq = sqrt($sq);
        }elseif($value === "sin"){
            //$sq = sin($sq);
            $sq=number_format(sin(deg2rad($sq)));
        }elseif($value === "cos"){
            //$sq = cos($sq);

            $sq=number_format(cos(deg2rad($sq)));
        }elseif($value === "tan"){
            //$sq = tan($sq);
            $sq=number_format(tan(deg2rad($sq)));

        }
    }
}
if(strpos($sq,'x')!== false){
    $sq = str_replace("x","*",$sq);
}
if(strpos($sq,'X')!== false){
    $sq = str_replace("X","*",$sq);
}
if(strpos($sq,'×')!== false){
    $sq = str_replace("×","*",$sq);
}
if(strpos($sq,'÷')!== false){
    $sq = str_replace("÷","/",$sq);
}
$sq = trim($sq);
$xx = $sq;
if($sq === ""){
    if($widgetType === "calculator" && $detail["types"][$widgetType]>0){
        foreach ($detail["words"] as $word => $type){
            if($type === $widgetType){

            }
        }?>
        <script language="javascript">
            document.getElementById("mathCalci").style.display = "block";
        </script>
        <?php
    }
}else{
    if($widgetType === "calculator" && $detail["types"][$widgetType]>=0){
        $result = Parser::solve($xx);?>
        <script language="javascript">
            document.getElementById("mathCalci").style.display = "block";
        </script>
        <?php
    }else{
        $result = Parser::solve($sq);?>
        <script language="javascript">
            document.getElementById("mathCalci").style.display = "block";
        </script>
        <?php
    }
}
?>
<script>
    var AppendFlag = 0;
    $( document ).ready(function() {
        var res = <?php echo $result; ?>;
        var elem = document.getElementById("output");
        elem.value = res;
        AppendFlag = 1;
        if (elem.selectionStart || elem.selectionStart == '0') {
            var elemLen = elem.value.length;
            elem.selectionStart = elemLen;
            elem.selectionEnd = elemLen;
            elem.focus();
        }else{
            elem.focus();
        }
    });
</script>


<div class="row inner-2x innerMT margin-left-none margin-right-none">
    <div class="calculator_main_widget widgets-center">
        <div class="widget_calcidiv innerL innerR">
            <div class="appDivHead text-left innerAll">Calculator</div>
            <div class="appDivBody text-center">
                <div id="mathCalci" class="calci innerB">
                    <form  id="calciForm" method="POST">
                        <input contenteditable="true" type="text" autocomplete="off" onpaste="return false;" class="form-control txtr" id="output" value="0" onblur="if(this.value == '') { this.value = '0'; } " size=40 onkeypress="return IsOneDecimalPoint(event)">
                        <div class="row" id="micalcbtns">
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="(" onClick="Par(0,0,'(')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value=")" onClick="Par(0,0,')')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="%" onClick="calcPerc(0,0,'%')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="&larr;" onClick="Bck(0,0)">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="7" onClick="Add(0,0,'7')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="8" onClick="Add(0,0,'8')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="9" onClick="Add(0,0,'9')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="CLR" onClick="Clr(0,0)">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="4" onClick="Add(0,0,'4')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="5" onClick="Add(0,0,'5')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="6" onClick="Add(0,0,'6')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="+" onClick="Fnc(0,0,'+')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="1" onClick="Add(0,0,'1')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="2" onClick="Add(0,0,'2')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="3" onClick="Add(0,0,'3')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="-" onClick="Fnc(0,0,'-')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="0" onClick="Add(0,0,'0')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" id="dot" value="." onClick="Pnt(0,0)">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="ln" onClick="Spc(0,0,'ln')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="*" onClick="Fnc(0,0,'*')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="sqrt" onClick="Spc(0,0,'sqrt')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="x^2" onClick="Spc(0,0,'pow2')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="&pi;" onClick="Add(0,0,'PI')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button type="submit" class="micalctcl calculator-key btn-block btn" value="/" onClick="Fnc(0,0,'/')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="sin" onClick="Spc(0,0,'sin')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="cos" onClick="Spc(0,0,'cos')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input type=button class="micalctcl calculator-key btn-block btn" value="tan" onClick="Spc(0,0,'tan')">
                            </div>
                            <div class="col-xs-3 col-xs-3 mbottom">
                                <input id="equal"  type=button class="micalctcl calculator-key btn-block btn" value="=" onClick="Res(0,0)">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<SCRIPT language="JavaScript">
    var Text = "Calculator programmed in Java Script";
    var Speed = 150;
    var Width = 64;
    var TextLength = Text.length;
    var Pos = 1 - Width + TextLength;
    var fact = Math.PI / 180.0;
    var inv = 0;    // if inverse function has to be taken
    var flag = 0;   // 0 if "=" is pressed
    var flag2 = 0;  // 1 if "+","-","*" or "/" pressed
    var popen = 0;  // 0 if no "(" is open
    var point = 0;  // 1 if "." is pressed



    function calcPerc(Fi,Ei,n) {
        AppendFlag = 1;
        var input =  document.forms["calciForm"].elements[Ei].value;
        document.getElementById("output").value = eval(eval(document.forms["calciForm"].elements[Ei].value) / 100);
        document.getElementById('dot').removeAttribute("disabled");

    }
    var cnt = 0;
    $("#output").keypress(function(event) {
        if (event.which == 37) {
            if(document.getElementById("output").value  != ''){
                calcPerc(0,0,'%');
            }
            event.preventDefault();
            return false;
        }
    });

    function TextMarquee()
    {
        Pos++;
        var TextOut = "";
        if (Pos == TextLength)
            Pos = 1 - Width;
        if (Pos < 0)
        {
            for (var i=1; i<=Math.abs(Pos); i++)
                TextOut = TextOut + " ";
            TextOut = TextOut + Text.substring(0,Width-i+1);
        }
        else
            TextOut = TextOut + Text.substring(Pos,Width+Pos);
        window.status = TextOut;
        window.setTimeout("TextMarquee()",Speed);
    }
    function Clr(Fi,Ei)
    {
        AppendFlag = 1;
        flag2 = 0;
        popen = 0;
        point = 0;
        cnt = 0;
        document.getElementById("output").value = '';
        document.getElementById("output").focus();

        document.getElementById('dot').removeAttribute("disabled");


    }
    $("#output").click(function(event) {
        if( document.getElementById("output").value  == '0') { document.getElementById("output").value = '';  cnt = 0;}
    });

    $("#output").keypress(function(event) {

        if(AppendFlag == 1){
            document.getElementById("output").value = '';
            AppendFlag = 0;
        }
        if (event.which == 13) {
            if(document.getElementById("output").value  != ''){

                var st = document.forms["calciForm"].elements[0].value;

                var len = st.length;

                var strChar = st.charAt(len - 1);
                var lastChar = strChar.charCodeAt(0);

                if (lastChar == 42 || lastChar == 43 || lastChar == 45 || lastChar == 46  || lastChar == 47){

                    event.preventDefault();
                    return false;

                }

                Res(0,0);
            }
            event.preventDefault();
            //return false;
        }
        if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; cnt = 0;}
    });

    $("#output").keypress(function(event) {
        if(document.getElementById("output").value  == ''){
            cnt = 0;

        }

    });
    $("#search_box").keypress(function(event) {
        if(event.which == 61){
            Res(0,0);
            event.preventDefault();
            return false;
        }
        if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; }
    });

    $("#output").keyup(function(event) {

        var st = document.forms["calciForm"].elements[0].value;
        var len = st.length;
        var strChar = st.charAt(st.length - 1);
        var lastChar = strChar.charCodeAt(0);

        // var remaingData = st.slice(0,-1);
        // var keyPressChar = String.fromCharCode(event.charCode);
        if (len==1 && (lastChar == 42 || lastChar == 43 || lastChar == 45 || lastChar == 46  || lastChar == 47)){
            //if (('/*-+.'.indexOf(keyPressChar) !== -1)){

            var result = 0 + strChar;

            document.forms["calciForm"].elements[0].value = result;
            event.preventDefault();
            return false;
            //}
        }

        if( document.getElementById("output").value  == '0') { document.getElementById("output").value = ''; cnt = 0; }
    });

    function Res(Fi,Ei)
    {
        var str = document.forms["calciForm"].elements[Ei].value;
        if ((flag2 == 0) && (popen == 0))
        {

            for (i = 0; i < str.length; i++)
                if (str.charAt(i) == '.') {

                    var length=parseInt(str.length/2);
                    var d=Math.pow(10, length);
                    var x = eval(document.forms["calciForm"].elements[Ei].value);
                    var input = document.getElementById("output").value =Math.round((x)* d)/  d;
                    point=1;

                    if(point==1){
                        $("#dot").attr("disabled","disabled");

                    }
                }
            var display = document.forms["calciForm"].elements[Ei].value;

            var x = eval(document.forms["calciForm"].elements[Ei].value);
            document.getElementById("output").value = parseFloat(x);
            flag = 0;
            point = 0;
            //AppendFlag = 1;

        }

    }


    function Add(Fi,Ei,n)
    {


        if(AppendFlag == 1){
            document.forms["calciForm"].elements[Ei].value = '';
            AppendFlag = 0;
        }
        var st = document.forms["calciForm"].elements[Ei].value;
        var len = st.length;
        if (st.charAt(len-1) == ")")
            return;
        if (n == "PI")
        {
            if (st.charAt(len-1) == ")" || st.charAt(len-1) == "0" || st.charAt(len-1) == "1" || st.charAt(len-1) == "2" || st.charAt(len-1) == "3" || st.charAt(len-1) == "4" || st.charAt(len-1) == "5" || st.charAt(len-1) == "6" || st.charAt(len-1) == "7" || st.charAt(len-1) == "8" || st.charAt(len-1) == "9")
                return;
            AppendFlag = 1;
            document.getElementById("output").value == Math.PI;
            var x = Math.PI;
            n = x;
        }
        if(document.forms["calciForm"].elements[Ei].value == '0')
        {
            if(n === '0'){

            }else{
                document.forms["calciForm"].elements[Ei].value = n;
            }
        }else{
            var secondLastChar = st.charAt(st.length - 2);
            var lastChar = st.charAt(st.length - 1);

            if ( ('/*-+.'.indexOf(secondLastChar) !== -1) && (secondLastChar !='') && (lastChar == '0')){
                var remaingData = st.slice(0,-1);
                document.forms["calciForm"].elements[Ei].value = remaingData + n;
            }else{
                document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
            }
        }

        document.getElementById("output").value =(document.forms["calciForm"].elements[Ei].value);
        flag = 1;
        flag2 = 0;

        document.getElementById('dot').removeAttribute("disabled");

    }
    function Par(Fi,Ei,n)
    {
        var st = document.forms["calciForm"].elements[Ei].value;
        var len = st.length;
        if ( (n == "(") )
        {
            AppendFlag = 0;
            if (st.charAt(len-1) == ")") {
                return;
            }else if (st.charAt(len-1) == "("){
                popen++;
            }else
            {
                if ( (len > 0) && (flag2 == 0) ){
                    if(st == '0'){
                        st = document.forms["calciForm"].elements[Ei].value = ''+n;
                        popen++;
                        return;
                    }

                    return;
                }else{
                    popen++;
                }
            }
        }

        else if ( (n == ")") && (popen > 0) )
        {
            if (st.charAt(len-1) == "(")
                return;
            else
                popen--;
        }
        else
            return;
        document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
        document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
        flag = 1;
        point = 0;

    }
    function Bck(Fi,Ei)
    {

        var st = document.forms["calciForm"].elements[Ei].value;

        var len = st.length;
        cnt = 0;
        if (len >= 0)
        {
            if (st.charAt(len-1) == ".")
                point = 0;
            if (st.charAt(len-1) == "+")
                flag2 = 0;
            if (st.charAt(len-1) == "-")
                flag2 = 0;
            if (st.charAt(len-1) == "*")
                flag2 = 0;
            if (st.charAt(len-1) == "/")
                flag2 = 0;
            if (st.charAt(len-1) == ")")
                popen++;
            if (st.charAt(len-1) == "(")
                popen--;
            document.forms["calciForm"].elements[Ei].value = st.substring(0,len-1);
            document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
            flag = 1;
        }
        if (len === 1){
            document.getElementById("output").value = 0;
        }

        document.getElementById('dot').removeAttribute("disabled");




    }
    function Pnt(Fi,Ei)
    {
        AppendFlag = 0;
        if ( (flag2 == 0) && (point == 0) )
        {

            flag2 = 1;
            flag = 1;
            point = 1;
            document.forms["calciForm"].elements[Ei].value =
                document.forms["calciForm"].elements[Ei].value + ".";
            document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
        }


    }

    function Fnc(Fi,Ei,n)
    {
        AppendFlag = 0;
        var st = document.forms["calciForm"].elements[Ei].value;
        var len = st.length;
        if (st.charAt(len-1) == "(" && n == '-')
            document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
        var lastChar = st.charAt(st.length - 1);
        if(('/*-+'.indexOf(lastChar) !== -1)){
            if ( ('/*-+'.indexOf(n) !== -1) ){
                var remaingData = st.slice(0,-1);
                document.forms["calciForm"].elements[Ei].value = remaingData + n;
            }
        }else{
            if (flag2 == 0)
            {
                if(st == 0 && n == '-'){
                    document.forms["calciForm"].elements[Ei].value = '';
                }
                flag2 = 1;
                flag = 1;
                point = 0;
                document.forms["calciForm"].elements[Ei].value = document.forms["calciForm"].elements[Ei].value + n;
                document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;

            }

        }
        document.getElementById('dot').removeAttribute("disabled");


    }



    function Inv()
    {
        inv = 1;
    }

    function Spc(Fi,Ei,f)
    {
        var y = '';
        if ( (flag2 == 0) && (popen == 0) )
        {
            var st = document.forms["calciForm"].elements[Ei].value;
            var x = '';
            if(st > 0){
                if (st.length == 0)
                    x = 0;
                else
                    x = eval(st);
                if (f=="sqrt")
                {
                    if(AppendFlag == 0){
                        y = '';
                        AppendFlag = 1;
                    }
                    if (inv == 0)
                        y = (x>0) ? Math.sqrt(x) : "";
                    else
                        y = x * x;
                }
                if (f=="pow2")
                {
                    if(AppendFlag == 0){
                        y = '';
                        AppendFlag = 1;
                    }
                    if (inv == 1)
                        y = (x>0) ? Math.sqrt(x) : "";
                    else
                        y = x * x;
                }
                if (f=="ln")
                {
                    if(AppendFlag == 0){
                        y = '';
                        AppendFlag = 1;
                    }
                    if (inv == 0)
                        y = (x>0) ? Math.log(x) : "";
                    else
                        y = Math.exp(x);
                }
            }else{
                AppendFlag = 1;
                y = "Invalid Input";
            }
            if (f=="exp")
            {
                if (inv == 1)
                    y = (x>0) ? Math.log(x) : "";
                else
                    y = Math.exp(x);
            }
            if (f=="sin")
            {
                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }
                if (inv == 0)

                //y = parseFloat(Math.sin(x*fact).toFixed());
                    y = parseFloat(Math.sin(x*fact));

                else
                    y = 1./fact * Math.asin(x);
            }
            if (f=="cos")
            {


                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }
                if (inv == 0)
                //y = parseFloat(Math.cos(x*fact).toFixed());
                    y = parseFloat(Math.cos(x*fact));
                else
                    y = 1./fact * Math.acos(x);
            }
            if (f=="tan")
            {

                if(AppendFlag == 0){
                    y = '';
                    AppendFlag = 1;
                }


                if (inv == 0){

                    //y = parseFloat(Math.tan(x*fact).toFixed());
                    y = parseFloat(Math.tan(x*fact));
                }

                else
                    y = 1./fact * Math.atan(x);
            }
            if (f=="inv")
            {
                y = (inv==0) ? 1 / x : x;
            }
            document.forms["calciForm"].elements[Ei].value = y;
            document.getElementById("output").value = document.forms["calciForm"].elements[Ei].value;
            inv = 0;
            point = 0;
        }
    }
    function isNumberKey(evt){
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 37 || charCode > 57) &&(charCode < 90 || charCode > 94 ))
            return false;
        return true;
    }

    function IsOneDecimalPoint(evt)
    {
        var input = document.getElementById("output").value;

        if (event.which == 46) {
            cnt++;
            //event.preventDefault();
            if(cnt > 1){
                if($.isNumeric(input)){
                    return false;
                }

            }
        }
        var target = evt.target || evt.srcElement;
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 37 || charCode > 57) &&(charCode < 90 || charCode > 94 ))
            return false;
        return true;

    }


</SCRIPT>
