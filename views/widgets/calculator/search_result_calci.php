
	<?php
	session_start(); //echo "Hello"; die;

	//include dependent files
	require_once('../../../config/config.php');
	require_once('../../../config/db_config.php');
	require_once('../../../helpers/errorMap.php');
	require_once('../../../helpers/coreFunctions.php');
	require_once('../../../helpers/stringHelper.php');
	require_once('../../../models/search/widgetSearchModel.php');
	error_reporting(0);
	global $rootUrl;
	$email = $_SESSION["email"];

	//For database connection
	$conn = createDBConnection('dbsearch');

	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	//Validating User LoggedIn and LoggedOUt status
	if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		$login_status = 1;
	}else{
		$login_status = 0;
	}
//print_r($login_status);
	//get query string from url
	$keywords =  cleanXSS(rawurldecode($_REQUEST['q'])); echo $keywords;


	//if keyword Null page redirect to index page
	if($keywords == "" && $keywords == null){

	}

	//convert search query into array of keywords.
	$keywordsArr = explode(" ", $keywords);
	$searchkey = "";
	foreach($keywordsArr as $key => $keyword){
		if($keyword != ""){
			$searchkey.=$keyword." ";
		}
	}
	//print_r($keywordsArr);

	$array2 = array();
	foreach($keywordsArr as $array1){
		$string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
		array_push($array2, $string);
	} //print_r($array2); die;

	$searchData = implode(" ", $array2); //print_r($searchData);
	// sanitizing search query from tags, extra spaces and special characters.
	$searchkey = strip_tags(trim($searchkey)); //print_r($searchkey);
	$searchkey = preg_replace("/\s\s([\s]+)?/", " ", $searchkey);  //print_r($searchkey);

	// get user ip address
	// $ipAddr = get_client_ip();
	// check rquest for blocking IP address.
	// $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
	// $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $searchkey);
	// if(noError($checkIPInfo)){
	// 	//clean requsest.
	// }else{
	// 	$returnArr["errCode"][5] = 5;
	// 	$returnArr["errMsg"] = $checkIPInfo["errMsg"];
	// }

	$logFileName = "CalculatorResult";
	$appId = 18;


	$keywords = strtolower($keywords);
	if(strpos($keywords, '&#039;')!== false){
		$keywords = str_replace("&#039;", "'", $keywords);
	}
	if(strpos($keywords, '&quot;')!== false){
		$keywords = str_replace("&quot;", '"', $keywords);
	}

	/****For Calculator Widget****/
	$trig = array("keywordsrt","sin","cos","tan");
	foreach($trig as $value)
	{
		if(strpos($keywords,$value)!== false){
			if(strpos($keywords,'calculator:')!== false){
				$keywords = str_replace("calculator:", "", $keywords);
			}
			if(strpos($keywords,'Calculator:')!== false){
				$keywords = str_replace("Calculator:", "", $keywords);
			}
			$keywords = str_replace($value, "", $keywords);
			$keywords = str_replace(array( '(', ')' ), '', $keywords);
			if($value === "sqrt"){
				$keywords = sqrt($keywords);
			}elseif($value === "sin"){
				$keywords = sin($keywords);
			}elseif($value === "cos"){
				$keywords = cos($keywords);
			}elseif($value === "tan"){
				$keywords = tan($keywords);
			}
		}
	}
	$mathString = trim($keywords);
	$number =  preg_replace("/[^a-z0-9]/i", '', $mathString); //echo $number;


	//Function to Query Search
	$detail = query_analyzer($keywords,$conn);
	if(noError($detail)){
		$detail = $detail["errMsg"];
	}else{
		printArr("Error analyzing Query");
	}

	$keys = array_keys($detail["types"]);
	$widgetType = $keys[0];
	if($widgetType == ""){
		$widgetType = $keys[1];
	}

	/* Header include */
	include '../../layout/header.php';

	if($login_status == 1){

		// getting logged in user's default app id
		$require   		 = $userRequiredFields . ",default_search_appId";
		$searchAppId 	 = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $require);
    	// print_r($require);

		if(noError($searchAppId)){
			$searchAppId = $searchAppId["errMsg"]['default_search_appId'];

			if(!empty($searchAppId)){

				// getting app details of user's default app.
				$appDetails = getAppDetails($conn, $searchAppId);
				if(noError($appDetails)){
					$appDetails = $appDetails["errMsg"][0];
					$appName = $appDetails["app_name"];
					$appName = str_replace(' ', '', $appName);
					$appURL = $appDetails["searchresult_url"];
					$appSearchLogo = $appDetails["searchengine_images"];
					$landingPage = $appDetails["landingPage_url"];
				} else {
					print("Error: ".$appDetails["errMsg"]);
					exit;
				}
			}
		}else{
			print("Error: ".$searchAppId["errMsg"]);
			exit;
		}
	}

	//if default app id is blank, get admin set default app
	if(empty($searchAppId)){
		//getting default search app as set by admin.
		$searchAppId = getDefaultAppId($conn);
		//echo "<pre>"; print_r($searchAppId); echo "</pre>";
		if(noError($searchAppId)){
			$appName = $searchAppId["errMsg"][0]["app_name"];
            $appName = str_replace(' ', '', $appName);
			$appURL = $searchAppId["errMsg"][0]["searchresult_url"]; //echo $appURL; die;
			$appSearchLogo = $searchAppId["errMsg"][0]["searchengine_images"];
			$landingPage = $searchAppId["errMsg"][0]["landingPage_url"];
			$searchAppId = $searchAppId["errMsg"][0]["default_app_id"];
		}else{
			print("Error Fetching default search engine");
			exit;
		}
	} //print_r($searchAppId); die;

	if($widgetType === "calculator" && $detail["types"]["calculator"]>1 || (is_numeric($number)) || strtolower($keywords) == 'calculator:') {  ?>

<?php }else{

		if($appName){
			$appURL = $rootUrlSearch.strtolower($appName)."/".$appURL."?q=".$keywords;
			?>
			<form id="my_form" name="my_form" action="<?php echo $appURL; ?>" enctype="application/x-www-form-urlencoded"  role="search" onsubmit=" return checkEmptyField();" hidden>
				<div class="col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2" style="margin-top:2em">
					<input type="hidden" name="cx" value="partner-pub-6535511040694921:3732337095" />
					<input type="hidden" name="cof" value="FORID:10" />
					<input type="hidden" name="ie" value="UTF-8" />
					<div class="col-xs-11 antiPadding">
						<input id="search_box" value="<?php echo $keywords; ?>"  name="q" class="indexInput form-control autoSuggest" type="text" placeholder="Enter Your Search Term and Start Earning"/>
					</div>
					<div class="col-xs-1 antiPadding">
						<input id="google_btn" class="searchBtn" type="submit"  name="sa" value="" />
					</div>
				</div>
			</form>
			<?php echo "<script type=\"text/javascript\">
					document.my_form.submit();
				</script>";
		}else{
			header("Location: ".$rootUrl.$appURL."?q=".$keywords);
		}
	}
?>

	<!-- Search Categories strip -->
	<!-- Widget + Search result Container + Ads -->
	<main>
		<div id="main-area">
			<!-- Widget + Search result Container -->
			<div id="contentFlip">
				<!-- getting widget calculation -->
				<div id="widget" class="inner-6x innerML"></div>
				<!-- Search Result Container -->
				<section id="searchContainer"></section>
			</div>
			<!-- Ads container -->
			<div id="sidebarFlip" style="display:none;"></div>
		</div>
	</main>
	<?php
	include("../../layout/transparent_footer.php");
	?>
	<script>
		//get search results on page load from appropriate app
		$(document).ready(function(){
			var keyword = '<?php echo $searchkey; ?>'; //alert(keyword);
			var currentCurrPref = '<?php echo $userCurrencyPreference; ?>';
			var login_status = "<?php echo $login_status;?>"; //alert(login_status);
			var flag = 1;
			var	appName = "calculator";
			var rootUrl = '<?php echo $rootUrl; ?>';
			var currRate = '<?php echo $getCurrCurrentRate; ?>';
			$( ".overlay" ).show();
			$(".closeBtnDiv").hide();

			if(login_status == 1) {
				// Load widget query using ajax
				$.ajax({
					type: 'GET',
					url: '../../../controllers/search/calciResultController.php',
					data: {q: keyword, currentCurrPref: currentCurrPref},
					dataType: 'html',
					async: true,
					success: function (data) {
						// This will replace all content in the body tag with what has been retrieved
						$("#widget").html();
						$("#widget").show();
						$("#widget").html(data).fadeIn(1000);
						$(".closeBtnDiv").hide();
					}
					/*,
					 statusCode: {
					 404: function() {
					 alert( "calciResult.php page not found" );
					 }
					 }*/
				});
			}else{
				window.location.href = rootUrl + 'views/prelogin/';
			}
		});
	</script>
