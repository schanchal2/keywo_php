
<?php
	session_start(); //echo "Hello"; die;
	//include dependent files
	require_once('../../../config/config.php');
	require_once('../../../config/db_config.php');
	require_once('../../../helpers/errorMap.php');
	require_once('../../../helpers/coreFunctions.php');
	require_once('../../../helpers/stringHelper.php');
	require_once('../../../models/search/widgetSearchModel.php');
	require_once('../../../helpers/deviceHelper.php');

	error_reporting(0);

	global $walletURLIP;

//	$_SESSION["email"] = 'sudeepmakwana@bitstreet.in';
	$email = $_SESSION["email"];

	//For database connection
	$conn = createDBConnection('dbsearch');

	if(noError($conn)){
		$conn = $conn["connection"];
	}else{
		print_r("Database Error");
	}
	//Validating User LoggedIn and LoggedOUt status
	if(isset($_SESSION["email"]) && !empty($_SESSION["email"])) {
		$login_status = 1;
	}else{
		$login_status = 0;
	}
//print_r($login_status);
	//get query string from url
	$keywords =  cleanXSS(rawurldecode($_REQUEST['q'])); //echo $keywords;

	//if keyword Null page redirect to index page
	if($keywords == "" && $keywords == null){

	}

	//convert search query into array of keywords.
	$keywordsArr = explode(" ", $keywords); //print_r($keywordsArr);
	$searchkey = "";
	foreach($keywordsArr as $key => $keyword){
		if($keyword != ""){
			$searchkey.=$keyword." ";
		}
	}
$array2 = array();
	foreach($keywordsArr as $array1){
		$string = str_replace(' ', '-', trim($array1)); // Replaces all spaces with hyphens.
		$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
		$string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
		array_push($array2, $string);
	}

	$searchData = implode(" ", $array2); //print_r($searchData);
	// sanitizing search query from tags, extra spaces and special characters.
	$searchkey = strip_tags(trim($searchkey)); //print_r($searchkey);
	$searchkey = preg_replace("/\s\s([\s]+)?/", " ", $searchkey);  //print_r($searchkey);

	// get user ip address
	// $ipAddr = get_client_ip();
	// check rquest for blocking IP address.
	// $user_agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL;
	// $checkIPInfo = checkClientIpInfo($ipAddr, $user_agent, $searchkey);
	// if(noError($checkIPInfo)){
	// 	//clean requsest.
	// }else{
	// 	$returnArr["errCode"][5] = 5;
	// 	$returnArr["errMsg"] = $checkIPInfo["errMsg"];
	// }

	$logFileName = "TimezoneResult";
	$appId = 16;

	$keywords = strtolower($keywords); //echo $keywords;
	if(strpos($keywords, '&#039;') !== false){
		$keywords = str_replace("&#039;", "'", $keywords);
	}
	if(strpos($keywords, '&quot;') !== false){
		$keywords = str_replace("&quot;", '"', $keywords);
	}

	//Function to Query Search
	$detail = query_analyzer($keywords,$conn); //echo "<pre>"; print_r($detail); echo "</pre>";

	if(noError($detail)){
		$detail = $detail["errMsg"];
	}else{
		printArr("Error analyzing Query");
	}


	$keys = array_keys($detail["types"]);
	$widgetType = $keys[0];
	if($widgetType == ""){
		$widgetType = $keys[1];
	}

/* Header include */
include '../../layout/header.php';

	if($login_status == 1){

		// getting logged in user's default app id
		$require   		 = "user_id,default_search_appId";
    	$searchAppId 	 = getUserInfo($email, $walletURLIPnotification.'api/notify/v2/', $require);
    	 //echo "<pre>"; print_r($searchAppId); echo "</pre>";

		if(noError($searchAppId)){
			$searchAppId = $searchAppId["errMsg"]['default_search_appId'];

			if(!empty($searchAppId)){

				// getting app details of user's default app.
				$appDetails = getAppDetails($conn, $searchAppId);
				//echo "<pre>"; print_r($appDetails); echo "</pre>";
				if(noError($appDetails)){
					$appDetails = $appDetails["errMsg"][0];
					$appName = $appDetails["app_name"];
                    $appName = str_replace(' ', '', $appName);
					$appURL = $appDetails["searchresult_url"];
					$appSearchLogo = $appDetails["searchengine_images"];
					$landingPage = $appDetails["landingPage_url"];
				} else {
					print("Error: ".$appDetails["errMsg"]);
					exit;
				}
			}
		}else{
			print("Error: ".$searchAppId["errMsg"]);
			exit;
		}
	}

	//if default app id is blank, get admin set default app
	if(empty($searchAppId)){
		//getting default search app as set by admin.
		$searchAppId = getDefaultAppId($conn);
		if(noError($searchAppId)){
			$appName = $searchAppId["errMsg"][0]["app_name"];
			$appURL = $searchAppId["errMsg"][0]["searchresult_url"];
			$appSearchLogo = $searchAppId["errMsg"][0]["searchengine_images"];
			$landingPage = $searchAppId["errMsg"][0]["landingPage_url"];
			$searchAppId = $searchAppId["errMsg"][0]["default_app_id"];
            $appName = str_replace(' ', '', $appName);
		}else{
			print("Error Fetching default search engine");
			exit;
		}
	}

	if($widgetType === "timezone" && $detail["types"]["timezone"]>1 || strtolower($keywords) === "timezone:")
		{  ?>

	<?php }else{

		if($appName){
			$appURL = $rootUrlSearch.strtolower($appName)."/".$appURL."?q=".$keywords;
			//print_r($appURL);
			?>
			<form id="my_form" name="my_form" action="<?php echo $appURL; ?>" enctype="application/x-www-form-urlencoded"  role="search" onsubmit=" return checkEmptyField();" hidden>
				<div class="col-md-6 col-md-offset-3 col-xs-8 col-xs-offset-2" style="margin-top:2em">
					<input type="hidden" name="cx" value="partner-pub-6535511040694921:3732337095" />
					<input type="hidden" name="cof" value="FORID:10" />
					<input type="hidden" name="ie" value="UTF-8" />
					<div class="col-xs-11 antiPadding">
						<input id="search_box" value="<?php echo $keywords; ?>"  name="q" class="indexInput form-control autoSuggest" type="text" placeholder="Enter Your Search Term and Start Earning"/>
					</div>
					<div class="col-xs-1 antiPadding">
						<input id="google_btn" class="searchBtn" type="submit"  name="sa" value="" />
					</div>
				</div>
			</form>
			<?php echo "<script type=\"text/javascript\">
					document.my_form.submit();
				</script>";
		}else{
			header("Location: ".$rootUrlSearch.$appURL."?q=".$keywords);
		}
	}
?>
<?php include '../../layout/transparent_footer.php'; ?>
		<main>

				<!-- getting widget calculation -->
				<div id="widget" class="inner-6x innerML" ></div>
				<!-- Search Result Container -->
				<section id="searchContainer"></section>
			</div>

			<!-- Ads container -->
			<div id="sidebarFlip" style="display:none;"></div>
		
	</main>
	<script>
		//get search results on page load from appropriate app
		$(document).ready(function(){
			var keyword = '<?php echo $searchkey; ?>';
			var currentCurrPref = '<?php echo $userCurrencyPreference; ?>';
			var login_status = "<?php echo ($login_status);?>";
			var flag = 1;
			var	appName = "timezone";
			var rootUrl = '<?php echo $rootUrl; ?>';
			var currRate = '<?php echo ($getCurrCurrentRate); ?>';
			$( ".overlay" ).show();
			$(".closeBtnDiv").hide();

			if(login_status == 1) {

			// Load widget query using ajax
			$.ajax({
				type:'GET',
				url: '../../../controllers/search/widgetTimeZoneController.php',
				data: {q:keyword, currentCurrPref:currentCurrPref},
				dataType: 'html',
				async:true,
				success: function(data){
						// This will replace all content in the body tag with what has been retrieved
						$("#widget").html();
						$("#widget").show();
						$("#widget").html(data).fadeIn(1000);
						$(".closeBtnDiv").hide();
				}
				/*,
				statusCode: {
					404: function() {
						alert( "timezoneResult.php page not found" );
					}
				}*/
			});
			}else{
				window.location.href = rootUrl + 'views/prelogin/';
			}
		});
	</script>
