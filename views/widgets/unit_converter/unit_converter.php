<?php
session_start();
// include dependent files
// require_once("protected/controller/widget_converter.php");
// require_once("protected/controller/config.php");
 error_reporting(0);

//get query string from url
$sq = urldecode($_GET["q"]);
$sq = strtolower($sq);
if(strpos($sq,'\\') !== false){
    $sq = str_replace("\\","",$sq);
}
if(strpos($sq,'/') !== false){
    $sq = str_replace("/", "", $sq);
}
$detail = query_analyzer($sq,$conn); //Function For Query Search
$qwords = array_keys($detail["errMsg"]["words"]);
$qtime = 1;
$seconduni = '';
$dotunit = '';

foreach(array_keys($qwords) AS $k ){
	$str = $qwords[$k];
	if(strpos($str,'.') !== false){
		$qtime = $str;
	}
	if(strpos($qtime,'.') !== false) {
		$x = explode('.', $qtime);
 		preg_match("/(\\d+)([a-zA-Z]+)/", $x[1], $matches);
    	if($matches[1] != ""){
    		$first = $x[0];
    		$sec = $matches[1];
    		$op = $first .".".$sec;
			$qint = (int)$op;
			$join = gettype($qint);
			$seconduni = $matches[2];
			$qtime = $op;
		}
	}
	if(strpos($str,'.') !== false) {
		$dot = explode('.', $str);
		$dotunit = $dot[0];
	}else{
		$dotunit = '';
	}
    preg_match("/(\\d+)([a-zA-Z]+)/", $str, $matches);
    if($matches[1] != ""){
		$qint = (int)$matches[1];
		$join = gettype($qint);
		$seconduni = $matches[2];
	}
	if($join == ""){
		$this_value = $detail["errMsg"]["words"][$qwords[$k]];
    	if($this_value == ""){
			$temp = gettype($qwords[$k]);
			if($temp == integer){
				$qtime = $qwords[$k];
			}
		}
	}elseif($join == integer){
   		if($matches[1] != ""){
			$qint = (int)$matches[1];
			$join = gettype($qint);
			$qtime = $qint;
			$seconduni = $matches[2];
		}else{
			preg_match("/(\\d+)([a-zA-Z]+)/",$x[1], $matches);
			if($matches[1] != ""){
				$first = $x[0];
				$sec = $matches[1];
				$op = $first .".".$sec;
				$qint = (int)$op;
				$join = gettype($qint);
				$seconduni = $matches[2];
				$qtime = $op;
			}
		}
   	}
}

if(noError($detail)){
	$detail = $detail["errMsg"];
} else {
	printArr("Error analyzing Query");
}
$unittype = array();
$unittype[0] = "Area";
$unittype[1] = "Bandwidth";
$unittype[2] = "Storage";
$unittype[3] = "Length";
$unittype[4] = "Energy";
$unittype[5] = "Frequency";
$unittype[6] = "Mileage";
$unittype[7] = "Mass";
$unittype[8] = "Angle";
$unittype[9] = "Pressure";
$unittype[10] = "Speed";
$unittype[11] = "Volume";
$unittype[12] = "Temperature";
$unittype[13] = "Timeunit";
$keys = array_keys($detail["types"]);
$widgetType = $keys[0];
if($widgetType == ""){
	$widgetType = $keys[1];
}
if(in_array($widgetType, $unittype, true) && $detail["types"][$widgetType]>0){
    $fromUniCode = $seconduni;
    $toUniCode = $dotunit;
    $amount = $qtime;
	foreach ($detail["words"] as $word => $type) {
		if($type === $widgetType){
			if(empty($fromUniCode)){
				$fromUniCode = $word;
			} else if(empty($toUniCode)){
				$toUniCode = $word;
			} else{
				break;
			}
		}
    }
	$typelist = typeanalyzer($conn);
	$unit_long = unit_long_list($conn);
	foreach($unit_long[errMsg] as $key => $value) {
		$e_unit = explode("," , $value);
		if (strcasecmp($fromUniCode, $e_unit[1]) == 0) {
			$fromUniCode = $e_unit[0];
		}elseif(strcasecmp($toUniCode, $e_unit[1]) == 0) {
			$toUniCode = $e_unit[0];
		}
	} //print_r($typelist); die;
	?>
		<script type = "text/javascript">
			var x = document.getElementById("unitname").value;
			$(function(){
				$('#zzz').on('keydown', '#fromnumber', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
			})
		</script>

<div class="row inner-2x innerMT margin-left-none margin-right-none">
  <div class="unit_converter_main_widget widgets-center">
    <div class="widget_unitappdiv clearfix padding-none">
        <div class="appDivHead text-left innerAll">Unit Converter</div>
        <div class="appDivBody text-center pull-left">
          <section id="tab-item-3">
            <div class="col-xs-12 widgetUnit">
              <div id="unitNote">
                <font class="unitNote">Select Unit Type For Conversion</font>
              </div>
            </div>
           <input id="to" value="<?php echo $toUniCode;?>" type="hidden"/>
					<input id="frm" value="<?php echo $fromUniCode;?>" type="hidden"/>
					<div class="col-md-6 SelectDiv">
						<select name="xuv" class="unitTypeSelect form-control" id="unitname" onchange="callphpUnitfunction();" flag ="3" xy="1" >
							<?php foreach($typelist["errMsg"] as $key => $value){
							?>
							<option value = "<?php echo $value;?>" fullname = "<?php echo $value; ?>" name="xuv" <?php if($widgetType==$value){ ?>selected<?php } ?>><?php echo $value; ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-md-12 editUnitContainerDiv padding-none">
						<div class="col-md-3 types" >
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" value="<?php echo $qtime;?>" type="number" class="" id="fromnumber" onkeypress="return isNumberKey(event)" onchange="changeunitset(this.id);" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="fromnumber"></label>
							</span>
						</div>

						<div class="col-md-3 Selectbox" id="unit_set1"></div>
						<div class="col-md-3 unitValue" >
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" value="" id="tonumber" type="number" class="" onkeypress="return isNumberKey(event)" onchange="changeunitset(this.id);" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="tonumber"></label>
							</span>
						</div>
						<div class="col-md-3 Selectbox" id="unit_set2"></div>
					</div>
<!--					<div class="col-md-12 widgetUnitButton">-->
<!--						<div class="changePassBtnMainDiv row">-->
<!--							<div class="closeBtnDiv" >-->
<!--								<button class="closeButton form-control" data-remodal-action="cancel">CLOSE</button>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
				</section>
			</div>
		</div>
	</div>
<?php }?>

<?php
  include("../../layout/transparent_footer.php");
?>
	<script src="<?php echo $rootUrl; ?>js/widget.js"></script>

	<script>
		$(document).ready(function() {
			callphpUnitfunction();
		});
	</script>
