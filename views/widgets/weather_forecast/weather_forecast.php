<?php
//include dependent files

 //echo "Hello"; die;
error_reporting(0);
?>

<?php

//get query string from url
$sq = urldecode($_GET["q"]); //echo $sq; die;
if(strpos($sq, '&#039;')!== false){
	$sq = str_replace("&#039;","'",$sq);
}
if(strpos($sq, '&quot;')!== false){
	$sq = str_replace("&quot;",'"', $sq);
}
if(strpos($sq,'\\')!== false){
	$sq = str_replace("\\","",$sq);
}
if(strpos($sq,'weather:')!== false){
	$sq = str_replace("weather:","",$sq);
}
if(strpos($sq,'weather')!== false){
	$sq = str_replace("weather","",$sq);
}
if(strpos($sq,'Weather:')!== false){
    $sq = str_replace("Weather:","",$sq);
}
if(strpos($sq,'WEATHER:')!== false){
    $sq = str_replace("WEATHER:","",$sq);
}
if(strpos($sq,'/')!== false){
    $sq = str_replace("/","",$sq);
}
//Function For Query Search
$detail = query_analyzer($sq, $conn);
if(noError($detail)){
	$detail = $detail["errMsg"];
} else {
	printArr("Error analyzing Query");
}

$qwords = array_keys($detail["errMsg"]["words"]);

foreach(array_keys($qwords) AS $k ){
	$str = $qwords[$k];
}
$keys = array_keys($detail["types"]);
$widgetType = $keys[0];
if($widgetType === ""){
	$widgetType = $keys[1];
}
if($sq === ""){
	$ipAddress = getClientIP(); //print_r($ipAddress);
	//$ipAddress = '203.90.80.138';
	/*Get user ip address details with geoplugin.net*/
	//$query = file_get_contents('http://freegeoip.net/json/'.$ipAddress);

	/* This weather ip does not send weather information on localhost */
	$query = file_get_contents('http://ip-api.com/json/'.$ipAddress);
	$query = json_decode($query,true); //print_r($query);

	$ipcity = $query['city'];
	$ipstate = $query['regionName'];
	$ipcon = $query['country'];
	$ipcountry = $query['countryCode'];
	$iptz = $query['timezone'];

	if($ipcity != '' && !empty($ipcity)){
		$city = $ipcity;
		$state = $ipstate;
		$country = $ipcon; //echo $country;die;
	}
	?>

<div class="clearfix padding-none widget_weatherdiv widgets-center" id = "forcast" style="display:none;">
		<div class="appDivHead innerAll text-left">Weather Forecast</div>
		<div class="appDivBody text-center"  style="height: auto;">
			<div class="col-md-12" id = "close" style = "display:none;">
				<img id = "closeicon" onclick="hideDiv()" src = "<?php echo $rootUrlImages?>cancel.png">
			</div>
			<div class="col-md-12 editLocation" id = "dropdown" style = "display:none;">
				<div class="col-md-4 Selectbox">
					<?php
						$getCountry = file_get_contents($rootUrl."controllers/widget/getCountries.php");
						$countryJson = json_decode($getCountry, TRUE);
						$count = count($countryJson['country']);
					?>
					<select class="countrySelect form-control" name = "country" id = "country-list" onChange = "getState()">
						<option value = "" disabled selected = 'selected'>Select Country</option>
						<?php
							for($i=0; $i<$count; $i++)
							{?>
							<option value = "<?php echo $cid = $countryJson['country'][$i]['country_id']; ?>"<?php if($countryJson['country'][$i]['country_id'] == $country){ ?> selected="selected"<?php }?>><?php echo $countryJson['country'][$i]['name']; ?></option>
							<?php
							}?>
					</select>
				</div>
				<div class="col-md-4 Selectbox">
					<select name = "state" id = "state-list" class="countrySelect form-control" onChange = "getCity()" >
						<option disabled selected = 'selected' value="">Select State</option>
					</select>
				</div>
				<div class="col-md-4 Selectbox">
					<select name = "city" id = "city-list" class="countrySelect form-control" onChange = "getweather(this.id)" >
						<option disabled selected = 'selected' value = "">Select City</option>
					</select>
				</div>
			</div>
			<div class="col-md-12 widgetWeather">
				<div id = "weatherloc" class="col-md-9 location">
					<?php
						if($state == ""){
							echo $city.','.' '.$country;
						}elseif($city == ""){
							echo $country;
						}elseif($state == "" && $city == ""){
							echo $country;
						}else{
							echo $city.','.' '.$state.','.' '.$country;
						}
					?>
				</div>
				<div class="col-md-3 locationchange" >
					<button class="changeButton padding-left-none padding-right-none form-control btn btn-primary pull-right" id="change" onclick="showDiv()" >CHANGE</button>
				</div>
			</div>
			<div class="col-md-12 widgetWeather" >
				<div id = "weatherdt" class="col-md-12 daytime"></div>
			</div>
			<div class="col-md-12 widgetWeather">
				<div id = "weatherdesc" class="col-md-12 weatherdesc"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2  iconDiv" >
					<img id = "weathericon" style = "float:left; height:64px; width:64px;" src = "../../../../images/fog.png">
				</div>
				<div class="col-md-5  tempDiv">
					<div class = "temp" id = "wtempcel" ></div>
					<div class = "wob-unit" style = "float:left; font-size:22px; ">
						<span class = "temp_c" id = "weatherc" onclick = "" style = "display:inline;font-size: 20px;cursor: pointer;">&#8451;</span>
						|
						<span id = "weatherf" class = "temp_f" onclick = "myCel(1)" style = "color:blue; margin-left:-1px;font-size: 20px;cursor: pointer;">&#8457;</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5  propDiv">
					<div class = "prop" id = "phw" >
						<div style="display:none;">Precipitation: <span id = "wob_pp" >0%</span></div>
						<div>Humidity: <span id = "humidity" >%</span></div>
						<div>Wind: <span class = "wob_t" id = "windspeed" > km/hr</span></div>
					</div>
				</div>
			</div>
			<div class="col-md-12 widgetUnitButton">
				<div class="changePassBtnMainDiv row">
					<!-- <div class="closeBtnDiv">
						<button class="closeButton form-control" data-remodal-action="cancel">CLOSE</button>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<?php
}else{ //This ELSE part is for searchquery contains any city name with weather command
		if($widgetType == "weather" && $detail["types"]["weather"]>0)
      	{
			foreach ($detail["words"] as $word => $type){
				if($type == $widgetType){
					$city = $word;
					$place = countrystatename($city,$conn);
					$st_con = $place["errMsg"];
					foreach($st_con As $key => $value){
						$value1 = explode(",", $value);
						$temp = $value1[2];
					}
					$country_short = countryshortname($conn);
					foreach($country_short["errMsg"] as $key => $con_value){
						$con_value1 = explode(',', $con_value);
						if($temp == $con_value1[0]){
							$short_nm = $con_value1[1];
							$long_nm = $con_value1[0];
							$country = $long_nm;
						}else{
							$country = NULL;
						}
					}
				}
			}
		$sqd = mb_convert_case(strtolower(trim($sq)), MB_CASE_TITLE, "UTF-8");
		$cities = citylist($conn);
		$pattern = $cities["errMsg"];
		foreach($pattern as $values){
			$sqdv = mb_convert_case(strtolower(trim($values)), MB_CASE_TITLE, "UTF-8");
			if($sqdv == $sqd){
				$city = $sqdv;
			}
		}
		$place = countrystatename($city,$conn);
		$st_con = $place["errMsg"];
		if($st_con == NULL){
			$city_loc = $city;
		}else{
			foreach($st_con As $key => $value){
				$city_loc = $value;
				$value1 = explode(",", $value);
			}
		}?>
	<div class="clearfix padding-none widget_weatherdiv widgets-center" id = "forcast" style = "display:none;">
		<div class="appDivHead innerAll text-left">Weather Forecast</div>
		<div class="appDivBody text-center" style="height: auto;">
			<div class="col-md-12" id = "close" style = "display:none;">
				<img id = "closeicon" onclick="hideDiv()" style = "float:right; height:20px; width:20px;" src = "<?php echo $rootUrlImages?>cancel.png">
			</div>
			<div class="col-md-12 editLocation" id = "dropdown" style = "display:none;">
				<div class="col-md-4 Selectbox">
					<?php
						$getCountry = file_get_contents($rootUrl."controllers/widget/getCountries.php");
						$countryJson = json_decode($getCountry, TRUE);
						$count = count($countryJson['country']);
					?>
					<select name="country" id="country-list" class="countrySelect form-control" onChange="getState()">
						<option value="" disabled selected = 'selected'>Select Country</option>
							<?php for($i=0; $i<$count; $i++){?>
						<option value="<?php echo $cid = $countryJson['country'][$i]['country_id']; ?>"<?php if($countryJson['country'][$i]['country_id']==$country){ ?> selected="selected"<?php }?>><?php echo $countryJson['country'][$i]['name']; ?></option><?php } ?>
					</select>
				</div>
				<div class="col-md-4 Selectbox">
					<select name="state" id="state-list" class="countrySelect form-control"  onChange="getCity()" >
						<option value="" disabled selected = 'selected'>Select State</option>
					</select>
				</div>
				<div class="col-md-4 Selectbox">
						<select name="city" id="city-list" class="countrySelect form-control"  onChange="getweather(this.id)" >
							<option value="" disabled selected = 'selected'>Select City</option>
						</select>
				</div>
			</div>
			<div class="col-md-12 widgetWeather">
				<div id = "weatherloc" class="col-md-9 location">
					<?php echo $city_loc;?>
				</div>
				<div class="col-md-3 locationchange">
					<button class="changeButton padding-left-none padding-right-none form-control btn btn-primary pull-right" id="change" onclick="showDiv()" >CHANGE</button>
				</div>
			</div>
			<div class="col-md-12 widgetWeather">
				<div id = "weatherdt" class="col-md-12 daytime"></div>
			</div>
			<div class="col-md-12 widgetWeather">
				<div id = "weatherdesc"  class="col-md-12 weatherdesc"></div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2  iconDiv">
					<img id = "weathericon"  style = "float:left; height:64px; width:64px;" src = "../../../../images/fog.png">
				</div>
				<div class="col-md-5  tempDiv">
					<div class = "temp" id = "wtempcel" ></div>
					<div class = "wob-unit" style = "float:left; font-size:22px;">
						<span class = "temp_c" id = "weatherc" onclick = "" style = "display:inline; font-size: 20px; cursor: pointer;">&#8451;</span>
						|
						<span id = "weatherf" class = "temp_f" onclick = "myCel(1)" style = "color:blue; margin-left:-1px;font-size: 20px; cursor: pointer;">&#8457;</span>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-5  propDiv">
					<div class = "prop" id = "phw" >
						<div style="display:none;">Precipitation: <span id = "wob_pp" >0%</span></div>
						<div>Humidity: <span id = "humidity" >%</span></div>
						<div>Wind: <span class = "wob_t" id = "windspeed"> km/hr</span></div>
					</div>
				</div>
			</div>
			<div class="col-md-12 widgetUnitButton">
				<div class="changePassBtnMainDiv row">
					<!-- <div class="closeBtnDiv">
						<button class="closeButton form-control" data-remodal-action="cancel">CLOSE</button>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<?php }} ?>

<script>
	var cityName = '<?php echo $ipcity; ?>';
</script>

<script src="<?php echo $rootUrl; ?>js/widget.js"></script>

