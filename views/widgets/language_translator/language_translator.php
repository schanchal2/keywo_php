<?php
//include dependent files
// require_once("protected/controller/widget_converter.php");
// require_once("protected/controller/config.php");
error_reporting(0);
?>

<?php
//get query string from url
$sq 		= urldecode($_GET["q"]);
$sq 		= strtolower($sq);
$appStatus  = urldecode($_GET["app_status"]);
if(strpos($sq,'\\') !== false){
    $sq = str_replace("\\","",$sq);
}
if(strpos($sq,'translator:')!== false){
	$sq = str_replace("translator:","",$sq);
}
$sq = trim($sq);
//Function For Query Search
$detail = query_analyzer($sq,$conn);
$qwords = array_keys($detail["errMsg"]["words"]);
if(noError($detail)){
	$detail = $detail["errMsg"];
} else {
	printArr("Error analyzing Query");
}
$keys = array_keys($detail["types"]);
$widgetType = $keys[0];
if($widgetType == ""){
	$widgetType = $keys[1];
}
 if($sq === ""){
	$fromCode = "en";
    $toCode = "hi";
 }else{
	$fromCode = "";
	$toCode = "";
	$toCnt = strpos($sq, "to");
	if(empty($toCnt)){
		$explode_value = explode(" ", $sq);
	}else{
		$explode_value = explode("to", $sq);
	}
	for ($i=0; $i < 2; $i++) {
		$lang = trim($explode_value[$i]);
		$query = "SELECT * FROM language_translator WHERE (language like '$lang%') AND status='Yes' LIMIT 0,1";
		$result = runQuery($query, $conn);
		if(noError($result)){
			$res = array();
			while($row = mysqli_fetch_assoc($result["dbResource"]))
				$res = $row;
		}else{
			print("Error: Fetching language codes");
		}
		if($i == 0){
			$fromCode = $res['languagecode'];
		}else{
			$toCode = $res['languagecode'];
		}
	}
 }
//Get Language name and code using this function
$langList = languageList($conn);
if($appStatus != "TRUE"){
?>
	<div id="languageConverter" class="widget_language_translator padding-left-none col-xs-12">
		<div class="col-xs-6 padding-none widget_appdiv translatorDiv">
			<div class="appDivHead innerAll text-left">Language Translator</div>
			<div class="appDivBody clearfix text-center">
				<section id="tab-item-3">
					<div class="col-xs-12 widgetUnit" style="margin-bottom: 5px;">
						<div class="col-xs-6" id="translator-first" >
						<div class="col-xs-12">
							<div class="col-xs-8">
								<select class="languageSelect form-control" id="langcodeFrom" style="width: 115px;">
									<?php foreach($langList["errMsg"] as $key => $value){
											$language = explode("," , $value);
									?>
									<option value="<?php echo $language[1];?>" fullname="<?php echo $language[0]; ?>" name="xuv"
									<?php if($language[1] === $fromCode){ ?>selected<?php } ?>><?php echo $language[0]; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4 switchLang">
								<div class="col-xs-6 col-xs-offset-6">
									<div class="rms_img switch-img swap-button" id="tta_revIcon" title="Swap languages" onclick="swap()"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 tta_vc">
							 <div id="trans">
							 </div>
							<textarea class="vk_txt tw-ta tw-text-large" id="textArea" maxlength="" onKeyUp="toCount('textArea','uBann','{CHAR} characters remaining',256);" onpaste="toCount('textArea','uBann','{CHAR} characters remaining',256);" placeholder="Enter Text" style="max-height:auto" ></textarea>
						</div>
						</div>
						<div class="tw-swapa"><div class="divider"></div></div>
						<div class="col-xs-6" id="translator-second" >
						<div class="col-xs-12">
							<div class="col-xs-8">
								<select class="languageSelect form-control" id="langcodeTo" style="width:115px;" onchange="calltranslatorfunction(this.id)" >
									<?php foreach($langList["errMsg"] as $key => $value){
											$language = explode("," , $value);
									?>
									<option value="<?php echo $language[1];?>" fullname="<?php echo $language[0]; ?>" name="xuv"
									<?php if($language[1] === $toCode){ ?>selected<?php } ?>><?php echo $language[0]; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-xs-12">
							<div lang="en" contentEditable="false" data-text="Translation" class="vk_txt tw-ta tw-text-large" id="textTranslate" style="color:#AFAFAF;padding-right:0px;word-break: break-all;" >Translation</div>
						</div>
						</div>
					</div>
					<div class="col-xs-12 widgetUnitButton">
						<div class="row bottomdiv">
							<span id="uBann" class="minitext">256 characters remaining</span>
							<div class="translate-button">
                <button class="translate" onClick="calltranslatorfunction(this.id)">Translate</button>
              </div>
						</div>
					</div>
				</section>
			</div>
	  </div>
  </div>
<?php }else{	?>
  <div id="languageConverter" class="widget_language_translator padding-left-none  col-xs-12">
		<div class="col-xs-6 padding-none widget_appdiv translatorDiv">
			<div class="appDivHead innerAll text-left">Language Translator</div>
			<div class="appDivBody clearfix text-center">
				<section id="tab-item-3">
					<div class="col-xs-12 widgetUnit" style="margin-bottom: 5px;">
						<div class="col-xs-6" id="translator-first" >
						<div class="col-xs-12">
							<div class="col-xs-8">
								<select class="languageSelect form-control" id="langcodeFrom" style="width: 115px;" >
									<?php foreach($langList["errMsg"] as $key => $value){
											$language = explode("," , $value);
									?>
									<option value="<?php echo $language[1];?>" fullname="<?php echo $language[0]; ?>" name="xuv"
									<?php if($language[1] === $fromCode){ ?>selected<?php } ?>><?php echo $language[0]; ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="col-xs-4 switchLang">
								<div class="col-xs-6 col-xs-offset-6">
									<div class="rms_img switch-img swap-button" id="tta_revIcon" title="Swap languages" onclick="swap()"></div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 tta_vc">
							 <div id="trans">
							 </div>
							<textarea class="vk_txt tw-ta tw-text-large" id="textArea" maxlength="" onKeyUp="toCount('textArea','uBann','{CHAR} characters remaining',256);" onpaste="toCount('textArea','uBann','{CHAR} characters remaining',256);" placeholder="Enter Text" style="max-height:auto" ></textarea>
						</div>
						</div>
						<div class="tw-swapa"><div class="divider"></div></div>
						<div class="col-xs-6" id="translator-second" >
						<div class="col-xs-12">
							<div class="col-xs-8">
								<select class="languageSelect form-control" id="langcodeTo" style="width:115px;" onchange="calltranslatorfunction(this.id)" >
									<?php foreach($langList["errMsg"] as $key => $value){
											$language = explode("," , $value);
									?>
									<option value="<?php echo $language[1];?>" fullname="<?php echo $language[0]; ?>" name="xuv"
									<?php if($language[1] === $toCode){ ?>selected<?php } ?>><?php echo $language[0]; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-xs-12">
							<div lang="en" contentEditable="false" data-text="Translation" class="vk_txt tw-ta tw-text-large" id="textTranslate" style="word-break: break-all;color:#AFAFAF;padding-right:0px;" >Translation</div>
						</div>
						</div>
					</div>
					<div class="col-xs-12 widgetUnitButton">
						<div class="row bottomdiv">
							<span id="uBann" class="minitext">256 characters remaining</span>
							<div class="translate-button">
                <button class="translate" onClick="calltranslatorfunction(this.id)">Translate</button>
              </div>
						</div>
					</div>
				</section>
			</div>
	  </div>
  </div>
	<?php
} ?>
<style>
	[contentEditable=false]:empty:not(:focus):before{
        content:attr(data-text)
    }
</style>
<script>
    var appStatus;
    $(document).ready(function() {
        appStatus = '<?php echo $appStatus; ?>';
        $('.swap-button').click(function() {
            if(appStatus != "TRUE"){
                $('#textArea').css({"min-height": "75px"});
            }
            document.getElementById("textArea").style.overflow = 'hidden';
            document.getElementById("textArea").style.height = 0;
            document.getElementById("textArea").style.height = document.getElementById("textArea").scrollHeight + 'px';
            var languages = [];
            $('.languageSelect').each(function(i) {
                languages[i] = $(this).val();
                //alert(languages);
            });
            languages.reverse();
            $('.languageSelect').each(function(i) {
                $(this).val(languages[i]);
            });
        });
    });
</script>

<script src="<?php echo $rootUrl; ?>js/widget.js"></script>
