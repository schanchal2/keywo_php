
<?php
//include("../../layout/header.php");
error_reporting(0);


//get query string from url
$sq = rawurldecode($_GET["q"]); //echo $sq;
$appStatus = rawurldecode($_GET["app_status"]);
$sq = strtolower($sq);


if(strpos($sq, '&#039;')!== false){
    $sq = str_replace("&#039;","'",$sq);
}
if(strpos($sq, '&quot;')!== false){
    $sq = str_replace("&quot;",'"', $sq);
}
if(strpos($sq,'coins:')!== false){
    $sq = str_replace("coins:", "", $sq);
}
//Function For Query Search
$detail = query_analyzer($sq,$conn);
if(noError($detail)){
    $detail = $detail["errMsg"];
} else {
    printArr("Error analyzing Query");
}
$qwords = array_keys($detail["errMsg"]["words"]);
foreach(array_keys($qwords) AS $k ){
    $str = $qwords[$k];
}
$keys = array_keys($detail["types"]);
$widgetType = $keys[0];
if($widgetType === ""){
    $widgetType = $keys[0];
}

//Fetch Cryptp currency data from URL
$jsonMainDataUrl = '../../crypto_json/currencyData.json';
$json_data = json_decode(file_get_contents($jsonMainDataUrl),true);
unset($id,$name,$symbol,$rank,$price_usd,$one_day_volume_usd,$market_cap_usd,$available_supply,$percent_change_1h,$percent_change_24h,$percent_change_7d);

if($sq === ""){
$currancyType = 'bitcoin';
for($i = 0; $i <= count($json_data); $i++){
    $name = $json_data[$i]['name'];
    $symbol = $json_data[$i]['symbol'];
    if(strtolower($name) == $currancyType || (strtolower($symbol) == $currancyType) ){
        $id = $json_data[$i]['id'];
        $rank = $json_data[$i]['rank'];
        $price_usd = empty($json_data[$i]['price_usd']) ? 0 : $json_data[$i]['price_usd'];
        $one_day_volume_usd = empty($json_data[$i]['24h_volume_usd']) ? 0 : $json_data[$i]['24h_volume_usd'];
        $market_cap_usd = empty($json_data[$i]['market_cap_usd']) ? 0 : $json_data[$i]['market_cap_usd'];
        $available_supply = empty($json_data[$i]['available_supply']) ? 0 : $json_data[$i]['available_supply'];
        $percent_change_1h = empty($json_data[$i]['percent_change_1h']) ? 0 : $json_data[$i]['percent_change_1h'];
        $percent_change_24h = empty($json_data[$i]['percent_change_24h']) ? 0 : $json_data[$i]['percent_change_24h'];
        $percent_change_7d = empty($json_data[$i]['percent_change_7d']) ? 0 : $json_data[$i]['percent_change_7d'];
        break;
    }
}
$imagePathAbs = 'http://coinmarketcap.com/static/img/coins/16x16/'.$id.'.png';
$one_hr_css = '';
if($percent_change_1h < 0){
    $one_hr_css ='red';
}else{
    $one_hr_css ='green';
}
$percent_change_24h_css = '';
if($percent_change_24h < 0){
    $percent_change_24h_css ='red';
}else{
    $percent_change_24h_css ='green';
}
$percent_change_7d_css = '';
if($percent_change_7d < 0){
    $percent_change_7d_css ='red';
}else{
    $percent_change_7d_css ='green';
}

//starts Function for Asorting JSON Array by specified name
function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();
    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }
        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }
        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }
    return $new_array;
}
// end the asorting function
$json = file_get_contents('../../crypto_json/currencyData.json');
$data1 = json_decode($json,true);
$data = array_sort($data1, 'id', SORT_ASC);
?>

<div class="row inner-2x innerMT margin-left-none margin-right-none">
    <div class="crypto_currency_main_widget">
        <div class="widget_cryptodiv">
            <div class="container-price">
                <div class="mainhead">
                    <span class="headerMain">ST Crypto Currency Market Data</span><span class="searchBut innerMR pull-right"><i class="fa fa-search" aria-hidden="true"></i></span>
                </div>

                <div id="searchtoogle">
                    <select id="currencySearch" name="currencySearch" class="selectpicker" data-live-search="true" onchange="ajaxCall();" >
                        <?php $name = empty($name) ? 'Bitcoin' : $name;
                        foreach($data as $LatestBlock)
                        {?>
                            <option value="<?php echo (''.$LatestBlock['name'].' ('.$LatestBlock['symbol'].')'); ?>" <?php if($LatestBlock['name'] == $name) {echo 'SELECTED'; }?> ><?php echo (''.$LatestBlock['name'].' ('.$LatestBlock['symbol'].')'); ?></option>
                            <?php
                        }?>
                    </select>
                </div>
                <div class="mainPrice">

                    <div class="row marg_padd">
                        <div class="col-sm-5 margMain" >
                            <div class="col-sm-2 marg" ><img id="image" src="<?php echo $imagePathAbs; ?>" /></div>
                            <div class="col-sm-9 marg brac" ><span id="name_head" class="heading"><?php echo $name; ?></span>(<span id="shortCode" class="shortName" ><?php echo $symbol; ?></span>)</div>
                        </div>
                        <div class="marg1 col-sm-7">
        <span  id="curPrice" class="curtPrice">
        <a href="javascript:;" onclick="convertPriceBtc('BTC');" class="adjFont" origPrice1="<?php echo (number_format($price_usd,2)." USD"); ?>"><?php echo (number_format($price_usd,2)." USD"); ?></a>
        </span>
                        </div>
                    </div>
                    <div class="row marg_padd2">
                        <div class="col-sm-4 marg1">
                            <div class="col-sm-5  timed background-transparent"> 1 Hour</div>
                            <div id="oneHour" class="col-sm-7 red">-0.01 %</div>
                        </div>
                        <div class="col-sm-4 marg1">
                            <div class="col-sm-5  timed background-transparent"> 24 Hour</div>
                            <div id="oneDay" class="col-sm-7 green">1.21 %</div>
                        </div>
                        <div class="col-sm-4 marg1">
                            <div class="col-sm-5 timed background-transparent"> 7 Day</div>
                            <div id="sevenDay" class="col-sm-7 green">0.6 %</div>
                        </div>
                    </div>
                    <div class="row marg_padd3">
                        <div class="col-sm-4 marg3">
                            <div class="col-sm-12 timed2">Market Cap</div>
                            <div class="col-sm-12 colorHigh">
                <span id="market" class="colorHigh">
                  <a href="javascript:;" onclick="convertPriceBtc('BTC');" origprice1="<?php echo (number_format($market_cap_usd,2)." USD"); ?>"><?php echo (number_format($market_cap_usd,2)." USD"); ?></a>
                </span>
                            </div>
                        </div>

                        <div class="col-sm-4 marg3">
                            <div class="col-sm-12 timed2">Volume (24h)</div>
                            <div class="col-sm-12 colorHigh">
                <span id="volume24" class="colorHigh">
                  <a href="javascript:;" onclick="convertPriceBtc('BTC');" origprice1="<?php echo (number_format($one_day_volume_usd,2)." USD"); ?>"><?php echo (number_format($one_day_volume_usd,2)." USD"); ?></a>
                </span>
                            </div>
                        </div>
                        <div class="col-sm-4 marg3">
                            <div class="col-sm-12 timed2">Available Supply</div>
                            <div class="col-sm-12 colorHigh" id="available"><?php echo (number_format($available_supply)); ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            }else{
                if($widgetType === "coins" && $detail["types"][$widgetType]>=1 || $widgetType === "coins:"){
                    foreach ($detail["words"] as $word => $type){
                        if($type === $widgetType){
                            $currancyType = $word;
                            for($i = 0; $i <= count($json_data); $i++){
                                $name = $json_data[$i]['name'];
                                $symbol = $json_data[$i]['symbol'];
                                if( (strtolower($name) == $currancyType) || (strtolower($symbol) == $currancyType) ){
                                    $id = $json_data[$i]['id'];
                                    $rank = $json_data[$i]['rank'];
                                    $price_usd = empty($json_data[$i]['price_usd']) ? 0 : $json_data[$i]['price_usd'];
                                    $one_day_volume_usd = empty($json_data[$i]['24h_volume_usd']) ? 0 : $json_data[$i]['24h_volume_usd'];
                                    $market_cap_usd = empty($json_data[$i]['market_cap_usd']) ? 0 : $json_data[$i]['market_cap_usd'];
                                    $available_supply = empty($json_data[$i]['available_supply']) ? 0 : $json_data[$i]['available_supply'];
                                    $percent_change_1h = empty($json_data[$i]['percent_change_1h']) ? 0 : $json_data[$i]['percent_change_1h'];
                                    $percent_change_24h = empty($json_data[$i]['percent_change_24h']) ? 0 : $json_data[$i]['percent_change_24h'];
                                    $percent_change_7d = empty($json_data[$i]['percent_change_7d']) ? 0 : $json_data[$i]['percent_change_7d'];
                                    break;
                                }
                            }
                            $imagePathAbs = 'http://coinmarketcap.com/static/img/coins/16x16/'.$id.'.png';
                            $one_hr_css = '';
                            if($percent_change_1h < 0){
                                $one_hr_css ='red';
                            }else{
                                $one_hr_css ='green';
                            }
                            $percent_change_24h_css = '';
                            if($percent_change_24h < 0){
                                $percent_change_24h_css ='red';
                            }else{
                                $percent_change_24h_css ='green';
                            }
                            $percent_change_7d_css = '';
                            if($percent_change_7d < 0){
                                $percent_change_7d_css ='red';
                            }else{
                                $percent_change_7d_css ='green';
                            }

                            //starts Function for Asorting JSON Array by specified name
                            function array_sort($array, $on, $order=SORT_ASC)
                            {
                                $new_array = array();
                                $sortable_array = array();
                                if (count($array) > 0) {
                                    foreach ($array as $k => $v) {
                                        if (is_array($v)) {
                                            foreach ($v as $k2 => $v2) {
                                                if ($k2 == $on) {
                                                    $sortable_array[$k] = $v2;
                                                }
                                            }
                                        } else {
                                            $sortable_array[$k] = $v;
                                        }
                                    }
                                    switch ($order) {
                                        case SORT_ASC:
                                            asort($sortable_array);
                                            break;
                                        case SORT_DESC:
                                            arsort($sortable_array);
                                            break;
                                    }
                                    foreach ($sortable_array as $k => $v) {
                                        $new_array[$k] = $array[$k];
                                    }
                                }
                                return $new_array;
                            }
                            // end the asorting function
                            $json = file_get_contents('crypto_json/currencyData.json');
                            $data1 = json_decode($json,true);
                            $data = array_sort($data1, 'id', SORT_ASC);
                        }
                    }?>
                    <div class="container-price">
                        <!-- search dailog on -->
                        <div class="mainhead">
                            <span class="headerMain">ST Crypto Currency Market Data</span><span class="searchBut"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                        <div id="searchtoogle" >
                            <select id="currencySearch" name="currencySearch" class="selectpicker" data-live-search="true" onchange="ajaxCall();" >
                                <?php $name = empty($name) ? 'Bitcoin' : $name;
                                foreach($data as $LatestBlock)
                                {?>
                                    <option value="<?php echo (''.$LatestBlock['name'].' ('.$LatestBlock['symbol'].')'); ?>" <?php if($LatestBlock['name'] == $name) {echo 'SELECTED'; }?> ><?php echo (''.$LatestBlock['name'].' ('.$LatestBlock['symbol'].')'); ?></option>
                                    <?php
                                }?>
                            </select>
                        </div>
                        <!---search dailog result..-->
                        <div class="mainPrice">
                            <div class="row marg_padd">
                                <div class="col-sm-5 margMain" >
                                    <div class="col-sm-2 marg" ><img id="image" src="<?php echo $imagePathAbs; ?>" /></div>
                                    <div class="col-sm-9 marg brac" ><span id="name_head" class="heading"><?php echo $name; ?></span>(<span id="shortCode" class="shortName" ><?php echo $symbol; ?></span>)</div>
                                </div>
                                <div class="marg1 col-sm-7">
        <span  id="curPrice" class="curtPrice">
        <a href="javascript:;" onclick="convertPriceBtc('BTC');" class="adjFont" origPrice1="<?php echo (number_format($price_usd,2)." USD"); ?>"><?php echo (number_format($price_usd,2)." USD"); ?></a>
        </span>
                                </div>
                            </div>
                            <div class="row marg_padd2">
                                <div class="col-sm-4 marg1">
                                    <div class="col-sm-5  timed"> 1 Hour</div>
                                    <div  id="oneHour" class="col-sm-7 <?php echo $one_hr_css; ?>" ><?php echo $percent_change_1h; ?> %</div>
                                </div>
                                <div class="col-sm-4 marg1" >
                                    <div class="col-sm-5  timed"> 24 Hour</div>
                                    <div  id="oneDay" class="col-sm-7 <?php echo $percent_change_24h_css; ?>" ><?php echo ($percent_change_24h); ?> %</div>
                                </div>
                                <div class="col-sm-4 marg1">
                                    <div class="col-sm-5 timed"> 7 Day</div>
                                    <div  id="sevenDay" class="col-sm-7 <?php echo $percent_change_7d_css ; ?>" ><?php echo $percent_change_7d ; ?> %</div>
                                </div>
                            </div>
                            <div class="row marg_padd3">
                                <div class="col-sm-4 marg3" >
                                    <div class="col-sm-12 timed2" >Market Cap</div>
                                    <div   class="col-sm-12 colorHigh" >
          <span id="market" class="colorHigh">
            <a href="javascript:;" onclick="convertPriceBtc('BTC');" origPrice1="<?php echo (number_format($market_cap_usd,2)." USD"); ?>"><?php echo (number_format($market_cap_usd,2)." USD"); ?></a>
          </span>
                                    </div>
                                </div>
                                <div class="col-sm-4 marg3" >
                                    <div class="col-sm-12 timed2" >Volume (24h)</div>
                                    <div  class="col-sm-12 colorHigh" >
          <span id="volume24" class="colorHigh" >
            <a  href="javascript:;" onclick="convertPriceBtc('BTC');" origPrice1="<?php echo (number_format($one_day_volume_usd,2)." USD"); ?>"><?php echo (number_format($one_day_volume_usd,2)." USD"); ?></a>
          </span>
                                    </div>
                                </div>
                                <div class="col-sm-4 marg3" >
                                    <div class="col-sm-12 timed2" >Available Supply</div>
                                    <div  class="col-sm-12 colorHigh" id="available"><?php echo (number_format($available_supply)); ?></div>
                                </div>
                            </div>

                        </div>
                    </div>
                <?php }
            }


            ?>

            <script>

                var appStatus;
                $(document).ready(function(){
                    /*$('.selectpicker').selectpicker({
                     size: 5,
                     width: '100%'
                     });*/
                    $(".searchBut").click(function(){
                        $("#searchtoogle").toggle(60);
                    });
                    appStatus = "<?php echo $appStatus ; ?>"; //alert(appStatus);
                    if(appStatus == "TRUE"){
                        adjFont();
                    }
                });

            </script>

            <script src="<?php echo $rootUrl; ?>js/widget.js"></script>
