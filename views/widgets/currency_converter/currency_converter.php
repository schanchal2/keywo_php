<?php
session_start();
	//include dependent files
	//require_once("protected/controller/widget_converter.php");
	error_reporting(0);?>

	<?php
	//get query parameter from url
	$email 		  = $_SESSION["email"]; //echo $email;
	$defualt_curr = urldecode($_GET["currentCurrPref"]); //echo $defualt_curr;
	$sq 		  = urldecode($_GET["q"]);//echo $sq;
	
	$defualt_curr = explode("/", $defualt_curr);
	$defualt_curr = $defualt_curr[0];



	if(substr_count($sq, '.')>1){
		$firstOccur = strpos($sq, ".");
		$firststr = str_replace(".", "", $sq);
		$sq = substr($sq, 0, $firstOccur).".".substr($firststr, $firstOccur, strlen($firststr)-1);
	}
	if(strpos($sq,'CURRENCY:')!== false){
		$sq = str_replace("CURRENCY:", "currency:", $sq);
	}
	if(strpos($sq,'\\')!== false){
		$sq = str_replace("\\", "", $sq);
	}
	if(strpos($sq,'/')!== false){
		$sq = str_replace("/", "", $sq);
	}
	//Function For Query Search of widget
	$detail = query_analyzer($sq, $conn);
	$qwords = array_keys($detail["errMsg"]["words"]);
	$qtime = 1;
	$secondcurr = ''; //echo "<br>". "chanchal".$secondcurr;
	foreach(array_keys($qwords) AS $k ){
		$str = $qwords[$k];
		if(strpos($str,'.') !== false ){
			$qtime = $str;
		}
		if(strpos($qtime,'.') !== false) {
			$x = explode('.', $qtime);
			preg_match("/(\\d+)([a-zA-Z]+)/",$x[1], $matches);
			if($matches[1] != ""){
				$first = $x[0];
				$sec = $matches[1];
				$op=$first .".".$sec;
				$qint = (int)$op;
				$join = gettype($qint);
				$secondcurr = $matches[2];
				$qtime = $op;
				//echo "<br>". "Testingif".$secondcurr;
			}
		}
		preg_match("/(\\d+)([a-zA-Z]+)/", $str, $matches);
		if($matches[1] != ""){
			$qint = (int)$matches[1];
			$join = gettype($qint);
			$secondcurr = $matches[2];
			//echo "<br>". "Testingelse".$secondcurr;
		}
		if($join == ""){
			$this_value = $detail["errMsg"]["words"][$qwords[$k]];
			if($this_value == ""){
				$temp = gettype($qwords[$k]);
				if($temp == integer){
					$qtime = $qwords[$k];
				}
			}
		}elseif($join == integer){
			if($matches[1]!=""){
				$qint = (int)$matches[1];
				$join = gettype($qint);
				$qtime = $qint;
				$secondcurr = $matches[2]; //echo "<br>". "Testingelseif".$secondcurr;
			}else{
				preg_match("/(\\d+)([a-zA-Z]+)/",$x[1], $matches);
				if($matches[1] != ""){
					$first = $x[0];
					$sec = $matches[1];
					$op=$first .".".$sec;
					$qint = (int)$op;
					$join = gettype($qint);
					$secondcurr = $matches[2];
					$qtime = $op;
				}
			}
		}
	}
	if(noError($detail)){
		$detail = $detail["errMsg"];
	} else {
		printArr("Error analyzing Query");
	}

	$keys = array_keys($detail["types"]);
	$widgetType = $keys[0];
	if($widgetType === ""){
		$widgetType = $keys[1];
	}
	if($secondcurr!==''){

		if($widgetType == "currency"){
			$fromCurrCode = $secondcurr; echo "$fromCurrCode";
			$toCurrCode = "";
			$amount = $qtime;
			foreach ($detail["words"] as $word => $type) {
				if($type === $widgetType){
					if(empty($fromCurrCode)){
						$fromCurrCode = $word;
					} else if(empty($toCurrCode)){
						$toCurrCode = $word;
					} else {
						break;
					}
				}
			}

			//Function to get currency convert Amount of given currency code
			$check_currency = convertCurrency($fromCurrCode, $toCurrCode, $amount, $conn);
//	echo "Testing";
//			print_r($check_currency);die;
			//Function to get currency (LongForm) name
			$curr1 = currency_longformCheck($fromCurrCode, $conn);
			$curr2 = currency_longformCheck($toCurrCode, $conn);
			$split = explode(" ",$check_currency["errMsg"]);
			$r_amount = round($split["0"],"3");
			$d_amount = $r_amount;

			//Function to get currency list
			$curr_list = currencylist($conn);
?>



<div onload="callphpCurrfunction();" class="row inner-2x innerMT margin-left-none margin-right-none">
  <div class="currency_converter_main_widget widgets-center">
    <div class="widget_currdiv padding-none">
			<div class="appDivHead text-left innerAll">Currency Converter</div>
				<div class="appDivBody text-center">
					<section id="tab-item-1">
					<div class="row">
						<div class="col-xs-12 widgetCurrency">
							<div class="col-xs-12">
								<div id = "cur_from">
									<span id="amt" class="firstCurramt"><?php echo $amount; echo " ";?></span>
									<span id="from_curr" class="firstCurrname"><?php echo ($curr1["errMsg"]["longforms"]);?></span>
								</div>
								<div id = "cur_to">
									<span id="d_amt" class="secCurramt text-blue"><?php echo $d_amount; echo " "; ?></span>
									<span id="to_curr" class="secCurrname"><?php echo ($curr2["errMsg"]["longforms"]);?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 editCurrencyContainerDiv">
						<div class="col-md-6">
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" onkeypress="return isNumberKey(event)" onchange="callphpCurrfunction(this.id)" type="number" flag ="1" value="<?php echo $amount;?>" id="changeCurr" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="changeCurr"></label>
							</span>
						</div>
						<div class="col-md-6 Selectbox">
							<select class="currencySelect form-control" id="currcode" onchange="callphpCurrfunction(this.id)" flag ="3" xy="1" >
								<?php foreach($curr_list["errMsg"] as $key => $value){
										$e_curr = explode("," , $value);
								?>
							    <option value="<?php echo $e_curr[1];?>" fullname="<?php echo $e_curr[0]; ?>" name="xuv"
								<?php if($curr1["errMsg"]["longforms"] === $e_curr[0]){ ?>selected<?php } ?>><?php echo $e_curr[0]; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-12 editCurrencyContainerDiv">
						<div class="col-md-6 edit-currency-input-div">
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" id="newchangeCurr" onkeypress="return isNumberKey(event)" onchange="callphpCurrfunction(this.id)" flag ="4"  type="number" value="<?php Print $d_amount; ?>" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="newchangeCurr"></label>
							</span>
						</div>
						<div class="col-md-6 Selectbox">
							<select class="currencySelect form-control" id="currcode2" onchange="callphpCurrfunction(this.id)" flag ="2" xy="4">
								<?php foreach($curr_list["errMsg"] as $key => $value){
										$e_curr = explode("," , $value);
								?>
								<option value="<?php echo $e_curr[1];?>" fullname="<?php echo $e_curr[0]; ?>" name="xuv"
								<?php if($curr2["errMsg"]["longforms"] === $e_curr[0]){ ?>selected<?php } ?>><?php echo $e_curr[0]; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>



					<div class="col-xs-12 widgetUnitButton">
						<div class="changePassBtnMainDiv row">
							<div class="closeBtnDiv">
								<button class="closeButton form-control" data-remodal-action="cancel">CLOSE</button>
							</div>
						</div>
					</div>
				</section>
				</div>
			<div class="appDivFoot text-center"></div>
		</div>
  </div>

  <?php
		}

	}else if( ($widgetType === "currency" || $widgetType === "coins") && $detail["types"]["currency"]>0){

		$fromCurrCode = "";
		$toCurrCode = "";
		$amount = $qtime;
		$widgetType = "currency";
		foreach ($detail["words"] as $word => $type) {
			if( ($type == $widgetType) || ($type == 'coins') ){
				if(empty($fromCurrCode)){
					$fromCurrCode = $word;
				} else if(empty($toCurrCode)){
					$toCurrCode = $word;
				} else{
					break;
				}
			}
     	}

    	if($fromCurrCode === ""){
     		$fromCurrCode = "BTC";
     	}
		if(isset($_SESSION["email"])){
			$login_status = 1;
		}else{
			$login_status = 0;
		}
		if($login_status === 1){
			if($toCurrCode === ""){
				$toCurrCode = $defualt_curr;
				//echo "<br>";echo $toCurrCode;
			}
		}
		if($login_status === 0){
			if($toCurrCode === ""){
				$toCurrCode = "USD";
			}
		}

		//Function for conversion of currency to get amount
		$check_currency = convertCurrency($fromCurrCode, $toCurrCode, $amount, $conn);

		//set longforms of currency code using this function
		$curr1 = currency_longformCheck($fromCurrCode,$conn);

		//set longforms of currency code using this function
		$curr2 = currency_longformCheck($toCurrCode,$conn);

		$split = explode(" ", $check_currency["errMsg"]);

		$r_amount = round($split["0"], "3");
		$d_amount = $r_amount;

		//set all currency code name list
		$curr_list = currencylist($conn); 	?>

  <div id="currencyConverter" class="currency_converter_main_widget widgets-center currencyDiv inner-2x innerB">
		<div class="padding-none widget_currdiv">
			<div class="appDivHead innerAll text-left">Currency Converter</div>
				<div class="appDivBody text-center clearfix" style="height: auto;">
					<section id="tab-item-1">
					<div class="col-xs-12 padding-none">
						<div class="col-md-12 widgetCurrency">
							<div class="col-md-12">
								<div id = "cur_from">
									<span id="amt" class="firstCurramt"><?php echo $amount; echo " ";?></span>
									<span id="from_curr" class="firstCurrname"><?php echo ($curr1["errMsg"]["longforms"]);?></span><?php echo " equal to "; ?>
								</div>
								<div id = "cur_to">
									<span id="d_amt" class="secCurramt"><?php echo $d_amount; echo " "; ?></span>
									<span id="to_curr" class="secCurrname"><?php echo ($curr2["errMsg"]["longforms"]);?></span>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 padding-none editCurrencyContainerDiv">
						<div class="col-md-6 padding-none currvalue">
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" onkeypress="return isNumberKey(event)" onchange="callphpCurrfunction(this.id)" type="number" flag ="1" value="<?php echo $amount;?>" id="changeCurr" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="changeCurr"></label>
							</span>
						</div>
						<div class="col-md-6 Selectbox">
							<select class="currencySelect form-control" id="currcode" onchange="callphpCurrfunction(this.id)" flag ="3" xy="1">
								<?php foreach($curr_list["errMsg"] as $key => $value){
										$e_curr = explode("," , $value);
								?>
							    <option value="<?php echo ($e_curr[1]);?>" fullname="<?php echo ($e_curr[0]); ?>" name="xuv"
								<?php if($curr1["errMsg"]["longforms"] === $e_curr[0]){ ?>selected<?php } ?>><?php echo ($e_curr[0]); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-12 padding-none editCurrencyContainerDiv">
						<div class="col-md-6 padding-none currvalue" >
							<span class="input input--hoshi">
							<input class="input__field input__field--hoshi" id="newchangeCurr"  onkeypress="return isNumberKey(event)" onchange="callphpCurrfunction(this.id)" flag ="4"  type="number" value="<?php Print $d_amount; ?>" />
							<label class="input__label input__label--hoshi input__label--hoshi-color-1" for="newchangeCurr"></label>
							</span>
						</div>
						<div class="col-md-6 Selectbox">
							<select class="currencySelect form-control" id="currcode2" onchange="callphpCurrfunction(this.id)" flag ="2" xy="4">
								<?php foreach($curr_list["errMsg"] as $key => $value){
										$e_curr = explode("," , $value);
								?>
								<option value="<?php echo ($e_curr[1]);?>" fullname="<?php echo ($e_curr[0]); ?>" name="xuv"
								<?php if($curr2["errMsg"]["longforms"] === $e_curr[0]){ ?>selected<?php } ?>><?php echo ($e_curr[0]); ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
<!--					<div class="col-md-12 widgetUnitButton">-->
<!--						<div class="changePassBtnMainDiv row">-->
<!--							<div class="closeBtnDiv">-->
<!--								<button class="closeButton form-control" data-remodal-action="cancel">CLOSE</button>-->
<!--							</div>-->
<!--						</div>-->
<!--					</div>-->
				</section>
				</div>
			<div class="appDivFoot text-center"></div>
		</div>
	</div>
	<?php }?>

	<script src="<?php echo $rootUrl; ?>js/widget.js"></script>
