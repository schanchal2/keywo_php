<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
	print("<script>");
    print("var t = setTimeout(\"window.location='../prelogin/index.php';\", 000);");
    print("</script>");
    die;
}

require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");
$transction_id = isset($_GET['transction_id'])?$_GET['transction_id']:'';
$transaction_boundary = isset($_GET['transaction_boundary'])?$_GET['transaction_boundary']:'';
$email = $_SESSION["email"];
$userRequiredFields = $userRequiredFields.",email,account_handle";

//fetch details of sender and receiver                  	
$userDetails = getUserInfo($email, $walletURLIP . 'api/v3/', $userRequiredFields);
$userDetails = $userDetails['errMsg'];

$dateTime = new DateTime(); 
$timezone = $dateTime->format('T');
$seconds = date_offset_get(new DateTime);
$minutes = $seconds/60;
$offset = ($minutes >= 0)?"+".$minutes:$minutes;

$result = getTransctionsDetails($userDetails['email'],$userDetails['_id'],$transction_id,$transaction_boundary);
 // printArr($result);
$tranDetails = $result['errMsg']; 
if($email != $tranDetails['sender']){ 
	$transType = "Received";
	 }else if($email == $tranDetails['sender']){ 
$transType = "Sent"; }else{
	$transType = $tranDetails['type'];
} 
?>
<?php if($tranDetails['type'] == 'paymentrequest'){ ?>	
 
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-Text-Primary-Blue"><?php echo $transType; ?></h4> </div>

<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Quantity :</div>
    <div class="col-sm-9 text-Blue margin-none h4">
        <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $tranDetails['amount']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$tranDetails['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
</div><div class="row innerMTB">
    
<?php if($email != $tranDetails['sender']){ 
	$receiverDesc = sprintf($descriptionArray['reciver_desc'], number_format($tranDetails['amount'],4), $keywoDefaultCurrencyName, $tranDetails['sender']);
	$description = $receiverDesc;
	?>
<div class="col-sm-3 text-Blue">From :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['sender']; ?></div>
<?php }else{ 
$senderDesc = sprintf($descriptionArray['sender_desc'], number_format($tranDetails['amount'],4), $keywoDefaultCurrencyName, $tranDetails['receiver']);
$description = $senderDesc;
	?>
<div class="col-sm-3 text-Blue">To :</div>
	<div class="col-sm-9 text-Gray" ><?php echo $tranDetails['receiver']; ?></div>
<?php } ?>	
    
</div>
<!-- <div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Country :</div>
    <div class="col-sm-9 text-Gray">India</div>
</div> -->
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Description :</div>
    <div class="col-sm-9 text-Gray"><?php echo $description; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Status :</div>
    <div class="col-sm-9 text-orange"><?php echo  ucfirst($tranDetails['transaction_status']); ?> <i class="fa fa-circle"></i>   </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Date  :</div>
    <div class="col-sm-9 text-Gray"><?php echo date("jS M, Y H:i a",$tranDetails['time']/1000)." GMT {$offset}({$timezone})"; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">IP :</div>
    <div class="col-sm-9 text-Gray"><?php echo $tranDetails['origin_ip']; ?></div>
</div>
<?php if(isset($tranDetails['note'])){ ?>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Note :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $tranDetails['note']; ?> </div>
</div>
<?php } ?>
<div class=" innerAll clearfix ">
<?php if($email != $tranDetails['sender'] && $tranDetails['transaction_status'] == 'pending'){ ?>
		<button type="button" id="reject-btn" class="btn btn-primary pull-right btn-trading-dark innerMR" onclick="rejectITDRequest('<?php echo urlencode(json_encode($result)); ?>','<?php echo $transction_id; ?>','<?php echo $tranDetails['amount']; ?>');" >Reject</button>
	<button type="button" id="accept-btn" class="btn btn-primary pull-right btn-trading innerMR" onclick="acceptITDRequest('<?php echo urlencode(json_encode($result)); ?>','<?php echo $transction_id; ?>','<?php echo $tranDetails['amount']; ?>');">Accept</button>
		<?php } ?>
</div>
<?php }else{ ?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title text-Text-Primary-Blue"><?php echo $transType; ?></h4> </div>

<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Quantity :</div>
    <div class="col-sm-9 text-Blue margin-none h4"><span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo $tranDetails['amount']." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)$tranDetails['amount'], 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                      </span></div>
</div><div class="row innerMTB">
    
<?php if($email != $tranDetails['sender']){ 
	$receiverDesc = sprintf($descriptionArray['reciver_desc'], number_format($tranDetails['amount'],4), $keywoDefaultCurrencyName, $tranDetails['sender']);
	$description = $receiverDesc;
	?>
<div class="col-sm-3 text-Blue">From :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['sender']; ?></div>
<?php }else{ 
$senderDesc = sprintf($descriptionArray['sender_desc'], number_format($tranDetails['amount'],4), $keywoDefaultCurrencyName, $tranDetails['receiver']);
$description = $senderDesc;
	?>
<div class="col-sm-3 text-Blue">To :</div>
	<div class="col-sm-9 text-Gray"><?php echo $tranDetails['receiver']; ?></div>
<?php } ?>	
    
</div>
<!-- <div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Country :</div>
    <div class="col-sm-9 text-Gray">India</div>
</div> -->
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Description :</div>
    <div class="col-sm-9 text-Gray"><?php echo $description; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Status :</div>
    <div class="col-sm-9 text-orange"><?php echo ucfirst($tranDetails['transaction_status']); ?> <i class="fa fa-circle"></i>   </div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Date  :</div>
    <div class="col-sm-9 text-Gray"><?php echo date("jS M, Y H:i a",$tranDetails['time']/1000)." GMT {$offset}({$timezone})"; ?></div>
</div>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">IP :</div>
    <div class="col-sm-9 text-Gray"><?php echo $tranDetails['origin_ip']; ?></div>
</div>
<?php if(isset($tranDetails['meta_details']['note'])){ ?>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue">Note :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $tranDetails['meta_details']['note']; ?> </div>
</div>
<?php } ?>
<?php if(isset($tranDetails['meta_details']['reason'])){ ?>
<div class="row innerMTB">
    <div class="col-sm-3 text-Blue" style="word-wrap: break-word;">Accept Note :</div>
    <div class="col-sm-9 text-Gray" style="word-wrap: break-word;"> <?php echo $tranDetails['meta_details']['reason']; ?> </div>
</div>
<?php } ?>

<?php } ?>
