<?php
session_start();

if(!isset($_SESSION["email"]) || empty($_SESSION["email"])){
    echo "Authorisation failure";
    exit;
}

require_once("../../config/config.php");
require_once("../../config/db_config.php");
require_once("../../helpers/errorMap.php");
require_once("../../helpers/coreFunctions.php");
require_once("../../models/keywords/userCartModel.php");
require_once("../../models/search/searchResultModel.php");
require_once("../../models/header/headerModel.php");
require_once "../../helpers/arrayHelper.php";
require_once('../../helpers/walletHelper.php');
require_once "../../helpers/stringHelper.php";
require_once("../../models/wallet/walletModel.php");
require_once("../../models/wallet/walletCashoutModel.php");

error_reporting(0);
//connect to keywords database
$conn = createDBConnection("dbkeywords");
if(noError($conn)){
    $returnArr = array();
    $conn = $conn["connection"];
}

$userEmail = $_SESSION['email'];
$olderTime = $_GET['backward_cursor'];
$newerTime = $_GET['forword_cursor'];
$status = $_GET['status'];
// $newerTime = "2017-03-16 15:53:23";
$cashoutRequests = getAllCashoutRequest($conn,$userEmail,$status,$olderTime,$newerTime);
//printArr($cashoutRequests); die;

?>
<div class="row">
    <div class="col-xs-12">
        <div class="table-responsive wallet__trasactionHistory text-Light-Black innerT inner-2x">
            <table class="table table-hover border-all text-center ">
                <thead class="text-White ">
                    <tr>
                        <th> Quantity </th>
                        <th>Discription </th>
                        <th>
                            <label class="dropdownOptions ">
                                <select class="selectpicker12" id="tr-status" onchange="getsortedRequests(this.value);" style="background-color: #90a1a9;border: none;">
                                    <option selected disabled>Status</option>
                                    <option value="" <?php if($status==""){ echo "selected"; }?>>All</option>
                                    <!-- <option value="pending" <?php //if($status=="pending" || $status=="pending_manual_verification"){ echo "selected"; }?>>Pending</option> -->
                                    <option value="pending" <?php if(strlen(strstr(strtolower(trim($status)), 'pending')) >0){ echo "selected"; }?>>Pending</option>
                                    <option value="hold" <?php if(strlen(strstr(strtolower(trim($status)), 'hold')) >0){ echo "selected"; }?>>Hold</option>
                                    <option value="processing" <?php if($status=="processing"){ echo "selected"; }?>>Inprogress</option>
                                    <option value="approved_tbc" <?php if($status=="approved_tbc"){ echo "selected"; }?>>Verified</option>
                                    <!-- <option value="rejected" <?php //if($status=="rejected"){ echo "selected"; }?>>Rejected</option> -->
                                    <option value="rejected" <?php if(strlen(strstr(strtolower(trim($status)), 'rejected')) >0){ echo "selected"; }?>>Rejected</option>
                                </select>
                            </label>
                        </th>
                        <th>Date </th>
                        <th> </th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php


                    foreach($cashoutRequests['request_container'] as $requests){ ?>
                        <?php  
                            if(trim($requests['status']) =='approved_tbc'){
                                $requestStatus = 'Verified';
                            //}elseif(trim($requests['status']) =='pending_manual_verification'){
                            }elseif(strlen(strstr(strtolower(trim($requests['status'])), 'pending_'))>0){
                                $requestStatus = 'Pending for Manual Verification';
                            }
                            elseif(strlen(strstr(strtolower(trim($requests['status'])), 'hold'))>0 ){
                                $requestStatus = 'Hold for Manual Verification';
                            }
                            elseif(strlen(strstr(strtolower(trim($requests['status'])), 'reject'))>0 ){
                                $requestStatus = 'Rejected';
                            }
                            else{
                                $requestStatus = $requests['status'];
                            }



                        ?>
                        <tr>
                            <td class="text-Dark-Blue text-left"> <span onclick="convertPrice('<?php echo $_SESSION['CurrPreference']; ?>');" origPrice="<?php echo ($requests['payment_amount']+$requests['cashout_fees'])." ".$keywoDefaultCurrencyName; ?>"><?php echo number_format((float)($requests['payment_amount']+$requests['cashout_fees']), 4, '.', '')." ".$keywoDefaultCurrencyName; ?>
                                </span> </td>
                            <td class="text-grayscale-4"> Cashout request of <?php echo ($requests['payment_amount']+$requests['cashout_fees']); ?> <?php echo $keywoDefaultCurrencyName; ?> </td>
                            <td class="text-orange ">    <?php echo ucfirst($requestStatus); ?>  </td>
                            <td class="text-grayscale-4">  <?php echo date("jS M, Y",strtotime($requests['request_date'])); ?> </td>
                            <td><a onclick="showRequestDetails('<?php echo $requests['id']; ?>','<?php echo $requests['request_date']; ?>');">Details</a></td>
                            <td>  
                                <div class="btn-group action-btn " role="group ">   
                                    <button type="button " class="btn text-Light-Black innerLR padding-right-none " onclick="removeCashoutRequest('<?php echo $requests['status']; ?>','<?php echo $requests['id']; ?>','<?php echo $requests['request_date'];?>');">  <i class="fa fa-close text-grayscale-6"> </i>  
                                    </button>     
                                </div>              
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            <?php if(empty($cashoutRequests['request_container'])){ ?>
                <h4 style="text-align: center;">No relevant data found!!!</h4>
            <?php } ?>
        </div>
    </div>
    </div>
    <div class="row keyword-marketplace-data innerT inner-2x">
    <div class="col-md-5">
        <div class="common-note pull-left innerMTB">Note : Data will be updated in every 24 hours</div>
    </div>
    <div class="col-md-7">
        <div class="pagination-cont pull-right">

            <ul class="pagination">

                <?php if(!empty($cashoutRequests['forword_cursor'])){ ?>
                    <li ><a onclick="showNewerRequest('<?php echo $cashoutRequests['forword_cursor']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <?php }else{ ?>
                    <li class="disabled"><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-left" aria-hidden="true"></i></a></li>
                <?php } ?>

                <?php if(!empty($cashoutRequests['backward_cursor'])){ ?>
                <li><a onclick="showOlderRequest('<?php echo $cashoutRequests['backward_cursor']; ?>');" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                <?php }else{ ?>
                    <li class="disabled"><a href="#" class="prev-arrow-pagination"><i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                <?php } ?>

                
                
            </ul>
        </div>
    </div>
</div>